/********************************************************************
 * Copyright (C) 2014 Aricent Inc . All Rights Reserved
 *
 * $Id: mplsnpapi.c,v 1.1 2014/04/30 09:28:14 siva Exp
 *
 * Description:This file contains the wrapper for
 *             for Hardware API's w.r.t MPLS
 *             <Part of dual node stacking model>
 *
 *******************************************************************/

#ifdef MPLS_WANTED
#ifndef _MPLSNP_C_
#include "mplsincs.h"
#include "nputil.h"


/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwEnableMpls                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwEnableMpls
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwEnableMpls
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwEnableMpls ()
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_ENABLE_MPLS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwDisableMpls                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwDisableMpls
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwDisableMpls
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwDisableMpls ()
{
    tFsHwNp             FsHwNp;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_DISABLE_MPLS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwGetPwCtrlChnlCapabilities                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwGetPwCtrlChnlCapabilities
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwGetPwCtrlChnlCapabilities
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwGetPwCtrlChnlCapabilities (UINT1 *pu1PwCCTypeCapabs)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwGetPwCtrlChnlCapabilities *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_GET_PW_CTRL_CHNL_CAPABILITIES,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwGetPwCtrlChnlCapabilities;

    pEntry->pu1PwCCTypeCapabs = pu1PwCCTypeCapabs;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwCreatePwVc                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwCreatePwVc
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwCreatePwVc
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwCreatePwVc (UINT4 u4VpnId, tMplsHwVcTnlInfo * pMplsHwVcInfo,
                        UINT4 u4Action)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwCreatePwVc *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_CREATE_PW_VC,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwCreatePwVc;

    pEntry->u4VpnId = u4VpnId;
    pEntry->pMplsHwVcInfo = pMplsHwVcInfo;
    pEntry->u4Action = u4Action;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwTraversePwVc                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwTraversePwVc
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwTraversePwVc
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwTraversePwVc (tMplsHwVcTnlInfo * pMplsHwVcInfo,
                          tMplsHwVcTnlInfo * pNextMplsHwVcInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwTraversePwVc *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_TRAVERSE_PW_VC,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwTraversePwVc;

    pEntry->pMplsHwVcInfo = pMplsHwVcInfo;
    pEntry->pNextMplsHwVcInfo = pNextMplsHwVcInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwGetPwVc                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwGetPwVc
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwGetPwVc
 *                                                                          
 *    Output(s)           : pMplsHwVcInfo                                           
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwGetPwVc (tMplsHwVcTnlInfo * pMplsHwVcInfo,
                          tMplsHwVcTnlInfo * pNextMplsHwVcInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwTraversePwVc *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_GET_PW_VC,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwTraversePwVc;

    pEntry->pMplsHwVcInfo = pMplsHwVcInfo;
    pEntry->pNextMplsHwVcInfo = pNextMplsHwVcInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwDeletePwVc                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwDeletePwVc
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwDeletePwVc
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwDeletePwVc (UINT4 u4VpnId, tMplsHwVcTnlInfo * pMplsHwDelVcInfo,
                        UINT4 u4Action)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwDeletePwVc *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_DELETE_PW_VC,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwDeletePwVc;

    pEntry->u4VpnId = u4VpnId;
    pEntry->pMplsHwDelVcInfo = pMplsHwDelVcInfo;
    pEntry->u4Action = u4Action;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwCreateILM                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwCreateILM
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwCreateILM
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwCreateILM (tMplsHwIlmInfo * pMplsHwIlmInfo, UINT4 u4Action)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwCreateILM *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_CREATE_I_L_M,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwCreateILM;

    pEntry->pMplsHwIlmInfo = pMplsHwIlmInfo;
    pEntry->u4Action = u4Action;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwTraverseILM                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwTraverseILM
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwTraverseILM
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwTraverseILM (tMplsHwIlmInfo * pMplsHwIlmInfo,
                         tMplsHwIlmInfo * pNextMplsHwIlmInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwTraverseILM *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_TRAVERSE_I_L_M,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwTraverseILM;

    pEntry->pMplsHwIlmInfo = pMplsHwIlmInfo;
    pEntry->pNextMplsHwIlmInfo = pNextMplsHwIlmInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwGetILM                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwGetILM
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwGetILM
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwGetILM (tMplsHwIlmInfo * pMplsHwIlmInfo,
                         tMplsHwIlmInfo * pNextMplsHwIlmInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwTraverseILM *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_GET_I_L_M,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwTraverseILM;

    pEntry->pMplsHwIlmInfo = pMplsHwIlmInfo;
    pEntry->pNextMplsHwIlmInfo = pNextMplsHwIlmInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwDeleteILM                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwDeleteILM
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwDeleteILM
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwDeleteILM (tMplsHwIlmInfo * pMplsHwDelIlmParams, UINT4 u4Action)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwDeleteILM *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_DELETE_I_L_M,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwDeleteILM;

    pEntry->pMplsHwDelIlmParams = pMplsHwDelIlmParams;
    pEntry->u4Action = u4Action;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwCreateL3FTN                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwCreateL3FTN
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwCreateL3FTN
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwCreateL3FTN (UINT4 u4VpnId, tMplsHwL3FTNInfo * pMplsHwL3FTNInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwCreateL3FTN *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_CREATE_L3_F_T_N,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwCreateL3FTN;

    pEntry->u4VpnId = u4VpnId;
    pEntry->pMplsHwL3FTNInfo = pMplsHwL3FTNInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwTraverseL3FTN                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwTraverseL3FTN
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwTraverseL3FTN
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwTraverseL3FTN (tMplsHwL3FTNInfo * pMplsHwL3FTNInfo,
                           tMplsHwL3FTNInfo * pNextMplsHwL3FTNInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwTraverseL3FTN *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_TRAVERSE_L3_F_T_N,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwTraverseL3FTN;

    pEntry->pMplsHwL3FTNInfo = pMplsHwL3FTNInfo;
    pEntry->pNextMplsHwL3FTNInfo = pNextMplsHwL3FTNInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *
 *    Function Name       : MplsFsMplsHwGetL3FTN
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes MplsFsMplsHwGetL3FTN
 *
 *    Input(s)            : Arguments of FsMplsHwGetL3FTN
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/
UINT1
MplsFsMplsHwGetL3FTN (UINT4 u4VpnId, UINT4 u4IfIndex,
                       tMplsHwL3FTNInfo * pMplsHwL3FTNInfo)
 {
     tFsHwNp                    FsHwNp;
     tMplsNpModInfo             *pMplsNpModInfo = NULL;
     tMplsNpWrFsMplsHwGetL3FTN  *pEntry = NULL;
     NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                          NP_MPLS_MOD,    /* Module ID */
                          FS_MPLS_HW_GET_L3_F_T_N,    /* Function/OpCode */
                          0,        /* IfIndex value if applicable */
                          0,        /* No. of Port Params */
                          0);    /* No. of PortList Parms */
     pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
     pEntry = &pMplsNpModInfo->MplsNpFsMplsHwGetL3FTN;

     pEntry->u4VpnId = u4VpnId;
     pEntry->u4IfIndex = u4IfIndex;
     pEntry->pMplsHwL3FTNInfo = pMplsHwL3FTNInfo;

     if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
     {
         return (FNP_FAILURE);
     }
     return (FNP_SUCCESS);
}
#ifdef NP_BACKWD_COMPATIBILITY

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwDeleteL3FTN                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwDeleteL3FTN
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwDeleteL3FTN
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwDeleteL3FTN (UINT4 u4L3EgrIntf)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwDeleteL3FTN *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_DELETE_L3_F_T_N,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwDeleteL3FTN;

    pEntry->u4L3EgrIntf = u4L3EgrIntf;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
#else

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwDeleteL3FTN                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwDeleteL3FTN
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwDeleteL3FTN
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwDeleteL3FTN (UINT4 u4VpnId, tMplsHwL3FTNInfo * pMplsHwL3FTNInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwDeleteL3FTN *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_DELETE_L3_F_T_N,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwDeleteL3FTN;

    pEntry->u4VpnId = u4VpnId;
    pEntry->pMplsHwL3FTNInfo = pMplsHwL3FTNInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
#endif

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsWpHwDeleteL3FTN                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsWpHwDeleteL3FTN
 *                                                                          
 *    Input(s)            : Arguments of FsMplsWpHwDeleteL3FTN
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsWpHwDeleteL3FTN (UINT4 u4VpnId, tMplsHwL3FTNInfo * pMplsHwL3FTNInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsWpHwDeleteL3FTN *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_WP_HW_DELETE_L3_F_T_N,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsWpHwDeleteL3FTN;

    pEntry->u4VpnId = u4VpnId;
    pEntry->pMplsHwL3FTNInfo = pMplsHwL3FTNInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwEnableMplsIf                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwEnableMplsIf
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwEnableMplsIf
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwEnableMplsIf (UINT4 u4Intf, tMplsIntfParamsSet * pMplsIntfParamsSet)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwEnableMplsIf *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_ENABLE_MPLS_IF,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwEnableMplsIf;

    pEntry->u4Intf = u4Intf;
    pEntry->pMplsIntfParamsSet = pMplsIntfParamsSet;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwDisableMplsIf                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwDisableMplsIf
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwDisableMplsIf
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwDisableMplsIf (UINT4 u4Intf)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwDisableMplsIf *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_DISABLE_MPLS_IF,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwDisableMplsIf;

    pEntry->u4Intf = u4Intf;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwCreateVplsVpn                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwCreateVplsVpn
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwCreateVplsVpn
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwCreateVplsVpn (UINT4 u4VplsInstance, UINT4 u4VpnId,
                           tMplsHwVplsInfo * pMplsHwVplsVpnInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwCreateVplsVpn *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_CREATE_VPLS_VPN,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwCreateVplsVpn;

    pEntry->u4VplsInstance = u4VplsInstance;
    pEntry->u4VpnId = u4VpnId;
    pEntry->pMplsHwVplsVpnInfo = pMplsHwVplsVpnInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwDeleteVplsVpn                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwDeleteVplsVpn
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwDeleteVplsVpn
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwDeleteVplsVpn (UINT4 u4VplsInstance, UINT4 u4VpnId,
                           tMplsHwVplsInfo * pMplsHwVplsVpnInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwDeleteVplsVpn *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_DELETE_VPLS_VPN,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwDeleteVplsVpn;

    pEntry->u4VplsInstance = u4VplsInstance;
    pEntry->u4VpnId = u4VpnId;
    pEntry->pMplsHwVplsVpnInfo = pMplsHwVplsVpnInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwVplsAddPwVc                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwVplsAddPwVc
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwVplsAddPwVc
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwVplsAddPwVc (UINT4 u4VplsInstance, UINT4 u4VpnId,
                         tMplsHwVplsInfo * pMplsHwVplsVcInfo, UINT4 u4Action)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwVplsAddPwVc *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_VPLS_ADD_PW_VC,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwVplsAddPwVc;

    pEntry->u4VplsInstance = u4VplsInstance;
    pEntry->u4VpnId = u4VpnId;
    pEntry->pMplsHwVplsVcInfo = pMplsHwVplsVcInfo;
    pEntry->u4Action = u4Action;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwVplsDeletePwVc                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwVplsDeletePwVc
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwVplsDeletePwVc
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwVplsDeletePwVc (UINT4 u4VplsInstance, UINT4 u4VpnId,
                            tMplsHwVplsInfo * pMplsHwVplsVcInfo, UINT4 u4Action)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwVplsDeletePwVc *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_VPLS_DELETE_PW_VC,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwVplsDeletePwVc;

    pEntry->u4VplsInstance = u4VplsInstance;
    pEntry->u4VpnId = u4VpnId;
    pEntry->pMplsHwVplsVcInfo = pMplsHwVplsVcInfo;
    pEntry->u4Action = u4Action;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwVplsFlushMac                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwVplsFlushMac
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwVplsFlushMac
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwVplsFlushMac (UINT4 u4VplsInstance, UINT4 u4VpnId,
                          tMplsHwVplsInfo * pMplsHwVplsVpnInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwVplsFlushMac *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_VPLS_FLUSH_MAC,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwVplsFlushMac;

    pEntry->u4VplsInstance = u4VplsInstance;
    pEntry->u4VpnId = u4VpnId;
    pEntry->pMplsHwVplsVpnInfo = pMplsHwVplsVpnInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
#ifdef HVPLS_WANTED
/***************************************************************************
 *
 *Function Name       : MplsFsMplsRegisterFwdAlarm
 *    
 *Description         : This function using its arguments populates the
 *                      generic NP structure and invokes NpUtilHwProgram
 *                      The Generic NP wrapper after validating the H/W
 *                      parameters invokes FsMplsRegisterFwdAlarm
 *         
 *Input(s)            : Arguments of FsMplsRegisterFwdAlarm
 *           
 *Output(s)           : None
 *         
 *Global Var Referred : None
 *               
 *Global Var Modified : None.
 *                 
 *Use of Recursion    : None.
 *                   
 *Exceptions or Operating
 *System Error Handling    : None.
 *          
 *Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 ******************************************************************************/

UINT1
MplsFsMplsRegisterFwdAlarm (UINT4 u4VpnId, tMplsHwVplsInfo *pMplsHwVplsVpnInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsRegisterFwdAlarm *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_REGISTER_FWD_ALARM,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsRegisterFwdAlarm;

    pEntry->u4VpnId = u4VpnId;
    pEntry->pMplsHwVplsVpnInfo = pMplsHwVplsVpnInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
/***************************************************************************
 *  
 *Function Name       : MplsFsMplsDeRegisterFwdAlarm
 *    
 *Description         : This function using its arguments populates the
 *                      generic NP structure and invokes NpUtilHwProgram
 *                      The Generic NP wrapper after validating the H/W
 *                      parameters invokes FsMplsDeRegisterFwdAlarm
 *         
 *Input(s)            : Arguments of FsMplsDeRegisterFwdAlarm
 *           
 *Output(s)           : None
 *             
 *Global Var Referred : None
 *               
 *Global Var Modified : None.
 *                 
 *Use of Recursion    : None.
 *                   
 *Exceptions or Operating
 *System Error Handling    : None.
 *                      
 *Returns            : FNP_SUCCESS OR FNP_FAILURE
 *                        
 *******************************************************************************/

UINT1
MplsFsMplsDeRegisterFwdAlarm (UINT4 u4VpnId)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsDeRegisterFwdAlarm *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_DE_REGISTER_FWD_ALARM,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsDeRegisterFwdAlarm;

    pEntry->u4VpnId = u4VpnId;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
#endif
/***************************************************************************
 *                                                                          
 *    Function Name       : MplsNpMplsCreateMplsInterface                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes NpMplsCreateMplsInterface
 *                                                                          
 *    Input(s)            : Arguments of NpMplsCreateMplsInterface
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsNpMplsCreateMplsInterface(tMplsHwMplsIntInfo *  pMplsHwMplsIntInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrNpMplsCreateMplsInterface *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         NP_MPLS_CREATE_MPLS_INTERFACE,    /* Function/OpCode */
                         pMplsHwMplsIntInfo->u4CfaIfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpNpMplsCreateMplsInterface;

    pEntry->pMplsHwMplsIntInfo = pMplsHwMplsIntInfo; 


    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwAddMplsRoute                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwAddMplsRoute
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwAddMplsRoute
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwAddMplsRoute (UINT4 u4VrId, UINT4 u4IpDestAddr,
                          UINT4 u4IpSubNetMask, tFsNpNextHopInfo routeEntry,
                          UINT1 *pbu1TblFull)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwAddMplsRoute *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_ADD_MPLS_ROUTE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwAddMplsRoute;

    pEntry->u4VrId = u4VrId;
    pEntry->u4IpDestAddr = u4IpDestAddr;
    pEntry->u4IpSubNetMask = u4IpSubNetMask;
    pEntry->routeEntry = routeEntry;
    pEntry->pbu1TblFull = pbu1TblFull;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwAddMplsIpv6Route                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwAddMplsIpv6Route
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwAddMplsIpv6Route
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwAddMplsIpv6Route (UINT4 u4VrId, UINT1 * pu1Ip6Prefix,
                              UINT1 u1PrefixLen, UINT1 * pu1NextHop,
                              UINT4 u4NHType, tFsNpIntInfo * pIntInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwAddMplsIpv6Route *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_ADD_MPLS_IPV6_ROUTE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwAddMplsIpv6Route;

    pEntry->u4VrId = u4VrId;
    pEntry->pu1Ip6Prefix = pu1Ip6Prefix;
    pEntry->u1PrefixLen = u1PrefixLen;
    pEntry->pu1NextHop = pu1NextHop;
    pEntry->u4NHType = u4NHType;
    pEntry->pIntInfo = pIntInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwDeleteMplsIpv6Route                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwDeleteMplsIpv6Route
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwDeleteMplsIpv6Route
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwDeleteMplsIpv6Route (UINT4 u4VrId, UINT1 *pu1Ip6Prefix,
                                 UINT1 u1PrefixLen, UINT1 *pu1NextHop,
                                 UINT4 u4IfIndex, tFsNpRouteInfo *pRouteInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwDeleteMplsIpv6Route *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_DELETE_MPLS_IPV6_ROUTE,    /* Function/OpCode */
                         u4IfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwDeleteMplsIpv6Route;

    pEntry->u4VrId = u4VrId;
    pEntry->pu1Ip6Prefix = pu1Ip6Prefix;
    pEntry->u1PrefixLen = u1PrefixLen;
    pEntry->pu1NextHop = pu1NextHop;
    pEntry->u4IfIndex = u4IfIndex;
    pEntry->pRouteInfo=pRouteInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}



/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwGetMplsStats                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwGetMplsStats
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwGetMplsStats
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwGetMplsStats (tMplsInputParams * pInputParams, tStatsType StatsType,
                          tStatsInfo * pStatsInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwGetMplsStats *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_GET_MPLS_STATS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwGetMplsStats;

    pEntry->pInputParams = pInputParams;
    pEntry->StatsType = StatsType;
    pEntry->pStatsInfo = pStatsInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwAddVpnAc                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwAddVpnAc
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwAddVpnAc
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwAddVpnAc (UINT4 u4VplsInstance, UINT4 u4VpnId,
                      tMplsHwVplsInfo * pMplsHwVplsVcInfo, UINT4 u4Action)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwAddVpnAc *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_ADD_VPN_AC,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwAddVpnAc;

    pEntry->u4VplsInstance = u4VplsInstance;
    pEntry->u4VpnId = u4VpnId;
    pEntry->pMplsHwVplsVcInfo = pMplsHwVplsVcInfo;
    pEntry->u4Action = u4Action;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwDeleteVpnAc                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwDeleteVpnAc
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwDeleteVpnAc
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwDeleteVpnAc (UINT4 u4VplsInstance, UINT4 u4VpnId,
                         tMplsHwVplsInfo * pMplsHwVplsVcInfo, UINT4 u4Action)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwDeleteVpnAc *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_DELETE_VPN_AC,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwDeleteVpnAc;

    pEntry->u4VplsInstance = u4VplsInstance;
    pEntry->u4VpnId = u4VpnId;
    pEntry->pMplsHwVplsVcInfo = pMplsHwVplsVcInfo;
    pEntry->u4Action = u4Action;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwGetVpnAc                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwGetVpnAc
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwGetVpnAc
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwGetVpnAc (UINT4 u4VplsInstance, UINT4 u4VpnId,
                         tMplsHwVplsInfo * pMplsHwVplsVcInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwGetVpnAc *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_GET_VPN_AC,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwGetVpnAc;

    pEntry->u4VplsInstance = u4VplsInstance;
    pEntry->u4VpnId = u4VpnId;
    pEntry->pMplsHwVplsVcInfo = pMplsHwVplsVcInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwP2mpAddILM                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwP2mpAddILM
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwP2mpAddILM
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwP2mpAddILM (UINT4 u4P2mpId, tMplsHwIlmInfo * pMplsHwP2mpIlmInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwP2mpAddILM *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_P2MP_ADD_I_L_M,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwP2mpAddILM;

    pEntry->u4P2mpId = u4P2mpId;
    pEntry->pMplsHwP2mpIlmInfo = pMplsHwP2mpIlmInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwP2mpRemoveILM                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwP2mpRemoveILM
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwP2mpRemoveILM
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwP2mpRemoveILM (UINT4 u4P2mpId, tMplsHwIlmInfo * pMplsHwP2mpIlmInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwP2mpRemoveILM *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_P2MP_REMOVE_I_L_M,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwP2mpRemoveILM;

    pEntry->u4P2mpId = u4P2mpId;
    pEntry->pMplsHwP2mpIlmInfo = pMplsHwP2mpIlmInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwP2mpTraverseILM                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwP2mpTraverseILM
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwP2mpTraverseILM
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwP2mpTraverseILM (UINT4 u4P2mpId,
                             tMplsHwIlmInfo * pMplsHwP2mpIlmInfo,
                             tMplsHwIlmInfo * pNextMplsHwP2mpIlmInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwP2mpTraverseILM *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_P2MP_TRAVERSE_I_L_M,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwP2mpTraverseILM;

    pEntry->u4P2mpId = u4P2mpId;
    pEntry->pMplsHwP2mpIlmInfo = pMplsHwP2mpIlmInfo;
    pEntry->pNextMplsHwP2mpIlmInfo = pNextMplsHwP2mpIlmInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwP2mpAddBud                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwP2mpAddBud
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwP2mpAddBud
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwP2mpAddBud (UINT4 u4P2mpId, UINT4 u4Action)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwP2mpAddBud *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_P2MP_ADD_BUD,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwP2mpAddBud;

    pEntry->u4P2mpId = u4P2mpId;
    pEntry->u4Action = u4Action;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwP2mpRemoveBud                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwP2mpRemoveBud
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwP2mpRemoveBud
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwP2mpRemoveBud (UINT4 u4P2mpId, UINT4 u4Action)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwP2mpRemoveBud *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_P2MP_REMOVE_BUD,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwP2mpRemoveBud;

    pEntry->u4P2mpId = u4P2mpId;
    pEntry->u4Action = u4Action;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwModifyPwVc                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwModifyPwVc
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwModifyPwVc
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwModifyPwVc (UINT4 u4VpnId, UINT4 u4OperActVpnId,
                        tMplsHwVcTnlInfo * pMplsHwVcInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwModifyPwVc *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_MODIFY_PW_VC,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwModifyPwVc;

    pEntry->u4VpnId = u4VpnId;
    pEntry->u4OperActVpnId = u4OperActVpnId;
    pEntry->pMplsHwVcInfo = pMplsHwVcInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

#ifdef MPLS_L3VPN_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwL3vpnIngressMap                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwL3vpnIngressMap
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwL3vpnIngressMap
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwL3vpnIngressMap (tMplsHwL3vpnIgressInfo * pMplsHwL3vpnIgressInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwL3vpnIngressMap *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_L3VPN_INGRESS_MAP,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwL3vpnIngressMap;

    pEntry->pMplsHwL3vpnIgressInfo = pMplsHwL3vpnIgressInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsHwL3vpnEgressMap                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsHwL3vpnEgressMap
 *                                                                          
 *    Input(s)            : Arguments of FsMplsHwL3vpnEgressMap
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsHwL3vpnEgressMap (tMplsHwL3vpnEgressInfo * pMplsHwL3vpnEgressInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsHwL3vpnEgressMap *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_L3VPN_EGRESS_MAP,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsHwL3vpnEgressMap;

    pEntry->pMplsHwL3vpnEgressInfo = pMplsHwL3vpnEgressInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
#endif /* MPLS_WANTED */
#endif
#endif
#ifdef MPLS_WANTED
#ifndef _MPLSNPX_C_

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsMbsmHwEnableMpls                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsMbsmHwEnableMpls
 *                                                                          
 *    Input(s)            : Arguments of FsMplsMbsmHwEnableMpls
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsMbsmHwEnableMpls (tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsMbsmHwEnableMpls *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_MBSM_HW_ENABLE_MPLS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwEnableMpls;

    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsMbsmHwDisableMpls                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsMbsmHwDisableMpls
 *                                                                          
 *    Input(s)            : Arguments of FsMplsMbsmHwDisableMpls
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsMbsmHwDisableMpls (tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsMbsmHwDisableMpls *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_MBSM_HW_DISABLE_MPLS,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwDisableMpls;

    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsMbsmHwGetPwCtrlChnlCapabilities                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsMbsmHwGetPwCtrlChnlCapabilities
 *                                                                          
 *    Input(s)            : Arguments of FsMplsMbsmHwGetPwCtrlChnlCapabilities
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsMbsmHwGetPwCtrlChnlCapabilities (UINT1 *pu1PwCCTypeCapabs)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsMbsmHwGetPwCtrlChnlCapabilities *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_MBSM_HW_GET_PW_CTRL_CHNL_CAPABILITIES,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwGetPwCtrlChnlCapabilities;

    pEntry->pu1PwCCTypeCapabs = pu1PwCCTypeCapabs;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsMbsmHwCreatePwVc                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsMbsmHwCreatePwVc
 *                                                                          
 *    Input(s)            : Arguments of FsMplsMbsmHwCreatePwVc
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsMbsmHwCreatePwVc (UINT4 u4VpnId, tMplsHwVcTnlInfo * pMplsHwVcInfo,
                            UINT4 u4Action, tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsMbsmHwCreatePwVc *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_MBSM_HW_CREATE_PW_VC,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwCreatePwVc;

    pEntry->u4VpnId = u4VpnId;
    pEntry->pMplsHwVcInfo = pMplsHwVcInfo;
    pEntry->u4Action = u4Action;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsMbsmHwDeletePwVc                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsMbsmHwDeletePwVc
 *                                                                          
 *    Input(s)            : Arguments of FsMplsMbsmHwDeletePwVc
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsMbsmHwDeletePwVc (UINT4 u4VpnId, tMplsHwVcTnlInfo * pMplsHwDelVcInfo,
                            UINT4 u4Action, tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsMbsmHwDeletePwVc *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_MBSM_HW_DELETE_PW_VC,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwDeletePwVc;

    pEntry->u4VpnId = u4VpnId;
    pEntry->pMplsHwDelVcInfo = pMplsHwDelVcInfo;
    pEntry->u4Action = u4Action;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsMbsmHwCreateILM                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsMbsmHwCreateILM
 *                                                                          
 *    Input(s)            : Arguments of FsMplsMbsmHwCreateILM
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsMbsmHwCreateILM (tMplsHwIlmInfo * pMplsHwIlmInfo, UINT4 u4Action,
                           tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsMbsmHwCreateILM *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_MBSM_HW_CREATE_I_L_M,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwCreateILM;

    pEntry->pMplsHwIlmInfo = pMplsHwIlmInfo;
    pEntry->u4Action = u4Action;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsMbsmHwDeleteILM                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsMbsmHwDeleteILM
 *                                                                          
 *    Input(s)            : Arguments of FsMplsMbsmHwDeleteILM
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsMbsmHwDeleteILM (tMplsHwIlmInfo * pMplsHwDelIlmParams, UINT4 u4Action,
                           tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsMbsmHwDeleteILM *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_MBSM_HW_DELETE_I_L_M,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwDeleteILM;

    pEntry->pMplsHwDelIlmParams = pMplsHwDelIlmParams;
    pEntry->u4Action = u4Action;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsMbsmHwCreateL3FTN                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsMbsmHwCreateL3FTN
 *                                                                          
 *    Input(s)            : Arguments of FsMplsMbsmHwCreateL3FTN
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsMbsmHwCreateL3FTN (UINT4 u4VpnId, tMplsHwL3FTNInfo * pMplsHwL3FTNInfo,
                             tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsMbsmHwCreateL3FTN *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_MBSM_HW_CREATE_L3_F_T_N,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwCreateL3FTN;

    pEntry->u4VpnId = u4VpnId;
    pEntry->pMplsHwL3FTNInfo = pMplsHwL3FTNInfo;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsMbsmHwDeleteL3FTN                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsMbsmHwDeleteL3FTN
 *                                                                          
 *    Input(s)            : Arguments of FsMplsMbsmHwDeleteL3FTN
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsMbsmHwDeleteL3FTN (UINT4 u4L3EgrIntf, tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsMbsmHwDeleteL3FTN *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_MBSM_HW_DELETE_L3_F_T_N,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwDeleteL3FTN;

    pEntry->u4L3EgrIntf = u4L3EgrIntf;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsMbsmHwCreateVplsVpn                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsMbsmHwCreateVplsVpn
 *                                                                          
 *    Input(s)            : Arguments of FsMplsMbsmHwCreateVplsVpn
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsMbsmHwCreateVplsVpn (UINT4 u4VplsInstance, UINT4 u4VpnId,
                               tMplsHwVplsInfo * pMplsHwVplsVpnInfo,
                               tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsMbsmHwCreateVplsVpn *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_MBSM_HW_CREATE_VPLS_VPN,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwCreateVplsVpn;

    pEntry->u4VplsInstance = u4VplsInstance;
    pEntry->u4VpnId = u4VpnId;
    pEntry->pMplsHwVplsVpnInfo = pMplsHwVplsVpnInfo;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsMbsmHwDeleteVplsVpn                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsMbsmHwDeleteVplsVpn
 *                                                                          
 *    Input(s)            : Arguments of FsMplsMbsmHwDeleteVplsVpn
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsMbsmHwDeleteVplsVpn (UINT4 u4VplsInstance, UINT4 u4VpnId,
                               tMplsHwVplsInfo * pMplsHwVplsVpnInfo,
                               tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsMbsmHwDeleteVplsVpn *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_MBSM_HW_DELETE_VPLS_VPN,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwDeleteVplsVpn;

    pEntry->u4VplsInstance = u4VplsInstance;
    pEntry->u4VpnId = u4VpnId;
    pEntry->pMplsHwVplsVpnInfo = pMplsHwVplsVpnInfo;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsMbsmHwVplsAddPwVc                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsMbsmHwVplsAddPwVc
 *                                                                          
 *    Input(s)            : Arguments of FsMplsMbsmHwVplsAddPwVc
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsMbsmHwVplsAddPwVc (UINT4 u4VplsInstance, UINT4 u4VpnId,
                             tMplsHwVplsInfo * pMplsHwVplsVcInfo,
                             UINT4 u4Action, tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsMbsmHwVplsAddPwVc *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_MBSM_HW_VPLS_ADD_PW_VC,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwVplsAddPwVc;

    pEntry->u4VplsInstance = u4VplsInstance;
    pEntry->u4VpnId = u4VpnId;
    pEntry->pMplsHwVplsVcInfo = pMplsHwVplsVcInfo;
    pEntry->u4Action = u4Action;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsMbsmHwVplsDeletePwVc                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsMbsmHwVplsDeletePwVc
 *                                                                          
 *    Input(s)            : Arguments of FsMplsMbsmHwVplsDeletePwVc
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsMbsmHwVplsDeletePwVc (UINT4 u4VplsInstance, UINT4 u4VpnId,
                                tMplsHwVplsInfo * pMplsHwVplsVcInfo,
                                UINT4 u4Action, tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsMbsmHwVplsDeletePwVc *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_MBSM_HW_VPLS_DELETE_PW_VC,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwVplsDeletePwVc;

    pEntry->u4VplsInstance = u4VplsInstance;
    pEntry->u4VpnId = u4VpnId;
    pEntry->pMplsHwVplsVcInfo = pMplsHwVplsVcInfo;
    pEntry->u4Action = u4Action;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsMbsmHwAddVpnAc                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsMbsmHwAddVpnAc
 *                                                                          
 *    Input(s)            : Arguments of FsMplsMbsmHwAddVpnAc
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsMbsmHwAddVpnAc (UINT4 u4VplsInstance, UINT4 u4VpnId,
                          tMplsHwVplsInfo * pMplsHwVplsVcInfo, UINT4 u4Action,
                          tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsMbsmHwAddVpnAc *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_MBSM_HW_ADD_VPN_AC,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwAddVpnAc;

    pEntry->u4VplsInstance = u4VplsInstance;
    pEntry->u4VpnId = u4VpnId;
    pEntry->pMplsHwVplsVcInfo = pMplsHwVplsVcInfo;
    pEntry->u4Action = u4Action;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsMbsmHwDeleteVpnAc                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsMbsmHwDeleteVpnAc
 *                                                                          
 *    Input(s)            : Arguments of FsMplsMbsmHwDeleteVpnAc
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsMbsmHwDeleteVpnAc (UINT4 u4VplsInstance, UINT4 u4VpnId,
                             tMplsHwVplsInfo * pMplsHwVplsVcInfo,
                             UINT4 u4Action, tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsMbsmHwDeleteVpnAc *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_MBSM_HW_DELETE_VPN_AC,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwDeleteVpnAc;

    pEntry->u4VplsInstance = u4VplsInstance;
    pEntry->u4VpnId = u4VpnId;
    pEntry->pMplsHwVplsVcInfo = pMplsHwVplsVcInfo;
    pEntry->u4Action = u4Action;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsNpMplsMbsmCreateMplsInterface                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes NpMplsMbsmCreateMplsInterface
 *                                                                          
 *    Input(s)            : Arguments of NpMplsMbsmCreateMplsInterface
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsNpMplsMbsmCreateMplsInterface (tMplsHwMplsIntInfo *  pMplsHwMplsIntInfo,tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrNpMplsMbsmCreateMplsInterface *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         NP_MPLS_MBSM_CREATE_MPLS_INTERFACE,    /* Function/OpCode */
                         pMplsHwMplsIntInfo->u4CfaIfIndex,    /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpNpMplsMbsmCreateMplsInterface;

    pEntry->pMplsHwMplsIntInfo = pMplsHwMplsIntInfo; 
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsMbsmHwP2mpAddILM                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsMbsmHwP2mpAddILM
 *                                                                          
 *    Input(s)            : Arguments of FsMplsMbsmHwP2mpAddILM
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsMbsmHwP2mpAddILM (UINT4 u4P2mpId, tMplsHwIlmInfo * pMplsHwP2mpIlmInfo,
                            tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsMbsmHwP2mpAddILM *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_MBSM_HW_P2MP_ADD_I_L_M,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwP2mpAddILM;

    pEntry->u4P2mpId = u4P2mpId;
    pEntry->pMplsHwP2mpIlmInfo = pMplsHwP2mpIlmInfo;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsMbsmHwP2mpRemoveILM                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsMbsmHwP2mpRemoveILM
 *                                                                          
 *    Input(s)            : Arguments of FsMplsMbsmHwP2mpRemoveILM
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsMbsmHwP2mpRemoveILM (UINT4 u4P2mpId,
                               tMplsHwIlmInfo * pMplsHwP2mpIlmInfo,
                               tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsMbsmHwP2mpRemoveILM *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_MBSM_HW_P2MP_REMOVE_I_L_M,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwP2mpRemoveILM;

    pEntry->u4P2mpId = u4P2mpId;
    pEntry->pMplsHwP2mpIlmInfo = pMplsHwP2mpIlmInfo;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
/***************************************************************************
 *
 *    Function Name       : MplsFsMplsMbsmHwGetVpnAc
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsMbsmHwGetVpnAc
 *
 *    Input(s)            : Arguments of FsMplsMbsmHwGetVpnAc
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

UINT1
MplsFsMplsMbsmHwGetVpnAc (UINT4 u4VplsInstance, UINT4 u4VpnId,
                         tMplsHwVplsInfo * pMplsHwVplsVcInfo,
                         tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsMbsmHwGetVpnAc *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_GET_VPN_AC,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwGetVpnAc;

    pEntry->u4VplsInstance = u4VplsInstance;
    pEntry->u4VpnId = u4VpnId;
    pEntry->pMplsHwVplsVcInfo = pMplsHwVplsVcInfo;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
/***************************************************************************
 *
 *    Function Name       : MplsFsMplsMbsmHwGetILM
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsMbsmHwGetILM
 *
 *    Input(s)            : Arguments of FsMplsMbsmHwGetILM
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

UINT1
MplsFsMplsMbsmHwGetILM (tMplsHwIlmInfo * pMplsHwIlmInfo,
                         tMplsHwIlmInfo * pNextMplsHwIlmInfo,
                         tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsMbsmHwTraverseILM *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_GET_I_L_M,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwTraverseILM;

    pEntry->pMplsHwIlmInfo = pMplsHwIlmInfo;
    pEntry->pNextMplsHwIlmInfo = pNextMplsHwIlmInfo;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}
/***************************************************************************
 *
 *    Function Name       : MplsFsMplsMbsmHwGetPwVc
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsMbsmHwGetPwVc
 *
 *    Input(s)            : Arguments of FsMplsMbsmHwGetPwVc
 *
 *    Output(s)           : pMplsHwVcInfo
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/

UINT1
MplsFsMplsMbsmHwGetPwVc (tMplsHwVcTnlInfo * pMplsHwVcInfo,
                          tMplsHwVcTnlInfo * pNextMplsHwVcInfo,
                          tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsMbsmHwTraversePwVc *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_HW_GET_PW_VC,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwTraversePwVc;

    pEntry->pMplsHwVcInfo = pMplsHwVcInfo;
    pEntry->pNextMplsHwVcInfo = pNextMplsHwVcInfo;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

#ifdef MPLS_L3VPN_WANTED

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsMbsmHwL3vpnIngressMap                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsMbsmHwL3vpnIngressMap
 *                                                                          
 *    Input(s)            : Arguments of FsMplsMbsmHwL3vpnIngressMap
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsMbsmHwL3vpnIngressMap (tMplsHwL3vpnIgressInfo *
                                 pMplsHwL3vpnIgressInfo,
                                 tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsMbsmHwL3vpnIngressMap *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_MBSM_HW_L3VPN_INGRESS_MAP,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwL3vpnIngressMap;

    pEntry->pMplsHwL3vpnIgressInfo = pMplsHwL3vpnIgressInfo;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsMplsMbsmHwL3vpnEgressMap                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsMplsMbsmHwL3vpnEgressMap
 *                                                                          
 *    Input(s)            : Arguments of FsMplsMbsmHwL3vpnEgressMap
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsMplsMbsmHwL3vpnEgressMap (tMplsHwL3vpnEgressInfo * pMplsHwL3vpnEgressInfo,
                                tMbsmSlotInfo * pSlotInfo)
{
    tFsHwNp             FsHwNp;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;
    tMplsNpWrFsMplsMbsmHwL3vpnEgressMap *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_MPLS_MOD,    /* Module ID */
                         FS_MPLS_MBSM_HW_L3VPN_EGRESS_MAP,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
    pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwL3vpnEgressMap;

    pEntry->pMplsHwL3vpnEgressInfo = pMplsHwL3vpnEgressInfo;
    pEntry->pSlotInfo = pSlotInfo;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

/***************************************************************************
 *
 *    Function Name       : MplsFsMplsMbsmHwGetL3FTN
 *
 *    Description         : This function using its arguments populates the
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes MplsFsMplsMbsmHwGetL3FTN
 *
 *    Input(s)            : Arguments of FsMplsMbsmHwGetL3FTN
 *
 *    Output(s)           : None
 *
 *    Global Var Referred : None
 *
 *    Global Var Modified : None.
 *
 *    Use of Recursion    : None.
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE
 *
 *****************************************************************************/
UINT1
MplsFsMplsMbsmHwGetL3FTN (UINT4 u4VpnId, UINT4 u4IfIndex,
                       tMplsHwL3FTNInfo * pMplsHwL3FTNInfo,
                       tMbsmSlotInfo * pSlotInfo)
 {
     tFsHwNp                    FsHwNp;
     tMplsNpModInfo             *pMplsNpModInfo = NULL;
     tMplsNpWrFsMplsMbsmHwGetL3FTN  *pEntry = NULL;
     NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                          NP_MPLS_MOD,    /* Module ID */
                          FS_MPLS_HW_GET_L3_F_T_N,    /* Function/OpCode */
                          0,        /* IfIndex value if applicable */
                          0,        /* No. of Port Params */
                          0);    /* No. of PortList Parms */
     pMplsNpModInfo = &(FsHwNp.MplsNpModInfo);
     pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwGetL3FTN;

     pEntry->u4VpnId = u4VpnId;
     pEntry->u4IfIndex = u4IfIndex;
     pEntry->pMplsHwL3FTNInfo = pMplsHwL3FTNInfo;
     pEntry->pSlotInfo = pSlotInfo;

     if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
     {
         return (FNP_FAILURE);
     }
     return (FNP_SUCCESS);
}
#endif /* MPLS_WANTED */
#endif
#endif
