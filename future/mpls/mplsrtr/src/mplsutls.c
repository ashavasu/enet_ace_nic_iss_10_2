/********************************************************************
 *                                                                  *
 * $RCSfile: mplsutls.c,v $
 *                                                                  *
 * $Id: mplsutls.c,v 1.29 2013/07/04 13:30:46 siva Exp $
 *                                                                  *
 * $Revision: 1.29 $                                                
 *                                                                  *
 *******************************************************************/

/*---------------------------------------------------------------------------*/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : mplsutls.c
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS   
 *    MODULE NAME            : MPLS_FM 
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains the utility function used by
 *                             the MPLS-FM
 *---------------------------------------------------------------------------*/

#include "mplsincs.h"
#include "l2vpincs.h"
#include "temacs.h"
#include "ldpext.h"
#include "l2vpextn.h"
#include "rpteext.h"
#include "teextrn.h"
#include "tcextrn.h"
#include "mplslsr.h"
#ifdef L2RED_WANTED
#include "mplsred.h"
#endif

/*****************************************************************************/
/* Function Name : MplsEnqueueMplsIncomingQ                                  */
/* Description   : Copies the message into buffer chain and enques to the    */
/*                 MPLS incoming Q                                           */
/* Input(s)      : au4IfInfo - 16 byte message to be enqueued                */
/* Output(s)     : None                                                      */
/* Return(s)     : MPLS_SUCCESS or MPLS_FAILURE                              */
/*****************************************************************************/

UINT1
MplsEnqueueMplsIncomingQ (UINT4 au4IfInfo[])
{
    tMplsBufChainHeader *pMsg = NULL;

    MPLSFM_DBG (MPLSFM_PRCS_ETEXT, "MplsEnqueueMplsIncomingQ : ENTRY \n");
    if (gu1MplsInitialised != TRUE)
    {
        MPLSFM_DBG (MPLSFM_PRCS_ETEXT,
                    "MplsEnqueueMplsIncomingQ : "
                    "MPLS is not initialised INTMD-EXIT \n");
        return (MPLS_FAILURE);
    }
    pMsg = MPLS_ALLOC_BUF_CHAIN (MPLS_DEF_BUF_SIZE, MPLS_DEF_BUF_OFF_SET);
    if (pMsg == NULL)
    {
        MPLSFM_DBG (MPLSFM_PRCS_ETEXT,
                    "MplsEnqueueMplsIncomingQ : INTMD-EXIT \n");
        return (MPLS_FAILURE);
    }

    if (MPLS_COPY_OVER_BUF_CHAIN (pMsg, (UINT1 *) au4IfInfo,
                                  MPLS_DEF_BUF_OFF_SET,
                                  MPLS_DEF_BUF_SIZE) == MPLS_BUF_FAILURE)
    {
        MPLS_REL_BUF_CHAIN (pMsg, MPLS_BUF_NORMAL_REL);
        MPLSFM_DBG (MPLSFM_PRCS_ETEXT,
                    "MplsEnqueueMplsIncomingQ : INTMD-EXIT \n");
        return (MPLS_FAILURE);
    }
    CRU_BUF_Set_SourceModuleId (pMsg, 0);
    CRU_BUF_Set_U2Reserved (pMsg, 0);
    if (MPLS_ENQUEUE (gFmInQId, (UINT1 *) (&pMsg),
                      OSIX_DEF_MSG_LEN) != MPLS_Q_SUCCESS)
    {

        MPLS_REL_BUF_CHAIN (pMsg, MPLS_BUF_NORMAL_REL);
        MPLSFM_DBG (MPLSFM_PRCS_ETEXT,
                    "MplsEnqueueMplsIncomingQ : INTMD-EXIT \n");
        return (MPLS_FAILURE);
    }

    if (MPLS_SEND_EVENT (gFmTaskId, MPLS_IN_MSG_Q_EVENT) != MPLS_Q_SUCCESS)
    {
        MPLSFM_DBG (MPLSFM_PRCS_ETEXT,
                    "MplsEnqueueMplsIncomingQ : INTMD-EXIT \n");
        return (MPLS_FAILURE);
    }

    MPLSFM_DBG (MPLSFM_PRCS_ETEXT, "MplsEnqueueMplsIncomingQ : EXIT \n");
    return (MPLS_SUCCESS);
}

/*****************************************************************************/
/* Function Name : MplsProcessGroupEvents                                    */
/* Description   : Process the Major Group events                            */
/* Input(s)      : None                                                      */
/* Output(s)     : au4IfInfo - Dequeued message                              */
/* Return(s)     : MPLS_SUCCESS or MPLS_FAILURE                              */
/*****************************************************************************/
INT4
MplsProcessGroupEvents (UINT4 au4IfInfo[])
{
    INT4                i4Rc = MPLS_FAILURE;

    switch (au4IfInfo[MPLS_EVENT] & MPLS_MAJOR_EVENT_MASK)
    {
        case MPLS_SNMP_GRP_EVENT:
            i4Rc = MplsProcessSnmpEvent (au4IfInfo);
            break;

        default:
            MPLSFM_DBG (MPLSFM_PRCS_PRCS, "Invalid Major Event\n");
    }

    return i4Rc;
}

/*****************************************************************************/
/* Function Name : MplsProcessSnmpEvent                                      */
/* Description   : Process the SNMP event given                              */
/* Input(s)      : au4IfInfo - Dequeued event                                */
/* Output(s)     : None                                                      */
/* Return(s)     : MPLS_SUCCESS or MPLS_FAILURE                              */
/*****************************************************************************/
INT4
MplsProcessSnmpEvent (UINT4 au4IfInfo[])
{
    INT4                i4Rc = MPLS_SUCCESS;

    /* Taking Semaphore to ensure data structure protection */
    MplsDsLock ();

    switch (au4IfInfo[MPLS_EVENT])
    {                            /* Minor Event */
        case MPLS_SNMP_ADMIN_STATUS_UP_EVENT:
            /* 
             * Initialise Tables and Interfaces for 
             * the specified incarnation 
             */
            if (MPLS_ADMIN_STATUS (au4IfInfo[MPLS_INCARN]) ==
                MPLS_ADMIN_STATUS_UP)
            {
                break;
            }
            if (MplsAllocateResources (au4IfInfo[MPLS_INCARN]) == MPLS_SUCCESS)
            {
                MPLS_ADMIN_STATUS (au4IfInfo[MPLS_INCARN]) =
                    MPLS_ADMIN_STATUS_UP;
            }
            else
            {
                i4Rc = MPLS_FAILURE;
            }
            break;
        case MPLS_SNMP_ADMIN_STATUS_DOWN_EVENT:
            if (MPLS_ADMIN_STATUS (au4IfInfo[MPLS_INCARN]) ==
                MPLS_ADMIN_STATUS_DOWN)
            {
                break;
            }

            MPLS_ADMIN_STATUS (au4IfInfo[MPLS_INCARN]) = MPLS_ADMIN_STATUS_DOWN;

            if (MplsRelResources (au4IfInfo[MPLS_INCARN]) != MPLS_SUCCESS)
            {
                i4Rc = MPLS_FAILURE;
            }
            break;

        default:
            MPLSFM_DBG (MPLSFM_PRCS_PRCS, "Invalid Minor Event\n");
    }

    MplsDsUnlock ();

    return i4Rc;
}

/*****************************************************************************/
/* Function Name : MplsCreateVcTableEntry                                    */
/* Description   : Opens the VC connection and UPdates the VC table          */
/* Input(s)      : u4Incarn -Incarnation                                     */
/*                 u1DirectionType - Direction type to be opened             */
/*                 u1EncapsType - Encapsulation type to be used on this      */
/*                                Vpi/Vci.                                   */
/*                 draft-ietf-mpls-atm-02 says, LLC/SNAP Encapsulation to be */
/*                 MUST be used for non-MPLS connection and NULL Encaps for  */
/*                 MPLS connection.                                          */
/*                 u4Label - For which label the connection to be opened     */
/*                 pIfEntry - IfIndex of pIfEntry is the Port number         */
/*                            that VC to be opened                           */
/*                 pTrfcParms - Points to the set of traffic parameters for  */
/*                              the VC to be opened.                         */
/* Output(s)     : pu4ConnHandle - Connection hanlder given by the MPOAAL5   */
/* Return(s)     : MPLS_SUCCESS or MPLS_FAILURE                              */
/*****************************************************************************/
#ifdef LANAI_WANTED
UINT1
MplsCreateVcTableEntry (UINT4 u4Incarn, UINT1 u1DirectionType,
                        UINT1 u1EncapsType, UINT4 u4Label,
                        tIfTableEntry * pIfEntry,
                        tMplsTrfcParms * pMplsTrfcParms, UINT4 *pu4ConnHandle)
{
    tVcTableEntry      *pVcTableEntry = NULL;
    UINT2               u2Vpi;
    UINT2               u2Vci;
    tMplsAtmConnInfo    MplsAtmConnInfo;

    MPLSFM_DBG (MPLSFM_PRCS_ETEXT, "MplsCreateVcTableEntry: ENTRY \n");
    MEMSET (&MplsAtmConnInfo, 0, sizeof (tMplsAtmConnInfo));

    if (pIfEntry == NULL)
    {
        MPLSFM_DBG (MPLSFM_PRCS_ETEXT, "MplsCreateVcTableEntry: INTMD-EXIT \n");
        return (MPLS_FAILURE);
    }
    pVcTableEntry = (tVcTableEntry *) MemAllocMemBlk (MPLS_VC_TBL_POOL_ID);
    if (pVcTableEntry == NULL)
    {
        MPLSFM_DBG (MPLSFM_PRCS_ETEXT, "MplsCreateVcTableEntry: INTMD-EXIT \n");
        return (MPLS_FAILURE);
    }

    u2Vci = (UINT2) (u4Label & MPLS_VCI_MASK);
    u2Vpi = (UINT2) ((u4Label & MPLS_VPI_MASK) >> MPLS_2_BYTE_SHIFT);

    MplsAtmConnInfo.u4PortNumber = pIfEntry->u4IfIndex;
    MplsAtmConnInfo.u1TrafficType = MPLS_UBR;
    MplsAtmConnInfo.u2Vpi = u2Vpi;
    MplsAtmConnInfo.u2Vci = u2Vci;
    MplsAtmConnInfo.u1DirectionType = u1DirectionType;
    MplsAtmConnInfo.u1EncapsType = u1EncapsType;
    MplsAtmConnInfo.u1ConnType = MPLS_PVC;
    MplsAtmConnInfo.u1ProtocolType = MPLS_IPV4;
    MplsAtmConnInfo.pMplsTrfcParms = pMplsTrfcParms;

    if (MplsUpdateAtmConnection (&MplsAtmConnInfo, MPLS_ATMX_OPR_ADD)
        == MPLS_FAILURE)
    {
        MemReleaseMemBlock (MPLS_VC_TBL_POOL_ID, (UINT1 *) pVcTableEntry);
        return (MPLS_FAILURE);
    }

    *pu4ConnHandle = MplsAtmConnInfo.u4Handle;

    pVcTableEntry->pIfEntry = pIfEntry;
    pVcTableEntry->u4ConnHandle = *pu4ConnHandle;
    pVcTableEntry->u4Label = u4Label;
    pVcTableEntry->u2Vci = MplsAtmConnInfo.u2Vci;
    pVcTableEntry->u2Vpi = MplsAtmConnInfo.u2Vpi;
    pVcTableEntry->u4PortNumber = MplsAtmConnInfo.u4PortNumber;
    pVcTableEntry->pMplsTrfcParms = MplsAtmConnInfo.pMplsTrfcParms;
    pVcTableEntry->u1TrafficType = MplsAtmConnInfo.u1TrafficType;
    pVcTableEntry->u1DirectionType = MplsAtmConnInfo.u1DirectionType;
    pVcTableEntry->u1EncapsType = MplsAtmConnInfo.u1EncapsType;
    pVcTableEntry->u1ConnType = MplsAtmConnInfo.u1ConnType;
    pVcTableEntry->u2ProtocolType = MplsAtmConnInfo.u1ProtocolType;
    pVcTableEntry->u2UsedCount = MPLS_ONE;

    MPLS_SLL_ADD (MPLS_VC_TABLE_LIST (u4Incarn),
                  (tMplsSllNode *) pVcTableEntry);
    MPLSFM_DBG (MPLSFM_PRCS_ETEXT, "MplsCreateVcTableEntry: EXIT \n");
    return (MPLS_SUCCESS);
}

/*****************************************************************************/
/* Function Name : MplsGetVcTableEntry                                       */
/* Description   : Search for the entry based on the u4ConnHandle            */
/* Input(s)      : u4IncarnNum - Incarnation number                          */
/*                 u4ConnHandle - Search based on this connection handle     */
/* Output(s)     : ppVcTableEntry - Pointer to the entry if exits            */
/*                               - NULL if entry doesn't exists              */
/* Return(s)     : MPLS_SUCCESS or MPLS_FAILURE                              */
/*****************************************************************************/
UINT1
MplsGetVcTableEntry (UINT4 u4IncarnNum, UINT4 u4ConnHandle,
                     tVcTableEntry ** ppVcTableEntry)
{
    tMplsSllNode       *pSllNode = NULL;

    MPLSFM_DBG (MPLSFM_PRCS_ETEXT, "MplsGetVcTableEntry : ENTRY \n");
    /* MplsSllScan macro is a "for" loop */
    MPLS_SLL_SCAN (MPLS_VC_TABLE_LIST (u4IncarnNum), pSllNode, tMplsSllNode *)
    {
        *ppVcTableEntry = (tVcTableEntry *) pSllNode;
        if ((*ppVcTableEntry)->u4ConnHandle == u4ConnHandle)
        {
            MPLSFM_DBG (MPLSFM_PRCS_ETEXT, "MplsGetVcTableEntry : EXIT \n");
            return (MPLS_SUCCESS);
        }
    }
    *ppVcTableEntry = (tVcTableEntry *) NULL;
    MPLSFM_DBG (MPLSFM_PRCS_ETEXT, "MplsGetVcTableEntry : INTMD-EXIT \n");
    return (MPLS_FAILURE);
}

/*****************************************************************************/
/* Function Name : MplsGetVcEntryForLabel                                    */
/* Description   : Search for the entry based on the u4Label                 */
/* Input(s)      : u4IncarnNum - Incarnation number                          */
/*                 u4Label      - Search based on this connection handle     */
/* Output(s)     : ppVcTableEntry - Pointer to the entry if exits            */
/*                               - NULL if entry doesn't exists              */
/* Return(s)     : MPLS_SUCCESS or MPLS_FAILURE                              */
/*****************************************************************************/

UINT1
MplsGetVcEntryForLabel (UINT4 u4IncarnNum,
                        UINT4 u4Label, UINT4 u4IfIndex,
                        tVcTableEntry ** ppVcTableEntry)
{
    tMplsSllNode       *pSllNode = NULL;

    MPLSFM_DBG (MPLSFM_PRCS_ETEXT, "MplsGetVcEntryForLabel : ENTRY \n");
    /* MplsSllScan macro is a "for" loop */
    MPLS_SLL_SCAN (MPLS_VC_TABLE_LIST (u4IncarnNum), pSllNode, tMplsSllNode *)
    {
        *ppVcTableEntry = (tVcTableEntry *) pSllNode;
        if (((*ppVcTableEntry)->u4Label == u4Label) &&
            ((*ppVcTableEntry)->pIfEntry->u4IfIndex == u4IfIndex))
        {
            MPLSFM_DBG (MPLSFM_PRCS_ETEXT, "MplsGetVcEntryForLabel : EXIT \n");
            return (MPLS_SUCCESS);
        }
    }
    *ppVcTableEntry = (tVcTableEntry *) NULL;
    MPLSFM_DBG (MPLSFM_PRCS_ETEXT, "MplsGetVcEntryForLabel : INTMD-EXIT \n");
    return (MPLS_FAILURE);
}

/*****************************************************************************/
/* Function Name : MplsDeleteVcTableEntry                                    */
/* Description   : Deletion of the VC table entry of Input connection Handle */
/* Input(s)      : u4Incarn     - Incarnation                                */
/*                 u4ConnHandle - Connection handle                          */
/* Output(s)     : None                                                      */
/* Return(s)     : MPLS_SUCCESS or MPLS_FAILURE                              */
/*****************************************************************************/
UINT1
MplsDeleteVcTableEntry (UINT4 u4Incarn, UINT4 u4ConnHandle)
{
    tVcTableEntry      *pVcTableEntry = NULL;
    tMplsAtmConnInfo    MplsAtmConnInfo;
    MPLSFM_DBG (MPLSFM_PRCS_ETEXT, "MplsDeleteVcTableEntry : ENTRY \n");

    MEMSET (&MplsAtmConnInfo, 0, sizeof (tMplsAtmConnInfo));
    if (MplsGetVcTableEntry (u4Incarn, u4ConnHandle, &pVcTableEntry)
        == MPLS_SUCCESS)
    {
        MplsAtmConnInfo.u4Handle = u4ConnHandle;
        MplsUpdateAtmConnection (&MplsAtmConnInfo, MPLS_ATMX_OPR_DEL);
        MPLS_SLL_DELETE (MPLS_VC_TABLE_LIST (u4Incarn),
                         (tMplsSllNode *) pVcTableEntry);
        MemReleaseMemBlock (MPLS_VC_TBL_POOL_ID, (UINT1 *) pVcTableEntry);
        MPLSFM_DBG (MPLSFM_PRCS_ETEXT, "MplsDeleteVcTableEntry : EXIT \n");
        return (MPLS_SUCCESS);
    }
    MPLSFM_DBG (MPLSFM_PRCS_ETEXT, "MplsDeleteVcTableEntry : INTMD-EXIT \n");
    return (MPLS_FAILURE);
}
#endif

/*****************************************************************************/
/* Function Name : MplsCheckFmAdminStatus                                    */
/* Description   : Checks the Admin Status of MPLS-FM for a given            */
/*                 Incarnation  for the incoming AdminStaus value.           */
/*                 return MPLS_SUCCESS if the incoming AdminStatus value     */
/*                 matches the Admin Status of MPLS-FM for the given Incarn. */
/* Input(s)      : u4Incarn - Incarnation number                             */
/*               : u1AdminStatus - Incoming AdminStatus value to be checked  */
/*               : u1AdminStatus can have values -                           */
/*                                          MPLS_ADMIN_STATUS_UP,            */
/*                                          MPLS_ADMIN_STATUS_DOWN,          */
/*                                          MPLS_ADMIN_STATUS_UP_IN_PRGRS,   */
/*                                          MPLS_ADMIN_STATUS_DOWN_IN_PRGRS. */
/* Output(s)     : None.                                                     */
/* Return(s)     : Returns MPLS_SUCCESS in case of Admin Status is UP else   */
/*                 returns MPLS_FAILURE.                                     */
/*****************************************************************************/

UINT1
MplsCheckFmAdminStatus (UINT4 u4Incarn, UINT1 u1AdminStatus)
{
    MPLSFM_DBG (MPLSFM_PRCS_ETEXT, "MplsCheckFmAdminStatus: ENTRY \n");
    if (MplsValidateIncarnId (u4Incarn) != MPLS_SUCCESS)
    {
        MPLSFM_DBG (MPLSFM_PRCS_ETEXT, "MplsCheckFmAdminStatus: INTMD-EXIT \n");
        return MPLS_FAILURE;
    }
    switch (u1AdminStatus)
    {
        case MPLS_ADMIN_STATUS_UP:
            if (MPLS_ADMIN_STATUS (u4Incarn) == MPLS_ADMIN_STATUS_UP)
            {
                MPLSFM_DBG (MPLSFM_PRCS_ETEXT,
                            "MplsCheckFmAdminStatus: EXIT \n");
                return MPLS_SUCCESS;
            }
            break;
        case MPLS_ADMIN_STATUS_DOWN:
            if (MPLS_ADMIN_STATUS (u4Incarn) == MPLS_ADMIN_STATUS_DOWN)
            {
                MPLSFM_DBG (MPLSFM_PRCS_ETEXT,
                            "MplsCheckFmAdminStatus: EXIT \n");
                return MPLS_SUCCESS;
            }
            break;
        case MPLS_ADMIN_STATUS_UP_IN_PRGRS:
            if (MPLS_ADMIN_STATUS (u4Incarn) == MPLS_ADMIN_STATUS_UP_IN_PRGRS)
            {
                MPLSFM_DBG (MPLSFM_PRCS_ETEXT,
                            "MplsCheckFmAdminStatus: EXIT \n");
                return MPLS_SUCCESS;
            }
            break;
        case MPLS_ADMIN_STATUS_DOWN_IN_PRGRS:
            if (MPLS_ADMIN_STATUS (u4Incarn) == MPLS_ADMIN_STATUS_DOWN_IN_PRGRS)
            {
                MPLSFM_DBG (MPLSFM_PRCS_ETEXT,
                            "MplsCheckFmAdminStatus: EXIT \n");
                return MPLS_SUCCESS;
            }
            break;
        default:
            break;
    }
    MPLSFM_DBG (MPLSFM_PRCS_ETEXT, "MplsCheckFmAdminStatus: INTMD-EXIT \n");
    return MPLS_FAILURE;
}

/* Generic Functions used by low level routines */
/****************************************************************************
 Function    :  MplsValidateIncarnId
 Input       :  u4IncarnId - Incarnation Id
 Output      :  The Routines Validates the given Incarnation Id
 Returns     :  MPLS_SUCCESS or MPLS_FAILURE
****************************************************************************/
UINT4
MplsValidateIncarnId (UINT4 u4IncarnId)
{
    if (u4IncarnId < MPLS_MAX_INCARNS)
    {
        return MPLS_SUCCESS;
    }

    return MPLS_FAILURE;
}

/****************************************************************************
 Function    :  MplsValidateElspMapIndex
 Input       :  u4IncarnId - Incarnation Id
                u4ElspMapId
                u1Exp
 Output      :  The Routines Validates the given ElspMapIndex and Exp value
 Returns     :  MPLS_SUCCESS or MPLS_FAILURE
*****************************************************************************/
UINT4
MplsValidateElspMapIndex (UINT4 u4IncarnId, UINT4 u4ElspMapId, UINT1 u1Exp)
{
    if (MplsValidateIncarnId (u4IncarnId) != MPLS_SUCCESS)
    {
        return MPLS_FAILURE;
    }

    /* To validate the Nhlfe Info Index Range, the CrgVarGroup should be 
     * configured */
    if (MPLS_ADMIN_STATUS (u4IncarnId) != MPLS_ADMIN_STATUS_UP)
    {
        return MPLS_FAILURE;
    }

    if (MPLS_QOS_POLICY (u4IncarnId) != MPLS_DIFF_SERV)
    {
        return MPLS_FAILURE;
    }

    if (u4ElspMapId >= MPLS_DIFFSERV_ELSP_MAP_ENTRIES (u4IncarnId))
    {
        return MPLS_FAILURE;
    }

    if (u1Exp >= MPLS_DIFFSERV_MAX_EXP)
    {
        return MPLS_FAILURE;
    }

    return MPLS_SUCCESS;
}

/****************************************************************************
 Function    :  MplsValidateElspMapIndex
 Input       :  u4IncarnId - Incarnation Id
                u4DiffParamsId
 Output      :  The Routines Validates the given DiffServParams Index
 Returns     :  MPLS_SUCCESS or MPLS_FAILURE
*****************************************************************************/
UINT4
MplsValidateDiffServParamsIndex (UINT4 u4IncarnId, UINT4 u4DiffParamsId)
{
    if (MplsValidateIncarnId (u4IncarnId) != MPLS_SUCCESS)
    {
        return MPLS_FAILURE;
    }

    /* To validate the Nhlfe Info Index Range, the CrgVarGroup should be 
     * configured */
    if (MPLS_ADMIN_STATUS (u4IncarnId) != MPLS_ADMIN_STATUS_UP)
    {
        return MPLS_FAILURE;
    }

    if (MPLS_QOS_POLICY (u4IncarnId) != MPLS_DIFF_SERV)
    {
        return MPLS_FAILURE;
    }

    if (u4DiffParamsId >= MPLS_DIFFSERV_PARAMS_ENTRIES (u4IncarnId))
    {
        return MPLS_FAILURE;
    }

    return MPLS_SUCCESS;
}

/****************************************************************************
 Function    :  MplsGetDiffServParamsIndexNext
 Input       :  u4IncarnId - Incarnation Id
                pu4NextDiffParamsId
 Output      :  The Routine gets the Next available DiffServParams Index for 
                creation
 Returns     :  MPLS_SUCCESS or MPLS_FAILURE
*****************************************************************************/
UINT4
MplsGetDiffServParamsIndexNext (UINT4 u4IncarnId, UINT4 *pu4NextDiffParamsId)
{
    if (MPLS_DIFFSERV_PARAMS_TOKEN_STACK_TOP (u4IncarnId) > 0)
    {
        /* Stack top actually points to one index above the top.  The next
         * available free label lies one index below stack top
         */
        *pu4NextDiffParamsId = MPLS_DIFFSERV_PARAMS_TOKEN_STACK
            (u4IncarnId, MPLS_DIFFSERV_PARAMS_TOKEN_STACK_TOP (u4IncarnId) - 1);
        return MPLS_SUCCESS;
    }
    return MPLS_FAILURE;
}

/****************************************************************************
 Function    :  MplsGetDiffServElspMapIndexNext
 Input       :  u4IncarnId - Incarnation Id
                pu4NextElspMapId
 Output      :  The Routine gets the Next available Elsp Map Index for 
                creation
 Returns     :  MPLS_SUCCESS or MPLS_FAILURE
*****************************************************************************/
UINT4
MplsGetDiffServElspMapIndexNext (UINT4 u4IncarnId, UINT4 *pu4NextElspMapId)
{
    if (MPLS_DIFFSERV_ELSP_TOKEN_STACK_TOP (u4IncarnId) > 0)
    {
        /* Stack top actually points to one index above the top.  The next
         * available free label lies one index below stack top
         */
        *pu4NextElspMapId = MPLS_DIFFSERV_ELSP_TOKEN_STACK
            (u4IncarnId, MPLS_DIFFSERV_ELSP_TOKEN_STACK_TOP (u4IncarnId) - 1);
        return MPLS_SUCCESS;
    }
    return MPLS_FAILURE;
}

/*****************************************************************************/
/* Function Name : MplsDsLock                                                  */
/* Description   : This function is used to take the mutual                  */
/*                 exclusion SEMa4 to avoid simultaneous access to           */
/*                 protocol data structures by the protocol task and         */
/*                 configuration task/thread.                                */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : MPLS_SUCCESS or MPLS_FAILURE                              */
/*****************************************************************************/
INT4
MplsDsLock (VOID)
{
    /*Taking Semaphore to ensure data structure protection */
    if (MPLS_TAKE_SEM ((tOsixSemId) gDsSemId) != MPLS_SEM_SUCCESS)
    {
        MPLSFM_DBG (MPLSFM_IF_ETEXT, "Take DS Semaphore Failed \n");
        MPLSFM_DBG (MPLSFM_IF_ETEXT, "MplsDsLock : INTMD-EXIT \n");
        return MPLS_FAILURE;
    }
    return MPLS_SUCCESS;
}

/*****************************************************************************/
/* Function Name : MplsDsUnlock                                                */
/* Description   : This function is used to release the mutual               */
/*                 exclusion Semaphore already taken avoid simultaneous      */
/*                 access to protocol data structures by the protocol task   */
/*                 and configuration task/thread.                            */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : MPLS_SUCCESS or MPLS_FAILURE                              */
/*****************************************************************************/
INT4
MplsDsUnlock (VOID)
{
    /*Releasing the Data Dtructure Protection Semaphore */
    if (MPLS_RELEASE_SEM ((tOsixSemId) gDsSemId) != MPLS_SEM_SUCCESS)
    {
        MPLSFM_DBG (MPLSFM_IF_ETEXT, "Release DS Semaphore Failed \n");
        MPLSFM_DBG (MPLSFM_IF_ETEXT, "MplsDsUnlock : INTMD-EXIT \n");
        return MPLS_FAILURE;
    }
    return MPLS_SUCCESS;
}

/*****************************************************************************/
/* Function Name : MplsConfLock                                              */
/* Description   : This function is used to take the mutual                  */
/*                 exclusion SEMa4 to avoid simultaneous access to           */
/*                 protocol data structures by the protocol task and         */
/*                 configuration task/thread.                                */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : MPLS_SUCCESS or MPLS_FAILURE                              */
/*****************************************************************************/
INT4
MplsConfLock (VOID)
{
    /*Taking Configuration Threads protection Semaphore */
    if (MPLS_TAKE_SEM ((tOsixSemId) gConfSemId) != MPLS_SEM_SUCCESS)
    {
        MPLSFM_DBG (MPLSFM_IF_ETEXT, "Take Config Semaphore Failed \n");
        MPLSFM_DBG (MPLSFM_IF_ETEXT, "MplsConfLock : INTMD-EXIT \n");
        return MPLS_FAILURE;
    }
    return MPLS_SUCCESS;
}

/*****************************************************************************/
/* Function Name : MplsConfUnlock                                            */
/* Description   : This function is used to release the mutual               */
/*                 exclusion Semaphore already taken avoid simultaneous      */
/*                 access to protocol data structures by the protocol task   */
/*                 and configuration task/thread.                            */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : MPLS_SUCCESS or MPLS_FAILURE                              */
/*****************************************************************************/
INT4
MplsConfUnlock (VOID)
{
    /*Releasing theConfiguration Threads Protection Semaphore */
    if (MPLS_RELEASE_SEM ((tOsixSemId) gConfSemId) != MPLS_SEM_SUCCESS)
    {
        MPLSFM_DBG (MPLSFM_IF_ETEXT, "Release COnfig Semaphore Failed \n");
        MPLSFM_DBG (MPLSFM_IF_ETEXT, "MplsConfUnlock : INTMD-EXIT \n");
        return MPLS_FAILURE;
    }
    return MPLS_SUCCESS;
}

/*****************************************************************************/
/* Function Name : MplsShutdown                                              */
/* Description   : This function is used to release all memory consumed by   */
/*                 Mpls moudle                                               */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : MPLS_SUCCESS or MPLS_FAILURE                              */
/*****************************************************************************/
VOID
MplsShutdown (VOID)
{
    /* This is a partial shutdown. reload command required more memory in 
     * target so we have removed all the mempools from the system */
#ifdef MPLS_SIG_WANTED
    MPLS_LDP_LOCK ();
    MPLS_RSVPTE_LOCK ();
#endif
#ifdef MPLS_L2VPN_WANTED
    MPLS_L2VPN_LOCK ();
#endif
    MPLS_CMN_LOCK ();
#ifdef MPLS_L2VPN_WANTED
    L2vpnSizingMemDeleteMemPools ();
#endif
#ifdef MPLS_SIG_WANTED
    LdpDelMemPools ();
    RsvpeTeDeleteMemPools ();
#endif
    TeDeleteMemPools ();
    MplsCommondbDeInit ();
    IndexMgrDeInit ();
    LblMgrShutDown ();
}
