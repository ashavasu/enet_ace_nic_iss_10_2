/********************************************************************
* Copyright (C) 2012 Aricent Inc . All Rights Reserved
*
* $Id: mplsgt.c,v 1.2 2012/06/08 11:05:41 siva Exp $
*
* Description: This file contains the common Get routines for MPLS
*********************************************************************/

#include "mplsincs.h"

/****************************************************************************
Function    :  MplsDsTeGetAllClassTypeTable
Input       :  u4ClassTypeIndex  - Class Type Index
               pGetDsTeClassType - Pointer to Class Type entry
               u4ObjectId        - Object ID
Description :  This function gets the Class Type Table objects
Output      :  None 
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
MplsDsTeGetAllClassTypeTable (UINT4 u4ClassTypeIndex,
                                tMplsDsTeClassType *pGetDsTeClassType)
{
    tMplsDsTeClassType ClassEntry;

    MEMSET (&ClassEntry, MPLS_ZERO, sizeof(tMplsDsTeClassType));

    ClassEntry = MPLS_DIFFSERV_CLASS_TYPE_ENTRY (MPLS_DEF_INCARN, 
                                                 u4ClassTypeIndex);

    if (ClassEntry.i4DsTeClassTypeRowStatus == MPLS_ZERO)
    {
        return SNMP_FAILURE;
    }

    pGetDsTeClassType->i4DsTeClassTypeRowStatus = 
        ClassEntry.i4DsTeClassTypeRowStatus; 
    pGetDsTeClassType->i4DsTeClassTypeBwPercent = 
        ClassEntry.i4DsTeClassTypeBwPercent;
   
    MEMCPY (pGetDsTeClassType->au1DsTeClassTypeDesc, ClassEntry.au1DsTeClassTypeDesc,
            STRLEN (ClassEntry.au1DsTeClassTypeDesc));

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  MplsDsTeGetAllClassTypeTcMapTable
Input       :  u4ClassTypeIndex       - Class type index
               u4ClassTcMapIndex      - ClassMap Index
               pGetDsTeClassTypeTcMap - Pointer to Traffic class map entry 
               u4ObjectId             - Object ID
Description :  This function gets Traffic class map table objects
Output      :  None 
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
MplsDsTeGetAllClassTypeTcMapTable (UINT4 u4ClassTypeIndex,
                                     UINT4 u4ClassTcMapIndex,
                                tMplsDsTeClassTypeTcMap *pGetDsTeClassTypeTcMap)
{
    tMplsDsTeClassTypeTcMap TeClassTypeTcMap;

    MEMSET (&TeClassTypeTcMap, MPLS_ZERO, sizeof (tMplsDsTeClassTypeTcMap));

    TeClassTypeTcMap = MPLS_DIFFSERV_TRAFFIC_CLASS_MAP (MPLS_DEF_INCARN, 
                                        u4ClassTypeIndex, u4ClassTcMapIndex);

    if (TeClassTypeTcMap.i4DsTeClassTypeToTcRowStatus == MPLS_ZERO)
    {
        return SNMP_FAILURE;
    }
    pGetDsTeClassTypeTcMap->u4DsTeTcType = TeClassTypeTcMap.u4DsTeTcType;

    MEMCPY (pGetDsTeClassTypeTcMap->au1DsTeClassTypeToTcDesc, 
            TeClassTypeTcMap.au1DsTeClassTypeToTcDesc,
            STRLEN (pGetDsTeClassTypeTcMap->au1DsTeClassTypeToTcDesc));
    
    pGetDsTeClassTypeTcMap->i4DsTeClassTypeToTcRowStatus = 
        TeClassTypeTcMap.i4DsTeClassTypeToTcRowStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  MplsDsTeGetAllTeclassMapTable
Input       :  u4ClassTypeIndex   - Class Type Index
               u4ClassTeClassPrio - TE class priority
               pGetDsTeClassPrio  - Pointer to TE class map entry
               u4ObjectId         - Object ID
Description :  This function gets all the objects in TE class map table
Output      :  None 
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
MplsDsTeGetAllTeclassMapTable (UINT4 u4ClassTypeIndex,
                                  UINT4 u4ClassTeClassPrio,
                                  tMplsDsTeClassPrio *pGetDsTeClassPrio)
{
    tMplsDsTeClassPrio TeClassTePrio;

    MEMSET (&TeClassTePrio, MPLS_ZERO, sizeof (tMplsDsTeClassPrio));

    TeClassTePrio = MPLS_DIFFSERV_TE_CLASS_MAP (MPLS_DEF_INCARN, 
                                           u4ClassTypeIndex, u4ClassTeClassPrio);

    if (TeClassTePrio.i4DsTeCRowStatus == DESTROY)
    {
        return SNMP_FAILURE;
    }
    MEMCPY(pGetDsTeClassPrio->au1DsTeCDesc, TeClassTePrio.au1DsTeCDesc, 
           STRLEN (TeClassTePrio.au1DsTeCDesc));

    pGetDsTeClassPrio->u4TeClassNumber = TeClassTePrio.u4TeClassNumber;

    pGetDsTeClassPrio->i4DsTeCRowStatus = TeClassTePrio.i4DsTeCRowStatus;

    return SNMP_SUCCESS;
}


