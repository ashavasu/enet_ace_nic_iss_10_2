/**(******************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mplsred.c,v 1.47 2014/11/28 02:13:13 siva Exp $
 *
 * Description:This file contains the MPLS redundancy related routines 
 *********************************************************************/
#include "mplsincs.h"
#include "mplsnp.h"
#include "mpls.h"
#include "mplcmndb.h"
#include "mplslsr.h"
#include "mplsftn.h"
#include "l2vpincs.h"
#include "mplsnpwr.h"
 
#define MPLS_RM_POOL_ID MPLSRTRMemPoolIds[MAX_MPLSRTR_RM_FRAME_SIZING_ID]
tOsixTaskId         gMplsHwAuditTskId = 0;
tMplsNodeStatus     gMplsNodeStatus;
tOsixQId            gMplsRmQId;
UINT1               gu1MplsRmInit = FALSE;
#ifdef NPAPI_WANTED
#ifndef LDP_GR_WANTED
static tFtnEntry   *MplsCheckFTNEntry (UINT4 MplsShimLabel);
static tInSegment  *MplsCheckILMEntry (UINT4 MplsShimLabel);
#endif
/* MPLS_P2MP_LSP_CHANGES - S */
#ifdef MPLS_TEST_WANTED
tOutSegment        *MplsCheckP2mpILMEntry (UINT4 u4OutLabel);
#else
static tOutSegment *MplsCheckP2mpILMEntry (UINT4 u4OutLabel);
#endif
/* MPLS_P2MP_LSP_CHANGES - E */
#endif
/*****************************************************************************/
/* Function Name : MplsRmInit                                                */
/* Description   : Registers MPLS with RM                                    */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : MPLS_SUCCESS or MPLS_FAILURE                              */
/*****************************************************************************/
UINT1
MplsRmInit (VOID)
{
#ifdef L2RED_WANTED
    MPLS_NODE_STATUS () = MPLS_NODE_IDLE;
#else
    MPLS_NODE_STATUS () = MPLS_NODE_ACTIVE;
#endif

    /* Register with RM module, to get event and messages from RM. */
    if (MplsRegisterWithRM () != MPLS_SUCCESS)
    {
        /* Indicate the status of initialization to the main routine */
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "INIT:Failed to Register with RM\t\n");
        return MPLS_FAILURE;
    }
    gu1MplsRmInit = TRUE;
    return MPLS_SUCCESS;
}

/*****************************************************************************/
/* Function Name : MplsRmDeInit                                              */
/* Description   : Deregisters MPLS with RM                                  */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : MPLS_SUCCESS or MPLS_FAILURE                              */
/*****************************************************************************/
UINT1
MplsRmDeInit (VOID)
{
    tMplsRmFrame       *pMplsRmFrame = NULL;

    gu1MplsRmInit = FALSE;
    while (OsixQueRecv (gMplsRmQId, (UINT1 *) (&pMplsRmFrame),
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        /* Release the buffer to pool */
        MemReleaseMemBlock (MPLS_RM_POOL_ID, (UINT1 *) pMplsRmFrame);
    }
    /* Register with RM module, to get event and messages from RM. */
    if (MplsDeRegisterWithRM () != MPLS_SUCCESS)
    {
        /* Indicate the status of initialization to the main routine */
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "INIT:Failed to Register with RM\t\n");
        return MPLS_FAILURE;
    }
#ifdef L2RED_WANTED
    MPLS_NODE_STATUS () = MPLS_NODE_IDLE;
#else
    MPLS_NODE_STATUS () = MPLS_NODE_ACTIVE;
#endif
    return MPLS_SUCCESS;
}

/*****************************************************************************/
/* Function Name : MplsRmTaskMain                                            */
/* Description   : This is the Entry point function for the MPLS RM module.  */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
MplsRedHandleRmEvents (VOID)
{
    tMplsRmFrame       *pMplsRmFrame = NULL;
    tMplsNodeStatus     MplsPrevNodeState = MPLS_NODE_IDLE;
    tRmProtoEvt         ProtoEvt;

    ProtoEvt.u4AppId = RM_MPLS_APP_ID;
    ProtoEvt.u4Error = RM_NONE;

    while (OsixQueRecv (gMplsRmQId, (UINT1 *) (&pMplsRmFrame),
                        OSIX_DEF_MSG_LEN, OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        /* In future if we go for signalling switch will be useful */
        /*otherwise if condition is ok. */
        switch (pMplsRmFrame->u1Event)
        {
            case GO_ACTIVE:

                if (MPLS_NODE_STATUS () == MPLS_NODE_ACTIVE)
                {
                    break;
                }
                MplsPrevNodeState = MPLS_NODE_STATUS ();

                MplsRmMakeNodeActive ();

                if (MplsPrevNodeState == MPLS_NODE_IDLE)
                {
                    ProtoEvt.u4Event = RM_IDLE_TO_ACTIVE_EVT_PROCESSED;
                }
                else
                {
                    ProtoEvt.u4Event = RM_STANDBY_TO_ACTIVE_EVT_PROCESSED;
                }

                RmApiHandleProtocolEvent (&ProtoEvt);

                break;
            case GO_STANDBY:
                if (MPLS_NODE_STATUS () == MPLS_NODE_STANDBY)
                {
                    break;
                }
                MPLS_NODE_STATUS () = MPLS_NODE_STANDBY;
                ProtoEvt.u4Event = RM_STANDBY_EVT_PROCESSED;
                RmApiHandleProtocolEvent (&ProtoEvt);
                break;
            case RM_INIT_HW_AUDIT:
#ifdef NPAPI_WANTED
                if (MPLS_NODE_STATUS () == MPLS_NODE_ACTIVE)
                {
                    if (gMplsHwAuditTskId == 0)
                    {
                        OsixTskCrt (MPLSRM_TSK_NAME,
                                    MPLSRM_TASK_PRIORITY,
                                    OSIX_DEFAULT_STACK_SIZE,
                                    (OsixTskEntry) MplsRmHwAuditMain,
                                    0, &gMplsHwAuditTskId);
                    }
                }
#endif
                break;

            case RM_MESSAGE:
                /* Release allocated memory by RM to Memory Pool */
                RM_FREE (pMplsRmFrame->pFrame);
                break;

            case RM_STANDBY_UP:
            case RM_STANDBY_DOWN:
                /* Release allocated memory by RM to Memory Pool */
                RmReleaseMemoryForMsg ((UINT1 *) pMplsRmFrame->pFrame);
                break;

            default:
                break;
        }
        MemReleaseMemBlock (MPLS_RM_POOL_ID, (UINT1 *) pMplsRmFrame);
    }
}

/*****************************************************************************/
/* Function Name      : MplsRegisterWithRM                                   */
/*                                                                           */
/* Description        : Registers MPLS with RM by providing an application   */
/*                      ID for MPLS and a call back function to be called    */
/*                      whenever RM needs to send an event to MPLS.          */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if registration is success then MPLS_SUCCESS         */
/*                      Otherwise MPLS_FAILURE                               */
/*****************************************************************************/
INT4
MplsRegisterWithRM (VOID)
{
    tRmRegParams        RmRegParams;

    RmRegParams.u4EntId = RM_MPLS_APP_ID;
    RmRegParams.pFnRcvPkt = MplsRcvPktFromRm;
    if (RmRegisterProtocols (&RmRegParams) == RM_FAILURE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsRegisterWithRM: Registration with RM FAILED\t\n");
        return MPLS_FAILURE;
    }
    return MPLS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : MplsDeRegisterWithRM                                 */
/*                                                                           */
/* Description        : Deregisters MPLS with RM                             */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if deregistration is success then MPLS_SUCCESS       */
/*                      Otherwise MPLS_FAILURE                               */
/*****************************************************************************/
INT4
MplsDeRegisterWithRM (VOID)
{
    if (RmDeRegisterProtocols (RM_MPLS_APP_ID) == RM_FAILURE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsDeRegisterWithRM: Deregistration with RM FAILED\t\n");
        return MPLS_FAILURE;
    }
    return MPLS_SUCCESS;
}

/*****************************************************************************/
/* Function Name      :MplsRcvPktFromRm                                      */
/*                                                                           */
/* Description        : This function constructs a message containing the    */
/*                      given RM event and RM message and post it to the     */
/*                      MPLS queue.                                          */
/*                                                                           */
/* Input(s)           : u1Event - Event given by RM module                   */
/*                      pData   - Msg to be enqueue                          */
/*                      u2DataLen - Msg size                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : if msg is enqueued and event sent then MPLS_SUCCESS  */
/*                      Otherwise MPLS_FAILURE                               */
/*****************************************************************************/
INT4
MplsRcvPktFromRm (UINT1 u1Event, tRmMsg * pData, UINT2 u2DataLen)
{
    tMplsRmFrame       *pMplsRmFrame = NULL;

    if (((u1Event == RM_MESSAGE) || (u1Event == RM_STANDBY_UP) ||
         (u1Event == RM_STANDBY_DOWN)) && (pData == NULL))
    {
        /* Message absent and hence no need to process and no need
         * to send anything to MPLS RED task. */

        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsRcvPktFromRm: RED Message is absent\t\n");
        return RM_FAILURE;
    }
    if (gu1MplsRmInit != TRUE)
    {
        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            RmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsRcvPktFromRm: MPLS RM is not initialised\t\n");
        return RM_FAILURE;
    }

    pMplsRmFrame = (tMplsRmFrame *) MemAllocMemBlk (MPLS_RM_POOL_ID);

    if (pMplsRmFrame == NULL)
    {
        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            RmReleaseMemoryForMsg ((UINT1 *) pData);
        }

        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsRcvPktFromRm: Failed to Allocate Memory\t\n");
        return RM_FAILURE;
    }
    MEMSET (pMplsRmFrame, 0, sizeof (tMplsRmFrame));
    pMplsRmFrame->pFrame = pData;
    pMplsRmFrame->u2Length = u2DataLen;
    pMplsRmFrame->u1Event = u1Event;

    if (OsixQueSend (gMplsRmQId, (UINT1 *) (&pMplsRmFrame),
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (MPLS_RM_POOL_ID, (UINT1 *) pMplsRmFrame);
        if (u1Event == RM_MESSAGE)
        {
            RM_FREE (pData);
        }
        else if ((u1Event == RM_STANDBY_UP) || (u1Event == RM_STANDBY_DOWN))
        {
            RmReleaseMemoryForMsg ((UINT1 *) pData);
        }
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Failed to EnQ the Msg to MPLSRM Q\t\n");
        return RM_FAILURE;
    }

    if (OsixEvtSend (gFmTaskId, MPLS_RM_EVENT) != OSIX_SUCCESS)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Failed to Post the Event\t\n");
        return RM_FAILURE;
    }

    return RM_SUCCESS;
}

/*****************************************************************************/
/* Function Name      :MplsRmMakeNodeActive.                                 */
/*                                                                           */
/* Description        : This function brings up the standby card to the      */
/*                      same state as the ACTIVE card.                       */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           :                                                      */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           :                                                      */
/*                                                                           */
/* Return Value(s)    : MPLS_SUCCESS - On success                              */
/*                      MPLS_FAILURE - On failure.                             */
/*****************************************************************************/
INT4
MplsRmMakeNodeActive (VOID)
{
    if (MPLS_NODE_STATUS () == MPLS_NODE_ACTIVE)
    {
        return MPLS_SUCCESS;
    }
    if (MPLS_NODE_STATUS () == MPLS_NODE_IDLE)
    {
        /* Make the node as active. */
        MPLS_NODE_STATUS () = MPLS_NODE_ACTIVE;
        /* Node is coming up first time. Hence initialize the
         * hardware. */
#ifdef NPAPI_WANTED
        if (MplsFsMplsHwEnableMpls () == FNP_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "Failed in Hardware initialization\t\n");
            return MPLS_FAILURE;
        }
#endif
/*enable the mpls module */
/*by default enabled, so no need of enabling the module */
        return MPLS_SUCCESS;
    }
    if (MPLS_NODE_STATUS () == MPLS_NODE_STANDBY)
    {
        MPLS_NODE_STATUS () = MPLS_NODE_ACTIVE;
    }
    return MPLS_SUCCESS;
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/* Function Name      : MplsRmHwAuditMain                                    */
/*                                                                           */
/* Description        : This function spawns the audit task. Audit task      */
/*                      audits the information present in sw and the         */
/*                      information present in hw and syncs hw with that of  */
/*                      sw.                                                  */
/*                                                                           */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
MplsRmHwAuditMain (INT1 *pi1Param)
{
    UNUSED_PARAM (pi1Param);
#ifdef LDP_GR_WANTED
    /* Lock taken for entire audit, since 
     * for dynamic entries Hw list might change if msg
     * comes from the peer */
    MPLS_CMN_LOCK ();

    MplsFTNTableAudit ();
    MplsILMTableAudit ();

    MPLS_CMN_UNLOCK ();    
#else
     MplsFTNTableAudit ();
     MplsILMTableAudit ();
#endif
#ifdef L2VPN_HA_WANTED
	/*VPLS Table Audit*/
	MplsVplsTableAudit ();
    /*PW Table Auditing */
    MplsPWTableAudit ();
#endif
    /*P2MP Table Auditing */
    MplsP2mpILMTableAudit ();    /* MPLS_P2MP_LSP_CHANGES */
    gMplsHwAuditTskId = 0;
}

/*****************************************************************************/
/* Function Name      : MplsFTNTableAudit                                    */
/*                                                                           */
/* Description        : This function audits the information present in      */
/*                      FTN sw and the information present in FTN hw and     */
/*                      syncs hw with that of sw.                            */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
#ifdef LDP_GR_WANTED
VOID
MplsFTNTableAudit (VOID)
{
    tFTNHwListEntry     *pFTNHwListEntry = NULL;
    tMplsIpAddress      FTNHwListFec;
    UINT4               u4NetMask = 0;
    UINT1               u1FtnHwListPrefLen = 0;
    tFtnEntry          *pFtnEntry = NULL;
    UINT4               u4Index = 0;
    UINT4               u4Prefix = 0;

#ifdef MPLS_IPV6_WANTED
    tIp6Addr            Ipv6Prefix;
    tIp6Addr            Ipv6NetMask;
#endif

    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsFTNTableAudit ENTRY\n");

    MEMSET (&FTNHwListFec, 0, sizeof(tMplsIpAddress));

#ifdef MPLS_IPV6_WANTED
    MEMSET(&Ipv6NetMask,0,sizeof(tIp6Addr));
    MEMSET(&Ipv6Prefix,0,sizeof(tIp6Addr));
#endif

    /* The Audit will take not take care of RSVP entries as of now
     * since Hw List contains only entries for NON-TE LSP's */

    /* Scan the FTN Hw List for incompletely programmed entries */
    pFTNHwListEntry = MplsHwListGetFirstFTNHwListEntry ();
    while (pFTNHwListEntry != NULL)
    {
        /* Check if the entry is incompletely programmed,
         * Delete both static and dynamic entries which
         * are half programmed */

        if (!(MPLS_CHECK_FTN_HW_LIST_NP_BITS (pFTNHwListEntry)))
        {
            /* For static entries, MARK the Hw Status as FALSE */
            if (MPLS_FTN_HW_LIST_IS_STATIC (pFTNHwListEntry) == MPLS_TRUE)
            {

#ifdef MPLS_IPV6_WANTED
                if (MPLS_FTN_HW_LIST_FEC_ADDR_TYPE (pFTNHwListEntry) == MPLS_IPV6_ADDR_TYPE)
                {
                    MEMCPY (&Ipv6Prefix, &MPLS_IPV6_FTN_HW_LIST_FEC (pFTNHwListEntry), IPV6_ADDR_LENGTH);
                    pFtnEntry = MplsSignalGetFtnIpv6TableEntry (&Ipv6Prefix);
                    if (pFtnEntry != NULL)
                    {
                        pFtnEntry->u1HwStatus = MPLS_FALSE;
                    }
                }
                else
#endif
                {
                    MEMCPY (&u4Prefix, MPLS_FTN_HW_LIST_FEC (pFTNHwListEntry), IPV4_ADDR_LENGTH);
                    pFtnEntry = MplsSignalGetFtnTableEntry (u4Prefix);
                    if (pFtnEntry != NULL)
                    {
                        pFtnEntry->u1HwStatus = MPLS_FALSE;
                    }
                }
            }
            if (MplsHwListDeleteFTNEntryFromHw (pFTNHwListEntry, NULL) == MPLS_FAILURE)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsFTNTableAudit: MplsHwListDeleteFTNEntryFromHw Failed\n");
            }
        }
        pFTNHwListEntry = MplsHwListGetNextFTNHwListEntry (pFTNHwListEntry);
    }

    /* Check for those entries, which are found in the control plane,
     * but not found in the data plane */

    for (u4Index = MPLS_ONE; u4Index <= gu4MplsDbMaxFtnEntries; u4Index++)
    {
        pFtnEntry = MplsGetFtnTableEntry (u4Index);
        if (pFtnEntry == NULL)
        {
            continue;
        }
        FTNHwListFec.i4IpAddrType = pFtnEntry->u1AddrType;

#ifdef MPLS_IPV6_WANTED
        if (pFtnEntry->u1AddrType == MPLS_IPV6_ADDR_TYPE)
	{
		MEMCPY (&Ipv6NetMask, pFtnEntry->DestAddrMax.u1_addr,IPV6_ADDR_LENGTH);
		MEMCPY (&FTNHwListFec.IpAddress, pFtnEntry->DestAddrMin.u1_addr, IPV6_ADDR_LENGTH);
		u1FtnHwListPrefLen=MplsGetIpv6Subnetmasklen(Ipv6NetMask.u1_addr);

		pFTNHwListEntry = MplsHwListGetFTNHwListEntry (FTNHwListFec,
				u1FtnHwListPrefLen);
	}
	else
#endif
	{

		MEMCPY (&u4NetMask, pFtnEntry->DestAddrMax.u4_addr, IPV4_ADDR_LENGTH);
		MEMCPY (&FTNHwListFec.IpAddress, pFtnEntry->DestAddrMin.u4_addr, IPV4_ADDR_LENGTH);
		MPLS_GET_PRFX_LEN_FROM_MASK (u4NetMask, u1FtnHwListPrefLen);

		pFTNHwListEntry = MplsHwListGetFTNHwListEntry (FTNHwListFec,
				u1FtnHwListPrefLen);
	}

        if ((pFTNHwListEntry == NULL) &&
            (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE) &&
            (pFtnEntry->u1RowStatus == MPLS_STATUS_ACTIVE))
        {
            MplsFTNHwAdd (pFtnEntry, NULL);
        }
    }
     CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsFTNTableAudit EXIT\n");
}
#else
VOID
MplsFTNTableAudit (VOID)
{
    tFtnEntry          *pFtnEntry = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4Index = 0;
    tMplsHwL3FTNInfo    MplsHwL3FTNInfo;
    tMplsHwL3FTNInfo    NextMplsHwL3FTNInfo;
    UINT4               u4VpnId = MPLS_DEF_VRF;

    MEMSET (&MplsHwL3FTNInfo, 0, sizeof (tMplsHwL3FTNInfo));
    MEMSET (&NextMplsHwL3FTNInfo, 0, sizeof (tMplsHwL3FTNInfo));

    /*FTN Table Scanning */
    MplsFsMplsHwTraverseL3FTN (&MplsHwL3FTNInfo, &NextMplsHwL3FTNInfo);
    if (NextMplsHwL3FTNInfo.MplsLabelList[0].u.MplsShimLabel != 0)
    {
        while (NextMplsHwL3FTNInfo.MplsLabelList[0].u.MplsShimLabel != 0)
        {
            MPLS_CMN_LOCK ();
            pFtnEntry = MplsCheckFTNEntry (NextMplsHwL3FTNInfo.
                                           MplsLabelList[0].u.MplsShimLabel);
            if (pFtnEntry == NULL)
            {
                pTeTnlInfo = TeGetTunnelEntryFromLabel (NextMplsHwL3FTNInfo.
                                                        MplsLabelList[0].u.
                                                        MplsShimLabel);
            }
            if ((pFtnEntry == NULL) &&
                (pTeTnlInfo == NULL) &&
                (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE))
            {
                /* No Entry in Control Plane so, Delete Data Plane */
                MplsFsMplsWpHwDeleteL3FTN (u4VpnId, &NextMplsHwL3FTNInfo);
            }
            else
            {
                if (pFtnEntry != NULL)
                {
                    pFtnEntry->u1HwStatus = MPLS_TRUE;
                }
            }
            MPLS_CMN_UNLOCK ();
            MplsHwL3FTNInfo = NextMplsHwL3FTNInfo;
            MEMSET (&NextMplsHwL3FTNInfo, 0, sizeof (tMplsHwL3FTNInfo));
            MplsFsMplsHwTraverseL3FTN (&MplsHwL3FTNInfo, &NextMplsHwL3FTNInfo);
        }
    }
    /* Software table scanning for H/w Table addition */
    /* TODO Software table scanning should be handled for Tunnels also. */
    for (u4Index = MPLS_ONE; u4Index <= gu4MplsDbMaxFtnEntries; u4Index++)
    {
        MPLS_CMN_LOCK ();
        pFtnEntry = MplsGetFtnTableEntry (u4Index);
        if (pFtnEntry == NULL)
        {
            MPLS_CMN_UNLOCK ();
            continue;
        }
        if ((pFtnEntry->u1HwStatus == MPLS_FALSE) &&
            (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE) &&
            (pFtnEntry->u1RowStatus == MPLS_STATUS_ACTIVE))
        {
            MplsFTNHwAdd (pFtnEntry, NULL);
        }
        pFtnEntry->u1HwStatus = MPLS_FALSE;
        MPLS_CMN_UNLOCK ();
    }
}
#endif
/*****************************************************************************/
/* Function Name      : MplsCheckFTNEntry                                    */
/*                                                                           */
/* Description        : This function checks whether the FTN Hw entry is     */
/*                      present in FTN sw                                    */
/*                                                                           */
/* Input(s)           : Label of the particular FTN Entry                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : Pointer of the FTN Entry or NULL                     */
/*****************************************************************************/
#ifndef LDP_GR_WANTED
tFtnEntry          *
MplsCheckFTNEntry (UINT4 MplsShimLabel)
{
    tXcEntry           *pXcEntry = NULL;
    tFtnEntry          *pFtnEntry = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4Index = 0;
    eDirection          Direction = MPLS_DIRECTION_ANY;

    for (u4Index = MPLS_ONE; u4Index <= gu4MplsDbMaxFtnEntries; u4Index++)
    {
        pFtnEntry = MplsGetFtnTableEntry (u4Index);
        if (pFtnEntry != NULL && pFtnEntry->u1RowStatus == ACTIVE)
        {
            if (pFtnEntry->i4ActionType == MPLS_FTN_ON_TE_LSP)
            {
                pTeTnlInfo = (tTeTnlInfo *) pFtnEntry->pActionPtr;

                if (pTeTnlInfo->u1TnlRole == TE_EGRESS)
                {
                    Direction = MPLS_DIRECTION_REVERSE;
                }
                else
                {
                    Direction = MPLS_DIRECTION_FORWARD;
                }

                pXcEntry = MplsGetXCEntryByDirection (pTeTnlInfo->u4TnlXcIndex,
                                                      Direction);
            }
            else
            {
                pXcEntry = pFtnEntry->pActionPtr;
            }
            if ((pXcEntry == NULL) || (pXcEntry->pOutIndex == NULL))
            {
                continue;
            }
            if (pXcEntry->pOutIndex->u4Label == MplsShimLabel)
            {
                return pFtnEntry;
            }
        }
    }
    return NULL;
}
#endif
/*****************************************************************************/
/* Function Name      : MplsILMTableAudit                                    */
/*                                                                           */
/* Description        : This function audits the information present in      */
/*                      ILM sw and the information present in ILM hw and     */
/*                      syncs hw with that of sw.                            */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
#ifdef LDP_GR_WANTED
VOID
MplsILMTableAudit (VOID)
{
    tMplsIpAddress     ILMHwListFec;
    uLabel             InLabel;
    tXcEntry           *pXcEntry = NULL;
    tInSegment         *pInSegment = NULL;
    tILMHwListEntry    *pILMHwListEntry = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4Index = 0;
    UINT4               u4XcIndex = 0;


    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsILMTableAudit ENTRY\n");
    MEMSET (&ILMHwListFec, 0, sizeof (tMplsIpAddress));
    MEMSET (&InLabel, 0, sizeof (uLabel));

    /* The Audit will not take care of RSVP-TE entries as of now, since the
       the Hw list conatins entries for NON-TE Lsp */

    /* Scan the ILM Hw List for incompletely programmed entries */
    pILMHwListEntry = MplsHwListGetFirstILMHwListEntry ();
    while (pILMHwListEntry != NULL)
    {
        if (!(MPLS_CHECK_ILM_HW_LIST_NP_BITS (pILMHwListEntry)))
        {
            if (MPLS_ILM_HW_LIST_IS_STATIC (pILMHwListEntry) == MPLS_TRUE)
            {
                pInSegment = MplsSignalGetInSegmentTableEntry (MPLS_ILM_HW_LIST_IN_L3_INTF (pILMHwListEntry),
                                                               MPLS_ILM_HW_LIST_IN_LABEL (pILMHwListEntry));
                if (pInSegment != NULL)
                {
                    INSEGMENT_HW_STATUS (pInSegment) = FALSE;
                }
            }
            if (MplsHwListDeleteILMEntryFromHw (pILMHwListEntry, NULL) == MPLS_FAILURE)
            {
                 CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsILMTableAudit: "
                                               "MplsHwListDeleteILMEntryFromHw Failed \t\n");
            }
        }
        pILMHwListEntry = MplsHwListGetNextILMHwListEntry (pILMHwListEntry);
    }

    for (u4Index = MPLS_ONE; u4Index <= gu4MplsDbMaxInSegEntries; u4Index++)
    {
        pInSegment = MplsGetInSegmentTableEntry (u4Index);
        if ((pInSegment == NULL) || (pInSegment->pXcIndex == NULL))
        {
            continue;
        }
        pXcEntry = pInSegment->pXcIndex;
        u4XcIndex = pXcEntry->u4Index;

        MEMCPY (&InLabel, &pInSegment->u4Label, sizeof (UINT4));

        /* if entry is found in the control plane => entry is static */
        /* For static entries FEC for ILm hw list entries is 0 */
        pILMHwListEntry = MplsHwListGetILMHwListEntry (ILMHwListFec,
                                                       0, pInSegment->u4IfIndex,
                                                       InLabel);

        if (TeCheckXcIndex (u4XcIndex, &pTeTnlInfo) == TE_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL, "TeCheckXcIndex not available\t\n");
        }

        /* Audit currenly supports NON-Te entries */
        if ((pILMHwListEntry == NULL) && (pTeTnlInfo == NULL) &&
            (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE) &&
            (pInSegment->u1RowStatus == MPLS_STATUS_ACTIVE))
        {
            if (MplsILMHwAdd (pInSegment, pTeTnlInfo, NULL) == MPLS_FAILURE)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsILMHwAdd Failed \t\n");
            }
        }
    }
    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsILMTableAudit EXIT\n");        

}
#else
VOID
MplsILMTableAudit (VOID)
{

    UINT4               u4XcIndex = 0;
    UINT4               u4Index = 0;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tXcEntry           *pXcEntry = NULL;
    tInSegment         *pInSegment = NULL;
    tMplsHwIlmInfo      MplsHwIlmInfo;
    tMplsHwIlmInfo      NextMplsHwIlmInfo;
    UINT4               u4Action = 0;
    tPwVcEntry         *pPwVcEntry = NULL;

    MEMSET (&MplsHwIlmInfo, 0, sizeof (tMplsHwIlmInfo));
    MEMSET (&NextMplsHwIlmInfo, 0, sizeof (tMplsHwIlmInfo));

    MplsFsMplsHwTraverseILM (&MplsHwIlmInfo, &NextMplsHwIlmInfo);
    if (NextMplsHwIlmInfo.InLabelList[0].u.MplsShimLabel != 0)
    {
        while (NextMplsHwIlmInfo.InLabelList[0].u.MplsShimLabel != 0)
        {
            MPLS_CMN_LOCK ();
            pInSegment =
                MplsCheckILMEntry (NextMplsHwIlmInfo.InLabelList[0].u.
                                   MplsShimLabel);
            MPLS_CMN_UNLOCK ();
            if ((pInSegment == NULL)
                && (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE))
            {
                MPLS_L2VPN_LOCK ();
                pPwVcEntry =
                    L2VpnGetPwVcEntryFromLabel (NextMplsHwIlmInfo.
                                                InLabelList[0].u.MplsShimLabel,
                                                0);
                MPLS_L2VPN_UNLOCK ();
                if (pPwVcEntry == NULL)
                {
                    /* No Entry in Control Plane so, Delete Data Plane */
                    MplsFsMplsHwDeleteILM (&NextMplsHwIlmInfo, u4Action);
                }
            }
            else if (pInSegment != NULL)
            {
                pInSegment->u1HwStatus = MPLS_TRUE;
            }
            MplsHwIlmInfo = NextMplsHwIlmInfo;
            MEMSET (&NextMplsHwIlmInfo, 0, sizeof (tMplsHwIlmInfo));
            MplsFsMplsHwTraverseILM (&MplsHwIlmInfo, &NextMplsHwIlmInfo);
        }
    }
    /* Software table scanning for H/w Table addition */
    for (u4Index = MPLS_ONE; u4Index <= gu4MplsDbMaxInSegEntries; u4Index++)
    {
        MPLS_CMN_LOCK ();
        pInSegment = MplsGetInSegmentTableEntry (u4Index);
        if ((pInSegment == NULL) || (pInSegment->pXcIndex == NULL))
        {
            MPLS_CMN_UNLOCK ();
            continue;
        }
        pXcEntry = pInSegment->pXcIndex;
        u4XcIndex = pXcEntry->u4Index;

        if (TeCheckXcIndex (u4XcIndex, &pTeTnlInfo) == TE_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL, "TeCheckXcIndex not available\t\n");
        }
        if ((pInSegment->u1HwStatus == MPLS_FALSE) &&
            (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE) &&
            (pInSegment->u1RowStatus == MPLS_STATUS_ACTIVE))
        {
            if (MplsILMHwAdd (pInSegment, pTeTnlInfo, NULL) == MPLS_FAILURE)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsILMHwAdd Failed \t\n");
            }
        }
        MPLS_CMN_UNLOCK ();
    }
}
#endif

/*****************************************************************************/
/* Function Name      : MplsCheckILMEntry                                    */
/*                                                                           */
/* Description        : This function checks whether the ILM Hw entry is     */
/*                      present in ILM sw                                    */
/*                                                                           */
/* Input(s)           : Label of the particular ILM Entry                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : Pointer of the ILM Entry or NULL .                   */
/*****************************************************************************/
#ifndef LDP_GR_WANTED
tInSegment         *
MplsCheckILMEntry (UINT4 MplsShimLabel)
{
    tInSegment         *pInSegment = NULL;
    UINT4               u4Index = 0;

    for (u4Index = MPLS_ONE; u4Index <= gu4MplsDbMaxInSegEntries; u4Index++)
    {
        pInSegment = MplsGetInSegmentTableEntry (u4Index);
        if (pInSegment != NULL)
        {
            if (pInSegment->u4Label == MplsShimLabel)
            {
                return pInSegment;
            }
        }
    }
    return NULL;
}
#endif
#ifdef L2VPN_HA_WANTED
VOID
MplsVplsTableAudit (VOID)
{
	tL2VpnVplsHwList L2VpnVplsHwListEntry;
	tL2VpnVplsHwList L2VpnVplsHwListKey;
	tMplsHwVplsInfo  MplsHwVplsInfo;	
	tVPLSEntry       *pVplsEntry = NULL;	
	UINT4			 u4Index=0;
	MEMSET (&L2VpnVplsHwListEntry, 0, sizeof (tL2VpnVplsHwList));
	if (L2VpnVplsHwListGetFirst (&L2VpnVplsHwListEntry) != MPLS_FAILURE)
	{
		do
		{
			if (L2VpnGetVplsEntryFromInstanceIndex (
						L2VPN_VPLS_HW_LIST_VPLS_INDEX (&L2VpnVplsHwListEntry)) == NULL)
			{
#ifdef NPAPI_WANTED
				/* Call for updating the NP */
#ifdef L2RED_WANTED
				if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
				{
					/* VPLS FDB */
					MEMSET (&MplsHwVplsInfo, 0, sizeof (tMplsHwVplsInfo));
					MplsHwVplsInfo.u2VplsFdbId = 
						L2VPN_VPLS_HW_LIST_FDBID (&L2VpnVplsHwListEntry);
					L2VpnVplsHwListRemove (&L2VpnVplsHwListEntry, 
							L2VPN_VPLS_NPAPI_SUCCESS);
					MplsFsMplsHwDeleteVplsVpn (
							L2VPN_VPLS_HW_LIST_VPLS_INDEX (&L2VpnVplsHwListEntry),
							L2VPN_VPLS_HW_LIST_VPNID (&L2VpnVplsHwListEntry), 
							&MplsHwVplsInfo);
					L2VpnVplsHwListRemove (&L2VpnVplsHwListEntry, 
							L2VPN_VPLS_NPAPI_CALLED);
				}
#endif
			}			
		}while (L2VpnVplsHwListGetNext (&L2VpnVplsHwListEntry, 
										&L2VpnVplsHwListEntry));	
	}
	else
	{
		CMNDB_DBG1 (DEBUG_ERROR_LEVEL, "%s : No node in HwList\n",
				__func__);
	}
	
	for (u4Index = 1; u4Index <= gL2VpnGlobalInfo.u4MaxVplsEntries; u4Index++)
	{
		pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4Index);
		if (pVplsEntry != NULL)
		{
			MEMSET (&L2VpnVplsHwListEntry, 0, sizeof (tL2VpnVplsHwList));
			MEMSET (&L2VpnVplsHwListKey, 0, sizeof (tL2VpnVplsHwList));
			L2VPN_VPLS_HW_LIST_VPLS_INDEX (&L2VpnVplsHwListKey) = u4Index;
			if (L2VpnVplsHwListGet (&L2VpnVplsHwListKey, &L2VpnVplsHwListEntry)
				!= MPLS_FAILURE)
			{
				if ((L2VPN_VPLS_HW_LIST_NPAPI_STATUS (&L2VpnVplsHwListEntry) &
					MPLS_L2VPN_VPLS_NPAPI_SUCCESS) == 
						MPLS_L2VPN_VPLS_NPAPI_SUCCESS)
				{
					continue;
				}
				
			}
			nmhSetFsMplsVplsRowStatus (u4Index, NOT_IN_SERVICE);	
			nmhSetFsMplsVplsRowStatus (u4Index, ACTIVE);	
		}
	}	
}
/*****************************************************************************/
/* Function Name      : MplsPWTableAudit                                     */
/*                                                                           */
/* Description        : This function audits the information present in      */
/*                      PW sw and the information present in PW hw and       */
/*                      syncs hw with that of sw.                            */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
MplsPWTableAudit (VOID)
{
    tPwVcEntry         *pPwEntry = NULL;
    tVPLSEntry         *pVplsEntry = NULL;
    tTMO_SLL_NODE      *pPwVplsNode = NULL;
    UINT4               u4Index = 0;
	tL2VpnPwHwList  	L2VpnPwHwListEntry;
	tL2VpnPwHwList  	L2VpnPwHwListKey;

	CMNDB_DBG1 (DEBUG_DEBUG_LEVEL, "%s : Entry\n", __func__);    
	MEMSET (&L2VpnPwHwListKey, 0, sizeof (tL2VpnPwHwList));
	MEMSET (&L2VpnPwHwListEntry, 0, sizeof (tL2VpnPwHwList));

	if (L2VpnHwListGetFirst (&L2VpnPwHwListEntry) != MPLS_FAILURE)
	{
		do
		{
			if ((L2VPN_PW_HW_LIST_NPAPI_STATUS (&L2VpnPwHwListEntry) &
						L2VPN_PWALL_NPAPI_SET) != L2VPN_PWALL_NPAPI_SET)
			{
				if (L2VPN_ZERO == 
						L2VPN_PW_HW_LIST_REMOTE_VE_ID (&L2VpnPwHwListEntry))
				{
					L2vpnGrDeleteLdpPwEntryOnMisMatch (&L2VpnPwHwListEntry);	
				}
#ifdef VPLS_GR_WANTED
				else
				{
					L2VpnBgpDeletePwEntryOnMismatch (&L2VpnPwHwListEntry);
				}
#endif
			}
		}
		while (L2VpnHwListGetNext (&L2VpnPwHwListEntry,&L2VpnPwHwListEntry) !=
				L2VPN_FAILURE);

	}
    else
    {

		CMNDB_DBG1 (DEBUG_ERROR_LEVEL, "%s : No node in HwList\n",
				__func__);
    }

    /* Software table scanning for H/w Table addition */
    for (u4Index = 1; u4Index <= gL2VpnGlobalInfo.u4MaxVplsEntries; u4Index++)
    {
        MPLS_L2VPN_LOCK ();
        pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4Index);
        if (pVplsEntry == NULL)
        {
            MPLS_L2VPN_UNLOCK ();
            continue;
        }
        L2VPN_VPLS_PW_SCAN (pVplsEntry, pPwVplsNode)
        {
            pPwEntry = L2VPN_GET_BASE_PTR (tPwVcEntry, VplsNode, pPwVplsNode);
			L2VPN_PW_HW_LIST_VPLS_INDEX (&L2VpnPwHwListKey) = u4Index;
			L2VPN_PW_HW_LIST_PW_INDEX (&L2VpnPwHwListKey) = 
												L2VPN_PWVC_INDEX (pPwEntry); 

			if ((L2VpnHwListGet (&L2VpnPwHwListKey, &L2VpnPwHwListEntry) ==
				L2VPN_FAILURE)&& L2VPN_IS_STATIC_PW (pPwEntry) && 
				(L2VPN_PWVC_OWNER_OTHER != L2VPN_PWVC_OWNER (pPwEntry)))
			{
				if ((MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE) &&
                	(pPwEntry->i1OperStatus == L2VPN_PWVC_UP))
            	{
                	L2VpnUpdatePwVcStatus (pPwEntry, L2VPN_PWVC_UP, NULL,
                                       L2VPN_PWVC_OPER_APP_CP_OR_MGMT);
            	}
			}
        }
        MPLS_L2VPN_UNLOCK ();
    }
	CMNDB_DBG1 (DEBUG_DEBUG_LEVEL, "%s : Exit\n", __func__);    
}
#endif
/*****************************************************************************/
/* Function Name      : MplsCheckPWEntry                                     */
/*                                                                           */
/* Description        : This function checks whether the PW Hw entry is      */
/*                      present in PW sw                                     */
/*                                                                           */
/* Input(s)           : Label of the particular PW Entry                     */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : Pointer of the PW VC Entry or NULL                   */
/*****************************************************************************/
/*
tPwVcEntry         *
MplsCheckPWEntry (UINT4 MplsShimLabel)
{
    tTMO_SLL_NODE      *pPwVplsNode = NULL;
    tVPLSEntry         *pVplsEntry = NULL;
    tPwVcEntry         *pPwEntry = NULL;
    UINT4               u4Index = 0;

    for (u4Index = 1; u4Index <= gL2VpnGlobalInfo.u4MaxVplsEntries; u4Index++)
    {
        pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4Index);
        if (pVplsEntry != NULL)
        {
            L2VPN_VPLS_PW_SCAN (pVplsEntry, pPwVplsNode)
            {
                pPwEntry =
                    L2VPN_GET_BASE_PTR (tPwVcEntry, VplsNode, pPwVplsNode);
                if (L2VPN_PWVC_OUTBOUND_VC_LABEL (pPwEntry) == MplsShimLabel)
                {
                    return pPwEntry;
                }
            }

        }
    }
    return NULL;
}
*/
/* MPLS_P2MP_LSP_CHANGES - S */
/*****************************************************************************/
/* Function Name      : MplsP2mpILMTableAudit                                */
/*                                                                           */
/* Description        : This function audits the information present in      */
/*                      P2MP ILM sw and the information present in ILM hw    */
/*                      and syncs hw with that of sw.                        */
/* Input(s)           : None.                                                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
MplsP2mpILMTableAudit (VOID)
{
    tOutSegment        *pOutSegment = NULL;
    tMplsHwIlmInfo      MplsHwP2mpIlmInfo;
    tMplsHwIlmInfo      NextMplsHwP2mpIlmInfo;
    UINT4               u4P2mpId = 0;
    UINT4               u4Index = 0;
    tXcEntry           *pXcEntry = NULL;

    MEMSET (&MplsHwP2mpIlmInfo, 0, sizeof (tMplsHwIlmInfo));
    MEMSET (&NextMplsHwP2mpIlmInfo, 0, sizeof (tMplsHwIlmInfo));

    for (u4P2mpId = 0; u4P2mpId <= MAX_TE_P2MP_TUNNEL_INFO; u4P2mpId++)
    {
        MplsFsMplsHwP2mpTraverseILM (u4P2mpId, &MplsHwP2mpIlmInfo,
                                     &NextMplsHwP2mpIlmInfo);
        while (NextMplsHwP2mpIlmInfo.OutLabelToLearn.u.MplsShimLabel != 0)
        {
            MPLS_CMN_LOCK ();
            pOutSegment =
                MplsCheckP2mpILMEntry
                (NextMplsHwP2mpIlmInfo.OutLabelToLearn.u.MplsShimLabel);
            MPLS_CMN_UNLOCK ();
            if ((pOutSegment == NULL)
                && (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE))
            {
                /* No Entry in Control Plane so, Delete in Data Plane */
                MplsFsMplsHwP2mpRemoveILM (u4P2mpId, &NextMplsHwP2mpIlmInfo);
            }
            else if (pOutSegment != NULL)
            {
                pOutSegment->u1HwStatus = MPLS_TRUE;
            }
            MplsHwP2mpIlmInfo = NextMplsHwP2mpIlmInfo;
            MEMSET (&NextMplsHwP2mpIlmInfo, 0, sizeof (tMplsHwIlmInfo));
            MplsFsMplsHwP2mpTraverseILM (u4P2mpId, &MplsHwP2mpIlmInfo,
                                         &NextMplsHwP2mpIlmInfo);
        }
    }
    /* Software table scanning for H/w Table addition */
    for (u4Index = MPLS_ONE; u4Index <= gu4MplsDbMaxOutSegEntries; u4Index++)
    {
        MPLS_CMN_LOCK ();
        pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
        if (pOutSegment == NULL)
        {
            MPLS_CMN_UNLOCK ();
            continue;
        }

        pXcEntry = (tXcEntry *) (pOutSegment->pXcIndex);
        if ((pXcEntry == NULL) || (pXcEntry->pInIndex == NULL))
        {
            MPLS_CMN_UNLOCK ();
            continue;
        }

        if ((pOutSegment->u1HwStatus == MPLS_FALSE) &&
            (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE) &&
            (pXcEntry->u1OperStatus == XC_OPER_UP) &&
            (pOutSegment->u1RowStatus == MPLS_STATUS_ACTIVE))
        {
            if (MplsP2mpILMHwAdd (pOutSegment, NULL) == MPLS_FAILURE)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsP2mpILMHwAdd failed\t\n");
            }

            pOutSegment->u1HwStatus = MPLS_TRUE;
        }
        MPLS_CMN_UNLOCK ();
    }
}

/*****************************************************************************/
/* Function Name      : MplsCheckP2mpILMEntry                                */
/*                                                                           */
/* Description        : This function checks whether the ILM Hw entry is     */
/*                      present in ILM sw                                    */
/*                                                                           */
/* Input(s)           : Label of the particular P2MP ILM Entry               */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : Pointer of the ILM Entry or NULL .                   */
/*****************************************************************************/
tOutSegment        *
MplsCheckP2mpILMEntry (UINT4 u4OutLabel)
{
    tOutSegment        *pOutSegment = NULL;
    UINT4               u4Index = 0;

    for (u4Index = MPLS_ONE; u4Index <= gu4MplsDbMaxOutSegEntries; u4Index++)
    {
        pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
        if (pOutSegment != NULL)
        {
            if (pOutSegment->u4Label == u4OutLabel)
            {
                return pOutSegment;
            }
        }
    }
    return NULL;
}

/* MPLS_P2MP_LSP_CHANGES - E */
#endif
