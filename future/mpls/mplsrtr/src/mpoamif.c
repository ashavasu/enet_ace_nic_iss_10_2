/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: mpoamif.c,v 1.27 2017/11/09 13:22:14 siva Exp $
 *
 * Description: This file contains functions to interact with OAM module.
 *****************************************************************************/

#include "mplsincs.h"
#include "mplsapi.h"
#include "mplcmndb.h"
#include "mplsftn.h"
#include "mplslsr.h"
#include "l2vpextn.h"
#include "oaminc.h"
#include "arp.h"
#include "oamext.h"
#include "mpoamdef.h"
#include "teextrn.h"
#include "oamnotif.h"
#include "tetdfs.h"
#include "teprot.h"
PRIVATE INT4
 
 
 
 MplsOamIfNotifyTnlStatusFromMeg (tOamFsMplsTpMeEntry * pOamFsMplsTpMeEntry,
                                  UINT1 u1MegStatus);
PRIVATE INT4
 
 
 
 MplsOamIfNotifyPwStatusFromMeg (tOamFsMplsTpMeEntry * pOamFsMplsTpMeEntry,
                                 UINT1 u1MegStatus);

/*****************************************************************************/
/* Function     : MplsOamIfHandlePathStatusChange                            */
/*                                                                           */
/* Description  : This function handles the path status change               */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                                                                           */
/* Output       : pOutMplsApiInfo - Pointer to the output API information    */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
MplsOamIfHandlePathStatusChange (tMplsApiInInfo * pInMplsApiInfo,
                                 tMplsApiOutInfo * pOutMplsApiInfo)
{
    INT4                i4RetVal = OSIX_FAILURE;

    switch (pInMplsApiInfo->InPathId.u4PathType)
    {
        case MPLS_PATH_TYPE_MEG_ID:
            /* Update Session index in MEG table 
             * pInMplsApiInfo->InPathId.MplsMegId.u4MegIndex
             * pInMplsApiInfo->InPathId.MplsMegId.u4MeIndex
             * pInMplsApiInfo->InPathId.MplsMegId.u4MpIndex
             *
             * pInMplsApiInfo->InOamPathStatus.u1PathStatus
             *
             * Notify ELPS and BFD about the path status
             */
            i4RetVal = OamUtilIsOamModuleEnabled (pInMplsApiInfo->u4ContextId);
            if (i4RetVal == OSIX_SUCCESS)
            {
                /* Handle the path status change for MEG */
                i4RetVal = MplsOamIfHdlPathStatusChgForMeg (pInMplsApiInfo,
                                                            pOutMplsApiInfo);
            }
            break;
        case MPLS_PATH_TYPE_TUNNEL:
            /* Update Session index in Tunnel table 
             * pInMplsApiInfo->InPathId.TnlId.u4SrcTnlNum
             * pInMplsApiInfo->InPathId.TnlId.u4LspNum
             * pInMplsApiInfo->InPathId.TnlId.u4SrcNodeId
             * pInMplsApiInfo->InPathId.TnlId.u4DstNodeId
             *
             * pInMplsApiInfo->InOamPathStatus.u1PathStatus
             * */
            i4RetVal = MplsOamIfHdlPathStatusChgForTnl (pInMplsApiInfo);
            break;
        case MPLS_PATH_TYPE_NONTE_LSP:
            /* Update Session index in PW table
             * pInMplsApiInfo->InPathId.LspId.u4FtnIndex
             *
             * pInMplsApiInfo->InOamPathStatus.u1PathStatus
             * */
            i4RetVal = MplsOamIfHdlPathStatusChgForLsp (pInMplsApiInfo);
            break;
        default:
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsOamIfHandlePathStatusChange: "
                        "Invalid path type: INTMD-EXIT \n");
            break;
    }
    if (i4RetVal == OSIX_FAILURE)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsOamIfHandlePathStatusChange: "
                    "Path status handling failed: INTMD-EXIT \n");
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function     : MplsOamIfUpdateLpsStatus                                   */
/*                                                                           */
/* Description  : This function handles the LPS status on LSP/PW             */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
MplsOamIfUpdateLpsStatus (tMplsApiInInfo * pInMplsApiInfo)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeUpdateInfo       TeUpdateInfo;
    UINT4               u4NodeId = 0;

    /* Update the Linear Protection Switching status for tunnel */
    if (pInMplsApiInfo->InPathId.u4PathType == MPLS_PATH_TYPE_TUNNEL)
    {
        pTeTnlInfo = TeGetTunnelInfo (pInMplsApiInfo->InPathId.
                                      TnlId.u4SrcTnlNum,
                                      pInMplsApiInfo->InPathId.
                                      TnlId.u4LspNum,
                                      pInMplsApiInfo->InPathId.
                                      TnlId.SrcNodeId.
                                      MplsRouterId.u4_addr[0],
                                      pInMplsApiInfo->InPathId.
                                      TnlId.DstNodeId.MplsRouterId.u4_addr[0]);
        if (pTeTnlInfo == NULL)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsOamIfUpdateLpsStatus: "
                        "Tunnel entry doesn't exist: INTMD-EXIT \n");
            return OSIX_FAILURE;
        }

        /* Initialise the TE update information */
        MEMSET (&TeUpdateInfo, 0, sizeof (tTeUpdateInfo));

        TeUpdateInfo.u4BkpTnlIndex =
            pInMplsApiInfo->InServicePathId.TnlId.u4SrcTnlNum;
        TeUpdateInfo.u4BkpTnlInstance =
            pInMplsApiInfo->InServicePathId.TnlId.u4LspNum;

        TeUpdateInfo.u1TnlSwitchApp = TE_SWITCH_APP_ELPS;

        u4NodeId = pInMplsApiInfo->InServicePathId.TnlId.SrcNodeId.
            MplsRouterId.u4_addr[0];
        u4NodeId = OSIX_HTONL (u4NodeId);
        MEMCPY (TeUpdateInfo.BkpTnlIngressLsrId, &u4NodeId, ROUTER_ID_LENGTH);

        u4NodeId = pInMplsApiInfo->InServicePathId.TnlId.DstNodeId.
            MplsRouterId.u4_addr[0];
        u4NodeId = OSIX_HTONL (u4NodeId);
        MEMCPY (TeUpdateInfo.BkpTnlEgressLsrId, &u4NodeId, ROUTER_ID_LENGTH);

        switch (pInMplsApiInfo->u4SubReqType)
        {
            case MPLS_LPS_PROT_AVAILABLE:
                TeUpdateInfo.u1LocalProtection = LOCAL_PROT_AVAIL;
                break;
            case MPLS_LPS_PROT_NOT_APPLICABLE:
                TeUpdateInfo.u1LocalProtection = LOCAL_PROT_NOT_APPLICABLE;
                break;
            case MPLS_LPS_PROT_NOT_AVAILABLE:
                TeUpdateInfo.u1LocalProtection = LOCAL_PROT_NOT_AVAIL;
                break;
            case MPLS_LPS_PROT_IN_USE:
                TeUpdateInfo.u1LocalProtection = LOCAL_PROT_IN_USE;
                break;
            default:
                MPLSFM_DBG (MPLSFM_IF_FAILURE,
                            "MplsOamIfUpdateLpsStatus: "
                            "Invalid sub request type : INTMD-EXIT \n");
                return OSIX_FAILURE;
        }
        TeUpdateInfo.u1TeUpdateOpr = TE_LOCAL_PROT_RELATED;
        TeUpdateInfo.u1CPOrOamOperChgReqd = FALSE;
        if (TeCmnExtUpdateTnlProtStatus (pTeTnlInfo, &TeUpdateInfo)
            == TE_FAILURE)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsOamIfUpdateLpsStatus: "
                        "Protection status updation for tunnel failed "
                        ": INTMD-EXIT \n");
            return OSIX_FAILURE;
        }
    }
    else
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsOamIfUpdateLpsStatus: "
                    "Protection status updation for tunnel failed "
                    ": INTMD-EXIT \n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : MplsOamIfHdlPathStatusChgForLsp                            */
/*                                                                           */
/* Description  : This function handles the path status change for LSP.      */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_FAILURE                                               */
/*****************************************************************************/
INT4
MplsOamIfHdlPathStatusChgForLsp (tMplsApiInInfo * pInMplsApiInfo)
{
    tFtnEntry          *pFtnEntry = NULL;

    /* Get the FTN entry from FEC */
    pFtnEntry = MplsFwdGetFtnTableEntry ((UINT4) pInMplsApiInfo->InPathId.LspId.
                                         PeerAddr.u4_addr[0]);
    if (pFtnEntry == NULL)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsOamIfHdlPathStatusChgForLsp: "
                    "MPLS FTN entry does not exist: INTMD-EXIT \n");
        return OSIX_FAILURE;
    }
    /* Path status change for LSP is not supported */
    MPLSFM_DBG (MPLSFM_IF_FAILURE,
                "MplsOamIfHdlPathStatusChgForLsp: "
                "MPLS path status changes for LSP is not "
                "supported: EXIT \n");

    return OSIX_FAILURE;
}

/*****************************************************************************/
/* Function     : MplsOamIfHdlPathStatusChgForTnl                            */
/*                                                                           */
/* Description  : This function handles the path status change for tunnel    */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
MplsOamIfHdlPathStatusChgForTnl (tMplsApiInInfo * pInMplsApiInfo)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeUpdateInfo       TeUpdateInfo;

    MEMSET (&TeUpdateInfo, 0, sizeof (tTeUpdateInfo));
    /* Get the tunnel information */
    pTeTnlInfo = TeGetTunnelInfo (pInMplsApiInfo->InPathId.TnlId.
                                  u4SrcTnlNum,
                                  pInMplsApiInfo->InPathId.TnlId.
                                  u4LspNum,
                                  pInMplsApiInfo->InPathId.TnlId.
                                  SrcNodeId.MplsRouterId.u4_addr[0],
                                  pInMplsApiInfo->InPathId.TnlId.
                                  DstNodeId.MplsRouterId.u4_addr[0]);
    if (pTeTnlInfo == NULL)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsOamIfHdlPathStatusChgForTnl "
                    "MPLS Tunnel entry does not exist: INTMD-EXIT \n");
        return OSIX_FAILURE;
    }

    MPLSFM_DBG2 (MPLSFM_IF_PRCS,
                 "MplsOamIfHdlPathStatusChgForTnl "
                 "Tunnel: %u %u \n", pTeTnlInfo->u4TnlIndex,
                 pTeTnlInfo->u4TnlInstance);

    if (pTeTnlInfo->bTnlIntOamEnable == FALSE)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsOamIfHdlPathStatusChgForTnl "
                    "OAM is not enabled on the tunnel: INTMD-EXIT \n");
        return OSIX_FAILURE;
    }
    if ((pInMplsApiInfo->u4SrcModId == MPLS_OAM_MODULE) &&
        (pTeTnlInfo->bIsOamEnabled == FALSE))
    {
        /* Proactive session parameters should have been updated 
         * before notifying OAM status */
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsOamIfHdlPathStatusChgForTnl "
                    "OAM is not enabled on the tunnel: " "INTMD-EXIT \n");
        return OSIX_FAILURE;
    }
    /* If tunnel operational status is made down, tunnel applications 
     * should be notified. */
    if (pInMplsApiInfo->InOamPathStatus.u1PathStatus == MPLS_PATH_STATUS_DOWN)
    {
        if (pInMplsApiInfo->u4SrcModId == MPLS_OAM_MODULE)
        {
            if (pTeTnlInfo->bIsOamEnabled == FALSE)
            {
                MPLSFM_DBG (MPLSFM_IF_FAILURE,
                            "MplsOamIfHdlPathStatusChgForTnl "
                            "OAM is not enabled on the tunnel: "
                            "INTMD-EXIT \n");
                return OSIX_FAILURE;
            }
        }
        TeUpdateInfo.u1TnlOperStatus = TE_OPER_DOWN;
    }
    else if (pInMplsApiInfo->InOamPathStatus.u1PathStatus ==
             MPLS_PATH_STATUS_UP)
    {
        if ((pInMplsApiInfo->u4SrcModId == MPLS_OAM_MODULE) ||
            (pInMplsApiInfo->u4SrcModId == BFD_MODULE))
        {
            pTeTnlInfo->bIsOamEnabled = TRUE;
        }
        TeUpdateInfo.u1TnlOperStatus = TE_OPER_UP;
    }
    else
    {
        pTeTnlInfo->bIsOamEnabled = FALSE;
        pTeTnlInfo->u1OamOperStatus = TE_OPER_UNKNOWN;
        TeUpdateInfo.u1TnlOperStatus = TE_OPER_UP;
    }
    TeUpdateInfo.u1TeUpdateOpr = TE_OAM_OPR_STATUS_RELATED;
    TeUpdateInfo.u1CPOrOamOperChgReqd = TRUE;

    if (pInMplsApiInfo->u4SrcModId == BFD_MODULE)
    {
        TeUpdateInfo.u4SrcModule = BFD_MODULE;
    }

    if (TeUpdateTnlStatus (pTeTnlInfo, &TeUpdateInfo) == TE_FAILURE)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsOamIfHdlPathStatusChgForTnl "
                    "MPLS tunnel update status for OAM application failed: "
                    "INTMD-EXIT \n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : MplsOamIfHdlPathStatusChgForMeg                            */
/*                                                                           */
/* Description  : This function handles the path status change for MEG       */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                                                                           */
/* Output       :  pOutMplsApiInfo - Pointer to the output API information   */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
MplsOamIfHdlPathStatusChgForMeg (tMplsApiInInfo * pInMplsApiInfo,
                                 tMplsApiOutInfo * pOutMplsApiInfo)
{
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntryTemp = NULL;
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntry = NULL;
    tOamFsMplsTpMegEntry OamFsMplsTpMegEntry;
    tOamFsMplsTpMegEntry *pOamFsMplsTpMegEntry = NULL;
    UINT4               u4MePrevOperStatus = 0;
    UINT4               u4MeCurOperStatus = 0;
    UINT1               u1MegStatus = 0;
    BOOL1               bIsMegIndexUnknown = FALSE;

    if (pOutMplsApiInfo == NULL)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsOamIfHdlPathStatusChgForMeg: "
                    "OUT API information should not be NULL : INTMD-EXIT \n");
        return OSIX_FAILURE;
    }
    /* ME structure initialisation */

    /* MEG indices are known to the BFD/TNL/PW */
    if ((pInMplsApiInfo->InPathId.MegId.u4MegIndex != 0) &&
        (pInMplsApiInfo->InPathId.MegId.u4MeIndex != 0) &&
        (pInMplsApiInfo->InPathId.MegId.u4MpIndex != 0))
    {
        pOamFsMplsTpMeEntryTemp = (tOamFsMplsTpMeEntry *)
            MemAllocMemBlk (MPLS_OAM_FSMPLS_TP_ME_TABLE_POOL_ID);
        if (pOamFsMplsTpMeEntryTemp == NULL)
        {
            return OSIX_FAILURE;
        }
        MEMSET (pOamFsMplsTpMeEntryTemp, 0, sizeof (tOamFsMplsTpMeEntry));

        /* Assign indices for ME entry fetch */
        pOamFsMplsTpMeEntryTemp->MibObject.u4FsMplsTpContextId =
            pInMplsApiInfo->u4ContextId;
        pOamFsMplsTpMeEntryTemp->MibObject.u4FsMplsTpMegIndex =
            pInMplsApiInfo->InPathId.MegId.u4MegIndex;
        pOamFsMplsTpMeEntryTemp->MibObject.u4FsMplsTpMeIndex =
            pInMplsApiInfo->InPathId.MegId.u4MeIndex;
        pOamFsMplsTpMeEntryTemp->MibObject.u4FsMplsTpMeMpIndex =
            pInMplsApiInfo->InPathId.MegId.u4MpIndex;

        pOamFsMplsTpMeEntry = OamGetFsMplsTpMeTable (pOamFsMplsTpMeEntryTemp);

        if (pOamFsMplsTpMeEntry == NULL)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsOamIfHdlPathStatusChgForMeg: "
                        "MPLS ME entry does not exist : INTMD-EXIT \n");
            MemReleaseMemBlock (MPLS_OAM_FSMPLS_TP_ME_TABLE_POOL_ID,
                                (UINT1 *) pOamFsMplsTpMeEntryTemp);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (MPLS_OAM_FSMPLS_TP_ME_TABLE_POOL_ID,
                            (UINT1 *) pOamFsMplsTpMeEntryTemp);
    }
    else
    {
        bIsMegIndexUnknown = TRUE;
        /* Scan the ME table to find out the associated PW and 
         * notify the PW indices to PW module */
        pOamFsMplsTpMeEntry = OamUtilGetMeEntryFromServiceInfo (pInMplsApiInfo);
        if (pOamFsMplsTpMeEntry == NULL)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsOamIfHdlPathStatusChgForMeg: "
                        "MPLS ME entry does not exist : INTMD-EXIT \n");
            return OSIX_FAILURE;
        }
        if ((pInMplsApiInfo->InServicePathId.u4PathType
             == MPLS_PATH_TYPE_PW) ||
            (pInMplsApiInfo->InServicePathId.u4PathType
             == MPLS_PATH_TYPE_TUNNEL))
        {
            pOutMplsApiInfo->u4ContextId =
                pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId;
            pOutMplsApiInfo->OutPathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
            pOutMplsApiInfo->OutPathId.MegId.u4MegIndex =
                pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex;
            pOutMplsApiInfo->OutPathId.MegId.u4MeIndex =
                pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex;
            pOutMplsApiInfo->OutPathId.MegId.u4MpIndex =
                pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex;
        }
        else
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsOamIfHdlPathStatusChgForMeg: "
                        "Invalid Path Type: INTMD-EXIT \n");
            return OSIX_FAILURE;
        }
    }

    if ((pInMplsApiInfo->u4SrcModId == BFD_MODULE) &&
        (pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeProactiveOamSessIndex == 0))
    {
        /* BFD session index should be updated before reporting 
         * BFD session up*/
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsOamIfHdlPathStatusChgForMeg: "
                    "Proactive session index is not updated: " "INTMD-EXIT \n");
        return OSIX_FAILURE;
    }

    /* Store the previous ME Oper status */
    u4MePrevOperStatus = pOamFsMplsTpMeEntry->u4FsMplsTpMeOperStatus;

    switch (pInMplsApiInfo->InOamPathStatus.u1PathStatus)
    {
        case MPLS_PATH_STATUS_DOWN:
            u1MegStatus = MPLS_MEG_DOWN;
            if (pInMplsApiInfo->u4SrcModId == BFD_MODULE)
            {
                /* CASE A: BFD session entry exists and BFD reports down */
                pOamFsMplsTpMeEntry->u4FsMplsTpMeOperStatus |=
                    OAM_ME_OPER_STATUS_OAM_DOWN;
            }
            else if ((pInMplsApiInfo->u4SrcModId == MPLSDB_MODULE) ||
                     (pInMplsApiInfo->u4SrcModId == L2VPN_MODULE))
            {
                pOamFsMplsTpMeEntry->u4FsMplsTpMeOperStatus |=
                    OAM_ME_OPER_STATUS_PATH_DOWN;
            }
            else
            {
                MPLSFM_DBG (MPLSFM_IF_FAILURE,
                            "MplsOamIfHdlPathStatusChgForMeg: "
                            "Invalid source module ID: INTMD-EXIT \n");
                return OSIX_FAILURE;
            }
            break;
        case MPLS_PATH_STATUS_UP:
            u1MegStatus = MPLS_MEG_UP;
            if (pInMplsApiInfo->u4SrcModId == BFD_MODULE)
            {
                /* BFD session entry exists and BFD reports up */
                pOamFsMplsTpMeEntry->u4FsMplsTpMeOperStatus &=
                    ~(OAM_ME_OPER_STATUS_OAM_DOWN);
                if (pOamFsMplsTpMeEntry->u4FsMplsTpMeOperStatus ==
                    u4MePrevOperStatus)
                {
                    MEMSET (&OamFsMplsTpMegEntry, 0,
                            sizeof (tOamFsMplsTpMegEntry));
                    OamFsMplsTpMegEntry.MibObject.u4FsMplsTpContextId =
                        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId;
                    OamFsMplsTpMegEntry.MibObject.u4FsMplsTpMegIndex =
                        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex;

                    /* Search MEG table for service type */
                    pOamFsMplsTpMegEntry = OamGetFsMplsTpMegTable
                        (&OamFsMplsTpMegEntry);

                    if (pOamFsMplsTpMegEntry == NULL)
                    {
                        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                                    "MplsOamIfHdlPathStatusChgForMeg: "
                                    "MEG entry doesnot exist : INTMD-EXIT \n");
                        return OSIX_FAILURE;
                    }

                    /* Update Tnl/PW OAM status */
                    if (pOamFsMplsTpMegEntry->MibObject.
                        i4FsMplsTpMegServiceType == OAM_SERVICE_TYPE_LSP)
                    {
                        MplsOamIfNotifyTnlStatusFromMeg (pOamFsMplsTpMeEntry,
                                                         u1MegStatus);
                    }
                    else if (pOamFsMplsTpMegEntry->MibObject.
                             i4FsMplsTpMegServiceType == OAM_SERVICE_TYPE_PW)
                    {
                        MplsOamIfNotifyPwStatusFromMeg (pOamFsMplsTpMeEntry,
                                                        u1MegStatus);
                    }
                }

            }
            else if ((pInMplsApiInfo->u4SrcModId == MPLSDB_MODULE) ||
                     (pInMplsApiInfo->u4SrcModId == L2VPN_MODULE))
            {
                pOamFsMplsTpMeEntry->u4FsMplsTpMeOperStatus &=
                    ~(OAM_ME_OPER_STATUS_PATH_DOWN);
                if ((bIsMegIndexUnknown) &&
                    (pOamFsMplsTpMeEntry->u4FsMplsTpMeOperStatus == 0))
                {
                    MEMSET (&OamFsMplsTpMegEntry, 0,
                            sizeof (tOamFsMplsTpMegEntry));
                    OamFsMplsTpMegEntry.MibObject.u4FsMplsTpContextId =
                        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId;
                    OamFsMplsTpMegEntry.MibObject.u4FsMplsTpMegIndex =
                        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex;

                    /* Search MEG table for service type */
                    pOamFsMplsTpMegEntry = OamGetFsMplsTpMegTable
                        (&OamFsMplsTpMegEntry);

                    if (pOamFsMplsTpMegEntry == NULL)
                    {
                        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                                    "MplsOamIfHdlPathStatusChgForMeg: "
                                    "MEG entry doesnot exist : INTMD-EXIT \n");
                        return OSIX_FAILURE;
                    }

                    /* Update Tnl/PW OAM status */
                    if (pOamFsMplsTpMegEntry->MibObject.
                        i4FsMplsTpMegServiceType == OAM_SERVICE_TYPE_LSP)
                    {
                        MplsOamIfNotifyTnlStatusFromMeg (pOamFsMplsTpMeEntry,
                                                         u1MegStatus);
                    }
                    else if (pOamFsMplsTpMegEntry->MibObject.
                             i4FsMplsTpMegServiceType == OAM_SERVICE_TYPE_PW)
                    {
                        MplsOamIfNotifyPwStatusFromMeg (pOamFsMplsTpMeEntry,
                                                        u1MegStatus);
                    }
                }
            }
            else
            {
                MPLSFM_DBG (MPLSFM_IF_FAILURE,
                            "MplsOamIfHdlPathStatusChgForMeg: "
                            "Invalid source module ID: INTMD-EXIT \n");
                return OSIX_FAILURE;
            }
            break;
        default:
            MPLSFM_DBG (MPLSFM_IF_MISC,
                        "MplsOamIfHdlPathStatusChgForMeg: "
                        "Invalid path status: " "INTMD-EXIT \n");
            return OSIX_FAILURE;
    }

    /* Store the current ME oper status. */
    u4MeCurOperStatus = pOamFsMplsTpMeEntry->u4FsMplsTpMeOperStatus;

    if (u4MePrevOperStatus == u4MeCurOperStatus)
    {
        /* This state can occur when the OAM enabled 
         * for the MEG first time */
        return OSIX_SUCCESS;
    }
    else if ((u1MegStatus == MPLS_MEG_DOWN) && (u4MePrevOperStatus != 0))
    {
        /* Since previous oper status is down and received status
         * is also down, do not notify. */
        return OSIX_SUCCESS;
    }
    else if ((u1MegStatus == MPLS_MEG_UP) && (u4MeCurOperStatus != 0))
    {
        /* Since current oper status is not up even though
         * received status is up, do not notify. */
        return OSIX_SUCCESS;
    }

    if (MplsOamIfNotifyMegStatusToApp (pInMplsApiInfo->u4SrcModId,
                                       pOamFsMplsTpMeEntry->u4FsMplsTpMeApps,
                                       pOamFsMplsTpMeEntry,
                                       u1MegStatus) == OSIX_FAILURE)
    {
        MPLSFM_DBG (MPLSFM_IF_MISC,
                    "MplsOamIfHdlPathStatusChgForMeg: "
                    "MEG status notification to application failed: "
                    "INTMD-EXIT \n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : MplsOamIfNotifyMegStatusToApp                              */
/*                                                                           */
/* Description  : This function notifies the MEG status to applications      */
/*                                                                           */
/* Input        : u4SrcModId - Source application identifier                 */
/*                u4DstModId - Destination application identifier            */
/*                pInMplsApiInfo - Pointer to the input API information.     */
/*                u1MegStatus - MEG status [UP/DOWN]                         */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
MplsOamIfNotifyMegStatusToApp (UINT4 u4SrcModId, UINT4 u4DstModId,
                               tOamFsMplsTpMeEntry * pOamFsMplsTpMeEntry,
                               UINT1 u1MegStatus)
{
    tMplsEventNotif    *pMplsEventNotif = NULL;
    tOamNotifyInfo      OamNotifyInfo;
    tOamFsMplsTpMegEntry OamFsMplsTpMegEntry;
    tOamFsMplsTpMegEntry *pOamFsMplsTpMegEntry = NULL;

    MEMSET (&OamFsMplsTpMegEntry, 0, sizeof (tOamFsMplsTpMegEntry));
    MEMSET (&OamNotifyInfo, 0, sizeof (tOamNotifyInfo));

    OamFsMplsTpMegEntry.MibObject.u4FsMplsTpContextId =
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId;
    OamFsMplsTpMegEntry.MibObject.u4FsMplsTpMegIndex =
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex;

    /* Search MEG table for service type */
    pOamFsMplsTpMegEntry = OamGetFsMplsTpMegTable (&OamFsMplsTpMegEntry);

    if (pOamFsMplsTpMegEntry == NULL)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsOamIfNotifyMegStatusToApp: "
                    "MEG entry doesnot exist : INTMD-EXIT \n");
        return OSIX_FAILURE;
    }

    if (u4SrcModId == BFD_MODULE)
    {
        /* If MEG_UP/MEG_DOWN is triggered by BFD module, 
         * don't send the path status again to BFD */
        u4DstModId &= ~(MPLS_APPLICATION_BFD);
        /* Update Tnl/PW OAM status */
        if (pOamFsMplsTpMegEntry->MibObject.
            i4FsMplsTpMegServiceType == OAM_SERVICE_TYPE_LSP)
        {
            MplsOamIfNotifyTnlStatusFromMeg (pOamFsMplsTpMeEntry, u1MegStatus);
        }
        else if (pOamFsMplsTpMegEntry->MibObject.
                 i4FsMplsTpMegServiceType == OAM_SERVICE_TYPE_PW)
        {
            MplsOamIfNotifyPwStatusFromMeg (pOamFsMplsTpMeEntry, u1MegStatus);
        }
    }

    /* Initialise the MPLS event notification structure */
    pMplsEventNotif = (tMplsEventNotif *)
        MemAllocMemBlk (MPLS_EVENT_NOTIF_POOL_ID);
    if (pMplsEventNotif == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pMplsEventNotif, 0, sizeof (tMplsEventNotif));

    pMplsEventNotif->u4ContextId =
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId;

    /* Assign MEG information */
    pMplsEventNotif->PathId.u4PathType = MPLS_PATH_TYPE_MEG_ID;
    pMplsEventNotif->PathId.MegId.u4MegIndex =
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex;
    pMplsEventNotif->PathId.MegId.u4MeIndex =
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex;
    pMplsEventNotif->PathId.MegId.u4MpIndex =
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex;

    /* Assign proactive session index, this will be used by BFD module
     * to search the session associated with this MEG */
    pMplsEventNotif->u4ProactiveSessIndex = pOamFsMplsTpMeEntry->MibObject.
        u4FsMplsTpMeProactiveOamSessIndex;

    /* Assign service type [Tunnel/Pseudowire/Section] */
    pMplsEventNotif->u1ServiceType =
        (UINT1) pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegServiceType;

    /* Notify SNMP Manager and Syslog. */
    OamNotifyInfo.u4ContextId =
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId;
    OamNotifyInfo.u4MegIndex =
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex;
    OamNotifyInfo.u4MeIndex = pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex;
    OamNotifyInfo.u4MpIndex =
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex;
    MEMCPY (OamNotifyInfo.au1MegName,
            pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegName,
            OAM_MEG_NAME_MAX_LEN);
    MEMCPY (OamNotifyInfo.au1MeName,
            pOamFsMplsTpMeEntry->MibObject.au1FsMplsTpMeName,
            OAM_ME_NAME_MAX_LEN);
    if (u1MegStatus == MPLS_MEG_UP)
    {
        OamNotifyInfo.i4MegOperStatus = OAM_MEG_OPER_UP;
        pMplsEventNotif->u2Event = MPLS_MEG_UP_EVENT;
    }
    else
    {
        OamNotifyInfo.i4MegOperStatus = OAM_MEG_OPER_DOWN;
        pMplsEventNotif->u2Event = MPLS_MEG_DOWN_EVENT;
    }
    OamNotifyInfo.u1MegSubOperStatus =
        (UINT1) pOamFsMplsTpMeEntry->u4FsMplsTpMeOperStatus;

    OamNotifSendTrapAndSyslog (&OamNotifyInfo, OAM_TRAP_DEFECT_CONDITION);

    if (MplsPortEventNotification (u4DstModId, pMplsEventNotif) == OSIX_FAILURE)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsOamIfNotifyMegStatusToApp: "
                    "MEG status notification to application failed:"
                    "INTMD-EXIT \n");
        MemReleaseMemBlock (MPLS_EVENT_NOTIF_POOL_ID,
                            (UINT1 *) pMplsEventNotif);
        return OSIX_FAILURE;
    }
    MemReleaseMemBlock (MPLS_EVENT_NOTIF_POOL_ID, (UINT1 *) pMplsEventNotif);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : MplsOamIfNotifyTnlStatusFromMeg                            */
/*                                                                           */
/* Description  : This function notifies the tunnel status from MEG          */
/*                                                                           */
/* Input        : pOamFsMplsTpMeEntry - Pointer to an ME information         */
/*                u1MegStatus - MEG status [UP/DOWN]                         */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
PRIVATE INT4
MplsOamIfNotifyTnlStatusFromMeg (tOamFsMplsTpMeEntry * pOamFsMplsTpMeEntry,
                                 UINT1 u1MegStatus)
{
    tMplsApiInInfo     *pInMplsApiInfo = NULL;
    UINT2               u2Len = 0;

    pInMplsApiInfo =
        (tMplsApiInInfo *) MemAllocMemBlk (MPLS_API_IN_INFO_POOL_ID);
    if (pInMplsApiInfo == NULL)
    {
        return OSIX_SUCCESS;
    }
    MEMSET (pInMplsApiInfo, 0, sizeof (tMplsApiInInfo));

    /* Copy the tunnel indices from row pointer */
    u2Len =
        (UINT2) pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeServicePointerLen;

    if ((u2Len < TUNNEL_TABLE_INDICES) || (u2Len >= OAM_MAX_OID_LEN))
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsOamIfNotifyTnlStatusFromMeg: "
                    "Service is not configured INTMD-EXIT \n");
        MemReleaseMemBlock (MPLS_API_IN_INFO_POOL_ID, (UINT1 *) pInMplsApiInfo);
        return OSIX_FAILURE;
    }

    u2Len--;
    pInMplsApiInfo->InPathId.TnlId.DstNodeId.
        MplsRouterId.u4_addr[0] =
        pOamFsMplsTpMeEntry->MibObject.au4FsMplsTpMeServicePointer[u2Len];

    u2Len--;
    pInMplsApiInfo->InPathId.TnlId.SrcNodeId.
        MplsRouterId.u4_addr[0] =
        pOamFsMplsTpMeEntry->MibObject.au4FsMplsTpMeServicePointer[u2Len];

    u2Len--;
    pInMplsApiInfo->InPathId.TnlId.u4LspNum =
        pOamFsMplsTpMeEntry->MibObject.au4FsMplsTpMeServicePointer[u2Len];

    u2Len--;
    pInMplsApiInfo->InPathId.TnlId.u4SrcTnlNum =
        pOamFsMplsTpMeEntry->MibObject.au4FsMplsTpMeServicePointer[u2Len];

    if (u1MegStatus == MPLS_MEG_UP)
    {
        pInMplsApiInfo->InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    }
    else if (u1MegStatus == MPLS_MEG_DOWN)
    {
        pInMplsApiInfo->InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_DOWN;
    }
    else
    {
        pInMplsApiInfo->InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UNKNOWN;
    }
    pInMplsApiInfo->u4SrcModId = MPLS_OAM_MODULE;
    /* Notify the MEG associated Tunnel's OAM status */
    if (MplsOamIfHdlPathStatusChgForTnl (pInMplsApiInfo) == OSIX_FAILURE)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsOamIfNotifyTnlStatusFromMeg: "
                    "MEG status notification to tunnel failed:"
                    "INTMD-EXIT \n");
        MemReleaseMemBlock (MPLS_API_IN_INFO_POOL_ID, (UINT1 *) pInMplsApiInfo);
        return OSIX_FAILURE;
    }
    MemReleaseMemBlock (MPLS_API_IN_INFO_POOL_ID, (UINT1 *) pInMplsApiInfo);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : MplsOamIfNotifyPwStatusFromMeg                            */
/*                                                                           */
/* Description  : This function notifies the tunnel status from MEG          */
/*                                                                           */
/* Input        : pOamFsMplsTpMeEntry - Pointer to an ME information         */
/*                u1MegStatus - MEG status [UP/DOWN]                         */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
PRIVATE INT4
MplsOamIfNotifyPwStatusFromMeg (tOamFsMplsTpMeEntry * pOamFsMplsTpMeEntry,
                                UINT1 u1MegStatus)
{
    tMplsApiInInfo     *pInMplsApiInfo = NULL;
    UINT4               u4PwIndex = 0;
    UINT2               u2Len = 0;

    u2Len =
        (UINT2) pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeServicePointerLen;

    if ((u2Len < 1) || (u2Len >= OAM_MAX_OID_LEN))
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsOamIfNotifyPwStatusFromMeg: "
                    "Service is not configured INTMD-EXIT \n");
        return OSIX_FAILURE;
    }

    u4PwIndex = pOamFsMplsTpMeEntry->MibObject.
        au4FsMplsTpMeServicePointer[u2Len - 1];

    pInMplsApiInfo =
        (tMplsApiInInfo *) MemAllocMemBlk (MPLS_API_IN_INFO_POOL_ID);
    if (pInMplsApiInfo == NULL)
    {
        return OSIX_SUCCESS;
    }
    MEMSET (pInMplsApiInfo, 0, sizeof (tMplsApiInInfo));

    pInMplsApiInfo->u4SrcModId = MPLS_OAM_MODULE;
    pInMplsApiInfo->InPathId.MegId.u4MegIndex =
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex;
    pInMplsApiInfo->InPathId.MegId.u4MeIndex =
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex;
    pInMplsApiInfo->InPathId.MegId.u4MpIndex =
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex;
    pInMplsApiInfo->u4ContextId =
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId;
    pInMplsApiInfo->InServicePathId.PwId.u4PwIndex = u4PwIndex;

    if (u1MegStatus == MPLS_MEG_UP)
    {
        pInMplsApiInfo->InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
    }
    else if (u1MegStatus == MPLS_MEG_DOWN)
    {
        pInMplsApiInfo->InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_DOWN;
    }
    else
    {
        pInMplsApiInfo->InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UNKNOWN;
    }
    /* Message posting to L2VPN module to update PW OAM status */
    if (L2VpnApiHandleExternalRequest (MPLS_OAM_UPDATE_PW_MEG_OAM_STATUS,
                                       pInMplsApiInfo, NULL) == OSIX_FAILURE)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsOamIfNotifyPwStatusFromMeg: "
                    "\r Meg Index updation failed for Pseudowire.\r\n");
        MemReleaseMemBlock (MPLS_API_IN_INFO_POOL_ID, (UINT1 *) pInMplsApiInfo);
        return OSIX_FAILURE;
    }
    MemReleaseMemBlock (MPLS_API_IN_INFO_POOL_ID, (UINT1 *) pInMplsApiInfo);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : MplsOamIfUpdateSessionParams                               */
/*                                                                           */
/* Description  : This function updates the session params in                */
/*                the path entry.                                            */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
MplsOamIfUpdateSessionParams (tMplsApiInInfo * pInMplsApiInfo)
{
    tOamFsMplsTpMegEntry OamFsMplsTpMegEntry;
    tTeUpdateInfo       TeUpdateInfo;
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntryTemp = NULL;
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntry = NULL;
    tOamFsMplsTpMegEntry *pOamFsMplsTpMegEntry = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4FsMplsTpMePrevOperStatus = 0;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT1               u1MplsMegStatus = MPLS_MEG_UP;

    pOamFsMplsTpMeEntryTemp = (tOamFsMplsTpMeEntry *)
        MemAllocMemBlk (MPLS_OAM_FSMPLS_TP_ME_TABLE_POOL_ID);
    if (pOamFsMplsTpMeEntryTemp == NULL)
    {
        return OSIX_FAILURE;
    }

    /* Do the session index updation in the path entry */
    switch (pInMplsApiInfo->InPathId.u4PathType)
    {
        case MPLS_PATH_TYPE_MEG_ID:
            i4RetVal = OamUtilIsOamModuleEnabled (pInMplsApiInfo->u4ContextId);
            if (i4RetVal == OSIX_FAILURE)
            {
                MPLSFM_DBG (MPLSFM_IF_FAILURE,
                            "MplsOamIfUpdateSessionParams: "
                            "OAM module is disabled: INTMD-EXIT \n");
                break;
            }
            MEMSET (pOamFsMplsTpMeEntryTemp, 0, sizeof (tOamFsMplsTpMeEntry));
            /* Assign the ME indices */
            pOamFsMplsTpMeEntryTemp->MibObject.u4FsMplsTpContextId =
                pInMplsApiInfo->u4ContextId;
            pOamFsMplsTpMeEntryTemp->MibObject.u4FsMplsTpMegIndex =
                pInMplsApiInfo->InPathId.MegId.u4MegIndex;
            pOamFsMplsTpMeEntryTemp->MibObject.u4FsMplsTpMeIndex =
                pInMplsApiInfo->InPathId.MegId.u4MeIndex;
            pOamFsMplsTpMeEntryTemp->MibObject.u4FsMplsTpMeMpIndex =
                pInMplsApiInfo->InPathId.MegId.u4MpIndex;

            pOamFsMplsTpMeEntry =
                OamGetFsMplsTpMeTable (pOamFsMplsTpMeEntryTemp);

            if (pOamFsMplsTpMeEntry == NULL)
            {
                MPLSFM_DBG (MPLSFM_IF_FAILURE,
                            "MplsOamIfUpdateSessionParams: "
                            "ME entry doesn't exist : INTMD-EXIT \n");
                i4RetVal = OSIX_FAILURE;
                break;
            }
            i4RetVal = OSIX_SUCCESS;
            pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeProactiveOamSessIndex =
                pInMplsApiInfo->InOamSessionParams.u4ProactiveSessIndex;
            u4FsMplsTpMePrevOperStatus =
                pOamFsMplsTpMeEntry->u4FsMplsTpMeOperStatus;
            if (pInMplsApiInfo->InOamSessionParams.u4ProactiveSessIndex != 0)
            {
                pOamFsMplsTpMeEntry->u4FsMplsTpMeApps |= MPLS_APPLICATION_BFD;
                pOamFsMplsTpMeEntry->u4FsMplsTpMeOperStatus |=
                    OAM_ME_OPER_STATUS_OAM_DOWN;
                u1MplsMegStatus = MPLS_MEG_DOWN;
            }
            else
            {
                /* BFD session entry got deleted in BFD module */
                pOamFsMplsTpMeEntry->u4FsMplsTpMeApps &=
                    ~(MPLS_APPLICATION_BFD);
                pOamFsMplsTpMeEntry->u4FsMplsTpMeOperStatus &=
                    ~(OAM_ME_OPER_STATUS_OAM_DOWN);
                u1MplsMegStatus = MPLS_MEG_UP;
            }

            /* Notify ELPS module - MEG status */
            /* If Current-status != Previous-status */
            if (((pOamFsMplsTpMeEntry->u4FsMplsTpMeOperStatus
                  == OAM_ME_OPER_STATUS_UP) &&
                 (u4FsMplsTpMePrevOperStatus != OAM_ME_OPER_STATUS_UP)) ||
                (!(u4FsMplsTpMePrevOperStatus & OAM_ME_OPER_STATUS_OAM_DOWN) &&
                 (pOamFsMplsTpMeEntry->u4FsMplsTpMeOperStatus &
                  OAM_ME_OPER_STATUS_OAM_DOWN)))
            {
                if (MplsOamIfNotifyMegStatusToApp (BFD_MODULE,
                                                   pOamFsMplsTpMeEntry->
                                                   u4FsMplsTpMeApps,
                                                   pOamFsMplsTpMeEntry,
                                                   u1MplsMegStatus) ==
                    OSIX_FAILURE)
                {
                    MPLSFM_DBG (MPLSFM_IF_MISC,
                                "MplsOamIfUpdateSessionParams: "
                                "MEG status notification to application failed: "
                                "INTMD-EXIT \n");
                    i4RetVal = OSIX_FAILURE;
                }
            }

            if (MPLS_MEG_DOWN == u1MplsMegStatus)
            {
                /* when BFD session is configured, */
                /* initial state of MPLS-OAM is notified to 
                 * Applications & tunnel above*/
                break;
            }

            MEMSET (&OamFsMplsTpMegEntry, 0, sizeof (tOamFsMplsTpMegEntry));
            OamFsMplsTpMegEntry.MibObject.u4FsMplsTpContextId =
                pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId;
            OamFsMplsTpMegEntry.MibObject.u4FsMplsTpMegIndex =
                pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex;

            /* Search MEG table for service type */
            pOamFsMplsTpMegEntry = OamGetFsMplsTpMegTable
                (&OamFsMplsTpMegEntry);

            if (pOamFsMplsTpMegEntry == NULL)
            {
                MPLSFM_DBG (MPLSFM_IF_FAILURE,
                            "MplsOamIfUpdateSessionParams: "
                            "MEG entry doesnot exist : INTMD-EXIT \n");
                MemReleaseMemBlock (MPLS_OAM_FSMPLS_TP_ME_TABLE_POOL_ID,
                                    (UINT1 *) pOamFsMplsTpMeEntryTemp);
                return OSIX_FAILURE;
            }

            /* Update Tnl/PW OAM status */
            if (pOamFsMplsTpMegEntry->MibObject.
                i4FsMplsTpMegServiceType == OAM_SERVICE_TYPE_LSP)
            {
                MplsOamIfNotifyTnlStatusFromMeg (pOamFsMplsTpMeEntry,
                                                 MPLS_MEG_OAM_UNKNOWN);
            }
            else if (pOamFsMplsTpMegEntry->MibObject.
                     i4FsMplsTpMegServiceType == OAM_SERVICE_TYPE_PW)
            {
                MplsOamIfNotifyPwStatusFromMeg (pOamFsMplsTpMeEntry,
                                                MPLS_MEG_OAM_UNKNOWN);
            }
            break;
        case MPLS_PATH_TYPE_TUNNEL:
            /* Fetch the tunnel entry for proaction session index updation */
            pTeTnlInfo = TeGetTunnelInfo (pInMplsApiInfo->InPathId.
                                          TnlId.u4SrcTnlNum,
                                          pInMplsApiInfo->InPathId.
                                          TnlId.u4LspNum,
                                          pInMplsApiInfo->InPathId.
                                          TnlId.SrcNodeId.
                                          MplsRouterId.u4_addr[0],
                                          pInMplsApiInfo->InPathId.
                                          TnlId.DstNodeId.
                                          MplsRouterId.u4_addr[0]);
            if (pTeTnlInfo == NULL)
            {
                MPLSFM_DBG (MPLSFM_IF_FAILURE,
                            "MplsOamIfUpdateSessionParams: "
                            "Tunnel entry doesn't exist : INTMD-EXIT \n");
                i4RetVal = OSIX_FAILURE;
                break;
            }

            if (pTeTnlInfo->bTnlIntOamEnable == FALSE)
            {
                /* Admin has not enabled the OAM for tunnel */
                MPLSFM_DBG (MPLSFM_IF_FAILURE,
                            "MplsOamIfUpdateSessionParams: "
                            "OAM is not enabled on the tunnel: INTMD-EXIT \n");
                i4RetVal = OSIX_FAILURE;
                break;
            }
            i4RetVal = OSIX_SUCCESS;

            pTeTnlInfo->u4ProactiveSessionIndex =
                pInMplsApiInfo->InOamSessionParams.u4ProactiveSessIndex;

            pTeTnlInfo->u1OamOperStatus = TE_OPER_UNKNOWN;

            MEMSET (&TeUpdateInfo, 0, sizeof (tTeUpdateInfo));
            /* BFD session entry is made admin down or deleted */
            if (pTeTnlInfo->u4ProactiveSessionIndex == 0)
            {
                /* External Proactive module informed that the OAM 
                 * has been torn down for this tunnel */
                pTeTnlInfo->bIsOamEnabled = FALSE;
                if ((pTeTnlInfo->u1CPOrMgmtOperStatus == TE_OPER_UP) &&
                    (pTeTnlInfo->u1TnlOperStatus == TE_OPER_DOWN))
                {
                    TeUpdateInfo.u1TnlOperStatus =
                        pTeTnlInfo->u1CPOrMgmtOperStatus;
                    TeUpdateInfo.u1TeUpdateOpr = TE_OAM_OPR_STATUS_RELATED;
                    TeUpdateInfo.u1CPOrOamOperChgReqd = TRUE;
                    if (TeUpdateTnlStatus (pTeTnlInfo, &TeUpdateInfo) ==
                        TE_FAILURE)
                    {
                        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                                    "MplsOamIfUpdateSessionParams: "
                                    "Failed to update the OAM oper. status: "
                                    "INTMD-EXIT \n");
                        i4RetVal = OSIX_FAILURE;
                    }
                }
            }
            else
            {
                /* External Proactive module informed that the OAM has 
                 * triggered for this tunnel */
                if (pInMplsApiInfo->u4SrcModId != BFD_MODULE)
                {
                    pTeTnlInfo->bIsOamEnabled = TRUE;
                }
            }
            break;
        case MPLS_PATH_TYPE_NONTE_LSP:
            /* NOT SUPPORTED Update Session index in PW table
             * pInMplsApiInfo->InPathId.LspId.u4FtnIndex
             *
             * pInMplsApiInfo->InOamSessionParams.u4ProactiveSessIndex
             * */
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsOamIfUpdateSessionParams: "
                        "NON-TE LSP not supported : INTMD-EXIT \n");
            i4RetVal = OSIX_FAILURE;
            break;
        default:
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsOamIfUpdateSessionParams: "
                        "Invalid path type: INTMD-EXIT \n");
            i4RetVal = OSIX_FAILURE;
            break;
    }
    MemReleaseMemBlock (MPLS_OAM_FSMPLS_TP_ME_TABLE_POOL_ID,
                        (UINT1 *) pOamFsMplsTpMeEntryTemp);
    return i4RetVal;
}

/*****************************************************************************/
/* Function     : MplsOamIfRegWithMegForNotif                                */
/*                                                                           */
/* Description  : This function registers with MPLS module for               */
/*                notifications (MEG_UP/MEG_DOWN/Application packets)        */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
MplsOamIfRegWithMegForNotif (tMplsApiInInfo * pInMplsApiInfo)
{
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntryTemp = NULL;
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntry = NULL;
    UINT1               u1MegStatus = 0;
    UINT4               u4ApplicationIdentifier = 0;

    /* Check for MPLS OAM module status */
    if (OamUtilIsOamModuleEnabled (pInMplsApiInfo->u4ContextId) == OSIX_FAILURE)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsOamIfUpdateSessionParams: "
                    "OAM module is disabled: INTMD-EXIT \n");
        return OSIX_FAILURE;
    }

    /* Check if the module that wants to register is
       a valid module */

    if (pInMplsApiInfo->InRegParams.u4ModId >= MPLS_MAX_APP_ID)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsOamIfRegWithMegForNotif: "
                    "Invalid Module ID : INTMD-EXIT \n");
        return OSIX_FAILURE;
    }

    /* Compute the application Id from the module Id */
    u4ApplicationIdentifier =
        (UINT4) (1 << pInMplsApiInfo->InRegParams.u4ModId);

    MPLSFM_DBG3 (MPLSFM_IF_PRCS,
                 "MplsOamIfRegWithMegForNotif: "
                 "Registration with MPLS module from AppId=%d "
                 "Events=%x Request type=%d\n",
                 pInMplsApiInfo->InRegParams.u4ModId,
                 pInMplsApiInfo->InRegParams.u4Events,
                 pInMplsApiInfo->u4SubReqType);

    if ((pInMplsApiInfo->u4SubReqType == MPLS_APP_REGISTER) &&
        (gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
         [pInMplsApiInfo->InRegParams.u4ModId] == NULL))
    {
        /* Create memory for first time registrations */
        if (pInMplsApiInfo->InRegParams.u4Events == 0)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsOamIfRegWithMegForNotif: "
                        "No events given for registration: INTMD-EXIT \n");
            return OSIX_FAILURE;
        }

        gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
            [pInMplsApiInfo->InRegParams.u4ModId]
            = (tMplsRegParams *) MemAllocMemBlk (MPLS_REG_APPS_POOL_ID);
        if (gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
            [pInMplsApiInfo->InRegParams.u4ModId] == NULL)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsOamIfRegWithMegForNotif: "
                        "MemAlloc failed for registration: INTMD-EXIT \n");
            return OSIX_FAILURE;
        }
    }

    if ((pInMplsApiInfo->u4SubReqType == MPLS_APP_REGISTER) &&
        (gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
         [pInMplsApiInfo->InRegParams.u4ModId] != NULL))
    {
        /* Update the module specific registration parameters */
        gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
            [pInMplsApiInfo->InRegParams.u4ModId]->u4ModId =
            pInMplsApiInfo->InRegParams.u4ModId;
        gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
            [pInMplsApiInfo->InRegParams.u4ModId]->u4Events =
            pInMplsApiInfo->InRegParams.u4Events;
        gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
            [pInMplsApiInfo->InRegParams.u4ModId]->pFnRcvPkt =
            pInMplsApiInfo->InRegParams.pFnRcvPkt;
    }

    if (pInMplsApiInfo->InPathId.u4PathType == MPLS_PATH_TYPE_MEG_ID)
    {
        /* Update ELPS registration with MEG table
         * pInMplsApiInfo->InPathId.MegId.u4MegIndex
         * pInMplsApiInfo->InPathId.MegId.u4MeIndex
         * pInMplsApiInfo->InPathId.MegId.u4MpIndex 
         *
         * Search the MEG table and update the ELPS registration and 
         * notify the status
         * */
        /* ME structure initialisation */
        pOamFsMplsTpMeEntryTemp = (tOamFsMplsTpMeEntry *)
            MemAllocMemBlk (MPLS_OAM_FSMPLS_TP_ME_TABLE_POOL_ID);
        if (pOamFsMplsTpMeEntryTemp == NULL)
        {
            return OSIX_FAILURE;
        }
        MEMSET (pOamFsMplsTpMeEntryTemp, 0, sizeof (tOamFsMplsTpMeEntry));

        /* Assign indices for ME entry fetch */
        pOamFsMplsTpMeEntryTemp->MibObject.u4FsMplsTpContextId =
            pInMplsApiInfo->u4ContextId;
        pOamFsMplsTpMeEntryTemp->MibObject.u4FsMplsTpMegIndex =
            pInMplsApiInfo->InPathId.MegId.u4MegIndex;
        pOamFsMplsTpMeEntryTemp->MibObject.u4FsMplsTpMeIndex =
            pInMplsApiInfo->InPathId.MegId.u4MeIndex;
        pOamFsMplsTpMeEntryTemp->MibObject.u4FsMplsTpMeMpIndex =
            pInMplsApiInfo->InPathId.MegId.u4MpIndex;

        pOamFsMplsTpMeEntry = OamGetFsMplsTpMeTable (pOamFsMplsTpMeEntryTemp);

        if (pOamFsMplsTpMeEntry == NULL)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsOamIfHdlPathStatusChgForMeg: "
                        "MPLS ME entry does not exist : INTMD-EXIT \n");
            MemReleaseMemBlock (MPLS_OAM_FSMPLS_TP_ME_TABLE_POOL_ID,
                                (UINT1 *) pOamFsMplsTpMeEntryTemp);
            return OSIX_FAILURE;
        }

        if (pInMplsApiInfo->u4SubReqType == MPLS_APP_DEREGISTER)
        {
            pOamFsMplsTpMeEntry->u4FsMplsTpMeApps &= ~(u4ApplicationIdentifier);
            MemReleaseMemBlock (MPLS_OAM_FSMPLS_TP_ME_TABLE_POOL_ID,
                                (UINT1 *) pOamFsMplsTpMeEntryTemp);
            return OSIX_SUCCESS;
        }
        pOamFsMplsTpMeEntry->u4FsMplsTpMeApps |= u4ApplicationIdentifier;
        if (pOamFsMplsTpMeEntry->u4FsMplsTpMeOperStatus
            == OAM_ME_OPER_STATUS_UP)
        {
            u1MegStatus = MPLS_MEG_UP;
        }
        else
        {
            u1MegStatus = MPLS_MEG_DOWN;
        }

        if (MplsOamIfNotifyMegStatusToApp (MPLS_MODULE, u4ApplicationIdentifier,
                                           pOamFsMplsTpMeEntry,
                                           u1MegStatus) == OSIX_FAILURE)
        {
            /* Release OAM semaphore lock */
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsOamIfHdlPathStatusChgForMeg: "
                        "MEG status notification failed: INTMD-EXIT \n");
            MemReleaseMemBlock (MPLS_OAM_FSMPLS_TP_ME_TABLE_POOL_ID,
                                (UINT1 *) pOamFsMplsTpMeEntryTemp);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (MPLS_OAM_FSMPLS_TP_ME_TABLE_POOL_ID,
                            (UINT1 *) pOamFsMplsTpMeEntryTemp);
        return OSIX_SUCCESS;
    }
    MPLSFM_DBG (MPLSFM_IF_FAILURE,
                "MplsOamIfHdlPathStatusChgForMeg: "
                "Invalid Path type registration: EXIT \n");
    return OSIX_FAILURE;
}

/*****************************************************************************/
/* Function     : MplsOamIfGetMegInfo                                        */
/*                                                                           */
/* Description  : This function gets the MPLS MEG information based on the   */
/*                request from MPLS external module.                         */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                                                                           */
/* Output       : pOutMplsApiInfo - Pointer to the output API information.   */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
MplsOamIfGetMegInfo (tMplsApiInInfo * pInMplsApiInfo,
                     tMplsApiOutInfo * pOutMplsApiInfo)
{
    tOamFsMplsTpMegEntry OamFsMplsTpMegEntry;
    tOamFsMplsTpGlobalConfigEntry OamFsMplsTpGlobalConfigEntry;
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntryTemp = NULL;
    tOamFsMplsTpMegEntry *pOamFsMplsTpMegEntry = NULL;
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntry = NULL;
    tOamFsMplsTpGlobalConfigEntry *pOamFsMplsTpGlobalConfigEntry = NULL;
    UINT2               u2Len = 0;
    UINT1               u1TcValue = 0;

    if (pOutMplsApiInfo == NULL)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsOamIfGetMegInfo: "
                    "OUT API information should not be NULL : INTMD-EXIT \n");
        return OSIX_FAILURE;
    }

    if (OamUtilIsOamModuleEnabled (pInMplsApiInfo->u4ContextId) == OSIX_FAILURE)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsOamIfUpdateSessionParams: "
                    "OAM module is disabled: INTMD-EXIT \n");
        return OSIX_FAILURE;
    }

    if (pInMplsApiInfo->u4SubReqType == MPLS_GET_MEG_ID_FROM_MEG_NAME)
    {
        return (MplsOamIfGetMegIdFromMegName (pInMplsApiInfo, pOutMplsApiInfo));
    }

    MEMSET (&OamFsMplsTpMegEntry, 0, sizeof (tOamFsMplsTpMegEntry));

    /* Assign the indices */
    OamFsMplsTpMegEntry.MibObject.u4FsMplsTpContextId =
        pInMplsApiInfo->u4ContextId;

    if (pInMplsApiInfo->u4SubReqType == MPLS_GET_MEG_FROM_SRC_ICC_MEP)
    {
        pOamFsMplsTpMeEntry = OamUtilGetMeEntryFromMegIccId
            (pInMplsApiInfo->u4ContextId,
             pInMplsApiInfo->unIntInfo.MplsPathId.
             unPathId.MplsIccMep.au1Icc,
             pInMplsApiInfo->unIntInfo.MplsPathId.
             unPathId.MplsIccMep.au1Umc,
             pInMplsApiInfo->unIntInfo.MplsPathId.
             unPathId.MplsIccMep.u2MepIndex);
        if ((pOamFsMplsTpMeEntry == NULL) ||
            (pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus != ACTIVE))
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsOamIfGetMegInfo "
                        "MPLS ME entry does not exist: INTMD-EXIT \n");
            return OSIX_FAILURE;
        }
        OamFsMplsTpMegEntry.MibObject.u4FsMplsTpMegIndex =
            pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex;
    }
    else
    {
        OamFsMplsTpMegEntry.MibObject.u4FsMplsTpMegIndex =
            pInMplsApiInfo->InPathId.MegId.u4MegIndex;
    }

    pOamFsMplsTpMegEntry = OamGetFsMplsTpMegTable (&OamFsMplsTpMegEntry);
    if ((pOamFsMplsTpMegEntry == NULL) ||
        (pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegRowStatus != ACTIVE))
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsOamIfGetMegInfo "
                    "MPLS MEG entry does not exist/MEG is not active:"
                    "INTMD-EXIT \n");
        return OSIX_FAILURE;
    }

    if (pInMplsApiInfo->u4SubReqType != MPLS_GET_MEG_FROM_SRC_ICC_MEP)
    {
        pOamFsMplsTpMeEntryTemp = (tOamFsMplsTpMeEntry *)
            MemAllocMemBlk (MPLS_OAM_FSMPLS_TP_ME_TABLE_POOL_ID);
        if (pOamFsMplsTpMeEntryTemp == NULL)
        {
            return OSIX_FAILURE;
        }
        MEMSET (pOamFsMplsTpMeEntryTemp, 0, sizeof (tOamFsMplsTpMeEntry));

        /* Assign the indices */
        pOamFsMplsTpMeEntryTemp->MibObject.u4FsMplsTpContextId =
            pInMplsApiInfo->u4ContextId;

        pOamFsMplsTpMeEntryTemp->MibObject.u4FsMplsTpMegIndex =
            pInMplsApiInfo->InPathId.MegId.u4MegIndex;

        pOamFsMplsTpMeEntryTemp->MibObject.u4FsMplsTpMeIndex =
            pInMplsApiInfo->InPathId.MegId.u4MeIndex;

        pOamFsMplsTpMeEntryTemp->MibObject.u4FsMplsTpMeMpIndex =
            pInMplsApiInfo->InPathId.MegId.u4MpIndex;

        pOamFsMplsTpMeEntry = OamGetFsMplsTpMeTable (pOamFsMplsTpMeEntryTemp);
        if ((pOamFsMplsTpMeEntry == NULL) ||
            (pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeRowStatus != ACTIVE))
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsOamIfGetMegInfo "
                        "MPLS ME entry does not exist: INTMD-EXIT \n");
            MemReleaseMemBlock (MPLS_OAM_FSMPLS_TP_ME_TABLE_POOL_ID,
                                (UINT1 *) pOamFsMplsTpMeEntryTemp);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (MPLS_OAM_FSMPLS_TP_ME_TABLE_POOL_ID,
                            (UINT1 *) pOamFsMplsTpMeEntryTemp);
    }
    if (pInMplsApiInfo->u4SubReqType == MPLS_GET_MEG_NAME_FROM_MEG_ID)
    {
        MEMCPY (pOutMplsApiInfo->OutPathId.MegMeName.au1MegName,
                pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegName,
                pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegNameLen);
        MEMCPY (pOutMplsApiInfo->OutPathId.MegMeName.au1MeName,
                pOamFsMplsTpMeEntry->MibObject.au1FsMplsTpMeName,
                pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeNameLen);
        return OSIX_SUCCESS;
    }
    /* Assign the values */
    pOutMplsApiInfo->u4ContextId =
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId;
    pOutMplsApiInfo->OutMegInfo.MplsMegId.u4MegIndex =
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex;
    pOutMplsApiInfo->OutMegInfo.MplsMegId.u4MeIndex =
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex;
    pOutMplsApiInfo->OutMegInfo.MplsMegId.u4MpIndex =
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex;

    pOutMplsApiInfo->u4PathType = MPLS_PATH_TYPE_MEG_ID;
    MEMCPY (pOutMplsApiInfo->OutMegInfo.MplsMegMeName.au1MegName,
            pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegName,
            pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegNameLen);
    MEMCPY (pOutMplsApiInfo->OutMegInfo.MplsMegMeName.au1MeName,
            pOamFsMplsTpMeEntry->MibObject.au1FsMplsTpMeName,
            pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeNameLen);

    u2Len =
        (UINT2) pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeServicePointerLen;

    if ((u2Len < TUNNEL_TABLE_INDICES) || (u2Len >= OAM_MAX_OID_LEN))
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsOamIfGetMegInfo: "
                    "Service is not configured INTMD-EXIT \n");
        return OSIX_FAILURE;
    }

    if (pOamFsMplsTpMegEntry->MibObject.
        i4FsMplsTpMegServiceType == OAM_SERVICE_TYPE_LSP)
    {
        /* Convert Service Pointer into Tunnel indices */
        u2Len--;
        pOutMplsApiInfo->OutMegInfo.MegMplsTnlId.u4DstLocalMapNum =
            pOamFsMplsTpMeEntry->MibObject.au4FsMplsTpMeServicePointer[u2Len];
        u2Len--;
        pOutMplsApiInfo->OutMegInfo.MegMplsTnlId.u4SrcLocalMapNum =
            pOamFsMplsTpMeEntry->MibObject.au4FsMplsTpMeServicePointer[u2Len];
        u2Len--;
        pOutMplsApiInfo->OutMegInfo.MegMplsTnlId.u4LspNum =
            pOamFsMplsTpMeEntry->MibObject.au4FsMplsTpMeServicePointer[u2Len];
        u2Len--;
        pOutMplsApiInfo->OutMegInfo.MegMplsTnlId.u4SrcTnlNum =
            pOamFsMplsTpMeEntry->MibObject.au4FsMplsTpMeServicePointer[u2Len];
    }
    else if (pOamFsMplsTpMegEntry->MibObject.
             i4FsMplsTpMegServiceType == OAM_SERVICE_TYPE_PW)
    {
        /* Convert Service Pointer into PW indices */
        pOutMplsApiInfo->OutMegInfo.MegPwId
            = pOamFsMplsTpMeEntry->MibObject.
            au4FsMplsTpMeServicePointer[u2Len - 1];
    }

    pOutMplsApiInfo->OutMegInfo.u4OperatorType =
        (UINT4) pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegOperatorType;

    MEMSET (&OamFsMplsTpGlobalConfigEntry, 0,
            sizeof (tOamFsMplsTpGlobalConfigEntry));
    /* Assign the index */
    OamFsMplsTpGlobalConfigEntry.MibObject.u4FsMplsTpContextId =
        pInMplsApiInfo->u4ContextId;

    pOamFsMplsTpGlobalConfigEntry = OamGetFsMplsTpGlobalConfigTable
        (&OamFsMplsTpGlobalConfigEntry);
    if (pOamFsMplsTpGlobalConfigEntry == NULL)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsOamIfGetMegInfo"
                    "GlobalId::NodeId get from global config table "
                    "failed: INTMD-EXIT \n");
        return OSIX_FAILURE;
    }
    pOutMplsApiInfo->OutMegInfo.MplsNodeId.u4GlobalId =
        pOamFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpGlobalId;
    pOutMplsApiInfo->OutMegInfo.MplsNodeId.u4NodeId =
        pOamFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpNodeIdentifier;

    pOutMplsApiInfo->OutMegInfo.u4MpIfIndex =
        (UINT4) pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMpIfIndex;

    pOutMplsApiInfo->OutMegInfo.u4IccSrcMepIndex =
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeSourceMepIndex;

    pOutMplsApiInfo->OutMegInfo.u4IccSinkMepIndex =
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeSinkMepIndex;

    pOutMplsApiInfo->OutMegInfo.u4MpType =
        (UINT4) pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMpType;

    pOutMplsApiInfo->OutMegInfo.u4MepDirection =
        (UINT4) pOamFsMplsTpMeEntry->MibObject.i4FsMplsTpMeMepDirection;

    pOutMplsApiInfo->OutMegInfo.u4ProactiveSessIndex =
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeProactiveOamSessIndex;

    MplsPortGetTrafficClassFromPhb (pOamFsMplsTpMeEntry->MibObject.
                                    i4FsMplsTpMeProactiveOamPhbTCValue,
                                    &u1TcValue);
    pOutMplsApiInfo->OutMegInfo.u4ProactiveTCValue = u1TcValue;

    MplsPortGetTrafficClassFromPhb (pOamFsMplsTpMeEntry->MibObject.
                                    i4FsMplsTpMeOnDemandOamPhbTCValue,
                                    &u1TcValue);
    pOutMplsApiInfo->OutMegInfo.u4OnDemandTCValue = u1TcValue;

    pOutMplsApiInfo->OutMegInfo.u4MeOperStatus =
        pOamFsMplsTpMeEntry->u4FsMplsTpMeOperStatus;

    MEMCPY (pOutMplsApiInfo->OutMegInfo.au1Icc,
            pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdIcc,
            pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdIccLen);

    pOutMplsApiInfo->OutMegInfo.u1ServiceType =
        (UINT1) pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegServiceType;

    pOutMplsApiInfo->OutMegInfo.u1ServiceLocation =
        (UINT1) pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegMpLocation;

    MEMCPY (pOutMplsApiInfo->OutMegInfo.au1Umc,
            pOamFsMplsTpMegEntry->MibObject.au1FsMplsTpMegIdUmc,
            pOamFsMplsTpMegEntry->MibObject.i4FsMplsTpMegIdUmcLen);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : MplsOamIfGetMegIdFromMegName                               */
/*                                                                           */
/* Description  : This function gets the MPLS MEG id from MEG name           */
/*                based on the request from MPLS external module.            */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                                                                           */
/* Output       : pOutMplsApiInfo - Pointer to the output API information.   */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
MplsOamIfGetMegIdFromMegName (tMplsApiInInfo * pInMplsApiInfo,
                              tMplsApiOutInfo * pOutMplsApiInfo)
{
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntry = NULL;

    pOamFsMplsTpMeEntry = OamUtilGetMeEntryFromMegName
        (pInMplsApiInfo->u4ContextId,
         pInMplsApiInfo->InPathId.MegMeName.au1MegName,
         pInMplsApiInfo->InPathId.MegMeName.au1MeName);
    if (pOamFsMplsTpMeEntry == NULL)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsOamIfGetMegIdFromMegName "
                    "MPLS ME entry does not exist: INTMD-EXIT \n");
        return OSIX_FAILURE;
    }
    pOutMplsApiInfo->u4ContextId =
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpContextId;
    pOutMplsApiInfo->OutPathId.MegId.u4MegIndex =
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMegIndex;
    pOutMplsApiInfo->OutPathId.MegId.u4MeIndex =
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeIndex;
    pOutMplsApiInfo->OutPathId.MegId.u4MpIndex =
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeMpIndex;
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : MplsOamIfCheckAndUpdateTnlForMeg                           */
/*                                                                           */
/* Description  : This function checks and updates the MEG indices           */
/*                in the tunnel table if tunnel exists                       */
/*                                                                           */
/* Input        : u4ContextId - ContextId                                    */
/*                pMplsMegId - Meg indices                                   */
/*                pTnlId - Tunnel indices                                    */
/* Output       : pMplsOamPathStatus - PATH status (UP/DOWN)                 */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
MplsOamIfCheckAndUpdateTnlForMeg (UINT4 u4ContextId, tMplsMegId * pMplsMegId,
                                  tMplsTnlLspId * pTnlId,
                                  tMplsOamPathStatus * pMplsOamPathStatus)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    UNUSED_PARAM (u4ContextId);

    /* Get the tunnel information */
    pTeTnlInfo = TeGetTunnelInfo (pTnlId->u4SrcTnlNum,
                                  pTnlId->u4LspNum,
                                  pTnlId->u4SrcLocalMapNum,
                                  pTnlId->u4DstLocalMapNum);
    if (pTeTnlInfo == NULL)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsOamIfCheckAndUpdateTnlForMeg "
                    "MPLS Tunnel entry does not exist: INTMD-EXIT \n");
        return OSIX_FAILURE;
    }
    pTeTnlInfo->u4MegIndex = pMplsMegId->u4MegIndex;
    pTeTnlInfo->u4MeIndex = pMplsMegId->u4MeIndex;
    pTeTnlInfo->u4MpIndex = pMplsMegId->u4MpIndex;

    /* Update the OAM path status */
    if ((pTeTnlInfo->u1TnlSwitchApp == TE_SWITCH_APP_ELPS) &&
        (pTeTnlInfo->u1TnlOperStatus == TE_OPER_UP) &&
        (pTeTnlInfo->u1CPOrMgmtOperStatus == TE_OPER_UP))
    {
        pMplsOamPathStatus->u1PathStatus = MPLS_PATH_STATUS_UP;
    }
    else if ((pTeTnlInfo->u1TnlSwitchApp != TE_SWITCH_APP_ELPS) &&
             (pTeTnlInfo->u1TnlOperStatus == TE_OPER_UP))
    {
        pMplsOamPathStatus->u1PathStatus = MPLS_PATH_STATUS_UP;
    }
    else
    {
        pMplsOamPathStatus->u1PathStatus = MPLS_PATH_STATUS_DOWN;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : MplsOamIfUpdateOamAppStatus                                */
/*                                                                           */
/* Description  : This function enables or disables the status notification  */
/*                of the path entry to Y.1731 module                         */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
MplsOamIfUpdateOamAppStatus (tMplsApiInInfo * pInMplsApiInfo)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    if (pInMplsApiInfo->InPathId.u4PathType == MPLS_PATH_TYPE_TUNNEL)
    {
        pTeTnlInfo = TeGetTunnelInfo (pInMplsApiInfo->InPathId.
                                      TnlId.u4SrcTnlNum,
                                      pInMplsApiInfo->InPathId.
                                      TnlId.u4LspNum,
                                      pInMplsApiInfo->InPathId.
                                      TnlId.SrcNodeId.
                                      MplsRouterId.u4_addr[0],
                                      pInMplsApiInfo->InPathId.
                                      TnlId.DstNodeId.MplsRouterId.u4_addr[0]);
        if (pTeTnlInfo == NULL)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsOamIfUpdateOamAppStatus: "
                        "Tunnel entry doesn't exist : INTMD-EXIT \n");
            return OSIX_FAILURE;
        }
        if (pTeTnlInfo->bTnlIntOamEnable == FALSE)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsOamIfUpdateOamAppStatus: "
                        "OAM is not enabled on the tunnel: INTMD-EXIT \n");
            return OSIX_FAILURE;
        }
        if (pInMplsApiInfo->u4SrcModId == Y1731_MODULE)
        {
            if (pInMplsApiInfo->InY1731OamStatus == TRUE)
            {
                pTeTnlInfo->u1MplsOamAppType = MPLS_OAM_TYPE_Y1731;
            }
            else
            {
                pTeTnlInfo->u1MplsOamAppType = MPLS_OAM_TYPE_NONE;
            }
        }
#ifdef RFC6374_WANTED
        if (pInMplsApiInfo->u4SrcModId == RFC6374_MODULE)
        {
            if (pInMplsApiInfo->InRFC6374Status == TRUE)
            {
                pTeTnlInfo->u1MplsPmAppType = MPLS_PM_TYPE_RFC6374;
            }
            else
            {
                pTeTnlInfo->u1MplsPmAppType = MPLS_OAM_TYPE_NONE;
            }
        }
#endif
    }
    else
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsOamIfUpdateOamAppStatus: "
                    "Invalid path type: INTMD-EXIT \n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : MplsOamIfRegAppCallBack                                    */
/*                                                                           */
/* Description  : This function registers with MPLS module for               */
/*                notifications (PATH_UP/PATH_DOWN/Application packets)      */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
MplsOamIfRegAppCallBack (tMplsApiInInfo * pInMplsApiInfo)
{
    MPLSFM_DBG3 (MPLSFM_IF_PRCS,
                 "MplsOamIfRegAppCallBack: "
                 "Registration with MPLS module from AppId=%d "
                 "Events=%x Request type=%d\n",
                 pInMplsApiInfo->InRegParams.u4ModId,
                 pInMplsApiInfo->InRegParams.u4Events,
                 pInMplsApiInfo->u4SubReqType);
    if ((pInMplsApiInfo->u4SubReqType == MPLS_APP_REG_CALL_BACK) &&
        (gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
         [pInMplsApiInfo->InRegParams.u4ModId] == NULL))
    {
        if (pInMplsApiInfo->InRegParams.u4Events == 0)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsOamIfRegAppCallBack: "
                        "No events given for registration: INTMD-EXIT \n");
            return OSIX_FAILURE;
        }
        gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
            [pInMplsApiInfo->InRegParams.u4ModId]
            = (tMplsRegParams *) MemAllocMemBlk (MPLS_REG_APPS_POOL_ID);
        if (gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
            [pInMplsApiInfo->InRegParams.u4ModId] == NULL)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsOamIfRegAppCallBack: "
                        "MemAlloc failed for registration: INTMD-EXIT \n");
            return OSIX_FAILURE;
        }
    }
    gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
        [pInMplsApiInfo->InRegParams.u4ModId]->u4ModId =
        pInMplsApiInfo->InRegParams.u4ModId;
    gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
        [pInMplsApiInfo->InRegParams.u4ModId]->u4Events =
        pInMplsApiInfo->InRegParams.u4Events;
    gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
        [pInMplsApiInfo->InRegParams.u4ModId]->pFnRcvPkt =
        pInMplsApiInfo->InRegParams.pFnRcvPkt;
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : MplsOamIfGetOamSessionId                                   */
/*                                                                           */
/* Description  : This function gets the session identifier of the OAM       */
/*                associated to the given MEP.                               */
/*                                                                           */
/* Input        : u4MepIndex -                                               */
/*                u4MeIndex -                                                */
/*                u4MpIndex -                                                */
/*                                                                           */
/* Output       : pu4OamSessionId - session identifier associated to the MEP */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
MplsOamIfGetOamSessionId (UINT4 u4MegIndex, UINT4 u4MeIndex,
                          UINT4 u4MpIndex, UINT4 *pu4OamSessionId)
{

    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntryTemp = NULL;
    tOamFsMplsTpMeEntry *pOamFsMplsTpMeEntry = NULL;

    pOamFsMplsTpMeEntryTemp = (tOamFsMplsTpMeEntry *)
        MemAllocMemBlk (MPLS_OAM_FSMPLS_TP_ME_TABLE_POOL_ID);
    if (pOamFsMplsTpMeEntryTemp == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pOamFsMplsTpMeEntryTemp, 0, sizeof (tOamFsMplsTpMeEntry));
    /* Assign the ME indices */
    pOamFsMplsTpMeEntryTemp->MibObject.u4FsMplsTpContextId = 0;
    pOamFsMplsTpMeEntryTemp->MibObject.u4FsMplsTpMegIndex = u4MegIndex;
    pOamFsMplsTpMeEntryTemp->MibObject.u4FsMplsTpMeIndex = u4MeIndex;
    pOamFsMplsTpMeEntryTemp->MibObject.u4FsMplsTpMeMpIndex = u4MpIndex;

    pOamFsMplsTpMeEntry = OamGetFsMplsTpMeTable (pOamFsMplsTpMeEntryTemp);
    if (pOamFsMplsTpMeEntry == NULL)
    {
        MemReleaseMemBlock (MPLS_OAM_FSMPLS_TP_ME_TABLE_POOL_ID,
                            (UINT1 *) pOamFsMplsTpMeEntryTemp);
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsOamIfUpdateSessionParams: "
                    "ME entry doesn't exist : INTMD-EXIT \n");
        return OSIX_FAILURE;
    }
    *pu4OamSessionId =
        pOamFsMplsTpMeEntry->MibObject.u4FsMplsTpMeProactiveOamSessIndex;
    MemReleaseMemBlock (MPLS_OAM_FSMPLS_TP_ME_TABLE_POOL_ID,
                        (UINT1 *) pOamFsMplsTpMeEntryTemp);

    return OSIX_SUCCESS;
}
