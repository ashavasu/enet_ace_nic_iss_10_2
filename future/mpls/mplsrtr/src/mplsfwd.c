/*---------------------------------------------------------------------------*/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: mplsfwd.c,v 1.91 2017/07/31 13:38:55 siva Exp $
 * ---------------------------------------------------------------------------*/

#include "mplsincs.h"
#include "mplsutil.h"
#include "mplsnp.h"
#include "tcp.h"
#include "mplslsr.h"
#include "fsvlan.h"
#include "l2vpincs.h"
#include "mplsftn.h"
#include "rpteext.h"
#include "arp.h"
#include "rtm.h"
#include "mpoamdef.h"
#include "mplcmndb.h"
#include "temacs.h"
#include "mplsdiff.h"

#ifdef MPLS_L3VPN_WANTED
#include "l3vpntmr.h"
#include "l3vpntdfsg.h"
#include "l3vpntdfs.h"
#include "l3vpnprot.h"
#include "l3vpndefn.h"
#include "ip.h"
extern INT4
 
 
 
 
IpInputHeaderValidateExtract (tIP_BUF_CHAIN_HEADER * pBuf, t_IP * pIp,
                              UINT2 u2Port);

extern INT4
        CfaExtractIpHdr (t_IP_HEADER * pIp, tCRU_BUF_CHAIN_HEADER * pBuf);
#endif

#define MPLSFWD_L3VPN_DBG(x, ARGS...)

PRIVATE             tLabelAction
MplsGetLblEntryAndAction (UINT1 *pPkt, UINT4 u4IfIndex, VOID **pEntry,
                          BOOL1 * bIsDblLblLookup, BOOL1 * bIsPacketOamPacket,
                          BOOL1 * pbIsRalPresent, UINT4 *pu4SearchLabel);
PRIVATE INT4        MplsLabelSwitching (tCRU_BUF_CHAIN_HEADER * pBuf,
                                        VOID *pVoid, BOOL1 bIsRalPresent);
PRIVATE INT4        MplsSendL3LabelPktToIp (tCRU_BUF_CHAIN_HEADER * pBuf,
                                            tEnetV2Header * pEthHdr,
                                            UINT4 u4L3Intf, UINT4 u4IfIndex);
/* MPLS_IPv6 add start */
#ifdef MPLS_IPV6_WANTED
PRIVATE INT4        MplsSendL3LabelPktToIpv6 (tCRU_BUF_CHAIN_HEADER * pBuf,
                                              UINT4 u4IfIndex);
#endif
/* MPLS_IPv6 add end*/
PRIVATE INT4        MplsLabelAndSendout (tCRU_BUF_CHAIN_HEADER *, tXcEntry *,
                                         UINT4, UINT4, BOOL1);
PRIVATE INT4        MplsRemoveMplsHdr (tCRU_BUF_CHAIN_HEADER *, tMplsHdr *);

#ifdef MPLS_L3VPN_WANTED
PRIVATE VOID        MplsTriggerArpRequest (UINT4 u4IpAddress, UINT2 u2Port);
#endif

#ifdef LNXIP4_WANTED
extern VOID         LnxIpPostPktToTap (UINT4, tCRU_BUF_CHAIN_HEADER *,
                                       tEnetV2Header *, UINT1);
#endif

/* ************************************************************************* *
 * Function Name   : MplsProcessLabeledPacket                                *
 * Description     : Processes the incoming MPLS labeled packet to do the    *
 *                   MPLS software forwading.                                *
 * Input(s)        : pBuf - Pointer pointing to the first Eth header of the  *
 *                          packet.                                          *
 *                   u4IfIndex - Source port index on which pkt was rcvd     *
 * Output(s)       : None                                                    *
 * Return(s)       : MPLS_FAILURE/MPLS_SUCCESS                               *
 * ************************************************************************* */
INT4
MplsProcessLabeledPacket (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                          UINT4 u4PktSize)
{
    UINT4               u4Temp;
    UINT4               u4Ttl = 0;
    UINT4               u4L3Intf = MPLS_ZERO;
    UINT4               u4Vsi;
    UINT4               au4SearchLabel[MPLS_MAX_LABEL_STACK];
    UINT4               u4AchHdr = 0;
    UINT2               u2Temp = 0;
    UINT2               u2VlanId = 0;
    UINT2               u2VlanIdInPkt = 0;
    UINT2               u2UdpDstPort = 0;
    UINT1               u1VlanMode = 0;
    UINT1               u1Version = 0;
    UINT1               u1IpProto = 0;
    UINT1               u1IfType = 0;
    UINT1              *pu1Buf = NULL;
    UINT1              *pPkt = NULL;
    UINT1              *pTmpHdr = NULL;
    VOID               *pEntry = NULL;
    tMplsHdr            MplsHdr;
    tMacAddr            SrcMac;
    tMacAddr            DstMacAddr;
    tLabelAction        Action;
    tPwVcEntry         *pPwVcEntry = NULL;
    tPwVcEntry         *pBkpPwVcEntry = NULL;
    tVPLSEntry         *pVplsEntry = NULL;
    tPwVcEnetEntry     *pPwVcEnetEntry = NULL;
    tPwVcEnetServSpecEntry *pPwVcEnetServSpecEntry = NULL;
    tPwVcPerfStatsEntry *pPwVcPerfInfo = NULL;
    UINT1               au1BufTmp[VLAN_TAGGED_HEADER_SIZE];
    UINT2               u2Tag = 0;
    UINT2               u2Protocol;
#ifdef CFA_WANTED
    UINT2               u2PortFound = L2VPN_ZERO;
    UINT2               u2MatchedAc = L2VPN_ZERO;
    tCRU_BUF_CHAIN_HEADER *pEthBuf = NULL;
    UINT1               u1Status = 0;
    UINT2               u2Port = 0;
#endif
#ifdef HVPLS_WANTED
    UINT2               u2ProtocolinVlan = 0;
    UINT2               u2EcfmType = 0x8902;
#endif
    BOOL1               bIsDblLblLookup = FALSE;
    BOOL1               bIsPacketOamPacket = FALSE;
    BOOL1               bIsRalPresent = FALSE;
    BOOL1               bIsRalHeaderStripped = FALSE;
    BOOL1               bIsPopSearch = FALSE;
    UINT4               u4DstIpAddress = 0;
#ifdef MPLS_L3VPN_WANTED
    t_IP_HEADER         IpHdr;
    UINT4               u4VrfId = 0;
    UINT4               u4ArpIfIndex = 0;
    INT1                i1Hw_addr[CFA_ENET_ADDR_LEN];
    UINT1               u1EncapType;
    tRtInfoQueryMsg     RtQuery;
    tNetIpv4RtInfo      NetIpRtInfo;
    UINT4               u4NextHop = 0;

#endif

    tEnetV2Header       EthHdr;
    UINT2               u2PriInPacket = 0;
    /* copy packet from CRU buffer to linear buffer starting at MPLS header
     * copy till the end of the buffer since i dont know how many MPLS headers
     * it contains in it */

#ifdef MPLS_L3VPN_WANTED
    MEMSET (&IpHdr, 0, sizeof (t_IP_HEADER));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));
    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
#endif

    MEMSET (&EthHdr, 0, sizeof (tEnetV2Header));

    pu1Buf = (UINT1 *) MemAllocMemBlk (MPLS_CFA_ENET_MTU_POOL_ID);
    if (pu1Buf == NULL)
    {
        return MPLS_FAILURE;
    }
    MEMSET (pu1Buf, 0, CFA_ENET_MTU);
    MEMSET (au1BufTmp, 0, VLAN_TAGGED_HEADER_SIZE);

    for (u2Temp = 0; u2Temp < MPLS_MAX_LABEL_STACK; u2Temp++)
    {
        au4SearchLabel[u2Temp] = MPLS_INVALID_LABEL;
    }

    u2Temp = 0;
    u4Temp = CFA_ENET_V2_HEADER_SIZE;
    if (CRU_BUF_Copy_FromBufChain (pBuf, pu1Buf, u4Temp, u4PktSize) ==
        CRU_FAILURE)
    {
        MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID, (UINT1 *) pu1Buf);
        return MPLS_FAILURE;
    }

    if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &EthHdr, 0,
                                   CFA_ENET_V2_HEADER_SIZE) == CRU_FAILURE)
    {
        MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID, (UINT1 *) pu1Buf);
        return MPLS_FAILURE;
    }

    pPkt = pu1Buf;

    /* Here we are going to process the packet which are coming from
     * NP/NPSIM. Whenever labeled packet coming from data plane(NP/NPSIM)
     * to control plane(CPU), then we will process the packet and send
     * packet to related module.
     */

#ifdef NPAPI_WANTED
    if ((u1IpProto == MPLS_UDP_PROT_ID) &&
        (((u2UdpDstPort == MPLS_BFD_UDP_DST_PORT) && (u1Version == IP_VERSION_4)
          && ((u4DstIpAddress & MPLS_4TH_BYTE_MASK) == IP_LOOPBACK_ADDRESS))
         || (u2UdpDstPort == MPLS_LSP_PING_UDP_DST_PORT)))
    {
        if (MplsOamRxHandleCtrlPkt (pBuf, pPkt, u4IfIndex, u4PktSize)
            == MPLS_SUCCESS)
        {
            /* Packet has been successfully delivered to OAM applications */
            MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID, (UINT1 *) pu1Buf);
            return MPLS_SUCCESS;
        }
        else
        {
            /* In case of NPAPI_WANTED, Handling of control packet has failed,
             * so, allow the packet to be forwarded through software forwarding. */
        }
    }
#endif

    do
    {
        MplsReadMplsHdr (pPkt, &MplsHdr);
        u4Ttl = MplsHdr.Ttl;

        if (bIsPopSearch == TRUE)
        {
            if (gMplsIncarn[MPLS_DEF_INCARN].DiffServGlobal.u1TnlModel ==
                MPLS_TNL_UNIFORM_MODEL)
            {
                /* Save TTL for egress processing */
                MPLS_SAVE_TTL (pBuf, gMplsIncarn[MPLS_DEF_INCARN].u1PrviTTL);
            }
            else
            {
                /* Save TTL for egress processing */
                MPLS_SAVE_TTL (pBuf, MplsHdr.Ttl);
                gMplsIncarn[MPLS_DEF_INCARN].u1iTTL = (UINT1) u4Ttl;
            }
            bIsPopSearch = FALSE;
        }
        else
        {
            gMplsIncarn[MPLS_DEF_INCARN].u1iTTL = (UINT1) u4Ttl;
            MPLS_SAVE_TTL (pBuf, u4Ttl);
        }

        Action =
            MplsGetLblEntryAndAction (pPkt, u4IfIndex, &pEntry,
                                      &bIsDblLblLookup, &bIsPacketOamPacket,
                                      &bIsRalPresent, au4SearchLabel);
        if (bIsPacketOamPacket == TRUE)
        {
            if (MplsOamRxHandleCtrlPkt (pBuf, pu1Buf, u4IfIndex, u4PktSize)
                == MPLS_SUCCESS)
            {
                /* Packet has been successfully delivered to OAM applications */
                MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                    (UINT1 *) pu1Buf);
                return MPLS_SUCCESS;
            }
            else
            {
                /* Since the packet received is a OAM control packet and the
                 * packet handling has failed, it should not be forwarded.
                 * Drop it. */
                MPLS_REL_BUF_CHAIN (pBuf, MPLS_BUF_NORMAL_REL);
                MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                    (UINT1 *) pu1Buf);
                return MPLS_FAILURE;
            }
        }
        pPkt += MPLS_HDR_LEN;
        if ((bIsRalPresent) && (!bIsRalHeaderStripped))
        {
            u4Temp += MPLS_HDR_LEN;
            pPkt += MPLS_HDR_LEN;
            bIsRalHeaderStripped = TRUE;
        }

        switch (Action)
        {
            case MPLS_HW_ACTION_POP_SEARCH:
                /* Pop topmost 'N' labels */
                if (bIsDblLblLookup)
                {
                    pPkt += MPLS_HDR_LEN;
                    u4Temp += MPLS_HDR_LEN;
                }
                gMplsIncarn[MPLS_DEF_INCARN].u1PrviTTL = (UINT1) u4Ttl;
                bIsPopSearch = TRUE;
                u4Temp += MPLS_HDR_LEN;
                break;

                /* LSR Action(s) */

            case MPLS_HW_ACTION_SWAP:
                if (bIsDblLblLookup == TRUE)
                {
                    u4Temp += MPLS_HDR_LEN;
                }
                CRU_BUF_Move_ValidOffset (pBuf, u4Temp);
                MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                    (UINT1 *) pu1Buf);
                if (gMplsIncarn[MPLS_DEF_INCARN].u1iTTL == MPLS_ZERO)
                {
                    return MPLS_FAILURE;
                }
                gMplsIncarn[MPLS_DEF_INCARN].u1OTTL =
                    (UINT1) (gMplsIncarn[MPLS_DEF_INCARN].u1iTTL - 1);
                if (gMplsIncarn[MPLS_DEF_INCARN].u1PrviTTL == 0)
                {
                    gMplsIncarn[MPLS_DEF_INCARN].u1PrviTTL =
                        gMplsIncarn[MPLS_DEF_INCARN].u1iTTL;
                    gMplsIncarn[MPLS_DEF_INCARN].u1PrvOTTL =
                        gMplsIncarn[MPLS_DEF_INCARN].u1OTTL;
                }
                return (MplsLabelSwitching (pBuf, pEntry, bIsRalPresent));

                /* Egress Actions */

            case MPLS_HW_ACTION_POP_L2_SWITCH:
                /* Verifying for ACH header we will remove outer
                 * Ethernet, Vlan ,Mpls headers and Pseudo wire label
                 */
                pTmpHdr = CRU_BUF_Get_DataPtr_IfLinear (pBuf,
                                                        u4Temp + MPLS_HDR_LEN,
                                                        MPLS_ACH_HEADER_LENGTH);
                if (pTmpHdr == NULL)
                {
                    MPLS_REL_BUF_CHAIN (pBuf, MPLS_BUF_NORMAL_REL);
                    MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                        (UINT1 *) pu1Buf);
                    return MPLS_FAILURE;
                }

                u4AchHdr = *(UINT4 *) (VOID *) pTmpHdr;

                if ((((UINT1) (u4AchHdr) & MPLS_ACH_TYPE_MASK) ==
                     MPLS_ACH_HEADER) || (bIsRalPresent == TRUE))
                {
                    if (MplsOamRxHandleCtrlPkt (pBuf, pu1Buf, u4IfIndex,
                                                u4PktSize) == MPLS_SUCCESS)
                    {
                        /* Packet has been successfully delivered
                         * to OAM applications
                         */
                        MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                            (UINT1 *) pu1Buf);
                        return MPLS_SUCCESS;
                    }
                    else
                    {
                        /* Since the packet received is a OAM control packet
                         * and the packet handling has failed, it should not be
                         * forwarded. Drop it. */
                        MPLS_REL_BUF_CHAIN (pBuf, MPLS_BUF_NORMAL_REL);
                        MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                            (UINT1 *) pu1Buf);
                        return MPLS_FAILURE;
                    }
                }
                /* Remove outer Ethernet, Vlan and Mpls headers */
                CRU_BUF_Move_ValidOffset (pBuf, u4Temp);
                /* Remove current MPLS header */
                CRU_BUF_Move_ValidOffset (pBuf, MPLS_HDR_LEN);

                pPwVcEntry = (tPwVcEntry *) pEntry;
                /*Remove pw ethernet control word */
                if ((gi4MplsSimulateFailure ==
                     L2VPN_SIM_FAILURE_CTRLW_MANDATORY)
                    && (pPwVcEntry->i1CwStatus == L2VPN_PWVC_CWPRESENT))
                {
                    CRU_BUF_Move_ValidOffset (pBuf, 4);

                    pPkt += 4;
                }

                pVplsEntry =
                    L2VpnGetVplsEntryFromInstanceIndex
                    (L2VPN_PWVC_VPLS_INDEX (pPwVcEntry));

                if (pPwVcEntry->u1CPOrMgmtOperStatus != L2VPN_PWVC_OPER_UP)
                {
                    MPLSFM_DBG1 (MPLSFM_PRCS_PRCS,
                                 "PW %d is not operationally UP\n",
                                 pPwVcEntry->u4PwVcIndex);
                    MPLS_REL_BUF_CHAIN (pBuf, MPLS_BUF_NORMAL_REL);
                    MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                        (UINT1 *) pu1Buf);
                    return MPLS_FAILURE;
                }

                if (pPwVcEntry->u1ElpsMode == ELPS_PG_ARCH_1_PLUS_1)
                {

                    pBkpPwVcEntry =
                        L2VpnGetPwVcEntryFromVcId (pPwVcEntry->u4BkpPwVcID);

                    if ((pBkpPwVcEntry != NULL) &&
                        (pPwVcEntry->u1PwPathType == L2VPN_PW_PROTECTION_PATH)
                        && (pBkpPwVcEntry->u1ProtStatus != LOCAL_PROT_IN_USE))
                    {
                        MPLSFM_DBG1 (MPLSFM_PRCS_PRCS,
                                     "PW %d is for alternate path of ELPS 1+1. "
                                     "Drop the pkt\n", pPwVcEntry->u4PwVcIndex);
                        MPLS_REL_BUF_CHAIN (pBuf, MPLS_BUF_NORMAL_REL);
                        MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                            (UINT1 *) pu1Buf);
                        return MPLS_FAILURE;

                    }
                }

                if (pVplsEntry != NULL)
                {
                    u4Vsi = (UINT4) L2VPN_VPLS_VSI (pVplsEntry);
                }
                else
                {
                    u4Vsi = 0;
                }
                /* Native Service Processing (NSP) PSN -> AC direction */
    /***********************************************************************
      | Pw Type | Enet Vlan Mode | PE - CE link (AC bound direction) Remarks | 
      | ********************************************************************* |
      | Tagged  | Change Vlan,   |                                            |
      |         | Remove Vlan    |                                            | 
      | --------------------------------------------------------------------- |
      | Raw     | Add Vlan       | Only service delimiting tag can be added.  | 
     ************************************************************************/
                u1VlanMode = (UINT1) L2VPN_PWVC_TYPE (pPwVcEntry);
                /* Learn the mac on first vlan after mpls hdr and pw */

                u4Temp = MAC_ADDR_LEN;
                MEMCPY (DstMacAddr, pPkt, MAC_ADDR_LEN);
                MEMCPY (SrcMac, pPkt + u4Temp, MAC_ADDR_LEN);
                if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u2Protocol,
                                               CFA_VLAN_TAG_OFFSET,
                                               CFA_VLAN_PROTOCOL_SIZE) ==
                    CRU_FAILURE)
                {
                    MPLS_REL_BUF_CHAIN (pBuf, MPLS_BUF_NORMAL_REL);
                    MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                        (UINT1 *) pu1Buf);
                    return MPLS_FAILURE;

                }
                u2Protocol = OSIX_NTOHS (u2Protocol);

                /* Get the VLAN Tag value only if Ethertype in the ethernet header is
                 * 0x8100 or 0x88a8. */
                if ((u2Protocol == VLAN_PROVIDER_PROTOCOL_ID) ||
                    (u2Protocol == VLAN_PROTOCOL_ID))
                {
                    u4Temp = CFA_VLAN_TAG_OFFSET + CFA_VLAN_PROTOCOL_SIZE;
                    MEMCPY (&u2VlanIdInPkt, pPkt + u4Temp, 2);
                    u2VlanIdInPkt = OSIX_NTOHS (u2VlanIdInPkt);
                    u2PriInPacket = u2VlanIdInPkt >> 13;
                    u2VlanIdInPkt = 0x0fff & u2VlanIdInPkt;
#ifdef HVPLS_WANTED
                    /*Check if CFM Packet is received, forward to MPLS-OAM to process the packet */
                    MEMCPY (&u2ProtocolinVlan, pPkt + u4Temp + 2, 2);
                    u2ProtocolinVlan = OSIX_NTOHS (u2ProtocolinVlan);
                    if (u2EcfmType == u2ProtocolinVlan
                        && pPwVcEntry->u4PwIfIndex != 0)
                    {
                        if (L2vpnHandleCfmPacket
                            (pBuf, pPwVcEntry->u4PwIfIndex,
                             u4PktSize) == MPLS_SUCCESS)
                        {
                            MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                                (UINT1 *) pu1Buf);
                            return MPLS_SUCCESS;
                        }
                        else
                        {
                            MPLSFM_DBG (MPLSFM_PRCS_PRCS,
                                        " CFM Packet delievery to OAM failed");
                            MPLS_REL_BUF_CHAIN (pBuf, MPLS_BUF_NORMAL_REL);
                            MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                                (UINT1 *) pu1Buf);
                            return MPLS_FAILURE;
                        }
                    }

#endif
                }

                /***********************************************************************
                | Pw Type | Enet Vlan Mode | PE - CE link (AC bound direction) Remarks | 
                | ********************************************************************* |
                | Tagged  | Change Vlan,   |                                            |
                |         | Remove Vlan    |                                            | 
                | --------------------------------------------------------------------- |
                | Raw     | Add Vlan       | Only service delimiting tag can be added.  | 
                ************************************************************************/
                u1VlanMode = (UINT1) L2VPN_PWVC_TYPE (pPwVcEntry);
                pPwVcEnetServSpecEntry = L2VPN_PWVC_ENET_ENTRY (pPwVcEntry);
                if (pPwVcEnetServSpecEntry == NULL)
                {
                    MPLS_REL_BUF_CHAIN (pBuf, MPLS_BUF_NORMAL_REL);
                    MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                        (UINT1 *) pu1Buf);
                    return MPLS_FAILURE;
                }

                /*VLAN_MEMSET (&TempVlanL2VpnInfo, VLAN_INIT_VAL, sizeof (tVlanL2VpnMap));

                   TempVlanL2VpnInfo.u4ContextId = VLAN_CURR_CONTEXT_ID ();
                   TempVlanL2VpnInfo.u4IfIndex = VLAN_GET_IFINDEX(pVlanIf->u2LocalPort);
                   Searching Port Based l2VPN Map table 

                   pVlanL2VpnInfo = RBTreeGet (gpVlanL2VpnPortMapTbl, &TempVlanL2VpnInfo); */

                if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS)
                {
                    if (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
                    {
                        pPwVcEnetEntry = (tPwVcEnetEntry *) TMO_SLL_First
                            (L2VPN_PWVC_ENET_ENTRY_LIST
                             (pPwVcEnetServSpecEntry));
                        if (pPwVcEnetEntry == NULL)
                        {
                            /* Invalid vlan for this VPLS instance - drop it */
                            break;
                        }
                        /* TODO, As of now learning is based on EnetPwVlan.
                           Native Service Processing (NSP) PSN->AC direction */
                        if (L2VPN_PWVC_ENET_VLAN_MODE (pPwVcEnetEntry) ==
                            L2VPN_VLANMODE_ADDVLAN)
                        {
                            u2VlanId = L2VPN_PWVC_ENET_PW_VLAN (pPwVcEnetEntry);
                        }
                        else
                        {
                            u2VlanId =
                                L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry);
                        }

                    }
                    /* VPLS Port based */
                    if (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
                    {
                        if (L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) ==
                            L2VPN_PWVC_ENET_DEF_PORT_VLAN)
                        {
                            u4IfIndex =
                                (UINT4)
                                L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry);
                            if (u2VlanId == L2VPN_PWVC_ENET_DEF_PORT_VLAN)
                            {
                                u2VlanId = L2VPN_ONE;
                            }
                        }
                    }
                    else        /* VPLS Vlan based */
                    {
                        /* Identify the ENET entry corr. to the incoming pkt */
                        TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST
                                      (pPwVcEnetServSpecEntry), pPwVcEnetEntry,
                                      tPwVcEnetEntry *)
                        {
                            if (u2VlanIdInPkt ==
                                L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry))
                            {
                                /* Found the matching AC entry */
                                u2VlanId = u2VlanIdInPkt;
                                u4IfIndex =
                                    (UINT4)
                                    L2VPN_PWVC_ENET_PORT_IF_INDEX
                                    (pPwVcEnetEntry);

                                break;
                            }
                            /* Vlan Identifier is retrieved from Enet Port Vlan 
                             * value when it is not present in the received 
                             * packet */
                            else if (u2VlanIdInPkt == L2VPN_ZERO)
                            {
                                u2VlanId = L2VPN_PWVC_ENET_PORT_VLAN
                                    (pPwVcEnetEntry);
                                u4IfIndex =
                                    (UINT4)
                                    L2VPN_PWVC_ENET_PORT_IF_INDEX
                                    (pPwVcEnetEntry);
                                break;
                            }
                        }
                        if (pPwVcEnetEntry == NULL)
                        {
                            /* Invalid vlan for this VPLS instance - drop it */
                            break;
                        }
                    }
                    if (L2VPN_VPLS_FDB_DEF_VAL ==
                        L2VPN_VPLS_FDB_ID (pVplsEntry))
                    {
#ifdef HVPLS_WANTED
                        if (L2VPN_PWVC_TYPE (pPwVcEntry) != L2VPN_PWVC_TYPE_ETH
                            && pPwVcEntry->u4PwIfIndex != 0)
                        {
                            /*if (AST_PORT_STATE_DISCARDING == VlanL2IwfGetVlanPortState (L2VPN_VPLS_FDB_ID (pVplsEntry), pPwVcEntry->u4PwIfIndex)) */
                            if (AST_PORT_STATE_DISCARDING !=
                                L2IwfGetInstPortState (RST_DEFAULT_INSTANCE,
                                                       pPwVcEntry->u4PwIfIndex))
                            {
                                VlanVplsLearn (u4Vsi, SrcMac,
                                               L2VPN_VPLS_FDB_ID (pVplsEntry),
                                               L2VPN_PWVC_INDEX (pPwVcEntry));
                            }

                        }
                        else
                        {
#endif
                            VlanVplsLearn (u4Vsi, SrcMac, u2VlanId,
                                           L2VPN_PWVC_INDEX (pPwVcEntry));
#ifdef HVPLS_WANTED
                        }
#endif
                    }
                    else
                    {
#ifdef HVPLS_WANTED
                        if (L2VPN_PWVC_TYPE (pPwVcEntry) != L2VPN_PWVC_TYPE_ETH
                            && pPwVcEntry->u4PwIfIndex != 0)
                        {
                            /*if (AST_PORT_STATE_DISCARDING == VlanL2IwfGetVlanPortState (L2VPN_VPLS_FDB_ID (pVplsEntry), pPwVcEntry->u4PwIfIndex)) */
                            if (AST_PORT_STATE_DISCARDING !=
                                L2IwfGetInstPortState (RST_DEFAULT_INSTANCE,
                                                       pPwVcEntry->u4PwIfIndex))
                            {
                                VlanVplsLearn (u4Vsi, SrcMac,
                                               L2VPN_VPLS_FDB_ID (pVplsEntry),
                                               L2VPN_PWVC_INDEX (pPwVcEntry));
                            }

                        }
                        else
                        {
#endif
                            VlanVplsLearn (u4Vsi, SrcMac,
                                           L2VPN_VPLS_FDB_ID (pVplsEntry),
                                           L2VPN_PWVC_INDEX (pPwVcEntry));
#ifdef HVPLS_WANTED
                        }
#endif

                    }

                }
                else            /* VPWS - PW is attached with only one vlan */
                {
                    pPwVcEnetEntry = (tPwVcEnetEntry *) TMO_SLL_First
                        (L2VPN_PWVC_ENET_ENTRY_LIST (pPwVcEnetServSpecEntry));
                    if (pPwVcEnetEntry == NULL)
                    {
                        MPLSFM_DBG (MPLSFM_PRCS_PRCS,
                                    "-E- Enet Entry does not exist");
                        MPLS_REL_BUF_CHAIN (pBuf, MPLS_BUF_NORMAL_REL);
                        MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                            (UINT1 *) pu1Buf);
                        return MPLS_FAILURE;
                    }
                    if (L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry) ==
                        L2VPN_PWVC_ENET_DEF_PORT_VLAN)
                    {
                        u4IfIndex =
                            (UINT4)
                            L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry);

                        if (u2VlanIdInPkt == 0)
                        {
                            u2VlanId =
                                L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry);
                        }
                        else
                        {
                            u2VlanId = u2VlanIdInPkt;
                        }
                    }
                    /* Vlan Id matching verification is done only if valid 
                     * Vlan Id is present */
                    else
                    {
                        if ((u2VlanIdInPkt != L2VPN_ZERO) &&
                            (u2VlanIdInPkt != L2VPN_PWVC_ENET_PORT_VLAN
                             (pPwVcEnetEntry)) &&
                            (L2VPN_PWVC_ENET_VLAN_MODE (pPwVcEnetEntry) ==
                             L2VPN_VLANMODE_NOCHANGE))
                        {
                            /* Vlan Id mismatch at egress node */

                            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                            MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                                (UINT1 *) pu1Buf);
                            return MPLS_FAILURE;

                        }
                        else
                        {
                            /* Vlan Id and Port IfIndex are retrieved and 
                             * sent to Vlan Module for L2 forwarding */
                            u2VlanId =
                                L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry);
                            u4IfIndex =
                                (UINT4)
                                L2VPN_PWVC_ENET_PORT_IF_INDEX (pPwVcEnetEntry);
                        }
                    }

                }
                if (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
                {

                    /* Native Service Processing (NSP) PSN->AC direction */
                    if (L2VPN_PWVC_ENET_VLAN_MODE (pPwVcEnetEntry) ==
                        L2VPN_VLANMODE_ADDVLAN)
                    {
                        u2VlanId = L2VPN_PWVC_ENET_PW_VLAN (pPwVcEnetEntry);
                        u2Protocol = OSIX_HTONS (VLAN_PROVIDER_PROTOCOL_ID);
                        /* copying source and dest mac address */
                        if (CRU_BUF_Copy_FromBufChain (pBuf, au1BufTmp, 0,
                                                       VLAN_TAG_OFFSET) ==
                            CRU_FAILURE)
                        {
                            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                            MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                                (UINT1 *) pu1Buf);
                            return MPLS_FAILURE;
                        }
                        u2Tag = 0;
                        u2Tag = (UINT2) (u2Tag << VLAN_TAG_PRIORITY_SHIFT);
                        u2Tag = (UINT2) (u2Tag | (u2VlanId & VLAN_ID_MASK));
                        u2Tag = (UINT2) (OSIX_HTONS (u2Tag));
                        MEMCPY (&au1BufTmp[VLAN_TAG_OFFSET],
                                (UINT1 *) &u2Protocol, VLAN_TYPE_OR_LEN_SIZE);
                        MEMCPY (&au1BufTmp[VLAN_TAG_VLANID_OFFSET],
                                (UINT1 *) &u2Tag, VLAN_TAG_SIZE);
                        CRU_BUF_Move_ValidOffset (pBuf, VLAN_TAG_OFFSET);
                        CRU_BUF_Prepend_BufChain (pBuf, au1BufTmp,
                                                  VLAN_TAGGED_HEADER_SIZE);
                    }
                }
                else if (L2VPN_PWVC_TYPE (pPwVcEntry) ==
                         L2VPN_PWVC_TYPE_ETH_VLAN)
                {
                    /* Do PW encapsulation based on the pwType and 
                     * pwEnetVlanMode                            */
                    if (L2VPN_PWVC_ENET_VLAN_MODE (pPwVcEnetEntry) ==
                        L2VPN_VLANMODE_CHANGEVLAN)
                    {
                        /* Rewrite the first vlan header in the pkt with
                         * pwEnetPortVlan */
                        u4Temp = CFA_VLAN_TAG_OFFSET + CFA_VLAN_PROTOCOL_SIZE;
                        MEMCPY (&u2Temp, pPkt + u4Temp, 2);
                        u2Temp = OSIX_NTOHS (u2Temp);

                        u2Temp = u2Temp & 0xf000;    /* nullify the vlan */
                        u2Temp =
                            u2Temp | L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry);
                        u2Temp = OSIX_HTONS (u2Temp);
                        CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) &u2Temp,
                                                   u4Temp, 2);
                    }
                    else
                    {
                        /* Add Vlan / No Change */
                    }
                }
                /* If this packet coming on PSW which has visibility in CFA If Manager
                   give the packet to CFA */
                CfaGetIfaceType (pPwVcEntry->u4PwIfIndex, &u1IfType);
#ifdef CFA_WANTED
                if (u1IfType != CFA_PSEUDO_WIRE)
                {
                    CRU_BUF_Set_U2Reserved (pBuf, u2PriInPacket);    /*Pri */
                    CRU_BUF_Set_U4Reserved1 (pBuf, u4Vsi);    /* Context */
                    CRU_BUF_Set_U4Reserved2 (pBuf, u2VlanId);
                    CRU_BUF_Set_U4Reserved3 (pBuf, u1VlanMode);
#ifdef HVPLS_WANTED
                    CRU_BUF_Prepend_BufChain
                        (pBuf, (UINT1 *) &pPwVcEntry->u4PwVcIndex,
                         sizeof (UINT4));
#endif
                    if (L2VPN_PWVC_MODE (pPwVcEntry) == L2VPN_VPLS)
                    {

                        if (L2VPN_PWVC_TYPE (pPwVcEntry) == L2VPN_PWVC_TYPE_ETH)
                        {
                            TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST
                                          (pPwVcEnetServSpecEntry),
                                          pPwVcEnetEntry, tPwVcEnetEntry *)
                            {
                                u4IfIndex =
                                    (UINT4)
                                    L2VPN_PWVC_ENET_PORT_IF_INDEX
                                    (pPwVcEnetEntry);

                                pEthBuf = CRU_BUF_Duplicate_BufChain (pBuf);
                                CRU_BUF_Set_U2Reserved (pEthBuf, (UINT2) MplsHdr.Exp);    /*Pri */
                                CRU_BUF_Set_U4Reserved1 (pEthBuf, u4Vsi);    /* Context */
                                CRU_BUF_Set_U4Reserved2 (pEthBuf, u2VlanId);
                                CRU_BUF_Set_U4Reserved3 (pEthBuf, u1VlanMode);

                                if (MPLS_IS_MCASTADDR (DstMacAddr) != MPLS_TRUE)
                                {
                                    if (VLAN_SUCCESS ==
                                        VlanGetFdbEntryDetails (u2VlanId,
                                                                DstMacAddr,
                                                                &u2Port,
                                                                &u1Status))
                                    {
                                        if ((UINT4) u2Port != u4IfIndex)
                                        {
                                            continue;
                                        }
                                        else
                                        {
                                            u2PortFound = L2VPN_ONE;
                                        }
                                    }
                                    MPLSFM_DBG (MPLSFM_PRCS_PRCS,
                                                "FdB entry not found, so the Packet will broadcast to all the attached interface");
                                }
                                u2MatchedAc = L2VPN_ONE;
                                if (CfaHandlePktFromMpls
                                    (pEthBuf, u4IfIndex /* UNUSED  for L2 */ ,
                                     MPLS_TO_L2, NULL,
                                     CFA_LINK_UCAST) == CFA_FAILURE)
                                {
                                    CRU_BUF_Release_MsgBufChain (pEthBuf,
                                                                 FALSE);
                                    MemReleaseMemBlock
                                        (MPLS_CFA_ENET_MTU_POOL_ID,
                                         (UINT1 *) pu1Buf);
                                    return MPLS_FAILURE;
                                }
                                if ((MPLS_IS_MCASTADDR (DstMacAddr) !=
                                     MPLS_TRUE) && (u2PortFound == L2VPN_ONE))
                                {
                                    MPLSFM_DBG (MPLSFM_PRCS_PRCS,
                                                "For unicast packet if the port entry exists in the mac table, then break from the loop so that packet could not be sent to other Ac ports");
                                    break;
                                }
                            }
                        }
                        else
                        {
                            TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST
                                          (pPwVcEnetServSpecEntry),
                                          pPwVcEnetEntry, tPwVcEnetEntry *)
                            {
                                if (u2VlanIdInPkt ==
                                    L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry))
                                {
                                    /* Found the matching AC entry */
                                    u2VlanId = u2VlanIdInPkt;
                                    u4IfIndex =
                                        (UINT4)
                                        L2VPN_PWVC_ENET_PORT_IF_INDEX
                                        (pPwVcEnetEntry);
                                    pEthBuf = CRU_BUF_Duplicate_BufChain (pBuf);
                                    CRU_BUF_Set_U2Reserved (pEthBuf, (UINT2) MplsHdr.Exp);    /*Pri */
                                    CRU_BUF_Set_U4Reserved1 (pEthBuf, u4Vsi);    /* Context */
                                    CRU_BUF_Set_U4Reserved2 (pEthBuf, u2VlanId);
                                    CRU_BUF_Set_U4Reserved3 (pEthBuf,
                                                             u1VlanMode);
                                    if (MPLS_IS_MCASTADDR (DstMacAddr) !=
                                        MPLS_TRUE)
                                    {
                                        if (VLAN_SUCCESS ==
                                            VlanGetFdbEntryDetails (u2VlanId,
                                                                    DstMacAddr,
                                                                    &u2Port,
                                                                    &u1Status))
                                        {
                                            if ((UINT4) u2Port != u4IfIndex)
                                            {
                                                continue;
                                            }
                                            else
                                            {
                                                u2PortFound = L2VPN_ONE;
                                            }
                                        }
                                        MPLSFM_DBG (MPLSFM_PRCS_PRCS,
                                                    "FdB entry not found, so the Packet will broadcast to all the attached interface");
                                    }
                                    u2MatchedAc = L2VPN_ONE;
                                    if (CfaHandlePktFromMpls
                                        (pEthBuf,
                                         u4IfIndex /* UNUSED  for L2 */ ,
                                         MPLS_TO_L2, NULL,
                                         CFA_LINK_UCAST) == CFA_FAILURE)
                                    {
                                        CRU_BUF_Release_MsgBufChain (pEthBuf,
                                                                     FALSE);
                                        MemReleaseMemBlock
                                            (MPLS_CFA_ENET_MTU_POOL_ID,
                                             (UINT1 *) pu1Buf);
                                        return MPLS_FAILURE;

                                    }
                                    if ((MPLS_IS_MCASTADDR (DstMacAddr) !=
                                         MPLS_TRUE)
                                        && (u2PortFound == L2VPN_ONE))
                                    {
                                        MPLSFM_DBG (MPLSFM_PRCS_PRCS,
                                                    "For unicast packet if the port entry exists in the mac table, then break from the loop so that packet could not be sent to other Ac ports");
                                        break;
                                    }

                                }
                            }

                        }
                        if (u2MatchedAc == L2VPN_ZERO)
                        {
                            TMO_SLL_Scan (L2VPN_PWVC_ENET_ENTRY_LIST
                                          (pPwVcEnetServSpecEntry),
                                          pPwVcEnetEntry, tPwVcEnetEntry *)
                            {
                                if (u2VlanIdInPkt ==
                                    L2VPN_PWVC_ENET_PORT_VLAN (pPwVcEnetEntry))
                                {
                                    /* Found the matching AC entry */
                                    u2VlanId = u2VlanIdInPkt;
                                    u4IfIndex =
                                        (UINT4)
                                        L2VPN_PWVC_ENET_PORT_IF_INDEX
                                        (pPwVcEnetEntry);
                                    pEthBuf = CRU_BUF_Duplicate_BufChain (pBuf);
                                    CRU_BUF_Set_U2Reserved (pEthBuf, (UINT2) MplsHdr.Exp);    /*Pri */
                                    CRU_BUF_Set_U4Reserved1 (pEthBuf, u4Vsi);    /* Context */
                                    CRU_BUF_Set_U4Reserved2 (pEthBuf, u2VlanId);
                                    CRU_BUF_Set_U4Reserved3 (pEthBuf,
                                                             u1VlanMode);

                                    if (CfaHandlePktFromMpls
                                        (pEthBuf,
                                         u4IfIndex /* UNUSED  for L2 */ ,
                                         MPLS_TO_L2, NULL,
                                         CFA_LINK_UCAST) == CFA_FAILURE)
                                    {
                                        CRU_BUF_Release_MsgBufChain (pEthBuf,
                                                                     FALSE);
                                        MemReleaseMemBlock
                                            (MPLS_CFA_ENET_MTU_POOL_ID,
                                             (UINT1 *) pu1Buf);
                                        return MPLS_FAILURE;

                                    }
                                }
                            }

                        }
                        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                    }
                    else
                    {
                        if (CfaHandlePktFromMpls
                            (pBuf, u4IfIndex /* UNUSED  for L2 */ ,
                             MPLS_TO_L2, NULL, CFA_LINK_UCAST) == CFA_FAILURE)
                        {
                            printf
                                ("\n%s %dCfaHandlePktFromMpls returns CFA_FAILURE \n",
                                 __func__, __LINE__);
                            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                            MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                                (UINT1 *) pu1Buf);
                            return MPLS_FAILURE;
                        }
                    }
                }
                else
                {
                    if (CfaHandlePktFromMpls
                        (pBuf, pPwVcEntry->u4PwIfIndex, L2PKT_MPLS_TO_CFA, NULL,
                         CFA_LINK_UCAST) == CFA_FAILURE)
                    {
                        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                        MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                            (UINT1 *) pu1Buf);
                        return MPLS_FAILURE;
                    }
                }

#else
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
#endif
                pPwVcPerfInfo =
                    L2VPN_PWVC_INFO_TABLE_PTR (L2VPN_PWVC_INDEX (pPwVcEntry));
                FSAP_U8_INC (&pPwVcPerfInfo->aPwVcPerfIntervalInfo
                             [L2VPN_PWVC_PERF_CURRENT_INTERVAL - 1].InHCPkts);
                MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                    (UINT1 *) pu1Buf);
                return MPLS_SUCCESS;

            case MPLS_HW_ACTION_POP_L3_SWITCH:

                if (bIsDblLblLookup == TRUE)
                {
                    u4Temp += MPLS_HDR_LEN;
                    pPkt += MPLS_HDR_LEN;
                }

                u1Version = *((UINT1 *) pPkt);
                u1Version = (UINT1) ((u1Version & IP_HEADER_VER_MASK) >>
                                     IP_HDR_VER_OFFSET_BITS);

                if (u1Version == IP_VERSION_4)
                {
                    /* If version value is 4. This is IPV4 packet */
                    MplsOamRxHandleIpv4CtrlPacket (pPkt, &u1IpProto,
                                                   &u2UdpDstPort,
                                                   &u4DstIpAddress);
                }
                else if (u1Version == IPV6_HEADER_VERSION)
                {
                    /* If version value is 6. This is IPV6 packet */
                    MplsOamRxHandleIpv6CtrlPacket (pPkt, &u1IpProto,
                                                   &u2UdpDstPort);
                }

                if (((u1IpProto == MPLS_UDP_PROT_ID) &&
                     (((u2UdpDstPort == MPLS_BFD_UDP_DST_PORT)
                       && (u1Version == IP_VERSION_4)
                       && ((u4DstIpAddress & MPLS_4TH_BYTE_MASK) ==
                           IP_LOOPBACK_ADDRESS))
                      || (u2UdpDstPort == MPLS_LSP_PING_UDP_DST_PORT)))
                    || (bIsRalPresent == TRUE))
                {
                    if (MplsOamRxHandleCtrlPkt (pBuf, pu1Buf,
                                                u4IfIndex,
                                                u4PktSize) == MPLS_SUCCESS)
                    {
                        /* Packet has been successfully delivered
                         * to OAM applications
                         */
                        MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                            (UINT1 *) pu1Buf);
                        return MPLS_SUCCESS;
                    }
                    else
                    {
                        /* Since the packet received is a OAM control packet
                         * and the packet handling has failed, it should not be
                         * forwarded. Drop it. */
                        MPLS_REL_BUF_CHAIN (pBuf, MPLS_BUF_NORMAL_REL);
                        MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                            (UINT1 *) pu1Buf);
                        return MPLS_FAILURE;
                    }
                }

                if (MplsGetL3Intf
                    (((tInSegment *) pEntry)->u4IfIndex,
                     &u4L3Intf) == MPLS_FAILURE)
                {
                    MPLSFM_DBG (MPLSFM_PRCS_PRCS, "-E- Invalid L3 If");
                    MPLS_REL_BUF_CHAIN (pBuf, MPLS_BUF_NORMAL_REL);
                    MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                        (UINT1 *) pu1Buf);
                    return MPLS_FAILURE;
                }

                CRU_BUF_Move_ValidOffset (pBuf, u4Temp);
                MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                    (UINT1 *) pu1Buf);
/* MPLS_IPv6 add start */
#ifdef MPLS_IPV6_WANTED
                if (u1Version == IPV6_HEADER_VERSION)
                {
                    return (MplsSendL3LabelPktToIpv6 (pBuf, u4L3Intf));
                }
                else
                {
#endif
/* MPLS_IPv6 add end */
                    return (MplsSendL3LabelPktToIp
                            (pBuf, &EthHdr, u4L3Intf, u4IfIndex));
/* MPLS_IPv6 add start */
#ifdef MPLS_IPV6_WANTED
                }
#endif
/* MPLS_IPv6 add end */
#ifdef MPLS_L3VPN_WANTED
            case MPLS_HW_ACTION_POP_L3VPN_LABEL_SWITCH:
                /* Remove outer Ethernet, Vlan and Mpls headers */
                CRU_BUF_Move_ValidOffset (pBuf, u4Temp);
                /* Remove current MPLS header */
                CRU_BUF_Move_ValidOffset (pBuf, MPLS_HDR_LEN);

                switch (((tL3VpnBgpRouteLabelEntry *) pEntry)->u1LabelPolicy)
                {
                    case L3VPN_BGP4_POLICY_PER_ROUTE:

                        u4IfIndex =
                            ((tL3VpnBgpRouteLabelEntry *) pEntry)->
                            L3VpnBgpRouteTerminationEntry.L3VpnBgpRoutePerRoute.
                            u4IfIndex;
                        u4VrfId =
                            ((tL3VpnBgpRouteLabelEntry *) pEntry)->
                            L3VpnBgpRouteTerminationEntry.L3VpnBgpRoutePerVrf.
                            u4VrfId;
                        CfaExtractIpHdr (&IpHdr, pBuf);

                        if (NetIpv4IfIsOurAddressInCxt (u4VrfId, IpHdr.u4Dest)
                            == NETIPV4_SUCCESS)
                        {

                            if (CfaHandlePktFromMpls
                                (pBuf, u4IfIndex, MPLS_TO_L3, NULL,
                                 CFA_LINK_UCAST) == CFA_FAILURE)
                            {
                                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                                MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                                    (UINT1 *) pu1Buf);

                                return MPLS_FAILURE;
                            }

                            MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                                (UINT1 *) pu1Buf);
                            break;
                        }

                        MEMCPY (&u4NextHop,
                                ((tL3VpnBgpRouteLabelEntry *) pEntry)->
                                L3VpnBgpRouteTerminationEntry.
                                L3VpnBgpRoutePerRoute.NextHop.au1Address,
                                ((tL3VpnBgpRouteLabelEntry *) pEntry)->
                                L3VpnBgpRouteTerminationEntry.
                                L3VpnBgpRoutePerRoute.NextHop.u2AddressLen);

                        if (u4NextHop == 0)
                        {
                            u4NextHop = IpHdr.u4Dest;

                            if (CfaIpIfGetIfIndexForNetInCxt (u4VrfId,
                                                              u4NextHop,
                                                              &u4ArpIfIndex) ==
                                CFA_FAILURE)
                            {
                                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                                MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                                    (UINT1 *) pu1Buf);
                                return MPLS_FAILURE;
                            }
                            if (ArpResolveWithIndex (u4ArpIfIndex, u4NextHop,
                                                     i1Hw_addr,
                                                     &u1EncapType) ==
                                ARP_FAILURE)
                            {
                                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                                MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                                    (UINT1 *) pu1Buf);
                                /* Arp Resolution Failed,
                                 * trigger ARP so that it is resolved for next packet*/
                                MplsTriggerArpRequest (u4NextHop,
                                                       (UINT2) NetIpRtInfo.
                                                       u4RtIfIndx);

                                return MPLS_FAILURE;
                            }
                            /* Call CFA Api to send the packet out */
                            if (CfaHandlePktFromMpls
                                (pBuf, u4ArpIfIndex, MPLSL3VPN_TO_CFA,
                                 (UINT1 *) i1Hw_addr,
                                 CFA_LINK_UCAST) == CFA_FAILURE)
                            {
                                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                                MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                                    (UINT1 *) pu1Buf);
                                return MPLS_FAILURE;
                            }
                            MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                                (UINT1 *) pu1Buf);
                            break;
                        }
                        if (CfaHandlePktFromMpls
                            (pBuf, u4IfIndex,
                             MPLSL3VPN_TO_CFA,
                             ((tL3VpnBgpRouteLabelEntry *) pEntry)->
                             L3VpnBgpRouteTerminationEntry.
                             L3VpnBgpRoutePerRoute.au1DstMac,
                             CFA_LINK_UCAST) == CFA_FAILURE)
                        {
                            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                            MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                                (UINT1 *) pu1Buf);
                            return MPLS_FAILURE;
                        }
                        MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                            (UINT1 *) pu1Buf);
                        break;

                    case L3VPN_BGP4_POLICY_PER_VRF:

                        u4VrfId =
                            ((tL3VpnBgpRouteLabelEntry *) pEntry)->
                            L3VpnBgpRouteTerminationEntry.L3VpnBgpRoutePerVrf.
                            u4VrfId;
                        CfaExtractIpHdr (&IpHdr, pBuf);

                        RtQuery.u4DestinationIpAddress = IpHdr.u4Dest;
                        RtQuery.u4DestinationSubnetMask = 0xFFFFFFFF;
                        RtQuery.u4ContextId = u4VrfId;
                        RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;

                        if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) ==
                            NETIPV4_FAILURE)
                        {
                            MPLSFWD_L3VPN_DBG
                                ("%s: IP route not found for the destination: %d\n",
                                 __FUNCTION__, IpHdr.u4Dest);
                            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                            MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                                (UINT1 *) pu1Buf);
                            return MPLS_FAILURE;
                        }

                        if (NetIpRtInfo.u4NextHop == 0
                            && NetIpRtInfo.u4RtIfIndx != 0)
                        {
                            NetIpRtInfo.u4NextHop = IpHdr.u4Dest;
                        }

                        /* Resolve ARP for the next hop */
                        if (CfaIpIfGetIfIndexForNetInCxt (u4VrfId,
                                                          NetIpRtInfo.u4NextHop,
                                                          &u4ArpIfIndex) ==
                            CFA_FAILURE)
                        {
                            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                            MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                                (UINT1 *) pu1Buf);
                            return MPLS_FAILURE;
                        }
                        if (NetIpv4IfIsOurAddressInCxt (u4VrfId, IpHdr.u4Dest)
                            == NETIPV4_SUCCESS)
                        {

                            if (CfaHandlePktFromMpls
                                (pBuf, u4ArpIfIndex, MPLS_TO_L3, NULL,
                                 CFA_LINK_UCAST) == CFA_FAILURE)
                            {
                                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                                MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                                    (UINT1 *) pu1Buf);

                                return MPLS_FAILURE;
                            }

                            MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                                (UINT1 *) pu1Buf);
                            break;
                        }

                        if (ArpResolveWithIndex
                            (u4ArpIfIndex, NetIpRtInfo.u4NextHop, i1Hw_addr,
                             &u1EncapType) == ARP_FAILURE)
                        {
                            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                            MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                                (UINT1 *) pu1Buf);
                            /* Arp Resolution Failed,
                             * trigger ARP so that it is resolved for next packet*/
                            MplsTriggerArpRequest (NetIpRtInfo.u4NextHop,
                                                   (UINT2) NetIpRtInfo.
                                                   u4RtIfIndx);
                            return MPLS_FAILURE;
                        }

                        /* Call CFA Api to send the packet out */
                        if (CfaHandlePktFromMpls
                            (pBuf, u4ArpIfIndex, MPLSL3VPN_TO_CFA,
                             (UINT1 *) i1Hw_addr,
                             CFA_LINK_UCAST) == CFA_FAILURE)
                        {
                            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                            MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                                (UINT1 *) pu1Buf);
                            return MPLS_FAILURE;
                        }
                        MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                            (UINT1 *) pu1Buf);
                        break;

                    default:
                        MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                            (UINT1 *) pu1Buf);
                        break;
                };
                return MPLS_SUCCESS;
#endif
            default:
                if (MplsOamRxHandleCtrlPkt (pBuf, pu1Buf, u4IfIndex, u4PktSize)
                    == MPLS_SUCCESS)
                {
                    /* Packet has been successfully delivered to OAM
                     * applications
                     */
                    MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                        (UINT1 *) pu1Buf);
                    return MPLS_SUCCESS;
                }
                MPLSFM_DBG (MPLSFM_PRCS_PRCS, "-E- Invalid Label action");
                MPLS_REL_BUF_CHAIN (pBuf, MPLS_BUF_NORMAL_REL);
                MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID,
                                    (UINT1 *) pu1Buf);
                return MPLS_FAILURE;
        }
    }
    while (MplsHdr.SI == 0);

    MPLS_REL_BUF_CHAIN (pBuf, MPLS_BUF_NORMAL_REL);
    MemReleaseMemBlock (MPLS_CFA_ENET_MTU_POOL_ID, (UINT1 *) pu1Buf);
    return MPLS_FAILURE;
}

/* ************************************************************************* *
 *  Function Name   : MplsProcessIpPkt                                       * 
 *  Description     : Called from CFA task to post packet to MPLS if         *
 *                    FTN is configured for the destionation IP              *
 *  Input           : pBuf - Packet Buffer                                   *
 *                    u4IfIndex - Incoming interface                         *
 *                    u4DestIp - Destination IP                              *
 *                    bCfaLockReqd - Flag to indicate if CfaLock is required *
 *  Output          : NONE                                                   *
 *  Returns         : MPLS_FAILURE / MPLS_SUCCESS                            *
 * ************************************************************************* */
INT4
MplsProcessIpPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex, UINT4 u4DestIp,
                  BOOL1 bCfaLockReqd)
{
    tFtnEntry          *pFtnEntry = NULL;
    tRtInfoQueryMsg     RtQuery;
    tNetIpv4RtInfo      NetIpRtInfo;

    UNUSED_PARAM (u4IfIndex);
    if (gu1MplsInitialised != TRUE)
    {
        /* MPLS is not enabled */
        return MPLS_FAILURE;
    }
    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (RtQuery));
    RtQuery.u4DestinationIpAddress = u4DestIp;
    RtQuery.u4DestinationSubnetMask = 0xffffffff;
    RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
    if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_SUCCESS)
    {
        if (NetIpRtInfo.u2RtProto == CIDR_LOCAL_ID)
        {
            /* If local net then do not do labeling */
            return MPLS_FAILURE;
        }
    }

    if (bCfaLockReqd == TRUE)
    {
        CFA_UNLOCK ();
    }
    MPLS_CMN_LOCK ();
    pFtnEntry = MplsFwdGetFtnTableEntry (u4DestIp);

    if (((pFtnEntry == NULL) || (pFtnEntry->u1RowStatus != ACTIVE)) ||
        (pFtnEntry->bIsHostAddrFecForPSN))
    {
        /* No FTN configured so, continue normal IP processing */
        MPLS_CMN_UNLOCK ();
        if (bCfaLockReqd == TRUE)
        {
            CFA_LOCK ();
        }
        return MPLS_FAILURE;
    }
    CRU_BUF_Set_U4Reserved1 (pBuf, pFtnEntry->u4FtnIndex);

    MPLS_CMN_UNLOCK ();
    if (bCfaLockReqd == TRUE)
    {
        CFA_LOCK ();
    }

    CRU_BUF_Set_SourceModuleId (pBuf, CFA_MODULE);
    CRU_BUF_Set_U2Reserved (pBuf, L3_TO_MPLS);

    /* Send the buffer to MPLS Queue  */

    if (OsixQueSend (gFmInQId, (UINT1 *) (&pBuf),
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)

    {
        /* Packet should go via MPLS but Unable to send to MPLS-FM Queue
         * so release the buffer and say pkt has taken care to the caller */
        MPLS_REL_BUF_CHAIN (pBuf, MPLS_BUF_NORMAL_REL);
    }
    else
    {
        /* Tell MPLS-FM that somebody is waiting for your attention */
        if (OsixEvtSend (gFmTaskId, MPLS_IN_MSG_Q_EVENT) == OSIX_FAILURE)
        {
            return MPLS_FAILURE;
        }
    }

    return MPLS_SUCCESS;
}

#ifdef MPLS_L3VPN_WANTED
INT4
MplsIsIpPktForL3Vpn (UINT4 u4IfIndex, UINT4 *pu4L3VpnContext)
{

    tL3vpnMplsL3VpnIfConfEntry *pL3VpnIfConfEntry = NULL;
    UINT4               u4ContextId = 0;

    pL3VpnIfConfEntry = L3VpnGetIfConfEntryWithIfIndex (u4IfIndex);
    if (pL3VpnIfConfEntry == NULL)
    {
        return MPLS_FALSE;
    }

    /* Find the Context Id */
    if (VcmIsVrfExist (L3VPN_P_VRF_NAME (pL3VpnIfConfEntry), &u4ContextId) ==
        VCM_FALSE)
    {
        return MPLS_FALSE;
    }
    *pu4L3VpnContext = u4ContextId;
    return MPLS_TRUE;
}

INT4
MplsProcessL3VpnPacket (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                        UINT4 u4DestIp, UINT4 u4ContextId)
{

    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;
    UINT4               u4Prefix = 0;
    UINT1              *pu1RteXcPointer = NULL;
    UINT4               u4XcIndex = 0;
    UINT4               u4InSegmentIndex = 0;
    UINT4               u4OutSegmentIndex = 0;
    tXcEntry           *pXcEntry = NULL;

    UNUSED_PARAM (u4IfIndex);
    /* Trie-look up */
    u4Prefix = OSIX_HTONL (u4DestIp);
    pL3vpnMplsL3VpnVrfRteEntry =
        L3VpnGetBestMatchRteTableEntryFromTrie (u4ContextId, u4Prefix);

    if (pL3vpnMplsL3VpnVrfRteEntry == NULL)
    {
        MPLSFWD_L3VPN_DBG ("RTE Entry not found for the Prefix: %d\n",
                           u4Prefix);
        return MPLS_FAILURE;
    }
    /*Find the XC Index and OutSegment Index from the RTE Entry */

    pu1RteXcPointer = L3VPN_P_RTE_XC_POINTER (pL3vpnMplsL3VpnVrfRteEntry);
    if (pu1RteXcPointer == NULL)
    {
        MPLSFWD_L3VPN_DBG ("XC Pointer array is NULL\n");
        return MPLS_FAILURE;
    }
    MEMCPY (&u4XcIndex, pu1RteXcPointer, sizeof (UINT4));
    pu1RteXcPointer = pu1RteXcPointer + sizeof (UINT4);
    u4XcIndex = OSIX_NTOHL (u4XcIndex);

    MEMCPY (&u4InSegmentIndex, pu1RteXcPointer, sizeof (UINT4));
    pu1RteXcPointer = pu1RteXcPointer + sizeof (UINT4);
    u4InSegmentIndex = OSIX_NTOHL (u4InSegmentIndex);

    MEMCPY (&u4OutSegmentIndex, pu1RteXcPointer, sizeof (UINT4));
    u4OutSegmentIndex = OSIX_NTOHL (u4OutSegmentIndex);

    MPLS_CMN_LOCK ();
    pXcEntry =
        MplsGetXCTableEntry (u4XcIndex, u4InSegmentIndex, u4OutSegmentIndex);
    if (pXcEntry == NULL || pXcEntry->pOutIndex == NULL)
    {
        MPLSFWD_L3VPN_DBG ("XC Entry is NULL or the OutSeg is NULL\n");
        MPLS_CMN_UNLOCK ();
        return MPLS_FAILURE;
    }
    MPLS_CMN_UNLOCK ();

    CRU_BUF_Set_U4Reserved1 (pBuf, u4XcIndex);
    CRU_BUF_Set_U4Reserved2 (pBuf, u4OutSegmentIndex);

    CRU_BUF_Set_SourceModuleId (pBuf, CFA_MODULE);
    CRU_BUF_Set_U2Reserved (pBuf, L3_TO_MPLS_L3VPN);

    if (OsixQueSend (gFmInQId, (UINT1 *) (&pBuf),
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)

    {
        /* Packet should go via MPLS but Unable to send to MPLS-FM Queue
         * so release the buffer and say pkt has taken care to the caller */
        MPLS_REL_BUF_CHAIN (pBuf, MPLS_BUF_NORMAL_REL);
    }
    else
    {
        /* Tell MPLS-FM that somebody is waiting for your attention */
        if (OsixEvtSend (gFmTaskId, MPLS_IN_MSG_Q_EVENT) == OSIX_FAILURE)
        {
            return MPLS_FAILURE;
        }
    }

    return MPLS_SUCCESS;

    /* Post Event to FM */

}

INT4
MplsLabelAndSendIpPktForL3Vpn (tCRU_BUF_CHAIN_HEADER * pBuf,
                               UINT4 XcIndex, UINT4 u4OutSegmentIndex)
{

    tXcEntry           *pXcEntry = NULL;
    UINT4               u4SI = 1;
    UINT4               u4Exp = 0;

    pXcEntry = MplsGetXCTableEntry (XcIndex, MPLS_ZERO    /*InSegment will always be Zero */
                                    , u4OutSegmentIndex);

    if (pXcEntry == NULL || (pXcEntry->u1RowStatus != ACTIVE))
    {
        MPLSFWD_L3VPN_DBG ("XC Entry is NULL or XC Row Status is NOT Active\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return MPLS_FAILURE;
    }

    if (MplsLabelAndSendout (pBuf, pXcEntry, u4SI, u4Exp, FALSE) ==
        MPLS_FAILURE)
    {
        MPLSFWD_L3VPN_DBG ("Error in MplsLabelAndSendout\n");
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return MPLS_FAILURE;
    }
    return MPLS_SUCCESS;

}

#endif

/************************************************************************
 *  Function Name   : MplsLabelAndSendIpPkt 
 *  Description     : Function to Put Label on IP pakcet and send through
 *                    TE (or) NonTE LSP. Called from MPLS-FM task while 
 *                    sending IP to a LSP                      
 *  Input           : pBuf - Incoming IP packet 
 *                    u4FtnIndex - FTN to be used to put the MPLS label
 *  Output          : NONE 
 *  Returns         : MPLS_FAILURE / MPLS_SUCCESS
 ************************************************************************/
INT4
MplsLabelAndSendIpPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4FtnIndex)
{
    tFtnEntry          *pFtnEntry = NULL;
    tXcEntry           *pXcEntry = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4SI = 1;
    UINT4               u4Exp = 0;
    eDirection          Direction = MPLS_DIRECTION_ANY;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;

    pFtnEntry = MplsGetFtnTableEntry (u4FtnIndex);
    if ((pFtnEntry == NULL) || (pFtnEntry->u1RowStatus != ACTIVE) ||
        (pFtnEntry->pActionPtr == NULL))
    {
        /* No FTN configured so, continue normal 
           IP processing */
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return MPLS_FAILURE;
    }

    if (pFtnEntry->i4ActionType == MPLS_FTN_ON_NON_TE_LSP)
    {
        pXcEntry = pFtnEntry->pActionPtr;
    }
    else
    {
        pTeTnlInfo = (tTeTnlInfo *) pFtnEntry->pActionPtr;

        if (pTeTnlInfo->u4TnlInstance == MPLS_ZERO)
        {
            CONVERT_TO_INTEGER (pTeTnlInfo->TnlIngressLsrId, u4IngressId);
            CONVERT_TO_INTEGER (pTeTnlInfo->TnlEgressLsrId, u4EgressId);

            u4IngressId = OSIX_NTOHL (u4IngressId);
            u4EgressId = OSIX_NTOHL (u4EgressId);

            pTeTnlInfo = TeGetTunnelInfo (pTeTnlInfo->u4TnlIndex,
                                          pTeTnlInfo->u4TnlPrimaryInstance,
                                          u4IngressId, u4EgressId);
        }

        if (pTeTnlInfo == NULL)
        {
            CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
            return MPLS_FAILURE;
        }

        /* Take the backup tunnel if the primary tunnel is down 
         * and protection in use */
        if (TE_TNL_LOCAL_PROTECTION_IN_USE (pTeTnlInfo) == LOCAL_PROT_IN_USE)
        {
            CONVERT_TO_INTEGER (pTeTnlInfo->BkpTnlIngressLsrId, u4IngressId);
            CONVERT_TO_INTEGER (pTeTnlInfo->BkpTnlEgressLsrId, u4EgressId);

            u4IngressId = OSIX_NTOHL (u4IngressId);
            u4EgressId = OSIX_NTOHL (u4EgressId);

            pTeTnlInfo = TeGetTunnelInfo
                (pTeTnlInfo->u4BkpTnlIndex, pTeTnlInfo->u4BkpTnlInstance,
                 u4IngressId, u4EgressId);
            if (pTeTnlInfo == NULL)
            {
                /* Backup tunnel is not ready */
                CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
                return MPLS_FAILURE;
            }
        }

        TeCmnExtGetDirectionFromTnl (pTeTnlInfo, &Direction);

        pXcEntry = MplsGetXCEntryByDirection (pTeTnlInfo->u4TnlXcIndex,
                                              Direction);

    }

    if ((pXcEntry == NULL) || (pXcEntry->u1RowStatus != ACTIVE))
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return MPLS_FAILURE;
    }
    if (MplsLabelAndSendout (pBuf, pXcEntry, u4SI, u4Exp, FALSE) ==
        MPLS_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return MPLS_FAILURE;
    }
    return MPLS_SUCCESS;
}

/* ************************************************************************* *
 *  Function Name   : MplsProcessL2Pkt                                       * 
 *  Description     : Called from CFA-VLAN task to post packet to MPLS if    *
 *                    VPLS info is configured for the particualr vlan.       *
 *  Input           : pBuf - Packet Buffer                                   *
 *                    u4IfIndex - Incoming interface                         *
 *                    u4VfId - PW index if the u1PktType is UCAST            *
 *                             VPLS instance index if the u1PktType is BCAST *
 *                    u1PktType - CFA_LINK_UCAST / CFA_LINK_BCAST            *
 *  Output          : NONE                                                   *
 *  Returns         : MPLS_FAILURE / MPLS_SUCCESS                            *
 * ************************************************************************* */
INT4
MplsProcessL2Pkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                  UINT4 u4VfId, UINT1 u1PktType)
{
    if (gu1MplsInitialised != TRUE)
    {
        MPLS_REL_BUF_CHAIN (pBuf, MPLS_BUF_NORMAL_REL);
        return MPLS_FAILURE;
    }
    CRU_BUF_Set_SourceModuleId (pBuf, VLAN_MODULE);
    CRU_BUF_Set_U2Reserved (pBuf, L2_TO_MPLS);
    CRU_BUF_Set_U4Reserved1 (pBuf, u4VfId);
    CRU_BUF_Set_U4Reserved2 (pBuf, u1PktType);
    CRU_BUF_Set_U4Reserved3 (pBuf, u4IfIndex);
    /* Send the buffer to MPLS Queue  */
    if (OsixQueSend (gFmInQId, (UINT1 *) (&pBuf),
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        /* Packet should go via MPLS but Unable to send to MPLS-FM Queue
         * so release the buffer and say pkt has taken care to the caller */
        MPLS_REL_BUF_CHAIN (pBuf, MPLS_BUF_NORMAL_REL);
    }
    else
    {
        /* Tell MPLS-FM that somebody is waiting for your attention */
        if (OsixEvtSend (gFmTaskId, MPLS_IN_MSG_Q_EVENT) == OSIX_FAILURE)
        {
            return MPLS_FAILURE;
        }
    }

    return MPLS_SUCCESS;
}

#ifdef HVPLS_WANTED
/* ************************************************************************* *
 *  Function Name   : MplsProcessMplsPkt                                     *
 *  Description     : This function is to post MPLS_TO_MPLS event in case    *
 *                    packet is received from one PW and is being forwarded  *
 *                    to another  PW                                         *
 *  Input           : pBuf - Packet Buffer                                   *
 *                    u4IfIndex - Incoming interface                         *
 *                    u4VfId - PW index if the u1PktType is UCAST            *
 *                             VPLS instance index if the u1PktType is BCAST *
 *                    u4PwVcIndex - Incoming PwVc Index                      *
 *                    u1PktType - CFA_LINK_UCAST / CFA_LINK_BCAST            *
 *  Output          : NONE                                                   *
 *  Returns         : MPLS_FAILURE / MPLS_SUCCESS                            *
 * ************************************************************************* */

INT4
MplsProcessMplsPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                    UINT4 u4VfId, UINT1 u1PktType, UINT4 u4PwVcIndex)
{
    if (gu1MplsInitialised != TRUE)
    {
        MPLS_REL_BUF_CHAIN (pBuf, MPLS_BUF_NORMAL_REL);
        return MPLS_FAILURE;
    }
    CRU_BUF_Set_SourceModuleId (pBuf, VLAN_MODULE);
    CRU_BUF_Set_U2Reserved (pBuf, MPLS_TO_MPLS);
    CRU_BUF_Set_U4Reserved1 (pBuf, u4VfId);
    CRU_BUF_Set_U4Reserved2 (pBuf, u1PktType);
    CRU_BUF_Set_U4Reserved3 (pBuf, u4IfIndex);
    CRU_BUF_Prepend_BufChain (pBuf, (UINT1 *) &u4PwVcIndex, sizeof (UINT4));
    /* Send the buffer to MPLS Queue  */
    if (OsixQueSend (gFmInQId, (UINT1 *) (&pBuf),
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        /* Packet should go via MPLS but Unable to send to MPLS-FM Queue
         * so release the buffer and say pkt has taken care to the caller */
        MPLS_REL_BUF_CHAIN (pBuf, MPLS_BUF_NORMAL_REL);
    }
    else
    {
        /* Tell MPLS-FM that somebody is waiting for your attention */
        if (OsixEvtSend (gFmTaskId, MPLS_IN_MSG_Q_EVENT) == OSIX_FAILURE)
        {
            return MPLS_FAILURE;
        }
    }

    return MPLS_SUCCESS;
}
#endif
/************************************************************************
 *  Function Name   : MplsSendL3LabelPktToIp 
 *  Description     : Function to remove the labeled packet and send
 *                    it to IP for normal IP processing. This will happen
 *                    at egress node.   
 *  Input           : pBuf - Labeled packet pointer
 *                    u4IfIndex - Incoming If Index 
 *                    pakcet
 *  Output          : NONE 
 *  Returns         : MPLS_FAILURE / MPLS_SUCCESS
 ************************************************************************/
PRIVATE INT4
MplsSendL3LabelPktToIp (tCRU_BUF_CHAIN_HEADER * pBuf, tEnetV2Header * pEthHdr,
                        UINT4 u4L3Intf, UINT4 u4IfIndex)
{
    tMplsHdr            MplsHdr;
    UINT4               u4Lbl = MPLS_ZERO;
    t_IP_HEADER        *pIpHdr = NULL;
    UINT4               u4Sum;
    UINT2               u2Checksum;
    UINT1               u1IpTTL;

#ifndef LNXIP4_WANTED
    UNUSED_PARAM (pEthHdr);
    UNUSED_PARAM (u4IfIndex);
#endif
#ifdef LNXIP4_WANTED
    UNUSED_PARAM (u4L3Intf);
#endif

#ifndef LNXIP4_WANTED
    UNUSED_PARAM (pEthHdr);
    UNUSED_PARAM (u4IfIndex);
#endif

    /* Save TTL for egress processing */
    MPLS_SAVE_TTL (pBuf, gMplsIncarn[MPLS_DEF_INCARN].u1iTTL);
    if (MplsRemoveMplsHdr (pBuf, &MplsHdr) == MPLS_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return MPLS_FAILURE;
    }
    u4Lbl = MplsHdr.u4Lbl;
    /* Get the label and check for implicit null. if it is 
     * implicit null, reduce ttl by one irrespective of tunnel mode*/
    if (CRU_BUF_Get_U2Reserved (pBuf) == CFA_TO_MPLS)
    {
        pIpHdr =
            (t_IP_HEADER *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear
            (pBuf, 0, MPLS_IP_HDR_LEN);
        if (pIpHdr != NULL)
        {
            u1IpTTL = pIpHdr->u1Ttl;
            /* Copy back the TTL from the MPLS Header to the IP Header */
            u2Checksum = OSIX_HTONS (pIpHdr->u2Cksum);

            if ((gMplsIncarn[MPLS_DEF_INCARN].DiffServGlobal.u1TnlModel ==
                 MPLS_TNL_UNIFORM_MODEL) || (u4Lbl == MPLS_IMPLICIT_NULL_LABEL))
            {
                gMplsIncarn[MPLS_DEF_INCARN].u1iTTL = (UINT1) MplsHdr.Ttl;
                u4Sum =
                    ((u2Checksum) + (((pIpHdr->u1Ttl) - (MplsHdr.Ttl)) << 8));
                u2Checksum = (UINT2) (u4Sum + (u4Sum >> 16));
                pIpHdr->u2Cksum = OSIX_HTONS (u2Checksum);
                pIpHdr->u1Ttl = (UINT1) MplsHdr.Ttl;
            }
            else
            {
                gMplsIncarn[MPLS_DEF_INCARN].u1iTTL = u1IpTTL;
                u4Sum =
                    (UINT4) ((u2Checksum) +
                             (((pIpHdr->u1Ttl) - (u1IpTTL - 1)) << 8));
                u2Checksum = (UINT2) (u4Sum + (u4Sum >> 16));
                pIpHdr->u2Cksum = OSIX_HTONS (u2Checksum);
                pIpHdr->u1Ttl = (UINT1) (u1IpTTL - MPLS_ONE);
            }
        }
    }
    if (MplsHdr.SI != 1)
    {
        /* Some more label present so, can not give it to
         * IP module */
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return MPLS_FAILURE;
    }
#ifdef LNXIP4_WANTED
    LnxIpPostPktToTap (u4IfIndex, pBuf, pEthHdr, IP_ID);
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
#else
#ifdef CFA_WANTED
    if (CfaHandlePktFromMpls (pBuf, u4L3Intf, MPLS_TO_L3,
                              NULL, CFA_LINK_UCAST) == CFA_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return MPLS_FAILURE;
    }
#else
    UNUSED_PARAM (u4L3Intf);
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
#endif
#endif

    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsLabelSwitching 
 *  Description     : Function to remove top label of the incoming packet
 *                    and add one or more stack labels and send it to CFA.
 *                    This should be called from MPLS FM module for Label
 *                    switching.                     
 *  Input           : pBuf - Labeled packet to be label switched  
 *                    pVoid - InSegment Entry pointer for this labeled
 *                    packet                     
 *                  : bIsRalPresent - Boolean variable to fill the 
 *                                    RAL label in packet
 *  Output          : NONE 
 *  Returns         : MPLS_FAILURE / MPLS_SUCCESS
 ************************************************************************/
PRIVATE INT4
MplsLabelSwitching (tCRU_BUF_CHAIN_HEADER * pBuf, VOID *pVoid,
                    BOOL1 bIsRalPresent)
{
    tXcEntry           *pXcEntry = NULL;
    tMplsHdr            MplsHdr;
    tInSegment         *pInSegment = pVoid;

    /* Remove Top MPLS Header */
    if (MplsRemoveMplsHdr (pBuf, &MplsHdr) == MPLS_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return MPLS_FAILURE;
    }

    pXcEntry = pInSegment->pXcIndex;
    if ((pXcEntry == NULL) || (pXcEntry->u1RowStatus != ACTIVE))
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return MPLS_FAILURE;
    }
    if (MplsLabelAndSendout (pBuf, pXcEntry, MplsHdr.SI, MplsHdr.Exp,
                             bIsRalPresent) == MPLS_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return MPLS_FAILURE;
    }
    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsLabelAndSendout 
 *  Description     : Function to put one or more outgoing labels and
 *                    send it to CFA for forwarding   
 *  Input           : pBuf - Pakcet to be labeled 
 *                    pXcEntry - XC Pointer to get Label stack label and
 *                    outgoing label                    
 *                  : bIsRalPresent - Boolean variable to fill the
 *                                    RAL label in packet
 *  Output          : NONE 
 *  Returns         : MPLS_FAILURE / MPLS_SUCCESS
 ************************************************************************/
PRIVATE INT4
MplsLabelAndSendout (tCRU_BUF_CHAIN_HEADER * pBuf, tXcEntry * pXcEntry,
                     UINT4 u4SI, UINT4 u4Exp, BOOL1 bIsRalPresent)
{
    tOutSegment        *pOutSegment = NULL;
    tLblEntry          *pLblEntry = NULL;
    tMplsHdr            MplsHdr;
    UINT4               u4MplsHdr = 0;
    UINT1               au1DstMac[CFA_ENET_ADDR_LEN], u1EncapType = 0;
    UINT1               au1ZeroMac[MAC_ADDR_LEN];
    t_IP_HEADER        *pIpHdr = NULL;
    UINT4               u4Ttl = MPLS_DEF_TTL;
    tArpQMsg            ArpQMsg;
    UINT4               u4IpPort = 0;
    UINT4               u4L3VlanIf = 0;
#ifdef CFA_WANTED
    UINT4               u4PktSize = 0;
    UINT4               u4IfIndex = 0;
#endif
    tCfaIfInfo          CfaIfInfo;
    UINT4               u4EgressId = 0;
    UINT4               u4IngressId = 0;
    tTeTnlInfo         *pFATeTnlInfo = NULL;
    eDirection          Direction = MPLS_DIRECTION_ANY;
    /* MPLS_IPv6 add start */
#ifdef MPLS_IPV6_WANTED
    tIp6Hdr            *pIpv6Hdr;
    UINT4               u4DstIntfIdx = 0;
    tIp6Addr            V6NhZeroAddr;
    MEMSET (&V6NhZeroAddr, 0, sizeof (tIp6Addr));
#endif
    /* MPLS_IPv6 add end */
    MEMSET (&ArpQMsg, 0, sizeof (tArpQMsg));
    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
    MEMSET (au1ZeroMac, 0, MAC_ADDR_LEN);

    if (pXcEntry == NULL)
    {
        return MPLS_FAILURE;
    }

    pOutSegment = pXcEntry->pOutIndex;
    if ((pOutSegment == NULL) || (pOutSegment->u1RowStatus != ACTIVE))
    {
        /* FTN is not fully configured */
        return MPLS_FAILURE;
    }

/* MPLS_IPv6 add start */
#ifdef MPLS_IPV6_WANTED
    if (CRU_BUF_Get_U2Reserved (pBuf) == IPV6_L3_TO_MPLS)
    {
        /*Get IPV6 Header from pBuf */
        /*Decrementing Hop Limit */
        pIpv6Hdr = (tIp6Hdr *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear
            (pBuf, 0, MPLS_IPV6_HDR_LEN);
        if (NULL != pIpv6Hdr)
        {
            if (gMplsIncarn[MPLS_DEF_INCARN].DiffServGlobal.u1TnlModel
                == MPLS_TNL_UNIFORM_MODEL)
            {
                if (pIpv6Hdr->u1Hlim == MPLS_ZERO)
                {
                    MPLSFM_DBG (MPLSFM_PRCS_PRCS,
                                "[IPV6 FM] HLim is Zero Func: MplsLabelAndSendOut Returns\n");
                    return MPLS_FAILURE;
                }
                u4Ttl = (UINT4) pIpv6Hdr->u1Hlim - 1;

            }
            else
            {
                u4Ttl = gMplsIncarn[MPLS_DEF_INCARN].u1TTL;
            }
            gMplsIncarn[MPLS_DEF_INCARN].u1iTTL = pIpv6Hdr->u1Hlim;
            if (gMplsIncarn[MPLS_DEF_INCARN].u1iTTL == MPLS_ZERO)
            {
                return MPLS_FAILURE;
            }
            gMplsIncarn[MPLS_DEF_INCARN].u1OTTL =
                (UINT1) (gMplsIncarn[MPLS_DEF_INCARN].u1iTTL - 1);
        }
    }
#endif
/* MPLS_IPv6 add start */

    if (CRU_BUF_Get_U2Reserved (pBuf) == L3_TO_MPLS)
    {
        /* Get IP Header from pBuf */
        /* u4Ttl = IpHdr->Ttl - 1; */
        pIpHdr = (t_IP_HEADER *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear
            (pBuf, 0, MPLS_IP_HDR_LEN);
        if (pIpHdr != NULL)
        {
            if (gMplsIncarn[MPLS_DEF_INCARN].DiffServGlobal.u1TnlModel
                == MPLS_TNL_UNIFORM_MODEL)
            {
                if (pIpHdr->u1Ttl == MPLS_ZERO)
                {
                    return MPLS_FAILURE;
                }
                u4Ttl = (UINT4) pIpHdr->u1Ttl - 1;

            }
            else
            {
                u4Ttl = gMplsIncarn[MPLS_DEF_INCARN].u1TTL;
            }

            gMplsIncarn[MPLS_DEF_INCARN].u1iTTL = pIpHdr->u1Ttl;
            if (gMplsIncarn[MPLS_DEF_INCARN].u1iTTL == MPLS_ZERO)
            {
                return MPLS_FAILURE;
            }

            gMplsIncarn[MPLS_DEF_INCARN].u1OTTL =
                (UINT1) (gMplsIncarn[MPLS_DEF_INCARN].u1iTTL - 1);

        }
    }
    /* If the LSR is TRANSIT,then copy the TTL from the MPLS Header into 
     * the new MPLS header */
    else if (CRU_BUF_Get_U2Reserved (pBuf) == CFA_TO_MPLS)
    {
        if (MPLS_GET_TTL (pBuf) == MPLS_ZERO)
        {
            return MPLS_FAILURE;
        }
        u4Ttl = MPLS_GET_TTL (pBuf) - 1;

        if (gMplsIncarn[MPLS_DEF_INCARN].DiffServGlobal.u1TnlModel
            == MPLS_TNL_UNIFORM_MODEL)
        {
            gMplsIncarn[MPLS_DEF_INCARN].u1iTTL = (UINT1) MPLS_GET_TTL (pBuf);
            gMplsIncarn[MPLS_DEF_INCARN].u1OTTL =
                (UINT1) (gMplsIncarn[MPLS_DEF_INCARN].u1iTTL - 1);
        }
    }

    MPLSFM_DBG2 (MPLSFM_PRCS_PRCS,
                 "-E- Packet with incoming TTL(%d),outgoing TTL (%d)\n",
                 (UINT4) gMplsIncarn[MPLS_DEF_INCARN].u1iTTL,
                 gMplsIncarn[MPLS_DEF_INCARN].u1OTTL);

    if (CRU_BUF_Get_U2Reserved (pBuf) != RPTE_TO_MPLSRTR)
    {
        /* Label Stacking Labels */
        if (pXcEntry->mplsLabelIndex != NULL)
        {
            TMO_SLL_Scan (&(pXcEntry->mplsLabelIndex->LblList), pLblEntry,
                          tLblEntry *)
            {
                if (pLblEntry->u1RowStatus != ACTIVE)
                {
                    continue;
                }

                if (pLblEntry->u4Label != MPLS_IMPLICIT_NULL_LABEL)
                {
                    MplsHdr.u4Lbl = pLblEntry->u4Label;
                    if (MPLS_GET_TTL (pBuf) == MPLS_ZERO)
                    {
                        MplsHdr.Ttl = u4Ttl;
                    }
                    else
                    {
                        if (gMplsIncarn[MPLS_DEF_INCARN].DiffServGlobal.
                            u1TnlModel == MPLS_TNL_UNIFORM_MODEL)
                        {
                            MplsHdr.Ttl = (MPLS_GET_TTL (pBuf) - 1);
                        }
                        else
                        {
                            MplsHdr.Ttl = (MPLS_GET_TTL (pBuf) - 1);
                            u4Ttl = (UINT4) gMplsIncarn[MPLS_DEF_INCARN].u1TTL;
                        }
                    }
                    MplsHdr.Exp = 0;
                    MplsHdr.SI = u4SI;
                    u4SI = 0;
                    MplsPutMplsHdr ((UINT1 *) &u4MplsHdr, &MplsHdr);
                    if (CRU_BUF_Prepend_BufChain
                        (pBuf, (UINT1 *) &u4MplsHdr,
                         sizeof (UINT4)) == CRU_FAILURE)
                    {
                        /* Unable to Prepend MPLS Header */
                        MPLSFWD_L3VPN_DBG
                            ("Error in CRU_BUF_Prepend_BufChain %d\n",
                             __LINE__);
                        return MPLS_FAILURE;
                    }
                }
            }
        }
    }
    if ((pXcEntry->pTeTnlInfo != NULL) &&
        (pXcEntry->pTeTnlInfo->u4TnlMapIndex != 0))
    {
        CONVERT_TO_INTEGER (pXcEntry->pTeTnlInfo->TnlMapIngressLsrId,
                            u4IngressId);
        CONVERT_TO_INTEGER (pXcEntry->pTeTnlInfo->TnlMapEgressLsrId,
                            u4EgressId);
        u4IngressId = OSIX_NTOHL (u4IngressId);
        u4EgressId = OSIX_NTOHL (u4EgressId);
        pFATeTnlInfo = TeGetTunnelInfo (pXcEntry->pTeTnlInfo->u4TnlMapIndex,
                                        pXcEntry->pTeTnlInfo->u4TnlMapInstance,
                                        u4IngressId, u4EgressId);

        if ((pFATeTnlInfo != NULL) &&
            (pFATeTnlInfo->u1TnlLocalProtectInUse == LOCAL_PROT_IN_USE))
        {
            if (pFATeTnlInfo->u1TnlRole == TE_INGRESS)
            {
                Direction = MPLS_DIRECTION_FORWARD;
            }
            else if (pFATeTnlInfo->u1TnlRole == TE_EGRESS)
            {
                Direction = MPLS_DIRECTION_REVERSE;
            }

            pXcEntry = MplsGetXCEntryByDirection (pFATeTnlInfo->u4TnlXcIndex,
                                                  Direction);

            if ((pXcEntry != NULL) &&
                (pXcEntry->pOutIndex != NULL) &&
                (pXcEntry->pOutIndex->GmplsOutSegment.OutSegmentDirection ==
                 pOutSegment->GmplsOutSegment.OutSegmentDirection))
            {
                pOutSegment = pXcEntry->pOutIndex;
            }
        }
    }

    if ((pOutSegment == NULL) || (pOutSegment->u1RowStatus != ACTIVE))
    {
        /* FTN is not fully configured */
        MPLSFWD_L3VPN_DBG ("OutSeg is NULL %d\n", __LINE__);
        return MPLS_FAILURE;
    }

    /* OutSegment Labels */
    if (pOutSegment->u4Label != MPLS_IMPLICIT_NULL_LABEL)
    {
        MplsHdr.u4Lbl = pOutSegment->u4Label;
        MplsHdr.Ttl = u4Ttl;
        MplsHdr.Exp = u4Exp;
        MplsHdr.SI = u4SI;
        MplsPutMplsHdr ((UINT1 *) &u4MplsHdr, &MplsHdr);
        if (CRU_BUF_Prepend_BufChain
            (pBuf, (UINT1 *) &u4MplsHdr, sizeof (UINT4)) == CRU_FAILURE)
        {
            /* Unable to Prepend MPLS Header */
            MPLSFWD_L3VPN_DBG ("Error in CRU_BUF_Prepend_BufChain %d\n",
                               __LINE__);
            return MPLS_FAILURE;
        }
    }
    else
    {
        u1EncapType = CFA_ENCAP_ENETV2;
    }

    /* Router Alert Label */
    if (bIsRalPresent == TRUE)
    {
        MplsHdr.u4Lbl = MPLS_ROUTER_ALERT_LABEL;
        MplsHdr.Ttl = u4Ttl;
        MplsHdr.Exp = 0;
        MplsHdr.SI = 0;
        MplsPutMplsHdr ((UINT1 *) &u4MplsHdr, &MplsHdr);
        if (CRU_BUF_Prepend_BufChain
            (pBuf, (UINT1 *) &u4MplsHdr, sizeof (UINT4)) == CRU_FAILURE)
        {
            /* Unable to Prepend MPLS Header */
            return MPLS_FAILURE;
        }
    }

#ifndef NPAPI_WANTED
    if (MEMCMP (pOutSegment->au1NextHopMac, au1ZeroMac, MAC_ADDR_LEN) !=
        MPLS_ZERO)
    {
        MEMCPY (au1DstMac, pOutSegment->au1NextHopMac, MAC_ADDR_LEN);
    }
    /* MPLS_IPv6 add start */
    else
#endif
    {
#ifdef MPLS_IPV6_WANTED
        if (MPLS_IPV6_ADDR_TYPE == pOutSegment->u1NHAddrType)
        {
            if (MPLS_ZERO !=
                MEMCMP (pOutSegment->NHAddr.u1_addr, V6NhZeroAddr.u1_addr,
                        MPLS_IPV6_ADDR_LEN))
            {
                if ((pXcEntry != NULL) && (pXcEntry->pOutIndex != NULL) &&
                    (CfaUtilGetIfIndexFromMplsTnlIf
                     (pXcEntry->pOutIndex->u4IfIndex, &u4DstIntfIdx,
                      TRUE) == CFA_FAILURE))
                {
                    return MPLS_FAILURE;
                }
                MPLSFM_DBG1 (MPLSFM_PRCS_PRCS,
                             "Related IfIndex for V6 Arp Resolve %x\n",
                             pOutSegment->u4IfIndex);
                if ((NetIpv6Nd6Resolve
                     (u4DstIntfIdx, (tIp6Addr *) & (pOutSegment->NHAddr),
                      (UINT1 *) au1DstMac)) == NETIPV6_FAILURE)
                {
                    MPLSFM_DBG1 (MPLSFM_PRCS_PRCS,
                                 "Arp Resolve Queue Posting failed for Next Hop %s\n",
                                 Ip6PrintAddr ((tIp6Addr *) &
                                               (pOutSegment->NHAddr)));
                    return L2VPN_FAILURE;
                }
            }
        }
        else
        {
#endif
            /* MPLS_IPv6 add end */
            if (pOutSegment->NHAddr.u4_addr[0] != MPLS_ZERO)
            {
                /* ARP Resolve */
                if (ArpResolve (pOutSegment->NHAddr.u4_addr[0],
                                (INT1 *) au1DstMac,
                                &u1EncapType) == ARP_FAILURE)
                {
                    if (MplsGetL3Intf (pOutSegment->u4IfIndex, &u4L3VlanIf) ==
                        MPLS_FAILURE)
                    {
                        MPLSFWD_L3VPN_DBG ("Error = %d\n", __LINE__);
                        return MPLS_FAILURE;
                    }
                    if (NetIpv4GetPortFromIfIndex (u4L3VlanIf, &u4IpPort) ==
                        NETIPV4_FAILURE)
                    {
                        MPLSFWD_L3VPN_DBG ("Error = %d\n", __LINE__);
                        return MPLS_FAILURE;
                    }
                    ArpQMsg.u4MsgType = ARP_APP_IF_MSG;
                    ArpQMsg.u2Port = (UINT2) u4IpPort;
                    ArpQMsg.u4IpAddr = pOutSegment->NHAddr.u4_addr[0];
                    /* Trigger ARP module for Resolving NextHopAddress */
                    if (ARP_FAILURE == ArpEnqueuePkt (&ArpQMsg))
                    {
                        MPLSFWD_L3VPN_DBG ("Error = %d\n", __LINE__);
                    }

                    MPLSFWD_L3VPN_DBG ("Error = %d\n", __LINE__);
                    return MPLS_FAILURE;
                }
            }
#ifdef MPLS_IPV6_WANTED
        }
#endif
    }
#ifdef CFA_WANTED
    if (MplsGetL3Intf (pOutSegment->u4IfIndex, &u4IfIndex) == MPLS_FAILURE)
    {
        MPLSFWD_L3VPN_DBG ("Error = %d\n", __LINE__);
        return MPLS_FAILURE;
    }

    if ((pOutSegment->u4Label != MPLS_IMPLICIT_NULL_LABEL) ||
        ((pOutSegment->u4Label == MPLS_IMPLICIT_NULL_LABEL) &&
         (u4SI == MPLS_NOT_BOS)))
    {
        /* Send the labeled Packet to CFA Task for transmission */
        if (CfaHandlePktFromMpls (pBuf, u4IfIndex, MPLS_TO_CFA,
                                  au1DstMac, CFA_LINK_UCAST) == CFA_SUCCESS)
        {
            MPLSFWD_L3VPN_DBG ("PACKET SENT OUT.................\n");
            return MPLS_SUCCESS;
        }
    }
    else                        /* Implicit NULL Label at Bottom Of Stack (BOS) */
    {
        u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);
        /* Send the unlabeled Packet to CFA Task for transmission */
#ifdef MPLS_IPV6_WANTED
        if (CRU_BUF_Get_U2Reserved (pBuf) == IPV6_L3_TO_MPLS)
        {
            if (CfaIwfEnetProcessTxFrame (pBuf, u4IfIndex, au1DstMac, u4PktSize,
                                          CFA_ENET_IPV6,
                                          u1EncapType) == CFA_SUCCESS)
            {
                return MPLS_SUCCESS;
            }
        }
#endif
        if (CfaIwfEnetProcessTxFrame (pBuf, u4IfIndex, au1DstMac, u4PktSize,
                                      CFA_ENET_IPV4,
                                      u1EncapType) == CFA_SUCCESS)
        {
            return MPLS_SUCCESS;
        }
    }
#endif
    MPLSFWD_L3VPN_DBG ("Error = %d\n", __LINE__);
    return MPLS_FAILURE;
}

/************************************************************************
 *  Function Name   : MplsRemoveMplsHdr 
 *  Description     : Function to copy Mpls Header from Label packet and
 *                    remove it from Packet  
 *  Input           : pBuf - Incoming Label Packet
 *  Output          : pMplsHdr - Top Mpls Header from Labeled packet 
 *  Returns         : MPLS_FAILURE / MPLS_SUCCESS
 ************************************************************************/
PRIVATE INT4
MplsRemoveMplsHdr (tCRU_BUF_CHAIN_HEADER * pBuf, tMplsHdr * pMplsHdr)
{
    UINT4               u4Hdr = 0;

    if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &u4Hdr, 0, sizeof (UINT4)) ==
        CRU_FAILURE)
    {
        return MPLS_FAILURE;
    }

    MplsReadMplsHdr ((UINT1 *) &u4Hdr, pMplsHdr);

    if (CRU_BUF_Move_ValidOffset (pBuf, sizeof (UINT4)) == CRU_FAILURE)
    {
        /* Unable to remove Mpls Header */
        return MPLS_FAILURE;
    }
    return MPLS_SUCCESS;
}

/* ************************************************************************* *
 * Function Name   : MplsGetLblEntryAndAction                                *
 * Description     : This will finds the in segment/pseudowire entry         *
 *                   corresponding to the label and identifies the action    *
 *                   to be taken on the packet                               *
 * Input(s)        : MplsHdr - MPLS header information                       *
 *                   u4IfIndex - Source port index on which pkt was rcvd     *
 *                 : pbIsRalPresent - Boolean variable to find the           *
 *                                    presence of RAL label in packet.       *
 * Output(s)       : pEntry - Pointer to the pseudowire/InSegment            *
 * Return(s)       : Action to be taken according to the header              *
 * ************************************************************************* */
PRIVATE             tLabelAction
MplsGetLblEntryAndAction (UINT1 *pPkt, UINT4 u4IfIndex, VOID **pEntry,
                          BOOL1 * bIsDblLblLookup, BOOL1 * bIsPacketOamPacket,
                          BOOL1 * pbIsRalPresent, UINT4 *pu4SearchLabel)
{
    UINT4               u4Lbl1 = MPLS_INVALID_LABEL;
    UINT4               u4Lbl2 = MPLS_INVALID_LABEL;
    UINT4               u4Lbl3 = MPLS_INVALID_LABEL;
    UINT4               u4Ttl = 0;
    UINT4               u4LsrXcIndex = 0;
    tPwVcMplsInTnlEntry *pPwVcMplsInTnl = NULL;
    tMplsHdr            MplsHdr;
    tInSegment         *pInSegment = NULL;
    tXcEntry           *pXcEntry = NULL;
    tPwVcEntry         *pPwVcEntry = NULL;
    MplsReadMplsHdr (pPkt, &MplsHdr);
    if (MplsHdr.u4Lbl == MPLS_ROUTER_ALERT_LABEL)
    {
        /* Router Alert Label */
        *pbIsRalPresent = TRUE;
        pPkt += MPLS_HDR_LEN;
        MplsReadMplsHdr (pPkt, &MplsHdr);
    }

    if ((MplsHdr.Ttl == 1) || (MplsHdr.u4Lbl == MPLS_GAL_LABEL))
    {
        /* First label TTL expired(TTL == 1) or label is Router
         * Alert Label( == 1) or label is GSl label( == 13).
         */
        *bIsPacketOamPacket = TRUE;
        return MPLS_HW_PUSH_LABEL;
    }

    /* Check whether the label belongs to any PW or not - Quick */
    *pEntry = L2VpnGetPwVcEntryFromLabel (MplsHdr.u4Lbl, u4IfIndex);
    if (*pEntry != NULL)
    {
#if 0
        printf ("%s : %d Found some PW\n", __FUNCTION__, __LINE__);
#endif
        /* Here we are going to verify PwVc entry contains valid
         * tunnel or not with the help of tunnel labels */
        pPwVcEntry = (tPwVcEntry *) * pEntry;

        if (pPwVcEntry->u1CPOrMgmtOperStatus != L2VPN_PWVC_OPER_UP)
        {
            return MPLS_HW_PUSH_LABEL;
        }

        /* Verifying MPLS type from PW */
        if (L2VPN_PWVC_MPLS_MPLS_TYPE ((tPwVcMplsEntry *)
                                       L2VPN_PWVC_PSN_ENTRY
                                       (pPwVcEntry)) == L2VPN_MPLS_TYPE_VCONLY)
        {
            return MPLS_HW_ACTION_POP_L2_SWITCH;
        }

        pPwVcMplsInTnl = L2VPN_PWVC_MPLS_IN_ENTRY ((tPwVcMplsEntry *)
                                                   L2VPN_PWVC_PSN_ENTRY
                                                   (pPwVcEntry));
        u4LsrXcIndex = pPwVcMplsInTnl->u4LsrXcIndex;

        /* If XC Index associated with PW is zero, it means underlying PSN
         * tunnel is using implicit-null label. So, action for received
         * packet should be POP_L2_SWITCH. */
        if (u4LsrXcIndex == 0)
        {
            return MPLS_HW_ACTION_POP_L2_SWITCH;
        }

        if ((pu4SearchLabel[1] != MPLS_INVALID_LABEL)
            && (!(MPLS_IS_RESERVED_LABEL (pu4SearchLabel[1]))))
        {
            pInSegment = MplsFwdGetTwoInSegmentLabelEntry (pu4SearchLabel[0],
                                                           pu4SearchLabel[1]);
        }
        else
        {
            pInSegment = MplsFwdGetInSegmentTableEntry (pu4SearchLabel[0]);
#if 0
            printf ("vishalPw : %s : %d Label=%d\n", __FUNCTION__, __LINE__,
                    pu4SearchLabel[0]);
#endif
        }

        if ((pInSegment != NULL) && (pInSegment->pXcIndex != NULL))
        {
            pXcEntry = (tXcEntry *) pInSegment->pXcIndex;
#if 0
            printf
                (" vishalPw %s : %d : u4LsrXcIndex=%d pXcEntry->u4Index=%d\n",
                 __FUNCTION__, __LINE__, u4LsrXcIndex, pXcEntry->u4Index);
#endif
            if (u4LsrXcIndex == pXcEntry->u4Index)
            {
                return MPLS_HW_ACTION_POP_L2_SWITCH;
            }
        }
        return MPLS_HW_PUSH_LABEL;
    }
#ifdef MPLS_L3VPN_WANTED
    /* Check whether the label belongs to any PW or not - Quick */
    *pEntry = L3vpnGetBgpRouteLabelEntry (MplsHdr.u4Lbl);
    if (*pEntry != NULL)
    {
        return MPLS_HW_ACTION_POP_L3VPN_LABEL_SWITCH;
    }
#endif
    u4Lbl1 = MplsHdr.u4Lbl;
    if (MplsHdr.SI != 1)
    {
        pPkt += MPLS_HDR_LEN;
        if (gMplsIncarn[MPLS_DEF_INCARN].DiffServGlobal.u1TnlModel
            != MPLS_TNL_UNIFORM_MODEL)
        {
            gMplsIncarn[MPLS_DEF_INCARN].u1iTTL = (UINT1) MplsHdr.Ttl;
        }
        MplsReadMplsHdr (pPkt, &MplsHdr);
        u4Lbl2 = MplsHdr.u4Lbl;
        u4Ttl = MplsHdr.Ttl;
        if (gMplsIncarn[MPLS_DEF_INCARN].DiffServGlobal.u1TnlModel
            == MPLS_TNL_UNIFORM_MODEL)
        {
            gMplsIncarn[MPLS_DEF_INCARN].u1iTTL = (UINT1) MplsHdr.Ttl;
        }
        if (MplsHdr.SI == 1)
        {
            if (!(MPLS_IS_RESERVED_LABEL (u4Lbl2)))
            {
                *bIsDblLblLookup = TRUE;
            }
        }
        else
        {
            pPkt += MPLS_HDR_LEN;
            MplsReadMplsHdr (pPkt, &MplsHdr);
            u4Lbl3 = MplsHdr.u4Lbl;
            if (MplsHdr.SI == 1)
            {
                *bIsDblLblLookup = TRUE;
            }
        }
    }

    if (*bIsDblLblLookup == TRUE)
    {
        *pEntry = MplsFwdGetTwoInSegmentLabelEntry (u4Lbl1, u4Lbl2);
        if (*pEntry == NULL)
        {
            *bIsDblLblLookup = FALSE;
        }
    }
    if (*bIsDblLblLookup == FALSE)
    {
        *pEntry = MplsFwdGetInSegmentTableEntry (u4Lbl1);
    }

    pInSegment = *pEntry;
    if (pInSegment == NULL)
    {
        /* Invalid label - increment stats if any related to this error */
        MPLSFM_DBG2 (MPLSFM_IF_RX,
                     "-E- Packet with INVALID LABEL(%d) on port(%d)\n",
                     MplsHdr.u4Lbl, u4IfIndex);
        return MPLS_HW_PUSH_LABEL;
    }
    pXcEntry = (tXcEntry *) INSEGMENT_XC_INDEX (pInSegment);

    if ((pXcEntry == NULL) || (pXcEntry->u1OperStatus != XC_OPER_UP) ||
        ((pXcEntry->pTeTnlInfo != NULL) &&
         (pXcEntry->pTeTnlInfo->u1CPOrMgmtOperStatus != TE_OPER_UP)))
    {
        /* Invalid label - increment stats if any related to this error */
        MPLSFM_DBG2 (MPLSFM_IF_RX,
                     "-E- Packet with INVALID LABEL(%d) on port(%d)\n",
                     MplsHdr.u4Lbl, u4IfIndex);
        return MPLS_HW_PUSH_LABEL;
    }

    if ((XC_OUTINDEX (pXcEntry) == NULL) ||
        (OUTSEGMENT_ROW_STATUS (XC_OUTINDEX (pXcEntry)) != ACTIVE))
    {
        if (*bIsDblLblLookup == TRUE)
        {
            if (u4Ttl == 1)
            {
                /* TTL 1 */
                *bIsPacketOamPacket = TRUE;
                return MPLS_HW_PUSH_LABEL;
            }
            if (u4Lbl3 != MPLS_INVALID_LABEL)
            {
                pu4SearchLabel[0] = u4Lbl1;
                pu4SearchLabel[1] = u4Lbl2;
                return MPLS_HW_ACTION_POP_SEARCH;
            }
            else
            {
                return MPLS_HW_ACTION_POP_L3_SWITCH;
            }
        }
        else
        {
            if ((u4Lbl1 != MPLS_INVALID_LABEL)
                && (u4Lbl2 != MPLS_INVALID_LABEL))
            {
                pu4SearchLabel[0] = u4Lbl1;
                return MPLS_HW_ACTION_POP_SEARCH;
            }
            return MPLS_HW_ACTION_POP_L3_SWITCH;
        }
    }
    else                        /* LSR */
    {
        if ((*bIsDblLblLookup == TRUE) && (u4Ttl == 1))
        {
            /* TTL 1 */
            *bIsPacketOamPacket = TRUE;
            return MPLS_HW_PUSH_LABEL;
        }
        return MPLS_HW_ACTION_SWAP;
    }
}

/* ***************************************************************************
 *                                   
 *    Function Name             : MplsPktHandleFromCfa
 *
 *    Description               : This function gives the incoming mpls packet
 *                                i.e from the driver to mpls. Buffer will be
 *                                released by GDD incase of failure otherwise
 *                                mpls should take care of releasing it after
 *                                processing.
 *
 *    Input(s)                  : PBuf - Pointer to the CRU Buffer. 
 *                                u2IfIndex - Incoming interface index
 *                                u4PktSize - Incoming packet size
 *
 *    Output(s)                 : None.
 *
 *    Returns                   : MPLS_SUCCESS or MPLS_FAILURE.
 *
 * ************************************************************************* */

INT4
MplsPktHandleFromCfa (tCRU_BUF_CHAIN_HEADER * pBuf, UINT2 u2IfIndex,
                      UINT4 u4PktSize)
{
    if (gu1MplsInitialised != TRUE)
    {
        return MPLS_FAILURE;
    }

    /* fill the rcvd intf, packet type and enqueue the packet to MPLS */
    CRU_BUF_Set_SourceModuleId (pBuf, CFA_MODULE);
    CRU_BUF_Set_U2Reserved (pBuf, CFA_TO_MPLS);    /* saying labelled packet */
    CRU_BUF_Set_U4Reserved1 (pBuf, u2IfIndex);
    CRU_BUF_Set_U4Reserved2 (pBuf, u4PktSize);
    if (OsixQueSend (gFmInQId, (UINT1 *) (&pBuf),
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        return (MPLS_FAILURE);
    }

    /* tell mpls that there is something to do for you */
    if (OsixEvtSend (gFmTaskId, MPLS_IN_MSG_Q_EVENT) == OSIX_FAILURE)
    {
        return MPLS_FAILURE;
    }
    return MPLS_SUCCESS;
}

/* ************************************************************************* *
 *  Function Name   : MplsHandlePktForByPassTnl                              * 
 *  Description     : Called from CFA task to post packet to MPLS if         *
 *                    FTN is configured for the destionation IP              *
 *  Input           : pBuf - Packet Buffer                                   *
 *                    u4IfIndex - Incoming interface                         *
 *                    u4DestIp - Destination IP                              *
 *  Output          : NONE                                                   *
 *  Returns         : MPLS_FAILURE / MPLS_SUCCESS                            *
 * ************************************************************************* */
INT4
MplsHandlePktForByPassTnl (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4XcIndex)
{
    tXcEntry           *pXcEntry = NULL;
    UINT4               u4SI = 1;
    UINT4               u4Exp = 0;

    pXcEntry = MplsGetXCEntryByDirection (u4XcIndex, MPLS_DEF_DIRECTION);
    if (pXcEntry == NULL)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, MPLS_BUF_NORMAL_REL);
        return MPLS_FAILURE;
    }
    /* Bottom of stack is set */
    if (MplsLabelAndSendout (pBuf, pXcEntry, u4SI, u4Exp, FALSE) ==
        MPLS_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, MPLS_BUF_NORMAL_REL);
        return MPLS_FAILURE;
    }
    return MPLS_SUCCESS;
}

#ifdef MPLS_L3VPN_WANTED
/* ************************************************************************* *
 *  Function Name   : MplsTriggerArpRequest                                  * 
 *  Description     : Trigger ARP request for given IP on the given Iface    *
 *  Input           : u4IpAddress - IP Address for which ARP is required     *
 *                    u2Port - Interface on which u4IpAddressis reachable    *
 *  Output          : NONE                                                   *
 *  Returns         : VOID                                                   *
 * ************************************************************************* */
PRIVATE VOID
MplsTriggerArpRequest (UINT4 u4IpAddress, UINT2 u2Port)
{
    tArpQMsg            ArpQMsg;

    MEMSET (&ArpQMsg, 0, sizeof (tArpQMsg));

    ArpQMsg.u4MsgType = ARP_APP_IF_MSG;
    ArpQMsg.u4IpAddr = u4IpAddress;
    ArpQMsg.u2Port = u2Port;

    /* Trigger ARP module for Resolving NextHopAddress */
    if (ArpEnqueuePkt (&ArpQMsg) == ARP_FAILURE)
    {
        MPLSFWD_L3VPN_DBG
            ("MplsTriggerArpRequest: ArpEnqueuePkt Failed at line %d\n",
             __LINE__);
    }
}
#endif

#ifdef MPLS_IPV6_WANTED

/* ************************************************************************* *
 *  Function Name   : MplsProcessIpv6Pkt                                       * 
 *  Description     : Called from CFA task to post packet to MPLS if         *
 *                    FTN is configured for the IPV6 destionation              *
 *  Input           : pBuf - Packet Buffer                                   *
 *                    u4IfIndex - Incoming interface                         *
 *                    DestIp - IPV6 Destination                               *
 *                    bCfaLockReqd - Flag to indicate if CfaLock is required *
 *  Output          : NONE                                                   *
 *  Returns         : MPLS_FAILURE / MPLS_SUCCESS                            *
 * ************************************************************************* */
INT4
MplsProcessIpv6Pkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex,
                    tIp6Addr V6DestIp, BOOL1 bCfaLockReqd)
{
    tFtnEntry          *pFtnEntry = NULL;
    tNetIpv6RtInfoQueryMsg V6RtQuery;
    tNetIpv6RtInfo      NetIpv6RtInfo;

    UNUSED_PARAM (u4IfIndex);
    if (gu1MplsInitialised != TRUE)
    {
        /* MPLS is not enabled */
        return MPLS_FAILURE;
    }
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&V6RtQuery, 0, sizeof (tNetIpv6RtInfoQueryMsg));
    MEMCPY (V6RtQuery.Ip6Dst.u1_addr, V6DestIp.u1_addr, MPLS_IPV6_ADDR_LEN);
    V6RtQuery.u1Prefixlen = (UINT1) MPLS_IPV6_MAX_PREFIX;
    V6RtQuery.u1QueryFlag = (UINT1) RTM6_QUERIED_FOR_NEXT_HOP;
    if (NetIpv6GetRoute (&V6RtQuery, &NetIpv6RtInfo) == NETIPV6_SUCCESS)
    {
        if (NetIpv6RtInfo.i1Proto == IP6_LOCAL_PROTOID)
        {
            /* If local net then do not do labeling */
            return MPLS_FAILURE;
        }
    }

    if (bCfaLockReqd == TRUE)
    {
        CFA_UNLOCK ();
    }
    MPLS_CMN_LOCK ();
    pFtnEntry = MplsFwdGetFtnIpv6TableEntry (&V6DestIp);
    if (((pFtnEntry == NULL) || (pFtnEntry->u1RowStatus != ACTIVE)) ||
        (pFtnEntry->bIsHostAddrFecForPSN))
    {
        /* No FTN configured so, continue normal IP processing */
        MPLS_CMN_UNLOCK ();
        if (bCfaLockReqd == TRUE)
        {
            CFA_LOCK ();
        }
        return MPLS_FAILURE;
    }
    CRU_BUF_Set_U4Reserved1 (pBuf, pFtnEntry->u4FtnIndex);
    MPLS_CMN_UNLOCK ();

    if (bCfaLockReqd == TRUE)
    {
        CFA_LOCK ();
    }

    CRU_BUF_Set_SourceModuleId (pBuf, CFA_MODULE);
    CRU_BUF_Set_U2Reserved (pBuf, IPV6_L3_TO_MPLS);

    /* Send the buffer to MPLS Queue  */

    if (OsixQueSend (gFmInQId, (UINT1 *) (&pBuf),
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)

    {
        /* Packet should go via MPLS but Unable to send to MPLS-FM Queue
         * so release the buffer and say pkt has taken care to the caller */
        MPLS_REL_BUF_CHAIN (pBuf, MPLS_BUF_NORMAL_REL);
    }
    else
    {
        /* Tell MPLS-FM that somebody is waiting for your attention */
        if (OsixEvtSend (gFmTaskId, MPLS_IN_MSG_Q_EVENT) == OSIX_FAILURE)
        {
            return MPLS_FAILURE;
        }
    }

    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsSendL3LabelPktToIpv6 
 *  Description     : Function to remove the labeled packet and send
 *                    it to IPV6 for normal IPV6 processing. This will happen
 *                    at egress node.   
 *  Input           : pBuf - Labeled packet pointer
 *                    u4IfIndex - Incoming If Index 
 *                    pakcet
 *  Output          : NONE 
 *  Returns         : MPLS_FAILURE / MPLS_SUCCESS
 ************************************************************************/
PRIVATE INT4
MplsSendL3LabelPktToIpv6 (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex)
{
    tMplsHdr            MplsHdr;
    UINT4               u4Lbl = MPLS_ZERO;
    tIp6Hdr            *pIpv6Hdr = NULL;
    UINT1               u1IpTTL;

    /* Save TTL for egress processing */
    MPLS_SAVE_TTL (pBuf, gMplsIncarn[MPLS_DEF_INCARN].u1iTTL);
    if (MplsRemoveMplsHdr (pBuf, &MplsHdr) == MPLS_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return MPLS_FAILURE;
    }
    u4Lbl = MplsHdr.u4Lbl;
    /* Get the label and check for implicit null. if it is 
     * implicit null, reduce ttl by one irrespective of tunnel mode*/
    if (CRU_BUF_Get_U2Reserved (pBuf) == CFA_TO_MPLS)
    {
        pIpv6Hdr =
            (tIp6Hdr *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear
            (pBuf, 0, MPLS_IPV6_HDR_LEN);
        if (pIpv6Hdr != NULL)
        {
            u1IpTTL = pIpv6Hdr->u1Hlim;
            if ((gMplsIncarn[MPLS_DEF_INCARN].DiffServGlobal.u1TnlModel ==
                 MPLS_TNL_UNIFORM_MODEL) || (u4Lbl == MPLS_IMPLICIT_NULL_LABEL))
            {
                gMplsIncarn[MPLS_DEF_INCARN].u1iTTL = (UINT1) MplsHdr.Ttl;
                pIpv6Hdr->u1Hlim = (UINT1) MplsHdr.Ttl;
            }
            else
            {
                gMplsIncarn[MPLS_DEF_INCARN].u1iTTL = u1IpTTL;
                pIpv6Hdr->u1Hlim = (UINT1) (u1IpTTL - MPLS_ONE);
            }
        }
    }
    if (MplsHdr.SI != 1)
    {
        /* Some more label present so, can not give it to
         * IP module */
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return MPLS_FAILURE;
    }
#ifdef CFA_WANTED
    if (CfaHandlePktFromMpls (pBuf, u4IfIndex, IPV6_MPLS_TO_L3,
                              NULL, CFA_LINK_UCAST) == CFA_FAILURE)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        return MPLS_FAILURE;
    }
#else
    UNUSED_PARAM (u4IfIndex);
    CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
#endif

    return MPLS_SUCCESS;
}

#endif
