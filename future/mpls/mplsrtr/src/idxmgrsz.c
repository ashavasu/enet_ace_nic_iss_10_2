/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: idxmgrsz.c,v 1.3 2013/11/29 11:04:14 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _INDEXMGRSZ_C
#include "mplsincs.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
IndexmgrSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < INDEXMGR_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsINDEXMGRSizingParams[i4SizingId].u4StructSize,
                              FsINDEXMGRSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(INDEXMGRMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            IndexmgrSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
IndexmgrSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsINDEXMGRSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, INDEXMGRMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
IndexmgrSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < INDEXMGR_MAX_SIZING_ID; i4SizingId++)
    {
        if (INDEXMGRMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (INDEXMGRMemPoolIds[i4SizingId]);
            INDEXMGRMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
