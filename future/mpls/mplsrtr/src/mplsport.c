/*---------------------------------------------------------------------------*/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: mplsport.c,v 1.31 2018/02/15 10:21:29 siva Exp $
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : mplsport.c
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : MPLS
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  : 
 *    DESCRIPTION            : This file contains imported APIs that are 
 *                             used by MPLS module.
 *---------------------------------------------------------------------------*/

#include "mplsincs.h"
#include "oamdefn.h"
#include "snmputil.h"
#include "elps.h"
#include "lspp.h"
#include "bfd.h"
#include "mplscli.h"
#include "fscfacli.h"
#include "ifmibcli.h"
#ifdef RFC6374_WANTED
#include "r6374.h"
#endif

extern INT4         IfMainAdminStatusSet (tSnmpIndex *, tRetVal *);
extern INT4         IfMainRowStatusSet (tSnmpIndex *, tRetVal *);
extern INT4         IfMainTypeSet (tSnmpIndex *, tRetVal *);
extern INT4         IfStackStatusSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData);

extern INT4         IfAliasSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData);
/*****************************************************************************/
/*                                                                           */
/* Function     : MplsPortEventNotification                                  */
/*                                                                           */
/* Description  : This function notifes the events and deliver the           */
/*                application specific packets to registered external        */
/*                modules.                                                   */
/*                                                                           */
/* Input        : u4DestModId - Destination Module identifier                */
/*                pMplsEventNotif - Pointer to an event notification         */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
INT4
MplsPortEventNotification (UINT4 u4DestModId, tMplsEventNotif * pMplsEventNotif)
{
    tLsppReqParams      LsppReqParams;
    tLsppRespParams     LsppRespParams;
#ifdef RFC6374_WANTED
    tR6374ReqParams     R6374ReqParams;
    tR6374RespParams    R6374RespParams;
#endif
    tBfdReqParams      *pBfdReqParams = NULL;
#ifdef HVPLS_WANTED
#ifdef ECFM_WANTED
    tCRU_BUF_CHAIN_HEADER *pCruBuf = NULL;
#endif
#endif
    INT4                i4RetVal = OSIX_FAILURE;

    MEMSET (&LsppReqParams, 0, sizeof (tLsppReqParams));
    MEMSET (&LsppRespParams, 0, sizeof (tLsppRespParams));

#ifdef RFC6374_WANTED
    MEMSET (&R6374ReqParams, 0, sizeof (tR6374ReqParams));
    MEMSET (&R6374RespParams, 0, sizeof (tR6374RespParams));
#endif

    pBfdReqParams =
        (tBfdReqParams *) MemAllocMemBlk (MPLS_BFD_REQ_PARAMS_POOL_ID);
    if (pBfdReqParams == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pBfdReqParams, 0, sizeof (tBfdReqParams));

    /* Notify the events to MPLS applications */
    if (u4DestModId & MPLS_APPLICATION_ELPS)
    {
        /* 1. MEG up/down indication
         * 2. PSC message Rx */
        if ((gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
             [MPLS_ELPS_APP_ID] != NULL) &&
            (gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
             [MPLS_ELPS_APP_ID]->pFnRcvPkt != NULL) &&
            ((gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
              [MPLS_ELPS_APP_ID]->u4Events & pMplsEventNotif->u2Event) != 0))
        {
            i4RetVal = (*(gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
                          [MPLS_ELPS_APP_ID]->pFnRcvPkt))
                (ELPS_MPLS_APP_ID, pMplsEventNotif);
        }

        if ((pMplsEventNotif->u2Event == MPLS_PSC_PACKET)
            && (i4RetVal == OSIX_FAILURE))
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsPortEventNotification: "
                        "ELSP events posting to ELPS module "
                        "is failed: INTMD-EXIT \n");
            CRU_BUF_Release_MsgBufChain (pMplsEventNotif->pBuf, FALSE);
            MemReleaseMemBlock (MPLS_BFD_REQ_PARAMS_POOL_ID,
                                (UINT1 *) pBfdReqParams);
            return OSIX_FAILURE;
        }
    }
    if (u4DestModId & MPLS_APPLICATION_BFD)
    {
        /* 1. MEG up/down indication 
         * 2. BFD message Rx */

        if (pMplsEventNotif->u2Event == MPLS_BFD_PACKET)
        {
            pBfdReqParams->u4ReqType = BFD_RX_MSG;
            pBfdReqParams->unReqInfo.BfdRecvBfdPacket.pBuf =
                pMplsEventNotif->pBuf;
            pBfdReqParams->unReqInfo.BfdRecvBfdPacket.u4IfIndex =
                pMplsEventNotif->u4InIfIndex;
        }
        else
        {
            pBfdReqParams->u4ReqType = BFD_PATH_STATUS_CHG;
            pBfdReqParams->unReqInfo.BfdOamInInfo.u4SessIndex =
                pMplsEventNotif->u4ProactiveSessIndex;
            switch (pMplsEventNotif->u2Event)
            {
                case MPLS_MEG_UP_EVENT:
                case MPLS_TNL_UP_EVENT:
                case MPLS_PW_UP_EVENT:
                    pBfdReqParams->unReqInfo.BfdOamInInfo.u4PathStatus =
                        MPLS_PATH_STATUS_UP;
                    break;
                case MPLS_MEG_DOWN_EVENT:
                case MPLS_TNL_DOWN_EVENT:
                case MPLS_PW_DOWN_EVENT:
                    pBfdReqParams->unReqInfo.BfdOamInInfo.u4PathStatus =
                        MPLS_PATH_STATUS_DOWN;
                    break;
                default:
                    MemReleaseMemBlock (MPLS_BFD_REQ_PARAMS_POOL_ID,
                                        (UINT1 *) pBfdReqParams);
                    return OSIX_FAILURE;
            }
        }
#ifdef BFD_WANTED
        i4RetVal = BfdApiHandleExtRequest (pMplsEventNotif->u4ContextId,
                                           pBfdReqParams, NULL);
#endif
        if ((pMplsEventNotif->u2Event == MPLS_BFD_PACKET) &&
            (i4RetVal == OSIX_FAILURE))
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsPortEventNotification: "
                        "BFD packet posting to BFD module "
                        "is failed: INTMD-EXIT \n");
            CRU_BUF_Release_MsgBufChain (pMplsEventNotif->pBuf, FALSE);
            MemReleaseMemBlock (MPLS_BFD_REQ_PARAMS_POOL_ID,
                                (UINT1 *) pBfdReqParams);
            return OSIX_FAILURE;
        }
    }
    if (u4DestModId & MPLS_APPLICATION_LSP_PING)
    {
        /* 1. LSP ping message Rx 
           Notify LSP Ping module here */

        LsppReqParams.u1MsgType = LSPP_RX_PDU_MSG;
        LsppReqParams.u4ContextId = pMplsEventNotif->u4ContextId;
        LsppReqParams.unMsgParam.LsppRxPduInfo.pBuf = pMplsEventNotif->pBuf;
        LsppReqParams.unMsgParam.LsppRxPduInfo.u4IfIndex =
            pMplsEventNotif->u4InIfIndex;
#ifdef LSPP_WANTED
        i4RetVal = LsppApiHandleExtRequest (&LsppReqParams, &LsppRespParams);
#endif
        if (i4RetVal == OSIX_FAILURE)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsPortEventNotification: "
                        "LSP ping packet posting to LSP ping module "
                        "is failed: INTMD-EXIT \n");
            CRU_BUF_Release_MsgBufChain (pMplsEventNotif->pBuf, FALSE);
            MemReleaseMemBlock (MPLS_BFD_REQ_PARAMS_POOL_ID,
                                (UINT1 *) pBfdReqParams);
            return OSIX_FAILURE;
        }
    }
#ifdef RFC6374_WANTED
    if (u4DestModId & MPLS_APPLICATION_RFC6374)
    {
        /* 1. RFC6374 message Rx 
           Notify RFC6374 module here */

        if ((pMplsEventNotif->u2Event == MPLS_TNL_UP_EVENT) ||
            (pMplsEventNotif->u2Event == MPLS_TNL_DOWN_EVENT) ||
            (pMplsEventNotif->u2Event == MPLS_PW_UP_EVENT) ||
            (pMplsEventNotif->u2Event == MPLS_PW_DOWN_EVENT))
        {
            if ((gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
                 [MPLS_RFC6374_APP_ID] != NULL) &&
                (gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
                 [MPLS_RFC6374_APP_ID]->pFnRcvPkt != NULL) &&
                ((gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
                  [MPLS_RFC6374_APP_ID]->u4Events & pMplsEventNotif->u2Event) !=
                 0))
            {
                i4RetVal = (*(gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
                              [MPLS_RFC6374_APP_ID]->pFnRcvPkt))
                    (MPLS_RFC6374_APP_ID, pMplsEventNotif);
            }
            if (i4RetVal == OSIX_FAILURE)
            {
                MPLSFM_DBG (MPLSFM_IF_FAILURE,
                            "MplsPortEventNotification: "
                            "Events posting to Y.1731/RFC6374 module "
                            "is failed: INTMD-EXIT \n");
                CRU_BUF_Release_MsgBufChain (pMplsEventNotif->pBuf, FALSE);
                MemReleaseMemBlock (MPLS_BFD_REQ_PARAMS_POOL_ID,
                                    (UINT1 *) pBfdReqParams);
                return OSIX_FAILURE;
            }
        }
        if (pMplsEventNotif->u2Event == MPLS_RFC6374_PACKET)
        {
            R6374ReqParams.u1MsgType = RFC6374_RX_PDU_MSG;
            R6374ReqParams.u4ContextId = pMplsEventNotif->u4ContextId;
            R6374ReqParams.unMsgParam.R6374RxPduInfo.pBuf =
                pMplsEventNotif->pBuf;
            R6374ReqParams.unMsgParam.R6374RxPduInfo.u4IfIndex =
                pMplsEventNotif->u4InIfIndex;
            i4RetVal =
                R6374ApiHandleExtRequest (&R6374ReqParams, &R6374RespParams);
            if (i4RetVal == OSIX_FAILURE)
            {
                MPLSFM_DBG (MPLSFM_IF_FAILURE,
                            "MplsPortEventNotification: "
                            "RFC6374 packet posting to LSP ping module "
                            "is failed: INTMD-EXIT \n");
                CRU_BUF_Release_MsgBufChain (pMplsEventNotif->pBuf, FALSE);
                MemReleaseMemBlock (MPLS_BFD_REQ_PARAMS_POOL_ID,
                                    (UINT1 *) pBfdReqParams);
                return OSIX_FAILURE;
            }
        }
    }
#endif
    if (u4DestModId & MPLS_APPLICATION_Y1731)
    {
        if ((gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
             [MPLS_Y1731_APP_ID] != NULL) &&
            (gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
             [MPLS_Y1731_APP_ID]->pFnRcvPkt != NULL) &&
            ((gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
              [MPLS_Y1731_APP_ID]->u4Events & pMplsEventNotif->u2Event) != 0))
        {
            i4RetVal = (*(gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
                          [MPLS_Y1731_APP_ID]->pFnRcvPkt))
                (MPLS_Y1731_APP_ID, pMplsEventNotif);
        }
        if ((pMplsEventNotif->u2Event == MPLS_Y1731_PACKET)
            && (i4RetVal == OSIX_FAILURE))
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsPortEventNotification: "
                        "Events posting to Y.1731 module "
                        "is failed: INTMD-EXIT \n");
            CRU_BUF_Release_MsgBufChain (pMplsEventNotif->pBuf, FALSE);
            MemReleaseMemBlock (MPLS_BFD_REQ_PARAMS_POOL_ID,
                                (UINT1 *) pBfdReqParams);
            return OSIX_FAILURE;
        }
    }

    /*Notify events to ERPS module if it has registered */

    if (u4DestModId & MPLS_APPLICATION_ERPS)
    {
        if ((gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
             [MPLS_ERPS_APP_ID] != NULL) &&
            (gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
             [MPLS_ERPS_APP_ID]->pFnRcvPkt != NULL) &&
            ((gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
              [MPLS_ERPS_APP_ID]->u4Events & pMplsEventNotif->u2Event) != 0))
        {
            /*Calling the registered callback routine */

            i4RetVal = (*(gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
                          [MPLS_ERPS_APP_ID]->pFnRcvPkt))
                (MPLS_APPLICATION_ERPS, pMplsEventNotif);
        }

        if (i4RetVal == OSIX_FAILURE)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsPortEventNotification: "
                        "ERPS events posting to ERPS module "
                        "has failed: INTMD-EXIT \n");
            CRU_BUF_Release_MsgBufChain (pMplsEventNotif->pBuf, FALSE);

            MemReleaseMemBlock (MPLS_BFD_REQ_PARAMS_POOL_ID,
                                (UINT1 *) pBfdReqParams);
            return OSIX_FAILURE;
        }

    }

#ifdef HVPLS_WANTED
    if (u4DestModId & MPLS_APPLICATION_ECFM)
    {
#ifdef ECFM_WANTED
        pCruBuf = CRU_BUF_Duplicate_BufChain (pMplsEventNotif->pBuf);
        if (pCruBuf == NULL)
        {
            CRU_BUF_Release_MsgBufChain (pMplsEventNotif->pBuf, FALSE);

            MemReleaseMemBlock (MPLS_BFD_REQ_PARAMS_POOL_ID,
                                (UINT1 *) pBfdReqParams);
            return OSIX_FAILURE;
        }
        /*ECFM releasing the buffer pCruBuf */
        /*The buffer is queued and event is posted to ECFM_LBLT TASK in API: EcfmHandleInFrameFromPort  */
        i4RetVal =
            EcfmHandleInFrameFromPort (pCruBuf, pMplsEventNotif->u4InIfIndex);
        if (i4RetVal == OSIX_FAILURE)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsPortEventNotification: "
                        "ECFM events posting to ECFM module "
                        "has failed: INTMD-EXIT \n");
            MemReleaseMemBlock (MPLS_BFD_REQ_PARAMS_POOL_ID,
                                (UINT1 *) pBfdReqParams);
            return OSIX_FAILURE;
        }
#endif
        /*VLAN releasing the buffer pMplsEventNotif->pBuf */
        /*The buffer is queued and event is posted to VLAN in API: VlanProcessPacket */
        /* The task has to be delayed by minimum possible milli seconds 
         * This is done to ensure that CPU(ERPS) has handled the RAPS PDU and perform action, 
         * then forwarding the RAPS to other node in the ring.
         * To delay the task by 1 milliseconds,we are passing a value 
         * of 1000 to OsixDelay, which internally converts the value to 
         * 1000000 nanoseconds.*/

        OsixDelay ((UINT4) (MPLS_CPU_HW_TRANS_INTERVAL * 1000),
                   (INT4) OSIX_MICRO_SECONDS);

        i4RetVal =
            VlanProcessPacket (pMplsEventNotif->pBuf,
                               pMplsEventNotif->u4InIfIndex);
        if (i4RetVal == OSIX_FAILURE)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsPortEventNotification: "
                        " Forwarding ECFM Packet to VLAN module "
                        "has failed: INTMD-EXIT \n");
            MemReleaseMemBlock (MPLS_BFD_REQ_PARAMS_POOL_ID,
                                (UINT1 *) pBfdReqParams);
            return OSIX_FAILURE;
        }

    }
#endif
    MemReleaseMemBlock (MPLS_BFD_REQ_PARAMS_POOL_ID, (UINT1 *) pBfdReqParams);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : MplsPortGetTrafficClassFromPhb                             */
/*                                                                           */
/* Description  : This function fetches the traffic class                    */
/*                (formerly known as EXP) from PHB value                     */
/*                                                                           */
/* Input        : i4PhbValue - Per Hop Behaviour value                       */
/*                pu1TrafficClass - Traffic class value                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
INT4
MplsPortGetTrafficClassFromPhb (INT4 i4PhbValue, UINT1 *pu1TrafficClass)
{
    UNUSED_PARAM (i4PhbValue);
    *pu1TrafficClass = MPLS_BEST_OF_SERVICE;
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : MplsPortVcmGetAliasName 
 *
 * DESCRIPTION      : Routine used to get the Alias Name for the context 
 *
 * INPUT            : u4ContextId - Context Identifier 
 *
 * OUTPUT           : pu1Alias - Context Name
 * 
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 * 
 **************************************************************************/
INT4
MplsPortVcmGetAliasName (UINT4 u4ContextId, UINT1 *pu1Alias)
{
    INT4                i4RetVal = VCM_FAILURE;

    i4RetVal = VcmGetAliasName (u4ContextId, pu1Alias);

    return (((i4RetVal == VCM_FAILURE) ? OSIX_FAILURE : OSIX_SUCCESS));
}

/****************************************************************************
* Function    : MplsPortIsVcmSwitchExist
* Description : This function check whether the entry is present   
*               for the correponding Switch-name in VCM.           
*               if yes it will return the Context-Id of the Switch.
* Input       : pu1Alias   - Name of the Switch.
* Output      : pu4VcNum   - Context-Id of the switch
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
MplsPortIsVcmSwitchExist (UINT1 *pu1Alias, UINT4 *pu4VcNum)
{
    if (MplsPortGetVcmSystemMode (MPLS_PROTOCOL_ID) == VCM_MI_MODE)
    {
        if (VcmIsVrfExist (pu1Alias, pu4VcNum) == VCM_FALSE)
        {
            return OSIX_FAILURE;
        }
    }
    else
    {
        if (STRCMP (pu1Alias, "default") == 0)
        {
            *pu4VcNum = MPLS_DEFAULT_CXT_ID;
        }
        else
        {
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : MplsPortGetVcmSystemMode
* Description : This function calls the VCM Module to get the
*               mode of the system (SI / MI).      
* Input       : u2ProtocolId - Protocol Identifier
* Output      : pu1Alias       - Switch Alias Name.
* Returns     : VCM_MI_MODE / VCM_SI_MODE
*****************************************************************************/
INT4
MplsPortGetVcmSystemMode (UINT2 u2ProtocolId)
{
    return (VcmGetSystemMode (u2ProtocolId));
}

/*****************************************************************************/
/* Function                  : MplsPortFmNotifyFaults                        */
/*                                                                           */
/* Description               : This function Sends the trap message to the   */
/*                             Fault Manager.                                */
/*                                                                           */
/* Input(s)                  : pTrapMsg     - SNMP Trap Message              */
/*                             pc1SysLogMsg - Syslog Message                 */
/*                                                                           */
/* Output                    : None                                          */
/*                                                                           */
/* Returns                   : None.                                         */
/*                                                                           */
/*****************************************************************************/
VOID
MplsPortFmNotifyFaults (tSNMP_VAR_BIND * pTrapMsg, CHR1 * pc1SysLogMsg)
{
#ifdef FM_WANTED
    tFmFaultMsg         FmFaultMsg;

    MEMSET (&FmFaultMsg, 0, sizeof (tFmFaultMsg));

    FmFaultMsg.pTrapMsg = pTrapMsg;
    FmFaultMsg.pSyslogMsg = (UINT1 *) pc1SysLogMsg;
    FmFaultMsg.u4ModuleId = FM_NOTIFY_MOD_ID_MPLS;

    if (FmApiNotifyFaults (&FmFaultMsg) == OSIX_FAILURE)
    {
        SNMP_AGT_FreeVarBindList (pTrapMsg);
    }
#else
    SNMP_AGT_FreeVarBindList (pTrapMsg);
    UNUSED_PARAM (pc1SysLogMsg);
#endif
    return;
}

/*****************************************************************************
 *
 *    Function Name      : MplsCreateMplsIfOrMplsTnlIf 
 *
 *    Description        : This function creates the MPLS Interface or MPLS
 *                         Tunnel Interface in ifTable or ifMainTable.
 *
 *                         This function should only be called from management
 *                         task like SNMP, CLI and WEB since it calls nmhSet
 *                         routines to delete the interface.
 *                         This function should not be called from any
 *                         protocol task.
 *
 *                         If the interface to be created is MPLS, it is 
 *                         over L3 VLAN Interface using ifStackTable.
 *
 *                         If the interface to be created is MPLS Tunnel, it is
 *                         stacked over MPLS Interface or HLSP Tunnel interface
 *                   using ifStackTable.
 *
 *                         Created interface index is returned to the caller.
 *
 *    Input(s)           : u4L3IpIntf - L3IPVLAN Interface / HLSP Tunnel interface.
 *                         
 *                         u4IfIndex  - IfIndex created.
 *                         Can be MPLS IfIndex or MPLS Tunnel IfIndex.
 *
 *                         i4IfType   - Interface Type to be created. 
 *                                      MPLS (166) or MPLS TUNNEL (150).
 *
 *    Output(s)          : None.
 *
 *    Returns            : MPLS_SUCCESS or MPLS_FAILURE.
 *
 *****************************************************************************/
INT4
MplsCreateMplsIfOrMplsTnlIf (UINT4 u4L3IpIntf, UINT4 u4IfIndex, INT4 i4IfType)
{
    UINT4               u4ErrCode = 0;
    UINT4               u4LowIfIndex = 0;    /* L3 IfIndex or MPLS IfIndex */
    UINT1               u1IfType = 0;    /* STATIC_HLSP */

    u4LowIfIndex = u4L3IpIntf;

    CFA_LOCK ();

    /* STATIC_HLSP */
    /* The below check is modified to accomodate stacking of tunnel interface 
     * over HLSP tunnel interface. If the lower layer(u4L3IpIntf)  is passed 
     * as HLSP tunnel interface, it is not required to retrieve the 
     * MPLS interface(166) */
    CfaGetIfType (u4L3IpIntf, &u1IfType);
    if ((i4IfType == CFA_MPLS_TUNNEL) && (u1IfType != CFA_MPLS_TUNNEL))
    {
        if (CfaUtilGetMplsIfFromIfIndex (u4L3IpIntf, &u4LowIfIndex, FALSE)
            == CFA_FAILURE)
        {
            CFA_UNLOCK ();
            return MPLS_FAILURE;
        }
    }

    if (nmhSetIfMainRowStatus ((INT4) u4IfIndex,
                               MPLS_STATUS_CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return MPLS_FAILURE;
    }

    if (nmhTestv2IfMainType (&u4ErrCode, (INT4) u4IfIndex,
                             i4IfType) == SNMP_FAILURE)
    {
        if (nmhSetIfMainRowStatus ((INT4) u4IfIndex, MPLS_STATUS_DESTROY)
            == SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            return MPLS_FAILURE;
        }

        CFA_UNLOCK ();
        return MPLS_FAILURE;
    }

    if (nmhSetIfMainType ((INT4) u4IfIndex, i4IfType) == SNMP_FAILURE)
    {
        if (nmhSetIfMainRowStatus ((INT4) u4IfIndex, MPLS_STATUS_DESTROY)
            == SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            return MPLS_FAILURE;
        }

        CFA_UNLOCK ();
        return MPLS_FAILURE;
    }

    if (nmhTestv2IfStackStatus (&u4ErrCode, (INT4) u4IfIndex,
                                (INT4) u4LowIfIndex,
                                MPLS_STATUS_CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        if (nmhSetIfMainRowStatus ((INT4) u4IfIndex, MPLS_STATUS_DESTROY)
            == SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            return MPLS_FAILURE;
        }

        CFA_UNLOCK ();
        return MPLS_FAILURE;
    }

    if (nmhSetIfStackStatus ((INT4) u4IfIndex, (INT4) u4LowIfIndex,
                             MPLS_STATUS_CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        if (nmhSetIfMainRowStatus ((INT4) u4IfIndex, MPLS_STATUS_DESTROY)
            == SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            return MPLS_FAILURE;
        }

        CFA_UNLOCK ();
        return MPLS_FAILURE;
    }

    if (nmhTestv2IfStackStatus (&u4ErrCode, (INT4) u4IfIndex,
                                (INT4) u4LowIfIndex, MPLS_STATUS_ACTIVE)
        == SNMP_FAILURE)
    {
        if (nmhSetIfStackStatus ((INT4) u4IfIndex, (INT4) u4LowIfIndex,
                                 MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            return MPLS_FAILURE;
        }
        if (nmhSetIfMainRowStatus ((INT4) u4IfIndex, MPLS_STATUS_DESTROY)
            == SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            return MPLS_FAILURE;
        }

        CFA_UNLOCK ();
        return MPLS_FAILURE;
    }

    if (nmhSetIfStackStatus ((INT4) u4IfIndex, (INT4) u4LowIfIndex,
                             MPLS_STATUS_ACTIVE) == SNMP_FAILURE)
    {
        if (nmhSetIfStackStatus ((INT4) u4IfIndex, (INT4) u4LowIfIndex,
                                 MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            return MPLS_FAILURE;
        }
        if (nmhSetIfMainRowStatus ((INT4) u4IfIndex, MPLS_STATUS_DESTROY)
            == SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            return MPLS_FAILURE;
        }

        CFA_UNLOCK ();
        return MPLS_FAILURE;
    }

    if (nmhTestv2IfMainRowStatus (&u4ErrCode, (INT4) u4IfIndex,
                                  MPLS_STATUS_ACTIVE) == SNMP_FAILURE)
    {
        if (nmhSetIfStackStatus ((INT4) u4IfIndex, (INT4) u4LowIfIndex,
                                 MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            return MPLS_FAILURE;
        }
        if (nmhSetIfMainRowStatus ((INT4) u4IfIndex, MPLS_STATUS_DESTROY)
            == SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            return MPLS_FAILURE;
        }

        CFA_UNLOCK ();
        return MPLS_FAILURE;
    }

    if (nmhSetIfMainRowStatus ((INT4) u4IfIndex, MPLS_STATUS_ACTIVE) ==
        SNMP_FAILURE)
    {
        if (nmhSetIfStackStatus ((INT4) u4IfIndex, (INT4) u4LowIfIndex,
                                 MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            return MPLS_FAILURE;
        }
        if (nmhSetIfMainRowStatus ((INT4) u4IfIndex, MPLS_STATUS_DESTROY) ==
            SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            return MPLS_FAILURE;
        }

        CFA_UNLOCK ();
        return MPLS_FAILURE;
    }

    if (nmhTestv2IfMainAdminStatus (&u4ErrCode, (INT4) u4IfIndex, CFA_IF_UP)
        == SNMP_FAILURE)
    {
        if (nmhSetIfStackStatus ((INT4) u4IfIndex, (INT4) u4LowIfIndex,
                                 MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            return MPLS_FAILURE;
        }
        if (nmhSetIfMainRowStatus ((INT4) u4IfIndex, MPLS_STATUS_DESTROY) ==
            SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            return MPLS_FAILURE;
        }

        CFA_UNLOCK ();
        return MPLS_FAILURE;
    }

    if (nmhSetIfMainAdminStatus (u4IfIndex, CFA_IF_UP) == SNMP_FAILURE)
    {
        if (nmhSetIfStackStatus ((INT4) u4IfIndex, (INT4) u4LowIfIndex,
                                 MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            return MPLS_FAILURE;
        }
        if (nmhSetIfMainRowStatus ((INT4) u4IfIndex, MPLS_STATUS_DESTROY) ==
            SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            return MPLS_FAILURE;
        }

        CFA_UNLOCK ();
        return MPLS_FAILURE;
    }

    CFA_UNLOCK ();
    return MPLS_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : MplsDeleteMplsIfOrMplsTnlIf 
 *
 *    Description        : This function deletes the MPLS Interface or MPLS
 *                         Tunnel Interface in ifTable or ifMainTable. 
 *
 *                         This function should only be called from management
 *                         task like SNMP, CLI and WEB since it calls nmhSet
 *                         routines to delete the interface.
 *                         This function should not be called from any
 *                         protocol task.
 *
 *                         If the interface to be deleted is MPLS Interface,
 *                         this function validates whether any interface are
 *                         stacked over it, if so returns failure, else
 *                         stacking of MPLS Interface over L3 VLAN is removed 
 *                         and MPLS Interface is deleted.
 *
 *                         If the interface to be deleted is MPLS Tunnel
 *                         Interface, MPLS Interface is retrieved from L3 IP 
 *                         Interface, stacking of MPLS Tunnel interface over
 *                         MPLS Interface is removed and MPLS Tunnel interface 
 *                         is deleted.
 *
 *    Input(s)           : u4L3IpIntf - Interface Index of L3 IP VLAN.
 *
 *                         u4IfIndex  - Interface Index to be deleted.
 *                                      Can be MPLS Interface or MPLS Tunnel
 *                                      Interface.
 *                         
 *                         i4IfType   - Type of the interface to be delted.
 *                                      Can be MPLS or MPLS Tunnel.
 *
 *                         u1Flag     - If True, Tunnel Entry in IfMainTable
 *                                      is deleted.
 *                                      If False, only stacking is removed.
 *
 *    Output(s)          : None
 *
 *    Returns            : MPLS_SUCCESS or MPLS_FAILURE.
 *
 *****************************************************************************/
INT4
MplsDeleteMplsIfOrMplsTnlIf (UINT4 u4L3IpIntf, UINT4 u4IfIndex, INT4 i4IfType,
                             BOOL1 bFlag)
{
    UINT4               u4ErrCode = 0;
    UINT4               u4LowIfIndex = u4L3IpIntf;
    UINT1               u1IfType = 0;    /* STATIC_HLSP */
    CFA_LOCK ();

    if (bFlag == MPLS_TRUE)
    {
        if (nmhTestv2IfMainRowStatus (&u4ErrCode, (INT4) u4IfIndex,
                                      MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            return MPLS_FAILURE;
        }

        if (nmhTestv2IfMainAdminStatus
            (&u4ErrCode, (INT4) u4IfIndex, CFA_IF_DOWN) == SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            return MPLS_FAILURE;
        }

        if (nmhSetIfMainAdminStatus (u4IfIndex, CFA_IF_DOWN) == SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            return MPLS_FAILURE;
        }
    }
    /* STATIC_HLSP */
    /* The below check is modified to accomodate stacking of tunnel interface over HLSP tunnel interface */
    /* If the lower layer(u4L3IpIntf)  is passed as HLSP tunnel interface, it is not required to retrieve the 
       MPLS interface(166) */
    CfaGetIfType (u4L3IpIntf, &u1IfType);
    if ((i4IfType == CFA_MPLS_TUNNEL) && (u1IfType != CFA_MPLS_TUNNEL))
    {
        if (CfaUtilGetMplsIfFromIfIndex (u4L3IpIntf, &u4LowIfIndex, FALSE)
            == CFA_FAILURE)
        {
            CFA_UNLOCK ();
            return MPLS_FAILURE;
        }
    }

    if (nmhTestv2IfStackStatus (&u4ErrCode, (INT4) u4IfIndex,
                                (INT4) u4LowIfIndex,
                                MPLS_STATUS_DESTROY) == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return MPLS_FAILURE;
    }

    if (nmhSetIfStackStatus ((INT4) u4IfIndex, (INT4) u4LowIfIndex,
                             MPLS_STATUS_DESTROY) == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return MPLS_FAILURE;
    }

    if (bFlag == MPLS_TRUE)
    {
        if (nmhSetIfMainRowStatus (u4IfIndex, MPLS_STATUS_DESTROY) ==
            SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            return MPLS_FAILURE;
        }
    }

    CFA_UNLOCK ();
    return MPLS_SUCCESS;
}

/*****************************************************************************
 *    Function Name      : MplsGetPhyPortFromLogicalIfIndex 
 *
 *    Description        : This function fetches the physical port for the 
 *                         corresponding logical interface index. 
 *                         
 *                         The logical ifIndex could be any one of the 
 *                         following types:
 *
 *                         1. L3 VLAN ifIndex
 *                         2. Router port ifIndex
 *                         3. PW ifIndex
 *
 *    Input(s)           : u4LogIfIndex - L3VLAN, Router port IfIndex, 
 *                                        PW IfIndex.
 *                         pu1MacAddr - next hop Mac address for this interface.
 *
 *    Output(s)          : pu4PhyIfIndex - the underlying physical port
 *
 *    Returns            : MPLS_SUCCESS or MPLS_FAILURE.
 *
 *****************************************************************************/
INT4
MplsGetPhyPortFromLogicalIfIndex (UINT4 u4LogIfIndex,
                                  UINT1 *pu1MacAddr, UINT4 *pu4PhyIfIndex)
{
    tPortList          *pTagPortList = NULL;
    tPortList          *pUntagPortList = NULL;
    tPortList          *pEgressPortList = NULL;
    tCfaIfInfo          IfInfo;
    BOOL1               bIsTag = FALSE;
    UINT4               u4PhyIfIndex = 0;
    BOOL1               bResult = 0;

    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

    if (CfaGetIfInfo (u4LogIfIndex, &IfInfo) != CFA_SUCCESS)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsGetPhyPortFromLogicalIfIndex: "
                    "Fetching Interface Info from CFA failed. \n");
        return MPLS_FAILURE;
    }

    switch (IfInfo.u1IfType)
    {
        case CFA_L3IPVLAN:
        {
            pTagPortList =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pTagPortList == NULL)
            {
                MPLSFM_DBG (MPLSFM_IF_FAILURE,
                            "MplsGetPhyPortFromLogicalIfIndex: "
                            "Error in allocating memory for pTagPortList\r\n");

                return MPLS_FAILURE;
            }
            pUntagPortList =
                (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

            if (pUntagPortList == NULL)
            {
                FsUtilReleaseBitList ((UINT1 *) pTagPortList);
                MPLSFM_DBG (MPLSFM_IF_FAILURE,
                            "MplsGetPhyPortFromLogicalIfIndex: "
                            "Error in allocating memory for pUntagPortList\r\n");

                return MPLS_FAILURE;
            }
            MEMSET (pTagPortList, 0, sizeof (tPortList));
            MEMSET (pUntagPortList, 0, sizeof (tPortList));
            /* Fetch the corresponding out physical ifIndex associated to 
             * the peer MAC.*/
            if (VlanIvrGetTxPortOrPortList (pu1MacAddr,
                                            IfInfo.u2VlanId,
                                            CFA_TRUE,
                                            pu4PhyIfIndex,
                                            &bIsTag,
                                            *pTagPortList,
                                            *pUntagPortList) == VLAN_FORWARD)
            {
                if (*pu4PhyIfIndex != VLAN_INVALID_PORT)
                {
                    FsUtilReleaseBitList ((UINT1 *) pTagPortList);
                    FsUtilReleaseBitList ((UINT1 *) pUntagPortList);
                    return MPLS_SUCCESS;
                }
                else
                {
                    /* Get the first port in the VLAN associated to L3 interface when 
                       there is no entry exists in FDB table for the given MAC to get the 
                       physical port. It is assumed that the VLAN has only one port. */

                    pEgressPortList =
                        (tPortList *) FsUtilAllocBitList (sizeof (tPortList));

                    if (pEgressPortList == NULL)
                    {
                        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                                    "MplsGetPhyPortFromLogicalIfIndex: "
                                    "Error in allocating memory for pEgressPortList\r\n");

                        FsUtilReleaseBitList ((UINT1 *) pTagPortList);
                        FsUtilReleaseBitList ((UINT1 *) pUntagPortList);
                        return MPLS_FAILURE;
                    }
                    MEMSET (*pEgressPortList, 0, sizeof (tPortList));

                    VlanGetVlanMemberPorts (IfInfo.u2VlanId, *pEgressPortList,
                                            *pUntagPortList);

                    for (u4PhyIfIndex = 1;
                         u4PhyIfIndex <= BRG_NUM_PHY_PLUS_LOG_PORTS;
                         u4PhyIfIndex++)
                    {
                        OSIX_BITLIST_IS_BIT_SET ((*pEgressPortList),
                                                 u4PhyIfIndex,
                                                 BRG_PORT_LIST_SIZE, bResult);

                        if (OSIX_TRUE == bResult)
                        {
                            *pu4PhyIfIndex = u4PhyIfIndex;
                            FsUtilReleaseBitList ((UINT1 *) pTagPortList);
                            FsUtilReleaseBitList ((UINT1 *) pUntagPortList);
                            FsUtilReleaseBitList ((UINT1 *) pEgressPortList);
                            return MPLS_SUCCESS;
                        }
                    }
                    FsUtilReleaseBitList ((UINT1 *) pEgressPortList);
                }
            }
            FsUtilReleaseBitList ((UINT1 *) pTagPortList);
            FsUtilReleaseBitList ((UINT1 *) pUntagPortList);

            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsGetPhyPortFromLogicalIfIndex: "
                        "Unable to get the physical port from the VLAN and VLAN+"
                        "NextHopMac \n");
        }
            break;

        case CFA_ENET:
        {
            /* The physical ifIndex for the Router port will be the same. */
            if (IfInfo.u1BridgedIface == CFA_DISABLED)
            {
                *pu4PhyIfIndex = u4LogIfIndex;
                return MPLS_SUCCESS;
            }
            else
            {
                MPLSFM_DBG (MPLSFM_IF_FAILURE,
                            "MplsGetPhyPortFromLogicalIfIndex: "
                            "Invalid interface. \n");
            }
        }
            break;

        default:
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsGetPhyPortFromLogicalIfIndex: "
                        "Invalid interface type. \n");
    }

    return MPLS_FAILURE;
}

/******************************************************************************
 *  Function Name : MplsPortTlmNotifyDiffServParams
 *  Description   : This routine updated the Diff-serv parameters 
 *  Input(s)      : None
 *  Output(s)     : None
 *  Return(s)     : None
 * ******************************************************************************/
VOID
MplsPortTlmNotifyDiffServParams (VOID)
{
    tTlmDiffservInfo    TlmDiffServInfo;
    tMplsDsTeClassType  ClassType;
    tMplsDsTeClassPrio  TeClass;

    UINT1               u1ClassIndex = MPLS_ZERO;
    UINT1               u1ClassTeIndex = MPLS_ZERO;

    MEMSET (&TlmDiffServInfo, MPLS_ZERO, sizeof (tTlmDiffservInfo));
    MEMSET (&ClassType, MPLS_ZERO, sizeof (tMplsDsTeClassType));
    MEMSET (&TeClass, MPLS_ZERO, sizeof (tMplsDsTeClassPrio));

    /*Updating Class type entries */
    for (u1ClassIndex = MPLS_ZERO; u1ClassIndex < MPLS_MAX_CLASS_TYPE_ENTRIES;
         u1ClassIndex++)
    {
        ClassType = MPLS_DIFFSERV_CLASS_TYPE_ENTRY (MPLS_DEF_INCARN,
                                                    u1ClassIndex);
        if (ClassType.i4DsTeClassTypeRowStatus == ACTIVE)
        {
            TlmDiffServInfo.aTlmClassType[u1ClassIndex].u4BwPercentage =
                (UINT4) ClassType.i4DsTeClassTypeBwPercent;
        }
    }

    /*Updating TE Class map entries */
    for (u1ClassIndex = MPLS_ZERO; u1ClassIndex < MPLS_MAX_CLASS_TYPE_ENTRIES;
         u1ClassIndex++)
    {
        for (u1ClassTeIndex = MPLS_ZERO; u1ClassTeIndex <
             MPLS_MAX_TE_CLASS_ENTRIES; u1ClassTeIndex++)
        {
            TeClass = MPLS_DIFFSERV_TE_CLASS_MAP (MPLS_DEF_INCARN, u1ClassIndex,
                                                  u1ClassTeIndex);

            if (TeClass.i4DsTeCRowStatus == ACTIVE)
            {
                TlmDiffServInfo.aTlmTeClassMap[TeClass.u4TeClassNumber].
                    u1ClassType = u1ClassIndex;
                TlmDiffServInfo.aTlmTeClassMap[TeClass.u4TeClassNumber].
                    u1PremptPrio = u1ClassTeIndex;
                TlmDiffServInfo.aTlmTeClassMap[TeClass.u4TeClassNumber].
                    u1IsSet = TRUE;
            }
        }
    }
#ifdef TLM_WANTED
    /*Update the info to TLM */
    TlmApiSetDiffservParams (&TlmDiffServInfo);
#endif
}

/*****************************************************************************
 *
 *    Function Name      : MplsStackMplsIfOrMplsTnlIf
 *
 *    Description        : This function does the stacking of MPLS Tunnel  
 *                         Interface over MPLS Interface                 
 *
 *    Input(s)           : u4L3IpIntf - Interface Index of L3 IP VLAN
 *                                         
 *                         u4IfIndex  - Interface Index to be stacked over.
 *                                      Can be MPLS Interface or MPLS Tunnel
 *                                      Interface.
 *                         i4IfType   - Type of the interface to be stacked
 *                                      Can be MPLS or MPLS Tunnel.
 *
 *    Output(s)          : None
 *
 *    Returns            : MPLS_SUCCESS or MPLS_FAILURE.
 *
 *****************************************************************************/
INT4
MplsStackMplsIfOrMplsTnlIf (UINT4 u4L3IpIntf, UINT4 u4IfIndex, INT4 i4IfType)
{
    UINT4               u4ErrCode = 0;
    UINT4               u4LowIfIndex = 0;    /* L3 IfIndex or MPLS IfIndex */
    UINT1               u1IfType = 0;    /* STATIC_HLSP */

    u4LowIfIndex = u4L3IpIntf;

    CFA_LOCK ();

    CfaGetIfType (u4L3IpIntf, &u1IfType);
    if ((i4IfType == CFA_MPLS_TUNNEL) && (u1IfType != CFA_MPLS_TUNNEL))
    {
        if (CfaUtilGetMplsIfFromIfIndex (u4L3IpIntf, &u4LowIfIndex, FALSE)
            == CFA_FAILURE)
        {
            CFA_UNLOCK ();
            return MPLS_FAILURE;
        }
    }

    if (nmhTestv2IfMainAdminStatus (&u4ErrCode, (INT4) u4IfIndex, CFA_IF_DOWN)
        == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return MPLS_FAILURE;
    }

    if (nmhSetIfMainAdminStatus (u4IfIndex, CFA_IF_DOWN) == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return MPLS_FAILURE;
    }

    if (nmhTestv2IfStackStatus (&u4ErrCode, (INT4) u4IfIndex,
                                (INT4) u4LowIfIndex,
                                MPLS_STATUS_CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return MPLS_FAILURE;
    }

    if (nmhSetIfStackStatus ((INT4) u4IfIndex, (INT4) u4LowIfIndex,
                             MPLS_STATUS_CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return MPLS_FAILURE;
    }

    if (nmhTestv2IfStackStatus (&u4ErrCode, (INT4) u4IfIndex,
                                (INT4) u4LowIfIndex, MPLS_STATUS_ACTIVE)
        == SNMP_FAILURE)
    {
        if (nmhSetIfStackStatus ((INT4) u4IfIndex, (INT4) u4LowIfIndex,
                                 MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            return MPLS_FAILURE;
        }
        CFA_UNLOCK ();
        return MPLS_FAILURE;
    }

    if (nmhSetIfStackStatus ((INT4) u4IfIndex, (INT4) u4LowIfIndex,
                             MPLS_STATUS_ACTIVE) == SNMP_FAILURE)
    {
        if (nmhSetIfStackStatus ((INT4) u4IfIndex, (INT4) u4LowIfIndex,
                                 MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            return MPLS_FAILURE;
        }
        CFA_UNLOCK ();
        return MPLS_FAILURE;
    }

    if (nmhTestv2IfMainAdminStatus (&u4ErrCode, u4IfIndex, (UINT4) CFA_IF_UP)
        == SNMP_FAILURE)
    {
        if (u4LowIfIndex != MPLS_ZERO)
        {
            if (nmhSetIfStackStatus ((INT4) u4IfIndex, (INT4) u4LowIfIndex,
                                     MPLS_STATUS_DESTROY) == SNMP_FAILURE)
            {
                CFA_UNLOCK ();
                return MPLS_FAILURE;
            }
        }
        CFA_UNLOCK ();
        return MPLS_FAILURE;
    }

    if (nmhSetIfMainAdminStatus (u4IfIndex, CFA_IF_UP) == SNMP_FAILURE)
    {
        if (u4LowIfIndex != MPLS_ZERO)
        {
            if (nmhSetIfStackStatus ((INT4) u4IfIndex, (INT4) u4LowIfIndex,
                                     MPLS_STATUS_DESTROY) == SNMP_FAILURE)
            {
                CFA_UNLOCK ();
                return MPLS_FAILURE;
            }
        }
        CFA_UNLOCK ();
        return MPLS_FAILURE;
    }

    CFA_UNLOCK ();
    return MPLS_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : MplsCreateMplsTnlIf 
 *
 *    Description        : This function creates the MPLS Tunnel Interface
 *                         ifTable or ifMainTable.
 *
 *    Input(s)           : u4IfIndex  - Mpls Tunnel IfIndex 
 *
 *    Output(s)          : None.
 *
 *    Returns            : MPLS_SUCCESS or MPLS_FAILURE.
 *
 *****************************************************************************/
INT4
MplsCreateMplsTnlIf (UINT4 u4MplsTunnelId, UINT4 u4IfIndex)
{

    UINT4               u4ErrCode = MPLS_ZERO;
    CHR1                ac1TunnelName[SNMP_MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE TunnelName;
    CFA_LOCK ();

    if (nmhSetIfMainRowStatus ((INT4) u4IfIndex,
                               MPLS_STATUS_CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return MPLS_FAILURE;
    }

    /* Set the ifAlias with the name which will be set to mplsTunnelName */
    MEMSET (&TunnelName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    MEMSET (&ac1TunnelName, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    SNPRINTF (ac1TunnelName, SNMP_MAX_OCTETSTRING_SIZE, "%s%d",
              "mplstunnel", u4MplsTunnelId);
    TunnelName.pu1_OctetList = (UINT1 *) ac1TunnelName;
    TunnelName.i4_Length = STRLEN (ac1TunnelName);

    if (nmhTestv2IfAlias (&u4ErrCode, (INT4) u4IfIndex,
                          &TunnelName) == SNMP_FAILURE)
    {
        if (nmhSetIfMainRowStatus ((INT4) u4IfIndex, MPLS_STATUS_DESTROY) ==
            SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            return MPLS_FAILURE;
        }
        CFA_UNLOCK ();
        return MPLS_FAILURE;
    }
    if (nmhSetIfAlias ((INT4) u4IfIndex, &TunnelName) == SNMP_FAILURE)
    {
        if (nmhSetIfMainRowStatus ((INT4) u4IfIndex, MPLS_STATUS_DESTROY) ==
            SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            return MPLS_FAILURE;
        }
        CFA_UNLOCK ();
        return MPLS_FAILURE;
    }

    if (nmhTestv2IfMainType (&u4ErrCode, (INT4) u4IfIndex,
                             CFA_MPLS_TUNNEL) == SNMP_FAILURE)
    {
        if (nmhSetIfMainRowStatus ((INT4) u4IfIndex, MPLS_STATUS_DESTROY) ==
            SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            return MPLS_FAILURE;
        }

        CFA_UNLOCK ();
        return MPLS_FAILURE;
    }

    if (nmhSetIfMainType ((INT4) u4IfIndex, CFA_MPLS_TUNNEL) == SNMP_FAILURE)
    {
        if (nmhSetIfMainRowStatus ((INT4) u4IfIndex, MPLS_STATUS_DESTROY) ==
            SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            return MPLS_FAILURE;
        }

        CFA_UNLOCK ();
        return MPLS_FAILURE;
    }

    if (nmhTestv2IfMainRowStatus (&u4ErrCode, (INT4) u4IfIndex,
                                  MPLS_STATUS_ACTIVE) == SNMP_FAILURE)
    {
        if (nmhSetIfMainRowStatus ((INT4) u4IfIndex, MPLS_STATUS_DESTROY) ==
            SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            return MPLS_FAILURE;
        }

        CFA_UNLOCK ();
        return MPLS_FAILURE;
    }

    if (nmhSetIfMainRowStatus (u4IfIndex, MPLS_STATUS_ACTIVE) == SNMP_FAILURE)
    {
        if (nmhSetIfMainRowStatus ((INT4) u4IfIndex, MPLS_STATUS_DESTROY) ==
            SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            return MPLS_FAILURE;
        }

        CFA_UNLOCK ();
        return MPLS_FAILURE;
    }

    if (nmhTestv2IfMainAdminStatus (&u4ErrCode, (INT4) u4IfIndex, CFA_IF_UP)
        == SNMP_FAILURE)
    {
        if (nmhSetIfMainRowStatus ((INT4) u4IfIndex, MPLS_STATUS_DESTROY) ==
            SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            return MPLS_FAILURE;
        }
        CFA_UNLOCK ();
        return MPLS_FAILURE;
    }

    if (nmhSetIfMainAdminStatus (u4IfIndex, CFA_IF_UP) == SNMP_FAILURE)
    {
        if (nmhSetIfMainRowStatus ((INT4) u4IfIndex, MPLS_STATUS_DESTROY) ==
            SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            return MPLS_FAILURE;
        }

        CFA_UNLOCK ();
        return MPLS_FAILURE;
    }

    CFA_UNLOCK ();
    return MPLS_SUCCESS;
}
