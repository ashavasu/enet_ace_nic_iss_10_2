/*---------------------------------------------------------------------------*/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mplsutil.c,v 1.63 2016/10/20 09:19:52 siva Exp $
 *
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : mplsutil.c
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : MPLS
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  : 20-JUN-2000
 *    DESCRIPTION            : This file contains utility routines that is
 *                             used by mpls modules.
 *---------------------------------------------------------------------------*/

#include "mplsrtr.h"
#include "mplsutil.h"
#include "mplsincs.h"
#include "ldpext.h"
#include "rpteext.h"
#include "teincs.h"
#include "mplsftn.h"

#ifdef MPLS_L3VPN_WANTED
#include "mpl3vpncmn.h"
#endif

PRIVATE VOID        MplsRouteChgHandleFtnAdd (tMplsRtEntryInfo * pRouteInfo);
PRIVATE VOID        MplsUpdateXcOperStatus (UINT4  u4IfIndex);

#ifdef MPLS_IPV6_WANTED
PRIVATE VOID        MplsIpv6RouteChgHandleFtnAdd (tMplsRtEntryInfo * pRouteInfo);
#endif

extern VOID L2VpnIfEventHandler ARG_LIST ((UINT4 u4IfIndex, UINT2 u2PortVlan,
                                           UINT1 u1Status));
extern INT4         MplsOamIfGetOamSessionId (UINT4 u4MegIndex, UINT4 u4MeIndex,
                                              UINT4 u4MpIndex,
                                              UINT4 *pu4OamSessionId);
#ifdef HVPLS_WANTED
extern INT4 L2VpnGetPwIndexFromPwIfIndex (UINT4 u4PwIfIndex, UINT4 *pu4PwIndex);
#endif

/*****************************************************************************/
/* Function Name : HASHInsertInOrder
 * Description   : This routine inserts a given hash node into the hash table.
 *                 Each bucket in the list is maintained in ascending order
 *                 and the insertion is done such that the ascending order is
 *                 maintained.
 *                 The ascending order calculation is based on certain
 *                 fields present in the structure.
 *
 * Input(s)      : au2Offset    - Array containing the byte offset of the
 *                                members of the structure, in the order in
 *                                which they form the indices
 *                 au2Length    - Array containing the length in bytes of the
 *                                members of the structure, in the order in
 *                                which they form the indices
 *                 au1ByteOrder - Array containing the order in which the
 *                                member of the structure is stored.
 *                                For example, if a member is UINT4, the way
 *                                it is stored depends on the endianess. So a
 *                                value HOST_ORDER is passed in this array
 *                                corresponding to the member.
 *                                But say for an array of characters, the
 *                                value passed should be NET_ORDER.
 *                 u2ArrLength  - Contains the number of members of the
 *                                structure that form the index. This
 *                                specifies the length of the above two arrays
 *                 pTable       - Pointer to the hash table to which the
 *                                given node needs to be added
 *                 pNode        - Pointer to the node that contains the value
 *                                of the indices for which the search is
 *                                taking place
 *                                NOTE : pNode points to the starting memory
 *                                location of the structure variable
 *                 u4HashIndex  - Contains the value of the hash index
 *                 u2NodeOffset - Contains the byte offset of the
 *                                tTMO_HASH_NODE member of the structure using
 *                                which the list is made
 * Output(s)     : None
 * Return(s)     : MPLS_SUCCESS/MPLS_FAILURE
 *****************************************************************************/
UINT1
HASHInsertInOrder (UINT2 au2Offset[], UINT2 au2Length[], UINT1 au1ByteOrder[],
                   UINT2 u2ArrLength, tTMO_HASH_TABLE * pTable, VOID *pNode,
                   UINT4 u4HashIndex, UINT2 u2NodeOffset)
{
    tTMO_SLL           *pList = NULL;

    pList = (tTMO_SLL *) & (pTable->HashList[u4HashIndex]);

    if (SLLInsertInOrder (au2Offset, au2Length, au1ByteOrder, u2ArrLength,
                          pList, pNode, u2NodeOffset) == MPLS_SUCCESS)
    {
        return MPLS_SUCCESS;
    }
    else
    {
        return MPLS_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name : HASHSearch
 * Description   : This routine searches for a node in the given Hash Table
 *                 with the specified indices value. Since each bucket is
 *                 maintained in ascending order the routine returns
 *                 failure, when a value greater than the search value is
 *                 encountered while traversing.
 *
 * Input(s)      : au2Offset    - Array containing the byte offset of the
 *                                members of the structure, in the order in
 *                                which they form the indices
 *                 au2Length    - Array containing the length in bytes of the
 *                                members of the structure, in the order in
 *                                which they form the indices
 *                 au1ByteOrder - Array containing the order in which the
 *                                member of the structure is stored.
 *                                For example, if a member is UINT4, the way
 *                                it is stored depends on the endianess. So a
 *                                value HOST_ORDER is passed in this array
 *                                corresponding to the member.
 *                                But say for an array of characters, the
 *                                value passed should be NET_ORDER.
 *                 u2ArrLength  - Contains the number of members of the
 *                                structure that form the index. This
 *                                specifies the length of the above two arrays
 *                 pTable       - Pointer to the hash table to which the
 *                                given node needs to be added
 *                 pNode        - Pointer to the node that is to be added to
 *                                the table
 *                                NOTE : pNode points to the starting memory
 *                                location of the structure variable
 *                 u4HashIndex  - Contains the value of the hash index
 *                 u2NodeOffset - Contains the byte offset of the
 *                                tTMO_HASH_NODE member of the structure using
 *                                which the list is made
 * Output(s)     : ppNode       - Will lead to the node, if the search is a
 *                                success
 * Return(s)     : MPLS_SUCCESS/MPLS_FAILURE
 *****************************************************************************/
UINT1
HASHSearch (UINT2 au2Offset[], UINT2 au2Length[], UINT1 au1ByteOrder[],
            UINT2 u2ArrLength, tTMO_HASH_TABLE * pTable, VOID *pNode,
            UINT4 u4HashIndex, UINT2 u2NodeOffset, VOID **ppNode)
{
    tTMO_SLL           *pList = NULL;

    pList = (tTMO_SLL *) & (pTable->HashList[u4HashIndex]);

    if (SLLSearch (au2Offset, au2Length, au1ByteOrder, u2ArrLength, pList,
                   pNode, u2NodeOffset, ppNode) == MPLS_SUCCESS)
    {
        return MPLS_SUCCESS;
    }
    else
    {
        return MPLS_FAILURE;
    }
}

/*****************************************************************************/
/* Function Name : SLLInsertInOrder
 * Description   : This routine inserts a given node into the given list.
 *                 The list is maintained in ascending order and the
 *                 insertion is done such that the ascending order is
 *                 maintained.
 *                 The ascending order calculation is based on certain
 *                 fields present in the structure.
 *
 * Input(s)      : au2Offset    - Array containing the byte offset of the
 *                                members of the structure, in the order in
 *                                which they form the indices
 *                 au2Length    - Array containing the length in bytes of the
 *                                members of the structure, in the order in
 *                                which they form the indices
 *                 au1ByteOrder - Array containing the order in which the
 *                                member of the structure is stored.
 *                                For example, if a member is UINT4, the way
 *                                it is stored depends on the endianess. So a
 *                                value HOST_ORDER is passed in this array
 *                                corresponding to the member.
 *                                But say for an array of characters, the
 *                                value passed should be NET_ORDER.
 *                 u2ArrLength  - Contains the number of members of the
 *                                structure that form the index. This
 *                                specifies the length of the above two arrays
 *                 pList        - Pointer to the sorted list to which the
 *                                given node needs to be added
 *                 pNode        - Pointer to the node that is to be added to
 *                                the list
 *                                NOTE : pNode points to the starting memory
 *                                location of the structure variable
 *                 u2NodeOffset - Contains the byte offset of the
 *                                tTMO_SLL_NODE member of the structure using
 *                                which the list is made
 * Output(s)     : None
 * Return(s)     : MPLS_SUCCESS/MPLS_FAILURE
 *****************************************************************************/
UINT1
SLLInsertInOrder (UINT2 au2Offset[], UINT2 au2Length[], UINT1 au1ByteOrder[],
                  UINT2 u2ArrLength, tTMO_SLL * pList, VOID *pNode,
                  UINT2 u2NodeOffset)
{
    UINT1               u1PosFound = MPLS_FALSE;
    UINT1               u1SameIndex = MPLS_TRUE;
    UINT2               u2Index;

    UINT1              *pu1PrevNode = NULL;
    UINT1              *pu1NextNode = NULL;
    UINT1              *pu1TmpNextNode = NULL;
    UINT1              *pu1Node = NULL;
    tTMO_SLL_NODE      *pPrevNode = NULL;
    tTMO_SLL_NODE      *pNextNode = NULL;
    tTMO_SLL_NODE      *pNodeToBeIns = NULL;

    if ((pList == NULL) || (pNode == NULL) || (u2ArrLength == MPLS_ZERO))
    {
        return MPLS_FAILURE;
    }

    /* Conventions : 
     * 1. pointers with name containing "PrevNode" or "NextNode" are 
     *    referring to nodes in List
     * 2. other pointers with name containing "Node" refers to the node
     *    that is to be inserted
     * 3. The "Node" will be inserted between "PrevNode" and "NextNode"
     */

    /* Getting the Node pointers into UINT1 pointers will be useful 
     * when need to offset the pointer by the given number of bytes */
    pu1Node = (UINT1 *) pNode;
    pNodeToBeIns = (tTMO_SLL_NODE *) (VOID *) (pu1Node + u2NodeOffset);

    pNextNode = TMO_SLL_First (pList);

    /* If the List is empty, the node is added as the first node */
    if (pNextNode == NULL)
    {
        TMO_SLL_Add (pList, pNodeToBeIns);
        return MPLS_SUCCESS;
    }

    TMO_SLL_Scan (pList, pu1TmpNextNode, UINT1 *)
    {
        pu1NextNode = (pu1TmpNextNode - u2NodeOffset);

        /* Preparing for countering Node containing duplicate Indices */
        u1SameIndex = MPLS_TRUE;

        for (u2Index = MPLS_ZERO; u2Index < u2ArrLength; u2Index++)
        {
            if (SLL_MEMCMP (pu1NextNode + au2Offset[u2Index],
                            pu1Node + au2Offset[u2Index],
                            au2Length[u2Index], au1ByteOrder[u2Index])
                < MPLS_ZERO)
            {
                /* When "NextNode" index value is less than 
                 * "Node" index value, the next node in the list 
                 * can be processed */
                u1SameIndex = MPLS_FALSE;
                break;
            }
            else if (SLL_MEMCMP (pu1NextNode + au2Offset[u2Index],
                                 pu1Node + au2Offset[u2Index],
                                 au2Length[u2Index],
                                 au1ByteOrder[u2Index]) > MPLS_ZERO)
            {
                /* When "NextNode" index value is greater than
                 * "Node" index value, the position is found */
                u1PosFound = MPLS_TRUE;
                u1SameIndex = MPLS_FALSE;
                break;
            }
        }

        if (u1PosFound == MPLS_TRUE)
        {
            /* Position found, break from the loop scanning the list */
            break;
        }

        if (u1SameIndex == MPLS_TRUE)
        {
            /* Since the indices of the "NextNode" and the "Node"
             * are same return failure */
            return MPLS_FAILURE;
        }
        pu1PrevNode = pu1NextNode;
    }

    if (pu1PrevNode == NULL)
    {
        /* Inserting "Node" as the first node in the list */
        TMO_SLL_Insert (pList, NULL, pNodeToBeIns);
        return MPLS_SUCCESS;
    }

    pPrevNode = (tTMO_SLL_NODE *) (VOID *) (pu1PrevNode + u2NodeOffset);

    if (pu1TmpNextNode != NULL)
    {
        /* Inserting "Node" in the middle of the list */
        pNextNode = (tTMO_SLL_NODE *) (VOID *) (pu1TmpNextNode);
        TMO_SLL_Insert_In_Middle (pList, pPrevNode, pNodeToBeIns, pNextNode);
    }
    else
    {
        /* Inserting "Node" as the last node in the list */
        TMO_SLL_Add (pList, pNodeToBeIns);
    }
    return MPLS_SUCCESS;
}

/*****************************************************************************/
/* Function Name : SLLSearch
 * Description   : This routine searches for a node in the given linked list
 *                 with the specified indices value. Since the list is
 *                 maintained in ascending order the routine returns
 *                 failure, when a value greater than the search value is
 *                 encountered while traversing.
 *
 * Input(s)      : au2Offset    - Array containing the byte offset of the
 *                                members of the structure, in the order in
 *                                which they form the indices
 *                 au2Length    - Array containing the length in bytes of the
 *                                members of the structure, in the order in
 *                                which they form the indices
 *                 au1ByteOrder - Array containing the order in which the
 *                                member of the structure is stored.
 *                                For example, if a member is UINT4, the way
 *                                it is stored depends on the endianess. So a
 *                                value HOST_ORDER is passed in this array
 *                                corresponding to the member.
 *                                But say for an array of characters, the
 *                                value passed should be NET_ORDER.
 *                 u2ArrLength  - Contains the number of members of the
 *                                structure that form the index. This
 *                                specifies the length of the above two arrays
 *                 pList        - Pointer to the sorted list that is to be
 *                                searched
 *                 pNode        - Pointer to the node that contains the value
 *                                of the indices for which the search is
 *                                taking place
 *                                NOTE : pNode points to the starting memory
 *                                location of the structure variable
 *                 u2NodeOffset - Contains the byte offset of the
 *                                tTMO_SLL_NODE member of the structure using
 *                                which the list is made
 * Output(s)     : ppNode       - Will lead to the node, if the search is a
 *                                success
 * Return(s)     : MPLS_SUCCESS/MPLS_FAILURE
 *****************************************************************************/
UINT1
SLLSearch (UINT2 au2Offset[], UINT2 au2Length[], UINT1 au1ByteOrder[],
           UINT2 u2ArrLength, tTMO_SLL * pList, VOID *pNode,
           UINT2 u2NodeOffset, VOID **ppNode)
{
    UINT1               u1NodeFound = MPLS_TRUE;
    UINT2               u2Index;

    UINT1              *pu1Node = NULL;
    UINT1              *pu1TempNode = NULL;
    UINT1              *pu1ScanNode = NULL;

    if ((pList == NULL) || (pNode == NULL) || (u2ArrLength == MPLS_ZERO))
    {
        return MPLS_FAILURE;
    }

    pu1Node = (UINT1 *) pNode;

    TMO_SLL_Scan (pList, pu1ScanNode, UINT1 *)
    {
        pu1TempNode = (pu1ScanNode - u2NodeOffset);
        u1NodeFound = MPLS_TRUE;

        for (u2Index = MPLS_ZERO; u2Index < u2ArrLength; u2Index++)
        {
            if (SLL_MEMCMP (pu1TempNode + au2Offset[u2Index],
                            pu1Node + au2Offset[u2Index],
                            au2Length[u2Index], au1ByteOrder[u2Index])
                < MPLS_ZERO)
            {
                /* When "TempNode" index value is less than 
                 * "Node" index value, the next node in the list 
                 * can be processed. */
                u1NodeFound = MPLS_FALSE;
                break;
            }
            else if (SLL_MEMCMP (pu1TempNode + au2Offset[u2Index],
                                 pu1Node + au2Offset[u2Index],
                                 au2Length[u2Index],
                                 au1ByteOrder[u2Index]) > MPLS_ZERO)
            {
                /* When "TempNode" index value is greater than
                 * "Node" index value, the search terminates */
                return MPLS_FAILURE;
            }
        }

        if (u1NodeFound == MPLS_TRUE)
        {
            *ppNode = (VOID *) pu1TempNode;
            return MPLS_SUCCESS;
        }
    }
    return MPLS_FAILURE;
}

/*****************************************************************************/
/* Function Name : SLL_MEMCMP
 * Description   : This routine takes in two UINT1 pointers and compares them.
 *                 Comparison is done accordingly depending on whether the
 *                 bytes are stored in network order or host order.
 *
 * Input(s)      : pu1One      - Pointer to the first set of bytes.
 *                 pu1Two      - Pointer to the second set of bytes.
 *                 u2Len       - Number of bytres that needs to be compared.
 *                 u1ByteOrder - The order in which the bytes are stored.
 * Output(s)     : NONE
 * Return(s)     : Value that is returned by function memcmp.
 *****************************************************************************/
INT4
SLL_MEMCMP (UINT1 *pu1One, UINT1 *pu1Two, UINT2 u2Len, UINT1 u1ByteOrder)
{
    UINT2               u2TempVal1;
    UINT2               u2TempVal2;
    UINT4               u4TempVal1;
    UINT4               u4TempVal2;

    if (u1ByteOrder == NET_ORDER)
    {
        return (memcmp (pu1One, pu1Two, u2Len));
    }

    /* In the case of the bytes being stored in Host order,
     * conversion to Network order is done before passing them
     * to memcmp.
     * Right now only lengths 2 and 4 are supported.
     * Any other length value will lead to erroneous results */
    switch (u2Len)
    {
        case MPLS_TWO:
            u2TempVal1 = *(UINT2 *) (VOID *) pu1One;
            u2TempVal2 = *(UINT2 *) (VOID *) pu1Two;
            u2TempVal1 = (OSIX_HTONS (u2TempVal1));
            u2TempVal2 = (OSIX_HTONS (u2TempVal2));
            return (memcmp ((UINT1 *) &u2TempVal1, (UINT1 *) &u2TempVal2,
                            u2Len));

        case MPLS_FOUR:
            u4TempVal1 = *(UINT4 *) (VOID *) pu1One;
            u4TempVal2 = *(UINT4 *) (VOID *) pu1Two;
            u4TempVal1 = (OSIX_HTONL (u4TempVal1));
            u4TempVal2 = (OSIX_HTONL (u4TempVal2));
            return (memcmp ((UINT1 *) &u4TempVal1, (UINT1 *) &u4TempVal2,
                            u2Len));

        default:
            return (memcmp (pu1One, pu1Two, u2Len));
    }
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MplsUtlInetNtoa                                  */
/*                                                                          */
/*    Description        : Minimal implementaion of inet_ntoa.              */
/*                         tMplsUtlInAddr is the equivalent of              */
/*                         'struct inaddr'                                  */
/*                                                                          */
/*    Input(s)           : InAddr - The address in struct in_addr format.   */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : Pointer to CHR1 containing ntoa-ed address.      */
/*                                                                          */
/****************************************************************************/
CHR1               *
MplsUtlInetNtoa (tMplsUtlInAddr InAddr)
{
    static CHR1         ac1Addr[20];
    /* We need 15 bytes to store 255.255.255.255 */

    CHR1               *pcByte;

    pcByte = (CHR1 *) & InAddr;

    (void) SNPRINTF (ac1Addr, sizeof (ac1Addr), "%d.%d.%d.%d",
                     (UINT1) pcByte[0], (UINT1) pcByte[1], (UINT1) pcByte[2],
                     (UINT1) pcByte[3]);

    return (ac1Addr);
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : MplsHandleIfStatusChange                         */
/*                                                                          */
/*    Description        : This function handles the Interface change       */
/*                                                                          */
/*    Input(s)           : u4IfIndex - Interface Index                      */
/*                         u1OperStatus - OperStatus                        */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : None                                             */
/*                                                                          */
/****************************************************************************/
VOID
MplsHandleIfStatusChange (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    tCfaIfInfo          IfInfo;
    UINT4               u4L3Iface = 0;
    UINT4               u4MplsIfIndex = 0;
    UINT4               u4IpAddr = 0;

#ifdef TLM_WANTED
    BOOL1               bTeLinkStackingPresent = FALSE;
    UINT4               u4TeLinkIf = 0;
#endif
#ifdef HVPLS_WANTED
    UINT4 			u4PwIndex = 0;
#endif
    MEMSET (&IfInfo, 0, sizeof (IfInfo));
    if (!((u1OperStatus == CFA_IF_UP) || (u1OperStatus == CFA_IF_DOWN)))
    {
        return;
    }
    if (CfaGetIfInfo (u4IfIndex, &IfInfo) == CFA_FAILURE)
    {
        return;
    }
    if ((IfInfo.u1IfType == CFA_MPLS) || (IfInfo.u1IfType == CFA_L3IPVLAN) ||
        (IfInfo.u1IfType == CFA_L3SUB_INTF) ||
        (IfInfo.u1IfType == CFA_MPLS_TUNNEL) ||
        (IfInfo.u1IfType == CFA_TELINK) ||
        ((IfInfo.u1IfType == CFA_ENET)
         && (IfInfo.u1BridgedIface == CFA_DISABLED)) ||
        (IfInfo.u1IfType == CFA_LOOPBACK))
    {
    if(CFA_IF_UP == u1OperStatus)
    {
        /*This function is added to handle MSR related cases in Static Lsp Binding
		After MSR when SNMP tries to set XC Operstatus UP, it finds CFA interface down, due to which
		XC Operstatus remains LOWER_LAYER_DOWN and labelled packets are dropped and not sent to MPLS*/
	 MPLSFM_DBG2 (MPLSFM_PRCS_PRCS, "MplsHandleIfStatusChange: Calling MplsUpdateXcOperStatus with Interface: %d IfType is: %d \n",
                                    u4IfIndex, IfInfo.u1IfType);
        MplsUpdateXcOperStatus (u4IfIndex);
    }
        if ((IfInfo.u1IfType == CFA_L3IPVLAN) || (IfInfo.u1IfType == CFA_L3SUB_INTF) ||
            ((IfInfo.u1IfType == CFA_ENET)
             && (IfInfo.u1BridgedIface == CFA_DISABLED)) ||
            (IfInfo.u1IfType == CFA_LOOPBACK))
        {
#ifdef CFA_WANTED
#ifdef TLM_WANTED
            u4TeLinkIf = MPLS_ZERO;
            if (CfaApiGetTeLinkIfFromL3If (u4IfIndex, &u4TeLinkIf,
                                           MPLS_TWO, FALSE) == CFA_SUCCESS)
            {
                bTeLinkStackingPresent = TRUE;
            }
            else if (CfaApiGetTeLinkIfFromL3If (u4IfIndex, &u4TeLinkIf,
                                                MPLS_ONE, FALSE) == CFA_SUCCESS)
            {
                bTeLinkStackingPresent = TRUE;
            }
            if (bTeLinkStackingPresent == TRUE)
            {
                return;
            }
#endif
#ifdef MPLS_L3VPN_WANTED
            L3VpnIfStChgEventHandler ((INT4) u4IfIndex, u1OperStatus);
#endif
            if (CfaUtilGetMplsIfFromIfIndex (u4IfIndex, &u4MplsIfIndex, FALSE)
                == CFA_FAILURE)
            {
#ifdef MPLS_LDP_WANTED
                LdpIfStChgEventHandler (u4IfIndex, u1OperStatus);
#endif
                return;
            }
#endif

            u4L3Iface = u4IfIndex;
        }
        else if (IfInfo.u1IfType == CFA_TELINK)
        {
#ifdef CFA_WANTED
#ifdef TLM_WANTED
            u4TeLinkIf = MPLS_ZERO;
            if (CfaApiGetTeLinkIfFromL3If (u4IfIndex, &u4TeLinkIf,
                                           MPLS_ONE, FALSE) == CFA_SUCCESS)
            {
                bTeLinkStackingPresent = TRUE;
            }

            if (bTeLinkStackingPresent == TRUE)
            {
                return;
            }

            if (CfaApiGetMplsIfFromTeLinkIf (u4IfIndex, &u4MplsIfIndex, FALSE)
                == CFA_FAILURE)
            {
                return;
            }
#endif
#endif
        }
        else if (IfInfo.u1IfType == CFA_MPLS_TUNNEL)
        {
            MplsRtrIfChgNotification (u4IfIndex, u1OperStatus);
        }
        else
        {
#ifdef CFA_WANTED
            if (CfaUtilGetL3IfFromMplsIf (u4IfIndex, &u4L3Iface, FALSE) ==
                CFA_FAILURE)
            {
                return;
            }
            u4MplsIfIndex = u4IfIndex;
            CfaGetIfIpAddr ((INT4)u4L3Iface, &u4IpAddr);
#endif
#ifdef MPLS_LDP_WANTED
            LdpIfStChgEventHandler (u4L3Iface,u1OperStatus);
#endif
#ifdef MPLS_RSVPTE_WANTED
            RsvpIfStChgEventHandler (u4MplsIfIndex, u4IpAddr, u1OperStatus,
                                     MPLS_ZERO);
#endif
        }
        CfaInterfaceStatusChangeIndication ((UINT2) u4MplsIfIndex,
                                            u1OperStatus);
    }
#ifdef MPLS_L2VPN_WANTED
    else if (IfInfo.u1IfType == CFA_ENET)
    {
        L2VpnIfEventHandler (u4IfIndex, MPLS_ZERO, u1OperStatus);
    }
#ifdef HVPLS_WANTED
    else if (IfInfo.u1IfType == CFA_PSEUDO_WIRE)
    {
	if (L2VPN_SUCCESS == L2VpnGetPwIndexFromPwIfIndex (u4IfIndex, &u4PwIndex))
	{
	    L2VpnIfEventHandler (u4IfIndex, MPLS_ZERO, u1OperStatus);		 
	}
    }
#endif
#endif
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : MplsPutMplsHdr                                             */
/*                                                                           */
/* Description  : Write the complete MPLS header in the given location       */
/*                                                                           */
/* Input        : pHdr - Pointer to the MPLS header                          */
/*                                                                           */
/* Output       : pBuf - Pointer to the buffer in which headers to be stored */
/*                                                                           */
/* Returns      : Nothing                                                    */
/*                                                                           */
/*****************************************************************************/
VOID
MplsPutMplsHdr (UINT1 *pBuf, tMplsHdr * pHdr)
{
    UINT4               u4Val;

    u4Val = ((pHdr->u4Lbl << 12) & 0xfffff000) |
        ((pHdr->Exp << 9) & 0x00000e00) |
        ((pHdr->SI << 8) & 0x00000100) | (pHdr->Ttl & 0x000000ff);
    PTR_ASSIGN4 (pBuf, u4Val);
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : MplsReadMplsHdr                                            */
/*                                                                           */
/* Description  : Read the complete MPLS header from the given location      */
/*                                                                           */
/* Input        : pHdr - Pointer to the MPLS header                          */
/*                                                                           */
/* Output       : pBuf - Pointer to the buffer in which headers to be read   */
/*                                                                           */
/* Returns      : Nothing                                                    */
/*                                                                           */
/*****************************************************************************/
VOID
MplsReadMplsHdr (UINT1 *pBuf, tMplsHdr * pHdr)
{
    UINT4               u4Hdr;

    PTR_FETCH4 (u4Hdr, pBuf);
    pHdr->u4Lbl = (u4Hdr & 0xfffff000) >> 12;
    pHdr->Exp = (u4Hdr & 0x00000e00) >> 9;
    pHdr->SI = (u4Hdr & 0x00000100) >> 8;
    pHdr->Ttl = u4Hdr & 0x000000ff;

    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : MplsGetDefaultIpNetmask                                    */
/*                                                                           */
/* Description  : Get the Default NetMask for the IP Address                 */
/*                                                                           */
/* Input        : u4HAddr - IP Address                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : Default mask for the address received                      */
/*                                                                           */
/*****************************************************************************/
UINT4
MplsGetDefaultIpNetmask (UINT4 u4HAddr)
{
    UINT4               u4Mask;

    if (IP_IS_ADDR_CLASS_A (u4HAddr))
    {
        u4Mask = 0xff000000;
    }
    else if (IP_IS_ADDR_CLASS_B (u4HAddr))
    {

        u4Mask = 0xffff0000;
    }
    else if (IP_IS_ADDR_CLASS_C (u4HAddr))
    {
        u4Mask = 0xffffff00;
    }
    else
    {
        u4Mask = 0xffffffff;
    }
    return u4Mask;
}

/*****************************************************************************/
/* Function Name : MplsIpRtChgEventHandler                                   */
/* Description   : This routine is called by IP module, for informing the    */
/*                 the Route changes.                                        */
/*                 Corresponding event is posted to LDP task along           */
/*                 with necessary parameters.                                */
/* Input(s)      : tNetIpv4RtInfo - Route info  as given by IP               */
/*                 ! Do not free pNetIpv4RtInfo,                             */
/*                 as it points to Trie Route Entry !                        */
/*                 u1CmdType - Species type of route change - NewEntry,      */
/*                            NextHop change, Interface change etc.,         */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
MplsIpRtChgEventHandler (tNetIpv4RtInfo * pNetIpv4RtInfo, tNetIpv4RtInfo * pNetIpv4RtInfo1, UINT1 u1CmdType)
{
    if (u1CmdType != NETIPV4_MODIFY_ROUTE)
    {
#ifdef MPLS_LDP_WANTED
        LdpIpRtChgEventHandler (pNetIpv4RtInfo, u1CmdType);
#endif
#ifdef MPLS_RSVPTE_WANTED
        RpteHandleRtChgNotification (pNetIpv4RtInfo, u1CmdType);
#endif
        MplsIpRtChgNotification (pNetIpv4RtInfo, u1CmdType);

#ifdef MPLS_L2VPN_WANTED
        L2VpnHandleRtChgNotification (pNetIpv4RtInfo, u1CmdType);
#endif
    }
    else
    {
#ifdef MPLS_LDP_WANTED
        LdpIpRtChgEventHandler (pNetIpv4RtInfo1, NETIPV4_DELETE_ROUTE);
#endif
#ifdef MPLS_RSVPTE_WANTED
        RpteHandleRtChgNotification (pNetIpv4RtInfo1, NETIPV4_DELETE_ROUTE);
#endif
        MplsIpRtChgNotification (pNetIpv4RtInfo1, NETIPV4_DELETE_ROUTE);

#ifdef MPLS_L2VPN_WANTED
        L2VpnHandleRtChgNotification (pNetIpv4RtInfo1, NETIPV4_DELETE_ROUTE);
#endif

#ifdef MPLS_LDP_WANTED
        LdpIpRtChgEventHandler (pNetIpv4RtInfo, NETIPV4_ADD_ROUTE);
#endif
#ifdef MPLS_RSVPTE_WANTED
        RpteHandleRtChgNotification (pNetIpv4RtInfo, NETIPV4_ADD_ROUTE);
#endif
        MplsIpRtChgNotification (pNetIpv4RtInfo, NETIPV4_ADD_ROUTE);

#ifdef MPLS_L2VPN_WANTED
        L2VpnHandleRtChgNotification (pNetIpv4RtInfo, NETIPV4_ADD_ROUTE);
#endif
    }
    return;
}

/*****************************************************************************/
/* Function Name : MplsIfChgEventHandle                                      */
/* Description   : This routine is called from MPLSRTR module                */
/*                 to handle the interface change notification.              */
/* Input(s)      : pIfInfo -> Interface information                          */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
MplsIfChgEventHandle (tMplsIfEntryInfo * pIfInfo)
{
    TeCmnExtHandleIfChange (pIfInfo);
    MplsHandleIfChgForNonTeFtn (pIfInfo); 
    return;
}

/*****************************************************************************/
/* Function Name : MplsHandleIfChgForNonTeFtn                                */
/* Description   : This routine is called to process interface events        */
/*                 for NON-TE FTN entries                                    */
/*                 to handle the interface change notification.              */
/* Input(s)      : pIfInfo -> Interface information                          */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
MplsHandleIfChgForNonTeFtn (tMplsIfEntryInfo *pIfInfo)
{
    UINT4               u4FtnIndex = MPLS_ZERO;
    tFtnEntry           *pFtnEntry = NULL;
    tXcEntry            *pXcEntry = NULL;
    tCfaIfInfo          CfaIfInfo;

      tGenU4Addr          Ftnaddress;

      MEMSET(&Ftnaddress,0,sizeof(tGenU4Addr));

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));
    if (CfaGetIfInfo (pIfInfo->u4IfIndex, &CfaIfInfo) == CFA_FAILURE)
    {
        MPLSFM_DBG (MPLFM_PRCS_ALL, "MplsHandleIfChgForNonTeFtn: FAILURE in "
                                    "CfaGetIfInfo\n");
        return;
    }

    /* No need to process event for interface other than MPLS tunnel interface */
    if (CfaIfInfo.u1IfType != CFA_MPLS_TUNNEL)
    {
        MPLSFM_DBG (MPLFM_PRCS_ALL, "MplsHandleIfChgForNonTeFtn: Rcvd Event for "
                                    "Non MPLS Tunnel interface\n"); 
        return;
    }

    MPLSFM_DBG2 (MPLFM_PRCS_ALL, "MplsHandleIfChgForNonTeFtn: Rcvd Interface Event: Evt Type = %d, "
                                 "for If Index: %d\n", pIfInfo->u1OperStatus, pIfInfo->u4IfIndex);
    MPLS_CMN_LOCK ();

    while (1)
    {
        pFtnEntry = MplsFtnTableNextEntry (u4FtnIndex);
        if (pFtnEntry == NULL)
        {
            break;
        }
 #ifdef MPLS_IPV6_WANTED
    if ( MPLS_IPV6_ADDR_TYPE == pFtnEntry->u1AddrType )
    {
        Ftnaddress.u2AddrType = MPLS_IPV6_ADDR_TYPE;
         MEMCPY(&Ftnaddress.Addr.Ip6Addr ,pFtnEntry->DestAddrMin.u1_addr, IPV6_ADDR_LENGTH);

    }
    else if ( MPLS_IPV4_ADDR_TYPE == pFtnEntry->u1AddrType )
#endif
    {
                    Ftnaddress.u2AddrType = MPLS_IPV4_ADDR_TYPE;
             Ftnaddress.Addr.u4Addr = pFtnEntry->DestAddrMin.u4_addr[0];
    }


        u4FtnIndex = pFtnEntry->u4FtnIndex;

        if (pFtnEntry->u1RowStatus != MPLS_STATUS_ACTIVE)
        {
            continue;
        }

        /* Only process event for NON-TE LSP's */
        if (pFtnEntry->i4ActionType != MPLS_FTN_ON_NON_TE_LSP)
        {
            continue;
        }

        pXcEntry = pFtnEntry->pActionPtr;  		

        if ((pXcEntry == NULL) || (pXcEntry->pOutIndex == NULL))
        {
            continue;
        }

        if (pIfInfo->u4IfIndex != pXcEntry->pOutIndex->u4IfIndex)
        {
            continue;
        }

        /* Process event only for static entries */
        if (pXcEntry->u1Owner != MPLS_OWNER_SNMP)
        {
            continue; 
        }   

        if (pIfInfo->u1OperStatus == CFA_IF_UP)
        {
            if ( XC_OPER_STATUS (pXcEntry) != XC_OPER_UP)
            {
                MPLSFM_DBG (MPLFM_PRCS_ALL, "MplsHandleIfChgForNonTeFtn: Updating XC Entry status "
                                            "to Oper UP\n");

                MplsDbUpdateXcOperStatus (pXcEntry, TRUE, CFA_IF_UP, XC_OUT_SEGMENT);
              
                if (pFtnEntry->u1HwStatus == MPLS_TRUE)
                {
                    MplsNonTePostEventForL2Vpn (pXcEntry->u4Index,
                                                &Ftnaddress,  
                                              L2VPN_MPLS_PWVC_LSP_UP);
                }
            }
        }
        else if (pIfInfo->u1OperStatus == CFA_IF_DOWN)
        {
            MPLSFM_DBG (MPLFM_PRCS_ALL, "MplsHandleIfChgForNonTeFtn: Updating XC Entry status "
                                        "to Oper DOWN\n");

	    MplsDbUpdateXcOperStatus (pXcEntry, TRUE, CFA_IF_DOWN, XC_OUT_SEGMENT);

	    if (MplsFTNDel (pFtnEntry) == MPLS_FAILURE)
	    {
		    MPLSFM_DBG (MPLFM_PRCS_ALL, "MplsHandleIfChgForNonTeFtn: FAILURE "
				    "in MplsFTNDel\n");
	    }

	    MplsNonTePostEventForL2Vpn (pXcEntry->u4Index,
			    &Ftnaddress,
			    L2VPN_MPLS_PWVC_LSP_DOWN);  
	}
    }
    MPLS_CMN_UNLOCK ();
}

/*****************************************************************************/
/* Function Name : MplsRouteChgEventHandle                                   */
/* Description   : This routine is called from MPLSRTR module                */
/*                 to give the route change notification.                    */
/* Input(s)      : pRouteInfo -> Route information                           */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
MplsRouteChgEventHandle (tMplsRtEntryInfo * pRouteInfo)
{
    /* Handle FTN Additions in Hardware for the added routes. */
    MplsRouteChgHandleFtnAdd (pRouteInfo);
    
    /* Start Restablishing the Oper Down Tunnels. */
    TeReEstOperDownTnlsFromRouteChg (pRouteInfo);
    return;
}

/*********************************************************************/
/* Function           : MplsIpIfChgEventHandler                      */
/* Description        : This function is called by IP whenever       */
/*                      thereis any change in the interface          */
/*                      attributes. This function ptr is passed to   */
/*                      IP at the time of MPLS Registrtaion with IP. */
/*                      data structures.                             */
/*                                                                   */
/* Input(s)           : pointer to the  interface                    */
/*                        record whose contents are changed.         */
/*                                                                   */
/* Output(s)          : None                                         */
/*                                                                   */
/* Returns            : None                                         */
/*                                                                   */
/*********************************************************************/
VOID
MplsIpIfChgEventHandler (tNetIpv4IfInfo * pIfInfo, UINT4 u4BitMap)
{
    UINT4               u4L3Iface = MPLS_ZERO;
    UINT4               u4MplsIf = MPLS_ZERO;

    UNUSED_PARAM (u4BitMap);
    RTM_PROT_UNLOCK ();
    if (NetIpv4GetCfaIfIndexFromPort (pIfInfo->u4IfIndex, &u4L3Iface)
        == NETIPV4_FAILURE)
    {
        RTM_PROT_LOCK ();
        return;
    }

    if (CfaUtilGetMplsIfFromIfIndex (u4L3Iface, &u4MplsIf, TRUE) == CFA_FAILURE)
    {
        RTM_PROT_LOCK ();
        return;
    }

    /* Interface change is not handled by LDP module. Posting this event to
     * LDP module is not done. */

#ifdef MPLS_RSVPTE_WANTED
    RsvpIfStChgEventHandler (u4MplsIf, pIfInfo->u4Addr,
                             (UINT1) pIfInfo->u4Oper, MPLS_ZERO);
#endif
    RTM_PROT_LOCK ();

    return;
}

/****************************************************************************/
/* Function Name   : MplsIpRtChgNotification                                */
/* Description     : This function handles Route Change notification        */
/* Input (s)       : pRtInfo - pointer to Rt Info                           */
/*                   u4BitMap - Bitmap                                      */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
MplsIpRtChgNotification (tNetIpv4RtInfo * pNetIpv4RtInfo, UINT1 u1CmdType)
{
    tMplsRtEntryInfo   *pRouteInfo = NULL;

    if (gu1MplsInitialised != TRUE)
    {
        MPLSFM_DBG (MPLSFM_PRCS_ETEXT,
                    " MplsIpRtChgNotification: "
                    " MPLS is not initialised : INTMD-EXIT \n");
        return;
    }

    if (MPLS_ADMIN_STATUS (MPLS_DEF_INCARN) != MPLS_ADMIN_STATUS_UP)
    {
        MPLSFM_DBG (MPLSFM_PRCS_ETEXT,
                    " MPLS Admin status is not up : INTMD-EXIT \n");
        return;
    }
    if (pNetIpv4RtInfo != NULL)
    {
        pRouteInfo = (tMplsRtEntryInfo *)
            MemAllocMemBlk (MPLS_IP_ROUTE_INFO_POOL_ID);

        if (pRouteInfo == NULL)
        {
            MPLSFM_DBG (MPLSFM_PRCS_ETEXT,
                        "IF: Memory Allocation for Ip Route Info failed \n");
            return;
        }

        /* Copying information to Ldp's strucure */
        MPLS_IPV4_U4_ADDR(pRouteInfo->DestAddr) = pNetIpv4RtInfo->u4DestNet;
        MPLS_IPV4_U4_ADDR(pRouteInfo->DestMask) = pNetIpv4RtInfo->u4DestMask;
        MPLS_IPV4_U4_ADDR(pRouteInfo->NextHop) = pNetIpv4RtInfo->u4NextHop;
        pRouteInfo->u4RtIfIndx = pNetIpv4RtInfo->u4RtIfIndx;
        pRouteInfo->u4RtNxtHopAS = pNetIpv4RtInfo->u4RtNxtHopAs;
        pRouteInfo->i4Metric1 = pNetIpv4RtInfo->i4Metric1;
        pRouteInfo->u1RowStatus = (UINT1) pNetIpv4RtInfo->u4RowStatus;
        pRouteInfo->u1CmdType = u1CmdType;
        pRouteInfo->u2AddrType=MPLS_ADDR_TYPE_IPV4;
    }
    else
    {
        MPLSFM_DBG (MPLSFM_PRCS_ETEXT,
                    " IF: No Route Information passed by IP : INTMD-EXIT \n");
        return;
    }

    if (OsixQueSend (gFmRtrChgQId, (UINT1 *) (&pRouteInfo),
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (MPLS_IP_ROUTE_INFO_POOL_ID, (UINT1 *) pRouteInfo);
        return;
    }

    /* tell mpls that there is something to do for you */
    if (OsixEvtSend (gFmTaskId, MPLS_ROUTE_CHG_NOTIF) == OSIX_FAILURE)
    {
        return;
    }
    return;
}

/****************************************************************************/
/* Function Name   : MplsRtrIfChgNotification                               */
/* Description     : This function posts a message to MPLS Router module    */
/*                   to handle Interface Status Change                      */
/*                   notification                                           */
/* Input (s)       : u4IfIndex    - MPLS Tunnel Interface Index             */
/*                   u1OperStatus - Oper Status of MPLS Tunnel Interface    */
/*                                  Index                                   */
/* Output (s)      : None                                                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
MplsRtrIfChgNotification (UINT4 u4IfIndex, UINT1 u1OperStatus)
{
    tMplsIfEntryInfo   *pIfInfo = NULL;

    if (gu1MplsInitialised != TRUE)
    {
        MPLSFM_DBG (MPLSFM_PRCS_ETEXT,
                    " MplsRtrIfChgNotification: "
                    " MPLS is not initialised : INTMD-EXIT \n");
        return;
    }

    if (MPLS_ADMIN_STATUS (MPLS_DEF_INCARN) != MPLS_ADMIN_STATUS_UP)
    {
        MPLSFM_DBG (MPLSFM_PRCS_ETEXT,
                    " MPLS Admin status is not up : INTMD-EXIT \n");
        return;
    }

    pIfInfo = (tMplsIfEntryInfo *) MemAllocMemBlk (MPLS_IP_IF_INFO_POOL_ID);

    if (pIfInfo == NULL)
    {
        MPLSFM_DBG (MPLSFM_PRCS_ETEXT,
                    "IF: Memory Allocation for IF Change Info failed \n");
        return;
    }

    pIfInfo->u4IfIndex = u4IfIndex;
    pIfInfo->u1OperStatus = u1OperStatus;

    if (OsixQueSend (gFmIfChgQId, (UINT1 *) (&pIfInfo),
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        MPLSFM_DBG (MPLSFM_PRCS_ETEXT, "IF: IF Change Queue Send Failed\n");
        MemReleaseMemBlock (MPLS_IP_IF_INFO_POOL_ID, (UINT1 *) pIfInfo);
        return;
    }

    /* tell mpls that there is something to do for you */
    if (OsixEvtSend (gFmTaskId, MPLS_INTF_CHG_NOTIF) == OSIX_FAILURE)
    {
        MPLSFM_DBG (MPLSFM_PRCS_ETEXT, "IF: IF Change Event Send failed\n");
        return;
    }
    return;
}

/******************************************************************************/
/* Function Name   : RpteCheckTnlFrrEntryInFTN                                */
/* Description     : This function handles the FTN programming at the headend */
/* Input (s)       : pTeTnlInfo - pointer to Tunnel Info                      */
/*                   u4BkpTnlInst - Backup tunnel instance                    */
/*                   bIsLocalRevert - TRUE - link/node up handling            */
/*                                    FALSE - link/node down handling         */
/* Output (s)      : None                                                     */
/* Returns         : None                                                     */
/******************************************************************************/
VOID
RpteCheckTnlFrrEntryInFTN (tTeTnlInfo * pTeTnlInfo, UINT4 u4BkpTnlInst,
                           BOOL1 bIsLocalRevert)
{
    tFrrTeTnlInfo       FrrTeTnlInfo;

    if (gu1MplsInitialised != TRUE)
    {
        MPLSFM_DBG (MPLSFM_PRCS_ETEXT,
                    " MPLS is not initialised : INTMD-EXIT \n");
        return;
    }
    if (MPLS_ADMIN_STATUS (MPLS_DEF_INCARN) != MPLS_ADMIN_STATUS_UP)
    {
        MPLSFM_DBG (MPLSFM_PRCS_ETEXT,
                    " MPLS Admin status is not up : INTMD-EXIT \n");
        return;
    }

    FrrTeTnlInfo.u4TnlIndex = pTeTnlInfo->u4TnlIndex;
    FrrTeTnlInfo.u4TnlInstance = pTeTnlInfo->u4TnlInstance;
    FrrTeTnlInfo.u4BkpTnlInstance = u4BkpTnlInst;
    MEMCPY (&(FrrTeTnlInfo.TnlIngressLsrId),
            &(pTeTnlInfo->TnlIngressLsrId), sizeof (tTeRouterId));
    MEMCPY (&(FrrTeTnlInfo.TnlEgressLsrId),
            &(pTeTnlInfo->TnlEgressLsrId), sizeof (tTeRouterId));
    if (bIsLocalRevert == MPLS_TRUE)
    {
        FrrTeTnlInfo.bIsLocalRevert = MPLS_TRUE;
    }
    else
    {
        FrrTeTnlInfo.bIsLocalRevert = MPLS_FALSE;
    }
    MplsCheckTnlFrrEntryInFTN (&FrrTeTnlInfo);
    MPLSFM_DBG (MPLSFM_PRCS_ETEXT, " RpteCheckTnlFrrEntryInFTN: INTMD-EXIT \n");
    return;
}


/*****************************************************************************/
/* Function Name : MplsUpdateXcOperStatus                                    */
/* Description   : This function updates XcOper Status on receiving interface*/
/*                 Up Event.                                                 */
/* Input(s)      : u4IfIndex -> Interface Which came up.                   */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
MplsUpdateXcOperStatus (UINT4  u4IfIndex)
{
    UINT4    u4InIfIndex = u4IfIndex;
    UINT4    u4XcInIndex = MPLS_ZERO;
    UINT4    u4XcOutIndex = MPLS_ZERO;
    tXcEntry       *  pXcEntry = NULL;
    MPLSFM_DBG2 (MPLSFM_PRCS_PRCS, "%s Entry with OperUp Index: %d \n", __FUNCTION__, u4IfIndex);	
    pXcEntry = MplsXCTableNextEntry(0,0,0);
    while((pXcEntry != NULL) && (pXcEntry->u1Owner == MPLS_OWNER_SNMP))
    {
        u4XcInIndex = MPLS_ZERO;
        u4XcOutIndex = MPLS_ZERO;
        if ((XC_ININDEX (pXcEntry) != NULL))
        {
            u4XcInIndex = (XC_ININDEX (pXcEntry))->u4Index;
            if (XC_OUTINDEX (pXcEntry) != NULL)
            {
                u4XcOutIndex = (XC_OUTINDEX (pXcEntry))->u4Index;
                if(u4InIfIndex == pXcEntry->pOutIndex->u4IfIndex)
                {
                    MplsDbUpdateXcOperStatus (pXcEntry, TRUE, CFA_IF_UP, XC_BOTH_SEGMENT);
                    MPLSFM_DBG5(MPLSFM_PRCS_PRCS, "%s Updated Xc Oper Status for XcIndex: %d O-IfIndex: %d andI-IfIndex: %d  with Value: %d \n",
	           	__FUNCTION__, (XC_INDEX (pXcEntry)), pXcEntry->pOutIndex->u4IfIndex, pXcEntry->pInIndex->u4IfIndex, pXcEntry->u1OperStatus);
                }
            }
            if (u4InIfIndex == pXcEntry->pInIndex->u4IfIndex )
            { 
                MPLSFM_DBG3(MPLSFM_PRCS_PRCS, "%s Compared OperUpIndex: %d and InSeg-IfIndex: %d \n", 
                       __FUNCTION__, u4InIfIndex, pXcEntry->pInIndex->u4IfIndex);      
                MplsDbUpdateXcOperStatus (pXcEntry, TRUE, CFA_IF_UP, XC_IN_SEGMENT);
                MPLSFM_DBG4(MPLSFM_PRCS_PRCS, "%s Updated Xc Oper Status for XcIndex: %d InSeg-ifIndex:%d with Value: %d \n",
	  				__FUNCTION__, (XC_INDEX (pXcEntry)), pXcEntry->pInIndex->u4IfIndex, pXcEntry->u1OperStatus);
                
            }
        }
        else if (XC_OUTINDEX (pXcEntry) != NULL)
        {
            u4XcOutIndex = (XC_OUTINDEX (pXcEntry))->u4Index;
            MPLSFM_DBG2(MPLSFM_PRCS_PRCS, "%s InsegmentTable doesnot exists for XCINDEX: %d\n",
	  				__FUNCTION__, (XC_INDEX (pXcEntry)));
        }
        pXcEntry = MplsXCTableNextEntry((XC_INDEX (pXcEntry)), u4XcInIndex, u4XcOutIndex);
    }
}

/*****************************************************************************/
/* Function Name : MplsRouteChgHandleFtnAdd                                  */
/* Description   : This function handles FTN Additions in Hardware for a     */
/*                 Route known.                                              */
/* Input(s)      : pRouteInfo -> Route information                           */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
MplsRouteChgHandleFtnAdd (tMplsRtEntryInfo * pRouteInfo)
{
    tFtnEntry          *pFtnEntry = NULL;
    tXcEntry           *pXcEntry = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4Prefix = MPLS_ZERO;
    UINT4               u4IngressId = MPLS_ZERO;
    UINT4               u4EgressId = MPLS_ZERO;
    UINT1               u1Dir = MPLS_DIRECTION_FORWARD;
    INT4                i4RetVal = MPLS_ZERO;

   tGenU4Addr          Ftnaddress;

      MEMSET(&Ftnaddress,0,sizeof(tGenU4Addr));

    u4Prefix = ((MPLS_IPV4_U4_ADDR(pRouteInfo->DestAddr)) & (MPLS_IPV4_U4_ADDR(pRouteInfo->DestMask)));

    MPLS_CMN_LOCK ();
    pFtnEntry = MplsSignalGetFtnTableEntry (u4Prefix);
    if (pFtnEntry == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return;
    }
    if (pFtnEntry->u1Storage == MPLS_STORAGE_VOLATILE)
    {
        MPLS_CMN_UNLOCK ();
        return;
    }

#ifdef MPLS_IPV6_WANTED
    if ( MPLS_IPV6_ADDR_TYPE == pFtnEntry->u1AddrType )
     {
         Ftnaddress.u2AddrType = MPLS_IPV6_ADDR_TYPE;
          MEMCPY(&Ftnaddress.Addr.Ip6Addr ,pFtnEntry->DestAddrMin.u1_addr, IPV6_ADDR_LENGTH);

   }
     else if ( MPLS_IPV4_ADDR_TYPE == pFtnEntry->u1AddrType )
 #endif
     {
                     Ftnaddress.u2AddrType = MPLS_IPV4_ADDR_TYPE;
              Ftnaddress.Addr.u4Addr = pFtnEntry->DestAddrMin.u4_addr[0];
    }

    if (pFtnEntry->i4ActionType == MPLS_FTN_ON_TE_LSP)
    {
        pTeTnlInfo = (tTeTnlInfo *) pFtnEntry->pActionPtr;
        if (pTeTnlInfo == NULL)
        {
            MPLS_CMN_UNLOCK ();
            return;
        }
        CONVERT_TO_INTEGER (pTeTnlInfo->TnlIngressLsrId, u4IngressId);
        CONVERT_TO_INTEGER (pTeTnlInfo->TnlEgressLsrId, u4EgressId);
        
        /* make FTN-entry's route status Exist, so that FTN-entry
         * can be added at tunnel oper up time */
        if (TE_TNL_OPER_STATUS(pTeTnlInfo) == TE_ADMIN_UP)
        {
	   if (TE_TNL_OPER_STATUS(pTeTnlInfo) != TE_OPER_UP)
          {
              pFtnEntry->u1RouteStatus = FTN_ROUTE_EXIST;
          }
          else if ((pRouteInfo->u1CmdType == NETIPV4_ADD_ROUTE) &&
                  (TE_TNL_FRR_PROT_METHOD (pTeTnlInfo) == TE_TNL_FRR_FACILITY_METHOD))
          {
              pFtnEntry->u1RouteStatus = FTN_ROUTE_EXIST;
          }
        }

        /* Get the primary tunnel info */
        pTeTnlInfo =
            TeGetTunnelInfo (pTeTnlInfo->u4TnlIndex,
                             pTeTnlInfo->u4TnlPrimaryInstance,
                             OSIX_HTONL (u4IngressId), OSIX_HTONL (u4EgressId));
        if (pTeTnlInfo == NULL)
        {
            MPLS_CMN_UNLOCK ();
            return;
        }
        if (pTeTnlInfo->u1TnlRole == TE_INGRESS)
        {
            u1Dir = MPLS_DIRECTION_FORWARD;
        }
        else if (pTeTnlInfo->u1TnlRole == TE_EGRESS)
        {
            u1Dir = MPLS_DIRECTION_REVERSE;
        }

        pXcEntry = MplsGetXCEntryByDirection (pTeTnlInfo->u4TnlXcIndex, u1Dir);
    }
    else
    {
        pXcEntry = pFtnEntry->pActionPtr;
    }

    if ((pXcEntry == NULL) || (pXcEntry->pOutIndex == NULL))
    {
        MPLS_CMN_UNLOCK ();
        return;
    }

    /* TE Tunnels might not necessarily have the same NH as the IGP NH as it depends
     * on TE Tunnel configuration (dynamic, explitcit-path or loose-hop)
     */
    if (pFtnEntry->i4ActionType != MPLS_FTN_ON_TE_LSP)
    {
    if (pXcEntry->pOutIndex->NHAddr.u4_addr[0] != MPLS_IPV4_U4_ADDR(pRouteInfo->NextHop))
    {
        MPLS_CMN_UNLOCK ();
        return;
    }
    }

    if (pRouteInfo->u1CmdType == NETIPV4_ADD_ROUTE)
    {
        MPLSFM_DBG1 (MPLSFM_PRCS_PRCS, "Rt %x added\n", u4Prefix);
       if((pFtnEntry->u1RouteStatus == FTN_ROUTE_NOT_EXIST) ||
         ((pTeTnlInfo != NULL) &&
         (TE_TNL_FRR_PROT_METHOD (pTeTnlInfo) == TE_TNL_FRR_FACILITY_METHOD)))
 	{
            /* NP Add Call */
	    pFtnEntry->u1RouteStatus = FTN_ROUTE_EXIST;
            if (pFtnEntry->u1HwStatus == MPLS_FALSE)
	    {
            	i4RetVal = MplsFTNAdd (pFtnEntry);

            	if (i4RetVal == MPLS_SUCCESS)
            	{
                
                if ((pFtnEntry->i4ActionType == MPLS_FTN_ON_NON_TE_LSP) &&  
                    (XC_OPER_STATUS (pXcEntry) == XC_OPER_UP))
                {
#ifdef MPLS_L2VPN_WANTED
                    /* Send Event to L2VPN task. L2VPN will check this
                     * XC index in its data base and if present then it
                     * will make the Pseudowire Oper Status to lower layer Down */
                     MplsNonTePostEventForL2Vpn (pXcEntry->u4Index,
                                                 &Ftnaddress,       
                                          L2VPN_MPLS_PWVC_LSP_UP);
#endif
                }
            	}
	     }
            else
            {
                MPLSFM_DBG1 (MPLSFM_PRCS_PRCS,
                             "MplsRouteChgEventHandle Prefix %x "
                             "FTN add failed in the HW\n", u4Prefix);
            }
        }
    }
    else if ((pFtnEntry->u1RouteStatus == FTN_ROUTE_EXIST) &&
             (pRouteInfo->u1CmdType == NETIPV4_DELETE_ROUTE))
    {
        MPLSFM_DBG1 (MPLSFM_PRCS_PRCS, "Rt %x deleted\n", u4Prefix);

        if (pFtnEntry->u1HwStatus == MPLS_TRUE)
        {
            if (MplsFTNDel (pFtnEntry) == MPLS_FAILURE)
            {
                MPLSFM_DBG (MPLFM_PRCS_ALL, "MplsHandleIfChgForNonTeFtn: FAILURE "
                        "in MplsFTNDel\n");
            }

	    MplsNonTePostEventForL2Vpn (pXcEntry->u4Index,
			    &Ftnaddress,
			    L2VPN_MPLS_PWVC_LSP_DOWN);
	}

        pFtnEntry->u1RouteStatus = FTN_ROUTE_NOT_EXIST;
    }
    MPLS_CMN_UNLOCK ();
}

/************************************************************************
 * Function Name   : MplsGetL3Intf
 * Description     : Function to get L3 Interface Index from MPLS If or 
 *                   MPLS Tunnel Interface Index. This function gets the
 *                   required information by fetching from the CFA.
 * Input           : u4MplsIfOrMplsTnlIf - MPLS If or MPLS Tnl If
 * Output          : pu4L3Intf           - L3 Interface Index
 * Returns         : MPLS_SUCCESS or MPLS_FAILURE
 ************************************************************************/
INT4
MplsGetL3Intf (UINT4 u4MplsIfOrMplsTnlIf, UINT4 *pu4L3Intf)
{
    tCfaIfInfo          IfInfo;
    UINT4               u4L3Intf = MPLS_ZERO;

    MEMSET ((UINT1 *) &IfInfo, MPLS_ZERO, sizeof (tCfaIfInfo));

    if (CfaGetIfInfo (u4MplsIfOrMplsTnlIf, &IfInfo) == CFA_FAILURE)
    {
        return MPLS_FAILURE;
    }

    if (IfInfo.u1IfType == CFA_MPLS_TUNNEL)
    {
        if (CfaUtilGetIfIndexFromMplsTnlIf (u4MplsIfOrMplsTnlIf,
                                            &u4L3Intf, TRUE) != CFA_SUCCESS)
        {
            return MPLS_FAILURE;
        }
        if (CfaGetIfInfo (u4L3Intf, &IfInfo) == CFA_FAILURE)
        {
            return MPLS_FAILURE;
        }

        if (IfInfo.u1IfType == CFA_MPLS)
        {
            if (CfaUtilGetL3IfFromMplsIf (u4L3Intf, &u4L3Intf,
                                          TRUE) != CFA_SUCCESS)
            {
                return MPLS_FAILURE;
            }
        }
        else if (IfInfo.u1IfType == CFA_MPLS_TUNNEL)
        {
            if (CfaUtilGetIfIndexFromMplsTnlIf (u4L3Intf, &u4L3Intf,
                                                TRUE) != CFA_SUCCESS)
            {
                return MPLS_FAILURE;
            }
        }

        *pu4L3Intf = u4L3Intf;
    }
    else
    {
        *pu4L3Intf = u4MplsIfOrMplsTnlIf;
    }

    return MPLS_SUCCESS;
}

/******************************************************************************
* Function Name : MplsGetFTNActionType 
* Description   : This routine is used to Get Action Type for the FTN Entry
* Input(s)      : u4Prefix   - ip address prefix
*                 pi4FtnActionType  - FTNAction Type
* Output(s)     : None
* Return(s)     : MPLS_SUCCESS/MPLS_FAILURE
******************************************************************************/
INT1
MplsGetFTNActionType (UINT4 u4Prefix, INT4 *pi4FtnActionType)
{
    tFtnEntry          *pFtnEntry = NULL;

    MPLS_CMN_LOCK ();

    pFtnEntry = MplsSignalGetFtnTableEntry (u4Prefix);

    if (pFtnEntry != NULL)
    {
        *pi4FtnActionType = pFtnEntry->i4ActionType;
        MPLS_CMN_UNLOCK ();
        return MPLS_SUCCESS;
    }

    MPLS_CMN_UNLOCK ();
    return MPLS_FAILURE;
}

/******************************************************************************
 *  Function Name : MplsSetIfInfo
 *  Description   : This routine is used to Set MPLS Interface information
 *  Input(s)      : pTeTnlInfo - Tunnel info
 *                   u4MplsTnlIfIndex - MPLS Tunnel Interface Index
 *                   i4IfParam -Interface parameter
 *  Output(s)     : None
 *  Return(s)     : MPLS_SUCCESS/MPLS_FAILURE
 * ******************************************************************************/
INT4
MplsSetIfInfo (tTeTnlInfo * pTeTnlInfo, UINT4 u4MplsTnlIfIndex, INT4 i4IfParam)
{
    tCfaIfInfo          CfaIfInfo;
    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    if ((TE_TNL_ISIF (pTeTnlInfo) == MPLS_TRUE) &&
        (TE_TNL_NAME (pTeTnlInfo) != NULL))
    {
        switch (i4IfParam)
        {
            case IF_OPER_STATUS:
                CfaIfInfo.u1IfOperStatus = pTeTnlInfo->u1TnlOperStatus;
                break;
            case IF_NAME:
                STRNCPY (CfaIfInfo.au1IfName, TE_TNL_NAME (pTeTnlInfo),
                         (CFA_MAX_PORT_NAME_LENGTH - 1));
                break;
            default:
                return MPLS_FAILURE;
        }

        if (CfaSetIfInfo (i4IfParam, u4MplsTnlIfIndex, &CfaIfInfo) ==
            CFA_FAILURE)
        {
            return MPLS_FAILURE;
        }
    }
    return MPLS_SUCCESS;

}

/******************************************************************************
 *  Function Name : MplsValidateDiffServTcType
 *  Description   : This routine is used to validate Traffic Class Type
 *  Input(s)      : u4TestTcType
 *  Output(s)     : None
 *  Return(s)     : TRUE/FALSE
 * ******************************************************************************/
UINT1
MplsValidateDiffServTcType (UINT4 u4TcType)
{
    switch (u4TcType)
    {
        case MPLS_DIFFSERV_DF_DSCP:
        case MPLS_DIFFSERV_CS1_DSCP:
        case MPLS_DIFFSERV_CS2_DSCP:
        case MPLS_DIFFSERV_CS3_DSCP:
        case MPLS_DIFFSERV_CS4_DSCP:
        case MPLS_DIFFSERV_CS5_DSCP:
        case MPLS_DIFFSERV_CS6_DSCP:
        case MPLS_DIFFSERV_CS7_DSCP:
        case MPLS_DIFFSERV_EF_DSCP:
        case MPLS_DIFFSERV_EF1_DSCP:
        case MPLS_DIFFSERV_AF1_PSC_DSCP:
        case MPLS_DIFFSERV_AF2_PSC_DSCP:
        case MPLS_DIFFSERV_AF3_PSC_DSCP:
        case MPLS_DIFFSERV_AF4_PSC_DSCP:
            return TRUE;
        default:
            return FALSE;
    }
}

/******************************************************************************
 *  Function Name : MplsIsOamMonitoringTnl
 *  Description   : This routine is used to check whether the tunnel is
 *                  monitored by OAM like BFD using the MEG.
 *  Input(s)      : pTeTnlInfo
 *  Output(s)     : None
 *  Return(s)     : TRUE/FALSE
 ******************************************************************************/
UINT1
MplsIsOamMonitoringTnl (tTeTnlInfo * pTeTnlInfo)
{
    UINT4               u4OamSessionId = 0;

    MplsOamIfGetOamSessionId (pTeTnlInfo->u4MegIndex, pTeTnlInfo->u4MeIndex,
                              pTeTnlInfo->u4MpIndex, &u4OamSessionId);

    if (u4OamSessionId != 0)
    {
        return TRUE;
    }

    return FALSE;
}

/******************************************************************************
 *  Function Name : MplsIsFtnExist
 *  Description   : This routine is used to check whether FTN entry is exist
 *                  for the destination ip
 *  Input(s)      : u4Prefix - Destination ip
 *                  u4Mask - Destination Mask
 *  Output(s)     : None
 *  Return(s)     : TRUE/FALSE
 ******************************************************************************/
UINT1
MplsIsFtnExist (tGenU4Addr * pPrefix, UINT4 u4Mask)
{
    tFtnEntry          *pFtnEntry = NULL;
#ifdef MPLS_IPV6_WANTED
    UINT1               au1Max[IPV6_ADDR_LENGTH];

    MEMSET(au1Max, 0, IPV6_ADDR_LENGTH);

    if ( MPLS_IPV6_ADDR_TYPE == pPrefix->u2AddrType )
    {
        pFtnEntry = MplsSignalGetFtnIpv6TableEntry(&(pPrefix->Addr.Ip6Addr));
    }
    else
#endif
    {
        pFtnEntry = MplsSignalGetFtnTableEntry (pPrefix->Addr.u4Addr);
    }

    if (pFtnEntry == NULL)
    {
        return FALSE;
    }

#ifdef MPLS_IPV6_WANTED
    if ( MPLS_IPV6_ADDR_TYPE == pPrefix->u2AddrType )
    {
        MplsGetIPV6Subnetmask((UINT1)u4Mask, (UINT1 *)au1Max);
        if ( (MEMCMP(pFtnEntry->DestAddrMax.u1_addr, au1Max, IPV6_ADDR_LENGTH) == 0) &&
             (pFtnEntry->u1HwStatus == TRUE))
        {
            return TRUE;
        }
    }
    else if ( MPLS_IPV4_ADDR_TYPE == pPrefix->u2AddrType )
#endif
    {
        if ((pFtnEntry->DestAddrMax.u4_addr[0] == u4Mask) &&
            (pFtnEntry->u1HwStatus == TRUE))
        {
            return TRUE;
        }
    }
    return FALSE;
}


#ifdef MPLS_IPV6_WANTED
/*****************************************************************************/
/* Function Name : MplsIpRtChgEventHandler                                   */
/* Description   : This routine is called by IP module, for informing the    */
/*                 the Route changes.                                        */
/*                 Corresponding event is posted to LDP task along           */
/*                 with necessary parameters.                                */
/* Input(s)      : tNetIpv4RtInfo - Route info  as given by IP               */
/*                 ! Do not free pNetIpv4RtInfo,                             */
/*                 as it points to Trie Route Entry !                        */
/*                 u1CmdType - Species type of route change - NewEntry,      */
/*                            NextHop change, Interface change etc.,         */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
MplsIpv6RtChgEventHandler (tNetIpv6HliParams * pNetIpv6HlParams)
{

#ifdef MPLS_LDP_WANTED
	LdpIpv6RtChgEventHandler (pNetIpv6HlParams);
#endif
	MplsIpv6RtChgNotification (pNetIpv6HlParams);

#ifdef MPLS_L2VPN_WANTED
    L2VpnHandleIpv6RtChgNotification (pNetIpv6HlParams);
#endif
    return;
}

UINT1 MplsGetIpv6Subnetmasklen (UINT1 *pu1IpAddress)
{
	INT4                i4Index = 0;
	UINT1               u1TempLen = 8;
	UINT1               u1PLen = 0;
	UINT1               u1Mask = 0;

	for (i4Index = 0; i4Index < IPV6_ADDR_LENGTH; i4Index++)
	{
		u1Mask = *(pu1IpAddress + i4Index);
		u1TempLen = 8;
		while (u1Mask)
		{
			u1Mask = (UINT1) (u1Mask << 1);
			u1PLen++;
			u1TempLen--;
		}
		if (u1TempLen != 0)
		{
			break;
		}
	}
	return u1PLen;
}

INT4
MplsGetIPV6Subnetmask (UINT1 u1Prefixlen, UINT1 *pu1Max)
{
	UINT4               u4Index = 0;
	UINT1               u1Len = u1Prefixlen;
	UINT4               u4Maskval = 0;
	UINT1               u1Remain = 0;

	while (u4Index < IPV6_ADDR_LENGTH)
	{
		pu1Max[u4Index] = 0x00;
		u4Index++;
	}

	u4Maskval = u1Len / MPLS_ONE_BYTE_BITS;
	u1Remain = (UINT1) (u1Len % MPLS_ONE_BYTE_BITS);

	for (u4Index = 0; u4Index < u4Maskval; u4Index++)
	{
		pu1Max[u4Index] = MPLS_ONE_BYTE_MAX_VAL;
	}

	if (u1Remain > 0 && u4Maskval < IPV6_ADDR_LENGTH)
	{
		pu1Max[u4Maskval] =
			(UINT1) (MPLS_ONE_BYTE_MAX_VAL << (MPLS_ONE_BYTE_BITS - u1Remain));
	}

	return MPLS_SUCCESS;
}

#endif

VOID
MplsGetPrefix(uGenU4Addr *pDestAddr,uGenU4Addr*pMask,UINT2 u2AddrType,uGenU4Addr *pPrefix)
{
#ifdef MPLS_IPV6_WANTED
	UINT1    u1Count=0;
#endif

	switch(u2AddrType)
	{
		case IPV4:
				pPrefix->u4Addr=pDestAddr->u4Addr & pMask->u4Addr;
			break;
#ifdef MPLS_IPV6_WANTED
		case IPV6:
			for(u1Count=0;u1Count<IPV6_ADDR_LENGTH;u1Count++)
			{
				pPrefix->Ip6Addr.u1_addr[u1Count]=pDestAddr->Ip6Addr.u1_addr[u1Count] & pMask->Ip6Addr.u1_addr[u1Count];
			}
			break;
#endif
		default:

			break;
	};
}


#ifdef MPLS_IPV6_WANTED
VOID  MplsIpv6RtChgNotification(tNetIpv6HliParams * pNetIpv6HlParams)
{
	tMplsRtEntryInfo   *pRouteInfo = NULL;
	uGenAddr DestMask;

        MEMSET(&DestMask,0,sizeof(uGenU4Addr));

	if (gu1MplsInitialised != TRUE)
	{
		MPLSFM_DBG (MPLSFM_PRCS_ETEXT,
				" MplsIpRtChgNotification: "
				" MPLS is not initialised : INTMD-EXIT \n");
		return;
	}

	if (MPLS_ADMIN_STATUS (MPLS_DEF_INCARN) != MPLS_ADMIN_STATUS_UP)
	{
		MPLSFM_DBG (MPLSFM_PRCS_ETEXT,
				" MPLS Admin status is not up : INTMD-EXIT \n");
		return;
	}
	if (pNetIpv6HlParams != NULL)
	{
		pRouteInfo = (tMplsRtEntryInfo *)
			MemAllocMemBlk (MPLS_IP_ROUTE_INFO_POOL_ID);

		if (pRouteInfo == NULL)
		{
			MPLSFM_DBG (MPLSFM_PRCS_ETEXT,
					"IF: Memory Allocation for Ip Route Info failed \n");
			return;
		}
		switch(pNetIpv6HlParams->u4Command)
		{
			case NETIPV6_ROUTE_CHANGE:
				MEMCPY(MPLS_IPV6_U4_ADDR(pRouteInfo->DestAddr),
						pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.Ip6Dst.u1_addr,
						IP6_ADDR_SIZE);
				MplsGetIPV6Subnetmask(pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u1Prefixlen,
						MPLS_IPV6_ADDR(DestMask));
				MEMCPY(MPLS_IPV6_U4_ADDR(pRouteInfo->DestMask),
						MPLS_IPV6_ADDR(DestMask),IP6_ADDR_SIZE);
				MEMCPY(MPLS_IPV6_U4_ADDR(pRouteInfo->NextHop),
						pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.NextHop.u1_addr,
						IP6_ADDR_SIZE);
				pRouteInfo->u4RtIfIndx = pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u4Index;
				pRouteInfo->i4Metric1 = (INT4)pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u4Metric;
				pRouteInfo->u1RowStatus = (UINT1)pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u4RowStatus;
				pRouteInfo->u2AddrType=MPLS_IPV6_ADDR_TYPE;
				switch(pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u2ChgBit)
				{
					case IP6_BIT_STATUS:
						switch(pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u4RowStatus)
						{
							case IP6FWD_DESTROY:
								pRouteInfo->u1CmdType =IP6FWD_DESTROY;
								break;
							default:
								MemReleaseMemBlock (MPLS_IP_ROUTE_INFO_POOL_ID, (UINT1 *) pRouteInfo);
								return;
								break;
						};
						break;
					case IP6_BIT_NXTHOP:
						MemReleaseMemBlock (MPLS_IP_ROUTE_INFO_POOL_ID, (UINT1 *) pRouteInfo);
						return;
						break;
					case IP6_BIT_ALL:
						switch(pNetIpv6HlParams->unIpv6HlCmdType.RouteChange.u4RowStatus)
						{
							case IP6FWD_ACTIVE:
								pRouteInfo->u1CmdType =IP6FWD_ACTIVE;
								break;
							default:
								MemReleaseMemBlock (MPLS_IP_ROUTE_INFO_POOL_ID, (UINT1 *) pRouteInfo);
								return;
								break;

						};
						break;
					default:
						MemReleaseMemBlock (MPLS_IP_ROUTE_INFO_POOL_ID, (UINT1 *) pRouteInfo);
						return;
						break;
				};

				break;

			default:
				MemReleaseMemBlock (MPLS_IP_ROUTE_INFO_POOL_ID, (UINT1 *) pRouteInfo);
				return;
				break;
		};
	}
	else
	{
		MPLSFM_DBG (MPLSFM_PRCS_ETEXT,
				" IF: No Route Information passed by IP : INTMD-EXIT \n");
		return;
	}

	if (OsixQueSend (gFmRtrChgQId, (UINT1 *) (&pRouteInfo),
				OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
	{
		MemReleaseMemBlock (MPLS_IP_ROUTE_INFO_POOL_ID, (UINT1 *) pRouteInfo);
		return;
	}

	/* tell mpls that there is something to do for you */
	if (OsixEvtSend (gFmTaskId, MPLS_ROUTE_CHG_NOTIF) == OSIX_FAILURE)
	{
		return;
	}
	return;
}

VOID
MplsIpv6RouteChgEventHandle (tMplsRtEntryInfo * pRouteInfo)
{
    /* Handle FTN Additions in Hardware for the added routes. */
    MplsIpv6RouteChgHandleFtnAdd (pRouteInfo);

    return;
}

VOID
MplsIpv6RouteChgHandleFtnAdd (tMplsRtEntryInfo * pRouteInfo)
{

	tFtnEntry          *pFtnEntry = NULL;
	tXcEntry           *pXcEntry = NULL;
	uGenU4Addr          Prefix;
    

	MEMSET(&Prefix,0,sizeof(uGenU4Addr));

	MplsGetPrefix(&pRouteInfo->DestAddr,&pRouteInfo->DestMask,MPLS_IPV6_ADDR_TYPE,&Prefix);

	MPLS_CMN_LOCK ();
	pFtnEntry = MplsSignalGetFtnIpv6TableEntry(&Prefix.Ip6Addr);
	if (pFtnEntry == NULL)
	{
		MPLS_CMN_UNLOCK ();
		return;
	}
	if (pFtnEntry->u1Storage == MPLS_STORAGE_VOLATILE)
	{
		MPLS_CMN_UNLOCK ();
		return;
	}
	if (pFtnEntry->i4ActionType != MPLS_FTN_ON_TE_LSP)
	{
		pXcEntry = pFtnEntry->pActionPtr;
	}

    if ((pXcEntry == NULL) || (pXcEntry->pOutIndex == NULL))
    {
        MPLS_CMN_UNLOCK ();
        return;
    }

	if (MEMCMP(pXcEntry->pOutIndex->NHAddr.u1_addr,MPLS_IPV6_U4_ADDR(pRouteInfo->NextHop),IP6_ADDR_SIZE)!= MPLS_ZERO)
	{
		MPLS_CMN_UNLOCK ();
		return;
	}

	if ((pFtnEntry->u1RouteStatus == FTN_ROUTE_NOT_EXIST) &&
			(pRouteInfo->u1CmdType == IP6FWD_ACTIVE))
	{
		MPLSFM_DBG1 (MPLSFM_PRCS_PRCS, "Rt %s added\n", Ip6PrintAddr(&Prefix.Ip6Addr));
		pFtnEntry->u1RouteStatus = FTN_ROUTE_EXIST;
		if (pFtnEntry->u1HwStatus == MPLS_FALSE)
		{
			if (MplsFTNAdd (pFtnEntry) == MPLS_FAILURE)    /*NP Add Call */
			{
				MPLSFM_DBG1 (MPLSFM_PRCS_PRCS,
						"MplsRouteChgEventHandle Prefix %s "
						"FTN add failed in the HW\n", Ip6PrintAddr(&Prefix.Ip6Addr));
			}
		}
	}
	else if ((pFtnEntry->u1RouteStatus == FTN_ROUTE_EXIST) &&
			(pRouteInfo->u1CmdType == IP6FWD_DESTROY))
	{
		MPLSFM_DBG1 (MPLSFM_PRCS_PRCS, "Rt %s deleted\n", Ip6PrintAddr(&Prefix.Ip6Addr));
		pFtnEntry->u1RouteStatus = FTN_ROUTE_NOT_EXIST;
	}
	MPLS_CMN_UNLOCK ();
}

#endif

