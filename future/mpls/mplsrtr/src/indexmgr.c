/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: indexmgr.c,v 1.19 2016/07/26 07:41:43 siva Exp $
 *
 * Description: This file contains functions that are associated
 *              with L2VpnIndex Table Manager.
 ********************************************************************/

 /*---------------------------------------------------------------------
 *  The L2Vpn index table manager has been created to provide support for 
 *  the L2Vpn Indexing requirements. However the usage of this library does 
 *  not restrict to this alone, rather it can be used by any application 
 *  which requires to maintain an unique set of identifiers, obtain and 
 *  release an identifier from that set.
 *
 *  The functions included in this file are 
 *  1) To initialise the index manager
 *  2) To shutdown the index manager
 *  3) Obtain a new index from an index space/group
 *  4) Obtain a specified  index from an index space/group
 *  5) Release an index to an index space/group
 *  
 *------------------------------------------------------------------------*/

#include "inmgrinc.h"
#include "mplsrtr.h"
#include "indexmgr.h"
#include "inmgrex.h"
#include "indexgbl.h"
#include "idxmgrsz.h"

extern UINT4        IssSzGetSizingParamsForModule (CHR1 * pu1ModName,
                                                   CHR1 * MacroName);

/*---------------------------------------------------------------------------
 * Function    : IndexMgrInitWithSem
 * Description : This funciton creates the index manager semaphore and 
 *               initialises the index manager.
 * Inputs      : None. 
 * Outputs     : The global variable gpIndexMgrGrpInfo will be allocated
 *               memory and suitably updated in case of successful 
 *               allocation. Another Pool Id corresponds to the free memory
 *               pool assigned to all required chunks for every group.
 * Returns     : MPLS_SUCCESS on successful memory allocation 
 *               and initialisation. Returns MPLS_FAILURE o.w.
---------------------------------------------------------------------------*/
UINT1
IndexMgrInitWithSem (VOID)
{
    /* Semaphore Create */
    if (OsixCreateSem (MPLS_INDEXMGR_SEM_NAME, 1, 0, &gIndexMgrSemId) !=
        OSIX_SUCCESS)
    {
        MPLSRTR_DBG (DBG_ERR_CRT, "INDEXMGR : Semaphore Create Failed\n");
        return MPLS_FAILURE;
    }
    if (IndexManagerInit () == MPLS_FAILURE)
    {
        MPLSRTR_DBG (DBG_ERR_CRT, "INDEXMGR : Initialization Failed\n");
        return MPLS_FAILURE;
    }
    return MPLS_SUCCESS;
}

/*---------------------------------------------------------------------------
 * Function    : IndexManagerInit
 * Description : This function allocates the necessary structures for the 
 *               Index Tbl manager. This function must be invoked before 
 *               using any other routines associated with the Index Manager.
 * Inputs      : None. 
 * Outputs     : The global variable gpIndexMgrGrpInfo will be allocated
 *               memory and suitably updated in case of successful 
 *               allocation. Another pool id corresponds to the free memory
 *               pool assigned to all required chunks for every group.
 * Returns     : MPLS_SUCCESS on successful memory allocation 
 *               and initialisation. Returns MPLS_FAILURE o.w.
---------------------------------------------------------------------------*/
UINT1
IndexManagerInit (VOID)
{
    UINT1               u1Index1 = 0, u1Index2 = 0;
    UINT4               u4Index = 0;
    UINT4               u4TotalChunksReqd = 0;    /* sum of maxchunks for every grp */
    UINT4               u4MaxIndices = 0;
    tIndexMgrGrpInfo   *pIndexMgrGrpInfo = NULL;
    tIndexMgrChunkInfo *pIndexMgrChunkInfo = NULL;

    if (FsINDEXMGRSizingParams[MAX_INDEXMGR_INDEX_MGR_GRP_SIZING_ID].
        u4PreAllocatedUnits != MAX_INDEXMGR_INDEX_MGR_GRP)
    {
        FsINDEXMGRSizingParams[MAX_INDEXMGR_INDEX_MGR_GRP_SIZING_ID].
            u4StructSize
            = (sizeof (tIndexMgrGrpInfo) *
               FsINDEXMGRSizingParams[MAX_INDEXMGR_INDEX_MGR_GRP_SIZING_ID].
               u4PreAllocatedUnits);
        FsINDEXMGRSizingParams[MAX_INDEXMGR_INDEX_MGR_GRP_SIZING_ID].
            u4PreAllocatedUnits = MAX_INDEXMGR_INDEX_MGR_GRP;
    }

    if (FsINDEXMGRSizingParams[MAX_INDEXMGR_INDEX_MGR_CHUNK_SIZING_ID].
        u4PreAllocatedUnits != MAX_INDEXMGR_INDEX_MGR_CHUNK)
    {
        FsINDEXMGRSizingParams[MAX_INDEXMGR_INDEX_MGR_CHUNK_SIZING_ID].
            u4StructSize
            = (sizeof (tIndexMgrChunkInfo) *
               FsINDEXMGRSizingParams[MAX_INDEXMGR_INDEX_MGR_CHUNK_SIZING_ID].
               u4PreAllocatedUnits);
        FsINDEXMGRSizingParams[MAX_INDEXMGR_INDEX_MGR_CHUNK_SIZING_ID].
            u4PreAllocatedUnits = MAX_INDEXMGR_INDEX_MGR_CHUNK;
    }

    for (u1Index1 = MPLS_ZERO; u1Index1 < MAX_INDEXMGR_GRPS_SPRTD; u1Index1++)
    {
        u4MaxIndices = MPLS_ZERO;

        u4MaxIndices
            = IssSzGetSizingParamsForModule (IndexMgrGrpEntry[u1Index1].
                                             pc1ModName,
                                             IndexMgrGrpEntry[u1Index1].
                                             pc1GrpName);

        if (u4MaxIndices != MPLS_ZERO)
        {
            au4GrpMaxIndices[u1Index1] = u4MaxIndices;
        }
    }

    /* Create Mempools for Index manager. */
    if (IndexmgrSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        return MPLS_FAILURE;
    }

    /* Memory for Index Groups structure being allocated */
    if (NULL == (gpIndexMgrGrpInfo = MemAllocMemBlk (GROUP_INFO_POOL_ID)))
    {
        MPLSRTR_DBG (DBG_ERR_CRT,
                     "INDEXMGR : Index Group Info - Mem alloc Failure\n");
        return MPLS_FAILURE;
    }

    /* Now to initialize each group */
    if (MPLS_FAILURE == IndexMgrInitGrps ())
    {
        /* Free all allocated memory & exit */
        MemReleaseMemBlock (GROUP_INFO_POOL_ID, (UINT1 *) gpIndexMgrGrpInfo);
        gpIndexMgrGrpInfo = NULL;
        MPLSRTR_DBG (DBG_ERR_CRT, "INDEXMGR : Index Group Info Init Failure\n");
        return MPLS_FAILURE;
    }

    /* Now to allocate memory for the Max-chunk-info structs for each grp */
    pIndexMgrGrpInfo = gpIndexMgrGrpInfo;
    for (u1Index1 = 0; u1Index1 < MAX_INDEXMGR_GRPS_SPRTD;
         u1Index1++, pIndexMgrGrpInfo++)
    {
        if (NULL == (pIndexMgrGrpInfo->pIndexChunkInfoTbl =
                     MemAllocMemBlk (CHUNK_INFO_POOL_ID)))
        {
            /* Release all memory unto this group & exit */
            pIndexMgrGrpInfo = gpIndexMgrGrpInfo;
            for (u1Index2 = 0; u1Index2 < u1Index1;
                 u1Index2++, pIndexMgrGrpInfo++)
            {
                MemReleaseMemBlock (CHUNK_INFO_POOL_ID, (UINT1 *)
                                    pIndexMgrGrpInfo->pIndexChunkInfoTbl);
                pIndexMgrGrpInfo->pIndexChunkInfoTbl = NULL;
            }

            MemReleaseMemBlock (GROUP_INFO_POOL_ID,
                                (UINT1 *) gpIndexMgrGrpInfo);
            gpIndexMgrGrpInfo = NULL;

            MPLSRTR_DBG (DBG_ERR_CRT,
                         "INDEXMGR : Chunk-info-table - Mem alloc Failure\n");

            return MPLS_FAILURE;
        }

        /* Initialize the chunk-info structure array */
        MEMSET ((VOID *) pIndexMgrGrpInfo->pIndexChunkInfoTbl, 0,
                sizeof (tIndexMgrChunkInfo) *
                pIndexMgrGrpInfo->u4MaxChunksSprtd);

        u4TotalChunksReqd += pIndexMgrGrpInfo->u4MaxChunksSprtd;
    }

    /* Now to assign appropriate no. of chunks to each group */
    pIndexMgrGrpInfo = gpIndexMgrGrpInfo;
    for (u1Index1 = 0; u1Index1 < MAX_INDEXMGR_GRPS_SPRTD;
         u1Index1++, pIndexMgrGrpInfo++)
    {
        /* Now to allocate memory from pool for every chunk-info per group */
        pIndexMgrChunkInfo = pIndexMgrGrpInfo->pIndexChunkInfoTbl;
        for (u4Index = 0; u4Index < pIndexMgrGrpInfo->u4MaxChunksSprtd;
             u4Index++, pIndexMgrChunkInfo++)
        {
            /* Allocate memory for the actual chunk */

            pIndexMgrChunkInfo->pu4IndexChunk = (UINT4 *)
                MemAllocMemBlk (CHUNK_TBL_POOL_ID);

            if (pIndexMgrChunkInfo->pu4IndexChunk == NULL)
            {
                IndexMgrDeInit ();
                MPLSRTR_DBG (DBG_ERR_CRT,
                             "INDEXMGR : Init chunk mem-alloc failure.\n");
                return MPLS_FAILURE;
            }

            MEMSET ((VOID *) pIndexMgrChunkInfo->pu4IndexChunk,
                    0xFF, MAX_INDEXMGR_CHUNK_SIZE);
            /* All bits 1 => all avail */
            pIndexMgrGrpInfo->u4NumChunksInGrp++;
        }
    }
    return MPLS_SUCCESS;
}

/*------------------------------------------------------------------------
 * * Function    : IndexMgrLock
 * * Description : This function takes the index manager lock.
 * * Inputs      : None
 * * Outputs     : None
 * * Returns     : None
 * ------------------------------------------------------------------------*/
VOID
IndexMgrLock ()
{
    if (OsixSemTake (gIndexMgrSemId) != OSIX_SUCCESS)
    {
        return;
    }
}

/*------------------------------------------------------------------------
 * * Function    : IndexMgrUnLock
 * * Description : This function gives the index manager lock.
 * * Inputs      : None
 * * Outputs     : None
 * * Returns     : None
 * ------------------------------------------------------------------------*/
VOID
IndexMgrUnLock ()
{
    OsixSemGive (gIndexMgrSemId);
}

/*------------------------------------------------------------------------
* Function    : IndexMgrDeInit
* Description : This function releases all the memory allocated for the 
*               entire data-structure. (Use for graceful shutdown)
* Inputs      : None
* Outputs     : None
* Returns     : MPLS_SUCCESS/MPLS_FAILURE
------------------------------------------------------------------------*/

UINT1
IndexMgrDeInit (VOID)
{
    UINT1               u1Index = 0;    /* for groups */
    UINT4               u4Index = 0;    /* for chunk infos per group */
    tIndexMgrGrpInfo   *pIndexMgrGrpInfo = gpIndexMgrGrpInfo;
    tIndexMgrChunkInfo *pIndexMgrChunkInfo = NULL;

    /* Now to release the memory in reverse order of allocation */
    for (u1Index = 0; u1Index < MAX_INDEXMGR_GRPS_SPRTD;
         u1Index++, pIndexMgrGrpInfo++)
    {
        /* Now to go thru' the chunk-info list */
        pIndexMgrChunkInfo = pIndexMgrGrpInfo->pIndexChunkInfoTbl;

        if (pIndexMgrChunkInfo == NULL)
        {
            continue;
        }

        for (u4Index = 0; u4Index < pIndexMgrGrpInfo->u4NumChunksInGrp;
             u4Index++, pIndexMgrChunkInfo++)
        {
            /* Release memory for the actual chunks to the pool */
            if (NULL == pIndexMgrChunkInfo->pu4IndexChunk)
            {
                continue;        /* This will never happen actually */
            }

            MemReleaseMemBlock (CHUNK_TBL_POOL_ID,
                                (UINT1 *) pIndexMgrChunkInfo->pu4IndexChunk);
            pIndexMgrChunkInfo->pu4IndexChunk = NULL;
        }

        /* Now to release memory for every chunk-info per group */
        MemReleaseMemBlock (CHUNK_INFO_POOL_ID,
                            (UINT1 *) pIndexMgrGrpInfo->pIndexChunkInfoTbl);
        pIndexMgrGrpInfo->pIndexChunkInfoTbl = NULL;
    }

    MemReleaseMemBlock (GROUP_INFO_POOL_ID, (UINT1 *) gpIndexMgrGrpInfo);
    pIndexMgrGrpInfo = gpIndexMgrGrpInfo = NULL;

    /* Delete Index Manager Memory Pools. */
    IndexmgrSizingMemDeleteMemPools ();

    return MPLS_SUCCESS;
}

/*--------------------------------------------------------------------------
 * Function    : IndexMgrGetIndexBasedOnFlag 
 * Description : This function returns a unique index for the specified 
 *               group. Returns 0 if all indices for the
 *               specified group have been exhausted.
 * Input       : GroupID (1 = PwIndices, 2 = InSegment Indices, 
 *               3 = OutSegment Indices, 4 = XC Indices,
 *               5 = Lbl Stack Indices, 6 = FTN Indices, 7 = LDP Entity indices,
 *               8 = Max LSP Indices, 9 = VPN Identifiers.). 
 *               Flag(FALSE = 0 ,TRUE=1) 
 * Outputs     : None
 * Returns     : Unique Index if available (ranging from 1 to max in that 
 *               group), 0 o.w.
--------------------------------------------------------------------------*/

UINT4
IndexMgrGetIndexBasedOnFlag (UINT1 u1GrpID, BOOL1 bFlag)
{
    UINT4               u4Index = 0;
    UINT1               u1BytePos = 0, u1BitPos = 0;
    tIndexMgrGrpInfo   *pIndexMgrGrpInfo = gpIndexMgrGrpInfo;
    tIndexMgrChunkInfo *pIndexMgrChunkInfo = NULL;
    UINT4              *pu4AvailOffset = NULL;

    if (gpIndexMgrGrpInfo == NULL)
    {
        MPLSRTR_DBG (DBG_ERR_CRT,
                     "INDEXMGR :Index Manager is not initialized \n");
        return 0;
    }

    /* Check if indices are available for the mentioned group */
    if (INDEXMGR_GRP_FULL == IndexMgrCheckGrpFull (u1GrpID))
    {
        return 0;
    }

    pIndexMgrGrpInfo += (u1GrpID - 1);
    pIndexMgrChunkInfo = pIndexMgrGrpInfo->pIndexChunkInfoTbl;
    pIndexMgrChunkInfo += pIndexMgrGrpInfo->u4AvailChunkID;
    pu4AvailOffset = pIndexMgrChunkInfo->pu4IndexChunk;
    pu4AvailOffset += pIndexMgrChunkInfo->u4CurOffset;

    /* Now to check each byte of it to get the avail-byte & bit positions */
    if (0 ==
        IndexMgrGetAvailByteBitPos (*pu4AvailOffset, &u1BytePos, &u1BitPos))
    {
        /* This is never possible */
        MPLSRTR_DBG (DBG_ERR_CRT,
                     "INDEXMGR : GetIndex : Byte, bit pos. fail \n");
        return 0;
    }

    /* Now to calculate the index value corresponding to this bit found */
    u4Index =
        (UINT4)((pIndexMgrGrpInfo->u4AvailChunkID * MAX_INDEXMGR_INDICES_PER_CHUNK)
        + (pIndexMgrChunkInfo->u4CurOffset * MAX_INDEXMGR_INDICES_PER_ENTRY)
        + (u1BytePos * INDEXMGR_BYTE_BLOCK_SIZE) + (u1BitPos));

    /* If Index Manager Flag is Set , mark the bit as allocated and adjust to the next available offset */

    if (bFlag == TRUE)
    {

        /* Now to mark that bit as allocated (make it zero) */
        *pu4AvailOffset &=
            aAsgnBitmap[(u1BytePos * INDEXMGR_BYTE_BLOCK_SIZE) + u1BitPos];

        /* Now to increment the allocated-indices count */
        pIndexMgrChunkInfo->u4NumKeysAlloc++;
        pIndexMgrGrpInfo->u4TotalKeysAlloc++;

        /* Now to adjust the next available offset and/or block */
        if (0 == IndexMgrUpdateChunkOffset (u1GrpID))
        {
            /* ideally, shouldn't fail */
            MPLSRTR_DBG (DBG_ERR_CRT,
                         "INDEXMGR : Offset Adjustment Failure \n");
            return 0;
        }
    }

    /* as u4Index is from 1 (& not 0) to max. sprtd. by grp */
    return (u4Index + 1);
}

/*--------------------------------------------------------------------------
 * Function    : IndexMgrGetAvailableIndex 
 * Description : This function returns a unique index for the specified 
 *               group. Returns 0 if all indices for the
 *               specified group have been exhausted.
 * Input       : GroupID (1 = PwIndices, 2 = InSegment Indices, 
 *               3 = OutSegment Indices, 4 = XC Indices,
 *               5 = Lbl Stack Indices, 6 = FTN Indices, 7 = LDP Entity indices,
 *               8 = Max LSP Indices, 9 = VPN Identifiers.). 
 * Outputs     : None
 * Returns     : Unique Index if available (ranging from 1 to max in that 
 *               group), 0 o.w.
--------------------------------------------------------------------------*/

UINT4
IndexMgrGetAvailableIndex (UINT1 u1GrpID)
{
    UINT4               u4Index = 0;
    MPLS_INDEXMGR_LOCK ();
    u4Index = IndexMgrGetIndexBasedOnFlag (u1GrpID, FALSE);
    MPLS_INDEXMGR_UNLOCK ();
    return u4Index;
}

/*--------------------------------------------------------------------------
 * Function    : IndexMgrSetIndexBasedOnFlag 
 * Description : This function returns the Index Value specified , if present .
 *          Returns 0 otherwise.
 * Inputs      : GroupID (1 = PwIndices, 2 = InSegment Indices, 
 *               3 = OutSegment Indices, 4 = XC Indices,
 *               5 = Lbl Stack Indices, 6 = FTN Indices, 7 = LDP Entity indices,
 *               8 = Max LSP Indices, 9 = VPN Identifiers.). 
 *               bFlag(FALSE = 0 ,TRUE=1)
 *               Index Value. 
 * Outputs     : None
 * Returns     : Unique Index if available (ranging from 1 to max in that 
 *               group), 0 o.w.
--------------------------------------------------------------------------*/
UINT4
IndexMgrSetIndexBasedOnFlag (UINT1 u1GrpID, UINT4 u4Index, BOOL1 bFlag)
{
    UINT4               u4BitPos = 0;
    UINT4               u4ChunkID = 0, u4Offset = 0;
    tIndexMgrGrpInfo   *pIndexMgrGrpInfo = gpIndexMgrGrpInfo;
    tIndexMgrChunkInfo *pIndexMgrChunkInfo = NULL;
    UINT4              *pu4AvailOffset = NULL;

    /* First check if the group id  is valid */

    if ((u1GrpID < 1) || (u1GrpID > MAX_INDEXMGR_GRPS_SPRTD))
    {
        MPLSRTR_DBG (DBG_ERR_CRT, "INDEXMGR :INVALID GROUP ID \n");
        return 0;
    }

    /* Check if the index to be set  is valid */
    if ((u4Index < 1) || (u4Index > au4GrpMaxIndices[u1GrpID - 1]))
    {
        MPLSRTR_DBG2 (DBG_ERR_CRT,
                      "INDEXMGR : Set Index FAILURE - %d not in range for Group %d\n",
                      u4Index, u1GrpID);
        return 0;
    }

    pIndexMgrGrpInfo += (u1GrpID - 1);

    /* To Calculate the appropriate bit position based on the Index Value  */

    u4ChunkID = ((u4Index - 1) / MAX_INDEXMGR_INDICES_PER_CHUNK);
    u4Offset =
        (((u4Index -
           1) % MAX_INDEXMGR_INDICES_PER_CHUNK) /
         MAX_INDEXMGR_INDICES_PER_ENTRY);
    u4BitPos =
        (((u4Index -
           1) % MAX_INDEXMGR_INDICES_PER_CHUNK) %
         MAX_INDEXMGR_INDICES_PER_ENTRY);

    /* Now to reach that offset */
    pIndexMgrChunkInfo = pIndexMgrGrpInfo->pIndexChunkInfoTbl;
    pIndexMgrChunkInfo += u4ChunkID;
    pu4AvailOffset = pIndexMgrChunkInfo->pu4IndexChunk;
    pu4AvailOffset += u4Offset;

    /* To Check if this particular bit is already allocated or not. If not allocated , set it . 
     * Otherwise return failure */

    if ((*pu4AvailOffset & (~(aAsgnBitmap[u4BitPos]))) == 0)
    {
        /* Index is Already Allocated */
        return 0;
    }

    else
    {
        if (bFlag == TRUE)
        {
            /* Now to mark that bit as allocated (make it zero) */
            (*pu4AvailOffset) &= aAsgnBitmap[u4BitPos];

            /* Now to increment the allocated-indices count */
            pIndexMgrChunkInfo->u4NumKeysAlloc++;
            pIndexMgrGrpInfo->u4TotalKeysAlloc++;

            /* Now to adjust the next available offset and/or block */
            if (0 == IndexMgrUpdateChunkOffset (u1GrpID))
            {
                /* ideally, shouldn't fail */
                MPLSRTR_DBG (DBG_ERR_CRT,
                             "INDEXMGR : Offset Adjustment Failure \n");
                return 0;
            }

        }

    }

    return u4Index;
    }

/*--------------------------------------------------------------------------
 * Function    : IndexMgrSetIndex 
 * Description : This function returns the Index Value specified , if present .
 *          Returns 0 otherwise.
 * Inputs       : GroupID (1 = PwIndices, 2 = InSegment Indices, 
 *               3 = OutSegment Indices, 4 = XC Indices,
 *               5 = Lbl Stack Indices, 6 = FTN Indices, 7 = LDP Entity indices,
 *               8 = Max LSP Indices, 9 = VPN Identifiers.). 
 *               Index Value. 
 * Outputs     : None
 * Returns     : Unique Index if available (ranging from 1 to max in that 
 *               group), 0 o.w.
--------------------------------------------------------------------------*/

UINT4
IndexMgrSetIndex (UINT1 u1GrpID, UINT4 u4Index)
{
    UINT4               u4SetIndex = 0;
    MPLS_INDEXMGR_LOCK ();
    u4SetIndex = IndexMgrSetIndexBasedOnFlag (u1GrpID, u4Index, TRUE);
    MPLS_INDEXMGR_UNLOCK ();
    return u4SetIndex;
}

/*--------------------------------------------------------------------------
 * Function    : IndexMgrCheckIndex 
 * Description : This function returns the Index Value specified , if present .
 *          Returns 0 otherwise.
 * Inputs       : GroupID (1 = PwIndices, 2 = InSegment Indices, 
 *               3 = OutSegment Indices, 4 = XC Indices,
 *               5 = Lbl Stack Indices, 6 = FTN Indices, 7 = LDP Entity indices,
 *               8 = Max LSP Indices, 9 = VPN Identifiers.).
 *               Index value
 * Outputs     : None
 * Returns     : Unique Index if available (ranging from 1 to max in that 
 *               group), 0 o.w.
--------------------------------------------------------------------------*/

UINT4
IndexMgrCheckIndex (UINT1 u1GrpID, UINT4 u4Index)
{
    UINT4               u4CheckIndex = 0;
    MPLS_INDEXMGR_LOCK ();
    u4CheckIndex = IndexMgrSetIndexBasedOnFlag (u1GrpID, u4Index, FALSE);
    MPLS_INDEXMGR_UNLOCK ();
    return u4CheckIndex;
}

/*---------------------------------------------------------------------------
 * Function    : IndexMgrRelIndex 
 * Description : This function releases an index to the specified group. 
 * Inputs       : GroupID (1 = PwIndices, 2 = InSegment Indices, 
 *               3 = OutSegment Indices, 4 = XC Indices,
 *               5 = Lbl Stack Indices, 6 = FTN Indices, 7 = LDP Entity indices,
 *               8 = Max LSP Indices, 9 = VPN Identifiers.).
 *               Index value
 * Outputs     : None
 * Returns     : MPLS_SUCCESS if able to 
 *               0 otherwise.
---------------------------------------------------------------------------*/

UINT1
IndexMgrRelIndex (UINT1 u1GrpID, UINT4 u4Index)
{
    UINT4               u4BitPos = 0;
    UINT4               u4ChunkID = 0, u4Offset = 0;
    tIndexMgrGrpInfo   *pIndexMgrGrpInfo = gpIndexMgrGrpInfo;
    tIndexMgrChunkInfo *pIndexMgrChunkInfo = NULL;
    UINT4              *pu4AvailOffset = NULL;

    /* First check if the group id  is valid */

    if ((u1GrpID < 1) || (u1GrpID > MAX_INDEXMGR_GRPS_SPRTD))
    {
        MPLSRTR_DBG (DBG_ERR_CRT, "INDEXMGR :INVALID GROUP ID  \n");
        return 0;
    }

    MPLS_INDEXMGR_LOCK ();

    pIndexMgrGrpInfo += (u1GrpID - 1);

    /* Check if the index to be released is valid */
    if ((u4Index < 1) || (u4Index > au4GrpMaxIndices[u1GrpID - 1]))
    {
        MPLSRTR_DBG2 (DBG_ERR_CRT,
                      "INDEXMGR : Rel Index FAILURE - %d not in range for Group %d\n",
                      u4Index, u1GrpID);
        MPLS_INDEXMGR_UNLOCK ();
        return 0;
    }

    /* OK to release an already released index (situation shouldn't occur) */

    u4ChunkID = ((u4Index - 1) / MAX_INDEXMGR_INDICES_PER_CHUNK);
    u4Offset =
        (((u4Index -
           1) % MAX_INDEXMGR_INDICES_PER_CHUNK) /
         MAX_INDEXMGR_INDICES_PER_ENTRY);
    u4BitPos =
        (((u4Index -
           1) % MAX_INDEXMGR_INDICES_PER_CHUNK) %
         MAX_INDEXMGR_INDICES_PER_ENTRY);

    /* Now to reach that offset */
    pIndexMgrChunkInfo = pIndexMgrGrpInfo->pIndexChunkInfoTbl;
    pIndexMgrChunkInfo += u4ChunkID;
    pu4AvailOffset = pIndexMgrChunkInfo->pu4IndexChunk;
    pu4AvailOffset += u4Offset;

    /* Now to make this particular bit zero */
    (*pu4AvailOffset) |= aAvlBitmap[u4BitPos];

    /* Now to readjust the available-offset order of these 2 if's imp */
    if (MAX_INDEXMGR_INDICES_PER_CHUNK == pIndexMgrChunkInfo->u4NumKeysAlloc)
    {
        /* offset was positioned at start, so now adjust it */
        pIndexMgrChunkInfo->u4CurOffset = u4Offset;
    }

    if (u4Offset < (pIndexMgrChunkInfo->u4CurOffset))
    {
        pIndexMgrChunkInfo->u4CurOffset = u4Offset;
    }

    /* Now to readjust the available-chunkID (order of these 2 if's is imp) */
    if (pIndexMgrGrpInfo->u4TotalKeysAlloc == au4GrpMaxIndices[u1GrpID - 1])
    {
        /* Available chunk was positioned at start, so now readjust */
        pIndexMgrGrpInfo->u4AvailChunkID = u4ChunkID;
    }

    if (u4ChunkID < (pIndexMgrGrpInfo->u4AvailChunkID))
    {
        pIndexMgrGrpInfo->u4AvailChunkID = u4ChunkID;
    }

    /* Now to decrement the no. of keys allocated */
    (pIndexMgrChunkInfo->u4NumKeysAlloc)--;
    (pIndexMgrGrpInfo->u4TotalKeysAlloc)--;

    MPLS_INDEXMGR_UNLOCK ();
    return MPLS_SUCCESS;
}

/*------------------------------------------------------------------------
* Function    : IndexMgrInitGroups
* Description : Initializes the fields of all the group structures involved
* Inputs      : None
* Outputs     : None
* Returns     : MPLS_SUCCESS/MPLS_FAILURE 
-------------------------------------------------------------------------*/

UINT1
IndexMgrInitGrps ()
{
    UINT1               u1Index = 0;
    tIndexMgrGrpInfo   *pIndexMgrGrpInfo = gpIndexMgrGrpInfo;

    for (u1Index = 0; u1Index < MAX_INDEXMGR_GRPS_SPRTD;
         u1Index++, pIndexMgrGrpInfo++)
    {
        pIndexMgrGrpInfo->u4NumChunksInGrp = 0;
        pIndexMgrGrpInfo->u4AvailChunkID = 0;
        pIndexMgrGrpInfo->u4TotalKeysAlloc = 0;
        pIndexMgrGrpInfo->pIndexChunkInfoTbl = NULL;

        pIndexMgrGrpInfo->u4MaxChunksSprtd =
            (((au4GrpMaxIndices[u1Index] % MAX_INDEXMGR_INDICES_PER_CHUNK) == 0)
             ? (au4GrpMaxIndices[u1Index] / MAX_INDEXMGR_INDICES_PER_CHUNK)
             : (au4GrpMaxIndices[u1Index] / MAX_INDEXMGR_INDICES_PER_CHUNK +
                1));
    }

    return MPLS_SUCCESS;
}

/*---------------------------------------------------------------------------
 * Function    : IndexMgrUpdateChunkOffset 
 * Description : This function searches for a new offset for allocation of 
 *               index in the same chunk that delivered the latest index. 
 *               If this chunk is full, it proceeds with other chunks.
 * Inputs       : GroupID (1 = PwIndices, 2 = InSegment Indices, 
 *               3 = OutSegment Indices, 4 = XC Indices,
 *               5 = Lbl Stack Indices, 6 = FTN Indices, 7 = LDP Entity indices,
 *               8 = Max LSP Indices, 9 = VPN Identifiers.).
 *               Index value
 * Outputs     : None
 * Returns     : MPLS_SUCCESS if updation successful 
 *               0 otherwise.
---------------------------------------------------------------------------*/

UINT1
IndexMgrUpdateChunkOffset (UINT1 u1GrpID)
{
    UINT4               u4MaxIndices = 0, u4Index = 0;
    tIndexMgrGrpInfo   *pIndexMgrGrpInfo = gpIndexMgrGrpInfo;
    tIndexMgrChunkInfo *pIndexMgrChunkInfo = NULL;
    UINT4              *pu4AvailOffset = NULL;

    if ((u1GrpID < 1) || (u1GrpID > MAX_INDEXMGR_GRPS_SPRTD))
    {
        return MPLS_FAILURE;
    }

    pIndexMgrGrpInfo += (u1GrpID - 1);
    pIndexMgrChunkInfo = pIndexMgrGrpInfo->pIndexChunkInfoTbl;
    pIndexMgrChunkInfo += pIndexMgrGrpInfo->u4AvailChunkID;

    if (MAX_INDEXMGR_INDICES_PER_CHUNK == pIndexMgrChunkInfo->u4NumKeysAlloc)
    {
        /* This chunk is full */
        pIndexMgrChunkInfo->u4CurOffset = 0;    /* reposition to 1st */

        u4MaxIndices = au4GrpMaxIndices[u1GrpID - 1];

        if (u4MaxIndices == pIndexMgrGrpInfo->u4TotalKeysAlloc)
        {
            /* All chunks are full */
            pIndexMgrGrpInfo->u4AvailChunkID = 0;
            /* Reposition to start */
            /* Nothing else needs to be done */
            return MPLS_SUCCESS;
        }
        else
        {
            /* Search for a new Chunk from next chunk */
            pu4AvailOffset = pIndexMgrChunkInfo->pu4IndexChunk;
            pu4AvailOffset += pIndexMgrChunkInfo->u4CurOffset;

            for (u4Index = ((pIndexMgrGrpInfo->u4AvailChunkID) + 1);
                 u4Index < pIndexMgrGrpInfo->u4NumChunksInGrp; u4Index++)
            {
                pIndexMgrChunkInfo++;

                if (pIndexMgrChunkInfo->u4NumKeysAlloc
                    < MAX_INDEXMGR_INDICES_PER_CHUNK)
                {
                    /* This chunk is available */
                    pIndexMgrGrpInfo->u4AvailChunkID = u4Index;
                    return MPLS_SUCCESS;
                }
            }
        }
    }
    else                        /* search for an offset in this chunk itself */
    {
        pu4AvailOffset = pIndexMgrChunkInfo->pu4IndexChunk;
        pu4AvailOffset += pIndexMgrChunkInfo->u4CurOffset;

        for (u4Index = pIndexMgrChunkInfo->u4CurOffset;
             u4Index < MAX_INDEXMGR_ENT_PER_CHUNK; u4Index++, pu4AvailOffset++)
        {
            if ((*pu4AvailOffset) != 0)
            {
                pIndexMgrChunkInfo->u4CurOffset = u4Index;
                break;
            }
        }
    }

    return MPLS_SUCCESS;
}

/*---------------------------------------------------------------------------
 * Function    : IndexMgrCheckGrpFull 
 * Description : This function checks if indices are available for the 
 *               specified group. 
 * Inputs       : GroupID (1 = PwIndices, 2 = InSegment Indices, 
 *               3 = OutSegment Indices, 4 = XC Indices,
 *               5 = Lbl Stack Indices, 6 = FTN Indices, 7 = LDP Entity indices,
 *               8 = Max LSP Indices, 9 = VPN Identifiers.).
 *               Index value
 * Outputs     : None
 * Returns     : INDEXMGR_GRP_FULL if all indices allocated 
 *               INDEXMGR_GRP_VACANT otherwise.
---------------------------------------------------------------------------*/

UINT1
IndexMgrCheckGrpFull (UINT1 u1GrpID)
{
    tIndexMgrGrpInfo   *pIndexMgrGrpInfo = gpIndexMgrGrpInfo;

    /* First check if the group id  is valid */
    if ((u1GrpID < 1) || (u1GrpID > MAX_INDEXMGR_GRPS_SPRTD))
    {
        MPLSRTR_DBG (DBG_ERR_CRT, "INDEXMGR :INVALID GROUP ID \n");
        return INDEXMGR_GRP_FULL;
    }

    pIndexMgrGrpInfo += (u1GrpID - 1);

    if (pIndexMgrGrpInfo->u4TotalKeysAlloc == au4GrpMaxIndices[u1GrpID - 1])
    {
        MPLSRTR_DBG (DBG_ERR_CRT,
                     "INDEXMGR : Index Unavailable : Group full \n");
        return INDEXMGR_GRP_FULL;
    }

    return INDEXMGR_GRP_VACANT;
}

/*---------------------------------------------------------------------------
 * Function    : IndexMgrGetAvailByteBitPos
 * Description : This function inspects the UINT4 Value to find the 1st bit 
 *               position which is allocatable (1).
 * Inputs      : u4AvailIndexBitmap : U4 value to inspect in.
 * Outputs     : pu1BytePos : Shall contain position of the byte holding the
 *               1st allocatable bit pos. (0 to 3)
 *               pu1BitPos : Bit pos. of allocatable bit in the byte.(0-7)   
 * Returns     : MPLS_SUCCESS if able to 
 *               0 otherwise. (should never happen)
---------------------------------------------------------------------------*/

UINT1
IndexMgrGetAvailByteBitPos (UINT4 u4AvailIndexBitmap, UINT1 *pu1BytePos,
                            UINT1 *pu1BitPos)
{
    /*if no bit available(u4AvailIndexBitmap == 0), return failure */
    if (0 == u4AvailIndexBitmap)    /* Ideally should never occur */
    {
        return 0;
    }

    /* Now to check every byte to get a (bit = 1) */
    for (*pu1BytePos = 0; *pu1BytePos < sizeof (u4AvailIndexBitmap);
         (*pu1BytePos)++)
    {
        if (u4AvailIndexBitmap & aAvlByteBmap[*pu1BytePos])
        {
            break;
        }
    }

    if (*pu1BytePos == sizeof (u4AvailIndexBitmap))
    {
        return MPLS_FAILURE;
    }

    /* Now to get the bit position */
    for (*pu1BitPos = 0; *pu1BitPos < INDEXMGR_BYTE_BLOCK_SIZE; (*pu1BitPos)++)
    {
        if (u4AvailIndexBitmap &
            aAvlBitmap[((*pu1BytePos) * INDEXMGR_BYTE_BLOCK_SIZE)
                       + (*pu1BitPos)])
        {
            break;
        }
    }

    return MPLS_SUCCESS;
}

/*--------------------------------------------------------------------------
FAQ on Index manager Library:
1) Scope and usage of this library.
2) How do we initialise this libarary.
3) Configuration and Debug support available in this libarary.
---------------------------------------------------------------------------*/
/*                        End of file indexmgr.c                           */
/*-------------------------------------------------------------------------*/
