/*---------------------------------------------------------------------------*/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: mplsapi.c,v 1.17 2016/02/18 09:48:07 siva Exp $
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : mplsapi.c
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : MPLS
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  : 
 *    DESCRIPTION            : This file contains exported APIs that are 
 *                             used by external modules of MPLS.
 *---------------------------------------------------------------------------*/

#include "mplsincs.h"
#include "mplsapi.h"
#include "mplcmndb.h"
#include "mplsftn.h"
#include "mplslsr.h"
#include "l2vpextn.h"
#include "oaminc.h"
#include "arp.h"
#include "mpoamdef.h"
#include "utlmacro.h"
#include "l2vpincs.h"

/*****************************************************************************/
/* Function     : MplsApiHandleExternalRequest                               */
/*                                                                           */
/* Description  : This function gets/sets the MPLS information based on the  */
/*                request from MPLS external module.                         */
/*                                                                           */
/* Input        : u4MainReqType - Request type                               */
/*                pInMplsApiInfo - Pointer to the input API information.     */
/*                                                                           */
/* Output       : pOutMplsApiInfo - Pointer to the output API information.   */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
MplsApiHandleExternalRequest (UINT4 u4MainReqType,
                              tMplsApiInInfo * pInMplsApiInfo,
                              tMplsApiOutInfo * pOutMplsApiInfo)
{
    INT4                i4RetVal = OSIX_FAILURE;
    BOOL1               bIsTnlOrLspExists = FALSE;

    tPwVcEntry         *pPwVcEntry = NULL;
    UINT4               u4PeerAddr = 0;
    UINT4               u4LsppPeerAddr = 0;
    UINT4               u4LsppPwVcId = 0;

    if (gu1MplsInitialised != TRUE)
    {
        return OSIX_FAILURE;
    }

    if (pInMplsApiInfo == NULL)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsApiHandleExternalRequest: "
                    "Provide valid in API information\r\n");
        return OSIX_FAILURE;
    }
    /* MPLS is not instantiated for multiple contexts, 
     * only default context is allowed now */
    if (pInMplsApiInfo->u4ContextId != MPLS_DEF_CONTEXT_ID)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsApiHandleExternalRequest: "
                    "Only MPLS default context is supported: " "INTMD-EXIT \n");
        return OSIX_FAILURE;
    }
    /* External requests handling */
    switch (u4MainReqType)
    {
        case MPLS_OAM_HANDLE_PATH_STATUS_CHG:
            /* This API is called from BFD module. 
             * Applications (ELPS, other future applications) which 
             * are registered with OAM will be notified about the path status.
             *
             * Inputs: 
             *   u4SubReqType = 0, u4ContextId = 0, u4SrcModId = BFD_MODULE,
             *   tMplsPathId = TNL/PW/MEG, 
             *   tMplsOamPathStatus = MPLS_PATH_STATUS_UP/MPLS_PATH_STATUS_DOWN 
             * */
            if (pInMplsApiInfo->InPathId.u4PathType == MPLS_PATH_TYPE_PW)
            {
                /* Update Session index in PW table
                 * pInMplsApiInfo->InPathId.PwId.PeerAddr.MplsRouterId.u4_addr
                 * pInMplsApiInfo->InPathId.PwId.u4VcId
                 *
                 * pInMplsApiInfo->InOamPathStatus.u1PathStatus
                 * */
                i4RetVal = L2VpnApiHandleExternalRequest
                    (MPLS_OAM_HANDLE_PATH_STATUS_CHG, pInMplsApiInfo, NULL);
            }
            else
            {
                MPLS_CMN_LOCK ();
                i4RetVal = MplsOamIfHandlePathStatusChange (pInMplsApiInfo,
                                                            pOutMplsApiInfo);
                MPLS_CMN_UNLOCK ();
            }
            break;

        case MPLS_OAM_UPDATE_SESSION_PARAMS:
            /* This API is called from BFD module to update the 
             * proactive session index for path (MEG/LSP/TNL/PW) 
             *
             * Inputs:
             *   u4SubReqType = 0, u4ContextId = 0, u4SrcModId = BFD_MODULE,
             *   tMplsPathId = MEG/LSP/TNL/PW,
             *   tMplsOamSessionParams = BFD session index
             * */
            if (pInMplsApiInfo->InPathId.u4PathType == MPLS_PATH_TYPE_PW)
            {
                i4RetVal =
                    L2VpnApiHandleExternalRequest
                    (MPLS_OAM_UPDATE_SESSION_PARAMS, pInMplsApiInfo, NULL);
            }
            else
            {
                MPLS_CMN_LOCK ();
                i4RetVal = MplsOamIfUpdateSessionParams (pInMplsApiInfo);
                MPLS_CMN_UNLOCK ();
            }
            break;

        case MPLS_OAM_REGISTER_MEG_FOR_NOTIF:
            /* This is called  from BFD, LSP Ping and ELPS modules.
             * This registration is for 
             * 1. Fault notifications
             * 2. Application specific Packet delivery
             *
             * Inputs:
             *   u4SubReqType = MPLS_APP_REGISTER/MPLS_APP_DEREGISTER,
             *   u4ContextId = 0, u4SrcModId = ELPS_MODULE,
             *   tMplsRegParams (u4Events = Interested events, 
             *   Callback function)
             * */
            MPLS_CMN_LOCK ();
            i4RetVal = MplsOamIfRegWithMegForNotif (pInMplsApiInfo);
            MPLS_CMN_UNLOCK ();
            break;
#ifdef HVPLS_WANTED
	case MPLS_REGISTER_PW_FOR_NOTIF:
	    /* This Request is triggered by ERPS Modue
	     *                Registration is for Fault Notification (PW UP/ DOWN)*/
	    i4RetVal = L2VpnApiHandleExternalRequest (MPLS_REGISTER_PW_FOR_NOTIF,
			    pInMplsApiInfo,
			    NULL);
	    break;

#endif

        case MPLS_PACKET_HANDLE_FROM_APP:
            /* This is called from BFD, LSP Ping and ELPS module for 
             * application specific packet transmission.
             * 
             * Inputs:
             *   u4SubReqType = 0, u4ContextId = 0, 
             *   u4SrcModId = BFD/LSP Ping/ELPS,
             *   tMplsPktInfo - Packet to be transmitted.
             * 
             * 1. Numbered interface: Resolve ARP if NextHop mac address 
             *                        doesn't exist for NextHopIP
             * 2. Un-numbered interface: Get the peer mac address from CFA for
             *                           un-numbered interface 
             * */
            /* Semaphore is not required for MPLS packets transmission 
             * as MPLS headers are already constructed by the applications */
            i4RetVal = MplsOamTxPacketHandleFromApp (pInMplsApiInfo);
            break;

        case MPLS_OAM_GET_MEG_INFO:
            /* 1. Inputs: 
             *      u4SubReqType = MPLS_GET_MEG_ID_FROM_MEG_NAME, 
             *      u4ContextId = 0, 
             *      u4SrcModId = BFD/LSP Ping
             *      tMplsPathId (MEG name, ME name)
             *    Outputs: 
             *      u4ContextId = 0,
             *      u4PathType = 0,
             *      tMplsPathId (MEG index, ME index, MP index)
             *
             * 2. Inputs: 
             *      u4SubReqType = MPLS_GET_MEG_NAME_FROM_MEG_ID, 
             *      u4ContextId = 0, 
             *      u4SrcModId = BFD/LSP Ping
             *      tMplsPathId (MEG index, ME index, MP index)
             *    Outputs: 
             *      u4ContextId = 0,
             *      u4PathType = 0,
             *      tMplsPathId (MEG index, ME index, MP index)
             *
             * 3. Inputs: 
             *      u4SubReqType = MPLS_GET_MEG_INFO_FROM_MEG_ID, 
             *      u4ContextId = 0, 
             *      u4SrcModId = BFD/LSP Ping
             *      tMplsPathId (MEG index, ME index, MP index)
             *    Outputs: 
             *      u4ContextId = 0,
             *      u4PathType = 0,
             *      tMplsMegApiInfo - MEG, ME informations
             ** 4. Inputs: 
             *      u4SubReqType = MPLS_GET_MEG_FROM_SRC_ICC_MEP, 
             *      u4ContextId = 0, 
             *      u4SrcModId = BFD/LSP Ping
             *      tMplsPathId (ICC, UMC and MEP index)
             *    Outputs: 
             *      u4ContextId = 0,
             *      u4PathType = 0,
             *      tMplsMegApiInfo - MEG, ME informations

             *    */
            MPLS_CMN_LOCK ();
            i4RetVal = MplsOamIfGetMegInfo (pInMplsApiInfo, pOutMplsApiInfo);
            MPLS_CMN_UNLOCK ();
            break;

        case MPLS_GET_TUNNEL_INFO:
            /* Inputs: 
             *   u4SubReqType = 0,
             *   u4ContextId = 0,
             *   u4SrcModId = BFD/LSP Ping/ELPS,
             *   tMplsPathId (Tunnel number and/or Tunnel instance 
             *   and/or Tunnel ingressid and/or Tunnel egressId and 
             *   Address type)
             * Outputs: 
             *     u4ContextId = 0,
             *     u4PathType = 0,
             *     tTeTnlApiInfo - Tunnel information [Tunnel, XC informations] 
             *    */
            MPLS_CMN_LOCK ();
            i4RetVal =
                MplsCmnExtGetTunnelInfo (pInMplsApiInfo, pOutMplsApiInfo);
            MPLS_CMN_UNLOCK ();
            break;

        case MPLS_GET_PW_INFO:
            /* 1. Inputs:
             *      u4SubReqType = MPLS_GET_PW_INFO_FROM_PW_INDEX,
             *      u4ContextId = 0,
             *      u4SrcModId = BFD/LSP Ping,
             *      tMplsPathId (tPwPathId - u4PwIndex)      
             *    Outputs: 
             *      u4ContextId = 0,
             *      u4PathType = 0,
             *      tPwApiInfo - PW informations
             * 2. Inputs: Peer Address, VC-Id
             *      u4SubReqType = MPLS_GET_PW_INFO_FROM_VCID,
             *      u4ContextId = 0,
             *      u4SrcModId = BFD/LSP Ping/ELPS,
             *      tMplsPathId (tPwPathId - PeerAddr, u4VcId)      
             *    Outputs:
             *      u4ContextId = 0,
             *      u4PathType = 0,
             *      tPwApiInfo - PW informations
             * 3. Inputs: 
             *      u4SubReqType = MPLS_GET_PW_INFO_FROM_GENTYPE2,
             *      u4ContextId = 0,
             *      u4SrcModId = LSP Ping,
             *      tMplsPathId (tPwPathId - AGI, Src GlobalId, Src NodeId, 
             *      Src AcId, Dst GlobalId, Dst NodeId, Dst AcId
             *    Outputs:
             *      u4ContextId = 0,
             *      u4PathType = 0,
             *      tPwApiInfo - PW informations
             * 4. Inputs: 
             *      u4SubReqType = MPLS_GET_PW_INFO_FROM_GENFEC,
             *      u4ContextId = 0,
             *      u4SrcModId = LSP Ping,
             *      tMplsPathId (tPwPathId - Pw FEC type, sender address, 
             *      remote address, Pw type, AGI type, AGI value, 
             *      local AII type,
             *      remote AII type, SAII, TAII
             *    Outputs:
             *      u4ContextId = 0,
             *      u4PathType = 0,
             *      tPwApiInfo - PW informations
             * 5. Inputs: 
             *      u4SubReqType = MPLS_GET_PW_INFO_FROM_PWIDFEC,
             *      u4ContextId = 0,
             *      u4SrcModId = LSP Ping,
             *      tMplsPathId (tPwPathId - Pw FEC type, sender address, 
             *      remote address, Pw type, u4VcId
             *    Outputs:
             *      u4ContextId = 0,
             *      u4PathType = 0,
             *      tPwApiInfo - PW informations
             * */
            i4RetVal = L2VpnApiHandleExternalRequest (MPLS_GET_PW_INFO,
                                                      pInMplsApiInfo,
                                                      pOutMplsApiInfo);
            break;

        case MPLS_GET_LSP_INFO:
            /* NOT SUPPORTED - XC information fetch is yet to be decided.
             * 1. Inputs: 
             *      u4SubReqType = MPLS_GET_FTN_INDEX_FROM_FEC,
             *      u4ContextId = 0,
             *      u4SrcModId = BFD/LSP Ping,
             *      tMplsPathId (tMplsLspId - PeerAddr, u4AddrType, u4AddrLen)      
             *    Outputs:
             *      u4ContextId = 0,
             *      u4PathType = 0,
             *      tMplsLspId (FTN index)  
             * 2. Inputs: 
             *      u4SubReqType = MPLS_GET_LSP_INFO_FROM_FTN_INDEX,
             *      u4ContextId = 0,
             *      u4SrcModId = BFD/LSP Ping,
             *      tMplsPathId (tMplsLspId - PeerAddr, u4AddrType, u4AddrLen)     
             *    Outputs:
             *      u4ContextId = 0,
             *      u4PathType = 0,
             *      tMplsXcApiInfo - Cross connect information
             * 3. Inputs: 
             *      u4SubReqType = MPLS_GET_LSP_INFO_FROM_XC_INDEX,
             *      u4ContextId = 0,
             *      u4SrcModId = BFD/LSP Ping,
             *      tMplsPathId (tMplsLspId - XC index)     
             *    Outputs:
             *      u4ContextId = 0,
             *      u4PathType = 0,
             *      tMplsXcApiInfo - Cross connect information
             *    */
            MPLS_CMN_LOCK ();
            i4RetVal = MplsCmnExtGetLspInfo (pInMplsApiInfo, pOutMplsApiInfo);
            MPLS_CMN_UNLOCK ();
            break;

        case MPLS_GET_NODE_ID:
            /* 1. Inputs: 
             *      u4SubReqType = MPLS_GET_ROUTER_ID,
             *      u4ContextId = 0,
             *      u4SrcModId = LSP Ping/BFD
             *    Outputs:
             *      u4ContextId = 0,
             *      u4PathType = 0,
             *      tMplsGlobalNodeId - Router Identifier
             *
             * 2. Inputs: 
             *      u4SubReqType = MPLS_GET_GLOBAL_NODE_ID,
             *      u4ContextId = 0,
             *      u4SrcModId = LSP Ping/BFD
             *    Outputs:
             *      u4ContextId = 0,
             *      u4PathType = 0,
             *      tMplsGlobalNodeId - Node Identifier
             *
             * 3. Inputs: 
             *      u4SubReqType = MPLS_GET_GLBNODEID_FROM_LOCALNUM,
             *      u4ContextId = 0,
             *      u4SrcModId = LSP Ping/BFD/ELPS
             *      u4LocalMapNum = Local Map number
             *    Outputs:
             *      u4ContextId = 0,
             *      u4PathType = 0,
             *      tMplsGlobalNodeId - Global Node Identifier
             *
             * 4. Inputs: 
             *      u4SubReqType = MPLS_GET_LOCALNUM_FROM_GLBNODEID,
             *      u4ContextId = 0,
             *      u4SrcModId = LSP Ping/BFD/ELPS
             *      tMplsGlobalNodeId - Global Node Identifier
             *    Outputs:
             *      u4ContextId = 0,
             *      u4PathType = 0,
             *      u4LocalMapNum = Local Map number
             *    */
            MPLS_CMN_LOCK ();
            i4RetVal = MplsCmnExtGetNodeId (pInMplsApiInfo, pOutMplsApiInfo);
            MPLS_CMN_UNLOCK ();
            break;

        case MPLS_GET_PATH_FROM_INLBL_INFO:
            /* Inputs: 
             *   u4SubReqType = 0,
             *   u4ContextId = 0,
             *   u4SrcModId = BFD/LSP Ping,
             *   tMplsInLblInfo - In-interface and in-label information
             * Outputs:
             *   u4ContextId = 0,
             *   u4PathType - MPLS_PATH_TYPE_TNL/MPLS_PATH_TYPE_LSP/
             *   MPLS_PATH_TYPE_PW/
             *   MPLS_PATH_TYPE_MEG_ID,
             *   tTeTnlApiInfo/tNonTeApiInfo/tPwApiInfo/tMplsMegApiInfo 
             *   - Forward path information
             *    */
            /* First search the insegment table, 
             * if entry doesnot exist - Provide the lsp/tunnel information
             * else search in the PW table and provide the PW information.
             * if none of the table matches for the in interface and in label
             * return failure.
             * */
            if (pOutMplsApiInfo == NULL)
            {
                MPLSFM_DBG (MPLSFM_IF_FAILURE,
                            "MplsApiHandleExternalRequest: "
                            "OUT API information should not be NULL"
                            ": INTMD-EXIT \n");
                return OSIX_FAILURE;
            }

            MPLS_CMN_LOCK ();
            i4RetVal = MplsCmnExtGetPathFromInLblInfo (pInMplsApiInfo,
                                                       pOutMplsApiInfo,
                                                       &bIsTnlOrLspExists);
            MPLS_CMN_UNLOCK ();
            /* Search the in label and in interface matching PW */
            if ((i4RetVal == OSIX_SUCCESS) && (bIsTnlOrLspExists == FALSE))
            {
                /* Return MEG information if MEG is enabled for Tunnel/PW */
                /* Search the inlabel in PW table and return the PW 
                 * information */
                i4RetVal = L2VpnApiHandleExternalRequest
                    (MPLS_GET_PATH_FROM_INLBL_INFO,
                     pInMplsApiInfo, pOutMplsApiInfo);

            }
            break;

        case MPLS_GET_REV_PATH_FROM_FWD_PATH:
            /* Inputs: 
             *   u4SubReqType = 0,
             *   u4ContextId = 0,
             *   u4SrcModId = BFD/LSP Ping,
             *   tMplsNodeId - Source IP information
             * Outputs:
             *   u4ContextId = 0,
             *   u4PathType - MPLS_PATH_TYPE_TNL/MPLS_PATH_TYPE_LSP
             *   tTeTnlApiInfo/tNonTeApiInfo - Reverse path information 
             *                                 if exists.
             *    */
            MPLS_CMN_LOCK ();
            i4RetVal = MplsCmnExtGetRevPathFromFwdPath (pInMplsApiInfo,
                                                        pOutMplsApiInfo);
            MPLS_CMN_UNLOCK ();
            break;

        case MPLS_GET_SERVICE_POINTER_OID:
            /* 1. Inputs: 
             *      u4SubReqType = MPLS_GET_BASE_OID_FROM_MEG_NAME, 
             *      u4ContextId = 0, 
             *      u4SrcModId = BFD/LSP Ping/ELPS
             *    Outputs: 
             *      u4ContextId = 0,
             *      u4PathType = 0,
             *      tMplsServiceOid - First accessible columnar object 
             *      of ME table
             *  
             *  2. Inputs:
             *       u4SubReqType = MPLS_GET_BASE_OID_FROM_TNL_INDEX,
             *       u4ContextId = 0,
             *       u4SrcModId = BFD/LSP Ping/ELPS,
             *       tMplsPathId (Tunnel number and/or Tunnel instance 
             *       and/or Tunnel ingressid and/or Tunnel egressId 
             *       and Address type)
             *     Outputs: 
             *       u4ContextId = 0,
             *       u4PathType = 0,
             *       tMplsServiceOid - First accessible columnar object of 
             *       Tunnel table
             *
             *  3. Inputs: 
             *       u4SubReqType = MPLS_GET_BASE_OID_FROM_FEC,
             *       u4ContextId = 0,
             *       u4SrcModId = BFD/LSP Ping,
             *       tMplsPathId (tMplsLspId - PeerAddr, u4AddrType, u4AddrLen) 
             *     Outputs:
             *       u4ContextId = 0,
             *       u4PathType = 0,
             *       tMplsServiceOid - First accessible columnar object of 
             *                         FTN table
             *
             *  4. Inputs: Peer Address, VC-Id
             *      u4SubReqType = MPLS_GET_BASE_OID_FROM_VCID,
             *      u4ContextId = 0,
             *      u4SrcModId = BFD/LSP Ping/ELPS,
             *      tMplsPathId (tPwPathId - PeerAddr, u4VcId)      
             *    Outputs:
             *       u4ContextId = 0,
             *       u4PathType = 0,
             *       tMplsServiceOid - First accessible columnar object
             *                         of PW table
             *
             *  5. Inputs:
             *       u4SubReqType = MPLS_GET_BASE_PW_OID/MPLS_GET_BASE_FTN_OID/
             *                      MPLS_GET_BASE_TNL_OID/MPLS_GET_BASE_MEG_OID,
             *       u4ContextId = 0,
             *       u4SrcModId = BFD/LSP Ping/ELPS,
             *     Outputs: 
             *       u4ContextId = 0,
             *       u4PathType = 0,
             *       tMplsServiceOid - First accessible columnar object of
             *                         ME/FTN/TNL/PW table except indices
             *    */
            if ((pInMplsApiInfo->u4SubReqType == MPLS_GET_BASE_OID_FROM_VCID) ||
                (pInMplsApiInfo->u4SubReqType == MPLS_GET_BASE_PW_OID))
            {
                if ( pInMplsApiInfo->u4SrcModId == LSPPING_MODULE)
                {
                    /* VCID of the PW must be of correct neighbour address */

                    MEMCPY(&u4LsppPeerAddr, pInMplsApiInfo->InPathId.PwId.DstNodeId.MplsRouterId.u1_addr,
                            sizeof (UINT4));
                    u4LsppPwVcId = pInMplsApiInfo->InPathId.PwId.u4VcId;

                    pPwVcEntry = L2VpnGetPwVcEntryFromVcId (u4LsppPwVcId);
                    if (pPwVcEntry == NULL)
                    {
                        i4RetVal = L2VpnApiHandleExternalRequest
                            (MPLS_GET_SERVICE_POINTER_OID, pInMplsApiInfo,
                             pOutMplsApiInfo);
                        break;
                    }

                    MEMCPY (&u4PeerAddr, &pPwVcEntry->PeerAddr, sizeof (UINT4));

                    if( u4LsppPeerAddr != u4PeerAddr )
                    {
                        i4RetVal =  OSIX_FAILURE;
                        break;
                    }
                }


                i4RetVal = L2VpnApiHandleExternalRequest
                    (MPLS_GET_SERVICE_POINTER_OID, pInMplsApiInfo,
                     pOutMplsApiInfo);
            }
            else
            {
                MPLS_CMN_LOCK ();
                i4RetVal = MplsCmnExtGetServicePointerOid (pInMplsApiInfo,
                                                           pOutMplsApiInfo);
                MPLS_CMN_UNLOCK ();
            }
            break;
        case MPLS_OAM_UPDATE_LPS_STATUS:
            if (pInMplsApiInfo->InPathId.u4PathType == MPLS_PATH_TYPE_PW)
            {
                i4RetVal =
                    L2VpnApiHandleExternalRequest (MPLS_OAM_UPDATE_LPS_STATUS,
                                                   pInMplsApiInfo, NULL);
            }
            else
            {
                MPLS_CMN_LOCK ();
                i4RetVal = MplsOamIfUpdateLpsStatus (pInMplsApiInfo);
                MPLS_CMN_UNLOCK ();
            }
            break;
        case MPLS_OAM_UPDATE_ACH_CHNL_CODE:
            if (pInMplsApiInfo->u4SrcModId != ELPS_MODULE)
            {
                i4RetVal = OSIX_FAILURE;
                break;
            }
            MPLS_ACH_PSC_CHNL_CODE (MPLS_DEF_CONTEXT_ID) =
                pInMplsApiInfo->InAchChnlCode;
            i4RetVal = OSIX_SUCCESS;
            break;
        case MPLS_OAM_REG_APP_FOR_NOTIF:
            if (pInMplsApiInfo->u4SubReqType == MPLS_APP_REG_CALL_BACK)
            {
                MPLS_CMN_LOCK ();
                i4RetVal = MplsOamIfRegAppCallBack (pInMplsApiInfo);
                MPLS_CMN_UNLOCK ();
            }
            else if (pInMplsApiInfo->u4SubReqType ==
                     MPLS_APP_REG_PATH_FOR_NOTIF)
            {
                if (pInMplsApiInfo->InPathId.u4PathType == MPLS_PATH_TYPE_PW)
                {
                    i4RetVal =
                        L2VpnApiHandleExternalRequest
                        (MPLS_APP_REG_PATH_FOR_NOTIF, pInMplsApiInfo, NULL);
                }
                else
                {
                    MPLS_CMN_LOCK ();
                    i4RetVal = MplsOamIfUpdateOamAppStatus (pInMplsApiInfo);
                    MPLS_CMN_UNLOCK ();
                }
            }
            break;
        case MPLS_GET_PACKET_COUNT:
            i4RetVal =
                L2VpnApiHandleExternalRequest
                (MPLS_GET_PACKET_COUNT, pInMplsApiInfo, pOutMplsApiInfo);
            break;
        case MPLS_INCR_OUT_PACKET_COUNT:
            i4RetVal =
                L2VpnApiHandleExternalRequest
                (MPLS_INCR_OUT_PACKET_COUNT, pInMplsApiInfo, pOutMplsApiInfo);
            break;
        case MPLS_INCR_IN_PACKET_COUNT:
            i4RetVal =
                L2VpnApiHandleExternalRequest
                (MPLS_INCR_IN_PACKET_COUNT, pInMplsApiInfo, pOutMplsApiInfo);
            break;
        case MPLS_UPDATE_APP_OWNER_FOR_PW:
            i4RetVal =
                L2VpnApiHandleExternalRequest
                (MPLS_UPDATE_APP_OWNER_FOR_PW, pInMplsApiInfo, pOutMplsApiInfo);
            break;
        default:
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsApiHandleExternalRequest: "
                        "Invalid Main request type: INTMD-EXIT \n");
            break;
    }                            /* switch */
    if (i4RetVal == OSIX_FAILURE)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsApiHandleExternalRequest: "
                    "MPLS information get failed for external module: "
                    "INTMD-EXIT \n");
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function     : MplsApiValDiffservTeParams                                 */
/* Description  : This function validates diffserv-te related params         */
/* Input        : u4ClassType    - class type value                          */
/*                u4Priority     - priority value                            */
/*                u4PhbId        - PHB value                                 */
/*                u4PscId        - PSC value                                 */
/*                u4ValType      - Validation type                           */
/* Output       : NONE.                                                      */
/* Returns      : MPLS_FAILURE/MPLS_SUCCESS                                  */
/*****************************************************************************/
INT4
MplsApiValDiffservTeParams (UINT4 u4ClassType, UINT4 u4Priority,
                            UINT4 u4PhbId, UINT4 u4PscId,
                            UINT4 u4ValType, UINT1 *pu1TeClassNumber)
{
    UINT4                u4RetVal = MPLS_ZERO;
    MPLS_CMN_LOCK ();

    switch (u4ValType)
    {
        case MPLS_DSTE_ENABLED_FLAG:
            u4RetVal = MplsIsDsTeEnabled ();
            break;

        case MPLS_DSTE_CLASS_TYPE_FLAG:
            u4RetVal = MplsIsClassTypePresent (u4ClassType);
            break;

        case MPLS_DSTE_TE_CLASS_FLAG:
            u4RetVal = MplsIsTeClassPresent (u4ClassType, u4Priority,
                                             pu1TeClassNumber);
            break;

        case MPLS_DSTE_PHB_FLAG:
            u4RetVal = MplsIsPHBConsistent (u4ClassType, u4PhbId);
            break;

        case MPLS_DSTE_PSC_FLAG:
            u4RetVal = MplsIsPSCConsistent (u4ClassType, u4PscId);
            break;

        default:
            break;
    }
    MPLS_CMN_UNLOCK ();

    return (INT4)u4RetVal;
}

/*****************************************************************************/
/* Function     : MplsIsDsTeEnabled                                          */
/* Description  : This function checkes if DS-TE is enabled                  */
/* Input        : NONE.                                                      */
/* Output       : NONE.                                                      */
/* Returns      : MPLS_FAILURE/MPLS_SUCCESS                                  */
/*****************************************************************************/
UINT4
MplsIsDsTeEnabled ()
{
    if (MPLS_DSTE_STATUS (MPLS_DEF_INCARN) == MPLS_DSTE_STATUS_ENABLE)
    {
        return MPLS_SUCCESS;
    }
    return MPLS_FAILURE;
}

/*****************************************************************************/
/* Function     : MplsIsClassTypePresent                                     */
/* Description  : This function validates the class type                     */
/* Input        : u4ClassType    - class type value                          */
/* Output       : NONE.                                                      */
/* Returns      : MPLS_FAILURE/MPLS_SUCCESS                                  */
/*****************************************************************************/
UINT4
MplsIsClassTypePresent (UINT4 u4ClassType)
{
    tMplsDsTeClassType  CTEntry;

    MEMSET (&CTEntry, MPLS_ZERO, sizeof (tMplsDsTeClassType));

    CTEntry = gMplsIncarn[MPLS_ZERO].DsTeGlobal.aMplsDsTeClassType[u4ClassType];

    if (CTEntry.i4DsTeClassTypeRowStatus == MPLS_STATUS_ACTIVE)
    {
        return MPLS_SUCCESS;
    }
    return MPLS_FAILURE;
}

/*****************************************************************************/
/* Function     : MplsIsTeClassPresent                                       */
/* Description  : This function validates the TE class                       */
/* Input        : u4ClassType    - class type value                          */
/*                u4Priority     - priority value                            */
/* Output       : NONE.                                                      */
/* Returns      : MPLS_FAILURE/MPLS_SUCCESS                                  */
/*****************************************************************************/
UINT4
MplsIsTeClassPresent (UINT4 u4ClassType, UINT4 u4Priority,
                      UINT1 *pu1TeClassNumber)
{
    tMplsDsTeClassPrio  TeClassEntry;

    MEMSET (&TeClassEntry, MPLS_ZERO, sizeof (tMplsDsTeClassPrio));

    TeClassEntry = gMplsIncarn[MPLS_ZERO].DsTeGlobal.aMplsDsTeClassPrio
        [u4ClassType][u4Priority];

    if (TeClassEntry.i4DsTeCRowStatus == MPLS_STATUS_ACTIVE)
    {
        *pu1TeClassNumber = TeClassEntry.u4TeClassNumber;
        return MPLS_SUCCESS;
    }
    return MPLS_FAILURE;
}

/*****************************************************************************/
/* Function     : MplsIsPHBConsistent                                        */
/* Description  : This function validates PHB consistent                     */
/* Input        : u4ClassType    - class type value                          */
/*                u4PhbId        - PHB value                                 */
/*                u4ValType      - Validation type                           */
/* Output       : NONE.                                                      */
/* Returns      : MPLS_FAILURE/MPLS_SUCCESS                                  */
/*****************************************************************************/
UINT4
MplsIsPHBConsistent (UINT4 u4ClassType, UINT4 u4PhbId)
{
    tMplsDsTeClassTypeTcMap TeTrafficClassEntry;
    UINT4               u4TrafficClass = MPLS_ZERO;
    UINT4               u4PscId = MPLS_ZERO;

    MplsGetPscId (u4PhbId, &u4PscId);

    MEMSET (&TeTrafficClassEntry, MPLS_ZERO, sizeof (tMplsDsTeClassTypeTcMap));

    for (u4TrafficClass = MPLS_ZERO;
         u4TrafficClass < MPLS_MAX_TRAFFIC_CLASS_ENTRIES; u4TrafficClass++)
    {
        TeTrafficClassEntry =
            gMplsIncarn[MPLS_ZERO].DsTeGlobal.
            aMplsDsTeClassTypeTcMap[u4ClassType][u4TrafficClass];
        if ((TeTrafficClassEntry.i4DsTeClassTypeToTcRowStatus ==
             MPLS_STATUS_ACTIVE)
            && (u4PscId == (UINT4) TeTrafficClassEntry.u4DsTeTcType))
        {
            return MPLS_SUCCESS;
        }
    }
    return MPLS_FAILURE;
}

/*****************************************************************************/
/* Function     : MplsIsPSCConsistent                                        */
/* Description  : This function validates PSC consistent                     */
/* Input        : u4ClassType    - class type value                          */
/*                u4PscId        - PSC value                                 */
/* Output       : NONE.                                                      */
/* Returns      : MPLS_FAILURE/MPLS_SUCCESS                                  */
/*****************************************************************************/
UINT4
MplsIsPSCConsistent (UINT4 u4ClassType, UINT4 u4PscId)
{
    tMplsDsTeClassTypeTcMap TeTrafficClassEntry;
    UINT4               u4TrafficClass = MPLS_ZERO;

    MEMSET (&TeTrafficClassEntry, MPLS_ZERO, sizeof (tMplsDsTeClassTypeTcMap));

    for (u4TrafficClass = MPLS_ZERO;
         u4TrafficClass < MPLS_MAX_TRAFFIC_CLASS_ENTRIES; u4TrafficClass++)
    {
        TeTrafficClassEntry =
            gMplsIncarn[MPLS_ZERO].DsTeGlobal.
            aMplsDsTeClassTypeTcMap[u4ClassType][u4TrafficClass];
        if ((TeTrafficClassEntry.i4DsTeClassTypeToTcRowStatus ==
             MPLS_STATUS_ACTIVE)
            && (u4PscId == (UINT4) TeTrafficClassEntry.u4DsTeTcType))
        {
            return MPLS_SUCCESS;
        }
    }
    return MPLS_FAILURE;
}

/*****************************************************************************/
/* Function     : MplsGetPscId                                               */
/* Description  : This function gets PSC value from phb                      */
/* Input        : u4PhbId     - phb value                                    */
/* Output       : pu4PscId    - psc value                                    */
/* Returns      : MPLS_FAILURE/MPLS_SUCCESS                                  */
/*****************************************************************************/

VOID
MplsGetPscId (UINT4 u4PhbId, UINT4 *pu4PscId)
{
    switch (u4PhbId)
    {
        case MPLS_DIFFSERV_AF11_DSCP:
        case MPLS_DIFFSERV_AF12_DSCP:
        case MPLS_DIFFSERV_AF13_DSCP:
            *pu4PscId = MPLS_DIFFSERV_AF1_PSC_DSCP;
            break;

        case MPLS_DIFFSERV_AF21_DSCP:
        case MPLS_DIFFSERV_AF22_DSCP:
        case MPLS_DIFFSERV_AF23_DSCP:
            *pu4PscId = MPLS_DIFFSERV_AF2_PSC_DSCP;
            break;

        case MPLS_DIFFSERV_AF31_DSCP:
        case MPLS_DIFFSERV_AF32_DSCP:
        case MPLS_DIFFSERV_AF33_DSCP:
            *pu4PscId = MPLS_DIFFSERV_AF3_PSC_DSCP;
            break;

        default:
            *pu4PscId = u4PhbId;
            break;
    }
    return;
}

/*****************************************************************************/
/* Function     : MplsApiUpdateChannelTypes                                  */
/*                                                                           */
/* Description  : This function update the Ach Channel types used in the     */
/*                MPLS to identify the OAM Control Packet Type.              */
/*                                                                           */
/* Input        : pMplsAchChannelTypeInfo - Pointer Holds all the channnel   */
/*                Types                                                      */
/*                bLockReq - Lock requested                                  */
/*                TRUE - Needs to take MPLS_CMN_LOCK ()                      */
/*                FALSE - Need not take MPLS_CMN_LOCK ()                     */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/

VOID
MplsApiUpdateChannelTypes (tMplsAchChannelType * pMplsAchChannelTypeInfo,
                           BOOL1 bLockReq)
{
    MPLS_FLAG_LOCK (bLockReq);
    gu2MplsAchChnlTypeCcBfd = pMplsAchChannelTypeInfo->u2AchChnlTypeCcBfd;
    gu2MplsAchChnlTypeCvBfd = pMplsAchChannelTypeInfo->u2AchChnlTypeCvBfd;
    gu2MplsAchChnlTypeCcIpv4 = pMplsAchChannelTypeInfo->u2AchChnlTypeCcIpv4;
    gu2MplsAchChnlTypeCvIpv4 = pMplsAchChannelTypeInfo->u2AchChnlTypeCvIpv4;
    gu2MplsAchChnlTypeCcIpv6 = pMplsAchChannelTypeInfo->u2AchChnlTypeCcIpv6;
    gu2MplsAchChnlTypeCvIpv6 = pMplsAchChannelTypeInfo->u2AchChnlTypeCvIpv6;
    MPLS_FLAG_UNLOCK (bLockReq);

    return;
}


#ifdef HVPLS_WANTED
/*****************************************************************************/
/* Function     : MplsApiFillRegInfo                                         */
/*                                                                           */
/* Description  : This function updates the registeration information        */
/*                in global                                                  */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer Holds Mpls Informatoin            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MPLS_SUCCESS: On Successful Registration                   */
/*                MPLS_FAILURE: If Registration gets failed                  */
/*****************************************************************************/

INT4
MplsApiFillRegInfo (tMplsApiInInfo * pInMplsApiInfo)
{
	if(pInMplsApiInfo == NULL)
	{
		MPLSFM_DBG (MPLSFM_IF_FAILURE,
				"MplsApiFillRegInfo: "
				"Rcvd Null MPLS information\n");
		return MPLS_FAILURE;
	}
	if ((pInMplsApiInfo->u4SubReqType == MPLS_APP_REGISTER) &&
			(gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
			 [pInMplsApiInfo->InRegParams.u4ModId] == NULL))
	{
		/* Create memory for first time registrations */
		if (pInMplsApiInfo->InRegParams.u4Events == 0)
		{
			MPLSFM_DBG (MPLSFM_IF_FAILURE,
					"MplsApiFillRegInfo: "
					"No events given for registration: INTMD-EXIT \n");
			return MPLS_FAILURE;
		}

		gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
			[pInMplsApiInfo->InRegParams.u4ModId]
			= (tMplsRegParams *) MemAllocMemBlk (MPLS_REG_APPS_POOL_ID);
		if (gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
				[pInMplsApiInfo->InRegParams.u4ModId] == NULL)
		{
			MPLSFM_DBG (MPLSFM_IF_FAILURE,
					"MplsApiFillRegInfo: "
					"MemAlloc failed for registration: INTMD-EXIT \n");
			return MPLS_FAILURE;
		}
	}

	if ((pInMplsApiInfo->u4SubReqType == MPLS_APP_REGISTER) &&
			(gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
			 [pInMplsApiInfo->InRegParams.u4ModId] != NULL))
	{
		/* Update the module specific registration parameters */
		gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
			[pInMplsApiInfo->InRegParams.u4ModId]->u4ModId =
			pInMplsApiInfo->InRegParams.u4ModId;
		gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
			[pInMplsApiInfo->InRegParams.u4ModId]->u4Events =
			pInMplsApiInfo->InRegParams.u4Events;
		gMplsIncarn[MPLS_DEF_INCARN].apAppRegParams
			[pInMplsApiInfo->InRegParams.u4ModId]->pFnRcvPkt =
			pInMplsApiInfo->InRegParams.pFnRcvPkt;
                return MPLS_SUCCESS;  
	}		
	return MPLS_FAILURE;
}
#endif
