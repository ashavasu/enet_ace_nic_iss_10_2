/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: labelmgr.c,v 1.20 2014/12/12 11:56:44 siva Exp $
 *
 * Description: This file contains functions that are associated
 *              with Label Space Manager.
 ********************************************************************/

 /*--------------------------------------------------------------------------
 *  The label manager has been created initially provide label manager support
 *  for the signaling protocols of MPLS both LDP and RSVP-TE. However the usage
 *  of this library does not restrict to the usage of the signaling protocols
 *  alone, rather it can be used by any application which requires to maintain
 *  an unique set of identifiers, obtain and release an identifier from that
 *  set.
 *
 *  The functions included in this file are 
 *  1) To initialise the label manager
 *  2) To shutdown the label manager
 *  3) Create a new label space ( also called label group or key group )
 *  4) Delete a label space ( also called label group or key group )
 *  5) Obtain a new label from a label space
 *  6) Release a label to a label space
 *  7) Check whether a label is within the label range of a label space
 *  8) Check whether a sub label range is with in the label range of a label 
 *     space.
 *  9) Obtain the next available label from a label space.
 *
 *---------------------------------------------------------------------------*/

#include "lbmgrinc.h"
#include "lblmgrex.h"
#include "labelmgr.h"
#include "mplsrtr.h"
#include "labelgbl.h"
#include "lblmgrsz.h"
extern UINT4        IssSzGetSizingMacroValue (CHR1 * pu1MacroName);

CHR1                gau1LblName[MPLS_MAX_LBL_GRP][MPLS_MAX_LBL_GRP_NAME_LEN] =
    { "LDP_MIN_LBL", "LDP_MAX_LBL",
    "RSVP_MIN_LBL", "RSVP_MAX_LBL",
    "VPLS_MIN_LBL", "VPLS_MAX_LBL",
    "STATIC_MIN_LBL", "STATIC_MAX_LBL",
    "L3VPN_MIN_LBL", "L3VPN_MAX_LBL",
    "BGP_VPLS_MIN_LBL", "BGP_VPLS_MAX_LBL"
};

/*---------------------------------------------------------------------------*/
/* 
 * Function    : LblMgrInit
 * Description : This function allocates the necessary strucutres for the 
 *               Label Space manager. This function must be invoked before 
 *               using any other routines associated with the Label Space 
 *               Manager.
 * Inputs      : None. 
 * Outputs     : The global variable gLblMgrInfo will be allocated for memory
 *               and suitably updated in case of successful allocations.
 * Returns     : LBL_SUCCESS on successful memory allocation and 
 *               initialisation. Returns LBL_FAILURE otherwise.
 */
/*---------------------------------------------------------------------------*/
UINT1
LblMgrInit ()
{
    UINT2               u2Index1 = 0;
    tAsgnInfoHead      *pAsgnInfoHead = NULL;
    tLblMgrInfo        *pLblMgrInfo = NULL;
    UINT1               u1MacroIndex = MPLS_ONE;
    UINT4               u4DynLbl = MPLS_ZERO;
    /* Global head for the Label Manager information accessed */
    pLblMgrInfo = &gLblMgrInfo;

    /* Mem set to zero */
    MEMSET ((VOID *) pLblMgrInfo, 0, sizeof (tLblMgrInfo));

    /* assigning the constants done in the header file */
    lblMgrInitMemConstants (pLblMgrInfo);

    gSystemSize.MplsSystemSize.u4MinLdpLblRange = LDP_GEN_MIN_LBL;
    gSystemSize.MplsSystemSize.u4MaxLdpLblRange = LDP_GEN_MAX_LBL;
    gSystemSize.MplsSystemSize.u4MinRsvpTeLblRange = RSVPTE_GEN_MIN_LBL;
    gSystemSize.MplsSystemSize.u4MaxRsvpTeLblRange = RSVPTE_GEN_MAX_LBL;
    gSystemSize.MplsSystemSize.u4MinL2VpnLblRange = VPLS_GEN_MIN_LBL;
    gSystemSize.MplsSystemSize.u4MaxL2VpnLblRange = VPLS_GEN_MAX_LBL;
    gSystemSize.MplsSystemSize.u4MinStaticLblRange = STATIC_GEN_MIN_LBL;
    gSystemSize.MplsSystemSize.u4MaxStaticLblRange = STATIC_GEN_MAX_LBL;
    gSystemSize.MplsSystemSize.u4MinL3VPNLblRange = L3VPN_GEN_MIN_LBL;
    gSystemSize.MplsSystemSize.u4MaxL3VPNLblRange = L3VPN_GEN_MAX_LBL;
    gSystemSize.MplsSystemSize.u4MinBgpVplsLblRange = BGP_VPLS_GEN_MIN_LBL;
    gSystemSize.MplsSystemSize.u4MaxBgpVplsLblRange = BGP_VPLS_GEN_MAX_LBL;

    if (FsLBLMGRSizingParams[MAX_LBLMGR_LBL_SPACE_GRP_SIZING_ID].
        u4PreAllocatedUnits != MAX_LBLMGR_LBL_SPACE_GRP)
    {
        LBL_MGR_NUM_MAX_LBL_GRPS (pLblMgrInfo)
            = FsLBLMGRSizingParams[MAX_LBLMGR_LBL_SPACE_GRP_SIZING_ID].
            u4PreAllocatedUnits;
        FsLBLMGRSizingParams[MAX_LBLMGR_LBL_SPACE_GRP_SIZING_ID].u4StructSize
            = (sizeof (tAsgnInfoHead) * LBL_MGR_NUM_MAX_LBL_GRPS (pLblMgrInfo));
        FsLBLMGRSizingParams[MAX_LBLMGR_LBL_SPACE_GRP_SIZING_ID].
            u4PreAllocatedUnits = MAX_LBLMGR_LBL_SPACE_GRP;
    }

    while (u1MacroIndex <= MPLS_MAX_LBL_GRP)
    {
        u4DynLbl = MPLS_ZERO;
        u4DynLbl = IssSzGetSizingMacroValue (gau1LblName[u1MacroIndex - 1]);
        if (u4DynLbl != MPLS_ZERO)
        {
            switch (u1MacroIndex)
            {
                case LDP_MIN_LBL_ID:
                    gSystemSize.MplsSystemSize.u4MinLdpLblRange = u4DynLbl;
                    break;
                case LDP_MAX_LBL_ID:
                    gSystemSize.MplsSystemSize.u4MaxLdpLblRange = u4DynLbl;
                    break;
                case RSVPTE_MIN_LBL_ID:
                    gSystemSize.MplsSystemSize.u4MinRsvpTeLblRange = u4DynLbl;
                    break;
                case RSVPTE_MAX_LBL_ID:
                    gSystemSize.MplsSystemSize.u4MaxRsvpTeLblRange = u4DynLbl;
                    break;
                case VPLS_MIN_LBL_ID:
                    gSystemSize.MplsSystemSize.u4MinL2VpnLblRange = u4DynLbl;
                    break;
                case VPLS_MAX_LBL_ID:
                    gSystemSize.MplsSystemSize.u4MaxL2VpnLblRange = u4DynLbl;
                    break;
                case STATIC_MIN_LBL_ID:
                    gSystemSize.MplsSystemSize.u4MinStaticLblRange = u4DynLbl;
                    break;
                case STATIC_MAX_LBL_ID:
                    gSystemSize.MplsSystemSize.u4MaxStaticLblRange = u4DynLbl;
                    break;
                case L3VPN_MIN_LBL_ID:
                    gSystemSize.MplsSystemSize.u4MinL3VPNLblRange = u4DynLbl;
                    break;
                case L3VPN_MAX_LBL_ID:
                    gSystemSize.MplsSystemSize.u4MaxL3VPNLblRange = u4DynLbl;
                    break;
                case BGP_VPLS_MIN_LBL_ID:
                    gSystemSize.MplsSystemSize.u4MinBgpVplsLblRange = u4DynLbl;
                    break;
                case BGP_VPLS_MAX_LBL_ID:
                    gSystemSize.MplsSystemSize.u4MaxBgpVplsLblRange = u4DynLbl;
                    break;
		default:
		    break;
            }
        }
        u1MacroIndex++;
    }

    /* Create memory pools for Label Manager */
    if (LblmgrSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        return LBL_FAILURE;
    }

    /* Memory for Label Space Groups Head structure being allocated */
    LBL_MGR_ASGN_INFO_HEAD (pLblMgrInfo) =
        MemAllocMemBlk (LBL_SPACE_GRPS_POOL_ID);
    if (LBL_MGR_ASGN_INFO_HEAD (pLblMgrInfo) == NULL)
    {
        LBLMGR_DBG (LBLMGR_MEM,
                    "LBL_MGR : Lbl Grps Head - Mem alloc Failure\n");
        return LBL_FAILURE;
    }
    MEMSET (LBL_MGR_ASGN_INFO_HEAD (pLblMgrInfo), 0, sizeof (tAsgnInfoHead) *
            LBL_MGR_NUM_MAX_LBL_GRPS (pLblMgrInfo));

    /* Semaphore created */

    if ((OsixCreateSem ((const UINT1 *) LBL_SEM_NAME,
                        LBL_SEM_INITIAL_COUNT,
                        OSIX_DEFAULT_SEM_MODE /*OSIX_SEM_PRIOR */ ,
                        LBL_MGR_SEM (pLblMgrInfo))) != OSIX_SUCCESS)
    {

        MEMSET ((VOID *) pLblMgrInfo, 0, sizeof (tLblMgrInfo));
        return LBL_FAILURE;
    }

    /* Groups Head information structures initialised */
    pAsgnInfoHead = LBL_MGR_ASGN_INFO_HEAD (pLblMgrInfo);
    for (u2Index1 = 0; u2Index1 < LBL_MGR_NUM_MAX_LBL_GRPS (pLblMgrInfo);
         pAsgnInfoHead++, u2Index1++)
    {

        pAsgnInfoHead->u1Status = LBL_INVALID;
        pAsgnInfoHead->u1AllocOrder = 0;
        pAsgnInfoHead->u2AsgnInfoGrpId = 0;
        TMO_SLL_Init (&(pAsgnInfoHead->Key1InfoList));

    }
    /* Number of Groups initally active set to zero */
    pLblMgrInfo->u4NumGrpsActive = 0;

    LBLMGR_DBG (LBLMGR_MEM, "LBL_MGR : Init Label Manager Success.\n");
    return LBL_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/* 
 * Function    : LblMgrShutDown
 * Description : This function deletes all the Label Space groups
 *               assoicated with the label manager. 
 * Inputs      : None.
 * Outputs     : None.
 * Returns     : None.
 */
/*---------------------------------------------------------------------------*/
VOID
LblMgrShutDown ()
{
    UINT2               u2Index1 = 0;
    tAsgnInfoHead      *pAsgnInfoHead = NULL;
    tAsgnKey1Info      *pAsgnKey1Info = NULL;
    tAsgnKey2Info      *pAsgnKey2Info = NULL;
    tAsgnKey2Info      *pNextKey2Info = NULL;
    tLblMgrInfo        *pLblMgrInfo = NULL;

    /* Global head for the Label Manager information accessed */
    pLblMgrInfo = &gLblMgrInfo;

    /* Pointer to Label space groups accessed */
    pAsgnInfoHead = LBL_MGR_ASGN_INFO_HEAD (pLblMgrInfo);

    if (pAsgnInfoHead == NULL)
    {
        /* 
         * In this case either the label space manager has not been created,
         * or the label space manager has already been shut down.
         */
        LBLMGR_DBG (LBLMGR_MEM, "LBL_MGR : No info to rel-Shutdown Success.\n");
        return;
    }

    for (u2Index1 = 0; u2Index1 < LBL_MGR_NUM_MAX_LBL_GRPS (pLblMgrInfo);
         pAsgnInfoHead++, u2Index1++)
    {

        if (pAsgnInfoHead->u1Status == LBL_VALID)
        {

            pAsgnKey1Info = (tAsgnKey1Info *) TMO_SLL_First
                ((tTMO_SLL *) (&(pAsgnInfoHead->Key1InfoList)));
            while (pAsgnKey1Info != NULL)
            {
                pAsgnKey2Info = pAsgnKey1Info->pAsgnKey2Info;

                while (pAsgnKey2Info != NULL)
                {
                    pNextKey2Info = pAsgnKey2Info->pNxtAsgnKey2Info;
                    pAsgnKey2Info->pNxtAsgnKey2Info = NULL;
                    MemReleaseMemBlock (LBL_MGR_KEY2_INFO_POOL_ID,
                                        (UINT1 *) pAsgnKey2Info);
                    pAsgnKey2Info = pNextKey2Info;
                }

                TMO_SLL_Delete ((tTMO_SLL *) (&(pAsgnInfoHead->Key1InfoList)),
                                &(pAsgnKey1Info->Key1SllNode));
                MemReleaseMemBlock (LBL_MGR_KEY1_INFO_POOL_ID,
                                    (UINT1 *) pAsgnKey1Info);
                pAsgnKey1Info = (tAsgnKey1Info *) TMO_SLL_First ((tTMO_SLL *)
                                                                 (&
                                                                  (pAsgnInfoHead->
                                                                   Key1InfoList)));
            }
        }
    }

    /* Release memory block for assign info head. */
    MemReleaseMemBlock (LBL_SPACE_GRPS_POOL_ID,
                        (UINT1 *) LBL_MGR_ASGN_INFO_HEAD (pLblMgrInfo));

    /* Delete label manager memory pool. */
    LblmgrSizingMemDeleteMemPools ();

    /* Deleting the semaphore */
    OsixSemDel (gLblMgrInfo.SemId);

    MEMSET ((VOID *) pLblMgrInfo, 0, sizeof (tLblMgrInfo));

    LBLMGR_DBG (LBLMGR_MEM, "LBL_MGR : Shutdown Label Manager Success.\n");
}

/*---------------------------------------------------------------------------*/
/* 
 * Function    : LblMgrLabelOverlapCheck
 * Description : This function checks if the incoming min and max key values  
 *               overlaps with the min and max boundaries of any group in the 
 *               list.
 *               If there is a match,FAILURE is returned. 
 *               Otherwise SUCCESS is returned.
 * Inputs      :   - Takes on any one of the following values.
 *             : pAsgnInfoHead - Pointer to the Head of the Group, from which
 *               the unique key is to be assigned.
 *           u4Key1Val - key1 Value
 *           u4Key2Min - Min key2 Value    
 *           u4Key2Max - Max key2 Value    
 * Outputs     : None.
 * Returns     : LBL_FAILURE if key min and max values overlap. 
 *               LBL_SUCCESS otherwise.
 */
/*---------------------------------------------------------------------------*/

UINT4
LblMgrLabelOverlapCheck (tAsgnInfoHead * pAsgnInfoHead, UINT4 u4Key1Val,
                         UINT4 u4Key2Min, UINT4 u4Key2Max)
{

    tAsgnKey1Info      *pAvailAsgnKey1Info = NULL;

    TMO_SLL_Scan ((tTMO_SLL *) (&(pAsgnInfoHead->Key1InfoList)),
                  pAvailAsgnKey1Info, tAsgnKey1Info *)
    {
        if (pAvailAsgnKey1Info->u4AsgnInfoKey1Val == u4Key1Val)
        {

            if ((((u4Key2Min >= pAvailAsgnKey1Info->u4AsgnInfoKey2Min) &&
                  (u4Key2Min <= pAvailAsgnKey1Info->u4AsgnInfoKey2Max))
                 ||
                 ((u4Key2Max >= pAvailAsgnKey1Info->u4AsgnInfoKey2Min) &&
                  (u4Key2Max <= pAvailAsgnKey1Info->u4AsgnInfoKey2Max))))

            {
                /* If there is overlap in key 2 min and max values then return 
                 * failure */
                return LBL_FAILURE;
            }
        }

    }

    return LBL_SUCCESS;

}

/*---------------------------------------------------------------------------*/
/* 
 * Function    : LblMgrLabelExactMatch
 * Description : This function checks if the incoming min and max key values  
 *               match with any in the list. If there is a match, SUCCESS is 
 *               returned. Otherwise failure is returned.
 * Inputs      :   - Takes on any one of the following values.
 *             : pAsgnInfoHead - Pointer to the Head of the Group, from which
 *               the unique key is to be assigned.
 *           u4Key1Val - Key1 Value
 *           u4Key2Min - Min key2 Value    
 *           u4Key2Max - Max key2 Value    
 * Outputs     : None.
 * Returns     : LBL_SUCCESS if key min and max values match. 
 *               LBL_FAILURE.
 */
/*---------------------------------------------------------------------------*/

UINT4
LblMgrLabelExactMatch (tAsgnInfoHead * pAsgnInfoHead, UINT4 u4Key1Val,
                       UINT4 u4Key2Min, UINT4 u4Key2Max)
{

    tAsgnKey1Info      *pAvailAsgnKey1Info = NULL;

    TMO_SLL_Scan ((tTMO_SLL *) (&(pAsgnInfoHead->Key1InfoList)),
                  pAvailAsgnKey1Info, tAsgnKey1Info *)
    {
        if (((pAvailAsgnKey1Info->u4AsgnInfoKey1Val == u4Key1Val) &&
             (pAvailAsgnKey1Info->u4AsgnInfoKey2Min == u4Key2Min) &&
             (pAvailAsgnKey1Info->u4AsgnInfoKey2Max == u4Key2Max)))

        {
            /* If there is a  match in key values , SUCCESS is returned */
            return LBL_SUCCESS;
        }
    }
    return LBL_FAILURE;

}

/*---------------------------------------------------------------------------*/
/* 
 * Function    : LblMgrCreateLabelSpaceGroup
 * Description : This function creates a Label Space group and identifies it 
 *               by a group id. The required memory for the Key1 and Key2 info 
 *               structures are obtained from the previously allocated memory 
 *               pool. 
 *               On Successful allocation and initialisation, the number of
 *               active groups get incremented. Otherwise failure is returned.
 * Inputs      :   u1AllocOrder - Takes on any one of the following values.
 *                 LBL_ALLOC_EVEN_NUM - Allocates only even number from the 
 *                                range of Key2 values. ( If the ATM VCs are uni
 *                                directional)
 *                 LBL_ALLOC_ODD_NUM - Allocates only odd number from the range
 *                                of Key2 values. (If the ATM VCs are uni 
 *                                directional)
 *                 LBL_ALLOC_BOTH_NUM - Allocates both odd and even number from 
 *                                the range of Key2 values. (If the ATM VCs are
 *                                bidirectional)
 *               pKeyInfo  - Pointer to array of structures, where the
 *               structures hold the values of the label ranges (min and max)
 *               values to be created.
 *               u2NumKeyInfo  - The number of structures contained in the 
 *               array pointed by 'pKeyIfno'.
 * Outputs     : pAsgnKeyGroupId - Pointer to UINT2 memory which will contain
 *               the Group Id on successful group creation.
 * Returns     : LBL_SUCCESS on successful group creation otherwise 
 *               LBL_FAILURE.
 */
/*---------------------------------------------------------------------------*/
UINT1
LblMgrCreateLabelSpaceGroup (UINT1 u1AllocOrder,
                             tKeyInfoStruct * pKeyInfo,
                             UINT2 u2NumKeyInfo, UINT4 u4ModuleId,
                             UINT4 u4InterfaceIndex, UINT2 *pAsgnKeyGroupId)
{

    BOOL1               b1Flag;
    UINT2               u2Index1 = 0;
    UINT2               u2Index2 = 0;
    UINT4               u4Index1 = 0;
    UINT4               u4Index2 = 0;
    UINT4               u4DifVal = 0;

    tLblMgrInfo        *pLblMgrInfo = NULL;
    tAsgnInfoHead      *pAsgnInfoHead = NULL;
    tKeyInfoStruct     *pTmp1KeyInfo = NULL;
    tAsgnKey1Info      *pAsgnKey1Info = NULL;
    tAsgnKey2Info      *pAsgnKey2Info = NULL;
    tAsgnKey2Info      *pPrevKey2Info = NULL;

    UINT4               u4NumKey1Info = 0;
    UINT4               u4NumKey2Info = 0;
    UINT4               u4ModuleLblMin = 0, u4ModuleLblMax = 0;
    /* Global head for the Label Manager information accessed */
    pLblMgrInfo = &gLblMgrInfo;

    /* Generic Label Range Validation based on Module ID for
     *  per platform label space  
     *  LDP     - 100 to 100K
     *  RSVPTE  - 100k + 1 10 160k
     *  L2VPN   - 160K + 1 to 200K 
     *  These macros will be modified to SNMP Objects later for
     *  flexible configuration */

    switch (u4ModuleId)
    {
        case LDP_LBL_MODULE_ID:
            u4ModuleLblMin = gSystemSize.MplsSystemSize.u4MinLdpLblRange;
            u4ModuleLblMax = gSystemSize.MplsSystemSize.u4MaxLdpLblRange;
            break;
        case RSVPTE_LBL_MODULE_ID:
            u4ModuleLblMin = gSystemSize.MplsSystemSize.u4MinRsvpTeLblRange;
            u4ModuleLblMax = gSystemSize.MplsSystemSize.u4MaxRsvpTeLblRange;
            break;
        case VPLS_LBL_MODULE_ID:
            u4ModuleLblMin = gSystemSize.MplsSystemSize.u4MinL2VpnLblRange;
            u4ModuleLblMax = gSystemSize.MplsSystemSize.u4MaxL2VpnLblRange;
            break;
        case STATIC_LBL_MODULE_ID:
            u4ModuleLblMin = gSystemSize.MplsSystemSize.u4MinStaticLblRange;
            u4ModuleLblMax = gSystemSize.MplsSystemSize.u4MaxStaticLblRange;
            break;

        case L3VPN_LBL_MODULE_ID:
            u4ModuleLblMin = gSystemSize.MplsSystemSize.u4MinL3VPNLblRange;
            u4ModuleLblMax = gSystemSize.MplsSystemSize.u4MaxL3VPNLblRange;

            break;
        case BGP_VPLS_LBL_MODULE_ID:
            u4ModuleLblMin = gSystemSize.MplsSystemSize.u4MinBgpVplsLblRange;
            u4ModuleLblMax = gSystemSize.MplsSystemSize.u4MaxBgpVplsLblRange;
            break;
        default:
            return LBL_FAILURE;

    }

    for (u4Index1 = 0, pTmp1KeyInfo = pKeyInfo;
         (u4Index1 < u2NumKeyInfo) &&
         (pTmp1KeyInfo->u4Key1Min == 0) &&
         (pTmp1KeyInfo->u4Key1Max == 0) &&
         (u4InterfaceIndex == PER_PLATFORM_INTERFACE_INDEX);
         u4Index1++, pTmp1KeyInfo++)
    {
        if (pTmp1KeyInfo->u4Key2Min < u4ModuleLblMin ||
            pTmp1KeyInfo->u4Key2Min > u4ModuleLblMax ||
            pTmp1KeyInfo->u4Key2Max < u4ModuleLblMin ||
            pTmp1KeyInfo->u4Key2Max > u4ModuleLblMax)
        {
            if (pTmp1KeyInfo->u4Key2Min < FREE_GEN_MIN_LBL ||
                pTmp1KeyInfo->u4Key2Max < FREE_GEN_MIN_LBL)
            {
                return LBL_FAILURE;
            }
        }
    }

    /* The incoming Module Id and Interface Index is verified for match with 
     * that of each group . If there is a match then the corresponding key 
     * values are verified . If the key1 and key2 min and max values are not 
     * equal to the  incoming Key values, then b1Flag is set to false. Else 
     * the flag value is true (Initial value). When the b1Flag value is true, 
     * group id is assigned and SUCCESS is returned.
     * */

    pAsgnInfoHead = LBL_MGR_ASGN_INFO_HEAD (pLblMgrInfo);

    for (u2Index1 = 0; u2Index1 < LBL_MGR_NUM_LBLGRPS_CUR_SPRTD (pLblMgrInfo);
         u2Index1++, pAsgnInfoHead++)
    {
        /* Checking if the Module Id and Interface Index matches with any in 
         * each of the groups.If, there is no match proceed to the next group*/

        if ((pAsgnInfoHead->u1Status != LBL_VALID)
            || (u4ModuleId != pAsgnInfoHead->u4ModuleId)
            || (u4InterfaceIndex != pAsgnInfoHead->u4InterfaceIndex))
        {
            continue;
        }
        /* Flag which indicates whether the key min and max values match or 
         * not. If there is no  match , Flag is set to False */

        b1Flag = TRUE;

        pTmp1KeyInfo = pKeyInfo;

        for (u2Index2 = 0; ((u2Index2 < u2NumKeyInfo) && (b1Flag == TRUE));
             u2Index2++, pTmp1KeyInfo++)
        {
            /* Checking whether the key min and max values match */

            for (u4Index1 = pTmp1KeyInfo->u4Key1Min;
                 (u4Index1 <= pTmp1KeyInfo->u4Key1Max) && (b1Flag == TRUE);
                 u4Index1++)
            {
                if (LblMgrLabelExactMatch
                    (pAsgnInfoHead, u4Index1, pTmp1KeyInfo->u4Key2Min,
                     pTmp1KeyInfo->u4Key2Max) == LBL_FAILURE)
                {
                    b1Flag = FALSE;
                }
            }
        }

        if (b1Flag == TRUE)
        {
            /* If there is  match in key values , then Group Id is assigned 
             * and SUCCESS is returned. */

            pAsgnInfoHead->u4AsgnCount++;
            (*pAsgnKeyGroupId) = pAsgnInfoHead->u2AsgnInfoGrpId;
            return LBL_SUCCESS;
        }
    }

    /* The incoming  Interface Index is verified for match with that of each 
     * group . If there is a match then the corresponding key values are 
     * verified . If the key1 and key2 min and max values overlap with the 
     * incoming Key values, then  return Failure.
     *  */

    pAsgnInfoHead = LBL_MGR_ASGN_INFO_HEAD (pLblMgrInfo);

    for (u2Index1 = 0; u2Index1 < LBL_MGR_NUM_LBLGRPS_CUR_SPRTD (pLblMgrInfo);
         u2Index1++, pAsgnInfoHead++)
    {
        if ((u4InterfaceIndex != pAsgnInfoHead->u4InterfaceIndex)
            || (pAsgnInfoHead->u1Status != LBL_VALID))
        {
            continue;
        }

        pTmp1KeyInfo = pKeyInfo;

        for (u2Index2 = 0; u2Index2 < u2NumKeyInfo; u2Index2++, pTmp1KeyInfo++)
        {

            for (u4Index1 = pTmp1KeyInfo->u4Key1Min;
                 u4Index1 <= pTmp1KeyInfo->u4Key1Max; u4Index1++)
            {
                /* If there is a overlap in Key values, return Failure */

                if ((LblMgrLabelOverlapCheck
                     (pAsgnInfoHead, u4Index1, pTmp1KeyInfo->u4Key2Min,
                      pTmp1KeyInfo->u4Key2Max)) == LBL_FAILURE)

                {
                    /* If there is overlap in key 2 min and max values then 
                     * return failure */
                    return LBL_FAILURE;
                }

            }

        }

    }

    /* 
     * The Key1 Min Val and Key1 Max Val for a given set should be disjoint
     * w.r.to other sets. This condition is being checked.
     */
    if (LblMgrChkDistinctLblRngsInLblGroup (pKeyInfo, u2NumKeyInfo) ==
        LBL_FAILURE)
    {
        LBLMGR_DBG (LBLMGR_LBL, "LBL_MGR : Lbl Grps not disjoint. "
                    "Sub Lbl Rng Check Fail.\n");
        return LBL_FAILURE;
    }

    /* Checking whether the group can be supported */
    if (LBL_MGR_NUM_LBLGRPS_CUR_SPRTD (pLblMgrInfo)
        == LBL_MGR_NUM_MAX_LBL_GRPS (pLblMgrInfo))
    {
        LBLMGR_DBG (LBLMGR_MEM,
                    "LBL_MGR : MAX Grps active. Lbl Rng Crn Fail.\n");
        return LBL_FAILURE;
    }

    pAsgnInfoHead = LBL_MGR_ASGN_INFO_HEAD (pLblMgrInfo);

    /*  Take semaphore */
    OsixSemTake (gLblMgrInfo.SemId);

    /* 
     * Check whether the memory for Key1Info & Key2Info is available.
     *
     * A max of 32 numbers will be allocated using one key2 Info structure.
     * Hence the Min value is subtracted from the max and the value = "DIFVAL" 
     * is divided by 32. The resultant = "DIVVAL". If the DIFVAL Mod 32 is 0,
     * then DIVVAL number of Key2Info nodes will be allocated. Otherwise
     * DIVVAL + 1 number of Key2Info nodes will be allocated.
     */
    pTmp1KeyInfo = pKeyInfo;

    for (u2Index1 = 0; u2Index1 < u2NumKeyInfo; u2Index1++)
    {

        u4NumKey1Info += ((pTmp1KeyInfo->u4Key1Max
                           - pTmp1KeyInfo->u4Key1Min) + 1);

        u4DifVal = ((pTmp1KeyInfo->u4Key2Max - pTmp1KeyInfo->u4Key2Min) + 1);

        if ((u4DifVal % KEY2_INFO_BLOCK_SIZE) == 0)
        {
            u4DifVal = (u4DifVal / KEY2_INFO_BLOCK_SIZE);
        }
        else
        {
            u4DifVal = (u4DifVal / KEY2_INFO_BLOCK_SIZE) + 1;
        }
        u4NumKey2Info += (u4DifVal * ((pTmp1KeyInfo->u4Key1Max
                                       - pTmp1KeyInfo->u4Key1Min) + 1));

        pTmp1KeyInfo++;
    }

    /* Check done for the required number of Key1 structures in the mem pool */
    if (MemGetFreeUnits (LBL_MGR_KEY1_INFO_POOL_ID) < u4NumKey1Info)
    {
        LBLMGR_DBG (LBLMGR_MEM,
                    "LBL_MGR : Mem for Key1 unavl. Lbl Rng Crn Fail.\n");

        /* Releasing the semaphore */
        OsixSemGive (gLblMgrInfo.SemId);
        return LBL_FAILURE;
    }

    /* Check done for the required number of Key2 structures in the mem pool */
    if (MemGetFreeUnits (LBL_MGR_KEY2_INFO_POOL_ID) < u4NumKey2Info)
    {
        LBLMGR_DBG (LBLMGR_MEM,
                    "LBL_MGR : Mem for Key2 unavl. Lbl Rng Crn Fail.\n");
        /* Releasing the semaphore */
        OsixSemGive (gLblMgrInfo.SemId);

        return LBL_FAILURE;
    }

    /* Required memory is available, the label space is now allocated. */
    pAsgnInfoHead = LBL_MGR_ASGN_INFO_HEAD (pLblMgrInfo);
    for (u2Index1 = 0;
         u2Index1 < LBL_MGR_NUM_MAX_LBL_GRPS (pLblMgrInfo);
         u2Index1++, pAsgnInfoHead++)
    {
        if (pAsgnInfoHead->u1Status == LBL_INVALID)
        {

            pAsgnInfoHead->u1Status = LBL_VALID;
            pAsgnInfoHead->u1AllocOrder = u1AllocOrder;
            pAsgnInfoHead->u4ModuleId = u4ModuleId;
            pAsgnInfoHead->u4InterfaceIndex = u4InterfaceIndex;
            TMO_SLL_Init (&(pAsgnInfoHead->Key1InfoList));
            /*
             * To ensure that the Group Id that is returned by this
             * function is Non Zero always u2Index1 + 1 is done 
             * in the following statement
             */
            pAsgnInfoHead->u2AsgnInfoGrpId = (UINT2) (u2Index1 + 1);
            LBL_MGR_NUM_LBLGRPS_CUR_SPRTD (pLblMgrInfo)++;

            break;
        }
    }

    pTmp1KeyInfo = pKeyInfo;

    for (u2Index1 = 0; u2Index1 < u2NumKeyInfo; u2Index1++, pTmp1KeyInfo++)
    {

        for (u4Index1 = pTmp1KeyInfo->u4Key1Min;
             u4Index1 <= pTmp1KeyInfo->u4Key1Max; u4Index1++)
        {
            pAsgnKey1Info = (tAsgnKey1Info *)
                MemAllocMemBlk (LBL_MGR_KEY1_INFO_POOL_ID);

            if (pAsgnKey1Info == NULL)
            {
                OsixSemGive (gLblMgrInfo.SemId);
                return LBL_FAILURE;
            }

            MEMSET ((VOID *) pAsgnKey1Info, 0, sizeof (tAsgnKey1Info));
            pAsgnKey1Info->u4AsgnInfoKey1Val = u4Index1;
            pAsgnKey1Info->u4AsgnInfoKey2Min = pTmp1KeyInfo->u4Key2Min;
            pAsgnKey1Info->u4AsgnInfoKey2Max = pTmp1KeyInfo->u4Key2Max;
            TMO_SLL_Add ((tTMO_SLL *) (&(pAsgnInfoHead->Key1InfoList)),
                         (tTMO_SLL_NODE *) (&pAsgnKey1Info->Key1SllNode));

            /* 
             * Assigning the maximum number of Key2 values that can be 
             * allocated for a single Key1 value.
             */
            if (u1AllocOrder == LBL_ALLOC_BOTH_NUM)
            {
                pAsgnKey1Info->u4MaxKey2Avail =
                    (pTmp1KeyInfo->u4Key2Max - pTmp1KeyInfo->u4Key2Min) + 1;
            }
            else if (((u1AllocOrder == LBL_ALLOC_ODD_NUM) &&
                      ((pTmp1KeyInfo->u4Key2Max % 2) == 0) &&
                      ((pTmp1KeyInfo->u4Key2Min % 2) == 0)) ||
                     ((u1AllocOrder == LBL_ALLOC_EVEN_NUM) &&
                      ((pTmp1KeyInfo->u4Key2Max % 2) != 0) &&
                      ((pTmp1KeyInfo->u4Key2Min % 2) != 0)))
            {
                pAsgnKey1Info->u4MaxKey2Avail =
                    (pTmp1KeyInfo->u4Key2Max - pTmp1KeyInfo->u4Key2Min) / 2;
            }
            else
            {
                pAsgnKey1Info->u4MaxKey2Avail =
                    ((pTmp1KeyInfo->u4Key2Max - pTmp1KeyInfo->u4Key2Min) / 2) +
                    1;
            }

            /* 
             * Determination of number of memory pools to be allocated for 
             * uniquely assigning Key2 Values.
             */
            u4DifVal = (pTmp1KeyInfo->u4Key2Max - pTmp1KeyInfo->u4Key2Min) + 1;
            if ((u4DifVal % KEY2_INFO_BLOCK_SIZE) == 0)
            {
                u4NumKey2Info = (u4DifVal / KEY2_INFO_BLOCK_SIZE);
            }
            else
            {
                u4NumKey2Info = (u4DifVal / KEY2_INFO_BLOCK_SIZE) + 1;
            }

            for (u4Index2 = 0; u4Index2 < u4NumKey2Info; u4Index2++)
            {
                pAsgnKey2Info = (tAsgnKey2Info *)
                    MemAllocMemBlk (LBL_MGR_KEY2_INFO_POOL_ID);

                if (pAsgnKey2Info == NULL)
                {
                    OsixSemGive (gLblMgrInfo.SemId);
                    return LBL_FAILURE;
                }
                /* 
                 * Bit map initialised to all 1s, This indicates that all 32 
                 * numbers can be allocated and this key info is available for 
                 * allocation.
                 */
                MEMSET ((VOID *) pAsgnKey2Info, 0, sizeof (tAsgnKey2Info));

                switch (u1AllocOrder)
                {
                    case LBL_ALLOC_BOTH_NUM:
                        pAsgnKey2Info->u4AsgnKey2Bmap = KEY2_ALL_AVAIL_BMAP;
                        break;
                    case LBL_ALLOC_ODD_NUM:
                        if (pTmp1KeyInfo->u4Key2Min % 2)
                        {
                            /* Min value is odd and allocation to be odd */
                            pAsgnKey2Info->u4AsgnKey2Bmap = KEY2_ODD_AVAIL_BMAP;
                        }
                        else
                        {
                            /* Min value is even and allocation to be odd */
                            pAsgnKey2Info->u4AsgnKey2Bmap = KEY2_EVN_AVAIL_BMAP;
                        }
                        break;
                    case LBL_ALLOC_EVEN_NUM:
                        if (!(pTmp1KeyInfo->u4Key2Min % 2))
                        {
                            /* Min value is even and allocation to be even */
                            pAsgnKey2Info->u4AsgnKey2Bmap = KEY2_ODD_AVAIL_BMAP;
                        }
                        else
                        {
                            /* Min value is odd and allocation to be even */
                            pAsgnKey2Info->u4AsgnKey2Bmap = KEY2_EVN_AVAIL_BMAP;
                        }
                        break;
		    default:
			break;
                }
                pAsgnKey2Info->pNxtAsgnKey2Info = NULL;

                if (pPrevKey2Info == NULL)
                {
                    /* First Key2 Info for the Group being linked */
                    pAsgnKey1Info->u2AvailKey2Offset = 0;
                    pAsgnKey1Info->pAsgnKey2Info = pAsgnKey2Info;
                    pAsgnKey1Info->pAvailAsgnKey2Info = pAsgnKey2Info;
                }
                else
                {
                    /* Subsequent Key2 Info for the Group being linked */
                    pPrevKey2Info->pNxtAsgnKey2Info = pAsgnKey2Info;
                }
                pPrevKey2Info = pAsgnKey2Info;
            }
            pPrevKey2Info = NULL;
        }
    }

    pAsgnInfoHead->pAvailAsgnKey1Info
        = (tAsgnKey1Info *) TMO_SLL_First (&(pAsgnInfoHead->Key1InfoList));

    (*pAsgnKeyGroupId) = pAsgnInfoHead->u2AsgnInfoGrpId;
    LBLMGR_DBG (LBLMGR_MEM, "LBL_MGR : Lbl Space Creation Success.\n");
    pAsgnInfoHead->u4AsgnCount++;

    /* Releasing the semaphore */
    OsixSemGive (gLblMgrInfo.SemId);

    return LBL_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/* 
 * Function    : LblMgrDeleteLabelSpaceGroup
 * Description : This function deletes a Label Space group identified 
 *               by a group id. All the associated allocated memory are 
 *               released to the corresponding memory pools.
 * Inputs      : u2AsgnKeyGroupID - Identifier of the label space group that 
 *               is to be deleted.
 * Outputs     : None.
 * Returns     : None.
 */
/*---------------------------------------------------------------------------*/
VOID
LblMgrDeleteLabelSpaceGroup (UINT2 u2AsgnKeyGroupId)
{

    UINT1               u1FoundFlag = LBL_FALSE;
    UINT2               u2Index1 = 0;
    tLblMgrInfo        *pLblMgrInfo = NULL;
    tAsgnInfoHead      *pAsgnInfoHead = NULL;
    tAsgnKey2Info      *pAsgnKey2Info = NULL;
    tAsgnKey2Info      *pNextKey2Info = NULL;
    tAsgnKey1Info      *pAsgnKey1Info = NULL;

    /* Global head for the Label Manager information accessed */
    pLblMgrInfo = &gLblMgrInfo;

    /* Accessing the first Group head */
    pAsgnInfoHead = LBL_MGR_ASGN_INFO_HEAD (pLblMgrInfo);

    /* Checking whether the Label space group released is a valid group */
    for (u2Index1 = 0;
         u2Index1 < LBL_MGR_NUM_MAX_LBL_GRPS (pLblMgrInfo);
         u2Index1++, pAsgnInfoHead++)
    {
        if ((pAsgnInfoHead->u1Status == LBL_VALID) &&
            (pAsgnInfoHead->u2AsgnInfoGrpId == u2AsgnKeyGroupId))
        {
            u1FoundFlag = LBL_TRUE;
            break;
        }
    }

    /* Label space group attempted to be released is a invalid group */
    if (u1FoundFlag == LBL_FALSE)
    {
        LBLMGR_DBG (LBLMGR_MEM,
                    "LBL_MGR : Not allocd grp attemtped for rel.\n");
        return;
    }

    pAsgnInfoHead->u4AsgnCount--;

    if (pAsgnInfoHead->u4AsgnCount != 0)
    {
        return;
    }

    /* 
     * The Key1 and Key2 information structures are accessed sequentially
     * and is released.
     */
    pAsgnKey1Info = (tAsgnKey1Info *) TMO_SLL_First
        ((tTMO_SLL *) (&(pAsgnInfoHead->Key1InfoList)));
    while (pAsgnKey1Info != NULL)
    {
        pAsgnKey2Info = pAsgnKey1Info->pAsgnKey2Info;

        while (pAsgnKey2Info != NULL)
        {
            pNextKey2Info = pAsgnKey2Info->pNxtAsgnKey2Info;
            pAsgnKey2Info->pNxtAsgnKey2Info = NULL;
            MemReleaseMemBlock (LBL_MGR_KEY2_INFO_POOL_ID,
                                (UINT1 *) pAsgnKey2Info);
            pAsgnKey2Info = pNextKey2Info;
        }
        TMO_SLL_Delete ((tTMO_SLL *) (&(pAsgnInfoHead->Key1InfoList)),
                        &(pAsgnKey1Info->Key1SllNode));
        MemReleaseMemBlock (LBL_MGR_KEY1_INFO_POOL_ID, (UINT1 *) pAsgnKey1Info);
        pAsgnKey1Info = (tAsgnKey1Info *) TMO_SLL_First
            ((tTMO_SLL *) (&(pAsgnInfoHead->Key1InfoList)));
    }

    pAsgnInfoHead->u1Status = LBL_INVALID;
    pAsgnInfoHead->u1AllocOrder = 0;
    pAsgnInfoHead->u2AsgnInfoGrpId = 0;
    TMO_SLL_Init ((tTMO_SLL *) & (pAsgnInfoHead->Key1InfoList));

    pLblMgrInfo->u4NumGrpsActive--;

    LBLMGR_DBG (LBLMGR_MEM, "LBL_MGR : Lbl Space Deletion Success.\n");

}

/*---------------------------------------------------------------------------*/
/*
 * Function    : LblMgrUpdateAvailKey2Info
 * Description : The function determines the next available Key2Info block
 *               and intialises the appropriate fileds in the Group head
 *               Information structure.
 * Inputs      : pAvailAsgnKey1Info - Pointer to the Key1 Inforamtion Structure,
 *               In which the current available offset for obtaining Key2
 *               value is to be updated.
 * Outputs     : None.
 * Returns     : LBL_SUCCESS on successful updation to next available Key(Label)
 *               value otherwise LBL_FAILURE.
 */
/*---------------------------------------------------------------------------*/
UINT1
LblMgrUpdateAvailKey2Info (tAsgnKey1Info * pAvailAsgnKey1Info)
{
    UINT1               u1FoundFlag = LBL_FALSE;
    UINT2               u2Offset = 0;
    tAsgnKey2Info      *pAsgnKey2Info = NULL;

    if (pAvailAsgnKey1Info->u2NumInfoKey2Asgn ==
        pAvailAsgnKey1Info->u4MaxKey2Avail)
    {

        /* The maximum number has been assinged hence there is no
           requirement to update the offset */
        return LBL_FAILURE;
    }

    pAsgnKey2Info = pAvailAsgnKey1Info->pAsgnKey2Info;
    while (u1FoundFlag == LBL_FALSE)
    {
        if ((pAsgnKey2Info->u4AsgnKey2Bmap & KEY2_ALL_AVAIL_BMAP))
        {
            pAvailAsgnKey1Info->pAvailAsgnKey2Info = pAsgnKey2Info;
            pAvailAsgnKey1Info->u2AvailKey2Offset = u2Offset;
            u1FoundFlag = LBL_TRUE;
        }
        else
        {
            pAsgnKey2Info = pAsgnKey2Info->pNxtAsgnKey2Info;
            u2Offset++;
        }
    }
    return LBL_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/*
 * Function    : LblMgrGetUniqueKeys
 * Description : The function returns unique keys
 * Inputs      : pAsgnInfoHead - Pointer to the Head of the Group, from which
 *               the unique key is to be assigned.
 *               u1RetType - For a value of "LABEL_ALLOC", the next available
 *                           unique label will be allocated and returned.
 *                           For a vlaue of "LABEL_VALUE", the next available
 *                           unique label value is returned. (The label is not
 *                           allocated).
 * Outputs     : pu4Key1 - Pointer to UINT2 type variable, which will contain
 *               the Key1 value.
 *               pu4Key2 - Pointer to UINT2 type variable, which will contain
 *               the Key2 value.
 * Returns     : None.
 */
/*---------------------------------------------------------------------------*/
VOID
LblMgrGetUniqueKeys (tAsgnInfoHead * pAsgnInfoHead,
                     UINT4 *pu4Key1, UINT4 *pu4Key2, UINT1 u1RetType)
{
    UINT1               u1Index = 0;
    UINT1               u1BitPosition = 0;
    UINT1               u1BytePosition = 0;
    UINT1               u1RetVal = 0;
    UINT4               u4AvailKeyBmap = 0;
    tAsgnKey1Info      *pAvailAsgnKey1Info = NULL;
    tAsgnKey2Info      *pAvailAsgnKey2Info = NULL;

    pAvailAsgnKey1Info = pAsgnInfoHead->pAvailAsgnKey1Info;
    /* Key1 value is assigned from the Head strucutre */
    (*pu4Key1) = pAvailAsgnKey1Info->u4AsgnInfoKey1Val;

    pAvailAsgnKey2Info = pAvailAsgnKey1Info->pAvailAsgnKey2Info;

    /* Key2 value is assigned from the Avail Asgn Key2 Structure */
    u4AvailKeyBmap = pAvailAsgnKey2Info->u4AsgnKey2Bmap;

    if (u4AvailKeyBmap & FIRST_BYTE_AVAIL_BMAP)
    {

        u1BytePosition = FIRST_BYTE;

        /* Key2 can be assigned using the first byte information itself */
        for (u1Index = 0; u1Index < BYTE_BLOCK_SIZE; u1Index++)
        {
            if (u4AvailKeyBmap & aAvlBmap[u1Index])
            {
                u1BitPosition = u1Index;
                if (u1RetType == LABEL_ALLOC)
                {
                    pAvailAsgnKey2Info->u4AsgnKey2Bmap &= aAsgnBmap[u1Index];
                }
                break;
            }
        }
    }
    else if (u4AvailKeyBmap & SECOND_BYTE_AVAIL_BMAP)
    {

        u1BytePosition = SECOND_BYTE;

        /* Key2 can be assigned using the Second byte information itself */
        for (u1Index = 0; u1Index < BYTE_BLOCK_SIZE; u1Index++)
        {
            if (u4AvailKeyBmap & aAvlBmap[BYTE_BLOCK_SIZE + u1Index])
            {
                u1BitPosition = u1Index;
                if (u1RetType == LABEL_ALLOC)
                {
                    pAvailAsgnKey2Info->u4AsgnKey2Bmap &=
                        aAsgnBmap[BYTE_BLOCK_SIZE + u1Index];
                }
                break;
            }
        }
    }
    else if (u4AvailKeyBmap & THIRD_BYTE_AVAIL_BMAP)
    {

        u1BytePosition = THIRD_BYTE;

        /* Key2 can be assigned using the third byte information itself */
        for (u1Index = 0; u1Index < BYTE_BLOCK_SIZE; u1Index++)
        {
            if (u4AvailKeyBmap &
                aAvlBmap[(BYTE_BLOCK_SIZE * THIRD_BYTE) + u1Index])
            {
                u1BitPosition = u1Index;
                if (u1RetType == LABEL_ALLOC)
                {
                    pAvailAsgnKey2Info->u4AsgnKey2Bmap &=
                        aAsgnBmap[(BYTE_BLOCK_SIZE * THIRD_BYTE) + u1Index];
                }
                break;
            }
        }
    }
    else if (u4AvailKeyBmap & FOURTH_BYTE_AVAIL_BMAP)
    {

        u1BytePosition = FOURTH_BYTE;

        /* Key2 can be assigned using the fourth byte information itself */
        for (u1Index = 0; u1Index < BYTE_BLOCK_SIZE; u1Index++)
        {
            if (u4AvailKeyBmap &
                aAvlBmap[(BYTE_BLOCK_SIZE * FOURTH_BYTE) + u1Index])
            {
                u1BitPosition = u1Index;
                if (u1RetType == LABEL_ALLOC)
                {
                    pAvailAsgnKey2Info->u4AsgnKey2Bmap &=
                        aAsgnBmap[(BYTE_BLOCK_SIZE * FOURTH_BYTE) + u1Index];
                }
                break;
            }
        }
    }

    /* Key2 value being assinged now */
    /* Key2  value = Min + (offset *32) + (Byte_pos * 8) + Bit_pos */
    (*pu4Key2) = (pAvailAsgnKey1Info->u4AsgnInfoKey2Min +
                  (pAvailAsgnKey1Info->u2AvailKey2Offset *
                   KEY2_INFO_BLOCK_SIZE) +
                  (u1BytePosition * BYTE_BLOCK_SIZE) + u1BitPosition);

    if (u1RetType == LABEL_ALLOC)
    {
        /* Number of keys assinged for this group is incremented */
        pAvailAsgnKey1Info->u2NumInfoKey2Asgn += 1;

        /* The Position of the Current available offset appropriately set */
        /* If some more key2 value can be assigned from the present
         * Key2info structure, the Current available offset is not modified.
         */
        if (!((pAvailAsgnKey2Info->u4AsgnKey2Bmap & KEY2_ALL_AVAIL_BMAP) &&
              (pAvailAsgnKey1Info->u2NumInfoKey2Asgn <
               pAvailAsgnKey1Info->u4MaxKey2Avail)))
        {

            /* All 32 keys in the current Key2 info is allocated,
             * hence current available offset is checked and updated.
             */
            u1RetVal = LblMgrUpdateAvailKey2Info (pAvailAsgnKey1Info);

            if (u1RetVal == LBL_FAILURE)
            {
                /* 
                 * All the Key2 values for the Key1 has been assigned, hence
                 * the pointer to be updated to the next Key1 value.
                 */
                TMO_SLL_Scan ((tTMO_SLL *) (&(pAsgnInfoHead->Key1InfoList)),
                              pAvailAsgnKey1Info, tAsgnKey1Info *)
                {
                    if (pAvailAsgnKey1Info->u2NumInfoKey2Asgn <
                        pAvailAsgnKey1Info->u4MaxKey2Avail)
                    {
                        pAsgnInfoHead->pAvailAsgnKey1Info = pAvailAsgnKey1Info;
                        break;
                    }
                }
            }
        }
    }
}

/*---------------------------------------------------------------------------*/
/*
 * Function    : lblMgrGetLblFromLblGroup
 * Description : This function is invoked to return a unique key combination
 *               from the group identified by the Group Id - u2GroupId
 * Inputs      : u2GroupId - Unique identifier of a key group.
 *               u1RetType - For a value of "LABEL_ALLOC", the next available
 *                           label will be allocated and returned.
 *                           For a vlaue of "LABEL_VALUE", the next available
 *                           label value will be returned. (The label is not
 *                           allocated).
 * Outputs     : pu4Key1 - Pointer to UINT2 type variable, which will contain
 *               the Key1 value.
 *               pu4Key2 - Pointer to UINT2 type variable, which will contain
 *               the Key2 value.
 * Returns     : LBL_SUCCESS - In case of success allocation of unique keys
 *               from the group otherwise LBL_FAILURE.
 */
/*---------------------------------------------------------------------------*/
UINT1
LblMgrGetNextLblFromLblGroup (UINT2 u2GroupId,
                              UINT4 *pu4Key1, UINT4 *pu4Key2, UINT1 u1RetType)
{

    UINT1               u1FoundFlag = LBL_FALSE;
    UINT2               u2Index1 = 0;
    tAsgnInfoHead      *pAsgnInfoHead = NULL;
    tLblMgrInfo        *pLblMgrInfo = NULL;

    /* Global head for the Label Manager information accessed */
    pLblMgrInfo = &gLblMgrInfo;

    /* Accessing the first Group head */
    pAsgnInfoHead = LBL_MGR_ASGN_INFO_HEAD (pLblMgrInfo);

    for (u2Index1 = 0;
         u2Index1 < LBL_MGR_NUM_MAX_LBL_GRPS (pLblMgrInfo);
         u2Index1++, pAsgnInfoHead++)
    {
        if ((pAsgnInfoHead->u1Status == LBL_VALID) &&
            (pAsgnInfoHead->u2AsgnInfoGrpId == u2GroupId))
        {
            u1FoundFlag = LBL_TRUE;
            break;
        }
    }

    if (u1FoundFlag == LBL_FALSE)
    {
        LBLMGR_DBG (LBLMGR_LBL,
                    "LBL_MGR : Lbl Space does not exist. Lbl Alloc Fail.\n");
        return LBL_FAILURE;
    }

    /*  Take semaphore */
    OsixSemTake (gLblMgrInfo.SemId);

    if (pAsgnInfoHead->pAvailAsgnKey1Info->u4MaxKey2Avail ==
        pAsgnInfoHead->pAvailAsgnKey1Info->u2NumInfoKey2Asgn)
    {

        LBLMGR_DBG (LBLMGR_LBL, "LBL_MGR : Max allocs done. Lbl Alloc Fail.\n");
        OsixSemGive (gLblMgrInfo.SemId);

        return LBL_FAILURE;
    }

    /* Unique key can be allocated or the value is obtained */
    LblMgrGetUniqueKeys (pAsgnInfoHead, pu4Key1, pu4Key2, u1RetType);

    /* Releasing the semaphore */
    OsixSemGive (gLblMgrInfo.SemId);

    LBLMGR_DBG (LBLMGR_LBL, "LBL_MGR : Lbl Obtained from Lbl Group Success.\n");
    LBLMGR_DBG2 (LBLMGR_LBL, "LBL_MGR : Lbl Obtained Key1 %d Key2 %d.\n",
                 *pu4Key1, *pu4Key2);
    return LBL_SUCCESS;

}

/*---------------------------------------------------------------------------*/
/*
 * Function    : LblMgrRelLblToLblGroup
 * Description : This function is invoked to release a key ( key1 and key2 )
 *               to the group.
 * Inputs      : u2GroupId - Identifier of the group to which the keys are to
 *               be released.
 *               u4Key1 - Value of u4Key1 that is released.
 *               u4Key2 - Value of the u4Key2 that is released.
 * Outputs     : None.
 * Returns     : LBL_SUCCESS if the keys are returned otherwise 
 *               LBL_FAILURE.
 */
/*---------------------------------------------------------------------------*/
UINT1
LblMgrRelLblToLblGroup (UINT2 u2GroupId, UINT4 u4Key1, UINT4 u4Key2)
{
    UINT1               u1FoundFlag = LBL_FALSE;
    UINT2               u2Index = 0;
    UINT2               u2Index1 = 0;
    UINT2               u2OffsetFromHead = 0;
    UINT4               u4ValFromKey2Min = 0;
    UINT4               u4ValFromMajOffset = 0;
    tAsgnInfoHead      *pAsgnInfoHead = NULL;
    tLblMgrInfo        *pLblMgrInfo = NULL;
    tAsgnKey1Info      *pAsgnKey1Info = NULL;
    tAsgnKey2Info      *pAsgnKey2Info = NULL;

    /* Global head for the Label Manager information accessed */
    pLblMgrInfo = &gLblMgrInfo;

    /* Accessing the first Group head */
    pAsgnInfoHead = LBL_MGR_ASGN_INFO_HEAD (pLblMgrInfo);

    for (u2Index1 = 0;
         u2Index1 < LBL_MGR_NUM_MAX_LBL_GRPS (pLblMgrInfo);
         u2Index1++, pAsgnInfoHead++)
    {
        if ((pAsgnInfoHead->u1Status == LBL_VALID) &&
            (pAsgnInfoHead->u2AsgnInfoGrpId == u2GroupId))
        {
            u1FoundFlag = LBL_TRUE;
            break;
        }
    }

    if (u1FoundFlag == LBL_FALSE)
    {
        LBLMGR_DBG (LBLMGR_LBL,
                    "LBL_MGR : Lbl Space does not exist. Lbl Rel Fail.\n");
        return LBL_FAILURE;
    }

    u1FoundFlag = LBL_FALSE;
    /* Check for the correctness of Key1 info done */
    TMO_SLL_Scan ((tTMO_SLL *) (&(pAsgnInfoHead->Key1InfoList)),
                  pAsgnKey1Info, tAsgnKey1Info *)
    {
        if (pAsgnKey1Info->u4AsgnInfoKey1Val == u4Key1)
        {
            u1FoundFlag = LBL_TRUE;
            break;
        }
    }

    if (u1FoundFlag == LBL_FALSE)
    {
        LBLMGR_DBG (LBLMGR_LBL,
                    "LBL_MGR : Key1 Value mismatch. Lbl Rel Fail.\n");
        return LBL_FAILURE;
    }

    /* Check for the correctness of Key2 info done */
    if ((u4Key2 < pAsgnKey1Info->u4AsgnInfoKey2Min) ||
        (u4Key2 > pAsgnKey1Info->u4AsgnInfoKey2Max))
    {
        LBLMGR_DBG (LBLMGR_LBL,
                    "LBL_MGR : Key2 Value mismatch. Lbl Rel Fail.\n");
        return LBL_FAILURE;
    }

    /*  Take semaphore */
    OsixSemTake (gLblMgrInfo.SemId);

    /* The Key2 value compare to Min value is determined */
    u4ValFromKey2Min = u4Key2 - (pAsgnKey1Info->u4AsgnInfoKey2Min);

    /* Offset from the Group head is determined */
    u2OffsetFromHead = (UINT2) (u4ValFromKey2Min / KEY2_INFO_BLOCK_SIZE);

    /* Pointer to the corresponding Key2Info strucutre is determined */
    pAsgnKey2Info = pAsgnKey1Info->pAsgnKey2Info;
    for (u2Index = 0; u2Index < u2OffsetFromHead; u2Index++)
    {
        pAsgnKey2Info = pAsgnKey2Info->pNxtAsgnKey2Info;
    }

    /* Determining the byte and bit position in the Key2 info struture */
    u4ValFromMajOffset = u4ValFromKey2Min -
        (u2OffsetFromHead * KEY2_INFO_BLOCK_SIZE);

    /* NOTE : Release of an already released key will have no effect */
    if (!(pAsgnKey2Info->u4AsgnKey2Bmap & aAvlBmap[u4ValFromMajOffset]))
    {
        pAsgnKey2Info->u4AsgnKey2Bmap |= aAvlBmap[u4ValFromMajOffset];
        pAsgnKey1Info->u2NumInfoKey2Asgn--;

        if (pAsgnKey1Info->u2AvailKey2Offset > u2OffsetFromHead)
        {
            /* 
             * New key can be allocated now from a structure that is
             * present earlier compared to the one set as currently available 
             * offset.
             */
            pAsgnKey1Info->u2AvailKey2Offset = u2OffsetFromHead;
            pAsgnKey1Info->pAvailAsgnKey2Info = pAsgnKey2Info;
        }
    }

    TMO_SLL_Scan ((tTMO_SLL *) (&(pAsgnInfoHead->Key1InfoList)),
                  pAsgnKey1Info, tAsgnKey1Info *)
    {
        if (pAsgnKey1Info->u2NumInfoKey2Asgn < pAsgnKey1Info->u4MaxKey2Avail)
        {
            pAsgnInfoHead->pAvailAsgnKey1Info = pAsgnKey1Info;
            break;
        }
    }

    /* Releasing the semaphore */
    OsixSemGive (gLblMgrInfo.SemId);

    LBLMGR_DBG (LBLMGR_LBL, "LBL_MGR : Lbl Released to Lbl Group Success.\n");
    LBLMGR_DBG2 (LBLMGR_LBL, "LBL_MGR : Lbl Released Key1 %d Key2 %d.\n",
                 u4Key1, u4Key2);
    return LBL_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/*
 * Function    : LblMgrAssignLblToLblGroup
 * Description : This function is invoked to assign a key ( key1 and key2 )
 *               to the group.The Statically allocated label (CLI) are updated 
 *               to the Label Manager through this function.
 * Inputs      : u2GroupId - Identifier of the group to which the keys are to
 *               be released.
 *               u4Key1 - Value of u4Key1 that is to be assigned.
 *               u4Key2 - Value of the u4Key2 that is to be assigned.
 * Outputs     : None.
 * Returns     : LBL_SUCCESS if the keys are returned otherwise 
 *               LBL_FAILURE.
 */
/*---------------------------------------------------------------------------*/
UINT1
LblMgrAssignLblToLblGroup (UINT2 u2GroupId, UINT4 u4Key1, UINT4 u4Key2)
{
    UINT1               u1FoundFlag = LBL_FALSE;
    UINT2               u2Index = 0;
    UINT2               u2Index1 = 0;
    UINT2               u2OffsetFromHead = 0;
    UINT4               u4ValFromKey2Min = 0;
    UINT4               u4ValFromMajOffset = 0;
    tAsgnInfoHead      *pAsgnInfoHead = NULL;
    tLblMgrInfo        *pLblMgrInfo = NULL;
    tAsgnKey1Info      *pAsgnKey1Info = NULL;
    tAsgnKey2Info      *pAsgnKey2Info = NULL;

    /* Global head for the Label Manager information accessed */
    pLblMgrInfo = &gLblMgrInfo;

    /* Accessing the first Group head */
    pAsgnInfoHead = LBL_MGR_ASGN_INFO_HEAD (pLblMgrInfo);

    for (u2Index1 = 0;
         u2Index1 < LBL_MGR_NUM_MAX_LBL_GRPS (pLblMgrInfo);
         u2Index1++, pAsgnInfoHead++)
    {
        if ((pAsgnInfoHead->u1Status == LBL_VALID) &&
            (pAsgnInfoHead->u2AsgnInfoGrpId == u2GroupId))
        {
            u1FoundFlag = LBL_TRUE;
            break;
        }
    }

    if (u1FoundFlag == LBL_FALSE)
    {
        LBLMGR_DBG (LBLMGR_LBL,
                    "LBL_MGR : Lbl Space does not exist."
                    "LblMgrAssignLblToLblGroup Failed.\n");
        return LBL_FAILURE;
    }

    u1FoundFlag = LBL_FALSE;
    /* Check for the correctness of Key1 info done */
    TMO_SLL_Scan ((tTMO_SLL *) (&(pAsgnInfoHead->Key1InfoList)),
                  pAsgnKey1Info, tAsgnKey1Info *)
    {
        if (pAsgnKey1Info->u4AsgnInfoKey1Val == u4Key1)
        {
            u1FoundFlag = LBL_TRUE;
            break;
        }
    }

    if (u1FoundFlag == LBL_FALSE)
    {
        LBLMGR_DBG (LBLMGR_LBL,
                    "LBL_MGR : Key1 Value mismatch."
                    "LblMgrAssignLblToLblGroup Failed.\n");
        return LBL_FAILURE;
    }

    /* Check for the correctness of Key2 info done */
    if ((u4Key2 < pAsgnKey1Info->u4AsgnInfoKey2Min) ||
        (u4Key2 > pAsgnKey1Info->u4AsgnInfoKey2Max))
    {
        LBLMGR_DBG (LBLMGR_LBL,
                    "LBL_MGR : Key2 Value mismatch"
                    "LblMgrAssignLblToLblGroup Failed.\n");
        return LBL_FAILURE;
    }

    /*  Take semaphore */
    OsixSemTake (gLblMgrInfo.SemId);

    /* The Key2 value compare to Min value is determined */
    u4ValFromKey2Min = u4Key2 - (pAsgnKey1Info->u4AsgnInfoKey2Min);

    /* Offset from the Group head is determined */
    u2OffsetFromHead = (UINT2) (u4ValFromKey2Min / KEY2_INFO_BLOCK_SIZE);

    /* Pointer to the corresponding Key2Info strucutre is determined */
    pAsgnKey2Info = pAsgnKey1Info->pAsgnKey2Info;
    for (u2Index = 0; u2Index < u2OffsetFromHead; u2Index++)
    {
        pAsgnKey2Info = pAsgnKey2Info->pNxtAsgnKey2Info;
    }

    /* Determining the byte and bit position in the Key2 info struture */
    u4ValFromMajOffset = u4ValFromKey2Min -
        (u2OffsetFromHead * KEY2_INFO_BLOCK_SIZE);

    if (pAsgnKey2Info->u4AsgnKey2Bmap & aAvlBmap[u4ValFromMajOffset])
    {
        u1FoundFlag = LBL_FALSE;
        pAsgnKey2Info->u4AsgnKey2Bmap &= aAsgnBmap[u4ValFromMajOffset];
        pAsgnKey1Info->u2NumInfoKey2Asgn++;

        if (pAsgnKey1Info->u2AvailKey2Offset > u2OffsetFromHead)
        {
            /* 
             * New key can be allocated now from a structure that is
             * present earlier compared to the one set as currently available 
             * offset.
             */
            pAsgnKey1Info->u2AvailKey2Offset = u2OffsetFromHead;
            pAsgnKey1Info->pAvailAsgnKey2Info = pAsgnKey2Info;
        }
    }
    else
    {
        u1FoundFlag = LBL_TRUE;
    }

    TMO_SLL_Scan ((tTMO_SLL *) (&(pAsgnInfoHead->Key1InfoList)),
                  pAsgnKey1Info, tAsgnKey1Info *)
    {
        if (pAsgnKey1Info->u2NumInfoKey2Asgn < pAsgnKey1Info->u4MaxKey2Avail)
        {
            pAsgnInfoHead->pAvailAsgnKey1Info = pAsgnKey1Info;
            break;
        }
    }

    /* Releasing the semaphore */
    OsixSemGive (gLblMgrInfo.SemId);

    if (u1FoundFlag == LBL_TRUE)
    {
        /* Already in use */
        return LBL_FAILURE;
    }
    LBLMGR_DBG (LBLMGR_LBL, "LBL_MGR : LblMgrAssignLblToLblGroup Success.\n");
    return LBL_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/*
 * Function    : LblMgrCheckLblInLblGroup
 * Description : This function is invoked to check whether a key ( key1 and 
 *               key2 ) is part of the group.
 * Inputs      : u2GroupId - Identifier of the group in which the presence of
 *               the key is to be checked. 
 *               u4Key1 - Value of u4Key1 that is to be checked.
 *               u4Key2 - Value of the u4Key2 that is to be checked.
 * Outputs     : None.
 * Returns     : LBL_SUCCESS if the key (Key1 and Key2 value combination) is 
 *               part of the group otherwise LBL_FAILURE.
 */
/*---------------------------------------------------------------------------*/
UINT1
LblMgrCheckLblInLblGroup (UINT2 u2GroupId, UINT4 u4Key1, UINT4 u4Key2)
{
    UINT1               u1FoundFlag = LBL_FALSE;
    UINT2               u2Index1;
    tAsgnInfoHead      *pAsgnInfoHead = NULL;
    tLblMgrInfo        *pLblMgrInfo = NULL;
    tAsgnKey1Info      *pAsgnKey1Info = NULL;

    /* Global head for the Label Manager information accessed */
    pLblMgrInfo = &gLblMgrInfo;

    /* Accessing the first Group head */
    pAsgnInfoHead = LBL_MGR_ASGN_INFO_HEAD (pLblMgrInfo);

    for (u2Index1 = 0;
         u2Index1 < LBL_MGR_NUM_MAX_LBL_GRPS (pLblMgrInfo);
         u2Index1++, pAsgnInfoHead++)
    {
        if ((pAsgnInfoHead->u1Status == LBL_VALID) &&
            (pAsgnInfoHead->u2AsgnInfoGrpId == u2GroupId))
        {
            u1FoundFlag = LBL_TRUE;
            break;
        }
    }

    if (u1FoundFlag == LBL_FALSE)
    {
        LBLMGR_DBG (LBLMGR_LBL,
                    "LBL_MGR : Lbl Space does not exist. Lbl Check Fail.\n");
        return LBL_FAILURE;
    }

    u1FoundFlag = LBL_FALSE;
    /* Check for the correctness of Key1 info done */
    TMO_SLL_Scan ((tTMO_SLL *) (&(pAsgnInfoHead->Key1InfoList)),
                  pAsgnKey1Info, tAsgnKey1Info *)
    {
        if (pAsgnKey1Info->u4AsgnInfoKey1Val == u4Key1)
        {
            u1FoundFlag = LBL_TRUE;
            break;
        }
    }

    if (u1FoundFlag == LBL_FALSE)
    {
        LBLMGR_DBG (LBLMGR_LBL,
                    "LBL_MGR : Key1 Value mismatch. Lbl Check Fail.\n");
        return LBL_FAILURE;
    }

    /* Check for the correctness of Key2 info done */
    if ((u4Key2 < pAsgnKey1Info->u4AsgnInfoKey2Min) ||
        (u4Key2 > pAsgnKey1Info->u4AsgnInfoKey2Max))
    {
        LBLMGR_DBG (LBLMGR_LBL,
                    "LBL_MGR : Key2 Value mismatch. Lbl Check Fail.\n");
        return LBL_FAILURE;
    }

    LBLMGR_DBG (LBLMGR_LBL,
                "LBL_MGR : Valid Key1 and Key2 (Lbl) cmbntn in Group.\n");
    return LBL_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/* 
 * Function    : LblMgrChkDistinctLblRngsInLblGroup
 * Description : This function is invoked to check whether the Key1, Key2 min
 *               and max values (label ranges) are mutually disjoint and hence
 *               can form a effective label space. 
 * Inputs      : pKeyInfo  - Pointer to array of structures, where the
 *               structures hold the values of the label ranges (min and max)
 *               values that are to be checked.
 *               u2NumKeyInfo  - The number of structures contained in the 
 *               array pointed by 'pKeyIfno'.
 * Outputs     : None.
 * Returns     : LBL_SUCCESS in case if the Key1, Key2 min and max values (label
 *               ranges) are mutually disjoint otherwise LBL_FAILURE.
 */
/*---------------------------------------------------------------------------*/
UINT1
LblMgrChkDistinctLblRngsInLblGroup (tKeyInfoStruct * pKeyInfo,
                                    UINT2 u2NumKeyInfo)
{
    UINT1               u2Index1;
    UINT1               u2Index2;
    tKeyInfoStruct     *pTmp1KeyInfo = NULL;
    tKeyInfoStruct     *pTmp2KeyInfo = NULL;

    /* In case the number of Key Groups passed equals zero, then there is 
     * no requirement to check, and failure returned as this is not a valid
     * condition. - Defensive Check. */
    if (u2NumKeyInfo == LBL_ZERO)
    {
        LBLMGR_DBG (LBLMGR_LBL,
                    "LBL_MGR : Num Grps 0. Invld Cndn. Disjoint Chk Fail.\n");
        return LBL_FAILURE;
    }

    /* In case the number of Key Groups passed equals one, then there is 
     * no requirement to check, as this group is independent and hence success
     * returned.*/
    if (u2NumKeyInfo == LBL_ONE)
    {
        if ((pKeyInfo->u4Key1Min > pKeyInfo->u4Key1Max) ||
            (pKeyInfo->u4Key2Min > pKeyInfo->u4Key2Max))
        {
            LBLMGR_DBG (LBLMGR_LBL,
                        "LBL_MGR : Invld Cndn. Min Val > Max Val.\n");
            return LBL_FAILURE;
        }
        else
        {
            LBLMGR_DBG (LBLMGR_LBL,
                        "LBL_MGR : Num Grps 1. Key Grps Disjoint.\n");
            return LBL_SUCCESS;
        }
    }

    /* Checking whether the minimum values are lesser than the max values */
    pTmp1KeyInfo = pKeyInfo;
    for (u2Index1 = 0; u2Index1 < u2NumKeyInfo; u2Index1++, pTmp1KeyInfo++)
    {
        if ((pTmp1KeyInfo->u4Key1Min > pTmp1KeyInfo->u4Key1Max) ||
            (pTmp1KeyInfo->u4Key2Min > pTmp1KeyInfo->u4Key2Max))
        {
            LBLMGR_DBG (LBLMGR_LBL,
                        "LBL_MGR : Invld Cndn. Min Val > Max Val.\n");
            return LBL_FAILURE;
        }
    }

    /* 
     * Taking each element by element and checking with the other elements in 
     * the given Key Group.
     */
    pTmp1KeyInfo = pKeyInfo;
    for (u2Index1 = 0; u2Index1 < u2NumKeyInfo; u2Index1++, pTmp1KeyInfo++)
    {
        pTmp2KeyInfo = pKeyInfo;
        for (u2Index2 = 0; u2Index2 < u2NumKeyInfo; u2Index2++, pTmp2KeyInfo++)
        {

            if (pTmp1KeyInfo == pTmp2KeyInfo)
            {
                /* Self check not done - hence continue */
                continue;
            }
            else
            {
                if ((pTmp1KeyInfo->u4Key1Min > pTmp2KeyInfo->u4Key1Max) ||
                    (pTmp2KeyInfo->u4Key1Min > pTmp1KeyInfo->u4Key1Max))
                {
                    /* Key1 values are disjoint and hence Key2 values are 
                     * irrelevant. So the subgroups are disjoint.*/
                    continue;
                }
                else if ((pTmp1KeyInfo->u4Key2Min > pTmp2KeyInfo->u4Key2Max) ||
                         (pTmp2KeyInfo->u4Key2Min > pTmp1KeyInfo->u4Key2Max))
                {
                    /* Key1 values are not disjoint, but Key2 values are
                     * disjoint. So the subgroups are disjoint.*/
                    continue;
                }
                else
                {
                    LBLMGR_DBG (LBLMGR_LBL, "LBL_MGR : Two sub grps are not "
                                "disjoint. Disjoint Chk Fail.\n");
                    return LBL_FAILURE;
                }
            }
        }
    }
    LBLMGR_DBG (LBLMGR_LBL,
                "LBL_MGR : The sub grps are Disjoint Chk Succss.\n");
    return LBL_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/* 
 * Function    : LblMgrChkSubLblRangeInLblGroup
 * Description : This function is invoked to check whether a given sub key group
 *               (label range) is a (subset) set of a given key group (label 
 *               range). 
 * Inputs      : u2GroupId - Identifier of the Key group (Label Range) in which 
 *               the sub Key Group (sub Label Range) is to be checked.
 *               pKeyInfo  - Pointer to array of structures, where the
 *               structures hold the values of the label ranges (min and max)
 *               values that are to be checked.
 *               u2NumKeyInfo  - The number of structures contained in the 
 *               array pointed by 'pKeyIfno'.
 * Outputs     : None.
 * Returns     : LBL_SUCCESS in case if the sub Key Group (sub Label Range) is
 *               part of the Key group (Label Range) otherwise LBL_FAILURE.
 */
/*---------------------------------------------------------------------------*/
UINT1
LblMgrChkSubLblRangeInLblGroup (UINT2 u2GroupId,
                                tKeyInfoStruct * pKeyInfo, UINT2 u2NumKeyInfo)
{

    UINT2               u2Index1 = 0;
    UINT2               u2Key2Key1Distance = 0;
    UINT1               u1MinFoundFlag = LBL_FALSE;
    UINT1               u1MaxFoundFlag = LBL_FALSE;

    tLblMgrInfo        *pLblMgrInfo = NULL;
    tAsgnInfoHead      *pAsgnInfoHead = NULL;
    tKeyInfoStruct     *pTmp1KeyInfo = NULL;
    tAsgnKey1Info      *pAsgnKey1Info = NULL;
    tAsgnKey1Info      *pMinKey1AsgnKey1Info = NULL;
    tAsgnKey1Info      *pMaxKey1AsgnKey1Info = NULL;

    /* Global head for the Label Manager information accessed */
    pLblMgrInfo = &gLblMgrInfo;

    /* Accessing the first Group head */
    pAsgnInfoHead = LBL_MGR_ASGN_INFO_HEAD (pLblMgrInfo);

    for (u2Index1 = 0;
         u2Index1 < LBL_MGR_NUM_MAX_LBL_GRPS (pLblMgrInfo);
         u2Index1++, pAsgnInfoHead++)
    {
        if ((pAsgnInfoHead->u1Status == LBL_VALID) &&
            (pAsgnInfoHead->u2AsgnInfoGrpId == u2GroupId))
        {
            u1MinFoundFlag = LBL_TRUE;
            break;
        }
    }

    if (u1MinFoundFlag == LBL_FALSE)
    {
        LBLMGR_DBG (LBLMGR_LBL, "LBL_MGR : Lbl Space does not exist. "
                    "Sub Lbl Rng Check Fail.\n");
        return LBL_FAILURE;
    }

    /* 
     * The Key1 Min Val and Key1 Max Val for a given set should be disjoint
     * w.r.to other sets. This condition is being checked.
     */
    if (LblMgrChkDistinctLblRngsInLblGroup (pKeyInfo, u2NumKeyInfo) ==
        LBL_FAILURE)
    {
        LBLMGR_DBG (LBLMGR_LBL, "LBL_MGR : Key1 Grps not disjoint. "
                    "Sub Lbl Rng Check Fail.\n");
        return LBL_FAILURE;
    }

    pTmp1KeyInfo = pKeyInfo;

    for (u2Index1 = 0; u2Index1 < u2NumKeyInfo; u2Index1++)
    {

        u1MinFoundFlag = LBL_FALSE;
        u1MaxFoundFlag = LBL_FALSE;
        pMinKey1AsgnKey1Info = NULL;
        pMaxKey1AsgnKey1Info = NULL;

        /* Check for the correctness of Key1 info done */
        TMO_SLL_Scan ((tTMO_SLL *) (&(pAsgnInfoHead->Key1InfoList)),
                      pAsgnKey1Info, tAsgnKey1Info *)
        {
            if ((u1MinFoundFlag == LBL_FALSE) &&
                (pAsgnKey1Info->u4AsgnInfoKey1Val == pTmp1KeyInfo->u4Key1Min))
            {
                u1MinFoundFlag = LBL_TRUE;
                u2Key2Key1Distance = LBL_ZERO;
                pMinKey1AsgnKey1Info = pAsgnKey1Info;
            }
            else if ((u1MinFoundFlag == LBL_TRUE) &&
                     (pAsgnKey1Info->u4AsgnInfoKey1Val
                      == pTmp1KeyInfo->u4Key1Max))
            {
                u1MaxFoundFlag = LBL_TRUE;
                pMaxKey1AsgnKey1Info = pAsgnKey1Info;
                u2Key2Key1Distance += LBL_ONE;
                break;
            }
            else if ((u1MinFoundFlag == LBL_TRUE)
                     && (u1MaxFoundFlag == LBL_FALSE)
                     && (pAsgnKey1Info->u4AsgnInfoKey1Val >
                         pTmp1KeyInfo->u4Key1Min)
                     && (pAsgnKey1Info->u4AsgnInfoKey1Val <
                         pTmp1KeyInfo->u4Key1Max))
            {
                u2Key2Key1Distance += LBL_ONE;
            }
        }

        /* 
         * Checking whether the sub key group (sub label range ) Key1 values 
         * are  within the main key group ( main label range).
         */
        if ((u1MinFoundFlag == LBL_FALSE) || (u1MaxFoundFlag == LBL_FALSE))
        {
            LBLMGR_DBG (LBLMGR_LBL, "LBL_MGR : Key1 Grp out of Range. "
                        "Sub Lbl Rng Check Fail.\n");
            return LBL_FAILURE;
        }

        /* 
         * By now we have located both entries having the MIN val and MAX val
         * of the Key1 values, and the following check ensures that the 
         * difference of the Key1 min and Key1 max in the sub group being 
         * checked is less or equal to the values in the original group.
         */
        if ((pMaxKey1AsgnKey1Info->u4AsgnInfoKey1Val -
             pMinKey1AsgnKey1Info->u4AsgnInfoKey1Val) != u2Key2Key1Distance)
        {
            LBLMGR_DBG (LBLMGR_LBL, "LBL_MGR : Incrrct Key1 Grp . "
                        "Sub Lbl Rng Check Fail.\n");
            return LBL_FAILURE;
        }

        /*
         * Check is now done whethre the Key2 values are within the valid range
         */
        if (!(((pMinKey1AsgnKey1Info->u4AsgnInfoKey2Min ==
                pMaxKey1AsgnKey1Info->u4AsgnInfoKey2Min) &&
               (pMinKey1AsgnKey1Info->u4AsgnInfoKey2Max ==
                pMaxKey1AsgnKey1Info->u4AsgnInfoKey2Max)) &&
              ((pTmp1KeyInfo->u4Key2Min
                >= pMinKey1AsgnKey1Info->u4AsgnInfoKey2Min) &&
               (pTmp1KeyInfo->u4Key2Min
                <= pMinKey1AsgnKey1Info->u4AsgnInfoKey2Max)) &&
              ((pTmp1KeyInfo->u4Key2Min
                >= pMaxKey1AsgnKey1Info->u4AsgnInfoKey2Min) &&
               (pTmp1KeyInfo->u4Key2Min
                <= pMaxKey1AsgnKey1Info->u4AsgnInfoKey2Max))))
        {
            LBLMGR_DBG (LBLMGR_LBL, "LBL_MGR : Incrrct Key2 Val . "
                        "Sub Lbl Rng Check Fail.\n");
            return LBL_FAILURE;

        }

        /* Moving to the next Key group values */
        pTmp1KeyInfo++;
    }

    LBLMGR_DBG (LBLMGR_LBL,
                "LBL_MGR : Sub Lbl Rng Prsnt in Global Rng. Check Succss.\n");
    return LBL_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/* 
 * Function    : LblMgrDbgEnable
 * Description : This function is invoked to enable Debug statements in the
 *               label manager
 * Inputs      : u4DbgEnableFlag - Boolean variable to enable disable the 
 *               debug funciton.This variable should have either a value
 *               of 1 or 0
 *               u4DbgVal - Bit mask indicating the debug level that is
 *               to be enabled or disabled.
 * Outputs     : None.
 * Returns     : None.
 */
/*---------------------------------------------------------------------------*/
VOID
LblMgrDbgEnable (UINT4 u4DbgEnableFlag, UINT4 u4DbgVal)
{
    switch (u4DbgEnableFlag)
    {
        case LBL_TRUE:
            LBLMGR_DBG_FLAG |= u4DbgVal;
            LBLMGR_DBG (LBLMGR_LBL, "LBL_MGR : Debug enabled.\n");
            break;
        case LBL_FALSE:
            LBLMGR_DBG (LBLMGR_LBL, "LBL_MGR : Debug disabled.\n");
            LBLMGR_DBG_FLAG &= (~(u4DbgVal));
            break;
        default:
            break;
    }
}

/*---------------------------------------------------------------------------*/
/* 
 * Function    : LblMgrAvailableLblInLblGroup
 * Description : This function is used to check whether the entry is there in 
 *               the given Group.
 * Inputs      : u2GroupId - Group Id in which the input label has to be 
 *                           checked.
 *               u4Key1    - Used for ATM Cases.For Others,it is 0.
 *               u4Key2    - Input Label that has to be checked. 
 * Outputs     : None.
 * Returns     : If the Label is present, LBL_SUCCESS is returned  
 *               else LBL_FAILURE is returned.
 */
/*---------------------------------------------------------------------------*/
UINT1
LblMgrAvailableLblInLblGroup (UINT2 u2GroupId, UINT4 u4Key1, UINT4 u4Key2)
{
    UINT1               u1FoundFlag = LBL_FALSE;
    UINT2               u2Index = 0;
    UINT2               u2Index1 = 0;
    UINT2               u2OffsetFromHead = 0;
    UINT4               u4ValFromKey2Min = 0;
    UINT4               u4ValFromMajOffset = 0;
    tAsgnInfoHead      *pAsgnInfoHead = NULL;
    tLblMgrInfo        *pLblMgrInfo = NULL;
    tAsgnKey1Info      *pAsgnKey1Info = NULL;
    tAsgnKey2Info      *pAsgnKey2Info = NULL;

    /* Global head for the Label Manager information accessed */
    pLblMgrInfo = &gLblMgrInfo;

    /* Accessing the first Group head */
    pAsgnInfoHead = LBL_MGR_ASGN_INFO_HEAD (pLblMgrInfo);

    for (u2Index1 = 0;
         u2Index1 < LBL_MGR_NUM_MAX_LBL_GRPS (pLblMgrInfo);
         u2Index1++, pAsgnInfoHead++)
    {
        if ((pAsgnInfoHead->u1Status == LBL_VALID) &&
            (pAsgnInfoHead->u2AsgnInfoGrpId == u2GroupId))
        {
            u1FoundFlag = LBL_TRUE;
            break;
        }
    }

    if (u1FoundFlag == LBL_FALSE)
    {
        LBLMGR_DBG (LBLMGR_LBL,
                    "LBL_MGR : Lbl Space does not exist."
                    "LblMgrAvailableLblInLblGroup Failed\n");
        return LBL_FAILURE;
    }

    u1FoundFlag = LBL_FALSE;
    /* Check for the correctness of Key1 info done */
    TMO_SLL_Scan ((tTMO_SLL *) (&(pAsgnInfoHead->Key1InfoList)),
                  pAsgnKey1Info, tAsgnKey1Info *)
    {
        if (pAsgnKey1Info->u4AsgnInfoKey1Val == u4Key1)
        {
            u1FoundFlag = LBL_TRUE;
            break;
        }
    }

    if (u1FoundFlag == LBL_FALSE)
    {
        LBLMGR_DBG (LBLMGR_LBL,
                    "LBL_MGR : Key1 Value mismatch."
                    "LblMgrAvailableLblInLblGroup Failed.\n");
        return LBL_FAILURE;
    }

    /* Check for the correctness of Key2 info done */
    if ((u4Key2 < pAsgnKey1Info->u4AsgnInfoKey2Min) ||
        (u4Key2 > pAsgnKey1Info->u4AsgnInfoKey2Max))
    {
        LBLMGR_DBG (LBLMGR_LBL,
                    "LBL_MGR : Key2 Value mismatch."
                    "LblMgrAvailableLblInLblGroup Failed.\n");
        return LBL_FAILURE;
    }

    /*  Take semaphore */
    OsixSemTake (gLblMgrInfo.SemId);

    /* The Key2 value compare to Min value is determined */
    u4ValFromKey2Min = u4Key2 - (pAsgnKey1Info->u4AsgnInfoKey2Min);

    /* Offset from the Group head is determined */
    u2OffsetFromHead = (UINT2) (u4ValFromKey2Min / KEY2_INFO_BLOCK_SIZE);

    /* Pointer to the corresponding Key2Info strucutre is determined */
    pAsgnKey2Info = pAsgnKey1Info->pAsgnKey2Info;
    for (u2Index = 0; u2Index < u2OffsetFromHead; u2Index++)
    {
        pAsgnKey2Info = pAsgnKey2Info->pNxtAsgnKey2Info;
    }

    /* Determining the byte and bit position in the Key2 info struture */
    u4ValFromMajOffset = u4ValFromKey2Min -
        (u2OffsetFromHead * KEY2_INFO_BLOCK_SIZE);

    if (pAsgnKey2Info->u4AsgnKey2Bmap & aAvlBmap[u4ValFromMajOffset])
    {
        u1FoundFlag = LBL_TRUE;
    }
    else
    {
        u1FoundFlag = LBL_FALSE;
    }

    TMO_SLL_Scan ((tTMO_SLL *) (&(pAsgnInfoHead->Key1InfoList)),
                  pAsgnKey1Info, tAsgnKey1Info *)
    {
        if (pAsgnKey1Info->u2NumInfoKey2Asgn < pAsgnKey1Info->u4MaxKey2Avail)
        {
            pAsgnInfoHead->pAvailAsgnKey1Info = pAsgnKey1Info;
            break;
        }
    }

    /* Releasing the semaphore */
    OsixSemGive (gLblMgrInfo.SemId);

    return ((u1FoundFlag == LBL_FALSE) ? LBL_FAILURE : LBL_SUCCESS);

}

/*---------------------------------------------------------------------------*/
/*
FAQ on Label manager Library:
1) Scope and usage of this library.
2) How do we initialise this libarary.
3) Configuration and Debug support available in this libarary.
*/
/*---------------------------------------------------------------------------*/
/*                        End of file labelmgr.c                             */
/*---------------------------------------------------------------------------*/
