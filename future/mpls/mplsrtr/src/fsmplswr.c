/* $Id: fsmplswr.c,v 1.25 2016/03/04 11:03:20 siva Exp $ */
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsmplslw.h"
# include  "fsmplswr.h"
# include  "fsmplsdb.h"
# include  "mplcmndb.h"
# include  "ldpext.h"
# include  "rpteext.h"
# include  "mplssize.h"
# include  "l2vpextn.h"

/********************************************************************
* Locally Modifed. To Register and Degister the Individual Objects  *
* for the Purpose of GR.                                            * 
********************************************************************/

VOID
RegisterFSMPLRTR ()
{
    RegisterFSMPLSRtrObj ();
    RegisterFSMPLSLdpObj ();
    RegisterFSMPLSRsvpteObj ();
    RegisterFSMPLSL2vpnObj ();
    RegisterFSMPLSScalarObj ();
    AddSysEntry ();
}

VOID
UnRegisterFSMPLRTR ()
{
    UnRegisterFSMPLSRtrObj ();
    UnRegisterFSMPLSLdpObj ();
    UnRegisterFSMPLSRsvpteObj ();
    UnRegisterFSMPLSL2vpnObj ();
    UnRegisterFSMPLSScalarObj ();
    DeleteSysEntry ();
}

INT4
FsMplsAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhGetFsMplsAdminStatus (&(pMultiData->i4_SLongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsQosPolicyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhGetFsMplsQosPolicy (&(pMultiData->i4_SLongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsFmDebugLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhGetFsMplsFmDebugLevel (&(pMultiData->i4_SLongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsTeDebugLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhGetFsMplsTeDebugLevel (&(pMultiData->u4_ULongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLsrLabelAllocationMethodGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_LDP_LOCK ();
    i4Return =
        nmhGetFsMplsLsrLabelAllocationMethod (&(pMultiData->i4_SLongValue));
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsDiffServElspPreConfExpPhbMapIndexGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return =
        nmhGetFsMplsDiffServElspPreConfExpPhbMapIndex (&
                                                       (pMultiData->
                                                        i4_SLongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhSetFsMplsAdminStatus (pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsQosPolicySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhSetFsMplsQosPolicy (pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsFmDebugLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhSetFsMplsFmDebugLevel (pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsTeDebugLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhSetFsMplsTeDebugLevel (pMultiData->u4_ULongValue);
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLsrLabelAllocationMethodSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_LDP_LOCK ();
    i4Return = nmhSetFsMplsLsrLabelAllocationMethod (pMultiData->i4_SLongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsDiffServElspPreConfExpPhbMapIndexSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return =
        nmhSetFsMplsDiffServElspPreConfExpPhbMapIndex (pMultiData->
                                                       i4_SLongValue);
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhTestv2FsMplsAdminStatus (pu4Error, pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsQosPolicyTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhTestv2FsMplsQosPolicy (pu4Error, pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsFmDebugLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return =
        nmhTestv2FsMplsFmDebugLevel (pu4Error, pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsTeDebugLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return =
        nmhTestv2FsMplsTeDebugLevel (pu4Error, pMultiData->u4_ULongValue);
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLsrLabelAllocationMethodTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_LDP_LOCK ();
    i4Return =
        nmhTestv2FsMplsLsrLabelAllocationMethod (pu4Error,
                                                 pMultiData->i4_SLongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsDiffServElspPreConfExpPhbMapIndexTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return =
        nmhTestv2FsMplsDiffServElspPreConfExpPhbMapIndex (pu4Error,
                                                          pMultiData->
                                                          i4_SLongValue);
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsAdminStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsAdminStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsQosPolicyDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsQosPolicy (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsFmDebugLevelDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsFmDebugLevel
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsTeDebugLevelDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsTeDebugLevel
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsLsrLabelAllocationMethodDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsLsrLabelAllocationMethod
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsDiffServElspPreConfExpPhbMapIndexDep (UINT4 *pu4Error,
                                            tSnmpIndexList * pSnmpIndexList,
                                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsDiffServElspPreConfExpPhbMapIndex
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMplsLdpEntityTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_LDP_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        i4Return =
            nmhGetFirstIndexFsMplsLdpEntityTable (pNextMultiIndex->pIndex[0].
                                                  pOctetStrValue,
                                                  &(pNextMultiIndex->pIndex[1].
                                                    u4_ULongValue));
    }
    else
    {
        i4Return =
            nmhGetNextIndexFsMplsLdpEntityTable (pFirstMultiIndex->pIndex[0].
                                                 pOctetStrValue,
                                                 pNextMultiIndex->pIndex[0].
                                                 pOctetStrValue,
                                                 pFirstMultiIndex->pIndex[1].
                                                 u4_ULongValue,
                                                 &(pNextMultiIndex->pIndex[1].
                                                   u4_ULongValue));
    }
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpEntityPHPRequestMethodGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_LDP_LOCK ();
    i4Return =
        nmhGetFsMplsLdpEntityPHPRequestMethod (pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               &(pMultiData->i4_SLongValue));
    MPLS_LDP_UNLOCK ();
    return i4Return;

}

INT4
FsMplsLdpEntityTransAddrTlvEnableGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_LDP_LOCK ();
    i4Return =
        nmhGetFsMplsLdpEntityTransAddrTlvEnable (pMultiIndex->pIndex[0].
                                                 pOctetStrValue,
                                                 pMultiIndex->pIndex[1].
                                                 u4_ULongValue,
                                                 &(pMultiData->i4_SLongValue));
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpEntityTransportAddressGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_LDP_LOCK ();
    i4Return =
        nmhGetFsMplsLdpEntityTransportAddress (pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               &(pMultiData->u4_ULongValue));
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpEntityLdpOverRsvpEnableGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_LDP_LOCK ();
    i4Return =
        nmhGetFsMplsLdpEntityLdpOverRsvpEnable (pMultiIndex->pIndex[0].
                                                pOctetStrValue,
                                                pMultiIndex->pIndex[1].
                                                u4_ULongValue,
                                                &(pMultiData->i4_SLongValue));
    MPLS_LDP_UNLOCK ();
    return i4Return;

}

INT4
FsMplsLdpEntityOutTunnelIndexGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_LDP_LOCK ();
    i4Return =
        nmhGetFsMplsLdpEntityOutTunnelIndex (pMultiIndex->pIndex[0].
                                             pOctetStrValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             &(pMultiData->u4_ULongValue));
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpEntityOutTunnelInstanceGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_LDP_LOCK ();
    i4Return =
        nmhGetFsMplsLdpEntityOutTunnelInstance (pMultiIndex->pIndex[0].
                                                pOctetStrValue,
                                                pMultiIndex->pIndex[1].
                                                u4_ULongValue,
                                                &(pMultiData->u4_ULongValue));
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpEntityOutTunnelIngressLSRIdGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_LDP_LOCK ();
    i4Return =
        nmhGetFsMplsLdpEntityOutTunnelIngressLSRId (pMultiIndex->pIndex[0].
                                                    pOctetStrValue,
                                                    pMultiIndex->pIndex[1].
                                                    u4_ULongValue,
                                                    &(pMultiData->
                                                      u4_ULongValue));
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpEntityOutTunnelEgressLSRIdGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_LDP_LOCK ();
    i4Return =
        nmhGetFsMplsLdpEntityOutTunnelEgressLSRId (pMultiIndex->pIndex[0].
                                                   pOctetStrValue,
                                                   pMultiIndex->pIndex[1].
                                                   u4_ULongValue,
                                                   &(pMultiData->
                                                     u4_ULongValue));
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpEntityInTunnelIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_LDP_LOCK ();
    i4Return =
        nmhGetFsMplsLdpEntityInTunnelIndex (pMultiIndex->pIndex[0].
                                            pOctetStrValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            &(pMultiData->u4_ULongValue));
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpEntityInTunnelInstanceGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_LDP_LOCK ();
    i4Return =
        nmhGetFsMplsLdpEntityInTunnelInstance (pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               &(pMultiData->u4_ULongValue));
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpEntityInTunnelIngressLSRIdGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_LDP_LOCK ();
    i4Return =
        nmhGetFsMplsLdpEntityInTunnelIngressLSRId (pMultiIndex->pIndex[0].
                                                   pOctetStrValue,
                                                   pMultiIndex->pIndex[1].
                                                   u4_ULongValue,
                                                   &(pMultiData->
                                                     u4_ULongValue));
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpEntityInTunnelEgressLSRIdGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsLdpEntityTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_LDP_LOCK ();
    i4Return =
        nmhGetFsMplsLdpEntityInTunnelEgressLSRId (pMultiIndex->pIndex[0].
                                                  pOctetStrValue,
                                                  pMultiIndex->pIndex[1].
                                                  u4_ULongValue,
                                                  &(pMultiData->u4_ULongValue));
    MPLS_LDP_UNLOCK ();
    return i4Return;
}
INT4 FsMplsLdpEntityIpv6TransportAddressGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsLdpEntityTable(
        pMultiIndex->pIndex[0].pOctetStrValue,
        pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsMplsLdpEntityIpv6TransportAddress(
        pMultiIndex->pIndex[0].pOctetStrValue,
        pMultiIndex->pIndex[1].u4_ULongValue,
        pMultiData->pOctetStrValue));

}
INT4 FsMplsLdpEntityIpv6TransAddrTlvEnableGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsLdpEntityTable(
        pMultiIndex->pIndex[0].pOctetStrValue,
        pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsMplsLdpEntityIpv6TransAddrTlvEnable(
        pMultiIndex->pIndex[0].pOctetStrValue,
        pMultiIndex->pIndex[1].u4_ULongValue,
        &(pMultiData->i4_SLongValue)));

}
INT4 FsMplsLdpEntityIpv6TransportAddrKindGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsLdpEntityTable(
        pMultiIndex->pIndex[0].pOctetStrValue,
        pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsMplsLdpEntityIpv6TransportAddrKind(
        pMultiIndex->pIndex[0].pOctetStrValue,
        pMultiIndex->pIndex[1].u4_ULongValue,
        &(pMultiData->i4_SLongValue)));

}
INT4 FsMplsLdpEntityBfdStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsLdpEntityTable(
        pMultiIndex->pIndex[0].pOctetStrValue,
        pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_LDP_LOCK ();
    i4Return = nmhGetFsMplsLdpEntityBfdStatus(
                  pMultiIndex->pIndex[0].pOctetStrValue,
                  pMultiIndex->pIndex[1].u4_ULongValue,
                  &(pMultiData->i4_SLongValue));
    MPLS_LDP_UNLOCK ();
    return i4Return;
}
INT4
FsMplsLdpEntityPHPRequestMethodSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_LDP_LOCK ();
    i4Return =
        nmhSetFsMplsLdpEntityPHPRequestMethod (pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue);
    MPLS_LDP_UNLOCK ();

    return i4Return;
}

INT4
FsMplsLdpEntityTransAddrTlvEnableSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_LDP_LOCK ();
    i4Return =
        nmhSetFsMplsLdpEntityTransAddrTlvEnable (pMultiIndex->pIndex[0].
                                                 pOctetStrValue,
                                                 pMultiIndex->pIndex[1].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpEntityTransportAddressSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_LDP_LOCK ();
    i4Return =
        nmhSetFsMplsLdpEntityTransportAddress (pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiData->u4_ULongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpEntityLdpOverRsvpEnableSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_LDP_LOCK ();
    i4Return =
        nmhSetFsMplsLdpEntityLdpOverRsvpEnable (pMultiIndex->pIndex[0].
                                                pOctetStrValue,
                                                pMultiIndex->pIndex[1].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpEntityOutTunnelIndexSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_LDP_LOCK ();
    i4Return =
        nmhSetFsMplsLdpEntityOutTunnelIndex (pMultiIndex->pIndex[0].
                                             pOctetStrValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiData->u4_ULongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpEntityOutTunnelInstanceSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_LDP_LOCK ();
    i4Return =
        nmhSetFsMplsLdpEntityOutTunnelInstance (pMultiIndex->pIndex[0].
                                                pOctetStrValue,
                                                pMultiIndex->pIndex[1].
                                                u4_ULongValue,
                                                pMultiData->u4_ULongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpEntityOutTunnelIngressLSRIdSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_LDP_LOCK ();
    i4Return =
        nmhSetFsMplsLdpEntityOutTunnelIngressLSRId (pMultiIndex->pIndex[0].
                                                    pOctetStrValue,
                                                    pMultiIndex->pIndex[1].
                                                    u4_ULongValue,
                                                    pMultiData->u4_ULongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpEntityOutTunnelEgressLSRIdSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_LDP_LOCK ();
    i4Return =
        nmhSetFsMplsLdpEntityOutTunnelEgressLSRId (pMultiIndex->pIndex[0].
                                                   pOctetStrValue,
                                                   pMultiIndex->pIndex[1].
                                                   u4_ULongValue,
                                                   pMultiData->u4_ULongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpEntityInTunnelIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_LDP_LOCK ();
    i4Return =
        nmhSetFsMplsLdpEntityInTunnelIndex (pMultiIndex->pIndex[0].
                                            pOctetStrValue,
                                            pMultiIndex->pIndex[1].
                                            u4_ULongValue,
                                            pMultiData->u4_ULongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpEntityInTunnelInstanceSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_LDP_LOCK ();
    i4Return =
        nmhSetFsMplsLdpEntityInTunnelInstance (pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiData->u4_ULongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpEntityInTunnelIngressLSRIdSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_LDP_LOCK ();
    i4Return =
        nmhSetFsMplsLdpEntityInTunnelIngressLSRId (pMultiIndex->pIndex[0].
                                                   pOctetStrValue,
                                                   pMultiIndex->pIndex[1].
                                                   u4_ULongValue,
                                                   pMultiData->u4_ULongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpEntityInTunnelEgressLSRIdSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_LDP_LOCK ();
    i4Return =
        nmhSetFsMplsLdpEntityInTunnelEgressLSRId (pMultiIndex->pIndex[0].
                                                  pOctetStrValue,
                                                  pMultiIndex->pIndex[1].
                                                  u4_ULongValue,
                                                  pMultiData->u4_ULongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}
INT4 FsMplsLdpEntityIpv6TransportAddressSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMplsLdpEntityIpv6TransportAddress(
        pMultiIndex->pIndex[0].pOctetStrValue,
        pMultiIndex->pIndex[1].u4_ULongValue,
        pMultiData->pOctetStrValue));

}

INT4 FsMplsLdpEntityIpv6TransAddrTlvEnableSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMplsLdpEntityIpv6TransAddrTlvEnable(
        pMultiIndex->pIndex[0].pOctetStrValue,
        pMultiIndex->pIndex[1].u4_ULongValue,
        pMultiData->i4_SLongValue));

}

INT4 FsMplsLdpEntityIpv6TransportAddrKindSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMplsLdpEntityIpv6TransportAddrKind(
        pMultiIndex->pIndex[0].pOctetStrValue,
        pMultiIndex->pIndex[1].u4_ULongValue,
        pMultiData->i4_SLongValue));

}
INT4 FsMplsLdpEntityBfdStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_LDP_LOCK ();
    i4Return = nmhSetFsMplsLdpEntityBfdStatus(
                   pMultiIndex->pIndex[0].pOctetStrValue,
                   pMultiIndex->pIndex[1].u4_ULongValue,
                   pMultiData->i4_SLongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}


INT4
FsMplsLdpEntityPHPRequestMethodTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_LDP_LOCK ();
    i4Return = nmhTestv2FsMplsLdpEntityPHPRequestMethod (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         pOctetStrValue,
                                                         pMultiIndex->pIndex[1].
                                                         u4_ULongValue,
                                                         pMultiData->
                                                         i4_SLongValue);
    MPLS_LDP_UNLOCK ();

    return i4Return;
}

INT4
FsMplsLdpEntityTransAddrTlvEnableTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_LDP_LOCK ();
    i4Return =
        nmhTestv2FsMplsLdpEntityTransAddrTlvEnable (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    pOctetStrValue,
                                                    pMultiIndex->pIndex[1].
                                                    u4_ULongValue,
                                                    pMultiData->i4_SLongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpEntityTransportAddressTest (UINT4 *pu4Error,
                                     tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_LDP_LOCK ();
    i4Return =
        nmhTestv2FsMplsLdpEntityTransportAddress (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  pOctetStrValue,
                                                  pMultiIndex->pIndex[1].
                                                  u4_ULongValue,
                                                  pMultiData->u4_ULongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpEntityLdpOverRsvpEnableTest (UINT4 *pu4Error,
                                      tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_LDP_LOCK ();
    i4Return =
        nmhTestv2FsMplsLdpEntityLdpOverRsvpEnable (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   pOctetStrValue,
                                                   pMultiIndex->pIndex[1].
                                                   u4_ULongValue,
                                                   pMultiData->i4_SLongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpEntityOutTunnelIndexTest (UINT4 *pu4Error,
                                   tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_LDP_LOCK ();
    i4Return =
        nmhTestv2FsMplsLdpEntityOutTunnelIndex (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                pOctetStrValue,
                                                pMultiIndex->pIndex[1].
                                                u4_ULongValue,
                                                pMultiData->u4_ULongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpEntityOutTunnelInstanceTest (UINT4 *pu4Error,
                                      tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_LDP_LOCK ();
    i4Return =
        nmhTestv2FsMplsLdpEntityOutTunnelInstance (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   pOctetStrValue,
                                                   pMultiIndex->pIndex[1].
                                                   u4_ULongValue,
                                                   pMultiData->u4_ULongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpEntityOutTunnelIngressLSRIdTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_LDP_LOCK ();
    i4Return =
        nmhTestv2FsMplsLdpEntityOutTunnelIngressLSRId (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       pOctetStrValue,
                                                       pMultiIndex->pIndex[1].
                                                       u4_ULongValue,
                                                       pMultiData->
                                                       u4_ULongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpEntityOutTunnelEgressLSRIdTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_LDP_LOCK ();
    i4Return =
        nmhTestv2FsMplsLdpEntityOutTunnelEgressLSRId (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      pOctetStrValue,
                                                      pMultiIndex->pIndex[1].
                                                      u4_ULongValue,
                                                      pMultiData->
                                                      u4_ULongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpEntityInTunnelIndexTest (UINT4 *pu4Error,
                                  tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_LDP_LOCK ();
    i4Return =
        nmhTestv2FsMplsLdpEntityInTunnelIndex (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiData->u4_ULongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpEntityInTunnelInstanceTest (UINT4 *pu4Error,
                                     tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_LDP_LOCK ();
    i4Return =
        nmhTestv2FsMplsLdpEntityInTunnelInstance (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  pOctetStrValue,
                                                  pMultiIndex->pIndex[1].
                                                  u4_ULongValue,
                                                  pMultiData->u4_ULongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpEntityInTunnelIngressLSRIdTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_LDP_LOCK ();
    i4Return =
        nmhTestv2FsMplsLdpEntityInTunnelIngressLSRId (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      pOctetStrValue,
                                                      pMultiIndex->pIndex[1].
                                                      u4_ULongValue,
                                                      pMultiData->
                                                      u4_ULongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpEntityInTunnelEgressLSRIdTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_LDP_LOCK ();
    i4Return =
        nmhTestv2FsMplsLdpEntityInTunnelEgressLSRId (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     pOctetStrValue,
                                                     pMultiIndex->pIndex[1].
                                                     u4_ULongValue,
                                                     pMultiData->u4_ULongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}
INT4 FsMplsLdpEntityIpv6TransportAddressTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsMplsLdpEntityIpv6TransportAddress(pu4Error,
        pMultiIndex->pIndex[0].pOctetStrValue,
        pMultiIndex->pIndex[1].u4_ULongValue,
        pMultiData->pOctetStrValue));

}

INT4 FsMplsLdpEntityIpv6TransAddrTlvEnableTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsMplsLdpEntityIpv6TransAddrTlvEnable(pu4Error,
        pMultiIndex->pIndex[0].pOctetStrValue,
        pMultiIndex->pIndex[1].u4_ULongValue,
        pMultiData->i4_SLongValue));

}

INT4 FsMplsLdpEntityIpv6TransportAddrKindTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsMplsLdpEntityIpv6TransportAddrKind(pu4Error,
        pMultiIndex->pIndex[0].pOctetStrValue,
        pMultiIndex->pIndex[1].u4_ULongValue,
        pMultiData->i4_SLongValue));

}
INT4 FsMplsLdpEntityBfdStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_LDP_LOCK ();
    i4Return = nmhTestv2FsMplsLdpEntityBfdStatus(pu4Error,
                    pMultiIndex->pIndex[0].pOctetStrValue,
                    pMultiIndex->pIndex[1].u4_ULongValue,
                    pMultiData->i4_SLongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}


INT4
FsMplsLdpEntityTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsLdpEntityTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsLdpLsrIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;

    UNUSED_PARAM (pMultiIndex);

    MPLS_LDP_LOCK ();

    i4Return = nmhGetFsMplsLdpLsrId (pMultiData->pOctetStrValue);

    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpForceOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;

    UNUSED_PARAM (pMultiIndex);

    MPLS_LDP_LOCK ();

    i4Return = nmhGetFsMplsLdpForceOption (&(pMultiData->i4_SLongValue));

    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpLsrIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;

    UNUSED_PARAM (pMultiIndex);

    MPLS_LDP_LOCK ();

    i4Return = nmhSetFsMplsLdpLsrId (pMultiData->pOctetStrValue);

    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpForceOptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;

    UNUSED_PARAM (pMultiIndex);

    MPLS_LDP_LOCK ();

    i4Return = nmhSetFsMplsLdpForceOption (pMultiData->i4_SLongValue);

    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpLsrIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;

    UNUSED_PARAM (pMultiIndex);

    MPLS_LDP_LOCK ();

    i4Return = nmhTestv2FsMplsLdpLsrId (pu4Error, pMultiData->pOctetStrValue);

    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpForceOptionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;

    UNUSED_PARAM (pMultiIndex);

    MPLS_LDP_LOCK ();

    i4Return =
        nmhTestv2FsMplsLdpForceOption (pu4Error, pMultiData->i4_SLongValue);

    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLdpLsrIdDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsLdpLsrId (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsLdpForceOptionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsLdpForceOption (pu4Error, pSnmpIndexList,
                                          pSnmpvarbinds));
}

INT4
FsMplsMaxIfTableEntriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhGetFsMplsMaxIfTableEntries (&(pMultiData->u4_ULongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsMaxFTNEntriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhGetFsMplsMaxFTNEntries (&(pMultiData->u4_ULongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsMaxInSegmentEntriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhGetFsMplsMaxInSegmentEntries (&(pMultiData->u4_ULongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsMaxOutSegmentEntriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhGetFsMplsMaxOutSegmentEntries (&(pMultiData->u4_ULongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsMaxXCEntriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhGetFsMplsMaxXCEntries (&(pMultiData->u4_ULongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsDiffServElspMapEntriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return =
        nmhGetFsMplsDiffServElspMapEntries (&(pMultiData->u4_ULongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsDiffServParamsEntriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhGetFsMplsDiffServParamsEntries (&(pMultiData->u4_ULongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsMaxHopListsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhGetFsMplsMaxHopLists (&(pMultiData->u4_ULongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsMaxPathOptPerListGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhGetFsMplsMaxPathOptPerList (&(pMultiData->u4_ULongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsMaxHopsPerPathOptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhGetFsMplsMaxHopsPerPathOption (&(pMultiData->u4_ULongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsMaxArHopListsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhGetFsMplsMaxArHopLists (&(pMultiData->u4_ULongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsMaxHopsPerArHopListGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhGetFsMplsMaxHopsPerArHopList (&(pMultiData->u4_ULongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsMaxRsvpTrfcParamsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_RSVPTE_LOCK ();
    i4Return = nmhGetFsMplsMaxRsvpTrfcParams (&(pMultiData->u4_ULongValue));
    MPLS_RSVPTE_UNLOCK ();
    return i4Return;
}

INT4
FsMplsMaxCrLdpTrfcParamsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_LDP_LOCK ();
    i4Return = nmhGetFsMplsMaxCrLdpTrfcParams (&(pMultiData->u4_ULongValue));
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsMaxDServElspsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhGetFsMplsMaxDServElsps (&(pMultiData->u4_ULongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsMaxDServLlspsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhGetFsMplsMaxDServLlsps (&(pMultiData->u4_ULongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsMaxTnlsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhGetFsMplsMaxTnls (&(pMultiData->u4_ULongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLsrMaxLdpEntitiesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_LDP_LOCK ();
    i4Return = nmhGetFsMplsLsrMaxLdpEntities (&(pMultiData->i4_SLongValue));
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLsrMaxLocalPeersGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhGetFsMplsLsrMaxLocalPeers (&(pMultiData->i4_SLongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLsrMaxRemotePeersGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhGetFsMplsLsrMaxRemotePeers (&(pMultiData->i4_SLongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLsrMaxIfacesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_LDP_LOCK ();
    i4Return = nmhGetFsMplsLsrMaxIfaces (&(pMultiData->i4_SLongValue));
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLsrMaxLspsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_LDP_LOCK ();
    i4Return = nmhGetFsMplsLsrMaxLsps (&(pMultiData->i4_SLongValue));
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLsrMaxVcMergeCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_LDP_LOCK ();
    i4Return = nmhGetFsMplsLsrMaxVcMergeCount (&(pMultiData->i4_SLongValue));
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLsrMaxVpMergeCountGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_LDP_LOCK ();
    i4Return = nmhGetFsMplsLsrMaxVpMergeCount (&(pMultiData->i4_SLongValue));
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLsrMaxCrlspTnlsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_LDP_LOCK ();
    i4Return = nmhGetFsMplsLsrMaxCrlspTnls (&(pMultiData->i4_SLongValue));
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsActiveRsvpTeTnlsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_RSVPTE_LOCK ();
    i4Return = nmhGetFsMplsActiveRsvpTeTnls (&(pMultiData->i4_SLongValue));
    MPLS_RSVPTE_UNLOCK ();
    return i4Return;
}

INT4
FsMplsActiveLspsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhGetFsMplsActiveLsps (&(pMultiData->i4_SLongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsActiveCrLspsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_LDP_LOCK ();
    i4Return = nmhGetFsMplsActiveCrLsps (&(pMultiData->i4_SLongValue));
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsCrlspDebugLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_LDP_LOCK ();
    i4Return = nmhGetFsMplsCrlspDebugLevel (&(pMultiData->i4_SLongValue));
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsCrlspDumpTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_LDP_LOCK ();
    i4Return = nmhGetFsMplsCrlspDumpType (&(pMultiData->i4_SLongValue));
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsCrlspDebugLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_LDP_LOCK ();
    i4Return = nmhSetFsMplsCrlspDebugLevel (pMultiData->i4_SLongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsCrlspDumpTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_LDP_LOCK ();
    i4Return = nmhSetFsMplsCrlspDumpType (pMultiData->i4_SLongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsCrlspDebugLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_LDP_LOCK ();
    i4Return =
        nmhTestv2FsMplsCrlspDebugLevel (pu4Error, pMultiData->i4_SLongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsCrlspDumpTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_LDP_LOCK ();
    i4Return =
        nmhTestv2FsMplsCrlspDumpType (pu4Error, pMultiData->i4_SLongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsCrlspDebugLevelDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsCrlspDebugLevel
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsCrlspDumpTypeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsCrlspDumpType
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMplsCrlspTnlTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_LDP_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        i4Return =
            nmhGetFirstIndexFsMplsCrlspTnlTable (&
                                                 (pNextMultiIndex->pIndex[0].
                                                  u4_ULongValue));
    }
    else
    {
        i4Return =
            nmhGetNextIndexFsMplsCrlspTnlTable (pFirstMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                &(pNextMultiIndex->pIndex[0].
                                                  u4_ULongValue));
    }
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsCrlspTnlRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsCrlspTnlTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_LDP_LOCK ();
    i4Return =
        nmhGetFsMplsCrlspTnlRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue));
    MPLS_LDP_UNLOCK ();
    return i4Return;

}

INT4
FsMplsCrlspTnlStorageTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsCrlspTnlTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_LDP_LOCK ();
    i4Return =
        nmhGetFsMplsCrlspTnlStorageType (pMultiIndex->pIndex[0].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue));
    MPLS_LDP_UNLOCK ();
    return i4Return;

}

INT4
FsMplsCrlspTnlRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_LDP_LOCK ();
    i4Return =
        nmhSetFsMplsCrlspTnlRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->i4_SLongValue);
    MPLS_LDP_UNLOCK ();

    return i4Return;
}

INT4
FsMplsCrlspTnlStorageTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_LDP_LOCK ();
    i4Return =
        nmhSetFsMplsCrlspTnlStorageType (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->i4_SLongValue);
    MPLS_LDP_UNLOCK ();

    return i4Return;
}

INT4
FsMplsCrlspTnlRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_LDP_LOCK ();
    i4Return = nmhTestv2FsMplsCrlspTnlRowStatus (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue);
    MPLS_LDP_UNLOCK ();

    return i4Return;
}

INT4
FsMplsCrlspTnlStorageTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_LDP_LOCK ();
    i4Return = nmhTestv2FsMplsCrlspTnlStorageType (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiData->i4_SLongValue);
    MPLS_LDP_UNLOCK ();

    return i4Return;
}

INT4
FsMplsCrlspTnlTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsCrlspTnlTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsCrlspDumpDirectionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_LDP_LOCK ();
    i4Return = nmhGetFsMplsCrlspDumpDirection (&(pMultiData->i4_SLongValue));
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsCrlspPersistanceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_LDP_LOCK ();
    i4Return = nmhGetFsMplsCrlspPersistance (&(pMultiData->i4_SLongValue));
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsCrlspDumpDirectionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_LDP_LOCK ();
    i4Return = nmhSetFsMplsCrlspDumpDirection (pMultiData->i4_SLongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsCrlspPersistanceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_LDP_LOCK ();
    i4Return = nmhSetFsMplsCrlspPersistance (pMultiData->i4_SLongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsCrlspDumpDirectionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_LDP_LOCK ();
    i4Return =
        nmhTestv2FsMplsCrlspDumpDirection (pu4Error, pMultiData->i4_SLongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsCrlspPersistanceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_LDP_LOCK ();
    i4Return =
        nmhTestv2FsMplsCrlspPersistance (pu4Error, pMultiData->i4_SLongValue);
    MPLS_LDP_UNLOCK ();
    return i4Return;
}

INT4
FsMplsCrlspDumpDirectionDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsCrlspDumpDirection
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsCrlspPersistanceDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsCrlspPersistance
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMplsTunnelRSVPResTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    INT4                i4Return = SNMP_FAILURE;
    /* Common Lock is removed from here and moved to nmh routines because nmh routine will be used for 
     * CLI and WEBNM related operations also. */
    if (pFirstMultiIndex == NULL)
    {
        i4Return =
            nmhGetFirstIndexFsMplsTunnelRSVPResTable (&
                                                      (pNextMultiIndex->
                                                       pIndex[0].
                                                       u4_ULongValue));
    }
    else
    {
        i4Return =
            nmhGetNextIndexFsMplsTunnelRSVPResTable (pFirstMultiIndex->
                                                     pIndex[0].u4_ULongValue,
                                                     &(pNextMultiIndex->
                                                       pIndex[0].
                                                       u4_ULongValue));
    }
    return i4Return;
}

INT4
FsMplsTunnelRSVPResTokenBucketRateGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsTunnelRSVPResTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Common Lock is removed from here and moved to nmh routines because nmh routine will be used for 
     * CLI and WEBNM related operations also. */
    i4Return =
        nmhGetFsMplsTunnelRSVPResTokenBucketRate (pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  &(pMultiData->u4_ULongValue));
    return i4Return;

}

INT4
FsMplsTunnelRSVPResTokenBucketSizeGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsTunnelRSVPResTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Common Lock is removed from here and moved to nmh routines because nmh routine will be used for 
     * CLI and WEBNM related operations also. */
    i4Return =
        nmhGetFsMplsTunnelRSVPResTokenBucketSize (pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  &(pMultiData->u4_ULongValue));
    return i4Return;

}

INT4
FsMplsTunnelRSVPResPeakDataRateGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsTunnelRSVPResTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Common Lock is removed from here and moved to nmh routines because nmh routine will be used for 
     * CLI and WEBNM related operations also. */
    i4Return =
        nmhGetFsMplsTunnelRSVPResPeakDataRate (pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               &(pMultiData->u4_ULongValue));
    return i4Return;
}

INT4
FsMplsTunnelRSVPResMinimumPolicedUnitGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsTunnelRSVPResTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Common Lock is removed from here and moved to nmh routines because nmh routine will be used for 
     * CLI and WEBNM related operations also. */
    i4Return =
        nmhGetFsMplsTunnelRSVPResMinimumPolicedUnit (pMultiIndex->pIndex[0].
                                                     u4_ULongValue,
                                                     &(pMultiData->
                                                       i4_SLongValue));
    return i4Return;
}

INT4
FsMplsTunnelRSVPResMaximumPacketSizeGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsTunnelRSVPResTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Common Lock is removed from here and moved to nmh routines because nmh routine will be used for 
     * CLI and WEBNM related operations also. */
    i4Return =
        nmhGetFsMplsTunnelRSVPResMaximumPacketSize (pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    &(pMultiData->
                                                      i4_SLongValue));
    return i4Return;

}

INT4
FsMplsTunnelRSVPResTokenBucketRateSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    /* Common Lock is removed from here and moved to nmh routines because nmh routine will be used for 
     * CLI and WEBNM related operations also. */
    i4Return =
        nmhSetFsMplsTunnelRSVPResTokenBucketRate (pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiData->u4_ULongValue);
    return i4Return;
}

INT4
FsMplsTunnelRSVPResTokenBucketSizeSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    /* Common Lock is removed from here and moved to nmh routines because nmh routine will be used for 
     * CLI and WEBNM related operations also. */
    i4Return =
        nmhSetFsMplsTunnelRSVPResTokenBucketSize (pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiData->u4_ULongValue);
    return i4Return;
}

INT4
FsMplsTunnelRSVPResPeakDataRateSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    /* Common Lock is removed from here and moved to nmh routines because nmh routine will be used for 
     * CLI and WEBNM related operations also. */
    i4Return =
        nmhSetFsMplsTunnelRSVPResPeakDataRate (pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiData->u4_ULongValue);
    return i4Return;
}

INT4
FsMplsTunnelRSVPResMinimumPolicedUnitSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    /* Common Lock is removed from here and moved to nmh routines because nmh routine will be used for 
     * CLI and WEBNM related operations also. */
    i4Return =
        nmhSetFsMplsTunnelRSVPResMinimumPolicedUnit (pMultiIndex->pIndex[0].
                                                     u4_ULongValue,
                                                     pMultiData->i4_SLongValue);
    return i4Return;
}

INT4
FsMplsTunnelRSVPResMaximumPacketSizeSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    /* Common Lock is removed from here and moved to nmh routines because nmh routine will be used for 
     * CLI and WEBNM related operations also. */
    i4Return =
        nmhSetFsMplsTunnelRSVPResMaximumPacketSize (pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiData->i4_SLongValue);
    return i4Return;
}

INT4
FsMplsTunnelRSVPResTokenBucketRateTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    /* Common Lock is removed from here and moved to nmh routines because nmh routine 
     * will be used for CLI and WEBNM related operations also. */
    i4Return = nmhTestv2FsMplsTunnelRSVPResTokenBucketRate (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            u4_ULongValue,
                                                            pMultiData->
                                                            u4_ULongValue);
    return i4Return;
}

INT4
FsMplsTunnelRSVPResTokenBucketSizeTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    /* Common Lock is removed from here and moved to nmh routines because nmh routine will be used for 
     * CLI and WEBNM related operations also. */
    i4Return = nmhTestv2FsMplsTunnelRSVPResTokenBucketSize (pu4Error,
                                                            pMultiIndex->
                                                            pIndex[0].
                                                            u4_ULongValue,
                                                            pMultiData->
                                                            u4_ULongValue);
    return i4Return;
}

INT4
FsMplsTunnelRSVPResPeakDataRateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    /* Common Lock is removed from here and moved to nmh routines because nmh routine will be used for 
     * CLI and WEBNM related operations also. */
    i4Return = nmhTestv2FsMplsTunnelRSVPResPeakDataRate (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         u4_ULongValue,
                                                         pMultiData->
                                                         u4_ULongValue);
    return i4Return;
}

INT4
FsMplsTunnelRSVPResMinimumPolicedUnitTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    /* Common Lock is removed from here and moved to nmh routines because nmh routine will be used for 
     * CLI and WEBNM related operations also. */
    i4Return = nmhTestv2FsMplsTunnelRSVPResMinimumPolicedUnit (pu4Error,
                                                               pMultiIndex->
                                                               pIndex[0].
                                                               u4_ULongValue,
                                                               pMultiData->
                                                               i4_SLongValue);
    return i4Return;
}

INT4
FsMplsTunnelRSVPResMaximumPacketSizeTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    /* Common Lock is removed from here and moved to nmh routines because nmh routine will be used for 
     * CLI and WEBNM related operations also. */
    i4Return = nmhTestv2FsMplsTunnelRSVPResMaximumPacketSize (pu4Error,
                                                              pMultiIndex->
                                                              pIndex[0].
                                                              u4_ULongValue,
                                                              pMultiData->
                                                              i4_SLongValue);
    return i4Return;
}

INT4
FsMplsTunnelRSVPResTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsTunnelRSVPResTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMplsTunnelCRLDPResTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    INT4                i4Return = SNMP_FAILURE;
    /* Common Lock is removed from here and moved to nmh routines because nmh routine will be used for 
     * CLI and WEBNM related operations also. */
    if (pFirstMultiIndex == NULL)
    {
        i4Return =
            nmhGetFirstIndexFsMplsTunnelCRLDPResTable (&
                                                       (pNextMultiIndex->
                                                        pIndex[0].
                                                        u4_ULongValue));
    }
    else
    {
        i4Return =
            nmhGetNextIndexFsMplsTunnelCRLDPResTable (pFirstMultiIndex->
                                                      pIndex[0].u4_ULongValue,
                                                      &(pNextMultiIndex->
                                                        pIndex[0].
                                                        u4_ULongValue));
    }
    return i4Return;
}

INT4
FsMplsTunnelCRLDPResPeakDataRateGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsTunnelCRLDPResTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Common Lock is removed from here and moved to nmh routines because nmh routine will be used for 
     * CLI and WEBNM related operations also. */
    i4Return =
        nmhGetFsMplsTunnelCRLDPResPeakDataRate (pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                &(pMultiData->u4_ULongValue));
    return i4Return;
}

INT4
FsMplsTunnelCRLDPResCommittedDataRateGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsTunnelCRLDPResTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Common Lock is removed from here and moved to nmh routines because nmh routine will be used for 
     * CLI and WEBNM related operations also. */
    i4Return =
        nmhGetFsMplsTunnelCRLDPResCommittedDataRate (pMultiIndex->pIndex[0].
                                                     u4_ULongValue,
                                                     &(pMultiData->
                                                       u4_ULongValue));
    return i4Return;
}

INT4
FsMplsTunnelCRLDPResPeakBurstSizeGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsTunnelCRLDPResTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Common Lock is removed from here and moved to nmh routines because nmh routine will be used for 
     * CLI and WEBNM related operations also. */
    i4Return =
        nmhGetFsMplsTunnelCRLDPResPeakBurstSize (pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 &(pMultiData->u4_ULongValue));
    return i4Return;
}

INT4
FsMplsTunnelCRLDPResCommittedBurstSizeGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsTunnelCRLDPResTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Common Lock is removed from here and moved to nmh routines because nmh routine will be used for 
     * CLI and WEBNM related operations also. */
    i4Return =
        nmhGetFsMplsTunnelCRLDPResCommittedBurstSize (pMultiIndex->pIndex[0].
                                                      u4_ULongValue,
                                                      &(pMultiData->
                                                        u4_ULongValue));
    return i4Return;
}

INT4
FsMplsTunnelCRLDPResExcessBurstSizeGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsTunnelCRLDPResTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    /* Common Lock is removed from here and moved to nmh routines because nmh routine will be used for 
     * CLI and WEBNM related operations also. */
    i4Return =
        nmhGetFsMplsTunnelCRLDPResExcessBurstSize (pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   &(pMultiData->
                                                     u4_ULongValue));
    return i4Return;
}

INT4
FsMplsTunnelCRLDPResPeakDataRateSet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    /* Common Lock is removed from here and moved to nmh routines because nmh routine will be used for 
     * CLI and WEBNM related operations also. */
    i4Return =
        nmhSetFsMplsTunnelCRLDPResPeakDataRate (pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiData->u4_ULongValue);
    return i4Return;
}

INT4
FsMplsTunnelCRLDPResCommittedDataRateSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    /* Common Lock is removed from here and moved to nmh routines because nmh routine will be used for 
     * CLI and WEBNM related operations also. */
    i4Return =
        nmhSetFsMplsTunnelCRLDPResCommittedDataRate (pMultiIndex->pIndex[0].
                                                     u4_ULongValue,
                                                     pMultiData->u4_ULongValue);
    return i4Return;
}

INT4
FsMplsTunnelCRLDPResPeakBurstSizeSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    /* Common Lock is removed from here and moved to nmh routines because nmh routine will be used for 
     * CLI and WEBNM related operations also. */
    i4Return =
        nmhSetFsMplsTunnelCRLDPResPeakBurstSize (pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiData->u4_ULongValue);
    return i4Return;
}

INT4
FsMplsTunnelCRLDPResCommittedBurstSizeSet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    /* Common Lock is removed from here and moved to nmh routines because nmh routine will be used for 
     * CLI and WEBNM related operations also. */
    i4Return =
        nmhSetFsMplsTunnelCRLDPResCommittedBurstSize (pMultiIndex->pIndex[0].
                                                      u4_ULongValue,
                                                      pMultiData->
                                                      u4_ULongValue);
    return i4Return;
}

INT4
FsMplsTunnelCRLDPResExcessBurstSizeSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    /* Common Lock is removed from here and moved to nmh routines because nmh routine will be used for 
     * CLI and WEBNM related operations also. */
    i4Return =
        nmhSetFsMplsTunnelCRLDPResExcessBurstSize (pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiData->u4_ULongValue);
    return i4Return;
}

INT4
FsMplsTunnelCRLDPResPeakDataRateTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    /* Common Lock is removed from here and moved to nmh routines because nmh routine will be used for 
     * CLI and WEBNM related operations also. */
    i4Return = nmhTestv2FsMplsTunnelCRLDPResPeakDataRate (pu4Error,
                                                          pMultiIndex->
                                                          pIndex[0].
                                                          u4_ULongValue,
                                                          pMultiData->
                                                          u4_ULongValue);
    return i4Return;
}

INT4
FsMplsTunnelCRLDPResCommittedDataRateTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    /* Common Lock is removed from here and moved to nmh routines because nmh routine will be used for 
     * CLI and WEBNM related operations also. */
    i4Return = nmhTestv2FsMplsTunnelCRLDPResCommittedDataRate (pu4Error,
                                                               pMultiIndex->
                                                               pIndex[0].
                                                               u4_ULongValue,
                                                               pMultiData->
                                                               u4_ULongValue);
    return i4Return;
}

INT4
FsMplsTunnelCRLDPResPeakBurstSizeTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    /* Common Lock is removed from here and moved to nmh routines because nmh routine will be used for 
     * CLI and WEBNM related operations also. */
    i4Return = nmhTestv2FsMplsTunnelCRLDPResPeakBurstSize (pu4Error,
                                                           pMultiIndex->
                                                           pIndex[0].
                                                           u4_ULongValue,
                                                           pMultiData->
                                                           u4_ULongValue);
    return i4Return;
}

INT4
FsMplsTunnelCRLDPResCommittedBurstSizeTest (UINT4 *pu4Error,
                                            tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    /* Common Lock is removed from here and moved to nmh routines because nmh routine will be used for 
     * CLI and WEBNM related operations also. */
    i4Return = nmhTestv2FsMplsTunnelCRLDPResCommittedBurstSize (pu4Error,
                                                                pMultiIndex->
                                                                pIndex[0].
                                                                u4_ULongValue,
                                                                pMultiData->
                                                                u4_ULongValue);
    return i4Return;
}

INT4
FsMplsTunnelCRLDPResExcessBurstSizeTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    /* Common Lock is removed from here and moved to nmh routines because nmh routine will be used for 
     * CLI and WEBNM related operations also. */
    i4Return = nmhTestv2FsMplsTunnelCRLDPResExcessBurstSize (pu4Error,
                                                             pMultiIndex->
                                                             pIndex[0].
                                                             u4_ULongValue,
                                                             pMultiData->
                                                             u4_ULongValue);
    return i4Return;
}

INT4
FsMplsTunnelCRLDPResTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsTunnelCRLDPResTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMplsDiffServElspMapTable (tSnmpIndex * pFirstMultiIndex,
                                        tSnmpIndex * pNextMultiIndex)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        i4Return =
            nmhGetFirstIndexFsMplsDiffServElspMapTable (&
                                                        (pNextMultiIndex->
                                                         pIndex[0].
                                                         i4_SLongValue),
                                                        &(pNextMultiIndex->
                                                          pIndex[1].
                                                          i4_SLongValue));
    }
    else
    {
        i4Return =
            nmhGetNextIndexFsMplsDiffServElspMapTable (pFirstMultiIndex->
                                                       pIndex[0].i4_SLongValue,
                                                       &(pNextMultiIndex->
                                                         pIndex[0].
                                                         i4_SLongValue),
                                                       pFirstMultiIndex->
                                                       pIndex[1].i4_SLongValue,
                                                       &(pNextMultiIndex->
                                                         pIndex[1].
                                                         i4_SLongValue));
    }
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsDiffServElspMapPhbDscpGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsDiffServElspMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_CMN_LOCK ();
    i4Return =
        nmhGetFsMplsDiffServElspMapPhbDscp (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[1].
                                            i4_SLongValue,
                                            &(pMultiData->i4_SLongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;

}

INT4
FsMplsDiffServElspMapStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsDiffServElspMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_CMN_LOCK ();
    i4Return =
        nmhGetFsMplsDiffServElspMapStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           &(pMultiData->i4_SLongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;

}

INT4
FsMplsDiffServElspMapPhbDscpSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return =
        nmhSetFsMplsDiffServElspMapPhbDscp (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiIndex->pIndex[1].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServElspMapStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return =
        nmhSetFsMplsDiffServElspMapStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiIndex->pIndex[1].i4_SLongValue,
                                           pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServElspMapPhbDscpTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return = nmhTestv2FsMplsDiffServElspMapPhbDscp (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[1].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServElspMapStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return = nmhTestv2FsMplsDiffServElspMapStatus (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[1].
                                                     i4_SLongValue,
                                                     pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServElspMapTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsDiffServElspMapTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMplsDiffServParamsTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        i4Return =
            nmhGetFirstIndexFsMplsDiffServParamsTable (&
                                                       (pNextMultiIndex->
                                                        pIndex[0].
                                                        i4_SLongValue));
    }
    else
    {
        i4Return =
            nmhGetNextIndexFsMplsDiffServParamsTable (pFirstMultiIndex->
                                                      pIndex[0].i4_SLongValue,
                                                      &(pNextMultiIndex->
                                                        pIndex[0].
                                                        i4_SLongValue));
    }
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsDiffServParamsServiceTypeGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsDiffServParamsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_CMN_LOCK ();
    i4Return =
        nmhGetFsMplsDiffServParamsServiceType (pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               &(pMultiData->i4_SLongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;

}

INT4
FsMplsDiffServParamsLlspPscDscpGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsDiffServParamsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_CMN_LOCK ();
    i4Return =
        nmhGetFsMplsDiffServParamsLlspPscDscp (pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               &(pMultiData->i4_SLongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;

}

INT4
FsMplsDiffServParamsElspTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsDiffServParamsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_CMN_LOCK ();
    i4Return =
        nmhGetFsMplsDiffServParamsElspType (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            &(pMultiData->i4_SLongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;

}

INT4
FsMplsDiffServParamsElspSigExpPhbMapIndexGet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsDiffServParamsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_CMN_LOCK ();
    i4Return =
        nmhGetFsMplsDiffServParamsElspSigExpPhbMapIndex (pMultiIndex->pIndex[0].
                                                         i4_SLongValue,
                                                         &(pMultiData->
                                                           i4_SLongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;

}

INT4
FsMplsDiffServParamsStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsDiffServParamsTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_CMN_LOCK ();
    i4Return =
        nmhGetFsMplsDiffServParamsStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;

}

INT4
FsMplsDiffServParamsServiceTypeSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return =
        nmhSetFsMplsDiffServParamsServiceType (pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServParamsLlspPscDscpSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return =
        nmhSetFsMplsDiffServParamsLlspPscDscp (pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServParamsElspTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return =
        nmhSetFsMplsDiffServParamsElspType (pMultiIndex->pIndex[0].
                                            i4_SLongValue,
                                            pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServParamsElspSigExpPhbMapIndexSet (tSnmpIndex * pMultiIndex,
                                              tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return =
        nmhSetFsMplsDiffServParamsElspSigExpPhbMapIndex (pMultiIndex->pIndex[0].
                                                         i4_SLongValue,
                                                         pMultiData->
                                                         i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServParamsStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return =
        nmhSetFsMplsDiffServParamsStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServParamsServiceTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return = nmhTestv2FsMplsDiffServParamsServiceType (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         i4_SLongValue,
                                                         pMultiData->
                                                         i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServParamsLlspPscDscpTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return = nmhTestv2FsMplsDiffServParamsLlspPscDscp (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         i4_SLongValue,
                                                         pMultiData->
                                                         i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServParamsElspTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return = nmhTestv2FsMplsDiffServParamsElspType (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      i4_SLongValue,
                                                      pMultiData->
                                                      i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServParamsElspSigExpPhbMapIndexTest (UINT4 *pu4Error,
                                               tSnmpIndex * pMultiIndex,
                                               tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return = nmhTestv2FsMplsDiffServParamsElspSigExpPhbMapIndex (pu4Error,
                                                                   pMultiIndex->
                                                                   pIndex[0].
                                                                   i4_SLongValue,
                                                                   pMultiData->
                                                                   i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServParamsStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return = nmhTestv2FsMplsDiffServParamsStatus (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServParamsTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsDiffServParamsTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMplsDiffServTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    INT4                i4Return = SNMP_FAILURE;
    /* Common Lock is removed from here and moved to nmh routines because nmh routine will be used for 
     * CLI and WEBNM related operations also. */
    if (pFirstMultiIndex == NULL)
    {
        i4Return =
            nmhGetFirstIndexFsMplsDiffServTable (&
                                                 (pNextMultiIndex->pIndex[0].
                                                  u4_ULongValue),
                                                 &(pNextMultiIndex->pIndex[1].
                                                   u4_ULongValue),
                                                 &(pNextMultiIndex->pIndex[2].
                                                   u4_ULongValue),
                                                 &(pNextMultiIndex->pIndex[3].
                                                   u4_ULongValue));
    }
    else
    {
        i4Return =
            nmhGetNextIndexFsMplsDiffServTable (pFirstMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                &(pNextMultiIndex->pIndex[0].
                                                  u4_ULongValue),
                                                pFirstMultiIndex->pIndex[1].
                                                u4_ULongValue,
                                                &(pNextMultiIndex->pIndex[1].
                                                  u4_ULongValue),
                                                pFirstMultiIndex->pIndex[2].
                                                u4_ULongValue,
                                                &(pNextMultiIndex->pIndex[2].
                                                  u4_ULongValue),
                                                pFirstMultiIndex->pIndex[3].
                                                u4_ULongValue,
                                                &(pNextMultiIndex->pIndex[3].
                                                  u4_ULongValue));
    }
    return i4Return;
}

INT4
FsMplsDiffServClassTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsDiffServTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_CMN_LOCK ();
    i4Return =
        nmhGetFsMplsDiffServClassType (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;

}

INT4
FsMplsDiffServServiceTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsDiffServTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_CMN_LOCK ();
    i4Return =
        nmhGetFsMplsDiffServServiceType (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;

}

INT4
FsMplsDiffServLlspPscGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsDiffServTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_CMN_LOCK ();
    i4Return =
        nmhGetFsMplsDiffServLlspPsc (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;

}

INT4
FsMplsDiffServElspTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsDiffServTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_CMN_LOCK ();
    i4Return =
        nmhGetFsMplsDiffServElspType (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      &(pMultiData->i4_SLongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;

}

INT4
FsMplsDiffServElspListIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsDiffServTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_CMN_LOCK ();
    i4Return =
        nmhGetFsMplsDiffServElspListIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiIndex->pIndex[3].u4_ULongValue,
                                           &(pMultiData->i4_SLongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;

}

INT4
FsMplsDiffServRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsDiffServTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_CMN_LOCK ();
    i4Return =
        nmhGetFsMplsDiffServRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       &(pMultiData->i4_SLongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;

}

INT4
FsMplsDiffServStorageTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsDiffServTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_CMN_LOCK ();
    i4Return =
        nmhGetFsMplsDiffServStorageType (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         &(pMultiData->i4_SLongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;

}

INT4
FsMplsDiffServClassTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return =
        nmhSetFsMplsDiffServClassType (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsRouterIDDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRouterID (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMplsPortTable (tSnmpIndex * pFirstMultiIndex,
                             tSnmpIndex * pNextMultiIndex)
{
    MPLS_L2VPN_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMplsPortTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMplsPortTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    MPLS_L2VPN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMplsPortBundleStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_L2VPN_LOCK ();
    i4Return =
        nmhGetFsMplsPortBundleStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                      &(pMultiData->i4_SLongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsPortMultiplexStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_L2VPN_LOCK ();
    i4Return =
        nmhGetFsMplsPortMultiplexStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsPortAllToOneBundleStatusGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_L2VPN_LOCK ();
    i4Return =
        nmhGetFsMplsPortAllToOneBundleStatus (pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              &(pMultiData->i4_SLongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsPortRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsPortTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_L2VPN_LOCK ();
    i4Return = nmhGetFsMplsPortRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                          &(pMultiData->i4_SLongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsPortBundleStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return =
        (nmhSetFsMplsPortBundleStatus
         (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsPortMultiplexStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return =
        (nmhSetFsMplsPortMultiplexStatus
         (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsPortAllToOneBundleStatusSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return =
        (nmhSetFsMplsPortAllToOneBundleStatus
         (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->i4_SLongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;

}

INT4
FsMplsPortRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = (nmhSetFsMplsPortRowStatus (pMultiIndex->pIndex[0].i4_SLongValue,
                                           pMultiData->i4_SLongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;

}

INT4
FsMplsPortBundleStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = (nmhTestv2FsMplsPortBundleStatus (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;

}

INT4
FsMplsPortMultiplexStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = (nmhTestv2FsMplsPortMultiplexStatus (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    i4_SLongValue,
                                                    pMultiData->i4_SLongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;

}

INT4
FsMplsPortAllToOneBundleStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = (nmhTestv2FsMplsPortAllToOneBundleStatus (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         i4_SLongValue,
                                                         pMultiData->
                                                         i4_SLongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsPortRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = (nmhTestv2FsMplsPortRowStatus (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              i4_SLongValue,
                                              pMultiData->i4_SLongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsPortTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsPortTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexFsMplsVplsAcMapTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexFsMplsVplsAcMapTable(
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			&(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexFsMplsVplsAcMapTable(
			pFirstMultiIndex->pIndex[0].u4_ULongValue,
			&(pNextMultiIndex->pIndex[0].u4_ULongValue),
			pFirstMultiIndex->pIndex[1].u4_ULongValue,
			&(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 FsMplsVplsAcMapPortIfIndexGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMplsVplsAcMapTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMplsVplsAcMapPortIfIndex(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsMplsVplsAcMapPortVlanGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMplsVplsAcMapTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMplsVplsAcMapPortVlan(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 FsMplsVplsAcMapRowStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceFsMplsVplsAcMapTable(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetFsMplsVplsAcMapRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 FsMplsVplsAcMapPortIfIndexSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsMplsVplsAcMapPortIfIndex(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsMplsVplsAcMapPortVlanSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsMplsVplsAcMapPortVlan(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsMplsVplsAcMapRowStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetFsMplsVplsAcMapRowStatus(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsMplsVplsAcMapPortIfIndexTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsMplsVplsAcMapPortIfIndex(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsMplsVplsAcMapPortVlanTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsMplsVplsAcMapPortVlan(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->u4_ULongValue));

}

INT4 FsMplsVplsAcMapRowStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2FsMplsVplsAcMapRowStatus(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiIndex->pIndex[1].u4_ULongValue,
		pMultiData->i4_SLongValue));

}

INT4 FsMplsVplsAcMapTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2FsMplsVplsAcMapTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4
FsMplsDiffServServiceTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return =
        nmhSetFsMplsDiffServServiceType (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServLlspPscSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return =
        nmhSetFsMplsDiffServLlspPsc (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiIndex->pIndex[1].u4_ULongValue,
                                     pMultiIndex->pIndex[2].u4_ULongValue,
                                     pMultiIndex->pIndex[3].u4_ULongValue,
                                     pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServElspTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return =
        nmhSetFsMplsDiffServElspType (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiIndex->pIndex[1].u4_ULongValue,
                                      pMultiIndex->pIndex[2].u4_ULongValue,
                                      pMultiIndex->pIndex[3].u4_ULongValue,
                                      pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServElspListIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return =
        nmhSetFsMplsDiffServElspListIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiIndex->pIndex[1].u4_ULongValue,
                                           pMultiIndex->pIndex[2].u4_ULongValue,
                                           pMultiIndex->pIndex[3].u4_ULongValue,
                                           pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return =
        nmhSetFsMplsDiffServRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiIndex->pIndex[2].u4_ULongValue,
                                       pMultiIndex->pIndex[3].u4_ULongValue,
                                       pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServStorageTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return =
        nmhSetFsMplsDiffServStorageType (pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].u4_ULongValue,
                                         pMultiIndex->pIndex[3].u4_ULongValue,
                                         pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServClassTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return = nmhTestv2FsMplsDiffServClassType (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[1].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[2].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[3].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServServiceTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return = nmhTestv2FsMplsDiffServServiceType (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiIndex->pIndex[1].
                                                   u4_ULongValue,
                                                   pMultiIndex->pIndex[2].
                                                   u4_ULongValue,
                                                   pMultiIndex->pIndex[3].
                                                   u4_ULongValue,
                                                   pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServLlspPscTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return = nmhTestv2FsMplsDiffServLlspPsc (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[1].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[2].
                                               u4_ULongValue,
                                               pMultiIndex->pIndex[3].
                                               u4_ULongValue,
                                               pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServElspTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return = nmhTestv2FsMplsDiffServElspType (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[1].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[2].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[3].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServElspListIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return = nmhTestv2FsMplsDiffServElspListIndex (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[1].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[2].
                                                     u4_ULongValue,
                                                     pMultiIndex->pIndex[3].
                                                     u4_ULongValue,
                                                     pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return = nmhTestv2FsMplsDiffServRowStatus (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[1].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[2].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[3].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServStorageTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return = nmhTestv2FsMplsDiffServStorageType (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiIndex->pIndex[1].
                                                   u4_ULongValue,
                                                   pMultiIndex->pIndex[2].
                                                   u4_ULongValue,
                                                   pMultiIndex->pIndex[3].
                                                   u4_ULongValue,
                                                   pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsDiffServTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMplsDiffServElspInfoTable (tSnmpIndex * pFirstMultiIndex,
                                         tSnmpIndex * pNextMultiIndex)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        i4Return =
            nmhGetFirstIndexFsMplsDiffServElspInfoTable (&
                                                         (pNextMultiIndex->
                                                          pIndex[0].
                                                          i4_SLongValue),
                                                         &(pNextMultiIndex->
                                                           pIndex[1].
                                                           i4_SLongValue));
    }
    else
    {
        i4Return =
            nmhGetNextIndexFsMplsDiffServElspInfoTable (pFirstMultiIndex->
                                                        pIndex[0].i4_SLongValue,
                                                        &(pNextMultiIndex->
                                                          pIndex[0].
                                                          i4_SLongValue),
                                                        pFirstMultiIndex->
                                                        pIndex[1].i4_SLongValue,
                                                        &(pNextMultiIndex->
                                                          pIndex[1].
                                                          i4_SLongValue));
    }
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsDiffServElspInfoPHBGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsDiffServElspInfoTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_CMN_LOCK ();
    i4Return =
        nmhGetFsMplsDiffServElspInfoPHB (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         &(pMultiData->i4_SLongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;

}

INT4
FsMplsDiffServElspInfoResourcePointerGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsDiffServElspInfoTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_CMN_LOCK ();
    i4Return =
        nmhGetFsMplsDiffServElspInfoResourcePointer (pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[1].
                                                     i4_SLongValue,
                                                     pMultiData->pOidValue);
    MPLS_CMN_UNLOCK ();
    return i4Return;

}

INT4
FsMplsDiffServElspInfoRowStatusGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsDiffServElspInfoTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_CMN_LOCK ();
    i4Return =
        nmhGetFsMplsDiffServElspInfoRowStatus (pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[1].
                                               i4_SLongValue,
                                               &(pMultiData->i4_SLongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;

}

INT4
FsMplsDiffServElspInfoStorageTypeGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsDiffServElspInfoTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_CMN_LOCK ();
    i4Return =
        nmhGetFsMplsDiffServElspInfoStorageType (pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[1].
                                                 i4_SLongValue,
                                                 &(pMultiData->i4_SLongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;

}

INT4
FsMplsDiffServElspInfoPHBSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return =
        nmhSetFsMplsDiffServElspInfoPHB (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].i4_SLongValue,
                                         pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServElspInfoResourcePointerSet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return =
        nmhSetFsMplsDiffServElspInfoResourcePointer (pMultiIndex->pIndex[0].
                                                     i4_SLongValue,
                                                     pMultiIndex->pIndex[1].
                                                     i4_SLongValue,
                                                     pMultiData->pOidValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServElspInfoRowStatusSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return =
        nmhSetFsMplsDiffServElspInfoRowStatus (pMultiIndex->pIndex[0].
                                               i4_SLongValue,
                                               pMultiIndex->pIndex[1].
                                               i4_SLongValue,
                                               pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServElspInfoStorageTypeSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return =
        nmhSetFsMplsDiffServElspInfoStorageType (pMultiIndex->pIndex[0].
                                                 i4_SLongValue,
                                                 pMultiIndex->pIndex[1].
                                                 i4_SLongValue,
                                                 pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServElspInfoPHBTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return = nmhTestv2FsMplsDiffServElspInfoPHB (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   i4_SLongValue,
                                                   pMultiIndex->pIndex[1].
                                                   i4_SLongValue,
                                                   pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServElspInfoResourcePointerTest (UINT4 *pu4Error,
                                           tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return = nmhTestv2FsMplsDiffServElspInfoResourcePointer (pu4Error,
                                                               pMultiIndex->
                                                               pIndex[0].
                                                               i4_SLongValue,
                                                               pMultiIndex->
                                                               pIndex[1].
                                                               i4_SLongValue,
                                                               pMultiData->
                                                               pOidValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServElspInfoRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return = nmhTestv2FsMplsDiffServElspInfoRowStatus (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         i4_SLongValue,
                                                         pMultiIndex->pIndex[1].
                                                         i4_SLongValue,
                                                         pMultiData->
                                                         i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServElspInfoStorageTypeTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_CMN_LOCK ();
    i4Return = nmhTestv2FsMplsDiffServElspInfoStorageType (pu4Error,
                                                           pMultiIndex->
                                                           pIndex[0].
                                                           i4_SLongValue,
                                                           pMultiIndex->
                                                           pIndex[1].
                                                           i4_SLongValue,
                                                           pMultiData->
                                                           i4_SLongValue);
    MPLS_CMN_UNLOCK ();

    return i4Return;
}

INT4
FsMplsDiffServElspInfoTableDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsDiffServElspInfoTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsDiffServElspInfoListIndexNextGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return =
        nmhGetFsMplsDiffServElspInfoListIndexNext (&
                                                   (pMultiData->i4_SLongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsTnlModelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhGetFsMplsTnlModel (&(pMultiData->i4_SLongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsRsrcMgmtTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhGetFsMplsRsrcMgmtType (&(pMultiData->i4_SLongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsTTLValGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhGetFsMplsTTLVal (&(pMultiData->i4_SLongValue));
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsTnlModelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhSetFsMplsTnlModel (pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsRsrcMgmtTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhSetFsMplsRsrcMgmtType (pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsTTLValSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhSetFsMplsTTLVal (pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsTnlModelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhTestv2FsMplsTnlModel (pu4Error, pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsRsrcMgmtTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return =
        nmhTestv2FsMplsRsrcMgmtType (pu4Error, pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsTTLValTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                  tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_CMN_LOCK ();
    i4Return = nmhTestv2FsMplsTTLVal (pu4Error, pMultiData->i4_SLongValue);
    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsTnlModelDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsTnlModel (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsrcMgmtTypeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsrcMgmtType
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsTTLValDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                 tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsTTLVal (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsDsTeStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsDsTeStatus (&(pMultiData->i4_SLongValue)));
}

INT4
FsMplsDsTeStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsDsTeStatus (pMultiData->i4_SLongValue));
}

INT4
FsMplsDsTeStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsDsTeStatus (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMplsDsTeStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsDsTeStatus (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMplsDsTeClassTypeTable (tSnmpIndex * pFirstMultiIndex,
                                      tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMplsDsTeClassTypeTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMplsDsTeClassTypeTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMplsDsTeClassTypeDescriptionGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsDsTeClassTypeTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetFsMplsDsTeClassTypeDescription
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsMplsDsTeClassTypeRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsDsTeClassTypeTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetFsMplsDsTeClassTypeRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMplsDsTeClassTypeBwPercentageGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsDsTeClassTypeTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetFsMplsDsTeClassTypeBwPercentage
            (pMultiIndex->pIndex[0].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMplsDsTeClassTypeDescriptionSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhSetFsMplsDsTeClassTypeDescription
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsMplsDsTeClassTypeRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMplsDsTeClassTypeRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsMplsDsTeClassTypeBwPercentageSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetFsMplsDsTeClassTypeBwPercentage
            (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsMplsDsTeClassTypeDescriptionTest (UINT4 *pu4Error,
                                    tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhTestv2FsMplsDsTeClassTypeDescription
            (pu4Error, pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiData->pOctetStrValue));

}

INT4
FsMplsDsTeClassTypeRowStatusTest (UINT4 *pu4Error,
                                  tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2FsMplsDsTeClassTypeRowStatus
            (pu4Error, pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiData->i4_SLongValue));

}

INT4
FsMplsDsTeClassTypeBwPercentageTest (UINT4 *pu4Error,
                                     tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2FsMplsDsTeClassTypeBwPercentage
            (pu4Error, pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiData->i4_SLongValue));

}

INT4
FsMplsDsTeClassTypeTableDep (UINT4 *pu4Error,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsDsTeClassTypeTable (pu4Error,
                                              pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMplsDsTeClassTypeToTcMapTable (tSnmpIndex * pFirstMultiIndex,
                                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMplsDsTeClassTypeToTcMapTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMplsDsTeClassTypeToTcMapTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMplsDsTeTcTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsDsTeClassTypeToTcMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetFsMplsDsTeTcType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMplsDsTeTcDescriptionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsDsTeClassTypeToTcMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetFsMplsDsTeTcDescription
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsMplsDsTeTcMapRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsDsTeClassTypeToTcMapTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetFsMplsDsTeTcMapRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMplsDsTeTcTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMplsDsTeTcType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsMplsDsTeTcDescriptionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMplsDsTeTcDescription
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsMplsDsTeTcMapRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMplsDsTeTcMapRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsMplsDsTeTcTypeTest (UINT4 *pu4Error,
                      tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsMplsDsTeTcType
            (pu4Error, pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsMplsDsTeTcDescriptionTest (UINT4 *pu4Error,
                             tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsMplsDsTeTcDescription
            (pu4Error, pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsMplsDsTeTcMapRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsMplsDsTeTcMapRowStatus
            (pu4Error, pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsMplsDsTeClassTypeToTcMapTableDep (UINT4 *pu4Error,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsDsTeClassTypeToTcMapTable (pu4Error,
                                                     pSnmpIndexList,
                                                     pSnmpvarbinds));
}

INT4
GetNextIndexFsMplsDsTeTeClassTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMplsDsTeTeClassTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMplsDsTeTeClassTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMplsDsTeTeClassDescGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsDsTeTeClassTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsDsTeTeClassDesc
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsMplsDsTeTeClassNumberGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsDsTeTeClassTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetFsMplsDsTeTeClassNumber
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
FsMplsDsTeTeClassRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsDsTeTeClassTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return (nmhGetFsMplsDsTeTeClassRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMplsDsTeTeClassDescSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMplsDsTeTeClassDesc
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsMplsDsTeTeClassNumberSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMplsDsTeTeClassNumber
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsMplsDsTeTeClassRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMplsDsTeTeClassRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsMplsDsTeTeClassDescTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2FsMplsDsTeTeClassDesc
            (pu4Error, pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
FsMplsDsTeTeClassNumberTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2FsMplsDsTeTeClassNumber
            (pu4Error, pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->u4_ULongValue));

}

INT4
FsMplsDsTeTeClassRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2FsMplsDsTeTeClassRowStatus
            (pu4Error, pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
FsMplsDsTeTeClassTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsDsTeTeClassTable (pu4Error,
                                            pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsL2VpnTrcFlagGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return = nmhGetFsMplsL2VpnTrcFlag (&(pMultiData->i4_SLongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnCleanupIntervalGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return = nmhGetFsMplsL2VpnCleanupInterval (&(pMultiData->i4_SLongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return = nmhGetFsMplsL2VpnAdminStatus (&(pMultiData->i4_SLongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnTrcFlagSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return = nmhSetFsMplsL2VpnTrcFlag (pMultiData->i4_SLongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnCleanupIntervalSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return = nmhSetFsMplsL2VpnCleanupInterval (pMultiData->i4_SLongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return = nmhSetFsMplsL2VpnAdminStatus (pMultiData->i4_SLongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnTrcFlagTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return =
        nmhTestv2FsMplsL2VpnTrcFlag (pu4Error, pMultiData->i4_SLongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnCleanupIntervalTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return =
        nmhTestv2FsMplsL2VpnCleanupInterval (pu4Error,
                                             pMultiData->i4_SLongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return =
        nmhTestv2FsMplsL2VpnAdminStatus (pu4Error, pMultiData->i4_SLongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnTrcFlagDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsL2VpnTrcFlag
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsL2VpnCleanupIntervalDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsL2VpnCleanupInterval
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsL2VpnAdminStatusDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsL2VpnAdminStatus
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMplsL2VpnPwTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    MPLS_L2VPN_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMplsL2VpnPwTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMplsL2VpnPwTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    MPLS_L2VPN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMplsL2VpnPwModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsL2VpnPwTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_L2VPN_LOCK ();
    i4Return = nmhGetFsMplsL2VpnPwMode (pMultiIndex->pIndex[0].u4_ULongValue,
                                        &(pMultiData->i4_SLongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnVplsIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsL2VpnPwTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_L2VPN_LOCK ();
    i4Return = nmhGetFsMplsL2VpnVplsIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &(pMultiData->u4_ULongValue));

    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnPwModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhSetFsMplsL2VpnPwMode (pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->i4_SLongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;

}

INT4
FsMplsL2VpnPwModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhTestv2FsMplsL2VpnPwMode (pu4Error,
                                           pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->i4_SLongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnVplsIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhSetFsMplsL2VpnVplsIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->u4_ULongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;

}

INT4
FsMplsL2VpnVplsIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhTestv2FsMplsL2VpnVplsIndex (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiData->u4_ULongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnPwLocalCapabAdvertGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsL2VpnPwTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_L2VPN_LOCK ();
    i4Return = nmhGetFsMplsL2VpnPwLocalCapabAdvert (pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiData->pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnPwLocalCCSelectedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsL2VpnPwTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_L2VPN_LOCK ();
    i4Return = nmhGetFsMplsL2VpnPwLocalCCSelected (pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiData->pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnPwLocalCVSelectedGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsL2VpnPwTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_L2VPN_LOCK ();
    i4Return = nmhGetFsMplsL2VpnPwLocalCVSelected (pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiData->pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnPwLocalCCAdvertGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsL2VpnPwTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_L2VPN_LOCK ();
    i4Return = nmhGetFsMplsL2VpnPwLocalCCAdvert (pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiData->pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnPwLocalCVAdvertGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsL2VpnPwTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_L2VPN_LOCK ();
    i4Return = nmhGetFsMplsL2VpnPwLocalCVAdvert (pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiData->pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnPwRemoteCCAdvertGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsL2VpnPwTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_L2VPN_LOCK ();
    i4Return = nmhGetFsMplsL2VpnPwRemoteCCAdvert (pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiData->pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnPwRemoteCVAdvertGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsL2VpnPwTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_L2VPN_LOCK ();
    i4Return = nmhGetFsMplsL2VpnPwRemoteCVAdvert (pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiData->pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnPwOamEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsL2VpnPwTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_L2VPN_LOCK ();
    i4Return = nmhGetFsMplsL2VpnPwOamEnable (pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             &(pMultiData->i4_SLongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnPwGenAGITypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsL2VpnPwTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_L2VPN_LOCK ();
    i4Return = nmhGetFsMplsL2VpnPwGenAGIType (pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              &(pMultiData->u4_ULongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnPwGenLocalAIITypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsL2VpnPwTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_L2VPN_LOCK ();
    i4Return = nmhGetFsMplsL2VpnPwGenLocalAIIType (pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   &(pMultiData->
                                                     u4_ULongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnPwGenRemoteAIITypeGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsL2VpnPwTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_L2VPN_LOCK ();
    i4Return = nmhGetFsMplsL2VpnPwGenRemoteAIIType (pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    &(pMultiData->
                                                      u4_ULongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnProactiveOamSsnIndexGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsL2VpnPwTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_L2VPN_LOCK ();
    i4Return = nmhGetFsMplsL2VpnProactiveOamSsnIndex (pMultiIndex->pIndex[0].
                                                      u4_ULongValue,
                                                      &(pMultiData->
                                                        u4_ULongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnPwAIIFormatGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsL2VpnPwTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_L2VPN_LOCK ();
    i4Return =
        nmhGetFsMplsL2VpnPwAIIFormat (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnIsStaticPwGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsL2VpnPwTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_L2VPN_LOCK ();
    i4Return =
        nmhGetFsMplsL2VpnIsStaticPw (pMultiIndex->pIndex[0].u4_ULongValue,
                                     &(pMultiData->i4_SLongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnNextFreePwEnetPwInstanceGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;

    if (nmhValidateIndexInstanceFsMplsL2VpnPwTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_L2VPN_LOCK ();

    i4Return =
        nmhGetFsMplsL2VpnNextFreePwEnetPwInstance (pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   &(pMultiData->
                                                     u4_ULongValue));

    MPLS_L2VPN_UNLOCK ();
    return i4Return;

}

INT4
FsMplsL2VpnPwSynchronizationStatusGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;

    if (nmhValidateIndexInstanceFsMplsL2VpnPwTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    MPLS_L2VPN_LOCK ();

    i4Return = nmhGetFsMplsL2VpnPwSynchronizationStatus
        (pMultiIndex->pIndex[0].u4_ULongValue, &(pMultiData->i4_SLongValue));

    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnPwLocalCapabAdvertSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhSetFsMplsL2VpnPwLocalCapabAdvert (pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiData->pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnPwLocalCCAdvertSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhSetFsMplsL2VpnPwLocalCCAdvert (pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiData->pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnPwLocalCVAdvertSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhSetFsMplsL2VpnPwLocalCVAdvert (pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiData->pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnPwRemoteCCAdvertSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhSetFsMplsL2VpnPwRemoteCCAdvert (pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiData->pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnPwRemoteCVAdvertSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhSetFsMplsL2VpnPwRemoteCVAdvert (pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiData->pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnPwOamEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhSetFsMplsL2VpnPwOamEnable (pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnPwGenAGITypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhSetFsMplsL2VpnPwGenAGIType (pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiData->u4_ULongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnPwGenLocalAIITypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhSetFsMplsL2VpnPwGenLocalAIIType (pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiData->u4_ULongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnPwGenRemoteAIITypeSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhSetFsMplsL2VpnPwGenRemoteAIIType (pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiData->u4_ULongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnPwAIIFormatSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return =
        nmhSetFsMplsL2VpnPwAIIFormat (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnPwLocalCapabAdvertTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhTestv2FsMplsL2VpnPwLocalCapabAdvert (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       u4_ULongValue,
                                                       pMultiData->
                                                       pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnPwLocalCCAdvertTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhTestv2FsMplsL2VpnPwLocalCCAdvert (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiData->pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnPwLocalCVAdvertTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhTestv2FsMplsL2VpnPwLocalCVAdvert (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiData->pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnPwRemoteCCAdvertTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhTestv2FsMplsL2VpnPwRemoteCCAdvert (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     u4_ULongValue,
                                                     pMultiData->
                                                     pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnPwRemoteCVAdvertTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhTestv2FsMplsL2VpnPwRemoteCVAdvert (pu4Error,
                                                     pMultiIndex->pIndex[0].
                                                     u4_ULongValue,
                                                     pMultiData->
                                                     pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnPwOamEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhTestv2FsMplsL2VpnPwOamEnable (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnPwGenAGITypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhTestv2FsMplsL2VpnPwGenAGIType (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiData->u4_ULongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnPwGenLocalAIITypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhTestv2FsMplsL2VpnPwGenLocalAIIType (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      u4_ULongValue,
                                                      pMultiData->
                                                      u4_ULongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnPwGenRemoteAIITypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhTestv2FsMplsL2VpnPwGenRemoteAIIType (pu4Error,
                                                       pMultiIndex->pIndex[0].
                                                       u4_ULongValue,
                                                       pMultiData->
                                                       u4_ULongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnPwAIIFormatTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhTestv2FsMplsL2VpnPwAIIFormat (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiData->pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLocalCCTypesCapabilitiesGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return =
        nmhGetFsMplsLocalCCTypesCapabilities (pMultiData->pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLocalCVTypesCapabilitiesGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return =
        nmhGetFsMplsLocalCVTypesCapabilities (pMultiData->pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsHwCCTypeCapabilitiesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return = nmhGetFsMplsHwCCTypeCapabilities (pMultiData->pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLocalCCTypesCapabilitiesSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return =
        nmhSetFsMplsLocalCCTypesCapabilities (pMultiData->pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLocalCVTypesCapabilitiesSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return =
        nmhSetFsMplsLocalCVTypesCapabilities (pMultiData->pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLocalCCTypesCapabilitiesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return = nmhTestv2FsMplsLocalCCTypesCapabilities (pu4Error,
                                                        pMultiData->
                                                        pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLocalCVTypesCapabilitiesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return = nmhTestv2FsMplsLocalCVTypesCapabilities (pu4Error,
                                                        pMultiData->
                                                        pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsLocalCCTypesCapabilitiesDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsLocalCCTypesCapabilities
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsLocalCVTypesCapabilitiesDep (UINT4 *pu4Error,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsLocalCVTypesCapabilities
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsL2VpnPwTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsL2VpnPwTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMplsVplsConfigTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    MPLS_L2VPN_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMplsVplsConfigTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMplsVplsConfigTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    MPLS_L2VPN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsMplsVplsVsiGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsVplsConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_L2VPN_LOCK ();
    i4Return = nmhGetFsMplsVplsVsi (pMultiIndex->pIndex[0].u4_ULongValue,
                                    &(pMultiData->i4_SLongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsVplsVpnIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsVplsConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_L2VPN_LOCK ();
    i4Return = nmhGetFsMplsVplsVpnId (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsVplsNameGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsVplsConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_L2VPN_LOCK ();
    i4Return = nmhGetFsMplsVplsName (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsVplsDescrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsVplsConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_L2VPN_LOCK ();
    i4Return = nmhGetFsMplsVplsDescr (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsVplsFdbHighWatermarkGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsVplsConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_L2VPN_LOCK ();
    i4Return = nmhGetFsMplsVplsFdbHighWatermark
        (pMultiIndex->pIndex[0].u4_ULongValue, &(pMultiData->u4_ULongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsVplsFdbLowWatermarkGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsVplsConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_L2VPN_LOCK ();
    i4Return = nmhGetFsMplsVplsFdbLowWatermark
        (pMultiIndex->pIndex[0].u4_ULongValue, &(pMultiData->u4_ULongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;

}

INT4
FsMplsVplsRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsVplsConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_L2VPN_LOCK ();
    i4Return = nmhGetFsMplsVplsRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                          &(pMultiData->i4_SLongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;

}

/* VPLS FDB */
INT4
FsMplsVplsL2MapFdbIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsVplsConfigTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MPLS_L2VPN_LOCK ();
    i4Return = nmhGetFsMplsVplsL2MapFdbId (pMultiIndex->pIndex[0].u4_ULongValue,
                                           &(pMultiData->i4_SLongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4 
FsmplsVplsMtuGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
	if (nmhValidateIndexInstanceFsMplsVplsConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	MPLS_L2VPN_LOCK ();
	i4Return = nmhGetFsmplsVplsMtu(pMultiIndex->pIndex[0].u4_ULongValue,
                                   &(pMultiData->u4_ULongValue));
	MPLS_L2VPN_UNLOCK ();
    return i4Return;

}
INT4 
FsmplsVplsStorageTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
	if (nmhValidateIndexInstanceFsMplsVplsConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	MPLS_L2VPN_LOCK ();
	i4Return = nmhGetFsmplsVplsStorageType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue));
	MPLS_L2VPN_UNLOCK ();
    return i4Return;

}
INT4 
FsmplsVplsSignalingTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
	if (nmhValidateIndexInstanceFsMplsVplsConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	MPLS_L2VPN_LOCK ();
	i4Return = nmhGetFsmplsVplsSignalingType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue));
	MPLS_L2VPN_UNLOCK ();
    return i4Return;

}
INT4 
FsmplsVplsControlWordGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
	if (nmhValidateIndexInstanceFsMplsVplsConfigTable(
		pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	MPLS_L2VPN_LOCK ();
	i4Return = nmhGetFsmplsVplsControlWord(
		pMultiIndex->pIndex[0].u4_ULongValue,
		&(pMultiData->i4_SLongValue));
	MPLS_L2VPN_UNLOCK ();
    return i4Return;

}

INT4
FsMplsVplsVsiSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhSetFsMplsVplsVsi (pMultiIndex->pIndex[0].u4_ULongValue,
                                    pMultiData->i4_SLongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsVplsVpnIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhSetFsMplsVplsVpnId (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsVplsNameSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhSetFsMplsVplsName (pMultiIndex->pIndex[0].u4_ULongValue,
                                     pMultiData->pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsVplsDescrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhSetFsMplsVplsDescr (pMultiIndex->pIndex[0].u4_ULongValue,
                                      pMultiData->pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsVplsFdbHighWatermarkSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhSetFsMplsVplsFdbHighWatermark
        (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsVplsFdbLowWatermarkSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhSetFsMplsVplsFdbLowWatermark
        (pMultiIndex->pIndex[0].u4_ULongValue, pMultiData->u4_ULongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsVplsRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhSetFsMplsVplsRowStatus (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->i4_SLongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

/* VPLS FDB */
INT4
FsMplsVplsL2MapFdbIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhSetFsMplsVplsL2MapFdbId (pMultiIndex->pIndex[0].u4_ULongValue,
                                           pMultiData->i4_SLongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4 
FsmplsVplsMtuSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
	i4Return = nmhSetFsmplsVplsMtu(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->u4_ULongValue);
	MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4 
FsmplsVplsStorageTypeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
	i4Return = nmhSetFsmplsVplsStorageType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue);
	MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4 
FsmplsVplsSignalingTypeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
	i4Return = nmhSetFsmplsVplsSignalingType(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue);
	MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4 
FsmplsVplsControlWordSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
	i4Return = nmhSetFsmplsVplsControlWord(
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue);
	MPLS_L2VPN_UNLOCK ();
    return i4Return;
}


INT4
FsMplsVplsVsiTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                   tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhTestv2FsMplsVplsVsi (pu4Error,
                                       pMultiIndex->pIndex[0].u4_ULongValue,
                                       pMultiData->i4_SLongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsVplsVpnIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhTestv2FsMplsVplsVpnId (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsVplsNameTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhTestv2FsMplsVplsName (pu4Error,
                                        pMultiIndex->pIndex[0].u4_ULongValue,
                                        pMultiData->pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsVplsDescrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhTestv2FsMplsVplsDescr (pu4Error,
                                         pMultiIndex->pIndex[0].u4_ULongValue,
                                         pMultiData->pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsVplsFdbHighWatermarkTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhTestv2FsMplsVplsFdbHighWatermark (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiData->u4_ULongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsVplsFdbLowWatermarkTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhTestv2FsMplsVplsFdbLowWatermark (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   u4_ULongValue,
                                                   pMultiData->u4_ULongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsVplsRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhTestv2FsMplsVplsRowStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             pMultiData->i4_SLongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

/* VPLS FDB */
INT4
FsMplsVplsL2MapFdbIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                          tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
    i4Return = nmhTestv2FsMplsVplsL2MapFdbId (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiData->i4_SLongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4 
FsmplsVplsMtuTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
	i4Return = nmhTestv2FsmplsVplsMtu(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->u4_ULongValue);
	MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4 
FsmplsVplsStorageTypeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
	i4Return = nmhTestv2FsmplsVplsStorageType(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue);
	MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4 
FsmplsVplsSignalingTypeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
	i4Return = nmhTestv2FsmplsVplsSignalingType(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4 
FsmplsVplsControlWordTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    MPLS_L2VPN_LOCK ();
	i4Return = nmhTestv2FsmplsVplsControlWord(pu4Error,
		pMultiIndex->pIndex[0].u4_ULongValue,
		pMultiData->i4_SLongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsVplsConfigTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsVplsConfigTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsPwMplsInboundTableDep (UINT4 *pu4Error,
                         tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsPwMplsInboundTable (pu4Error,
                                          pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsL2VpnMaxPwVcEntriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return = nmhGetFsMplsL2VpnMaxPwVcEntries (&(pMultiData->u4_ULongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnMaxPwVcMplsEntriesGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return =
        nmhGetFsMplsL2VpnMaxPwVcMplsEntries (&(pMultiData->u4_ULongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnMaxPwVcMplsInOutEntriesGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return =
        nmhGetFsMplsL2VpnMaxPwVcMplsInOutEntries (&(pMultiData->u4_ULongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnMaxEthernetPwVcsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return = nmhGetFsMplsL2VpnMaxEthernetPwVcs (&(pMultiData->u4_ULongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnMaxPwVcEnetEntriesGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return =
        nmhGetFsMplsL2VpnMaxPwVcEnetEntries (&(pMultiData->u4_ULongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnActivePwVcEntriesGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return =
        nmhGetFsMplsL2VpnActivePwVcEntries (&(pMultiData->u4_ULongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnActivePwVcMplsEntriesGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return =
        nmhGetFsMplsL2VpnActivePwVcMplsEntries (&(pMultiData->u4_ULongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnActivePwVcEnetEntriesGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return =
        nmhGetFsMplsL2VpnActivePwVcEnetEntries (&(pMultiData->u4_ULongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnNoOfPwVcEntriesCreatedGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return =
        nmhGetFsMplsL2VpnNoOfPwVcEntriesCreated (&(pMultiData->u4_ULongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnNoOfPwVcEntriesDeletedGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return =
        nmhGetFsMplsL2VpnNoOfPwVcEntriesDeleted (&(pMultiData->u4_ULongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnMaxPwVcEntriesSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return = nmhSetFsMplsL2VpnMaxPwVcEntries (pMultiData->u4_ULongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnMaxPwVcMplsEntriesSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return = nmhSetFsMplsL2VpnMaxPwVcMplsEntries (pMultiData->u4_ULongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnMaxPwVcMplsInOutEntriesSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return =
        nmhSetFsMplsL2VpnMaxPwVcMplsInOutEntries (pMultiData->u4_ULongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnMaxEthernetPwVcsSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return = nmhSetFsMplsL2VpnMaxEthernetPwVcs (pMultiData->u4_ULongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnMaxPwVcEnetEntriesSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return = nmhSetFsMplsL2VpnMaxPwVcEnetEntries (pMultiData->u4_ULongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnMaxPwVcEntriesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return =
        nmhTestv2FsMplsL2VpnMaxPwVcEntries (pu4Error,
                                            pMultiData->u4_ULongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnMaxPwVcMplsEntriesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return =
        nmhTestv2FsMplsL2VpnMaxPwVcMplsEntries (pu4Error,
                                                pMultiData->u4_ULongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnMaxPwVcMplsInOutEntriesTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return =
        nmhTestv2FsMplsL2VpnMaxPwVcMplsInOutEntries (pu4Error,
                                                     pMultiData->u4_ULongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnMaxEthernetPwVcsTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return =
        nmhTestv2FsMplsL2VpnMaxEthernetPwVcs (pu4Error,
                                              pMultiData->u4_ULongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnMaxPwVcEnetEntriesTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);
    MPLS_L2VPN_LOCK ();
    i4Return =
        nmhTestv2FsMplsL2VpnMaxPwVcEnetEntries (pu4Error,
                                                pMultiData->u4_ULongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
GetNextIndexFsPwMplsInboundTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    MPLS_L2VPN_LOCK ();
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsPwMplsInboundTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsPwMplsInboundTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            MPLS_L2VPN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    MPLS_L2VPN_UNLOCK ();
    return SNMP_SUCCESS;
}

INT4
FsPwMplsInboundTunnelIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;

    MPLS_L2VPN_LOCK ();
    if (nmhValidateIndexInstanceFsPwMplsInboundTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        MPLS_L2VPN_UNLOCK ();
        return SNMP_FAILURE;
    }
    i4Return =
        nmhGetFsPwMplsInboundTunnelIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                          &(pMultiData->u4_ULongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsPwMplsInboundTunnelInstanceGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;

    MPLS_L2VPN_LOCK ();
    if (nmhValidateIndexInstanceFsPwMplsInboundTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        MPLS_L2VPN_UNLOCK ();
        return SNMP_FAILURE;
    }

    i4Return =
        nmhGetFsPwMplsInboundTunnelInstance (pMultiIndex->pIndex[0].
                                             u4_ULongValue,
                                             &(pMultiData->u4_ULongValue));
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsPwMplsInboundTunnelEgressLSRGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;

    MPLS_L2VPN_LOCK ();
    if (nmhValidateIndexInstanceFsPwMplsInboundTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        MPLS_L2VPN_UNLOCK ();
        return SNMP_FAILURE;
    }

    i4Return =
        nmhGetFsPwMplsInboundTunnelEgressLSR (pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiData->pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsPwMplsInboundTunnelIngressLSRGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;

    MPLS_L2VPN_LOCK ();
    if (nmhValidateIndexInstanceFsPwMplsInboundTable
        (pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        MPLS_L2VPN_UNLOCK ();
        return SNMP_FAILURE;
    }

    i4Return =
        nmhGetFsPwMplsInboundTunnelIngressLSR (pMultiIndex->pIndex[0].
                                               u4_ULongValue,
                                               pMultiData->pOctetStrValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsPwMplsInboundTunnelIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;

    MPLS_L2VPN_LOCK ();

    i4Return =
        nmhSetFsPwMplsInboundTunnelIndex (pMultiIndex->pIndex[0].u4_ULongValue,
                                          pMultiData->u4_ULongValue);
    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsPwMplsInboundTunnelEgressLSRSet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;

    MPLS_L2VPN_LOCK ();

    i4Return =
        nmhSetFsPwMplsInboundTunnelEgressLSR (pMultiIndex->pIndex[0].
                                              u4_ULongValue,
                                              pMultiData->pOctetStrValue);

    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsPwMplsInboundTunnelIngressLSRSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;

    MPLS_L2VPN_LOCK ();

    i4Return = nmhSetFsPwMplsInboundTunnelIngressLSR (pMultiIndex->pIndex[0].
                                                      u4_ULongValue,
                                                      pMultiData->
                                                      pOctetStrValue);

    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsPwMplsInboundTunnelIndexTest (UINT4 *pu4Error,
                                tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;

    MPLS_L2VPN_LOCK ();

    i4Return = nmhTestv2FsPwMplsInboundTunnelIndex (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiData->u4_ULongValue);

    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsPwMplsInboundTunnelEgressLSRTest (UINT4 *pu4Error,
                                    tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;

    MPLS_L2VPN_LOCK ();

    i4Return = nmhTestv2FsPwMplsInboundTunnelEgressLSR (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        u4_ULongValue,
                                                        pMultiData->
                                                        pOctetStrValue);

    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsPwMplsInboundTunnelIngressLSRTest (UINT4 *pu4Error,
                                     tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;

    MPLS_L2VPN_LOCK ();

    i4Return = nmhTestv2FsPwMplsInboundTunnelIngressLSR (pu4Error,
                                                         pMultiIndex->pIndex[0].
                                                         u4_ULongValue,
                                                         pMultiData->
                                                         pOctetStrValue);

    MPLS_L2VPN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsL2VpnMaxPwVcEntriesDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsL2VpnMaxPwVcEntries
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsL2VpnMaxPwVcMplsEntriesDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsL2VpnMaxPwVcMplsEntries
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsL2VpnMaxPwVcMplsInOutEntriesDep (UINT4 *pu4Error,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsL2VpnMaxPwVcMplsInOutEntries
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsL2VpnMaxEthernetPwVcsDep (UINT4 *pu4Error,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsL2VpnMaxEthernetPwVcs
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsL2VpnMaxPwVcEnetEntriesDep (UINT4 *pu4Error,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsL2VpnMaxPwVcEnetEntries
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRouterIDGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;

    UNUSED_PARAM (pMultiIndex);

    MPLS_CMN_LOCK ();

    i4Return = nmhGetFsMplsRouterID (pMultiData->pOctetStrValue);

    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsRouterIDSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;

    UNUSED_PARAM (pMultiIndex);

    MPLS_CMN_LOCK ();

    i4Return = nmhSetFsMplsRouterID (pMultiData->pOctetStrValue);

    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsRouterIDTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    INT4                i4Return = SNMP_FAILURE;

    UNUSED_PARAM (pMultiIndex);

    MPLS_CMN_LOCK ();

    i4Return = nmhTestv2FsMplsRouterID (pu4Error, pMultiData->pOctetStrValue);

    MPLS_CMN_UNLOCK ();
    return i4Return;
}

INT4
FsMplsSimulateFailureGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsSimulateFailure (&(pMultiData->i4_SLongValue)));
}

INT4
FsMplsiTTLValGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsiTTLVal (&(pMultiData->i4_SLongValue)));
}

INT4
FsMplsOTTLValGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsOTTLVal (&(pMultiData->i4_SLongValue)));
}

INT4
FsMplsprviTTLValGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsprviTTLVal (&(pMultiData->i4_SLongValue)));
}

INT4
FsMplsprvOTTLValGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsprvOTTLVal (&(pMultiData->i4_SLongValue)));
}

INT4
FsMplsSimulateFailureSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsSimulateFailure (pMultiData->i4_SLongValue));
}

INT4
FsMplsSimulateFailureTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsSimulateFailure (pu4Error,
                                            pMultiData->i4_SLongValue));
}

INT4
FsMplsSimulateFailureDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsSimulateFailure (pu4Error,
                                           pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsLdpGrCapabilityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);

    MPLS_LDP_LOCK ();

    i4RetVal = nmhGetFsMplsLdpGrCapability (&(pMultiData->i4_SLongValue));

    MPLS_LDP_UNLOCK ();

    return i4RetVal;
}

INT4
FsMplsLdpGrForwardEntryHoldTimeGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);

    MPLS_LDP_LOCK ();

    i4RetVal =
        nmhGetFsMplsLdpGrForwardEntryHoldTime (&(pMultiData->i4_SLongValue));

    MPLS_LDP_UNLOCK ();

    return i4RetVal;
}

INT4
FsMplsLdpGrMaxRecoveryTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);

    MPLS_LDP_LOCK ();

    i4RetVal = nmhGetFsMplsLdpGrMaxRecoveryTime (&(pMultiData->i4_SLongValue));

    MPLS_LDP_UNLOCK ();

    return i4RetVal;
}

INT4
FsMplsLdpGrNeighborLivenessTimeGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);

    MPLS_LDP_LOCK ();

    i4RetVal =
        nmhGetFsMplsLdpGrNeighborLivenessTime (&(pMultiData->i4_SLongValue));

    MPLS_LDP_UNLOCK ();

    return i4RetVal;
}

INT4
FsMplsLdpGrProgressStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);

    MPLS_LDP_LOCK ();

    i4RetVal = nmhGetFsMplsLdpGrProgressStatus (&(pMultiData->i4_SLongValue));

    MPLS_LDP_UNLOCK ();

    return i4RetVal;
}

INT4
FsMplsLdpGrCapabilitySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);

    MPLS_LDP_LOCK ();

    i4RetVal = nmhSetFsMplsLdpGrCapability (pMultiData->i4_SLongValue);

    MPLS_LDP_UNLOCK ();

    return i4RetVal;
}

INT4
FsMplsLdpGrForwardEntryHoldTimeSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);

    MPLS_LDP_LOCK ();

    i4RetVal =
        nmhSetFsMplsLdpGrForwardEntryHoldTime (pMultiData->i4_SLongValue);

    MPLS_LDP_UNLOCK ();

    return i4RetVal;
}

INT4
FsMplsLdpGrMaxRecoveryTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);

    MPLS_LDP_LOCK ();

    i4RetVal = nmhSetFsMplsLdpGrMaxRecoveryTime (pMultiData->i4_SLongValue);

    MPLS_LDP_UNLOCK ();

    return i4RetVal;
}

INT4
FsMplsLdpGrNeighborLivenessTimeSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);

    MPLS_LDP_LOCK ();

    i4RetVal =
        nmhSetFsMplsLdpGrNeighborLivenessTime (pMultiData->i4_SLongValue);

    MPLS_LDP_UNLOCK ();

    return i4RetVal;
}

INT4
FsMplsLdpGrCapabilityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);

    MPLS_LDP_LOCK ();
    i4RetVal = nmhTestv2FsMplsLdpGrCapability (pu4Error,
                                               pMultiData->i4_SLongValue);
    MPLS_LDP_UNLOCK ();

    return i4RetVal;
}

INT4
FsMplsLdpGrForwardEntryHoldTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);

    MPLS_LDP_LOCK ();
    i4RetVal = nmhTestv2FsMplsLdpGrForwardEntryHoldTime (pu4Error,
                                                         pMultiData->
                                                         i4_SLongValue);
    MPLS_LDP_UNLOCK ();

    return i4RetVal;
}

INT4
FsMplsLdpGrMaxRecoveryTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);

    MPLS_LDP_LOCK ();

    i4RetVal = nmhTestv2FsMplsLdpGrMaxRecoveryTime (pu4Error,
                                                    pMultiData->i4_SLongValue);

    MPLS_LDP_UNLOCK ();

    return i4RetVal;
}

INT4
FsMplsLdpGrNeighborLivenessTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);

    MPLS_LDP_LOCK ();

    i4RetVal = nmhTestv2FsMplsLdpGrNeighborLivenessTime (pu4Error,
                                                         pMultiData->
                                                         i4_SLongValue);
    MPLS_LDP_UNLOCK ();

    return i4RetVal;
}

INT4
FsMplsLdpGrCapabilityDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsLdpGrCapability (pu4Error, pSnmpIndexList,
                                           pSnmpvarbinds));
}

INT4
FsMplsLdpGrForwardEntryHoldTimeDep (UINT4 *pu4Error,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsLdpGrForwardEntryHoldTime (pu4Error, pSnmpIndexList,
                                                     pSnmpvarbinds));
}

INT4
FsMplsLdpGrMaxRecoveryTimeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsLdpGrMaxRecoveryTime
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsLdpGrNeighborLivenessTimeDep (UINT4 *pu4Error,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsLdpGrNeighborLivenessTime (pu4Error,
                                                     pSnmpIndexList,
                                                     pSnmpvarbinds));
}

INT4
GetNextIndexFsMplsLdpPeerTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    INT4                i4RetVal = SNMP_FAILURE;
    MPLS_LDP_LOCK ();

    if (pFirstMultiIndex == NULL)
    {
        i4RetVal = nmhGetFirstIndexFsMplsLdpPeerTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pNextMultiIndex->pIndex[2].pOctetStrValue);
    }
    else
    {
        i4RetVal = nmhGetNextIndexFsMplsLdpPeerTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue);
    }

    MPLS_LDP_UNLOCK ();

    return i4RetVal;
}

INT4
FsMplsLdpPeerGrReconnectTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
    if (nmhValidateIndexInstanceFsMplsLdpPeerTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    MPLS_LDP_LOCK ();
    i4RetVal = nmhGetFsMplsLdpPeerGrReconnectTime
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue, &(pMultiData->i4_SLongValue));

    MPLS_LDP_UNLOCK ();

    return i4RetVal;
}

INT4
FsMplsLdpPeerGrRecoveryTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    if (nmhValidateIndexInstanceFsMplsLdpPeerTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    MPLS_LDP_LOCK ();

    i4RetVal = nmhGetFsMplsLdpPeerGrRecoveryTime
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue, &(pMultiData->i4_SLongValue));

    MPLS_LDP_UNLOCK ();

    return i4RetVal;

}

INT4
FsMplsLdpPeerGrProgressStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;

    if (nmhValidateIndexInstanceFsMplsLdpPeerTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    MPLS_LDP_LOCK ();

    i4RetVal = nmhGetFsMplsLdpPeerGrProgressStatus
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOctetStrValue, &(pMultiData->i4_SLongValue));

    MPLS_LDP_UNLOCK ();

    return i4RetVal;

}

INT4
FsMplsRsvpTeGrMaxWaitTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);

    MPLS_CMN_LOCK ();

    i4RetVal = nmhSetFsMplsRsvpTeGrMaxWaitTime (pMultiData->i4_SLongValue);

    MPLS_CMN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMplsLdpGrMaxWaitTimeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);

    MPLS_CMN_LOCK ();

    i4RetVal = nmhSetFsMplsLdpGrMaxWaitTime (pMultiData->i4_SLongValue);

    MPLS_CMN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMplsRsvpTeGrMaxWaitTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);

    MPLS_CMN_LOCK ();

    i4RetVal =
        nmhTestv2FsMplsRsvpTeGrMaxWaitTime (pu4Error,
                                            pMultiData->i4_SLongValue);

    MPLS_CMN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMplsLdpGrMaxWaitTimeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);

    MPLS_CMN_LOCK ();

    i4RetVal =
        nmhTestv2FsMplsLdpGrMaxWaitTime (pu4Error, pMultiData->i4_SLongValue);

    MPLS_CMN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMplsRsvpTeGrMaxWaitTimeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsRsvpTeGrMaxWaitTime
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsLdpGrMaxWaitTimeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsLdpGrMaxWaitTime
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsRsvpTeGrMaxWaitTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);

    MPLS_CMN_LOCK ();

    i4RetVal = nmhGetFsMplsRsvpTeGrMaxWaitTime (&(pMultiData->i4_SLongValue));

    MPLS_CMN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMplsLdpGrMaxWaitTimeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);

    MPLS_CMN_LOCK ();

    i4RetVal = nmhGetFsMplsLdpGrMaxWaitTime (&(pMultiData->i4_SLongValue));

    MPLS_CMN_UNLOCK ();

    return i4RetVal;
}

INT4
FsMplsLdpConfigurationSequenceTLVEnableGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);

    MPLS_LDP_LOCK ();

    i4RetVal =
        nmhGetFsMplsLdpConfigurationSequenceTLVEnable (&
                                                       (pMultiData->
                                                        i4_SLongValue));

    MPLS_LDP_UNLOCK ();

    return i4RetVal;
}

INT4
FsMplsLdpConfigurationSequenceTLVEnableSet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);

    MPLS_LDP_LOCK ();

    i4RetVal =
        nmhSetFsMplsLdpConfigurationSequenceTLVEnable (pMultiData->
                                                       i4_SLongValue);

    MPLS_LDP_UNLOCK ();

    return i4RetVal;
}

INT4
FsMplsLdpConfigurationSequenceTLVEnableTest (UINT4 *pu4Error,
                                             tSnmpIndex * pMultiIndex,
                                             tRetVal * pMultiData)
{
    INT4                i4RetVal = SNMP_FAILURE;
    UNUSED_PARAM (pMultiIndex);

    MPLS_LDP_LOCK ();

    i4RetVal =
        nmhTestv2FsMplsLdpConfigurationSequenceTLVEnable (pu4Error,
                                                          pMultiData->
                                                          i4_SLongValue);

    MPLS_LDP_UNLOCK ();

    return i4RetVal;
}

INT4
FsMplsLdpConfigurationSequenceTLVEnableDep (UINT4 *pu4Error,
                                            tSnmpIndexList * pSnmpIndexList,
                                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsLdpConfigurationSequenceTLVEnable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


#ifdef MPLS_L3VPN_WANTED

INT4 GetNextIndexFsMplsL3VpnVrfEgressRteTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMplsL3VpnVrfEgressRteTable(
            &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMplsL3VpnVrfEgressRteTable(
            pFirstMultiIndex->pIndex[0].u4_ULongValue,
            &(pNextMultiIndex->pIndex[0].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}
INT4 FsMplsL3VpnVrfNameGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsL3VpnVrfEgressRteTable(
        pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsMplsL3VpnVrfName(
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->pOctetStrValue));

}
INT4 FsMplsL3VpnVrfEgressRteInetCidrDestTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsL3VpnVrfEgressRteTable(
        pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsMplsL3VpnVrfEgressRteInetCidrDestType(
        pMultiIndex->pIndex[0].u4_ULongValue,
        &(pMultiData->i4_SLongValue)));

}
INT4 FsMplsL3VpnVrfEgressRteInetCidrDestGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsL3VpnVrfEgressRteTable(
        pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsMplsL3VpnVrfEgressRteInetCidrDest(
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->pOctetStrValue));

}
INT4 FsMplsL3VpnVrfEgressRteInetCidrPfxLenGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsL3VpnVrfEgressRteTable(
        pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsMplsL3VpnVrfEgressRteInetCidrPfxLen(
        pMultiIndex->pIndex[0].u4_ULongValue,
        &(pMultiData->u4_ULongValue)));

}
INT4 FsMplsL3VpnVrfEgressRteInetCidrLabelPolicyGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsL3VpnVrfEgressRteTable(
        pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsMplsL3VpnVrfEgressRteInetCidrLabelPolicy(
        pMultiIndex->pIndex[0].u4_ULongValue,
        &(pMultiData->i4_SLongValue)));

}
INT4 FsMplsL3VpnVrfEgressRteInetCidrNHopTypeGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsL3VpnVrfEgressRteTable(
        pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsMplsL3VpnVrfEgressRteInetCidrNHopType(
        pMultiIndex->pIndex[0].u4_ULongValue,
        &(pMultiData->i4_SLongValue)));

}
INT4 FsMplsL3VpnVrfEgressRteInetCidrNextHopGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsL3VpnVrfEgressRteTable(
        pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsMplsL3VpnVrfEgressRteInetCidrNextHop(
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->pOctetStrValue));

}
INT4 FsMplsL3VpnVrfEgressRteInetCidrIfIndexGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsL3VpnVrfEgressRteTable(
        pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
   return(nmhGetFsMplsL3VpnVrfEgressRteInetCidrIfIndex(
        pMultiIndex->pIndex[0].u4_ULongValue,
        &(pMultiData->i4_SLongValue)));

}
INT4 FsMplsL3VpnVrfEgressRteInetCidrMetric1Get(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsL3VpnVrfEgressRteTable(
        pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsMplsL3VpnVrfEgressRteInetCidrMetric1(
        pMultiIndex->pIndex[0].u4_ULongValue,
        &(pMultiData->i4_SLongValue)));

}
INT4 FsMplsL3VpnVrfEgressRtePathAttrLabelGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsL3VpnVrfEgressRteTable(
        pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue =pMultiIndex->pIndex[0].u4_ULongValue;

    return SNMP_SUCCESS;

}
INT4 FsMplsL3VpnVrfEgressRteInetCidrStatusGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsL3VpnVrfEgressRteTable(
        pMultiIndex->pIndex[0].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return(nmhGetFsMplsL3VpnVrfEgressRteInetCidrStatus(
        pMultiIndex->pIndex[0].u4_ULongValue,
        &(pMultiData->i4_SLongValue)));

}
INT4 FsMplsL3VpnVrfNameSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMplsL3VpnVrfName(
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->pOctetStrValue));

}

INT4 FsMplsL3VpnVrfEgressRteInetCidrDestTypeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMplsL3VpnVrfEgressRteInetCidrDestType(
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->i4_SLongValue));

}

INT4 FsMplsL3VpnVrfEgressRteInetCidrDestSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMplsL3VpnVrfEgressRteInetCidrDest(
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->pOctetStrValue));

}

INT4 FsMplsL3VpnVrfEgressRteInetCidrPfxLenSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMplsL3VpnVrfEgressRteInetCidrPfxLen(
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->u4_ULongValue));

}

INT4 FsMplsL3VpnVrfEgressRteInetCidrLabelPolicySet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMplsL3VpnVrfEgressRteInetCidrLabelPolicy(
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->i4_SLongValue));

}

INT4 FsMplsL3VpnVrfEgressRteInetCidrNHopTypeSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMplsL3VpnVrfEgressRteInetCidrNHopType(
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->i4_SLongValue));

}

INT4 FsMplsL3VpnVrfEgressRteInetCidrNextHopSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMplsL3VpnVrfEgressRteInetCidrNextHop(
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->pOctetStrValue));

}

INT4 FsMplsL3VpnVrfEgressRteInetCidrIfIndexSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMplsL3VpnVrfEgressRteInetCidrIfIndex(
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->i4_SLongValue));

}

INT4 FsMplsL3VpnVrfEgressRteInetCidrMetric1Set(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMplsL3VpnVrfEgressRteInetCidrMetric1(
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->i4_SLongValue));

}

INT4 FsMplsL3VpnVrfEgressRteInetCidrStatusSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMplsL3VpnVrfEgressRteInetCidrStatus(
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->i4_SLongValue));

}

INT4 FsMplsL3VpnVrfNameTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsMplsL3VpnVrfName(pu4Error,
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->pOctetStrValue));

}

INT4 FsMplsL3VpnVrfEgressRteInetCidrDestTypeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrDestType(pu4Error,
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->i4_SLongValue));
}

INT4 FsMplsL3VpnVrfEgressRteInetCidrDestTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrDest(pu4Error,
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->pOctetStrValue));

}

INT4 FsMplsL3VpnVrfEgressRteInetCidrPfxLenTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrPfxLen(pu4Error,
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->u4_ULongValue));

}

INT4 FsMplsL3VpnVrfEgressRteInetCidrLabelPolicyTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrLabelPolicy(pu4Error,
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->i4_SLongValue));

}

INT4 FsMplsL3VpnVrfEgressRteInetCidrNHopTypeTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrNHopType(pu4Error,
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->i4_SLongValue));

}

INT4 FsMplsL3VpnVrfEgressRteInetCidrNextHopTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrNextHop(pu4Error,
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->pOctetStrValue));

}

INT4 FsMplsL3VpnVrfEgressRteInetCidrIfIndexTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrIfIndex(pu4Error,
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->i4_SLongValue));

}

INT4 FsMplsL3VpnVrfEgressRteInetCidrMetric1Test(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrMetric1(pu4Error,
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->i4_SLongValue));

}

INT4 FsMplsL3VpnVrfEgressRteInetCidrStatusTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrStatus(pu4Error,
        pMultiIndex->pIndex[0].u4_ULongValue,
        pMultiData->i4_SLongValue));

}

INT4 FsMplsL3VpnVrfEgressRteTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
    return(nmhDepv2FsMplsL3VpnVrfEgressRteTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


#endif

VOID
RegisterFSMPLSLdpObj ()
{
    SNMPRegisterMib (&FsMplsLdpEntityTableOID, &FsMplsLdpEntityTableEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsCrlspTnlTableOID, &FsMplsCrlspTnlTableEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsLdpPeerTableOID, &FsMplsLdpPeerTableEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsLsrLabelAllocationMethodOID,
                     &FsMplsLsrLabelAllocationMethodEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsLdpLsrIdOID, &FsMplsLdpLsrIdEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsLdpForceOptionOID, &FsMplsLdpForceOptionEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsMaxCrLdpTrfcParamsOID,
                     &FsMplsMaxCrLdpTrfcParamsEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsLsrMaxLdpEntitiesOID, &FsMplsLsrMaxLdpEntitiesEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsLsrMaxIfacesOID, &FsMplsLsrMaxIfacesEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsLsrMaxLspsOID, &FsMplsLsrMaxLspsEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsLsrMaxVcMergeCountOID,
                     &FsMplsLsrMaxVcMergeCountEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsLsrMaxVpMergeCountOID,
                     &FsMplsLsrMaxVpMergeCountEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsLsrMaxCrlspTnlsOID, &FsMplsLsrMaxCrlspTnlsEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsActiveCrLspsOID, &FsMplsActiveCrLspsEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsCrlspDebugLevelOID, &FsMplsCrlspDebugLevelEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsCrlspDumpTypeOID, &FsMplsCrlspDumpTypeEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsCrlspDumpDirectionOID,
                     &FsMplsCrlspDumpDirectionEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsCrlspPersistanceOID, &FsMplsCrlspPersistanceEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsLdpGrCapabilityOID, &FsMplsLdpGrCapabilityEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsLdpGrForwardEntryHoldTimeOID,
                     &FsMplsLdpGrForwardEntryHoldTimeEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsLdpGrMaxRecoveryTimeOID,
                     &FsMplsLdpGrMaxRecoveryTimeEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsLdpGrNeighborLivenessTimeOID,
                     &FsMplsLdpGrNeighborLivenessTimeEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsLdpGrProgressStatusOID,
                     &FsMplsLdpGrProgressStatusEntry, SNMP_MSR_TGR_TRUE);

}

VOID
UnRegisterFSMPLSLdpObj ()
{
    SNMPUnRegisterMib (&FsMplsLdpEntityTableOID, &FsMplsLdpEntityTableEntry);
    SNMPUnRegisterMib (&FsMplsCrlspTnlTableOID, &FsMplsCrlspTnlTableEntry);
    SNMPUnRegisterMib (&FsMplsLdpPeerTableOID, &FsMplsLdpPeerTableEntry);
    SNMPUnRegisterMib (&FsMplsLsrLabelAllocationMethodOID,
                       &FsMplsLsrLabelAllocationMethodEntry);
    SNMPUnRegisterMib (&FsMplsLdpLsrIdOID, &FsMplsLdpLsrIdEntry);
    SNMPUnRegisterMib (&FsMplsLdpForceOptionOID, &FsMplsLdpForceOptionEntry);
    SNMPUnRegisterMib (&FsMplsMaxCrLdpTrfcParamsOID,
                       &FsMplsMaxCrLdpTrfcParamsEntry);
    SNMPUnRegisterMib (&FsMplsLsrMaxLdpEntitiesOID,
                       &FsMplsLsrMaxLdpEntitiesEntry);
    SNMPUnRegisterMib (&FsMplsLsrMaxIfacesOID, &FsMplsLsrMaxIfacesEntry);
    SNMPUnRegisterMib (&FsMplsLsrMaxLspsOID, &FsMplsLsrMaxLspsEntry);
    SNMPUnRegisterMib (&FsMplsLsrMaxVcMergeCountOID,
                       &FsMplsLsrMaxVcMergeCountEntry);
    SNMPUnRegisterMib (&FsMplsLsrMaxVpMergeCountOID,
                       &FsMplsLsrMaxVpMergeCountEntry);
    SNMPUnRegisterMib (&FsMplsLsrMaxCrlspTnlsOID, &FsMplsLsrMaxCrlspTnlsEntry);
    SNMPUnRegisterMib (&FsMplsActiveCrLspsOID, &FsMplsActiveCrLspsEntry);
    SNMPUnRegisterMib (&FsMplsCrlspDebugLevelOID, &FsMplsCrlspDebugLevelEntry);
    SNMPUnRegisterMib (&FsMplsCrlspDumpTypeOID, &FsMplsCrlspDumpTypeEntry);
    SNMPUnRegisterMib (&FsMplsCrlspDumpDirectionOID,
                       &FsMplsCrlspDumpDirectionEntry);
    SNMPUnRegisterMib (&FsMplsCrlspPersistanceOID,
                       &FsMplsCrlspPersistanceEntry);
    SNMPUnRegisterMib (&FsMplsLdpGrCapabilityOID, &FsMplsLdpGrCapabilityEntry);
    SNMPUnRegisterMib (&FsMplsLdpGrForwardEntryHoldTimeOID,
                       &FsMplsLdpGrForwardEntryHoldTimeEntry);
    SNMPUnRegisterMib (&FsMplsLdpGrMaxRecoveryTimeOID,
                       &FsMplsLdpGrMaxRecoveryTimeEntry);
    SNMPUnRegisterMib (&FsMplsLdpGrNeighborLivenessTimeOID,
                       &FsMplsLdpGrNeighborLivenessTimeEntry);
    SNMPUnRegisterMib (&FsMplsLdpGrProgressStatusOID,
                       &FsMplsLdpGrProgressStatusEntry);
}

VOID
RegisterFSMPLSRsvpteObj ()
{
    SNMPRegisterMib (&FsMplsMaxRsvpTrfcParamsOID, &FsMplsMaxRsvpTrfcParamsEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsActiveRsvpTeTnlsOID, &FsMplsActiveRsvpTeTnlsEntry,
                     SNMP_MSR_TGR_TRUE);
}

VOID
UnRegisterFSMPLSRsvpteObj ()
{
    SNMPUnRegisterMib (&FsMplsMaxRsvpTrfcParamsOID,
                       &FsMplsMaxRsvpTrfcParamsEntry);
    SNMPUnRegisterMib (&FsMplsActiveRsvpTeTnlsOID,
                       &FsMplsActiveRsvpTeTnlsEntry);
}

VOID
RegisterFSMPLSL2vpnObj ()
{
    SNMPRegisterMib (&FsMplsVplsAcMapTableOID, &FsMplsVplsAcMapTableEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsPortTableOID, &FsMplsPortTableEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsL2VpnPwTableOID, &FsMplsL2VpnPwTableEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsVplsConfigTableOID, &FsMplsVplsConfigTableEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsPwMplsInboundTableOID, &FsPwMplsInboundTableEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsL2VpnTrcFlagOID, &FsMplsL2VpnTrcFlagEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsL2VpnCleanupIntervalOID,
                     &FsMplsL2VpnCleanupIntervalEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsL2VpnAdminStatusOID, &FsMplsL2VpnAdminStatusEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsLocalCCTypesCapabilitiesOID,
                     &FsMplsLocalCCTypesCapabilitiesEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsLocalCVTypesCapabilitiesOID,
                     &FsMplsLocalCVTypesCapabilitiesEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsL2VpnMaxPwVcEntriesOID,
                     &FsMplsL2VpnMaxPwVcEntriesEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsL2VpnMaxPwVcMplsEntriesOID,
                     &FsMplsL2VpnMaxPwVcMplsEntriesEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsL2VpnMaxPwVcMplsInOutEntriesOID,
                     &FsMplsL2VpnMaxPwVcMplsInOutEntriesEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsL2VpnMaxEthernetPwVcsOID,
                     &FsMplsL2VpnMaxEthernetPwVcsEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsL2VpnMaxPwVcEnetEntriesOID,
                     &FsMplsL2VpnMaxPwVcEnetEntriesEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsL2VpnActivePwVcEntriesOID,
                     &FsMplsL2VpnActivePwVcEntriesEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsL2VpnActivePwVcMplsEntriesOID,
                     &FsMplsL2VpnActivePwVcMplsEntriesEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsL2VpnActivePwVcEnetEntriesOID,
                     &FsMplsL2VpnActivePwVcEnetEntriesEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsL2VpnNoOfPwVcEntriesCreatedOID,
                     &FsMplsL2VpnNoOfPwVcEntriesCreatedEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsL2VpnNoOfPwVcEntriesDeletedOID,
                     &FsMplsL2VpnNoOfPwVcEntriesDeletedEntry,
                     SNMP_MSR_TGR_TRUE);
}

VOID
UnRegisterFSMPLSL2vpnObj ()
{
    SNMPUnRegisterMib (&FsMplsVplsAcMapTableOID, &FsMplsVplsAcMapTableEntry);
    SNMPUnRegisterMib (&FsMplsPortTableOID, &FsMplsPortTableEntry);
    SNMPUnRegisterMib (&FsMplsL2VpnPwTableOID, &FsMplsL2VpnPwTableEntry);
    SNMPUnRegisterMib (&FsMplsVplsConfigTableOID, &FsMplsVplsConfigTableEntry);
    SNMPUnRegisterMib (&FsPwMplsInboundTableOID, &FsPwMplsInboundTableEntry);
    SNMPUnRegisterMib (&FsMplsL2VpnTrcFlagOID, &FsMplsL2VpnTrcFlagEntry);
    SNMPUnRegisterMib (&FsMplsL2VpnCleanupIntervalOID,
                       &FsMplsL2VpnCleanupIntervalEntry);
    SNMPUnRegisterMib (&FsMplsL2VpnAdminStatusOID,
                       &FsMplsL2VpnAdminStatusEntry);
    SNMPUnRegisterMib (&FsMplsLocalCCTypesCapabilitiesOID,
                       &FsMplsLocalCCTypesCapabilitiesEntry);
    SNMPUnRegisterMib (&FsMplsLocalCVTypesCapabilitiesOID,
                       &FsMplsLocalCVTypesCapabilitiesEntry);
    SNMPUnRegisterMib (&FsMplsL2VpnMaxPwVcEntriesOID,
                       &FsMplsL2VpnMaxPwVcEntriesEntry);
    SNMPUnRegisterMib (&FsMplsL2VpnMaxPwVcMplsEntriesOID,
                       &FsMplsL2VpnMaxPwVcMplsEntriesEntry);
    SNMPUnRegisterMib (&FsMplsL2VpnMaxPwVcMplsInOutEntriesOID,
                       &FsMplsL2VpnMaxPwVcMplsInOutEntriesEntry);
    SNMPUnRegisterMib (&FsMplsL2VpnMaxEthernetPwVcsOID,
                       &FsMplsL2VpnMaxEthernetPwVcsEntry);
    SNMPUnRegisterMib (&FsMplsL2VpnMaxPwVcEnetEntriesOID,
                       &FsMplsL2VpnMaxPwVcEnetEntriesEntry);
    SNMPUnRegisterMib (&FsMplsL2VpnActivePwVcEntriesOID,
                       &FsMplsL2VpnActivePwVcEntriesEntry);
    SNMPUnRegisterMib (&FsMplsL2VpnActivePwVcMplsEntriesOID,
                       &FsMplsL2VpnActivePwVcMplsEntriesEntry);
    SNMPUnRegisterMib (&FsMplsL2VpnActivePwVcEnetEntriesOID,
                       &FsMplsL2VpnActivePwVcEnetEntriesEntry);
    SNMPUnRegisterMib (&FsMplsL2VpnNoOfPwVcEntriesCreatedOID,
                       &FsMplsL2VpnNoOfPwVcEntriesCreatedEntry);
    SNMPUnRegisterMib (&FsMplsL2VpnNoOfPwVcEntriesDeletedOID,
                       &FsMplsL2VpnNoOfPwVcEntriesDeletedEntry);
}

VOID
RegisterFSMPLSRtrObj ()
{
    SNMPRegisterMib (&FsMplsTunnelRSVPResTableOID,
                     &FsMplsTunnelRSVPResTableEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsTunnelCRLDPResTableOID,
                     &FsMplsTunnelCRLDPResTableEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsDiffServElspMapTableOID,
                     &FsMplsDiffServElspMapTableEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsDiffServParamsTableOID,
                     &FsMplsDiffServParamsTableEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsDiffServTableOID, &FsMplsDiffServTableEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsDiffServElspInfoTableOID,
                     &FsMplsDiffServElspInfoTableEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsDsTeClassTypeTableOID,
                     &FsMplsDsTeClassTypeTableEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsDsTeClassTypeToTcMapTableOID,
                     &FsMplsDsTeClassTypeToTcMapTableEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsDsTeTeClassTableOID, &FsMplsDsTeTeClassTableEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsAdminStatusOID, &FsMplsAdminStatusEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsQosPolicyOID, &FsMplsQosPolicyEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsFmDebugLevelOID, &FsMplsFmDebugLevelEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsTeDebugLevelOID, &FsMplsTeDebugLevelEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsDiffServElspPreConfExpPhbMapIndexOID,
                     &FsMplsDiffServElspPreConfExpPhbMapIndexEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsRsvpTeGrMaxWaitTimeOID,
                     &FsMplsRsvpTeGrMaxWaitTimeEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsLdpGrMaxWaitTimeOID, &FsMplsLdpGrMaxWaitTimeEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsMaxIfTableEntriesOID, &FsMplsMaxIfTableEntriesEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsMaxFTNEntriesOID, &FsMplsMaxFTNEntriesEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsMaxInSegmentEntriesOID,
                     &FsMplsMaxInSegmentEntriesEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsMaxOutSegmentEntriesOID,
                     &FsMplsMaxOutSegmentEntriesEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsMaxXCEntriesOID, &FsMplsMaxXCEntriesEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsDiffServElspMapEntriesOID,
                     &FsMplsDiffServElspMapEntriesEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsDiffServParamsEntriesOID,
                     &FsMplsDiffServParamsEntriesEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsMaxHopListsOID, &FsMplsMaxHopListsEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsMaxPathOptPerListOID, &FsMplsMaxPathOptPerListEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsMaxHopsPerPathOptionOID,
                     &FsMplsMaxHopsPerPathOptionEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsMaxArHopListsOID, &FsMplsMaxArHopListsEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsMaxHopsPerArHopListOID,
                     &FsMplsMaxHopsPerArHopListEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsMaxDServElspsOID, &FsMplsMaxDServElspsEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsMaxDServLlspsOID, &FsMplsMaxDServLlspsEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsMaxTnlsOID, &FsMplsMaxTnlsEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsLsrMaxLocalPeersOID, &FsMplsLsrMaxLocalPeersEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsLsrMaxRemotePeersOID, &FsMplsLsrMaxRemotePeersEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsActiveLspsOID, &FsMplsActiveLspsEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsDiffServElspInfoListIndexNextOID,
                     &FsMplsDiffServElspInfoListIndexNextEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsTnlModelOID, &FsMplsTnlModelEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsRsrcMgmtTypeOID, &FsMplsRsrcMgmtTypeEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsTTLValOID, &FsMplsTTLValEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsDsTeStatusOID, &FsMplsDsTeStatusEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsRouterIDOID, &FsMplsRouterIDEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsHwCCTypeCapabilitiesOID,
                     &FsMplsHwCCTypeCapabilitiesEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsSimulateFailureOID, &FsMplsSimulateFailureEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsiTTLValOID, &FsMplsiTTLValEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsOTTLValOID, &FsMplsOTTLValEntry, SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsprviTTLValOID, &FsMplsprviTTLValEntry,
                     SNMP_MSR_TGR_TRUE);
    SNMPRegisterMib (&FsMplsprvOTTLValOID, &FsMplsprvOTTLValEntry,
                     SNMP_MSR_TGR_TRUE);
}

VOID
UnRegisterFSMPLSRtrObj ()
{
    SNMPUnRegisterMib (&FsMplsTunnelRSVPResTableOID,
                       &FsMplsTunnelRSVPResTableEntry);
    SNMPUnRegisterMib (&FsMplsTunnelCRLDPResTableOID,
                       &FsMplsTunnelCRLDPResTableEntry);
    SNMPUnRegisterMib (&FsMplsDiffServElspMapTableOID,
                       &FsMplsDiffServElspMapTableEntry);
    SNMPUnRegisterMib (&FsMplsDiffServParamsTableOID,
                       &FsMplsDiffServParamsTableEntry);
    SNMPUnRegisterMib (&FsMplsDiffServTableOID, &FsMplsDiffServTableEntry);
    SNMPUnRegisterMib (&FsMplsDiffServElspInfoTableOID,
                       &FsMplsDiffServElspInfoTableEntry);
    SNMPUnRegisterMib (&FsMplsDsTeClassTypeTableOID,
                       &FsMplsDsTeClassTypeTableEntry);
    SNMPUnRegisterMib (&FsMplsDsTeClassTypeToTcMapTableOID,
                       &FsMplsDsTeClassTypeToTcMapTableEntry);
    SNMPUnRegisterMib (&FsMplsDsTeTeClassTableOID,
                       &FsMplsDsTeTeClassTableEntry);
    SNMPUnRegisterMib (&FsMplsAdminStatusOID, &FsMplsAdminStatusEntry);
    SNMPUnRegisterMib (&FsMplsQosPolicyOID, &FsMplsQosPolicyEntry);
    SNMPUnRegisterMib (&FsMplsFmDebugLevelOID, &FsMplsFmDebugLevelEntry);
    SNMPUnRegisterMib (&FsMplsTeDebugLevelOID, &FsMplsTeDebugLevelEntry);
    SNMPUnRegisterMib (&FsMplsDiffServElspPreConfExpPhbMapIndexOID,
                       &FsMplsDiffServElspPreConfExpPhbMapIndexEntry);
    SNMPUnRegisterMib (&FsMplsRsvpTeGrMaxWaitTimeOID,
                       &FsMplsRsvpTeGrMaxWaitTimeEntry);
    SNMPUnRegisterMib (&FsMplsLdpGrMaxWaitTimeOID,
                       &FsMplsLdpGrMaxWaitTimeEntry);
    SNMPUnRegisterMib (&FsMplsMaxIfTableEntriesOID,
                       &FsMplsMaxIfTableEntriesEntry);
    SNMPUnRegisterMib (&FsMplsMaxFTNEntriesOID, &FsMplsMaxFTNEntriesEntry);
    SNMPUnRegisterMib (&FsMplsMaxInSegmentEntriesOID,
                       &FsMplsMaxInSegmentEntriesEntry);
    SNMPUnRegisterMib (&FsMplsMaxOutSegmentEntriesOID,
                       &FsMplsMaxOutSegmentEntriesEntry);
    SNMPUnRegisterMib (&FsMplsMaxXCEntriesOID, &FsMplsMaxXCEntriesEntry);
    SNMPUnRegisterMib (&FsMplsDiffServElspMapEntriesOID,
                       &FsMplsDiffServElspMapEntriesEntry);
    SNMPUnRegisterMib (&FsMplsDiffServParamsEntriesOID,
                       &FsMplsDiffServParamsEntriesEntry);
    SNMPUnRegisterMib (&FsMplsMaxHopListsOID, &FsMplsMaxHopListsEntry);
    SNMPUnRegisterMib (&FsMplsMaxPathOptPerListOID,
                       &FsMplsMaxPathOptPerListEntry);
    SNMPUnRegisterMib (&FsMplsMaxHopsPerPathOptionOID,
                       &FsMplsMaxHopsPerPathOptionEntry);
    SNMPUnRegisterMib (&FsMplsMaxArHopListsOID, &FsMplsMaxArHopListsEntry);
    SNMPUnRegisterMib (&FsMplsMaxHopsPerArHopListOID,
                       &FsMplsMaxHopsPerArHopListEntry);
    SNMPUnRegisterMib (&FsMplsMaxDServElspsOID, &FsMplsMaxDServElspsEntry);
    SNMPUnRegisterMib (&FsMplsMaxDServLlspsOID, &FsMplsMaxDServLlspsEntry);
    SNMPUnRegisterMib (&FsMplsMaxTnlsOID, &FsMplsMaxTnlsEntry);
    SNMPUnRegisterMib (&FsMplsLsrMaxLocalPeersOID,
                       &FsMplsLsrMaxLocalPeersEntry);
    SNMPUnRegisterMib (&FsMplsLsrMaxRemotePeersOID,
                       &FsMplsLsrMaxRemotePeersEntry);
    SNMPUnRegisterMib (&FsMplsActiveLspsOID, &FsMplsActiveLspsEntry);
    SNMPUnRegisterMib (&FsMplsDiffServElspInfoListIndexNextOID,
                       &FsMplsDiffServElspInfoListIndexNextEntry);
    SNMPUnRegisterMib (&FsMplsTnlModelOID, &FsMplsTnlModelEntry);
    SNMPUnRegisterMib (&FsMplsRsrcMgmtTypeOID, &FsMplsRsrcMgmtTypeEntry);
    SNMPUnRegisterMib (&FsMplsTTLValOID, &FsMplsTTLValEntry);
    SNMPUnRegisterMib (&FsMplsDsTeStatusOID, &FsMplsDsTeStatusEntry);
    SNMPUnRegisterMib (&FsMplsRouterIDOID, &FsMplsRouterIDEntry);
    SNMPUnRegisterMib (&FsMplsHwCCTypeCapabilitiesOID,
                       &FsMplsHwCCTypeCapabilitiesEntry);
    SNMPUnRegisterMib (&FsMplsSimulateFailureOID, &FsMplsSimulateFailureEntry);
    SNMPUnRegisterMib (&FsMplsiTTLValOID, &FsMplsiTTLValEntry);
    SNMPUnRegisterMib (&FsMplsOTTLValOID, &FsMplsOTTLValEntry);
    SNMPUnRegisterMib (&FsMplsprviTTLValOID, &FsMplsprviTTLValEntry);
    SNMPUnRegisterMib (&FsMplsprvOTTLValOID, &FsMplsprvOTTLValEntry);
}

VOID
RegisterFSMPLSScalarObj ()
{
    SNMPRegisterMib (&FsMplsLdpConfigurationSequenceTLVEnableOID,
                     &FsMplsLdpConfigurationSequenceTLVEntry,
                     SNMP_MSR_TGR_TRUE);
}

VOID
UnRegisterFSMPLSScalarObj ()
{
    SNMPUnRegisterMib (&FsMplsLdpConfigurationSequenceTLVEnableOID,
                       &FsMplsLdpConfigurationSequenceTLVEntry);
}

VOID
DeleteSysEntry ()
{
    SNMPDelSysorEntry (&fsmplsOID, (const UINT1 *) "fsmpls");
}

VOID
AddSysEntry ()
{
    SNMPAddSysorEntry (&fsmplsOID, (const UINT1 *) "fsmpls");
}

#ifdef MPLS_L3VPN_WANTED
VOID
RegisterFSMPLSL3vpnObj ()
{
        SNMPRegisterMib (&FsMplsL3VpnVrfEgressRteTableOID,
                        &FsMplsL3VpnVrfEgressRteTableEntry,
                        SNMP_MSR_TGR_TRUE);
}

VOID
UnRegisterFSMPLSL3vpnObj ()
{
        SNMPUnRegisterMib (&FsMplsL3VpnVrfEgressRteTableOID,
                        &FsMplsL3VpnVrfEgressRteTableEntry);

}

#endif

