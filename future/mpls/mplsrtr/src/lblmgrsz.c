/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: lblmgrsz.c,v 1.3 2013/11/29 11:04:14 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _LBLMGRSZ_C
#include "mplsincs.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
LblmgrSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < LBLMGR_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsLBLMGRSizingParams[i4SizingId].u4StructSize,
                              FsLBLMGRSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(LBLMGRMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            LblmgrSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
LblmgrSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsLBLMGRSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, LBLMGRMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
LblmgrSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < LBLMGR_MAX_SIZING_ID; i4SizingId++)
    {
        if (LBLMGRMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (LBLMGRMemPoolIds[i4SizingId]);
            LBLMGRMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
