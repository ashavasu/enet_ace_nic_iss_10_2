/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 *$Id: mplsnpwr.c,v 1.8 2015/11/12 12:25:27 siva Exp $
 * File Name  : mplsnpwr.c
 *
 * Description: All prototypes for Network Processor API functions
 *              done here
 *******************************************************************/

#ifdef MPLS_WANTED
#ifndef _MPLSNP_C_
#include "mplsincs.h"
#include "nputil.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsNpWrHwProgram                                         
 *                                                                          
 *    Description         : This function takes care of calling appropriate 
 *                          Np call using the tMplsNpModInfo
 *                                                                          
 *    Input(s)            : FsHwNpParam of type tfsHwNp                     
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

PUBLIC UINT1
MplsNpWrHwProgram (tFsHwNp * pFsHwNp)
{
    INT4                i4RetVal = FNP_SUCCESS;
    UINT4               u4Opcode = 0;
    tMplsNpModInfo     *pMplsNpModInfo = NULL;

    if (NULL == pFsHwNp)
    {
        return FNP_FAILURE;
    }

    u4Opcode = pFsHwNp->u4Opcode;
    pMplsNpModInfo = &(pFsHwNp->MplsNpModInfo);

    if (NULL == pMplsNpModInfo)
    {
        return FNP_FAILURE;
    }

    switch (u4Opcode)
    {
        case FS_MPLS_HW_ENABLE_MPLS:
        {
            i4RetVal = FsMplsHwEnableMpls ();
            break;
        }
        case FS_MPLS_HW_DISABLE_MPLS:
        {
            i4RetVal = FsMplsHwDisableMpls ();
            break;
        }
        case FS_MPLS_HW_GET_PW_CTRL_CHNL_CAPABILITIES:
        {
            tMplsNpWrFsMplsHwGetPwCtrlChnlCapabilities *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsHwGetPwCtrlChnlCapabilities;
            if (NULL == pEntry)
            {
                i4RetVal= FNP_FAILURE;
                break;
            }
            i4RetVal=
                FsMplsHwGetPwCtrlChnlCapabilities (pEntry->pu1PwCCTypeCapabs);
            break;
        }
        case FS_MPLS_HW_CREATE_PW_VC:
        {
            tMplsNpWrFsMplsHwCreatePwVc *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsHwCreatePwVc;
            if (NULL == pEntry)
            {
                i4RetVal= FNP_FAILURE;
                break;
            }
            i4RetVal=
                FsMplsHwCreatePwVc (pEntry->u4VpnId, pEntry->pMplsHwVcInfo,
                                    pEntry->u4Action);
            break;
        }
        case FS_MPLS_HW_TRAVERSE_PW_VC:
        {
            tMplsNpWrFsMplsHwTraversePwVc *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsHwTraversePwVc;
            if (NULL == pEntry)
            {
                i4RetVal= FNP_FAILURE;
                break;
            }
            i4RetVal=
                FsMplsHwTraversePwVc (pEntry->pMplsHwVcInfo,
                                      pEntry->pNextMplsHwVcInfo);
            break;
        }
        case FS_MPLS_HW_DELETE_PW_VC:
        {
            tMplsNpWrFsMplsHwDeletePwVc *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsHwDeletePwVc;
            if (NULL == pEntry)
            {
                i4RetVal= FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsHwDeletePwVc (pEntry->u4VpnId, pEntry->pMplsHwDelVcInfo,
                                    pEntry->u4Action);
            break;
        }
		case FS_MPLS_HW_GET_PW_VC:
		{
			tMplsNpWrFsMplsHwTraversePwVc *pEntry = NULL;
			pEntry = &pMplsNpModInfo->MplsNpFsMplsHwTraversePwVc;
			if (NULL == pEntry)
			{
				i4RetVal = FNP_FAILURE;
				break;
			}
			i4RetVal =
				FsMplsHwGetPwVc (pEntry->pMplsHwVcInfo, 
											pEntry->pNextMplsHwVcInfo);
			break;
		}
        case FS_MPLS_HW_CREATE_I_L_M:
        {
            tMplsNpWrFsMplsHwCreateILM *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsHwCreateILM;
            if (NULL == pEntry)
            {
                i4RetVal= FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsHwCreateILM (pEntry->pMplsHwIlmInfo, pEntry->u4Action);
            break;
        }
        case FS_MPLS_HW_TRAVERSE_I_L_M:
        {
            tMplsNpWrFsMplsHwTraverseILM *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsHwTraverseILM;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsHwTraverseILM (pEntry->pMplsHwIlmInfo,
                                     pEntry->pNextMplsHwIlmInfo);
            break;
        }
        case FS_MPLS_HW_DELETE_I_L_M:
        {
            tMplsNpWrFsMplsHwDeleteILM *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsHwDeleteILM;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsHwDeleteILM (pEntry->pMplsHwDelIlmParams,
                                   pEntry->u4Action);
            break;
        }
        case FS_MPLS_HW_GET_I_L_M:
        {
            tMplsNpWrFsMplsHwTraverseILM *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsHwTraverseILM;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsHwGetILM (pEntry->pMplsHwIlmInfo,
                                   pEntry->pNextMplsHwIlmInfo);
            break;
        }
		case FS_MPLS_HW_CREATE_L3_F_T_N:
        {
            tMplsNpWrFsMplsHwCreateL3FTN *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsHwCreateL3FTN;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsHwCreateL3FTN (pEntry->u4VpnId, pEntry->pMplsHwL3FTNInfo);
            break;
        }
        case FS_MPLS_HW_TRAVERSE_L3_F_T_N:
        {
            tMplsNpWrFsMplsHwTraverseL3FTN *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsHwTraverseL3FTN;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsHwTraverseL3FTN (pEntry->pMplsHwL3FTNInfo,
                                       pEntry->pNextMplsHwL3FTNInfo);
            break;
        }
        case FS_MPLS_HW_GET_L3_F_T_N:
        {
            tMplsNpWrFsMplsHwGetL3FTN *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsHwGetL3FTN;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsHwGetL3FTN (pEntry->u4VpnId, pEntry->u4IfIndex,
                                       pEntry->pMplsHwL3FTNInfo);
            break;
        }
#ifdef NP_BACKWD_COMPATIBILITY
        case FS_MPLS_HW_DELETE_L3_F_T_N:
        {
            tMplsNpWrFsMplsHwDeleteL3FTN *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsHwDeleteL3FTN;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal = FsMplsHwDeleteL3FTN (pEntry->u4L3EgrIntf);
            break;
        }
#else
        case FS_MPLS_HW_DELETE_L3_F_T_N:
        {
            tMplsNpWrFsMplsHwDeleteL3FTN *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsHwDeleteL3FTN;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsHwDeleteL3FTN (pEntry->u4VpnId, pEntry->pMplsHwL3FTNInfo);
            break;
        }
#endif
        case FS_MPLS_WP_HW_DELETE_L3_F_T_N:
        {
            tMplsNpWrFsMplsWpHwDeleteL3FTN *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsWpHwDeleteL3FTN;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsWpHwDeleteL3FTN (pEntry->u4VpnId,
                                       pEntry->pMplsHwL3FTNInfo);
            break;
        }
        case FS_MPLS_HW_ENABLE_MPLS_IF:
        {
            tMplsNpWrFsMplsHwEnableMplsIf *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsHwEnableMplsIf;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsHwEnableMplsIf (pEntry->u4Intf,
                                      pEntry->pMplsIntfParamsSet);
            break;
        }
        case FS_MPLS_HW_DISABLE_MPLS_IF:
        {
            tMplsNpWrFsMplsHwDisableMplsIf *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsHwDisableMplsIf;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal= FsMplsHwDisableMplsIf (pEntry->u4Intf);
            break;
        }
        case FS_MPLS_HW_CREATE_VPLS_VPN:
        {
            tMplsNpWrFsMplsHwCreateVplsVpn *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsHwCreateVplsVpn;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsHwCreateVplsVpn (pEntry->u4VplsInstance, pEntry->u4VpnId,
                                       pEntry->pMplsHwVplsVpnInfo);
            break;
        }
        case FS_MPLS_HW_DELETE_VPLS_VPN:
        {
            tMplsNpWrFsMplsHwDeleteVplsVpn *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsHwDeleteVplsVpn;
            if (NULL == pEntry)
            {
                i4RetVal= FNP_FAILURE;
                break;
            }
            i4RetVal=
                FsMplsHwDeleteVplsVpn (pEntry->u4VplsInstance, pEntry->u4VpnId,
                                       pEntry->pMplsHwVplsVpnInfo);
            break;
        }
        case FS_MPLS_HW_VPLS_ADD_PW_VC:
        {
            tMplsNpWrFsMplsHwVplsAddPwVc *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsHwVplsAddPwVc;
            if (NULL == pEntry)
            {
                i4RetVal= FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsHwVplsAddPwVc (pEntry->u4VplsInstance, pEntry->u4VpnId,
                                     pEntry->pMplsHwVplsVcInfo,
                                     pEntry->u4Action);
            break;
        }
        case FS_MPLS_HW_VPLS_DELETE_PW_VC:
        {
            tMplsNpWrFsMplsHwVplsDeletePwVc *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsHwVplsDeletePwVc;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsHwVplsDeletePwVc (pEntry->u4VplsInstance, pEntry->u4VpnId,
                                        pEntry->pMplsHwVplsVcInfo,
                                        pEntry->u4Action);
            break;
        }
        case FS_MPLS_HW_VPLS_FLUSH_MAC:
        {
            tMplsNpWrFsMplsHwVplsFlushMac *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsHwVplsFlushMac;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsHwVplsFlushMac (pEntry->u4VplsInstance, pEntry->u4VpnId,
                                      pEntry->pMplsHwVplsVpnInfo);
            break;
        }
#ifdef HVPLS_WANTED
         case FS_MPLS_REGISTER_FWD_ALARM:
        {
            tMplsNpWrFsMplsRegisterFwdAlarm *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsRegisterFwdAlarm;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsRegisterFwdAlarm (pEntry->u4VpnId, pEntry->pMplsHwVplsVpnInfo);
            break;
        }
         case FS_MPLS_DE_REGISTER_FWD_ALARM:
        {
            tMplsNpWrFsMplsDeRegisterFwdAlarm *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsDeRegisterFwdAlarm;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsDeRegisterFwdAlarm (pEntry->u4VpnId);
            break;
        }

#endif
        case NP_MPLS_CREATE_MPLS_INTERFACE:
        {
            tMplsNpWrNpMplsCreateMplsInterface *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpNpMplsCreateMplsInterface;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                (INT4)NpMplsCreateMplsInterfaceWr (pEntry->pMplsHwMplsIntInfo);
            break;
        }
        case FS_MPLS_HW_ADD_MPLS_ROUTE:
        {
            tMplsNpWrFsMplsHwAddMplsRoute *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsHwAddMplsRoute;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                (INT4)FsMplsHwAddMplsRoute (pEntry->u4VrId, pEntry->u4IpDestAddr,
                                      pEntry->u4IpSubNetMask,
                                      pEntry->routeEntry, pEntry->pbu1TblFull);
            break;
        }
       #if defined (IP6_WANTED) && defined (MPLS_IPV6_WANTED)
       case FS_MPLS_HW_ADD_MPLS_IPV6_ROUTE:
         {
             tMplsNpWrFsMplsHwAddMplsIpv6Route *pEntry = NULL;
             pEntry = &pMplsNpModInfo->MplsNpFsMplsHwAddMplsIpv6Route;
             if (NULL == pEntry)
             {
                 i4RetVal = FNP_FAILURE;
                 break;
             }
             i4RetVal =
                 (INT4)FsMplsHwAddMplsIpv6Route (pEntry->u4VrId, pEntry->pu1Ip6Prefix,
                                           pEntry->u1PrefixLen,
                                           pEntry->pu1NextHop, pEntry->u4NHType,
                                           pEntry->pIntInfo);
             break;
         }
        case FS_MPLS_HW_DELETE_MPLS_IPV6_ROUTE:
         {
             tMplsNpWrFsMplsHwDeleteMplsIpv6Route *pEntry = NULL;
             pEntry = &pMplsNpModInfo->MplsNpFsMplsHwDeleteMplsIpv6Route;
             if (NULL == pEntry)
              {
                 i4RetVal = FNP_FAILURE;
                 break;
             }
             i4RetVal =
                 (INT4)FsMplsHwDeleteMplsIpv6Route (pEntry->u4VrId,
                                              pEntry->pu1Ip6Prefix,
                                              pEntry->u1PrefixLen,
                                              pEntry->pu1NextHop,
                                              pEntry->u4IfIndex,
                                              pEntry->pRouteInfo);
             break;
         }
        #endif
        case FS_MPLS_HW_GET_MPLS_STATS:
        {
            tMplsNpWrFsMplsHwGetMplsStats *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsHwGetMplsStats;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsHwGetMplsStats (pEntry->pInputParams, pEntry->StatsType,
                                      pEntry->pStatsInfo);
            break;
        }
        case FS_MPLS_HW_ADD_VPN_AC:
        {
            tMplsNpWrFsMplsHwAddVpnAc *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsHwAddVpnAc;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsHwAddVpnAc (pEntry->u4VplsInstance, pEntry->u4VpnId,
                                  pEntry->pMplsHwVplsVcInfo, pEntry->u4Action);
            break;
        }
		case FS_MPLS_HW_GET_VPN_AC:
        {
            tMplsNpWrFsMplsHwGetVpnAc *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsHwGetVpnAc;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsHwGetVpnAc (pEntry->pMplsHwVplsVcInfo);
            break;
        }

        case FS_MPLS_HW_DELETE_VPN_AC:
        {
            tMplsNpWrFsMplsHwDeleteVpnAc *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsHwDeleteVpnAc;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsHwDeleteVpnAc (pEntry->u4VplsInstance, pEntry->u4VpnId,
                                     pEntry->pMplsHwVplsVcInfo,
                                     pEntry->u4Action);
            break;
        }
        case FS_MPLS_HW_P2MP_ADD_I_L_M:
        {
            tMplsNpWrFsMplsHwP2mpAddILM *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsHwP2mpAddILM;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsHwP2mpAddILM (pEntry->u4P2mpId,
                                    pEntry->pMplsHwP2mpIlmInfo);
            break;
        }
        case FS_MPLS_HW_P2MP_REMOVE_I_L_M:
        {
            tMplsNpWrFsMplsHwP2mpRemoveILM *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsHwP2mpRemoveILM;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsHwP2mpRemoveILM (pEntry->u4P2mpId,
                                       pEntry->pMplsHwP2mpIlmInfo);
            break;
        }
        case FS_MPLS_HW_P2MP_TRAVERSE_I_L_M:
        {
            tMplsNpWrFsMplsHwP2mpTraverseILM *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsHwP2mpTraverseILM;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsHwP2mpTraverseILM (pEntry->u4P2mpId,
                                         pEntry->pMplsHwP2mpIlmInfo,
                                         pEntry->pNextMplsHwP2mpIlmInfo);
            break;
        }
        case FS_MPLS_HW_P2MP_ADD_BUD:
        {
            tMplsNpWrFsMplsHwP2mpAddBud *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsHwP2mpAddBud;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal = FsMplsHwP2mpAddBud (pEntry->u4P2mpId, pEntry->u4Action);
            break;
        }
        case FS_MPLS_HW_P2MP_REMOVE_BUD:
        {
            tMplsNpWrFsMplsHwP2mpRemoveBud *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsHwP2mpRemoveBud;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsHwP2mpRemoveBud (pEntry->u4P2mpId, pEntry->u4Action);
            break;
        }
        case FS_MPLS_HW_MODIFY_PW_VC:
        {
            tMplsNpWrFsMplsHwModifyPwVc *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsHwModifyPwVc;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsHwModifyPwVc (pEntry->u4VpnId, pEntry->u4OperActVpnId,
                                    pEntry->pMplsHwVcInfo);
            break;
        }
#ifdef MPLS_L3VPN_WANTED
        case FS_MPLS_HW_L3VPN_INGRESS_MAP:
        {
            tMplsNpWrFsMplsHwL3vpnIngressMap *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsHwL3vpnIngressMap;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal = FsMplsHwL3vpnIngressMap (pEntry->pMplsHwL3vpnIgressInfo);
            break;
        }
        case FS_MPLS_HW_L3VPN_EGRESS_MAP:
        {
            tMplsNpWrFsMplsHwL3vpnEgressMap *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsHwL3vpnEgressMap;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal = FsMplsHwL3vpnEgressMap (pEntry->pMplsHwL3vpnEgressInfo);
            break;
        }
#endif /* MPLS_WANTED */
#endif
#endif
#ifdef MPLS_WANTED
#ifdef MBSM_WANTED
        case FS_MPLS_MBSM_HW_ENABLE_MPLS:
        {
            tMplsNpWrFsMplsMbsmHwEnableMpls *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwEnableMpls;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal = FsMplsMbsmHwEnableMpls (pEntry->pSlotInfo);
            break;
        }
        case FS_MPLS_MBSM_HW_DISABLE_MPLS:
        {
            tMplsNpWrFsMplsMbsmHwDisableMpls *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwDisableMpls;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal = FsMplsMbsmHwDisableMpls (pEntry->pSlotInfo);
            break;
        }
        case FS_MPLS_MBSM_HW_GET_PW_CTRL_CHNL_CAPABILITIES:
        {
            tMplsNpWrFsMplsMbsmHwGetPwCtrlChnlCapabilities *pEntry = NULL;
            pEntry =
                &pMplsNpModInfo->MplsNpFsMplsMbsmHwGetPwCtrlChnlCapabilities;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
#if 0
            u1RetVal =
                FsMplsMbsmHwGetPwCtrlChnlCapabilities (pEntry->
     pu1PwCCTypeCapabs);
#endif
            break;
        }
        case FS_MPLS_MBSM_HW_CREATE_PW_VC:
        {
            tMplsNpWrFsMplsMbsmHwCreatePwVc *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwCreatePwVc;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsMbsmHwCreatePwVc (pEntry->u4VpnId, pEntry->pMplsHwVcInfo,
                                        pEntry->u4Action, pEntry->pSlotInfo);
            break;
        }
        case FS_MPLS_MBSM_HW_DELETE_PW_VC:
        {
            tMplsNpWrFsMplsMbsmHwDeletePwVc *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwDeletePwVc;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsMbsmHwDeletePwVc (pEntry->u4VpnId,
                                        pEntry->pMplsHwDelVcInfo,
                                        pEntry->u4Action, pEntry->pSlotInfo);
            break;
        }
        case FS_MPLS_MBSM_HW_CREATE_I_L_M:
        {
            tMplsNpWrFsMplsMbsmHwCreateILM *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwCreateILM;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsMbsmHwCreateILM (pEntry->pMplsHwIlmInfo, pEntry->u4Action,
                                       pEntry->pSlotInfo);
            break;
        }
        case FS_MPLS_MBSM_HW_DELETE_I_L_M:
        {
            tMplsNpWrFsMplsMbsmHwDeleteILM *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwDeleteILM;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsMbsmHwDeleteILM (pEntry->pMplsHwDelIlmParams,
                                       pEntry->u4Action, pEntry->pSlotInfo);
            break;
        }
        case FS_MPLS_MBSM_HW_CREATE_L3_F_T_N:
        {
            tMplsNpWrFsMplsMbsmHwCreateL3FTN *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwCreateL3FTN;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsMbsmHwCreateL3FTN (pEntry->u4VpnId,
                                         pEntry->pMplsHwL3FTNInfo,
                                         pEntry->pSlotInfo);
            break;
        }
        case FS_MPLS_MBSM_HW_DELETE_L3_F_T_N:
        {
            tMplsNpWrFsMplsMbsmHwDeleteL3FTN *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwDeleteL3FTN;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsMbsmHwDeleteL3FTN (pEntry->u4L3EgrIntf,
                                         pEntry->pSlotInfo);
            break;
        }
        case FS_MPLS_MBSM_HW_CREATE_VPLS_VPN:
        {
            tMplsNpWrFsMplsMbsmHwCreateVplsVpn *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwCreateVplsVpn;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsMbsmHwCreateVplsVpn (pEntry->u4VplsInstance,
                                           pEntry->u4VpnId,
                                           pEntry->pMplsHwVplsVpnInfo,
                                           pEntry->pSlotInfo);
            break;
        }
        case FS_MPLS_MBSM_HW_DELETE_VPLS_VPN:
        {
            tMplsNpWrFsMplsMbsmHwDeleteVplsVpn *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwDeleteVplsVpn;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsMbsmHwDeleteVplsVpn (pEntry->u4VplsInstance,
                                           pEntry->u4VpnId,
                                           pEntry->pMplsHwVplsVpnInfo,
                                           pEntry->pSlotInfo);
            break;
        }
        case FS_MPLS_MBSM_HW_VPLS_ADD_PW_VC:
        {
            tMplsNpWrFsMplsMbsmHwVplsAddPwVc *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwVplsAddPwVc;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsMbsmHwVplsAddPwVc (pEntry->u4VplsInstance,
                                         pEntry->u4VpnId,
                                         pEntry->pMplsHwVplsVcInfo,
                                         pEntry->u4Action, pEntry->pSlotInfo);
            break;
        }
        case FS_MPLS_MBSM_HW_VPLS_DELETE_PW_VC:
        {
            tMplsNpWrFsMplsMbsmHwVplsDeletePwVc *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwVplsDeletePwVc;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsMbsmHwVplsDeletePwVc (pEntry->u4VplsInstance,
                                            pEntry->u4VpnId,
                                            pEntry->pMplsHwVplsVcInfo,
                                            pEntry->u4Action,
                                            pEntry->pSlotInfo);
            break;
        }
        case FS_MPLS_MBSM_HW_ADD_VPN_AC:
        {
            tMplsNpWrFsMplsMbsmHwAddVpnAc *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwAddVpnAc;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsMbsmHwAddVpnAc (pEntry->u4VplsInstance, pEntry->u4VpnId,
                                      pEntry->pMplsHwVplsVcInfo,
                                      pEntry->u4Action, pEntry->pSlotInfo);
            break;
        }
        case FS_MPLS_MBSM_HW_DELETE_VPN_AC:
        {
            tMplsNpWrFsMplsMbsmHwDeleteVpnAc *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwDeleteVpnAc;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsMbsmHwDeleteVpnAc (pEntry->u4VplsInstance,
                                         pEntry->u4VpnId,
                                         pEntry->pMplsHwVplsVcInfo,
                                         pEntry->u4Action, pEntry->pSlotInfo);
            break;
        }
        case NP_MPLS_MBSM_CREATE_MPLS_INTERFACE:
        {
            tMplsNpWrNpMplsMbsmCreateMplsInterface *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpNpMplsMbsmCreateMplsInterface;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                NpMplsMbsmCreateMplsInterfaceWr (pEntry->pMplsHwMplsIntInfo,
                                                 pEntry->pSlotInfo);
            break;
        }
        case FS_MPLS_MBSM_HW_P2MP_ADD_I_L_M:
        {
            tMplsNpWrFsMplsMbsmHwP2mpAddILM *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwP2mpAddILM;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsMbsmHwP2mpAddILM (pEntry->u4P2mpId,
                                        pEntry->pMplsHwP2mpIlmInfo,
                                        pEntry->pSlotInfo);
            break;
        }
        case FS_MPLS_MBSM_HW_P2MP_REMOVE_I_L_M:
        {
            tMplsNpWrFsMplsMbsmHwP2mpRemoveILM *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwP2mpRemoveILM;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsMbsmHwP2mpRemoveILM (pEntry->u4P2mpId,
                                           pEntry->pMplsHwP2mpIlmInfo,
                                           pEntry->pSlotInfo);
            break;
        }
        case FS_MPLS_MBSM_HW_GET_VPN_AC:
        {
            tMplsNpWrFsMplsMbsmHwGetVpnAc *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwGetVpnAc;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsMbsmHwGetVpnAc (pEntry->pMplsHwVplsVcInfo, pEntry->pSlotInfo);
            break;
        }
        case FS_MPLS_MBSM_HW_GET_PW_VC:
        {
            tMplsNpWrFsMplsMbsmHwTraversePwVc *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwTraversePwVc;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsMbsmHwGetPwVc (pEntry->pMplsHwVcInfo,
                                            pEntry->pNextMplsHwVcInfo,
                                            pEntry->pSlotInfo);
            break;
        }
        case FS_MPLS_MBSM_HW_GET_I_L_M:
        {
            tMplsNpWrFsMplsMbsmHwTraverseILM *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwTraverseILM;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsMbsmHwGetILM (pEntry->pMplsHwIlmInfo,
                                   pEntry->pNextMplsHwIlmInfo,
                                   pEntry->pSlotInfo);
            break;
        }
        case FS_MPLS_MBSM_HW_GET_L3_F_T_N:
        {
            tMplsNpWrFsMplsMbsmHwGetL3FTN *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwGetL3FTN;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsMbsmHwGetL3FTN (pEntry->u4VpnId, pEntry->u4IfIndex,
                                       pEntry->pMplsHwL3FTNInfo,
                                       pEntry->pSlotInfo);
            break;
        }

#ifdef MPLS_L3VPN_WANTED
        case FS_MPLS_MBSM_HW_L3VPN_INGRESS_MAP:
        {
            tMplsNpWrFsMplsMbsmHwL3vpnIngressMap *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwL3vpnIngressMap;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsMbsmHwL3vpnIngressMap (pEntry->pMplsHwL3vpnIgressInfo,
                                             pEntry->pSlotInfo);
            break;
        }
        case FS_MPLS_MBSM_HW_L3VPN_EGRESS_MAP:
        {
            tMplsNpWrFsMplsMbsmHwL3vpnEgressMap *pEntry = NULL;
            pEntry = &pMplsNpModInfo->MplsNpFsMplsMbsmHwL3vpnEgressMap;
            if (NULL == pEntry)
            {
                i4RetVal = FNP_FAILURE;
                break;
            }
            i4RetVal =
                FsMplsMbsmHwL3vpnEgressMap (pEntry->pMplsHwL3vpnEgressInfo,
                                            pEntry->pSlotInfo);
            break;
        }
#endif /* MPLS_L3VPN_WANTED */
#endif /* MBSM_WANTED */
#endif /* MPLS_WANTED */
        default:
            i4RetVal = FNP_FAILURE;
            break;

    }                            /* switch */

    return ((UINT1)i4RetVal);
}
