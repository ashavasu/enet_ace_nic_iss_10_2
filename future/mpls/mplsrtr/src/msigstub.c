# include  "lr.h"
# include  "fssnmp.h"
# include  "fsmplslw.h"
# include  "fsmplswr.h"
# include  "ldpext.h"
# include  "rpteext.h"
# include  "mplsextn.h"
# include  "cli.h"

INT1
nmhGetFsMplsLsrLabelAllocationMethod (INT4
                                      *pi4RetValFsMplsLsrLabelAllocationMethod)
{
    UNUSED_PARAM (pi4RetValFsMplsLsrLabelAllocationMethod);
    return SNMP_SUCCESS;
}

INT1
nmhSetFsMplsLsrLabelAllocationMethod (INT4
                                      i4SetValFsMplsLsrLabelAllocationMethod)
{
    UNUSED_PARAM (i4SetValFsMplsLsrLabelAllocationMethod);
    return SNMP_FAILURE;
}

INT1
nmhTestv2FsMplsLsrLabelAllocationMethod (UINT4 *pu4ErrorCode,
                                         INT4
                                         i4TestValFsMplsLsrLabelAllocationMethod)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsMplsLsrLabelAllocationMethod);
    return SNMP_FAILURE;
}

INT1
nmhGetFsMplsLdpLsrId (tSNMP_OCTET_STRING_TYPE * pRetValFsMplsLdpLsrId)
{
    UNUSED_PARAM (pRetValFsMplsLdpLsrId);
    return SNMP_FAILURE;
}

INT1
nmhSetFsMplsLdpLsrId (tSNMP_OCTET_STRING_TYPE * pSetValFsMplsLdpLsrId)
{
    UNUSED_PARAM (pSetValFsMplsLdpLsrId);
    return SNMP_FAILURE;
}

INT1
nmhTestv2FsMplsLdpLsrId (UINT4 *pu4ErrorCode,
                         tSNMP_OCTET_STRING_TYPE * pTestValFsMplsLdpLsrId)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pTestValFsMplsLdpLsrId);
    return SNMP_FAILURE;
}

INT1
nmhGetFsMplsLdpForceOption (INT4 *pi4RetValFsMplsLdpForceOption)
{
    UNUSED_PARAM (pi4RetValFsMplsLdpForceOption);
    return SNMP_FAILURE;
}

INT1
nmhSetFsMplsLdpForceOption (INT4 i4SetValFsMplsLdpForceOption)
{
    UNUSED_PARAM (i4SetValFsMplsLdpForceOption);
    return SNMP_FAILURE;
}

INT1
nmhTestv2FsMplsLdpForceOption (UINT4 *pu4ErrorCode,
                               INT4 i4TestValFsMplsLdpForceOption)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsMplsLdpForceOption);
    return SNMP_FAILURE;
}

INT1
nmhGetFirstIndexFsMplsLdpEntityTable (tSNMP_OCTET_STRING_TYPE *
                                      pMplsLdpEntityLdpId,
                                      UINT4 *pu4MplsLdpEntityIndex)
{
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (pu4MplsLdpEntityIndex);
    return SNMP_FAILURE;
}

INT1
nmhGetNextIndexFsMplsLdpEntityTable (tSNMP_OCTET_STRING_TYPE *
                                     pMplsLdpEntityLdpId,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextMplsLdpEntityLdpId,
                                     UINT4 u4MplsLdpEntityIndex,
                                     UINT4 *pu4NextMplsLdpEntityIndex)
{
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (pNextMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (pu4NextMplsLdpEntityIndex);
    return SNMP_FAILURE;
}

INT1
nmhValidateIndexInstanceFsMplsLdpEntityTable (tSNMP_OCTET_STRING_TYPE *
                                              pMplsLdpEntityLdpId,
                                              UINT4 u4MplsLdpEntityIndex)
{
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    return SNMP_FAILURE;
}

INT1
nmhGetFsMplsLdpEntityPHPRequestMethod (tSNMP_OCTET_STRING_TYPE *
                                       pMplsLdpEntityLdpId,
                                       UINT4 u4MplsLdpEntityIndex,
                                       INT4
                                       *pi4RetValFsMplsLdpEntityPHPRequestMethod)
{
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (pi4RetValFsMplsLdpEntityPHPRequestMethod);
    return SNMP_FAILURE;
}

INT1
nmhGetFsMplsLdpEntityTransAddrTlvEnable (tSNMP_OCTET_STRING_TYPE
                                         * pMplsLdpEntityLdpId,
                                         UINT4 u4MplsLdpEntityIndex,
                                         INT4
                                         *pi4RetValFsMplsLdpEntityTransAddrTlvEnable)
{
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (pi4RetValFsMplsLdpEntityTransAddrTlvEnable);
    return SNMP_FAILURE;
}

INT1
nmhGetFsMplsLdpEntityTransportAddress (tSNMP_OCTET_STRING_TYPE
                                       * pMplsLdpEntityLdpId,
                                       UINT4 u4MplsLdpEntityIndex,
                                       UINT4
                                       *pu4RetValFsMplsLdpEntityTransportAddress)
{
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (pu4RetValFsMplsLdpEntityTransportAddress);
    return SNMP_FAILURE;
}

INT1
nmhGetFsMplsLdpEntityLdpOverRsvpEnable (tSNMP_OCTET_STRING_TYPE
                                        * pMplsLdpEntityLdpId,
                                        UINT4 u4MplsLdpEntityIndex,
                                        INT4
                                        *pi4RetValFsMplsLdpEntityLdpOverRsvpEnable)
{
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (pi4RetValFsMplsLdpEntityLdpOverRsvpEnable);
    return SNMP_FAILURE;
}

INT1
nmhGetFsMplsLdpEntityOutTunnelIndex (tSNMP_OCTET_STRING_TYPE
                                     * pMplsLdpEntityLdpId,
                                     UINT4 u4MplsLdpEntityIndex,
                                     UINT4
                                     *pu4RetValFsMplsLdpEntityOutTunnelIndex)
{
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (pu4RetValFsMplsLdpEntityOutTunnelIndex);
    return SNMP_FAILURE;
}

INT1
nmhGetFsMplsLdpEntityOutTunnelInstance (tSNMP_OCTET_STRING_TYPE
                                        * pMplsLdpEntityLdpId,
                                        UINT4 u4MplsLdpEntityIndex,
                                        UINT4
                                        *pu4RetValFsMplsLdpEntityOutTunnelInstance)
{
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (pu4RetValFsMplsLdpEntityOutTunnelInstance);
    return SNMP_FAILURE;
}

INT1
nmhGetFsMplsLdpEntityOutTunnelIngressLSRId (tSNMP_OCTET_STRING_TYPE
                                            * pMplsLdpEntityLdpId,
                                            UINT4 u4MplsLdpEntityIndex,
                                            UINT4
                                            *pu4RetValFsMplsLdpEntityOutTunnelIngressLSRId)
{
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (pu4RetValFsMplsLdpEntityOutTunnelIngressLSRId);
    return SNMP_FAILURE;
}

INT1
nmhGetFsMplsLdpEntityOutTunnelEgressLSRId (tSNMP_OCTET_STRING_TYPE
                                           * pMplsLdpEntityLdpId,
                                           UINT4 u4MplsLdpEntityIndex,
                                           UINT4
                                           *pu4RetValFsMplsLdpEntityOutTunnelEgressLSRId)
{
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (pu4RetValFsMplsLdpEntityOutTunnelEgressLSRId);
    return SNMP_FAILURE;
}

INT1
nmhGetFsMplsLdpEntityInTunnelIndex (tSNMP_OCTET_STRING_TYPE
                                    * pMplsLdpEntityLdpId,
                                    UINT4 u4MplsLdpEntityIndex,
                                    UINT4
                                    *pu4RetValFsMplsLdpEntityInTunnelIndex)
{
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (pu4RetValFsMplsLdpEntityInTunnelIndex);
    return SNMP_FAILURE;
}

INT1
nmhGetFsMplsLdpEntityInTunnelInstance (tSNMP_OCTET_STRING_TYPE
                                       * pMplsLdpEntityLdpId,
                                       UINT4 u4MplsLdpEntityIndex,
                                       UINT4
                                       *pu4RetValFsMplsLdpEntityInTunnelInstance)
{
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (pu4RetValFsMplsLdpEntityInTunnelInstance);
    return SNMP_FAILURE;
}

INT1
nmhGetFsMplsLdpEntityInTunnelIngressLSRId (tSNMP_OCTET_STRING_TYPE
                                           * pMplsLdpEntityLdpId,
                                           UINT4 u4MplsLdpEntityIndex,
                                           UINT4
                                           *pu4RetValFsMplsLdpEntityInTunnelIngressLSRId)
{
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (pu4RetValFsMplsLdpEntityInTunnelIngressLSRId);
    return SNMP_FAILURE;
}

INT1
nmhGetFsMplsLdpEntityInTunnelEgressLSRId (tSNMP_OCTET_STRING_TYPE
                                          * pMplsLdpEntityLdpId,
                                          UINT4 u4MplsLdpEntityIndex,
                                          UINT4
                                          *pu4RetValFsMplsLdpEntityInTunnelEgressLSRId)
{
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (pu4RetValFsMplsLdpEntityInTunnelEgressLSRId);
    return SNMP_FAILURE;
}

INT1
nmhSetFsMplsLdpEntityPHPRequestMethod (tSNMP_OCTET_STRING_TYPE *
                                       pMplsLdpEntityLdpId,
                                       UINT4 u4MplsLdpEntityIndex,
                                       INT4
                                       i4SetValFsMplsLdpEntityPHPRequestMethod)
{
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (i4SetValFsMplsLdpEntityPHPRequestMethod);
    return SNMP_FAILURE;
}

INT1
nmhSetFsMplsLdpEntityTransAddrTlvEnable (tSNMP_OCTET_STRING_TYPE
                                         * pMplsLdpEntityLdpId,
                                         UINT4 u4MplsLdpEntityIndex,
                                         INT4
                                         i4SetValFsMplsLdpEntityTransAddrTlvEnable)
{
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (i4SetValFsMplsLdpEntityTransAddrTlvEnable);
    return SNMP_FAILURE;
}

INT1
nmhSetFsMplsLdpEntityTransportAddress (tSNMP_OCTET_STRING_TYPE *
                                       pMplsLdpEntityLdpId,
                                       UINT4 u4MplsLdpEntityIndex,
                                       UINT4
                                       u4SetValFsMplsLdpEntityTransportAddress)
{
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (u4SetValFsMplsLdpEntityTransportAddress);
    return SNMP_FAILURE;
}

INT1
nmhSetFsMplsLdpEntityLdpOverRsvpEnable (tSNMP_OCTET_STRING_TYPE
                                        * pMplsLdpEntityLdpId,
                                        UINT4 u4MplsLdpEntityIndex,
                                        INT4
                                        i4SetValFsMplsLdpEntityLdpOverRsvpEnable)
{
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (i4SetValFsMplsLdpEntityLdpOverRsvpEnable);
    return SNMP_FAILURE;
}

INT1
nmhSetFsMplsLdpEntityOutTunnelIndex (tSNMP_OCTET_STRING_TYPE
                                     * pMplsLdpEntityLdpId,
                                     UINT4 u4MplsLdpEntityIndex,
                                     UINT4
                                     u4SetValFsMplsLdpEntityOutTunnelIndex)
{
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (u4SetValFsMplsLdpEntityOutTunnelIndex);
    return SNMP_FAILURE;
}

INT1
nmhSetFsMplsLdpEntityOutTunnelInstance (tSNMP_OCTET_STRING_TYPE
                                        * pMplsLdpEntityLdpId,
                                        UINT4 u4MplsLdpEntityIndex,
                                        UINT4
                                        u4SetValFsMplsLdpEntityOutTunnelInstance)
{
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (u4SetValFsMplsLdpEntityOutTunnelInstance);
    return SNMP_FAILURE;
}

INT1
nmhSetFsMplsLdpEntityOutTunnelIngressLSRId (tSNMP_OCTET_STRING_TYPE
                                            * pMplsLdpEntityLdpId,
                                            UINT4 u4MplsLdpEntityIndex,
                                            UINT4
                                            u4SetValFsMplsLdpEntityOutTunnelIngressLSRId)
{
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (u4SetValFsMplsLdpEntityOutTunnelIngressLSRId);
    return SNMP_FAILURE;
}

INT1
nmhSetFsMplsLdpEntityOutTunnelEgressLSRId (tSNMP_OCTET_STRING_TYPE
                                           * pMplsLdpEntityLdpId,
                                           UINT4 u4MplsLdpEntityIndex,
                                           UINT4
                                           u4SetValFsMplsLdpEntityOutTunnelEgressLSRId)
{
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (u4SetValFsMplsLdpEntityOutTunnelEgressLSRId);
    return SNMP_FAILURE;
}

INT1
nmhSetFsMplsLdpEntityInTunnelIndex (tSNMP_OCTET_STRING_TYPE
                                    * pMplsLdpEntityLdpId,
                                    UINT4 u4MplsLdpEntityIndex,
                                    UINT4 u4SetValFsMplsLdpEntityInTunnelIndex)
{
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (u4SetValFsMplsLdpEntityInTunnelIndex);
    return SNMP_FAILURE;
}

INT1
nmhSetFsMplsLdpEntityInTunnelInstance (tSNMP_OCTET_STRING_TYPE
                                       * pMplsLdpEntityLdpId,
                                       UINT4 u4MplsLdpEntityIndex,
                                       UINT4
                                       u4SetValFsMplsLdpEntityInTunnelInstance)
{
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (u4SetValFsMplsLdpEntityInTunnelInstance);
    return SNMP_FAILURE;
}

INT1
nmhSetFsMplsLdpEntityInTunnelIngressLSRId (tSNMP_OCTET_STRING_TYPE
                                           * pMplsLdpEntityLdpId,
                                           UINT4 u4MplsLdpEntityIndex,
                                           UINT4
                                           u4SetValFsMplsLdpEntityInTunnelIngressLSRId)
{
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (u4SetValFsMplsLdpEntityInTunnelIngressLSRId);
    return SNMP_FAILURE;
}

INT1
nmhSetFsMplsLdpEntityInTunnelEgressLSRId (tSNMP_OCTET_STRING_TYPE
                                          * pMplsLdpEntityLdpId,
                                          UINT4 u4MplsLdpEntityIndex,
                                          UINT4
                                          u4SetValFsMplsLdpEntityInTunnelEgressLSRId)
{
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (u4SetValFsMplsLdpEntityInTunnelEgressLSRId);
    return SNMP_FAILURE;
}

INT1
nmhTestv2FsMplsLdpEntityPHPRequestMethod (UINT4 *pu4ErrorCode,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsLdpEntityLdpId,
                                          UINT4 u4MplsLdpEntityIndex,
                                          INT4
                                          i4TestValFsMplsLdpEntityPHPRequestMethod)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (i4TestValFsMplsLdpEntityPHPRequestMethod);
    return SNMP_FAILURE;
}

INT1
nmhTestv2FsMplsLdpEntityTransAddrTlvEnable (UINT4 *pu4ErrorCode,
                                            tSNMP_OCTET_STRING_TYPE
                                            * pMplsLdpEntityLdpId,
                                            UINT4 u4MplsLdpEntityIndex,
                                            INT4
                                            i4TestValFsMplsLdpEntityTransAddrTlvEnable)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (i4TestValFsMplsLdpEntityTransAddrTlvEnable);
    return SNMP_FAILURE;
}

INT1
nmhTestv2FsMplsLdpEntityTransportAddress (UINT4 *pu4ErrorCode,
                                          tSNMP_OCTET_STRING_TYPE
                                          * pMplsLdpEntityLdpId,
                                          UINT4 u4MplsLdpEntityIndex,
                                          UINT4
                                          u4TestValFsMplsLdpEntityTransportAddress)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (u4TestValFsMplsLdpEntityTransportAddress);
    return SNMP_FAILURE;
}

INT1
nmhTestv2FsMplsLdpEntityLdpOverRsvpEnable (UINT4 *pu4ErrorCode,
                                           tSNMP_OCTET_STRING_TYPE
                                           * pMplsLdpEntityLdpId,
                                           UINT4 u4MplsLdpEntityIndex,
                                           INT4
                                           i4TestValFsMplsLdpEntityLdpOverRsvpEnable)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (i4TestValFsMplsLdpEntityLdpOverRsvpEnable);
    return SNMP_FAILURE;
}

INT1
nmhTestv2FsMplsLdpEntityOutTunnelIndex (UINT4 *pu4ErrorCode,
                                        tSNMP_OCTET_STRING_TYPE
                                        * pMplsLdpEntityLdpId,
                                        UINT4 u4MplsLdpEntityIndex,
                                        UINT4
                                        u4TestValFsMplsLdpEntityOutTunnelIndex)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (u4TestValFsMplsLdpEntityOutTunnelIndex);
    return SNMP_FAILURE;
}

INT1
nmhTestv2FsMplsLdpEntityOutTunnelInstance (UINT4 *pu4ErrorCode,
                                           tSNMP_OCTET_STRING_TYPE
                                           * pMplsLdpEntityLdpId,
                                           UINT4 u4MplsLdpEntityIndex,
                                           UINT4
                                           u4TestValFsMplsLdpEntityOutTunnelInstance)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (u4TestValFsMplsLdpEntityOutTunnelInstance);
    return SNMP_FAILURE;
}

INT1
nmhTestv2FsMplsLdpEntityOutTunnelIngressLSRId (UINT4 *pu4ErrorCode,
                                               tSNMP_OCTET_STRING_TYPE
                                               * pMplsLdpEntityLdpId,
                                               UINT4 u4MplsLdpEntityIndex,
                                               UINT4
                                               u4TestValFsMplsLdpEntityOutTunnelIngressLSRId)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (u4TestValFsMplsLdpEntityOutTunnelIngressLSRId);
    return SNMP_FAILURE;
}

INT1
nmhTestv2FsMplsLdpEntityOutTunnelEgressLSRId (UINT4 *pu4ErrorCode,
                                              tSNMP_OCTET_STRING_TYPE
                                              * pMplsLdpEntityLdpId,
                                              UINT4 u4MplsLdpEntityIndex,
                                              UINT4
                                              u4TestValFsMplsLdpEntityOutTunnelEgressLSRId)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (u4TestValFsMplsLdpEntityOutTunnelEgressLSRId);
    return SNMP_FAILURE;
}

INT1
nmhTestv2FsMplsLdpEntityInTunnelIndex (UINT4 *pu4ErrorCode,
                                       tSNMP_OCTET_STRING_TYPE
                                       * pMplsLdpEntityLdpId,
                                       UINT4 u4MplsLdpEntityIndex,
                                       UINT4
                                       u4TestValFsMplsLdpEntityInTunnelIndex)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (u4TestValFsMplsLdpEntityInTunnelIndex);
    return SNMP_FAILURE;
}

INT1
nmhTestv2FsMplsLdpEntityInTunnelInstance (UINT4 *pu4ErrorCode,
                                          tSNMP_OCTET_STRING_TYPE
                                          * pMplsLdpEntityLdpId,
                                          UINT4 u4MplsLdpEntityIndex,
                                          UINT4
                                          u4TestValFsMplsLdpEntityInTunnelInstance)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (u4TestValFsMplsLdpEntityInTunnelInstance);
    return SNMP_FAILURE;
}

INT1
nmhTestv2FsMplsLdpEntityInTunnelIngressLSRId (UINT4 *pu4ErrorCode,
                                              tSNMP_OCTET_STRING_TYPE
                                              * pMplsLdpEntityLdpId,
                                              UINT4 u4MplsLdpEntityIndex,
                                              UINT4
                                              u4TestValFsMplsLdpEntityInTunnelIngressLSRId)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (u4TestValFsMplsLdpEntityInTunnelIngressLSRId);
    return SNMP_FAILURE;
}

INT1
nmhTestv2FsMplsLdpEntityInTunnelEgressLSRId (UINT4 *pu4ErrorCode,
                                             tSNMP_OCTET_STRING_TYPE
                                             * pMplsLdpEntityLdpId,
                                             UINT4 u4MplsLdpEntityIndex,
                                             UINT4
                                             u4TestValFsMplsLdpEntityInTunnelEgressLSRId)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pMplsLdpEntityLdpId);
    UNUSED_PARAM (u4MplsLdpEntityIndex);
    UNUSED_PARAM (u4TestValFsMplsLdpEntityInTunnelEgressLSRId);
    return SNMP_FAILURE;
}

INT1
nmhGetFsMplsLsrMaxLdpEntities (INT4 *pi4RetValFsMplsLsrMaxLdpEntities)
{
    UNUSED_PARAM (pi4RetValFsMplsLsrMaxLdpEntities);
    return SNMP_SUCCESS;
}

INT1
nmhGetFsMplsLsrMaxLocalPeers (INT4 *pi4RetValFsMplsLsrMaxLocalPeers)
{
    UNUSED_PARAM (pi4RetValFsMplsLsrMaxLocalPeers);
    return SNMP_SUCCESS;
}

INT1
nmhGetFsMplsLsrMaxRemotePeers (INT4 *pi4RetValFsMplsLsrMaxRemotePeers)
{
    UNUSED_PARAM (pi4RetValFsMplsLsrMaxRemotePeers);
    return SNMP_SUCCESS;
}

INT1
nmhGetFsMplsLsrMaxIfaces (INT4 *pi4RetValFsMplsLsrMaxIfaces)
{
    UNUSED_PARAM (pi4RetValFsMplsLsrMaxIfaces);
    return SNMP_SUCCESS;
}

INT1
nmhGetFsMplsLsrMaxLsps (INT4 *pi4RetValFsMplsLsrMaxLsps)
{
    UNUSED_PARAM (pi4RetValFsMplsLsrMaxLsps);
    return SNMP_SUCCESS;
}

INT1
nmhGetFsMplsLsrMaxVcMergeCount (INT4 *pi4RetValFsMplsLsrMaxVcMergeCount)
{
    UNUSED_PARAM (pi4RetValFsMplsLsrMaxVcMergeCount);
    return SNMP_SUCCESS;
}

INT1
nmhGetFsMplsLsrMaxVpMergeCount (INT4 *pi4RetValFsMplsLsrMaxVpMergeCount)
{
    UNUSED_PARAM (pi4RetValFsMplsLsrMaxVpMergeCount);
    return SNMP_SUCCESS;
}

INT1
nmhGetFsMplsLsrMaxCrlspTnls (INT4 *pi4RetValFsMplsLsrMaxCrlspTnls)
{
    UNUSED_PARAM (pi4RetValFsMplsLsrMaxCrlspTnls);
    return SNMP_SUCCESS;
}

INT1
nmhGetFsMplsCrlspDebugLevel (INT4 *pi4RetValFsMplsCrlspDebugLevel)
{
    UNUSED_PARAM (pi4RetValFsMplsCrlspDebugLevel);
    return SNMP_SUCCESS;
}

INT1
nmhTestv2FsMplsCrlspDebugLevel (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsMplsCrlspDebugLevel)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsMplsCrlspDebugLevel);
    return SNMP_SUCCESS;
}

INT1
nmhSetFsMplsCrlspDebugLevel (INT4 i4SetValFsMplsCrlspDebugLevel)
{
    UNUSED_PARAM (i4SetValFsMplsCrlspDebugLevel);
    return SNMP_SUCCESS;
}

INT1
nmhGetFsMplsCrlspDumpType (INT4 *pi4RetValFsMplsCrlspDumpType)
{
    UNUSED_PARAM (pi4RetValFsMplsCrlspDumpType);
    return SNMP_SUCCESS;
}

INT1
nmhTestv2FsMplsCrlspDumpType (UINT4 *pu4ErrorCode,
                              INT4 i4TestValFsMplsCrlspDumpType)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsMplsCrlspDumpType);
    return SNMP_SUCCESS;
}

INT1
nmhSetFsMplsCrlspDumpType (INT4 i4SetValFsMplsCrlspDumpType)
{
    UNUSED_PARAM (i4SetValFsMplsCrlspDumpType);
    return SNMP_SUCCESS;
}

INT1
nmhValidateIndexInstanceFsMplsCrlspTnlTable (UINT4 u4FsMplsCrlspTnlIndex)
{
    UNUSED_PARAM (u4FsMplsCrlspTnlIndex);
    return SNMP_FAILURE;
}

INT1
nmhGetFirstIndexFsMplsCrlspTnlTable (UINT4 *pu4FsMplsCrlspTnlIndex)
{
    UNUSED_PARAM (pu4FsMplsCrlspTnlIndex);
    return SNMP_FAILURE;
}

INT1
nmhGetNextIndexFsMplsCrlspTnlTable (UINT4 u4FsMplsCrlspTnlIndex,
                                    UINT4 *pu4NextFsMplsCrlspTnlIndex)
{
    UNUSED_PARAM (u4FsMplsCrlspTnlIndex);
    UNUSED_PARAM (pu4NextFsMplsCrlspTnlIndex);
    return SNMP_FAILURE;
}

INT1
nmhGetFsMplsCrlspTnlRowStatus (UINT4 u4FsMplsCrlspTnlIndex,
                               INT4 *pi4RetValFsMplsCrlspTnlRowStatus)
{
    UNUSED_PARAM (u4FsMplsCrlspTnlIndex);
    UNUSED_PARAM (pi4RetValFsMplsCrlspTnlRowStatus);
    return SNMP_FAILURE;
}

INT1
nmhTestv2FsMplsCrlspTnlRowStatus (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsMplsCrlspTnlIndex,
                                  INT4 i4TestValFsMplsCrlspTnlRowStatus)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4FsMplsCrlspTnlIndex);
    UNUSED_PARAM (i4TestValFsMplsCrlspTnlRowStatus);
    return SNMP_FAILURE;
}

INT1
nmhSetFsMplsCrlspTnlRowStatus (UINT4 u4FsMplsCrlspTnlIndex,
                               INT4 i4SetValFsMplsCrlspTnlRowStatus)
{
    UNUSED_PARAM (u4FsMplsCrlspTnlIndex);
    UNUSED_PARAM (i4SetValFsMplsCrlspTnlRowStatus);
    return SNMP_FAILURE;
}

INT1
nmhGetFsMplsCrlspTnlStorageType (UINT4 u4FsMplsCrlspTnlIndex,
                                 INT4 *pi4RetValFsMplsCrlspTnlStorageType)
{
    UNUSED_PARAM (u4FsMplsCrlspTnlIndex);
    UNUSED_PARAM (pi4RetValFsMplsCrlspTnlStorageType);
    return SNMP_FAILURE;
}

INT1
nmhSetFsMplsCrlspTnlStorageType (UINT4 u4FsMplsCrlspTnlIndex,
                                 INT4 i4SetValFsMplsCrlspTnlStorageType)
{
    UNUSED_PARAM (u4FsMplsCrlspTnlIndex);
    UNUSED_PARAM (i4SetValFsMplsCrlspTnlStorageType);
    return SNMP_FAILURE;
}

INT1
nmhTestv2FsMplsCrlspTnlStorageType (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsMplsCrlspTnlIndex,
                                    INT4 i4TestValFsMplsCrlspTnlStorageType)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4FsMplsCrlspTnlIndex);
    UNUSED_PARAM (i4TestValFsMplsCrlspTnlStorageType);
    return SNMP_FAILURE;
}

INT1
nmhGetFsMplsCrlspDumpDirection (INT4 *pi4RetValFsMplsCrlspDumpDirection)
{
    UNUSED_PARAM (pi4RetValFsMplsCrlspDumpDirection);
    return SNMP_SUCCESS;
}

INT1
nmhTestv2FsMplsCrlspDumpDirection (UINT4 *pu4ErrorCode,
                                   INT4 i4TestValFsMplsCrlspDumpDirection)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsMplsCrlspDumpDirection);
    return SNMP_FAILURE;
}

INT1
nmhSetFsMplsCrlspDumpDirection (INT4 i4SetValFsMplsCrlspDumpDirection)
{
    UNUSED_PARAM (i4SetValFsMplsCrlspDumpDirection);
    return SNMP_FAILURE;
}

INT1
nmhGetFsMplsCrlspPersistance (INT4 *pi4RetValFsMplsCrlspPersistance)
{
    UNUSED_PARAM (pi4RetValFsMplsCrlspPersistance);
    return SNMP_SUCCESS;
}

INT1
nmhTestv2FsMplsCrlspPersistance (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsMplsCrlspPersistance)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsMplsCrlspPersistance);
    return SNMP_FAILURE;
}

INT1
nmhSetFsMplsCrlspPersistance (INT4 i4SetValFsMplsCrlspPersistance)
{
    UNUSED_PARAM (i4SetValFsMplsCrlspPersistance);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsCrlspDumpType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsCrlspDumpType (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsCrlspTnlTable
 Input       :  The Indices
                FsMplsCrlspTnlIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsCrlspTnlTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsCrlspDumpDirection
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsCrlspDumpDirection (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsCrlspPersistance
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsCrlspPersistance (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsLdpEntityTable
 Input       :  The Indices
                MplsLdpEntityLdpId
                MplsLdpEntityIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsLdpEntityTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

INT1
nmhDepv2FsMplsLdpLsrId (UINT4 *pu4ErrorCode,
                        tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

INT1
nmhDepv2FsMplsLdpForceOption (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

UINT4
LdpRegOrDeRegNonTeLspWithL2Vpn (UINT4 u4Prefix, UINT4 u4OutIfIndex,
                                BOOL1 bIsRegFlag)
{
    UNUSED_PARAM (u4Prefix);
    UNUSED_PARAM (u4OutIfIndex);
    UNUSED_PARAM (bIsRegFlag);
    return SNMP_FAILURE;
}
