/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmplslw.c,v 1.34 2016/07/22 09:45:47 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "fsmplslw.h"
# include  "mplsincs.h"
# include  "mplsdiff.h"
# include  "tedsdefs.h"
# include  "mplsdsrm.h"
# include  "dsrmgblex.h"
#include   "rtm.h"
extern UINT1        LdpDumpAllCrLsps (INT4 *pi4RetValFsMplsDisplayCrLsps);

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsAdminStatus
 Input       :  The Indices

                The Object 
                retValFsMplsAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsAdminStatus (INT4 *pi4RetValFsMplsAdminStatus)
{
    *pi4RetValFsMplsAdminStatus = (INT4) MPLS_ADMIN_STATUS (MPLS_DEF_INCARN);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsQosPolicy
 Input       :  The Indices

                The Object 
                retValFsMplsQosPolicy
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsQosPolicy (INT4 *pi4RetValFsMplsQosPolicy)
{
    *pi4RetValFsMplsQosPolicy = gMplsIncarn[MPLS_DEF_INCARN].u1QosPolicy;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsFmDebugLevel
 Input       :  The Indices

                The Object 
                retValFsMplsFmDebugLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsFmDebugLevel (INT4 *pi4RetValFsMplsFmDebugLevel)
{
    *pi4RetValFsMplsFmDebugLevel = MPLSFM_DBG_FLAG;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsDiffServElspPreConfExpPhbMapIndex
 Input       :  The Indices

                The Object 
                retValFsMplsDiffServElspPreConfExpPhbMapIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsDiffServElspPreConfExpPhbMapIndex (INT4
                                               *pi4RetValFsMplsDiffServElspPreConfExpPhbMapIndex)
{
    if (MPLS_PRECONF_ELSP_MAP (MPLS_DEF_INCARN) == NULL)
    {
        *pi4RetValFsMplsDiffServElspPreConfExpPhbMapIndex =
            MPLS_INVALID_EXP_PHB_MAP_INDEX;
    }
    else
    {
        *pi4RetValFsMplsDiffServElspPreConfExpPhbMapIndex =
            MPLS_PRECONF_ELSP_MAP (MPLS_DEF_INCARN)->u4ElspExpPhbMapIndex;
    }

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMplsAdminStatus
 Input       :  The Indices

                The Object 
                setValFsMplsAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsAdminStatus (INT4 i4SetValFsMplsAdminStatus)
{
    if (MPLS_ADMIN_STATUS (MPLS_DEF_INCARN) == i4SetValFsMplsAdminStatus)
    {
        return SNMP_SUCCESS;
    }
    MPLS_ADMIN_STATUS (MPLS_DEF_INCARN) = i4SetValFsMplsAdminStatus;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsQosPolicy
 Input       :  The Indices

                The Object 
                setValFsMplsQosPolicy
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsQosPolicy (INT4 i4SetValFsMplsQosPolicy)
{
    gMplsIncarn[MPLS_DEF_INCARN].u1QosPolicy = (UINT1) i4SetValFsMplsQosPolicy;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsMplsFmDebugLevel
 Input       :  The Indices

                The Object 
                setValFsMplsFmDebugLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsFmDebugLevel (INT4 i4SetValFsMplsFmDebugLevel)
{
    MPLSFM_DBG_FLAG = i4SetValFsMplsFmDebugLevel;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsDiffServElspPreConfExpPhbMapIndex
 Input       :  The Indices

                The Object 
                setValFsMplsDiffServElspPreConfExpPhbMapIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsDiffServElspPreConfExpPhbMapIndex (INT4
                                               i4SetValFsMplsDiffServElspPreConfExpPhbMapIndex)
{
    if (i4SetValFsMplsDiffServElspPreConfExpPhbMapIndex ==
        MPLS_INVALID_EXP_PHB_MAP_INDEX)
    {
        if (MPLS_PRECONF_ELSP_MAP (MPLS_DEF_INCARN) != NULL)
        {
            MPLS_PRECONF_ELSP_MAP (MPLS_DEF_INCARN)->u1UsedCount--;
            MPLS_PRECONF_ELSP_MAP (MPLS_DEF_INCARN) = NULL;
        }
    }
    else
    {
        /* Validity of this Map Index, i.e. whether an Elsp Mapping with this
         * Index exists will be checked during the Test operation. So, here
         * just take the Pointer to the appropriate tElspMapRow and assign it 
         */
        if (MPLS_DIFFSERV_ELSP_MAP_ENTRY (MPLS_DEF_INCARN,
                                          i4SetValFsMplsDiffServElspPreConfExpPhbMapIndex)
            != NULL)
        {
            MPLS_PRECONF_ELSP_MAP (MPLS_DEF_INCARN) =
                MPLS_DIFFSERV_ELSP_MAP_ENTRY (MPLS_DEF_INCARN,
                                              i4SetValFsMplsDiffServElspPreConfExpPhbMapIndex);

            MPLS_PRECONF_ELSP_MAP (MPLS_DEF_INCARN)->u4ElspExpPhbMapIndex =
                i4SetValFsMplsDiffServElspPreConfExpPhbMapIndex;
            MPLS_PRECONF_ELSP_MAP (MPLS_DEF_INCARN)->u1UsedCount++;
        }
        else
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMplsAdminStatus
 Input       :  The Indices

                The Object 
                testValFsMplsAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsAdminStatus (UINT4 *pu4ErrorCode,
                            INT4 i4TestValFsMplsAdminStatus)
{
    switch (i4TestValFsMplsAdminStatus)
    {
        case MPLS_ADMIN_STATUS_UP:
            /* Allow Admin Status Up in all cases except when Down In Progress
             */
            if (MPLS_ADMIN_STATUS (MPLS_DEF_INCARN) ==
                MPLS_ADMIN_STATUS_DOWN_IN_PRGRS)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;

        case MPLS_ADMIN_STATUS_DOWN:
            /* Allow Admin Status Down in all cases except when Up In Progress
             */
            if (MPLS_ADMIN_STATUS (MPLS_DEF_INCARN) ==
                MPLS_ADMIN_STATUS_UP_IN_PRGRS)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsQosPolicy
 Input       :  The Indices

                The Object 
                testValFsMplsQosPolicy
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsQosPolicy (UINT4 *pu4ErrorCode, INT4 i4TestValFsMplsQosPolicy)
{

    switch (i4TestValFsMplsQosPolicy)
    {
        case MPLS_STD_IP:
        case MPLS_RFC1349:
        case MPLS_DIFF_SERV:
            break;

        default:
            /* Qos Policy not supported */
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    if (MPLS_ADMIN_STATUS (MPLS_DEF_INCARN) != MPLS_ADMIN_STATUS_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsFmDebugLevel
 Input       :  The Indices

                The Object 
                testValFsMplsFmDebugLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsFmDebugLevel (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsMplsFmDebugLevel)
{
    UNUSED_PARAM (i4TestValFsMplsFmDebugLevel);

    UNUSED_PARAM (pu4ErrorCode);

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsDiffServElspPreConfExpPhbMapIndex
 Input       :  The Indices

                The Object 
                testValFsMplsDiffServElspPreConfExpPhbMapIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsDiffServElspPreConfExpPhbMapIndex (UINT4 *pu4ErrorCode,
                                                  INT4
                                                  i4TestValFsMplsDiffServElspPreConfExpPhbMapIndex)
{
    tElspMapRow        *pElspMap = NULL;
    UINT1               u1Exp;

    if (MPLS_ADMIN_STATUS (MPLS_DEF_INCARN) != MPLS_ADMIN_STATUS_UP)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4TestValFsMplsDiffServElspPreConfExpPhbMapIndex ==
        MPLS_INVALID_EXP_PHB_MAP_INDEX)
    {
        return SNMP_SUCCESS;
    }

    /* PreConfigured Map Index can be configured only after the CfgVarTable
     * is configured. This would imply that the Mem Pools for Elsp Maps and
     * DiffServ Params have already been allocated and so, ElspMapIndex can
     * correspond to a valid ElspMap 
     */

    pElspMap = MPLS_DIFFSERV_ELSP_MAP_ENTRY (MPLS_DEF_INCARN,
                                             i4TestValFsMplsDiffServElspPreConfExpPhbMapIndex);
    if (pElspMap == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if (pElspMap->u1Creator != MPLS_SNMP)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    for (u1Exp = 0; u1Exp < MPLS_DIFFSERV_MAX_EXP; u1Exp++)
    {
        if ((pElspMap->aElspExpPhbMapArray[u1Exp].u1RowStatus !=
             MPLS_STATUS_ACTIVE)
            && (pElspMap->aElspExpPhbMapArray[u1Exp].u1RowStatus !=
                MPLS_STATUS_DOWN))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsAdminStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsAdminStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsQosPolicy
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsQosPolicy (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsFmDebugLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsFmDebugLevel (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsTeDebugLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsTeDebugLevel (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsLsrLabelAllocationMethod
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsLsrLabelAllocationMethod (UINT4 *pu4ErrorCode,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsDiffServElspPreConfExpPhbMapIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsDiffServElspPreConfExpPhbMapIndex (UINT4 *pu4ErrorCode,
                                                 tSnmpIndexList *
                                                 pSnmpIndexList,
                                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsMaxIfTableEntries
 Input       :  The Indices

                The Object 
                retValFsMplsMaxIfTableEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsMaxIfTableEntries (UINT4 *pu4RetValFsMplsMaxIfTableEntries)
{
    *pu4RetValFsMplsMaxIfTableEntries = MAX_MPLS_INTERFACES;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsMaxFTNEntries
 Input       :  The Indices

                The Object 
                retValFsMplsMaxFTNEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsMaxFTNEntries (UINT4 *pu4RetValFsMplsMaxFTNEntries)
{
    *pu4RetValFsMplsMaxFTNEntries = MAX_MPLSDB_FTN_ENTRY;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsMaxInSegmentEntries
 Input       :  The Indices

                The Object 
                retValFsMplsMaxInSegmentEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsMaxInSegmentEntries (UINT4 *pu4RetValFsMplsMaxInSegmentEntries)
{
    *pu4RetValFsMplsMaxInSegmentEntries = MAX_MPLSDB_LSR_INSEGMENT;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsMaxOutSegmentEntries
 Input       :  The Indices

                The Object 
                retValFsMplsMaxOutSegmentEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsMaxOutSegmentEntries (UINT4 *pu4RetValFsMplsMaxOutSegmentEntries)
{
    *pu4RetValFsMplsMaxOutSegmentEntries = MAX_MPLSDB_LSR_OUTSEGMENT;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsMaxXCEntries
 Input       :  The Indices

                The Object 
                retValFsMplsMaxXCEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsMaxXCEntries (UINT4 *pu4RetValFsMplsMaxXCEntries)
{
    *pu4RetValFsMplsMaxXCEntries = MAX_MPLSDB_LSR_XCENTRY;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsDiffServElspMapEntries
 Input       :  The Indices

                The Object 
                retValFsMplsDiffServElspMapEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsDiffServElspMapEntries (UINT4
                                    *pu4RetValFsMplsDiffServElspMapEntries)
{
    *pu4RetValFsMplsDiffServElspMapEntries =
        gMplsIncarn[MPLS_DEF_INCARN].DiffServGlobal.u4DiffServElspMapEntries;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsDiffServParamsEntries
 Input       :  The Indices

                The Object 
                retValFsMplsDiffServParamsEntries
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsDiffServParamsEntries (UINT4 *pu4RetValFsMplsDiffServParamsEntries)
{
    *pu4RetValFsMplsDiffServParamsEntries =
        gMplsIncarn[MPLS_DEF_INCARN].DiffServGlobal.u4DiffServParamsEntries;

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsActiveLsps
 Input       :  The Indices

                The Object 
                retValFsMplsActiveLsps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsActiveLsps (INT4 *pi4RetValFsMplsActiveLsps)
{
#ifdef MPLS_LDP_WANTED
    LdpDumpAllNormalLsps (pi4RetValFsMplsActiveLsps);
#else
    UNUSED_PARAM (pi4RetValFsMplsActiveLsps);
#endif

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsActiveCrLsps
 Input       :  The Indices

                The Object 
                retValFsMplsActiveCrLsps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsActiveCrLsps (INT4 *pi4RetValFsMplsActiveCrLsps)
{
#ifdef MPLS_LDP_WANTED
    LdpDumpAllCrLsps (pi4RetValFsMplsActiveCrLsps);
#else
    UNUSED_PARAM (pi4RetValFsMplsActiveCrLsps);
#endif
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMplsDiffServElspMapTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsDiffServElspMapTable
 Input       :  The Indices
                FsMplsDiffServElspMapIndex
                FsMplsDiffServElspMapExpIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMplsDiffServElspMapTable (INT4
                                                    i4FsMplsDiffServElspMapIndex,
                                                    INT4
                                                    i4FsMplsDiffServElspMapExpIndex)
{

    if (i4FsMplsDiffServElspMapIndex < MPLS_ZERO)
    {
        return SNMP_FAILURE;
    }
    if ((i4FsMplsDiffServElspMapExpIndex < MPLS_ZERO) ||
        (i4FsMplsDiffServElspMapExpIndex > MPLS_MAX_UINT1))
    {
        return SNMP_FAILURE;
    }
    if (MplsValidateElspMapIndex (MPLS_DEF_INCARN,
                                  (UINT4) i4FsMplsDiffServElspMapIndex,
                                  (UINT1) i4FsMplsDiffServElspMapExpIndex) !=
        MPLS_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsDiffServElspMapTable
 Input       :  The Indices
                FsMplsDiffServElspMapIndex
                FsMplsDiffServElspMapExpIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMplsDiffServElspMapTable (INT4 *pi4FsMplsDiffServElspMapIndex,
                                            INT4
                                            *pi4FsMplsDiffServElspMapExpIndex)
{
    UINT4               u4ElspMapId = MPLS_ZERO;
    UINT1               u1Exp = MPLS_ZERO;
    tElspMapRow        *pElspMap = NULL;

    for (u4ElspMapId = 0;
         u4ElspMapId < MPLS_DIFFSERV_ELSP_MAP_ENTRIES (MPLS_DEF_INCARN);
         u4ElspMapId++)
    {
        pElspMap = MPLS_DIFFSERV_ELSP_MAP_ENTRY (MPLS_DEF_INCARN, u4ElspMapId);
        if (pElspMap != NULL)
        {
            for (u1Exp = 0; u1Exp < MPLS_DIFFSERV_MAX_EXP; u1Exp++)
            {
                if (pElspMap->aElspExpPhbMapArray[u1Exp].
                    u1RowStatus != MPLS_STATUS_DOWN)
                {
                    *pi4FsMplsDiffServElspMapIndex = u4ElspMapId;
                    *pi4FsMplsDiffServElspMapExpIndex = u1Exp;
                    return SNMP_SUCCESS;
                }
            }
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMplsDiffServElspMapTable
 Input       :  The Indices
                FsMplsDiffServElspMapIndex
                nextFsMplsDiffServElspMapIndex
                FsMplsDiffServElspMapExpIndex
                nextFsMplsDiffServElspMapExpIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMplsDiffServElspMapTable (INT4 i4FsMplsDiffServElspMapIndex,
                                           INT4
                                           *pi4NextFsMplsDiffServElspMapIndex,
                                           INT4 i4FsMplsDiffServElspMapExpIndex,
                                           INT4
                                           *pi4NextFsMplsDiffServElspMapExpIndex)
{
    UINT4               u4ElspMapId = MPLS_ZERO;
    UINT1               u1Exp = MPLS_ZERO;
    tElspMapRow        *pElspMap = NULL;

    u4ElspMapId = i4FsMplsDiffServElspMapIndex;
    u1Exp = (UINT1) (i4FsMplsDiffServElspMapExpIndex + 1);

    for (; u4ElspMapId < MPLS_DIFFSERV_ELSP_MAP_ENTRIES (MPLS_DEF_INCARN);
         u4ElspMapId++)
    {
        pElspMap = MPLS_DIFFSERV_ELSP_MAP_ENTRY (MPLS_DEF_INCARN, u4ElspMapId);
        if (pElspMap != NULL)
        {
            for (; u1Exp < MPLS_DIFFSERV_MAX_EXP; u1Exp++)
            {
                if (pElspMap->aElspExpPhbMapArray[u1Exp].
                    u1RowStatus != MPLS_STATUS_DOWN)
                {
                    *pi4NextFsMplsDiffServElspMapIndex = u4ElspMapId;
                    *pi4NextFsMplsDiffServElspMapExpIndex = u1Exp;
                    return SNMP_SUCCESS;
                }
            }
        }
        u1Exp = 0;
    }
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsDiffServElspMapPhbDscp
 Input       :  The Indices
                FsMplsDiffServElspMapIndex
                FsMplsDiffServElspMapExpIndex

                The Object 
                retValFsMplsDiffServElspMapPhbDscp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsDiffServElspMapPhbDscp (INT4 i4FsMplsDiffServElspMapIndex,
                                    INT4 i4FsMplsDiffServElspMapExpIndex,
                                    INT4 *pi4RetValFsMplsDiffServElspMapPhbDscp)
{
    tElspMapRow        *pElspMap = NULL;

    pElspMap = MPLS_DIFFSERV_ELSP_MAP_ENTRY (MPLS_DEF_INCARN,
                                             i4FsMplsDiffServElspMapIndex);
    if (pElspMap != NULL)
    {
        if (pElspMap->aElspExpPhbMapArray[i4FsMplsDiffServElspMapExpIndex].
            u1RowStatus != MPLS_STATUS_DOWN)
        {
            *pi4RetValFsMplsDiffServElspMapPhbDscp =
                pElspMap->aElspExpPhbMapArray
                [i4FsMplsDiffServElspMapExpIndex].u1ElspPhbDscp;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsDiffServElspMapStatus
 Input       :  The Indices
                FsMplsDiffServElspMapIndex
                FsMplsDiffServElspMapExpIndex

                The Object 
                retValFsMplsDiffServElspMapStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsDiffServElspMapStatus (INT4 i4FsMplsDiffServElspMapIndex,
                                   INT4 i4FsMplsDiffServElspMapExpIndex,
                                   INT4 *pi4RetValFsMplsDiffServElspMapStatus)
{
    tElspMapRow        *pElspMap = NULL;

    pElspMap = MPLS_DIFFSERV_ELSP_MAP_ENTRY (MPLS_DEF_INCARN,
                                             i4FsMplsDiffServElspMapIndex);
    if (pElspMap != NULL)
    {
        if (pElspMap->aElspExpPhbMapArray[i4FsMplsDiffServElspMapExpIndex].
            u1RowStatus != MPLS_STATUS_DOWN)
        {
            *pi4RetValFsMplsDiffServElspMapStatus =
                pElspMap->
                aElspExpPhbMapArray[i4FsMplsDiffServElspMapExpIndex].
                u1RowStatus;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMplsDiffServElspMapPhbDscp
 Input       :  The Indices
                FsMplsDiffServElspMapIndex
                FsMplsDiffServElspMapExpIndex

                The Object 
                setValFsMplsDiffServElspMapPhbDscp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsDiffServElspMapPhbDscp (INT4 i4FsMplsDiffServElspMapIndex,
                                    INT4 i4FsMplsDiffServElspMapExpIndex,
                                    INT4 i4SetValFsMplsDiffServElspMapPhbDscp)
{
    tElspMapRow        *pElspMap = NULL;

    pElspMap = MPLS_DIFFSERV_ELSP_MAP_ENTRY (MPLS_DEF_INCARN,
                                             i4FsMplsDiffServElspMapIndex);
    if (pElspMap != NULL)
    {
        pElspMap->aElspExpPhbMapArray[i4FsMplsDiffServElspMapExpIndex].
            u1ElspPhbDscp = (UINT1) i4SetValFsMplsDiffServElspMapPhbDscp;
        pElspMap->aElspExpPhbMapArray[i4FsMplsDiffServElspMapExpIndex].
            u1IsExpValid = MPLS_TRUE;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsMplsDiffServElspMapStatus
 Input       :  The Indices
                FsMplsDiffServElspMapIndex
                FsMplsDiffServElspMapExpIndex

                The Object 
                setValFsMplsDiffServElspMapStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsDiffServElspMapStatus (INT4 i4FsMplsDiffServElspMapIndex,
                                   INT4 i4FsMplsDiffServElspMapExpIndex,
                                   INT4 i4SetValFsMplsDiffServElspMapStatus)
{
    tElspMapRow        *pElspMap = NULL;
    tElspMapRow        *pNewElspMap = NULL;
    tElspPhbMapEntry   *pElspMapEntry = NULL;
    UINT1               u1Exp;

    switch (i4SetValFsMplsDiffServElspMapStatus)
    {
        case MPLS_STATUS_ACTIVE:
            pElspMap = MPLS_DIFFSERV_ELSP_MAP_ENTRY (MPLS_DEF_INCARN,
                                                     i4FsMplsDiffServElspMapIndex);
            if (pElspMap->u4ElspExpPhbMapIndex !=
                (UINT4) i4FsMplsDiffServElspMapIndex)
            {
                return SNMP_FAILURE;
            }

            pElspMapEntry =
                &(pElspMap->
                  aElspExpPhbMapArray[i4FsMplsDiffServElspMapExpIndex]);
            pElspMapEntry->u1RowStatus = MPLS_STATUS_ACTIVE;
            break;

        case MPLS_STATUS_CREATE_AND_WAIT:
            pElspMap = MPLS_DIFFSERV_ELSP_MAP_ENTRY (MPLS_DEF_INCARN,
                                                     i4FsMplsDiffServElspMapIndex);
            if (pElspMap == NULL)
            {
                pNewElspMap = (tElspMapRow *)
                    MemAllocMemBlk (MPLS_DIFFSERV_ELSP_MAP_POOL_ID);

                if (pNewElspMap == NULL)
                {
                    return SNMP_FAILURE;
                }
                MEMSET (pNewElspMap, 0, sizeof (tElspMapRow));
                pNewElspMap->u1Creator = MPLS_SNMP;
                pNewElspMap->u1UsedCount = MPLS_ZERO;
                pNewElspMap->u4ElspExpPhbMapIndex =
                    i4FsMplsDiffServElspMapIndex;
                for (u1Exp = 0; u1Exp < MPLS_DIFFSERV_MAX_EXP; u1Exp++)
                {
                    /* By default, created Exp should be
                     * Valid and have PHB Value DF
                     */
                    if (u1Exp != i4FsMplsDiffServElspMapExpIndex)
                    {
                        pNewElspMap->aElspExpPhbMapArray[u1Exp].u1IsExpValid =
                            MPLS_FALSE;
                        pNewElspMap->aElspExpPhbMapArray[u1Exp].u1RowStatus =
                            MPLS_STATUS_DOWN;
                    }
                }

                MPLS_DIFFSERV_ELSP_MAP_ENTRY (MPLS_DEF_INCARN,
                                              i4FsMplsDiffServElspMapIndex) =
                    pElspMap = pNewElspMap;

                /* This cannot go below 0 because after it reaches zero alloc 
                 * mem would have failed
                 */
                MPLS_DIFFSERV_ELSP_TOKEN_STACK_TOP (MPLS_DEF_INCARN)--;
            }
            pElspMap->
                aElspExpPhbMapArray[i4FsMplsDiffServElspMapExpIndex].
                u1ElspPhbDscp = MPLS_DIFFSERV_DF_DSCP;
            pElspMap->
                aElspExpPhbMapArray[i4FsMplsDiffServElspMapExpIndex].
                u1IsExpValid = MPLS_TRUE;
            pElspMap->
                aElspExpPhbMapArray[i4FsMplsDiffServElspMapExpIndex].
                u1RowStatus = MPLS_STATUS_NOT_READY;

            break;

        case MPLS_STATUS_DESTROY:
            pElspMap = MPLS_DIFFSERV_ELSP_MAP_ENTRY (MPLS_DEF_INCARN,
                                                     i4FsMplsDiffServElspMapIndex);
            pElspMap->aElspExpPhbMapArray[i4FsMplsDiffServElspMapExpIndex].
                u1RowStatus = MPLS_STATUS_DOWN;

            if (MPLS_PRECONF_ELSP_MAP (MPLS_DEF_INCARN) != NULL)
            {
                if (MEMCMP (MPLS_PRECONF_ELSP_MAP (MPLS_DEF_INCARN), pElspMap,
                            sizeof (tElspMapRow)) == 0)
                {
                    MPLS_PRECONF_ELSP_MAP (MPLS_DEF_INCARN) = NULL;
                }
            }

            for (u1Exp = 0; u1Exp < MPLS_DIFFSERV_MAX_EXP; u1Exp++)
            {
                if (pElspMap->
                    aElspExpPhbMapArray[u1Exp].u1RowStatus != MPLS_STATUS_DOWN)
                {
                    return SNMP_SUCCESS;
                }
            }
            MPLS_DIFFSERV_ELSP_MAP_ENTRY (MPLS_DEF_INCARN,
                                          i4FsMplsDiffServElspMapIndex) = NULL;
            /* TODO if you do the following operation stack sequence will go
             * siganlled case also needs to be corrected
             MPLS_DIFFSERV_ELSP_TOKEN_STACK (MPLS_DEF_INCARN,
             MPLS_DIFFSERV_ELSP_TOKEN_STACK_TOP
             (MPLS_DEF_INCARN)) =
             pElspMap->u4ElspExpPhbMapIndex;
             */
            /* This cannot go above max because you cannot release more blocks
             * than allocated
             */
            MPLS_DIFFSERV_ELSP_TOKEN_STACK_TOP (MPLS_DEF_INCARN)++;
            if (MemReleaseMemBlock (MPLS_DIFFSERV_ELSP_MAP_POOL_ID,
                                    (UINT1 *) (pElspMap)) == MPLS_MEM_FAILURE)
            {
                return SNMP_FAILURE;
            }
            break;
        case MPLS_STATUS_CREATE_AND_GO:
        case MPLS_STATUS_NOT_INSERVICE:
        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMplsDiffServElspMapPhbDscp
 Input       :  The Indices
                FsMplsDiffServElspMapIndex
                FsMplsDiffServElspMapExpIndex

                The Object 
                testValFsMplsDiffServElspMapPhbDscp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsDiffServElspMapPhbDscp (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMplsDiffServElspMapIndex,
                                       INT4 i4FsMplsDiffServElspMapExpIndex,
                                       INT4
                                       i4TestValFsMplsDiffServElspMapPhbDscp)
{
    tElspMapRow        *pElspMap = NULL;

    if (MPLS_ADMIN_STATUS (MPLS_DEF_INCARN) != MPLS_ADMIN_STATUS_UP)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    pElspMap = MPLS_DIFFSERV_ELSP_MAP_ENTRY (MPLS_DEF_INCARN,
                                             i4FsMplsDiffServElspMapIndex);

    if ((i4TestValFsMplsDiffServElspMapPhbDscp < MPLS_ZERO) ||
        (i4TestValFsMplsDiffServElspMapPhbDscp > MPLS_MAX_UINT1) ||
        (MplsDiffServValidatePhbDscp
         ((UINT1) i4TestValFsMplsDiffServElspMapPhbDscp) != MPLS_SUCCESS))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((pElspMap != NULL)
        && (pElspMap->u4ElspExpPhbMapIndex ==
            (UINT4) i4FsMplsDiffServElspMapIndex)
        && (pElspMap->u1Creator == MPLS_SNMP)
        && (pElspMap->aElspExpPhbMapArray[i4FsMplsDiffServElspMapExpIndex].
            u1RowStatus != MPLS_STATUS_ACTIVE))
    {
        return SNMP_SUCCESS;
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsDiffServElspMapStatus
 Input       :  The Indices
                FsMplsDiffServElspMapIndex
                FsMplsDiffServElspMapExpIndex

                The Object 
                testValFsMplsDiffServElspMapStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsDiffServElspMapStatus (UINT4 *pu4ErrorCode,
                                      INT4 i4FsMplsDiffServElspMapIndex,
                                      INT4 i4FsMplsDiffServElspMapExpIndex,
                                      INT4 i4TestValFsMplsDiffServElspMapStatus)
{
    tElspMapRow        *pElspMap = NULL;
    tElspPhbMapEntry   *pElspEntry = NULL;
    UINT4               u4ElspMapIndex = MPLS_ZERO;

    /* Check whether pointers for MapTable were created or not */
    if ((MPLS_ADMIN_STATUS (MPLS_DEF_INCARN) != MPLS_ADMIN_STATUS_UP) ||
        (MPLS_DIFFSERV_ELSP_MAP_TABLE (MPLS_DEF_INCARN) == NULL))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* Check for validity of indices */
    if ((i4FsMplsDiffServElspMapIndex > MAX_MPLSRTR_ELSPMAP_ROW_ENTRIES) ||
        (i4FsMplsDiffServElspMapIndex < 0) ||
        (i4FsMplsDiffServElspMapExpIndex >= MPLS_DIFFSERV_MAX_EXP) ||
        (i4FsMplsDiffServElspMapExpIndex < 0))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    pElspMap = MPLS_DIFFSERV_ELSP_MAP_ENTRY (MPLS_DEF_INCARN,
                                             i4FsMplsDiffServElspMapIndex);
    if (pElspMap != NULL)
    {
        pElspEntry =
            &(pElspMap->aElspExpPhbMapArray[i4FsMplsDiffServElspMapExpIndex]);
    }
    else if (i4TestValFsMplsDiffServElspMapStatus !=
             MPLS_STATUS_CREATE_AND_WAIT)
    {
        /* If this is not a creation request and no entries are already
         * present, return Error. 
         */
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    switch (i4TestValFsMplsDiffServElspMapStatus)
    {
        case MPLS_STATUS_ACTIVE:
            if ((pElspMap->u4ElspExpPhbMapIndex ==
                 (UINT4) i4FsMplsDiffServElspMapIndex)
                && (pElspMap->u1Creator == MPLS_SNMP)
                && (pElspEntry->u1RowStatus == MPLS_STATUS_NOT_READY)
                && (pElspEntry->u1IsExpValid == MPLS_TRUE))
            {
                return SNMP_SUCCESS;
            }
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
            break;
        case MPLS_STATUS_DESTROY:
            if ((pElspMap->u4ElspExpPhbMapIndex ==
                 (UINT4) i4FsMplsDiffServElspMapIndex) &&
                (pElspMap->u1Creator == MPLS_SNMP) &&
                (pElspMap->u1UsedCount == MPLS_ZERO))
            {
                /* TODO Signalled case usage yet to be verified */
                return SNMP_SUCCESS;
            }
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
            break;

        case MPLS_STATUS_CREATE_AND_WAIT:
            if (pElspMap == NULL)
            {
                if (MplsGetDiffServElspMapIndexNext (MPLS_DEF_INCARN,
                                                     &u4ElspMapIndex) ==
                    MPLS_SUCCESS)
                {
                    /* Allow using of previously deleted indices */
                    if (u4ElspMapIndex >= (UINT4) i4FsMplsDiffServElspMapIndex)
                    {
                        return SNMP_SUCCESS;
                    }
                }
                else
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                    return SNMP_FAILURE;
                }
            }
            else
            {
                if (pElspEntry->u1RowStatus != MPLS_STATUS_DOWN)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                    return SNMP_FAILURE;
                }
            }
            break;

        case MPLS_STATUS_CREATE_AND_GO:
        case MPLS_STATUS_NOT_INSERVICE:
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsDiffServElspMapTable
 Input       :  The Indices
                FsMplsDiffServElspMapIndex
                FsMplsDiffServElspMapExpIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsDiffServElspMapTable (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMplsDiffServParamsTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsDiffServParamsTable
 Input       :  The Indices
                FsMplsDiffServParamsIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMplsDiffServParamsTable (INT4
                                                   i4FsMplsDiffServParamsIndex)
{

    if (MplsValidateDiffServParamsIndex
        (MPLS_DEF_INCARN, i4FsMplsDiffServParamsIndex) != MPLS_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsDiffServParamsTable
 Input       :  The Indices
                FsMplsDiffServParamsIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMplsDiffServParamsTable (INT4 *pi4FsMplsDiffServParamsIndex)
{
    UINT4               u4DiffParamsId = MPLS_ZERO;
    tMplsDiffServParams *pDiffServParams = NULL;

    for (u4DiffParamsId = 0;
         u4DiffParamsId < MPLS_DIFFSERV_PARAMS_ENTRIES (MPLS_DEF_INCARN);
         u4DiffParamsId++)
    {
        pDiffServParams =
            MPLS_DIFFSERV_PARAMS_ENTRY (MPLS_DEF_INCARN, u4DiffParamsId);
        if (pDiffServParams != NULL)
        {
            *pi4FsMplsDiffServParamsIndex = u4DiffParamsId;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMplsDiffServParamsTable
 Input       :  The Indices
                FsMplsDiffServParamsIndex
                nextFsMplsDiffServParamsIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMplsDiffServParamsTable (INT4 i4FsMplsDiffServParamsIndex,
                                          INT4
                                          *pi4NextFsMplsDiffServParamsIndex)
{
    UINT4               u4DiffParamsId = MPLS_ZERO;
    tMplsDiffServParams *pDiffServParams = NULL;

    u4DiffParamsId = i4FsMplsDiffServParamsIndex + 1;

    for (; u4DiffParamsId < MPLS_DIFFSERV_PARAMS_ENTRIES (MPLS_DEF_INCARN);
         u4DiffParamsId++)
    {
        pDiffServParams =
            MPLS_DIFFSERV_PARAMS_ENTRY (MPLS_DEF_INCARN, u4DiffParamsId);
        if (pDiffServParams != NULL)
        {
            *pi4NextFsMplsDiffServParamsIndex = u4DiffParamsId;
            return SNMP_SUCCESS;
        }
    }

    u4DiffParamsId = 0;

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsDiffServParamsServiceType
 Input       :  The Indices
                FsMplsDiffServParamsIndex

                The Object 
                retValFsMplsDiffServParamsServiceType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsDiffServParamsServiceType (INT4 i4FsMplsDiffServParamsIndex,
                                       INT4
                                       *pi4RetValFsMplsDiffServParamsServiceType)
{
    tMplsDiffServParams *pDiffServParams = NULL;

    pDiffServParams = MPLS_DIFFSERV_PARAMS_ENTRY (MPLS_DEF_INCARN,
                                                  i4FsMplsDiffServParamsIndex);
    if (pDiffServParams != NULL)
    {
        *pi4RetValFsMplsDiffServParamsServiceType
            = pDiffServParams->u1ServiceType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsDiffServParamsLlspPscDscp
 Input       :  The Indices
                FsMplsDiffServParamsIndex

                The Object 
                retValFsMplsDiffServParamsLlspPscDscp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsDiffServParamsLlspPscDscp (INT4 i4FsMplsDiffServParamsIndex,
                                       INT4
                                       *pi4RetValFsMplsDiffServParamsLlspPscDscp)
{
    tMplsDiffServParams *pDiffServParams = NULL;

    pDiffServParams = MPLS_DIFFSERV_PARAMS_ENTRY (MPLS_DEF_INCARN,
                                                  i4FsMplsDiffServParamsIndex);
    if (pDiffServParams != NULL)
    {
        if (pDiffServParams->u1ServiceType == MPLS_DIFFSERV_LLSP)
        {
            *pi4RetValFsMplsDiffServParamsLlspPscDscp
                = pDiffServParams->unLspParams.u1LlspPscDscp;
        }
        else
        {
            *pi4RetValFsMplsDiffServParamsLlspPscDscp = MPLS_DIFFSERV_DF_DSCP;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsDiffServParamsElspType
 Input       :  The Indices
                FsMplsDiffServParamsIndex

                The Object 
                retValFsMplsDiffServParamsElspType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsDiffServParamsElspType (INT4 i4FsMplsDiffServParamsIndex,
                                    INT4 *pi4RetValFsMplsDiffServParamsElspType)
{
    tMplsDiffServParams *pDiffServParams = NULL;

    pDiffServParams = MPLS_DIFFSERV_PARAMS_ENTRY (MPLS_DEF_INCARN,
                                                  i4FsMplsDiffServParamsIndex);
    if (pDiffServParams != NULL)
    {
        if (pDiffServParams->u1ServiceType == MPLS_DIFFSERV_ELSP)
        {
            *pi4RetValFsMplsDiffServParamsElspType
                = pDiffServParams->unLspParams.ElspParams.u1ElspType;
        }
        else
        {
            *pi4RetValFsMplsDiffServParamsElspType = MPLS_ZERO;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsDiffServParamsElspSigExpPhbMapIndex
 Input       :  The Indices
                FsMplsDiffServParamsIndex

                The Object 
                retValFsMplsDiffServParamsElspSigExpPhbMapIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsDiffServParamsElspSigExpPhbMapIndex (INT4
                                                 i4FsMplsDiffServParamsIndex,
                                                 INT4
                                                 *pi4RetValFsMplsDiffServParamsElspSigExpPhbMapIndex)
{
    tMplsDiffServParams *pDiffServParams = NULL;

    pDiffServParams = MPLS_DIFFSERV_PARAMS_ENTRY (MPLS_DEF_INCARN,
                                                  i4FsMplsDiffServParamsIndex);
    if (pDiffServParams != NULL)
    {
        if ((pDiffServParams->u1ServiceType == MPLS_DIFFSERV_ELSP)
            && (pDiffServParams->unLspParams.ElspParams.u1ElspType ==
                MPLS_DIFFSERV_SIG_ELSP)
            && (pDiffServParams->unLspParams.ElspParams.pElspSigMap != NULL))
        {
            *pi4RetValFsMplsDiffServParamsElspSigExpPhbMapIndex
                = pDiffServParams->unLspParams.ElspParams.pElspSigMap->
                u4ElspExpPhbMapIndex;
        }
        else
        {
            *pi4RetValFsMplsDiffServParamsElspSigExpPhbMapIndex
                = MPLS_INVALID_EXP_PHB_MAP_INDEX;
        }
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsDiffServParamsStatus
 Input       :  The Indices
                FsMplsDiffServParamsIndex

                The Object 
                retValFsMplsDiffServParamsStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsDiffServParamsStatus (INT4 i4FsMplsDiffServParamsIndex,
                                  INT4 *pi4RetValFsMplsDiffServParamsStatus)
{
    tMplsDiffServParams *pDiffServParams = NULL;

    pDiffServParams = MPLS_DIFFSERV_PARAMS_ENTRY (MPLS_DEF_INCARN,
                                                  i4FsMplsDiffServParamsIndex);
    if (pDiffServParams != NULL)
    {
        *pi4RetValFsMplsDiffServParamsStatus
            = pDiffServParams->u1DiffServParamsStatus;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMplsDiffServParamsServiceType
 Input       :  The Indices
                FsMplsDiffServParamsIndex

                The Object 
                setValFsMplsDiffServParamsServiceType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsDiffServParamsServiceType (INT4 i4FsMplsDiffServParamsIndex,
                                       INT4
                                       i4SetValFsMplsDiffServParamsServiceType)
{
    tMplsDiffServParams *pDiffServParams = NULL;

    if (MPLS_ADMIN_STATUS (MPLS_DEF_INCARN) == MPLS_ADMIN_STATUS_DOWN)
    {
        return SNMP_FAILURE;
    }

    if (MPLS_QOS_POLICY (MPLS_DEF_INCARN) != MPLS_DIFF_SERV)
    {
        return SNMP_FAILURE;
    }

    pDiffServParams = MPLS_DIFFSERV_PARAMS_ENTRY (MPLS_DEF_INCARN,
                                                  i4FsMplsDiffServParamsIndex);
    if (pDiffServParams != NULL)
    {
        pDiffServParams->u1ServiceType
            = (UINT1) i4SetValFsMplsDiffServParamsServiceType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsMplsDiffServParamsLlspPscDscp
 Input       :  The Indices
                FsMplsDiffServParamsIndex

                The Object 
                setValFsMplsDiffServParamsLlspPscDscp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsDiffServParamsLlspPscDscp (INT4 i4FsMplsDiffServParamsIndex,
                                       INT4
                                       i4SetValFsMplsDiffServParamsLlspPscDscp)
{
    tMplsDiffServParams *pDiffServParams = NULL;

    if (MPLS_ADMIN_STATUS (MPLS_DEF_INCARN) == MPLS_ADMIN_STATUS_DOWN)
    {
        return SNMP_FAILURE;
    }

    if (MPLS_QOS_POLICY (MPLS_DEF_INCARN) != MPLS_DIFF_SERV)
    {
        return SNMP_FAILURE;
    }

    pDiffServParams = MPLS_DIFFSERV_PARAMS_ENTRY (MPLS_DEF_INCARN,
                                                  i4FsMplsDiffServParamsIndex);
    if (pDiffServParams != NULL)
    {
        pDiffServParams->unLspParams.u1LlspPscDscp
            = (UINT1) i4SetValFsMplsDiffServParamsLlspPscDscp;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsMplsDiffServParamsElspType
 Input       :  The Indices
                FsMplsDiffServParamsIndex

                The Object 
                setValFsMplsDiffServParamsElspType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsDiffServParamsElspType (INT4 i4FsMplsDiffServParamsIndex,
                                    INT4 i4SetValFsMplsDiffServParamsElspType)
{
    tMplsDiffServParams *pDiffServParams = NULL;

    if (MPLS_ADMIN_STATUS (MPLS_DEF_INCARN) == MPLS_ADMIN_STATUS_DOWN)
    {
        return SNMP_FAILURE;
    }

    if (MPLS_QOS_POLICY (MPLS_DEF_INCARN) != MPLS_DIFF_SERV)
    {
        return SNMP_FAILURE;
    }

    pDiffServParams = MPLS_DIFFSERV_PARAMS_ENTRY (MPLS_DEF_INCARN,
                                                  i4FsMplsDiffServParamsIndex);
    if (pDiffServParams != NULL)
    {
        pDiffServParams->unLspParams.ElspParams.u1ElspType
            = (UINT1) i4SetValFsMplsDiffServParamsElspType;
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetFsMplsDiffServParamsElspSigExpPhbMapIndex
 Input       :  The Indices
                FsMplsDiffServParamsIndex

                The Object 
                setValFsMplsDiffServParamsElspSigExpPhbMapIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsDiffServParamsElspSigExpPhbMapIndex (INT4
                                                 i4FsMplsDiffServParamsIndex,
                                                 INT4
                                                 i4SetValFsMplsDiffServParamsElspSigExpPhbMapIndex)
{
    tMplsDiffServParams *pDiffServParams = NULL;

    if (MPLS_ADMIN_STATUS (MPLS_DEF_INCARN) == MPLS_ADMIN_STATUS_DOWN)
    {
        return SNMP_FAILURE;
    }

    if (MPLS_QOS_POLICY (MPLS_DEF_INCARN) != MPLS_DIFF_SERV)
    {
        return SNMP_FAILURE;
    }

    /* Fix to avoid crash when the FsMplsDiffServParamsElspSigExpPhbMapIndex
     * is saved with invalid value */

    if (((UINT4) i4SetValFsMplsDiffServParamsElspSigExpPhbMapIndex
         >= MPLS_DIFFSERV_ELSP_MAP_ENTRIES (MPLS_DEF_INCARN)) ||
        (i4SetValFsMplsDiffServParamsElspSigExpPhbMapIndex < MPLS_ZERO))
    {
        return SNMP_FAILURE;
    }

    pDiffServParams = MPLS_DIFFSERV_PARAMS_ENTRY (MPLS_DEF_INCARN,
                                                  i4FsMplsDiffServParamsIndex);
    if (pDiffServParams != NULL)
    {
        pDiffServParams->unLspParams.ElspParams.pElspSigMap
            = MPLS_DIFFSERV_ELSP_MAP_ENTRY (MPLS_DEF_INCARN,
                                            i4SetValFsMplsDiffServParamsElspSigExpPhbMapIndex);
        pDiffServParams->unLspParams.ElspParams.pElspSigMap->
            u4ElspExpPhbMapIndex =
            i4SetValFsMplsDiffServParamsElspSigExpPhbMapIndex;

        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsDiffServParamsStatus
 Input       :  The Indices
                FsMplsDiffServParamsIndex

                The Object 
                setValFsMplsDiffServParamsStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsDiffServParamsStatus (INT4 i4FsMplsDiffServParamsIndex,
                                  INT4 i4SetValFsMplsDiffServParamsStatus)
{
    tMplsDiffServParams *pDiffServParams = NULL;
    tMplsDiffServParams *pNewDiffServParams = NULL;

    if (MPLS_ADMIN_STATUS (MPLS_DEF_INCARN) == MPLS_ADMIN_STATUS_DOWN)
    {
        return SNMP_FAILURE;
    }

    if (MPLS_QOS_POLICY (MPLS_DEF_INCARN) != MPLS_DIFF_SERV)
    {
        return SNMP_FAILURE;
    }

    switch (i4SetValFsMplsDiffServParamsStatus)
    {
        case MPLS_STATUS_ACTIVE:
            pDiffServParams = MPLS_DIFFSERV_PARAMS_ENTRY (MPLS_DEF_INCARN,
                                                          i4FsMplsDiffServParamsIndex);
            if ((pDiffServParams == NULL) ||
                (pDiffServParams->u4DiffServParamsIndex !=
                 (UINT4) i4FsMplsDiffServParamsIndex))
            {
                return SNMP_FAILURE;
            }

            if (pDiffServParams->u1DiffServParamsStatus == MPLS_STATUS_ACTIVE)
            {
                return SNMP_FAILURE;
            }

            pDiffServParams->u1DiffServParamsStatus = MPLS_STATUS_ACTIVE;
            return SNMP_SUCCESS;
            break;

        case MPLS_STATUS_CREATE_AND_WAIT:
            pNewDiffServParams = (tMplsDiffServParams *)
                MemAllocMemBlk (MPLS_DIFFSERV_PARAMS_POOL_ID);

            if (pNewDiffServParams == NULL)
            {
                return SNMP_FAILURE;
            }

            MEMSET (pNewDiffServParams, 0, sizeof (tMplsDiffServParams));
            pNewDiffServParams->u1Creator = MPLS_SNMP;
            pNewDiffServParams->u1ServiceType = MPLS_GEN_LSP;
            pNewDiffServParams->unLspParams.ElspParams.pElspSigMap = NULL;

            /* Default value for LlspPsc and ElspType to
             * be set */
            pNewDiffServParams->unLspParams.u1LlspPscDscp =
                MPLS_DIFFSERV_DF_DSCP;
            pNewDiffServParams->unLspParams.ElspParams.u1ElspType =
                MPLS_DIFFSERV_PRECONF_ELSP;
            pNewDiffServParams->u1UsedCount = MPLS_ZERO;

            pNewDiffServParams->u4DiffServParamsIndex
                = i4FsMplsDiffServParamsIndex;
            pNewDiffServParams->u1DiffServParamsStatus = MPLS_STATUS_NOT_READY;

            MPLS_DIFFSERV_PARAMS_ENTRY (MPLS_DEF_INCARN,
                                        i4FsMplsDiffServParamsIndex)
                = pNewDiffServParams;
            /* This cannot go below 0 because after it reaches zero alloc mem
             * would have failed
             */
            MPLS_DIFFSERV_PARAMS_TOKEN_STACK_TOP (MPLS_DEF_INCARN)--;
            break;

        case MPLS_STATUS_DESTROY:
            pDiffServParams = MPLS_DIFFSERV_PARAMS_ENTRY (MPLS_DEF_INCARN,
                                                          i4FsMplsDiffServParamsIndex);
            if (pDiffServParams == NULL)
            {
                return SNMP_FAILURE;
            }
            MPLS_DIFFSERV_PARAMS_ENTRY (MPLS_DEF_INCARN,
                                        i4FsMplsDiffServParamsIndex) = NULL;
            MPLS_DIFFSERV_PARAMS_TOKEN_STACK (MPLS_DEF_INCARN,
                                              MPLS_DIFFSERV_PARAMS_TOKEN_STACK_TOP
                                              (MPLS_DEF_INCARN)) =
                pDiffServParams->u4DiffServParamsIndex;
            /* This cannot go above max because you cannot release more blocks
             * than allocated
             */
            MPLS_DIFFSERV_PARAMS_TOKEN_STACK_TOP (MPLS_DEF_INCARN)++;
            MemReleaseMemBlock (MPLS_DIFFSERV_PARAMS_POOL_ID,
                                (UINT1 *) (pDiffServParams));
            return MPLS_SUCCESS;
            break;
        case MPLS_STATUS_CREATE_AND_GO:
        case MPLS_STATUS_NOT_INSERVICE:
        default:
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMplsDiffServParamsServiceType
 Input       :  The Indices
                FsMplsDiffServParamsIndex

                The Object 
                testValFsMplsDiffServParamsServiceType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsDiffServParamsServiceType (UINT4 *pu4ErrorCode,
                                          INT4 i4FsMplsDiffServParamsIndex,
                                          INT4
                                          i4TestValFsMplsDiffServParamsServiceType)
{
    tMplsDiffServParams *pDiffServParams = NULL;

    if (MPLS_ADMIN_STATUS (MPLS_DEF_INCARN) != MPLS_ADMIN_STATUS_UP)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (MPLS_QOS_POLICY (MPLS_DEF_INCARN) != MPLS_DIFF_SERV)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4FsMplsDiffServParamsIndex >=
        (INT4) MPLS_DIFFSERV_PARAMS_ENTRIES (MPLS_DEF_INCARN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pDiffServParams = MPLS_DIFFSERV_PARAMS_ENTRY (MPLS_DEF_INCARN,
                                                  i4FsMplsDiffServParamsIndex);

    if ((pDiffServParams != NULL) &&
        (pDiffServParams->u4DiffServParamsIndex ==
         (UINT4) i4FsMplsDiffServParamsIndex)
        && (pDiffServParams->u1DiffServParamsStatus != MPLS_STATUS_ACTIVE)
        && (pDiffServParams->u1Creator == MPLS_SNMP))
    {

        if ((i4TestValFsMplsDiffServParamsServiceType != MPLS_DIFFSERV_LLSP)
            && (i4TestValFsMplsDiffServParamsServiceType !=
                MPLS_DIFFSERV_ELSP)
            && (i4TestValFsMplsDiffServParamsServiceType != MPLS_GEN_LSP)
            && (i4TestValFsMplsDiffServParamsServiceType != MPLS_INTSERV_LSP))
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }

        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsDiffServParamsLlspPscDscp
 Input       :  The Indices
                FsMplsDiffServParamsIndex

                The Object 
                testValFsMplsDiffServParamsLlspPscDscp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsDiffServParamsLlspPscDscp (UINT4 *pu4ErrorCode,
                                          INT4 i4FsMplsDiffServParamsIndex,
                                          INT4
                                          i4TestValFsMplsDiffServParamsLlspPscDscp)
{
    tMplsDiffServParams *pDiffServParams = NULL;

    if (MPLS_ADMIN_STATUS (MPLS_DEF_INCARN) != MPLS_ADMIN_STATUS_UP)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (MPLS_QOS_POLICY (MPLS_DEF_INCARN) != MPLS_DIFF_SERV)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4FsMplsDiffServParamsIndex >=
        (INT4) MPLS_DIFFSERV_PARAMS_ENTRIES (MPLS_DEF_INCARN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pDiffServParams = MPLS_DIFFSERV_PARAMS_ENTRY (MPLS_DEF_INCARN,
                                                  i4FsMplsDiffServParamsIndex);

    if ((pDiffServParams != NULL) &&
        (pDiffServParams->u4DiffServParamsIndex ==
         (UINT4) i4FsMplsDiffServParamsIndex)
        && (pDiffServParams->u1DiffServParamsStatus != MPLS_STATUS_ACTIVE)
        && (pDiffServParams->u1Creator == MPLS_SNMP))
    {
        if ((i4TestValFsMplsDiffServParamsLlspPscDscp >= MPLS_ZERO) &&
            (i4TestValFsMplsDiffServParamsLlspPscDscp <= MPLS_MAX_UINT1) &&
            (MplsDiffServValidatePscDscp
             ((UINT1) i4TestValFsMplsDiffServParamsLlspPscDscp) ==
             MPLS_SUCCESS))
        {
            return SNMP_SUCCESS;
        }
        else
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsDiffServParamsElspType
 Input       :  The Indices
                FsMplsDiffServParamsIndex

                The Object 
                testValFsMplsDiffServParamsElspType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsDiffServParamsElspType (UINT4 *pu4ErrorCode,
                                       INT4 i4FsMplsDiffServParamsIndex,
                                       INT4
                                       i4TestValFsMplsDiffServParamsElspType)
{
    tMplsDiffServParams *pDiffServParams = NULL;

    if (MPLS_ADMIN_STATUS (MPLS_DEF_INCARN) != MPLS_ADMIN_STATUS_UP)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (MPLS_QOS_POLICY (MPLS_DEF_INCARN) != MPLS_DIFF_SERV)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4FsMplsDiffServParamsIndex >=
        (INT4) MPLS_DIFFSERV_PARAMS_ENTRIES (MPLS_DEF_INCARN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pDiffServParams = MPLS_DIFFSERV_PARAMS_ENTRY (MPLS_DEF_INCARN,
                                                  i4FsMplsDiffServParamsIndex);

    if ((pDiffServParams != NULL) &&
        (pDiffServParams->u4DiffServParamsIndex ==
         (UINT4) i4FsMplsDiffServParamsIndex)
        && (pDiffServParams->u1DiffServParamsStatus != MPLS_STATUS_ACTIVE)
        && (pDiffServParams->u1Creator == MPLS_SNMP))
    {

        if ((i4TestValFsMplsDiffServParamsElspType != MPLS_DIFFSERV_SIG_ELSP)
            && (i4TestValFsMplsDiffServParamsElspType !=
                MPLS_DIFFSERV_PRECONF_ELSP))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }

        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsDiffServParamsElspSigExpPhbMapIndex
 Input       :  The Indices
                FsMplsDiffServParamsIndex

                The Object 
                testValFsMplsDiffServParamsElspSigExpPhbMapIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsDiffServParamsElspSigExpPhbMapIndex (UINT4 *pu4ErrorCode,
                                                    INT4
                                                    i4FsMplsDiffServParamsIndex,
                                                    INT4
                                                    i4TestValFsMplsDiffServParamsElspSigExpPhbMapIndex)
{
    tMplsDiffServParams *pDiffServParams = NULL;
    tElspMapRow        *pElspSigMap = NULL;
    UINT1               u1Exp = MPLS_ZERO;

    if (MPLS_ADMIN_STATUS (MPLS_DEF_INCARN) != MPLS_ADMIN_STATUS_UP)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (MPLS_QOS_POLICY (MPLS_DEF_INCARN) != MPLS_DIFF_SERV)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (i4FsMplsDiffServParamsIndex >=
        (INT4) MPLS_DIFFSERV_PARAMS_ENTRIES (MPLS_DEF_INCARN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pDiffServParams = MPLS_DIFFSERV_PARAMS_ENTRY (MPLS_DEF_INCARN,
                                                  i4FsMplsDiffServParamsIndex);

    if ((pDiffServParams != NULL) &&
        (pDiffServParams->u4DiffServParamsIndex ==
         (UINT4) i4FsMplsDiffServParamsIndex)
        && (pDiffServParams->u1DiffServParamsStatus != MPLS_STATUS_ACTIVE)
        && (pDiffServParams->u1Creator == MPLS_SNMP))
    {
        if (((UINT4) i4TestValFsMplsDiffServParamsElspSigExpPhbMapIndex
             >= MPLS_DIFFSERV_ELSP_MAP_ENTRIES (MPLS_DEF_INCARN)) ||
            (i4TestValFsMplsDiffServParamsElspSigExpPhbMapIndex < MPLS_ZERO))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        pElspSigMap = MPLS_DIFFSERV_ELSP_MAP_ENTRY (MPLS_DEF_INCARN,
                                                    i4TestValFsMplsDiffServParamsElspSigExpPhbMapIndex);
        if (pElspSigMap != NULL)
        {
            if (pElspSigMap->u1Creator != MPLS_SNMP)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            for (u1Exp = 0; u1Exp < MPLS_DIFFSERV_MAX_EXP; u1Exp++)
            {
                if ((pElspSigMap->aElspExpPhbMapArray[u1Exp].u1RowStatus !=
                     MPLS_STATUS_ACTIVE)
                    && (pElspSigMap->aElspExpPhbMapArray[u1Exp].u1RowStatus !=
                        MPLS_STATUS_DOWN))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
            return SNMP_SUCCESS;
        }
    }

    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsDiffServParamsStatus
 Input       :  The Indices
                FsMplsDiffServParamsIndex

                The Object 
                testValFsMplsDiffServParamsStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsDiffServParamsStatus (UINT4 *pu4ErrorCode,
                                     INT4 i4FsMplsDiffServParamsIndex,
                                     INT4 i4TestValFsMplsDiffServParamsStatus)
{
    tMplsDiffServParams *pDiffServParams = NULL;
    UINT4               u4DiffServParamsIndex = MPLS_ZERO;

    if (MPLS_ADMIN_STATUS (MPLS_DEF_INCARN) == MPLS_ADMIN_STATUS_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (MPLS_QOS_POLICY (MPLS_DEF_INCARN) != MPLS_DIFF_SERV)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMplsDiffServParamsStatus == MPLS_STATUS_ACTIVE)
        || (i4TestValFsMplsDiffServParamsStatus == MPLS_STATUS_DESTROY))
    {
        if (i4FsMplsDiffServParamsIndex >=
            (INT4) MPLS_DIFFSERV_PARAMS_ENTRIES (MPLS_DEF_INCARN))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
        pDiffServParams = MPLS_DIFFSERV_PARAMS_ENTRY (MPLS_DEF_INCARN,
                                                      i4FsMplsDiffServParamsIndex);
    }

    switch (i4TestValFsMplsDiffServParamsStatus)
    {
        case MPLS_STATUS_ACTIVE:
            if ((pDiffServParams != NULL) &&
                (pDiffServParams->u4DiffServParamsIndex ==
                 (UINT4) i4FsMplsDiffServParamsIndex)
                && (pDiffServParams->u1Creator == MPLS_SNMP)
                && (pDiffServParams->u1DiffServParamsStatus !=
                    MPLS_STATUS_ACTIVE))
            {
                if ((pDiffServParams->u1ServiceType == MPLS_DIFFSERV_ELSP) &&
                    (pDiffServParams->unLspParams.ElspParams.u1ElspType ==
                     MPLS_DIFFSERV_SIG_ELSP)
                    && (MPLS_SIG_ELSP_MAP (pDiffServParams) == NULL))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
                return SNMP_SUCCESS;
            }
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            break;

        case MPLS_STATUS_DESTROY:
            if ((pDiffServParams != NULL) &&
                (pDiffServParams->u4DiffServParamsIndex ==
                 (UINT4) i4FsMplsDiffServParamsIndex)
                && (pDiffServParams->u1Creator == MPLS_SNMP)
                && (pDiffServParams->u1UsedCount == MPLS_ZERO))
            {
                return SNMP_SUCCESS;
            }
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            break;

        case MPLS_STATUS_CREATE_AND_WAIT:
            if (MplsGetDiffServParamsIndexNext (MPLS_DEF_INCARN,
                                                &u4DiffServParamsIndex) ==
                MPLS_SUCCESS)
            {
                if (u4DiffServParamsIndex ==
                    (UINT4) i4FsMplsDiffServParamsIndex)
                {
                    pDiffServParams =
                        MPLS_DIFFSERV_PARAMS_ENTRY (MPLS_DEF_INCARN,
                                                    i4FsMplsDiffServParamsIndex);
                    if (pDiffServParams == NULL)
                    {
                        return SNMP_SUCCESS;
                    }
                }
            }
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            break;

        case MPLS_STATUS_CREATE_AND_GO:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            break;

        case MPLS_STATUS_NOT_INSERVICE:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    }
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsDiffServParamsTable
 Input       :  The Indices
                FsMplsDiffServParamsIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsDiffServParamsTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTnlModel
 Input       :  The Indices

                The Object 
                retValFsMplsTnlModel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTnlModel (INT4 *pi4RetValFsMplsTnlModel)
{
    *pi4RetValFsMplsTnlModel =
        gMplsIncarn[MPLS_DEF_INCARN].DiffServGlobal.u1TnlModel;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsrcMgmtType
 Input       :  The Indices

                The Object 
                retValFsMplsRsrcMgmtType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsrcMgmtType (INT4 *pi4RetValFsMplsRsrcMgmtType)
{
    *pi4RetValFsMplsRsrcMgmtType =
        RESOURCE_MANAGEMENT_TYPE (gDiffServRMGblInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTTLVal
 Input       :  The Indices

                The Object
                retValFsMplsTTLVal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTTLVal (INT4 *pi4RetValFsMplsTTLVal)
{
    *pi4RetValFsMplsTTLVal = gMplsIncarn[MPLS_DEF_INCARN].u1TTL;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMplsTnlModel
 Input       :  The Indices

                The Object 
                setValFsMplsTnlModel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsTnlModel (INT4 i4SetValFsMplsTnlModel)
{
    gMplsIncarn[MPLS_DEF_INCARN].DiffServGlobal.u1TnlModel =
        (UINT1) i4SetValFsMplsTnlModel;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsrcMgmtType
 Input       :  The Indices

                The Object 
                setValFsMplsRsrcMgmtType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsrcMgmtType (INT4 i4SetValFsMplsRsrcMgmtType)
{
    RESOURCE_MANAGEMENT_TYPE (gDiffServRMGblInfo) =
        (UINT1) i4SetValFsMplsRsrcMgmtType;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsTTLVal
 Input       :  The Indices

                The Object
                setValFsMplsTTLVal
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsTTLVal (INT4 i4SetValFsMplsTTLVal)
{
    gMplsIncarn[MPLS_DEF_INCARN].u1TTL = (UINT1) i4SetValFsMplsTTLVal;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMplsTnlModel
 Input       :  The Indices

                The Object 
                testValFsMplsTnlModel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsTnlModel (UINT4 *pu4ErrorCode, INT4 i4TestValFsMplsTnlModel)
{
    switch (i4TestValFsMplsTnlModel)
    {
        case UNIFORM_MODEL:
        case PIPE_MODEL:
        case SHORT_PIPE_MODEL:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsrcMgmtType
 Input       :  The Indices

                The Object 
                testValFsMplsRsrcMgmtType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsrcMgmtType (UINT4 *pu4ErrorCode,
                             INT4 i4TestValFsMplsRsrcMgmtType)
{
    switch (i4TestValFsMplsRsrcMgmtType)
    {
        case TE_DS_AGGREGATE_RESOURCES:
        case TE_DS_PEROABASED_RESOURCES:
        case TE_DS_CLASSTYPE_RESOURCES:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsTTLVal
 Input       :  The Indices

                The Object
                testValFsMplsTTLVal
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsTTLVal (UINT4 *pu4ErrorCode, INT4 i4TestValFsMplsTTLVal)
{
    if ((i4TestValFsMplsTTLVal <= MPLS_TTL_MIN_VAL) ||
        (i4TestValFsMplsTTLVal > MPLS_TTL_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsTnlModel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsTnlModel (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsrcMgmtType
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsrcMgmtType (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsTTLVal
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsTTLVal (UINT4 *pu4ErrorCode,
                      tSnmpIndexList * pSnmpIndexList,
                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsDsTeStatus
 Input       :  The Indices

                The Object 
                retValFsMplsDsTeStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsDsTeStatus (INT4 *pi4RetValFsMplsDsTeStatus)
{
    MPLS_CMN_LOCK ();
    *pi4RetValFsMplsDsTeStatus = gi4MplsDsTeStatus;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsiTTLVal
 Input       :  The Indices

                The Object
                retValFsMplsiTTLVal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsiTTLVal (INT4 *pi4RetValFsMplsiTTLVal)
{
    *pi4RetValFsMplsiTTLVal = gMplsIncarn[MPLS_DEF_INCARN].u1iTTL;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsOTTLVal
 Input       :  The Indices

                The Object
                retValFsMplsOTTLVal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsOTTLVal (INT4 *pi4RetValFsMplsOTTLVal)
{
    *pi4RetValFsMplsOTTLVal = gMplsIncarn[MPLS_DEF_INCARN].u1OTTL;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsprviTTLVal
 Input       :  The Indices

                The Object
                retValFsMplsprviTTLVal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsprviTTLVal (INT4 *pi4RetValFsMplsprviTTLVal)
{
    *pi4RetValFsMplsprviTTLVal = gMplsIncarn[MPLS_DEF_INCARN].u1PrviTTL;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsprvOTTLVal
 Input       :  The Indices

                The Object
                retValFsMplsprvOTTLVal
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsprvOTTLVal (INT4 *pi4RetValFsMplsprvOTTLVal)
{
    *pi4RetValFsMplsprvOTTLVal = gMplsIncarn[MPLS_DEF_INCARN].u1PrvOTTL;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMplsDsTeStatus
 Input       :  The Indices

                The Object 
                setValFsMplsDsTeStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsDsTeStatus (INT4 i4SetValFsMplsDsTeStatus)
{
    MPLS_CMN_LOCK ();
    MPLS_DSTE_STATUS (MPLS_DEF_INCARN) = i4SetValFsMplsDsTeStatus;
    gi4MplsDsTeStatus = i4SetValFsMplsDsTeStatus;

    if (i4SetValFsMplsDsTeStatus == MPLS_DSTE_STATUS_ENABLE)
    {
        MplsPortTlmNotifyDiffServParams ();
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMplsDsTeStatus
 Input       :  The Indices

                The Object 
                testValFsMplsDsTeStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsDsTeStatus (UINT4 *pu4ErrorCode, INT4 i4TestValFsMplsDsTeStatus)
{
    MPLS_CMN_LOCK ();
    if ((i4TestValFsMplsDsTeStatus != MPLS_DSTE_STATUS_ENABLE) &&
        (i4TestValFsMplsDsTeStatus != MPLS_DSTE_STATUS_DISABLE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsDsTeStatus
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsDsTeStatus (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMplsDsTeClassTypeTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsDsTeClassTypeTable
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMplsDsTeClassTypeTable (UINT4
                                                  u4FsMplsDsTeClassTypeIndex)
{
    if (u4FsMplsDsTeClassTypeIndex >= MPLS_MAX_TE_CLASS_ENTRIES)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsDsTeClassTypeTable
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMplsDsTeClassTypeTable (UINT4 *pu4FsMplsDsTeClassTypeIndex)
{
    UINT4               u4ClassId = MPLS_ZERO;
    tMplsDsTeClassType  ClassEntry;

    MEMSET (&ClassEntry, MPLS_ZERO, sizeof (tMplsDsTeClassType));

    MPLS_CMN_LOCK ();

    for (u4ClassId = 0; u4ClassId < MPLS_MAX_CLASS_TYPE_ENTRIES; u4ClassId++)
    {
        ClassEntry = MPLS_DIFFSERV_CLASS_TYPE_ENTRY (MPLS_DEF_INCARN,
                                                     u4ClassId);
        /* first entry in table */
        if (ClassEntry.i4DsTeClassTypeRowStatus != MPLS_ZERO)
        {
            *pu4FsMplsDsTeClassTypeIndex = u4ClassId;

            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }

    MPLS_CMN_UNLOCK ();

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMplsDsTeClassTypeTable
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex
                nextFsMplsDsTeClassTypeIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMplsDsTeClassTypeTable (UINT4 u4FsMplsDsTeClassTypeIndex,
                                         UINT4 *pu4NextFsMplsDsTeClassTypeIndex)
{
    UINT4               u4ClassId = MPLS_ZERO;
    tMplsDsTeClassType  ClassEntry;

    MEMSET (&ClassEntry, MPLS_ZERO, sizeof (tMplsDsTeClassType));

    MPLS_CMN_LOCK ();

    u4ClassId = u4FsMplsDsTeClassTypeIndex + 1;

    for (; u4ClassId < MPLS_MAX_CLASS_TYPE_ENTRIES; u4ClassId++)
    {
        ClassEntry = MPLS_DIFFSERV_CLASS_TYPE_ENTRY (MPLS_DEF_INCARN,
                                                     u4ClassId);

        if (ClassEntry.i4DsTeClassTypeRowStatus != MPLS_ZERO)
        {
            *pu4NextFsMplsDsTeClassTypeIndex = u4ClassId;

            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsDsTeClassTypeDescription
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex

                The Object 
                retValFsMplsDsTeClassTypeDescription
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsDsTeClassTypeDescription (UINT4 u4FsMplsDsTeClassTypeIndex,
                                      tSNMP_OCTET_STRING_TYPE
                                      * pRetValFsMplsDsTeClassTypeDescription)
{
    tMplsDsTeClassType  ClassEntry;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&ClassEntry, MPLS_ZERO, sizeof (tMplsDsTeClassType));

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDsTeGetAllClassTypeTable (u4FsMplsDsTeClassTypeIndex,
                                             &ClassEntry);

    MPLS_CMN_UNLOCK ();

    pRetValFsMplsDsTeClassTypeDescription->i4_Length
        = STRLEN (ClassEntry.au1DsTeClassTypeDesc);

    MEMCPY (pRetValFsMplsDsTeClassTypeDescription->pu1_OctetList,
            ClassEntry.au1DsTeClassTypeDesc,
            pRetValFsMplsDsTeClassTypeDescription->i4_Length);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMplsDsTeClassTypeRowStatus
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex

                The Object 
                retValFsMplsDsTeClassTypeRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsDsTeClassTypeRowStatus (UINT4 u4FsMplsDsTeClassTypeIndex,
                                    INT4 *pi4RetValFsMplsDsTeClassTypeRowStatus)
{
    tMplsDsTeClassType  ClassEntry;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&ClassEntry, MPLS_ZERO, sizeof (tMplsDsTeClassType));

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDsTeGetAllClassTypeTable (u4FsMplsDsTeClassTypeIndex,
                                             &ClassEntry);
    MPLS_CMN_UNLOCK ();

    *pi4RetValFsMplsDsTeClassTypeRowStatus =
        ClassEntry.i4DsTeClassTypeRowStatus;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMplsDsTeClassTypeBwPercentage
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex

                The Object 
                retValFsMplsDsTeClassTypeBwPercentage
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsDsTeClassTypeBwPercentage (UINT4 u4FsMplsDsTeClassTypeIndex,
                                       INT4
                                       *pi4RetValFsMplsDsTeClassTypeBwPercentage)
{
    tMplsDsTeClassType  ClassEntry;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&ClassEntry, MPLS_ZERO, sizeof (tMplsDsTeClassType));

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDsTeGetAllClassTypeTable (u4FsMplsDsTeClassTypeIndex,
                                             &ClassEntry);

    MPLS_CMN_UNLOCK ();

    *pi4RetValFsMplsDsTeClassTypeBwPercentage =
        ClassEntry.i4DsTeClassTypeBwPercent;

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMplsDsTeClassTypeDescription
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex

                The Object 
                setValFsMplsDsTeClassTypeDescription
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsDsTeClassTypeDescription (UINT4 u4FsMplsDsTeClassTypeIndex,
                                      tSNMP_OCTET_STRING_TYPE
                                      * pSetValFsMplsDsTeClassTypeDescription)
{
    tMplsDsTeClassType  ClassEntry;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&ClassEntry, MPLS_ZERO, sizeof (tMplsDsTeClassType));

    MEMCPY (ClassEntry.au1DsTeClassTypeDesc,
            pSetValFsMplsDsTeClassTypeDescription->pu1_OctetList,
            pSetValFsMplsDsTeClassTypeDescription->i4_Length);

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDsTeSetAllClassTypeTable (u4FsMplsDsTeClassTypeIndex,
                                             &ClassEntry,
                                             MPLS_DSTE_CLASS_TYPE_DESCRIPTION);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMplsDsTeClassTypeRowStatus
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex

                The Object 
                setValFsMplsDsTeClassTypeRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsDsTeClassTypeRowStatus (UINT4 u4FsMplsDsTeClassTypeIndex,
                                    INT4 i4SetValFsMplsDsTeClassTypeRowStatus)
{
    tMplsDsTeClassType  ClassEntry;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&ClassEntry, MPLS_ZERO, sizeof (tMplsDsTeClassType));

    ClassEntry.i4DsTeClassTypeRowStatus = i4SetValFsMplsDsTeClassTypeRowStatus;

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDsTeSetAllClassTypeTable (u4FsMplsDsTeClassTypeIndex,
                                             &ClassEntry,
                                             MPLS_DSTE_CLASS_TYPE_ROW_STATUS);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMplsDsTeClassTypeBwPercentage
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex

                The Object 
                setValFsMplsDsTeClassTypeBwPercentage
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsDsTeClassTypeBwPercentage (UINT4 u4FsMplsDsTeClassTypeIndex,
                                       INT4
                                       i4SetValFsMplsDsTeClassTypeBwPercentage)
{
    tMplsDsTeClassType  ClassEntry;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&ClassEntry, MPLS_ZERO, sizeof (tMplsDsTeClassType));

    ClassEntry.i4DsTeClassTypeBwPercent =
        i4SetValFsMplsDsTeClassTypeBwPercentage;

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDsTeSetAllClassTypeTable (u4FsMplsDsTeClassTypeIndex,
                                             &ClassEntry,
                                             MPLS_DSTE_CLASS_TYPE_BW_PERCENT);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMplsDsTeClassTypeDescription
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex

                The Object 
                testValFsMplsDsTeClassTypeDescription
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsDsTeClassTypeDescription (UINT4 *pu4ErrorCode,
                                         UINT4 u4FsMplsDsTeClassTypeIndex,
                                         tSNMP_OCTET_STRING_TYPE
                                         *
                                         pTestValFsMplsDsTeClassTypeDescription)
{
    tMplsDsTeClassType  ClassEntry;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&ClassEntry, MPLS_ZERO, sizeof (tMplsDsTeClassType));

    MEMCPY (ClassEntry.au1DsTeClassTypeDesc,
            pTestValFsMplsDsTeClassTypeDescription->pu1_OctetList,
            pTestValFsMplsDsTeClassTypeDescription->i4_Length);

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDsTeTestAllClassTypeTable (pu4ErrorCode,
                                              u4FsMplsDsTeClassTypeIndex,
                                              &ClassEntry,
                                              MPLS_DSTE_CLASS_TYPE_DESCRIPTION);
    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsDsTeClassTypeRowStatus
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex

                The Object 
                testValFsMplsDsTeClassTypeRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsDsTeClassTypeRowStatus (UINT4 *pu4ErrorCode,
                                       UINT4 u4FsMplsDsTeClassTypeIndex,
                                       INT4
                                       i4TestValFsMplsDsTeClassTypeRowStatus)
{
    tMplsDsTeClassType  ClassEntry;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&ClassEntry, MPLS_ZERO, sizeof (tMplsDsTeClassType));

    ClassEntry.i4DsTeClassTypeRowStatus = i4TestValFsMplsDsTeClassTypeRowStatus;

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDsTeTestAllClassTypeTable (pu4ErrorCode,
                                              u4FsMplsDsTeClassTypeIndex,
                                              &ClassEntry,
                                              MPLS_DSTE_CLASS_TYPE_ROW_STATUS);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsDsTeClassTypeBwPercentage
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex

                The Object 
                testValFsMplsDsTeClassTypeBwPercentage
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsDsTeClassTypeBwPercentage (UINT4 *pu4ErrorCode,
                                          UINT4 u4FsMplsDsTeClassTypeIndex,
                                          INT4
                                          i4TestValFsMplsDsTeClassTypeBwPercentage)
{
    tMplsDsTeClassType  ClassEntry;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&ClassEntry, MPLS_ZERO, sizeof (tMplsDsTeClassType));

    ClassEntry.i4DsTeClassTypeBwPercent =
        i4TestValFsMplsDsTeClassTypeBwPercentage;

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDsTeTestAllClassTypeTable (pu4ErrorCode,
                                              u4FsMplsDsTeClassTypeIndex,
                                              &ClassEntry,
                                              MPLS_DSTE_CLASS_TYPE_BW_PERCENT);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsDsTeClassTypeTable
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsDsTeClassTypeTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMplsDsTeClassTypeToTcMapTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsDsTeClassTypeToTcMapTable
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex
                FsMplsDsTeTcIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceFsMplsDsTeClassTypeToTcMapTable
    (UINT4 u4FsMplsDsTeClassTypeIndex, UINT4 u4FsMplsDsTeTcIndex)
{
    if (u4FsMplsDsTeClassTypeIndex >= MPLS_MAX_CLASS_TYPE_ENTRIES)
    {
        return SNMP_FAILURE;
    }

    if (u4FsMplsDsTeTcIndex > MPLS_MAX_TRAFFIC_CLASS_ENTRIES)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsDsTeClassTypeToTcMapTable
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex
                FsMplsDsTeTcIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 
     
     
     
     
     
     
     
    nmhGetFirstIndexFsMplsDsTeClassTypeToTcMapTable
    (UINT4 *pu4FsMplsDsTeClassTypeIndex, UINT4 *pu4FsMplsDsTeTcIndex)
{
    UINT4               u4ClassId;
    UINT4               u4TrafficClassId;
    tMplsDsTeClassTypeTcMap TrafficClassMapEntry;

    MEMSET (&TrafficClassMapEntry, MPLS_ZERO, sizeof (tMplsDsTeClassTypeTcMap));

    MPLS_CMN_LOCK ();

    for (u4ClassId = 0; u4ClassId < MPLS_MAX_CLASS_TYPE_ENTRIES; u4ClassId++)
    {
        for (u4TrafficClassId = 0;
             u4TrafficClassId < MPLS_MAX_TRAFFIC_CLASS_ENTRIES;
             u4TrafficClassId++)
        {
            TrafficClassMapEntry =
                MPLS_DIFFSERV_TRAFFIC_CLASS_MAP (MPLS_DEF_INCARN, u4ClassId,
                                                 u4TrafficClassId);
            if (TrafficClassMapEntry.i4DsTeClassTypeToTcRowStatus != MPLS_ZERO)
            {
                *pu4FsMplsDsTeClassTypeIndex = u4ClassId;
                *pu4FsMplsDsTeTcIndex = u4TrafficClassId;

                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
        }
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMplsDsTeClassTypeToTcMapTable
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex
                nextFsMplsDsTeClassTypeIndex
                FsMplsDsTeTcIndex
                nextFsMplsDsTeTcIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 
     
     
     
     
     
     
     
    nmhGetNextIndexFsMplsDsTeClassTypeToTcMapTable
    (UINT4 u4FsMplsDsTeClassTypeIndex,
     UINT4 *pu4NextFsMplsDsTeClassTypeIndex,
     UINT4 u4FsMplsDsTeTcIndex, UINT4 *pu4NextFsMplsDsTeTcIndex)
{
    UINT4               u4ClassId = MPLS_ZERO;
    UINT4               u4TrafficClassId = MPLS_ZERO;
    tMplsDsTeClassTypeTcMap TrafficClassMapEntry;

    MEMSET (&TrafficClassMapEntry, MPLS_ZERO, sizeof (tMplsDsTeClassTypeTcMap));

    MPLS_CMN_LOCK ();

    u4ClassId = u4FsMplsDsTeClassTypeIndex;
    u4TrafficClassId = u4FsMplsDsTeTcIndex + 1;

    for (; u4ClassId < MPLS_MAX_CLASS_TYPE_ENTRIES; u4ClassId++)
    {
        for (; u4TrafficClassId < MPLS_MAX_TRAFFIC_CLASS_ENTRIES;
             u4TrafficClassId++)
        {
            TrafficClassMapEntry = MPLS_DIFFSERV_TRAFFIC_CLASS_MAP
                (MPLS_DEF_INCARN, u4ClassId, u4TrafficClassId);

            if (TrafficClassMapEntry.i4DsTeClassTypeToTcRowStatus != MPLS_ZERO)
            {
                *pu4NextFsMplsDsTeClassTypeIndex = u4ClassId;
                *pu4NextFsMplsDsTeTcIndex = u4TrafficClassId;

                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
        }
        u4TrafficClassId = 0;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsDsTeTcType
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex
                FsMplsDsTeTcIndex

                The Object 
                retValFsMplsDsTeTcType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsDsTeTcType (UINT4 u4FsMplsDsTeClassTypeIndex,
                        UINT4 u4FsMplsDsTeTcIndex,
                        INT4 *pi4RetValFsMplsDsTeTcType)
{
    tMplsDsTeClassTypeTcMap TrafficClassMapEntry;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TrafficClassMapEntry, MPLS_ZERO, sizeof (tMplsDsTeClassTypeTcMap));

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDsTeGetAllClassTypeTcMapTable (u4FsMplsDsTeClassTypeIndex,
                                                  u4FsMplsDsTeTcIndex,
                                                  &TrafficClassMapEntry);
    MPLS_CMN_UNLOCK ();

    *pi4RetValFsMplsDsTeTcType = TrafficClassMapEntry.u4DsTeTcType;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMplsDsTeTcDescription
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex
                FsMplsDsTeTcIndex

                The Object 
                retValFsMplsDsTeTcDescription
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsDsTeTcDescription (UINT4 u4FsMplsDsTeClassTypeIndex,
                               UINT4 u4FsMplsDsTeTcIndex,
                               tSNMP_OCTET_STRING_TYPE
                               * pRetValFsMplsDsTeTcDescription)
{
    tMplsDsTeClassTypeTcMap TrafficClassMapEntry;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TrafficClassMapEntry, MPLS_ZERO, sizeof (tMplsDsTeClassTypeTcMap));

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDsTeGetAllClassTypeTcMapTable (u4FsMplsDsTeClassTypeIndex,
                                                  u4FsMplsDsTeTcIndex,
                                                  &TrafficClassMapEntry);

    MPLS_CMN_UNLOCK ();

    pRetValFsMplsDsTeTcDescription->i4_Length
        = STRLEN (TrafficClassMapEntry.au1DsTeClassTypeToTcDesc);

    MEMCPY (pRetValFsMplsDsTeTcDescription->pu1_OctetList,
            TrafficClassMapEntry.au1DsTeClassTypeToTcDesc,
            pRetValFsMplsDsTeTcDescription->i4_Length);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMplsDsTeTcMapRowStatus
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex
                FsMplsDsTeTcIndex

                The Object 
                retValFsMplsDsTeTcMapRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsDsTeTcMapRowStatus (UINT4 u4FsMplsDsTeClassTypeIndex,
                                UINT4 u4FsMplsDsTeTcIndex,
                                INT4 *pi4RetValFsMplsDsTeTcMapRowStatus)
{
    tMplsDsTeClassTypeTcMap TrafficClassMapEntry;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TrafficClassMapEntry, MPLS_ZERO, sizeof (tMplsDsTeClassTypeTcMap));

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDsTeGetAllClassTypeTcMapTable (u4FsMplsDsTeClassTypeIndex,
                                                  u4FsMplsDsTeTcIndex,
                                                  &TrafficClassMapEntry);

    MPLS_CMN_UNLOCK ();

    *pi4RetValFsMplsDsTeTcMapRowStatus =
        TrafficClassMapEntry.i4DsTeClassTypeToTcRowStatus;

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMplsDsTeTcType
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex
                FsMplsDsTeTcIndex

                The Object 
                setValFsMplsDsTeTcType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsDsTeTcType (UINT4 u4FsMplsDsTeClassTypeIndex,
                        UINT4 u4FsMplsDsTeTcIndex,
                        INT4 i4SetValFsMplsDsTeTcType)
{
    tMplsDsTeClassTypeTcMap TrafficClassMapEntry;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TrafficClassMapEntry, MPLS_ZERO, sizeof (tMplsDsTeClassTypeTcMap));

    MPLS_CMN_LOCK ();

    TrafficClassMapEntry.u4DsTeTcType = i4SetValFsMplsDsTeTcType;

    i1RetVal = MplsDsTeSetAllClassTypeTcMapTable (u4FsMplsDsTeClassTypeIndex,
                                                  u4FsMplsDsTeTcIndex,
                                                  &TrafficClassMapEntry,
                                                  MPLS_DSTE_TC_MAP_TYPE);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMplsDsTeTcDescription
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex
                FsMplsDsTeTcIndex

                The Object 
                setValFsMplsDsTeTcDescription
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsDsTeTcDescription (UINT4 u4FsMplsDsTeClassTypeIndex,
                               UINT4 u4FsMplsDsTeTcIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pSetValFsMplsDsTeTcDescription)
{
    tMplsDsTeClassTypeTcMap TrafficClassMapEntry;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TrafficClassMapEntry, MPLS_ZERO, sizeof (tMplsDsTeClassTypeTcMap));

    MEMCPY (TrafficClassMapEntry.au1DsTeClassTypeToTcDesc,
            pSetValFsMplsDsTeTcDescription->pu1_OctetList,
            pSetValFsMplsDsTeTcDescription->i4_Length);

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDsTeSetAllClassTypeTcMapTable (u4FsMplsDsTeClassTypeIndex,
                                                  u4FsMplsDsTeTcIndex,
                                                  &TrafficClassMapEntry,
                                                  MPLS_DSTE_TC_MAP_DESC);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMplsDsTeTcMapRowStatus
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex
                FsMplsDsTeTcIndex

                The Object 
                setValFsMplsDsTeTcMapRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsDsTeTcMapRowStatus (UINT4 u4FsMplsDsTeClassTypeIndex,
                                UINT4 u4FsMplsDsTeTcIndex,
                                INT4 i4SetValFsMplsDsTeTcMapRowStatus)
{
    tMplsDsTeClassTypeTcMap TrafficClassMapEntry;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TrafficClassMapEntry, MPLS_ZERO, sizeof (tMplsDsTeClassTypeTcMap));

    TrafficClassMapEntry.i4DsTeClassTypeToTcRowStatus =
        i4SetValFsMplsDsTeTcMapRowStatus;

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDsTeSetAllClassTypeTcMapTable (u4FsMplsDsTeClassTypeIndex,
                                                  u4FsMplsDsTeTcIndex,
                                                  &TrafficClassMapEntry,
                                                  MPLS_DSTE_TC_MAP_ROW_STATUS);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMplsDsTeTcType
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex
                FsMplsDsTeTcIndex

                The Object 
                testValFsMplsDsTeTcType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsDsTeTcType (UINT4 *pu4ErrorCode,
                           UINT4 u4FsMplsDsTeClassTypeIndex,
                           UINT4 u4FsMplsDsTeTcIndex,
                           INT4 i4TestValFsMplsDsTeTcType)
{
    tMplsDsTeClassTypeTcMap TrafficClassMapEntry;
    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4NextFsMplsDsTeTcIndex = MPLS_ZERO;	

    MEMSET (&TrafficClassMapEntry, MPLS_ZERO, sizeof (tMplsDsTeClassTypeTcMap));

    for (; i4NextFsMplsDsTeTcIndex < MPLS_MAX_TRAFFIC_CLASS_ENTRIES;
            i4NextFsMplsDsTeTcIndex++)
    {
        TrafficClassMapEntry = MPLS_DIFFSERV_TRAFFIC_CLASS_MAP (MPLS_DEF_INCARN,
                u4FsMplsDsTeClassTypeIndex, i4NextFsMplsDsTeTcIndex);
        if (TrafficClassMapEntry.u4DsTeTcType == (UINT4) i4TestValFsMplsDsTeTcType)
	{
	    if (STRLEN(TrafficClassMapEntry.au1DsTeClassTypeToTcDesc) == MPLS_ZERO)
            {
                continue;
            }
            else
            {
                return i1RetVal;
            }
        }
    }
    MEMSET (&TrafficClassMapEntry, MPLS_ZERO, sizeof (tMplsDsTeClassTypeTcMap));

    TrafficClassMapEntry.u4DsTeTcType = i4TestValFsMplsDsTeTcType;

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDsTeTestAllClassTypeTcMapTable (pu4ErrorCode,
                                                   u4FsMplsDsTeClassTypeIndex,
                                                   u4FsMplsDsTeTcIndex,
                                                   &TrafficClassMapEntry,
                                                   MPLS_DSTE_TC_MAP_TYPE);
    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsDsTeTcDescription
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex
                FsMplsDsTeTcIndex

                The Object 
                testValFsMplsDsTeTcDescription
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsDsTeTcDescription (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsMplsDsTeClassTypeIndex,
                                  UINT4 u4FsMplsDsTeTcIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pTestValFsMplsDsTeTcDescription)
{
    tMplsDsTeClassTypeTcMap TrafficClassMapEntry;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TrafficClassMapEntry, MPLS_ZERO, sizeof (tMplsDsTeClassTypeTcMap));

    MEMCPY (TrafficClassMapEntry.au1DsTeClassTypeToTcDesc,
            pTestValFsMplsDsTeTcDescription->pu1_OctetList,
            pTestValFsMplsDsTeTcDescription->i4_Length);

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDsTeTestAllClassTypeTcMapTable (pu4ErrorCode,
                                                   u4FsMplsDsTeClassTypeIndex,
                                                   u4FsMplsDsTeTcIndex,
                                                   &TrafficClassMapEntry,
                                                   MPLS_DSTE_TC_MAP_DESC);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsDsTeTcMapRowStatus
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex
                FsMplsDsTeTcIndex

                The Object 
                testValFsMplsDsTeTcMapRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsDsTeTcMapRowStatus (UINT4 *pu4ErrorCode,
                                   UINT4 u4FsMplsDsTeClassTypeIndex,
                                   UINT4 u4FsMplsDsTeTcIndex,
                                   INT4 i4TestValFsMplsDsTeTcMapRowStatus)
{
    tMplsDsTeClassTypeTcMap TrafficClassMapEntry;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TrafficClassMapEntry, MPLS_ZERO, sizeof (tMplsDsTeClassTypeTcMap));

    TrafficClassMapEntry.i4DsTeClassTypeToTcRowStatus =
        i4TestValFsMplsDsTeTcMapRowStatus;

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDsTeTestAllClassTypeTcMapTable (pu4ErrorCode,
                                                   u4FsMplsDsTeClassTypeIndex,
                                                   u4FsMplsDsTeTcIndex,
                                                   &TrafficClassMapEntry,
                                                   MPLS_DSTE_TC_MAP_ROW_STATUS);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsDsTeClassTypeToTcMapTable
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex
                FsMplsDsTeTcIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsDsTeClassTypeToTcMapTable (UINT4 *pu4ErrorCode,
                                         tSnmpIndexList * pSnmpIndexList,
                                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMplsDsTeTeClassTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsDsTeTeClassTable
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex
                FsMplsDsTeTeClassPriority
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 
     
     
     
     
     
     
     
    nmhValidateIndexInstanceFsMplsDsTeTeClassTable
    (UINT4 u4FsMplsDsTeClassTypeIndex, UINT4 u4FsMplsDsTeTeClassPriority)
{
    if (u4FsMplsDsTeClassTypeIndex >= MPLS_MAX_CLASS_TYPE_ENTRIES)
    {
        return SNMP_FAILURE;
    }
    if (u4FsMplsDsTeTeClassPriority >= MPLS_MAX_PRIORITY)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsDsTeTeClassTable
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex
                FsMplsDsTeTeClassPriority
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 
     
     
     
     
     
     
     
    nmhGetFirstIndexFsMplsDsTeTeClassTable
    (UINT4 *pu4FsMplsDsTeClassTypeIndex, UINT4 *pu4FsMplsDsTeTeClassPriority)
{
    UINT4               u4ClassId = MPLS_ZERO;
    UINT4               u4TeClassPriority = MPLS_ZERO;
    tMplsDsTeClassPrio  TeClassEntry;

    MEMSET (&TeClassEntry, MPLS_ZERO, sizeof (tMplsDsTeClassPrio));

    MPLS_CMN_LOCK ();

    for (u4ClassId = 0; u4ClassId < MPLS_MAX_CLASS_TYPE_ENTRIES; u4ClassId++)
    {
        for (u4TeClassPriority = MPLS_MIN_PRIORITY;
             u4TeClassPriority < MPLS_MAX_PRIORITY; u4TeClassPriority++)
        {
            TeClassEntry = MPLS_DIFFSERV_TE_CLASS_MAP (MPLS_DEF_INCARN,
                                                       u4ClassId,
                                                       u4TeClassPriority);
            if (TeClassEntry.i4DsTeCRowStatus != MPLS_ZERO)
            {
                *pu4FsMplsDsTeClassTypeIndex = u4ClassId;
                *pu4FsMplsDsTeTeClassPriority = u4TeClassPriority;

                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
        }
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMplsDsTeTeClassTable
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex
                nextFsMplsDsTeClassTypeIndex
                FsMplsDsTeTeClassPriority
                nextFsMplsDsTeTeClassPriority
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 
     
     
     
     
     
     
     
    nmhGetNextIndexFsMplsDsTeTeClassTable
    (UINT4 u4FsMplsDsTeClassTypeIndex,
     UINT4 *pu4NextFsMplsDsTeClassTypeIndex,
     UINT4 u4FsMplsDsTeTeClassPriority, UINT4 *pu4NextFsMplsDsTeTeClassPriority)
{
    UINT4               u4ClassId = MPLS_ZERO;
    UINT4               u4TeClassPriority = MPLS_ZERO;
    tMplsDsTeClassPrio  TeClassEntry;

    MEMSET (&TeClassEntry, MPLS_ZERO, sizeof (tMplsDsTeClassPrio));

    MPLS_CMN_LOCK ();

    u4ClassId = u4FsMplsDsTeClassTypeIndex;
    u4TeClassPriority = u4FsMplsDsTeTeClassPriority + 1;

    for (; u4ClassId < MPLS_MAX_CLASS_TYPE_ENTRIES; u4ClassId++)
    {
        for (; u4TeClassPriority < MPLS_MAX_PRIORITY; u4TeClassPriority++)
        {
            TeClassEntry = MPLS_DIFFSERV_TE_CLASS_MAP (MPLS_DEF_INCARN,
                                                       u4ClassId,
                                                       u4TeClassPriority);
            if (TeClassEntry.i4DsTeCRowStatus != MPLS_ZERO)
            {
                *pu4NextFsMplsDsTeClassTypeIndex = u4ClassId;
                *pu4NextFsMplsDsTeTeClassPriority = u4TeClassPriority;

                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
        }

        u4TeClassPriority = 0;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsDsTeTeClassDesc
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex
                FsMplsDsTeTeClassPriority

                The Object 
                retValFsMplsDsTeTeClassDesc
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsDsTeTeClassDesc (UINT4 u4FsMplsDsTeClassTypeIndex,
                             UINT4 u4FsMplsDsTeTeClassPriority,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsMplsDsTeTeClassDesc)
{
    tMplsDsTeClassPrio  TeClassEntry;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TeClassEntry, MPLS_ZERO, sizeof (tMplsDsTeClassPrio));

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDsTeGetAllTeclassMapTable (u4FsMplsDsTeClassTypeIndex,
                                              u4FsMplsDsTeTeClassPriority,
                                              &TeClassEntry);

    MPLS_CMN_UNLOCK ();

    pRetValFsMplsDsTeTeClassDesc->i4_Length
        = STRLEN (TeClassEntry.au1DsTeCDesc);

    MEMCPY (pRetValFsMplsDsTeTeClassDesc->pu1_OctetList,
            TeClassEntry.au1DsTeCDesc, pRetValFsMplsDsTeTeClassDesc->i4_Length);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMplsDsTeTeClassNumber
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex
                FsMplsDsTeTeClassPriority

                The Object 
                retValFsMplsDsTeTeClassNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsDsTeTeClassNumber (UINT4 u4FsMplsDsTeClassTypeIndex,
                               UINT4 u4FsMplsDsTeTeClassPriority,
                               UINT4 *pu4RetValFsMplsDsTeTeClassNumber)
{
    tMplsDsTeClassPrio  TeClassEntry;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TeClassEntry, MPLS_ZERO, sizeof (tMplsDsTeClassPrio));

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDsTeGetAllTeclassMapTable (u4FsMplsDsTeClassTypeIndex,
                                              u4FsMplsDsTeTeClassPriority,
                                              &TeClassEntry);

    *pu4RetValFsMplsDsTeTeClassNumber = TeClassEntry.u4TeClassNumber;

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMplsDsTeTeClassRowStatus
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex
                FsMplsDsTeTeClassPriority

                The Object 
                retValFsMplsDsTeTeClassRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsDsTeTeClassRowStatus (UINT4 u4FsMplsDsTeClassTypeIndex,
                                  UINT4 u4FsMplsDsTeTeClassPriority,
                                  INT4 *pi4RetValFsMplsDsTeTeClassRowStatus)
{
    tMplsDsTeClassPrio  TeClassEntry;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TeClassEntry, MPLS_ZERO, sizeof (tMplsDsTeClassPrio));

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDsTeGetAllTeclassMapTable (u4FsMplsDsTeClassTypeIndex,
                                              u4FsMplsDsTeTeClassPriority,
                                              &TeClassEntry);

    MPLS_CMN_UNLOCK ();

    *pi4RetValFsMplsDsTeTeClassRowStatus = TeClassEntry.i4DsTeCRowStatus;

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMplsDsTeTeClassDesc
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex
                FsMplsDsTeTeClassPriority

                The Object 
                setValFsMplsDsTeTeClassDesc
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsDsTeTeClassDesc (UINT4 u4FsMplsDsTeClassTypeIndex,
                             UINT4 u4FsMplsDsTeTeClassPriority,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsMplsDsTeTeClassDesc)
{
    tMplsDsTeClassPrio  TeClassEntry;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TeClassEntry, MPLS_ZERO, sizeof (tMplsDsTeClassPrio));

    MEMCPY (TeClassEntry.au1DsTeCDesc,
            pSetValFsMplsDsTeTeClassDesc->pu1_OctetList,
            pSetValFsMplsDsTeTeClassDesc->i4_Length);

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDsTeSetAllTeclassMapTable (u4FsMplsDsTeClassTypeIndex,
                                              u4FsMplsDsTeTeClassPriority,
                                              &TeClassEntry,
                                              MPLS_DSTE_TE_CLASS_DESC);
    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMplsDsTeTeClassNumber
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex
                FsMplsDsTeTeClassPriority

                The Object 
                setValFsMplsDsTeTeClassNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsDsTeTeClassNumber (UINT4 u4FsMplsDsTeClassTypeIndex,
                               UINT4 u4FsMplsDsTeTeClassPriority,
                               UINT4 u4SetValFsMplsDsTeTeClassNumber)
{
    tMplsDsTeClassPrio  TeClassEntry;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TeClassEntry, MPLS_ZERO, sizeof (tMplsDsTeClassPrio));

    TeClassEntry.u4TeClassNumber = u4SetValFsMplsDsTeTeClassNumber;

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDsTeSetAllTeclassMapTable (u4FsMplsDsTeClassTypeIndex,
                                              u4FsMplsDsTeTeClassPriority,
                                              &TeClassEntry,
                                              MPLS_DSTE_TE_CLASS_NUMBER);
    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMplsDsTeTeClassRowStatus
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex
                FsMplsDsTeTeClassPriority

                The Object 
                setValFsMplsDsTeTeClassRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsDsTeTeClassRowStatus (UINT4 u4FsMplsDsTeClassTypeIndex,
                                  UINT4 u4FsMplsDsTeTeClassPriority,
                                  INT4 i4SetValFsMplsDsTeTeClassRowStatus)
{
    tMplsDsTeClassPrio  TeClassEntry;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TeClassEntry, MPLS_ZERO, sizeof (tMplsDsTeClassPrio));

    TeClassEntry.i4DsTeCRowStatus = i4SetValFsMplsDsTeTeClassRowStatus;

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDsTeSetAllTeclassMapTable (u4FsMplsDsTeClassTypeIndex,
                                              u4FsMplsDsTeTeClassPriority,
                                              &TeClassEntry,
                                              MPLS_DSTE_TE_CLASS_ROW_STATUS);
    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMplsDsTeTeClassDesc
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex
                FsMplsDsTeTeClassPriority

                The Object 
                testValFsMplsDsTeTeClassDesc
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsDsTeTeClassDesc (UINT4 *pu4ErrorCode,
                                UINT4 u4FsMplsDsTeClassTypeIndex,
                                UINT4 u4FsMplsDsTeTeClassPriority,
                                tSNMP_OCTET_STRING_TYPE
                                * pTestValFsMplsDsTeTeClassDesc)
{
    tMplsDsTeClassPrio  TeClassEntry;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TeClassEntry, MPLS_ZERO, sizeof (tMplsDsTeClassPrio));

    MEMCPY (TeClassEntry.au1DsTeCDesc,
            pTestValFsMplsDsTeTeClassDesc->pu1_OctetList,
            pTestValFsMplsDsTeTeClassDesc->i4_Length);

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDsTeTestAllTeclassMapTable (pu4ErrorCode,
                                               u4FsMplsDsTeClassTypeIndex,
                                               u4FsMplsDsTeTeClassPriority,
                                               &TeClassEntry,
                                               MPLS_DSTE_TE_CLASS_DESC);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsDsTeTeClassNumber
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex
                FsMplsDsTeTeClassPriority

                The Object 
                testValFsMplsDsTeTeClassNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsDsTeTeClassNumber (UINT4 *pu4ErrorCode,
                                  UINT4 u4FsMplsDsTeClassTypeIndex,
                                  UINT4 u4FsMplsDsTeTeClassPriority,
                                  UINT4 u4TestValFsMplsDsTeTeClassNumber)
{
    tMplsDsTeClassPrio  TeClassEntry;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TeClassEntry, MPLS_ZERO, sizeof (tMplsDsTeClassPrio));

    TeClassEntry.u4TeClassNumber = u4TestValFsMplsDsTeTeClassNumber;

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDsTeTestAllTeclassMapTable (pu4ErrorCode,
                                               u4FsMplsDsTeClassTypeIndex,
                                               u4FsMplsDsTeTeClassPriority,
                                               &TeClassEntry,
                                               MPLS_DSTE_TE_CLASS_NUMBER);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsDsTeTeClassRowStatus
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex
                FsMplsDsTeTeClassPriority

                The Object 
                testValFsMplsDsTeTeClassRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsDsTeTeClassRowStatus (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsMplsDsTeClassTypeIndex,
                                     UINT4 u4FsMplsDsTeTeClassPriority,
                                     INT4 i4TestValFsMplsDsTeTeClassRowStatus)
{
    tMplsDsTeClassPrio  TeClassEntry;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TeClassEntry, MPLS_ZERO, sizeof (tMplsDsTeClassPrio));

    TeClassEntry.i4DsTeCRowStatus = i4TestValFsMplsDsTeTeClassRowStatus;

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDsTeTestAllTeclassMapTable (pu4ErrorCode,
                                               u4FsMplsDsTeClassTypeIndex,
                                               u4FsMplsDsTeTeClassPriority,
                                               &TeClassEntry,
                                               MPLS_DSTE_TE_CLASS_ROW_STATUS);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsDsTeTeClassTable
 Input       :  The Indices
                FsMplsDsTeClassTypeIndex
                FsMplsDsTeTeClassPriority
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsDsTeTeClassTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsCrlspDebugLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsCrlspDebugLevel (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsTunnelCRLDPResTable
 Input       :  The Indices
                MplsTunnelResourceIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsTunnelCRLDPResTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsDiffServElspInfoTable
 Input       :  The Indices
                FsMplsDiffServElspInfoListIndex
                FsMplsDiffServElspInfoIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsDiffServElspInfoTable (UINT4 *pu4ErrorCode,
                                     tSnmpIndexList * pSnmpIndexList,
                                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRouterID
 Input       :  The Indices

                The Object
                retValFsMplsRouterID
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRouterID (tSNMP_OCTET_STRING_TYPE * pRetValFsMplsRouterID)
{
    UINT1               au1IpAddr[ROUTER_ID_LENGTH];

    MEMSET (au1IpAddr, MPLS_ZERO, ROUTER_ID_LENGTH);

    if (MEMCMP (MPLS_ROUTER_ID (MPLS_DEF_INCARN).au1Ipv4Addr,
                au1IpAddr, ROUTER_ID_LENGTH) == MPLS_ZERO)
    {
        /* Since this is a temporary object where LDP LSR ID is stored until
         * interface of active LSR ID goes down, it will be zero after that
         * and the length will be zero. */
        pRetValFsMplsRouterID->i4_Length = MPLS_ZERO;
        return SNMP_SUCCESS;
    }

    MEMCPY (pRetValFsMplsRouterID->pu1_OctetList,
            MPLS_ROUTER_ID (MPLS_DEF_INCARN).au1Ipv4Addr, ROUTER_ID_LENGTH);
    pRetValFsMplsRouterID->i4_Length = ROUTER_ID_LENGTH;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRouterID
 Input       :  The Indices

                The Object
                setValFsMplsRouterID
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRouterID (tSNMP_OCTET_STRING_TYPE * pSetValFsMplsRouterID)
{
    UINT4               u4IpAddress = MPLS_ZERO;

    if (pSetValFsMplsRouterID->i4_Length != MPLS_ZERO)
    {
        MEMCPY ((UINT1 *) &u4IpAddress,
                pSetValFsMplsRouterID->pu1_OctetList, ROUTER_ID_LENGTH);
        u4IpAddress = OSIX_NTOHL (u4IpAddress);
    }
    else
    {
        /* This is to tackle the no form of the CLI Command 'router-id'.
         * If the length is zero, we need to choose the Highest Ip Address
         * available on the system. */
        NetIpv4GetHighestIpAddr (&u4IpAddress);
    }
    if (MEMCMP ((UINT1 *) &u4IpAddress,
                MPLS_ROUTER_ID (MPLS_DEF_INCARN).au1Ipv4Addr,
                ROUTER_ID_LENGTH) == 0)
    {
        /* New LSR id and the current LSR id sare same,
         * so skip the LSR id change*/
        return SNMP_SUCCESS;
    }

    u4IpAddress = OSIX_HTONL (u4IpAddress);
    MEMCPY (MPLS_ROUTER_ID (MPLS_DEF_INCARN).au1Ipv4Addr,
            (UINT1 *) &u4IpAddress, ROUTER_ID_LENGTH);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRouterID
 Input       :  The Indices

                The Object
                testValFsMplsRouterID
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRouterID (UINT4 *pu4ErrorCode,
                         tSNMP_OCTET_STRING_TYPE * pTestValFsMplsRouterID)
{
    UINT4               u4IpAddress = MPLS_ZERO;
    tRtInfoQueryMsg     RtQuery;
    tNetIpv4RtInfo      NetIpRtInfo;
    UINT4               u4Mask = 0xffffffff;

    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

    MEMCPY ((UINT1 *) &u4IpAddress, pTestValFsMplsRouterID->pu1_OctetList,
            ROUTER_ID_LENGTH);

    u4IpAddress = OSIX_NTOHL (u4IpAddress);

    /* Ldp LSR Identifier to be set must be present in any of the local
     * interface */
    if (NetIpv4IfIsOurAddress (u4IpAddress) == NETIPV4_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    RtQuery.u4DestinationIpAddress = u4IpAddress;
    RtQuery.u4DestinationSubnetMask = u4Mask;
    RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;

    /* Now, Validating whether the Interface of the IP Address to be
     * configured as Router ID is UP or DOWN. If it is down,
     * NetIpv4GetRoute will return FAILURE. */
    if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if (NetIpRtInfo.u2RtProto != CIDR_LOCAL_ID)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRouterID
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRouterID (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsSimulateFailure
 Input       :  The Indices

                The Object 
                retValFsMplsSimulateFailure
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsSimulateFailure (INT4 *pi4RetValFsMplsSimulateFailure)
{
    *pi4RetValFsMplsSimulateFailure = gi4MplsSimulateFailure;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMplsSimulateFailure
 Input       :  The Indices

                The Object 
                setValFsMplsSimulateFailure
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsSimulateFailure (INT4 i4SetValFsMplsSimulateFailure)
{
    gi4MplsSimulateFailure = i4SetValFsMplsSimulateFailure;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMplsSimulateFailure
 Input       :  The Indices

                The Object 
                testValFsMplsSimulateFailure
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsSimulateFailure (UINT4 *pu4ErrorCode,
                                INT4 i4TestValFsMplsSimulateFailure)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4TestValFsMplsSimulateFailure);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsSimulateFailure
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsSimulateFailure (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsRsvpTeGrMaxWaitTime
 Input       :  The Indices

                The Object 
                retValFsMplsRsvpTeGrMaxWaitTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsRsvpTeGrMaxWaitTime (INT4 *pi4RetValFsMplsRsvpTeGrMaxWaitTime)
{
    *pi4RetValFsMplsRsvpTeGrMaxWaitTime = (INT4)
        MPLS_RPTE_GR_MAX_WAIT_TIME (MPLS_DEF_INCARN);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsRsvpTeGrMaxWaitTime
 Input       :  The Indices

                The Object 
                setValFsMplsRsvpTeGrMaxWaitTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsRsvpTeGrMaxWaitTime (INT4 i4SetValFsMplsRsvpTeGrMaxWaitTime)
{
    MPLS_RPTE_GR_MAX_WAIT_TIME (MPLS_DEF_INCARN) =
        (UINT2) i4SetValFsMplsRsvpTeGrMaxWaitTime;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsRsvpTeGrMaxWaitTime
 Input       :  The Indices

                The Object 
                testValFsMplsRsvpTeGrMaxWaitTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsRsvpTeGrMaxWaitTime (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValFsMplsRsvpTeGrMaxWaitTime)
{
    if ((i4TestValFsMplsRsvpTeGrMaxWaitTime < MPLS_MIN_GR_MAX_WAIT_TIME) ||
        (i4TestValFsMplsRsvpTeGrMaxWaitTime > MPLS_MAX_GR_MAX_WAIT_TIME))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsRsvpTeGrMaxWaitTime
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsRsvpTeGrMaxWaitTime (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLdpGrMaxWaitTime
 Input       :  The Indices

                The Object 
                retValFsMplsLdpGrMaxWaitTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLdpGrMaxWaitTime (INT4 *pi4RetValFsMplsLdpGrMaxWaitTime)
{
	/* deprecated */
    INT4                i4DefaultLdpGrMaxWaitTimer = 600;
    *pi4RetValFsMplsLdpGrMaxWaitTime = i4DefaultLdpGrMaxWaitTimer;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsLdpGrMaxWaitTime
 Input       :  The Indices

                The Object 
                setValFsMplsLdpGrMaxWaitTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsLdpGrMaxWaitTime (INT4 i4SetValFsMplsLdpGrMaxWaitTime)
{
	/* deprecated */
	UNUSED_PARAM(i4SetValFsMplsLdpGrMaxWaitTime);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsLdpGrMaxWaitTime
 Input       :  The Indices

                The Object 
                testValFsMplsLdpGrMaxWaitTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsLdpGrMaxWaitTime (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsMplsLdpGrMaxWaitTime)
{
	/* deprecated */
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(i4TestValFsMplsLdpGrMaxWaitTime);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhDepv2FsMplsLdpGrMaxWaitTime
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsLdpGrMaxWaitTime (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

#ifdef MPLS_L3VPN_WANTED
/* LOW LEVEL Routines for Table : FsMplsL3VpnVrfEgressRteTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsL3VpnVrfEgressRteTable
 Input       :  The Indices
                FsMplsL3VpnVrfEgressRtePathAttrLabel
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsMplsL3VpnVrfEgressRteTable(UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel)
{
        UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
        return SNMP_SUCCESS;
}
/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsL3VpnVrfEgressRteTable
 Input       :  The Indices
                FsMplsL3VpnVrfEgressRtePathAttrLabel
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsMplsL3VpnVrfEgressRteTable(UINT4 *pu4FsMplsL3VpnVrfEgressRtePathAttrLabel)
{
        tL3VpnBgpRouteLabelEntry *pL3VpnBgpRouteLabelEntry = NULL;
        tL3VpnBgpRouteLabelEntry L3VpnBgpRouteLabelEntry;

        MEMSET(&L3VpnBgpRouteLabelEntry,0,sizeof(tL3VpnBgpRouteLabelEntry));

        pL3VpnBgpRouteLabelEntry=L3vpnGetNextBgpRouteLabelTable(&L3VpnBgpRouteLabelEntry);
        if( pL3VpnBgpRouteLabelEntry==NULL)
        {
                return SNMP_FAILURE;
        }

        *pu4FsMplsL3VpnVrfEgressRtePathAttrLabel=L3VPN_P_BGPROUTELABEL_LABEL(pL3VpnBgpRouteLabelEntry);

        return SNMP_SUCCESS;
}
/****************************************************************************
Function    :  nmhGetNextIndexFsMplsL3VpnVrfEgressRteTable
Input       :  The Indices
FsMplsL3VpnVrfEgressRtePathAttrLabel
nextFsMplsL3VpnVrfEgressRtePathAttrLabel
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsMplsL3VpnVrfEgressRteTable(UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel ,UINT4 *pu4NextFsMplsL3VpnVrfEgressRtePathAttrLabel )
{
        tL3VpnBgpRouteLabelEntry *pL3VpnBgpRouteLabelEntry = NULL;
        tL3VpnBgpRouteLabelEntry L3VpnBgpRouteLabelEntry;

        MEMSET(&L3VpnBgpRouteLabelEntry,0,sizeof(tL3VpnBgpRouteLabelEntry));

        L3VPN_BGPROUTELABEL_LABEL(L3VpnBgpRouteLabelEntry)=u4FsMplsL3VpnVrfEgressRtePathAttrLabel;

        pL3VpnBgpRouteLabelEntry=L3vpnGetNextBgpRouteLabelTable(&L3VpnBgpRouteLabelEntry);
        if( pL3VpnBgpRouteLabelEntry==NULL)
        {
                return SNMP_FAILURE;
        }

        *pu4NextFsMplsL3VpnVrfEgressRtePathAttrLabel=L3VPN_P_BGPROUTELABEL_LABEL(pL3VpnBgpRouteLabelEntry);

        return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetFsMplsL3VpnVrfName
Input       :  The Indices
FsMplsL3VpnVrfEgressRtePathAttrLabel

The Object
retValFsMplsL3VpnVrfName
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhGetFsMplsL3VpnVrfName(UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel , tSNMP_OCTET_STRING_TYPE * pRetValFsMplsL3VpnVrfName)
{
        tL3VpnBgpRouteLabelEntry *pL3VpnBgpRouteLabelEntry = NULL;
        tL3VpnBgpRouteLabelEntry L3VpnBgpRouteLabelEntry;
        UINT4 u4VrfId = 0;
/*        UINT1 u1VrfName[L3VPN_MAX_VRF_NAME_LEN];*/

        MEMSET(&L3VpnBgpRouteLabelEntry,0,sizeof(tL3VpnBgpRouteLabelEntry));

        L3VPN_BGPROUTELABEL_LABEL(L3VpnBgpRouteLabelEntry)=u4FsMplsL3VpnVrfEgressRtePathAttrLabel;


        pL3VpnBgpRouteLabelEntry=L3vpnGetBgpRouteLabelTable(&L3VpnBgpRouteLabelEntry);
        if( pL3VpnBgpRouteLabelEntry==NULL)
        {
                return SNMP_FAILURE;
        }
        if(L3VPN_P_BGPROUTELABEL_POLICY(pL3VpnBgpRouteLabelEntry)==L3VPN_BGP4_POLICY_PER_VRF)
        {
                u4VrfId=L3VPN_P_BGPROUTELABELVRF_VRFID (pL3VpnBgpRouteLabelEntry);

                if(VcmGetAliasName(u4VrfId,pRetValFsMplsL3VpnVrfName->pu1_OctetList)
                                != VCM_SUCCESS)
                {
                        return SNMP_FAILURE;
                }
#if 0
                pRetValFsMplsL3VpnVrfName->i4_Length = (INT4)STRLEN(u1VrfName);
                MEMCPY(pRetValFsMplsL3VpnVrfName->pu1_OctetList,u1VrfName,
                                pRetValFsMplsL3VpnVrfName->i4_Length);
#endif
                pRetValFsMplsL3VpnVrfName->i4_Length 
			= (INT4)STRLEN(L3VPN_P_BGPROUTELABELVRF_VNAME(pL3VpnBgpRouteLabelEntry));
                MEMCPY(pRetValFsMplsL3VpnVrfName->pu1_OctetList, L3VPN_P_BGPROUTELABELVRF_VNAME(pL3VpnBgpRouteLabelEntry),
                                pRetValFsMplsL3VpnVrfName->i4_Length);
        }

        return SNMP_SUCCESS;
}
/****************************************************************************
Function    :  nmhGetFsMplsL3VpnVrfEgressRteInetCidrDestType
Input       :  The Indices
FsMplsL3VpnVrfEgressRtePathAttrLabel

The Object
retValFsMplsL3VpnVrfEgressRteInetCidrDestType
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhGetFsMplsL3VpnVrfEgressRteInetCidrDestType(UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel , INT4 *pi4RetValFsMplsL3VpnVrfEgressRteInetCidrDestType)
{
        UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
        UNUSED_PARAM(pi4RetValFsMplsL3VpnVrfEgressRteInetCidrDestType);
        return SNMP_SUCCESS;
}
/****************************************************************************
Function    :  nmhGetFsMplsL3VpnVrfEgressRteInetCidrDest
Input       :  The Indices
FsMplsL3VpnVrfEgressRtePathAttrLabel

The Object
retValFsMplsL3VpnVrfEgressRteInetCidrDest
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhGetFsMplsL3VpnVrfEgressRteInetCidrDest(UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel , tSNMP_OCTET_STRING_TYPE * pRetValFsMplsL3VpnVrfEgressRteInetCidrDest)
{
        UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
        UNUSED_PARAM(pRetValFsMplsL3VpnVrfEgressRteInetCidrDest);
        return SNMP_SUCCESS;
}
/****************************************************************************
Function    :  nmhGetFsMplsL3VpnVrfEgressRteInetCidrPfxLen
Input       :  The Indices
FsMplsL3VpnVrfEgressRtePathAttrLabel

The Object
retValFsMplsL3VpnVrfEgressRteInetCidrPfxLen
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhGetFsMplsL3VpnVrfEgressRteInetCidrPfxLen(UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel , UINT4 *pu4RetValFsMplsL3VpnVrfEgressRteInetCidrPfxLen)
{
        UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
        UNUSED_PARAM(pu4RetValFsMplsL3VpnVrfEgressRteInetCidrPfxLen);
        return SNMP_SUCCESS;
}
/****************************************************************************
Function    :  nmhGetFsMplsL3VpnVrfEgressRteInetCidrLabelPolicy
Input       :  The Indices
FsMplsL3VpnVrfEgressRtePathAttrLabel

The Object
retValFsMplsL3VpnVrfEgressRteInetCidrLabelPolicy
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhGetFsMplsL3VpnVrfEgressRteInetCidrLabelPolicy(UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel , INT4 *pi4RetValFsMplsL3VpnVrfEgressRteInetCidrLabelPolicy)
{
        tL3VpnBgpRouteLabelEntry *pL3VpnBgpRouteLabelEntry = NULL;
        tL3VpnBgpRouteLabelEntry L3VpnBgpRouteLabelEntry;

        MEMSET(&L3VpnBgpRouteLabelEntry,0,sizeof(tL3VpnBgpRouteLabelEntry));

        L3VPN_BGPROUTELABEL_LABEL(L3VpnBgpRouteLabelEntry)=u4FsMplsL3VpnVrfEgressRtePathAttrLabel;


        pL3VpnBgpRouteLabelEntry=L3vpnGetBgpRouteLabelTable(&L3VpnBgpRouteLabelEntry);
        if( pL3VpnBgpRouteLabelEntry==NULL)
        {
                return SNMP_FAILURE;
        }
        *pi4RetValFsMplsL3VpnVrfEgressRteInetCidrLabelPolicy=
                L3VPN_P_BGPROUTELABEL_POLICY(pL3VpnBgpRouteLabelEntry);

        return SNMP_SUCCESS;
}
/****************************************************************************
Function    :  nmhGetFsMplsL3VpnVrfEgressRteInetCidrNHopType
Input       :  The Indices
FsMplsL3VpnVrfEgressRtePathAttrLabel

The Object
retValFsMplsL3VpnVrfEgressRteInetCidrNHopType
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhGetFsMplsL3VpnVrfEgressRteInetCidrNHopType(UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel , INT4 *pi4RetValFsMplsL3VpnVrfEgressRteInetCidrNHopType)
{
        UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
        UNUSED_PARAM(pi4RetValFsMplsL3VpnVrfEgressRteInetCidrNHopType);
        return SNMP_SUCCESS;
}
/****************************************************************************
Function    :  nmhGetFsMplsL3VpnVrfEgressRteInetCidrNextHop
Input       :  The Indices
FsMplsL3VpnVrfEgressRtePathAttrLabel

The Object
retValFsMplsL3VpnVrfEgressRteInetCidrNextHop
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhGetFsMplsL3VpnVrfEgressRteInetCidrNextHop(UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel , tSNMP_OCTET_STRING_TYPE * pRetValFsMplsL3VpnVrfEgressRteInetCidrNextHop)
{
        UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
        UNUSED_PARAM(pRetValFsMplsL3VpnVrfEgressRteInetCidrNextHop);
        return SNMP_SUCCESS;
}
/****************************************************************************
Function    :  nmhGetFsMplsL3VpnVrfEgressRteInetCidrIfIndex
Input       :  The Indices
FsMplsL3VpnVrfEgressRtePathAttrLabel

The Object
retValFsMplsL3VpnVrfEgressRteInetCidrIfIndex
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhGetFsMplsL3VpnVrfEgressRteInetCidrIfIndex(UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel , INT4 *pi4RetValFsMplsL3VpnVrfEgressRteInetCidrIfIndex)
{
        UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
        UNUSED_PARAM(pi4RetValFsMplsL3VpnVrfEgressRteInetCidrIfIndex);
        return SNMP_SUCCESS;
}
/****************************************************************************
Function    :  nmhGetFsMplsL3VpnVrfEgressRteInetCidrMetric1
Input       :  The Indices
FsMplsL3VpnVrfEgressRtePathAttrLabel

The Object
retValFsMplsL3VpnVrfEgressRteInetCidrMetric1
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhGetFsMplsL3VpnVrfEgressRteInetCidrMetric1(UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel , INT4 *pi4RetValFsMplsL3VpnVrfEgressRteInetCidrMetric1)
{
        UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
        UNUSED_PARAM(pi4RetValFsMplsL3VpnVrfEgressRteInetCidrMetric1);
        return SNMP_SUCCESS;
}
/****************************************************************************
Function    :  nmhGetFsMplsL3VpnVrfEgressRteInetCidrStatus
Input       :  The Indices
FsMplsL3VpnVrfEgressRtePathAttrLabel

The Object
retValFsMplsL3VpnVrfEgressRteInetCidrStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhGetFsMplsL3VpnVrfEgressRteInetCidrStatus(UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel , INT4 *pi4RetValFsMplsL3VpnVrfEgressRteInetCidrStatus)
{

        tL3VpnBgpRouteLabelEntry *pL3VpnBgpRouteLabelEntry = NULL;
        tL3VpnBgpRouteLabelEntry L3VpnBgpRouteLabelEntry;

        MEMSET(&L3VpnBgpRouteLabelEntry,0,sizeof(tL3VpnBgpRouteLabelEntry));

        L3VPN_BGPROUTELABEL_LABEL(L3VpnBgpRouteLabelEntry)=u4FsMplsL3VpnVrfEgressRtePathAttrLabel;


        pL3VpnBgpRouteLabelEntry=L3vpnGetBgpRouteLabelTable(&L3VpnBgpRouteLabelEntry);
        if( pL3VpnBgpRouteLabelEntry==NULL)
        {
                return SNMP_FAILURE;
        }
        *pi4RetValFsMplsL3VpnVrfEgressRteInetCidrStatus= (INT4)L3VPN_BGPROUTEROW_STATUS(L3VpnBgpRouteLabelEntry);

        return SNMP_SUCCESS;
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetFsMplsL3VpnVrfName
Input       :  The Indices
FsMplsL3VpnVrfEgressRtePathAttrLabel

The Object
setValFsMplsL3VpnVrfName
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhSetFsMplsL3VpnVrfName(UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel , tSNMP_OCTET_STRING_TYPE *pSetValFsMplsL3VpnVrfName)
{
        tL3VpnBgpRouteLabelEntry *pL3VpnBgpRouteLabelEntry = NULL;
        tL3VpnBgpRouteLabelEntry L3VpnBgpRouteLabelEntry;
        UINT4 u4VrfId = 0;

        MEMSET(&L3VpnBgpRouteLabelEntry,0,sizeof(tL3VpnBgpRouteLabelEntry));

        L3VPN_BGPROUTELABEL_LABEL(L3VpnBgpRouteLabelEntry)=u4FsMplsL3VpnVrfEgressRtePathAttrLabel;


        pL3VpnBgpRouteLabelEntry=L3vpnGetBgpRouteLabelTable(&L3VpnBgpRouteLabelEntry);
        if( pL3VpnBgpRouteLabelEntry==NULL)
        {
                return SNMP_FAILURE;
        }
        if(L3VPN_P_BGPROUTELABEL_POLICY(pL3VpnBgpRouteLabelEntry)==L3VPN_BGP4_POLICY_PER_VRF)
        {
                MEMCPY(L3VPN_P_BGPROUTELABELVRF_VNAME(pL3VpnBgpRouteLabelEntry),
                                pSetValFsMplsL3VpnVrfName->pu1_OctetList,
                                pSetValFsMplsL3VpnVrfName->i4_Length);

                if (VcmIsVrfExist (pSetValFsMplsL3VpnVrfName->pu1_OctetList,&u4VrfId) ==VCM_FALSE)
                {
                        return SNMP_FAILURE;

                }
                L3VPN_P_BGPROUTELABELVRF_VRFID(pL3VpnBgpRouteLabelEntry)=u4VrfId;

        }
        return SNMP_SUCCESS;
}
/****************************************************************************
Function    :  nmhSetFsMplsL3VpnVrfEgressRteInetCidrDestType
Input       :  The Indices
FsMplsL3VpnVrfEgressRtePathAttrLabel

The Object
setValFsMplsL3VpnVrfEgressRteInetCidrDestType
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhSetFsMplsL3VpnVrfEgressRteInetCidrDestType(UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel , INT4 i4SetValFsMplsL3VpnVrfEgressRteInetCidrDestType)
{
        UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
        UNUSED_PARAM(i4SetValFsMplsL3VpnVrfEgressRteInetCidrDestType);
        return SNMP_SUCCESS;
}
/****************************************************************************
Function    :  nmhSetFsMplsL3VpnVrfEgressRteInetCidrDest
Input       :  The Indices
FsMplsL3VpnVrfEgressRtePathAttrLabel

The Object
setValFsMplsL3VpnVrfEgressRteInetCidrDest
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhSetFsMplsL3VpnVrfEgressRteInetCidrDest(UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel , tSNMP_OCTET_STRING_TYPE *pSetValFsMplsL3VpnVrfEgressRteInetCidrDest)
{
        UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
        UNUSED_PARAM(pSetValFsMplsL3VpnVrfEgressRteInetCidrDest);
        return SNMP_SUCCESS;
}
/****************************************************************************
Function    :  nmhSetFsMplsL3VpnVrfEgressRteInetCidrPfxLen
Input       :  The Indices
FsMplsL3VpnVrfEgressRtePathAttrLabel

The Object
setValFsMplsL3VpnVrfEgressRteInetCidrPfxLen
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhSetFsMplsL3VpnVrfEgressRteInetCidrPfxLen(UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel , UINT4 u4SetValFsMplsL3VpnVrfEgressRteInetCidrPfxLen)
{
        UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
        UNUSED_PARAM(u4SetValFsMplsL3VpnVrfEgressRteInetCidrPfxLen);
        return SNMP_SUCCESS;
}
/****************************************************************************
Function    :  nmhSetFsMplsL3VpnVrfEgressRteInetCidrLabelPolicy
Input       :  The Indices
FsMplsL3VpnVrfEgressRtePathAttrLabel

The Object
setValFsMplsL3VpnVrfEgressRteInetCidrLabelPolicy
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhSetFsMplsL3VpnVrfEgressRteInetCidrLabelPolicy(UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel , INT4 i4SetValFsMplsL3VpnVrfEgressRteInetCidrLabelPolicy)
{

        tL3VpnBgpRouteLabelEntry *pL3VpnBgpRouteLabelEntry = NULL;
        tL3VpnBgpRouteLabelEntry L3VpnBgpRouteLabelEntry;

        MEMSET(&L3VpnBgpRouteLabelEntry,0,sizeof(tL3VpnBgpRouteLabelEntry));

        L3VPN_BGPROUTELABEL_LABEL(L3VpnBgpRouteLabelEntry)=u4FsMplsL3VpnVrfEgressRtePathAttrLabel;

        pL3VpnBgpRouteLabelEntry=L3vpnGetBgpRouteLabelTable(&L3VpnBgpRouteLabelEntry);
        if( pL3VpnBgpRouteLabelEntry==NULL)
        {
                return SNMP_FAILURE;
        }
        L3VPN_P_BGPROUTELABEL_POLICY(pL3VpnBgpRouteLabelEntry)=
                (UINT1)i4SetValFsMplsL3VpnVrfEgressRteInetCidrLabelPolicy;

        return SNMP_SUCCESS;
}
/****************************************************************************
Function    :  nmhSetFsMplsL3VpnVrfEgressRteInetCidrNHopType
Input       :  The Indices
FsMplsL3VpnVrfEgressRtePathAttrLabel

The Object
setValFsMplsL3VpnVrfEgressRteInetCidrNHopType
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhSetFsMplsL3VpnVrfEgressRteInetCidrNHopType(UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel , INT4 i4SetValFsMplsL3VpnVrfEgressRteInetCidrNHopType)
{
        UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
        UNUSED_PARAM(i4SetValFsMplsL3VpnVrfEgressRteInetCidrNHopType);
        return SNMP_SUCCESS;
}
/****************************************************************************
Function    :  nmhSetFsMplsL3VpnVrfEgressRteInetCidrNextHop
Input       :  The Indices
FsMplsL3VpnVrfEgressRtePathAttrLabel

The Object
setValFsMplsL3VpnVrfEgressRteInetCidrNextHop
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhSetFsMplsL3VpnVrfEgressRteInetCidrNextHop(UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel , tSNMP_OCTET_STRING_TYPE *pSetValFsMplsL3VpnVrfEgressRteInetCidrNextHop)
{
        UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
        UNUSED_PARAM(pSetValFsMplsL3VpnVrfEgressRteInetCidrNextHop);
        return SNMP_SUCCESS;
}
/****************************************************************************
Function    :  nmhSetFsMplsL3VpnVrfEgressRteInetCidrIfIndex
Input       :  The Indices
FsMplsL3VpnVrfEgressRtePathAttrLabel

The Object
setValFsMplsL3VpnVrfEgressRteInetCidrIfIndex
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhSetFsMplsL3VpnVrfEgressRteInetCidrIfIndex(UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel , INT4 i4SetValFsMplsL3VpnVrfEgressRteInetCidrIfIndex)
{
        UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
        UNUSED_PARAM(i4SetValFsMplsL3VpnVrfEgressRteInetCidrIfIndex);
        return SNMP_SUCCESS;
}
/****************************************************************************
Function    :  nmhSetFsMplsL3VpnVrfEgressRteInetCidrMetric1
Input       :  The Indices
FsMplsL3VpnVrfEgressRtePathAttrLabel

The Object
setValFsMplsL3VpnVrfEgressRteInetCidrMetric1
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhSetFsMplsL3VpnVrfEgressRteInetCidrMetric1(UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel , INT4 i4SetValFsMplsL3VpnVrfEgressRteInetCidrMetric1)
{
        UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
        UNUSED_PARAM(i4SetValFsMplsL3VpnVrfEgressRteInetCidrMetric1);
        return SNMP_SUCCESS;
}
/****************************************************************************
Function    :  nmhSetFsMplsL3VpnVrfEgressRteInetCidrStatus
Input       :  The Indices
FsMplsL3VpnVrfEgressRtePathAttrLabel

The Object
setValFsMplsL3VpnVrfEgressRteInetCidrStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhSetFsMplsL3VpnVrfEgressRteInetCidrStatus(UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel , INT4 i4SetValFsMplsL3VpnVrfEgressRteInetCidrStatus)
{
        if(L3vpnUtlEgressRteTableRowStatusHdl(u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
                                i4SetValFsMplsL3VpnVrfEgressRteInetCidrStatus)!=OSIX_SUCCESS)
        {
                return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2FsMplsL3VpnVrfName
Input       :  The Indices
FsMplsL3VpnVrfEgressRtePathAttrLabel

The Object
testValFsMplsL3VpnVrfName
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhTestv2FsMplsL3VpnVrfName(UINT4 *pu4ErrorCode , UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel , tSNMP_OCTET_STRING_TYPE *pTestValFsMplsL3VpnVrfName)
{
        tL3VpnBgpRouteLabelEntry *pL3VpnBgpRouteLabelEntry = NULL;
        tL3VpnBgpRouteLabelEntry L3VpnBgpRouteLabelEntry;
        UINT4                    u4VrfId=0;

        MEMSET(&L3VpnBgpRouteLabelEntry,0,sizeof(tL3VpnBgpRouteLabelEntry));

        L3VPN_BGPROUTELABEL_LABEL(L3VpnBgpRouteLabelEntry)=u4FsMplsL3VpnVrfEgressRtePathAttrLabel;


        pL3VpnBgpRouteLabelEntry=L3vpnGetBgpRouteLabelTable(&L3VpnBgpRouteLabelEntry);
        if( pL3VpnBgpRouteLabelEntry==NULL)
        {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
        }

        if (pTestValFsMplsL3VpnVrfName->i4_Length >
                        L3VPN_MAX_VRF_NAME_LEN)
        {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
        }
        if(pL3VpnBgpRouteLabelEntry->u4RowStatus==ACTIVE)
        {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
        }

        if (VcmIsVrfExist(pTestValFsMplsL3VpnVrfName->pu1_OctetList,&u4VrfId) ==VCM_FALSE)
        {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
}
/****************************************************************************
Function    :  nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrDestType
Input       :  The Indices
FsMplsL3VpnVrfEgressRtePathAttrLabel

The Object
testValFsMplsL3VpnVrfEgressRteInetCidrDestType
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrDestType(UINT4 *pu4ErrorCode , UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel , INT4 i4TestValFsMplsL3VpnVrfEgressRteInetCidrDestType)
{
        UNUSED_PARAM(pu4ErrorCode);
        UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
        UNUSED_PARAM(i4TestValFsMplsL3VpnVrfEgressRteInetCidrDestType);
        return SNMP_SUCCESS;
}
/****************************************************************************
Function    :  nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrDest
Input       :  The Indices
FsMplsL3VpnVrfEgressRtePathAttrLabel

The Object
testValFsMplsL3VpnVrfEgressRteInetCidrDest
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrDest(UINT4 *pu4ErrorCode , UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel , tSNMP_OCTET_STRING_TYPE *pTestValFsMplsL3VpnVrfEgressRteInetCidrDest)
{
        UNUSED_PARAM(pu4ErrorCode);
        UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
        UNUSED_PARAM(pTestValFsMplsL3VpnVrfEgressRteInetCidrDest);
        return SNMP_SUCCESS;
}
/****************************************************************************
Function    :  nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrPfxLen
Input       :  The Indices
FsMplsL3VpnVrfEgressRtePathAttrLabel

The Object
testValFsMplsL3VpnVrfEgressRteInetCidrPfxLen
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrPfxLen(UINT4 *pu4ErrorCode , UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel , UINT4 u4TestValFsMplsL3VpnVrfEgressRteInetCidrPfxLen)
{
        UNUSED_PARAM(pu4ErrorCode);
        UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
        UNUSED_PARAM(u4TestValFsMplsL3VpnVrfEgressRteInetCidrPfxLen);
        return SNMP_SUCCESS;
}
/****************************************************************************
Function    :  nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrLabelPolicy
Input       :  The Indices
FsMplsL3VpnVrfEgressRtePathAttrLabel

The Object
testValFsMplsL3VpnVrfEgressRteInetCidrLabelPolicy
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrLabelPolicy(UINT4 *pu4ErrorCode , UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel , INT4 i4TestValFsMplsL3VpnVrfEgressRteInetCidrLabelPolicy)
{

        tL3VpnBgpRouteLabelEntry *pL3VpnBgpRouteLabelEntry = NULL;
        tL3VpnBgpRouteLabelEntry L3VpnBgpRouteLabelEntry;

        MEMSET(&L3VpnBgpRouteLabelEntry,0,sizeof(tL3VpnBgpRouteLabelEntry));

        L3VPN_BGPROUTELABEL_LABEL(L3VpnBgpRouteLabelEntry)=u4FsMplsL3VpnVrfEgressRtePathAttrLabel;


        pL3VpnBgpRouteLabelEntry=L3vpnGetBgpRouteLabelTable(&L3VpnBgpRouteLabelEntry);
        if( pL3VpnBgpRouteLabelEntry==NULL)
        {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
        }

        if(pL3VpnBgpRouteLabelEntry->u4RowStatus==ACTIVE)
        {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
        }

        if((i4TestValFsMplsL3VpnVrfEgressRteInetCidrLabelPolicy != L3VPN_BGP4_POLICY_PER_VRF)
                        && (i4TestValFsMplsL3VpnVrfEgressRteInetCidrLabelPolicy != L3VPN_BGP4_POLICY_PER_ROUTE ))
        {
                return SNMP_FAILURE;
        }

        return SNMP_SUCCESS;

}
/****************************************************************************
Function    :  nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrNHopType
Input       :  The Indices
FsMplsL3VpnVrfEgressRtePathAttrLabel

The Object
testValFsMplsL3VpnVrfEgressRteInetCidrNHopType
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrNHopType(UINT4 *pu4ErrorCode , UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel , INT4 i4TestValFsMplsL3VpnVrfEgressRteInetCidrNHopType)
{
        UNUSED_PARAM(pu4ErrorCode);
        UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
        UNUSED_PARAM(i4TestValFsMplsL3VpnVrfEgressRteInetCidrNHopType);
        return SNMP_SUCCESS;
}
/****************************************************************************
Function    :  nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrNextHop
Input       :  The Indices
FsMplsL3VpnVrfEgressRtePathAttrLabel

The Object
testValFsMplsL3VpnVrfEgressRteInetCidrNextHop
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrNextHop(UINT4 *pu4ErrorCode , UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel , tSNMP_OCTET_STRING_TYPE *pTestValFsMplsL3VpnVrfEgressRteInetCidrNextHop)
{
        UNUSED_PARAM(pu4ErrorCode);
        UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
        UNUSED_PARAM(pTestValFsMplsL3VpnVrfEgressRteInetCidrNextHop);
        return SNMP_SUCCESS;
}
/****************************************************************************
Function    :  nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrIfIndex
Input       :  The Indices
FsMplsL3VpnVrfEgressRtePathAttrLabel

The Object
testValFsMplsL3VpnVrfEgressRteInetCidrIfIndex
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrIfIndex(UINT4 *pu4ErrorCode , UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel , INT4 i4TestValFsMplsL3VpnVrfEgressRteInetCidrIfIndex)
{
        UNUSED_PARAM(pu4ErrorCode);
        UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
        UNUSED_PARAM(i4TestValFsMplsL3VpnVrfEgressRteInetCidrIfIndex);
        return SNMP_SUCCESS;
}
/****************************************************************************
Function    :  nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrMetric1
Input       :  The Indices
FsMplsL3VpnVrfEgressRtePathAttrLabel

The Object
testValFsMplsL3VpnVrfEgressRteInetCidrMetric1
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrMetric1(UINT4 *pu4ErrorCode , UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel , INT4 i4TestValFsMplsL3VpnVrfEgressRteInetCidrMetric1)
{
        UNUSED_PARAM(pu4ErrorCode);
        UNUSED_PARAM(u4FsMplsL3VpnVrfEgressRtePathAttrLabel);
        UNUSED_PARAM(i4TestValFsMplsL3VpnVrfEgressRteInetCidrMetric1);
        return SNMP_SUCCESS;
}
/****************************************************************************
Function    :  nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrStatus
Input       :  The Indices
FsMplsL3VpnVrfEgressRtePathAttrLabel

The Object
testValFsMplsL3VpnVrfEgressRteInetCidrStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhTestv2FsMplsL3VpnVrfEgressRteInetCidrStatus(UINT4 *pu4ErrorCode , UINT4 u4FsMplsL3VpnVrfEgressRtePathAttrLabel , INT4 i4TestValFsMplsL3VpnVrfEgressRteInetCidrStatus)
{
        if(L3vpnUtlTestEgressRteTableRowStatus(u4FsMplsL3VpnVrfEgressRtePathAttrLabel,
                                i4TestValFsMplsL3VpnVrfEgressRteInetCidrStatus,pu4ErrorCode)!=OSIX_SUCCESS)
        {
                return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;

}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2FsMplsL3VpnVrfEgressRteTable
Input       :  The Indices
FsMplsL3VpnVrfEgressRtePathAttrLabel
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1 nmhDepv2FsMplsL3VpnVrfEgressRteTable(UINT4 *pu4ErrorCode, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpVarBind)
{
        UNUSED_PARAM(pu4ErrorCode);
        UNUSED_PARAM(pSnmpIndexList);
        UNUSED_PARAM(pSnmpVarBind);
        return SNMP_SUCCESS;
}


#endif
