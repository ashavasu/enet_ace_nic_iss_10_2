/*****************************************************************************
 *
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: mpoamtx.c,v 1.5 2014/07/12 12:23:54 siva Exp $
 *
 * Description: This file contains the MPLS packet transmission functions
 *              from applications like BFD, ELPS etc.,
 *****************************************************************************/

#include "mplsdefs.h"
#include "mplsincs.h"
#include "ipv6.h"
#include "mpoamdef.h"
#include "arp.h"

extern INT4         MplsOamTxPacketHandleFromApp (tMplsApiInInfo *
                                                  pInMplsApiInfo);
/*****************************************************************************/
/* Function     : MplsOamTxPacketHandleFromApp                               */
/*                                                                           */
/* Description  : This function is used by MPLS applications to transmit     */
/*                application specific payload through MPLS switching.       */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
MplsOamTxPacketHandleFromApp (tMplsApiInInfo * pInMplsApiInfo)
{
    tCfaIfInfo          CfaIfInfo;
    tArpQMsg            ArpQMsg;
    UINT4               u4IpPort = 0;
    UINT4               u4IfIndex = 0;
#ifdef CFA_WANTED
    UINT4               u4PktSize = 0;
#endif
    UINT1               au1ZeroMac[MAC_ADDR_LEN];
    UINT1               u1EncapType = 0;

    MEMSET (au1ZeroMac, MPLS_ZERO, MAC_ADDR_LEN);
    MEMSET (&ArpQMsg, 0, sizeof (tArpQMsg));
    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    if (MplsGetL3Intf (pInMplsApiInfo->InPktInfo.u4OutIfIndex, &u4IfIndex)
        == MPLS_FAILURE)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsOamTxPacketHandleFromApp: Unable to get L3 interface "
                    "from MPLS tunnel interface doesnot exist: "
                    "INTMD-EXIT \n");
        return OSIX_FAILURE;
    }

    if (MEMCMP (pInMplsApiInfo->InPktInfo.au1DstMac, au1ZeroMac, MAC_ADDR_LEN)
        == MPLS_ZERO)
    {
        if (pInMplsApiInfo->InPktInfo.NextHopIpAddr.u4_addr[0] == MPLS_ZERO)
        {
            CfaGetIfUnnumPeerMac (u4IfIndex,
                                  pInMplsApiInfo->InPktInfo.au1DstMac);
        }
        else if (ArpResolve
                 (pInMplsApiInfo->InPktInfo.NextHopIpAddr.u4_addr[0],
                  (INT1 *) pInMplsApiInfo->InPktInfo.au1DstMac,
                  &u1EncapType) == ARP_FAILURE)
        {
            /* Unable to Resolve ARP for Next Hop */
            if (NetIpv4GetPortFromIfIndex (u4IfIndex, &u4IpPort) ==
                NETIPV4_FAILURE)
            {
                MPLSFM_DBG (MPLSFM_IF_FAILURE,
                            "MplsOamTxPacketHandleFromApp: Unable to get IP port "
                            "from L3 interface: INTMD-EXIT \n");
                return OSIX_FAILURE;
            }
            ArpQMsg.u4MsgType = ARP_APP_IF_MSG;
            ArpQMsg.u2Port = (UINT2) u4IpPort;
            ArpQMsg.u4IpAddr = pInMplsApiInfo->InPktInfo.
                NextHopIpAddr.u4_addr[0];
            /* Trigger ARP module for Resolving NextHopAddress */
            if (ArpEnqueuePkt (&ArpQMsg) == ARP_FAILURE)
            {
                MPLSFM_DBG2 (MPLSFM_MAIN_PRCS,
                             "\rMplsOamTxPacketHandleFromApp:ArpEnqueuePkt Failed. %d in fuction %s\n",
                             __LINE__, __func__);
                return OSIX_FAILURE;
            }
            MPLSFM_DBG1 (MPLSFM_IF_FAILURE,
                         "MplsOamTxPacketHandleFromApp: ARP resolution triggered"
                         "for IP address=%x: INTMD-EXIT \n", ArpQMsg.u4IpAddr);
            return OSIX_FAILURE;
        }
    }

#ifdef CFA_WANTED
    if (pInMplsApiInfo->InPktInfo.bIsMplsLabelledPacket == TRUE)
    {
        /* Send the labeled Packet to CFA Task for transmission */
        if (CfaHandlePktFromMpls (pInMplsApiInfo->InPktInfo.pBuf,
                                  u4IfIndex,
                                  MPLS_TO_CFA,
                                  pInMplsApiInfo->InPktInfo.au1DstMac,
                                  CFA_LINK_UCAST) != CFA_SUCCESS)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsOamTxPacketHandleFromApp: Packet transmission"
                        "failed in MPLS module: INTMD-EXIT \n");
            return OSIX_FAILURE;
        }
    }
    else
    {
        /* PHP implicit NULL (3) case handling */
        u4PktSize = CRU_BUF_Get_ChainValidByteCount
            (pInMplsApiInfo->InPktInfo.pBuf);
        /* Send the unlabeled Packet to CFA Task for transmission */
        if (CfaIwfEnetProcessTxFrame (pInMplsApiInfo->InPktInfo.pBuf,
                                      u4IfIndex,
                                      pInMplsApiInfo->InPktInfo.au1DstMac,
                                      u4PktSize, CFA_ENET_IPV4, u1EncapType)
            != CFA_SUCCESS)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsOamTxPacketHandleFromApp: Packet transmission"
                        "failed in MPLS module: INTMD-EXIT \n");
            return OSIX_FAILURE;
        }
    }
#endif
    return OSIX_SUCCESS;
}
