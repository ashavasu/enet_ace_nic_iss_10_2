/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: mplsmain.c,v 1.56 2016/02/03 10:37:56 siva Exp $
 *
 * Description: This file contains initialisation functions for router module.
 *****************************************************************************/

#define MPLS_MAIN_C
#include "mplsrtr.h"
#include "dsrmgbl.h"
#include "mplcmndb.h"
#include "teextrn.h"
#include "mplsred.h"
#include "mplcmndb.h"
#include "lblmgrex.h"
#include "mplsutil.h"
#include "ldpext.h"
#include "rpteext.h"
#include "oamext.h"
#include "mpoamdef.h"
#include "tcextrn.h"
#include "l2vpextn.h"
#include "mplsincs.h"
#include "indexmgr.h"
#include "inmgrex.h"
#include "tcapi.h"
#include "l2vpincs.h"
#ifdef L2RED_WANTED
#include "mplsred.h"
#endif

#ifdef MPLS_L3VPN_WANTED
#include "l3vpntmr.h"
#include "l3vpntdfsg.h"
#include "l3vpntdfs.h"
#include "l3vpnprot.h"
#include "l3vpndefn.h"
#endif

#include "iss.h"

#ifdef SNMP_2_WANTED
#include "fsmplswr.h"
#include "fsmpfrwr.h"
#include "stdtewr.h"
#include "stdgmtwr.h"
#include "stdlsrwr.h"
#include "fslsrwr.h"
#include "stdgmlwr.h"
#include "stdftnwr.h"
#include "stdldpwr.h"
#include "stdgnlwr.h"
#include "fsrsvpwr.h"
#include "stdpwwr.h"
#include "stdpwmwr.h"
#include "stdpwewr.h"
#include "fsmpnowr.h"
#include "fsmptewr.h"
#include "mspwwrg.h"            /*MS-PW */
#include "thlwrg.h"                /*STATIC_HLSP */
/* MPLS_P2MP_LSP_CHANGES - S */
#include "tpmwrg.h"
/* MPLS_P2MP_LSP_CHANGES - E */

#include "bgp.h"

static VOID         RegisterFSMPLS (VOID);
#endif
#include "arp.h"
#include "rtm.h"
static VOID MplsTCdsInitRMIfInfo ARG_LIST ((tDiffServRMGblInfo *));
PRIVATE VOID        MplsTmrInitArpTmrDesc (VOID);
PRIVATE UINT1 MplsGetRemainingTime ARG_LIST ((tTimerListId TimerList,
                                              tTmrAppTimer * pTimer,
                                              UINT4 *pTimeLeft));
extern UINT4        IssSzGetSizingMacroValue (CHR1 * pu1MacroName);
/*****************************************************************************/
/* Function Name : MplsFmMain                                                */
/* Description   : This is the main function to be invoked when MPLS task    */
/*                 is created. This function calls the Initialization and    */
/*                 process functions.                                        */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

VOID
MplsFmMain (INT1 *pDummy)
{
    UINT4               au4IfInfo[MPLS_ENQ_MSG_LEN];
    UINT4               u4DeQState;
    UINT4               u4RcvdEvent;
    tMplsRtEntryInfo   *pRouteInfo = NULL;
    tMplsIfEntryInfo   *pIfInfo = NULL;
    tMplsBufChainHeader *pMsg = NULL;
    tFrrTeTnlInfo       FrrTeTnlInfo;
    tTmrAppTimer       *pExpiredTimers = NULL;
    UINT1               u1TimerId = MPLS_ZERO;
    UINT1               u1IfType = MPLS_ZERO;
    INT2                i2OffSet = MPLS_ZERO;
#ifdef HVPLS_WANTED
    UINT4				u4PwVcIndex;
    tPwVcEntry          *pPwVcEntry = NULL;
#endif
    /* Dummy Pointers for system sizing */
    tMplsDiffServParamsSize *pMplsDiffServParamsSize = NULL;
    tMplsDiffServParamTokenStack *pMplsDiffServParamTokenStack = NULL;
    tElspMapRowSize    *pElspMapRowSize = NULL;
    tElspMapTokenStackSize *pElspMapTokenStackSize = NULL;
    tAsgnInfoHeadSize  *pAsgnInfoHeadSize = NULL;
    tIndexMgrGrpInfoSize *pIndexMgrGrpInfoSize = NULL;
    tIndexMgrChunkInfoSize *pIndexMgrChunkInfoSize = NULL;
    tIndexMgrChunkSize *pIndexMgrChunkSize = NULL;
#ifdef VPLSADS_WANTED
    tBgp4RegisterASNInfo BgpRegisterInfo;
#endif

    UNUSED_PARAM (pMplsDiffServParamsSize);
    UNUSED_PARAM (pMplsDiffServParamTokenStack);
    UNUSED_PARAM (pElspMapRowSize);
    UNUSED_PARAM (pElspMapTokenStackSize);
    UNUSED_PARAM (pAsgnInfoHeadSize);
    UNUSED_PARAM (pIndexMgrGrpInfoSize);
    UNUSED_PARAM (pIndexMgrChunkInfoSize);
    UNUSED_PARAM (pIndexMgrChunkSize);

    MPLSFM_DBG (MPLSFM_MAIN_ETEXT, "MplsFmMain MODULE : ENTRY\n");
    MPLSFM_DBG (MPLSFM_MAIN_ETEXT, "MplsFmMain : ENTRY\n");
    UNUSED_PARAM (pDummy);
    /*
     * Initialization of MPLS-FM
     */
    if (MplsInit () == MPLS_FAILURE)
    {
        MPLS_INIT_COMPLETE (OSIX_FAILURE);
        MPLSFM_DBG (MPLSFM_MAIN_ETEXT, "Initialization Failed \n");
        MPLSFM_DBG (MPLSFM_MAIN_ETEXT, "MplsFmMain : INTMD-EXIT \n");
        return;
    }

    if (LblMgrInit () != LBL_SUCCESS)
    {
        MPLS_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (MplsCommondbInitWithSem () != MPLS_SUCCESS)
    {
        MPLS_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

	#ifdef LDP_GR_WANTED
    if (MplsHwListInit () == MPLS_FAILURE)
    {
        MPLS_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
	#endif
	
	#if defined(VPLS_GR_WANTED) || defined(LDP_GR_WANTED)
    if(MplsCreateRbTreeForPwHwList() == MPLS_FAILURE )
    {
        MPLS_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
	if (MplsCreateRbTreeForVplsHwList() == MPLS_FAILURE )
	{
		MPLS_INIT_COMPLETE (OSIX_FAILURE);
		return;
	}
	#endif

    /* Setting the Config Parameters to the default value */
    TeSetCfgParams ();

    /* TC module entry point function is called */
    if (TcMain () != TC_SUCCESS)
    {
        MPLS_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    MplsTCdsInitRMIfInfo (&gDiffServRMGblInfo);

    if (IndexMgrInitWithSem () != MPLS_SUCCESS)
    {
        MPLS_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (TeInit () == TE_FAILURE)
    {
        MPLS_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (OamMainInit () == OSIX_FAILURE)
    {
        MPLS_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

#ifdef L2RED_WANTED
    if (OsixQueCrt ((UINT1 *) MPLSRM_QNAME, OSIX_MAX_Q_MSG_LEN, MPLSRM_QDEPTH,
                    &gMplsRmQId) != OSIX_SUCCESS)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "INIT:MPLSRMQ, Creation Failed \t\n");
        MPLS_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }

    if (MplsRmInit () != MPLS_SUCCESS)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "INIT:Failed to initialize MplsRm\t\n");
        OsixQueDel (gMplsRmQId);
        MPLS_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
#endif
    /* 
     * Currently Registering with FutureIP is done to
     * handle route change events.
     */
    if (MplsRegisterWithIP () == MPLS_FAILURE)
    {
        MPLS_INIT_COMPLETE (OSIX_FAILURE);
#ifdef L2RED_WANTED
        MplsRmDeInit ();
        OsixQueDel (gMplsRmQId);
#endif
        return;
    }

#ifdef MPLS_IPV6_WANTED
	if(MplsRegisterWithIpv6() == MPLS_FAILURE)
	{
		MPLS_INIT_COMPLETE (OSIX_FAILURE);
#ifdef L2RED_WANTED
		MplsRmDeInit();
		OsixQueDel (gMplsRmQId);
#endif
	}
#endif


#ifdef SNMP_2_WANTED
    RegisterFSMPLS ();
#endif

    MPLS_INIT_COMPLETE (OSIX_SUCCESS);

#ifdef MPLS_L3VPN_WANTED
#ifdef L3VPN
    Bgp4L3VpnRegister (MplsBgp4NotifyASN);
#endif
#endif

#ifdef VPLSADS_WANTED

    MEMSET (&BgpRegisterInfo, MPLS_ZERO, sizeof (tBgp4RegisterASNInfo));
    BgpRegisterInfo.pBgp4NotifyASN = MplsBgp4NotifyASN;
    Bgp4ASNRegister (&BgpRegisterInfo);
#endif

    while (MPLS_TRUE)
    {

        if (MPLS_WAIT_FOR_EVENT
            (gFmTaskId,
             (MPLS_IN_MSG_Q_EVENT | MPLS_ROUTE_CHG_NOTIF | MPLS_TNL_FRR_NOTIF |
              MPLS_RM_EVENT | MPLS_TIMER_EVENT | MPLS_INTF_CHG_NOTIF),
             MPLS_WAIT_FOREVER, &u4RcvdEvent) != MPLS_Q_SUCCESS)
        {
            continue;
        }
        if (gu1MplsInitialised != TRUE)
        {
            MPLSFM_DBG (MPLSFM_PRCS_ETEXT,
                        "MPLS is not initialized INTMD-EXIT \n");
            continue;
        }
        if ((u4RcvdEvent & MPLS_INTF_CHG_NOTIF) == MPLS_INTF_CHG_NOTIF)
        {
            u4DeQState = MPLS_DEQUEUE (gFmIfChgQId, (UINT1 *) (&pIfInfo),
                                       OSIX_DEF_MSG_LEN, MPLS_NO_WAIT);
            while (u4DeQState == OSIX_SUCCESS)
            {
                MplsIfChgEventHandle (pIfInfo);
                MemReleaseMemBlock (MPLS_IP_IF_INFO_POOL_ID, (UINT1 *) pIfInfo);
                u4DeQState =
                    MPLS_DEQUEUE (gFmIfChgQId, (UINT1 *) (&pIfInfo),
                                  OSIX_DEF_MSG_LEN, MPLS_NO_WAIT);
            }
        }
        if ((u4RcvdEvent & MPLS_ROUTE_CHG_NOTIF) == MPLS_ROUTE_CHG_NOTIF)
        {
            u4DeQState = MPLS_DEQUEUE (gFmRtrChgQId, (UINT1 *) (&pRouteInfo),
                                       OSIX_DEF_MSG_LEN, MPLS_NO_WAIT);
            while (u4DeQState == OSIX_SUCCESS)
            {
#ifdef MPLS_IPV6_WANTED
		    if(pRouteInfo->u2AddrType==MPLS_IPV6_ADDR_TYPE)
		    {
			    MplsIpv6RouteChgEventHandle (pRouteInfo);
		    }
		    else
#endif
		    {
			    MplsRouteChgEventHandle (pRouteInfo);
		    }

		    MemReleaseMemBlock (MPLS_IP_ROUTE_INFO_POOL_ID,
				    (UINT1 *) pRouteInfo);
		    u4DeQState =
                    MPLS_DEQUEUE (gFmRtrChgQId, (UINT1 *) (&pRouteInfo),
                                  OSIX_DEF_MSG_LEN, MPLS_NO_WAIT);
            }
        }
        if ((u4RcvdEvent & MPLS_TNL_FRR_NOTIF) == MPLS_TNL_FRR_NOTIF)
        {
            u4DeQState = MPLS_DEQUEUE (gFmFrrQId, (UINT1 *) (&pMsg),
                                       OSIX_DEF_MSG_LEN, MPLS_NO_WAIT);
            while (u4DeQState == OSIX_SUCCESS)
            {
                if (CRU_BUF_Copy_FromBufChain (pMsg,
                                               (UINT1 *) &FrrTeTnlInfo,
                                               MPLS_ZERO,
                                               sizeof (tFrrTeTnlInfo))
                    != sizeof (tFrrTeTnlInfo))
                {
                    /*
                     * Buffer not required and hence released.
                     */
                    CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                    break;
                }
                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                MplsCheckTnlFrrEntryInFTN (&FrrTeTnlInfo);
                u4DeQState =
                    MPLS_DEQUEUE (gFmFrrQId, (UINT1 *) (&pMsg),
                                  OSIX_DEF_MSG_LEN, MPLS_NO_WAIT);
            }
        }
        if ((u4RcvdEvent & MPLS_IN_MSG_Q_EVENT) == MPLS_IN_MSG_Q_EVENT)
        {
            u4DeQState = MPLS_DEQUEUE (gFmInQId, (UINT1 *) (&pMsg),
                                       OSIX_DEF_MSG_LEN, MPLS_NO_WAIT);
            while (u4DeQState == OSIX_SUCCESS)
            {
                switch (CRU_BUF_Get_U2Reserved (pMsg))
                {
#ifdef MBSM_WANTED
                    case MBSM_MSG_CARD_INSERT:
                    case MBSM_MSG_CARD_REMOVE:
                        MplsMbsmUpdateLCStatus (pMsg,
                                                CRU_BUF_Get_U2Reserved (pMsg));
                        MPLS_REL_BUF_CHAIN (pMsg, MPLS_BUF_NORMAL_REL);
                        break;
#endif
                    case CFA_TO_MPLS:
                        /* buffer will be released in the TX thread in *_TO_MPLS */
                        MplsProcessLabeledPacket (pMsg,
                                                  CRU_BUF_Get_U4Reserved1
                                                  (pMsg),
                                                  CRU_BUF_Get_U4Reserved2
                                                  (pMsg));
                        break;
#ifdef MPLS_IPV6_WANTED
                    /* MPLS_IPv6 add start */
		      case IPV6_L3_TO_MPLS:
                    /* MPLS_IPv6 add end */
#endif
                    case L3_TO_MPLS:
                        MplsLabelAndSendIpPkt (pMsg,
                                               CRU_BUF_Get_U4Reserved1 (pMsg));
                        break;
#ifdef MPLS_L3VPN_WANTED
                    case L3_TO_MPLS_L3VPN:
                        MplsLabelAndSendIpPktForL3Vpn (pMsg,
                                                       CRU_BUF_Get_U4Reserved1
                                                       (pMsg),
                                                       CRU_BUF_Get_U4Reserved2
                                                       (pMsg));
                        break;
#endif
                    case L2_TO_MPLS:

                        /* For the packets which are coming from CFA in pwIfIndex
                         * the Vfid was not passed from CFA, so find VFid */
                        CfaGetIfType (CRU_BUF_Get_U4Reserved3 (pMsg),
                                      &u1IfType);
                        if (u1IfType == CFA_PSEUDO_WIRE)
                        {
                            if (L2VpnApiGetPwIndexFromPwIfIndex
                                (CRU_BUF_Get_U4Reserved3 (pMsg),
                                 &CRU_BUF_Get_U4Reserved1 (pMsg)) ==
                                L2VPN_FAILURE)
                            {
                                /*realising the buffer if cfa Ifindex mapping is not done */
                                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                                break;
                            }
                        }
                        if (CRU_BUF_Get_U4Reserved2 (pMsg) == CFA_LINK_UCAST)
                        {
							if (L2VpnWrEncapAndFwdPkt (pMsg,
                                                       CRU_BUF_Get_U4Reserved1
                                                       (pMsg),
                                                       CRU_BUF_Get_U4Reserved3
                                                       (pMsg)) == L2VPN_FAILURE)
                            {
                                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                            }
                        }
                        else    /* Broadcast */
                        {
                            if (L2VpnMplsL2PktVplsFwd (pMsg,
                                                       CRU_BUF_Get_U4Reserved1
                                                       (pMsg),
                                                       CRU_BUF_Get_U4Reserved3
                                                       (pMsg)) == L2VPN_FAILURE)
                            {
                                /* Release the original buffer since a new buffer is sent on each PW */
                                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                            }
                        }
                        break;
#ifdef HVPLS_WANTED
                    case MPLS_TO_MPLS:
                        /* For the packets which are coming from CFA in pwIfIndex
                         * the Vfid was not passed from CFA, so find VFid */
                        CfaGetIfType (CRU_BUF_Get_U4Reserved3 (pMsg),
                                &u1IfType);
                        if (u1IfType == CFA_PSEUDO_WIRE)
                        {
                            if (L2VpnApiGetPwIndexFromPwIfIndex
                                    (CRU_BUF_Get_U4Reserved3 (pMsg),
                                     &CRU_BUF_Get_U4Reserved1 (pMsg)) ==
                                    L2VPN_FAILURE)
                            {
                                /*realising the buffer if cfa Ifindex mapping is not done */
                                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                                break;
                            }
                        }
                        CRU_BUF_Copy_FromBufChain (pMsg, (UINT1 *) &u4PwVcIndex, 0, sizeof (UINT4));
                        if (u4PwVcIndex < MAX_L2VPN_PW_VC_ENTRIES)
                        {	
                            CRU_BUF_Move_ValidOffset (pMsg, sizeof (UINT4));
                        }
                        else
                        {
                            u4PwVcIndex = 0;
                        }
                        if (CRU_BUF_Get_U4Reserved2 (pMsg) == CFA_LINK_UCAST)
                        {
                            
                            if (L2VpnWrEncapAndFwdPkt (pMsg,
                                        CRU_BUF_Get_U4Reserved1(pMsg),
                                        CRU_BUF_Get_U4Reserved3
                                        (pMsg)) == L2VPN_FAILURE)
                            {
                                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                            }
                        }
                        else    /* Broadcast */
                        {
                            pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwVcIndex);
                            if (L2VpnMplsPktVplsFwd (pMsg,
                                        pPwVcEntry->u4VplsInstance,
                                        CRU_BUF_Get_U4Reserved3
                                        (pMsg),u4PwVcIndex) == L2VPN_FAILURE)
                            {
                                /* Release the original buffer since a new buffer is sent on each PW */
                                CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
                            }
                        }
                        break;
#endif

                    default:
                        /* Processing the Events stored in the Msg */
                        if (MPLS_COPY_FROM_BUF_CHAIN (pMsg, (UINT1 *) au4IfInfo,
                                                      MPLS_DEF_BUF_OFF_SET,
                                                      MPLS_DEF_BUF_SIZE) ==
                            MPLS_DEF_BUF_SIZE)
                        {
                            MplsProcessGroupEvents (au4IfInfo);
                        }
                        MPLS_REL_BUF_CHAIN (pMsg, MPLS_BUF_NORMAL_REL);
                }

                u4DeQState =
                    MPLS_DEQUEUE (gFmInQId, (UINT1 *) (&pMsg), OSIX_DEF_MSG_LEN,
                                  MPLS_NO_WAIT);

            }                    /* End of DeQ while loop */
        }
#ifdef L2RED_WANTED
        if ((u4RcvdEvent & MPLS_RM_EVENT) == MPLS_RM_EVENT)
        {
            MplsRedHandleRmEvents ();
        }                        /* End of DeQ while loop */
#endif

        if ((u4RcvdEvent & MPLS_TIMER_EVENT) == MPLS_TIMER_EVENT)
        {
            while ((pExpiredTimers =
                    TmrGetNextExpiredTimer (MPLS_TMR_LIST_ID (MPLS_DEF_INCARN)))
                   != NULL)
            {
                u1TimerId = ((tTmrBlk *) pExpiredTimers)->u1TimerId;

                if (u1TimerId >= (UINT1) MPLS_MAX_TIMERS)
                {
                    MPLSFM_DBG1 ((MPLSFM_DBG_TIMER | MPLSFM_DBG_ALL),
                                 "Trying Processing an Invalid Timer : %d "
                                 "- Skipped\n", u1TimerId);
                    continue;
                }

                MPLSFM_DBG1 ((MPLSFM_DBG_TIMER | MPLSFM_DBG_ALL),
                             "Timer ID : %d\n", u1TimerId);

                i2OffSet = MPLS_TIMER_DESC_OFFSET (MPLS_ZERO, u1TimerId);

                (*(MPLS_TIMER_DESC_FUNC (MPLS_ZERO, u1TimerId)))
                    ((UINT1 *) pExpiredTimers - i2OffSet);
            }
        }

    }                            /* while (TRUE) */
}                                /* End of function MplsFmMain */

/*****************************************************************************/
/* Function Name : MplsInit                                                  */
/* Description   : Initialization of global variables to all incarnations    */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/

UINT1
MplsInit (void)
{
    UINT4               u4IncarnNum;
    UINT4               u4IpAddr = 0;

    MPLSFM_DBG (MPLSFM_MAIN_ETEXT, "MplsInit : ENTRY \n");

    /*Get MPLSFM task id */
    if (OsixTskIdSelf (&gFmTaskId) == OSIX_FAILURE)
    {
        MPLSFM_DBG (MPLSFM_MAIN_PRCS,
                    "MAIN : MplsFM : Getting Task Id Failed\n");
        return MPLS_FAILURE;
    }

    MPLS_IN_Q_DEPTH = IssSzGetSizingMacroValue ("MPLS_IN_Q_DEPTH");

    if ((MPLS_IN_Q_DEPTH == 0) || (MPLS_IN_Q_DEPTH > MPLS_MAX_IN_Q_DEPTH))
    {
        MPLS_IN_Q_DEPTH = MPLS_DEF_IN_Q_DEPTH;
    }
    if (MPLS_CREATE_Q ((UINT1 *) MPLS_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                       MPLS_IN_Q_DEPTH, (tOsixQId *) & gFmInQId)
        != MPLS_Q_SUCCESS)
    {
        MPLSFM_DBG (MPLSFM_MAIN_PRCS, "\nCreation of Incoming Q: Failed\n");
        MPLSFM_DBG (MPLSFM_MAIN_ETEXT, "MplsInit : INTMD-EXIT \n");
        return (MPLS_FAILURE);
    }
    if (MPLS_CREATE_Q ((UINT1 *) MPLS_ROUTE_CHG_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                       FsMPLSRTRSizingParams
                       [MAX_MPLSRTR_RT_CHG_ENTRIES_SIZING_ID].
                       u4PreAllocatedUnits,
                       (tOsixQId *) & gFmRtrChgQId) != MPLS_Q_SUCCESS)
    {
        MPLS_DELETE_Q (gFmInQId);
        MPLSFM_DBG (MPLSFM_MAIN_PRCS,
                    "\nCreation of Route change event handle Q: Failed\n");
        MPLSFM_DBG (MPLSFM_MAIN_ETEXT, "MplsInit : INTMD-EXIT \n");
        return (MPLS_FAILURE);
    }
    if (MPLS_CREATE_Q ((UINT1 *) MPLS_FRR_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                       MPLS_IN_Q_DEPTH, (tOsixQId *) & gFmFrrQId)
        != MPLS_Q_SUCCESS)
    {
        MPLS_DELETE_Q (gFmInQId);
        MPLS_DELETE_Q (gFmRtrChgQId);
        MPLSFM_DBG (MPLSFM_MAIN_PRCS, "\nCreation of FRR Q: Failed\n");
        MPLSFM_DBG (MPLSFM_MAIN_ETEXT, "MplsInit : INTMD-EXIT \n");
        return (MPLS_FAILURE);
    }

    if (MPLS_CREATE_Q ((UINT1 *) MPLS_IF_CHG_Q_NAME, OSIX_MAX_Q_MSG_LEN,
                       MPLS_IN_Q_DEPTH, (tOsixQId *) & gFmIfChgQId)
        != MPLS_Q_SUCCESS)
    {
        MPLS_DELETE_Q (gFmInQId);
        MPLS_DELETE_Q (gFmRtrChgQId);
        MPLS_DELETE_Q (gFmFrrQId);
        MPLSFM_DBG (MPLSFM_MAIN_PRCS, "\nCreation of IF CHG Q: Failed\n");
        MPLSFM_DBG (MPLSFM_MAIN_ETEXT, "MplsInit : INTMD-EXIT \n");
        return (MPLS_FAILURE);
    }

    /*Creation of Semaphore for Configuration threads Protection */
    if (MPLS_CREATE_SEM (MFM_CONFIG_SEM, MFM_SEM_COUNT,
                         MFM_SEM_MODE, (tOsixSemId *) & gConfSemId)
        != MPLS_SEM_SUCCESS)
    {
        MPLS_DELETE_Q (gFmInQId);
        MPLS_DELETE_Q (gFmRtrChgQId);
        MPLS_DELETE_Q (gFmFrrQId);
        MPLS_DELETE_Q (gFmIfChgQId);
        MPLSFM_DBG (MPLSFM_MAIN_PRCS,
                    "\nCreation of Configuration Thread Protection "
                    "Semaphore: Failed\n");
        MPLSFM_DBG (MPLSFM_MAIN_ETEXT, "MplsInit : INTMD-EXIT \n");
        return (MPLS_FAILURE);
    }

    /*Creation of Semaphore for Data Structure Protection */
    if (MPLS_CREATE_SEM (MFM_DS_SEM, MFM_SEM_COUNT,
                         MFM_SEM_MODE, (tOsixSemId *) & gDsSemId)
        != MPLS_SEM_SUCCESS)
    {
        MPLS_DELETE_Q (gFmInQId);
        MPLS_DELETE_Q (gFmRtrChgQId);
        MPLS_DELETE_Q (gFmFrrQId);
        MPLS_DELETE_Q (gFmIfChgQId);
        MPLS_DELETE_SEM (gConfSemId);
        MPLSFM_DBG (MPLSFM_MAIN_PRCS,
                    "\nCreation of Data Structure Protection "
                    "Semaphore: Failed\n");
        MPLSFM_DBG (MPLSFM_MAIN_ETEXT, "MplsInit : INTMD-EXIT \n");
        return (MPLS_FAILURE);
    }

    if (TmrCreateTimerList (MFM_TASK_NAME,
                            MPLS_TIMER_EVENT, NULL,
                            &(MPLS_TMR_LIST_ID (MPLS_DEF_INCARN)))
        != TMR_SUCCESS)
    {
        MPLS_DELETE_Q (gFmInQId);
        MPLS_DELETE_Q (gFmRtrChgQId);
        MPLS_DELETE_Q (gFmFrrQId);
        MPLS_DELETE_Q (gFmIfChgQId);
        MPLS_DELETE_SEM (gConfSemId);
        MPLS_DELETE_SEM (gDsSemId);
        MPLSFM_DBG (MPLSFM_MAIN_PRCS,
                    "\rArp Resolution Timer List creation: Failed\n");
        MPLSFM_DBG (MPLSFM_MAIN_ETEXT, "MplsInit : INTMD-EXIT \n");
        return (MPLS_FAILURE);
    }
    MplsTmrInitArpTmrDesc ();

    /* Initialise all the Qos Policies with their corresponding Tos values */
    MplsInitTosValsForStdQosPolicies ();

    /* Initialization of global parameters for all the Incarnations */
    for (u4IncarnNum = MPLS_ZERO; u4IncarnNum < MPLS_MAX_INCARNS; u4IncarnNum++)
    {
        MPLS_QOS_POLICY (u4IncarnNum) = MPLS_STD_IP;
        /* Assign the Qos Policy to be
         * adopted by this Incarn */

        /* SNMP Group Declaration */
        MPLS_ADMIN_STATUS (u4IncarnNum) = MPLS_ADMIN_STATUS_DOWN;
        gMplsIncarn[u4IncarnNum].DiffServGlobal.u4DiffServStatus =
            MPLS_DSTE_STATUS_DISABLE;
        MPLS_FEC_TYPE (u4IncarnNum) = MPLS_FEC_DEST_ADDR;
#ifdef LANAI_WANTED
        MPLS_VC_TABLE_ENTRIES (u4IncarnNum) = MPLS_DEF_VC_TABLE_ENTRIES;
        MPLS_ATM_INFO_ENTRIES (u4IncarnNum) = MPLS_DEF_ATM_INFO_ENTRIES;
#endif

        if (FsMPLSRTRSizingParams[MAX_MPLSRTR_DIFFSERV_PARAMS_TABLE_SIZING_ID].
            u4PreAllocatedUnits != MAX_MPLSRTR_DIFFSERV_PARAMS_TABLE)
        {
            MPLS_DIFFSERV_PARAMS_ENTRIES (u4IncarnNum) =
                FsMPLSRTRSizingParams
                [MAX_MPLSRTR_DIFFSERV_PARAMS_TABLE_SIZING_ID].
                u4PreAllocatedUnits;

            FsMPLSRTRSizingParams[MAX_MPLSRTR_DIFFSERV_PARAMS_TABLE_SIZING_ID].
                u4StructSize
                = (sizeof (tMplsDiffServParams) *
                   MPLS_DIFFSERV_PARAMS_ENTRIES (u4IncarnNum));
            FsMPLSRTRSizingParams[MAX_MPLSRTR_DIFFSERV_PARAMS_TABLE_SIZING_ID].
                u4PreAllocatedUnits = MAX_MPLSRTR_DIFFSERV_PARAMS_TABLE;
        }
        else
        {
            MPLS_DIFFSERV_PARAMS_ENTRIES (u4IncarnNum) =
                MAX_MPLSRTR_MPLS_DIFFSERV_PARAMS;
        }

        if (FsMPLSRTRSizingParams
            [MAX_MPLSRTR_DIFFSERV_PARAMS_TOKEN_STACK_SIZING_ID].
            u4PreAllocatedUnits != MAX_MPLSRTR_DIFFSERV_PARAMS_TOKEN_STACK)
        {
            FsMPLSRTRSizingParams
                [MAX_MPLSRTR_DIFFSERV_PARAMS_TOKEN_STACK_SIZING_ID].
                u4StructSize
                = (sizeof (UINT4) *
                   FsMPLSRTRSizingParams
                   [MAX_MPLSRTR_DIFFSERV_PARAMS_TOKEN_STACK_SIZING_ID].
                   u4PreAllocatedUnits);

            FsMPLSRTRSizingParams
                [MAX_MPLSRTR_DIFFSERV_PARAMS_TOKEN_STACK_SIZING_ID].
                u4PreAllocatedUnits = MAX_MPLSRTR_DIFFSERV_PARAMS_TOKEN_STACK;
        }

        if (FsMPLSRTRSizingParams[MAX_MPLSRTR_ELSPMAP_TABLE_SIZING_ID].
            u4PreAllocatedUnits != MAX_MPLSRTR_ELSPMAP_TABLE)
        {
            MPLS_DIFFSERV_ELSP_MAP_ENTRIES (u4IncarnNum) =
                FsMPLSRTRSizingParams[MAX_MPLSRTR_ELSPMAP_TABLE_SIZING_ID].
                u4PreAllocatedUnits;

            FsMPLSRTRSizingParams[MAX_MPLSRTR_ELSPMAP_TABLE_SIZING_ID].
                u4StructSize
                = (sizeof (tElspMapRow) *
                   FsMPLSRTRSizingParams[MAX_MPLSRTR_ELSPMAP_TABLE_SIZING_ID].
                   u4PreAllocatedUnits);

            FsMPLSRTRSizingParams[MAX_MPLSRTR_ELSPMAP_TABLE_SIZING_ID].
                u4PreAllocatedUnits = MAX_MPLSRTR_ELSPMAP_TABLE;
        }
        else
        {
            MPLS_DIFFSERV_ELSP_MAP_ENTRIES (u4IncarnNum) =
                MAX_MPLSRTR_ELSPMAP_ROW_ENTRIES;
        }

        if (FsMPLSRTRSizingParams[MAX_MPLSRTR_ELSPMAP_TOKEN_STACK_SIZING_ID].
            u4PreAllocatedUnits != MAX_MPLSRTR_ELSPMAP_TOKEN_STACK)
        {
            FsMPLSRTRSizingParams[MAX_MPLSRTR_ELSPMAP_TOKEN_STACK_SIZING_ID].
                u4StructSize
                = (sizeof (UINT4) *
                   FsMPLSRTRSizingParams
                   [MAX_MPLSRTR_ELSPMAP_TOKEN_STACK_SIZING_ID].
                   u4PreAllocatedUnits);

            FsMPLSRTRSizingParams[MAX_MPLSRTR_ELSPMAP_TOKEN_STACK_SIZING_ID].
                u4PreAllocatedUnits = MAX_MPLSRTR_ELSPMAP_TOKEN_STACK;
        }

        MPLS_DIFFSERV_TNL_MODEL (u4IncarnNum) = PIPE_MODEL;
        MPLS_TTL_VALUE (u4IncarnNum) = MPLS_DEF_TTL_VAL;

        MPLSFM_DBG_FLAG = MPLSFM_DEF_DBG_FLAG;

#ifdef LANAI_WANTED
        MPLS_SLL_INIT (MPLS_VC_TABLE_LIST (u4IncarnNum));
#endif
        if (MplsAllocateResources (u4IncarnNum) == MPLS_FAILURE)
        {
            return (MPLS_FAILURE);
        }

#ifdef ISS_WANTED
        u4IpAddr = IssGetIpAddrFromNvRam ();
        u4IpAddr = OSIX_HTONL (u4IpAddr);
#else
        NetIpv4GetHighestIpAddr (&u4IpAddr);
#endif
        MEMCPY ((MPLS_ROUTER_ID (u4IncarnNum).au1Ipv4Addr),
                (UINT1 *) &u4IpAddr, ROUTER_ID_LENGTH);
        MPLS_ACH_PSC_CHNL_CODE (u4IncarnNum) = MPLS_ACH_CHANNEL_PSC;
        MPLS_ADMIN_STATUS (u4IncarnNum) = MPLS_ADMIN_STATUS_UP;
        MPLS_RPTE_GR_MAX_WAIT_TIME (u4IncarnNum) = MPLS_DEF_GR_MAX_WAIT_TIME;
    }                            /* for each incarn */

    gu1MplsInitialised = TRUE;
    MPLSFM_DBG (MPLSFM_MAIN_ETEXT, "MplsInit : EXIT \n");
    return (MPLS_SUCCESS);
}

/*****************************************************************************/
/* Function Name : MplsAllocateResources                                     */
/* Description   : This function calls all Memory allocation function,       */
/*                 and initialisation of Tables and interfaces for the       */
/*                 input incarnation.                                        */
/* Input(s)      : u4IncarnNum - Incarnation number                          */
/* Output(s)     : None                                                      */
/* Return(s)     : MPLS_SUCCESS     if  Initialization is sucess             */
/*                 or MPLS_FAILURE  if  Initialization is failure            */
/*****************************************************************************/

UINT1
MplsAllocateResources (UINT4 u4IncarnNum)
{

    MPLSFM_DBG (MPLSFM_MAIN_ETEXT, "MplsAllocateResources : ENTRY \n");
    /* 
     * Creation of Memory pools 
     */
    if (MplsCreateMemPools (u4IncarnNum) == MPLS_FAILURE)
    {

        MPLSFM_DBG (MPLSFM_MAIN_MEM, "Creation of Memory pools failed\n");
        if (MplsRelResources (u4IncarnNum) == MPLS_FAILURE)
        {
            MPLSFM_DBG (MPLSFM_MAIN_MEM, "Resources Release is failed\n");
        }
        MPLSFM_DBG (MPLSFM_MAIN_ETEXT, "MplsAllocateResources :INTMD-EXIT  \n");
        return (MPLS_FAILURE);
    }

    /* Allocation of memory and Initialization of the DiffServ Related Memory */
    if (MplsInitDiffServTables (u4IncarnNum) == MPLS_FAILURE)
    {
        MPLSFM_DBG (MPLSFM_MAIN_PRCS,
                    "DiffServ Table Initialization  failed\n");
        if (MplsRelResources (u4IncarnNum) == MPLS_FAILURE)
        {
            MPLSFM_DBG (MPLSFM_MAIN_MEM, "Resources Release failed\n");
        }
        return (MPLS_FAILURE);
    }

    /* Make the DiffServ Global status UP */
    gMplsIncarn[MPLS_DEF_INCARN].DiffServGlobal.u4DiffServStatus
        = MPLS_DSTE_STATUS_DISABLE;

    MPLSFM_DBG (MPLSFM_MAIN_ETEXT, "MplsAllocateResources :EXIT  \n");
    return (MPLS_SUCCESS);
}

/*****************************************************************************/
/* Function Name : MplsCreateMemPools                                        */
/* Description   : Creation of the memory pools for the structures other     */
/*                 than the Tables and databases                             */
/* Input(s)      : u4IncarnNum - Incarnation number                          */
/* Output(s)     : None                                                      */
/* Return(s)     : MPLS_SUCCESS or MPLS_FAILURE                              */
/*****************************************************************************/

UINT1
MplsCreateMemPools (UINT4 u4IncarnNum)
{
    UNUSED_PARAM (u4IncarnNum);

    MPLSFM_DBG (MPLSFM_MAIN_ETEXT, "MplsCreateMemPools :ENTRY  \n");

    if (MplsrtrSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        return MPLS_FAILURE;
    }
#ifdef LANAI_WANTED
    /* Creation of ATM Related Mempool */
    if (LanaiSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        return MPLS_FAILURE;
    }
#endif

    MPLSFM_DBG (MPLSFM_MAIN_ETEXT, "MplsCreateMemPools :EXIT  \n");
    return (MPLS_SUCCESS);
}

/*****************************************************************************/
/* Function Name : MplsRelResources                                          */
/* Description   : Release all the resources held by MPLSFM module           */
/* Input(s)      : u4IncarnNum                                               */
/* Output(s)     : None                                                      */
/* Return(s)     : MPLS_SUCCESS     if Release of resources is success       */
/*                 or MPLS_FAILURE  if Release of resources fails            */
/*****************************************************************************/

UINT1
MplsRelResources (UINT4 u4IncarnNum)
{
#ifdef LANAI_WANTED
    tVcTableEntry      *pVcTableEntry = NULL;
    tMplsAtmConnInfo    MplsAtmConnInfo;
    tMplsSllNode       *pVcSllNode = NULL;
#endif
    MPLSFM_DBG (MPLSFM_MAIN_ETEXT, "MplsRelResources : ENTRY  \n");

#ifdef LANAI_WANTED
    pVcSllNode = (tMplsSllNode *)
        MPLS_SLL_FIRST (MPLS_VC_TABLE_LIST (u4IncarnNum));
    while (pVcSllNode != NULL)
    {
        pVcTableEntry = (tVcTableEntry *) pVcSllNode;
        MplsAtmConnInfo.u4Handle = pVcTableEntry->u4ConnHandle;
        MplsUpdateAtmConnection (&MplsAtmConnInfo, MPLS_ATMX_OPR_DEL);
        MPLS_SLL_DELETE (MPLS_VC_TABLE_LIST (u4IncarnNum), pVcSllNode);
        pVcSllNode = (tMplsSllNode *)
            MPLS_SLL_FIRST (MPLS_VC_TABLE_LIST (u4IncarnNum));
    }
    MPLS_SLL_INIT (MPLS_VC_TABLE_LIST (u4IncarnNum));
#endif

    /* Deletion of the DiffServ Related Tables */
    MplsShutdownDiffServTables (u4IncarnNum);
    gMplsIncarn[u4IncarnNum].DiffServGlobal.u4DiffServStatus =
        MPLS_DSTE_STATUS_DISABLE;

    /* Delete Memory pools in router module. */
    if (MplsDeleteMemPools (u4IncarnNum) == MPLS_FAILURE)
    {
        MPLSFM_DBG (MPLSFM_MAIN_MEM, "Deletion of Memory pools failed\n");
        MPLSFM_DBG (MPLSFM_MAIN_ETEXT, "MplsRelResources : INTMD-EXIT  \n");
    }

    MPLSFM_DBG (MPLSFM_MAIN_ETEXT, "MplsRelResources : EXIT  \n");
    return (MPLS_SUCCESS);
}

/*****************************************************************************/
/* Function Name : MplsDeleteMemPools                                        */
/* Description   : Function to Delete the Memory pools for the input         */
/*                  incarnation                                              */
/* Input(s)      : u4IncarnNum - Incarnation number                          */
/* Output(s)     : None                                                      */
/* Return(s)     : MPLS_SUCCESS or MPLS_FAILURE                              */
/*****************************************************************************/

UINT1
MplsDeleteMemPools (UINT4 u4IncarnNum)
{
    tMplsRtEntryInfo   *pRouteInfo = NULL;
    tMplsIfEntryInfo   *pIfInfo = NULL;
    tMplsBufChainHeader *pMsg = NULL;

    UNUSED_PARAM (u4IncarnNum);

    MPLSFM_DBG (MPLSFM_MAIN_ETEXT, "MplsDeleteMemPools : ENTRY  \n");

    while (MPLS_DEQUEUE (gFmIfChgQId, (UINT1 *) (&pIfInfo),
                         OSIX_DEF_MSG_LEN, MPLS_NO_WAIT) == OSIX_SUCCESS)
    {
        MemReleaseMemBlock (MPLS_IP_IF_INFO_POOL_ID, (UINT1 *) pIfInfo);
    }

    while (MPLS_DEQUEUE (gFmRtrChgQId, (UINT1 *) (&pRouteInfo),
                         OSIX_DEF_MSG_LEN, MPLS_NO_WAIT) == OSIX_SUCCESS)
    {
        MemReleaseMemBlock (MPLS_IP_ROUTE_INFO_POOL_ID, (UINT1 *) pRouteInfo);
    }

    while (MPLS_DEQUEUE (gFmFrrQId, (UINT1 *) (&pMsg),
                         OSIX_DEF_MSG_LEN, MPLS_NO_WAIT) == OSIX_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
    }

    while (MPLS_DEQUEUE (gFmInQId, (UINT1 *) (&pMsg),
                         OSIX_DEF_MSG_LEN, MPLS_NO_WAIT) == OSIX_SUCCESS)
    {
        MPLS_REL_BUF_CHAIN (pMsg, MPLS_BUF_NORMAL_REL);
    }

#ifdef LANAI_WANTED
    /* Deletion of ATM Mempool */
    LanaiSizingMemDeleteMemPools ();
#endif
    MplsrtrSizingMemDeleteMemPools ();

    MPLSFM_DBG (MPLSFM_MAIN_ETEXT, "MplsDeleteMemPools : EXIT  \n");
    return (MPLS_SUCCESS);
}

/*****************************************************************************/
/* Function Name : MplsInitTosValsForStdQosPolicies                          */
/* Description   : Initialises the Standard Qos policies with the Tos values */
/*                 that are valid within that policy.                        */
/*                 Standard Qos Policies considered are -                    */
/*                   STD IP -> 0 - 7 Tos values                              */
/*                   RFC-1349 -> 0, 1, 2, 4, 8 are valid Tos Values.         */
/*                   DIFF-SERV -> TODO                                       */
/* Input(s)      : None.                                                     */
/* Output(s)     : None.                                                     */
/* Return(s)     : None.                                                     */
/*****************************************************************************/

VOID
MplsInitTosValsForStdQosPolicies ()
{
    UINT1               u1QosType;
    UINT1              *pu1TosVals = NULL;
    UINT1               u1Index;
    UINT1               u1Tos;

    MPLSFM_DBG (MPLSFM_MAIN_ETEXT,
                "MplsInitTosValsForStdQosPolicies :ENTRY  \n");
    for (u1QosType = MPLS_ZERO; u1QosType < MPLS_MAX_QOS_POLICIES; u1QosType++)
    {

        switch (u1QosType)
        {
            case MPLS_STD_IP:
                pu1TosVals = MPLS_TOSVALS_ARR (u1QosType);

                /* 
                 * FutureMPLS supports foll Tos values - 0, 4, 8, 12, 16, 20, 24,28.
                 * PRECEDENCE bits are not supported.
                 *
                 *   0    1    2    3     4     5     6   7
                 *   -----------------------------------------
                 *  | PRECEDENCE |  D  |  T  |  R  |  UNUSED  |
                 *   -----------------------------------------
                 */
                for (u1Index = 0, u1Tos = 0; u1Tos <= 28; u1Index++, u1Tos += 4)
                {
                    pu1TosVals[u1Index] = u1Tos;
                }

                /* Total Number of Tos values that are valid within this 
                 * Policy */
                MPLS_TOSVALS_COUNT (u1QosType) = u1Index;
                break;

            case MPLS_RFC1349:
                pu1TosVals = MPLS_TOSVALS_ARR (u1QosType);
                /* Valid Tos Values range from 0 to 8 - 0, 1, 2, 4, 8 */
                for (u1Index = u1Tos = 0; u1Tos <= 8; u1Index++,
                     (u1Tos = (UINT1) (u1Tos ? (u1Tos * 2) : 1)))
                {
                    pu1TosVals[u1Index] = u1Tos;
                }
                /* Need to store the maximum valid count here 
                 * Total No of Tos values that are valid within this Policy */
                MPLS_TOSVALS_COUNT (u1QosType) = u1Index;
                break;

            case MPLS_DIFF_SERV:
                pu1TosVals = MPLS_TOSVALS_ARR (u1QosType);
                /* This is used to store LLSP PSC Values
                 * Valid values are initalized
                 */
                pu1TosVals[0] = MPLS_DIFFSERV_DF_DSCP;
                pu1TosVals[1] = MPLS_DIFFSERV_CS1_DSCP;
                pu1TosVals[2] = MPLS_DIFFSERV_CS2_DSCP;
                pu1TosVals[3] = MPLS_DIFFSERV_CS3_DSCP;
                pu1TosVals[4] = MPLS_DIFFSERV_CS4_DSCP;
                pu1TosVals[5] = MPLS_DIFFSERV_CS5_DSCP;
                pu1TosVals[6] = MPLS_DIFFSERV_CS6_DSCP;
                pu1TosVals[7] = MPLS_DIFFSERV_CS7_DSCP;
                pu1TosVals[8] = MPLS_DIFFSERV_EF_DSCP;
                pu1TosVals[9] = MPLS_DIFFSERV_AF1_PSC_DSCP;
                pu1TosVals[10] = MPLS_DIFFSERV_AF2_PSC_DSCP;
                pu1TosVals[11] = MPLS_DIFFSERV_AF3_PSC_DSCP;
                pu1TosVals[12] = MPLS_DIFFSERV_AF4_PSC_DSCP;
                /* Total Number of Tos values that are valid in this Policy */
                MPLS_TOSVALS_COUNT (u1QosType) = 13;
                break;
        }
    }
    MPLSFM_DBG (MPLSFM_MAIN_ETEXT,
                "MplsInitTosValsForStdQosPolicies :EXIT  \n");
}

/*****************************************************************************/
/* Function Name : MplsEnqSnmpEvent                                          */
/* Description   : This enqueues the SNMP event to MPLS-FM task              */
/* Input(s)      : u4Incarn - Incarnation Number                             */
/*                 u4SnmpEvent - Event to enqueue                            */
/* Output(s)     : None                                                      */
/* Return(s)     : MPLS_SUCCESS or MPLS_FAILURE                              */
/*****************************************************************************/

UINT1
MplsEnqSnmpEvent (UINT4 u4Incarn, UINT4 u4SnmpEvent)
{
    UINT4               au4IfInfo[MPLS_ENQ_MSG_LEN];

    MPLSFM_DBG (MPLSFM_IF_ETEXT, "MplsEnqSnmpEvent : ENTRY \n");

    au4IfInfo[MPLS_EVENT] = u4SnmpEvent;
    au4IfInfo[MPLS_INCARN] = u4Incarn;

    /* Enqueues to MPLS incoming Queue */
    if (MplsEnqueueMplsIncomingQ (au4IfInfo) == MPLS_FAILURE)
    {
        MPLSFM_DBG (MPLSFM_IF_ETEXT, "MplsEnqSnmpEvent : INTMD-EXIT \n");
        return (MPLS_FAILURE);
    }

    MPLSFM_DBG (MPLSFM_IF_ETEXT, " MplsEnqSnmpEvent : EXIT \n");
    return (MPLS_SUCCESS);
}

/*****************************************************************************/
/* Function Name : RegisterFSMPLS                                            */
/* Description   : Function to register all MPLS mibs with SNMP agent        */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
#ifdef SNMP_2_WANTED
VOID
RegisterFSMPLS (VOID)
{
    RegisterFSMPLSRtrObj ();
    RegisterSTDTE ();
    RegisterSTDGMT ();
    RegisterFSMPTE ();
    RegisterFSHLSP ();            /* STATIC_HLSP */
    RegisterLSR ();
    RegisterFSLSR ();
    RegisterSTDGML ();
    RegisterFTN ();
    /* MPLS_P2MP_LSP_CHANGES - S */
    RegisterP2MP ();
    /* MPLS_P2MP_LSP_CHANGES - E */
    OamMainRegisterOamMibs ();

    AddSysEntry ();
}
#endif /* SNMP_2_WANTED */

/*****************************************************************************/
/* Function Name : MplsTCdsInitRMIfInfo                                      */
/* Description   : Function to Init RMIfInfo. Should be ported after         */
/*                 ntegration with external components                       */
/* Input(s)      : pMplsTCRmInfo - RMIfInfo Pointer                          */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
MplsTCdsInitRMIfInfo (tDiffServRMGblInfo * pMplsTCRmInfo)
{
    UINT4               u4Index;

    /*Needs to be uncommented when ClassType is supported */
    /*pMplsTCRmInfo->u1RsrcMgmtType = TE_DS_CLASSTYPE_RESOURCES; */
    pMplsTCRmInfo->u1RsrcMgmtType = TE_DS_AGGREGATE_RESOURCES;
    /*pMplsTCRmInfo->u1RsrcMgmtType = TE_DS_PEROABASED_RESOURCES; */

    for (u4Index = 0; u4Index < TE_DS_MAX_IFACES; u4Index++)
    {
        pMplsTCRmInfo->aRMIfInfo[u4Index].au1RMSupportedPhbs[0] =
            MPLS_DIFFSERV_AF11_DSCP;
        pMplsTCRmInfo->aRMIfInfo[u4Index].au1RMSupportedPhbs[1] =
            MPLS_DIFFSERV_AF12_DSCP;
        pMplsTCRmInfo->aRMIfInfo[u4Index].au1RMSupportedPhbs[2] =
            MPLS_DIFFSERV_AF13_DSCP;
        pMplsTCRmInfo->aRMIfInfo[u4Index].au1RMSupportedPhbs[3] =
            MPLS_DIFFSERV_AF22_DSCP;
        pMplsTCRmInfo->aRMIfInfo[u4Index].au1RMSupportedPhbs[4] =
            MPLS_DIFFSERV_AF32_DSCP;
        pMplsTCRmInfo->aRMIfInfo[u4Index].au1RMSupportedPhbs[5] =
            MPLS_DIFFSERV_AF33_DSCP;
        pMplsTCRmInfo->aRMIfInfo[u4Index].au1RMSupportedPhbs[6] =
            MPLS_DIFFSERV_AF21_DSCP;

        pMplsTCRmInfo->aRMIfInfo[u4Index].au1RMSupportedPSC[0] =
            MPLS_DIFFSERV_AF1_PSC_DSCP;
        pMplsTCRmInfo->aRMIfInfo[u4Index].au1RMSupportedPSC[1] =
            MPLS_DIFFSERV_AF2_PSC_DSCP;
        pMplsTCRmInfo->aRMIfInfo[u4Index].au1RMSupportedPSC[2] =
            MPLS_DIFFSERV_AF3_PSC_DSCP;

        pMplsTCRmInfo->aRMIfInfo[u4Index].au1RMSupportedClassType[0] =
            TE_DS_CLASS_TYPE0;
        pMplsTCRmInfo->aRMIfInfo[u4Index].au1RMSupportedClassType[1] =
            TE_DS_CLASS_TYPE1;
    }
}

/*****************************************************************************/
/* Function Name : MplsRegisterWithIP                                         */
/* Description   : Function for registering with IP.                         */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : LDP_SUCCESS/LDP_FAILURE                                   */
/*****************************************************************************/
UINT1
MplsRegisterWithIP ()
{
    tNetIpRegInfo       RegInfo;

    MEMSET (&RegInfo, 0, sizeof (tNetIpRegInfo));

    RegInfo.u1ProtoId = MPLS_IP_PROTOCOL_ID;

    /* Ask for Interface Status Change and Route Change Events */
    RegInfo.u2InfoMask = (NETIPV4_ROUTECHG_REQ | NETIPV4_IFCHG_REQ);
    RegInfo.pProtoPktRecv = NULL;
    RegInfo.pRtChng = MplsIpRtChgEventHandler;
    RegInfo.pIfStChng = MplsIpIfChgEventHandler;

    if ((NetIpv4RegisterHigherLayerProtocol (&RegInfo)) == NETIPV4_FAILURE)
    {
        return MPLS_FAILURE;
    }
    return MPLS_SUCCESS;
}

/*****************************************************************************/
/* Function Name : MplsDeRegisterWithIP                                       */
/* Description   : Function for De-Registering with IP.                      */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : NETIPV4_SUCCESS/NETIPV4_FAILURE                                   */
/*****************************************************************************/
UINT1
MplsDeRegisterWithIP ()
{
    return ((UINT1) NetIpv4DeRegisterHigherLayerProtocol ((UINT1)
                                                          MPLS_IP_PROTOCOL_ID));
}

/*****************************************************************************/
/* Function Name : MplsDeInit                                                */
/* Description   : Initialization of global variables to all incarnations    */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
MplsDeInit (VOID)
{
    tTmrBlk            *pTmrBlk = NULL;
    UINT4               u4TimeLeft = MPLS_ZERO;

    MPLSFM_DBG (MPLSFM_MAIN_ETEXT, "MplsDeInit : ENTRY \n");

    /*Deletion of Incoming Q */
    if (gFmInQId != MPLS_ZERO)
    {
        MPLSFM_DBG (MPLSFM_MAIN_PRCS, "\nDeletion of Incoming Q\n");
        OsixQueDel (gFmInQId);
        gFmInQId = MPLS_ZERO;
    }

    /*Deletion of Incoming Q */
    if (gFmRtrChgQId != MPLS_ZERO)
    {
        MPLSFM_DBG (MPLSFM_MAIN_PRCS,
                    "\nDeletion of Route change event handle Q\n");
        OsixQueDel (gFmRtrChgQId);
        gFmRtrChgQId = MPLS_ZERO;
    }

    /*Deletion of Incoming Q */
    if (gFmIfChgQId != MPLS_ZERO)
    {
        MPLSFM_DBG (MPLSFM_MAIN_PRCS,
                    "\nDeletion of Interface change event handle Q\n");
        OsixQueDel (gFmIfChgQId);
        gFmIfChgQId = MPLS_ZERO;
    }

    /*Deletion of Incoming Q */
    if (gFmFrrQId != MPLS_ZERO)
    {
        MPLSFM_DBG (MPLSFM_MAIN_PRCS, "\nDeletion of FRR Q\n");
        OsixQueDel (gFmFrrQId);
        gFmFrrQId = MPLS_ZERO;
    }

    /*Deletion of Semaphore for Configuration threads Protection */
    if (gConfSemId != MPLS_ZERO)
    {
        MPLSFM_DBG (MPLSFM_MAIN_PRCS,
                    "\n Deletion of Configuration Thread Protection Semaphore\n");
        OsixSemDel (gConfSemId);
        gConfSemId = MPLS_ZERO;
    }

    /*Deletion of Semaphore for Data Structure Protection */
    if (gDsSemId != MPLS_ZERO)
    {
        MPLSFM_DBG (MPLSFM_MAIN_PRCS,
                    "\n Deletion of Data Structure Protection Semaphore\n");
        OsixSemDel (gDsSemId);
        gDsSemId = MPLS_ZERO;
    }

    pTmrBlk = &(MPLS_FTN_ARP_TMR_BLK (MPLS_DEF_INCARN));
    if (MplsGetRemainingTime (MPLS_TMR_LIST_ID (MPLS_DEF_INCARN),
                              &(pTmrBlk->TimerNode), &u4TimeLeft)
        == MPLS_SUCCESS)
    {
        TmrStopTimer (MPLS_TMR_LIST_ID (MPLS_DEF_INCARN),
                      &(pTmrBlk->TimerNode));
    }

    pTmrBlk = &(MPLS_ILM_ARP_TMR_BLK (MPLS_DEF_INCARN));
    if (MplsGetRemainingTime (MPLS_TMR_LIST_ID (MPLS_DEF_INCARN),
                              &(pTmrBlk->TimerNode), &u4TimeLeft)
        == MPLS_SUCCESS)
    {
        TmrStopTimer (MPLS_TMR_LIST_ID (MPLS_DEF_INCARN),
                      &(pTmrBlk->TimerNode));
    }

    pTmrBlk = &(MPLS_TNL_ARP_TMR_BLK (MPLS_DEF_INCARN));
    if (MplsGetRemainingTime (MPLS_TMR_LIST_ID (MPLS_DEF_INCARN),
                              &(pTmrBlk->TimerNode), &u4TimeLeft)
        == MPLS_SUCCESS)
    {
        TmrStopTimer (MPLS_TMR_LIST_ID (MPLS_DEF_INCARN),
                      &(pTmrBlk->TimerNode));
    }

    pTmrBlk = &(MPLS_PW_ARP_TMR_BLK (MPLS_DEF_INCARN));
    if (MplsGetRemainingTime (MPLS_TMR_LIST_ID (MPLS_DEF_INCARN),
                              &(pTmrBlk->TimerNode), &u4TimeLeft)
        == MPLS_SUCCESS)
    {
        TmrStopTimer (MPLS_TMR_LIST_ID (MPLS_DEF_INCARN),
                      &(pTmrBlk->TimerNode));
    }
#ifdef MPLS_L3VPN_WANTED
    pTmrBlk = &(MPLS_L3VPN_ARP_TMR_BLK (MPLS_DEF_INCARN));
    if (MplsGetRemainingTime (MPLS_TMR_LIST_ID (MPLS_DEF_INCARN),
                              &(pTmrBlk->TimerNode), &u4TimeLeft)
        == MPLS_SUCCESS)
    {
        TmrStopTimer (MPLS_TMR_LIST_ID (MPLS_DEF_INCARN),
                      &(pTmrBlk->TimerNode));
    }
#endif

    TmrDeleteTimerList (MPLS_TMR_LIST_ID (MPLS_DEF_INCARN));
    MPLS_TMR_LIST_ID (MPLS_DEF_INCARN) = NULL;

    OamMainDeInit ();

    MPLSFM_DBG (MPLSFM_MAIN_ETEXT, "MplsDeInit : EXIT \n");
}

/*****************************************************************************/
/* Function Name : MplsIsMplsInitialised                                     */
/* Description   : MPLS module initialisation status                         */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : TRUE/ FALSE                                               */
/*****************************************************************************/
BOOL1
MplsIsMplsInitialised ()
{
    if (gu1MplsInitialised == TRUE)
    {
        return TRUE;
    }
    return FALSE;
}

/*****************************************************************************/
/* Function Name : MplsShutdownModule                                        */
/* Description   : MPLS module deinitialization                              */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : MPLS_SUCCESS/ MPLS_FAILURE                                */
/*****************************************************************************/
INT4
MplsShutdownModule (VOID)
{
    tMplsRtEntryInfo   *pRouteInfo = NULL;
    tMplsIfEntryInfo   *pIfInfo = NULL;
    tMplsBufChainHeader *pMsg = NULL;

    MPLSFM_DBG (MPLSFM_MAIN_ETEXT, "MplsShutdownModule : ENTRY \n");
    gu1MplsInitialised = FALSE;

    while (MPLS_DEQUEUE (gFmIfChgQId, (UINT1 *) (&pIfInfo),
                         OSIX_DEF_MSG_LEN, MPLS_NO_WAIT) == OSIX_SUCCESS)
    {
        MemReleaseMemBlock (MPLS_IP_IF_INFO_POOL_ID, (UINT1 *) pIfInfo);
    }

    while (MPLS_DEQUEUE (gFmRtrChgQId, (UINT1 *) (&pRouteInfo),
                         OSIX_DEF_MSG_LEN, MPLS_NO_WAIT) == OSIX_SUCCESS)
    {
        MemReleaseMemBlock (MPLS_IP_ROUTE_INFO_POOL_ID, (UINT1 *) pRouteInfo);
    }
    while (MPLS_DEQUEUE (gFmFrrQId, (UINT1 *) (&pMsg),
                         OSIX_DEF_MSG_LEN, MPLS_NO_WAIT) == OSIX_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pMsg, FALSE);
    }

    while (MPLS_DEQUEUE (gFmInQId, (UINT1 *) (&pMsg),
                         OSIX_DEF_MSG_LEN, MPLS_NO_WAIT) == OSIX_SUCCESS)
    {
        MPLS_REL_BUF_CHAIN (pMsg, MPLS_BUF_NORMAL_REL);
    }

#ifdef L2RED_WANTED
#ifdef NPAPI_WANTED
    if (gMplsHwAuditTskId != 0)
    {
        OsixTskDel (gMplsHwAuditTskId);
        gMplsHwAuditTskId = 0;
    }
#endif
#endif

    MPLS_L2VPN_LOCK ();
    L2VpnDeInit ();
    MPLS_L2VPN_UNLOCK ();

#ifdef MPLS_LDP_WANTED
    /* User has issued "shutdown LDP" prior to reload/MPLS module 
     * shutdown 
     */
    if (IssGetModuleSystemControl (LDP_MODULE_ID) != MODULE_SHUTDOWN)
    {
        LdpShutDownProcess ();
    }
#endif

#ifdef MPLS_RSVPTE_WANTED
    /* User has issued "shutdown rsvpte" prior to reload/MPLS module 
     * shutdown 
     */
    if (IssGetModuleSystemControl (RSVPTE_MODULE_ID) != MODULE_SHUTDOWN)
    {
        MPLS_RSVPTE_LOCK ();
        RpteDeInitRsvpTe ();
        MPLS_RSVPTE_UNLOCK ();
    }
#endif

    MPLS_CMN_LOCK ();
#if defined(VPLS_GR_WANTED) || defined(LDP_GR_WANTED)
    MplsL2vpnHwListDestroy();
    MplsL2vpnVplsHwListDestroy();
#endif
    TeDeInit ();
    MplsCommondbDeInit ();
    MPLS_CMN_UNLOCK ();
    LblMgrShutDown ();
    TcShutdown ();
    MPLS_INDEXMGR_LOCK ();
    IndexMgrDeInit ();
    MPLS_INDEXMGR_UNLOCK ();

#ifdef L2RED_WANTED
    if (MplsRmDeInit () == MPLS_FAILURE)
    {
        MPLSFM_DBG (MPLSFM_MAIN_ETEXT,
                    "MplsShutdownModule : MPLS RM deinit failed \n");
        return MPLS_FAILURE;
    }
#endif
    MPLSFM_DBG (MPLSFM_MAIN_ETEXT, "MplsShutdownModule : EXIT \n");
    return MPLS_SUCCESS;
}

/*****************************************************************************/
/* Function Name : MplsStartModule                                           */
/* Description   : MPLS module memory allocations and timer creations etc.   */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : MPLS_SUCCESS/MPLS_FAILURE                                 */
/*****************************************************************************/
INT4
MplsStartModule (VOID)
{
    if (LblMgrInit () == LBL_FAILURE)
    {
        MPLSFM_DBG (MPLSFM_MAIN_PRCS, "\nLblMgrInit failed\n");
        return MPLS_FAILURE;
    }
    MPLS_CMN_LOCK ();
    if (MplsCmnDbInit () == MPLS_FAILURE)
    {
        MPLSFM_DBG (MPLSFM_MAIN_PRCS, "\nMplsCmnDbInit failed\n");
        MPLS_CMN_UNLOCK ();
        return MPLS_FAILURE;
    }
    MPLS_CMN_UNLOCK ();

    /* Setting the Config Parameters to the default value */
    TeSetCfgParams ();
    /* TC module entry point function is called */
    if (TcMain () == TC_FAILURE)
    {
        MPLSFM_DBG (MPLSFM_MAIN_PRCS,
                    "\nTcMain: Traffic controller init failed\n");
        return MPLS_FAILURE;
    }

    MplsTCdsInitRMIfInfo (&gDiffServRMGblInfo);

    MPLS_INDEXMGR_LOCK ();
    if (IndexManagerInit () == MPLS_FAILURE)
    {
        MPLSFM_DBG (MPLSFM_MAIN_PRCS, "\nIndexMgrInit failed\n");
        MPLS_INDEXMGR_UNLOCK ();
        return MPLS_FAILURE;
    }
    MPLS_INDEXMGR_UNLOCK ();

    MPLS_CMN_LOCK ();
    if (TeInit () == TE_FAILURE)
    {
        MPLSFM_DBG (MPLSFM_MAIN_PRCS, "\nTraffic engineering init failed\n");
        MPLS_CMN_UNLOCK ();
        return MPLS_FAILURE;
    }
    MPLS_CMN_UNLOCK ();

    /* For L2VPN Init */
    MPLS_L2VPN_LOCK ();
    if (L2VpnInit () == L2VPN_FAILURE)
    {
        MPLSFM_DBG (MPLSFM_MAIN_PRCS, "\nL2VpnInit failed\n");
        L2VpnDeInit ();
        MPLS_L2VPN_UNLOCK ();
        return MPLS_FAILURE;
    }
    MPLS_L2VPN_UNLOCK ();

#ifdef MPLS_LDP_WANTED
    /* For LDP Init */
    MPLS_LDP_LOCK ();
    if (LdpInit () == LDP_FAILURE)
    {
        MPLSFM_DBG (MPLSFM_MAIN_PRCS, "\nLdpInit failed\n");
        MPLS_LDP_UNLOCK ();
        return MPLS_FAILURE;
    }
    MPLS_LDP_UNLOCK ();
#endif

#ifdef MPLS_RSVPTE_WANTED
    /* For RPTE Init */
    MPLS_RSVPTE_LOCK ();
    if (RpteInitRsvpTe () == RPTE_FAILURE)
    {
        MPLSFM_DBG (MPLSFM_MAIN_PRCS, "\nRpteInitRsvpTe failed\n");
        MPLS_RSVPTE_UNLOCK ();
        return MPLS_FAILURE;
    }
    MPLS_RSVPTE_UNLOCK ();
#endif

#ifdef L2RED_WANTED
    if (MplsRmInit () == MPLS_FAILURE)
    {
        MPLSFM_DBG (MPLSFM_MAIN_PRCS, "\nMplsRmInit failed\n");
        return MPLS_FAILURE;
    }
#endif
    gu1MplsInitialised = TRUE;
    return MPLS_SUCCESS;
}

/*****************************************************************************/
/* Function Name : MplsTmrInitArpTmrDesc                                     */
/* Description   : Function used to Initialize the callback function for     */
/*                 handling Arp Resolution Timer Expiry for FTN, ILM, PW and */
/*                 Tunnel.                                                   */
/* Input(s)      : None.                                                     */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
MplsTmrInitArpTmrDesc ()
{
    MPLS_TIMER_DESC_OFFSET (MPLS_DEF_INCARN, MPLS_FTN_ARP_TIMER) = MPLS_ZERO;
    MPLS_TIMER_DESC_FUNC (MPLS_DEF_INCARN, MPLS_FTN_ARP_TIMER)
        = MplsHandleFtnArpResolveTmrEvent;

    MPLS_TIMER_DESC_OFFSET (MPLS_DEF_INCARN, MPLS_ILM_ARP_TIMER) = MPLS_ZERO;
    MPLS_TIMER_DESC_FUNC (MPLS_DEF_INCARN, MPLS_ILM_ARP_TIMER)
        = MplsHandleIlmArpResolveTmrEvent;

    MPLS_TIMER_DESC_OFFSET (MPLS_DEF_INCARN, MPLS_TNL_ARP_TIMER) = MPLS_ZERO;
    MPLS_TIMER_DESC_FUNC (MPLS_DEF_INCARN, MPLS_TNL_ARP_TIMER)
        = MplsHandleTnlArpResolveTmrEvent;

    MPLS_TIMER_DESC_OFFSET (MPLS_DEF_INCARN, MPLS_PW_ARP_TIMER) = MPLS_ZERO;
    MPLS_TIMER_DESC_FUNC (MPLS_DEF_INCARN, MPLS_PW_ARP_TIMER)
        = MplsHandlePwArpResolveTmrEvent;

    MPLS_TIMER_DESC_OFFSET (MPLS_DEF_INCARN, MPLS_TNL_RETRIG_TIMER)
        = MPLS_OFFSET (tTeTnlInfo, TnlRetrigTmr);
    MPLS_TIMER_DESC_FUNC (MPLS_DEF_INCARN, MPLS_TNL_RETRIG_TIMER)
        = TeTnlRetriggerTmrEvent;

#ifdef MPLS_L3VPN_WANTED
    MPLS_TIMER_DESC_OFFSET (MPLS_DEF_INCARN, MPLS_L3VPN_ARP_TIMER) = MPLS_ZERO;
    MPLS_TIMER_DESC_FUNC (MPLS_DEF_INCARN, MPLS_L3VPN_ARP_TIMER)
        = MplsL3VpnHandleEgressMapArpResolveTmrEvent;
#endif

}

/*****************************************************************************/
/* Function Name : MplsTriggerArpResolve                                     */
/* Description   : Function used to enqueue packets to ARP Module to resolve */
/*                 Arp and to start Arp Resolve Timer for FTN or ILM or TNL. */
/* Input(s)      : u4IpAddress      - IP Address of Next Hop for which ARP   */
/*                                    needs to be resolved.                  */
/*                 u1ArpTimerFlag   - A flag to indicate which ARP timer to  */
/*                                    start.                                 */
/*                                    1 - Arp Resolution Timer for FTN       */
/*                                    2 - Arp Resolution Timer for ILM       */
/*                                    3 - Arp Resolution Timer for Tnl Add   */
/*                                    4 - Arp Resolution Timer for PW        */
/* Output(s)     : None                                                      */
/* Return(s)     : MPLS_SUCCESS or MPLS_FAILURE                              */
/*****************************************************************************/
INT4
MplsTriggerArpResolve (UINT4 u4IpAddress, UINT1 u1ArpTimerFlag)
{
    tArpQMsg            ArpQMsg;
    tTimerListId        TimerListId;
    tTmrBlk            *pTmrBlk = NULL;
    UINT4               u4TimeLeft = MPLS_ZERO;
    tRtInfoQueryMsg     RtQuery;
    tNetIpv4RtInfo      NetIpRtInfo;

    MEMSET (&ArpQMsg, 0, sizeof (tArpQMsg));
    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (RtQuery));

    RtQuery.u4DestinationIpAddress = u4IpAddress;
    RtQuery.u4DestinationSubnetMask = 0xffffffff;
    RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
    if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_SUCCESS)
    {
        ArpQMsg.u4MsgType = ARP_APP_IF_MSG;
        ArpQMsg.u4IpAddr = u4IpAddress;
        ArpQMsg.u2Port = (UINT2) NetIpRtInfo.u4RtIfIndx;

        /* Trigger ARP module for Resolving NextHopAddress */
        if (ArpEnqueuePkt (&ArpQMsg) == ARP_FAILURE)
        {
            MPLSFM_DBG2 (MPLSFM_MAIN_PRCS,
                         "\rMplsTriggerArpResolve: ArpEnqueuePkt failed at line no. %d in fuction %s\n",
                         __LINE__, __func__);
        }
    }
    TimerListId = MPLS_TMR_LIST_ID (MPLS_DEF_INCARN);

    switch (u1ArpTimerFlag)
    {
        case MPLS_FTN_ARP_TIMER:
            pTmrBlk = &(MPLS_FTN_ARP_TMR_BLK (MPLS_DEF_INCARN));
            break;
        case MPLS_ILM_ARP_TIMER:
            pTmrBlk = &(MPLS_ILM_ARP_TMR_BLK (MPLS_DEF_INCARN));
            break;
        case MPLS_TNL_ARP_TIMER:
            pTmrBlk = &(MPLS_TNL_ARP_TMR_BLK (MPLS_DEF_INCARN));
            break;
        case MPLS_PW_ARP_TIMER:
            pTmrBlk = &(MPLS_PW_ARP_TMR_BLK (MPLS_DEF_INCARN));
            break;

        default:
            return MPLS_FAILURE;
    }

    if ((MplsGetRemainingTime (TimerListId,
                               &(pTmrBlk->TimerNode),
                               &u4TimeLeft) == MPLS_SUCCESS)
        && (u4TimeLeft == MPLS_ZERO))
    {
        if (TmrStart (TimerListId, pTmrBlk, u1ArpTimerFlag,
                      MPLS_ARP_RESOLVE_TIMEOUT, MPLS_ZERO) == TMR_FAILURE)
        {
            MPLSFM_DBG2 (MPLSFM_MAIN_PRCS,
                         "\rArp Resolve Timer start failed for %d %x\n",
                         u1ArpTimerFlag, u4IpAddress);
            return MPLS_FAILURE;
        }
    }

    return MPLS_SUCCESS;
}

#ifdef MPLS_L3VPN_WANTED
/*****************************************************************************/
/* Function Name : MplsL3VpnTriggerArpResolve                                */
/* Description   : Function used to enqueue packets to ARP Module to resolve */
/*                 Arp and to start Arp Resolve Timer for L3VPN.             */
/* Input(s)      : u4IpAddress      - IP Address of Next Hop for which ARP   */
/*                                    needs to be resolved.                  */
/*                 u4Port           - Port on which ARP is to be resolved    */
/*                                                                           */
/* Output(s)     : None                                                      */
/* Return(s)     : MPLS_SUCCESS or MPLS_FAILURE                              */
/*****************************************************************************/
INT4
MplsL3VpnTriggerArpResolve (UINT4 u4IpAddress, UINT4 u4Port)
{
    tArpQMsg            ArpQMsg;
    tTimerListId        TimerListId;
    tTmrBlk            *pTmrBlk = NULL;
    UINT4               u4TimeLeft = MPLS_ZERO;

    MEMSET (&ArpQMsg, 0, sizeof (tArpQMsg));

    ArpQMsg.u4MsgType = ARP_APP_IF_MSG;
    ArpQMsg.u4IpAddr = u4IpAddress;
    ArpQMsg.u2Port = (UINT2) u4Port;

    /* Trigger ARP module for Resolving NextHopAddress */
    if (ArpEnqueuePkt (&ArpQMsg) == ARP_FAILURE)
    {
        MPLSFM_DBG2 (MPLSFM_MAIN_PRCS,
                     "\rMplsL3VpnTriggerArpResolve: ArpEnqueuePkt failed at line no. %d in fuction %s\n",
                     __LINE__, __func__);
    }

    TimerListId = MPLS_TMR_LIST_ID (MPLS_DEF_INCARN);

    /*MPLS_L3VPN_ARP_TIMER */
    pTmrBlk = &(MPLS_L3VPN_ARP_TMR_BLK (MPLS_DEF_INCARN));

    if ((MplsGetRemainingTime (TimerListId,
                               &(pTmrBlk->TimerNode),
                               &u4TimeLeft) == MPLS_SUCCESS)
        && (u4TimeLeft == MPLS_ZERO))
    {
        if (TmrStart (TimerListId, pTmrBlk, MPLS_L3VPN_ARP_TIMER,
                      MPLS_ARP_RESOLVE_TIMEOUT, MPLS_ZERO) == TMR_FAILURE)
        {
            MPLSFM_DBG1 (MPLSFM_MAIN_PRCS,
                         "\rArp Resolve Timer start failed for %d %x\n",
                         u4IpAddress);
            return MPLS_FAILURE;
        }
    }

    return MPLS_SUCCESS;
}
#endif

/************************************************************************
 *  Function Name   : MplsGetRemainingTime
 *  Description     : Function used to get remaining time for the given
 *                    Timer.
 *  Input           : Timer List,Timer for which the remaining time has to
 *                    be fetched.
 *  Output          : Remaining Time.
 *  Returns         : L2VPN_SUCCESS on Success or else L2VPN_FAILURE
 ************************************************************************/
UINT1
MplsGetRemainingTime (tTimerListId TimerList, tTmrAppTimer * pTimer,
                      UINT4 *pTimeLeft)
{
    UINT4               u4TicksLeft = 0;
    UINT4               u4RetVal;

    u4RetVal = TmrGetRemainingTime (TimerList, pTimer, &u4TicksLeft);

    if (u4RetVal != TMR_SUCCESS)
    {
        return MPLS_FAILURE;
    }
    /* Convert ticks to secs */
    *pTimeLeft = u4TicksLeft / SYS_NUM_OF_TIME_UNITS_IN_A_SEC;

    return MPLS_SUCCESS;
}

/*****************************************************************************/
/* Function Name : MplsStartRequestedTimers                                  */
/* Description   : Function used to Start the Timers as requested by the     */
/*                 Applications.                                             */
/* Input(s)      : u1Event - Event to be set by the Application              */
/*                 ptr     - Void Pointer                                    */
/* Output(s)     : None                                                      */
/* Return(s)     : MPLS_SUCCESS/MPLS_FAILURE                                 */
/*****************************************************************************/
INT4
MplsStartRequestedTimers (UINT1 u1Event, VOID *ptr)
{
    tTimerListId        TimerListId;
    tTmrBlk            *pTmrBlk = NULL;
    UINT4               u4TmrId = MPLS_ZERO;
    UINT4               u4TimerValue = MPLS_ZERO;

    MPLSFM_DBG (MPLSFM_MAIN_PRCS, "MplsStartRequestedTimers: Entry\n");

    TimerListId = MPLS_TMR_LIST_ID (MPLS_DEF_INCARN);

    switch (u1Event)
    {
        case MPLS_RPTE_TNL_RETRIGGER_EVENT:
            pTmrBlk = (tTmrBlk *) ptr;
            u4TimerValue = TE_TNL_RETRIG_TIME;
            u4TmrId = MPLS_TNL_RETRIG_TIMER;
            MPLSFM_DBG (MPLSFM_MAIN_PRCS, "Tnl Retriggering Timer ");
            break;

        case MPLS_RPTE_MAX_WAIT_TMR_EVENT:

            pTmrBlk = &(MPLS_RPTE_GR_MAX_WAIT_TMR_BLK (MPLS_DEF_INCARN));
            u4TimerValue = MPLS_RPTE_GR_MAX_WAIT_TIME (MPLS_DEF_INCARN);
            u4TmrId = MPLS_RPTE_MAX_WAIT_TIMER;
            MPLSFM_DBG (MPLSFM_MAIN_PRCS, "Rpte Max Wait Timer ");
            break;

        default:
            MPLSFM_DBG (MPLSFM_MAIN_PRCS, "Invalid Event Passed.\n");
            MPLSFM_DBG (MPLSFM_MAIN_PRCS, "MplsStartRequestedTimers: Exit\n");
            return MPLS_FAILURE;
    }

    if (TmrStart (TimerListId, pTmrBlk, u4TmrId, u4TimerValue, MPLS_ZERO) ==
        TMR_FAILURE)
    {
        MPLSFM_DBG (MPLSFM_MAIN_PRCS, "Start Failed\n");
    }
    else
    {
        MPLSFM_DBG (MPLSFM_MAIN_PRCS, "Started\n");
    }

    MPLSFM_DBG (MPLSFM_MAIN_PRCS, "MplsStartRequestedTimers: Exit\n");
    return MPLS_SUCCESS;
}

/*****************************************************************************/
/* Function Name : MplsStopRequestedTimers                                   */
/* Description   : Function used to Stop the Timers as requested by the      */
/*                 Applications.                                             */
/* Input(s)      : u1Event - Event to be set by the Application              */
/*                 ptr     - Void Pointer                                    */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
MplsStopRequestedTimers (UINT1 u1Event, VOID *ptr)
{
    tTimerListId        TimerListId;
    tTmrBlk            *pTmrBlk = NULL;
    UINT4               u4TimeLeft = MPLS_ZERO;

    MPLSFM_DBG (MPLSFM_MAIN_PRCS, "MplsStopRequestedTimers: Entry\n");

    TimerListId = MPLS_TMR_LIST_ID (MPLS_DEF_INCARN);

    switch (u1Event)
    {
        case MPLS_RPTE_TNL_RETRIGGER_EVENT:

            pTmrBlk = (tTmrBlk *) ptr;
            MPLSFM_DBG (MPLSFM_MAIN_PRCS, "Tunnel Retrigger timer ");
            break;

        case MPLS_RPTE_MAX_WAIT_TMR_EVENT:

            pTmrBlk = &(MPLS_RPTE_GR_MAX_WAIT_TMR_BLK (MPLS_DEF_INCARN));
            MPLSFM_DBG (MPLSFM_MAIN_PRCS, "RpteGrMax Wait timer ");
            break;

        default:
            MPLSFM_DBG (MPLSFM_MAIN_PRCS, "Invalid Event Passed.\n");
            MPLSFM_DBG (MPLSFM_MAIN_PRCS, "MplsStopRequestedTimers: Exit\n");
            return;
    }

    if (MplsGetRemainingTime (TimerListId, &(pTmrBlk->TimerNode),
                              &u4TimeLeft) == MPLS_SUCCESS)
    {
        TmrStopTimer (TimerListId, &(pTmrBlk->TimerNode));
        MPLSFM_DBG (MPLSFM_MAIN_PRCS, "running - stopped\n");
    }
    else
    {
        MPLSFM_DBG (MPLSFM_MAIN_PRCS, "not running - not stopped\n");
    }

    MPLSFM_DBG (MPLSFM_MAIN_PRCS, "MplsStopRequestedTimers: Exit\n");
    return;
}

#if defined(VPLSADS_WANTED) || defined(MPLS_L3VPN_WANTED)
/*****************************************************************************/
/* Function Name : MplsBgp4NotifyASN                                         */
/* Description   : Callback function used to update ASN value in local MPLS  */
/*                 data base.                                                */
/* Input(s)      : u1AsnType  - ASN Type                                     */
/*                 u4AsnValue - ASN Value                                    */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
INT4
MplsBgp4NotifyASN (UINT1 u1AsnType, UINT4 u4AsnValue)
{
    MplsUpdateASN (u1AsnType, u4AsnValue);
    return MPLS_SUCCESS;
}

#endif

#ifdef MPLS_IPV6_WANTED

UINT1 
MplsRegisterWithIpv6()
{
       UINT4               u4Mask = 0;

       u4Mask = NETIPV6_ADDRESS_CHANGE|NETIPV6_ROUTE_CHANGE;
       if (NetIpv6RegisterHigherLayerProtocol (MPLS_IP_PROTOCOL_ID, u4Mask,
                               MplsIpv6RtChgEventHandler)
                       == NETIPV6_FAILURE)
       {
               return MPLS_FAILURE;
       }

       return MPLS_SUCCESS;
}

/*****************************************************************************/
/* Function Name : MplsDeRegisterWithIP                                      */
/* Description   : Function for De-Registering with IP.                      */
/* Input(s)      : None                                                      */
/* Output(s)     : None                                                      */
/* Return(s)     : NETIPV4_SUCCESS/NETIPV4_FAILURE                           */
/*****************************************************************************/
UINT1
MplsDeRegisterWithIpv6()
{
	return ((UINT1) NetIpv6DeRegisterHigherLayerProtocol ((UINT1)
				MPLS_IP_PROTOCOL_ID));
}

/*****************************************************************************/
/* Function Name : MplsStartArpResolveTimer                                  */
/* Description   : Function used to and to start Arp Resolve Timer for FTN   */
/*                 or ILM or TNL or PW.                                      */
/* Input(s)      : u1ArpTimerFlag   - A flag to indicate which ARP timer to  */
/*                                    start.                                 */
/*                                    1 - Arp Resolution Timer for FTN       */
/*                                    2 - Arp Resolution Timer for ILM       */
/*                                    3 - Arp Resolution Timer for Tnl Add   */
/*                                    4 - Arp Resolution Timer for PW        */
/* Output(s)     : None                                                      */
/* Return(s)     : MPLS_SUCCESS or MPLS_FAILURE                              */
/*****************************************************************************/
INT4
MplsStartArpResolveTimer (UINT1 u1ArpTimerFlag)
{
    tTimerListId        TimerListId;
    tTmrBlk            *pTmrBlk = NULL;
    UINT4               u4TimeLeft = MPLS_ZERO;

    TimerListId = MPLS_TMR_LIST_ID (MPLS_DEF_INCARN);

    switch (u1ArpTimerFlag)
    {
        case MPLS_FTN_ARP_TIMER:
            pTmrBlk = &(MPLS_FTN_ARP_TMR_BLK (MPLS_DEF_INCARN));
            break;
        case MPLS_ILM_ARP_TIMER:
            pTmrBlk = &(MPLS_ILM_ARP_TMR_BLK (MPLS_DEF_INCARN));
            break;
        case MPLS_TNL_ARP_TIMER:
            pTmrBlk = &(MPLS_TNL_ARP_TMR_BLK (MPLS_DEF_INCARN));
            break;
        case MPLS_PW_ARP_TIMER:
            pTmrBlk = &(MPLS_PW_ARP_TMR_BLK (MPLS_DEF_INCARN));
            break;

        default:
            return MPLS_FAILURE;
    }

    if ((MplsGetRemainingTime (TimerListId,
                               &(pTmrBlk->TimerNode),
                               &u4TimeLeft) == MPLS_SUCCESS)
        && (u4TimeLeft == MPLS_ZERO))
    {
        if (TmrStart (TimerListId, pTmrBlk, u1ArpTimerFlag,
                      MPLS_ARP_RESOLVE_TIMEOUT, MPLS_ZERO) == TMR_FAILURE)
        {
            MPLSFM_DBG1 (MPLSFM_MAIN_PRCS,
                         "\rArp Resolve Timer start failed for %d \n",
                         u1ArpTimerFlag);
            return MPLS_FAILURE;
        }
    }

    return MPLS_SUCCESS;
}

#endif
