/*---------------------------------------------------------------------------*/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: mpcmnext.c,v 1.26 2017/11/09 13:22:14 siva Exp $
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : mpcmnutl.c
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : MPLS
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  : 
 *    DESCRIPTION            : This file contains exported APIs that are 
 *                             used by external modules of MPLS.
 *---------------------------------------------------------------------------*/

#include "mplsincs.h"
#include "mplsapi.h"
#include "mplcmndb.h"
#include "mplsftn.h"
#include "mplslsr.h"
#include "l2vpextn.h"
#include "oaminc.h"
#include "arp.h"
#include "oamext.h"
#include "mpoamdef.h"
#include "teextrn.h"
#include "rpteext.h"

PRIVATE INT4        MplsCmnExtFillXcApiInfo (tXcEntry * pXcEntry,
                                             tMplsXcApiInfo * pXcApiInfo,
                                             eDirection Direction);
PRIVATE INT4        MplsCmnExtFillTunnelInfo (tTeTnlInfo * pTeTnlInfo,
                                              tMplsApiOutInfo *
                                              pOutMplsApiInfo);
PRIVATE INT4        MplsCmnExtFillLspInfoForFtn (tFtnEntry * pFtnEntry,
                                                 tMplsApiOutInfo *
                                                 pOutMplsApiInfo);
extern UINT4        MplsTunnelName[13];
/*****************************************************************************/
/* Function     : MplsCmnExtGetLspInfo                                       */
/*                                                                           */
/* Description  : This function provides the LSP information.                */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                                                                           */
/* Output       : pOutMplsApiInfo - Pointer to the output API information.   */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
MplsCmnExtGetLspInfo (tMplsApiInInfo * pInMplsApiInfo,
                      tMplsApiOutInfo * pOutMplsApiInfo)
{
    tFtnEntry          *pFtnEntry = NULL;
    tXcEntry           *pXcEntry = NULL;

    if (pOutMplsApiInfo == NULL)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsCmnExtGetLspInfo: "
                    "OUT API information should not be NULL : INTMD-EXIT \n");
        return OSIX_FAILURE;
    }

    pOutMplsApiInfo->u4SrcModId = pInMplsApiInfo->u4SrcModId;

    if (pInMplsApiInfo->u4SubReqType == MPLS_GET_LSP_INFO_FROM_XC_INDEX)
    {
        pXcEntry =
            MplsGetXCEntryByDirection (pInMplsApiInfo->InPathId.LspId.u4XCIndex,
                                       MPLS_DEF_DIRECTION);
        if (pXcEntry == NULL)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsCmnExtGetLspInfo "
                        "XC information/In Segment information "
                        "does not exist: INTMD-EXIT \n");
            return OSIX_FAILURE;
        }

        if (MplsCmnExtFillXcApiInfo (pXcEntry,
                                     &pOutMplsApiInfo->OutNonTeTnlInfo.
                                     XcApiInfo, MPLS_DEF_DIRECTION)
            == OSIX_FAILURE)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsCmnExtGetPathFromInLblInfo"
                        "Cross Connect information filling failed :"
                        "INTMD-EXIT \n");
            return OSIX_FAILURE;
        }
        pOutMplsApiInfo->u4ContextId = MPLS_DEF_CONTEXT_ID;
        pOutMplsApiInfo->u4PathType = MPLS_PATH_TYPE_NONTE_LSP;
    }
    else if (pInMplsApiInfo->u4SubReqType == MPLS_GET_FTN_INDEX_FROM_FEC)
    {
        /* Get the FTN info. from prefix FEC */
        pFtnEntry = MplsFwdGetFtnTableEntry (pInMplsApiInfo->InPathId.
                                             LspId.PeerAddr.u4_addr[0]);

        if (pFtnEntry == NULL)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsCmnExtGetLspInfo FTN entry doesnot exist:"
                        "INTMD-EXIT \n");
            return OSIX_FAILURE;
        }

        pOutMplsApiInfo->OutPathId.LspId.u4FtnIndex = pFtnEntry->u4FtnIndex;
    }
    else if (pInMplsApiInfo->u4SubReqType == MPLS_GET_LSP_INFO_FROM_FTN_INDEX)
    {
        pFtnEntry =
            MplsGetFtnTableEntry (pInMplsApiInfo->InPathId.LspId.u4FtnIndex);
        if (pFtnEntry == NULL)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsCmnExtGetLspInfo FTN entry doesnot exist:"
                        "INTMD-EXIT \n");
            return OSIX_FAILURE;
        }
        if (MplsCmnExtFillLspInfoForFtn (pFtnEntry, pOutMplsApiInfo)
            == OSIX_FAILURE)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsCmnExtGetLspInfo"
                        "MPLS Filling LSP info. for FTN failed: INTMD-EXIT \n");
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : MplsCmnExtGetNodeId                                        */
/*                                                                           */
/* Description  : This function provides the node identifiers based          */
/*                on the given information.                                  */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                                                                           */
/* Output       : pOutMplsApiInfo - Pointer to the output API information.   */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
MplsCmnExtGetNodeId (tMplsApiInInfo * pInMplsApiInfo,
                     tMplsApiOutInfo * pOutMplsApiInfo)
{
    tOamFsMplsTpGlobalConfigEntry OamFsMplsTpGlobalConfigEntry;
    tOamFsMplsTpNodeMapEntry OamFsMplsTpNodeMapEntry;
    tOamFsMplsTpNodeMapEntry *pOamFsMplsTpNodeMapEntry = NULL;
    tOamFsMplsTpGlobalConfigEntry *pOamFsMplsTpGlobalConfigEntry = NULL;

    if (pOutMplsApiInfo == NULL)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsCmnExtGetNodeId: "
                    "OUT API information should not be NULL : INTMD-EXIT \n");
        return OSIX_FAILURE;
    }

    /* Other than router-id, it is expected to check the OAM module status */
    if (pInMplsApiInfo->u4SubReqType != MPLS_GET_ROUTER_ID)
    {
        if (OamUtilIsOamModuleEnabled (pInMplsApiInfo->u4ContextId)
            == OSIX_FAILURE)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsOamUpdateSessionParams: "
                        "OAM module is disabled: INTMD-EXIT \n");
            return OSIX_FAILURE;
        }
    }
    switch (pInMplsApiInfo->u4SubReqType)
    {
        case MPLS_GET_ROUTER_ID:
            /* Get the MPLS router-id */
            if ((MplsCmnExtGetRouterId (pInMplsApiInfo, pOutMplsApiInfo))
                == OSIX_FAILURE)
            {
                MPLSFM_DBG (MPLSFM_IF_FAILURE,
                            "MplsCmnExtGetNodeId"
                            "Failed to get the Router ID"
                            "failed: INTMD-EXIT \n");
                return OSIX_FAILURE;
            }
            break;
        case MPLS_GET_GLOBAL_NODE_ID:
            MEMSET (&OamFsMplsTpGlobalConfigEntry, 0,
                    sizeof (tOamFsMplsTpGlobalConfigEntry));
            /* Assign the index */
            OamFsMplsTpGlobalConfigEntry.MibObject.u4FsMplsTpContextId =
                pInMplsApiInfo->u4ContextId;

            pOamFsMplsTpGlobalConfigEntry = OamGetFsMplsTpGlobalConfigTable
                (&OamFsMplsTpGlobalConfigEntry);
            if (pOamFsMplsTpGlobalConfigEntry == NULL)
            {
                MPLSFM_DBG (MPLSFM_IF_FAILURE,
                            "MplsCmnExtGetNodeId"
                            "GlobalId::NodeId get from global config table "
                            "failed: INTMD-EXIT \n");
                return OSIX_FAILURE;
            }
            pOutMplsApiInfo->OutNodeId.u4GlobalId =
                pOamFsMplsTpGlobalConfigEntry->MibObject.u4FsMplsTpGlobalId;
            pOutMplsApiInfo->OutNodeId.u4NodeId =
                pOamFsMplsTpGlobalConfigEntry->MibObject.
                u4FsMplsTpNodeIdentifier;
            break;
        case MPLS_GET_GLBNODEID_FROM_LOCALNUM:
            /* Assign the indices */
            OamFsMplsTpNodeMapEntry.MibObject.u4FsMplsTpContextId =
                pInMplsApiInfo->u4ContextId;

            OamFsMplsTpNodeMapEntry.MibObject.u4FsMplsTpNodeMapLocalNum =
                pInMplsApiInfo->InLocalNum;

            pOamFsMplsTpNodeMapEntry = OamGetFsMplsTpNodeMapTable
                (&OamFsMplsTpNodeMapEntry);
            if (pOamFsMplsTpNodeMapEntry == NULL)
            {
                MPLSFM_DBG (MPLSFM_IF_FAILURE,
                            "MplsCmnExtGetNodeId"
                            "GlobalId::NodeId get from local number failed:"
                            "INTMD-EXIT \n");
                return OSIX_FAILURE;
            }
            pOutMplsApiInfo->OutNodeId.u4GlobalId =
                pOamFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpNodeMapGlobalId;
            pOutMplsApiInfo->OutNodeId.u4NodeId =
                pOamFsMplsTpNodeMapEntry->MibObject.u4FsMplsTpNodeMapNodeId;
            break;
        case MPLS_GET_LOCALNUM_FROM_GLBNODEID:
            /* Get Global Id and Node Id from local map number */
            if (OamUtilGetLocalMapNum (pInMplsApiInfo->u4ContextId,
                                       pInMplsApiInfo->InNodeId.
                                       MplsGlobalNodeId.u4GlobalId,
                                       pInMplsApiInfo->InNodeId.
                                       MplsGlobalNodeId.u4NodeId,
                                       &pOutMplsApiInfo->OutLocalNum)
                == OSIX_FAILURE)
            {
                MPLSFM_DBG (MPLSFM_IF_FAILURE,
                            "MplsCmnExtGetNodeId"
                            "Local number get from GlobalId::NodeId failed:"
                            "INTMD-EXIT \n");
                return OSIX_FAILURE;
            }
            break;
        default:
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsCmnExtGetNodeId Invalid sub request type:"
                        "INTMD-EXIT \n");
            return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : MplsCmnExtGetPathFromInLblInfo                             */
/*                                                                           */
/* Description  : This function provides the path information based on the   */
/*                given incoming interface and incoming label.               */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                pOutMplsApiInfo - Pointer to the output API information    */
/*                pbIsTnlOrLspExists - Pointer to the Tnl or LSP exists flag */
/*                                                                           */
/* Output       : pOutMplsApiInfo - Pointer to the output API information.   */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
MplsCmnExtGetPathFromInLblInfo (tMplsApiInInfo * pInMplsApiInfo,
                                tMplsApiOutInfo * pOutMplsApiInfo,
                                BOOL1 * pbIsTnlOrLspExists)
{
    tInSegment         *pInSegment = NULL;
    tXcEntry           *pXcEntry = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;

    /* Search the in segment table for the matching in label 
     * and incoming interface */
    pInSegment = MplsSignalGetInSegmentTableEntry (pInMplsApiInfo->InInLblInfo.
                                                   u4InIf,
                                                   pInMplsApiInfo->InInLblInfo.
                                                   u4Inlabel);

    if (pInSegment != NULL)
    {
        /* Tunnel or LSP entry exists */
        *pbIsTnlOrLspExists = TRUE;
    }
    else
    {
        /* Tunnel or LSP entry doesn't exist */
        *pbIsTnlOrLspExists = FALSE;
        return OSIX_SUCCESS;
    }

    pOutMplsApiInfo->u4SrcModId = pInMplsApiInfo->u4SrcModId;

    /* If XC is NULL return failure from here */
    if (pInSegment->pXcIndex == NULL)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsCmnExtGetPathFromInLblInfo"
                    "Cross Connect information doesn't exist:" "INTMD-EXIT \n");
        return OSIX_FAILURE;
    }

    pXcEntry = pInSegment->pXcIndex;
    /* Check whether the tunnel is associated with the XC information */
    if (pXcEntry->pTeTnlInfo == NULL)
    {
        pXcEntry = pInSegment->pXcIndex;
        /* Check the Tunnel pointer, if tnl exists return tnl information 
         * Else LSP information */
        if (MplsCmnExtFillXcApiInfo (pXcEntry,
                                     &pOutMplsApiInfo->OutNonTeTnlInfo.
                                     XcApiInfo, MPLS_DEF_DIRECTION)
            == OSIX_FAILURE)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsCmnExtGetPathFromInLblInfo"
                        "Cross Connect information filling failed :"
                        "INTMD-EXIT \n");
            return OSIX_FAILURE;
        }
        pOutMplsApiInfo->u4ContextId = MPLS_DEF_CONTEXT_ID;
        pOutMplsApiInfo->u4PathType = MPLS_PATH_TYPE_NONTE_LSP;
    }
    else
    {
        pOutMplsApiInfo->u4ContextId = MPLS_DEF_CONTEXT_ID;
        pOutMplsApiInfo->u4PathType = MPLS_PATH_TYPE_TUNNEL;

        pTeTnlInfo = pXcEntry->pTeTnlInfo;
        if ((pTeTnlInfo->u4MegIndex != 0) &&
            (pTeTnlInfo->u4MeIndex != 0) && (pTeTnlInfo->u4MpIndex != 0))
        {
            pInMplsApiInfo->u4SubReqType = MPLS_GET_MEG_INFO_FROM_MEG_ID;
            pInMplsApiInfo->u4ContextId = MPLS_DEF_CONTEXT_ID;
            /* Assign the MEG indices */
            pInMplsApiInfo->InPathId.MegId.u4MegIndex = pTeTnlInfo->u4MegIndex;
            pInMplsApiInfo->InPathId.MegId.u4MeIndex = pTeTnlInfo->u4MeIndex;
            pInMplsApiInfo->InPathId.MegId.u4MpIndex = pTeTnlInfo->u4MpIndex;

            if (MplsOamIfGetMegInfo (pInMplsApiInfo, pOutMplsApiInfo)
                == OSIX_FAILURE)
            {
                MPLSFM_DBG (MPLSFM_IF_FAILURE,
                            "MplsCmnExtGetPathFromInLblInfo"
                            "Filling MEG information failed:" "INTMD-EXIT \n");
                return OSIX_FAILURE;
            }
        }
        else if (MplsCmnExtFillTunnelInfo (pXcEntry->pTeTnlInfo,
                                           pOutMplsApiInfo) == OSIX_FAILURE)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsCmnExtGetPathFromInLblInfo"
                        "Filling tunnel information failed:" "INTMD-EXIT \n");
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : MplsCmnExtGetRevPathFromFwdPath                            */
/*                                                                           */
/* Description  : This function provides the reverse path information for    */
/*                the given forward path.                                    */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                                                                           */
/* Output       : pOutMplsApiInfo - Pointer to the output API information.   */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
MplsCmnExtGetRevPathFromFwdPath (tMplsApiInInfo * pInMplsApiInfo,
                                 tMplsApiOutInfo * pOutMplsApiInfo)
{
    tFtnEntry          *pFtnEntry = NULL;

    /*  Unidirectional tunnel (Applicable case - This is only 
     *                                           expected here)
     *    - Need to fetch the tunnel that goes to source of the 
     *      application packet in the FTN table
     *  Co-routed bi-directional tunnel (Not applicable case)
     *    - XC information provides forward and reverse LSP
     *  Associated bi-directional tunnel (Not applicable case)
     *    - Destination tunnel information will be provided in the
     *      forward tunnel information itself.
     *   */

    /* Given forward LSP, getting reverse LSP information is 
     * NOT SUPPORTED 
     *
     * Other path type (PW) is not applicable as PW itself is
     * bi-directional.
     * */
    if (pOutMplsApiInfo == NULL)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsCmnExtGetRevPathFromFwdPath: "
                    "OUT API information should not be NULL" ": INTMD-EXIT \n");
        return OSIX_FAILURE;
    }

    if (pInMplsApiInfo->InNodeId.u4NodeType != MPLS_ADDR_TYPE_IPV4)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsCmnExtGetRevPathFromFwdPath: "
                    "Invalid source address type: INTMD-EXIT \n");
        return OSIX_FAILURE;
    }
    if (pInMplsApiInfo->InNodeId.MplsRouterId.u4_addr[0] != 0)
    {
        pFtnEntry = MplsFwdGetFtnTableEntry (pInMplsApiInfo->InNodeId.
                                             MplsRouterId.u4_addr[0]);
        if (pFtnEntry == NULL)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsCmnExtGetRevPathFromFwdPath: "
                        "MPLS FTN entry does not exist: INTMD-EXIT \n");
            return OSIX_FAILURE;
        }

        pOutMplsApiInfo->u4SrcModId = pInMplsApiInfo->u4SrcModId;

        if (MplsCmnExtFillLspInfoForFtn (pFtnEntry, pOutMplsApiInfo)
            == OSIX_FAILURE)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsCmnExtGetRevPathFromFwdPath: "
                        "MPLS Filling LSP info. for FTN failed: INTMD-EXIT \n");
            return OSIX_FAILURE;
        }
    }
    else
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsCmnExtGetRevPathFromFwdPath: "
                    "Invalid source address: INTMD-EXIT \n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : MplsCmnExtGetServicePointerOid                             */
/*                                                                           */
/* Description  : This function provides the first accessible column         */
/*                OID of the table with/without indices.                     */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                                                                           */
/* Output       : pOutMplsApiInfo - Pointer to the output API information.   */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
MplsCmnExtGetServicePointerOid (tMplsApiInInfo * pInMplsApiInfo,
                                tMplsApiOutInfo * pOutMplsApiInfo)
{
    tMplsApiOutInfo    *pOutMplsApiInfoTemp = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tFtnEntry          *pFtnEntry = NULL;
    UINT4               u4TnlIngressId = 0;
    UINT4               u4TnlEgressId = 0;
    UINT4               u4OidLen = 0;
    INT4                i4RetVal = OSIX_FAILURE;

    if (pOutMplsApiInfo == NULL)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsCmnExtGetServicePointerOid: "
                    "OUT API information should not be NULL" ": INTMD-EXIT \n");
        return OSIX_FAILURE;
    }

    if ((pInMplsApiInfo->u4SubReqType == MPLS_GET_BASE_OID_FROM_MEG_NAME) ||
        (pInMplsApiInfo->u4SubReqType == MPLS_GET_BASE_MEG_OID))
    {
        if (OamUtilIsOamModuleEnabled (pInMplsApiInfo->u4ContextId)
            == OSIX_FAILURE)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsOamUpdateSessionParams: "
                        "OAM module is disabled: INTMD-EXIT \n");
            return OSIX_FAILURE;
        }
    }

    pOutMplsApiInfoTemp =
        (tMplsApiOutInfo *) MemAllocMemBlk (MPLS_API_OUT_INFO_POOL_ID);
    if (pOutMplsApiInfoTemp == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pOutMplsApiInfoTemp, 0, sizeof (tMplsApiOutInfo));

    switch (pInMplsApiInfo->u4SubReqType)
    {
            /* Get the first accessible object from ME table with index */
        case MPLS_GET_BASE_OID_FROM_MEG_NAME:
            /* Get the Meg indices from MEG name */
            i4RetVal = MplsOamIfGetMegIdFromMegName (pInMplsApiInfo,
                                                     pOutMplsApiInfoTemp);
            if (i4RetVal == OSIX_FAILURE)
            {
                MPLSFM_DBG (MPLSFM_IF_FAILURE,
                            "MplsCmnExtGetServicePointerOid"
                            "MEG indices get from MEG, ME names failed:"
                            "INTMD-EXIT \n");
                break;
            }
            MEMCPY (pOutMplsApiInfo->OutServiceOid.au4ServiceOidList,
                    FsMplsTpMeName, sizeof (FsMplsTpMeName));
            /* Get the exact no. of OIDs */
            u4OidLen = sizeof (FsMplsTpMeName) / sizeof (UINT4);

            pOutMplsApiInfo->OutServiceOid.au4ServiceOidList[u4OidLen] =
                pOutMplsApiInfoTemp->u4ContextId;
            u4OidLen = u4OidLen + 1;
            pOutMplsApiInfo->OutServiceOid.au4ServiceOidList[u4OidLen] =
                pOutMplsApiInfoTemp->OutPathId.MegId.u4MegIndex;
            u4OidLen = u4OidLen + 1;
            pOutMplsApiInfo->OutServiceOid.au4ServiceOidList[u4OidLen] =
                pOutMplsApiInfoTemp->OutPathId.MegId.u4MeIndex;
            u4OidLen = u4OidLen + 1;
            pOutMplsApiInfo->OutServiceOid.au4ServiceOidList[u4OidLen] =
                pOutMplsApiInfoTemp->OutPathId.MegId.u4MpIndex;
            u4OidLen = u4OidLen + 1;
            /* Assign the Service OID length */
            pOutMplsApiInfo->OutServiceOid.u2ServiceOidLen = (UINT2) u4OidLen;
            break;

            /* Get the first accessible object from Tunnel table with index */
        case MPLS_GET_BASE_OID_FROM_TNL_INDEX:
            if ((pInMplsApiInfo->InPathId.TnlId.u4SrcTnlNum != 0) &&
                (pInMplsApiInfo->InPathId.TnlId.u4LspNum != 0) &&
                (pInMplsApiInfo->InPathId.TnlId.SrcNodeId.
                 MplsRouterId.u4_addr[0] != 0) &&
                (pInMplsApiInfo->InPathId.TnlId.DstNodeId.
                 MplsRouterId.u4_addr[0] != 0))
            {
                /* All tunnel table indices are given, so just get 
                   the tunnel entry indices and append with 
                   the base tunnel object OID */
                pTeTnlInfo = TeGetTunnelInfo (pInMplsApiInfo->InPathId.
                                              TnlId.u4SrcTnlNum,
                                              pInMplsApiInfo->InPathId.
                                              TnlId.u4LspNum,
                                              pInMplsApiInfo->InPathId.
                                              TnlId.SrcNodeId.
                                              MplsRouterId.u4_addr[0],
                                              pInMplsApiInfo->InPathId.
                                              TnlId.DstNodeId.
                                              MplsRouterId.u4_addr[0]);
            }
            else
            {
                /* Partial tunnel indices can be provided from external modules
                 * to get the complete the indices */
                pTeTnlInfo =
                    TeGetTnlInfoByOwnerAndRole (pInMplsApiInfo->InPathId.TnlId.
                                                u4SrcTnlNum,
                                                pInMplsApiInfo->InPathId.TnlId.
                                                u4LspNum,
                                                pInMplsApiInfo->InPathId.TnlId.
                                                SrcNodeId.MplsRouterId.
                                                u4_addr[0],
                                                pInMplsApiInfo->InPathId.TnlId.
                                                DstNodeId.MplsRouterId.
                                                u4_addr[0], TE_TNL_OWNER_SNMP,
                                                0);
            }

            if (pTeTnlInfo == NULL)
            {
                MPLSFM_DBG (MPLSFM_IF_FAILURE,
                            "MplsCmnExtGetServicePointerOid"
                            "Tunnel entry doesnot exist: " "INTMD-EXIT \n");
                i4RetVal = OSIX_FAILURE;
                break;
            }
            i4RetVal = OSIX_SUCCESS;
            MEMCPY (pOutMplsApiInfo->OutServiceOid.au4ServiceOidList,
                    MplsTunnelName, sizeof (MplsTunnelName));
            u4OidLen = sizeof (MplsTunnelName) / sizeof (UINT4);

            pOutMplsApiInfo->OutServiceOid.au4ServiceOidList[u4OidLen] =
                pTeTnlInfo->u4TnlIndex;
            u4OidLen = u4OidLen + 1;

            pOutMplsApiInfo->OutServiceOid.au4ServiceOidList[u4OidLen] =
                pTeTnlInfo->u4TnlInstance;
            u4OidLen = u4OidLen + 1;

            MEMCPY (&u4TnlIngressId, &pTeTnlInfo->TnlIngressLsrId,
                    sizeof (UINT4));
            u4TnlIngressId = OSIX_NTOHL (u4TnlIngressId);
            pOutMplsApiInfo->OutServiceOid.au4ServiceOidList[u4OidLen] =
                u4TnlIngressId;
            u4OidLen = u4OidLen + 1;

            MEMCPY (&u4TnlEgressId, &pTeTnlInfo->TnlEgressLsrId,
                    sizeof (UINT4));
            u4TnlEgressId = OSIX_NTOHL (u4TnlEgressId);
            pOutMplsApiInfo->OutServiceOid.au4ServiceOidList[u4OidLen] =
                u4TnlEgressId;
            u4OidLen = u4OidLen + 1;

            /* Assign the Service OID length */
            pOutMplsApiInfo->OutServiceOid.u2ServiceOidLen = (UINT2) u4OidLen;
            break;

            /* Get the first accessible object from FTN table with index */
        case MPLS_GET_BASE_OID_FROM_FEC:
            pFtnEntry = MplsFwdGetFtnTableEntry (pInMplsApiInfo->
                                                 InPathId.LspId.PeerAddr.
                                                 u4_addr[0]);
            if (pFtnEntry == NULL)
            {
                MPLSFM_DBG (MPLSFM_IF_FAILURE,
                            "MplsCmnExtGetServicePointerOid"
                            "FTN entry doesnot exist: " "INTMD-EXIT \n");
                i4RetVal = OSIX_FAILURE;
                break;
            }
            i4RetVal = OSIX_SUCCESS;
            MEMCPY (pOutMplsApiInfo->OutServiceOid.au4ServiceOidList,
                    MplsFTNRowStatus, sizeof (MplsFTNRowStatus));
            u4OidLen = sizeof (MplsFTNRowStatus) / sizeof (UINT4);
            pOutMplsApiInfo->OutServiceOid.au4ServiceOidList[u4OidLen] =
                pFtnEntry->u4FtnIndex;
            u4OidLen = u4OidLen + 1;

            /* Assign the Service OID length */
            pOutMplsApiInfo->OutServiceOid.u2ServiceOidLen = (UINT2) u4OidLen;
            break;

            /* Get the first accessible object from FTN table without index */
        case MPLS_GET_BASE_FTN_OID:
            MEMCPY (pOutMplsApiInfo->OutServiceOid.au4ServiceOidList,
                    MplsFTNRowStatus, sizeof (MplsFTNRowStatus));

            u4OidLen = sizeof (MplsFTNRowStatus) / sizeof (UINT4);
            /* Assign the Service OID length */
            pOutMplsApiInfo->OutServiceOid.u2ServiceOidLen = (UINT2) u4OidLen;
            i4RetVal = OSIX_SUCCESS;
            break;

            /* Get the first accessible object from Tunnel table without index */
        case MPLS_GET_BASE_TNL_OID:
            MEMCPY (pOutMplsApiInfo->OutServiceOid.au4ServiceOidList,
                    MplsTunnelName, sizeof (MplsTunnelName));
            u4OidLen = sizeof (MplsTunnelName) / sizeof (UINT4);
            /* Assign the Service OID length */
            pOutMplsApiInfo->OutServiceOid.u2ServiceOidLen = (UINT2) u4OidLen;
            i4RetVal = OSIX_SUCCESS;
            break;

            /* Get the first accessible object from ME table without index */
        case MPLS_GET_BASE_MEG_OID:
            MEMCPY (pOutMplsApiInfo->OutServiceOid.au4ServiceOidList,
                    FsMplsTpMeName, sizeof (FsMplsTpMeName));
            u4OidLen = sizeof (FsMplsTpMeName) / sizeof (UINT4);
            /* Assign the Service OID length */
            pOutMplsApiInfo->OutServiceOid.u2ServiceOidLen = (UINT2) u4OidLen;
            i4RetVal = OSIX_SUCCESS;
            break;

        default:
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsCmnExtGetServicePointerOid : "
                        "Invalid Sub-request type: INTMD-EXIT \n");
            break;
    }
    MemReleaseMemBlock (MPLS_API_OUT_INFO_POOL_ID,
                        (UINT1 *) pOutMplsApiInfoTemp);
    return i4RetVal;
}

/*****************************************************************************/
/* Function     : MplsCmnExtGetTunnelInfo                                    */
/*                                                                           */
/* Description  : This function gets the tunnel information based on the     */
/*                tunnel indices.                                            */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                                                                           */
/* Output       : pOutMplsApiInfo - Pointer to the output API information.   */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
MplsCmnExtGetTunnelInfo (tMplsApiInInfo * pInMplsApiInfo,
                         tMplsApiOutInfo * pOutMplsApiInfo)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
#ifdef RFC6374_WANTED
    tFtnEntry          *pFtnEntry = NULL;
    tOutSegment        *pOutSegment = NULL;
    tXcEntry           *pXcEntry = NULL;
#endif
    UINT4               u4TnlIndex = 0;
    UINT4               u4TnlInst = 0;
    UINT4               u4TnlIngressId = 0;
    UINT4               u4TnlEgressId = 0;

    if (pOutMplsApiInfo == NULL)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsCmnExtGetTunnelInfo: "
                    "OUT API information should not be NULL : INTMD-EXIT \n");
        return OSIX_FAILURE;
    }
    /* Get the Source Module ID */
    pOutMplsApiInfo->u4SrcModId = pInMplsApiInfo->u4SrcModId;

    u4TnlIndex = pInMplsApiInfo->InPathId.TnlId.u4SrcTnlNum;
    u4TnlInst = pInMplsApiInfo->InPathId.TnlId.u4LspNum;

#ifdef RFC6374_WANTED
    /* The below will be used by RFC6374 to get the 
     * Out Label, Interface Index
     * next Hop Mac and Label stack count 
     * to construct the label information where the 6374 
     * the service was created over LDP[dynamic tunnel entries got created] */
    if (u4TnlIndex == MPLS_ZERO)
    {
        pFtnEntry = MplsSignalGetFtnTableEntry
            (pInMplsApiInfo->InPathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0]);

        if (pFtnEntry != NULL)
        {
            pXcEntry = pFtnEntry->pActionPtr;
            if (pXcEntry == NULL)
            {
                MPLSFM_DBG (MPLSFM_IF_FAILURE, "MplsCmnExtGetTunnelInfo"
                            "XC information does not exist: INTMD-EXIT \n");
                return OSIX_FAILURE;
            }
            pOutSegment = pXcEntry->pOutIndex;

            if (pOutSegment == NULL)
            {
                MPLSFM_DBG (MPLSFM_IF_FAILURE, "MplsCmnExtGetTunnelInfo"
                            "Out Segment information does not exist: INTMD-EXIT \n");
                return OSIX_FAILURE;
            }
            /* Out Label index */
            pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.u4SrcTnlNum =
                pOutSegment->u4Label;
            /* Out Interface index */
            pOutMplsApiInfo->OutTeTnlInfo.u4TnlIfIndex = pOutSegment->u4IfIndex;
            /* Net Hop MAC address */
            MEMCPY (pOutMplsApiInfo->OutTeTnlInfo.au1NextHopMac,
                    pOutSegment->au1NextHopMac, MAC_ADDR_LEN);
            /* Label stack count information is not available either in 
             * pOutSegment or pFtnEntry or pXcEntry structure but it is 
             * the mandatory parameter for R6374 to construct the PDU. 
             * So it is assumned and hard coded as one */
            pOutMplsApiInfo->OutTeTnlInfo.XcApiInfo.FwdOutSegInfo.u1LblStkCnt =
                MPLS_ONE;
        }
        return OSIX_SUCCESS;
    }
#endif
    /* Get Tunnel indices */
    if ((pInMplsApiInfo->InPathId.TnlId.SrcNodeId.u4NodeType ==
         MPLS_ADDR_TYPE_IPV4) &&
        (pInMplsApiInfo->InPathId.TnlId.DstNodeId.u4NodeType ==
         MPLS_ADDR_TYPE_IPV4))
    {
        u4TnlIngressId = pInMplsApiInfo->InPathId.TnlId.SrcNodeId.
            MplsRouterId.u4_addr[0];
        u4TnlEgressId = pInMplsApiInfo->InPathId.TnlId.DstNodeId.
            MplsRouterId.u4_addr[0];
    }
    else if ((pInMplsApiInfo->InPathId.TnlId.SrcNodeId.u4NodeType ==
              MPLS_ADDR_TYPE_GLOBAL_NODE_ID) &&
             (pInMplsApiInfo->InPathId.TnlId.DstNodeId.u4NodeType ==
              MPLS_ADDR_TYPE_GLOBAL_NODE_ID))
    {
        /* Fetch the source and destination local map numbers and then fetch 
         * the tunnel information */
        if (OamUtilGetLocalMapNum (pInMplsApiInfo->u4ContextId,
                                   pInMplsApiInfo->InPathId.TnlId.SrcNodeId.
                                   MplsGlobalNodeId.u4GlobalId,
                                   pInMplsApiInfo->InPathId.TnlId.SrcNodeId.
                                   MplsGlobalNodeId.u4NodeId, &u4TnlIngressId)
            == OSIX_FAILURE)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsCmnExtGetTunnelInfo"
                        "Local number for Source GlobalId::NodeId does not "
                        "exist: INTMD-EXIT \n");
            return OSIX_FAILURE;
        }
        if (OamUtilGetLocalMapNum (pInMplsApiInfo->u4ContextId,
                                   pInMplsApiInfo->InPathId.TnlId.DstNodeId.
                                   MplsGlobalNodeId.u4GlobalId,
                                   pInMplsApiInfo->InPathId.TnlId.DstNodeId.
                                   MplsGlobalNodeId.u4NodeId, &u4TnlEgressId)
            == OSIX_FAILURE)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsCmnExtGetTunnelInfo"
                        "Local number for Destination GlobalId::NodeId "
                        "does not exist: INTMD-EXIT \n");
            return OSIX_FAILURE;
        }
    }
    else if (pInMplsApiInfo->InPathId.TnlId.SrcNodeId.u4NodeType ==
             MPLS_ADDR_TYPE_GLOBAL_NODE_ID)
    {
        /* Fetch the source and destination local map numbers and then fetch 
         * the tunnel information */
        if (OamUtilGetLocalMapNum (pInMplsApiInfo->u4ContextId,
                                   pInMplsApiInfo->InPathId.TnlId.SrcNodeId.
                                   MplsGlobalNodeId.u4GlobalId,
                                   pInMplsApiInfo->InPathId.TnlId.SrcNodeId.
                                   MplsGlobalNodeId.u4NodeId, &u4TnlIngressId)
            == OSIX_FAILURE)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsCmnExtGetTunnelInfo"
                        "Local number for Source GlobalId::NodeId does not "
                        "exist: INTMD-EXIT \n");
            return OSIX_FAILURE;
        }
    }
    else
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsCmnExtGetTunnelInfo"
                    "Invalid node type: INTMD-EXIT \n");
        return OSIX_FAILURE;
    }

    if ((u4TnlIndex != 0) && (u4TnlInst != 0) &&
        (u4TnlIngressId != 0) && (u4TnlEgressId != 0))
    {
        if (TeGetTunnelInfoByPrimaryTnlInst (u4TnlIndex, u4TnlInst,
                                             u4TnlIngressId, u4TnlEgressId,
                                             &pTeTnlInfo) == TE_FAILURE)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsCmnExtGetTunnelInfo"
                        "Local number for Destination GlobalId::NodeId "
                        "does not exist: INTMD-EXIT \n");
            return OSIX_FAILURE;
        }
    }
    else
    {
        pTeTnlInfo = TeGetActiveTnlInfo (u4TnlIndex, u4TnlInst,
                                         u4TnlIngressId, u4TnlEgressId);
    }
    if (MplsCmnExtFillTunnelInfo (pTeTnlInfo, pOutMplsApiInfo) == OSIX_FAILURE)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsCmnExtGetTunnelInfo"
                    "Filling tunnel information failed: INTMD-EXIT \n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : MplsCmnExtFillTunnelInfo                                   */
/*                                                                           */
/* Description  : This function fills the Tnl information based on the       */
/*                tunnel pointer information given.                          */
/*                                                                           */
/* Input        : pTeTnlInfo - Pointer to an tunnel information              */
/*                                                                           */
/* Output       : pOutMplsApiInfo - Pointer to an out API information        */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
PRIVATE INT4
MplsCmnExtFillTunnelInfo (tTeTnlInfo * pTeTnlInfo,
                          tMplsApiOutInfo * pOutMplsApiInfo)
{
    tOamFsMplsTpNodeMapEntry OamFsMplsTpNodeMapEntry;
    tOamFsMplsTpNodeMapEntry *pOamFsMplsTpNodeMapEntry = NULL;
    tXcEntry           *pXcEntry = NULL;
    UINT4               u4NodeId = 0;
    UINT4               u4L3Intf = 0;
    UINT4               u4TnlXcIndex = 0;

    if (pTeTnlInfo == NULL)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsCmnExtFillTunnelInfo "
                    "Tunnel entry does not exist: INTMD-EXIT \n");
        return OSIX_FAILURE;
    }

    if (((pOutMplsApiInfo->u4SrcModId == BFD_MODULE) ||
         (pOutMplsApiInfo->u4SrcModId == ELPS_MODULE)) &&
        (pTeTnlInfo->u4OrgTnlXcIndex != 0))
    {
        u4TnlXcIndex = pTeTnlInfo->u4OrgTnlXcIndex;
    }
    else
    {
        u4TnlXcIndex = pTeTnlInfo->u4TnlXcIndex;
    }

    pXcEntry = MplsGetXCEntryByDirection (u4TnlXcIndex, MPLS_DIRECTION_FORWARD);
    if (pXcEntry == NULL)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsCmnExtFillTunnelInfo "
                    "XC information/In Segment information "
                    "does not exist: INTMD-EXIT \n");
        return OSIX_FAILURE;
    }

    if (pTeTnlInfo->u4TnlLsrIdMapInfo == 0)
    {
        pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.SrcNodeId.u4NodeType =
            MPLS_ADDR_TYPE_IPV4;
        pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.DstNodeId.u4NodeType =
            MPLS_ADDR_TYPE_IPV4;

        MEMCPY (&u4NodeId, pTeTnlInfo->TnlIngressLsrId, ROUTER_ID_LENGTH);
        u4NodeId = OSIX_NTOHL (u4NodeId);
        pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.SrcNodeId.
            MplsRouterId.u4_addr[0] = u4NodeId;

        MEMCPY (&u4NodeId, pTeTnlInfo->TnlEgressLsrId, ROUTER_ID_LENGTH);
        u4NodeId = OSIX_NTOHL (u4NodeId);
        pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.DstNodeId.
            MplsRouterId.u4_addr[0] = u4NodeId;

        MEMCPY (&u4NodeId, pTeTnlInfo->TnlIngressLsrId, ROUTER_ID_LENGTH);
        u4NodeId = OSIX_NTOHL (u4NodeId);
        pOutMplsApiInfo->OutTeTnlInfo.ExtendedTnlId.u4_addr[0] = u4NodeId;
    }
    else
    {
        /* MPLS-TP case, both IngressId and EgressId are configured as
         * local map numbers */
        pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.SrcNodeId.u4NodeType =
            MPLS_ADDR_TYPE_GLOBAL_NODE_ID;
        pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.DstNodeId.u4NodeType =
            MPLS_ADDR_TYPE_GLOBAL_NODE_ID;

        MEMCPY (&pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.u4SrcLocalMapNum,
                pTeTnlInfo->TnlIngressLsrId, ROUTER_ID_LENGTH);
        MEMCPY (&pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.u4DstLocalMapNum,
                pTeTnlInfo->TnlEgressLsrId, ROUTER_ID_LENGTH);
        pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.u4SrcLocalMapNum =
            OSIX_NTOHL (pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                        u4SrcLocalMapNum);
        pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.u4DstLocalMapNum =
            OSIX_NTOHL (pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.
                        u4DstLocalMapNum);

        MEMSET (&OamFsMplsTpNodeMapEntry, 0, sizeof (tOamFsMplsTpNodeMapEntry));

        /* Assign the source indices */
        OamFsMplsTpNodeMapEntry.MibObject.u4FsMplsTpContextId =
            MPLS_DEF_CONTEXT_ID;

        OamFsMplsTpNodeMapEntry.MibObject.u4FsMplsTpNodeMapLocalNum =
            pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.u4SrcLocalMapNum;

        pOamFsMplsTpNodeMapEntry = OamGetFsMplsTpNodeMapTable
            (&OamFsMplsTpNodeMapEntry);
        if (pOamFsMplsTpNodeMapEntry == NULL)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsCmnExtFillTunnelInfo "
                        "Information get source map number from "
                        "node map table failed: INTMD-EXIT \n");
            return OSIX_FAILURE;
        }
        pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.SrcNodeId.MplsGlobalNodeId.
            u4GlobalId = pOamFsMplsTpNodeMapEntry->MibObject.
            u4FsMplsTpNodeMapGlobalId;
        pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.SrcNodeId.MplsGlobalNodeId.
            u4NodeId = pOamFsMplsTpNodeMapEntry->MibObject.
            u4FsMplsTpNodeMapNodeId;

        /* Assign the destination indices */
        OamFsMplsTpNodeMapEntry.MibObject.u4FsMplsTpContextId =
            MPLS_DEF_CONTEXT_ID;

        OamFsMplsTpNodeMapEntry.MibObject.u4FsMplsTpNodeMapLocalNum =
            pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.u4DstLocalMapNum;

        pOamFsMplsTpNodeMapEntry = OamGetFsMplsTpNodeMapTable
            (&OamFsMplsTpNodeMapEntry);
        if (pOamFsMplsTpNodeMapEntry == NULL)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsCmnExtFillTunnelInfo "
                        "Information get destination map number from "
                        "node map table failed: INTMD-EXIT \n");
            return OSIX_FAILURE;
        }

        pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.DstNodeId.MplsGlobalNodeId.
            u4GlobalId = pOamFsMplsTpNodeMapEntry->MibObject.
            u4FsMplsTpNodeMapGlobalId;
        pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.DstNodeId.MplsGlobalNodeId.
            u4NodeId = pOamFsMplsTpNodeMapEntry->MibObject.
            u4FsMplsTpNodeMapNodeId;
    }
    /* Fill all the common information of a tunnel */
    pOutMplsApiInfo->u4ContextId = MPLS_DEF_CONTEXT_ID;    /* Default context */
    pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.u4SrcTnlNum = pTeTnlInfo->u4TnlIndex;
    pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.u4DstTnlNum =
        pTeTnlInfo->u4DestTnlIndex;
    pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.u4LspNum = pTeTnlInfo->u4TnlInstance;
    pOutMplsApiInfo->OutTeTnlInfo.TnlLspId.u4DstLspNum =
        pTeTnlInfo->u4DestTnlInstance;
    pOutMplsApiInfo->OutTeTnlInfo.u4TnlPrimaryInstance =
        pTeTnlInfo->u4TnlPrimaryInstance;
    /*Tunnel Hardware status - To check NP programming */
    pOutMplsApiInfo->OutTeTnlInfo.u1TnlHwStatus = pTeTnlInfo->u1TnlHwStatus;

    if (MplsCmnExtFillXcApiInfo (pXcEntry,
                                 &pOutMplsApiInfo->OutTeTnlInfo.
                                 XcApiInfo, MPLS_DIRECTION_FORWARD)
        == OSIX_FAILURE)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsCmnExtFillTunnelInfo "
                    "Filling XC information failed: INTMD-EXIT \n");
        return OSIX_FAILURE;
    }

    if (pTeTnlInfo->u4TnlMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
    {
        pXcEntry = MplsGetXCEntryByDirection (u4TnlXcIndex,
                                              MPLS_DIRECTION_REVERSE);
        if (pXcEntry == NULL)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsCmnExtFillTunnelInfo "
                        "XC information/In Segment information "
                        "does not exist: INTMD-EXIT \n");
            return OSIX_FAILURE;
        }

        if (MplsCmnExtFillXcApiInfo (pXcEntry,
                                     &pOutMplsApiInfo->OutTeTnlInfo.
                                     XcApiInfo, MPLS_DIRECTION_REVERSE)
            == OSIX_FAILURE)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsCmnExtFillTunnelInfo "
                        "Filling XC information failed: INTMD-EXIT \n");
            return OSIX_FAILURE;
        }
    }
    pOutMplsApiInfo->OutTeTnlInfo.MplsMegId.u4MegIndex = pTeTnlInfo->u4MegIndex;
    pOutMplsApiInfo->OutTeTnlInfo.MplsMegId.u4MeIndex = pTeTnlInfo->u4MeIndex;
    pOutMplsApiInfo->OutTeTnlInfo.MplsMegId.u4MpIndex = pTeTnlInfo->u4MpIndex;

    pOutMplsApiInfo->OutTeTnlInfo.u4TnlIfIndex = pTeTnlInfo->u4TnlIfIndex;
    pOutMplsApiInfo->OutTeTnlInfo.u1TnlSgnlPrtcl = pTeTnlInfo->u1TnlSgnlPrtcl;
    pOutMplsApiInfo->OutTeTnlInfo.u4ProactiveSessIndex =
        pTeTnlInfo->u4ProactiveSessionIndex;
    pOutMplsApiInfo->OutTeTnlInfo.u4TnlType = pTeTnlInfo->u4TnlType;

    STRNCPY (pOutMplsApiInfo->OutTeTnlInfo.au1TnlName, pTeTnlInfo->au1TnlName,
             MPLS_STRLEN_MIN (pOutMplsApiInfo->OutTeTnlInfo.au1TnlName,
                              pTeTnlInfo->au1TnlName));
    pOutMplsApiInfo->OutTeTnlInfo.au1TnlName[MPLS_STRLEN_MIN
                                             (pOutMplsApiInfo->OutTeTnlInfo.
                                              au1TnlName,
                                              pTeTnlInfo->au1TnlName)] = '\0';
    MEMCPY (pOutMplsApiInfo->OutTeTnlInfo.au1NextHopMac,
            pTeTnlInfo->au1NextHopMac, MAC_ADDR_LEN);

    if (pTeTnlInfo->u1TnlRole == MPLS_TE_INGRESS)
    {
        /* To get the L3 interface from Tunnel Interface Index  */
        if (MplsGetL3Intf
            (pOutMplsApiInfo->OutTeTnlInfo.XcApiInfo.FwdOutSegInfo.u4OutIf,
             &u4L3Intf) == MPLS_FAILURE)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsCmnExtFillTunnelInfo "
                        "Interface do not exist: INTMD-EXIT \n");
        }

        /* To get thei VLAN ID of the L3 interface associated to
         * MPLS tunnel interface from L3 interface index  */
        if (CfaGetVlanId (u4L3Intf,
                          &(pOutMplsApiInfo->OutTeTnlInfo.XcApiInfo.
                            FwdOutSegInfo.u2VlanId)) == CFA_FAILURE)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsCmnExtFillTunnelInfo "
                        "Get VlanId Failed: INTMD-EXIT \n");

        }

        /* To get the physical port */
        if (MplsGetPhyPortFromLogicalIfIndex
            (u4L3Intf,
             pOutMplsApiInfo->OutTeTnlInfo.au1NextHopMac,
             &(pOutMplsApiInfo->OutTeTnlInfo.XcApiInfo.FwdOutSegInfo.
               u4PhyPort)) != MPLS_SUCCESS)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsCmnExtFillTunnelInfo "
                        "Fetching Physical port for the L3 interface failed.\n");
        }

    }
    if ((pTeTnlInfo->u1TnlRole == MPLS_TE_EGRESS) &&
        (pTeTnlInfo->u4TnlMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL))
    {
        /* To get the L3 interface from Tunnel Interface Index  */
        if (MplsGetL3Intf
            (pOutMplsApiInfo->OutTeTnlInfo.XcApiInfo.RevOutSegInfo.u4OutIf,
             &u4L3Intf) == MPLS_FAILURE)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsCmnExtFillTunnelInfo "
                        "Interface do not exist: INTMD-EXIT \n");
        }

        /* To get thei VLAN ID of the L3 interface associated to
         * MPLS tunnel interface from L3 interface index  */
        if (CfaGetVlanId (u4L3Intf,
                          &(pOutMplsApiInfo->OutTeTnlInfo.XcApiInfo.
                            RevOutSegInfo.u2VlanId)) == CFA_FAILURE)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsCmnExtFillTunnelInfo "
                        "Get VlanId Failed: INTMD-EXIT \n");
        }

        /* To get the physical port */
        if (MplsGetPhyPortFromLogicalIfIndex
            (u4L3Intf,
             pOutMplsApiInfo->OutTeTnlInfo.au1NextHopMac,
             &(pOutMplsApiInfo->OutTeTnlInfo.XcApiInfo.RevOutSegInfo.
               u4PhyPort)) != MPLS_SUCCESS)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsCmnExtFillTunnelInfo "
                        "Fetching Physical port for the L3 interface failed.\n");
        }
    }

    pOutMplsApiInfo->OutTeTnlInfo.u1TnlRole = pTeTnlInfo->u1TnlRole;
    pOutMplsApiInfo->OutTeTnlInfo.u1OperStatus =
        pTeTnlInfo->u1CPOrMgmtOperStatus;
    pOutMplsApiInfo->OutTeTnlInfo.u1TnlOwner = pTeTnlInfo->u1TnlOwner;
    pOutMplsApiInfo->OutTeTnlInfo.u1TnlMode = (UINT1) pTeTnlInfo->u4TnlMode;
    pOutMplsApiInfo->OutTeTnlInfo.u1LsrIdMapInfo = (UINT1)
        pTeTnlInfo->u4TnlLsrIdMapInfo;

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : MplsCmnExtFillLspInfoForFtn                                */
/*                                                                           */
/* Description  : This function fills the LSP information for FTN entry.     */
/*                                                                           */
/* Input        : pFtnEntry - Pointer to an FTN information                  */
/*                                                                           */
/* Output       : pOutMplsApiInfo - Pointer to an out API information        */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
PRIVATE INT4
MplsCmnExtFillLspInfoForFtn (tFtnEntry * pFtnEntry,
                             tMplsApiOutInfo * pOutMplsApiInfo)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tXcEntry           *pXcEntry = NULL;

    if (pFtnEntry->i4ActionType == REDIRECTTUNNEL)
    {
        pTeTnlInfo = pFtnEntry->pActionPtr;
        if (MplsCmnExtFillTunnelInfo (pTeTnlInfo, pOutMplsApiInfo)
            == OSIX_FAILURE)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsCmnExtGetRevPathFromFwdPath: "
                        "Filling of tunnel information failed: "
                        "INTMD-EXIT \n");
            return OSIX_FAILURE;
        }
        pOutMplsApiInfo->u4ContextId = MPLS_DEF_CONTEXT_ID;
        pOutMplsApiInfo->u4PathType = MPLS_PATH_TYPE_TUNNEL;
    }
    else if (pFtnEntry->i4ActionType == REDIRECTLSP)
    {
        pXcEntry = pFtnEntry->pActionPtr;
        if (pXcEntry == NULL)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsCmnExtGetRevPathFromFwdPath: "
                        "XC is not associated with the FTN entry: "
                        "INTMD-EXIT \n");
            return OSIX_FAILURE;
        }
        if (MplsCmnExtFillXcApiInfo (pXcEntry,
                                     &pOutMplsApiInfo->OutNonTeTnlInfo.
                                     XcApiInfo, MPLS_DEF_DIRECTION)
            == OSIX_FAILURE)
        {
            MPLSFM_DBG (MPLSFM_IF_FAILURE,
                        "MplsCmnExtGetRevPathFromFwdPath: "
                        "Filling NON-TE LSP information failed: "
                        "INTMD-EXIT \n");
            return OSIX_FAILURE;
        }
        pOutMplsApiInfo->u4ContextId = MPLS_DEF_CONTEXT_ID;
        pOutMplsApiInfo->u4PathType = MPLS_PATH_TYPE_NONTE_LSP;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : MplsCmnExtFillXcApiInfo                                    */
/*                                                                           */
/* Description  : This function fills the XC information based on the        */
/*                in segment information given.                              */
/*                                                                           */
/* Input        : pXcEntry    - Pointer to the XC information.               */
/*                InDirection   - Forward/Reverse direction                  */
/*                                                                           */
/* Output       : pXcApiInfo - Pointer to the XC API information.            */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
PRIVATE INT4
MplsCmnExtFillXcApiInfo (tXcEntry * pXcEntry, tMplsXcApiInfo * pXcApiInfo,
                         eDirection InDirection)
{
    tMplsInSegInfo      InSegInfo;
    tMplsOutSegInfo     OutSegInfo;
    tInSegment         *pInSegment = NULL;
    tOutSegment        *pOutSegment = NULL;
    tLblEntry          *pLblEntry = NULL;
    UINT1               u1StkCnt = 0;
    tXcEntry           *pStXcEntry = NULL;
    tTeTnlInfo         *pFATeTnlInfo = NULL;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;
    eDirection          Direction = MPLS_DIRECTION_ANY;

    MEMSET (&(InSegInfo), 0, sizeof (tMplsInSegInfo));

    MEMSET (&(OutSegInfo), 0, sizeof (tMplsOutSegInfo));

    /* Return error if In segment/XC is NULL */
    if (pXcEntry == NULL)
    {
        MPLSFM_DBG (MPLSFM_IF_FAILURE,
                    "MplsCmnExtFillXcApiInfo"
                    "In segment doesnot exist: INTMD-EXIT \n");
        return OSIX_FAILURE;
    }
    pInSegment = pXcEntry->pInIndex;
    /* Get the outsegment information */
    pOutSegment = pXcEntry->pOutIndex;

    if (pInSegment != NULL)
    {
        InSegInfo.u4InIf = pInSegment->u4IfIndex;
        InSegInfo.Direction = pInSegment->GmplsInSegment.InSegmentDirection;
        InSegInfo.u2AddrFmly = pInSegment->u2AddrFmly;
        InSegInfo.u1LblAction = pInSegment->u1LblAction;
        InSegInfo.u1LblStkCnt = MPLS_ONE;

        /* Fill label information for in segment */
        if (pInSegment->mplsLabelIndex == NULL)
        {
            InSegInfo.LblInfo[u1StkCnt].u4Label = pInSegment->u4Label;
            InSegInfo.LblInfo[u1StkCnt].u1Ttl = MPLS_DEF_TTL;
            InSegInfo.LblInfo[u1StkCnt].u1Exp = MPLS_BEST_OF_SERVICE;
            InSegInfo.LblInfo[u1StkCnt].u1SI = MPLS_ONE;
            InSegInfo.LblInfo[u1StkCnt].u1Protocol = pInSegment->u1Owner;
        }
        else
        {
            pLblEntry =
                (tLblEntry *) TMO_SLL_First (&pInSegment->mplsLabelIndex->
                                             LblList);
            if (pLblEntry == NULL)
            {
                MPLSFM_DBG (MPLSFM_IF_FAILURE,
                            "MplsCmnExtFillXcApiInfo"
                            "Information doesnot exist in the in label "
                            "stack table: INTMD-EXIT \n");
                return OSIX_FAILURE;
            }
            while (pLblEntry != NULL)
            {
                InSegInfo.LblInfo[u1StkCnt].u4Label = pLblEntry->u4Label;
                InSegInfo.LblInfo[u1StkCnt].u1Ttl = MPLS_DEF_TTL;
                InSegInfo.LblInfo[u1StkCnt].u1Exp = MPLS_BEST_OF_SERVICE;
                InSegInfo.LblInfo[u1StkCnt].u1SI = MPLS_ZERO;
                InSegInfo.LblInfo[u1StkCnt].u1Protocol = pInSegment->u1Owner;
                pLblEntry = (tLblEntry *)
                    TMO_SLL_Next (&pInSegment->mplsLabelIndex->LblList,
                                  &(pLblEntry->Next));
                u1StkCnt++;
            }
        }
        if (u1StkCnt != 0)
        {
            InSegInfo.LblInfo[u1StkCnt - 1].u1SI = MPLS_ONE;
            InSegInfo.u1LblStkCnt = u1StkCnt;
        }

        Direction = pInSegment->GmplsInSegment.InSegmentDirection;
    }

    /* To initialise the label stack count here */
    u1StkCnt = 0;

    if (pOutSegment != NULL)
    {
        Direction = pOutSegment->GmplsOutSegment.OutSegmentDirection;
    }

    if ((pXcEntry->pTeTnlInfo != NULL) &&
        (pXcEntry->pTeTnlInfo->u4TnlMapIndex != 0))
    {
        CONVERT_TO_INTEGER (pXcEntry->pTeTnlInfo->TnlMapIngressLsrId,
                            u4IngressId);
        CONVERT_TO_INTEGER (pXcEntry->pTeTnlInfo->TnlMapEgressLsrId,
                            u4EgressId);
        u4IngressId = OSIX_NTOHL (u4IngressId);
        u4EgressId = OSIX_NTOHL (u4EgressId);
        pFATeTnlInfo = TeGetTunnelInfo (pXcEntry->pTeTnlInfo->u4TnlMapIndex,
                                        pXcEntry->pTeTnlInfo->u4TnlMapInstance,
                                        u4IngressId, u4EgressId);

        if ((pFATeTnlInfo != NULL) &&
            (pFATeTnlInfo->u1TnlLocalProtectInUse == LOCAL_PROT_IN_USE))
        {
            pStXcEntry
                = MplsGetXCEntryByDirection (pFATeTnlInfo->u4TnlXcIndex,
                                             InDirection);

            if ((pStXcEntry != NULL) &&
                (pStXcEntry->pOutIndex != NULL) &&
                ((pStXcEntry->pOutIndex->GmplsOutSegment.OutSegmentDirection ==
                  Direction)))
            {
                pOutSegment = pStXcEntry->pOutIndex;
            }
        }
    }

    /* Fill the outsegment information if exits */
    if (pOutSegment != NULL)
    {
        OutSegInfo.NHAddr.u4_addr[0] = pOutSegment->NHAddr.u4_addr[0];
        OutSegInfo.u4OutIf = pOutSegment->u4IfIndex;
        OutSegInfo.Direction = pOutSegment->GmplsOutSegment.OutSegmentDirection;
        MEMCPY (OutSegInfo.au1NextHopMac,
                pOutSegment->au1NextHopMac, MAC_ADDR_LEN);
        OutSegInfo.u1NHAddrType = pOutSegment->u1NHAddrType;
        OutSegInfo.u1LblStkCnt = MPLS_ONE;

        OutSegInfo.LblInfo[u1StkCnt].u4Label = pOutSegment->u4Label;
        OutSegInfo.LblInfo[u1StkCnt].u1Ttl = MPLS_DEF_TTL;
        OutSegInfo.LblInfo[u1StkCnt].u1Exp = MPLS_BEST_OF_SERVICE;
        OutSegInfo.LblInfo[u1StkCnt].u1SI = MPLS_ZERO;
        OutSegInfo.LblInfo[u1StkCnt].u1Protocol = pOutSegment->u1Owner;

        if (pXcEntry->mplsLabelIndex != NULL)
        {
            pLblEntry = (tLblEntry *) TMO_SLL_First (&pXcEntry->
                                                     mplsLabelIndex->LblList);
            if (pLblEntry == NULL)
            {
                MPLSFM_DBG (MPLSFM_IF_FAILURE,
                            "MplsCmnExtFillXcApiInfo"
                            "Information doesnot exist in the out label "
                            "stack table:" "INTMD-EXIT \n");
                return OSIX_FAILURE;
            }

            while (pLblEntry != NULL)
            {
                u1StkCnt++;
                OutSegInfo.LblInfo[u1StkCnt].u4Label = pLblEntry->u4Label;
                OutSegInfo.LblInfo[u1StkCnt].u1Ttl = MPLS_DEF_TTL;
                OutSegInfo.LblInfo[u1StkCnt].u1Exp = MPLS_BEST_OF_SERVICE;
                OutSegInfo.LblInfo[u1StkCnt].u1SI = MPLS_ZERO;
                OutSegInfo.LblInfo[u1StkCnt].u1Protocol = pOutSegment->u1Owner;
                pLblEntry = (tLblEntry *)
                    TMO_SLL_Next (&pXcEntry->mplsLabelIndex->LblList,
                                  &(pLblEntry->Next));
            }
        }
        OutSegInfo.LblInfo[u1StkCnt].u1SI = MPLS_ONE;
        OutSegInfo.u1LblStkCnt = (UINT1) (u1StkCnt + 1);
    }

    pXcApiInfo->u1OperStatus = pXcEntry->u1OperStatus;
    if (pInSegment != NULL)
    {
        if (InDirection == MPLS_DIRECTION_FORWARD)
        {
            MEMCPY (&pXcApiInfo->FwdInSegInfo, &InSegInfo,
                    sizeof (tMplsInSegInfo));
            pXcApiInfo->u4XcSegInfo |= MPLS_XC_FWD_IN_SEG_INFO;
        }
        else
        {
            MEMCPY (&pXcApiInfo->RevInSegInfo, &InSegInfo,
                    sizeof (tMplsInSegInfo));
            pXcApiInfo->u4XcSegInfo |= MPLS_XC_REV_IN_SEG_INFO;
        }
    }
    if (pOutSegment != NULL)
    {
        if (InDirection == MPLS_DIRECTION_FORWARD)
        {
            MEMCPY (&pXcApiInfo->FwdOutSegInfo, &OutSegInfo,
                    sizeof (tMplsOutSegInfo));
            pXcApiInfo->u4XcSegInfo |= MPLS_XC_FWD_OUT_SEG_INFO;
        }
        else
        {
            MEMCPY (&pXcApiInfo->RevOutSegInfo, &OutSegInfo,
                    sizeof (tMplsOutSegInfo));
            pXcApiInfo->u4XcSegInfo |= MPLS_XC_REV_OUT_SEG_INFO;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : MplsCmnExtGetRouterId                                      */
/*                                                                           */
/* Description  : This function sets the Context Id and Router Id            */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                                                                           */
/* Output       : pOutMplsApiInfo - Pointer to the output API information.   */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*****************************************************************************/
INT4
MplsCmnExtGetRouterId (tMplsApiInInfo * pInMplsApiInfo,
                       tMplsApiOutInfo * pOutMplsApiInfo)
{
    UINT4               u4NodeId = 0;

    pOutMplsApiInfo->u4ContextId = pInMplsApiInfo->u4ContextId;

    MEMCPY (&u4NodeId,
            MPLS_ROUTER_ID (pInMplsApiInfo->u4ContextId).au1Ipv4Addr,
            ROUTER_ID_LENGTH);
    u4NodeId = OSIX_NTOHL (u4NodeId);
    pOutMplsApiInfo->OutNodeId.u4NodeId = u4NodeId;

    return OSIX_SUCCESS;
}
