#define _LANAISZ_C
#include "mplsincs.h"
INT4  LanaiSizingMemCreateMemPools()
{
    INT4 i4RetVal;
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < LANAI_MAX_SIZING_ID; i4SizingId++) {
        i4RetVal = MemCreateMemPool( 
                          FsLANAISizingParams[i4SizingId].u4StructSize,
                          FsLANAISizingParams[i4SizingId].u4PreAllocatedUnits,
                          MEM_DEFAULT_MEMORY_TYPE,
                          &(LANAIMemPoolIds[ i4SizingId]));
        if( i4RetVal == (INT4) MEM_FAILURE) {
            LanaiSizingMemDeleteMemPools();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}


INT4   LanaiSzRegisterModuleSizingParams( CHR1 *pu1ModName)
{
      /* Copy the Module Name */ 
       IssSzRegisterModuleSizingParams( pu1ModName, FsLANAISizingParams); 
      return OSIX_SUCCESS; 
}


VOID  LanaiSizingMemDeleteMemPools()
{
    INT4 i4SizingId;

    for( i4SizingId=0; i4SizingId < LANAI_MAX_SIZING_ID; i4SizingId++) {
        if(LANAIMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool( LANAIMemPoolIds[ i4SizingId] );
            LANAIMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
