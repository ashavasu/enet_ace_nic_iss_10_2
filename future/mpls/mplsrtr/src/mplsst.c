/********************************************************************
* Copyright (C) 2012 Aricent Inc . All Rights Reserved
*
* $Id: mplsst.c,v 1.1 2012/05/14 14:07:35 siva Exp $
*
* Description: This file contains the common Set routines for MPLS
*********************************************************************/

#include "mplsincs.h"
/****************************************************************************
Function    :  MplsDsTeSetAllClassTypeTable
Input       :  u4ClassTypeIndex  - Class Type Index
               pSetDsTeClassType - Pointer to Class Type entry
               u4ObjectId        - Object ID
Description :  This Routine Take the Indices &
               Sets the Value accordingly.
Output      :  None 
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
MplsDsTeSetAllClassTypeTable (UINT4 u4ClassTypeIndex,
                                tMplsDsTeClassType *pSetDsTeClassType,
                                UINT4 u4ObjectId)
{
    tMplsDsTeClassType ClassEntry;

    MEMSET (&ClassEntry, MPLS_ZERO, sizeof(tMplsDsTeClassType));

    if ((MPLS_ADMIN_STATUS (MPLS_DEF_INCARN) != MPLS_ADMIN_STATUS_UP) ||
        (MPLS_DSTE_STATUS (MPLS_DEF_INCARN) != MPLS_DSTE_STATUS_DISABLE)||
        (MPLS_QOS_POLICY (MPLS_DEF_INCARN) != MPLS_DIFF_SERV))
    {
        return SNMP_FAILURE;
    }

    ClassEntry = MPLS_DIFFSERV_CLASS_TYPE_ENTRY (MPLS_DEF_INCARN, 
                                                 u4ClassTypeIndex);
    switch (u4ObjectId)
    {
        case MPLS_DSTE_CLASS_TYPE_ROW_STATUS:
        {    
            if (pSetDsTeClassType->i4DsTeClassTypeRowStatus != CREATE_AND_WAIT)
            {
                if (ClassEntry.i4DsTeClassTypeRowStatus == 
                    pSetDsTeClassType->i4DsTeClassTypeRowStatus)
                {
                    return SNMP_SUCCESS;
                }
            }
            if (pSetDsTeClassType->i4DsTeClassTypeRowStatus == CREATE_AND_WAIT)
            {
               ClassEntry.i4DsTeClassTypeRowStatus = NOT_READY;
            }
            else if (pSetDsTeClassType->i4DsTeClassTypeRowStatus == ACTIVE)
            {
                ClassEntry.i4DsTeClassTypeRowStatus = ACTIVE;
            }
            else if (pSetDsTeClassType->i4DsTeClassTypeRowStatus == 
                     NOT_IN_SERVICE)
            {
                ClassEntry.i4DsTeClassTypeRowStatus = NOT_IN_SERVICE;
            }
            else if (pSetDsTeClassType->i4DsTeClassTypeRowStatus == DESTROY)
            {
                ClassEntry.i4DsTeClassTypeRowStatus = MPLS_ZERO;
                MEMSET (&ClassEntry, MPLS_ZERO, sizeof(tMplsDsTeClassType));
            }

            break;
        }

        case  MPLS_DSTE_CLASS_TYPE_BW_PERCENT:
        {
            ClassEntry.i4DsTeClassTypeBwPercent = 
                pSetDsTeClassType->i4DsTeClassTypeBwPercent;
            break; 
        }
        case MPLS_DSTE_CLASS_TYPE_DESCRIPTION:
        {
            MEMCPY (ClassEntry.au1DsTeClassTypeDesc, 
                    pSetDsTeClassType->au1DsTeClassTypeDesc,
                    STRLEN (pSetDsTeClassType->au1DsTeClassTypeDesc));
            break;
        }
        default:
        {
            return SNMP_FAILURE;
        }
    }
   MPLS_DIFFSERV_CLASS_TYPE_ENTRY (MPLS_DEF_INCARN,u4ClassTypeIndex) = ClassEntry;
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  MplsDsTeSetAllClassTypeTcMapTable
Input       :  u4ClassTypeIndex       - Class Type Index
               u4ClassTcMapIndex      - Class Map Index
               pGetDsTeClassTypeTcMap - Pointer to Traffic class map table entry
               u4ObjectId             - Object ID
Description :  This function sets all the Traffic class map table objects
Output      :  None 
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
MplsDsTeSetAllClassTypeTcMapTable (UINT4 u4ClassTypeIndex,
                                     UINT4 u4ClassTcMapIndex,
                                tMplsDsTeClassTypeTcMap *pSetDsTeClassTypeTcMap,
                                UINT4 u4ObjectId)
{
    tMplsDsTeClassTypeTcMap TeClassTypeTcMap;

    MEMSET (&TeClassTypeTcMap, MPLS_ZERO, sizeof(tMplsDsTeClassTypeTcMap));

    if ((MPLS_ADMIN_STATUS (MPLS_DEF_INCARN) != MPLS_ADMIN_STATUS_UP) ||
        (MPLS_DSTE_STATUS (MPLS_DEF_INCARN) != MPLS_DSTE_STATUS_DISABLE)||
        (MPLS_QOS_POLICY (MPLS_DEF_INCARN) != MPLS_DIFF_SERV))
    {
        return SNMP_FAILURE;
    }

    TeClassTypeTcMap = MPLS_DIFFSERV_TRAFFIC_CLASS_MAP (MPLS_DEF_INCARN, 
                                           u4ClassTypeIndex, u4ClassTcMapIndex);
    switch (u4ObjectId)
    {
        case MPLS_DSTE_TC_MAP_TYPE:
        {
            TeClassTypeTcMap.u4DsTeTcType = 
                pSetDsTeClassTypeTcMap->u4DsTeTcType;
            break;
        }
        case MPLS_DSTE_TC_MAP_DESC:
        {
            MEMCPY (TeClassTypeTcMap.au1DsTeClassTypeToTcDesc, 
                    pSetDsTeClassTypeTcMap->au1DsTeClassTypeToTcDesc,
                    STRLEN (pSetDsTeClassTypeTcMap->au1DsTeClassTypeToTcDesc));
            break;
        }
        case MPLS_DSTE_TC_MAP_ROW_STATUS:
        {    
            if (pSetDsTeClassTypeTcMap->i4DsTeClassTypeToTcRowStatus != 
                CREATE_AND_WAIT)
            {
                if (TeClassTypeTcMap.i4DsTeClassTypeToTcRowStatus == 
                    pSetDsTeClassTypeTcMap->i4DsTeClassTypeToTcRowStatus)
                {
                    return SNMP_SUCCESS;
                }
            }
            if (pSetDsTeClassTypeTcMap->i4DsTeClassTypeToTcRowStatus == 
                CREATE_AND_WAIT)
            {
               TeClassTypeTcMap.i4DsTeClassTypeToTcRowStatus = NOT_READY;
            }
            else if (pSetDsTeClassTypeTcMap->i4DsTeClassTypeToTcRowStatus == 
                     ACTIVE)
            {
                TeClassTypeTcMap.i4DsTeClassTypeToTcRowStatus = ACTIVE;
            }
            else if (pSetDsTeClassTypeTcMap->i4DsTeClassTypeToTcRowStatus == 
                     NOT_IN_SERVICE)
            {
                TeClassTypeTcMap.i4DsTeClassTypeToTcRowStatus = NOT_IN_SERVICE;
            }
            else if (pSetDsTeClassTypeTcMap->i4DsTeClassTypeToTcRowStatus == 
                     DESTROY)
            {
                TeClassTypeTcMap.i4DsTeClassTypeToTcRowStatus = MPLS_ZERO;
                MEMSET (&TeClassTypeTcMap, MPLS_ZERO, 
                        sizeof(tMplsDsTeClassTypeTcMap));
            }
            break;
        }     
        default:
             return SNMP_FAILURE;
    }
    MPLS_DIFFSERV_TRAFFIC_CLASS_MAP (MPLS_DEF_INCARN,u4ClassTypeIndex, u4ClassTcMapIndex) = TeClassTypeTcMap; 
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  MplsDsTeSetAllTeclassMapTable
Input       :  u4ClassTypeIndex     - Class Type Index
               u4ClassTeClassPrio   - TE class Priority
               pSetDsTeClassPrio    - Pointer to TE class map entry
               u4ObjectId           - Object ID
Description :  This function sets all the TE class map table objects
Output      :  None 
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
MplsDsTeSetAllTeclassMapTable (UINT4 u4ClassTypeIndex,
                                  UINT4 u4ClassTeClassPrio,
                                  tMplsDsTeClassPrio *pSetDsTeClassPrio,
                                  UINT4 u4ObjectId)
{
    tMplsDsTeClassPrio TeClassTePrio;

    MEMSET (&TeClassTePrio, MPLS_ZERO, sizeof (tMplsDsTeClassPrio));

    if ((MPLS_ADMIN_STATUS (MPLS_DEF_INCARN) != MPLS_ADMIN_STATUS_UP) ||
        (MPLS_DSTE_STATUS (MPLS_DEF_INCARN) != MPLS_DSTE_STATUS_DISABLE)||
        (MPLS_QOS_POLICY (MPLS_DEF_INCARN) != MPLS_DIFF_SERV))
    {
        return SNMP_FAILURE;
    }

    TeClassTePrio = MPLS_DIFFSERV_TE_CLASS_MAP (MPLS_DEF_INCARN, 
                                           u4ClassTypeIndex, u4ClassTeClassPrio);

    switch (u4ObjectId)
    {
        case MPLS_DSTE_TE_CLASS_DESC:
        {
            MEMCPY (TeClassTePrio.au1DsTeCDesc, pSetDsTeClassPrio->au1DsTeCDesc,
                    STRLEN (pSetDsTeClassPrio->au1DsTeCDesc));
            break;
        }
        case MPLS_DSTE_TE_CLASS_NUMBER:
        {
            TeClassTePrio.u4TeClassNumber = pSetDsTeClassPrio->u4TeClassNumber;
            break;
        }
        case MPLS_DSTE_TE_CLASS_ROW_STATUS:
        {
            if (pSetDsTeClassPrio->i4DsTeCRowStatus != CREATE_AND_WAIT)
            {
                if (TeClassTePrio.i4DsTeCRowStatus == 
                    pSetDsTeClassPrio->i4DsTeCRowStatus)
                {
                    return SNMP_SUCCESS;
                }
            }
            if (pSetDsTeClassPrio->i4DsTeCRowStatus == CREATE_AND_WAIT)
            {
               TeClassTePrio.i4DsTeCRowStatus = NOT_READY;
            }
            else if (pSetDsTeClassPrio->i4DsTeCRowStatus == ACTIVE)
            {
               TeClassTePrio.i4DsTeCRowStatus = ACTIVE;
            }
            else if (pSetDsTeClassPrio->i4DsTeCRowStatus == NOT_IN_SERVICE)
            {
               TeClassTePrio.i4DsTeCRowStatus = NOT_IN_SERVICE;
            }
            else if (pSetDsTeClassPrio->i4DsTeCRowStatus == DESTROY)
            {
                TeClassTePrio.i4DsTeCRowStatus = MPLS_ZERO;
                MEMSET (&TeClassTePrio, MPLS_ZERO, sizeof(tMplsDsTeClassPrio));
            }
            break;
        }
        default:
        {
            return SNMP_FAILURE;
        }
    }
    MPLS_DIFFSERV_TE_CLASS_MAP (MPLS_DEF_INCARN,u4ClassTypeIndex, u4ClassTeClassPrio) = TeClassTePrio;  
    return SNMP_SUCCESS;
}

