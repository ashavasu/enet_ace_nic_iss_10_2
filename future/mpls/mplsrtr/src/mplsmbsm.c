/********************************************************************
 * * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * *
 * * $Id: mplsmbsm.c,v 1.17 2015/10/21 11:57:40 siva Exp $
 * *
 * * Description: MPLS MBSM Module related functions 
 * *********************************************************************/
#ifdef MBSM_WANTED

#include "mplcmndb.h"
#include "mplsdbinc.h"
#include "mplslsr.h"
#include "mplsftn.h"
#include "cfa.h"
#include "npapi.h"
#include "ipnp.h"
#include "mplsnp.h"
#include "tedsdefs.h"
#include "tedsmacs.h"
#include "temacs.h"
#include "fsmplslw.h"
#include "mplstdfs.h"
#include "mplsglob.h"
#include "mplsdiff.h"
#include "mplssize.h"
#include "l2vpextn.h"
#include "l2vpincs.h"
#include "teincs.h"
#ifdef MPLS_L3VPN_WANTED
#include "l3vpntmr.h"
#include "l3vpntdfsg.h"
#include "l3vpntdfs.h"
#include "l3vpnprot.h"
#include "l3vpndefn.h"
#include "l3vpnextn.h"
#endif

static INT4 MplsMbsmFTNAdd PROTO ((tMbsmSlotInfo * pSlotInfo));
extern INT4         MplsTnlNPAdd (tTeTnlInfo *, tXcEntry *,
                                  tMbsmSlotInfo * pSlotInfo);
static INT4 MplsMbsmPwVcAdd PROTO ((tMbsmSlotInfo * pSlotInfo));
static INT4 MplsMbsmILMAdd PROTO ((tMbsmSlotInfo * pSlotInfo));
static INT4 MplsMbsmVplsVpnAdd PROTO ((tMbsmSlotInfo * pSlotInfo));
static INT4 MplsMbsmP2mpILMAdd PROTO ((tMbsmSlotInfo * pSlotInfo));
#ifdef MPLS_L3VPN_WANTED
static INT4 MplsMbsmL3vpnIngressMapAdd PROTO ((tMbsmSlotInfo * pSlotInfo));
static INT4 MplsMbsmL3vpnEgressMapAdd PROTO ((tMbsmSlotInfo * pSlotInfo));
#endif

/*****************************************************************************/
/*                                                                           */
/* Function     : MplsMbsmUpdateCardStatus                                */
/*                                                                           */
/* Description  : Allocates a CRU buffer and enqueues the line card          */
/*                change status to the MPLS task                             */
/*                                                                           */
/* Input        : pProtoMsg - Contains the slot and port information         */
/*                u1Cmd    - Line card status (UP/DOWN)                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MBSM_SUCCESS or MBSM_FAILURE                               */
/*                                                                           */
/*****************************************************************************/

INT4
MplsMbsmUpdateCardStatus (tMbsmProtoMsg * pProtoMsg, UINT1 u1Cmd)
{
    tCRU_BUF_CHAIN_HEADER *pBuf = NULL;

    if (gu1MplsInitialised != TRUE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsMbsmUpdateCardStatus: MPLS is not initialized\n");
        return MBSM_FAILURE;
    }
    pBuf = CRU_BUF_Allocate_MsgBufChain ((sizeof (tMbsmProtoMsg)), 0);
    if (pBuf == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "CRU Buffer Allocation Failed\n");
        return MBSM_FAILURE;
    }

    if (CRU_BUF_Copy_OverBufChain (pBuf, (UINT1 *) pProtoMsg, 0,
                                   sizeof (tMbsmProtoMsg)) != CRU_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Copy Over CRU Buffer Failed\n");
        return MBSM_FAILURE;
    }
    CRU_BUF_Set_U2Reserved (pBuf, u1Cmd);

    if (OsixQueSend (gFmInQId, (UINT1 *) (&pBuf),
                     OSIX_DEF_MSG_LEN) != OSIX_SUCCESS)
    {
        CRU_BUF_Release_MsgBufChain (pBuf, FALSE);
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Send To MPLS Q Failed\n");
        return MBSM_FAILURE;
    }

    if (OsixEvtSend (gFmTaskId, MPLS_IN_MSG_Q_EVENT) == OSIX_FAILURE)
    {
        return MBSM_FAILURE;
    }

    return MBSM_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : MplsMbsmUpdateLCStatus                                     */
/*                                                                           */
/* Description  : Initialise the line card, based on the line card status    */
/*                received                                                   */
/*                                                                           */
/* Input        : pBuf     - Buffer containing the protocol message info.    */
/*                u1Cmd    - Line card status (UP/DOWN)                      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

VOID
MplsMbsmUpdateLCStatus (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 u1Cmd)
{
    INT4                i4RetStatus = MBSM_SUCCESS;
    tMbsmSlotInfo      *pSlotInfo = NULL;
    tMbsmProtoMsg       ProtoMsg;
    tMbsmProtoAckMsg    ProtoAckMsg;
    tMplsHwL3FTNInfo    MplsHwL3FTNInfo;

    MEMSET (&ProtoMsg, 0, sizeof (tMbsmProtoMsg));
    MEMSET (&ProtoAckMsg, 0, sizeof (tMbsmProtoAckMsg));
    MEMSET (&MplsHwL3FTNInfo, 0, sizeof (tMplsHwL3FTNInfo));

    if (CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &ProtoMsg, 0,
                                   sizeof (tMbsmProtoMsg)) == 0)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Copy From CRU Buffer Failed\n");
        i4RetStatus = MBSM_FAILURE;
    }

    /* A Slot is attached or detached.
     *
     * The following needs to be done in MPLS module.
     *     1. Initialisation or Deinitialisation of MPLS specific parameter
     *        for the slot attached / detached.
     *     2. FTN entries needs to be added / removed in the slot attached /
     *        detached.
     *
     * The following is NOT DONE in MPLS module.
     *     1. ILM entries addition / removal in the slot attached / detached.
     *     2. PW Entries addition / removal in the slot attached / detached.
     * Reason: ILM or PW Entries involving interfaces on 2 different slots
     *         can be added in the hardware only when operational status of the
     *         interface is notified as UP and other protocol signaling is
     *         through. When entries are added in the hardware during this time,
     *         actual slot information needs to be retrieved based on the
     *         IF INDEX value and programming SHOULD be done. This should be
     *         taken care in porting.
     */
    if (i4RetStatus != MBSM_FAILURE)
    {
        pSlotInfo = &(ProtoMsg.MbsmSlotInfo);
        if (!MBSM_SLOT_INFO_ISPORTMSG (pSlotInfo))
        {
            if (u1Cmd == MBSM_MSG_CARD_INSERT)
            {
                if (MplsFsMplsMbsmHwEnableMpls (pSlotInfo) == FNP_FAILURE)
                {
                    i4RetStatus = MBSM_FAILURE;
                }
                if (MplsMbsmILMAdd (pSlotInfo) == MPLS_FAILURE)
                {
                    i4RetStatus = MBSM_FAILURE;
                }
                if (MplsMbsmFTNAdd (pSlotInfo) == MPLS_FAILURE)
                {
                    i4RetStatus = MBSM_FAILURE;
                }
                /*Both VPWS and VPLS PwVc will add in this Function
                   If Pw is VPLS Pw,AC will also add in this Function */
                if (MplsMbsmPwVcAdd (pSlotInfo) == MPLS_FAILURE)
                {
                    i4RetStatus = MBSM_FAILURE;
                }
                if (MplsMbsmVplsVpnAdd (pSlotInfo) == MPLS_FAILURE)
                {
                    i4RetStatus = MBSM_FAILURE;
                }
                if (MplsMbsmP2mpILMAdd (pSlotInfo) == MPLS_FAILURE)
                {
                    i4RetStatus = MBSM_FAILURE;
                }
#ifdef MPLS_L3VPN_WANTED
                if (MplsMbsmL3vpnIngressMapAdd (pSlotInfo) == MPLS_FAILURE)
                {
                    i4RetStatus = MBSM_FAILURE;
                }
                if (MplsMbsmL3vpnEgressMapAdd (pSlotInfo) == MPLS_FAILURE)
                {
                    i4RetStatus = MBSM_FAILURE;
                }
#endif
            }
        }

        ProtoAckMsg.i4SlotId = pSlotInfo->i4SlotId;
        ProtoAckMsg.i4ProtoCookie = ProtoMsg.i4ProtoCookie;
    }
    ProtoAckMsg.i4RetStatus = i4RetStatus;
    MbsmSendAckFromProto (&ProtoAckMsg);
}

/*****************************************************************************/
/*                                                                           */
/* Function     : MplsMbsmPwVcAdd                                            */
/*                                                                           */
/* Description  : Initialise the MPLS protocol on the line card              */
/*                                                                           */
/* Input        : pSlotInfo - Contains the slot information                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MBSM_SUCCESS or MBSM_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
INT4
MplsMbsmPwVcAdd (tMbsmSlotInfo * pSlotInfo)
{
    UINT4               u4PwVcIndex = 0;
    tPwVcEntry         *pPwVcEntry = NULL;

    MPLS_L2VPN_LOCK ();
    for (u4PwVcIndex = 1; u4PwVcIndex <=
         L2VPN_MAX_PWVC_ENTRIES (gpPwVcGlobalInfo); u4PwVcIndex++)
    {
        pPwVcEntry = L2VpnGetPwVcEntryFromIndex (u4PwVcIndex);

        if (pPwVcEntry == NULL)
        {
            continue;
        }

        /*PwVc will add as VPWS or VPLS as per L2VPN_PWVC_MODE 
         * VPWS means MplsFsMplsHwCreatePwVc HwCalls will be used
         * VPLS means FsMplsMbsmHwVplsAddPwVc HwCalls will be used
         * For VPLS AC, MplsFsMplsMbsmHwAddVpnAc HwCalls will be used*/
        if (L2VPN_PWVC_OPER_STATUS (pPwVcEntry) == L2VPN_PWVC_OPER_UP)
        {
            if (L2VpnPwVcHwAdd (pPwVcEntry, (VOID *) pSlotInfo) ==
                L2VPN_FAILURE)
            {
                MPLS_L2VPN_UNLOCK ();
                return MPLS_FAILURE;
            }
        }
    }

    MPLS_L2VPN_UNLOCK ();
    return MPLS_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : MplsMbsmILMAdd                                             */
/*                                                                           */
/* Description  : Initialise the MPLS protocol on the line card              */
/*                                                                           */
/* Input        : pSlotInfo - Contains the slot information                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MBSM_SUCCESS or MBSM_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
INT4
MplsMbsmILMAdd (tMbsmSlotInfo * pSlotInfo)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tXcEntry           *pXcEntry = NULL;
    tInSegment         *pInSegment = NULL;
    UINT4               u4Index = 0;
    UINT4               u4XcIndex = 0;

    MPLS_CMN_LOCK ();
    for (u4Index = MPLS_ONE; u4Index <= gu4MplsDbMaxInSegEntries; u4Index++)
    {
        pInSegment = MplsGetInSegmentTableEntry (u4Index);
        if ((pInSegment == NULL) || (pInSegment->pXcIndex == NULL))
        {
            continue;
        }
        pXcEntry = pInSegment->pXcIndex;
        u4XcIndex = pXcEntry->u4Index;

        if (TeCheckXcIndex (u4XcIndex, &pTeTnlInfo) == TE_FAILURE)
        {
        }
        if ((pInSegment->u1RowStatus == MPLS_STATUS_ACTIVE) &&
            (XC_OPER_STATUS (pXcEntry) == XC_OPER_UP))
        {
            if (MplsILMHwAdd (pInSegment, pTeTnlInfo, (VOID *) pSlotInfo) ==
                MPLS_FAILURE)
            {
                MPLS_CMN_UNLOCK ();
                return MPLS_FAILURE;
            }
        }
    }
    MPLS_CMN_UNLOCK ();
    return MPLS_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : MplsMbsmFTNAdd                                             */
/*                                                                           */
/* Description  : Initialise the MPLS protocol on the line card              */
/*                                                                           */
/* Input        : pSlotInfo - Contains the slot information                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MBSM_SUCCESS or MBSM_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
INT4
MplsMbsmFTNAdd (tMbsmSlotInfo * pSlotInfo)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tXcEntry           *pXcEntry = NULL;
    tOutSegment        *pOutSegment = NULL;
    UINT4               u4Index = 0;
    UINT4               u4XcIndex = 0;

    MPLS_CMN_LOCK ();
    for (u4Index = MPLS_ONE; u4Index <= gu4MplsDbMaxOutSegEntries; u4Index++)
    {
        pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
        if (pOutSegment == NULL || pOutSegment->pXcIndex == NULL)
        {
            continue;
        }

        pXcEntry = pOutSegment->pXcIndex;
        
        if(pXcEntry == NULL)
        {
            continue;
        }
        if(pXcEntry->pTeTnlInfo == NULL)
        {
            continue;
        }
     
        u4XcIndex = pXcEntry->u4Index;
        if (TeCheckXcIndex (u4XcIndex, &pTeTnlInfo) == TE_FAILURE)
        {
            MPLS_CMN_UNLOCK ();
            return MPLS_FAILURE;
        }
        if ((XC_OPER_STATUS (pXcEntry) == XC_OPER_UP)
            && (pXcEntry->pOutIndex != NULL))
        {
            if (MplsTnlNPAdd (pTeTnlInfo, pXcEntry, (VOID *) pSlotInfo) ==
                MPLS_FAILURE)
            {
                MPLS_CMN_UNLOCK ();
                return MPLS_FAILURE;
						}
            }
    }
    MPLS_CMN_UNLOCK ();
    return MPLS_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : MplsMbsmVplsVpnAdd                                         */
/*                                                                           */
/* Description  : Initialise the MPLS protocol on the line card              */
/*                                                                           */
/* Input        : pSlotInfo - Contains the slot information                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MBSM_SUCCESS or MBSM_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
INT4
MplsMbsmVplsVpnAdd (tMbsmSlotInfo * pSlotInfo)
{
    UINT4               u4Index = 1;
    tVPLSEntry         *pVplsEntry = NULL;
    UINT4               u4VpnId = 0;
    tMplsHwVplsInfo     MplsHwVplsInfo;

    UNUSED_PARAM (u4VpnId);

    MPLS_L2VPN_LOCK ();
    for (u4Index = 1; u4Index <= gL2VpnGlobalInfo.u4MaxVplsEntries; u4Index++)
    {
        pVplsEntry = L2VpnGetVplsEntryFromInstanceIndex (u4Index);
        if (pVplsEntry == NULL)
        {
            continue;
        }
        MEMCPY (&u4VpnId,
                &pVplsEntry->au1VplsVpnID[STRLEN (MPLS_OUI_VPN_ID)],
                sizeof (UINT4));
        u4VpnId = OSIX_HTONL (u4VpnId);
        MEMSET (&MplsHwVplsInfo, 0, sizeof (tMplsHwVplsInfo));
        MplsHwVplsInfo.u2VplsFdbId = L2VPN_VPLS_FDB_ID (pVplsEntry);
        if (L2VPN_VPLS_NAME (pVplsEntry)[0] != 0)
        {
            if (MplsFsMplsMbsmHwCreateVplsVpn (L2VPN_VPLS_INDEX (pVplsEntry),
                                               u4VpnId, &MplsHwVplsInfo,
                                               (VOID *) pSlotInfo) ==
                FNP_FAILURE)
            {
                MPLS_L2VPN_UNLOCK ();
                return MPLS_FAILURE;
            }
        }
    }
    MPLS_L2VPN_UNLOCK ();
    return MPLS_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : MplsMbsmP2mpILMAdd                                         */
/*                                                                           */
/* Description  : Initialise the MPLS protocol on the line card              */
/*                                                                           */
/* Input        : pSlotInfo - Contains the slot information                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MBSM_SUCCESS or MBSM_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
INT4
MplsMbsmP2mpILMAdd (tMbsmSlotInfo * pSlotInfo)
{
    tOutSegment        *pOutSegment = NULL;
    tXcEntry           *pXcEntry = NULL;
    UINT4               u4Index = 0;

    MPLS_CMN_LOCK ();
    for (u4Index = MPLS_ONE; u4Index <= gu4MplsDbMaxOutSegEntries; u4Index++)
    {
        pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
        if (pOutSegment == NULL)
        {
            continue;
        }
        pXcEntry = (tXcEntry *) (pOutSegment->pXcIndex);
        if (pXcEntry != NULL && (XC_OPER_STATUS (pXcEntry) == XC_OPER_UP)
            && (pOutSegment->u1RowStatus == MPLS_STATUS_ACTIVE) &&
            (pXcEntry->pInIndex != NULL))
        {
            if (MplsP2mpILMHwAdd (pOutSegment, (VOID *) pSlotInfo) ==
                MPLS_FAILURE)
            {
                MPLS_CMN_UNLOCK ();
                return MPLS_FAILURE;
            }
        }
    }
    MPLS_CMN_UNLOCK ();
    return MPLS_SUCCESS;
}

#ifdef MPLS_L3VPN_WANTED
/*****************************************************************************/
/*                                                                           */
/* Function     : MplsMbsmL3vpnIngressMapAdd                                 */
/*                                                                           */
/* Description  : Initialise the MPLS protocol on the line card              */
/*                                                                           */
/* Input        : pSlotInfo - Contains the slot information                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MBSM_SUCCESS or MBSM_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
INT4
MplsMbsmL3vpnIngressMapAdd (tMbsmSlotInfo * pSlotInfo)
{
    tL3vpnMplsL3VpnVrfRteEntry *pL3vpnMplsL3VpnVrfRteEntry = NULL;

    MPLS_CMN_LOCK ();
    pL3vpnMplsL3VpnVrfRteEntry = (tL3vpnMplsL3VpnVrfRteEntry *)
        RBTreeGetFirst (L3VPN_RTE_TABLE);
    while (pL3vpnMplsL3VpnVrfRteEntry != NULL)
    {
        MPLS_CMN_UNLOCK ();
        if (MplsL3vpnHwIngressMap
            (pL3vpnMplsL3VpnVrfRteEntry, (VOID *) pSlotInfo) == L3VPN_FAILURE)
        {
            return MPLS_FAILURE;
        }
        MPLS_CMN_LOCK ();
        pL3vpnMplsL3VpnVrfRteEntry = (tL3vpnMplsL3VpnVrfRteEntry *)
            RBTreeGetNext (L3VPN_RTE_TABLE, pL3vpnMplsL3VpnVrfRteEntry, NULL);
    }
    MPLS_CMN_UNLOCK ();

    return MPLS_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : MplsMbsmL3vpnEgressMapAdd                                  */
/*                                                                           */
/* Description  : Initialise the MPLS protocol on the line card              */
/*                                                                           */
/* Input        : pSlotInfo - Contains the slot information                  */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MBSM_SUCCESS or MBSM_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
INT4
MplsMbsmL3vpnEgressMapAdd (tMbsmSlotInfo * pSlotInfo)
{
    tL3VpnBgpRouteLabelEntry *pL3VPNRouteEntry = NULL;

    MPLS_CMN_LOCK ();
    pL3VPNRouteEntry = (tL3VpnBgpRouteLabelEntry *)
        L3vpnGetFirstBgpRouteLabelTable ();
    while (pL3VPNRouteEntry != NULL)
    {
        if (MplsL3vpnHwEgressMap (pL3VPNRouteEntry, (VOID *) pSlotInfo)
            == L3VPN_FAILURE)
        {
            MPLS_CMN_UNLOCK ();
            return MPLS_FAILURE;
        }
        pL3VPNRouteEntry = (tL3VpnBgpRouteLabelEntry *)
            L3vpnGetNextBgpRouteLabelTable (pL3VPNRouteEntry);
    }
    MPLS_CMN_UNLOCK ();

    return MPLS_SUCCESS;
}

#endif
#endif
