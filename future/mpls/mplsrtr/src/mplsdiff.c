/********************************************************************
 *                                                                  *
 * $RCSfile: mplsdiff.c,v $
 *                                                                  *
 * $Id: mplsdiff.c,v 1.7 2012/05/14 14:05:53 siva Exp $                                                                  *
 *
 * $Revision: 1.7 $                                                
 *                                                                  *
 *******************************************************************/

/*---------------------------------------------------------------------------*/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : mplsdiff.c
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS   
 *    MODULE NAME            : MPLS_FM 
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : 
 *-----------------------------------------------------------------------------
 */

#include "mplsincs.h"

/*****************************************************************************/
/* Function Name : MplsInitDiffServTables                                    */
/* Description   : This function initializes the DiffServ related tables used*/
/*                 for maintaining the token space for DiffServParams and    */
/*                 ElspMap Info                                              */
/* Input(s)      : u4IncarnId - Incarnation Id                               */
/* Output(s)     : The related tables get initialized                        */
/* Return(s)     : MPLS_SUCCESS or MPLS_FAILURE                              */
/*****************************************************************************/
UINT4
MplsInitDiffServTables (UINT4 u4IncarnId)
{
    UINT4               u4Index;

    MPLSFM_DBG (MPLSFM_DIFF_ETEXT, "MplsInitDiffServTables : ENTRY\n");

    /* Allocate DiffServParams Table and Token Stack */

    gMplsIncarn[u4IncarnId].DiffServGlobal.ppaDiffServParamsTable =
        MemAllocMemBlk (MPLS_DIFFSERV_PARAMS_TABLE_POOL_ID);

    if (gMplsIncarn[u4IncarnId].DiffServGlobal.ppaDiffServParamsTable == NULL)
    {
        MPLSFM_DBG (MPLSFM_DIFF_ETEXT, "MplsInitDiffServTables :EXIT  \n");
        return MPLS_FAILURE;
    }

    gMplsIncarn[u4IncarnId].DiffServGlobal.pau4DiffServParamsTokenStack =
        MemAllocMemBlk (MPLS_DIFFSERV_PARAMS_TOKEN_STACK_POOL_ID);

    if (gMplsIncarn[u4IncarnId].DiffServGlobal.pau4DiffServParamsTokenStack ==
        NULL)
    {
        MemReleaseMemBlock (MPLS_DIFFSERV_PARAMS_TABLE_POOL_ID,
                            (UINT1 *) gMplsIncarn[u4IncarnId].DiffServGlobal.
                            ppaDiffServParamsTable);

        gMplsIncarn[u4IncarnId].DiffServGlobal.ppaDiffServParamsTable = NULL;

        MPLSFM_DBG (MPLSFM_DIFF_ETEXT, "MplsInitDiffServTables :EXIT  \n");
        return MPLS_FAILURE;
    }

    for (u4Index = 0; u4Index < MPLS_DIFFSERV_PARAMS_ENTRIES (u4IncarnId);
         u4Index++)
    {
        MPLS_DIFFSERV_PARAMS_TOKEN_STACK (u4IncarnId, u4Index)
            = MPLS_DIFFSERV_PARAMS_ENTRIES (u4IncarnId) - u4Index - 1;
        /* Set the DIFFSERV_PARAMS table to NULLs */
        MPLS_DIFFSERV_PARAMS_ENTRY (u4IncarnId, u4Index) = NULL;
    }

    MPLS_DIFFSERV_PARAMS_TOKEN_STACK_TOP (u4IncarnId)
        = MPLS_DIFFSERV_PARAMS_ENTRIES (u4IncarnId);

    /* Allocate Elsp Map Table and Token Stack */
    gMplsIncarn[u4IncarnId].DiffServGlobal.ppaElspMapTable =
        MemAllocMemBlk (MPLS_ELSPMAP_TABLE_POOL_ID);

    if (gMplsIncarn[u4IncarnId].DiffServGlobal.ppaElspMapTable == NULL)
    {

        MemReleaseMemBlock (MPLS_DIFFSERV_PARAMS_TABLE_POOL_ID,
                            (UINT1 *) gMplsIncarn[u4IncarnId].DiffServGlobal.
                            ppaDiffServParamsTable);

        gMplsIncarn[u4IncarnId].DiffServGlobal.ppaDiffServParamsTable = NULL;

        MemReleaseMemBlock (MPLS_DIFFSERV_PARAMS_TOKEN_STACK_POOL_ID,
                            (UINT1 *) gMplsIncarn[u4IncarnId].DiffServGlobal.
                            pau4DiffServParamsTokenStack);

        gMplsIncarn[u4IncarnId].DiffServGlobal.pau4DiffServParamsTokenStack
            = NULL;

        MPLSFM_DBG (MPLSFM_DIFF_ETEXT, "MplsInitDiffServTables :EXIT  \n");
        return MPLS_FAILURE;
    }

    gMplsIncarn[u4IncarnId].DiffServGlobal.pau4ElspMapTokenStack =
        MemAllocMemBlk (MPLS_ELSPMAP_TOKEN_STACK_POOL_ID);

    if (gMplsIncarn[u4IncarnId].DiffServGlobal.pau4ElspMapTokenStack == NULL)
    {

        MemReleaseMemBlock (MPLS_DIFFSERV_PARAMS_TABLE_POOL_ID,
                            (UINT1 *) gMplsIncarn[u4IncarnId].DiffServGlobal.
                            ppaDiffServParamsTable);

        gMplsIncarn[u4IncarnId].DiffServGlobal.ppaDiffServParamsTable = NULL;

        MemReleaseMemBlock (MPLS_DIFFSERV_PARAMS_TOKEN_STACK_POOL_ID,
                            (UINT1 *) gMplsIncarn[u4IncarnId].DiffServGlobal.
                            pau4DiffServParamsTokenStack);

        gMplsIncarn[u4IncarnId].DiffServGlobal.pau4DiffServParamsTokenStack
            = NULL;
        MemReleaseMemBlock (MPLS_ELSPMAP_TABLE_POOL_ID,
                            (UINT1 *) gMplsIncarn[u4IncarnId].DiffServGlobal.
                            ppaElspMapTable);

        gMplsIncarn[u4IncarnId].DiffServGlobal.ppaElspMapTable = NULL;

        MPLSFM_DBG (MPLSFM_DIFF_ETEXT, "MplsInitDiffServTables :EXIT  \n");
        return MPLS_FAILURE;
    }

    for (u4Index = 0; u4Index < MPLS_DIFFSERV_ELSP_MAP_ENTRIES (u4IncarnId);
         u4Index++)
    {
        MPLS_DIFFSERV_ELSP_TOKEN_STACK (u4IncarnId, u4Index)
            = MPLS_DIFFSERV_ELSP_MAP_ENTRIES (u4IncarnId) - u4Index - 1;
        /* Set the ELSP_MAP_ENTRY table to NULLs */
        MPLS_DIFFSERV_ELSP_MAP_ENTRY (u4IncarnId, u4Index) = NULL;
    }

    MPLS_DIFFSERV_ELSP_TOKEN_STACK_TOP (u4IncarnId)
        = MPLS_DIFFSERV_ELSP_MAP_ENTRIES (u4IncarnId);

    MPLSFM_DBG (MPLSFM_DIFF_ETEXT, "MplsInitDiffServTables :EXIT  \n");
    return MPLS_SUCCESS;
}

/*****************************************************************************/
/* Function Name : MplsShutdownDiffServTables                                */
/* Description   : This function is used to destroy the tables used for      */
/*                 maintaining                                               */
/*                 the token space used for DiffServParams and ElspMap Info  */
/* Input(s)      : u4IncarnId - Incarnation Id                               */
/* Output(s)     : The related tables get destroyed                          */
/* Return(s)     : MPLS_SUCCESS or MPLS_FAILURE                              */
/*****************************************************************************/
UINT4
MplsShutdownDiffServTables (UINT4 u4IncarnId)
{
    /* Free DiffServParams Table and Token Stack */
    MPLSFM_DBG (MPLSFM_DIFF_ETEXT, "MplsShutdownDiffServTables : ENTRY\n");

    MemReleaseMemBlock (MPLS_DIFFSERV_PARAMS_TABLE_POOL_ID,
                        (UINT1 *) gMplsIncarn[u4IncarnId].DiffServGlobal.
                        ppaDiffServParamsTable);
    gMplsIncarn[u4IncarnId].DiffServGlobal.ppaDiffServParamsTable = NULL;

    MemReleaseMemBlock (MPLS_DIFFSERV_PARAMS_TOKEN_STACK_POOL_ID,
                        (UINT1 *) gMplsIncarn[u4IncarnId].DiffServGlobal.
                        pau4DiffServParamsTokenStack);
    gMplsIncarn[u4IncarnId].DiffServGlobal.pau4DiffServParamsTokenStack = NULL;

    MemReleaseMemBlock (MPLS_ELSPMAP_TABLE_POOL_ID,
                        (UINT1 *) gMplsIncarn[u4IncarnId].DiffServGlobal.
                        ppaElspMapTable);
    gMplsIncarn[u4IncarnId].DiffServGlobal.ppaElspMapTable = NULL;

    MemReleaseMemBlock (MPLS_ELSPMAP_TOKEN_STACK_POOL_ID,
                        (UINT1 *) gMplsIncarn[u4IncarnId].DiffServGlobal.
                        pau4ElspMapTokenStack);
    gMplsIncarn[u4IncarnId].DiffServGlobal.pau4ElspMapTokenStack = NULL;

    MPLSFM_DBG (MPLSFM_DIFF_ETEXT, "MplsShutdownDiffServTables :EXIT  \n");
    return MPLS_SUCCESS;
}

/*****************************************************************************/
/* Function Name :MplsAllocMemDiffServParams                                 */
/* Description   : This function Allocates memory for a DiffServParams record*/
/*                 in an Nhlfe                                               */
/* Input(s)      : u4Incarn - Incarnation Id                                 */
/*                 pNhlfe  - Pointer to the Nhlfe Info structure in which    */
/*                 the DiffServParams Info needs to be allocated             */
/* Output(s)     : The DiffServParams Structure gets allocated and           */
/*                 initialized                                               */
/* Return(s)     : MPLS_SUCCESS or MPLS_FAILURE                              */
/*****************************************************************************/
UINT4
MplsAllocMemDiffServParams (UINT4 u4Incarn, tNHLFE * pNhlfe)
{
    UINT4               u4DiffParamsId;

    MPLSFM_DBG (MPLSFM_DIFF_ETEXT, "MplsAllocMemDiffServParams : ENTRY\n");

    if (pNhlfe == NULL)
    {
        /* pNhlfe should not be NULL */
        MPLSFM_DBG (MPLSFM_DIFF_ETEXT,
                    "MplsAllocMemDiffServParams :INTMD-EXIT\n");
        return MPLS_FAILURE;
    }

    pNhlfe->pDiffServParams = NULL;

    if (MplsGetDiffServParamsIndexNext (u4Incarn, &u4DiffParamsId) !=
        MPLS_SUCCESS)
    {

        MPLSFM_DBG (MPLSFM_DIFF_ETEXT, "MplsAllocMemDiffServParams :EXIT  \n");

        return MPLS_FAILURE;
    }

    pNhlfe->pDiffServParams = (tMplsDiffServParams *)
        MemAllocMemBlk (MPLS_DIFFSERV_PARAMS_POOL_ID);

    if (pNhlfe->pDiffServParams == NULL)
    {
        MPLSFM_DBG (MPLSFM_DIFF_ETEXT, "MplsAllocMemDiffServParams :EXIT  \n");
        return MPLS_FAILURE;
    }
    pNhlfe->pDiffServParams->u4DiffServParamsIndex = u4DiffParamsId;
    pNhlfe->pDiffServParams->u1Creator = MPLS_CTRL_PROTO;
    MPLS_DIFFSERV_PARAMS_ENTRY (u4Incarn, u4DiffParamsId) =
        pNhlfe->pDiffServParams;
    MPLS_DIFFSERV_PARAMS_TOKEN_STACK_TOP (u4Incarn)--;

    pNhlfe->pDiffServParams->u1ServiceType = MPLS_GEN_LSP;

    MPLSFM_DBG (MPLSFM_DIFF_ETEXT, "MplsAllocMemDiffServParams :EXIT  \n");
    return MPLS_SUCCESS;
}

/*****************************************************************************/
/* Function Name :MplsRelMemDiffServParams                                   */
/* Description   : This function releases the DiffServParams Info from an    */
/*                 Nhlfe structure                                           */
/* Input(s)      : u4Incarn - Incarnation Id                                 */
/*                 pNhlfe - Pointer to the Nhlfe Info structure from which   */
/*                 the DiffServParams Info needs to be freed                 */
/* Output(s)     : The DiffServParams Info gets freed                        */
/* Return(s)     : MPLS_SUCCESS or MPLS_FAILURE                              */
/*****************************************************************************/
UINT4
MplsRelMemDiffServParams (UINT4 u4Incarn, tNHLFE * pNhlfe)
{
    /* For Preconfigured ELSP and for LLSP the DiffServParams need 
     * to be freed  For Signaled ELSP the EXP to PHB map needs to be also 
     * freed.The preconfigured Map table in case of Signaled Elsps should be 
     * allocated and freed during initialization and shutdown of entire module
     */

    MPLSFM_DBG (MPLSFM_DIFF_ETEXT, "MplsRelMemDiffServParams : ENTRY\n");

    if ((pNhlfe == NULL) || (pNhlfe->pDiffServParams == NULL))
    {
        /* pNhlfe or pDiffServParams field inside pNhlfe should not be NULL */
        MPLSFM_DBG (MPLSFM_DIFF_ETEXT, "MplsRelMemDiffServParams :EXIT  \n");
        return MPLS_FAILURE;
    }

    MPLS_DIFFSERV_PARAMS_ENTRY (u4Incarn,
                                pNhlfe->pDiffServParams->
                                u4DiffServParamsIndex) = NULL;
    MPLS_DIFFSERV_PARAMS_TOKEN_STACK (u4Incarn,
                                      MPLS_DIFFSERV_PARAMS_TOKEN_STACK_TOP
                                      (u4Incarn)) =
        pNhlfe->pDiffServParams->u4DiffServParamsIndex;
    /* This cannot go above max because you cannot release more blocks
     * than allocated
     */
    MPLS_DIFFSERV_PARAMS_TOKEN_STACK_TOP (u4Incarn)++;

    MemReleaseMemBlock (MPLS_DIFFSERV_PARAMS_POOL_ID,
                        (UINT1 *) (pNhlfe->pDiffServParams));
    pNhlfe->pDiffServParams = NULL;

    MPLSFM_DBG (MPLSFM_DIFF_ETEXT, "MplsRelMemDiffServParams :EXIT  \n");
    return MPLS_SUCCESS;
}

/*****************************************************************************/
/* Function Name : MplsDiffServValidatePhbDscp                                */
/* Description   : This function validates the Phb Dscp value                */
/* Input(s)      : u1PhbDscp - Phb Dscp value                                */
/* Output(s)     : None                                                      */
/* Return(s)     : MPLS_SUCCESS or MPLS_FAILURE                              */
/*****************************************************************************/
UINT4
MplsDiffServValidatePhbDscp (UINT1 u1PhbDscp)
{
    MPLSFM_DBG (MPLSFM_DIFF_ETEXT, "MplsDiffServValidatePhbDscp :ENTRY\n");
    switch (u1PhbDscp)
    {
        case MPLS_DIFFSERV_DF_DSCP:
        case MPLS_DIFFSERV_CS1_DSCP:
        case MPLS_DIFFSERV_CS2_DSCP:
        case MPLS_DIFFSERV_CS3_DSCP:
        case MPLS_DIFFSERV_CS4_DSCP:
        case MPLS_DIFFSERV_CS5_DSCP:
        case MPLS_DIFFSERV_CS6_DSCP:
        case MPLS_DIFFSERV_CS7_DSCP:
        case MPLS_DIFFSERV_EF_DSCP:
        case MPLS_DIFFSERV_AF11_DSCP:
        case MPLS_DIFFSERV_AF12_DSCP:
        case MPLS_DIFFSERV_AF13_DSCP:
        case MPLS_DIFFSERV_AF21_DSCP:
        case MPLS_DIFFSERV_AF22_DSCP:
        case MPLS_DIFFSERV_AF23_DSCP:
        case MPLS_DIFFSERV_AF31_DSCP:
        case MPLS_DIFFSERV_AF32_DSCP:
        case MPLS_DIFFSERV_AF33_DSCP:
        case MPLS_DIFFSERV_AF41_DSCP:
        case MPLS_DIFFSERV_AF42_DSCP:
        case MPLS_DIFFSERV_AF43_DSCP:
            break;
        default:
            MPLSFM_DBG (MPLSFM_DIFF_ETEXT,
                        "MplsDiffServValidatePhbDscp :INTMD-EXIT\n");
            return MPLS_FAILURE;
    }

    MPLSFM_DBG (MPLSFM_DIFF_ETEXT, "MplsDiffServValidatePhbDscp :EXIT\n");
    return MPLS_SUCCESS;
}

/*****************************************************************************/
/* Function Name :MplsDiffServValidatePscDscp                                */
/* Description   : This function validates the Psc Dscp value                */
/* Input(s)      : u1PscDscp - Psc Dscp value                                */
/* Output(s)     : None                                                      */
/* Return(s)     : MPLS_SUCCESS or MPLS_FAILURE                              */
/*****************************************************************************/
UINT4
MplsDiffServValidatePscDscp (UINT1 u1PscDscp)
{
    MPLSFM_DBG (MPLSFM_DIFF_ETEXT, "MplsDiffServValidatePscDscp :ENTRY\n");
    switch (u1PscDscp)
    {
        case MPLS_DIFFSERV_DF_DSCP:
        case MPLS_DIFFSERV_CS1_DSCP:
        case MPLS_DIFFSERV_CS2_DSCP:
        case MPLS_DIFFSERV_CS3_DSCP:
        case MPLS_DIFFSERV_CS4_DSCP:
        case MPLS_DIFFSERV_CS5_DSCP:
        case MPLS_DIFFSERV_CS6_DSCP:
        case MPLS_DIFFSERV_CS7_DSCP:
        case MPLS_DIFFSERV_EF_DSCP:
        case MPLS_DIFFSERV_AF1_PSC_DSCP:
        case MPLS_DIFFSERV_AF2_PSC_DSCP:
        case MPLS_DIFFSERV_AF3_PSC_DSCP:
        case MPLS_DIFFSERV_AF4_PSC_DSCP:
            break;
        default:
            MPLSFM_DBG (MPLSFM_DIFF_ETEXT,
                        "MplsDiffServValidatePscDscp :INTMD-EXIT\n");
            return MPLS_FAILURE;
    }

    MPLSFM_DBG (MPLSFM_DIFF_ETEXT, "MplsDiffServValidatePscDscp :EXIT\n");
    return MPLS_SUCCESS;
}

/*****************************************************************************/
/* Function Name : MplsDiffServGetPreConfMap                                 */
/* Description   : This function copies the Preconfigured map from its Global*/
/*                 structure into the Array passed by reference              */
/* Input(s)      : u4Incarn  - Incarn Id                                     */
/* Output(s)     : aElspPhbMapEntry - A PreAllocated array of size           */
/*                 MPLS_DIFFSERV_MAX_EXP into which the Preconfigured map is */
/*                 copied                                                    */
/* Return(s)     : MPLS_SUCCESS or MPLS_FAILURE                              */
/*****************************************************************************/
UINT4
MplsDiffServGetPreConfMap (UINT4 u4Incarn, tElspPhbMapEntry aElspPhbMapEntry[])
{
    UINT1               u1Exp;
    tElspPhbMapEntry   *pPreConfMapEntry = NULL;

    if ((u4Incarn >= MPLS_MAX_INCARNS) ||
        (MPLS_ADMIN_STATUS (u4Incarn) != MPLS_ADMIN_STATUS_UP))
    {
        return MPLS_FAILURE;
    }
    if (IS_MPLS_DIFFSERV_ENABLED (u4Incarn) == MPLS_ZERO)
    {
        return MPLS_FAILURE;
    }
    if (gMplsIncarn[u4Incarn].DiffServGlobal.u4DiffServStatus
        == MPLS_DSTE_STATUS_DISABLE)
    {
        return MPLS_FAILURE;
    }
    if (gMplsIncarn[u4Incarn].DiffServGlobal.pElspPreConfMap == NULL)
    {
        return MPLS_FAILURE;
    }
    pPreConfMapEntry = gMplsIncarn[u4Incarn].DiffServGlobal.pElspPreConfMap->
        aElspExpPhbMapArray;
    for (u1Exp = 0; u1Exp < MPLS_DIFFSERV_MAX_EXP; u1Exp++)
    {
        aElspPhbMapEntry[u1Exp].u1IsExpValid
            = pPreConfMapEntry[u1Exp].u1IsExpValid;
        aElspPhbMapEntry[u1Exp].u1ElspPhbDscp
            = pPreConfMapEntry[u1Exp].u1ElspPhbDscp;
    }
    return MPLS_SUCCESS;
}
