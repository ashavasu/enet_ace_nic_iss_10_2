/*---------------------------------------------------------------------------*/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: mpoamrx.c,v 1.23 2017/11/09 13:22:14 siva Exp $
 * -----------------------------------------------------------------------------
 * FILE  NAME             : mpoamrx.c
 * PRINCIPAL AUTHOR       : Aricent Inc.
 * SUBSYSTEM NAME         : MPLS
 * MODULE NAME            : MPLS
 * LANGUAGE               : ANSI-C
 * TARGET ENVIRONMENT     : Linux (Portable)
 * DATE OF FIRST RELEASE  : 17-SEP-2010
 * DESCRIPTION            : This file contains the function related to
 *                          OAM processing.
 * ---------------------------------------------------------------------------*/

#include "mplsdefs.h"
#include "mplsincs.h"
#include "mpoamdef.h"
#include "ipv6.h"
extern INT4
 
 
 
 
MplsCmnExtGetPathFromInLblInfo (tMplsApiInInfo * pInMplsApiInfo,
                                tMplsApiOutInfo * pOutMplsApiInfo,
                                BOOL1 * pbIsTnlOrLspExists);

PRIVATE VOID        MplsOamRxBufDump (UINT1 *, UINT1 *, INT4);

/* *************************************************************************** *
 * Function Name   : MplsOamRxBufDump                                          *
 * Description     : Function to display contents in MPLS packet.              *
 * Input(s)        : pu1Buf - Pointer pointing to the first MPLS label of the  *
 *                            packet.                                          *
 *                 : pu1Title - Help string for packet dump                    *
 *                 : i4Len - Length of the packet                              *
 * Output(s)       : None                                                      *
 * Return(s)       : None                                                      *
 * *************************************************************************  */

PRIVATE VOID
MplsOamRxBufDump (UINT1 *pu1Buf, UINT1 *pu1Title, INT4 i4Len)
{
    INT4                i4Tmp = 0;

    if (!pu1Buf)
    {
        return;
    }

    printf ("\n%s\n", pu1Title);
    for (i4Tmp = 0; i4Tmp < i4Len; i4Tmp++)
    {
        if (i4Tmp)
        {
            if (!(i4Tmp % 24))
            {
                printf ("\n");
            }
            else
            {
                if (!(i4Tmp % 8))
                {
                    printf ("  ");
                }
                else
                {
                    if (i4Tmp)
                    {
                        printf (" ");
                    }
                }
            }
        }
        printf ("%02X", pu1Buf[i4Tmp]);
    }
    printf ("\n");
}

/* ************************************************************************** *
 * Function Name  : MplsOamRxHandleIpv4CtrlPacket                             *
 * Description    : Processes the packet which contains IP header.            *
 * Input(s)       : pu1Pkt - Pointer pointing to the IP header of the packet. *
 * Output(s)      : pu1IpProto -Protocol value, which is present in IP header.*
 *                : pu2UdpDstPort - UDP destination port value.               *
 * Return(s)      : None                                                      *
 * ************************************************************************   */
VOID
MplsOamRxHandleIpv4CtrlPacket (UINT1 *pu1Pkt, UINT1 *pu1IpProto,
                               UINT2 *pu2UdpDstPort, UINT4 *pu4DstIpAddress)
{
    UINT4               u4DstIpAddress = 0;
    UINT1               u1IpHdrLength = 0;
    UINT1               u1Offset = 0;
    UINT1               u1IpTtl = 0;
    UINT1               u1IpHdrOpt = 0;

    /* Verifying TTL value is 1 or not in IP header */
    u1IpTtl = *((UINT1 *) (pu1Pkt + MPLS_TTL_OFFSET_IN_IP));
    if (u1IpTtl == 1)
    {
        MPLSFM_DBG (MPLSFM_PRCS_PRCS, "IP header TTL count reaches to 1\n");
    }

    u1IpHdrLength = *((UINT1 *) pu1Pkt);
    /* The minimum value for Header Length field is 5, which
     * is a length of 5*32 = 160 bits = 20 bytes. Being a 4-bit
     * value, the maximum length is 15 words (15*32 bits) or
     * 480 bits = 60 bytes. If this field contains value more
     * than 5, then IP header contains Options.
     */

    if ((u1IpHdrLength & IP_HEADER_LEN_MASK) > MIN_IP_HDR_LEN_IN_32BIT_WORD)
    {
        /* If IP header length field value is > 5, then
         * IP header will contain option field.
         *
         * If option type(1 bit copy flag + 2 bits class
         * + 5 bits option) value is 20 or 0x14 and value
         * filed is 0. This is router alert option.
         *
         * Router Alert option format:
         *
         * 1 byte     1 byte      2byte
         * ------------------------------
         * | type   |  length  |  value  |
         * ------------------------------
         */

        u1IpHdrOpt = *((UINT1 *) (pu1Pkt + IP_HDR_LEN));

        u1Offset = IP_HDR_LEN + IP_ROUTER_ALERT_VALUE_OFFSET;
        if (((u1IpHdrOpt & MPLS_ROUTE_ALERT_OPTION_MASK) ==
             MPLS_ROUTE_ALERT_OPTION) &&
            (*((UINT2 *) (VOID *) (pu1Pkt + u1Offset)) == 0))
        {
            MPLSFM_DBG (MPLSFM_PRCS_PRCS, "IP header contains "
                        "Router Alert option\n");
        }
    }

    /* verifying destination IP address is loopback or not */
    PTR_FETCH4 (u4DstIpAddress, pu1Pkt + IP_PKT_OFF_DEST);
    *pu4DstIpAddress = u4DstIpAddress;
    if ((u4DstIpAddress & MPLS_4TH_BYTE_MASK) == IP_LOOPBACK_ADDRESS)
    {
        MPLSFM_DBG (MPLSFM_PRCS_PRCS, "IP header contains "
                    "loopback destination address\n");
    }

    *pu1IpProto = *((UINT1 *) (pu1Pkt + MPLS_HL_PROTID_OFFSET));
    u1IpHdrLength = u1IpHdrLength & IP_HEADER_LEN_MASK;

    /* Here we are getting the Destination port from
     * UDP header based on length of IP header. If in case
     * any options in IP header(20 bytes + option bytes) +
     * ACH header(MPLS_HDR_LEN) + UDP source port(2 bytes).
     */
    u1Offset = (UINT1) ((u1IpHdrLength * MPLS_HDR_LEN) + UDP_DST_PORT_OFFSET);
    PTR_FETCH2 (*pu2UdpDstPort, pu1Pkt + u1Offset);
}

/* ************************************************************************** *
 * Function Name  : MplsOamRxHandleIpv6CtrlPacket                             *
 * Description    : Processes the packet which contains IPV6 header.          *
 * Input(s)       : pu1Pkt -Pointer pointing to the IPV6 header of the packet.*
 * Output(s)      : pu1IpProto - Next header value, which is present          *
 *                               in IPV6 header.                              *
 *                : pu2UdpDstPort - UDP destination port value.               *
 * Return(s)      : None                                                      *
 * ************************************************************************   */
VOID
MplsOamRxHandleIpv6CtrlPacket (UINT1 *pu1Pkt, UINT1 *pu1IpProto,
                               UINT2 *pu2UdpDstPort)
{
    tIp6Addr            Ipv6Addr;
    UINT1               u1HopLimit = 0;
    UINT1               u1Offset = 0;

    /* Verifying Hoplimit value is 1 or not in IPV6 header */
    u1HopLimit = *((UINT1 *) (pu1Pkt + IP6_OFFSET_FOR_HOPLIMIT_FIELD));
    if (u1HopLimit == 1)
    {
        MPLSFM_DBG (MPLSFM_PRCS_PRCS, "IPV6 header "
                    "hoplimit count reaches to 1\n");
    }

    /* verifying destination IPV6 address is loopback or not */
    MEMCPY (Ipv6Addr.u4_addr, pu1Pkt + IP6_OFFSET_FOR_DESTADDR_FIELD,
            IP6_ADDR_SIZE);

    if (IS_MPLS_IPV6_ADDR_LOOPBACK (Ipv6Addr))
    {
        MPLSFM_DBG (MPLSFM_PRCS_PRCS, "IPV6 header contains "
                    "loopback destination address\n");
    }

    *pu1IpProto = *((UINT1 *) (pu1Pkt + IP6_OFFSET_FOR_NEXT_HDR_FIELD));
    /* Here we are getting the Destination port from
     * UDP header based on length of IPv6 header(40 bytes) +
     * ACH header(MPLS_HDR_LEN) + UDP source port(2 bytes).
     */
    u1Offset = IPV6_HEADER_LEN + UDP_DST_PORT_OFFSET;
    PTR_FETCH2 (*pu2UdpDstPort, pu1Pkt + u1Offset);
}

tMplsEventNotif     MplsEventNotif;
/* ************************************************************************** *
 * Function Name  : MplsOamRxHandleCtrlPkt                                    *
 * Description    : Processes the incoming MPLS labeled packet which contains *
 *                  ACH header, Router Alert Label and TTL expiry.            *
 * Input(s)       : pBuf - CRU buffer                                         *
 *                  pu1Pkt - Linear pointer pointing to the first MPLS label  *
 *                           of the packet.                                   *
 *                : u4PktSize - Size of the packet                            *
 * Output(s)      : None                                                      *
 * Return(s)      : MPLS_FAILURE/MPLS_SUCCESS                                 *
 * ************************************************************************   */
INT4
MplsOamRxHandleCtrlPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pu1Pkt,
                        UINT4 u4IfIndex, UINT4 u4PktSize)
{
    tMplsHdr            MplsHdr;
    tMplsApiInInfo     *pInMplsApiInfo = NULL;
    tMplsApiOutInfo    *pOutMplsApiInfo = NULL;
    UINT4               u4LastLabel = 0;
    UINT4               u4PrevLabel = 0;
    UINT4               u4DstIpAddress = 0;
    UINT2               u2AchChnlType = 0;
    UINT2               u2AchTlvLen = 0;
    UINT2               u2UdpDstPort = 0;
    UINT1               u1IpProto = 0;
    UINT1               u1Version = 0;
    UINT1               u1AchValue = 0;
    UINT1               u1Reserved = 0;
    UINT1               u1AchVersion = 0;
    UINT1              *pu1TmpBuf = pu1Pkt;
    CHR1                ac1Tmp[MPLS_BUFFER_LENGTH];
    BOOL1               bIsIpv4Pkt = FALSE;
    BOOL1               bIsIpv6Pkt = FALSE;
    BOOL1               bFirstLbl = TRUE;
    BOOL1               bIsTnlOrLspExists = FALSE;

    SNPRINTF (ac1Tmp, sizeof (ac1Tmp),
              "MPLS: PktDump: IfIndex : %d PktSize: %d\n", u4IfIndex,
              u4PktSize);

    MEMSET (&MplsEventNotif, 0, sizeof (tMplsEventNotif));

    while (1)
    {
        if (!bFirstLbl)
        {
            u4PrevLabel = MplsHdr.u4Lbl;
        }

        MplsReadMplsHdr (pu1Pkt, &MplsHdr);
        if (bFirstLbl)
        {
            u4PrevLabel = MplsHdr.u4Lbl;
            bFirstLbl = FALSE;
        }
        if (MplsHdr.Ttl == 1)
        {
            MPLSFM_DBG (MPLSFM_PRCS_PRCS, "TTL expired\n");
        }

        if (MplsHdr.u4Lbl == MPLS_ROUTER_ALERT_LABEL)
        {
            MPLSFM_DBG (MPLSFM_PRCS_PRCS, "Router Alert Label in Packet\n");
        }

        if (MplsHdr.SI == 0)
        {
            if (MplsHdr.u4Lbl == MPLS_GAL_LABEL)
            {
                /* NOTE : This code has been implemented based on
                 * section 4.2 from RFC 5585.
                 *
                 * Currently we are giving support only for MPLS-TP
                 * environment. So, GAL MUST always be at the bottom
                 * of the label stack followed by ACH header.
                 *
                 * Remove this check for other MPLS environments.
                 */
                MPLSFM_DBG (MPLSFM_PRCS_PRCS, "GAL label MUST be at "
                            "the bottom of the label stack\n");
                return MPLS_FAILURE;
            }
            pu1Pkt += MPLS_HDR_LEN;
            continue;
        }

        pu1Pkt += MPLS_HDR_LEN;
        /* Here we are going to verify whether packet contains
         * ACH header or not.
         */
        u1AchValue = *((UINT1 *) pu1Pkt) & MPLS_ACH_TYPE_MASK;

        /* Here we are going to verify GAL label followed by
         * ACH header
         */
        if ((MplsHdr.u4Lbl == MPLS_GAL_LABEL) &&
            (u1AchValue != MPLS_ACH_HEADER))
        {
            MPLSFM_DBG (MPLSFM_PRCS_PRCS, "GAL label not followed by"
                        " ACH header");
            return MPLS_FAILURE;
        }

        if (u1AchValue == MPLS_ACH_HEADER)
        {
            /* Version field of ACH Header should be zero */
            u1AchVersion = *((UINT1 *) pu1Pkt) & MPLS_ACH_VERSION_MASK;
            if (u1AchVersion != 0)
            {
                MPLSFM_DBG (MPLSFM_PRCS_PRCS, "Version field not zero in"
                            " ACH header\n");
                return MPLS_FAILURE;
            }
        }

        if (MplsHdr.u4Lbl == MPLS_GAL_LABEL)
        {
            u4LastLabel = u4PrevLabel;
        }
        else
        {
            u4LastLabel = MplsHdr.u4Lbl;
        }

        if (MPLSFM_DBG_FLAG & MPLSFM_PKT_DUMP)
        {
            MplsOamRxBufDump (pu1TmpBuf, (UINT1 *) ac1Tmp, (INT4) u4PktSize);
        }

        if (u1AchValue == MPLS_ACH_HEADER)
        {
            MPLSFM_DBG (MPLSFM_PRCS_PRCS, "ACH Header in Packet\n");

            /* Reserved field of ACH Header should be zero */
            u1Reserved = *(pu1Pkt + 1);
            if (u1Reserved != 0)
            {
                MPLSFM_DBG (MPLSFM_PRCS_PRCS, "Reserved field not zero in"
                            " ACH header\n");
                return MPLS_FAILURE;
            }

            PTR_FETCH2 (u2AchChnlType, pu1Pkt + MPLS_ACH_CHNL_TYPE_OFFSET);
            if ((u2AchChnlType == MPLS_ACH_CHANNEL_CC_BFD) ||
                (u2AchChnlType == MPLS_ACH_CHANNEL_CV_BFD))
            {
                MPLSFM_DBG (MPLSFM_PRCS_PRCS, "BFD Packet\n");
                CRU_BUF_Move_ValidOffset (pBuf, CFA_ENET_V2_HEADER_SIZE);
                MplsEventNotif.pBuf = pBuf;
                MplsEventNotif.u4ContextId = MPLS_DEF_CONTEXT_ID;
                MplsEventNotif.u4InIfIndex = u4IfIndex;
                MplsEventNotif.u1PayloadOffset = (UINT1) ((pu1Pkt - pu1TmpBuf));
                MplsEventNotif.u1ServiceType = 0;
                MplsEventNotif.u2Event = MPLS_BFD_PACKET;
                MplsPortEventNotification (MPLS_APPLICATION_BFD,
                                           &MplsEventNotif);
                return MPLS_SUCCESS;
            }
            else if (u2AchChnlType == MPLS_ACH_CHANNEL_LSP_PING)
            {
                MPLSFM_DBG (MPLSFM_PRCS_PRCS, "LSP Ping Packet\n");
                /* LSP Ping and PSC specific ACH channel code */
                CRU_BUF_Move_ValidOffset (pBuf, CFA_ENET_V2_HEADER_SIZE);
                MplsEventNotif.pBuf = pBuf;
                MplsEventNotif.u4ContextId = MPLS_DEF_CONTEXT_ID;
                MplsEventNotif.u4InIfIndex = u4IfIndex;
                MplsEventNotif.u1PayloadOffset = (UINT1) ((pu1Pkt - pu1TmpBuf));
                MplsEventNotif.u1ServiceType = 0;
                MplsEventNotif.u2Event = MPLS_LSPPING_PACKET;
                MplsPortEventNotification (MPLS_APPLICATION_LSP_PING,
                                           &MplsEventNotif);
                return MPLS_SUCCESS;
            }
            else if (u2AchChnlType == MPLS_ACH_CHANNEL_Y1731)
            {
                MPLSFM_DBG (MPLSFM_PRCS_PRCS, "Y.1731 Packet\n");
                CRU_BUF_Move_ValidOffset (pBuf, CFA_ENET_V2_HEADER_SIZE);
                MplsEventNotif.pBuf = pBuf;
                MplsEventNotif.u4ContextId = MPLS_DEF_CONTEXT_ID;
                MplsEventNotif.u4InIfIndex = u4IfIndex;
                MplsEventNotif.u1PayloadOffset = (UINT1) ((pu1Pkt - pu1TmpBuf));
                MplsEventNotif.u1ServiceType = 0;
                MplsEventNotif.u2Event = MPLS_Y1731_PACKET;
                MplsPortEventNotification (MPLS_APPLICATION_Y1731,
                                           &MplsEventNotif);
                return MPLS_SUCCESS;
            }
#ifdef RFC6374_WANTED
            else if ((u2AchChnlType == MPLS_ACH_CHANNEL_TYPE_A_RFC6374) ||
                     (u2AchChnlType == MPLS_ACH_CHANNEL_TYPE_B_RFC6374) ||
                     (u2AchChnlType == MPLS_ACH_CHANNEL_TYPE_C_RFC6374) ||
                     (u2AchChnlType == MPLS_ACH_CHANNEL_TYPE_D_RFC6374) ||
                     (u2AchChnlType == MPLS_ACH_CHANNEL_TYPE_E_RFC6374))
            {
                MPLSFM_DBG (MPLSFM_PRCS_PRCS, "RFC6374 Packet\n");
                CRU_BUF_Move_ValidOffset (pBuf, CFA_ENET_V2_HEADER_SIZE);
                MplsEventNotif.pBuf = pBuf;
                MplsEventNotif.u4ContextId = MPLS_DEF_CONTEXT_ID;
                MplsEventNotif.u4InIfIndex = u4IfIndex;
                MplsEventNotif.u1PayloadOffset = (UINT1) ((pu1Pkt - pu1TmpBuf));
                MplsEventNotif.u1ServiceType = 0;
                MplsEventNotif.u2Event = MPLS_RFC6374_PACKET;
                MplsPortEventNotification (MPLS_APPLICATION_RFC6374,
                                           &MplsEventNotif);
                return MPLS_SUCCESS;
            }
#endif
            else if (MPLS_ACH_PSC_CHNL_CODE (MPLS_DEF_CONTEXT_ID) ==
                     u2AchChnlType)
            {
                pInMplsApiInfo =
                    (tMplsApiInInfo *)
                    MemAllocMemBlk (MPLS_API_IN_INFO_POOL_ID);
                if (pInMplsApiInfo == NULL)
                {
                    return MPLS_FAILURE;
                }

                pOutMplsApiInfo =
                    (tMplsApiOutInfo *)
                    MemAllocMemBlk (MPLS_API_OUT_INFO_POOL_ID);
                if (pOutMplsApiInfo == NULL)
                {
                    MemReleaseMemBlock (MPLS_API_IN_INFO_POOL_ID,
                                        (UINT1 *) pInMplsApiInfo);
                    return MPLS_FAILURE;
                }

                MEMSET (pInMplsApiInfo, 0, sizeof (tMplsApiInInfo));
                MEMSET (pOutMplsApiInfo, 0, sizeof (tMplsApiOutInfo));

                MPLSFM_DBG (MPLSFM_PRCS_PRCS, "PSC Packet\n");
                pInMplsApiInfo->InInLblInfo.u4InIf = u4IfIndex;
                pInMplsApiInfo->InInLblInfo.u4Inlabel = u4LastLabel;
                MPLS_CMN_LOCK ();
                if (MplsCmnExtGetPathFromInLblInfo (pInMplsApiInfo,
                                                    pOutMplsApiInfo,
                                                    &bIsTnlOrLspExists)
                    == OSIX_FAILURE)
                {
                    MPLSFM_DBG (MPLSFM_PRCS_PRCS,
                                "Path (LSP/Tunnel) information doesnot exist"
                                "for incoming LSE\n");
                    MPLS_CMN_UNLOCK ();
                    MemReleaseMemBlock (MPLS_API_IN_INFO_POOL_ID,
                                        (UINT1 *) pInMplsApiInfo);
                    MemReleaseMemBlock (MPLS_API_OUT_INFO_POOL_ID,
                                        (UINT1 *) pOutMplsApiInfo);
                    return MPLS_FAILURE;
                }
                MPLS_CMN_UNLOCK ();
                if (bIsTnlOrLspExists == FALSE)
                {
                    /* Return MEG information if MEG is enabled for Tunnel/PW */
                    /* Search the inlabel in PW table and return the PW 
                     * information */

                    if (L2VpnApiHandleExternalRequest
                        (MPLS_GET_PATH_FROM_INLBL_INFO,
                         pInMplsApiInfo, pOutMplsApiInfo) == OSIX_FAILURE)
                    {
                        MPLSFM_DBG (MPLSFM_PRCS_PRCS,
                                    "Path (PW) information doesnot exist"
                                    "for incoming LSE\n");
                        MemReleaseMemBlock (MPLS_API_IN_INFO_POOL_ID,
                                            (UINT1 *) pInMplsApiInfo);
                        MemReleaseMemBlock (MPLS_API_OUT_INFO_POOL_ID,
                                            (UINT1 *) pOutMplsApiInfo);
                        return MPLS_FAILURE;
                    }

                }
                if (pOutMplsApiInfo->u4PathType != MPLS_PATH_TYPE_MEG_ID)
                {
                    MPLSFM_DBG (MPLSFM_PRCS_PRCS,
                                "Path (LSP/PW) information should be "
                                "associated with MEG for PSC packets handling\n");
                    MemReleaseMemBlock (MPLS_API_IN_INFO_POOL_ID,
                                        (UINT1 *) pInMplsApiInfo);
                    MemReleaseMemBlock (MPLS_API_OUT_INFO_POOL_ID,
                                        (UINT1 *) pOutMplsApiInfo);
                    return MPLS_FAILURE;
                }

                CRU_BUF_Move_ValidOffset (pBuf, CFA_ENET_V2_HEADER_SIZE);
                MplsEventNotif.pBuf = pBuf;
                MplsEventNotif.u4ContextId = MPLS_DEF_CONTEXT_ID;
                MplsEventNotif.u4InIfIndex = u4IfIndex;
                MplsEventNotif.PathId.u4PathType = pOutMplsApiInfo->u4PathType;
                MplsEventNotif.PathId.MegId.u4MegIndex =
                    pOutMplsApiInfo->OutMegInfo.MplsMegId.u4MegIndex;
                MplsEventNotif.PathId.MegId.u4MeIndex =
                    pOutMplsApiInfo->OutMegInfo.MplsMegId.u4MeIndex;
                MplsEventNotif.PathId.MegId.u4MpIndex =
                    pOutMplsApiInfo->OutMegInfo.MplsMegId.u4MpIndex;
                MplsEventNotif.u1PayloadOffset = (UINT1) ((pu1Pkt - pu1TmpBuf));
                MplsEventNotif.u1ServiceType =
                    pOutMplsApiInfo->OutMegInfo.u1ServiceType;
                MplsEventNotif.u2Event = MPLS_PSC_PACKET;
                MplsPortEventNotification (MPLS_APPLICATION_ELPS,
                                           &MplsEventNotif);
                MemReleaseMemBlock (MPLS_API_IN_INFO_POOL_ID,
                                    (UINT1 *) pInMplsApiInfo);
                MemReleaseMemBlock (MPLS_API_OUT_INFO_POOL_ID,
                                    (UINT1 *) pOutMplsApiInfo);
                return MPLS_SUCCESS;
            }

            /* Move the ACH header */
            pu1Pkt += MPLS_ACH_HEADER_LENGTH;
            if (u2AchChnlType == MPLS_ACH_CHANNEL_CC_IPV4)
            {
                bIsIpv4Pkt = TRUE;
            }
            else if (u2AchChnlType == MPLS_ACH_CHANNEL_CV_IPV4)
            {
                /* Fetch ACH TLV header length */
                PTR_FETCH2 (u2AchTlvLen, pu1Pkt);
                /* Move the ACH TLV header */
                pu1Pkt += MPLS_ACH_TLV_HEADER_LENGTH;
                bIsIpv4Pkt = TRUE;
            }
            else if ((u2AchChnlType == MPLS_ACH_CHANNEL_CC_IPV6) ||
                     (u2AchChnlType == MPLS_ACH_CHANNEL_CV_IPV6))
            {
                /* Fetch ACH TLV header length */
                PTR_FETCH2 (u2AchTlvLen, pu1Pkt);
                /* Move the ACH TLV header */
                pu1Pkt += MPLS_ACH_TLV_HEADER_LENGTH;
                bIsIpv6Pkt = TRUE;
            }
            /* NOTE : Here we are going to add code for LSP Ping and PSC,
             * which are coming in ACH header.
             */
            else
            {
                MPLSFM_DBG (MPLSFM_PRCS_PRCS, "Invalid Channel type in "
                            "ACH Header\n");
                return MPLS_FAILURE;
            }
            /* Skip the ACH TLVs to validate IP header */
            pu1Pkt += u2AchTlvLen;
        }
        else
        {
            /* Here we are going to process normal IP/IPV6 packet
             * without ACH header.
             */

            /* Here we are diffrentiating the network protocol
             * based on version filed of the header followed by
             * MPLS label stack.
             */
            u1Version = (UINT1) ((*((UINT1 *) pu1Pkt) & IP_HEADER_VER_MASK) >>
                                 IP_HDR_VER_OFFSET_BITS);

            if (u1Version == IP_VERSION_4)
            {
                /* If version value is 4. This is IPV4 packet */
                bIsIpv4Pkt = TRUE;
            }

            if (u1Version == IPV6_HEADER_VERSION)
            {
                /* If version value is 6. This is IPV6 packet */
                bIsIpv6Pkt = TRUE;
            }
        }

        if (bIsIpv4Pkt == TRUE)
        {
            MplsOamRxHandleIpv4CtrlPacket (pu1Pkt, &u1IpProto, &u2UdpDstPort,
                                           &u4DstIpAddress);
        }
        else if (bIsIpv6Pkt == TRUE)
        {
            MplsOamRxHandleIpv6CtrlPacket (pu1Pkt, &u1IpProto, &u2UdpDstPort);
        }

        /* BFD UDP port value : 3784 */
        if ((u1IpProto == MPLS_UDP_PROT_ID) &&
            (u2UdpDstPort == MPLS_BFD_UDP_DST_PORT))
        {
            MPLSFM_DBG (MPLSFM_PRCS_PRCS, "BFD Packet with UDP\n");
            CRU_BUF_Move_ValidOffset (pBuf, CFA_ENET_V2_HEADER_SIZE);
            MplsEventNotif.pBuf = pBuf;
            MplsEventNotif.u4ContextId = MPLS_DEF_CONTEXT_ID;
            MplsEventNotif.u4InIfIndex = u4IfIndex;
            MplsEventNotif.u1PayloadOffset = (UINT1) ((pu1Pkt - pu1TmpBuf));
            MplsEventNotif.u1ServiceType = 0;
            MplsEventNotif.u2Event = MPLS_BFD_PACKET;
            MplsPortEventNotification (MPLS_APPLICATION_BFD, &MplsEventNotif);
        }
        else if ((u1IpProto == MPLS_UDP_PROT_ID) &&
                 (u2UdpDstPort == MPLS_LSP_PING_UDP_DST_PORT))
        {
            /* LSP Ping UDp port value : 3503 */
            MPLSFM_DBG (MPLSFM_PRCS_PRCS, "LSP Ping packet with UDP\n");
            CRU_BUF_Move_ValidOffset (pBuf, CFA_ENET_V2_HEADER_SIZE);
            MplsEventNotif.pBuf = pBuf;
            MplsEventNotif.u4ContextId = MPLS_DEF_CONTEXT_ID;
            MplsEventNotif.u4InIfIndex = u4IfIndex;
            MplsEventNotif.u1PayloadOffset = (UINT1) ((pu1Pkt - pu1TmpBuf));
            MplsEventNotif.u1ServiceType = 0;
            MplsEventNotif.u2Event = MPLS_LSPPING_PACKET;
            MplsPortEventNotification (MPLS_APPLICATION_LSP_PING,
                                       &MplsEventNotif);
        }
        else
        {
            /* Remove the break statement once we handle
             * all types of packets.
             */
            MPLSFM_DBG (MPLSFM_PRCS_PRCS, "Unknown Packet\n");
            break;
        }
        return MPLS_SUCCESS;
    }
    return MPLS_FAILURE;
}
