/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mplrtrsz.c,v 1.3 2013/11/29 11:04:14 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _MPLSRTRSZ_C
#include "mplsincs.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
MplsrtrSizingMemCreateMemPools ()
{
    INT4                i4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < MPLSRTR_MAX_SIZING_ID; i4SizingId++)
    {
        i4RetVal =
            MemCreateMemPool (FsMPLSRTRSizingParams[i4SizingId].u4StructSize,
                              FsMPLSRTRSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(MPLSRTRMemPoolIds[i4SizingId]));
        if (i4RetVal == (INT4) MEM_FAILURE)
        {
            MplsrtrSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
MplsrtrSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsMPLSRTRSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, MPLSRTRMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
MplsrtrSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < MPLSRTR_MAX_SIZING_ID; i4SizingId++)
    {
        if (MPLSRTRMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (MPLSRTRMemPoolIds[i4SizingId]);
            MPLSRTRMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
