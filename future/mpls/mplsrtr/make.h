#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# $Id: make.h,v 1.8 2014/02/27 13:50:40 siva Exp $
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                               |
# |                                                                          |
# |   MAKE TOOL(S) USED      : GNU make                                      |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 05/11/2001                                    |
# |                                                                          |
# |   DESCRIPTION            : Specifies the options and modules to be       |
# |                            including for building the Label Manager      |
# |                            product.                                      |
# |                                                                          |
# +--------------------------------------------------------------------------+


# Set the PROJ_BASE_DIR as the directory where you untar the project files
PROJECT_NAME            = MPLSRTR
PROJECT_BASE_DIR        = ${BASE_DIR}/mpls/mplsrtr
MPLS_INCL_DIR           = $(BASE_DIR)/mpls/mplsinc
PROJECT_SOURCE_DIR      = ${PROJECT_BASE_DIR}/src
PROJECT_INCLUDE_DIR     = ${PROJECT_BASE_DIR}/inc
PROJECT_OBJECT_DIR      = ${PROJECT_BASE_DIR}/obj
MPLS_CDM_DIR            = $(BASE_DIR)/mpls/mplsdb
MPLS_L2VPN_DIR          = $(BASE_DIR)/mpls/l2vpn/inc
MPLS_L3VPN_DIR          = $(BASE_DIR)/mpls/l3vpn/inc
MPLS_FM_INCL_DIR        = ${BASE_DIR}/mpls/mplsfm/inc
MPLS_LDP_INCL_DIR       = ${BASE_DIR}/mpls/ldp/inc
MPLS_TE_INCL_DIR        = ${BASE_DIR}/mpls/te/inc
MPLS_RSVPTE_INCL_DIR    = ${BASE_DIR}/mpls/rsvpte/inc
MPLS_OAM_INCL_DIR       = ${BASE_DIR}/mpls/oam/inc

# Specify the project include directories and dependencies
PROJECT_INCLUDE_FILES  = $(PROJECT_INCLUDE_DIR)/mplsrtr.h \
                         $(PROJECT_INCLUDE_DIR)/indexgbl.h \
                         $(MPLS_INCL_DIR)/mplsdefs.h

PROJECT_FINAL_INCLUDES_DIRS= -I$(PROJECT_INCLUDE_DIR) \
                             -I$(MPLS_INCL_DIR) \
                             -I$(MPLS_FM_INCL_DIR) \
                             -I$(MPLS_LDP_INCL_DIR) \
                             -I$(MPLS_TE_INCL_DIR) \
                             -I$(MPLS_RSVPTE_INCL_DIR) \
                             -I$(MPLS_CDM_DIR) \
                             -I$(MPLS_L2VPN_DIR) \
                             -I$(MPLS_L3VPN_DIR) \
                             -I$(MPLS_OAM_INCL_DIR) \
                               $(COMMON_INCLUDE_DIRS)

PROJECT_FINAL_INCLUDE_FILES= $(PROJECT_INCLUDE_FILES)

PROJECT_DEPENDENCIES= $(COMMON_DEPENDENCIES) \
                      $(PROJECT_FINAL_INCLUDE_FILES) \
                      $(MPLS_BASE_DIR)/make.h \
                      $(PROJECT_BASE_DIR)/Makefile \
                      $(PROJECT_BASE_DIR)/make.h
