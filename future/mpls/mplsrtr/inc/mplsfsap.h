
/********************************************************************
 *                                                                  *
 * $RCSfile: mplsfsap.h,v $
 *                                                                  *
 * $Date: 2007/02/01 14:57:08 $                                     
 *                                                                  *
 * $Revision: 1.3 $                                                 
 *                                                                  *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : mplsfsap.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : MPLS-FM
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains FSAP porting related macros 
 *                             Presently SRM IfMsg related macros are present.
 *---------------------------------------------------------------------------*/
#ifndef _MPLS_FSAP_H
#define _MPLS_FSAP_H


/* 
 * On Target, Information across interfaces is exchanged through procedural
 * interface, using tIfMsg structure.
 * The following are macros related to IfMsg supported by FSAP-II SRM libraries.
 */

#define MPLS_ALLOC_IFMSG         IfMsgAllocateIfMsg
#define MPLS_REL_IFMSG           IfMsgReleaseIfMsg
#define tMplsIfMsg               tIfMsg

/* Macros for accessing the fields of tIfMsg */
#define MPLS_IFMSG_TYPE(pIfMsg) \
                       (pIfMsg->u2MsgType)
#define MPLS_IFMSG_PRIMITIVE(pIfMsg)  \
                 (pIfMsg->u2Primitive)
#define MPLS_IFMSG_PROT_ID(pIfMsg)  \
                 (pIfMsg->u4ProtocolId)
#define MPLS_IFMSG_DIRECTION(pIfMsg) \
                 (pIfMsg->u2Direction)
#define MPLS_IFMSG_DATA_LEN(pIfMsg)  \
                 (pIfMsg->u4DataLen)
#define MPLS_IFMSG_PROT_INFO(pIfMsg)  \
                 (pIfMsg->pProtocolInfo)
#define MPLS_IFMSG_DATA(pIfMsg)  \
                 (pIfMsg->pData)
#define MPLS_IFMSG_IFID(pIfMsg)  \
                 (pIfMsg->IfId)
#define MPLS_IFMSG_REL_FUNC(pIfMsg)  \
                      (pIfMsg->ReleaseFunc)
#define MPLS_IFMSG_SRC_MAC_ADDR(pIfMsg) \
               (((tIpProtInfo*)(pIfMsg->pProtocolInfo))->Ethernet.au1SrcAddr)
#define MPLS_IFMSG_DEST_MAC_ADDR(pIfMsg) \
               (((tIpProtInfo*)(pIfMsg->pProtocolInfo))->Ethernet.au1DstAddr)
#define MPLS_IFMSG_MAC_PROT_TYPE(pIfMsg) \
               (((tIpProtInfo*)(pIfMsg->pProtocolInfo))->Ethernet.u2PktType)
/* End of Macros for accessing the fields of tIfMsg */

#define MPLS_IFMSG_SUCCESS      IFMSG_SUCCESS
#define MPLS_IFMSG_FAILURE      IFMSG_FAILURE

/* IfMsg Error Codes */
#define MPLS_IFMSG_ERR_INV_CFG IFMSG_ERR_INV_CFG    /*Invalid Input Config Vals */
#define MPLS_IFMSG_ERR_MEM_RSC IFMSG_ERR_MEM_RSC    /*Fail to alloc mem resource */
#define MPLS_IFMSG_ERR_NO_INIT IFMSG_ERR_NO_INIT    /* Alloc/Rel called before 
                                                       Init */

/* Values for u2Direction of tIfMsg */
#define MPLS_IFMSG_DIR_DOWN  IFMSG_DIR_DOWN    /* Source protocol is higher layer i
                                               to Destination protocol module */
#define MPLS_IFMSG_DIR_UP    IFMSG_DIR_UP    /* Source protocol is lower layer to
                                               Destination protocol module */

/* Values for u2MsgType of tIfMsg */
#define MPLS_IFMSG_TYPE_DATA    IFMSG_TYPE_DATA    /* u2MsgType = DATA     */
#define MPLS_IFMSG_TYPE_CTRL    IFMSG_TYPE_CTRL    /* u2MsgType = CONTROL  */


#endif /* _MPLS_FSAP_H */
