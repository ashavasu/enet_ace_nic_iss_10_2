
/********************************************************************
 *                                                                  *
 * $Id: mplsdbg.h,v 1.6 2015/03/10 10:35:28 siva Exp $
 *                                                                  *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : mplsdbg.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : MPLSFM
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Contains all the data structures and
 *    definitions required for Debugging in mplsfm.
 *---------------------------------------------------------------------------*/

#ifndef _MPLSFM_DBG_H
#define _MPLSFM_DBG_H
#include "utltrc.h"

/* Macros used in MPLSFM Debugging */
#define       MPLSFM_DBG_FLAG      gu4MplsDbg

#define     MPLSFM_DEF_DBG_FLAG    0x00000000
/* Values assigned to the sub-modules of MPLSFM, for Logging purpose */
#define    MPLSFM_MAIN_DEBUG           0x10000000
#define    MPLSFM_IF_DEBUG             0x20000000
#define    MPLSFM_PRCS_DEBUG           0x40000000
#define    MPLSFM_DIFF_DEBUG           0x02000000
#define    MPLSFM_PKT_DUMP             0x04000000

/* Log Types defined in MPLSFM */
#define     MPLSFM_DBG_MEM     0x00000001
#define     MPLSFM_DBG_TIMER   0x00000002
#define     MPLSFM_DBG_SEM     0x00000004
#define     MPLSFM_DBG_RX      0x00000008
#define     MPLSFM_DBG_PRCS    0x00000020
#define     MPLSFM_DBG_SNMP    0x00000040
#define     MPLSFM_DBG_FAILURE 0x00000080
#define     MPLSFM_DBG_MISC    0x00000200
#define     MPLSFM_DBG_ETEXT   0x00000400
#define     MPLSFM_DBG_IF      0x00000800
#define     MPLSFM_DBG_ALL     0x000000ff

 /* values used in MPLSFM_DBG depending on the categories under which 
  * the log message falls. 
  */

/* Main Module : */
#define     MPLSFM_MAIN_MEM     (MPLSFM_MAIN_DEBUG | MPLSFM_DBG_MEM)
#define     MPLSFM_MAIN_PRCS    (MPLSFM_MAIN_DEBUG | MPLSFM_DBG_PRCS)
#define     MPLSFM_MAIN_ETEXT   (MPLSFM_MAIN_DEBUG | MPLSFM_DBG_ETEXT)
#define     MPLSFM_MAIN_MISC    (MPLSFM_MAIN_DEBUG | MPLSFM_DBG_MISC)
#define     MPLSFM_MAIN_ALL     (MPLSFM_MAIN_DEBUG | MPLSFM_DBG_ALL)

/* Interface Module : */
#define     MPLSFM_IF_MEM      (MPLSFM_IF_DEBUG | MPLSFM_DBG_MEM)
#define     MPLSFM_IF_TIMER    (MPLSFM_IF_DEBUG | MPLSFM_DBG_TIMER)
#define     MPLSFM_IF_RX       (MPLSFM_IF_DEBUG | MPLSFM_DBG_RX)
#define     MPLSFM_IF_TX       (MPLSFM_IF_DEBUG | MPLSFM_DBG_TX)
#define     MPLSFM_IF_PRCS     (MPLSFM_IF_DEBUG | MPLSFM_DBG_PRCS)
#define     MPLSFM_IF_SNMP     (MPLSFM_IF_DEBUG | MPLSFM_DBG_SNMP)
#define     MPLSFM_IF_FAILURE  (MPLSFM_IF_DEBUG | MPLSFM_DBG_FAILURE)
#define     MPLSFM_IF_ETEXT    (MPLSFM_IF_DEBUG | MPLSFM_DBG_ETEXT)
#define     MPLSFM_IF_MISC     (MPLSFM_IF_DEBUG | MPLSFM_DBG_MISC)
#define     MPLSFM_IF_ALL      (MPLSFM_IF_DEBUG | MPLSFM_DBG_ALL)

#define     MPLSFM_IF_IF       (MPLSFM_IF_DEBUG | MPLSFM_DBG_IF)
#define     MPLSFM_IFIF_IF     (MPLSFM_IFIF_DEBUG | MPLSFM_DBG_IF)

/*  Processing  Module: */
#define     MPLSFM_PRCS_MEM    (MPLSFM_PRCS_DEBUG | MPLSFM_DBG_MEM)
#define     MPLSFM_PRCS_RX     (MPLSFM_PRCS_DEBUG | MPLSFM_DBG_RX)
#define     MPLSFM_PRCS_TX     (MPLSFM_PRCS_DEBUG | MPLSFM_DBG_TX)
#define     MPLSFM_PRCS_PRCS   (MPLSFM_PRCS_DEBUG | MPLSFM_DBG_PRCS)
#define     MPLSFM_PRCS_ETEXT  (MPLSFM_PRCS_DEBUG | MPLSFM_DBG_ETEXT)
#define     MPLFM_PRCS_MISC    (MPLSFM_PRCS_DEBUG | MPLSFM_DBG_MISC)
#define     MPLFM_PRCS_ALL     (MPLSFM_PRCS_DEBUG | MPLSFM_DBG_ALL)

#define     MPLSFM_DIFF_ETEXT   (MPLSFM_DIFF_DEBUG | MPLSFM_DBG_ETEXT)
#define     MPLSFM_DIFF_PRCS    (MPLSFM_DIFF_DEBUG | MPLSFM_DBG_PRCS)

#define MPLSFM_ERR(pu1Format)   \
        if(MPLSFM_ERR_DEBUG == (MPLSFM_ERR_DEBUG & MPLSFM_DBG_FLAG)) \
          UtlTrcLog   (MPLSFM_ERR_DEBUG, MPLSFM_ERR_DEBUG, "MPLSFM", pu1Format)

#define MPLSFM_ERR1(pu1Format, Arg1)       \
        if(MPLSFM_ERR_DEBUG == (MPLSFM_ERR_DEBUG & MPLSFM_DBG_FLAG)) \
             UtlTrcLog (MPLSFM_ERR_DEBUG, MPLSFM_ERR_DEBUG, "MPLSFM",pu1Format,Arg1)

#define MPLSFM_DBG(u4Value, pu1Format)     \
        if(u4Value == (u4Value & MPLSFM_DBG_FLAG)) \
                  UtlTrcLog  (MPLSFM_DBG_FLAG, u4Value, "MPLSFM", pu1Format)

#define MPLSFM_DBG1(u4Value, pu1Format, Arg1)     \
        if(u4Value == (u4Value & MPLSFM_DBG_FLAG)) \
             UtlTrcLog  (MPLSFM_DBG_FLAG, u4Value, "MPLSFM", pu1Format, Arg1)

#define MPLSFM_DBG2(u4Value, pu1Format, Arg1, Arg2)     \
        if(u4Value == (u4Value & MPLSFM_DBG_FLAG)) \
             UtlTrcLog  (MPLSFM_DBG_FLAG, u4Value, "MPLSFM", pu1Format, Arg1, Arg2)

#define MPLSFM_DBG3(u4Value, pu1Format, Arg1, Arg2, Arg3)     \
        if(u4Value == (u4Value & MPLSFM_DBG_FLAG)) \
             UtlTrcLog  (MPLSFM_DBG_FLAG, u4Value, "MPLSFM", pu1Format, Arg1, Arg2, Arg3)
#define MPLSFM_DBG4(u4Value, pu1Format, Arg1, Arg2, Arg3, Arg4)     \
        if(u4Value == (u4Value & MPLSFM_DBG_FLAG)) \
             UtlTrcLog  (MPLSFM_DBG_FLAG, u4Value, "MPLSFM", pu1Format, Arg1, Arg2, Arg3, Arg4)

#define MPLSFM_DBG5(u4Value, pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5)   \
        if(u4Value == (u4Value & MPLSFM_DBG_FLAG)) \
             UtlTrcLog  (MPLSFM_DBG_FLAG, u4Value, "MPLSFM", pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5)

#define MPLSFM_DBG6(u4Value, pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)     \
        if(u4Value == (u4Value & MPLSFM_DBG_FLAG)) \
             UtlTrcLog  (MPLSFM_DBG_FLAG, u4Value, "MPLSFM", pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)



#endif /* _MPLSFM_DBG_H */
