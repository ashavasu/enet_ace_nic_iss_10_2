/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: idxmgrsz.h,v 1.2 2010/10/19 06:58:28 prabuc Exp $
*
* Description: This file contains sizing related MACROS
*
*******************************************************************/
enum {
    MAX_INDEXMGR_INDEX_MGR_CHUNK_SIZING_ID,
    MAX_INDEXMGR_INDEX_MGR_GRP_SIZING_ID,
    MAX_INDEXMGR_INDEX_TBL_CHUNK_SIZING_ID,
    INDEXMGR_MAX_SIZING_ID
};


#ifdef  _INDEXMGRSZ_C
tMemPoolId INDEXMGRMemPoolIds[ INDEXMGR_MAX_SIZING_ID];
INT4  IndexmgrSizingMemCreateMemPools(VOID);
VOID  IndexmgrSizingMemDeleteMemPools(VOID);
INT4  IndexmgrSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _INDEXMGRSZ_C  */
extern tMemPoolId INDEXMGRMemPoolIds[ ];
extern INT4  IndexmgrSizingMemCreateMemPools(VOID);
extern VOID  IndexmgrSizingMemDeleteMemPools(VOID);
extern INT4  IndexmgrSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _INDEXMGRSZ_C  */


#ifdef  _INDEXMGRSZ_C
tFsModSizingParams FsINDEXMGRSizingParams [] = {
{ "tIndexMgrChunkInfoSize", "MAX_INDEXMGR_INDEX_MGR_CHUNK", sizeof(tIndexMgrChunkInfoSize),MAX_INDEXMGR_INDEX_MGR_CHUNK, MAX_INDEXMGR_INDEX_MGR_CHUNK,0 },
{ "tIndexMgrGrpInfoSize", "MAX_INDEXMGR_INDEX_MGR_GRP", sizeof(tIndexMgrGrpInfoSize),MAX_INDEXMGR_INDEX_MGR_GRP, MAX_INDEXMGR_INDEX_MGR_GRP,0 },
{ "tIndexMgrChunkSize", "MAX_INDEXMGR_INDEX_TBL_CHUNK", sizeof(tIndexMgrChunkSize),MAX_INDEXMGR_INDEX_TBL_CHUNK, MAX_INDEXMGR_INDEX_TBL_CHUNK,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _INDEXMGRSZ_C  */
extern tFsModSizingParams FsINDEXMGRSizingParams [];
#endif /*  _INDEXMGRSZ_C  */


