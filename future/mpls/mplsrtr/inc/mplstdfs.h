/********************************************************************
 * $Id: mplstdfs.h,v 1.14 2014/02/27 13:50:44 siva Exp $
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : mplstdfs.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : MPLS FM
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains structures used by MPLS-FM
 *----------------------------------------------------------------------------*/

#ifndef _MPLSTDFS_H
#define _MPLSTDFS_H

#define   tTrieKey                tKey
#define   tTrieCreateParams       tCreateParams
#define   tTrieInputParams        tInputParams
#define   tTrieOutputParams       tOutputParams
#define   tTrieScanOutParams      tScanOutParams
#define   tTrieDeleteOutParams    tDeleteOutParams



typedef struct _CfgVarGroup {
    UINT1 u1AdminStatus;
    UINT1 u1FecType;
    UINT2 u2Resvd;
    UINT4 u4VcTableEntries;
    UINT4 u4AtmInfoEntries;
    UINT4 u4EightBytePoolEntries;
    tMplsRouterId RouterId; /* Router identifier for MPLS module */
    UINT2 u2PscAchChnlType; /* PSC ACH channel code */
    UINT1 au1Pad [2]; /* Padding */
} tCfgVarGroup;
typedef struct _MplsGlobal {
   
   tTimerListId     TmrListId;
   tTmrDesc         aMplsTmrDesc[MPLS_MAX_TIMERS];
   tTmrBlk          FtnArpTmrBlk;
   tTmrBlk          IlmArpTmrBlk;
   tTmrBlk          TnlArpTmrBlk;
   tTmrBlk          PwArpTmrBlk;
   /* Graceful Restart related Timers */
   tTmrBlk          RpteMaxWaitTmrBlk;
   tTmrBlk          LdpMaxWaitTmrBlk;
#ifdef MPLS_L3VPN_WANTED
   tTmrBlk          L3VpnEgressMapArpTmrBlk;
#endif
#ifdef LANAI_WANTED
   tMplsSll         VcTableList;
#endif
   tDiffServGlobal  DiffServGlobal;
   tDsTeGlobal      DsTeGlobal;
   UINT1            au1TosVals[MPLS_DIFF_TOS_VALS]; 
   tCfgVarGroup     CfgVarGroup;
   tMplsRegParams  *apAppRegParams[MAX_MPLSRTR_MPLS_APPLICATIONS];
   UINT2           u2RpteMaxWaitTimer;
   UINT2           u2LdpMaxWaitTimer;
   UINT1           u1QosPolicy;
   UINT1           u1TTL;
   UINT1           u1iTTL;
   UINT1           u1OTTL;
   UINT1           u1PrviTTL;
   UINT1           u1PrvOTTL;
   UINT1           u1Resvd[2];
} tMplsGlobal;


typedef struct _QosPolicy {
UINT1 au1TosVals[MPLS_DIFF_TOS_VALS];
UINT1 u1TosValCount;
UINT1 u1Rsvd1;
UINT2 u2Rsvd2;
} tQosPolicy;

#endif /*_MPLSTDFS_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file mplstdfs.h                             */
/*---------------------------------------------------------------------------*/
