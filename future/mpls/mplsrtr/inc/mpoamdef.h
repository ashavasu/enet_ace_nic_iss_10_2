/********************************************************************
* $Id: mpoamdef.h,v 1.12 2017/11/09 13:22:14 siva Exp $
***************************************************************/
/*****************************************************************************/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *-----------------------------------------------------------------------------
 * FILE  NAME             : mpoamdef.h
 * PRINCIPAL AUTHOR       : Aricent Inc.
 * SUBSYSTEM NAME         : MPLS-FM
 * MODULE NAME            :
 * LANGUAGE               : ANSI-C
 * TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)
 * DATE OF FIRST RELEASE  :
 * DESCRIPTION            : This file contains OAM declarations
 *---------------------------------------------------------------------------*/
#ifndef _MPOAM_DEF_H
#define _MPOAM_DEF_H

/* Macro's for ACH, VCCV GAL, RAL and BFD */
#define MPLS_GAL_LABEL               13
#define MPLS_ACH_HEADER_LENGTH       4
#define MPLS_ACH_TLV_HEADER_LENGTH   4
#define MPLS_ACH_CHNL_TYPE_OFFSET    2
#define MPLS_ACH_HEADER              0x10
#define MPLS_ACH_TYPE_MASK           0xf0
#define MPLS_ACH_VERSION_MASK        0x0f

#define MPLS_ACH_CHANNEL_LSP_PING    0x0003
#define MPLS_ACH_CHANNEL_CC_BFD      gu2MplsAchChnlTypeCcBfd
#define MPLS_ACH_CHANNEL_CV_BFD      gu2MplsAchChnlTypeCvBfd
#define MPLS_ACH_CHANNEL_PSC         0x0009
#define MPLS_ACH_CHANNEL_CC_IPV4     gu2MplsAchChnlTypeCcIpv4
#define MPLS_ACH_CHANNEL_CV_IPV4     gu2MplsAchChnlTypeCvIpv4
#define MPLS_ACH_CHANNEL_CC_IPV6     gu2MplsAchChnlTypeCcIpv6
#define MPLS_ACH_CHANNEL_CV_IPV6     gu2MplsAchChnlTypeCvIpv6
#define MPLS_ACH_CHANNEL_Y1731       0x8902
#ifdef RFC6374_WANTED
#define MPLS_ACH_CHANNEL_TYPE_A_RFC6374   0x000A
#define MPLS_ACH_CHANNEL_TYPE_B_RFC6374   0x000B
#define MPLS_ACH_CHANNEL_TYPE_C_RFC6374   0x000C
#define MPLS_ACH_CHANNEL_TYPE_D_RFC6374   0x000D
#define MPLS_ACH_CHANNEL_TYPE_E_RFC6374   0x000E
#endif

/* UDP destination port for BFD and LSP ping */
#define MPLS_BFD_UDP_DST_PORT        0x0EC8   /* 3784 */
#define MPLS_LSP_PING_UDP_DST_PORT   0x0DAF   /* 3503 */

#define MPLS_BUFFER_LENGTH           80

#define IS_MPLS_IPV6_ADDR_LOOPBACK(a)     ( (((a).u4_addr[3] & 0x0000007f) == \
                                             0x0000007f) && \
                                           (a).u4_addr[2] == 0xffff0000 && \
                                           (a).u4_addr[1] == 0 && \
                                           (a).u4_addr[0] == 0 )

INT4
MplsOamRxHandleCtrlPkt ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT1 *pu1Pkt,
                                   UINT4 u4IfIndex, UINT4 u4PktSize));
VOID
MplsOamRxHandleIpv4CtrlPacket ARG_LIST ((UINT1 *pu1Pkt, UINT1 *pu1IpProto,
                                         UINT2 *pu2UdpDstPort,  UINT4 *pu4DstIpAddress));
VOID
MplsOamRxHandleIpv6CtrlPacket ARG_LIST ((UINT1 *pu1Pkt, UINT1 *pu1IpProto,
                                         UINT2 *pu2UdpDstPort));
#endif /*_MPOAM_DEF_H */

