
/********************************************************************
 *
 * $RCSfile: mplsrtr.h,v $
 *
 * $Date: 2007/02/01 14:57:08 $
 *
 * $Revision: 1.2 $
 *
 *******************************************************************/

/*---------------------------------------------------------------------------*/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : mplsrtr.h 
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : MPLS
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  : 20-JUN-2000
 *    DESCRIPTION            : This file contains all the associated include
 *                             files for the final mplsexe.
 *---------------------------------------------------------------------------*/

#ifndef _MPLSRTR_H
#define _MPLSRTR_H


#include "lr.h"
#include "mplssize.h"
#include "mpls.h"
#include "mplsdefs.h"
#include "mplsdiff.h"
#include "mplfmext.h"

#include "tedsdefs.h"
#include "mplsdsrm.h"

#include "mplsnpex.h"


#endif  /* _MPLSRTR_H  */

/*---------------------------------------------------------------------------*/
/*                        End of file mplsrtr.h                              */
/*---------------------------------------------------------------------------*/
