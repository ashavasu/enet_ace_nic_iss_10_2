
/********************************************************************
 *                                                                  *
 * $Id: mplsincs.h,v 1.13 2015/09/15 07:03:07 siva Exp $                                                                  *
 ********************************************************************/
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : mplsincs.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS-FM   
 *    MODULE NAME            : 
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains include functions 
 *                             used by MPLS-FM
 *---------------------------------------------------------------------------*/
#ifndef _MPLS_INCS_H
#define _MPLS_INCS_H


#include "lr.h"

#include "ip.h"
#include "cust.h"
#include "cfa.h"
#include "include.h"

#include "mpls.h"
#include "vcm.h"
#include "fm.h"
#include "elps.h"
#include "triecidr.h"
#include "trieapif.h"
#include "mplssize.h"

#include "indexmgr.h"
#include "lblmgrex.h"
#include "labelmgr.h"
#include "mplsred.h" 
#include "mplsdefs.h"
#include "mplsfsap.h"
#include "mplsdiff.h"
#include "mplfmext.h"
#include "mplstdfs.h"
#include "mplsport.h"
#include "mplsprot.h"
#include "diffprot.h"
#include "mplsextn.h"
#include "mplsmacs.h"
#include "mplsdbg.h"
#include "mplsglob.h"
#include "bfd.h"
#include "oamtdfsg.h"
#include "oamtdfs.h"
#include "mplrtrsz.h"
#include "idxmgrsz.h"
#include "lblmgrsz.h"
#include "tlm.h"
#ifdef LANAI_WANTED
#include "lanaisz.h"
#endif

/* MPLS NP include */
#include "mplsnpex.h"
#ifdef NPAPI_WANTED
#include "npapi.h"
#include "nputil.h"
#include "cfanp.h"
#endif
#endif /*_MPLS_INCS_H*/
