/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmplsdb.h,v 1.27 2016/10/19 11:25:20 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMPLSDB_H
#define _FSMPLSDB_H

UINT1 FsMplsLdpEntityTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMplsCrlspTnlTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMplsTunnelRSVPResTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMplsTunnelCRLDPResTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMplsDiffServElspMapTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMplsDiffServParamsTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMplsDiffServTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMplsDiffServElspInfoTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32 ,SNMP_DATA_TYPE_INTEGER32};
UINT1 FsMplsDsTeClassTypeTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMplsDsTeClassTypeToTcMapTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMplsDsTeTeClassTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMplsL2VpnPwTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMplsVplsConfigTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsPwMplsInboundTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMplsPortTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 FsMplsVplsAcMapTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMplsLdpPeerTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_OCTET_PRIM};
#ifdef MPLS_L3VPN_WANTED
UINT1 FsMplsL3VpnVrfEgressRteTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
#endif
UINT4 fsmpls [] ={1,3,6,1,4,1,2076,13,1};
tSNMP_OID_TYPE fsmplsOID = {9, fsmpls};


/* Generated OID's for tables */
UINT4 FsMplsLdpEntityTable [] ={1,3,6,1,4,1,2076,13,1,1,7};
tSNMP_OID_TYPE FsMplsLdpEntityTableOID = {11, FsMplsLdpEntityTable};


UINT4 FsMplsCrlspTnlTable [] ={1,3,6,1,4,1,2076,13,1,4,3};
tSNMP_OID_TYPE FsMplsCrlspTnlTableOID = {11, FsMplsCrlspTnlTable};


UINT4 FsMplsTunnelRSVPResTable [] ={1,3,6,1,4,1,2076,13,1,6,1};
tSNMP_OID_TYPE FsMplsTunnelRSVPResTableOID = {11, FsMplsTunnelRSVPResTable};


UINT4 FsMplsTunnelCRLDPResTable [] ={1,3,6,1,4,1,2076,13,1,6,2};
tSNMP_OID_TYPE FsMplsTunnelCRLDPResTableOID = {11, FsMplsTunnelCRLDPResTable};


UINT4 FsMplsDiffServElspMapTable [] ={1,3,6,1,4,1,2076,13,1,7,1};
tSNMP_OID_TYPE FsMplsDiffServElspMapTableOID = {11, FsMplsDiffServElspMapTable};


UINT4 FsMplsDiffServParamsTable [] ={1,3,6,1,4,1,2076,13,1,7,2};
tSNMP_OID_TYPE FsMplsDiffServParamsTableOID = {11, FsMplsDiffServParamsTable};


UINT4 FsMplsDiffServTable [] ={1,3,6,1,4,1,2076,13,1,7,3};
tSNMP_OID_TYPE FsMplsDiffServTableOID = {11, FsMplsDiffServTable};


UINT4 FsMplsDiffServElspInfoTable [] ={1,3,6,1,4,1,2076,13,1,7,4};
tSNMP_OID_TYPE FsMplsDiffServElspInfoTableOID = {11, FsMplsDiffServElspInfoTable};


UINT4 FsMplsDsTeClassTypeTable [] ={1,3,6,1,4,1,2076,13,1,8,2};
tSNMP_OID_TYPE FsMplsDsTeClassTypeTableOID = {11, FsMplsDsTeClassTypeTable};


UINT4 FsMplsDsTeClassTypeToTcMapTable [] ={1,3,6,1,4,1,2076,13,1,8,3};
tSNMP_OID_TYPE FsMplsDsTeClassTypeToTcMapTableOID = {11, FsMplsDsTeClassTypeToTcMapTable};


UINT4 FsMplsDsTeTeClassTable [] ={1,3,6,1,4,1,2076,13,1,8,4};
tSNMP_OID_TYPE FsMplsDsTeTeClassTableOID = {11, FsMplsDsTeTeClassTable};


UINT4 FsMplsL2VpnPwTable [] ={1,3,6,1,4,1,2076,13,1,5,1,4};
tSNMP_OID_TYPE FsMplsL2VpnPwTableOID = {12, FsMplsL2VpnPwTable};


UINT4 FsMplsVplsConfigTable [] ={1,3,6,1,4,1,2076,13,1,5,1,5};
tSNMP_OID_TYPE FsMplsVplsConfigTableOID = {12, FsMplsVplsConfigTable};


UINT4 FsPwMplsInboundTable [] ={1,3,6,1,4,1,2076,13,1,5,1,6};
tSNMP_OID_TYPE FsPwMplsInboundTableOID = {12, FsPwMplsInboundTable};


UINT4 FsMplsPortTable [] ={1,3,6,1,4,1,2076,13,1,5,1,11};
tSNMP_OID_TYPE FsMplsPortTableOID = {12, FsMplsPortTable};

UINT4 FsMplsVplsAcMapTable [] ={1,3,6,1,4,1,2076,13,1,5,1,12};
tSNMP_OID_TYPE FsMplsVplsAcMapTableOID = {12, FsMplsVplsAcMapTable};


UINT4 FsMplsLdpPeerTable [] ={1,3,6,1,4,1,2076,13,1,10,6};
tSNMP_OID_TYPE FsMplsLdpPeerTableOID = {11, FsMplsLdpPeerTable};


#ifdef MPLS_L3VPN_WANTED
UINT4 FsMplsL3VpnVrfEgressRteTable [] = {1,3,6,1,4,1,2076,13,1,12,1};
tSNMP_OID_TYPE FsMplsL3VpnVrfEgressRteTableOID = {11, FsMplsL3VpnVrfEgressRteTable};
#endif





UINT4 FsMplsAdminStatus [ ] ={1,3,6,1,4,1,2076,13,1,1,1};
UINT4 FsMplsQosPolicy [ ] ={1,3,6,1,4,1,2076,13,1,1,2};
UINT4 FsMplsFmDebugLevel [ ] ={1,3,6,1,4,1,2076,13,1,1,3};
UINT4 FsMplsTeDebugLevel [ ] ={1,3,6,1,4,1,2076,13,1,1,4};
UINT4 FsMplsLsrLabelAllocationMethod [ ] ={1,3,6,1,4,1,2076,13,1,1,5};
UINT4 FsMplsDiffServElspPreConfExpPhbMapIndex [ ] ={1,3,6,1,4,1,2076,13,1,1,6};
UINT4 FsMplsLdpEntityPHPRequestMethod [ ] ={1,3,6,1,4,1,2076,13,1,1,7,1,1};
UINT4 FsMplsLdpEntityTransAddrTlvEnable [ ] ={1,3,6,1,4,1,2076,13,1,1,7,1,2};
UINT4 FsMplsLdpEntityTransportAddress [ ] ={1,3,6,1,4,1,2076,13,1,1,7,1,3};
UINT4 FsMplsLdpEntityLdpOverRsvpEnable [ ] ={1,3,6,1,4,1,2076,13,1,1,7,1,4};
UINT4 FsMplsLdpEntityOutTunnelIndex [ ] ={1,3,6,1,4,1,2076,13,1,1,7,1,5};
UINT4 FsMplsLdpEntityOutTunnelInstance [ ] ={1,3,6,1,4,1,2076,13,1,1,7,1,6};
UINT4 FsMplsLdpEntityOutTunnelIngressLSRId [ ] ={1,3,6,1,4,1,2076,13,1,1,7,1,7};
UINT4 FsMplsLdpEntityOutTunnelEgressLSRId [ ] ={1,3,6,1,4,1,2076,13,1,1,7,1,8};
UINT4 FsMplsLdpEntityInTunnelIndex [ ] ={1,3,6,1,4,1,2076,13,1,1,7,1,9};
UINT4 FsMplsLdpEntityInTunnelInstance [ ] ={1,3,6,1,4,1,2076,13,1,1,7,1,10};
UINT4 FsMplsLdpEntityInTunnelIngressLSRId [ ] ={1,3,6,1,4,1,2076,13,1,1,7,1,11};
UINT4 FsMplsLdpEntityInTunnelEgressLSRId [ ] ={1,3,6,1,4,1,2076,13,1,1,7,1,12};
UINT4 FsMplsLdpEntityIpv6TransAddrTlvEnable [ ] ={1,3,6,1,4,1,2076,13,1,1,7,1,13};
UINT4 FsMplsLdpEntityIpv6TransportAddrKind [ ] ={1,3,6,1,4,1,2076,13,1,1,7,1,14};
UINT4 FsMplsLdpEntityIpv6TransportAddress [ ] ={1,3,6,1,4,1,2076,13,1,1,7,1,15};
UINT4 FsMplsLdpEntityBfdStatus [ ] ={1,3,6,1,4,1,2076,13,1,1,7,1,16};
UINT4 FsMplsLdpLsrId [ ] ={1,3,6,1,4,1,2076,13,1,1,8};
UINT4 FsMplsLdpForceOption [ ] ={1,3,6,1,4,1,2076,13,1,1,9};
UINT4 FsMplsRsvpTeGrMaxWaitTime [ ] ={1,3,6,1,4,1,2076,13,1,1,10};
UINT4 FsMplsLdpGrMaxWaitTime [ ] ={1,3,6,1,4,1,2076,13,1,1,11};
UINT4 FsMplsMaxIfTableEntries [ ] ={1,3,6,1,4,1,2076,13,1,2,1};
UINT4 FsMplsMaxFTNEntries [ ] ={1,3,6,1,4,1,2076,13,1,2,2};
UINT4 FsMplsMaxInSegmentEntries [ ] ={1,3,6,1,4,1,2076,13,1,2,3};
UINT4 FsMplsMaxOutSegmentEntries [ ] ={1,3,6,1,4,1,2076,13,1,2,4};
UINT4 FsMplsMaxXCEntries [ ] ={1,3,6,1,4,1,2076,13,1,2,5};
UINT4 FsMplsDiffServElspMapEntries [ ] ={1,3,6,1,4,1,2076,13,1,2,6};
UINT4 FsMplsDiffServParamsEntries [ ] ={1,3,6,1,4,1,2076,13,1,2,7};
UINT4 FsMplsMaxHopLists [ ] ={1,3,6,1,4,1,2076,13,1,2,8};
UINT4 FsMplsMaxPathOptPerList [ ] ={1,3,6,1,4,1,2076,13,1,2,9};
UINT4 FsMplsMaxHopsPerPathOption [ ] ={1,3,6,1,4,1,2076,13,1,2,10};
UINT4 FsMplsMaxArHopLists [ ] ={1,3,6,1,4,1,2076,13,1,2,11};
UINT4 FsMplsMaxHopsPerArHopList [ ] ={1,3,6,1,4,1,2076,13,1,2,12};
UINT4 FsMplsMaxRsvpTrfcParams [ ] ={1,3,6,1,4,1,2076,13,1,2,13};
UINT4 FsMplsMaxCrLdpTrfcParams [ ] ={1,3,6,1,4,1,2076,13,1,2,14};
UINT4 FsMplsMaxDServElsps [ ] ={1,3,6,1,4,1,2076,13,1,2,15};
UINT4 FsMplsMaxDServLlsps [ ] ={1,3,6,1,4,1,2076,13,1,2,16};
UINT4 FsMplsMaxTnls [ ] ={1,3,6,1,4,1,2076,13,1,2,17};
UINT4 FsMplsLsrMaxLdpEntities [ ] ={1,3,6,1,4,1,2076,13,1,2,18};
UINT4 FsMplsLsrMaxLocalPeers [ ] ={1,3,6,1,4,1,2076,13,1,2,19};
UINT4 FsMplsLsrMaxRemotePeers [ ] ={1,3,6,1,4,1,2076,13,1,2,20};
UINT4 FsMplsLsrMaxIfaces [ ] ={1,3,6,1,4,1,2076,13,1,2,21};
UINT4 FsMplsLsrMaxLsps [ ] ={1,3,6,1,4,1,2076,13,1,2,22};
UINT4 FsMplsLsrMaxVcMergeCount [ ] ={1,3,6,1,4,1,2076,13,1,2,23};
UINT4 FsMplsLsrMaxVpMergeCount [ ] ={1,3,6,1,4,1,2076,13,1,2,24};
UINT4 FsMplsLsrMaxCrlspTnls [ ] ={1,3,6,1,4,1,2076,13,1,2,25};
UINT4 FsMplsActiveRsvpTeTnls [ ] ={1,3,6,1,4,1,2076,13,1,3,1};
UINT4 FsMplsActiveLsps [ ] ={1,3,6,1,4,1,2076,13,1,3,2};
UINT4 FsMplsActiveCrLsps [ ] ={1,3,6,1,4,1,2076,13,1,3,3};
UINT4 FsMplsCrlspDebugLevel [ ] ={1,3,6,1,4,1,2076,13,1,4,1};
UINT4 FsMplsCrlspDumpType [ ] ={1,3,6,1,4,1,2076,13,1,4,2};
UINT4 FsMplsCrlspTnlIndex [ ] ={1,3,6,1,4,1,2076,13,1,4,3,1,1};
UINT4 FsMplsCrlspTnlRowStatus [ ] ={1,3,6,1,4,1,2076,13,1,4,3,1,2};
UINT4 FsMplsCrlspTnlStorageType [ ] ={1,3,6,1,4,1,2076,13,1,4,3,1,3};
UINT4 FsMplsCrlspDumpDirection [ ] ={1,3,6,1,4,1,2076,13,1,4,4};
UINT4 FsMplsCrlspPersistance [ ] ={1,3,6,1,4,1,2076,13,1,4,5};
UINT4 FsMplsTunnelRSVPResTokenBucketRate [ ] ={1,3,6,1,4,1,2076,13,1,6,1,1,1};
UINT4 FsMplsTunnelRSVPResTokenBucketSize [ ] ={1,3,6,1,4,1,2076,13,1,6,1,1,2};
UINT4 FsMplsTunnelRSVPResPeakDataRate [ ] ={1,3,6,1,4,1,2076,13,1,6,1,1,3};
UINT4 FsMplsTunnelRSVPResMinimumPolicedUnit [ ] ={1,3,6,1,4,1,2076,13,1,6,1,1,4};
UINT4 FsMplsTunnelRSVPResMaximumPacketSize [ ] ={1,3,6,1,4,1,2076,13,1,6,1,1,5};
UINT4 FsMplsTunnelCRLDPResPeakDataRate [ ] ={1,3,6,1,4,1,2076,13,1,6,2,1,1};
UINT4 FsMplsTunnelCRLDPResCommittedDataRate [ ] ={1,3,6,1,4,1,2076,13,1,6,2,1,2};
UINT4 FsMplsTunnelCRLDPResPeakBurstSize [ ] ={1,3,6,1,4,1,2076,13,1,6,2,1,3};
UINT4 FsMplsTunnelCRLDPResCommittedBurstSize [ ] ={1,3,6,1,4,1,2076,13,1,6,2,1,4};
UINT4 FsMplsTunnelCRLDPResExcessBurstSize [ ] ={1,3,6,1,4,1,2076,13,1,6,2,1,5};
UINT4 FsMplsDiffServElspMapIndex [ ] ={1,3,6,1,4,1,2076,13,1,7,1,1,1};
UINT4 FsMplsDiffServElspMapExpIndex [ ] ={1,3,6,1,4,1,2076,13,1,7,1,1,2};
UINT4 FsMplsDiffServElspMapPhbDscp [ ] ={1,3,6,1,4,1,2076,13,1,7,1,1,3};
UINT4 FsMplsDiffServElspMapStatus [ ] ={1,3,6,1,4,1,2076,13,1,7,1,1,4};
UINT4 FsMplsDiffServParamsIndex [ ] ={1,3,6,1,4,1,2076,13,1,7,2,1,1};
UINT4 FsMplsDiffServParamsServiceType [ ] ={1,3,6,1,4,1,2076,13,1,7,2,1,2};
UINT4 FsMplsDiffServParamsLlspPscDscp [ ] ={1,3,6,1,4,1,2076,13,1,7,2,1,3};
UINT4 FsMplsDiffServParamsElspType [ ] ={1,3,6,1,4,1,2076,13,1,7,2,1,4};
UINT4 FsMplsDiffServParamsElspSigExpPhbMapIndex [ ] ={1,3,6,1,4,1,2076,13,1,7,2,1,5};
UINT4 FsMplsDiffServParamsStatus [ ] ={1,3,6,1,4,1,2076,13,1,7,2,1,6};
UINT4 FsMplsDiffServClassType [ ] ={1,3,6,1,4,1,2076,13,1,7,3,1,1};
UINT4 FsMplsDiffServServiceType [ ] ={1,3,6,1,4,1,2076,13,1,7,3,1,2};
UINT4 FsMplsDiffServLlspPsc [ ] ={1,3,6,1,4,1,2076,13,1,7,3,1,3};
UINT4 FsMplsDiffServElspType [ ] ={1,3,6,1,4,1,2076,13,1,7,3,1,4};
UINT4 FsMplsDiffServElspListIndex [ ] ={1,3,6,1,4,1,2076,13,1,7,3,1,5};
UINT4 FsMplsDiffServRowStatus [ ] ={1,3,6,1,4,1,2076,13,1,7,3,1,6};
UINT4 FsMplsDiffServStorageType [ ] ={1,3,6,1,4,1,2076,13,1,7,3,1,7};
UINT4 FsMplsDiffServElspInfoListIndex [ ] ={1,3,6,1,4,1,2076,13,1,7,4,1,1};
UINT4 FsMplsDiffServElspInfoIndex [ ] ={1,3,6,1,4,1,2076,13,1,7,4,1,2};
UINT4 FsMplsDiffServElspInfoPHB [ ] ={1,3,6,1,4,1,2076,13,1,7,4,1,3};
UINT4 FsMplsDiffServElspInfoResourcePointer [ ] ={1,3,6,1,4,1,2076,13,1,7,4,1,4};
UINT4 FsMplsDiffServElspInfoRowStatus [ ] ={1,3,6,1,4,1,2076,13,1,7,4,1,5};
UINT4 FsMplsDiffServElspInfoStorageType [ ] ={1,3,6,1,4,1,2076,13,1,7,4,1,6};
UINT4 FsMplsDiffServElspInfoListIndexNext [ ] ={1,3,6,1,4,1,2076,13,1,7,5};
UINT4 FsMplsTnlModel [ ] ={1,3,6,1,4,1,2076,13,1,7,6};
UINT4 FsMplsRsrcMgmtType [ ] ={1,3,6,1,4,1,2076,13,1,7,7};
UINT4 FsMplsTTLVal [ ] ={1,3,6,1,4,1,2076,13,1,7,8};
UINT4 FsMplsDsTeStatus [ ] ={1,3,6,1,4,1,2076,13,1,8,1};
UINT4 FsMplsDsTeClassTypeIndex [ ] ={1,3,6,1,4,1,2076,13,1,8,2,1,1};
UINT4 FsMplsDsTeClassTypeDescription [ ] ={1,3,6,1,4,1,2076,13,1,8,2,1,2};
UINT4 FsMplsDsTeClassTypeRowStatus [ ] ={1,3,6,1,4,1,2076,13,1,8,2,1,3};
UINT4 FsMplsDsTeClassTypeBwPercentage [ ] ={1,3,6,1,4,1,2076,13,1,8,2,1,4};
UINT4 FsMplsDsTeTcIndex [ ] ={1,3,6,1,4,1,2076,13,1,8,3,1,1};
UINT4 FsMplsDsTeTcType [ ] ={1,3,6,1,4,1,2076,13,1,8,3,1,2};
UINT4 FsMplsDsTeTcDescription [ ] ={1,3,6,1,4,1,2076,13,1,8,3,1,3};
UINT4 FsMplsDsTeTcMapRowStatus [ ] ={1,3,6,1,4,1,2076,13,1,8,3,1,4};
UINT4 FsMplsDsTeTeClassPriority [ ] ={1,3,6,1,4,1,2076,13,1,8,4,1,1};
UINT4 FsMplsDsTeTeClassDesc [ ] ={1,3,6,1,4,1,2076,13,1,8,4,1,2};
UINT4 FsMplsDsTeTeClassNumber [ ] ={1,3,6,1,4,1,2076,13,1,8,4,1,3};
UINT4 FsMplsDsTeTeClassRowStatus [ ] ={1,3,6,1,4,1,2076,13,1,8,4,1,4};
UINT4 FsMplsL2VpnTrcFlag [ ] ={1,3,6,1,4,1,2076,13,1,5,1,1};
UINT4 FsMplsL2VpnCleanupInterval [ ] ={1,3,6,1,4,1,2076,13,1,5,1,2};
UINT4 FsMplsL2VpnAdminStatus [ ] ={1,3,6,1,4,1,2076,13,1,5,1,3};
UINT4 FsMplsL2VpnPwMode [ ] ={1,3,6,1,4,1,2076,13,1,5,1,4,1,1};
UINT4 FsMplsL2VpnVplsIndex [ ] ={1,3,6,1,4,1,2076,13,1,5,1,4,1,2};
UINT4 FsMplsL2VpnPwLocalCapabAdvert [ ] ={1,3,6,1,4,1,2076,13,1,5,1,4,1,3};
UINT4 FsMplsL2VpnPwLocalCCSelected [ ] ={1,3,6,1,4,1,2076,13,1,5,1,4,1,4};
UINT4 FsMplsL2VpnPwLocalCVSelected [ ] ={1,3,6,1,4,1,2076,13,1,5,1,4,1,5};
UINT4 FsMplsL2VpnPwLocalCCAdvert [ ] ={1,3,6,1,4,1,2076,13,1,5,1,4,1,6};
UINT4 FsMplsL2VpnPwLocalCVAdvert [ ] ={1,3,6,1,4,1,2076,13,1,5,1,4,1,7};
UINT4 FsMplsL2VpnPwRemoteCCAdvert [ ] ={1,3,6,1,4,1,2076,13,1,5,1,4,1,8};
UINT4 FsMplsL2VpnPwRemoteCVAdvert [ ] ={1,3,6,1,4,1,2076,13,1,5,1,4,1,9};
UINT4 FsMplsL2VpnPwOamEnable [ ] ={1,3,6,1,4,1,2076,13,1,5,1,4,1,10};
UINT4 FsMplsL2VpnPwGenAGIType [ ] ={1,3,6,1,4,1,2076,13,1,5,1,4,1,11};
UINT4 FsMplsL2VpnPwGenLocalAIIType [ ] ={1,3,6,1,4,1,2076,13,1,5,1,4,1,12};
UINT4 FsMplsL2VpnPwGenRemoteAIIType [ ] ={1,3,6,1,4,1,2076,13,1,5,1,4,1,13};
UINT4 FsMplsL2VpnProactiveOamSsnIndex [ ] ={1,3,6,1,4,1,2076,13,1,5,1,4,1,14};
UINT4 FsMplsL2VpnPwAIIFormat [ ] ={1,3,6,1,4,1,2076,13,1,5,1,4,1,15};
UINT4 FsMplsL2VpnIsStaticPw [ ] ={1,3,6,1,4,1,2076,13,1,5,1,4,1,16};
UINT4 FsMplsL2VpnNextFreePwEnetPwInstance [ ] ={1,3,6,1,4,1,2076,13,1,5,1,4,1,17};
UINT4 FsMplsL2VpnPwSynchronizationStatus [ ] ={1,3,6,1,4,1,2076,13,1,5,1,4,1,18};
UINT4 FsMplsVplsInstanceIndex [ ] ={1,3,6,1,4,1,2076,13,1,5,1,5,1,1};
UINT4 FsMplsVplsVsi [ ] ={1,3,6,1,4,1,2076,13,1,5,1,5,1,2};
UINT4 FsMplsVplsVpnId [ ] ={1,3,6,1,4,1,2076,13,1,5,1,5,1,3};
UINT4 FsMplsVplsName [ ] ={1,3,6,1,4,1,2076,13,1,5,1,5,1,4};
UINT4 FsMplsVplsDescr [ ] ={1,3,6,1,4,1,2076,13,1,5,1,5,1,5};
UINT4 FsMplsVplsFdbHighWatermark [ ] ={1,3,6,1,4,1,2076,13,1,5,1,5,1,6};
UINT4 FsMplsVplsFdbLowWatermark [ ] ={1,3,6,1,4,1,2076,13,1,5,1,5,1,7};
UINT4 FsMplsVplsRowStatus [ ] ={1,3,6,1,4,1,2076,13,1,5,1,5,1,8};
UINT4 FsMplsVplsL2MapFdbId [ ] ={1,3,6,1,4,1,2076,13,1,5,1,5,1,9};
UINT4 FsmplsVplsMtu [ ] ={1,3,6,1,4,1,2076,13,1,5,1,5,1,10};
UINT4 FsmplsVplsStorageType [ ] ={1,3,6,1,4,1,2076,13,1,5,1,5,1,11};
UINT4 FsmplsVplsSignalingType [ ] ={1,3,6,1,4,1,2076,13,1,5,1,5,1,12};
UINT4 FsmplsVplsControlWord [ ] ={1,3,6,1,4,1,2076,13,1,5,1,5,1,13};
UINT4 FsPwMplsInboundTunnelIndex [ ] ={1,3,6,1,4,1,2076,13,1,5,1,6,1,1};
UINT4 FsPwMplsInboundTunnelInstance [ ] ={1,3,6,1,4,1,2076,13,1,5,1,6,1,2};
UINT4 FsPwMplsInboundTunnelEgressLSR [ ] ={1,3,6,1,4,1,2076,13,1,5,1,6,1,3};
UINT4 FsPwMplsInboundTunnelIngressLSR [ ] ={1,3,6,1,4,1,2076,13,1,5,1,6,1,4};
UINT4 FsMplsLocalCCTypesCapabilities [ ] ={1,3,6,1,4,1,2076,13,1,5,1,7};
UINT4 FsMplsLocalCVTypesCapabilities [ ] ={1,3,6,1,4,1,2076,13,1,5,1,8};
UINT4 FsMplsRouterID [ ] ={1,3,6,1,4,1,2076,13,1,5,1,9};
UINT4 FsMplsHwCCTypeCapabilities [ ] ={1,3,6,1,4,1,2076,13,1,5,1,10};
UINT4 FsMplsPortBundleStatus [ ] ={1,3,6,1,4,1,2076,13,1,5,1,11,1,1};
UINT4 FsMplsPortMultiplexStatus [ ] ={1,3,6,1,4,1,2076,13,1,5,1,11,1,2};
UINT4 FsMplsPortAllToOneBundleStatus [ ] ={1,3,6,1,4,1,2076,13,1,5,1,11,1,3};
UINT4 FsMplsPortRowStatus [ ] ={1,3,6,1,4,1,2076,13,1,5,1,11,1,4};
UINT4 FsMplsVplsAcMapVplsIndex [ ] ={1,3,6,1,4,1,2076,13,1,5,1,12,1,1};
UINT4 FsMplsVplsAcMapAcIndex [ ] ={1,3,6,1,4,1,2076,13,1,5,1,12,1,2};
UINT4 FsMplsVplsAcMapPortIfIndex [ ] ={1,3,6,1,4,1,2076,13,1,5,1,12,1,3};
UINT4 FsMplsVplsAcMapPortVlan [ ] ={1,3,6,1,4,1,2076,13,1,5,1,12,1,4};
UINT4 FsMplsVplsAcMapRowStatus [ ] ={1,3,6,1,4,1,2076,13,1,5,1,12,1,5};
UINT4 FsMplsL2VpnMaxPwVcEntries [ ] ={1,3,6,1,4,1,2076,13,1,5,2,1};
UINT4 FsMplsL2VpnMaxPwVcMplsEntries [ ] ={1,3,6,1,4,1,2076,13,1,5,2,2};
UINT4 FsMplsL2VpnMaxPwVcMplsInOutEntries [ ] ={1,3,6,1,4,1,2076,13,1,5,2,3};
UINT4 FsMplsL2VpnMaxEthernetPwVcs [ ] ={1,3,6,1,4,1,2076,13,1,5,2,4};
UINT4 FsMplsL2VpnMaxPwVcEnetEntries [ ] ={1,3,6,1,4,1,2076,13,1,5,2,5};
UINT4 FsMplsL2VpnActivePwVcEntries [ ] ={1,3,6,1,4,1,2076,13,1,5,2,6};
UINT4 FsMplsL2VpnActivePwVcMplsEntries [ ] ={1,3,6,1,4,1,2076,13,1,5,2,7};
UINT4 FsMplsL2VpnActivePwVcEnetEntries [ ] ={1,3,6,1,4,1,2076,13,1,5,2,8};
UINT4 FsMplsL2VpnNoOfPwVcEntriesCreated [ ] ={1,3,6,1,4,1,2076,13,1,5,2,9};
UINT4 FsMplsL2VpnNoOfPwVcEntriesDeleted [ ] ={1,3,6,1,4,1,2076,13,1,5,2,10};
UINT4 FsMplsSimulateFailure [ ] ={1,3,6,1,4,1,2076,13,1,9,1};
UINT4 FsMplsiTTLVal [ ] ={1,3,6,1,4,1,2076,13,1,9,2};
UINT4 FsMplsOTTLVal [ ] ={1,3,6,1,4,1,2076,13,1,9,3};
UINT4 FsMplsprviTTLVal [ ] ={1,3,6,1,4,1,2076,13,1,9,4};
UINT4 FsMplsprvOTTLVal [ ] ={1,3,6,1,4,1,2076,13,1,9,5};
UINT4 FsMplsLdpGrCapability [ ] ={1,3,6,1,4,1,2076,13,1,10,1};
UINT4 FsMplsLdpGrForwardEntryHoldTime [ ] ={1,3,6,1,4,1,2076,13,1,10,2};
UINT4 FsMplsLdpGrMaxRecoveryTime [ ] ={1,3,6,1,4,1,2076,13,1,10,3};
UINT4 FsMplsLdpGrNeighborLivenessTime [ ] ={1,3,6,1,4,1,2076,13,1,10,4};
UINT4 FsMplsLdpGrProgressStatus [ ] ={1,3,6,1,4,1,2076,13,1,10,5};
UINT4 FsMplsLdpPeerGrReconnectTime [ ] ={1,3,6,1,4,1,2076,13,1,10,6,1,1};
UINT4 FsMplsLdpPeerGrRecoveryTime [ ] ={1,3,6,1,4,1,2076,13,1,10,6,1,2};
UINT4 FsMplsLdpPeerGrProgressStatus [ ] ={1,3,6,1,4,1,2076,13,1,10,6,1,3};
UINT4 FsMplsLdpConfigurationSequenceTLVEnable [ ] ={1,3,6,1,4,1,2076,13,1,11,1};
#ifdef MPLS_L3VPN_WANTED
UINT4 FsMplsL3VpnVrfName [ ] ={1,3,6,1,4,1,2076,13,1,12,1,1,1};
UINT4 FsMplsL3VpnVrfEgressRteInetCidrDestType [ ] ={1,3,6,1,4,1,2076,13,1,12,1,1,2};
UINT4 FsMplsL3VpnVrfEgressRteInetCidrDest [ ] ={1,3,6,1,4,1,2076,13,1,12,1,1,3};
UINT4 FsMplsL3VpnVrfEgressRteInetCidrPfxLen [ ] ={1,3,6,1,4,1,2076,13,1,12,1,1,4};
UINT4 FsMplsL3VpnVrfEgressRteInetCidrLabelPolicy [ ] ={1,3,6,1,4,1,2076,13,1,12,1,1,5};
UINT4 FsMplsL3VpnVrfEgressRteInetCidrNHopType [ ] ={1,3,6,1,4,1,2076,13,1,12,1,1,6};
UINT4 FsMplsL3VpnVrfEgressRteInetCidrNextHop [ ] ={1,3,6,1,4,1,2076,13,1,12,1,1,7};
UINT4 FsMplsL3VpnVrfEgressRteInetCidrIfIndex [ ] ={1,3,6,1,4,1,2076,13,1,12,1,1,8};
UINT4 FsMplsL3VpnVrfEgressRteInetCidrMetric1 [ ] ={1,3,6,1,4,1,2076,13,1,12,1,1,9};
UINT4 FsMplsL3VpnVrfEgressRtePathAttrLabel [ ] ={1,3,6,1,4,1,2076,13,1,12,1,1,10};
UINT4 FsMplsL3VpnVrfEgressRteInetCidrStatus [ ] ={1,3,6,1,4,1,2076,13,1,12,1,1,11};

#endif

tSNMP_OID_TYPE FsMplsAdminStatusOID = {11, FsMplsAdminStatus};


tSNMP_OID_TYPE FsMplsQosPolicyOID = {11, FsMplsQosPolicy};


tSNMP_OID_TYPE FsMplsFmDebugLevelOID = {11, FsMplsFmDebugLevel};


tSNMP_OID_TYPE FsMplsTeDebugLevelOID = {11, FsMplsTeDebugLevel};


tSNMP_OID_TYPE FsMplsLsrLabelAllocationMethodOID = {11, FsMplsLsrLabelAllocationMethod};


tSNMP_OID_TYPE FsMplsDiffServElspPreConfExpPhbMapIndexOID = {11, FsMplsDiffServElspPreConfExpPhbMapIndex};


tSNMP_OID_TYPE FsMplsLdpLsrIdOID = {11, FsMplsLdpLsrId};


tSNMP_OID_TYPE FsMplsLdpForceOptionOID = {11, FsMplsLdpForceOption};


tSNMP_OID_TYPE FsMplsRsvpTeGrMaxWaitTimeOID = {11, FsMplsRsvpTeGrMaxWaitTime};


tSNMP_OID_TYPE FsMplsLdpGrMaxWaitTimeOID = {11, FsMplsLdpGrMaxWaitTime};


tSNMP_OID_TYPE FsMplsMaxIfTableEntriesOID = {11, FsMplsMaxIfTableEntries};


tSNMP_OID_TYPE FsMplsMaxFTNEntriesOID = {11, FsMplsMaxFTNEntries};


tSNMP_OID_TYPE FsMplsMaxInSegmentEntriesOID = {11, FsMplsMaxInSegmentEntries};


tSNMP_OID_TYPE FsMplsMaxOutSegmentEntriesOID = {11, FsMplsMaxOutSegmentEntries};


tSNMP_OID_TYPE FsMplsMaxXCEntriesOID = {11, FsMplsMaxXCEntries};


tSNMP_OID_TYPE FsMplsDiffServElspMapEntriesOID = {11, FsMplsDiffServElspMapEntries};


tSNMP_OID_TYPE FsMplsDiffServParamsEntriesOID = {11, FsMplsDiffServParamsEntries};


tSNMP_OID_TYPE FsMplsMaxHopListsOID = {11, FsMplsMaxHopLists};


tSNMP_OID_TYPE FsMplsMaxPathOptPerListOID = {11, FsMplsMaxPathOptPerList};


tSNMP_OID_TYPE FsMplsMaxHopsPerPathOptionOID = {11, FsMplsMaxHopsPerPathOption};


tSNMP_OID_TYPE FsMplsMaxArHopListsOID = {11, FsMplsMaxArHopLists};


tSNMP_OID_TYPE FsMplsMaxHopsPerArHopListOID = {11, FsMplsMaxHopsPerArHopList};


tSNMP_OID_TYPE FsMplsMaxRsvpTrfcParamsOID = {11, FsMplsMaxRsvpTrfcParams};


tSNMP_OID_TYPE FsMplsMaxCrLdpTrfcParamsOID = {11, FsMplsMaxCrLdpTrfcParams};


tSNMP_OID_TYPE FsMplsMaxDServElspsOID = {11, FsMplsMaxDServElsps};


tSNMP_OID_TYPE FsMplsMaxDServLlspsOID = {11, FsMplsMaxDServLlsps};


tSNMP_OID_TYPE FsMplsMaxTnlsOID = {11, FsMplsMaxTnls};


tSNMP_OID_TYPE FsMplsLsrMaxLdpEntitiesOID = {11, FsMplsLsrMaxLdpEntities};


tSNMP_OID_TYPE FsMplsLsrMaxLocalPeersOID = {11, FsMplsLsrMaxLocalPeers};


tSNMP_OID_TYPE FsMplsLsrMaxRemotePeersOID = {11, FsMplsLsrMaxRemotePeers};


tSNMP_OID_TYPE FsMplsLsrMaxIfacesOID = {11, FsMplsLsrMaxIfaces};


tSNMP_OID_TYPE FsMplsLsrMaxLspsOID = {11, FsMplsLsrMaxLsps};


tSNMP_OID_TYPE FsMplsLsrMaxVcMergeCountOID = {11, FsMplsLsrMaxVcMergeCount};


tSNMP_OID_TYPE FsMplsLsrMaxVpMergeCountOID = {11, FsMplsLsrMaxVpMergeCount};


tSNMP_OID_TYPE FsMplsLsrMaxCrlspTnlsOID = {11, FsMplsLsrMaxCrlspTnls};


tSNMP_OID_TYPE FsMplsActiveRsvpTeTnlsOID = {11, FsMplsActiveRsvpTeTnls};


tSNMP_OID_TYPE FsMplsActiveLspsOID = {11, FsMplsActiveLsps};


tSNMP_OID_TYPE FsMplsActiveCrLspsOID = {11, FsMplsActiveCrLsps};


tSNMP_OID_TYPE FsMplsCrlspDebugLevelOID = {11, FsMplsCrlspDebugLevel};


tSNMP_OID_TYPE FsMplsCrlspDumpTypeOID = {11, FsMplsCrlspDumpType};


tSNMP_OID_TYPE FsMplsCrlspDumpDirectionOID = {11, FsMplsCrlspDumpDirection};


tSNMP_OID_TYPE FsMplsCrlspPersistanceOID = {11, FsMplsCrlspPersistance};


tSNMP_OID_TYPE FsMplsDiffServElspInfoListIndexNextOID = {11, FsMplsDiffServElspInfoListIndexNext};


tSNMP_OID_TYPE FsMplsTnlModelOID = {11, FsMplsTnlModel};


tSNMP_OID_TYPE FsMplsRsrcMgmtTypeOID = {11, FsMplsRsrcMgmtType};


tSNMP_OID_TYPE FsMplsTTLValOID = {11, FsMplsTTLVal};


tSNMP_OID_TYPE FsMplsDsTeStatusOID = {11, FsMplsDsTeStatus};


tSNMP_OID_TYPE FsMplsL2VpnTrcFlagOID = {12, FsMplsL2VpnTrcFlag};


tSNMP_OID_TYPE FsMplsL2VpnCleanupIntervalOID = {12, FsMplsL2VpnCleanupInterval};


tSNMP_OID_TYPE FsMplsL2VpnAdminStatusOID = {12, FsMplsL2VpnAdminStatus};


tSNMP_OID_TYPE FsMplsLocalCCTypesCapabilitiesOID = {12, FsMplsLocalCCTypesCapabilities};


tSNMP_OID_TYPE FsMplsLocalCVTypesCapabilitiesOID = {12, FsMplsLocalCVTypesCapabilities};


tSNMP_OID_TYPE FsMplsRouterIDOID = {12, FsMplsRouterID};


tSNMP_OID_TYPE FsMplsHwCCTypeCapabilitiesOID = {12, FsMplsHwCCTypeCapabilities};


tSNMP_OID_TYPE FsMplsL2VpnMaxPwVcEntriesOID = {12, FsMplsL2VpnMaxPwVcEntries};


tSNMP_OID_TYPE FsMplsL2VpnMaxPwVcMplsEntriesOID = {12, FsMplsL2VpnMaxPwVcMplsEntries};


tSNMP_OID_TYPE FsMplsL2VpnMaxPwVcMplsInOutEntriesOID = {12, FsMplsL2VpnMaxPwVcMplsInOutEntries};


tSNMP_OID_TYPE FsMplsL2VpnMaxEthernetPwVcsOID = {12, FsMplsL2VpnMaxEthernetPwVcs};


tSNMP_OID_TYPE FsMplsL2VpnMaxPwVcEnetEntriesOID = {12, FsMplsL2VpnMaxPwVcEnetEntries};


tSNMP_OID_TYPE FsMplsL2VpnActivePwVcEntriesOID = {12, FsMplsL2VpnActivePwVcEntries};


tSNMP_OID_TYPE FsMplsL2VpnActivePwVcMplsEntriesOID = {12, FsMplsL2VpnActivePwVcMplsEntries};


tSNMP_OID_TYPE FsMplsL2VpnActivePwVcEnetEntriesOID = {12, FsMplsL2VpnActivePwVcEnetEntries};


tSNMP_OID_TYPE FsMplsL2VpnNoOfPwVcEntriesCreatedOID = {12, FsMplsL2VpnNoOfPwVcEntriesCreated};


tSNMP_OID_TYPE FsMplsL2VpnNoOfPwVcEntriesDeletedOID = {12, FsMplsL2VpnNoOfPwVcEntriesDeleted};


tSNMP_OID_TYPE FsMplsSimulateFailureOID = {11, FsMplsSimulateFailure};


tSNMP_OID_TYPE FsMplsiTTLValOID = {11, FsMplsiTTLVal};


tSNMP_OID_TYPE FsMplsOTTLValOID = {11, FsMplsOTTLVal};


tSNMP_OID_TYPE FsMplsprviTTLValOID = {11, FsMplsprviTTLVal};


tSNMP_OID_TYPE FsMplsprvOTTLValOID = {11, FsMplsprvOTTLVal};


tSNMP_OID_TYPE FsMplsLdpGrCapabilityOID = {11, FsMplsLdpGrCapability};


tSNMP_OID_TYPE FsMplsLdpGrForwardEntryHoldTimeOID = {11, FsMplsLdpGrForwardEntryHoldTime};


tSNMP_OID_TYPE FsMplsLdpGrMaxRecoveryTimeOID = {11, FsMplsLdpGrMaxRecoveryTime};


tSNMP_OID_TYPE FsMplsLdpGrNeighborLivenessTimeOID = {11, FsMplsLdpGrNeighborLivenessTime};


tSNMP_OID_TYPE FsMplsLdpGrProgressStatusOID = {11, FsMplsLdpGrProgressStatus};


tSNMP_OID_TYPE FsMplsLdpConfigurationSequenceTLVEnableOID = {11, FsMplsLdpConfigurationSequenceTLVEnable};




tMbDbEntry FsMplsAdminStatusMibEntry[]= {

{{11,FsMplsAdminStatus}, NULL, FsMplsAdminStatusGet, FsMplsAdminStatusSet, FsMplsAdminStatusTest, FsMplsAdminStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},
};
tMibData FsMplsAdminStatusEntry = { 1, FsMplsAdminStatusMibEntry };

tMbDbEntry FsMplsQosPolicyMibEntry[]= {

{{11,FsMplsQosPolicy}, NULL, FsMplsQosPolicyGet, FsMplsQosPolicySet, FsMplsQosPolicyTest, FsMplsQosPolicyDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},
};
tMibData FsMplsQosPolicyEntry = { 1, FsMplsQosPolicyMibEntry };

tMbDbEntry FsMplsFmDebugLevelMibEntry[]= {

{{11,FsMplsFmDebugLevel}, NULL, FsMplsFmDebugLevelGet, FsMplsFmDebugLevelSet, FsMplsFmDebugLevelTest, FsMplsFmDebugLevelDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsFmDebugLevelEntry = { 1, FsMplsFmDebugLevelMibEntry };

tMbDbEntry FsMplsTeDebugLevelMibEntry[]= {

{{11,FsMplsTeDebugLevel}, NULL, FsMplsTeDebugLevelGet, FsMplsTeDebugLevelSet, FsMplsTeDebugLevelTest, FsMplsTeDebugLevelDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsTeDebugLevelEntry = { 1, FsMplsTeDebugLevelMibEntry };

tMbDbEntry FsMplsLsrLabelAllocationMethodMibEntry[]= {

{{11,FsMplsLsrLabelAllocationMethod}, NULL, FsMplsLsrLabelAllocationMethodGet, FsMplsLsrLabelAllocationMethodSet, FsMplsLsrLabelAllocationMethodTest, FsMplsLsrLabelAllocationMethodDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},
};
tMibData FsMplsLsrLabelAllocationMethodEntry = { 1, FsMplsLsrLabelAllocationMethodMibEntry };

tMbDbEntry FsMplsDiffServElspPreConfExpPhbMapIndexMibEntry[]= {

{{11,FsMplsDiffServElspPreConfExpPhbMapIndex}, NULL, FsMplsDiffServElspPreConfExpPhbMapIndexGet, FsMplsDiffServElspPreConfExpPhbMapIndexSet, FsMplsDiffServElspPreConfExpPhbMapIndexTest, FsMplsDiffServElspPreConfExpPhbMapIndexDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsDiffServElspPreConfExpPhbMapIndexEntry = { 1, FsMplsDiffServElspPreConfExpPhbMapIndexMibEntry };

tMbDbEntry FsMplsLdpEntityTableMibEntry[]= {

{{13,FsMplsLdpEntityPHPRequestMethod}, GetNextIndexFsMplsLdpEntityTable, FsMplsLdpEntityPHPRequestMethodGet, FsMplsLdpEntityPHPRequestMethodSet, FsMplsLdpEntityPHPRequestMethodTest, FsMplsLdpEntityTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsLdpEntityTableINDEX, 2, 0, 0, "1"},

{{13,FsMplsLdpEntityTransAddrTlvEnable}, GetNextIndexFsMplsLdpEntityTable, FsMplsLdpEntityTransAddrTlvEnableGet, FsMplsLdpEntityTransAddrTlvEnableSet, FsMplsLdpEntityTransAddrTlvEnableTest, FsMplsLdpEntityTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsLdpEntityTableINDEX, 2, 0, 0, "2"},

{{13,FsMplsLdpEntityTransportAddress}, GetNextIndexFsMplsLdpEntityTable, FsMplsLdpEntityTransportAddressGet, FsMplsLdpEntityTransportAddressSet, FsMplsLdpEntityTransportAddressTest, FsMplsLdpEntityTableDep, SNMP_DATA_TYPE_IP_ADDR_PRIM, SNMP_READWRITE, FsMplsLdpEntityTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsLdpEntityLdpOverRsvpEnable}, GetNextIndexFsMplsLdpEntityTable, FsMplsLdpEntityLdpOverRsvpEnableGet, FsMplsLdpEntityLdpOverRsvpEnableSet, FsMplsLdpEntityLdpOverRsvpEnableTest, FsMplsLdpEntityTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsLdpEntityTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsLdpEntityOutTunnelIndex}, GetNextIndexFsMplsLdpEntityTable, FsMplsLdpEntityOutTunnelIndexGet, FsMplsLdpEntityOutTunnelIndexSet, FsMplsLdpEntityOutTunnelIndexTest, FsMplsLdpEntityTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsLdpEntityTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsLdpEntityOutTunnelInstance}, GetNextIndexFsMplsLdpEntityTable, FsMplsLdpEntityOutTunnelInstanceGet, FsMplsLdpEntityOutTunnelInstanceSet, FsMplsLdpEntityOutTunnelInstanceTest, FsMplsLdpEntityTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsLdpEntityTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsLdpEntityOutTunnelIngressLSRId}, GetNextIndexFsMplsLdpEntityTable, FsMplsLdpEntityOutTunnelIngressLSRIdGet, FsMplsLdpEntityOutTunnelIngressLSRIdSet, FsMplsLdpEntityOutTunnelIngressLSRIdTest, FsMplsLdpEntityTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsLdpEntityTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsLdpEntityOutTunnelEgressLSRId}, GetNextIndexFsMplsLdpEntityTable, FsMplsLdpEntityOutTunnelEgressLSRIdGet, FsMplsLdpEntityOutTunnelEgressLSRIdSet, FsMplsLdpEntityOutTunnelEgressLSRIdTest, FsMplsLdpEntityTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsLdpEntityTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsLdpEntityInTunnelIndex}, GetNextIndexFsMplsLdpEntityTable, FsMplsLdpEntityInTunnelIndexGet, FsMplsLdpEntityInTunnelIndexSet, FsMplsLdpEntityInTunnelIndexTest, FsMplsLdpEntityTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsLdpEntityTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsLdpEntityInTunnelInstance}, GetNextIndexFsMplsLdpEntityTable, FsMplsLdpEntityInTunnelInstanceGet, FsMplsLdpEntityInTunnelInstanceSet, FsMplsLdpEntityInTunnelInstanceTest, FsMplsLdpEntityTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsLdpEntityTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsLdpEntityInTunnelIngressLSRId}, GetNextIndexFsMplsLdpEntityTable, FsMplsLdpEntityInTunnelIngressLSRIdGet, FsMplsLdpEntityInTunnelIngressLSRIdSet, FsMplsLdpEntityInTunnelIngressLSRIdTest, FsMplsLdpEntityTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsLdpEntityTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsLdpEntityInTunnelEgressLSRId}, GetNextIndexFsMplsLdpEntityTable, FsMplsLdpEntityInTunnelEgressLSRIdGet, FsMplsLdpEntityInTunnelEgressLSRIdSet, FsMplsLdpEntityInTunnelEgressLSRIdTest, FsMplsLdpEntityTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsLdpEntityTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsLdpEntityIpv6TransAddrTlvEnable}, GetNextIndexFsMplsLdpEntityTable, FsMplsLdpEntityIpv6TransAddrTlvEnableGet, FsMplsLdpEntityIpv6TransAddrTlvEnableSet, FsMplsLdpEntityIpv6TransAddrTlvEnableTest, FsMplsLdpEntityTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsLdpEntityTableINDEX, 2, 0, 0, "2"},

{{13,FsMplsLdpEntityIpv6TransportAddrKind}, GetNextIndexFsMplsLdpEntityTable, FsMplsLdpEntityIpv6TransportAddrKindGet, FsMplsLdpEntityIpv6TransportAddrKindSet, FsMplsLdpEntityIpv6TransportAddrKindTest, FsMplsLdpEntityTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsLdpEntityTableINDEX, 2, 0, 0, "2"},

{{13,FsMplsLdpEntityIpv6TransportAddress}, GetNextIndexFsMplsLdpEntityTable, FsMplsLdpEntityIpv6TransportAddressGet, FsMplsLdpEntityIpv6TransportAddressSet, FsMplsLdpEntityIpv6TransportAddressTest, FsMplsLdpEntityTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMplsLdpEntityTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsLdpEntityBfdStatus}, GetNextIndexFsMplsLdpEntityTable, FsMplsLdpEntityBfdStatusGet, FsMplsLdpEntityBfdStatusSet, FsMplsLdpEntityBfdStatusTest, FsMplsLdpEntityTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsLdpEntityTableINDEX, 2, 0, 0, NULL},
};

tMibData FsMplsLdpEntityTableEntry = { 16, FsMplsLdpEntityTableMibEntry };

tMbDbEntry FsMplsLdpLsrIdMibEntry[]= {

{{11,FsMplsLdpLsrId}, NULL, FsMplsLdpLsrIdGet, FsMplsLdpLsrIdSet, FsMplsLdpLsrIdTest, FsMplsLdpLsrIdDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsLdpLsrIdEntry = { 1, FsMplsLdpLsrIdMibEntry };

tMbDbEntry FsMplsLdpForceOptionMibEntry[]= {

{{11,FsMplsLdpForceOption}, NULL, FsMplsLdpForceOptionGet, FsMplsLdpForceOptionSet, FsMplsLdpForceOptionTest, FsMplsLdpForceOptionDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};
tMibData FsMplsLdpForceOptionEntry = { 1, FsMplsLdpForceOptionMibEntry };

tMbDbEntry FsMplsRsvpTeGrMaxWaitTimeMibEntry[]= {

{{11,FsMplsRsvpTeGrMaxWaitTime}, NULL, FsMplsRsvpTeGrMaxWaitTimeGet, FsMplsRsvpTeGrMaxWaitTimeSet, FsMplsRsvpTeGrMaxWaitTimeTest, FsMplsRsvpTeGrMaxWaitTimeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "600"},
};
tMibData FsMplsRsvpTeGrMaxWaitTimeEntry = { 1, FsMplsRsvpTeGrMaxWaitTimeMibEntry };

tMbDbEntry FsMplsLdpGrMaxWaitTimeMibEntry[]= {

{{11,FsMplsLdpGrMaxWaitTime}, NULL, FsMplsLdpGrMaxWaitTimeGet, FsMplsLdpGrMaxWaitTimeSet, FsMplsLdpGrMaxWaitTimeTest, FsMplsLdpGrMaxWaitTimeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "600"},
};
tMibData FsMplsLdpGrMaxWaitTimeEntry = { 1, FsMplsLdpGrMaxWaitTimeMibEntry };

tMbDbEntry FsMplsMaxIfTableEntriesMibEntry[]= {

{{11,FsMplsMaxIfTableEntries}, NULL, FsMplsMaxIfTableEntriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsMaxIfTableEntriesEntry = { 1, FsMplsMaxIfTableEntriesMibEntry };

tMbDbEntry FsMplsMaxFTNEntriesMibEntry[]= {

{{11,FsMplsMaxFTNEntries}, NULL, FsMplsMaxFTNEntriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsMaxFTNEntriesEntry = { 1, FsMplsMaxFTNEntriesMibEntry };

tMbDbEntry FsMplsMaxInSegmentEntriesMibEntry[]= {

{{11,FsMplsMaxInSegmentEntries}, NULL, FsMplsMaxInSegmentEntriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsMaxInSegmentEntriesEntry = { 1, FsMplsMaxInSegmentEntriesMibEntry };

tMbDbEntry FsMplsMaxOutSegmentEntriesMibEntry[]= {

{{11,FsMplsMaxOutSegmentEntries}, NULL, FsMplsMaxOutSegmentEntriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsMaxOutSegmentEntriesEntry = { 1, FsMplsMaxOutSegmentEntriesMibEntry };

tMbDbEntry FsMplsMaxXCEntriesMibEntry[]= {

{{11,FsMplsMaxXCEntries}, NULL, FsMplsMaxXCEntriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsMaxXCEntriesEntry = { 1, FsMplsMaxXCEntriesMibEntry };

tMbDbEntry FsMplsDiffServElspMapEntriesMibEntry[]= {

{{11,FsMplsDiffServElspMapEntries}, NULL, FsMplsDiffServElspMapEntriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsDiffServElspMapEntriesEntry = { 1, FsMplsDiffServElspMapEntriesMibEntry };

tMbDbEntry FsMplsDiffServParamsEntriesMibEntry[]= {

{{11,FsMplsDiffServParamsEntries}, NULL, FsMplsDiffServParamsEntriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsDiffServParamsEntriesEntry = { 1, FsMplsDiffServParamsEntriesMibEntry };

tMbDbEntry FsMplsMaxHopListsMibEntry[]= {

{{11,FsMplsMaxHopLists}, NULL, FsMplsMaxHopListsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsMaxHopListsEntry = { 1, FsMplsMaxHopListsMibEntry };

tMbDbEntry FsMplsMaxPathOptPerListMibEntry[]= {

{{11,FsMplsMaxPathOptPerList}, NULL, FsMplsMaxPathOptPerListGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsMaxPathOptPerListEntry = { 1, FsMplsMaxPathOptPerListMibEntry };

tMbDbEntry FsMplsMaxHopsPerPathOptionMibEntry[]= {

{{11,FsMplsMaxHopsPerPathOption}, NULL, FsMplsMaxHopsPerPathOptionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsMaxHopsPerPathOptionEntry = { 1, FsMplsMaxHopsPerPathOptionMibEntry };

tMbDbEntry FsMplsMaxArHopListsMibEntry[]= {

{{11,FsMplsMaxArHopLists}, NULL, FsMplsMaxArHopListsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsMaxArHopListsEntry = { 1, FsMplsMaxArHopListsMibEntry };

tMbDbEntry FsMplsMaxHopsPerArHopListMibEntry[]= {

{{11,FsMplsMaxHopsPerArHopList}, NULL, FsMplsMaxHopsPerArHopListGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsMaxHopsPerArHopListEntry = { 1, FsMplsMaxHopsPerArHopListMibEntry };

tMbDbEntry FsMplsMaxRsvpTrfcParamsMibEntry[]= {

{{11,FsMplsMaxRsvpTrfcParams}, NULL, FsMplsMaxRsvpTrfcParamsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsMaxRsvpTrfcParamsEntry = { 1, FsMplsMaxRsvpTrfcParamsMibEntry };

tMbDbEntry FsMplsMaxCrLdpTrfcParamsMibEntry[]= {

{{11,FsMplsMaxCrLdpTrfcParams}, NULL, FsMplsMaxCrLdpTrfcParamsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsMaxCrLdpTrfcParamsEntry = { 1, FsMplsMaxCrLdpTrfcParamsMibEntry };

tMbDbEntry FsMplsMaxDServElspsMibEntry[]= {

{{11,FsMplsMaxDServElsps}, NULL, FsMplsMaxDServElspsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsMaxDServElspsEntry = { 1, FsMplsMaxDServElspsMibEntry };

tMbDbEntry FsMplsMaxDServLlspsMibEntry[]= {

{{11,FsMplsMaxDServLlsps}, NULL, FsMplsMaxDServLlspsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsMaxDServLlspsEntry = { 1, FsMplsMaxDServLlspsMibEntry };

tMbDbEntry FsMplsMaxTnlsMibEntry[]= {

{{11,FsMplsMaxTnls}, NULL, FsMplsMaxTnlsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsMaxTnlsEntry = { 1, FsMplsMaxTnlsMibEntry };

tMbDbEntry FsMplsLsrMaxLdpEntitiesMibEntry[]= {

{{11,FsMplsLsrMaxLdpEntities}, NULL, FsMplsLsrMaxLdpEntitiesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsLsrMaxLdpEntitiesEntry = { 1, FsMplsLsrMaxLdpEntitiesMibEntry };

tMbDbEntry FsMplsLsrMaxLocalPeersMibEntry[]= {

{{11,FsMplsLsrMaxLocalPeers}, NULL, FsMplsLsrMaxLocalPeersGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsLsrMaxLocalPeersEntry = { 1, FsMplsLsrMaxLocalPeersMibEntry };

tMbDbEntry FsMplsLsrMaxRemotePeersMibEntry[]= {

{{11,FsMplsLsrMaxRemotePeers}, NULL, FsMplsLsrMaxRemotePeersGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsLsrMaxRemotePeersEntry = { 1, FsMplsLsrMaxRemotePeersMibEntry };

tMbDbEntry FsMplsLsrMaxIfacesMibEntry[]= {

{{11,FsMplsLsrMaxIfaces}, NULL, FsMplsLsrMaxIfacesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsLsrMaxIfacesEntry = { 1, FsMplsLsrMaxIfacesMibEntry };

tMbDbEntry FsMplsLsrMaxLspsMibEntry[]= {

{{11,FsMplsLsrMaxLsps}, NULL, FsMplsLsrMaxLspsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsLsrMaxLspsEntry = { 1, FsMplsLsrMaxLspsMibEntry };

tMbDbEntry FsMplsLsrMaxVcMergeCountMibEntry[]= {

{{11,FsMplsLsrMaxVcMergeCount}, NULL, FsMplsLsrMaxVcMergeCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsLsrMaxVcMergeCountEntry = { 1, FsMplsLsrMaxVcMergeCountMibEntry };

tMbDbEntry FsMplsLsrMaxVpMergeCountMibEntry[]= {

{{11,FsMplsLsrMaxVpMergeCount}, NULL, FsMplsLsrMaxVpMergeCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsLsrMaxVpMergeCountEntry = { 1, FsMplsLsrMaxVpMergeCountMibEntry };

tMbDbEntry FsMplsLsrMaxCrlspTnlsMibEntry[]= {

{{11,FsMplsLsrMaxCrlspTnls}, NULL, FsMplsLsrMaxCrlspTnlsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsLsrMaxCrlspTnlsEntry = { 1, FsMplsLsrMaxCrlspTnlsMibEntry };

tMbDbEntry FsMplsActiveRsvpTeTnlsMibEntry[]= {

{{11,FsMplsActiveRsvpTeTnls}, NULL, FsMplsActiveRsvpTeTnlsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsActiveRsvpTeTnlsEntry = { 1, FsMplsActiveRsvpTeTnlsMibEntry };

tMbDbEntry FsMplsActiveLspsMibEntry[]= {

{{11,FsMplsActiveLsps}, NULL, FsMplsActiveLspsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsActiveLspsEntry = { 1, FsMplsActiveLspsMibEntry };

tMbDbEntry FsMplsActiveCrLspsMibEntry[]= {

{{11,FsMplsActiveCrLsps}, NULL, FsMplsActiveCrLspsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsActiveCrLspsEntry = { 1, FsMplsActiveCrLspsMibEntry };

tMbDbEntry FsMplsCrlspDebugLevelMibEntry[]= {

{{11,FsMplsCrlspDebugLevel}, NULL, FsMplsCrlspDebugLevelGet, FsMplsCrlspDebugLevelSet, FsMplsCrlspDebugLevelTest, FsMplsCrlspDebugLevelDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsCrlspDebugLevelEntry = { 1, FsMplsCrlspDebugLevelMibEntry };

tMbDbEntry FsMplsCrlspDumpTypeMibEntry[]= {

{{11,FsMplsCrlspDumpType}, NULL, FsMplsCrlspDumpTypeGet, FsMplsCrlspDumpTypeSet, FsMplsCrlspDumpTypeTest, FsMplsCrlspDumpTypeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsCrlspDumpTypeEntry = { 1, FsMplsCrlspDumpTypeMibEntry };

tMbDbEntry FsMplsCrlspTnlTableMibEntry[]= {

{{13,FsMplsCrlspTnlIndex}, GetNextIndexFsMplsCrlspTnlTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMplsCrlspTnlTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsCrlspTnlRowStatus}, GetNextIndexFsMplsCrlspTnlTable, FsMplsCrlspTnlRowStatusGet, FsMplsCrlspTnlRowStatusSet, FsMplsCrlspTnlRowStatusTest, FsMplsCrlspTnlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsCrlspTnlTableINDEX, 1, 0, 1, NULL},

{{13,FsMplsCrlspTnlStorageType}, GetNextIndexFsMplsCrlspTnlTable, FsMplsCrlspTnlStorageTypeGet, FsMplsCrlspTnlStorageTypeSet, FsMplsCrlspTnlStorageTypeTest, FsMplsCrlspTnlTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsCrlspTnlTableINDEX, 1, 0, 0, NULL},
};
tMibData FsMplsCrlspTnlTableEntry = { 3, FsMplsCrlspTnlTableMibEntry };

tMbDbEntry FsMplsCrlspDumpDirectionMibEntry[]= {

{{11,FsMplsCrlspDumpDirection}, NULL, FsMplsCrlspDumpDirectionGet, FsMplsCrlspDumpDirectionSet, FsMplsCrlspDumpDirectionTest, FsMplsCrlspDumpDirectionDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "3"},
};
tMibData FsMplsCrlspDumpDirectionEntry = { 1, FsMplsCrlspDumpDirectionMibEntry };

tMbDbEntry FsMplsCrlspPersistanceMibEntry[]= {

{{11,FsMplsCrlspPersistance}, NULL, FsMplsCrlspPersistanceGet, FsMplsCrlspPersistanceSet, FsMplsCrlspPersistanceTest, FsMplsCrlspPersistanceDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},
};
tMibData FsMplsCrlspPersistanceEntry = { 1, FsMplsCrlspPersistanceMibEntry };

tMbDbEntry FsMplsL2VpnTrcFlagMibEntry[]= {

{{12,FsMplsL2VpnTrcFlag}, NULL, FsMplsL2VpnTrcFlagGet, FsMplsL2VpnTrcFlagSet, FsMplsL2VpnTrcFlagTest, FsMplsL2VpnTrcFlagDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},
};
tMibData FsMplsL2VpnTrcFlagEntry = { 1, FsMplsL2VpnTrcFlagMibEntry };

tMbDbEntry FsMplsL2VpnCleanupIntervalMibEntry[]= {

{{12,FsMplsL2VpnCleanupInterval}, NULL, FsMplsL2VpnCleanupIntervalGet, FsMplsL2VpnCleanupIntervalSet, FsMplsL2VpnCleanupIntervalTest, FsMplsL2VpnCleanupIntervalDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "60000"},
};
tMibData FsMplsL2VpnCleanupIntervalEntry = { 1, FsMplsL2VpnCleanupIntervalMibEntry };

tMbDbEntry FsMplsL2VpnAdminStatusMibEntry[]= {

{{12,FsMplsL2VpnAdminStatus}, NULL, FsMplsL2VpnAdminStatusGet, FsMplsL2VpnAdminStatusSet, FsMplsL2VpnAdminStatusTest, FsMplsL2VpnAdminStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},
};
tMibData FsMplsL2VpnAdminStatusEntry = { 1, FsMplsL2VpnAdminStatusMibEntry };

tMbDbEntry FsMplsL2VpnPwTableMibEntry[]= {

{{14,FsMplsL2VpnPwMode}, GetNextIndexFsMplsL2VpnPwTable, FsMplsL2VpnPwModeGet, FsMplsL2VpnPwModeSet, FsMplsL2VpnPwModeTest, FsMplsL2VpnPwTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsL2VpnPwTableINDEX, 1, 0, 0, "1"},

{{14,FsMplsL2VpnVplsIndex}, GetNextIndexFsMplsL2VpnPwTable, FsMplsL2VpnVplsIndexGet, FsMplsL2VpnVplsIndexSet, FsMplsL2VpnVplsIndexTest, FsMplsL2VpnPwTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsL2VpnPwTableINDEX, 1, 0, 0, NULL},

{{14,FsMplsL2VpnPwLocalCapabAdvert}, GetNextIndexFsMplsL2VpnPwTable, FsMplsL2VpnPwLocalCapabAdvertGet, FsMplsL2VpnPwLocalCapabAdvertSet, FsMplsL2VpnPwLocalCapabAdvertTest, FsMplsL2VpnPwTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMplsL2VpnPwTableINDEX, 1, 0, 0, NULL},

{{14,FsMplsL2VpnPwLocalCCSelected}, GetNextIndexFsMplsL2VpnPwTable, FsMplsL2VpnPwLocalCCSelectedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMplsL2VpnPwTableINDEX, 1, 0, 0, NULL},

{{14,FsMplsL2VpnPwLocalCVSelected}, GetNextIndexFsMplsL2VpnPwTable, FsMplsL2VpnPwLocalCVSelectedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMplsL2VpnPwTableINDEX, 1, 0, 0, NULL},

{{14,FsMplsL2VpnPwLocalCCAdvert}, GetNextIndexFsMplsL2VpnPwTable, FsMplsL2VpnPwLocalCCAdvertGet, FsMplsL2VpnPwLocalCCAdvertSet, FsMplsL2VpnPwLocalCCAdvertTest, FsMplsL2VpnPwTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMplsL2VpnPwTableINDEX, 1, 0, 0, NULL},

{{14,FsMplsL2VpnPwLocalCVAdvert}, GetNextIndexFsMplsL2VpnPwTable, FsMplsL2VpnPwLocalCVAdvertGet, FsMplsL2VpnPwLocalCVAdvertSet, FsMplsL2VpnPwLocalCVAdvertTest, FsMplsL2VpnPwTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMplsL2VpnPwTableINDEX, 1, 0, 0, NULL},

{{14,FsMplsL2VpnPwRemoteCCAdvert}, GetNextIndexFsMplsL2VpnPwTable, FsMplsL2VpnPwRemoteCCAdvertGet, FsMplsL2VpnPwRemoteCCAdvertSet, FsMplsL2VpnPwRemoteCCAdvertTest, FsMplsL2VpnPwTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMplsL2VpnPwTableINDEX, 1, 0, 0, NULL},

{{14,FsMplsL2VpnPwRemoteCVAdvert}, GetNextIndexFsMplsL2VpnPwTable, FsMplsL2VpnPwRemoteCVAdvertGet, FsMplsL2VpnPwRemoteCVAdvertSet, FsMplsL2VpnPwRemoteCVAdvertTest, FsMplsL2VpnPwTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMplsL2VpnPwTableINDEX, 1, 0, 0, NULL},

{{14,FsMplsL2VpnPwOamEnable}, GetNextIndexFsMplsL2VpnPwTable, FsMplsL2VpnPwOamEnableGet, FsMplsL2VpnPwOamEnableSet, FsMplsL2VpnPwOamEnableTest, FsMplsL2VpnPwTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsL2VpnPwTableINDEX, 1, 1, 0, "1"},

{{14,FsMplsL2VpnPwGenAGIType}, GetNextIndexFsMplsL2VpnPwTable, FsMplsL2VpnPwGenAGITypeGet, FsMplsL2VpnPwGenAGITypeSet, FsMplsL2VpnPwGenAGITypeTest, FsMplsL2VpnPwTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsL2VpnPwTableINDEX, 1, 0, 0, "0"},

{{14,FsMplsL2VpnPwGenLocalAIIType}, GetNextIndexFsMplsL2VpnPwTable, FsMplsL2VpnPwGenLocalAIITypeGet, FsMplsL2VpnPwGenLocalAIITypeSet, FsMplsL2VpnPwGenLocalAIITypeTest, FsMplsL2VpnPwTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsL2VpnPwTableINDEX, 1, 0, 0, "0"},

{{14,FsMplsL2VpnPwGenRemoteAIIType}, GetNextIndexFsMplsL2VpnPwTable, FsMplsL2VpnPwGenRemoteAIITypeGet, FsMplsL2VpnPwGenRemoteAIITypeSet, FsMplsL2VpnPwGenRemoteAIITypeTest, FsMplsL2VpnPwTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsL2VpnPwTableINDEX, 1, 0, 0, "0"},

{{14,FsMplsL2VpnProactiveOamSsnIndex}, GetNextIndexFsMplsL2VpnPwTable, FsMplsL2VpnProactiveOamSsnIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMplsL2VpnPwTableINDEX, 1, 0, 0, "0"},

{{14,FsMplsL2VpnPwAIIFormat}, GetNextIndexFsMplsL2VpnPwTable, FsMplsL2VpnPwAIIFormatGet, FsMplsL2VpnPwAIIFormatSet, FsMplsL2VpnPwAIIFormatTest, FsMplsL2VpnPwTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMplsL2VpnPwTableINDEX, 1, 0, 0, "0"},

{{14,FsMplsL2VpnIsStaticPw}, GetNextIndexFsMplsL2VpnPwTable, FsMplsL2VpnIsStaticPwGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMplsL2VpnPwTableINDEX, 1, 0, 0, NULL},

{{14,FsMplsL2VpnNextFreePwEnetPwInstance}, GetNextIndexFsMplsL2VpnPwTable, FsMplsL2VpnNextFreePwEnetPwInstanceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMplsL2VpnPwTableINDEX, 1, 0, 0, NULL},

{{14,FsMplsL2VpnPwSynchronizationStatus}, GetNextIndexFsMplsL2VpnPwTable, FsMplsL2VpnPwSynchronizationStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMplsL2VpnPwTableINDEX, 1, 0, 0, NULL},
};
tMibData FsMplsL2VpnPwTableEntry = { 18, FsMplsL2VpnPwTableMibEntry };

tMbDbEntry FsMplsVplsConfigTableMibEntry[]= {

{{14,FsMplsVplsInstanceIndex}, GetNextIndexFsMplsVplsConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMplsVplsConfigTableINDEX, 1, 0, 0, NULL},

{{14,FsMplsVplsVsi}, GetNextIndexFsMplsVplsConfigTable, FsMplsVplsVsiGet, FsMplsVplsVsiSet, FsMplsVplsVsiTest, FsMplsVplsConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMplsVplsConfigTableINDEX, 1, 0, 0, NULL},

{{14,FsMplsVplsVpnId}, GetNextIndexFsMplsVplsConfigTable, FsMplsVplsVpnIdGet, FsMplsVplsVpnIdSet, FsMplsVplsVpnIdTest, FsMplsVplsConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMplsVplsConfigTableINDEX, 1, 0, 0, NULL},

{{14,FsMplsVplsName}, GetNextIndexFsMplsVplsConfigTable, FsMplsVplsNameGet, FsMplsVplsNameSet, FsMplsVplsNameTest, FsMplsVplsConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMplsVplsConfigTableINDEX, 1, 0, 0, ""},

{{14,FsMplsVplsDescr}, GetNextIndexFsMplsVplsConfigTable, FsMplsVplsDescrGet, FsMplsVplsDescrSet, FsMplsVplsDescrTest, FsMplsVplsConfigTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMplsVplsConfigTableINDEX, 1, 0, 0, ""},

{{14,FsMplsVplsFdbHighWatermark}, GetNextIndexFsMplsVplsConfigTable, FsMplsVplsFdbHighWatermarkGet, FsMplsVplsFdbHighWatermarkSet, FsMplsVplsFdbHighWatermarkTest, FsMplsVplsConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsVplsConfigTableINDEX, 1, 0, 0, "95"},

{{14,FsMplsVplsFdbLowWatermark}, GetNextIndexFsMplsVplsConfigTable, FsMplsVplsFdbLowWatermarkGet, FsMplsVplsFdbLowWatermarkSet, FsMplsVplsFdbLowWatermarkTest, FsMplsVplsConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsVplsConfigTableINDEX, 1, 0, 0, "90"},

{{14,FsMplsVplsRowStatus}, GetNextIndexFsMplsVplsConfigTable, FsMplsVplsRowStatusGet, FsMplsVplsRowStatusSet, FsMplsVplsRowStatusTest, FsMplsVplsConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsVplsConfigTableINDEX, 1, 0, 1, NULL},

{{14,FsMplsVplsL2MapFdbId}, GetNextIndexFsMplsVplsConfigTable, FsMplsVplsL2MapFdbIdGet, FsMplsVplsL2MapFdbIdSet, FsMplsVplsL2MapFdbIdTest, FsMplsVplsConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMplsVplsConfigTableINDEX, 1, 0, 0, "4096"},


{{14,FsmplsVplsMtu}, GetNextIndexFsMplsVplsConfigTable, FsmplsVplsMtuGet, FsmplsVplsMtuSet, FsmplsVplsMtuTest, FsMplsVplsConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsVplsConfigTableINDEX, 1, 0, 0, "1518"},

{{14,FsmplsVplsStorageType}, GetNextIndexFsMplsVplsConfigTable, FsmplsVplsStorageTypeGet, FsmplsVplsStorageTypeSet, FsmplsVplsStorageTypeTest, FsMplsVplsConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsVplsConfigTableINDEX, 1, 0, 0, "2"},

{{14,FsmplsVplsSignalingType}, GetNextIndexFsMplsVplsConfigTable, FsmplsVplsSignalingTypeGet, FsmplsVplsSignalingTypeSet, FsmplsVplsSignalingTypeTest, FsMplsVplsConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsVplsConfigTableINDEX, 1, 0, 0, "3"},

{{14,FsmplsVplsControlWord}, GetNextIndexFsMplsVplsConfigTable, FsmplsVplsControlWordGet, FsmplsVplsControlWordSet, FsmplsVplsControlWordTest, FsMplsVplsConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsVplsConfigTableINDEX, 1, 0, 0, "2"},
};
tMibData FsMplsVplsConfigTableEntry = { 13, FsMplsVplsConfigTableMibEntry };

tMbDbEntry FsPwMplsInboundTableMibEntry[]= {

{{14,FsPwMplsInboundTunnelIndex}, GetNextIndexFsPwMplsInboundTable, FsPwMplsInboundTunnelIndexGet, FsPwMplsInboundTunnelIndexSet, FsPwMplsInboundTunnelIndexTest, FsPwMplsInboundTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsPwMplsInboundTableINDEX, 1, 0, 0, NULL},

{{14,FsPwMplsInboundTunnelInstance}, GetNextIndexFsPwMplsInboundTable, FsPwMplsInboundTunnelInstanceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsPwMplsInboundTableINDEX, 1, 0, 0, NULL},

{{14,FsPwMplsInboundTunnelEgressLSR}, GetNextIndexFsPwMplsInboundTable, FsPwMplsInboundTunnelEgressLSRGet, FsPwMplsInboundTunnelEgressLSRSet, FsPwMplsInboundTunnelEgressLSRTest, FsPwMplsInboundTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsPwMplsInboundTableINDEX, 1, 0, 0, NULL},

{{14,FsPwMplsInboundTunnelIngressLSR}, GetNextIndexFsPwMplsInboundTable, FsPwMplsInboundTunnelIngressLSRGet, FsPwMplsInboundTunnelIngressLSRSet, FsPwMplsInboundTunnelIngressLSRTest, FsPwMplsInboundTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsPwMplsInboundTableINDEX, 1, 0, 0, NULL},
};
tMibData FsPwMplsInboundTableEntry = { 4, FsPwMplsInboundTableMibEntry };

tMbDbEntry FsMplsLocalCCTypesCapabilitiesMibEntry[]= {

{{12,FsMplsLocalCCTypesCapabilities}, NULL, FsMplsLocalCCTypesCapabilitiesGet, FsMplsLocalCCTypesCapabilitiesSet, FsMplsLocalCCTypesCapabilitiesTest, FsMplsLocalCCTypesCapabilitiesDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsLocalCCTypesCapabilitiesEntry = { 1, FsMplsLocalCCTypesCapabilitiesMibEntry };

tMbDbEntry FsMplsLocalCVTypesCapabilitiesMibEntry[]= {

{{12,FsMplsLocalCVTypesCapabilities}, NULL, FsMplsLocalCVTypesCapabilitiesGet, FsMplsLocalCVTypesCapabilitiesSet, FsMplsLocalCVTypesCapabilitiesTest, FsMplsLocalCVTypesCapabilitiesDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsLocalCVTypesCapabilitiesEntry = { 1, FsMplsLocalCVTypesCapabilitiesMibEntry };

tMbDbEntry FsMplsRouterIDMibEntry[]= {

{{12,FsMplsRouterID}, NULL, FsMplsRouterIDGet, FsMplsRouterIDSet, FsMplsRouterIDTest, FsMplsRouterIDDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsRouterIDEntry = { 1, FsMplsRouterIDMibEntry };

tMbDbEntry FsMplsHwCCTypeCapabilitiesMibEntry[]= {

{{12,FsMplsHwCCTypeCapabilities}, NULL, FsMplsHwCCTypeCapabilitiesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsHwCCTypeCapabilitiesEntry = { 1, FsMplsHwCCTypeCapabilitiesMibEntry };

tMbDbEntry FsMplsPortTableMibEntry[]= {

{{14,FsMplsPortBundleStatus}, GetNextIndexFsMplsPortTable, FsMplsPortBundleStatusGet, FsMplsPortBundleStatusSet, FsMplsPortBundleStatusTest, FsMplsPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsPortTableINDEX, 1, 0, 0, "1"},

{{14,FsMplsPortMultiplexStatus}, GetNextIndexFsMplsPortTable, FsMplsPortMultiplexStatusGet, FsMplsPortMultiplexStatusSet, FsMplsPortMultiplexStatusTest, FsMplsPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsPortTableINDEX, 1, 0, 0, "1"},

{{14,FsMplsPortAllToOneBundleStatus}, GetNextIndexFsMplsPortTable, FsMplsPortAllToOneBundleStatusGet, FsMplsPortAllToOneBundleStatusSet, FsMplsPortAllToOneBundleStatusTest, FsMplsPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsPortTableINDEX, 1, 0, 0, "2"},

{{14,FsMplsPortRowStatus}, GetNextIndexFsMplsPortTable, FsMplsPortRowStatusGet, FsMplsPortRowStatusSet, FsMplsPortRowStatusTest, FsMplsPortTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsPortTableINDEX, 1, 0, 1, NULL},
};
tMibData FsMplsPortTableEntry = { 4, FsMplsPortTableMibEntry };

tMbDbEntry FsMplsVplsAcMapTableMibEntry[]= {

{{14,FsMplsVplsAcMapVplsIndex}, GetNextIndexFsMplsVplsAcMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMplsVplsAcMapTableINDEX, 2, 0, 0, NULL},

{{14,FsMplsVplsAcMapAcIndex}, GetNextIndexFsMplsVplsAcMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMplsVplsAcMapTableINDEX, 2, 0, 0, NULL},

{{14,FsMplsVplsAcMapPortIfIndex}, GetNextIndexFsMplsVplsAcMapTable, FsMplsVplsAcMapPortIfIndexGet, FsMplsVplsAcMapPortIfIndexSet, FsMplsVplsAcMapPortIfIndexTest, FsMplsVplsAcMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsVplsAcMapTableINDEX, 2, 0, 0, NULL},

{{14,FsMplsVplsAcMapPortVlan}, GetNextIndexFsMplsVplsAcMapTable, FsMplsVplsAcMapPortVlanGet, FsMplsVplsAcMapPortVlanSet, FsMplsVplsAcMapPortVlanTest, FsMplsVplsAcMapTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsVplsAcMapTableINDEX, 2, 0, 0, NULL},

{{14,FsMplsVplsAcMapRowStatus}, GetNextIndexFsMplsVplsAcMapTable, FsMplsVplsAcMapRowStatusGet, FsMplsVplsAcMapRowStatusSet, FsMplsVplsAcMapRowStatusTest, FsMplsVplsAcMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsVplsAcMapTableINDEX, 2, 0, 1, NULL},
};
tMibData FsMplsVplsAcMapTableEntry = { 5, FsMplsVplsAcMapTableMibEntry };

tMbDbEntry FsMplsL2VpnMaxPwVcEntriesMibEntry[]= {

{{12,FsMplsL2VpnMaxPwVcEntries}, NULL, FsMplsL2VpnMaxPwVcEntriesGet, FsMplsL2VpnMaxPwVcEntriesSet, FsMplsL2VpnMaxPwVcEntriesTest, FsMplsL2VpnMaxPwVcEntriesDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "256"},
};
tMibData FsMplsL2VpnMaxPwVcEntriesEntry = { 1, FsMplsL2VpnMaxPwVcEntriesMibEntry };

tMbDbEntry FsMplsL2VpnMaxPwVcMplsEntriesMibEntry[]= {

{{12,FsMplsL2VpnMaxPwVcMplsEntries}, NULL, FsMplsL2VpnMaxPwVcMplsEntriesGet, FsMplsL2VpnMaxPwVcMplsEntriesSet, FsMplsL2VpnMaxPwVcMplsEntriesTest, FsMplsL2VpnMaxPwVcMplsEntriesDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "16"},
};
tMibData FsMplsL2VpnMaxPwVcMplsEntriesEntry = { 1, FsMplsL2VpnMaxPwVcMplsEntriesMibEntry };

tMbDbEntry FsMplsL2VpnMaxPwVcMplsInOutEntriesMibEntry[]= {

{{12,FsMplsL2VpnMaxPwVcMplsInOutEntries}, NULL, FsMplsL2VpnMaxPwVcMplsInOutEntriesGet, FsMplsL2VpnMaxPwVcMplsInOutEntriesSet, FsMplsL2VpnMaxPwVcMplsInOutEntriesTest, FsMplsL2VpnMaxPwVcMplsInOutEntriesDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "256"},
};
tMibData FsMplsL2VpnMaxPwVcMplsInOutEntriesEntry = { 1, FsMplsL2VpnMaxPwVcMplsInOutEntriesMibEntry };

tMbDbEntry FsMplsL2VpnMaxEthernetPwVcsMibEntry[]= {

{{12,FsMplsL2VpnMaxEthernetPwVcs}, NULL, FsMplsL2VpnMaxEthernetPwVcsGet, FsMplsL2VpnMaxEthernetPwVcsSet, FsMplsL2VpnMaxEthernetPwVcsTest, FsMplsL2VpnMaxEthernetPwVcsDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "256"},
};
tMibData FsMplsL2VpnMaxEthernetPwVcsEntry = { 1, FsMplsL2VpnMaxEthernetPwVcsMibEntry };

tMbDbEntry FsMplsL2VpnMaxPwVcEnetEntriesMibEntry[]= {

{{12,FsMplsL2VpnMaxPwVcEnetEntries}, NULL, FsMplsL2VpnMaxPwVcEnetEntriesGet, FsMplsL2VpnMaxPwVcEnetEntriesSet, FsMplsL2VpnMaxPwVcEnetEntriesTest, FsMplsL2VpnMaxPwVcEnetEntriesDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "256"},
};
tMibData FsMplsL2VpnMaxPwVcEnetEntriesEntry = { 1, FsMplsL2VpnMaxPwVcEnetEntriesMibEntry };

tMbDbEntry FsMplsL2VpnActivePwVcEntriesMibEntry[]= {

{{12,FsMplsL2VpnActivePwVcEntries}, NULL, FsMplsL2VpnActivePwVcEntriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsL2VpnActivePwVcEntriesEntry = { 1, FsMplsL2VpnActivePwVcEntriesMibEntry };

tMbDbEntry FsMplsL2VpnActivePwVcMplsEntriesMibEntry[]= {

{{12,FsMplsL2VpnActivePwVcMplsEntries}, NULL, FsMplsL2VpnActivePwVcMplsEntriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsL2VpnActivePwVcMplsEntriesEntry = { 1, FsMplsL2VpnActivePwVcMplsEntriesMibEntry };

tMbDbEntry FsMplsL2VpnActivePwVcEnetEntriesMibEntry[]= {

{{12,FsMplsL2VpnActivePwVcEnetEntries}, NULL, FsMplsL2VpnActivePwVcEnetEntriesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsL2VpnActivePwVcEnetEntriesEntry = { 1, FsMplsL2VpnActivePwVcEnetEntriesMibEntry };

tMbDbEntry FsMplsL2VpnNoOfPwVcEntriesCreatedMibEntry[]= {

{{12,FsMplsL2VpnNoOfPwVcEntriesCreated}, NULL, FsMplsL2VpnNoOfPwVcEntriesCreatedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsL2VpnNoOfPwVcEntriesCreatedEntry = { 1, FsMplsL2VpnNoOfPwVcEntriesCreatedMibEntry };

tMbDbEntry FsMplsL2VpnNoOfPwVcEntriesDeletedMibEntry[]= {

{{12,FsMplsL2VpnNoOfPwVcEntriesDeleted}, NULL, FsMplsL2VpnNoOfPwVcEntriesDeletedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsL2VpnNoOfPwVcEntriesDeletedEntry = { 1, FsMplsL2VpnNoOfPwVcEntriesDeletedMibEntry };

tMbDbEntry FsMplsTunnelRSVPResTableMibEntry[]= {

{{13,FsMplsTunnelRSVPResTokenBucketRate}, GetNextIndexFsMplsTunnelRSVPResTable, FsMplsTunnelRSVPResTokenBucketRateGet, FsMplsTunnelRSVPResTokenBucketRateSet, FsMplsTunnelRSVPResTokenBucketRateTest, FsMplsTunnelRSVPResTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsTunnelRSVPResTableINDEX, 1, 0, 0, "0"},

{{13,FsMplsTunnelRSVPResTokenBucketSize}, GetNextIndexFsMplsTunnelRSVPResTable, FsMplsTunnelRSVPResTokenBucketSizeGet, FsMplsTunnelRSVPResTokenBucketSizeSet, FsMplsTunnelRSVPResTokenBucketSizeTest, FsMplsTunnelRSVPResTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsTunnelRSVPResTableINDEX, 1, 0, 0, "0"},

{{13,FsMplsTunnelRSVPResPeakDataRate}, GetNextIndexFsMplsTunnelRSVPResTable, FsMplsTunnelRSVPResPeakDataRateGet, FsMplsTunnelRSVPResPeakDataRateSet, FsMplsTunnelRSVPResPeakDataRateTest, FsMplsTunnelRSVPResTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsTunnelRSVPResTableINDEX, 1, 0, 0, "0"},

{{13,FsMplsTunnelRSVPResMinimumPolicedUnit}, GetNextIndexFsMplsTunnelRSVPResTable, FsMplsTunnelRSVPResMinimumPolicedUnitGet, FsMplsTunnelRSVPResMinimumPolicedUnitSet, FsMplsTunnelRSVPResMinimumPolicedUnitTest, FsMplsTunnelRSVPResTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsTunnelRSVPResTableINDEX, 1, 0, 0, "0"},

{{13,FsMplsTunnelRSVPResMaximumPacketSize}, GetNextIndexFsMplsTunnelRSVPResTable, FsMplsTunnelRSVPResMaximumPacketSizeGet, FsMplsTunnelRSVPResMaximumPacketSizeSet, FsMplsTunnelRSVPResMaximumPacketSizeTest, FsMplsTunnelRSVPResTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsTunnelRSVPResTableINDEX, 1, 0, 0, "1500"},
};
tMibData FsMplsTunnelRSVPResTableEntry = { 5, FsMplsTunnelRSVPResTableMibEntry };

tMbDbEntry FsMplsTunnelCRLDPResTableMibEntry[]= {

{{13,FsMplsTunnelCRLDPResPeakDataRate}, GetNextIndexFsMplsTunnelCRLDPResTable, FsMplsTunnelCRLDPResPeakDataRateGet, FsMplsTunnelCRLDPResPeakDataRateSet, FsMplsTunnelCRLDPResPeakDataRateTest, FsMplsTunnelCRLDPResTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsTunnelCRLDPResTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsTunnelCRLDPResCommittedDataRate}, GetNextIndexFsMplsTunnelCRLDPResTable, FsMplsTunnelCRLDPResCommittedDataRateGet, FsMplsTunnelCRLDPResCommittedDataRateSet, FsMplsTunnelCRLDPResCommittedDataRateTest, FsMplsTunnelCRLDPResTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsTunnelCRLDPResTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsTunnelCRLDPResPeakBurstSize}, GetNextIndexFsMplsTunnelCRLDPResTable, FsMplsTunnelCRLDPResPeakBurstSizeGet, FsMplsTunnelCRLDPResPeakBurstSizeSet, FsMplsTunnelCRLDPResPeakBurstSizeTest, FsMplsTunnelCRLDPResTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsTunnelCRLDPResTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsTunnelCRLDPResCommittedBurstSize}, GetNextIndexFsMplsTunnelCRLDPResTable, FsMplsTunnelCRLDPResCommittedBurstSizeGet, FsMplsTunnelCRLDPResCommittedBurstSizeSet, FsMplsTunnelCRLDPResCommittedBurstSizeTest, FsMplsTunnelCRLDPResTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsTunnelCRLDPResTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsTunnelCRLDPResExcessBurstSize}, GetNextIndexFsMplsTunnelCRLDPResTable, FsMplsTunnelCRLDPResExcessBurstSizeGet, FsMplsTunnelCRLDPResExcessBurstSizeSet, FsMplsTunnelCRLDPResExcessBurstSizeTest, FsMplsTunnelCRLDPResTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsTunnelCRLDPResTableINDEX, 1, 0, 0, NULL},
};
tMibData FsMplsTunnelCRLDPResTableEntry = { 5, FsMplsTunnelCRLDPResTableMibEntry };

tMbDbEntry FsMplsDiffServElspMapTableMibEntry[]= {

{{13,FsMplsDiffServElspMapIndex}, GetNextIndexFsMplsDiffServElspMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMplsDiffServElspMapTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsDiffServElspMapExpIndex}, GetNextIndexFsMplsDiffServElspMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMplsDiffServElspMapTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsDiffServElspMapPhbDscp}, GetNextIndexFsMplsDiffServElspMapTable, FsMplsDiffServElspMapPhbDscpGet, FsMplsDiffServElspMapPhbDscpSet, FsMplsDiffServElspMapPhbDscpTest, FsMplsDiffServElspMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsDiffServElspMapTableINDEX, 2, 0, 0, "0"},

{{13,FsMplsDiffServElspMapStatus}, GetNextIndexFsMplsDiffServElspMapTable, FsMplsDiffServElspMapStatusGet, FsMplsDiffServElspMapStatusSet, FsMplsDiffServElspMapStatusTest, FsMplsDiffServElspMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsDiffServElspMapTableINDEX, 2, 0, 1, NULL},
};
tMibData FsMplsDiffServElspMapTableEntry = { 4, FsMplsDiffServElspMapTableMibEntry };

tMbDbEntry FsMplsDiffServParamsTableMibEntry[]= {

{{13,FsMplsDiffServParamsIndex}, GetNextIndexFsMplsDiffServParamsTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMplsDiffServParamsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsDiffServParamsServiceType}, GetNextIndexFsMplsDiffServParamsTable, FsMplsDiffServParamsServiceTypeGet, FsMplsDiffServParamsServiceTypeSet, FsMplsDiffServParamsServiceTypeTest, FsMplsDiffServParamsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsDiffServParamsTableINDEX, 1, 0, 0, "0"},

{{13,FsMplsDiffServParamsLlspPscDscp}, GetNextIndexFsMplsDiffServParamsTable, FsMplsDiffServParamsLlspPscDscpGet, FsMplsDiffServParamsLlspPscDscpSet, FsMplsDiffServParamsLlspPscDscpTest, FsMplsDiffServParamsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsDiffServParamsTableINDEX, 1, 0, 0, "0"},

{{13,FsMplsDiffServParamsElspType}, GetNextIndexFsMplsDiffServParamsTable, FsMplsDiffServParamsElspTypeGet, FsMplsDiffServParamsElspTypeSet, FsMplsDiffServParamsElspTypeTest, FsMplsDiffServParamsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsDiffServParamsTableINDEX, 1, 0, 0, "0"},

{{13,FsMplsDiffServParamsElspSigExpPhbMapIndex}, GetNextIndexFsMplsDiffServParamsTable, FsMplsDiffServParamsElspSigExpPhbMapIndexGet, FsMplsDiffServParamsElspSigExpPhbMapIndexSet, FsMplsDiffServParamsElspSigExpPhbMapIndexTest, FsMplsDiffServParamsTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMplsDiffServParamsTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsDiffServParamsStatus}, GetNextIndexFsMplsDiffServParamsTable, FsMplsDiffServParamsStatusGet, FsMplsDiffServParamsStatusSet, FsMplsDiffServParamsStatusTest, FsMplsDiffServParamsTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsDiffServParamsTableINDEX, 1, 0, 1, NULL},
};
tMibData FsMplsDiffServParamsTableEntry = { 6, FsMplsDiffServParamsTableMibEntry };

tMbDbEntry FsMplsDiffServTableMibEntry[]= {

{{13,FsMplsDiffServClassType}, GetNextIndexFsMplsDiffServTable, FsMplsDiffServClassTypeGet, FsMplsDiffServClassTypeSet, FsMplsDiffServClassTypeTest, FsMplsDiffServTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMplsDiffServTableINDEX, 4, 0, 0, NULL},

{{13,FsMplsDiffServServiceType}, GetNextIndexFsMplsDiffServTable, FsMplsDiffServServiceTypeGet, FsMplsDiffServServiceTypeSet, FsMplsDiffServServiceTypeTest, FsMplsDiffServTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsDiffServTableINDEX, 4, 0, 0, "0"},

{{13,FsMplsDiffServLlspPsc}, GetNextIndexFsMplsDiffServTable, FsMplsDiffServLlspPscGet, FsMplsDiffServLlspPscSet, FsMplsDiffServLlspPscTest, FsMplsDiffServTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsDiffServTableINDEX, 4, 0, 0, NULL},

{{13,FsMplsDiffServElspType}, GetNextIndexFsMplsDiffServTable, FsMplsDiffServElspTypeGet, FsMplsDiffServElspTypeSet, FsMplsDiffServElspTypeTest, FsMplsDiffServTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsDiffServTableINDEX, 4, 0, 0, "0"},

{{13,FsMplsDiffServElspListIndex}, GetNextIndexFsMplsDiffServTable, FsMplsDiffServElspListIndexGet, FsMplsDiffServElspListIndexSet, FsMplsDiffServElspListIndexTest, FsMplsDiffServTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMplsDiffServTableINDEX, 4, 0, 0, NULL},

{{13,FsMplsDiffServRowStatus}, GetNextIndexFsMplsDiffServTable, FsMplsDiffServRowStatusGet, FsMplsDiffServRowStatusSet, FsMplsDiffServRowStatusTest, FsMplsDiffServTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsDiffServTableINDEX, 4, 0, 1, NULL},

{{13,FsMplsDiffServStorageType}, GetNextIndexFsMplsDiffServTable, FsMplsDiffServStorageTypeGet, FsMplsDiffServStorageTypeSet, FsMplsDiffServStorageTypeTest, FsMplsDiffServTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsDiffServTableINDEX, 4, 0, 0, NULL},
};
tMibData FsMplsDiffServTableEntry = { 7, FsMplsDiffServTableMibEntry };

tMbDbEntry FsMplsDiffServElspInfoTableMibEntry[]= {

{{13,FsMplsDiffServElspInfoListIndex}, GetNextIndexFsMplsDiffServElspInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMplsDiffServElspInfoTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsDiffServElspInfoIndex}, GetNextIndexFsMplsDiffServElspInfoTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_NOACCESS, FsMplsDiffServElspInfoTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsDiffServElspInfoPHB}, GetNextIndexFsMplsDiffServElspInfoTable, FsMplsDiffServElspInfoPHBGet, FsMplsDiffServElspInfoPHBSet, FsMplsDiffServElspInfoPHBTest, FsMplsDiffServElspInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsDiffServElspInfoTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsDiffServElspInfoResourcePointer}, GetNextIndexFsMplsDiffServElspInfoTable, FsMplsDiffServElspInfoResourcePointerGet, FsMplsDiffServElspInfoResourcePointerSet, FsMplsDiffServElspInfoResourcePointerTest, FsMplsDiffServElspInfoTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, FsMplsDiffServElspInfoTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsDiffServElspInfoRowStatus}, GetNextIndexFsMplsDiffServElspInfoTable, FsMplsDiffServElspInfoRowStatusGet, FsMplsDiffServElspInfoRowStatusSet, FsMplsDiffServElspInfoRowStatusTest, FsMplsDiffServElspInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsDiffServElspInfoTableINDEX, 2, 0, 1, NULL},

{{13,FsMplsDiffServElspInfoStorageType}, GetNextIndexFsMplsDiffServElspInfoTable, FsMplsDiffServElspInfoStorageTypeGet, FsMplsDiffServElspInfoStorageTypeSet, FsMplsDiffServElspInfoStorageTypeTest, FsMplsDiffServElspInfoTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsDiffServElspInfoTableINDEX, 2, 0, 0, NULL},
};
tMibData FsMplsDiffServElspInfoTableEntry = { 6, FsMplsDiffServElspInfoTableMibEntry };

tMbDbEntry FsMplsDiffServElspInfoListIndexNextMibEntry[]= {

{{11,FsMplsDiffServElspInfoListIndexNext}, NULL, FsMplsDiffServElspInfoListIndexNextGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsDiffServElspInfoListIndexNextEntry = { 1, FsMplsDiffServElspInfoListIndexNextMibEntry };

tMbDbEntry FsMplsTnlModelMibEntry[]= {

{{11,FsMplsTnlModel}, NULL, FsMplsTnlModelGet, FsMplsTnlModelSet, FsMplsTnlModelTest, FsMplsTnlModelDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};
tMibData FsMplsTnlModelEntry = { 1, FsMplsTnlModelMibEntry };

tMbDbEntry FsMplsRsrcMgmtTypeMibEntry[]= {

{{11,FsMplsRsrcMgmtType}, NULL, FsMplsRsrcMgmtTypeGet, FsMplsRsrcMgmtTypeSet, FsMplsRsrcMgmtTypeTest, FsMplsRsrcMgmtTypeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};
tMibData FsMplsRsrcMgmtTypeEntry = { 1, FsMplsRsrcMgmtTypeMibEntry };

tMbDbEntry FsMplsTTLValMibEntry[]= {

{{11,FsMplsTTLVal}, NULL, FsMplsTTLValGet, FsMplsTTLValSet, FsMplsTTLValTest, FsMplsTTLValDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "255"},
};
tMibData FsMplsTTLValEntry = { 1, FsMplsTTLValMibEntry };

tMbDbEntry FsMplsDsTeStatusMibEntry[]= {

{{11,FsMplsDsTeStatus}, NULL, FsMplsDsTeStatusGet, FsMplsDsTeStatusSet, FsMplsDsTeStatusTest, FsMplsDsTeStatusDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},
};
tMibData FsMplsDsTeStatusEntry = { 1, FsMplsDsTeStatusMibEntry };

tMbDbEntry FsMplsDsTeClassTypeTableMibEntry[]= {

{{13,FsMplsDsTeClassTypeIndex}, GetNextIndexFsMplsDsTeClassTypeTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMplsDsTeClassTypeTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsDsTeClassTypeDescription}, GetNextIndexFsMplsDsTeClassTypeTable, FsMplsDsTeClassTypeDescriptionGet, FsMplsDsTeClassTypeDescriptionSet, FsMplsDsTeClassTypeDescriptionTest, FsMplsDsTeClassTypeTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMplsDsTeClassTypeTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsDsTeClassTypeRowStatus}, GetNextIndexFsMplsDsTeClassTypeTable, FsMplsDsTeClassTypeRowStatusGet, FsMplsDsTeClassTypeRowStatusSet, FsMplsDsTeClassTypeRowStatusTest, FsMplsDsTeClassTypeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsDsTeClassTypeTableINDEX, 1, 0, 1, NULL},

{{13,FsMplsDsTeClassTypeBwPercentage}, GetNextIndexFsMplsDsTeClassTypeTable, FsMplsDsTeClassTypeBwPercentageGet, FsMplsDsTeClassTypeBwPercentageSet, FsMplsDsTeClassTypeBwPercentageTest, FsMplsDsTeClassTypeTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMplsDsTeClassTypeTableINDEX, 1, 0, 0, "0"},
};
tMibData FsMplsDsTeClassTypeTableEntry = { 4, FsMplsDsTeClassTypeTableMibEntry };

tMbDbEntry FsMplsDsTeClassTypeToTcMapTableMibEntry[]= {

{{13,FsMplsDsTeTcIndex}, GetNextIndexFsMplsDsTeClassTypeToTcMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMplsDsTeClassTypeToTcMapTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsDsTeTcType}, GetNextIndexFsMplsDsTeClassTypeToTcMapTable, FsMplsDsTeTcTypeGet, FsMplsDsTeTcTypeSet, FsMplsDsTeTcTypeTest, FsMplsDsTeClassTypeToTcMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsDsTeClassTypeToTcMapTableINDEX, 2, 0, 0, "0"},

{{13,FsMplsDsTeTcDescription}, GetNextIndexFsMplsDsTeClassTypeToTcMapTable, FsMplsDsTeTcDescriptionGet, FsMplsDsTeTcDescriptionSet, FsMplsDsTeTcDescriptionTest, FsMplsDsTeClassTypeToTcMapTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMplsDsTeClassTypeToTcMapTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsDsTeTcMapRowStatus}, GetNextIndexFsMplsDsTeClassTypeToTcMapTable, FsMplsDsTeTcMapRowStatusGet, FsMplsDsTeTcMapRowStatusSet, FsMplsDsTeTcMapRowStatusTest, FsMplsDsTeClassTypeToTcMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsDsTeClassTypeToTcMapTableINDEX, 2, 0, 1, NULL},
};
tMibData FsMplsDsTeClassTypeToTcMapTableEntry = { 4, FsMplsDsTeClassTypeToTcMapTableMibEntry };

tMbDbEntry FsMplsDsTeTeClassTableMibEntry[]= {

{{13,FsMplsDsTeTeClassPriority}, GetNextIndexFsMplsDsTeTeClassTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMplsDsTeTeClassTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsDsTeTeClassDesc}, GetNextIndexFsMplsDsTeTeClassTable, FsMplsDsTeTeClassDescGet, FsMplsDsTeTeClassDescSet, FsMplsDsTeTeClassDescTest, FsMplsDsTeTeClassTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMplsDsTeTeClassTableINDEX, 2, 0, 0, NULL},

{{13,FsMplsDsTeTeClassNumber}, GetNextIndexFsMplsDsTeTeClassTable, FsMplsDsTeTeClassNumberGet, FsMplsDsTeTeClassNumberSet, FsMplsDsTeTeClassNumberTest, FsMplsDsTeTeClassTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsDsTeTeClassTableINDEX, 2, 0, 0, "0"},

{{13,FsMplsDsTeTeClassRowStatus}, GetNextIndexFsMplsDsTeTeClassTable, FsMplsDsTeTeClassRowStatusGet, FsMplsDsTeTeClassRowStatusSet, FsMplsDsTeTeClassRowStatusTest, FsMplsDsTeTeClassTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsDsTeTeClassTableINDEX, 2, 0, 1, NULL},
};
tMibData FsMplsDsTeTeClassTableEntry = { 4, FsMplsDsTeTeClassTableMibEntry };

tMbDbEntry FsMplsSimulateFailureMibEntry[]= {

{{11,FsMplsSimulateFailure}, NULL, FsMplsSimulateFailureGet, FsMplsSimulateFailureSet, FsMplsSimulateFailureTest, FsMplsSimulateFailureDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsSimulateFailureEntry = { 1, FsMplsSimulateFailureMibEntry };

tMbDbEntry FsMplsiTTLValMibEntry[]= {

{{11,FsMplsiTTLVal}, NULL, FsMplsiTTLValGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, "255"},
};
tMibData FsMplsiTTLValEntry = { 1, FsMplsiTTLValMibEntry };

tMbDbEntry FsMplsOTTLValMibEntry[]= {

{{11,FsMplsOTTLVal}, NULL, FsMplsOTTLValGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, "255"},
};
tMibData FsMplsOTTLValEntry = { 1, FsMplsOTTLValMibEntry };

tMbDbEntry FsMplsprviTTLValMibEntry[]= {

{{11,FsMplsprviTTLVal}, NULL, FsMplsprviTTLValGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, "0"},
};
tMibData FsMplsprviTTLValEntry = { 1, FsMplsprviTTLValMibEntry };

tMbDbEntry FsMplsprvOTTLValMibEntry[]= {

{{11,FsMplsprvOTTLVal}, NULL, FsMplsprvOTTLValGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, "0"},
};
tMibData FsMplsprvOTTLValEntry = { 1, FsMplsprvOTTLValMibEntry };

tMbDbEntry FsMplsLdpGrCapabilityMibEntry[]= {

{{11,FsMplsLdpGrCapability}, NULL, FsMplsLdpGrCapabilityGet, FsMplsLdpGrCapabilitySet, FsMplsLdpGrCapabilityTest, FsMplsLdpGrCapabilityDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},
};
tMibData FsMplsLdpGrCapabilityEntry = { 1, FsMplsLdpGrCapabilityMibEntry };

tMbDbEntry FsMplsLdpGrForwardEntryHoldTimeMibEntry[]= {

{{11,FsMplsLdpGrForwardEntryHoldTime}, NULL, FsMplsLdpGrForwardEntryHoldTimeGet, FsMplsLdpGrForwardEntryHoldTimeSet, FsMplsLdpGrForwardEntryHoldTimeTest, FsMplsLdpGrForwardEntryHoldTimeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "600"},
};
tMibData FsMplsLdpGrForwardEntryHoldTimeEntry = { 1, FsMplsLdpGrForwardEntryHoldTimeMibEntry };

tMbDbEntry FsMplsLdpGrMaxRecoveryTimeMibEntry[]= {

{{11,FsMplsLdpGrMaxRecoveryTime}, NULL, FsMplsLdpGrMaxRecoveryTimeGet, FsMplsLdpGrMaxRecoveryTimeSet, FsMplsLdpGrMaxRecoveryTimeTest, FsMplsLdpGrMaxRecoveryTimeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "120"},
};
tMibData FsMplsLdpGrMaxRecoveryTimeEntry = { 1, FsMplsLdpGrMaxRecoveryTimeMibEntry };

tMbDbEntry FsMplsLdpGrNeighborLivenessTimeMibEntry[]= {

{{11,FsMplsLdpGrNeighborLivenessTime}, NULL, FsMplsLdpGrNeighborLivenessTimeGet, FsMplsLdpGrNeighborLivenessTimeSet, FsMplsLdpGrNeighborLivenessTimeTest, FsMplsLdpGrNeighborLivenessTimeDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "120"},
};
tMibData FsMplsLdpGrNeighborLivenessTimeEntry = { 1, FsMplsLdpGrNeighborLivenessTimeMibEntry };

tMbDbEntry FsMplsLdpGrProgressStatusMibEntry[]= {

{{11,FsMplsLdpGrProgressStatus}, NULL, FsMplsLdpGrProgressStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, NULL},
};
tMibData FsMplsLdpGrProgressStatusEntry = { 1, FsMplsLdpGrProgressStatusMibEntry };

tMbDbEntry FsMplsLdpPeerTableMibEntry[]= {

{{13,FsMplsLdpPeerGrReconnectTime}, GetNextIndexFsMplsLdpPeerTable, FsMplsLdpPeerGrReconnectTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMplsLdpPeerTableINDEX, 3, 0, 0, NULL},

{{13,FsMplsLdpPeerGrRecoveryTime}, GetNextIndexFsMplsLdpPeerTable, FsMplsLdpPeerGrRecoveryTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMplsLdpPeerTableINDEX, 3, 0, 0, NULL},

{{13,FsMplsLdpPeerGrProgressStatus}, GetNextIndexFsMplsLdpPeerTable, FsMplsLdpPeerGrProgressStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMplsLdpPeerTableINDEX, 3, 0, 0, NULL},
};
tMibData FsMplsLdpPeerTableEntry = { 3, FsMplsLdpPeerTableMibEntry };

tMbDbEntry FsMplsLdpConfigurationSequenceTLVMibEntry[]= {
{{11,FsMplsLdpConfigurationSequenceTLVEnable}, NULL, FsMplsLdpConfigurationSequenceTLVEnableGet, FsMplsLdpConfigurationSequenceTLVEnableSet, FsMplsLdpConfigurationSequenceTLVEnableTest, FsMplsLdpConfigurationSequenceTLVEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "0"},
};
tMibData FsMplsLdpConfigurationSequenceTLVEntry = { 1, FsMplsLdpConfigurationSequenceTLVMibEntry };
#ifdef MPLS_L3VPN_WANTED
tMbDbEntry FsMplsL3VpnVrfEgressRteTableMibEntry[]= {
{{13,FsMplsL3VpnVrfName}, GetNextIndexFsMplsL3VpnVrfEgressRteTable, FsMplsL3VpnVrfNameGet, FsMplsL3VpnVrfNameSet, FsMplsL3VpnVrfNameTest, FsMplsL3VpnVrfEgressRteTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMplsL3VpnVrfEgressRteTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsL3VpnVrfEgressRteInetCidrDestType}, GetNextIndexFsMplsL3VpnVrfEgressRteTable, FsMplsL3VpnVrfEgressRteInetCidrDestTypeGet, FsMplsL3VpnVrfEgressRteInetCidrDestTypeSet, FsMplsL3VpnVrfEgressRteInetCidrDestTypeTest, FsMplsL3VpnVrfEgressRteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsL3VpnVrfEgressRteTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsL3VpnVrfEgressRteInetCidrDest}, GetNextIndexFsMplsL3VpnVrfEgressRteTable, FsMplsL3VpnVrfEgressRteInetCidrDestGet, FsMplsL3VpnVrfEgressRteInetCidrDestSet, FsMplsL3VpnVrfEgressRteInetCidrDestTest, FsMplsL3VpnVrfEgressRteTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMplsL3VpnVrfEgressRteTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsL3VpnVrfEgressRteInetCidrPfxLen}, GetNextIndexFsMplsL3VpnVrfEgressRteTable, FsMplsL3VpnVrfEgressRteInetCidrPfxLenGet, FsMplsL3VpnVrfEgressRteInetCidrPfxLenSet, FsMplsL3VpnVrfEgressRteInetCidrPfxLenTest, FsMplsL3VpnVrfEgressRteTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsL3VpnVrfEgressRteTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsL3VpnVrfEgressRteInetCidrLabelPolicy}, GetNextIndexFsMplsL3VpnVrfEgressRteTable, FsMplsL3VpnVrfEgressRteInetCidrLabelPolicyGet, FsMplsL3VpnVrfEgressRteInetCidrLabelPolicySet, FsMplsL3VpnVrfEgressRteInetCidrLabelPolicyTest, FsMplsL3VpnVrfEgressRteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsL3VpnVrfEgressRteTableINDEX, 1, 0, 0, "1"},
{{13,FsMplsL3VpnVrfEgressRteInetCidrNHopType}, GetNextIndexFsMplsL3VpnVrfEgressRteTable, FsMplsL3VpnVrfEgressRteInetCidrNHopTypeGet, FsMplsL3VpnVrfEgressRteInetCidrNHopTypeSet, FsMplsL3VpnVrfEgressRteInetCidrNHopTypeTest, FsMplsL3VpnVrfEgressRteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsL3VpnVrfEgressRteTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsL3VpnVrfEgressRteInetCidrNextHop}, GetNextIndexFsMplsL3VpnVrfEgressRteTable, FsMplsL3VpnVrfEgressRteInetCidrNextHopGet, FsMplsL3VpnVrfEgressRteInetCidrNextHopSet, FsMplsL3VpnVrfEgressRteInetCidrNextHopTest, FsMplsL3VpnVrfEgressRteTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMplsL3VpnVrfEgressRteTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsL3VpnVrfEgressRteInetCidrIfIndex}, GetNextIndexFsMplsL3VpnVrfEgressRteTable, FsMplsL3VpnVrfEgressRteInetCidrIfIndexGet, FsMplsL3VpnVrfEgressRteInetCidrIfIndexSet, FsMplsL3VpnVrfEgressRteInetCidrIfIndexTest, FsMplsL3VpnVrfEgressRteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsL3VpnVrfEgressRteTableINDEX, 1, 0, 0, "0"},

{{13,FsMplsL3VpnVrfEgressRteInetCidrMetric1}, GetNextIndexFsMplsL3VpnVrfEgressRteTable, FsMplsL3VpnVrfEgressRteInetCidrMetric1Get, FsMplsL3VpnVrfEgressRteInetCidrMetric1Set, FsMplsL3VpnVrfEgressRteInetCidrMetric1Test, FsMplsL3VpnVrfEgressRteTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsMplsL3VpnVrfEgressRteTableINDEX, 1, 0, 0, "-1"},

{{13,FsMplsL3VpnVrfEgressRtePathAttrLabel}, GetNextIndexFsMplsL3VpnVrfEgressRteTable, FsMplsL3VpnVrfEgressRtePathAttrLabelGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMplsL3VpnVrfEgressRteTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsL3VpnVrfEgressRteInetCidrStatus}, GetNextIndexFsMplsL3VpnVrfEgressRteTable, FsMplsL3VpnVrfEgressRteInetCidrStatusGet, FsMplsL3VpnVrfEgressRteInetCidrStatusSet, FsMplsL3VpnVrfEgressRteInetCidrStatusTest, FsMplsL3VpnVrfEgressRteTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsL3VpnVrfEgressRteTableINDEX, 1, 0, 1, NULL},
};
tMibData FsMplsL3VpnVrfEgressRteTableEntry = { 11, FsMplsL3VpnVrfEgressRteTableMibEntry };
#endif
#endif /* _FSMPLSDB_H */
