
/********************************************************************
 *                                                                  *
 * $RCSfile: lbmgrinc.h,v $                                          
 *                                                                  *
 * $Date: 2007/02/01 14:57:08 $
 *                                                                  *
 * $Revision: 1.2 $                                             
 *                                                                  *
 ********************************************************************/

/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : lbmgrinc.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS - Utility    
 *    MODULE NAME            : Inteface Module
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains list of FSAP and other header
 *                             files utilised by the label manager.
 *----------------------------------------------------------------------------*/

#ifndef _LBMGRINC_H
#define _LBMGRINC_H


#include "lr.h"
#include "mplssize.h"

#endif /*_LBMGRINC_H */
/*---------------------------------------------------------------------------*/
/*                        End of file lbmgrinc.h                             */
/*---------------------------------------------------------------------------*/
