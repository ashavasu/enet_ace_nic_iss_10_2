enum {
    MAX_LANAI_ATM_INFO_SIZING_ID,
    MAX_LANAI_VC_TABLE_ENTRIES_SIZING_ID,
    LANAI_MAX_SIZING_ID
};


#ifdef  _LANAISZ_C
tMemPoolId LANAIMemPoolIds[ LANAI_MAX_SIZING_ID];
INT4  LanaiSizingMemCreateMemPools(VOID);
VOID  LanaiSizingMemDeleteMemPools(VOID);
INT4  LanaiSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _LANAISZ_C  */
extern tMemPoolId LANAIMemPoolIds[ ];
extern INT4  LanaiSizingMemCreateMemPools(VOID);
extern VOID  LanaiSizingMemDeleteMemPools(VOID);
extern INT4  LanaiSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _LANAISZ_C  */


#ifdef  _LANAISZ_C
tFsModSizingParams FsLANAISizingParams [] = {
{ "tMplsAtmInfo", "MAX_LANAI_ATM_INFO", sizeof(tMplsAtmInfo),MAX_LANAI_ATM_INFO, MAX_LANAI_ATM_INFO,0 },
{ "tVcTableEntry", "MAX_LANAI_VC_TABLE_ENTRIES", sizeof(tVcTableEntry),MAX_LANAI_VC_TABLE_ENTRIES, MAX_LANAI_VC_TABLE_ENTRIES,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _LANAISZ_C  */
extern tFsModSizingParams FsLANAISizingParams [];
#endif /*  _LANAISZ_C  */


