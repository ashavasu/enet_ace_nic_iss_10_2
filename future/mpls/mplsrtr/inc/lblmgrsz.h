/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: lblmgrsz.h,v 1.2 2010/10/19 06:58:29 prabuc Exp $
*
* Description: This file contains sizing related MACROS
*
*******************************************************************/
enum {
    MAX_LBLMGR_LBL_MGR_KEY1_INFO_SIZING_ID,
    MAX_LBLMGR_LBL_MGR_KEY2_INFO_SIZING_ID,
    MAX_LBLMGR_LBL_SPACE_GRP_SIZING_ID,
    LBLMGR_MAX_SIZING_ID
};


#ifdef  _LBLMGRSZ_C
tMemPoolId LBLMGRMemPoolIds[ LBLMGR_MAX_SIZING_ID];
INT4  LblmgrSizingMemCreateMemPools(VOID);
VOID  LblmgrSizingMemDeleteMemPools(VOID);
INT4  LblmgrSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _LBLMGRSZ_C  */
extern tMemPoolId LBLMGRMemPoolIds[ ];
extern INT4  LblmgrSizingMemCreateMemPools(VOID);
extern VOID  LblmgrSizingMemDeleteMemPools(VOID);
extern INT4  LblmgrSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _LBLMGRSZ_C  */


#ifdef  _LBLMGRSZ_C
tFsModSizingParams FsLBLMGRSizingParams [] = {
{ "tAsgnKey1Info", "MAX_LBLMGR_LBL_MGR_KEY1_INFO", sizeof(tAsgnKey1Info),MAX_LBLMGR_LBL_MGR_KEY1_INFO, MAX_LBLMGR_LBL_MGR_KEY1_INFO,0 },
{ "tAsgnKey2Info", "MAX_LBLMGR_LBL_MGR_KEY2_INFO", sizeof(tAsgnKey2Info),MAX_LBLMGR_LBL_MGR_KEY2_INFO, MAX_LBLMGR_LBL_MGR_KEY2_INFO,0 },
{ "tAsgnInfoHeadSize", "MAX_LBLMGR_LBL_SPACE_GRP", sizeof(tAsgnInfoHeadSize),MAX_LBLMGR_LBL_SPACE_GRP, MAX_LBLMGR_LBL_SPACE_GRP,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _LBLMGRSZ_C  */
extern tFsModSizingParams FsLBLMGRSizingParams [];
#endif /*  _LBLMGRSZ_C  */


