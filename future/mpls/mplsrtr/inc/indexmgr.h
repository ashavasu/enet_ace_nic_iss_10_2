
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: indexmgr.h,v 1.11 2012/12/20 11:18:49 siva Exp $
 *              
 ********************************************************************/

/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *---------------------------------------------------------------------------
 *    FILE  NAME             : indexmgr.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS - Utility    
 *    MODULE NAME            : L2Vpn Index Tbl Mgr. Inteface Module
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                        
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains typedefintions, constant
 *                             definitions, global declarations that are
 *                             associated with Index Space (Group-Key) Mngr. 
 *-------------------------------------------------------------------------*/

#ifndef _INDEXMGR_H
#define _INDEXMGR_H


#define MAX_INDEX_MGR_GROUP_INFO_SIZE    (MAX_INDEXMGR_GRPS_SPRTD * sizeof (tIndexMgrGrpInfo))
#define MAX_INDEX_MGR_CHUNK_INFO_SIZE    (MAX_INDEXMGR_MAX_OF_CHUNKS_PER_GROUP * \
                                      sizeof (tIndexMgrChunkInfo))
/*============================================*/   

/* debugs definition - dummy */   
#define INDEXMGR_SUPPRESS_WARNING(x) x = x; 

#define DBG_ERR_CRT          0xffffffff   
#define MPLSRTR_DBG_FLAG     0xffffffff

#define MPLSRTR_DBG(u4Value, pu1Format) \
        if (u4Value == (u4Value & MPLSRTR_DBG_FLAG)) \
           UtlTrcLog (MPLSRTR_DBG_FLAG, u4Value, "MRTR", pu1Format)

#define MPLSRTR_DBG2(u4Value, pu1Format, Arg1, Arg2) \
        if (u4Value == (u4Value & MPLSRTR_DBG_FLAG)) \
         UtlTrcLog (MPLSRTR_DBG_FLAG, u4Value, "MRTR", pu1Format,Arg1,Arg2)        
/* debug definitions end */    
  
/*  Definition of memory type */
#define INDEXMGR_MEM_MODE         MEM_DEFAULT_MEMORY_TYPE

#define INDEXMGR_BYTE_BLOCK_SIZE  8 /* 1 byte = 8 bits */ 

#define INDEXMGR_SOME_AVAIL_BMAP  0xFFFFFFFF
         /* Mask to check if at least 1 bit of UINT4 is 1 */     

/*Helps in determining at least 1 bit is available(=1) in the UINT4 bitmap*/

#define INDEXMGR_GRP_FULL         1
#define INDEXMGR_GRP_VACANT       0

 /* Index Manager  semaphore name */
#define MPLS_INDEXMGR_SEM_NAME (UINT1 *)"IMRS"

/* Typedefinitions used for the Group Key Manager. */
/* structure to hold "INDEX_CHUNK_SIZE" memory chunks holding bitmaps */
typedef struct IndexMgrChunkInfo
{
   UINT4          u4NumKeysAlloc; /* Number of keys allocated*/
                                  /* UINT2 is enough...yet to play-safe */ 
   UINT4          u4CurOffset;    /* Current offset in the chunk */
   UINT4         *pu4IndexChunk;  /* pointer to continuous memory chunk */
} tIndexMgrChunkInfo;


/* This structure is added for system sizing.
 * It should not be used for any other purpose
 */

typedef struct _IndexMgrChunkInfoSize
{
   UINT1          au1IndexMgrChunkInfo[MAX_INDEX_MGR_CHUNK_INFO_SIZE];
}
tIndexMgrChunkInfoSize;

/* structure to hold information about an index-group */
typedef struct IndexMgrGrpInfo
{
   UINT4          u4NumChunksInGrp; /* Number of chunks in this group */
                                 /*  UINT2 suffices, yet to play-safe */
   UINT4          u4MaxChunksSprtd; /* Max chunks supported for this group*/
   UINT4          u4AvailChunkID; /* Chunk-ID wherefrom key-alloc is made */
   UINT4          u4TotalKeysAlloc; /* Total Keys allocated for this grp */
   tIndexMgrChunkInfo *pIndexChunkInfoTbl;
                                    /* Ptr to arr of chunk-info's */ 
} tIndexMgrGrpInfo; 


typedef struct _IndexMgrGrpTable
{
    CHR1         *pc1ModName;        /* Module Name to this group belongs to */
    CHR1         *pc1GrpName;        /* Group Name */

} tIndexMgrGrpTable;

/* This structure is added for system sizing.
 * It should not be used for any other purpose
 */
typedef struct _IndexMgrGrpInfoSize
{
   UINT1          au1IndexMgrGrpInfo[MAX_INDEX_MGR_GROUP_INFO_SIZE];
}
tIndexMgrGrpInfoSize;


/* This structure is added for system sizing.
 * It should not be used for any other purpose
 */
typedef struct _IndexMgrChunkSize
{
    UINT1         au1IndexMgrChunk[MAX_INDEXMGR_CHUNK_SIZE];
}
tIndexMgrChunkSize;


/* Function Prototypes */
/* External Functions : Available for calling by other modules. */
UINT1 IndexMgrInitWithSem ARG_LIST ((VOID));
UINT1 IndexMgrDeInit ARG_LIST ((VOID));

/* Internal functions : Internally called by this module only */
UINT4 IndexMgrGetIndexBasedOnFlag ARG_LIST ((UINT1 u1GrpID,BOOL1 bFlag));
UINT4 IndexMgrGetAvailableIndex ARG_LIST ((UINT1 u1GrpID));

UINT4 IndexMgrSetIndexBasedOnFlag ARG_LIST ((UINT1 u1GrpID,UINT4 u4Index,BOOL1 bFlag));
UINT4 IndexMgrSetIndex ARG_LIST ((UINT1 u1GrpID,UINT4 u4Index));
UINT4 IndexMgrCheckIndex ARG_LIST ((UINT1 u1GrpID,UINT4 u4Index));
 
UINT1 IndexMgrRelIndex ARG_LIST ((UINT1 u1GrpID, UINT4 u4Index));

UINT1 IndexMgrInitGrps ARG_LIST ((VOID));
UINT1 IndexMgrCheckGrpFull ARG_LIST ((UINT1 u1GrpID));
UINT1 IndexMgrGetAvailByteBitPos 
  ARG_LIST ((UINT4 u4AvailIndexBitmap, UINT1 *pu1BytePos, UINT1 *pu1BitPos));
UINT1 IndexMgrUpdateChunkOffset ARG_LIST ((UINT1 u1GrpID));  
/* Reference of SRM MEM Function */

#endif /*_INDEXMGR_H */
/*-------------------------------------------------------------------------
                        End of file indexmgr.h                             
--------------------------------------------------------------------------*/
