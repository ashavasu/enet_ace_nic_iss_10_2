
/********************************************************************
 *                                                                  *
 * $RCSfile: inmgrinc.h,v $                                          
 *                                                                  *
 * $Date: 2007/02/01 14:57:08 $
 *                                                                  *
 * $Revision: 1.2 $                                             
 *                                                                  *
 ********************************************************************/

/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *--------------------------------------------------------------------
 *    FILE  NAME             : inmgrinc.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS - Utility    
 *    MODULE NAME            : L2Vpn Index Tbl Mgr : Inteface Module
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains list of FSAP and other 
 *                             header files utilised by the index manager.
 *-----------------------------------------------------------------------*/

#ifndef _INDEXMGRINC_H
#define _INDEXMGRINC_H

#include "osxstd.h"
#include "srmmem.h"
#include "utltrc.h"

#endif /*_INDEXMGRINC_H */

/*--------------------------------------------------------------------------
*                         End of file inmgrinc.h                          
--------------------------------------------------------------------------*/
