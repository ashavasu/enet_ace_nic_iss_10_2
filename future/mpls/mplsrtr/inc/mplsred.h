/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: mplsred.h,v 1.8 2014/11/28 02:13:14 siva Exp $
*
* Description: This file contains MPLS redundancy related MACROS
*
*******************************************************************/

#ifndef _MPLSRM_H
#define _MPLSRM_H

#include "lr.h"
#include "rmgr.h"
#include "mplcmndb.h"
#include "mplsftn.h"
#include "mplslsr.h"


#define MPLSRM_MEM_TYPE           MEM_DEFAULT_MEMORY_TYPE
#define MPLSRM_QDEPTH             MAX_MPLSRTR_RM_FRAME
#define MPLSRM_QMODE              OSIX_LOCAL
#define MPLSRM_QNAME              "MPLSRMQ"
#define MPLSRM_TSK_NAME           ((UINT1 *)"MPRM")

#define MPLSRM_TASK_PRIORITY      220
/* Node Status */
typedef UINT4 tMplsNodeStatus;

#define MPLS_NODE_IDLE                             RM_INIT
#define MPLS_NODE_ACTIVE                           RM_ACTIVE
#define MPLS_NODE_STANDBY                          RM_STANDBY

#define MPLS_NODE_STATUS()         gMplsNodeStatus
#define MPLS_RM_GET_STATIC_CONFIG_STATUS() RmGetStaticConfigStatus () 
extern tOsixTaskId     gMplsHwAuditTskId;
extern tMplsNodeStatus gMplsNodeStatus;
extern tOsixQId        gMplsRmQId;
#define MPLS_IS_NP_PROGRAMMING_ALLOWED() \
        ((gMplsNodeStatus == MPLS_NODE_ACTIVE) ? MPLS_TRUE: MPLS_FALSE)


/* Peer node Id.*/ 
typedef VOID * tMplsRmPeerId;
/* To handle message/events given by redundancy manager. */
typedef struct {
    tRmMsg        *pFrame;      /*Message given by RM module. */
    tMplsRmPeerId   PeerId;
    UINT2          u2Length;   /* Length of message given by RM module. */
    UINT1          u1Event;    /* Event given by RM module. */
    UINT1          u1Reserved;
}tMplsRmFrame;


/* Rm Functions*/

UINT1 MplsRmInit(VOID);
INT4 MplsRegisterWithRM (VOID);
INT4 MplsDeRegisterWithRM (VOID);
INT4 MplsRmMakeNodeActive (VOID);
INT4 MplsRcvPktFromRm (UINT1 u1Event, tRmMsg *pData, UINT2 u2DataLen);
VOID
MplsRmHwAuditMain (INT1 *pi1Param);
VOID MplsFTNTableAudit(VOID);
VOID MplsILMTableAudit(VOID);
VOID MplsPWTableAudit(VOID);
VOID MplsVplsTableAudit(VOID);
UINT1 MplsRmDeInit (VOID);
VOID
MplsRedHandleRmEvents (VOID);

VOID MplsP2mpILMTableAudit(VOID); /* MPLS_P2MP_LSP_CHANGES */
#endif


