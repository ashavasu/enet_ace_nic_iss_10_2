
/********************************************************************
 *                                                                  *
 * $RCSfile: mplsport.h,v $
 *                                                                  *
 * $Date: 2010/09/24 06:29:25 $ 
 *                                                                  *
 * $Revision: 1.13 $
 *                                                                  *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : mplsport.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : MPLS-FM
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains porting related macros 
 *---------------------------------------------------------------------------*/
#ifndef _MPLS_PORT_H
#define _MPLS_PORT_H

/*
 * Functions from libraries - FSAP -II
 */

/* FutureOSIX related macros */
#define MPLS_CREATE_TASK         OsixTskCrt
#define MPLS_DELETE_TASK         OsixTskDel
#define MPLS_SEND_EVENT          OsixEvtSend 
#define MPLS_WAIT_FOR_EVENT      OsixEvtRecv
#define MPLS_CREATE_Q            OsixQueCrt
#define MPLS_DELETE_Q            OsixQueDel
#define MPLS_ENQUEUE             OsixQueSend
#define MPLS_DEQUEUE             OsixQueRecv
#define MPLS_Q_SUCCESS           OSIX_SUCCESS
#define MPLS_Q_FAILURE           OSIX_FAILURE
#define MPLS_CREATE_SEM          OsixCreateSem
#define MPLS_DELETE_SEM          OsixSemDel

#define MPLS_TAKE_SEM(pu1Sem) \
        OsixSemTake(pu1Sem)
#define MPLS_RELEASE_SEM(pu1Sem) \
        OsixSemGive(pu1Sem)
 
#define MPLS_SEM_SUCCESS         OSIX_SUCCESS
#define MPLS_SEM_FAILURE         OSIX_FAILURE
#define MPLS_NO_WAIT             OSIX_NO_WAIT
#define MPLS_WAIT_FOREVER        OSIX_WAIT
#define MPLS_BUF_SUCCESS         CRU_SUCCESS
#define MPLS_BUF_FAILURE         CRU_FAILURE
#define MPLS_BUF_NORMAL_REL      FALSE
#define MPLS_PRI_URGENT          OSIX_MSG_URGENT
#define MPLS_NORMAL_MSG          OSIX_MSG_NORMAL
#define MLPS_EVENT_SUCCESS       OSIX_SUCCESS
#define MPLS_GLOBAL_MODE         OSIX_GLOBAL
#define MPLS_LOCAL_MODE          OSIX_LOCAL
#define MPLS_MEM_SUCCESS         MEM_SUCCESS
#define MPLS_MEM_FAILURE         MEM_FAILURE
#define MPLS_HTONS               OSIX_HTONS
#define MPLS_NTOHS               OSIX_NTOHS
#define MPLS_HTONL               OSIX_HTONL
#define MPLS_NTOHL               OSIX_NTOHL
#define MPLS_OSIX_SUCCESS        OSIX_SUCCESS

/* FutureSRM related macros */
#define MPLS_ALLOC_BUF_CHAIN       CRU_BUF_Allocate_MsgBufChain
#define MPLS_REL_BUF_CHAIN         CRU_BUF_Release_MsgBufChain
#define MPLS_GET_VALID_BYTE_COUNT  CRU_BUF_Get_ChainValidByteCount
#define MPLS_PREPEND_TO_BUF        CRU_BUF_Prepend_BufChain
#define MPLS_MOVE_VALID_OFFSET     CRU_BUF_Move_ValidOffset
#define MPLS_COPY_OVER_BUF_CHAIN   CRU_BUF_Copy_OverBufChain
#define MPLS_COPY_FROM_BUF_CHAIN   CRU_BUF_Copy_FromBufChain
#define MPLS_DELETE_BUF_AT_END(pBuf, u4Size)     \
          CRU_BUF_Delete_BufChainAtEnd((pBuf), (u4Size))

#define MPLS_DEFAULT_MEM_TYPE    MEM_DEFAULT_MEMORY_TYPE

/* Macros for accessing the fields of CRU Buffer */
#define MPLS_GET_IFID_FROM_BUF(pBuf)  \
             ((pBuf)->ModuleData.InterfaceId)
#define MPLS_GET_IFTYPE_FROM_BUF(pBuf) \
           (CRU_BUF_Get_Interface_Type(MPLS_GET_IFID_FROM_BUF(pBuf)))
/* End of macros for accessing fields of CRU Buffer */

/* FutureUTL related functions */
#define MPLS_SLL_INIT       TMO_SLL_Init
#define MPLS_SLL_INIT_NODE  TMO_SLL_Init_Node
#define MPLS_SLL_ADD        TMO_SLL_Add
#define MPLS_SLL_DELETE     TMO_SLL_Delete
#define MPLS_SLL_COUNT      TMO_SLL_Count
#define MPLS_SLL_FIRST      TMO_SLL_First
#define MPLS_SLL_Last       TMO_SLL_Last
#define MPLS_SLL_NEXT       TMO_SLL_Next
#define MPLS_SLL_SCAN       TMO_SLL_Scan
#define MPLS_SLL_FREE       TMO_SLL_Free
#define MPLS_SLL_CLEAR      TMO_SLL_Clear
/* FUTURE_UTL */

/* Functions expected from SNMP */
#define MPLS_SEND_SNMP_TRAP      SNMP_AGT_RIF_Notify_V1_Or_V2_Trap
#define MPLS_GET_OID_FROM_STR    SNMP_AGT_GetOidFromString
#define MPLS_FORM_SNMP_VB        SNMP_AGT_FormVarBind

/* Interface related functions */

#define mplsSendMpoaal5PktToIfMod(pBuf, u4ConHandle) \
        mpoaalRcvPktFromHl(u4ConHandle, pBuf)
#define mplsEnqLblBindReq           LdpMplsEventHandler
#define MPLS_RCV_PACKET_FROM_CFA    mplsProcessIncomingAtmPkt

   /* FutureLDP related Events */
#define MPLS_LBL_REQ_EVENT   LDP_MPLS_LBL_REQ_EVENT

/* CFA Related macros */
#define MPLS_CFA_IWFENET_PROCESS_TXFRAME CfaIwfEnetProcessTxFrame
#define MPLS_ARP_TASK_ENQUEUE            arpTaskEnqueuePkt
#define MPLS_IP_HANDLE_PKT_FROM_CFA      CfaProcessIncomingIpPkt 
#define MPLS_CFA_PROCESS_OUTGOING_IP_PKT CfaProcessOutgoingIpPkt
#define MPLS_GET_MAC_ADDR                arp_resolve
#define MPLS_SEND_TO_ARP                 ArpRecv
#define MPLS_SEND_TO_IP                  IpRecv
#define MPLS_GET_SRC_LAN_ADDR            IP_LOWER_LAYER_ADDRESS
#define MPLS_CFA_CREATE_VC_INTERFACE     CfaIfmCreateMplsAtmVcInterface
#define MPLS_CFA_DELETE_VC_INTERFACE     CfaIfmDeleteMplsAtmVcInterface
#define MPLS_SEND_PACKET_TO_CFA          CfaHandlePktFromMpls
#define MPLS_TO_IP_PORT_NO               CFA_IF_HIGH_INDEX
#define MPLS_CFA_SUCCESS                 CFA_SUCCESS      
#define MPLS_CFA_FAILURE                 CFA_FAILURE      
#define MPLS_CFA_OUT_IP_PKT              CFA_OUTGOING  
#define MPLS_CFA_IN_IP_PKT               CFA_INCOMING  
#define MPLS_CFA_GET_IF_INFO             CfaGetIfInfo
#define MPLS_ENCAP_ENETV2                CFA_ENCAP_ENETV2
#define tMplsIfInfo                      tCfaIfInfo
#define tMplsAtmConfigVcStruct           tAtmVcConfigStruct
#define MPLS_IP_UCAST                    IP_UCAST      
#define MPLS_IP_MCAST                    IP_MCAST      
#define MPLS_IP_BCAST                    IP_BCAST     

/* Trie related macros */

#define MPLS_TRIE_CREATE          TrieCreate
#define MPLS_TRIE_SHUTDOWN        TrieShutDown
#define MPLS_TRIE_DELETE          TrieDelete
#define MPLS_TRIE_ADD_ENTRY       TrieAddEntry
#define MPLS_TRIE_DELETE_ENTRY    TrieDeleteEntry

#define MPLS_GET_BEST_FTN_ENTRY  TrieLookupEntry
#define MPLS_GET_EXACT_FTN_ENTRY TrieSearchEntry
#define MPLS_SEARCH_ILM_ENTRY    TrieSearchEntry

#define MPLS_TRIE_GET_NEXT_ENTRY  TrieGetNextEntry

#define MPLS_REL_ILM_ENTRY_FUNC  mplsRelIlmEntry
/* Trie related defines */
#define MPLS_TRIE_SUCCESS     IP_SUCCESS
#define MPLS_TRIE_FAILURE     IP_FAILURE
    
/* IP Related defines */
#define MPLS_IP_SUCCESS        IP_SUCCESS
#define MPLS_IP_FAILURE        IP_FAILURE

#define MPLS_IP_GET_PORT_FROM_IF_INDEX IpGetPortFromIfIndex 
#define MPLS_GET_IFID_FROM_IFINDEX     IpGetIfIdFromIfIndex
#define MPLS_ENET_PHYS_INTERFACE_TYPE  IP_ENET_PHYS_INTERFACE_TYPE

/* ATM SAR Driver Related defines */
   /* ATM_SUCCESS is the value being returned while updating Switching Matrix
    * */
#define MPLS_ATM_SUCCESS    ATM_SUCCES
#define ATM_SUCCES          1

/* MPOAAL5 Related defines */
#define  MPLS_MPOAAL5_SUCCESS  MPOAAL_SUCCESS
#define  MPLS_MPOAAL5_FAILURE  MPOAAL_FAILURE

#endif /*_MPLS_PORT_H */
