
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: labelmgr.h,v 1.6 2011/10/25 09:29:38 siva Exp $
 *              
 ********************************************************************/

/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : labelmgr.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS - Utility    
 *    MODULE NAME            : Inteface Module
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains typedefintions, constant
 *                             definitions, global declarations that are
 *                             associated with Label Space (Group-Key) Manager. 
 *---------------------------------------------------------------------------*/

#ifndef _LABELMGR_H
#define _LABELMGR_H

/* Definitions declared for memory pool related functions. */

#define LBL_MEM_MODE            MEM_DEFAULT_MEMORY_TYPE
#define LBL_MGR_ASSGN_INFO_HEAD_SIZE  (MAX_LBl_GRPS_SUPRTD * sizeof (tAsgnInfoHead))

/* Definitions for Bit Maps */
#define FIRST_BYTE_AVAIL_BMAP  0xFF000000
#define SECOND_BYTE_AVAIL_BMAP 0x00FF0000
#define THIRD_BYTE_AVAIL_BMAP  0x0000FF00
#define FOURTH_BYTE_AVAIL_BMAP 0x000000FF
#define KEY2_ALL_AVAIL_BMAP    0xFFFFFFFF
#define KEY2_ODD_AVAIL_BMAP    0xAAAAAAAA
#define KEY2_EVN_AVAIL_BMAP    0x55555555
#define KEY2_INFO_BLOCK_SIZE   32
#define BYTE_BLOCK_SIZE        8

#define FIRST_BYTE             0
#define SECOND_BYTE            1
#define THIRD_BYTE             2
#define FOURTH_BYTE            3

#define LBL_ZERO               0
#define LBL_ONE                1

/* Definitions for Semaphores in label manager */
#define LBL_SEM_INITIAL_COUNT  1

/*
 * Typedefinitions used for the Group Key Manager.
 */

typedef struct _tAsgnKey2Info
{
   UINT4                  u4AsgnKey2Bmap;
   struct _tAsgnKey2Info* pNxtAsgnKey2Info;
}tAsgnKey2Info;

typedef struct _tAsgnKey1Info
{
   tTMO_SLL_NODE  Key1SllNode;
   UINT4          u4AsgnInfoKey1Val;  /* Key1 value of the group */
   UINT4          u4AsgnInfoKey2Min;  /* Key2 Min value of the group */
   UINT4          u4AsgnInfoKey2Max;  /* Key2 Max value of the group */
   UINT4          u4MaxKey2Avail;     /* Max num of Key2 vals that can be 
                                         assgnd.*/
   UINT2          u2NumInfoKey2Asgn;  /* Num of Key2 vals assgnd so far.  */
   UINT2          u2AvailKey2Offset;  /* Current offset value */
   tAsgnKey2Info* pAsgnKey2Info;      /* Ptr to the first key2 Info Structure */
   tAsgnKey2Info* pAvailAsgnKey2Info; /* Ptr to the current key2 Info Strctre */
}tAsgnKey1Info;

typedef struct _tAsgnInfoHead
{
   UINT1          u1Status;           /* Status of this strucuture */
   UINT1          u1AllocOrder;       /* Order (ie Even/Odd) alloc of labels */
   UINT2          u2AsgnInfoGrpId;    /* Group id of the group */
   tAsgnKey1Info* pAvailAsgnKey1Info; /* Ptr to the key1 Info Strctre from which
                                       * label can be assigned.*/
   UINT4          u4InterfaceIndex;   /*                                       */
   UINT4   u4ModuleId;         /* To identify the Module Owner for a label 
      group. */
   
   UINT4   u4AsgnCount;        /* Number of label spaces created so far */
   tTMO_SLL       Key1InfoList;       /* Singly linkd list of Key1 info ptrs. */
}tAsgnInfoHead;


/* This structure is added for system sizing.
 * It should not be used for any other purpose
 */

typedef struct _AsgnInfoHeadSize
{
    UINT1         au1AsgnInfoHead[LBL_MGR_ASSGN_INFO_HEAD_SIZE];
}
tAsgnInfoHeadSize;

typedef struct _tLblMgrInfo
{
   UINT4          u4NumGrpsActive;
   UINT4          u4MaxLblGrpsSprtd;
   UINT4          u4MaxKey1GrpsPerLblGrpSprtd;
   UINT4          u4MaxKey2GrpsPerKey1GrpSprtd;
   UINT4          u4DbgLvl;
   tOsixSemId     SemId;
   tAsgnInfoHead* pAsgnInfoHead;
}tLblMgrInfo;


/* 
 * Macros for accessing the fields of the label manager 
 */

#define LBL_MGR_ASGN_INFO_HEAD(x) \
    x->pAsgnInfoHead

#define LBL_MGR_KEY1_INFO_POOL_ID \
    LBLMGRMemPoolIds[MAX_LBLMGR_LBL_MGR_KEY1_INFO_SIZING_ID]  

#define LBL_MGR_KEY2_INFO_POOL_ID \
    LBLMGRMemPoolIds[MAX_LBLMGR_LBL_MGR_KEY2_INFO_SIZING_ID] 

#define LBL_SPACE_GRPS_POOL_ID \
    LBLMGRMemPoolIds[MAX_LBLMGR_LBL_SPACE_GRP_SIZING_ID]

#define LBL_MGR_NUM_MAX_LBL_GRPS(x) \
    x->u4MaxLblGrpsSprtd

#define LBL_MGR_NUM_MAX_KEY1_GRPS(x) \
    x->u4MaxKey1GrpsPerLblGrpSprtd

#define LBL_MGR_NUM_MAX_KEY2_GRPS(x) \
    x->u4MaxKey2GrpsPerKey1GrpSprtd

#define LBL_MGR_NUM_LBLGRPS_CUR_SPRTD(x) \
    x->u4NumGrpsActive

#define LBL_MGR_SEM(x)\
    (tOsixSemId*)(&(x->SemId))

#define  lblMgrInitMemConstants(pLblMgrInfo) \
   LBL_MGR_NUM_MAX_LBL_GRPS(pLblMgrInfo)  = MAX_LBl_GRPS_SUPRTD;\
   LBL_MGR_NUM_MAX_KEY1_GRPS(pLblMgrInfo) = MAX_KEY1_GRP_PER_LBL_GRP_SPRTD;\
   LBL_MGR_NUM_MAX_KEY2_GRPS(pLblMgrInfo) = MAX_KEY2_GRP_PER_KEY2_GRP_SPRTD;\
   LBLMGR_DBG_FLAG = (UINT4)(~(LBLMGR_DBG_ALL))

/* Function prototypes */

UINT4 LblMgrLabelExactMatch
 ARG_LIST ((tAsgnInfoHead *pAsgnInfoHead, 
    UINT4 u4Key1Val, 
    UINT4 u4Key2Min ,
        UINT4 u4Key2Max));

UINT4 LblMgrLabelOverlapCheck
 ARG_LIST ((tAsgnInfoHead *pAsgnInfoHead, 
    UINT4 u4Key1Val, 
    UINT4 u4Key2Min ,
        UINT4 u4Key2Max));

UINT1 LblMgrUpdateAvailKey2Info 
      ARG_LIST ((tAsgnKey1Info* pAvailAsgnKey1Info));

VOID LblMgrGetUniqueKeys 
     ARG_LIST ((tAsgnInfoHead* pAsgnInfoHead, 
                UINT4* pu4Key1, 
                UINT4* pu4Key2,
                UINT1  u1RetType));

UINT1 LblMgrChkDistinctLblRngsInLblGroup 
      ARG_LIST ((tKeyInfoStruct* pKeyInfo,
                 UINT2           u2NumKeyInfo));



/* Macros used in LDP Debugging */
#define    LBLMGR_DBG_FLAG      gLblMgrInfo.u4DbgLvl
#define    LBLMGR_MEM           0x00000001
#define    LBLMGR_LBL           0x00000002
#define    LBLMGR_DBG_ALL       0x00000003

#define    LBL_SEM_NAME         "LBLS"

#define LBLMGR_DBG(u4Value, pu1Format)     \
          UtlTrcLog  (LBLMGR_DBG_FLAG, u4Value, "LBLMGR", pu1Format)

#define LBLMGR_DBG2(u4Value, pu1Format, Arg1, Arg2)     \
          UtlTrcLog  (LBLMGR_DBG_FLAG, u4Value, "LBLMGR", pu1Format, Arg1, Arg2)

/* Reference of SRM MEM Function */


#endif /*_LABELMGR_H */
/*---------------------------------------------------------------------------*/
/*                        End of file labelmgr.h                             */
/*---------------------------------------------------------------------------*/
