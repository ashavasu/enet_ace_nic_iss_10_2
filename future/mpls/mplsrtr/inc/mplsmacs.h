/********************************************************************
 * $Id: mplsmacs.h,v 1.16 2014/08/25 12:19:57 siva Exp $
*******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : mplsmacs.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : MPLS-FM
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains porting related macros 
 *---------------------------------------------------------------------------*/

#ifndef _MPLS_MACS_H
#define _MPLS_MACS_H

#define MPLS_IF_TABLE_LIST(x) \
              (&(gMplsIncarn[x].IfTableList))
#define MPLS_VC_TABLE_LIST(x) \
              (&(gMplsIncarn[x].VcTableList))

#define MPLS_TNL_INFO_TABLE(x) \
              (gMplsIncarn[(x)].aTnlInfo)
#define MPLS_TNL_ENTRY(u4Incarn, u2TnlId, u2TnlInstance) \
              (gMplsIncarn[(u4Incarn)].aTnlInfo[(u2TnlId)][(u2TnlInstance)])
#define TNL_GET_FTN_LINKS_TO_NHLFE(pTnlInfo)  \
              (&((pTnlInfo)->pNhlfe->FtnLinksList))
#define TNL_NH_OPER_STATUS(pTnlInfo) \
                 ((pTnlInfo)->pNhlfe->u1OperStatus)
#define FTN_GET_FTN_LINKS_TO_NHLFE(pFtn)  \
              (&((pFtn)->pNhlfe->FtnLinksList))
#define FTN_FECTYPE(pFtn) \
              ((pFtn)->FecParams.u1FecType)
#define FTN_TNLID(pFtn) \
              ((pFtn)->FecParams.u2TnlId)
#define IS_FECTYPE_TNL(u1FecType) \
          (( ((u1FecType) == MPLS_FEC_CRLSP_TYPE) || \
          ((u1FecType) == MPLS_FEC_RSVP_TYPE) || \
          ((u1FecType) == MPLS_FEC_SNMP_TNL_TYPE)) ? MPLS_TRUE: MPLS_FALSE)
#define FTN_TRFCPARMS(pFtn) \
              ((pFtn)->FecParams.MplsTrfcParms)
#define ILM_TRFCPARMS(pIlm) \
              ((pIlm)->FecParams.MplsTrfcParms)

#define ILM_NH_OUT_IFENRY(pIlm) \
              ((pIlm)->pNhlfe->pOutIfEntry)

#define MPLS_TOS_VAL(u4Incarn, u1Index)\
       (gMplsIncarn[u4Incarn].au1TosVals[u1Index])

#define MPLS_GET_IP_SERVICE_TYPE(pBuf, pu1ServiceType)\
             MPLS_COPY_FROM_BUF_CHAIN (pBuf, pu1ServiceType, \
                                       MPLS_IP_TOS_OFFSET,\
                                       MPLS_UINT1_SIZE)
#define MPLS_GET_TOS_FROM_SERVICE_TYPE(u1ServiceType) \
             (((u1ServiceType) & MPLS_TOS_DTR_MASK) >> 2)

#define MPLS_GET_HL_PROTID_FROM_IPPKT(pBuf, pu1HlProtId)\
             MPLS_COPY_FROM_BUF_CHAIN (pBuf, (UINT1 *)pu1HlProtId, \
                                       MPLS_HL_PROTID_OFFSET,\
                                       MPLS_UINT1_SIZE)

#define MPLS_TNL_NHLFE_ENTRY(u4Incarn, u2TnlId, u2TnlInstance) \
              (((tMplsTnlInfo *)(gMplsIncarn[(u4Incarn)].aTnlInfo[(u2TnlId)][(u2TnlInstance)]))->pNhlfe)

#define MPLS_GET_DEST_IP_ADDR(pBuf, pAddr)\
             MPLS_COPY_FROM_BUF_CHAIN (pBuf, (UINT1 *)pAddr, \
                                       MPLS_IP_DEST_ADDR_OFFSET,\
                                       MPLS_IP_ADDR_OCTETS)

#define MPLS_REL_MEM_ILM \
             mplsRelMemILM

#define MPLS_REL_MEM_FTN \
             mplsRelMemFTN

#define MPLS_REL_MEM_TNL \
             mplsRelMemTnl

#define MPLS_REL_MEM_ATM_INFO \
             mplsRelAtmInfo

#define MPLS_REL_MEM_MAC_INFO \
             mplsRelMacInfo

#define MPLS_REL_MEM_NHLFE \
             mplsRelMemNhlfe

#define MPLS_VC_TBL_POOL_ID                  LANAIMemPoolIds[MAX_LANAI_VC_TABLE_ENTRIES_SIZING_ID]
#define MPLS_ATM_INFO_POOL_ID              LANAIMemPoolIds[MAX_LANAI_ATM_INFO_SIZING_ID]
#define MPLS_IP_ROUTE_INFO_POOL_ID           MPLSRTRMemPoolIds[MAX_MPLSRTR_RT_CHG_ENTRIES_SIZING_ID]
#define MPLS_IP_IF_INFO_POOL_ID              MPLSRTRMemPoolIds[MAX_MPLSRTR_IF_CHG_ENTRIES_SIZING_ID]
#define MPLS_REG_APPS_POOL_ID                MPLSRTRMemPoolIds[MAX_MPLSRTR_MPLS_APPLICATIONS_SIZING_ID]
#define MPLS_BFD_REQ_PARAMS_POOL_ID          MPLSRTRMemPoolIds[MAX_MPLSRTR_BFD_REQ_PARAMS_SIZING_ID]
#define MPLS_API_IN_INFO_POOL_ID             MPLSRTRMemPoolIds[MAX_MPLSRTR_MPLS_API_IN_INFO_SIZING_ID]
#define MPLS_API_OUT_INFO_POOL_ID            MPLSRTRMemPoolIds[MAX_MPLSRTR_MPLS_API_OUT_INFO_SIZING_ID]
#define MPLS_EVENT_NOTIF_POOL_ID             MPLSRTRMemPoolIds[MAX_MPLSRTR_MPLS_EVENT_NOTIF_SIZING_ID]
#define MPLS_OAM_FSMPLS_TP_ME_TABLE_POOL_ID  MPLSRTRMemPoolIds[MAX_MPLSRTR_OAM_FSMPLS_TP_ME_TABLE_SIZING_ID]
#define MPLS_CFA_ENET_MTU_POOL_ID            MPLSRTRMemPoolIds[MAX_CFA_ENET_MTU_SIZING_ID]
#define MPLS_TMR_LIST_ID(x) \
              (gMplsIncarn[x].TmrListId)

#define MPLS_TIMER_DESC_FUNC(x, y) \
                  (gMplsIncarn[x].aMplsTmrDesc[y]).TmrExpFn

#define MPLS_TIMER_DESC_OFFSET(x, y) \
                  (gMplsIncarn[x].aMplsTmrDesc[y]).i2Offset

#define MPLS_FTN_ARP_TMR_BLK(x) \
               (gMplsIncarn[x].FtnArpTmrBlk)

#define MPLS_ILM_ARP_TMR_BLK(x) \
               (gMplsIncarn[x].IlmArpTmrBlk)

#define MPLS_TNL_ARP_TMR_BLK(x) \
               (gMplsIncarn[x].TnlArpTmrBlk)

#define MPLS_PW_ARP_TMR_BLK(x) \
               (gMplsIncarn[x].PwArpTmrBlk)

#define MPLS_RPTE_GR_MAX_WAIT_TMR_BLK(x) \
               (gMplsIncarn[x].RpteMaxWaitTmrBlk)

#define MPLS_LDP_GR_MAX_WAIT_TMR_BLK(x) \
               (gMplsIncarn[x].LdpMaxWaitTmrBlk)

#ifdef MPLS_L3VPN_WANTED
#define MPLS_L3VPN_ARP_TMR_BLK(x) \
               (gMplsIncarn[x].L3VpnEgressMapArpTmrBlk)
#endif

#define NHLFE_GET_IFTYPE(pNhlfe) \
            ((pNhlfe)->pOutIfEntry->u2IfType)

#define MPLS_ADMIN_STATUS(x) \
              (gMplsIncarn[x].CfgVarGroup.u1AdminStatus)

#define MPLS_FEC_TYPE(x) \
              (gMplsIncarn[x].CfgVarGroup.u1FecType)
#define MPLS_VC_TABLE_ENTRIES(x) \
              (gMplsIncarn[x].CfgVarGroup.u4VcTableEntries)
#define MPLS_IF_TABLE_ENTRIES(x) \
              (gMplsIncarn[x].CfgVarGroup.u4IfTableEntries)
#define MPLS_FTN_ENTRIES(x) \
              (gMplsIncarn[x].CfgVarGroup.u4FTNEntries)

#define MPLS_TNL_ENTRIES(x) \
              (gMplsIncarn[x].CfgVarGroup.u4TnlEntries)
#define MPLS_QOS_POLICY(x) \
              (gMplsIncarn[x].u1QosPolicy)
#define MPLS_TOSVALS_ARR(x) \
              (gaQosPolicies[(x)].au1TosVals)
#define MPLS_TOSVALS_COUNT(x) \
              (gaQosPolicies[(x)].u1TosValCount)

#define MPLS_ILM_ENTRIES(x) \
              (gMplsIncarn[x].CfgVarGroup.u4ILMEntries)
#define MPLS_MAC_INFO_ENTRIES(x) \
              (gMplsIncarn[x].CfgVarGroup.u4MacInfoEntries)
#define MPLS_ATM_INFO_ENTRIES(x) \
              (gMplsIncarn[x].CfgVarGroup.u4AtmInfoEntries)
#define MPLS_FEC_PARAMS_ENTRIES(x) \
              (gMplsIncarn[x].CfgVarGroup.u4FecParamsEntries)
#define MPLS_NHLFE_ENTRIES(x) \
              (gMplsIncarn[x].CfgVarGroup.u4NHLFEEntries)

#define MPLS_NHLFE_ENTRY(u4IncarnId, u4NhlfeId) \
          (gMplsIncarn[u4IncarnId].ppaNhlfeInfoTable[u4NhlfeId])

#define MPLS_NHLFE_TOKEN_STACK_TOP(u4IncarnId) \
          (gMplsIncarn[u4IncarnId].u4NhlfeInfoTokenStackTop)

#define MPLS_NHLFE_TOKEN_STACK(u4IncarnId, u4Top) \
          (gMplsIncarn[u4IncarnId].pau4NhlfeInfoTokenStack[u4Top])

#define MPLS_ROUTER_ID(u4IncarnId) \
          (gMplsIncarn[u4IncarnId].CfgVarGroup.RouterId)

#define MPLS_ACH_PSC_CHNL_CODE(u4IncarnId) \
          (gMplsIncarn[u4IncarnId].CfgVarGroup.u2PscAchChnlType)

#define MPLS_RPTE_GR_MAX_WAIT_TIME(u4IncarnId) \
          (gMplsIncarn[u4IncarnId].u2RpteMaxWaitTimer)


    /* Macro for getting the link type. x is pu1MacAddr */
#define MPLS_GET_LINK_PKT_TYPE(x)  \
   ((*((UINT4 *)x) == 0xffffffff) && (*((UINT2 *)x + MPLS_TWO) == 0xffff)) ?  \
              MPLS_LINK_BCAST : \
                 ((x[MPLS_ZERO] & 0x01) ? MPLS_LINK_MCAST : MPLS_LINK_UCAST)

#define MPLS_CALC_CHKSUM(u2ChkSum, u1IpTtl, u1LblTtl) \
{    \
   UINT4 u4Sum = ((u2ChkSum) + (((u1IpTtl) - (u1LblTtl)) << 8));  \
   u2ChkSum = (UINT2) (u4Sum + (u4Sum >> 16)) ;  \
}    \


#define MPLS_COMPUTE_HASH_INDEX(u4TnlId)  \
             ((((u4TnlId) >> 24) ^ (((u4TnlId) & 0xFF0000) >> 16) ^ \
               (((u4TnlId) & 0xFF00) >> 8) ^ \
               ((u4TnlId) & 0xFF)) % MPLS_HASH_SIZE)

#define MPLS_GET_NEXT_VALID_TOS(u1InTos) \
            (u1InTos ? (u1InTos * 2) : 1)    /* Valid Tos values are 0, 1, 2, 4 and
                                               8 as per RFC 1349. */
 /* Macros for accessing Info from tLspInfo */
#define LSPINFO_FECTYPE(pLspInfo)    ((pLspInfo)->FecParams.u1FecType)
#define LSPINFO_TOS(pLspInfo)        ((pLspInfo)->FecParams.u1Tos)
#define LSPINFO_NETMASK(pLspInfo)    ((pLspInfo)->FecParams.u4DestMask)
#define LSPINFO_ADDR(pLspInfo)       ((pLspInfo)->FecParams.u4DestAddrPrefix)
#define LSPINFO_SRCADDR(pLspInfo)    ((pLspInfo)->FecParams.u4SrcAddr)
#define LSPINFO_TNLID(pLspInfo)      ((pLspInfo)->FecParams.u2TnlId)
#define LSPINFO_INGRESSID(pLspInfo)  ((pLspInfo)->FecParams.u4IngressId)
#define LSPINFO_TNLINSTANCE(pLspInfo)  ((pLspInfo)->FecParams.u2TnlInstance)
#define LSPINFO_TRFCPARMS(pLspInfo)  ((pLspInfo)->FecParams.MplsTrfcParms)

#define FTN_DEST_ADDR(pFtn)          ((pFtn)->FecParams.u4DestAddrPrefix)
#define FTN_DEST_MASK(pFtn)          ((pFtn)->FecParams.u4DestMask)
#define FTN_GET_TOS(pFtn)            ((pFtn)->FecParams.u1Tos)
#define FTN_SRC_ADDR(pFtn)           ((pFtn)->FecParams.u4SrcAddr)
#define FTN_OUT_IFINDEX(pFtn)        ((pFtn)->pNhlfe->u4OutIfIndex)
#define FTN_OUT_IFENTRY(pFtn)        ((pFtn)->pNhlfe->pOutIfEntry)
#define FTN_LBL_OPER(pFtn)           ((pFtn)->pNhlfe->u1Operation)
#define FTN_OPER_PUSHES(pFtn)        ((pFtn)->pNhlfe->u1Pushes)
#define FTN_OUT_LBL(pFtn)            ((pFtn)->pNhlfe->u4OutLabel)
#define FTN_CONN_HANDLE(pFtn)        ((pFtn)->pNhlfe->u4ConnHandle)
#define FTN_NH_IPADDR(pFtn)          ((pFtn)->pNhlfe->u4NextHopAddr)
#define FTN_NH_OPERSTATUS(pFtn)      ((pFtn)->pNhlfe->u1OperStatus)
#define FTN_TTL_DECR(pFtn)           ((pFtn)->pNhlfe->u1NumDecTtl)
#define FTN_TNL_INSTANCE(pFtn)       ((pFtn)->FecParams.u2TnlInstance)
#define FTN_STATUS(pFtn)             ((pFtn)->u1FtnStatus)


#define MPLS_MIN(a,b)             (a < b ? a : b)
#define MPLS_MAX(a,b)             (a > b ? a : b)

#define MPLS_GET_TTL(pBuf) CRU_BUF_Get_U4Reserved3(pBuf)
#define MPLS_SAVE_TTL(pBuf,ttl) CRU_BUF_Set_U4Reserved3(pBuf,ttl)
#endif /*_MPLS_MACS_H */
