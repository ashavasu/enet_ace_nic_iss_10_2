
/********************************************************************
 *                                                                  *
 * $RCSfile: diffprot.h,v $
 *                                                                  *
 * $Date: 2010/09/24 06:29:25 $                                     
 *                                                                  *
 * $Revision: 1.4 $                                                
 *                                                                  *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : diffprot.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : MPLS-FM
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file DiffServ related prototypes
 *-----------------------------------------------------------------------------
 */

#ifndef DIFF_PROT_H
#define DIFF_PROT_H

UINT4
MplsInitDiffServTables ARG_LIST((UINT4 u4IncarnId));
UINT4
MplsShutdownDiffServTables ARG_LIST((UINT4 u4IncarnId));
UINT4
MplsAllocMemDiffServParams ARG_LIST((UINT4 u4Incarn, tNHLFE * pNhlfe));
UINT4
MplsRelMemDiffServParams ARG_LIST((UINT4 u4Incarn, tNHLFE * pNhlfe));
UINT4
mplsRelMemSigExpMap ARG_LIST((UINT4 u4Incarn, tMplsDiffServParams * pDiffServParams));
UINT4
mplsDiffServProcGetDscpFromExp ARG_LIST((UINT4 u4Incarn, tNHLFE * pNhlfe,
                                UINT1 u1Exp, UINT1 *pu1Dscp));
UINT4
mplsDiffServProcGetLabelledPktDscps ARG_LIST((UINT4 u4Incarn, tNHLFE * pNhlfe,
                                     UINT1 u1Exp, tMplsDscpRecord * pDscpRecord));
UINT4
mplsDiffServProcSetEgresToIpTos ARG_LIST((UINT4 u4Incarn,
                                 tMplsBufChainHeader * pBuf,
                                 tMplsDscpRecord * pDscpRecord));
UINT4
mplsDiffServProcGetDscpFromMap ARG_LIST((tElspPhbMapEntry * paExpMapArray,
                                UINT1 u1Exp, UINT1 *pu1Dscp));
UINT4
mplsDiffServProcGetExpFromDscp ARG_LIST((UINT4 u4Incarn, tNHLFE * pNhlfe,
                                UINT1 u1Dscp, UINT1 *pu1Exp));
UINT4 
MplsDiffServValidatePhbDscp ARG_LIST((UINT1 u1PhbDscp));
UINT4 
MplsDiffServValidatePscDscp ARG_LIST((UINT1 u1PscDscp));

#endif /* DIFF_PROT_H */
