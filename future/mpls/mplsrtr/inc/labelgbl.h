
/********************************************************************
 *                                                                  *
 * $RCSfile: labelgbl.h,v $                                          
 *                                                                  *
 * $Date: 2007/02/01 14:57:08 $
 *                                                                  *
 * $Revision: 1.2 $                                             
 *                                                                  *
 ********************************************************************/

/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : labelgbl.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS - Utility    
 *    MODULE NAME            : Inteface Module
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains global declarations that are
 *                             associated with Label Space (Group-Key) Manager. 
 *---------------------------------------------------------------------------*/

#ifndef _LABELGBL_H
#define _LABELGBL_H

tLblMgrInfo gLblMgrInfo;

/* Global Variables used by Label Resource Manager */
/* Global Array Variable initialised with Key available indication bit maps */

UINT4 aAvlBmap[] = {
   0x80000000, 0x40000000, 0x20000000, 0x10000000,
   0x08000000, 0x04000000, 0x02000000, 0x01000000,
   0x00800000, 0x00400000, 0x00200000, 0x00100000,
   0x00080000, 0x00040000, 0x00020000, 0x00010000,
   0x00008000, 0x00004000, 0x00002000, 0x00001000,
   0x00000800, 0x00000400, 0x00000200, 0x00000100,
   0x00000080, 0x00000040, 0x00000020, 0x00000010,
   0x00000008, 0x00000004, 0x00000002, 0x00000001
};

UINT4 aAsgnBmap[] = {
   0x7FFFFFFF, 0xBFFFFFFF, 0xDFFFFFFF, 0xEFFFFFFF,
   0xF7FFFFFF, 0xFBFFFFFF, 0xFDFFFFFF, 0xFEFFFFFF,
   0xFF7FFFFF, 0xFFBFFFFF, 0xFFDFFFFF, 0xFFEFFFFF,
   0xFFF7FFFF, 0xFFFBFFFF, 0xFFFDFFFF, 0xFFFEFFFF,
   0xFFFF7FFF, 0xFFFFBFFF, 0xFFFFDFFF, 0xFFFFEFFF,
   0xFFFFF7FF, 0xFFFFFBFF, 0xFFFFFDFF, 0xFFFFFEFF,
   0xFFFFFF7F, 0xFFFFFFBF, 0xFFFFFFDF, 0xFFFFFFEF,
   0xFFFFFFF7, 0xFFFFFFFB, 0xFFFFFFFD, 0xFFFFFFFE
};

#endif /*_LABELGBL_H */
/*---------------------------------------------------------------------------*/
/*                        End of file labelgbl.h                             */
/*---------------------------------------------------------------------------*/
