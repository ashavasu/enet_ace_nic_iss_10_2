
/********************************************************************
 *                                                                  *
 * $RCSfile: mplsglob.h,v $
 *                                                                  *
 * $Id: mplsglob.h,v 1.15 2013/03/21 12:16:40 siva Exp $                                     
 *                                                                  *
 * $Revision: 1.15 $                                                 
 *                                                                  *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : mplsglob.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : MPLS-FM
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains global declarations
 *---------------------------------------------------------------------------*/

#ifndef MPLS_GLOB_H
#define MPLS_GLOB_H
#ifdef MPLS_MAIN_C

tMplsGlobal         gMplsIncarn[MPLS_MAX_INCARNS];

tQosPolicy          gaQosPolicies[MPLS_MAX_QOS_POLICIES];    /* Set of Qos Policies,suprtd */

tOsixQId            gFmInQId;

tOsixQId            gFmRtrChgQId;

tOsixQId            gFmIfChgQId;

tOsixQId            gFmFrrQId;

tOsixSemId          gConfSemId;

tOsixSemId          gDsSemId;

tOsixTaskId         gFmTaskId;

INT1                gu1MplsInitialised;

UINT4               gu4MplsQueDepth;

/* Denotes Diff-Serv capability of the Router
 * This variable corresponds to fsMplsDsTeStatus object of
 * fsmpls MIB */
INT4                gi4MplsDsTeStatus;

/* Denotes the Failure condition needs to be simulated.
 * This variable corresponds to the TEST object of 
 * fsmpls MIB */
INT4         gi4MplsSimulateFailure;

/* FM_TRACE - DEBUG Added */
UINT4               gu4MplsDbg;

UINT2               gu2GenLblSpaceGrpId;

/* Ach Channel Types */
/*Specifies the Continuity Check for RAW Bfd */
UINT2 gu2MplsAchChnlTypeCcBfd  = MPLS_ACH_CHANNEL_DEF_CC_BFD;
/*Specifies the Continuity Verification for RAW Bfd */
UINT2 gu2MplsAchChnlTypeCvBfd  = MPLS_ACH_CHANNEL_DEF_CV_BFD;
/*Specifies the Continuity Check for MPLS with Ipv4 */
UINT2 gu2MplsAchChnlTypeCcIpv4 = MPLS_ACH_CHANNEL_DEF_CC_IPV4;
/*Specifies the Continuity Verification for MPLS with Ipv4 */
UINT2 gu2MplsAchChnlTypeCvIpv4 = MPLS_ACH_CHANNEL_DEF_CV_IPV4;
/*Specifies the Continuity Check for MPLS with Ipv6 */
UINT2 gu2MplsAchChnlTypeCcIpv6 = MPLS_ACH_CHANNEL_DEF_CC_IPV6;
/*Specifies the Continuity Verification for MPLS with Ipv6 */
UINT2 gu2MplsAchChnlTypeCvIpv6 = MPLS_ACH_CHANNEL_DEF_CV_IPV6;

#else

extern tMplsGlobal  gMplsIncarn[];

extern tQosPolicy   gaQosPolicies[];

extern tOsixSemId   gConfSemId;

extern tOsixSemId   gDsSemId;

extern tOsixQId     gFmInQId;

extern tOsixQId     gFmRtrChgQId;

extern tOsixQId     gFmIfChgQId;

extern tOsixQId     gFmFrrQId;

extern tOsixTaskId  gFmTaskId;


/* Denotes Diff-Serv capability of the Router
 * This variable corresponds to fsMplsDsTeStatus object of
 * fsmpls MIB */
extern INT4         gi4MplsDsTeStatus;

/* FM_TRACE - DEBUG Added */
extern UINT4        gu4MplsDbg;

extern INT1         gu1MplsInitialised;

extern INT4         gi4MplsSimulateFailure;

extern UINT2        gu2GenLblSpaceGrpId;

PUBLIC UINT2 gu2MplsAchChnlTypeCcBfd;
PUBLIC UINT2 gu2MplsAchChnlTypeCvBfd;
PUBLIC UINT2 gu2MplsAchChnlTypeCcIpv4;
PUBLIC UINT2 gu2MplsAchChnlTypeCvIpv4;
PUBLIC UINT2 gu2MplsAchChnlTypeCcIpv6;
PUBLIC UINT2 gu2MplsAchChnlTypeCvIpv6;
#endif /* if MPLS_MAIN_C */

#endif /*_MPLS_GLOB_H */
