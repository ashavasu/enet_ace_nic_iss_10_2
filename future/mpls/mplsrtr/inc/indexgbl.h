
/********************************************************************
 *                                                                  *
 * $Id: indexgbl.h,v 1.14 2016/07/28 07:47:36 siva Exp $
 *                                                                  *
 ********************************************************************/

/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *---------------------------------------------------------------------------
 *    FILE  NAME             : indexgbl.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS - Utility    
 *    MODULE NAME            : L2Vpn Index Tbl Mgr : Inteface Module
 *    LANGUAGE               : ANSI-C
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains global declarations that 
 *                             are associated with L2Vpn Index Table Manager
 *                             
 *-------------------------------------------------------------------------*/

#ifndef _INDEXGBL_H
#define _INDEXGBL_H

/* Global ptr to the table of index groups */
tIndexMgrGrpInfo *gpIndexMgrGrpInfo = NULL;

/* Global Pool Id for Memory Pool of bitmap chunks */
#define CHUNK_TBL_POOL_ID  INDEXMGRMemPoolIds[MAX_INDEXMGR_INDEX_TBL_CHUNK_SIZING_ID]
#define GROUP_INFO_POOL_ID INDEXMGRMemPoolIds[MAX_INDEXMGR_INDEX_MGR_GRP_SIZING_ID]
#define CHUNK_INFO_POOL_ID INDEXMGRMemPoolIds[MAX_INDEXMGR_INDEX_MGR_CHUNK_SIZING_ID]
/* Index manager Global Semaphore */
tOsixSemId   gIndexMgrSemId;

/* ==== Start : To add a group, define it & append it to the array ==== */
/* The foll. 3 constants need to be configured as per requirement */



/* Define new entries here... */

UINT4 au4GrpMaxIndices[] = {
                          MAX_L2VPN_PW_VC_ENTRIES,        /* Max Indices for Grp 1 */ 
                          MAX_MPLSDB_LSR_INSEGMENT,       /* Max Indices for Grp 2 */
                          MAX_MPLSDB_LSR_OUTSEGMENT,      /* Max Indices for Grp 3 */
                          MAX_MPLSDB_LSR_XCENTRY,         /* Max Indices for Grp 4 */
                    MAX_MPLSDB_LSR_LBLSTKENTRY,     /* Max Indices for Grp 5 */
                          MAX_MPLSDB_FTN_ENTRY,           /* Max Indices for Grp 6 */
                          MAX_LDP_ENTITIES,               /* Max Indices for Grp 7 */
                          MAX_LDP_LSP_UPSTR_LCB,          /* Max Indices for Grp 8 */
                          MAX_L2VPN_VPN_ENTRIES,          /* Max Indices for Grp 9 */
                          MAX_MPLSOAM_FSMPLSTPMEGTABLE,   /* Max Indices for Grp 10 */
                          MAX_L2VPN_ENET_ENTRIES          /* Max Indices for Grp 11 */
                
                         /* Append newly defined entries here, in this array */
                         /* Max Groups supported is MAXUINT1 = 255*/
      
      /* Last one is same as Max Indices for Grp No. INDEXMGR_MAX_GRPS_SPRTD */
                         };

tIndexMgrGrpTable IndexMgrGrpEntry[] = {
    { "L2VPN", "MAX_L2VPN_PW_VC_ENTRIES" },
    { "MPLSDB", "MAX_MPLSDB_LSR_INSEGMENT" },
    { "MPLSDB", "MAX_MPLSDB_LSR_OUTSEGMENT" },
    { "MPLSDB", "MAX_MPLSDB_LSR_XCENTRY" },
    { "MPLSDB", "MAX_MPLSDB_LSR_LBLSTKENTRY" },
    { "MPLSDB", "MAX_MPLSDB_FTN_ENTRY" },
    { "LDP", "MAX_LDP_ENTITIES" },
    { "LDP", "MAX_LDP_LSP_UPSTR_LCB" },
    /* The value MAX_L2VPN_VPN_ENTRIES is equivalent to MAX_L2VPN_PW_VC_ENTRIES
     * MAX_L2VPN_VPN_ENTRIES will not be present in sizing file of L2VPN as per.
     * design. So, using the value MAX_L2VPN_PW_VC_ENTRIES for the below mapping. 
     */
    { "L2VPN", "MAX_L2VPN_PW_VC_ENTRIES" },
    { "MPLSOAM", "MAX_MPLSOAM_FSMPLSTPMEGTABLE" },
    { "L2VPN", "MAX_L2VPN_ENET_ENTRIES"}
};

/* ==== End : To add a group, define it & append it to the array ====== */

/* Global Variables used by Index Resource Manager */
/* Global Array Variable initialised with Key available indication bit maps */

UINT4 aAvlByteBmap[] = {
    0xFF000000, 0x00FF0000, 0x0000FF00, 0x000000FF 
};

UINT4 aAvlBitmap[] = {
   0x80000000, 0x40000000, 0x20000000, 0x10000000,
   0x08000000, 0x04000000, 0x02000000, 0x01000000,
   0x00800000, 0x00400000, 0x00200000, 0x00100000,
   0x00080000, 0x00040000, 0x00020000, 0x00010000,
   0x00008000, 0x00004000, 0x00002000, 0x00001000,
   0x00000800, 0x00000400, 0x00000200, 0x00000100,
   0x00000080, 0x00000040, 0x00000020, 0x00000010,
   0x00000008, 0x00000004, 0x00000002, 0x00000001
};

UINT4 aAsgnBitmap[] = {
   0x7FFFFFFF, 0xBFFFFFFF, 0xDFFFFFFF, 0xEFFFFFFF,
   0xF7FFFFFF, 0xFBFFFFFF, 0xFDFFFFFF, 0xFEFFFFFF,
   0xFF7FFFFF, 0xFFBFFFFF, 0xFFDFFFFF, 0xFFEFFFFF,
   0xFFF7FFFF, 0xFFFBFFFF, 0xFFFDFFFF, 0xFFFEFFFF,
   0xFFFF7FFF, 0xFFFFBFFF, 0xFFFFDFFF, 0xFFFFEFFF,
   0xFFFFF7FF, 0xFFFFFBFF, 0xFFFFFDFF, 0xFFFFFEFF,
   0xFFFFFF7F, 0xFFFFFFBF, 0xFFFFFFDF, 0xFFFFFFEF,
   0xFFFFFFF7, 0xFFFFFFFB, 0xFFFFFFFD, 0xFFFFFFFE
};

#endif /*_INDEXGBL_H */

/*-------------------------------------------------------------------------
*                        End of file indexgbl.h                             
-------------------------------------------------------------------------*/
