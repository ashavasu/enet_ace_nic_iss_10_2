
/********************************************************************
 * $Id: mplsprot.h,v 1.22 2014/12/24 11:04:36 siva Exp $
*******************************************************************/
/********************************************************************
 *                                                                  *
 * $RCSfile: mplsprot.h,v $                                         
 *                                                                  *
 * $Date: 2014/12/24 11:04:36 $ 
 *                                                                  *
 * $Revision: 1.22 $                                                
 *                                                                  *
 ********************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : mplsprot.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS-FM   
 *    MODULE NAME            : 
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains prototypes used by MPLS-FM
 *---------------------------------------------------------------------------*/

#ifndef _MPLS_PROT_H
#define _MPLS_PROT_H

#include "teextrn.h"
/* Main sub module function prototypes */

/* Proto types of mplsmain.c file */
 

UINT1 MplsInit      ARG_LIST ((void));

VOID MplsDeInit ARG_LIST((VOID));
UINT1 MplsAllocateResources ARG_LIST ((UINT4 u4IncarnNum));

UINT1 MplsCreateMemPools ARG_LIST ((UINT4 u4IncarnNum));


UINT1 MplsRelResources ARG_LIST ((UINT4 u4IncarnNum));

UINT1 MplsDeleteMemPools ARG_LIST ((UINT4 u4IncarnNum));

UINT1 mplsInitTosValsForQosType ARG_LIST ((UINT4 u4Incarn));
UINT1 mplsGetTosFromIpHdr ARG_LIST ((UINT4 u4Incarn,
                                     tMplsBufChainHeader * pBuf,
                                     UINT1 *pu1Tos));
VOID MplsInitTosValsForStdQosPolicies ARG_LIST ((VOID));

/* End of mplsmain.c */

/* Process sub module function prototypes */
/* Proto types of mplsproc.c file */
UINT1 MplsProcessMain ARG_LIST ((UINT4 au4IfInfo[]));
UINT1 mplsUpdateIpHdrForTtlChg ARG_LIST ((tMplsBufChainHeader * pBuf,
                                          UINT1 u1LblTtl));

/* End of mplsproc.c */

/* Proto types of mplsmlib.c file */

UINT1 mplsGetNextTos ARG_LIST ((UINT4 u4Incarn, UINT1 u1InTos,
                                UINT1 *pu1NextTos));
/* End of mplsmlib.c */

/* Interface sub module function prototypes */

/* Proto types of mplsif.c file */
UINT1 mplsInitInterfaces ARG_LIST ((UINT4 u4Incarn));
UINT1 mplsCloseInterfaces ARG_LIST ((UINT4 u4Incarn));
UINT1 mplsGetIfMacAddr ARG_LIST ((UINT4 u4IfIndex, tMacAddr IfMacAddr));

UINT1 mplsGetIfIdFromIfIndex ARG_LIST ((UINT4 u4IfIndex, tIfId *pIfId));

UINT4 IpGetIfIndexFromIfId ARG_LIST ((tIP_INTERFACE * pIfId));

/* Proto types of mplsieth.c file */
UINT1 mplsRegWithEnet ARG_LIST ((UINT4 u4NtwPort, UINT4 u4IfIndex,
                                 INT4 (*FnEthEventHandler) (UINT4 u4Event,
                                                            UINT4 u4IfaceIndex,
                                                            tMplsBufChainHeader
                                                            * pBuf,
                                                            UINT4 u4Len)));

UINT1 mplsUnRegWithEnet ARG_LIST ((UINT4 u4IfIndex));

INT4 mplsEthEventHandler ARG_LIST ((UINT4 u4Event, UINT4 u4NtwIfNum,
                                    tMplsBufChainHeader * pBuf, UINT4 u4Len));

UINT1 mplsEnqPktToMplsFm ARG_LIST ((tMplsBufChainHeader * pBuf,
                                    UINT4 u4IfIndex));
/* End of mplsieth.c */

/* Proto types of mplsiatm.c file */
UINT1 mplsRxPktFromMpoaal5 ARG_LIST ((tMplsBufChainHeader * pBuf,
                                      UINT4 u4ConnHandle));
UINT1 mplsUpdateAtmXTable ARG_LIST ((UINT1 u1MergeSupport, UINT1 u1Operation,
                                     UINT4 u4InPort, UINT4 u4InLabel,
                                     UINT4 u4OutPort, UINT4 u4OutLabel));

/* End of mplsiatm.c */

/* Proto types of mplsildp.c file */
UINT1 MplsMlibUpdate ARG_LIST ((UINT2 u2MlibOperation, tLspInfo * pLspInfo));
/* End of mplsildp.c */

/* Proto types of mplsisnm.c file */
UINT1 MplsEnqSnmpEvent ARG_LIST ((UINT4 u4Incarn, UINT4 u4SnmpEvent));
/* End of mplsisnm.c */

void mplsEnqLblBindReq ARG_LIST ((UINT4 u4Event, UINT2 u2IncarnId,
                                  void *pMplsParms));

UINT1 mplsSendIpPktToIfMod ARG_LIST ((tMplsBufChainHeader * pBuf,
                                      UINT4 u4IfIndex, UINT1 u1LinkType));

UINT1 MplsEnqueueMplsIncomingQ ARG_LIST ((UINT4 au4IfInfo[]));

UINT1 mplsValidateTosValue ARG_LIST ((UINT4 u4Incarn, UINT1 u1Tos));

UINT1 mplsGetIPHdrRouteAlertOption ARG_LIST (( tMplsBufChainHeader *pBuf, 
                                               UINT1 *pu1RouteAlertOpSet));
UINT1 MplsCheckFmAdminStatus ARG_LIST ((UINT4 u4Incarn, UINT1 u1AdminStatus));



UINT4 MplsGetDiffServParamsIndexNext ARG_LIST ((UINT4 u4IncarnId, 
                                               UINT4 *pu4NextDiffParamsId));

UINT4 MplsGetDiffServElspMapIndexNext ARG_LIST ((UINT4 u4IncarnId, 
                                                UINT4 *pu4NextElspMapId));

INT1 MplsGetFTNActionType(UINT4, INT4 *pi4FtnActionType);

UINT4 MplsValidateIncarnId ARG_LIST((UINT4 u4IncarnId));


UINT4 MplsValidateElspMapIndex ARG_LIST((UINT4 u4IncarnId, UINT4 u4ElspMapId, 
                                        UINT1 u1Exp));

UINT4 MplsValidateDiffServParamsIndex ARG_LIST((UINT4 u4IncarnId, 
                                               UINT4 u4DiffParamsId));
INT4
MplsProcessGroupEvents ARG_LIST ((UINT4 au4IfInfo[]));
INT4
MplsProcessSnmpEvent ARG_LIST ((UINT4 au4IfInfo[]));

/*  End of  proto types of mplsutls.h */

/* Forwading related functions */
INT4
MplsProcessLabeledPacket ARG_LIST ((tCRU_BUF_CHAIN_HEADER * pBuf,
                                    UINT4 u4IfIndex, UINT4 u4PktSize));
INT4
MplsLabelAndSendIpPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4FtnIndex);

#ifdef MPLS_L3VPN_WANTED
INT4
MplsLabelAndSendIpPktForL3Vpn (tCRU_BUF_CHAIN_HEADER * pBuf, 
                              UINT4 XcIndex, UINT4 u4OutSegmentIndex);
#endif

UINT4 mplsValidateIfIndex ARG_LIST ((INT4 i4IfIndex));
UINT4 mplsIpIfGetAddr ARG_LIST ((UINT2 u2Port));
UINT1 mplsIpGetPortFromIfIndex ARG_LIST ((UINT2 u2IfIndex, UINT2 * pu2Port));
INT4 
MplsHandlePktFromRpteForByPassTnl (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4XcIndex);
VOID
MplsRouteChgEventHandle (tMplsRtEntryInfo *pRouteInfo);
VOID
MplsIfChgEventHandle (tMplsIfEntryInfo * pIfInfo);

VOID
MplsHandleIfChgForNonTeFtn (tMplsIfEntryInfo *pIfInfo);

UINT4
MplsIsDsTeEnabled (VOID);

UINT4
MplsIsClassTypePresent (UINT4 u4ClassType);

UINT4
MplsIsTeClassPresent (UINT4 u4ClassType, UINT4 u4Priority,UINT1 *pu1TeClassNum);

UINT4
MplsIsPHBConsistent (UINT4 u4ClassType, UINT4 u4PhbId);

UINT4
MplsIsPSCConsistent (UINT4 u4ClassType, UINT4 u4PscId);


INT1
MplsDsTeTestAllClassTypeTable (UINT4 *pu4ErrorCode, UINT4 u4ClassTypeIndex,
                                  tMplsDsTeClassType *pTestDsTeClassType,
                                  UINT4 u4ObjectId);

INT1
MplsDsTeGetAllClassTypeTable (UINT4 u4ClassTypeIndex,
                                tMplsDsTeClassType *pGetDsTeClassType);


INT1
MplsDsTeSetAllClassTypeTable (UINT4 u4ClassTypeIndex,
                                tMplsDsTeClassType *pSetDsTeClassType,
                                UINT4 u4ObjectId);

INT1
MplsDsTeTestAllClassTypeTcMapTable (UINT4 *pu4ErrorCode, UINT4 u4ClassTypeIndex,
                                      UINT4 u4ClassTcMapIndex,
                                  tMplsDsTeClassTypeTcMap *pSetDsTeClassTypeTcMap,
                                  UINT4 u4ObjectId);


INT1
MplsDsTeGetAllClassTypeTcMapTable (UINT4 u4ClassTypeIndex,
                                     UINT4 u4ClassTcMapIndex,
                                     tMplsDsTeClassTypeTcMap *pGetDsTeClassTypeTcMap);

INT1
MplsDsTeSetAllClassTypeTcMapTable (UINT4 u4ClassTypeIndex,
                                     UINT4 u4ClassTcMapIndex,
                                tMplsDsTeClassTypeTcMap *pSetDsTeClassTypeTcMap,
                                UINT4 u4ObjectId);

INT1
MplsDsTeTestAllTeclassMapTable (UINT4 *pu4ErrorCode, UINT4 u4ClassTypeIndex,
                                      UINT4 u4ClassTeClassPrio,
                                  tMplsDsTeClassPrio *pSetDsTeClassPrio,
                                  UINT4 u4ObjectId);


INT1
MplsDsTeSetAllTeclassMapTable (UINT4 u4ClassTypeIndex,
                                  UINT4 u4ClassTeClassPrio,
                                  tMplsDsTeClassPrio *pSetDsTeClassPrio,
                                  UINT4 u4ObjectId);


INT1
MplsDsTeGetAllTeclassMapTable (UINT4 u4ClassTypeIndex,
                                  UINT4 u4ClassTeClassPrio,
                                  tMplsDsTeClassPrio *pGetDsTeClassPrio);

VOID
MplsGetPscId (UINT4 u4PhbId, UINT4 *pu4PscId);
VOID
MplsGetDnStrInfoUsingRecoveryLbl ARG_LIST ((UINT4 u4RecoveryLabel,
                                            tTeTnlInfo **pTeTnlInfo,
                                            UINT4 *pu4OutLabel));

VOID
MplsGetDnStrInfoUsingRecoveryPath ARG_LIST ((UINT4 u4RecoveryLabel,
                                             UINT4 u4NextHop,
                                             tTeTnlInfo **pTeTnlInfo,
                                             UINT4 *pu4OutLabel));

#if defined(MPLS_L3VPN_WANTED) || defined(VPLSADS_WANTED)
INT4
MplsBgp4NotifyASN (UINT1 u1AsnType, UINT4 u4AsnValue);

INT4
MplsValidateRdRtWithAsn(UINT1 *pu1RdRt);

#endif

#ifdef MPLS_IPV6_WANTED
VOID
MplsIpv6RouteChgEventHandle (tMplsRtEntryInfo *pRouteInfo);
#endif


#endif /*_MPLS_PROT_H*/
