/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmpfrdb.h,v 1.6 2016/08/02 09:58:47 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSMPFRDB_H
#define _FSMPFRDB_H

UINT1 FsMplsFrrConstTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMplsTunnelExtTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMplsBypassTunnelIfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMplsFrrTunARHopTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fsmpfr [] ={1,3,6,1,4,1,2076,13,3};
tSNMP_OID_TYPE fsmpfrOID = {9, fsmpfr};


UINT4 FsMplsFrrDetourIncoming [ ] ={1,3,6,1,4,1,2076,13,3,1,1};
UINT4 FsMplsFrrDetourOutgoing [ ] ={1,3,6,1,4,1,2076,13,3,1,2};
UINT4 FsMplsFrrDetourOriginating [ ] ={1,3,6,1,4,1,2076,13,3,1,3};
UINT4 FsMplsFrrSwitchover [ ] ={1,3,6,1,4,1,2076,13,3,1,4};
UINT4 FsMplsFrrConfIfs [ ] ={1,3,6,1,4,1,2076,13,3,1,5};
UINT4 FsMplsFrrActProtectedIfs [ ] ={1,3,6,1,4,1,2076,13,3,1,6};
UINT4 FsMplsFrrConfProtectionTuns [ ] ={1,3,6,1,4,1,2076,13,3,1,7};
UINT4 FsMplsFrrActProtectionTuns [ ] ={1,3,6,1,4,1,2076,13,3,1,8};
UINT4 FsMplsFrrActProtectedLSPs [ ] ={1,3,6,1,4,1,2076,13,3,1,9};
UINT4 FsMplsFrrRevertiveMode [ ] ={1,3,6,1,4,1,2076,13,3,1,10};
UINT4 FsMplsFrrDetourMergingEnabled [ ] ={1,3,6,1,4,1,2076,13,3,1,11};
UINT4 FsMplsFrrDetourEnabled [ ] ={1,3,6,1,4,1,2076,13,3,1,12};
UINT4 FsMplsFrrCspfRetryInterval [ ] ={1,3,6,1,4,1,2076,13,3,1,13};
UINT4 FsMplsFrrCspfRetryCount [ ] ={1,3,6,1,4,1,2076,13,3,1,14};
UINT4 FsMplsFrrNotifsEnabled [ ] ={1,3,6,1,4,1,2076,13,3,1,15};
UINT4 FsMplsFrrMakeAfterBreakEnabled [ ] ={1,3,6,1,4,1,2076,13,3,1,16};
UINT4 FsMplsFrrConstIfIndex [ ] ={1,3,6,1,4,1,2076,13,3,2,1,1,1,1};
UINT4 FsMplsFrrConstProtectionMethod [ ] ={1,3,6,1,4,1,2076,13,3,2,1,1,1,2};
UINT4 FsMplsFrrConstProtectionType [ ] ={1,3,6,1,4,1,2076,13,3,2,1,1,1,3};
UINT4 FsMplsFrrConstSetupPrio [ ] ={1,3,6,1,4,1,2076,13,3,2,1,1,1,4};
UINT4 FsMplsFrrConstHoldingPrio [ ] ={1,3,6,1,4,1,2076,13,3,2,1,1,1,5};
UINT4 FsMplsFrrConstSEStyle [ ] ={1,3,6,1,4,1,2076,13,3,2,1,1,1,6};
UINT4 FsMplsFrrConstInclAnyAffinity [ ] ={1,3,6,1,4,1,2076,13,3,2,1,1,1,7};
UINT4 FsMplsFrrConstInclAllAffinity [ ] ={1,3,6,1,4,1,2076,13,3,2,1,1,1,8};
UINT4 FsMplsFrrConstExclAnyAffinity [ ] ={1,3,6,1,4,1,2076,13,3,2,1,1,1,9};
UINT4 FsMplsFrrConstHopLimit [ ] ={1,3,6,1,4,1,2076,13,3,2,1,1,1,10};
UINT4 FsMplsFrrConstBandwidth [ ] ={1,3,6,1,4,1,2076,13,3,2,1,1,1,11};
UINT4 FsMplsFrrConstRowStatus [ ] ={1,3,6,1,4,1,2076,13,3,2,1,1,1,12};
UINT4 FsMplsTunnelExtProtIfIndex [ ] ={1,3,6,1,4,1,2076,13,3,2,1,2,1,1};
UINT4 FsMplsTunnelExtProtectionType [ ] ={1,3,6,1,4,1,2076,13,3,2,1,2,1,2};
UINT4 FsMplsTunnelExtBkpTunIdx [ ] ={1,3,6,1,4,1,2076,13,3,2,1,2,1,3};
UINT4 FsMplsTunnelExtBkpInst [ ] ={1,3,6,1,4,1,2076,13,3,2,1,2,1,4};
UINT4 FsMplsTunnelExtBkpIngrLSRId [ ] ={1,3,6,1,4,1,2076,13,3,2,1,2,1,5};
UINT4 FsMplsTunnelExtBkpEgrLSRId [ ] ={1,3,6,1,4,1,2076,13,3,2,1,2,1,6};
UINT4 FsMplsTunnelExtOne2OnePlrId [ ] ={1,3,6,1,4,1,2076,13,3,2,1,2,1,7};
UINT4 FsMplsTunnelExtOne2OnePlrSenderAddrType [ ] ={1,3,6,1,4,1,2076,13,3,2,1,2,1,8};
UINT4 FsMplsTunnelExtOne2OnePlrSenderAddr [ ] ={1,3,6,1,4,1,2076,13,3,2,1,2,1,9};
UINT4 FsMplsTunnelExtOne2OnePlrAvoidNAddrType [ ] ={1,3,6,1,4,1,2076,13,3,2,1,2,1,10};
UINT4 FsMplsTunnelExtOne2OnePlrAvoidNAddr [ ] ={1,3,6,1,4,1,2076,13,3,2,1,2,1,11};
UINT4 FsMplsTunnelExtDetourActive [ ] ={1,3,6,1,4,1,2076,13,3,2,1,2,1,12};
UINT4 FsMplsTunnelExtDetourMerging [ ] ={1,3,6,1,4,1,2076,13,3,2,1,2,1,13};
UINT4 FsMplsTunnelExtFacRouteDBProtTunStatus [ ] ={1,3,6,1,4,1,2076,13,3,2,1,2,1,14};
UINT4 FsMplsTunnelExtFacRouteDBProtTunResvBw [ ] ={1,3,6,1,4,1,2076,13,3,2,1,2,1,15};
UINT4 FsMplsTunnelExtProtectionMethod [ ] ={1,3,6,1,4,1,2076,13,3,2,1,2,1,16};
UINT4 FsMplsTunnelExtMaxGblRevertTime [ ] ={1,3,6,1,4,1,2076,13,3,2,1,2,1,17};
UINT4 FsMplsBypassTunnelIfIndex [ ] ={1,3,6,1,4,1,2076,13,3,2,1,3,1,1};
UINT4 FsMplsBypassTunnelIndex [ ] ={1,3,6,1,4,1,2076,13,3,2,1,3,1,2};
UINT4 FsMplsBypassTunnelIngressLSRId [ ] ={1,3,6,1,4,1,2076,13,3,2,1,3,1,3};
UINT4 FsMplsBypassTunnelEgressLSRId [ ] ={1,3,6,1,4,1,2076,13,3,2,1,3,1,4};
UINT4 FsMplsBypassTunnelRowStatus [ ] ={1,3,6,1,4,1,2076,13,3,2,1,3,1,5};
UINT4 FsMplsFrrTunARHopProtectType [ ] ={1,3,6,1,4,1,2076,13,3,2,1,4,1,1};
UINT4 FsMplsFrrTunARHopProtectTypeInUse [ ] ={1,3,6,1,4,1,2076,13,3,2,1,4,1,2};
UINT4 FsMplsFrrTunARHopLabel [ ] ={1,3,6,1,4,1,2076,13,3,2,1,4,1,3};
UINT4 FsMplsFrrTunARBwProtAvailable [ ] ={1,3,6,1,4,1,2076,13,3,2,1,4,1,4};


tMbDbEntry fsmpfrMibEntry[]= {

{{11,FsMplsFrrDetourIncoming}, NULL, FsMplsFrrDetourIncomingGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, "0"},

{{11,FsMplsFrrDetourOutgoing}, NULL, FsMplsFrrDetourOutgoingGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, "0"},

{{11,FsMplsFrrDetourOriginating}, NULL, FsMplsFrrDetourOriginatingGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, "0"},

{{11,FsMplsFrrSwitchover}, NULL, FsMplsFrrSwitchoverGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, "0"},

{{11,FsMplsFrrConfIfs}, NULL, FsMplsFrrConfIfsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, "0"},

{{11,FsMplsFrrActProtectedIfs}, NULL, FsMplsFrrActProtectedIfsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, "0"},

{{11,FsMplsFrrConfProtectionTuns}, NULL, FsMplsFrrConfProtectionTunsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, "0"},

{{11,FsMplsFrrActProtectionTuns}, NULL, FsMplsFrrActProtectionTunsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, "0"},

{{11,FsMplsFrrActProtectedLSPs}, NULL, FsMplsFrrActProtectedLSPsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, "0"},

{{11,FsMplsFrrRevertiveMode}, NULL, FsMplsFrrRevertiveModeGet, FsMplsFrrRevertiveModeSet, FsMplsFrrRevertiveModeTest, FsMplsFrrRevertiveModeDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, NULL, 0, 0, 0, NULL},

{{11,FsMplsFrrDetourMergingEnabled}, NULL, FsMplsFrrDetourMergingEnabledGet, FsMplsFrrDetourMergingEnabledSet, FsMplsFrrDetourMergingEnabledTest, FsMplsFrrDetourMergingEnabledDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsMplsFrrDetourEnabled}, NULL, FsMplsFrrDetourEnabledGet, FsMplsFrrDetourEnabledSet, FsMplsFrrDetourEnabledTest, FsMplsFrrDetourEnabledDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "1"},

{{11,FsMplsFrrCspfRetryInterval}, NULL, FsMplsFrrCspfRetryIntervalGet, FsMplsFrrCspfRetryIntervalSet, FsMplsFrrCspfRetryIntervalTest, FsMplsFrrCspfRetryIntervalDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "30000"},

{{11,FsMplsFrrCspfRetryCount}, NULL, FsMplsFrrCspfRetryCountGet, FsMplsFrrCspfRetryCountSet, FsMplsFrrCspfRetryCountTest, FsMplsFrrCspfRetryCountDep, SNMP_DATA_TYPE_GAUGE32, SNMP_READWRITE, NULL, 0, 0, 0, "10"},

{{11,FsMplsFrrNotifsEnabled}, NULL, FsMplsFrrNotifsEnabledGet, FsMplsFrrNotifsEnabledSet, FsMplsFrrNotifsEnabledTest, FsMplsFrrNotifsEnabledDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsMplsFrrMakeAfterBreakEnabled}, NULL, FsMplsFrrMakeAfterBreakEnabledGet, FsMplsFrrMakeAfterBreakEnabledSet, FsMplsFrrMakeAfterBreakEnabledTest, FsMplsFrrMakeAfterBreakEnabledDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{14,FsMplsFrrConstIfIndex}, GetNextIndexFsMplsFrrConstTable, FsMplsFrrConstIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMplsFrrConstTableINDEX, 4, 0, 0, NULL},

{{14,FsMplsFrrConstProtectionMethod}, GetNextIndexFsMplsFrrConstTable, FsMplsFrrConstProtectionMethodGet, FsMplsFrrConstProtectionMethodSet, FsMplsFrrConstProtectionMethodTest, FsMplsFrrConstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsFrrConstTableINDEX, 4, 0, 0, "2"},

{{14,FsMplsFrrConstProtectionType}, GetNextIndexFsMplsFrrConstTable, FsMplsFrrConstProtectionTypeGet, FsMplsFrrConstProtectionTypeSet, FsMplsFrrConstProtectionTypeTest, FsMplsFrrConstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsFrrConstTableINDEX, 4, 0, 0, "1"},

{{14,FsMplsFrrConstSetupPrio}, GetNextIndexFsMplsFrrConstTable, FsMplsFrrConstSetupPrioGet, FsMplsFrrConstSetupPrioSet, FsMplsFrrConstSetupPrioTest, FsMplsFrrConstTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsFrrConstTableINDEX, 4, 0, 0, "7"},

{{14,FsMplsFrrConstHoldingPrio}, GetNextIndexFsMplsFrrConstTable, FsMplsFrrConstHoldingPrioGet, FsMplsFrrConstHoldingPrioSet, FsMplsFrrConstHoldingPrioTest, FsMplsFrrConstTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsFrrConstTableINDEX, 4, 0, 0, "0"},

{{14,FsMplsFrrConstSEStyle}, GetNextIndexFsMplsFrrConstTable, FsMplsFrrConstSEStyleGet, FsMplsFrrConstSEStyleSet, FsMplsFrrConstSEStyleTest, FsMplsFrrConstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsFrrConstTableINDEX, 4, 0, 0, "1"},

{{14,FsMplsFrrConstInclAnyAffinity}, GetNextIndexFsMplsFrrConstTable, FsMplsFrrConstInclAnyAffinityGet, FsMplsFrrConstInclAnyAffinitySet, FsMplsFrrConstInclAnyAffinityTest, FsMplsFrrConstTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsFrrConstTableINDEX, 4, 0, 0, "0"},

{{14,FsMplsFrrConstInclAllAffinity}, GetNextIndexFsMplsFrrConstTable, FsMplsFrrConstInclAllAffinityGet, FsMplsFrrConstInclAllAffinitySet, FsMplsFrrConstInclAllAffinityTest, FsMplsFrrConstTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsFrrConstTableINDEX, 4, 0, 0, "0"},

{{14,FsMplsFrrConstExclAnyAffinity}, GetNextIndexFsMplsFrrConstTable, FsMplsFrrConstExclAnyAffinityGet, FsMplsFrrConstExclAnyAffinitySet, FsMplsFrrConstExclAnyAffinityTest, FsMplsFrrConstTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsFrrConstTableINDEX, 4, 0, 0, "0"},

{{14,FsMplsFrrConstHopLimit}, GetNextIndexFsMplsFrrConstTable, FsMplsFrrConstHopLimitGet, FsMplsFrrConstHopLimitSet, FsMplsFrrConstHopLimitTest, FsMplsFrrConstTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsFrrConstTableINDEX, 4, 0, 0, "32"},

{{14,FsMplsFrrConstBandwidth}, GetNextIndexFsMplsFrrConstTable, FsMplsFrrConstBandwidthGet, FsMplsFrrConstBandwidthSet, FsMplsFrrConstBandwidthTest, FsMplsFrrConstTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsFrrConstTableINDEX, 4, 0, 0, "0"},

{{14,FsMplsFrrConstRowStatus}, GetNextIndexFsMplsFrrConstTable, FsMplsFrrConstRowStatusGet, FsMplsFrrConstRowStatusSet, FsMplsFrrConstRowStatusTest, FsMplsFrrConstTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsFrrConstTableINDEX, 4, 0, 1, NULL},

{{14,FsMplsTunnelExtProtIfIndex}, GetNextIndexFsMplsTunnelExtTable, FsMplsTunnelExtProtIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMplsTunnelExtTableINDEX, 4, 0, 0, "0"},

{{14,FsMplsTunnelExtProtectionType}, GetNextIndexFsMplsTunnelExtTable, FsMplsTunnelExtProtectionTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMplsTunnelExtTableINDEX, 4, 0, 0, NULL},

{{14,FsMplsTunnelExtBkpTunIdx}, GetNextIndexFsMplsTunnelExtTable, FsMplsTunnelExtBkpTunIdxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMplsTunnelExtTableINDEX, 4, 0, 0, NULL},

{{14,FsMplsTunnelExtBkpInst}, GetNextIndexFsMplsTunnelExtTable, FsMplsTunnelExtBkpInstGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMplsTunnelExtTableINDEX, 4, 0, 0, NULL},

{{14,FsMplsTunnelExtBkpIngrLSRId}, GetNextIndexFsMplsTunnelExtTable, FsMplsTunnelExtBkpIngrLSRIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMplsTunnelExtTableINDEX, 4, 0, 0, NULL},

{{14,FsMplsTunnelExtBkpEgrLSRId}, GetNextIndexFsMplsTunnelExtTable, FsMplsTunnelExtBkpEgrLSRIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMplsTunnelExtTableINDEX, 4, 0, 0, NULL},

{{14,FsMplsTunnelExtOne2OnePlrId}, GetNextIndexFsMplsTunnelExtTable, FsMplsTunnelExtOne2OnePlrIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMplsTunnelExtTableINDEX, 4, 0, 0, NULL},

{{14,FsMplsTunnelExtOne2OnePlrSenderAddrType}, GetNextIndexFsMplsTunnelExtTable, FsMplsTunnelExtOne2OnePlrSenderAddrTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMplsTunnelExtTableINDEX, 4, 0, 0, NULL},

{{14,FsMplsTunnelExtOne2OnePlrSenderAddr}, GetNextIndexFsMplsTunnelExtTable, FsMplsTunnelExtOne2OnePlrSenderAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMplsTunnelExtTableINDEX, 4, 0, 0, NULL},

{{14,FsMplsTunnelExtOne2OnePlrAvoidNAddrType}, GetNextIndexFsMplsTunnelExtTable, FsMplsTunnelExtOne2OnePlrAvoidNAddrTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMplsTunnelExtTableINDEX, 4, 0, 0, NULL},

{{14,FsMplsTunnelExtOne2OnePlrAvoidNAddr}, GetNextIndexFsMplsTunnelExtTable, FsMplsTunnelExtOne2OnePlrAvoidNAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMplsTunnelExtTableINDEX, 4, 0, 0, NULL},

{{14,FsMplsTunnelExtDetourActive}, GetNextIndexFsMplsTunnelExtTable, FsMplsTunnelExtDetourActiveGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMplsTunnelExtTableINDEX, 4, 0, 0, NULL},

{{14,FsMplsTunnelExtDetourMerging}, GetNextIndexFsMplsTunnelExtTable, FsMplsTunnelExtDetourMergingGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMplsTunnelExtTableINDEX, 4, 0, 0, NULL},

{{14,FsMplsTunnelExtFacRouteDBProtTunStatus}, GetNextIndexFsMplsTunnelExtTable, FsMplsTunnelExtFacRouteDBProtTunStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMplsTunnelExtTableINDEX, 4, 0, 0, NULL},

{{14,FsMplsTunnelExtFacRouteDBProtTunResvBw}, GetNextIndexFsMplsTunnelExtTable, FsMplsTunnelExtFacRouteDBProtTunResvBwGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMplsTunnelExtTableINDEX, 4, 0, 0, NULL},

{{14,FsMplsTunnelExtProtectionMethod}, GetNextIndexFsMplsTunnelExtTable, FsMplsTunnelExtProtectionMethodGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMplsTunnelExtTableINDEX, 4, 0, 0, NULL},

{{14,FsMplsTunnelExtMaxGblRevertTime}, GetNextIndexFsMplsTunnelExtTable, FsMplsTunnelExtMaxGblRevertTimeGet, FsMplsTunnelExtMaxGblRevertTimeSet, FsMplsTunnelExtMaxGblRevertTimeTest, FsMplsTunnelExtTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsTunnelExtTableINDEX, 4, 0, 0, "600000"},

{{14,FsMplsBypassTunnelIfIndex}, GetNextIndexFsMplsBypassTunnelIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, FsMplsBypassTunnelIfTableINDEX, 4, 0, 0, NULL},

{{14,FsMplsBypassTunnelIndex}, GetNextIndexFsMplsBypassTunnelIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMplsBypassTunnelIfTableINDEX, 4, 0, 0, NULL},

{{14,FsMplsBypassTunnelIngressLSRId}, GetNextIndexFsMplsBypassTunnelIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMplsBypassTunnelIfTableINDEX, 4, 0, 0, NULL},

{{14,FsMplsBypassTunnelEgressLSRId}, GetNextIndexFsMplsBypassTunnelIfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMplsBypassTunnelIfTableINDEX, 4, 0, 0, NULL},

{{14,FsMplsBypassTunnelRowStatus}, GetNextIndexFsMplsBypassTunnelIfTable, FsMplsBypassTunnelRowStatusGet, FsMplsBypassTunnelRowStatusSet, FsMplsBypassTunnelRowStatusTest, FsMplsBypassTunnelIfTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsBypassTunnelIfTableINDEX, 4, 0, 1, NULL},

{{14,FsMplsFrrTunARHopProtectType}, GetNextIndexFsMplsFrrTunARHopTable, FsMplsFrrTunARHopProtectTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMplsFrrTunARHopTableINDEX, 2, 0, 0, NULL},

{{14,FsMplsFrrTunARHopProtectTypeInUse}, GetNextIndexFsMplsFrrTunARHopTable, FsMplsFrrTunARHopProtectTypeInUseGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsMplsFrrTunARHopTableINDEX, 2, 0, 0, NULL},

{{14,FsMplsFrrTunARHopLabel}, GetNextIndexFsMplsFrrTunARHopTable, FsMplsFrrTunARHopLabelGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMplsFrrTunARHopTableINDEX, 2, 0, 0, NULL},

{{14,FsMplsFrrTunARBwProtAvailable}, GetNextIndexFsMplsFrrTunARHopTable, FsMplsFrrTunARBwProtAvailableGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsMplsFrrTunARHopTableINDEX, 2, 0, 0, NULL},
};
tMibData fsmpfrEntry = { 54, fsmpfrMibEntry };
#endif /* _FSMPFRDB_H */

