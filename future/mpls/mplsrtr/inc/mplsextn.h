
/* $Id: mplsextn.h,v 1.9 2012/12/11 15:09:54 siva Exp $*/
/********************************************************************
 *                                                                  *
 * $RCSfile: mplsextn.h,v $                                          
 *                                                                  *
 * $Date: 2012/12/11 15:09:54 $
 *                                                                  *
 * $Revision: 1.9 $ 
 *                                                                  *
 ********************************************************************/
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : mplsextn.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS-FM   
 *    MODULE NAME            : 
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains extern functions
 *                             used by MPLS-FM
 *---------------------------------------------------------------------------*/
#ifndef _MPLS_EXTN_H
#define _MPLS_EXTN_H

extern INT4 arpTaskEnqueuePkt (tCRU_BUF_CHAIN_HEADER *pBuf, UINT2 u2Port,
                                       UINT2 u2Protocol, UINT1 u1EncapType);

extern UINT1 LdpDumpAllNormalLsps ARG_LIST ((INT4 *pNumLsps));
/* ******************** TE Related nmh START ************************ */

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsMplsTeAdminStatus ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetFsMplsTeMaxTnls ARG_LIST((UINT4 ));

extern INT1
nmhSetFsMplsTeMaxHopLists ARG_LIST((UINT4 ));

extern INT1
nmhSetFsMplsTeMaxPathOptPerList ARG_LIST((UINT4 ));

extern INT1
nmhSetFsMplsTeMaxHopsPerPathOption ARG_LIST((UINT4 ));

extern INT1
nmhSetFsMplsTeMaxArHopLists ARG_LIST((UINT4 ));

extern INT1
nmhSetFsMplsTeMaxHopsPerArHopList ARG_LIST((UINT4 ));

extern INT1
nmhSetFsMplsTeMaxRsvpTrfcParams ARG_LIST((UINT4 ));

extern INT1
nmhSetFsMplsTeMaxCrLdpTrfcParams ARG_LIST((UINT4 ));

extern INT1
nmhSetFsMplsTeMaxDServElps ARG_LIST((UINT4 ));

extern INT1
nmhSetFsMplsTeMaxDServLlps ARG_LIST((UINT4 ));


extern INT1
nmhSetFsMplsTeAdminStatus ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2FsMplsTeMaxTnls ARG_LIST((UINT4 *  ,UINT4 ));

extern INT1
nmhTestv2FsMplsTeMaxHopLists ARG_LIST((UINT4 *  ,UINT4 ));

extern INT1
nmhTestv2FsMplsTeMaxPathOptPerList ARG_LIST((UINT4 *  ,UINT4 ));

extern INT1
nmhTestv2FsMplsTeMaxHopsPerPathOption ARG_LIST((UINT4 *  ,UINT4 ));

extern INT1
nmhTestv2FsMplsTeMaxArHopLists ARG_LIST((UINT4 *  ,UINT4 ));

extern INT1
nmhTestv2FsMplsTeMaxHopsPerArHopList ARG_LIST((UINT4 *  ,UINT4 ));

extern INT1
nmhTestv2FsMplsTeMaxRsvpTrfcParams ARG_LIST((UINT4 *  ,UINT4 ));

extern INT1
nmhTestv2FsMplsTeMaxCrLdpTrfcParams ARG_LIST((UINT4 *  ,UINT4 ));

extern INT1
nmhTestv2FsMplsTeMaxDServElps ARG_LIST((UINT4 *  ,UINT4 ));

extern INT1
nmhTestv2FsMplsTeMaxDServLlps ARG_LIST((UINT4 *  ,UINT4 ));


extern INT1
nmhTestv2FsMplsTeAdminStatus ARG_LIST((UINT4 *  ,INT4 ));

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsMplsTunnelRSVPResRowStatus ARG_LIST((UINT4 ,INT4 *));

extern INT1
nmhGetFsMplsTunnelRSVPResStorageType ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetFsMplsTunnelRSVPResRowStatus ARG_LIST((UINT4  ,INT4 ));

extern INT1
nmhSetFsMplsTunnelRSVPResStorageType ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2FsMplsTunnelRSVPResRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

extern INT1
nmhTestv2FsMplsTunnelRSVPResStorageType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));


/* ******************** TE Related nmh END ************************ */


/* ******************** LDP Related nmh END ************************ */

/* Proto type for Low Level GET Routine All Objects.  */

extern INT1
nmhGetFsMplsLdpCrlspDumpDirection ARG_LIST((INT4 *));

extern INT1
nmhGetFsMplsLdpCrlspPersistance ARG_LIST((INT4 *));

extern INT1
nmhGetFsMplsLdpCrlspMd5Option ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

extern INT1
nmhSetFsMplsLdpCrlspDumpDirection ARG_LIST((INT4 ));

extern INT1
nmhSetFsMplsLdpCrlspPersistance ARG_LIST((INT4 ));

extern INT1
nmhSetFsMplsLdpCrlspMd5Option ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

extern INT1
nmhTestv2FsMplsLdpCrlspDumpDirection ARG_LIST((UINT4 *  ,INT4 ));

extern INT1
nmhTestv2FsMplsLdpCrlspPersistance ARG_LIST((UINT4 *  ,INT4 ));

extern INT1
nmhTestv2FsMplsLdpCrlspMd5Option ARG_LIST((UINT4 *  ,INT4 ));


/* ******************** LDP Related nmh END ************************ */
extern VOID RsvpIfStChgEventHandler (UINT4 u4IfIndex, UINT4 u4IpAddr,
                                     UINT1 u1OperState, INT4 i4ctrlMplsIfIndex);
/*extern VOID L2VpnIfEventHandler ARG_LIST ((UINT4 u4IfIndex, UINT2 u2PortVlan,
                                           UINT1 u1Status));*/
#endif /*_MPLS_EXTN_H*/
