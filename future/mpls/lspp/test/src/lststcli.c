/********************************************************************
 ** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 **
 ** $Id: lststcli.c,v 1.2 2010/10/31 00:52:14 prabuc Exp $
 **
 ** Description: LSPP  UT Test cases cli file.
 ** NOTE: This file should not be include in release packaging.
 ** ***********************************************************************/

#include "lr.h"
#include "cli.h"
#include "lsppinc.h"
#include "lsppcli.h"
#include "lststcli.h"

#define MAX_ARGS 5

extern VOID         LsppExecuteUt (UINT4 u4FileNumber, UINT4 u4TestNumber);
extern VOID         LsppExecuteUtAll (VOID);
extern VOID         LsppExecuteUtFile (UINT4 u4File);
extern UINT4        gu4LsppUtStubsReqd;

/*  Function is called from lststcmd.def file */

INT4
cli_process_lspp_test_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[MAX_ARGS];
    INT4                i4Inst = 0;
    INT1                argno = 0;
    CliRegisterLock (CliHandle, LsppMainTaskLock, LsppMainTaskUnLock);
    LSPP_LOCK;

    va_start (ap, u4Command);
    UNUSED_PARAM (CliHandle);
    i4Inst = va_arg (ap, INT4);

    /* Walk through the arguments and store in args array.
     *      * */
    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == MAX_ARGS)
            break;
    }

    va_end (ap);

    switch (u4Command)

    {
        case CLI_LSPP_UT_TEST:

            gu4LsppUtStubsReqd = OSIX_TRUE;

            if (args[0] == NULL)
            {
                if (args[2] != NULL)
                {
                    LsppExecuteUt (*(UINT4 *) (args[1]), *(UINT4 *) (args[2]));
                }
                else
                {
                    LsppExecuteUtFile (*(UINT4 *) (args[1]));
                }
            }
            else
            {
                LsppExecuteUtAll ();
            }

            gu4LsppUtStubsReqd = OSIX_FALSE;
            break;
    }

    CliUnRegisterLock (CliHandle);
    LSPP_UNLOCK;

    return CLI_SUCCESS;
}
