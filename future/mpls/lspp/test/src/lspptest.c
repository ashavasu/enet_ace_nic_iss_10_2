/********************************************************************
 ** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 **
 ** $Id: lspptest.c,v 1.7 2010/11/23 04:53:25 siva Exp $
 **
 ** Description: LSPP  UT Test cases file.
 ** NOTE: This file should not be include in release packaging.
 ** ***********************************************************************/

#include "lspptest.h"
#include "iss.h"

UINT4               gu4LsppUtTestNumber = 0;
UINT4               gu4LsppUtStubsReqd = OSIX_FALSE;

VOID
LsppExecuteUt (UINT4 u4FileNumber, UINT4 u4TestNumber)
{
    INT4                i4RetVal = OSIX_FAILURE;

    switch (u4FileNumber)
    {
            /* lsppapi.c */
        case 1:
            /* {
               switch (u4TestNumber)
               {
               case 1:
               i4RetVal = LsppUtApi_1();
               break;
               case 2:
               i4RetVal = LsppUtApi_2();
               break;
               case 3:
               i4RetVal = LsppUtApi_3();
               break;
               case 4:
               i4RetVal = LsppUtApi_4();
               break;
               default:
               printf("Invalid Test for file lsppapi.c \r\n");
               return;
               }
               }
               if (i4RetVal == OSIX_SUCCESS)    
               {
               printf("Unit Test Case: %d of lsppapi.c is Passed \r\n",
               u4TestNumber);
               }
               else
               {
               printf("Unit Test Case: %d of lsppapi.c is Failed \r\n",
               u4TestNumber);
               } */
            return;
            /* lsppcli.c */
        case 2:
        {

            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = LsppUtCli_1 ();
                    break;
                case 2:
                    i4RetVal = LsppUtCli_2 ();
                    break;
                case 3:
                    i4RetVal = LsppUtCli_3 ();
                    break;
                case 4:
                    i4RetVal = LsppUtCli_4 ();
                    break;
                case 5:
                    i4RetVal = LsppUtCli_5 ();
                    break;
                case 6:
                    i4RetVal = LsppUtCli_6 ();
                    break;
                case 7:
                    i4RetVal = LsppUtCli_7 ();
                    break;
                case 8:
                    i4RetVal = LsppUtCli_8 ();
                    break;
                case 9:
                    i4RetVal = LsppUtCli_9 ();
                    break;
                case 10:
                    i4RetVal = LsppUtCli_10 ();
                    break;
                case 11:
                    i4RetVal = LsppUtCli_11 ();
                    break;
                case 12:
                    i4RetVal = LsppUtCli_12 ();
                    break;
                case 13:
                    i4RetVal = LsppUtCli_13 ();
                    break;
                default:
                    printf ("Invalid Test for file lsppcli.c \r\n");
                    return;
            }
        }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of lsppcli.c is Passed \r\n",
                        u4TestNumber);
            }
            else
            {
                printf ("Unit Test Case: %d of lsppcli.c is Failed \r\n",
                        u4TestNumber);

            }
            return;

        case 3:
            return;

        case 4:
        {

            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = LsppUtCore_1 ();
                    break;
                case 2:
                    i4RetVal = LsppUtCore_2 ();
                    break;
                case 3:
                    i4RetVal = LsppUtCore_3 ();
                    break;
                case 4:
                    i4RetVal = LsppUtCore_4 ();
                    break;
                case 5:
                    i4RetVal = LsppUtCore_5 ();
                    break;
                case 6:
                    i4RetVal = LsppUtCore_6 ();
                    break;
                case 7:
                    i4RetVal = LsppUtCore_7 ();
                    break;
                case 8:
                    i4RetVal = LsppUtCore_8 ();
                    break;
                case 9:
                    i4RetVal = LsppUtCore_9 ();
                    break;
                case 10:
                    i4RetVal = LsppUtCore_10 ();
                    break;
                case 11:
                    i4RetVal = LsppUtCore_11 ();
                    break;
                case 12:
                    i4RetVal = LsppUtCore_12 ();
                    break;
                case 13:
                    i4RetVal = LsppUtCore_13 ();
                    break;
                case 14:
                    i4RetVal = LsppUtCore_14 ();
                    break;
                case 15:
                    i4RetVal = LsppUtCore_15 ();
                    break;
                case 16:
                    i4RetVal = LsppUtCore_16 ();
                    break;
                case 17:
                    i4RetVal = LsppUtCore_17 ();
                    break;
                case 18:
                    i4RetVal = LsppUtCore_18 ();
                    break;
                case 19:
                    i4RetVal = LsppUtCore_19 ();
                    break;
                case 20:
                    i4RetVal = LsppUtCore_20 ();
                    break;
                case 21:
                    i4RetVal = LsppUtCore_21 ();
                    break;
                case 22:
                    i4RetVal = LsppUtCore_22 ();
                    break;
                case 23:
                    i4RetVal = LsppUtCore_23 ();
                    break;
                case 24:
                    i4RetVal = LsppUtCore_24 ();
                    break;
                case 25:
                    i4RetVal = LsppUtCore_25 ();
                    break;
                case 26:
                    i4RetVal = LsppUtCore_26 ();
                    break;
                case 27:
                    i4RetVal = LsppUtCore_27 ();
                    break;
                case 28:
                    i4RetVal = LsppUtCore_28 ();
                    break;
                case 29:
                    i4RetVal = LsppUtCore_29 ();
                    break;
                case 30:
                    i4RetVal = LsppUtCore_30 ();
                    break;
                    /*            case 31:
                       i4RetVal = LsppUtCore_31();
                       break; */
                case 32:
                    i4RetVal = LsppUtCore_32 ();
                    break;
                case 34:
                    i4RetVal = LsppUtCore_34 ();
                    break;
                case 35:
                    i4RetVal = LsppUtCore_35 ();
                    break;
                case 37:
                    i4RetVal = LsppUtCore_37 ();
                    break;
                case 39:
                    i4RetVal = LsppUtCore_39 ();
                    break;
                default:
                    printf ("Invalid Test for file lsppcore.c \r\n");
                    return;
            }
        }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of lsppcore.c is Passed \r\n",
                        u4TestNumber);
            }
            else
            {
                printf ("Unit Test Case: %d of lsppcore.c is Failed \r\n",
                        u4TestNumber);
            }
            return;

            /* lsppdb.c */
        case 5:
        {
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = LsppUtDb_1 ();
                    break;
                case 2:
                    i4RetVal = LsppUtDb_2 ();
                    break;
                case 3:
                    i4RetVal = LsppUtDb_3 ();
                    break;
                case 4:
                    i4RetVal = LsppUtDb_4 ();
                    break;
                case 5:
                    i4RetVal = LsppUtDb_5 ();
                    break;
                case 6:
                    i4RetVal = LsppUtDb_6 ();
                    break;
                default:
                    printf ("Invalid Test for file lsppDb.c \r\n");
                    return;
            }
        }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of lsppdb.c is Passed \r\n",
                        u4TestNumber);
            }
            else
            {
                printf ("Unit Test Case: %d of lsppdb.c is Failed \r\n",
                        u4TestNumber);
            }
            return;

        case 6:
            return;

        case 7:
            return;

            /* lsppdval.c */
        case 8:
        {
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = LsppUtDval_1 ();
                    break;
                    /*
                       case 2:
                       i4RetVal = LsppUtDval_2 ();
                       break;
                       case 3:
                       i4RetVal = LsppUtDval_3 ();
                       break;
                     */
                default:
                    printf ("Invalid Test for file lsppDval.c \r\n");
                    return;
            }
        }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of lsppdval.c is Passed \r\n",
                        u4TestNumber);
            }
            else
            {
                printf ("Unit Test Case: %d of lsppdval.c is Failed \r\n",
                        u4TestNumber);
            }
            return;

        case 9:
            return;

            /* lsppext.c */
        case 10:
        {
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = LsppUtExt_1 ();
                    break;
                case 2:
                    i4RetVal = LsppUtExt_2 ();
                    break;
                default:
                    printf ("Invalid Test for file lsppext.c \r\n");
                    return;
            }
        }
            if (i4RetVal == OSIX_SUCCESS)
            {
                PRINTF ("Unit Test Case: %d of lsppext.c is Passed \r\n",
                        u4TestNumber);
            }
            else
            {
                printf ("Unit Test Case: %d of lsppext.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* lspplw.c */
        case 11:
        {
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = LsppUtLw_1 ();
                    break;
                case 2:
                    i4RetVal = LsppUtLw_2 ();
                    break;
                case 3:
                    i4RetVal = LsppUtLw_3 ();
                    break;
                case 4:
                    i4RetVal = LsppUtLw_4 ();
                    break;
                case 5:
                    i4RetVal = LsppUtLw_5 ();
                    break;
                default:
                    printf ("Invalid Test for file lspplw.c \r\n");
                    return;
            }
        }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of lspplw.c is Passed \r\n",
                        u4TestNumber);

            }
            else
            {
                printf ("Unit Test Case: %d of lspplw.c is Failed \r\n",
                        u4TestNumber);
            }
            return;

        case 12:
            return;

            /* lsppmain.c */
        case 13:
        {
            /*  switch (u4TestNumber)
               {
               case 1:
               i4RetVal = LsppUtMain_1 ();
               break;
               case 2:
               i4RetVal = LsppUtMain_2 ();
               break;
               case 3:
               i4RetVal = LsppUtMain_3 ();
               break;
               case 4:
               i4RetVal = LsppUtMain_4 ();
               break;
               default:
               printf("Invalid Test for file lsppmain.c \r\n");
               return;
               }
               }
               if (i4RetVal == OSIX_SUCCESS)    
               {
               printf("Unit Test Case: %d of lsppmain.c is Passed \r\n",
               u4TestNumber);
               }
               else
               {
               printf("Unit Test Case: %d of lsppmain.c is Failed \r\n",
               u4TestNumber); */
        }
            return;
            /* lsppport.c */
        case 14:
        {
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = LsppUtPort_1 ();
                    break;
                case 2:
                    i4RetVal = LsppUtPort_2 ();
                    break;
                case 3:
                    i4RetVal = LsppUtPort_3 ();
                    break;
                default:
                    printf ("Invalid Test for file lsppport.c \r\n");
                    return;
            }
        }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of lsppport.c is Passed \r\n",
                        u4TestNumber);
            }
            else
            {
                printf ("Unit Test Case: %d of lsppport.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* lsppque.c */

        case 15:
        {
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = LsppUtQue_1 ();
                    break;
                case 2:
                    i4RetVal = LsppUtQue_2 ();
                    break;
                case 3:
                    i4RetVal = LsppUtQue_3 ();
                    break;
                default:
                    printf ("Invalid Test for file lsppque.c \r\n");
                    return;
            }
        }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of lsppque.c is Passed \r\n",
                        u4TestNumber);
            }
            else
            {
                printf ("Unit Test Case: %d of lsppque.c is Failed \r\n",
                        u4TestNumber);
            }
            return;

            /* lspprx.c */
        case 16:
        {
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = LsppUtRx_1 ();
                    break;
                case 2:
                    i4RetVal = LsppUtRx_2 ();
                    break;
                case 3:
                    i4RetVal = LsppUtRx_3 ();
                    break;
                case 4:
                    i4RetVal = LsppUtRx_4 ();
                    break;
                case 5:
                    i4RetVal = LsppUtRx_5 ();
                    break;
                case 6:
                    i4RetVal = LsppUtRx_6 ();
                    break;
                case 7:
                    i4RetVal = LsppUtRx_7 ();
                    break;
                case 8:
                    i4RetVal = LsppUtRx_8 ();
                    break;
                case 9:
                    i4RetVal = LsppUtRx_9 ();
                    break;
                case 10:
                    i4RetVal = LsppUtRx_10 ();
                    break;
                case 11:
                    i4RetVal = LsppUtRx_11 ();
                    break;
                case 12:
                    i4RetVal = LsppUtRx_12 ();
                    break;
                case 13:
                    i4RetVal = LsppUtRx_13 ();
                    break;
                case 14:
                    i4RetVal = LsppUtRx_14 ();
                    break;
                case 15:
                    i4RetVal = LsppUtRx_15 ();
                    break;
                case 16:
                    i4RetVal = LsppUtRx_16 ();
                    break;
                case 17:
                    i4RetVal = LsppUtRx_17 ();
                    break;
                    /*     case 18:
                       i4RetVal = LsppUtRx_18 ();
                       break;
                       case 19:
                       i4RetVal = LsppUtRx_19 ();
                       break; */
                case 20:
                    i4RetVal = LsppUtRx_20 ();
                    break;
                case 21:
                    i4RetVal = LsppUtRx_21 ();
                    break;
                case 22:
                    i4RetVal = LsppUtRx_22 ();
                    break;
                case 23:
                    i4RetVal = LsppUtRx_23 ();
                    break;
                case 24:
                    i4RetVal = LsppUtRx_24 ();
                    break;
                case 25:
                    i4RetVal = LsppUtRx_25 ();
                    break;
                case 26:
                    i4RetVal = LsppUtRx_26 ();
                    break;
                case 27:
                    i4RetVal = LsppUtRx_27 ();
                    break;        /*
                                   case 28:
                                   i4RetVal = LsppUtRx_28 ();
                                   break; */
                case 29:
                    i4RetVal = LsppUtRx_29 ();
                    break;
                case 30:
                    i4RetVal = LsppUtRx_30 ();
                    break;
                case 31:
                    i4RetVal = LsppUtRx_31 ();
                    break;
                case 32:
                    i4RetVal = LsppUtRx_32 ();
                    break;
                case 33:
                    i4RetVal = LsppUtRx_33 ();
                    break;
                case 34:
                    i4RetVal = LsppUtRx_34 ();
                    break;
                default:
                    printf ("Invalid Test for file lspprx.c \r\n");
                    return;
            }
        }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of lspprx.c is Passed \r\n",
                        u4TestNumber);
            }
            else
            {
                printf ("Unit Test Case: %d of lspprx.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* lsppsock.c */
        case 17:
        {
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = LsppUtSock_1 ();
                    break;
                case 2:
                    i4RetVal = LsppUtSock_2 ();
                    break;
                case 3:
                    i4RetVal = LsppUtSock_3 ();
                    break;
                    /*       case 4:
                       i4RetVal = LsppUtSock_4 (); 
                       break; */
                default:
                    printf ("Invalid Test for file lsppsock.c \r\n");
                    return;
            }
        }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of lsppsock.c is Passed \r\n",
                        u4TestNumber);
            }
            else
            {
                printf ("Unit Test Case: %d of lsppsock.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* lspptask.c */
        case 19:
            /*  {
               switch (u4TestNumber)
               {
               case 1:
               i4RetVal = LsppUtTask_1 ();
               break;
               default:
               printf("Invalid Test for file lspptask.c \r\n");
               return;
               }
               }
               if (i4RetVal == OSIX_SUCCESS)    
               {
               printf("Unit Test Case: %d of lspptask.c is Passed \r\n",
               u4TestNumber);
               }
               else
               {
               printf("Unit Test Case: %d of lspptask.c is Failed \r\n",
               u4TestNumber);
               } */
            return;
            /* lspptmr.c */
        case 20:
        {

            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = LsppUtTmr_1 ();
                    break;
                case 2:
                    i4RetVal = LsppUtTmr_2 ();
                    break;
                case 3:
                    i4RetVal = LsppUtTmr_3 ();
                    break;
                case 4:
                    i4RetVal = LsppUtTmr_4 ();
                    break;
                case 5:
                    i4RetVal = LsppUtTmr_5 ();
                    break;
                default:
                    printf ("Invalid Test for file lspptmr.c \r\n");
                    return;
            }
        }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of lspptmr.c is Passed \r\n",
                        u4TestNumber);
            }
            else
            {
                printf ("Unit Test Case: %d of lspptmr.c is Failed \r\n",
                        u4TestNumber);
            }
            return;

            /* lspptrap.c */
        case 21:
/*           {
               switch (u4TestNumber)
               {
                   case 1:
                      i4RetVal = LsppUtTrap_1 ();
                      break;
                   case 2:
                      i4RetVal = LsppUtTrap_2 ();
                      break;
                   case 3:
                      i4RetVal = LsppUtTrap_3 ();
                      break;
                   case 4:
                      i4RetVal = LsppUtTrap_4 ();
                      break;
                   case 5:
                      i4RetVal = LsppUtTrap_5 ();
                      break;
                   default:
                      printf("Invalid Test for file lspptrap.c \r\n");
                      return;
               }
           }
           if (i4RetVal == OSIX_SUCCESS)
           {
              printf("Unit Test Case: %d of lspptrap.c is Passed \r\n",
                      u4TestNumber);
           }
           else
           {
              printf("Unit Test Case: %d of lspptrap.c is Failed \r\n",
                     u4TestNumber);
           } */
            return;

            /* lspptrc.c */
        case 22:
        {
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = LsppUtTrc_1 ();
                    break;
                case 2:
                    i4RetVal = LsppUtTrc_2 ();
                    break;
                case 3:
                    i4RetVal = LsppUtTrc_3 ();
                    break;
                case 4:
                    i4RetVal = LsppUtTrc_4 ();
                    break;
                case 5:
                    i4RetVal = LsppUtTrc_5 ();
                    break;
                default:
                    printf ("Invalid Test for file lspptrc.c \r\n");
                    return;
            }
        }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of lspptrc.c is Passed \r\n",
                        u4TestNumber);
            }
            else
            {
                printf ("Unit Test Case: %d of lspptrc.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* lspptx.c */
        case 23:
        {
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = LsppUtTx_1 ();
                    break;

                case 2:
                    i4RetVal = LsppUtTx_2 ();
                    break;
                case 3:
                    i4RetVal = LsppUtTx_3 ();
                    break;
                case 4:
                    i4RetVal = LsppUtTx_4 ();
                    break;
                case 5:
                    i4RetVal = LsppUtTx_5 ();
                    break;
                    /*   case 6:
                       i4RetVal = LsppUtTx_6 (); 
                       break; */
                case 7:
                    i4RetVal = LsppUtTx_7 ();
                    break;
                case 8:
                    i4RetVal = LsppUtTx_8 ();
                    break;
                default:
                    printf ("Invalid Test for file lspptx.c \r\n");
                    return;
            }
        }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of lspptx.c is Passed \r\n",
                        u4TestNumber);
            }
            else
            {
                printf ("Unit Test Case: %d of lspptx.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* lspputil.c */
        case 24:
        {
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = LsppUtUtil_1 ();
                    break;
                case 2:
                    i4RetVal = LsppUtUtil_2 ();
                    break;
                case 3:
                    i4RetVal = LsppUtUtil_3 ();
                    break;
                case 4:
                    i4RetVal = LsppUtUtil_4 ();
                    break;
                case 5:
                    i4RetVal = LsppUtUtil_5 ();
                    break;
                default:
                    printf ("Invalid Test for file lspputil.c \r\n");
                    return;
            }
        }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of lspputil.c is Passed \r\n",
                        u4TestNumber);
            }
            else
            {
                printf ("Unit Test Case: %d of lspputil.c is Failed \r\n",
                        u4TestNumber);
            }
            return;
            /* lspputl.c */
        case 25:
        {
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = LsppUtUtl_1 ();
                    break;
                case 2:
                    i4RetVal = LsppUtUtl_2 ();
                    break;
                default:
                    printf ("Invalid Test for file lspputl.c \r\n");
                    return;
            }
        }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of lspputl.c is Passed \r\n",
                        u4TestNumber);
            }
            else
            {
                printf ("Unit Test Case: %d of lspputl.c is Failed \r\n",
                        u4TestNumber);
            }
            return;

        case 26:
            return;

        case 27:
        {

            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = LsppUtCxt_1 ();
                    break;
                case 2:
                    i4RetVal = LsppUtCxt_2 ();
                    break;
                case 3:
                    i4RetVal = LsppUtCxt_3 ();
                    break;
                case 4:
                    i4RetVal = LsppUtCxt_4 ();
                    break;
                case 5:
                    i4RetVal = LsppUtCxt_5 ();
                    break;
                case 6:
                    i4RetVal = LsppUtCxt_6 ();
                    break;
                case 7:
                    i4RetVal = LsppUtCxt_7 ();
                    break;
                case 8:
                    i4RetVal = LsppUtCxt_8 ();
                    break;
                case 9:
                    i4RetVal = LsppUtCxt_9 ();
                    break;
                case 10:
                    i4RetVal = LsppUtCxt_10 ();
                    break;
                case 11:
                    i4RetVal = LsppUtCxt_11 ();
                    break;
                case 12:
                    i4RetVal = LsppUtCxt_12 ();
                    break;
                case 13:
                    i4RetVal = LsppUtCxt_13 ();
                    break;
                default:
                    printf ("Invalid Test for file lsppcxt.c \r\n");
                    return;
            }
        }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of lsppcxt.c is Passed \r\n",
                        u4TestNumber);
            }
            else
            {
                printf ("Unit Test Case: %d of lsppcxt.c is Failed \r\n",
                        u4TestNumber);
            }
            return;

            /* PW 128/129 FEC  */
        case 28:
        {
            switch (u4TestNumber)
            {
                case 1:
                    i4RetVal = LsppUtPw_1 ();
                    break;
                case 2:
                    i4RetVal = LsppUtPw_2 ();
                    break;
                case 3:
                    i4RetVal = LsppUtPw_3 ();
                    break;
                case 4:
                    i4RetVal = LsppUtPw_4 ();
                    break;
                case 5:
                    i4RetVal = LsppUtPw_5 ();
                    break;
                case 6:
                    i4RetVal = LsppUtPw_6 ();
                    break;
                case 7:
                    i4RetVal = LsppUtPw_7 ();
                    break;
                case 8:
                    i4RetVal = LsppUtPw_8 ();
                    break;
                case 9:
                    i4RetVal = LsppUtPw_9 ();
                    break;
                case 10:
                    i4RetVal = LsppUtPw_10 ();
                    break;
                case 11:
                    i4RetVal = LsppUtPw_11 ();
                    break;
                case 12:
                    i4RetVal = LsppUtPw_12 ();
                    break;
                case 13:
                    i4RetVal = LsppUtPw_13 ();
                    break;
                case 14:
                    i4RetVal = LsppUtPw_14 ();
                    break;
                case 15:
                    i4RetVal = LsppUtPw_15 ();
                    break;
                case 16:
                    i4RetVal = LsppUtPw_16 ();
                    break;
                case 17:
                    i4RetVal = LsppUtPw_17 ();
                    break;
                case 18:
                    i4RetVal = LsppUtPw_18 ();
                    break;
                case 19:
                    i4RetVal = LsppUtPw_19 ();
                    break;
                case 20:
                    i4RetVal = LsppUtPw_20 ();
                    break;
                case 21:
                    i4RetVal = LsppUtPw_21 ();
                    break;
                case 22:
                    i4RetVal = LsppUtPw_22 ();
                    break;
                case 23:
                    i4RetVal = LsppUtPw_23 ();
                    break;
                case 24:
                    i4RetVal = LsppUtPw_24 ();
                    break;
                case 25:
                    i4RetVal = LsppUtPw_25 ();
                    break;
                case 26:
                    i4RetVal = LsppUtPw_26 ();
                    break;
                case 27:
                    i4RetVal = LsppUtPw_27 ();
                    break;
                case 28:
                    i4RetVal = LsppUtPw_28 ();
                    break;
                case 29:
                    i4RetVal = LsppUtPw_29 ();
                    break;
                case 30:
                    i4RetVal = LsppUtPw_30 ();
                    break;
                case 31:
                    i4RetVal = LsppUtPw_31 ();
                    break;
                case 32:
                    i4RetVal = LsppUtPw_32 ();
                    break;
                case 33:
                    i4RetVal = LsppUtPw_33 ();
                    break;
                default:
                    printf ("Invalid Test for PW 128/129 FEC \r\n");
                    return;
            }
        }
            if (i4RetVal == OSIX_SUCCESS)
            {
                printf ("Unit Test Case: %d of PW 128/129 FEC is Passed \r\n",
                        u4TestNumber);
            }
            else
            {
                printf ("Unit Test Case: %d of PW 128/129 FEC is Failed \r\n",
                        u4TestNumber);
            }
            return;
    }
}

VOID
LsppExecuteUtAll (VOID)
{
    UINT1               u1File = 0;

    for (u1File = 1; u1File <= 28; u1File++)
    {
        LsppExecuteUtFile (u1File);
    }
}

VOID
LsppExecuteUtFile (UINT4 u4File)
{
    UINT1               u1Case = 0;

    for (u1Case = 1; u1Case <= gTestCase[u4File - 1].u1Case; u1Case++)
    {
        LsppExecuteUt (u4File, u1Case);

    }
}

#if 0

INT4
LsppTstTxPingPkt (VOID)
{
    INT4                i4RetVal = OSIX_FAILURE;

    /* Ping to an egress node. */
    gu1RespndrNodeType = LSPP_EGRESS;

    /***********************************************************************/
    /***********************************************************************/
    /***********************************************************************/
    /*Connectivity verification for various FECs (LDP, RSVP-TE, PW and MEG. */
    /***********************************************************************/
    /***********************************************************************/
    /***********************************************************************/

    /* LDP FEC
       CliTakeAppContext();
       MGMT_UNLOCK ();
       CliExecuteAppCmd ("ping mpls ipv4 30.0.0.0/24");
       CliGiveAppContext();
       MGMT_LOCK ();

       CliTakeAppContext();
       MGMT_UNLOCK ();
       CliExecuteAppCmd ("ping mpls ipv6 --");
       CliGiveAppContext();
       MGMT_LOCK ();
     */

    /* MEP - Static LSP */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    i4RetVal = CliExecuteAppCmd ("ping mpls meg-name meg1 meLsp");
    CliGiveAppContext ();
    MGMT_LOCK ();

    if (i4RetVal != CLI_SUCCES)
    {
        return OSIX_FAILURE;
    }

    /* MEP - Static PW */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    i4RetVal = CliExecuteAppCmd ("ping mpls meg-name meg1 mePw");
    CliGiveAppContext ();
    MGMT_LOCK ();

    if (i4RetVal != CLI_SUCCES)
    {
        return OSIX_FAILURE;
    }

    /* RSVP-TE FEC - ipv4 */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    i4RetVal = CliExecuteAppCmd ("ping mpls traffic-eng Tunnel 1");
    CliGiveAppContext ();
    MGMT_LOCK ();

    if (i4RetVal != CLI_SUCCES)
    {
        return OSIX_FAILURE;
    }

    /* RSVP-TE FEC - ipv4 - with LSP number */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls traffic-eng Tunnel 1 lsp 1");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* RSVP-TE FEC - ipv6 */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls traffic-eng Tunnel 2");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* PW FEC 128 */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls pseudowire 30.0.0.10 1");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* PW FEC 129 - VC ID is set as 2 for 129 FEC identification */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls pseudowire 30.0.0.10 2");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Ping to a transit node. */
    gu1RespndrNodeType = LSPP_TRANSIT;

    /* LDP FEC
       CliTakeAppContext();
       MGMT_UNLOCK ();
       CliExecuteAppCmd ("ping mpls ipv4 30.0.0.0/24 ttl 5");
       CliGiveAppContext();
       MGMT_LOCK ();

       CliTakeAppContext();
       MGMT_UNLOCK ();
       CliExecuteAppCmd ("ping mpls ipv6 -- ttl 5");
       CliGiveAppContext();
       MGMT_LOCK ();
     */

    /* RSVP-TE FEC - ipv4 */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls traffic-eng Tunnel 1 ttl 5");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* RSVP-TE FEC - ipv6 */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls traffic-eng Tunnel 2 ttl 5");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* MEP - Static LSP */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls meg-name meg1 meLsp global-id 1 "
                      "node-id 1 if-num 1");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Ping to an egress node. */
    gu1RespndrNodeType = LSPP_EGRESS;

    /***********************************************************************/
    /***********************************************************************/
    /***********************************************************************/
    /* Connectivity verification for an MPLS-TP Tunnel/PW with different   */
    /* options.                                                            */
    /***********************************************************************/
    /***********************************************************************/
    /***********************************************************************/

    /* Repeat Count */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls meg-name meg1 meLsp repeat 1");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* WFR timout */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls meg-name meg1 meLsp repeat 1 timeout 2 "
                      "timeout-unit sec");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /*Packet size to sent */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls meg-name meg1 meLsp repeat 1 size 200 ");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /*Sweep option with range of pkt sizes */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls meg-name meg1 meLsp repeat 1 "
                      "sweep 200 300 50 ");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /*Pad pattern option */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls meg-name meg1 meLsp repeat 1 "
                      "size 300 pad lspp");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /*Reply dscp option */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls meg-name meg1 meLsp repeat 1 " "reply dscp 1");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /*Reply pad option */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls meg-name meg1 meLsp repeat 1 "
                      "reply pad-tlv");
    CliGiveAppContext ();
    MGMT_LOCK ();
    /*Reply mode option - ipv4 */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls meg-name meg1 meLsp repeat 1 "
                      "reply mode ipv4");
    CliGiveAppContext ();
    MGMT_LOCK ();
#if 0
    /*Reply mode option - ipv6 */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls meg-name meg1 meLsp repeat 1 "
                      "reply mode ipv6");
    CliGiveAppContext ();
    MGMT_LOCK ();
#endif
    /*Reply mode option - router alert */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls meg-name meg1 meLsp repeat 1 "
                      "reply mode router-alert");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /*Reply mode option -  no-reply */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls meg-name meg1 meLsp repeat 1 "
                      "reply mode no-reply");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /*WTS - timeout */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls meg-name meg1 meLsp repeat 1 "
                      "interval 2 interval-unit sec");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* EXP - option */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls meg-name meg1 meLsp repeat 1 " "exp 1");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Verbose - option */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls meg-name meg1 meLsp repeat 1 verbose");
    CliGiveAppContext ();
    MGMT_LOCK ();
    /* Force explicit NULL - option */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls meg-name meg1 meLsp repeat 1 "
                      "force-explicit-null");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* DSMAP and Interface & Lbl Stack options */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls meg-name meg1 meLsp repeat 1 "
                      "dsmap rx-intf-label");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* FEC validate flag - option */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls meg-name meg1 meLsp repeat 1 " "flags fec");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Same Sequecne number option */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls meg-name meg1 meLsp repeat 1 " "same-seq-num");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Burst option */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls meg-name meg1 meLsp repeat 1 " "burst size 2");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Reverse path verify - option */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls meg-name meg1 meLsp repeat 1 "
                      "rev-path-verify");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Encapsulation Type - option */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls meg-name meg1 meLsp repeat 1 "
                      "encap-type mpls-ach-ip");
    CliGiveAppContext ();
    MGMT_LOCK ();

 /***********************************************************************/
    /***********************************************************************/
    /***********************************************************************/
    /* Connectivity verification for an MPLS Tunnel with different options. */
    /***********************************************************************/
    /***********************************************************************/
    /***********************************************************************/

    /* Repeat Count */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls traffic-eng Tunnel 1 repeat 1");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* WFR timout */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls traffic-eng Tunnel 1 repeat 1 timeout 2 "
                      "timeout-unit sec");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /*Packet size to sent */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls traffic-eng Tunnel 1 repeat 1 size 200 ");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /*Sweep option with range of pkt sizes */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls traffic-eng Tunnel 1 repeat 1 "
                      "sweep 200 300 50 ");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /*Pad pattern option */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls traffic-eng Tunnel 1 repeat 1 "
                      "size 300 pad lspp");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /*Reply dscp option */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls traffic-eng Tunnel 1 repeat 1 "
                      "reply dscp 1");
    CliGiveAppContext ();
    MGMT_LOCK ();
    /*Reply pad option */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls traffic-eng Tunnel 1 repeat 1 "
                      "reply pad-tlv");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /*Reply mode option - ipv4 */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls traffic-eng Tunnel 1 repeat 1 "
                      "reply mode ipv4");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /*Reply mode option - ipv6 */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls traffic-eng Tunnel 1 repeat 1 "
                      "reply mode ipv6");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /*Reply mode option - router alert */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls traffic-eng Tunnel 1 repeat 1 "
                      "reply mode router-alert");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /*Reply mode option -  no-reply */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls traffic-eng Tunnel 1 repeat 1 "
                      "reply mode no-reply");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /*WTS - timeout */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls traffic-eng Tunnel 1 repeat 1 "
                      "interval 2 interval-unit sec");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* EXP - option */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls traffic-eng Tunnel 1 repeat 1 " "exp 1");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Verbose - option */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls traffic-eng Tunnel 1 repeat 1 verbose");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Force explicit NULL - option */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls traffic-eng Tunnel 1 repeat 1 "
                      "force-explicit-null");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* DSMAP and Interface & Lbl Stack options */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls traffic-eng Tunnel 1 repeat 1 "
                      "dsmap rx-intf-label");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* FEC validate flag - option */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls traffic-eng Tunnel 1 repeat 1 " "flags fec");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Same Sequecne number option */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls traffic-eng Tunnel 1 repeat 1 "
                      "same-seq-num");
    CliGiveAppContext ();
    MGMT_LOCK ();

    /* Burst option */
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("ping mpls traffic-eng Tunnel 1 repeat 1 "
                      "burst size 2");
    CliGiveAppContext ();
    MGMT_LOCK ();

    return OSIX_SUCCESS;
}

#endif

VOID
LsppUtFrameIpUdpHeader (UINT1 **pBuf, UINT1 u1PktType)
{
    t_IP_HEADER         LsppIpHeader;
    tLsppUdpHeader      FrameUdpHdr;

    MEMSET (&FrameUdpHdr, 0, sizeof (tLsppUdpHeader));

    MEMSET (&LsppIpHeader, 0, sizeof (t_IP_HEADER));

    if (u1PktType == LSPP_INVALID_PKT)
    {
        LsppIpHeader.u1Ver_hdrlen = IP_VERS_AND_HLEN (10, 5);
    }
    else if (u1PktType == LSPP_VALID_PKT)
    {
        LsppIpHeader.u1Ver_hdrlen = IP_VERS_AND_HLEN (4, 5);
    }

    LsppIpHeader.u1Tos = 200;
    LsppIpHeader.u2Totlen = OSIX_HTONS ((UINT2) (64 + IP_HDR_LEN +
                                                 LSPP_UDP_HDR_LEN));
    LsppIpHeader.u2Id = 0;
    LsppIpHeader.u2Fl_offs = 0;
    LsppIpHeader.u1Ttl = 1;
    LsppIpHeader.u1Proto = IPPROTO_UDP;
    LsppIpHeader.u2Cksum = 0;
    if (u1PktType == LSPP_INVALID_PKT)
    {
        LsppIpHeader.u4Src = OSIX_HTONL (0x7f000001);
    }
    else if (u1PktType == LSPP_VALID_PKT)
    {
        LsppIpHeader.u4Src = OSIX_HTONL (0x0a000001);
    }

    LsppIpHeader.u4Dest = OSIX_HTONL (0x7f000001);

    MEMCPY (*pBuf, &LsppIpHeader, 20);

    if (u1PktType == LSPP_VALID_PKT)
    {
        FrameUdpHdr.u2Dest_u_port = OSIX_HTONS (0x0daf);
        FrameUdpHdr.u2Src_u_port = OSIX_HTONS (0x0daf);
    }
    else if (u1PktType == LSPP_INVALID_PKT)
    {
        FrameUdpHdr.u2Dest_u_port = OSIX_HTONS (0x0d0d);
        FrameUdpHdr.u2Src_u_port = OSIX_HTONS (0x0d0d);
    }

    FrameUdpHdr.u2Len = OSIX_HTONS ((UINT2) (56));

    MEMCPY (*pBuf, &FrameUdpHdr, 8);

    return;
}

VOID
LsppUtFrameHeader (UINT1 **pBuf, UINT1 u1MsgType, UINT1 u1PktType)
{
    tLsppHeader         LsppHeader;

    MEMSET (&LsppHeader, 0, sizeof (tLsppHeader));

    if (u1PktType == LSPP_VALID_PKT)
    {
        LsppHeader.u2Version = 1;
    }
    else if (u1PktType == LSPP_INVALID_PKT)
    {
        LsppHeader.u2Version = 5;
    }

    LsppHeader.u2GlobalFlag = 0;
    LsppHeader.u1MessageType = u1MsgType;

    LsppHeader.u1ReplyMode = 3;
    LsppHeader.u4SenderHandle = 1;
    LsppHeader.u4SequenceNum = 1;
    LsppHeader.u4TimeStampSentInSec = 60;
    LsppHeader.u4TimeStampSentInMicroSec = 0;
    LsppHeader.u4TimeStampRxInSec = 0;
    LsppHeader.u4TimeStampRxInMicroSec = 0;

    if (u1MsgType == LSPP_ECHO_REQUEST)
    {
        LsppHeader.u1ReturnCode = 0;
        LsppHeader.u1ReturnSubCode = 0;
    }
    else if (u1MsgType == LSPP_ECHO_REPLY)
    {
        LsppHeader.u1ReturnCode = 1;
        LsppHeader.u1ReturnSubCode = 0;
    }

    MEMCPY (*pBuf, &LsppHeader.u2Version, 2);
    *pBuf = *pBuf + 2;

    MEMCPY (*pBuf, &LsppHeader.u2GlobalFlag, 2);
    *pBuf = *pBuf + 2;

    MEMCPY (*pBuf, &LsppHeader.u1MessageType, 1);
    *pBuf = *pBuf + 1;

    MEMCPY (*pBuf, &LsppHeader.u1ReplyMode, 1);
    *pBuf = *pBuf + 1;

    MEMCPY (*pBuf, &LsppHeader.u1ReturnCode, 1);
    *pBuf = *pBuf + 1;

    MEMCPY (*pBuf, &LsppHeader.u1ReturnSubCode, 1);
    *pBuf = *pBuf + 1;

    MEMCPY (*pBuf, &LsppHeader.u4SenderHandle, 4);
    *pBuf = *pBuf + 4;

    MEMCPY (*pBuf, &LsppHeader.u4SequenceNum, 4);
    *pBuf = *pBuf + 4;

    MEMCPY (*pBuf, &LsppHeader.u4TimeStampSentInSec, 4);
    *pBuf = *pBuf + 4;

    MEMCPY (*pBuf, &LsppHeader.u4TimeStampSentInSec, 4);
    *pBuf = *pBuf + 4;

    MEMCPY (*pBuf, &LsppHeader.u4TimeStampSentInMicroSec, 4);
    *pBuf = *pBuf + 4;

    MEMCPY (*pBuf, &LsppHeader.u4TimeStampRxInSec, 4);
    *pBuf = *pBuf + 4;

    MEMCPY (*pBuf, &LsppHeader.u4TimeStampRxInMicroSec, 4);
    *pBuf = *pBuf + 4;

    return;
}

VOID
LsppUtUdpateHeader (tLsppPduInfo * pLsppPduInfo)
{
    tLsppHeader        *pLsppHeader = NULL;

    pLsppHeader = &(pLsppPduInfo->LsppHeader);

    pLsppHeader->u2Version = 1;
    pLsppHeader->u2GlobalFlag = 0;
    pLsppHeader->u1MessageType = 1;
    pLsppHeader->u1ReplyMode = 2;
    pLsppHeader->u4SenderHandle = 1;
    pLsppHeader->u4SequenceNum = 1;
    pLsppHeader->u4TimeStampSentInSec = 1000;
    pLsppHeader->u4TimeStampSentInMicroSec = 0;

    return;
}

VOID
LsppUtUdpateTgtFec (tLsppPduInfo * pLsppPduInfo, UINT1 u1PktType)
{
    UINT1               u1FecStackDepth = 0;

    /* Updating FEC stack Tlv is present and Updating an Invalid
     * SubTlvType */
    pLsppPduInfo->u2FecStackTlvLength = 100;
    pLsppPduInfo->u1FecStackDepth = 1;

    if (u1PktType == LSPP_INVALID_PKT)
    {
        pLsppPduInfo->LsppFecTlv[u1FecStackDepth].u2TlvType = 100;
    }
    else if (u1PktType == LSPP_VALID_PKT)
    {

        pLsppPduInfo->LsppFecTlv[u1FecStackDepth].u2TlvType = 1;
    }

    pLsppPduInfo->u2TlvsPresent =
        pLsppPduInfo->u2TlvsPresent | LSPP_TARGET_FEC_TLV;
}

VOID
LsppUtFillPathId (tLsppPathId * pLsppPathId, UINT1 u1PathType)
{
    switch (u1PathType)
    {
        case LSPP_PATH_TYPE_MEP:
            pLsppPathId->unPathId.MepIndices.u4MegIndex = 1;
            pLsppPathId->unPathId.MepIndices.u4MpIndex = 1;
            pLsppPathId->unPathId.MepIndices.u4MeIndex = 1;
            break;
    }
    return;
}

INT4
LsppUtUtl_1 (VOID)
{
    tLsppFsLsppGlobalConfigTableEntry LsppOldFsLsppGlobalConfigTable;
    tLsppFsLsppGlobalConfigTableEntry LsppFsLsppGlobalConfigTable;
    tLsppIsSetFsLsppGlobalConfigTableEntry LsppIsSetGlobalConfigTable;

    MEMSET (&(LsppOldFsLsppGlobalConfigTable), 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (&(LsppFsLsppGlobalConfigTable), 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (&(LsppIsSetGlobalConfigTable), 0,
            sizeof (tLsppIsSetFsLsppGlobalConfigTableEntry));

    LsppOldFsLsppGlobalConfigTable.MibObject.u4FsLsppContextId = 0;
    LsppOldFsLsppGlobalConfigTable.MibObject.i4FsLsppSystemControl = LSPP_START;
    LsppOldFsLsppGlobalConfigTable.MibObject.u4FsLsppAgeOutTime = 0;
    LsppOldFsLsppGlobalConfigTable.MibObject.i4FsLsppTrapStatus = 0;
    LsppOldFsLsppGlobalConfigTable.MibObject.i4FsLsppTraceLevel = 0;
    LsppOldFsLsppGlobalConfigTable.MibObject.i4FsLsppAgeOutTmrUnit =
        LSPP_TMR_UNIT_MIN;
    LsppOldFsLsppGlobalConfigTable.MibObject.i4FsLsppClearEchoStats =
        LSPP_SNMP_FALSE;
    LsppOldFsLsppGlobalConfigTable.MibObject.i4FsLsppBfdBtStrapRespReq =
        LSPP_SNMP_TRUE;

    LsppFsLsppGlobalConfigTable.MibObject.u4FsLsppContextId = 1;
    LsppFsLsppGlobalConfigTable.MibObject.i4FsLsppClearEchoStats =
        LSPP_SNMP_TRUE;
    LsppIsSetGlobalConfigTable.bFsLsppClearEchoStats = OSIX_TRUE;

    if (LsppUtilUpdateFsLsppGlobalConfigTable (&LsppOldFsLsppGlobalConfigTable,
                                               &LsppFsLsppGlobalConfigTable,
                                               &LsppIsSetGlobalConfigTable) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppFsLsppGlobalConfigTable), 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (&(LsppIsSetGlobalConfigTable), 0,
            sizeof (tLsppIsSetFsLsppGlobalConfigTableEntry));

    LsppFsLsppGlobalConfigTable.MibObject.u4FsLsppContextId = 1;
    LsppFsLsppGlobalConfigTable.MibObject.i4FsLsppSystemControl = LSPP_SHUTDOWN;
    LsppIsSetGlobalConfigTable.bFsLsppSystemControl = OSIX_TRUE;

    if (LsppUtilUpdateFsLsppGlobalConfigTable (&LsppOldFsLsppGlobalConfigTable,
                                               &LsppFsLsppGlobalConfigTable,
                                               &LsppIsSetGlobalConfigTable) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppFsLsppGlobalConfigTable), 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (&(LsppIsSetGlobalConfigTable), 0,
            sizeof (tLsppIsSetFsLsppGlobalConfigTableEntry));

    LsppFsLsppGlobalConfigTable.MibObject.u4FsLsppContextId = 1;
    LsppFsLsppGlobalConfigTable.MibObject.i4FsLsppSystemControl = LSPP_START;
    LsppIsSetGlobalConfigTable.bFsLsppSystemControl = OSIX_TRUE;

    if (LsppUtilUpdateFsLsppGlobalConfigTable (&LsppOldFsLsppGlobalConfigTable,
                                               &LsppFsLsppGlobalConfigTable,
                                               &LsppIsSetGlobalConfigTable) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtUtl_2 (VOID)
{

    tLsppFsLsppPingTraceTableEntry LsppOldFsLsppPingTraceTable;
    tLsppFsLsppPingTraceTableEntry LsppFsLsppPingTraceTable;
    tLsppIsSetFsLsppPingTraceTableEntry LsppIsSetPingTraceTable;

    MEMSET (&(LsppOldFsLsppPingTraceTable), 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppFsLsppPingTraceTable), 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppFsLsppPingTraceTable.MibObject.i4FsLsppRowStatus = CREATE_AND_GO;
    LsppFsLsppPingTraceTable.MibObject.i4FsLsppRequestType = LSPP_REQ_TYPE_PING;
    LsppFsLsppPingTraceTable.MibObject.i4FsLsppPathPointerLen = 13;

    if (LsppUtilUpdateFsLsppPingTraceTable (NULL, &LsppFsLsppPingTraceTable,
                                            &LsppIsSetPingTraceTable) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    LsppFsLsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    if (LsppUtilUpdateFsLsppPingTraceTable (NULL, &LsppFsLsppPingTraceTable,
                                            &LsppIsSetPingTraceTable) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    LsppFsLsppPingTraceTable.MibObject.i4FsLsppPathType = 10;

    if (LsppUtilUpdateFsLsppPingTraceTable (NULL, &LsppFsLsppPingTraceTable,
                                            &LsppIsSetPingTraceTable) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    LsppFsLsppPingTraceTable.MibObject.i4FsLsppPathType =
        LSPP_PATH_TYPE_LDP_IPV4;

    LsppFsLsppPingTraceTable.MibObject.i4FsLsppPathPointerLen = 10;

    if (LsppUtilUpdateFsLsppPingTraceTable (NULL, &LsppFsLsppPingTraceTable,
                                            &LsppIsSetPingTraceTable) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    LsppFsLsppPingTraceTable.MibObject.i4FsLsppPathType =
        LSPP_PATH_TYPE_LDP_IPV6;

    LsppFsLsppPingTraceTable.MibObject.i4FsLsppPathPointerLen = 10;

    if (LsppUtilUpdateFsLsppPingTraceTable (NULL, &LsppFsLsppPingTraceTable,
                                            &LsppIsSetPingTraceTable) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    LsppFsLsppPingTraceTable.MibObject.i4FsLsppPathType =
        LSPP_PATH_TYPE_FEC_128;

    LsppFsLsppPingTraceTable.MibObject.i4FsLsppPathPointerLen = 10;

    if (LsppUtilUpdateFsLsppPingTraceTable (NULL, &LsppFsLsppPingTraceTable,
                                            &LsppIsSetPingTraceTable) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    LsppFsLsppPingTraceTable.MibObject.i4FsLsppPathType = LSPP_PATH_TYPE_MEP;

    LsppFsLsppPingTraceTable.MibObject.i4FsLsppPathPointerLen = 10;

    if (LsppUtilUpdateFsLsppPingTraceTable (NULL, &LsppFsLsppPingTraceTable,
                                            &LsppIsSetPingTraceTable) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    LsppFsLsppPingTraceTable.MibObject.i4FsLsppRowStatus = DESTROY;
    LsppFsLsppPingTraceTable.MibObject.i4FsLsppRequestOwner =
        LSPP_REQ_OWNER_BFD;

    if (LsppUtilUpdateFsLsppPingTraceTable (NULL, &LsppFsLsppPingTraceTable,
                                            &LsppIsSetPingTraceTable) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtDb_1 (VOID)
{
    tLsppFsLsppGlobalConfigTableEntry LsppGlobalConfigTable;
    tLsppIsSetFsLsppGlobalConfigTableEntry LsppIsSetFsLsppGlobalConfigTable;

    MEMSET (&(LsppGlobalConfigTable), 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (&LsppIsSetFsLsppGlobalConfigTable, 0,
            sizeof (tLsppIsSetFsLsppGlobalConfigTableEntry));

    LsppGlobalConfigTable.MibObject.u4FsLsppContextId = 1;
    LsppGlobalConfigTable.MibObject.i4FsLsppTrapStatus =
        LSPP_PING_COMPLETION_TRAP;

    if (LsppDbGlobalConfigInitialTest
        (&LsppGlobalConfigTable,
         &LsppIsSetFsLsppGlobalConfigTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    LsppGlobalConfigTable.MibObject.i4FsLsppSystemControl = LSPP_START;
    LsppIsSetFsLsppGlobalConfigTable.bFsLsppSystemControl = OSIX_TRUE;

    if (LsppDbGlobalConfigInitialTest
        (&LsppGlobalConfigTable,
         &LsppIsSetFsLsppGlobalConfigTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtDb_2 (VOID)
{
    tLsppFsLsppGlobalConfigTableEntry LsppFsLsppGlobalConfigTable;
    tLsppIsSetFsLsppGlobalConfigTableEntry LsppIsSetGlobalConfigTable;
    tLsppFsLsppGlobalConfigTableEntry *pLsppGlobalConfigTable = NULL;
    UINT4               u4ErrorCode = 0;

    MEMSET (&(LsppFsLsppGlobalConfigTable), 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (&(LsppIsSetGlobalConfigTable), 0,
            sizeof (tLsppIsSetFsLsppGlobalConfigTableEntry));

    LsppFsLsppGlobalConfigTable.MibObject.u4FsLsppContextId = 1;
    LsppIsSetGlobalConfigTable.bFsLsppSystemControl = OSIX_TRUE;
    LsppFsLsppGlobalConfigTable.MibObject.i4FsLsppSystemControl =
        LSPP_START - 1;

    if (LsppTestAllFsLsppGlobalConfigTable (&u4ErrorCode,
                                            &LsppFsLsppGlobalConfigTable,
                                            &LsppIsSetGlobalConfigTable) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppFsLsppGlobalConfigTable), 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (&(LsppIsSetGlobalConfigTable), 0,
            sizeof (tLsppIsSetFsLsppGlobalConfigTableEntry));

    LsppIsSetGlobalConfigTable.bFsLsppTrapStatus = OSIX_TRUE;
    LsppFsLsppGlobalConfigTable.MibObject.i4FsLsppTrapStatus = -1;

    pLsppGlobalConfigTable =
        LsppFsLsppGlobalConfigTableCreateApi (&LsppFsLsppGlobalConfigTable);

    if (pLsppGlobalConfigTable == NULL)
    {
        return OSIX_FAILURE;
    }

    if (LsppTestAllFsLsppGlobalConfigTable (&u4ErrorCode,
                                            &LsppFsLsppGlobalConfigTable,
                                            &LsppIsSetGlobalConfigTable) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppFsLsppGlobalConfigTable), 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (&(LsppIsSetGlobalConfigTable), 0,
            sizeof (tLsppIsSetFsLsppGlobalConfigTableEntry));

    LsppIsSetGlobalConfigTable.bFsLsppTrapStatus = OSIX_TRUE;
    LsppFsLsppGlobalConfigTable.MibObject.i4FsLsppTrapStatus =
        LSPP_ALL_TRAP_ENABLED + 1;

    if (LsppTestAllFsLsppGlobalConfigTable (&u4ErrorCode,
                                            &LsppFsLsppGlobalConfigTable,
                                            &LsppIsSetGlobalConfigTable) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppFsLsppGlobalConfigTable), 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (&(LsppIsSetGlobalConfigTable), 0,
            sizeof (tLsppIsSetFsLsppGlobalConfigTableEntry));

    LsppIsSetGlobalConfigTable.bFsLsppTraceLevel = OSIX_TRUE;
    LsppFsLsppGlobalConfigTable.MibObject.i4FsLsppTraceLevel = -1;

    if (LsppTestAllFsLsppGlobalConfigTable (&u4ErrorCode,
                                            &LsppFsLsppGlobalConfigTable,
                                            &LsppIsSetGlobalConfigTable) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppFsLsppGlobalConfigTable), 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (&(LsppIsSetGlobalConfigTable), 0,
            sizeof (tLsppIsSetFsLsppGlobalConfigTableEntry));

    LsppIsSetGlobalConfigTable.bFsLsppTraceLevel = OSIX_TRUE;
    LsppFsLsppGlobalConfigTable.MibObject.i4FsLsppTraceLevel =
        LSPP_ALL_TRACES_ENABLED + 1;

    if (LsppTestAllFsLsppGlobalConfigTable (&u4ErrorCode,
                                            &LsppFsLsppGlobalConfigTable,
                                            &LsppIsSetGlobalConfigTable) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppFsLsppGlobalConfigTable), 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (&(LsppIsSetGlobalConfigTable), 0,
            sizeof (tLsppIsSetFsLsppGlobalConfigTableEntry));

    LsppIsSetGlobalConfigTable.bFsLsppAgeOutTime = OSIX_TRUE;
    LsppFsLsppGlobalConfigTable.MibObject.u4FsLsppAgeOutTime =
        LSPP_MAX_AGE_OUT_TIME + 1;

    if (LsppTestAllFsLsppGlobalConfigTable (&u4ErrorCode,
                                            &LsppFsLsppGlobalConfigTable,
                                            &LsppIsSetGlobalConfigTable) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppFsLsppGlobalConfigTable), 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (&(LsppIsSetGlobalConfigTable), 0,
            sizeof (tLsppIsSetFsLsppGlobalConfigTableEntry));

    LsppIsSetGlobalConfigTable.bFsLsppAgeOutTmrUnit = OSIX_TRUE;
    LsppFsLsppGlobalConfigTable.MibObject.i4FsLsppAgeOutTmrUnit =
        LSPP_TMR_UNIT_MIN + 1;

    if (LsppTestAllFsLsppGlobalConfigTable (&u4ErrorCode,
                                            &LsppFsLsppGlobalConfigTable,
                                            &LsppIsSetGlobalConfigTable) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppFsLsppGlobalConfigTable), 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (&(LsppIsSetGlobalConfigTable), 0,
            sizeof (tLsppIsSetFsLsppGlobalConfigTableEntry));

    LsppIsSetGlobalConfigTable.bFsLsppAgeOutTmrUnit = OSIX_TRUE;
    LsppFsLsppGlobalConfigTable.MibObject.i4FsLsppAgeOutTmrUnit =
        LSPP_TMR_UNIT_MILLISEC - 1;

    if (LsppTestAllFsLsppGlobalConfigTable (&u4ErrorCode,
                                            &LsppFsLsppGlobalConfigTable,
                                            &LsppIsSetGlobalConfigTable) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppFsLsppGlobalConfigTable), 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (&(LsppIsSetGlobalConfigTable), 0,
            sizeof (tLsppIsSetFsLsppGlobalConfigTableEntry));

    LsppIsSetGlobalConfigTable.bFsLsppClearEchoStats = OSIX_TRUE;
    LsppFsLsppGlobalConfigTable.MibObject.i4FsLsppClearEchoStats =
        LSPP_SNMP_FALSE + 1;

    if (LsppTestAllFsLsppGlobalConfigTable (&u4ErrorCode,
                                            &LsppFsLsppGlobalConfigTable,
                                            &LsppIsSetGlobalConfigTable) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppFsLsppGlobalConfigTable), 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (&(LsppIsSetGlobalConfigTable), 0,
            sizeof (tLsppIsSetFsLsppGlobalConfigTableEntry));

    LsppIsSetGlobalConfigTable.bFsLsppBfdBtStrapRespReq = OSIX_TRUE;
    LsppFsLsppGlobalConfigTable.MibObject.i4FsLsppBfdBtStrapRespReq =
        LSPP_SNMP_FALSE + 1;

    if (LsppTestAllFsLsppGlobalConfigTable (&u4ErrorCode,
                                            &LsppFsLsppGlobalConfigTable,
                                            &LsppIsSetGlobalConfigTable) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;

}

INT4
LsppUtDb_3 (VOID)
{
    tLsppFsLsppPingTraceTableEntry LsppFsLsppPingTraceTable;
    tLsppIsSetFsLsppPingTraceTableEntry LsppIsSetPingTraceTable;
    tLsppFsLsppGlobalConfigTableEntry LsppGlobalConfigTable;
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTable = NULL;
    UINT4               u4SenderHandle = 0;

    MEMSET (&(LsppFsLsppPingTraceTable), 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));
    MEMSET (&(LsppGlobalConfigTable), 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));

    LsppFsLsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    if (LsppDbPingTraceInitialTest (&LsppFsLsppPingTraceTable,
                                    &LsppIsSetPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (LsppGetAllFsLsppGlobalConfigTable (&LsppGlobalConfigTable) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    IndexManagerSetNextFreeIndex (LsppGlobalConfigTable.IndexMgrId,
                                  &u4SenderHandle);

    LsppFsLsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppFsLsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    pLsppFsLsppPingTraceTable =
        LsppFsLsppPingTraceTableCreateApi (&LsppFsLsppPingTraceTable);

    if (pLsppFsLsppPingTraceTable == NULL)
    {
        return OSIX_FAILURE;
    }

    LsppFsLsppPingTraceTable.MibObject.i4FsLsppRowStatus = NOT_READY;
    LsppIsSetPingTraceTable.bFsLsppRowStatus = OSIX_TRUE;

    if (LsppDbPingTraceInitialTest (pLsppFsLsppPingTraceTable,
                                    &LsppIsSetPingTraceTable) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);

    MEMSET (&(LsppFsLsppPingTraceTable), 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppFsLsppPingTraceTable.MibObject.u4FsLsppContextId = 1;
    LsppFsLsppPingTraceTable.MibObject.i4FsLsppRowStatus = CREATE_AND_WAIT;

    if (LsppDbPingTraceInitialTest (pLsppFsLsppPingTraceTable,
                                    &LsppIsSetPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
LsppUtDb_4 (VOID)
{
    tLsppFsLsppPingTraceTableEntry LsppPingTraceTable;
    tLsppIsSetFsLsppPingTraceTableEntry LsppIsSetPingTraceTable;
    tLsppFsLsppGlobalConfigTableEntry LsppGlobalConfigTable;
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTable = NULL;
    UINT4               u4ErrorCode = 0;
    UINT4               u4SenderHandle = 0;

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));
    MEMSET (&(LsppGlobalConfigTable), 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (LsppGetAllFsLsppGlobalConfigTable (&LsppGlobalConfigTable) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    IndexManagerSetNextFreeIndex (LsppGlobalConfigTable.IndexMgrId,
                                  &u4SenderHandle);

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;

    pLsppFsLsppPingTraceTable =
        LsppFsLsppPingTraceTableCreateApi (&LsppPingTraceTable);

    if (pLsppFsLsppPingTraceTable == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.i4FsLsppRequestType =
        LSPP_REQ_TYPE_TRACE_ROUTE + 1;
    LsppIsSetPingTraceTable.bFsLsppRequestType = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.i4FsLsppPathType = LSPP_PATH_TYPE_MEP + 1;
    LsppIsSetPingTraceTable.bFsLsppPathType = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.i4FsLsppPathType = LSPP_PATH_TYPE_LDP_IPV4 - 1;
    LsppIsSetPingTraceTable.bFsLsppPathType = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.i4FsLsppPathType = LSPP_PATH_TYPE_RSVP_IPV4;
    LsppIsSetPingTraceTable.bFsLsppPathPointer = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.u4FsLsppTgtMipGlobalId = 0;
    LsppIsSetPingTraceTable.bFsLsppTgtMipGlobalId = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.u4FsLsppTgtMipNodeId = 0;
    LsppIsSetPingTraceTable.bFsLsppTgtMipNodeId = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.u4FsLsppTgtMipIfNum = 0;
    LsppIsSetPingTraceTable.bFsLsppTgtMipIfNum = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.i4FsLsppReplyMode =
        LSPP_REPLY_APPLICATION_CC + 1;
    LsppIsSetPingTraceTable.bFsLsppReplyMode = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.i4FsLsppReplyMode = LSPP_NO_REPLY - 1;
    LsppIsSetPingTraceTable.bFsLsppReplyMode = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.u4FsLsppRepeatCount =
        LSPP_MAX_REPEAT_COUNT + 1;
    LsppIsSetPingTraceTable.bFsLsppRepeatCount = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.u4FsLsppRepeatCount =
        LSPP_MIN_REPEAT_COUNT - 1;
    LsppIsSetPingTraceTable.bFsLsppRepeatCount = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.u4FsLsppPacketSize = LSPP_MAX_PACKET_SIZE + 1;
    LsppIsSetPingTraceTable.bFsLsppPacketSize = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.u4FsLsppPacketSize = LSPP_MIN_PACKET_SIZE - 1;
    LsppIsSetPingTraceTable.bFsLsppPacketSize = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.i4FsLsppSweepOption = LSPP_SNMP_TRUE;
    LsppPingTraceTable.MibObject.u4FsLsppPacketSize = LSPP_MIN_PACKET_SIZE;
    LsppIsSetPingTraceTable.bFsLsppPacketSize = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_TRUE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    pLsppFsLsppPingTraceTable->MibObject.i4FsLsppSweepOption = LSPP_SNMP_TRUE;
    LsppPingTraceTable.MibObject.u4FsLsppPacketSize = LSPP_MIN_PACKET_SIZE;
    LsppIsSetPingTraceTable.bFsLsppPacketSize = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    STRCPY (LsppPingTraceTable.MibObject.au1FsLsppPadPattern, "xyz");
    LsppPingTraceTable.MibObject.i4FsLsppPadPatternLen = 3;
    LsppIsSetPingTraceTable.bFsLsppPadPattern = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.u4FsLsppTTLValue = LSPP_MAX_TTL + 1;
    LsppIsSetPingTraceTable.bFsLsppTTLValue = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.u4FsLsppTTLValue = LSPP_MIN_TTL - 1;
    LsppIsSetPingTraceTable.bFsLsppTTLValue = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.u4FsLsppWFRInterval = LSPP_MAX_WFR_TIME + 1;
    LsppIsSetPingTraceTable.bFsLsppWFRInterval = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.u4FsLsppWFRInterval = LSPP_MIN_WFR_TIME - 1;
    LsppIsSetPingTraceTable.bFsLsppWFRInterval = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.i4FsLsppWFRTmrUnit = LSPP_TMR_UNIT_MIN + 1;
    LsppIsSetPingTraceTable.bFsLsppWFRTmrUnit = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.i4FsLsppWFRTmrUnit =
        LSPP_TMR_UNIT_MILLISEC - 1;

    LsppIsSetPingTraceTable.bFsLsppWFRTmrUnit = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.u4FsLsppWTSInterval = LSPP_MAX_WTS_TIME + 1;
    LsppIsSetPingTraceTable.bFsLsppWTSInterval = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.i4FsLsppRequestType =
        LSPP_REQ_TYPE_TRACE_ROUTE;
    LsppPingTraceTable.MibObject.u4FsLsppWTSInterval = LSPP_MAX_WTS_TIME;
    LsppIsSetPingTraceTable.bFsLsppWTSInterval = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_TRUE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.i4FsLsppRequestType =
        LSPP_REQ_TYPE_TRACE_ROUTE;
    LsppPingTraceTable.MibObject.u4FsLsppWTSInterval = LSPP_MAX_WTS_TIME;
    LsppIsSetPingTraceTable.bFsLsppWTSInterval = OSIX_TRUE;

    pLsppFsLsppPingTraceTable->MibObject.i4FsLsppRequestType =
        LSPP_REQ_TYPE_TRACE_ROUTE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.i4FsLsppWTSTmrUnit = LSPP_TMR_UNIT_MIN + 1;
    LsppIsSetPingTraceTable.bFsLsppWTSTmrUnit = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.i4FsLsppWTSTmrUnit =
        LSPP_TMR_UNIT_MILLISEC - 1;
    LsppIsSetPingTraceTable.bFsLsppWTSTmrUnit = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.u4FsLsppReplyDscpValue =
        LSPP_MAX_DSCP_VALUE + 1;
    LsppIsSetPingTraceTable.bFsLsppReplyDscpValue = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.i4FsLsppSweepOption = LSPP_SNMP_FALSE + 1;
    LsppIsSetPingTraceTable.bFsLsppSweepOption = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.u4FsLsppSweepMinimum =
        LSPP_MAX_SWEEP_START_VALUE + 1;
    LsppIsSetPingTraceTable.bFsLsppSweepMinimum = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.u4FsLsppSweepMinimum =
        LSPP_MIN_SWEEP_START_VALUE - 1;
    LsppIsSetPingTraceTable.bFsLsppSweepMinimum = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.u4FsLsppSweepMaximum =
        LSPP_MAX_SWEEP_END_VALUE + 1;
    LsppIsSetPingTraceTable.bFsLsppSweepMaximum = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.u4FsLsppSweepMaximum =
        LSPP_MIN_SWEEP_END_VALUE - 1;
    LsppIsSetPingTraceTable.bFsLsppSweepMaximum = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.u4FsLsppSweepIncrement =
        LSPP_MAX_SWEEP_INCREMENT_VALUE + 1;
    LsppIsSetPingTraceTable.bFsLsppSweepIncrement = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.u4FsLsppSweepIncrement =
        LSPP_MIN_SWEEP_INCREMENT_VALUE + 1;
    LsppIsSetPingTraceTable.bFsLsppSweepIncrement = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.i4FsLsppBurstOption = LSPP_SNMP_FALSE + 1;
    LsppIsSetPingTraceTable.bFsLsppBurstOption = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.u4FsLsppBurstSize = LSPP_MAX_BURST_SIZE + 1;
    LsppIsSetPingTraceTable.bFsLsppBurstSize = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.u4FsLsppEXPValue = LSPP_MAX_EXP_VALUE + 1;
    LsppIsSetPingTraceTable.bFsLsppEXPValue = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.i4FsLsppDsMap = LSPP_SNMP_FALSE + 1;
    LsppIsSetPingTraceTable.bFsLsppDsMap = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.i4FsLsppFecValidate = LSPP_SNMP_FALSE + 1;
    LsppIsSetPingTraceTable.bFsLsppFecValidate = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.i4FsLsppReplyPadTlv = LSPP_SNMP_FALSE + 1;
    LsppIsSetPingTraceTable.bFsLsppReplyPadTlv = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.i4FsLsppForceExplicitNull =
        LSPP_SNMP_FALSE + 1;
    LsppIsSetPingTraceTable.bFsLsppForceExplicitNull = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.i4FsLsppInterfaceLabelTlv =
        LSPP_SNMP_FALSE + 1;
    LsppIsSetPingTraceTable.bFsLsppInterfaceLabelTlv = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.i4FsLsppSameSeqNumOption = LSPP_SNMP_FALSE + 1;
    LsppIsSetPingTraceTable.bFsLsppSameSeqNumOption = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.i4FsLsppVerbose = LSPP_SNMP_FALSE + 1;
    LsppIsSetPingTraceTable.bFsLsppVerbose = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.i4FsLsppReversePathVerify =
        LSPP_SNMP_FALSE + 1;
    LsppIsSetPingTraceTable.bFsLsppReversePathVerify = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    LsppPingTraceTable.MibObject.i4FsLsppEncapType =
        LSPP_ENCAP_TYPE_VCCV_NEGOTIATED + 1;
    LsppIsSetPingTraceTable.bFsLsppEncapType = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = DESTROY + 1;
    LsppIsSetPingTraceTable.bFsLsppRowStatus = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppPingTraceTable.MibObject.i4FsLsppRequestType =
        LSPP_REQ_TYPE_TRACE_ROUTE;
    LsppPingTraceTable.MibObject.i4FsLsppReversePathVerify = LSPP_SNMP_TRUE;
    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    if (LsppTestAllFsLsppPingTraceTable (&u4ErrorCode,
                                         &LsppPingTraceTable,
                                         &LsppIsSetPingTraceTable, OSIX_FALSE,
                                         OSIX_FALSE) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }
    LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
    return OSIX_SUCCESS;
}

INT4
LsppUtDb_5 (VOID)
{
    tLsppFsLsppPingTraceTableEntry LsppPingTraceTable;
    tLsppIsSetFsLsppPingTraceTableEntry LsppIsSetPingTraceTable;
    tLsppFsLsppGlobalConfigTableEntry LsppGlobalConfigTable;
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTable = NULL;
    UINT4               u4SenderHandle = 0;

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppIsSetPingTraceTable), 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));
    MEMSET (&(LsppGlobalConfigTable), 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = ACTIVE;

    if (LsppDbPingTraceDependencyTest (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (LsppGetAllFsLsppGlobalConfigTable (&LsppGlobalConfigTable) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    IndexManagerSetNextFreeIndex (LsppGlobalConfigTable.IndexMgrId,
                                  &u4SenderHandle);

    LsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;

    pLsppFsLsppPingTraceTable =
        LsppFsLsppPingTraceTableCreateApi (&LsppPingTraceTable);

    if (pLsppFsLsppPingTraceTable == NULL)
    {
        return OSIX_FAILURE;
    }

    LsppPingTraceTable.MibObject.i4FsLsppRequestType =
        LSPP_REQ_TYPE_TRACE_ROUTE;

    LsppPingTraceTable.MibObject.i4FsLsppReversePathVerify = LSPP_SNMP_TRUE;

    if (LsppDbPingTraceDependencyTest (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);

    LsppPingTraceTable.MibObject.i4FsLsppRowStatus = CREATE_AND_GO;

    if (LsppDbPingTraceDependencyTest (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
LsppUtDb_6 (VOID)
{
    tLsppFsLsppPingTraceTableEntry LsppPingTraceTable;

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRequestType =
        LSPP_REQ_TYPE_TRACE_ROUTE;

    LsppPingTraceTable.MibObject.i4FsLsppReversePathVerify = LSPP_SNMP_TRUE;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRequestType =
        LSPP_REQ_TYPE_TRACE_ROUTE;

    LsppPingTraceTable.MibObject.i4FsLsppDsMap = LSPP_SNMP_FALSE;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRequestType =
        LSPP_REQ_TYPE_TRACE_ROUTE;

    LsppPingTraceTable.MibObject.i4FsLsppVerbose = LSPP_SNMP_TRUE;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRequestType =
        LSPP_REQ_TYPE_TRACE_ROUTE;

    LsppPingTraceTable.MibObject.i4FsLsppSameSeqNumOption = LSPP_SNMP_TRUE;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRequestType =
        LSPP_REQ_TYPE_TRACE_ROUTE;

    LsppPingTraceTable.MibObject.i4FsLsppSweepOption = LSPP_SNMP_TRUE;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRequestType =
        LSPP_REQ_TYPE_TRACE_ROUTE;

    LsppPingTraceTable.MibObject.i4FsLsppBurstOption = LSPP_SNMP_TRUE;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRequestType =
        LSPP_REQ_TYPE_TRACE_ROUTE;

    LsppPingTraceTable.MibObject.u4FsLsppTgtMipGlobalId = 1;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRequestType =
        LSPP_REQ_TYPE_TRACE_ROUTE;

    LsppPingTraceTable.MibObject.u4FsLsppTgtMipNodeId = 1;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRequestType =
        LSPP_REQ_TYPE_TRACE_ROUTE;

    LsppPingTraceTable.MibObject.u4FsLsppTgtMipIfNum = 1;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRequestType =
        LSPP_REQ_TYPE_TRACE_ROUTE;

    LsppPingTraceTable.MibObject.u4FsLsppTgtMipIfNum = 1;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRequestType = LSPP_REQ_TYPE_PING;

    LsppPingTraceTable.MibObject.i4FsLsppDsMap = LSPP_SNMP_FALSE;
    LsppPingTraceTable.MibObject.i4FsLsppInterfaceLabelTlv = LSPP_SNMP_TRUE;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRequestType = LSPP_REQ_TYPE_PING;

    LsppPingTraceTable.MibObject.i4FsLsppDsMap = LSPP_SNMP_FALSE;
    LsppPingTraceTable.MibObject.i4FsLsppInterfaceLabelTlv = LSPP_SNMP_TRUE;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRequestType = LSPP_REQ_TYPE_PING;

    LsppPingTraceTable.MibObject.i4FsLsppSameSeqNumOption = LSPP_SNMP_TRUE;
    LsppPingTraceTable.MibObject.i4FsLsppSweepOption = LSPP_SNMP_TRUE;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRequestType = LSPP_REQ_TYPE_PING;

    LsppPingTraceTable.MibObject.i4FsLsppSameSeqNumOption = LSPP_SNMP_TRUE;
    LsppPingTraceTable.MibObject.i4FsLsppBurstOption = LSPP_SNMP_TRUE;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRequestType = LSPP_REQ_TYPE_PING;

    LsppPingTraceTable.MibObject.i4FsLsppSweepOption = LSPP_SNMP_TRUE;
    LsppPingTraceTable.MibObject.i4FsLsppSameSeqNumOption = LSPP_SNMP_TRUE;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRequestType = LSPP_REQ_TYPE_PING;

    LsppPingTraceTable.MibObject.i4FsLsppSweepOption = LSPP_SNMP_TRUE;
    LsppPingTraceTable.MibObject.i4FsLsppBurstOption = LSPP_SNMP_TRUE;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRequestType = LSPP_REQ_TYPE_PING;

    LsppPingTraceTable.MibObject.i4FsLsppBurstOption = LSPP_SNMP_TRUE;
    LsppPingTraceTable.MibObject.i4FsLsppSameSeqNumOption = LSPP_SNMP_TRUE;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRequestType = LSPP_REQ_TYPE_PING;

    LsppPingTraceTable.MibObject.i4FsLsppBurstOption = LSPP_SNMP_TRUE;
    LsppPingTraceTable.MibObject.i4FsLsppSweepOption = LSPP_SNMP_TRUE;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRequestType = LSPP_REQ_TYPE_PING;

    LsppPingTraceTable.MibObject.i4FsLsppPathType = LSPP_PATH_TYPE_RSVP_IPV4;
    LsppPingTraceTable.MibObject.i4FsLsppReversePathVerify = LSPP_SNMP_TRUE;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRequestType = LSPP_REQ_TYPE_PING;

    LsppPingTraceTable.MibObject.i4FsLsppPathType = LSPP_PATH_TYPE_MEP;
    LsppPingTraceTable.MibObject.i4FsLsppReversePathVerify = LSPP_SNMP_TRUE;
    LsppPingTraceTable.MibObject.i4FsLsppReplyMode = LSPP_REPLY_IP_UDP;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRequestType = LSPP_REQ_TYPE_PING;

    LsppPingTraceTable.MibObject.i4FsLsppPathType = LSPP_PATH_TYPE_FEC_128;
    LsppPingTraceTable.MibObject.i4FsLsppForceExplicitNull = LSPP_SNMP_TRUE;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRequestType = LSPP_REQ_TYPE_PING;

    LsppPingTraceTable.MibObject.i4FsLsppPathType = LSPP_PATH_TYPE_FEC_128;
    LsppPingTraceTable.MibObject.u4FsLsppTTLValue = LSPP_MIN_TTL;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRequestType = LSPP_REQ_TYPE_PING;

    LsppPingTraceTable.MibObject.i4FsLsppPathType = LSPP_PATH_TYPE_FEC_129;
    LsppPingTraceTable.MibObject.i4FsLsppForceExplicitNull = LSPP_SNMP_TRUE;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRequestType = LSPP_REQ_TYPE_PING;

    LsppPingTraceTable.MibObject.i4FsLsppPathType = LSPP_PATH_TYPE_FEC_129;
    LsppPingTraceTable.MibObject.u4FsLsppTTLValue = LSPP_MIN_TTL;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRequestType = LSPP_REQ_TYPE_PING;

    LsppPingTraceTable.MibObject.i4FsLsppPathType = LSPP_PATH_TYPE_FEC_129;
    LsppPingTraceTable.MibObject.u4FsLsppTgtMipGlobalId = 1;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRequestType = LSPP_REQ_TYPE_PING;

    LsppPingTraceTable.MibObject.i4FsLsppPathType = LSPP_PATH_TYPE_FEC_129;
    LsppPingTraceTable.MibObject.u4FsLsppTgtMipNodeId = 1;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRequestType = LSPP_REQ_TYPE_PING;

    LsppPingTraceTable.MibObject.i4FsLsppPathType = LSPP_PATH_TYPE_FEC_129;
    LsppPingTraceTable.MibObject.u4FsLsppTgtMipIfNum = 1;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRequestType = LSPP_REQ_TYPE_PING;

    LsppPingTraceTable.MibObject.i4FsLsppPathType = LSPP_PATH_TYPE_MEP;
    LsppPingTraceTable.MibObject.u4FsLsppTgtMipGlobalId = 0;
    LsppPingTraceTable.MibObject.u4FsLsppTgtMipIfNum = 1;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRequestType = LSPP_REQ_TYPE_PING;

    LsppPingTraceTable.MibObject.i4FsLsppPathType = LSPP_PATH_TYPE_MEP;
    LsppPingTraceTable.MibObject.u4FsLsppTgtMipGlobalId = 0;
    LsppPingTraceTable.MibObject.u4FsLsppTgtMipNodeId = 1;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRequestType = LSPP_REQ_TYPE_PING;

    LsppPingTraceTable.MibObject.i4FsLsppPathType = LSPP_PATH_TYPE_MEP;
    LsppPingTraceTable.MibObject.u4FsLsppTgtMipGlobalId = 1;
    LsppPingTraceTable.MibObject.u4FsLsppTgtMipNodeId = 0;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRequestType = LSPP_REQ_TYPE_PING;

    LsppPingTraceTable.MibObject.i4FsLsppPathType = LSPP_PATH_TYPE_MEP;
    LsppPingTraceTable.MibObject.u4FsLsppTgtMipGlobalId = 1;
    LsppPingTraceTable.MibObject.u4FsLsppTgtMipIfNum = 0;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRequestType = LSPP_REQ_TYPE_PING;

    LsppPingTraceTable.MibObject.i4FsLsppPathType = LSPP_PATH_TYPE_MEP;
    LsppPingTraceTable.MibObject.u4FsLsppTgtMipNodeId = 1;
    LsppPingTraceTable.MibObject.u4FsLsppTTLValue = 0;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppPathType = LSPP_PATH_TYPE_RSVP_IPV4;
    LsppPingTraceTable.MibObject.i4FsLsppEncapType =
        LSPP_ENCAP_TYPE_VCCV_NEGOTIATED;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppPathType = LSPP_PATH_TYPE_FEC_128;
    LsppPingTraceTable.MibObject.i4FsLsppEncapType = LSPP_ENCAP_TYPE_IP;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppPathType = LSPP_PATH_TYPE_FEC_129;
    LsppPingTraceTable.MibObject.i4FsLsppEncapType = LSPP_ENCAP_TYPE_IP;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppReplyMode = LSPP_REPLY_IP_UDP;
    LsppPingTraceTable.MibObject.i4FsLsppEncapType =
        LSPP_ENCAP_TYPE_VCCV_NEGOTIATED;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.u4FsLsppReplyDscpValue = 1;
    LsppPingTraceTable.MibObject.i4FsLsppEncapType =
        LSPP_ENCAP_TYPE_VCCV_NEGOTIATED;

    if (LsppDbPingTraceCheckDependency (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtSock_1 (VOID)
{
    INT4                i4SockId = -1;

    if (LsppSockInitSockParams (i4SockId) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtSock_2 (VOID)
{
    INT4                i4SockId = gLsppGlobals.i4LsppSockId;
    gLsppGlobals.i4LsppSockId = -1;
    tCRU_BUF_CHAIN_DESC *pBuf = NULL;
    UINT4               u4ContextId = 0;
    UINT1               u1DscpValue = 0;

    pBuf = LSPP_ALLOC_CRU_BUF (100, 0);

    if (pBuf == NULL)
    {
        return OSIX_FAILURE;
    }

    if (LsppSockTransmitEchoRplyPkt (u4ContextId, pBuf, 0, &u1DscpValue, 0,
                                     OSIX_TRUE) != OSIX_FAILURE)
    {
        gLsppGlobals.i4LsppSockId = i4SockId;
        return OSIX_FAILURE;
    }

    if (LsppSockTransmitEchoRplyPkt (u4ContextId, pBuf, 0, &u1DscpValue, 0,
                                     OSIX_FALSE) != OSIX_FAILURE)
    {
        gLsppGlobals.i4LsppSockId = i4SockId;
        return OSIX_FAILURE;
    }

    if (LsppSockTransmitEchoRplyPkt (u4ContextId, pBuf, 0, &u1DscpValue, 0,
                                     OSIX_FALSE) != OSIX_FAILURE)
    {
        gLsppGlobals.i4LsppSockId = i4SockId;
        return OSIX_FAILURE;
    }

    gLsppGlobals.i4LsppSockId = i4SockId;

    if (LsppSockTransmitEchoRplyPkt (u4ContextId, pBuf, 0, &u1DscpValue, 0,
                                     OSIX_FALSE) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
LsppUtSock_3 (VOID)
{
    INT4                i4SockId = gLsppGlobals.i4LsppSockId;
    gLsppGlobals.i4LsppSockId = -1;

    if (LsppSockAddFd () != OSIX_FAILURE)
    {
        gLsppGlobals.i4LsppSockId = i4SockId;
        return OSIX_FAILURE;
    }

    gLsppGlobals.i4LsppSockId = i4SockId;
    return OSIX_SUCCESS;
}

INT4
LsppUtSock_4 (VOID)
{
    INT4                i4SockId = gLsppGlobals.i4LsppSockId;
    tLsppEchoMsg       *pLsppEchoMsg[MAX_LSPP_FSLSPP_ECHOMSG_ENTRY];
    INT4                i = 0;

    gLsppGlobals.i4LsppSockId = -1;

    LsppSockReceiveEchoPkt ();

    gLsppGlobals.i4LsppSockId = i4SockId;

    for (i = 0; i < MAX_LSPP_FSLSPP_ECHOMSG_ENTRY; i++)
    {
        if ((pLsppEchoMsg[i] = (tLsppEchoMsg *)
             MemAllocMemBlk (gLsppGlobals.LsppFsLsppEchoMsgPoolId)) == NULL)
        {
            return OSIX_FAILURE;
        }
    }

    LsppSockReceiveEchoPkt ();

    for (i = 0; i < MAX_LSPP_FSLSPP_ECHOMSG_ENTRY; i++)
    {
        if (MemReleaseMemBlock (gLsppGlobals.LsppFsLsppEchoMsgPoolId,
                                (UINT1 *) pLsppEchoMsg[i]) == MEM_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }
    LsppSockReceiveEchoPkt ();
    return OSIX_SUCCESS;
}

INT4
LsppUtUtil_1 (VOID)
{
    tLsppFsLsppPingTraceTableEntry LsppPingTraceTable;

    MEMSET (&(LsppPingTraceTable), 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.PathId.u1PathType = LSPP_PATH_TYPE_MEP + 1;

    if (LsppUtilTestBaseOid (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    LsppPingTraceTable.PathId.u1PathType = LSPP_PATH_TYPE_LDP_IPV4;

    if (LsppUtilTestBaseOid (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    LsppPingTraceTable.PathId.u1PathType = LSPP_PATH_TYPE_LDP_IPV6;

    if (LsppUtilTestBaseOid (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    LsppPingTraceTable.PathId.u1PathType = LSPP_PATH_TYPE_RSVP_IPV6;

    if (LsppUtilTestBaseOid (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    LsppPingTraceTable.PathId.u1PathType = LSPP_PATH_TYPE_RSVP_IPV4;

    if (LsppUtilTestBaseOid (&LsppPingTraceTable) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
LsppUtUtil_2 (VOID)
{
    tLsppEchoMsg        LsppEchoMsg;

    MEMSET (&LsppEchoMsg, 0, sizeof (tLsppEchoMsg));

    LsppEchoMsg.LsppPduInfo.u2TlvsPresent = LSPP_TARGET_FEC_TLV;
    LsppEchoMsg.LsppPduInfo.u1FecStackDepth = 1;
    LsppEchoMsg.LsppPduInfo.LsppFecTlv[0].u2TlvType = LSPP_FEC_LDP_IPV4;

    LsppUtilCalcPingPduSize (&LsppEchoMsg);

    LsppEchoMsg.LsppPduInfo.LsppFecTlv[0].u2TlvType = LSPP_FEC_LDP_IPV6;

    LsppUtilCalcPingPduSize (&LsppEchoMsg);

    LsppEchoMsg.LsppPduInfo.LsppFecTlv[0].u2TlvType = LSPP_FEC_RSVP_IPV6;

    LsppUtilCalcPingPduSize (&LsppEchoMsg);

    LsppEchoMsg.LsppPduInfo.LsppFecTlv[0].u2TlvType = LSPP_FEC_STATIC_PW + 1;

    LsppUtilCalcPingPduSize (&LsppEchoMsg);

    return OSIX_SUCCESS;
}

INT4
LsppUtUtil_3 (VOID)
{
    tLsppEchoMsg        LsppEchoMsg;
    tLsppTeTnlInfo      LsppTeTnlInfo;

    MEMSET (&LsppEchoMsg, 0, sizeof (tLsppEchoMsg));
    MEMSET (&LsppTeTnlInfo, 0, sizeof (tLsppTeTnlInfo));

    LsppEchoMsg.LsppPduInfo.LsppFecTlv[0].RsvpTeFecTlv.u1AddrType =
        LSPP_MPLS_ADDR_TYPE_IPV6;

    LsppUtilFillRsvpFecInfo (&LsppTeTnlInfo, &LsppEchoMsg, 0);

    return OSIX_SUCCESS;
}

INT4
LsppUtUtil_4 (VOID)
{

    tLsppEchoMsg        LsppEchoMsg;
    tLsppNonTeInfo      LsppNonTeInfo;

    MEMSET (&LsppEchoMsg, 0, sizeof (tLsppEchoMsg));
    MEMSET (&LsppNonTeInfo, 0, sizeof (tLsppNonTeInfo));

    LsppNonTeInfo.LdpInfo.u4AddrType = LSPP_MPLS_ADDR_TYPE_IPV4;

    LsppUtilFillLdpFecInfo (&LsppNonTeInfo, &LsppEchoMsg, 0);

    LsppNonTeInfo.LdpInfo.u4AddrType = LSPP_MPLS_ADDR_TYPE_IPV4;

    LsppUtilFillLdpFecInfo (&LsppNonTeInfo, &LsppEchoMsg, 0);

    return OSIX_SUCCESS;
}

INT4
LsppUtUtil_5 (VOID)
{
    tLsppPathId         PathId;
    UINT4               au4ServiceOid[LSPP_MAX_OID_LEN];
    INT4                i4OidLen = 0;

    MEMSET (au4ServiceOid, 0, LSPP_MAX_OID_LEN);

    PathId.u1PathType = LSPP_PATH_TYPE_LDP_IPV4;

    if (LsppUtilGetPathPointer (0, &PathId, au4ServiceOid, &i4OidLen) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    PathId.u1PathType = LSPP_PATH_TYPE_LDP_IPV6;

    if (LsppUtilGetPathPointer (0, &PathId, au4ServiceOid, &i4OidLen) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    PathId.u1PathType = LSPP_PATH_TYPE_RSVP_IPV6;

    if (LsppUtilGetPathPointer (0, &PathId, au4ServiceOid, &i4OidLen) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    PathId.u1PathType = LSPP_PATH_TYPE_MEP + 1;

    if (LsppUtilGetPathPointer (0, &PathId, au4ServiceOid, &i4OidLen) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
LsppUtCli_1 (VOID)
{
    tCliHandle          CliHandle = 0;
    UINT4               u4Command = 0;

    u4Command = 10;
    LSPP_UNLOCK;
    if (cli_process_Lspp_show_cmd (CliHandle, u4Command, NULL) != CLI_FAILURE)
    {
        LSPP_LOCK;
        return OSIX_FAILURE;
    }

    LSPP_LOCK;
    return OSIX_SUCCESS;
}

INT4
LsppUtCli_2 (VOID)
{
    tCliHandle          CliHandle = 0;
    UINT4               u4ContextId = 0;

    u4ContextId = 1;

    if (LsppCliShowGlobalStats (CliHandle, &u4ContextId, NULL) != CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }

    u4ContextId = 0;
    LsppCxtDeleteGlobalStats (u4ContextId);

    if (LsppCliShowGlobalStats (CliHandle, NULL, NULL) != CLI_FAILURE)
    {
        LsppCxtCreateGlobalStats (u4ContextId);
        return OSIX_FAILURE;
    }
    LsppCxtCreateGlobalStats (u4ContextId);
    return OSIX_SUCCESS;
}

INT4
LsppUtCli_3 (VOID)
{
    tCliHandle          CliHandle = 0;
    UINT4               u4ContextId = 0;

    u4ContextId = 1;

    if (LsppCliShowGlobalConfig (CliHandle, &u4ContextId) != CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }

    u4ContextId = 0;
    LsppCxtDeleteGlobalConfig (u4ContextId);

    if (LsppCliShowGlobalConfig (CliHandle, NULL) != CLI_FAILURE)
    {
        LsppCxtCreateGlobalConfig (u4ContextId);
        return OSIX_FAILURE;
    }

    LsppCxtCreateGlobalConfig (u4ContextId);
    return OSIX_SUCCESS;
}

INT4
LsppUtCli_4 (VOID)
{
    tCliHandle          CliHandle = 0;

    if (LsppCliShowPingTraceTable (CliHandle, NULL, NULL, NULL, 0) !=
        CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtCli_5 (VOID)
{
    UINT4               u4ContextId = 1;
    UINT4               u4SenderHandle = 0;

    if (LsppCliGetSendersHandle (u4ContextId, &u4SenderHandle) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtCli_6 (VOID)
{
    UINT4               u4ContextId = 0;

    if (LsppCliGetCxtIdFromName ((UINT1 *) "Invalid", &u4ContextId) !=
        CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtCli_7 (VOID)
{
    UINT4               u4ContextId = 1;
    INT4                i4OldDebugLevel = 0;

    if (LsppCliGetDebugLevel (u4ContextId, &i4OldDebugLevel) != CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtCli_8 (VOID)
{
    UINT4               u4ContextId = 1;
    INT4                i4OldTrapLevel = 0;

    if (LsppCliGetTrapLevel (u4ContextId, &i4OldTrapLevel) != CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtCli_9 (VOID)
{
    tLsppPathId         PathId;
    tCliHandle          CliHandle = 0;
    UINT1               au1String[LSPP_MAX_NAME_LEN];

    MEMSET (au1String, 0, LSPP_MAX_NAME_LEN);
    MEMSET (&PathId, 0, sizeof (tLsppPathId));

    STRCPY (au1String, "400.700.400/24");

    if (LsppCliGetLdpIpv4FecAndMask (CliHandle, au1String, NULL) != CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }

    STRCPY (au1String, "172.30.3.61/50");

    if (LsppCliGetLdpIpv4FecAndMask (CliHandle, au1String, &PathId) !=
        CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }

    STRCPY (au1String, "172.30.3.61/24/12");

    if (LsppCliGetLdpIpv4FecAndMask (CliHandle, au1String, &PathId) !=
        CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
LsppUtCli_10 (VOID)
{
    tLsppPathId         PathId;
    tCliHandle          CliHandle = 0;
    UINT1               au1String[LSPP_MAX_NAME_LEN + 20];

    MEMSET (au1String, 0, sizeof (au1String));
    MEMSET (&PathId, 0, sizeof (tLsppPathId));

    STRCPY (au1String, "1232/12");

    if (LsppCliGetLdpIpv6FecAndMask (CliHandle, au1String, NULL) != CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }

    STRCPY (au1String, "AF00:CD13:A2B8:F034:B711:1234:56A2:0012/200");

    if (LsppCliGetLdpIpv6FecAndMask (CliHandle, au1String, &PathId) !=
        CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }

    STRCPY (au1String, "AF00:CD13:A2B8:F034:B711:1234:56A2:0012/60/12");

    if (LsppCliGetLdpIpv6FecAndMask (CliHandle, au1String, &PathId) !=
        CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
LsppUtCli_11 (VOID)
{
    tLsppFsLsppPingTraceTableEntry LsppPingTraceTable;
    tCliHandle          CliHandle = 0;

    MEMSET (&LsppPingTraceTable, 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppPingTraceTable.MibObject.i4FsLsppRequestType = LSPP_REQ_TYPE_PING;
    LsppPingTraceTable.MibObject.i4FsLsppPathType = LSPP_PATH_TYPE_MEP;

    if (LsppCliPrintOutputHeader (CliHandle, &LsppPingTraceTable) !=
        CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }

    LsppPingTraceTable.MibObject.i4FsLsppRequestType =
        LSPP_REQ_TYPE_TRACE_ROUTE;
    LsppPingTraceTable.MibObject.i4FsLsppPathType = LSPP_PATH_TYPE_MEP;

    if (LsppCliPrintOutputHeader (CliHandle, &LsppPingTraceTable) !=
        CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtCli_12 (VOID)
{
    tLsppFsLsppPingTraceTableEntry LsppFsLsppPingTraceTable;
    tLsppFsLsppGlobalConfigTableEntry LsppGlobalConfigTable;
    tLsppFsLsppEchoSequenceTableEntry LsppEchoSequenceEntry;
    tLsppFsLsppEchoSequenceTableEntry *pLsppEchoSequenceEntry = NULL;
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTable = NULL;

    tCliHandle          CliHandle = 0;
    UINT4               u4SenderHandle = 0;

    MEMSET (&LsppFsLsppPingTraceTable, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&LsppGlobalConfigTable, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (&LsppEchoSequenceEntry, 0,
            sizeof (tLsppFsLsppEchoSequenceTableEntry));

    if (LsppCliSendEchoRqstShowOutput (CliHandle, &LsppFsLsppPingTraceTable) !=
        CLI_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (LsppGetAllFsLsppGlobalConfigTable (&LsppGlobalConfigTable) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    IndexManagerSetNextFreeIndex (LsppGlobalConfigTable.IndexMgrId,
                                  &u4SenderHandle);

    pLsppFsLsppPingTraceTable =
        LsppFsLsppPingTraceTableCreateApi (&LsppFsLsppPingTraceTable);

    if (pLsppFsLsppPingTraceTable == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&LsppFsLsppPingTraceTable, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppFsLsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppFsLsppPingTraceTable.MibObject.i4FsLsppRequestType = LSPP_REQ_TYPE_PING;
    LsppFsLsppPingTraceTable.MibObject.i4FsLsppPathType = LSPP_PATH_TYPE_MEP;

    if (LsppCliSendEchoRqstShowOutput (CliHandle, &LsppFsLsppPingTraceTable) !=
        CLI_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&LsppFsLsppPingTraceTable, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppFsLsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppFsLsppPingTraceTable.MibObject.i4FsLsppRequestType =
        LSPP_REQ_TYPE_TRACE_ROUTE;
    LsppFsLsppPingTraceTable.MibObject.i4FsLsppPathType =
        LSPP_PATH_TYPE_RSVP_IPV4;

    if (LsppCliSendEchoRqstShowOutput (CliHandle, &LsppFsLsppPingTraceTable) !=
        CLI_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    MEMSET (&LsppFsLsppPingTraceTable, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppFsLsppPingTraceTable.MibObject.u4FsLsppSenderHandle = u4SenderHandle;
    LsppFsLsppPingTraceTable.MibObject.i4FsLsppRequestType =
        LSPP_REQ_TYPE_TRACE_ROUTE;
    LsppFsLsppPingTraceTable.MibObject.i4FsLsppPathType =
        LSPP_PATH_TYPE_RSVP_IPV4;

    OsixCreateSem ((const UINT1 *) "LSPP", 1, 0,
                   &(LsppFsLsppPingTraceTable.SemId));

    LsppFsLsppPingTraceTable.u4LastUpdatedSeqIndex = 1;

    if (LsppCliSendEchoRqstShowOutput (CliHandle, &LsppFsLsppPingTraceTable) !=
        CLI_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        OsixSemDel (LsppFsLsppPingTraceTable.SemId);
        return OSIX_FAILURE;
    }

    LsppEchoSequenceEntry.MibObject.u4FsLsppContextId =
        LsppFsLsppPingTraceTable.MibObject.u4FsLsppContextId;

    LsppEchoSequenceEntry.MibObject.u4FsLsppSenderHandle =
        LsppFsLsppPingTraceTable.MibObject.u4FsLsppSenderHandle;

    LsppEchoSequenceEntry.MibObject.u4FsLsppSequenceNumber =
        LsppFsLsppPingTraceTable.u4LastUpdatedSeqIndex;

    pLsppEchoSequenceEntry =
        LsppFsLsppEchoSequenceTableCreateApi (&LsppEchoSequenceEntry);

    if (pLsppEchoSequenceEntry == NULL)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        OsixSemDel (LsppFsLsppPingTraceTable.SemId);
        return OSIX_FAILURE;
    }

    LsppFsLsppPingTraceTable.MibObject.u4FsLsppRepeatCount = 1;
    LsppFsLsppPingTraceTable.MibObject.i4FsLsppRequestOwner =
        LSPP_REQ_OWNER_BFD;

    if (LsppCliSendEchoRqstShowOutput (CliHandle, &LsppFsLsppPingTraceTable) !=
        CLI_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        OsixSemDel (LsppFsLsppPingTraceTable.SemId);
        return OSIX_FAILURE;
    }

    LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
    LsppCxtDeleteEchoSequences
        (pLsppEchoSequenceEntry->MibObject.u4FsLsppContextId,
         pLsppEchoSequenceEntry->MibObject.u4FsLsppSenderHandle);

    OsixSemDel (LsppFsLsppPingTraceTable.SemId);
    return OSIX_SUCCESS;
}

INT4
LsppUtCli_13 (VOID)
{
    tLsppFsLsppPingTraceTableEntry LsppFsLsppPingTraceTable;
    tCliHandle          CliHandle = 0;
    tLsppFsLsppHopTableEntry LsppHopInfoTable;

    MEMSET (&(LsppHopInfoTable), 0, sizeof (tLsppFsLsppHopTableEntry));
    MEMSET (&LsppFsLsppPingTraceTable, 0, sizeof (LsppFsLsppPingTraceTable));

    LsppHopInfoTable.MibObject.u4FsLsppContextId = 1;
    LsppHopInfoTable.MibObject.i4FsLsppHopAddrType = LSPP_MPLS_ADDR_TYPE_IPV4;

    /*LsppHopInfoTable.MibObject.au1FsLsppHopAddr = "" ; */
    LsppCliDisplayHopInfo (CliHandle, &LsppHopInfoTable,
                           &LsppFsLsppPingTraceTable);

    LsppHopInfoTable.MibObject.i4FsLsppHopAddrType = LSPP_MPLS_ADDR_TYPE_IPV6;

    LsppCliDisplayHopInfo (CliHandle, &LsppHopInfoTable,
                           &LsppFsLsppPingTraceTable);

    LsppHopInfoTable.MibObject.i4FsLsppHopAddrType =
        LSPP_ADDR_TYPE_NOT_APPLICABLE;

    LsppCliDisplayHopInfo (CliHandle, &LsppHopInfoTable,
                           &LsppFsLsppPingTraceTable);

    LsppHopInfoTable.MibObject.i4FsLsppHopRxAddrType = LSPP_IPV4_NUMBERED;

    LsppCliDisplayHopInfo (CliHandle, &LsppHopInfoTable,
                           &LsppFsLsppPingTraceTable);

    LsppHopInfoTable.MibObject.i4FsLsppHopRxAddrType = LSPP_IPV4_UNNUMBERED;

    LsppCliDisplayHopInfo (CliHandle, &LsppHopInfoTable,
                           &LsppFsLsppPingTraceTable);

    LsppHopInfoTable.MibObject.i4FsLsppHopRxAddrType = LSPP_IPV6_NUMBERED;

    LsppCliDisplayHopInfo (CliHandle, &LsppHopInfoTable,
                           &LsppFsLsppPingTraceTable);

    LsppHopInfoTable.MibObject.i4FsLsppHopRxAddrType = LSPP_IPV6_UNNUMBERED;

    LsppCliDisplayHopInfo (CliHandle, &LsppHopInfoTable,
                           &LsppFsLsppPingTraceTable);

    LsppHopInfoTable.MibObject.i4FsLsppHopRxAddrType =
        LSPP_ADDR_TYPE_NOT_APPLICABLE;

    LsppCliDisplayHopInfo (CliHandle, &LsppHopInfoTable,
                           &LsppFsLsppPingTraceTable);
    return OSIX_SUCCESS;
}

INT4
LsppUtLw_1 (VOID)
{
    UINT4               u4FsLsppContextId = LSPP_INVALID_CONTEXT;

    if (nmhValidateIndexInstanceFsLsppGlobalConfigTable (u4FsLsppContextId) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    u4FsLsppContextId = 1;
    if (nmhValidateIndexInstanceFsLsppGlobalConfigTable (u4FsLsppContextId) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtLw_2 (VOID)
{
    UINT4               u4FsLsppContextId = LSPP_INVALID_CONTEXT;

    if (nmhValidateIndexInstanceFsLsppGlobalStatsTable (u4FsLsppContextId) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    u4FsLsppContextId = 1;

    if (nmhValidateIndexInstanceFsLsppGlobalStatsTable (u4FsLsppContextId) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtLw_3 (VOID)
{
    UINT4               u4FsLsppContextId = LSPP_INVALID_CONTEXT;
    UINT4               u4FsLsppSenderHandle = 0;

    if (nmhValidateIndexInstanceFsLsppPingTraceTable (u4FsLsppContextId,
                                                      u4FsLsppSenderHandle) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    u4FsLsppContextId = 1;

    if (nmhValidateIndexInstanceFsLsppPingTraceTable (u4FsLsppContextId,
                                                      u4FsLsppSenderHandle) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    u4FsLsppSenderHandle = LSPP_MAX_SENDERS_HANDLE;

    if (nmhValidateIndexInstanceFsLsppPingTraceTable (u4FsLsppContextId,
                                                      u4FsLsppSenderHandle) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
LsppUtLw_4 (VOID)
{
    UINT4               u4FsLsppContextId = LSPP_INVALID_CONTEXT;
    UINT4               u4FsLsppSenderHandle = 0;
    UINT4               u4FsLsppSequenceNumber = 0;

    if (nmhValidateIndexInstanceFsLsppEchoSequenceTable (u4FsLsppContextId,
                                                         u4FsLsppSenderHandle,
                                                         u4FsLsppSequenceNumber)
        != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    u4FsLsppContextId = 0;

    if (nmhValidateIndexInstanceFsLsppEchoSequenceTable (u4FsLsppContextId,
                                                         u4FsLsppSenderHandle,
                                                         u4FsLsppSequenceNumber)
        != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    u4FsLsppSenderHandle = LSPP_MAX_SENDERS_HANDLE;

    if (nmhValidateIndexInstanceFsLsppEchoSequenceTable (u4FsLsppContextId,
                                                         u4FsLsppSenderHandle,
                                                         u4FsLsppSequenceNumber)
        != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    u4FsLsppSequenceNumber = LSPP_MAX_SEQUENCE_ENTRY;

    if (nmhValidateIndexInstanceFsLsppEchoSequenceTable (u4FsLsppContextId,
                                                         u4FsLsppSenderHandle,
                                                         u4FsLsppSequenceNumber)
        != SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtLw_5 (VOID)
{
    UINT4               u4FsLsppContextId = LSPP_INVALID_CONTEXT;
    UINT4               u4FsLsppSenderHandle = 0;
    UINT4               u4FsLsppHopIndex = 0;

    if (nmhValidateIndexInstanceFsLsppHopTable (u4FsLsppContextId,
                                                u4FsLsppSenderHandle,
                                                u4FsLsppHopIndex) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    u4FsLsppContextId = 1;

    if (nmhValidateIndexInstanceFsLsppHopTable (u4FsLsppContextId,
                                                u4FsLsppSenderHandle,
                                                u4FsLsppHopIndex) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }
    u4FsLsppSenderHandle = 1;

    if (nmhValidateIndexInstanceFsLsppHopTable (u4FsLsppContextId,
                                                u4FsLsppSenderHandle,
                                                u4FsLsppHopIndex) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    u4FsLsppHopIndex = LSPP_MAX_HOP_ENTRY;

    if (nmhValidateIndexInstanceFsLsppHopTable (u4FsLsppContextId,
                                                u4FsLsppSenderHandle,
                                                u4FsLsppHopIndex) !=
        SNMP_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
LsppUtDval_1 (VOID)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTable = NULL;

    if (LsppInitializeFsLsppPingTraceTable (pLsppFsLsppPingTraceTable)
        != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtDval_2 (VOID)
{
    /*
       tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigTable = NULL;

       if (LsppInitFsLsppGlobalConfigTable (pLsppFsLsppGlobalConfigTable)
       != OSIX_FAILURE)
       {
       return OSIX_FAILURE;
       }
     */
    return OSIX_SUCCESS;
}

INT4
LsppUtDval_3 (VOID)
{
    tLsppFsLsppGlobalStatsTableEntry *pLsppFsLsppGlobalStatsTable = NULL;

    if (LsppInitFsLsppGlobalStatsTable (pLsppFsLsppGlobalStatsTable)
        != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtExt_1 (VOID)
{
    tLsppFsLsppGlobalConfigTableEntry LsppFsLsppGlobalConfigEntry;
    tLsppExtTrigInfo    LsppExtTrigInfo;
    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigEntry = NULL;
    UINT4               u4ContextId = 1;

    MEMSET (&(LsppExtTrigInfo), 0, sizeof (tLsppExtTrigInfo));
    MEMSET (&(LsppFsLsppGlobalConfigEntry), 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));

    if (LsppExtTriggerPing (u4ContextId, &LsppExtTrigInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    LsppFsLsppGlobalConfigEntry.MibObject.u4FsLsppContextId = 1;

    pLsppFsLsppGlobalConfigEntry =
        LsppFsLsppGlobalConfigTableCreateApi (&LsppFsLsppGlobalConfigEntry);

    if (pLsppFsLsppGlobalConfigEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    LsppExtTrigInfo.LsppPathId.u1PathType = (LSPP_PATH_TYPE_MEP + 1);

    if (LsppExtTriggerPing (u4ContextId, &LsppExtTrigInfo) != OSIX_FAILURE)
    {
        LsppCxtDeleteGlobalConfig
            (pLsppFsLsppGlobalConfigEntry->MibObject.u4FsLsppContextId);

        return OSIX_FAILURE;
    }

    LsppExtTrigInfo.LsppPathId.u1PathType = LSPP_PATH_TYPE_MEP;

    if (LsppExtTriggerPing (u4ContextId, &LsppExtTrigInfo) != OSIX_FAILURE)
    {

        LsppCxtDeleteGlobalConfig
            (pLsppFsLsppGlobalConfigEntry->MibObject.u4FsLsppContextId);

        return OSIX_FAILURE;
    }

    LsppExtTrigInfo.LsppPathId.u1PathType = LSPP_PATH_TYPE_FEC_128;

    if (LsppExtTriggerPing (u4ContextId, &LsppExtTrigInfo) != OSIX_FAILURE)
    {
        LsppCxtDeleteGlobalConfig
            (pLsppFsLsppGlobalConfigEntry->MibObject.u4FsLsppContextId);

        return OSIX_FAILURE;
    }

    LsppCxtDeleteGlobalConfig
        (pLsppFsLsppGlobalConfigEntry->MibObject.u4FsLsppContextId);

    return OSIX_SUCCESS;
}

INT4
LsppUtExt_2 (VOID)
{

    tLsppFsLsppGlobalConfigTableEntry LsppFsLsppGlobalConfigEntry;
    tLsppExtTrigInfo    LsppExtTrigInfo;
    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigEntry = NULL;
    UINT4               u4ContextId = 1;

    MEMSET (&(LsppExtTrigInfo), 0, sizeof (tLsppExtTrigInfo));
    MEMSET (&(LsppFsLsppGlobalConfigEntry), 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));

    LsppFsLsppGlobalConfigEntry.MibObject.u4FsLsppContextId = 1;

    pLsppFsLsppGlobalConfigEntry =
        LsppFsLsppGlobalConfigTableCreateApi (&LsppFsLsppGlobalConfigEntry);

    if (pLsppFsLsppGlobalConfigEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    LsppExtTrigInfo.LsppPathId.u1PathType = LSPP_PATH_TYPE_LDP_IPV4;

    if (LsppExtTriggerPing (u4ContextId, &LsppExtTrigInfo) != OSIX_FAILURE)
    {
        LsppCxtDeleteGlobalConfig
            (pLsppFsLsppGlobalConfigEntry->MibObject.u4FsLsppContextId);

        return OSIX_FAILURE;
    }

    LsppExtTrigInfo.LsppPathId.u1PathType = LSPP_PATH_TYPE_LDP_IPV6;

    if (LsppExtTriggerPing (u4ContextId, &LsppExtTrigInfo) != OSIX_FAILURE)
    {
        LsppCxtDeleteGlobalConfig
            (pLsppFsLsppGlobalConfigEntry->MibObject.u4FsLsppContextId);

        return OSIX_FAILURE;
    }

    LsppExtTrigInfo.LsppPathId.u1PathType = LSPP_PATH_TYPE_RSVP_IPV4;

    if (LsppExtTriggerPing (u4ContextId, &LsppExtTrigInfo) != OSIX_FAILURE)
    {
        LsppCxtDeleteGlobalConfig
            (pLsppFsLsppGlobalConfigEntry->MibObject.u4FsLsppContextId);

        return OSIX_FAILURE;
    }

    LsppExtTrigInfo.LsppPathId.u1PathType = LSPP_PATH_TYPE_RSVP_IPV6;

    if (LsppExtTriggerPing (u4ContextId, &LsppExtTrigInfo) != OSIX_FAILURE)
    {
        LsppCxtDeleteGlobalConfig
            (pLsppFsLsppGlobalConfigEntry->MibObject.u4FsLsppContextId);

        return OSIX_FAILURE;
    }

    LsppExtTrigInfo.LsppPathId.u1PathType = LSPP_PATH_TYPE_FEC_129;

    if (LsppExtTriggerPing (u4ContextId, &LsppExtTrigInfo) != OSIX_FAILURE)
    {
        LsppCxtDeleteGlobalConfig
            (pLsppFsLsppGlobalConfigEntry->MibObject.u4FsLsppContextId);

        return OSIX_FAILURE;
    }

    LsppCxtDeleteGlobalConfig
        (pLsppFsLsppGlobalConfigEntry->MibObject.u4FsLsppContextId);

    return OSIX_SUCCESS;
}

INT4
LsppUtQue_1 (VOID)
{
    tLsppQMsg          *pLsppQMsg;

    if ((pLsppQMsg = (tLsppQMsg *)
         MemAllocMemBlk (gLsppGlobals.LsppFsLsppQMsgPoolId)) == NULL)
    {
        return OSIX_FAILURE;
    }
    pLsppQMsg->u4ContextId = LSPP_INVALID_CONTEXT;
    pLsppQMsg->u1MsgType = LSPP_CREATE_CONTEXT_MSG;

    OsixQueSend (gLsppGlobals.lsppQueId, (UINT1 *) &pLsppQMsg,
                 OSIX_DEF_MSG_LEN);

    gLsppGlobals.u4LsppTrc = OSIX_FALSE;
    LsppQueProcessMsgs ();

    pLsppQMsg = (tLsppQMsg *)
        MemAllocMemBlk (gLsppGlobals.LsppFsLsppQMsgPoolId);
    if (pLsppQMsg == NULL)
    {
        return OSIX_FAILURE;
    }

    pLsppQMsg->u4ContextId = LSPP_INVALID_CONTEXT;
    pLsppQMsg->u1MsgType = LSPP_DELETE_CONTEXT_MSG;

    OsixQueSend (gLsppGlobals.lsppQueId, (UINT1 *) &pLsppQMsg,
                 OSIX_DEF_MSG_LEN);

    LsppQueProcessMsgs ();

    pLsppQMsg = (tLsppQMsg *)
        MemAllocMemBlk (gLsppGlobals.LsppFsLsppQMsgPoolId);
    if (pLsppQMsg == NULL)
    {
        return OSIX_FAILURE;
    }

    pLsppQMsg->u4ContextId = 1;
    pLsppQMsg->u1MsgType = LSPP_RX_PDU_MSG;
    pLsppQMsg->unMsgParam.LsppRxPduInfo.pBuf = LSPP_ALLOC_CRU_BUF (100, 0);

    OsixQueSend (gLsppGlobals.lsppQueId, (UINT1 *) &pLsppQMsg,
                 OSIX_DEF_MSG_LEN);

    LsppQueProcessMsgs ();

    pLsppQMsg = (tLsppQMsg *)
        MemAllocMemBlk (gLsppGlobals.LsppFsLsppQMsgPoolId);
    if (pLsppQMsg == NULL)
    {
        return OSIX_FAILURE;
    }

    pLsppQMsg->u4ContextId = 1;
    pLsppQMsg->u1MsgType = LSPP_BFD_BOOTSTRAP_ECHO_REQ;

    OsixQueSend (gLsppGlobals.lsppQueId, (UINT1 *) &pLsppQMsg,
                 OSIX_DEF_MSG_LEN);

    LsppQueProcessMsgs ();

    pLsppQMsg = (tLsppQMsg *)
        MemAllocMemBlk (gLsppGlobals.LsppFsLsppQMsgPoolId);
    if (pLsppQMsg == NULL)
    {
        return OSIX_FAILURE;
    }

    pLsppQMsg->u4ContextId = 1;
    pLsppQMsg->u1MsgType = LSPP_BFD_BOOTSTRAP_ECHO_REPLY;

    OsixQueSend (gLsppGlobals.lsppQueId, (UINT1 *) &pLsppQMsg,
                 OSIX_DEF_MSG_LEN);

    LsppQueProcessMsgs ();

    pLsppQMsg = (tLsppQMsg *)
        MemAllocMemBlk (gLsppGlobals.LsppFsLsppQMsgPoolId);
    if (pLsppQMsg == NULL)
    {
        return OSIX_FAILURE;
    }

    pLsppQMsg->u1MsgType = LSPP_BFD_BOOTSTRAP_ECHO_REPLY + 1;

    OsixQueSend (gLsppGlobals.lsppQueId, (UINT1 *) &pLsppQMsg,
                 OSIX_DEF_MSG_LEN);

    LsppQueProcessMsgs ();

    return OSIX_SUCCESS;
}

INT4
LsppUtQue_2 (VOID)
{
    UINT4               Entry = 0;
    tLsppQMsg          *pLsppQMsg = NULL;
    tLsppQMsg          *pMsg;
    UINT1               u1ErrorCode = 0;

    pLsppQMsg = MemAllocMemBlk (gLsppGlobals.LsppFsLsppQMsgPoolId);

    if (pLsppQMsg == NULL)
    {
        return OSIX_FAILURE;
    }

    pMsg = pLsppQMsg;
    for (Entry = 0; Entry < LSPP_QUEUE_DEPTH; Entry++)
    {
        OsixQueSend (gLsppGlobals.lsppQueId, (UINT1 *) &pMsg, OSIX_DEF_MSG_LEN);
    }

    pLsppQMsg->u1MsgType = LSPP_RX_PDU_MSG;
    pLsppQMsg->unMsgParam.LsppRxPduInfo.pBuf = LSPP_ALLOC_CRU_BUF (100, 0);

    if (LsppQueEnqMsg (pLsppQMsg, &u1ErrorCode) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    pLsppQMsg = MemAllocMemBlk (gLsppGlobals.LsppFsLsppQMsgPoolId);
    pLsppQMsg->u1MsgType = LSPP_RX_PDU_MSG + 1;

    if (LsppQueEnqMsg (pLsppQMsg, &u1ErrorCode) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    for (Entry = 0; Entry < LSPP_QUEUE_DEPTH; Entry++)
    {
        OsixQueRecv (gLsppGlobals.lsppQueId,
                     (UINT1 *) &pMsg, OSIX_DEF_MSG_LEN, OSIX_NO_WAIT);
    }

    MemReleaseMemBlock (gLsppGlobals.LsppFsLsppQMsgPoolId, (UINT1 *) pLsppQMsg);

    return OSIX_SUCCESS;
}

INT4
LsppUtQue_3 (VOID)
{

    UINT4               u4ContextId = 0;
    tLsppRxPduInfo      LsppRxPduInfo;
    tLsppEchoMsg       *pEchoMsg[MAX_LSPP_FSLSPP_QUEUE_MSG_ENTRY];
    INT4                Poolid = 0;

    MEMSET (&(LsppRxPduInfo), 0, sizeof (tLsppRxPduInfo));
    LsppRxPduInfo.pBuf = LSPP_ALLOC_CRU_BUF (100, 0);

    for (Poolid = 0; Poolid < MAX_LSPP_FSLSPP_ECHOMSG_ENTRY; Poolid++)
    {
        if ((pEchoMsg[Poolid] = (tLsppEchoMsg *)
             MemAllocMemBlk (gLsppGlobals.LsppFsLsppEchoMsgPoolId)) == NULL)
        {
            return OSIX_FAILURE;
        }
    }

    if (LsppQueRxPdu (u4ContextId, &LsppRxPduInfo) != OSIX_FAILURE)
    {
        for (Poolid = 0; Poolid < MAX_LSPP_FSLSPP_ECHOMSG_ENTRY; Poolid++)
        {
            if (MemReleaseMemBlock (gLsppGlobals.LsppFsLsppEchoMsgPoolId,
                                    (UINT1 *) pEchoMsg[Poolid]) == MEM_FAILURE)
            {
                return OSIX_FAILURE;
            }
        }

        return OSIX_FAILURE;
    }

    for (Poolid = 0; Poolid < MAX_LSPP_FSLSPP_ECHOMSG_ENTRY; Poolid++)
    {
        if (MemReleaseMemBlock (gLsppGlobals.LsppFsLsppEchoMsgPoolId,
                                (UINT1 *) pEchoMsg[Poolid]) == MEM_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtApi_1 (VOID)
{
    tLsppReqParams      LsppReqParams;
    tLsppRespParams     LsppRespParams;
    tLsppQMsg          *pLsppQMsgDummy[MAX_LSPP_FSLSPP_QUEUE_MSG_ENTRY];
    INT4                Poolid = 0;

    MEMSET (&(LsppReqParams), 0, sizeof (tLsppReqParams));
    MEMSET (&(LsppRespParams), 0, sizeof (tLsppRespParams));

    for (Poolid = 0; Poolid < MAX_LSPP_FSLSPP_QUEUE_MSG_ENTRY; Poolid++)
    {
        if ((pLsppQMsgDummy[Poolid] = (tLsppQMsg *)
             MemAllocMemBlk (gLsppGlobals.LsppFsLsppQMsgPoolId)) == NULL)
        {
            return OSIX_FAILURE;
        }
    }

    if (LsppApiHandleExtRequest (&LsppReqParams,
                                 &LsppRespParams) != OSIX_FAILURE)
    {
        for (Poolid = 0; Poolid < MAX_LSPP_FSLSPP_QUEUE_MSG_ENTRY; Poolid++)
        {
            if (MemReleaseMemBlock (gLsppGlobals.LsppFsLsppQMsgPoolId,
                                    (UINT1 *) pLsppQMsgDummy[Poolid]) ==
                MEM_FAILURE)
            {
                return OSIX_FAILURE;
            }
        }
        return OSIX_FAILURE;
    }

    for (Poolid = 0; Poolid < MAX_LSPP_FSLSPP_QUEUE_MSG_ENTRY; Poolid++)
    {
        if (MemReleaseMemBlock (gLsppGlobals.LsppFsLsppQMsgPoolId,
                                (UINT1 *) pLsppQMsgDummy[Poolid]) ==
            MEM_FAILURE)
        {
            return OSIX_FAILURE;
        }
    }

    LsppReqParams.u1MsgType = (LSPP_RX_PDU_MSG + 1);

    if (LsppApiHandleExtRequest (&LsppReqParams,
                                 &LsppRespParams) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;

}

INT4
LsppUtApi_2 (VOID)
{
    INT4                i4SockId = 0;
    UINT4               LsppTaskId = gLsppGlobals.lsppTaskId;

    gLsppGlobals.lsppTaskId = 0;

    LsppApiPktRcvdOnSocket (i4SockId);

    gLsppGlobals.lsppTaskId = LsppTaskId;
    return OSIX_SUCCESS;
}

INT4
LsppUtApi_3 (VOID)
{

    tLsppReqParams      LsppReqParams;
    tLsppRespParams     LsppRespParams;

    MEMSET (&(LsppReqParams), 0, sizeof (tLsppReqParams));
    MEMSET (&(LsppRespParams), 0, sizeof (tLsppRespParams));

    LsppReqParams.u1MsgType = LSPP_CREATE_CONTEXT_MSG;
    LsppReqParams.u4ContextId = 1;

    if (LsppApiHandleExtRequest (&(LsppReqParams), &(LsppRespParams))
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    LsppReqParams.u1MsgType = LSPP_BFD_BOOTSTRAP_ECHO_REQ;
    LsppReqParams.u4ContextId = 1;

    if (LsppApiHandleExtRequest (&(LsppReqParams), &(LsppRespParams))
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    LsppReqParams.u1MsgType = LSPP_BFD_BOOTSTRAP_ECHO_REPLY;
    LsppReqParams.u4ContextId = 1;

    if (LsppApiHandleExtRequest (&(LsppReqParams), &(LsppRespParams))
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    LsppReqParams.u1MsgType = LSPP_DELETE_CONTEXT_MSG;
    LsppReqParams.u4ContextId = 1;

    if (LsppApiHandleExtRequest (&(LsppReqParams), &(LsppRespParams))
        != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
LsppUtApi_4 (VOID)
{
    UINT4               u4IpIfIndex = 0;
    UINT4               u4VcmCxtId = 0;
    UINT1               u1Event = 0;

    u1Event = 4;

    LsppApiVcmCallback (u4IpIfIndex, u4VcmCxtId, u1Event);

    u1Event = 2;

    LsppApiVcmCallback (u4IpIfIndex, u4VcmCxtId, u1Event);

    return OSIX_SUCCESS;
}

INT4
LsppUtTmr_1 (VOID)
{
    tTmrBlk             TmrBlk;
    tTimerListId        TmrId;
    UINT1               u1TmrId = 0;
    UINT4               u4Secs = 0;
    UINT4               u4MilliSecs = 0;

    MEMSET (&TmrId, 0, sizeof (tTimerListId));
    MEMSET (&(TmrBlk), 0, sizeof (tTmrBlk));

    LsppTmrStartTmr (&TmrBlk, u1TmrId, u4Secs, u4MilliSecs);

    LsppTmrStopTmr (&TmrBlk);

    return OSIX_SUCCESS;
}

INT4
LsppUtTmr_2 (VOID)
{
    tLsppFsLsppPingTraceTableEntry LsppFsLsppPingTraceTable;

    MEMSET (&(LsppFsLsppPingTraceTable), 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppTmrWTSTmrExp (&(LsppFsLsppPingTraceTable.WTSTimer));
    return OSIX_SUCCESS;
}

INT4
LsppUtTmr_3 (VOID)
{
    tLsppFsLsppEchoSequenceTableEntry LsppFsLsppEchoSequenceTable;

    MEMSET (&(LsppFsLsppEchoSequenceTable), 0,
            sizeof (tLsppFsLsppEchoSequenceTableEntry));
    LsppTmrWFRTmrExp (&(LsppFsLsppEchoSequenceTable.WFRTimer));
    return OSIX_SUCCESS;
}

INT4
LsppUtTmr_4 (VOID)
{
    tLsppFsLsppPingTraceTableEntry LsppFsLsppPingTraceTable;

    MEMSET (&(LsppFsLsppPingTraceTable), 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppFsLsppPingTraceTable.MibObject.u4FsLsppContextId = LSPP_INVALID_CONTEXT;
    LsppFsLsppPingTraceTable.MibObject.u4FsLsppSenderHandle = 1;

    LsppTmrAgeOutTmrExp (&(LsppFsLsppPingTraceTable));

    return OSIX_SUCCESS;
}

INT4
LsppUtTmr_5 (VOID)
{
    tLsppFsLsppGlobalConfigTableEntry LsppFsLsppGlobalConfigTable;
    tLsppFsLsppGlobalConfigTableEntry LsppOldFsLsppGlobalConfigTable;
    tLsppFsLsppPingTraceTableEntry LsppFsLsppPingTraceTable;
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTable = NULL;

    MEMSET (&(LsppFsLsppGlobalConfigTable), 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (&(LsppOldFsLsppGlobalConfigTable), 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (&(LsppFsLsppPingTraceTable), 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    LsppFsLsppPingTraceTable.MibObject.u4FsLsppSenderHandle = 1;
    LsppFsLsppPingTraceTable.MibObject.i4FsLsppStatus = LSPP_ECHO_SUCCESS;

    pLsppFsLsppPingTraceTable =
        LsppFsLsppPingTraceTableCreateApi (&LsppFsLsppPingTraceTable);

    if (pLsppFsLsppPingTraceTable == NULL)
    {
        return OSIX_FAILURE;
    }

    if (LsppTmrRestartAgeOutTimer (&LsppFsLsppGlobalConfigTable,
                                   &LsppOldFsLsppGlobalConfigTable)
        != OSIX_FAILURE)
    {
        LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
        return OSIX_FAILURE;
    }

    LsppCxtDeletePingTraceNode (pLsppFsLsppPingTraceTable);
    return OSIX_SUCCESS;
}

INT4
LsppUtTask_1 (VOID)
{
    INT1                u1Dummy;

    LsppTaskSpawnLsppTask (&u1Dummy);
    return OSIX_SUCCESS;
}

INT4
LsppUtMain_1 (VOID)
{

    UINT4               u4Node = 0;
    VOID               *Temp = NULL;

    Temp = gLsppGlobals.lsppTmrLst;

    if (LsppMainTaskInit () != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OsixDeleteSem (u4Node, LSPP_MUT_EXCL_SEM_NAME) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (LsppMainTaskInit () != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (OsixDeleteSem (u4Node, LSPP_MUT_EXCL_SEM_NAME) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (OsixDeleteQ (u4Node, LSPP_QUEUE_NAME) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    gLsppGlobals.lsppTmrLst = NULL;

    if (LsppMainTaskInit () != OSIX_FAILURE)
    {
        gLsppGlobals.lsppTmrLst = Temp;
        return OSIX_FAILURE;
    }

    gLsppGlobals.lsppTmrLst = Temp;
    return OSIX_SUCCESS;
}

INT4
LsppUtMain_2 (VOID)
{
    tOsixSemId          SemId;

    MEMSET (&(SemId), 0, sizeof (tOsixSemId));

    MEMCPY (&(SemId), &(gLsppGlobals.lsppTaskSemId), sizeof (tOsixSemId));
    MEMSET (&(gLsppGlobals.lsppTaskSemId), 0, sizeof (tOsixSemId));

    if (LsppMainTaskLock != SNMP_FAILURE)
    {
        MEMCPY (&(gLsppGlobals.lsppTaskSemId), &(SemId), sizeof (tOsixSemId));
        return OSIX_FAILURE;
    }

    MEMCPY (&(gLsppGlobals.lsppTaskSemId), &(SemId), sizeof (tOsixSemId));
    return OSIX_SUCCESS;
}

INT4
LsppUtMain_3 (VOID)
{
    if (LsppMainVcmRegister () != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtMain_4 (VOID)
{
    if (LsppMainSysLogRegister () != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtTrc_1 (VOID)
{
    UINT4               u4ContextId = 0;
    UINT4               u4ArrayIndex = 0;

    u4ContextId = LSPP_INVALID_CONTEXT;
    gLsppGlobals.u4LsppTrc = OSIX_FALSE;

    LsppTrcIssEventLogNotify (u4ContextId, u4ArrayIndex);

    gLsppGlobals.u4LsppTrc = OSIX_TRUE;

    u4ContextId = 1;
    LsppTrcIssEventLogNotify (u4ContextId, u4ArrayIndex);

    return OSIX_SUCCESS;
}

INT4
LsppUtTrc_2 (VOID)
{
    UINT4               u4ContextId = 0;
    UINT4               u4ArrayIndex = 0;

    u4ContextId = LSPP_INVALID_CONTEXT;
    gLsppGlobals.u4LsppTrc = OSIX_FALSE;

    LsppTrcLsppEventLogNotify (u4ContextId, u4ArrayIndex);

    gLsppGlobals.u4LsppTrc = OSIX_TRUE;

    u4ContextId = 1;
    LsppTrcLsppEventLogNotify (u4ContextId, u4ArrayIndex);

    return OSIX_SUCCESS;
}

INT4
LsppUtTrc_3 (VOID)
{
    LsppTrcPrint (NULL, 0, NULL);
    return OSIX_SUCCESS;
}

INT4
LsppUtTrc_4 (VOID)
{
    LsppTrcWrite ("Trace");
    return OSIX_SUCCESS;
}

INT4
LsppUtTrc_5 (VOID)
{
    gLsppGlobals.u4LsppTrc = OSIX_TRUE;
    LsppTrc (1, NULL);
    gLsppGlobals.u4LsppTrc = OSIX_FALSE;
    return OSIX_SUCCESS;
}

INT4
LsppUtPort_1 (VOID)
{
    tLsppExtInParams    LsppExtInParams;
    tLsppExtOutParams   LsppExtOutParams;

    MEMSET (&(LsppExtInParams), 0, sizeof (tLsppExtInParams));
    MEMSET (&(LsppExtOutParams), 0, sizeof (tLsppExtOutParams));

    LsppExtInParams.u4RequestType = (LSPP_EXT_GET_VCM_CONTEXT_NAME + 1);

    if (LsppPortHandleExtInteraction (&(LsppExtInParams),
                                      &(LsppExtOutParams)) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
LsppUtPort_2 (VOID)
{
    UINT4               u4ContextId = 0;

    u4ContextId = LSPP_INVALID_CONTEXT;

    if (LsppPortIsContextExist (u4ContextId) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
LsppUtPort_3 (VOID)
{
    UINT4               u4ContextId = 0;
    tLsppMegIndices     LsppMegIndices;
    tLsppMegMeName      LsppMegMeName;

    MEMSET (&(LsppMegIndices), 0, sizeof (tLsppMegIndices));
    MEMSET (&(LsppMegMeName), 0, sizeof (tLsppMegMeName));

    u4ContextId = LSPP_INVALID_CONTEXT;

    if (LsppPortGetMegMeName (u4ContextId, &LsppMegIndices, &LsppMegMeName)
        != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/* LsppTxFramePingPayload */
INT4
LsppUtTx_1 (VOID)
{
    tLsppPduInfo        LsppPduInfo;
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;
    UINT4               u4BufOffset = 0;
    UINT4               u4ContextId = 0;

    MEMSET (&LsppPduInfo, 0, sizeof (tLsppPduInfo));

    /* Upadting Inputs */
    pMsg = LSPP_ALLOC_CRU_BUF (100, 0);
    if (pMsg == NULL)
    {
        return OSIX_FAILURE;
    }

    u4BufOffset = 50;

    /* Failure Case : Updating Input with Invalid Target Fec Tlv Type */
    LsppUtUdpateHeader (&LsppPduInfo);
    LsppUtUdpateTgtFec (&LsppPduInfo, LSPP_INVALID_PKT);

    if (LsppTxFramePingPayload (pMsg, &u4BufOffset,
                                &LsppPduInfo, u4ContextId) == OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    LSPP_RELEASE_CRU_BUF (pMsg, 0);
    return OSIX_SUCCESS;
}

/* LsppTxGetReplyPath */
INT4
LsppUtTx_2 (VOID)
{
    tLsppEchoMsg        LsppEchoMsg;
    UINT1               u1DscpValue = 0;
    UINT1               u1ReplyPath = 0;
    BOOL1               bRouterAlertOpt = OSIX_FALSE;

    MEMSET (&LsppEchoMsg, 0, sizeof (tLsppEchoMsg));

    /* Case 1- Updating Inputs */
    LsppEchoMsg.LsppPduInfo.u2TlvsPresent =
        LsppEchoMsg.LsppPduInfo.u2TlvsPresent | LSPP_REPLY_TOS_BYTE_TLV;
    LsppEchoMsg.LsppPduInfo.LsppReplyTosTlv.u1TosValue = 45;
    LsppEchoMsg.LsppPduInfo.LsppHeader.u1ReplyMode = LSPP_REPLY_IP_UDP;

    u1ReplyPath = LsppTxGetReplyPath (&LsppEchoMsg, &u1DscpValue,
                                      &bRouterAlertOpt);

    if (u1ReplyPath != LSPP_REPLY_VIA_IP_PATH && u1DscpValue != 45)
    {
        return OSIX_FAILURE;
    }

    /* Case 2- Updating Inputs */
    MEMSET (&LsppEchoMsg, 0, sizeof (tLsppEchoMsg));
    u1DscpValue = 0;
    u1ReplyPath = 0;
    bRouterAlertOpt = OSIX_FALSE;

    LsppEchoMsg.LsppPduInfo.LsppHeader.u1ReplyMode =
        LSPP_REPLY_IP_UDP_ROUTER_ALERT;

    LsppEchoMsg.u1IsRouteExists = OSIX_FALSE;

    u1ReplyPath = LsppTxGetReplyPath (&LsppEchoMsg, &u1DscpValue,
                                      &bRouterAlertOpt);

    if ((u1ReplyPath != LSPP_REPLY_VIA_LSP) &&
        (u1DscpValue != 0) && (bRouterAlertOpt != OSIX_FALSE))
    {
        return OSIX_FAILURE;
    }

    /* Case 3- Updating Inputs */
    MEMSET (&LsppEchoMsg, 0, sizeof (tLsppEchoMsg));
    u1DscpValue = 0;
    u1ReplyPath = 0;
    bRouterAlertOpt = OSIX_FALSE;

    LsppEchoMsg.LsppPduInfo.LsppHeader.u1ReplyMode =
        LSPP_REPLY_IP_UDP_ROUTER_ALERT;

    LsppEchoMsg.u1IsRouteExists = OSIX_TRUE;

    u1ReplyPath = LsppTxGetReplyPath (&LsppEchoMsg, &u1DscpValue,
                                      &bRouterAlertOpt);

    if ((u1ReplyPath != LSPP_REPLY_VIA_IP_PATH) &&
        (u1DscpValue != 0) && (bRouterAlertOpt != OSIX_TRUE))
    {
        return OSIX_FAILURE;
    }

    /* Case 4- Updating Inputs */
    MEMSET (&LsppEchoMsg, 0, sizeof (tLsppEchoMsg));
    u1DscpValue = 0;
    u1ReplyPath = 0;
    bRouterAlertOpt = OSIX_FALSE;

    LsppEchoMsg.LsppPduInfo.LsppHeader.u1ReplyMode = LSPP_REPLY_APPLICATION_CC;

    u1ReplyPath = LsppTxGetReplyPath (&LsppEchoMsg, &u1DscpValue,
                                      &bRouterAlertOpt);

    if ((u1ReplyPath != LSPP_REPLY_VIA_LSP) &&
        (u1DscpValue != 0) && (bRouterAlertOpt != OSIX_FALSE))
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/* LsppTxSendPacketToBfd */
INT4
LsppUtTx_3 (VOID)
{
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;
    tLsppEchoMsg       *pLsppEchoMsg = NULL;
    UINT1               u1ReplyPath = 0;

    /* Upadting Inputs */
    u1ReplyPath = LSPP_NO_REPLY;

    if (LsppTxSendPacketToBfd (pMsg, pLsppEchoMsg, u1ReplyPath) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/* LsppTxEchoPkt */
INT4
LsppUtTx_4 (VOID)
{
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;
    tLsppOutSegInfo    *pOutSegInfo = NULL;
    UINT4               u4ContextId = 0;

    /* Case 1- Upadting Inputs */
    if (LsppTxEchoPkt (pMsg, u4ContextId, pOutSegInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Case 2- Upadting Inputs */
    pMsg = LSPP_ALLOC_CRU_BUF (100, 0);

    if (LsppTxEchoPkt (pMsg, u4ContextId, pOutSegInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/* LsppTxUpdateOutGoingLblStk */
INT4
LsppUtTx_5 (VOID)
{
    tLsppEchoMsg        LsppEchoMsg;

    MEMSET (&LsppEchoMsg, 0, sizeof (tLsppEchoMsg));

    /* Failure Case 1- Invalid Label Stack Depth */
    LsppEchoMsg.u1LabelStackDepth = 0;
    if (LsppTxUpdateOutGoingLblStk (&LsppEchoMsg) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case 2- Invalid Label Stack Depth */
    LsppEchoMsg.u1LabelStackDepth = 100;
    if (LsppTxUpdateOutGoingLblStk (&LsppEchoMsg) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Success Case 3 - Adding TTL */
    MEMSET (&LsppEchoMsg, 0, sizeof (tLsppEchoMsg));
    LsppEchoMsg.u1LabelStackDepth = 7;
    LsppEchoMsg.LsppPathInfo.u1PathType = LSPP_PATH_TYPE_STATIC_PW;
    LsppEchoMsg.u1CcSelected = LSPP_VCCV_CC_TTL;
    LsppEchoMsg.LsppPduInfo.LsppHeader.u1ReplyMode = 1;

    if (LsppTxUpdateOutGoingLblStk (&LsppEchoMsg) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case 4 - Adding TTL */
    MEMSET (&LsppEchoMsg, 0, sizeof (tLsppEchoMsg));
    LsppEchoMsg.u1LabelStackDepth = 7;
    LsppEchoMsg.LsppPathInfo.u1PathType = LSPP_PATH_TYPE_FEC_128;
    LsppEchoMsg.u1CcSelected = LSPP_VCCV_CC_RAL;

    if (LsppTxUpdateOutGoingLblStk (&LsppEchoMsg) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case 5 - Adding GAL label with label stack size already 7 */
    MEMSET (&LsppEchoMsg, 0, sizeof (tLsppEchoMsg));
    LsppEchoMsg.u1LabelStackDepth = 7;
    LsppEchoMsg.LsppPathInfo.u1PathType = LSPP_PATH_TYPE_STATIC_TNL;
    LsppEchoMsg.u1CcSelected = LSPP_VCCV_CC_RAL;

    if (LsppTxUpdateOutGoingLblStk (&LsppEchoMsg) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case 6 - Adding GAL label with label stack size already 7 */
    MEMSET (&LsppEchoMsg, 0, sizeof (tLsppEchoMsg));
    LsppEchoMsg.u1LabelStackDepth = 7;
    LsppEchoMsg.LsppPathInfo.u1PathType = LSPP_PATH_TYPE_RSVP_IPV4;
    LsppEchoMsg.LsppPduInfo.LsppHeader.u1ReplyMode =
        LSPP_REPLY_IP_UDP_ROUTER_ALERT;
    LsppEchoMsg.u1IsRouteExists = OSIX_FALSE;

    if (LsppTxUpdateOutGoingLblStk (&LsppEchoMsg) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/* LsppTxFrameAndSendPdu */
INT4
LsppUtTx_6 (VOID)
{
    tLsppEchoMsg        LsppEchoMsg;
    tLsppFsLsppPingTraceTableEntry LsppPingTraceEntry;
    tLsppFsLsppEchoSequenceTableEntry EchoSequenceEntry;
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;
    tLsppDsMapTlv      *pRxDsMapTlv = NULL;
    UINT4               u4BfdDisc = 0;

    MEMSET (&LsppEchoMsg, 0, sizeof (tLsppEchoMsg));
    MEMSET (&LsppPingTraceEntry, 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&EchoSequenceEntry, 0, sizeof (tLsppFsLsppEchoSequenceTableEntry));

    /* Updating Inputs */
    LsppPingTraceEntry.MibObject.i4FsLsppRequestType = LSPP_REQ_TYPE_PING;
    LsppPingTraceEntry.MibObject.i4FsLsppRequestOwner = LSPP_REQ_OWNER_MGMT;
    LsppPingTraceEntry.u1RequestOwnerSubType = LSPP_REQ_OWNER_MGMT_CLI;
    LsppEchoMsg.LsppPduInfo.LsppHeader.u1MessageType = LSPP_ECHO_REQUEST;
    LsppPingTraceEntry.MibObject.u4FsLsppContextId = 0;
    LsppPingTraceEntry.MibObject.i4FsLsppReplyMode = 4;
    LsppPingTraceEntry.MibObject.i4FsLsppDsMap = LSPP_SNMP_FALSE;
    LsppPingTraceEntry.MibObject.i4FsLsppInterfaceLabelTlv = LSPP_SNMP_FALSE;
    LsppPingTraceEntry.MibObject.i4FsLsppForceExplicitNull = LSPP_SNMP_FALSE;
    LsppPingTraceEntry.MibObject.i4FsLsppFecValidate = LSPP_SNMP_FALSE;
    LsppPingTraceEntry.MibObject.i4FsLsppReversePathVerify = LSPP_SNMP_FALSE;
    LsppPingTraceEntry.MibObject.u4FsLsppEXPValue = 0;
    LsppPingTraceEntry.MibObject.u4FsLsppTTLValue = 40;
    LsppPingTraceEntry.MibObject.u4FsLsppReplyDscpValue = 0;
    LsppPingTraceEntry.MibObject.i4FsLsppSweepOption = LSPP_SNMP_FALSE;
    LsppPingTraceEntry.MibObject.i4FsLsppBurstOption = LSPP_SNMP_FALSE;
    LsppPingTraceEntry.MibObject.u4FsLsppPacketSize = 200;
    LsppPingTraceEntry.MibObject.i4FsLsppReplyPadTlv = LSPP_SNMP_TRUE;

    LsppPingTraceEntry.MibObject.u4FsLsppSenderHandle = 1;
    LsppPingTraceEntry.u4LastGeneratedSeqNum = 1;

    LsppPingTraceEntry.PathId.u1PathType = LSPP_PATH_TYPE_MEP;
    LsppPingTraceEntry.MibObject.i4FsLsppEncapType = LSPP_ENCAP_TYPE_ACH;

    LsppUtFillPathId (&(LsppPingTraceEntry.PathId), LSPP_PATH_TYPE_MEP);
    LsppCoreUpdateEchoMsgInfo (&LsppPingTraceEntry, &LsppEchoMsg,
                               pRxDsMapTlv, u4BfdDisc);

    pMsg = LSPP_ALLOC_CRU_BUF (100, 0);

    /* Failure Case 1: Updating Input with Invalid Target Fec Tlv Type */
    LsppEchoMsg.LsppPduInfo.LsppFecTlv[0].u2TlvType = 100;

    if (LsppTxFrameAndSendPdu (&LsppEchoMsg, &LsppPingTraceEntry,
                               &EchoSequenceEntry, pMsg) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case 2: Encapsulate Pdu Failure */
    LsppEchoMsg.LsppPduInfo.LsppFecTlv[0].u2TlvType = LSPP_FEC_STATIC_LSP;
    LsppEchoMsg.u1LabelStackDepth = 10;

    if (LsppTxFrameAndSendPdu (&LsppEchoMsg, &LsppPingTraceEntry,
                               &EchoSequenceEntry, pMsg) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case 3: Transmit On Socket failure */
    LsppEchoMsg.LsppPduInfo.u2TlvsPresent =
        LsppEchoMsg.LsppPduInfo.u2TlvsPresent | LSPP_REPLY_TOS_BYTE_TLV;

    LsppEchoMsg.LsppPduInfo.LsppHeader.u1ReplyMode = LSPP_REPLY_IP_UDP;
    LsppEchoMsg.LsppPduInfo.LsppHeader.u1MessageType = LSPP_ECHO_REPLY;
    LsppEchoMsg.LsppPduInfo.LsppReplyTosTlv.u1TosValue = 0;

    if (LsppTxFrameAndSendPdu (&LsppEchoMsg, &LsppPingTraceEntry,
                               &EchoSequenceEntry, pMsg) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case 4: Transmit Echo Request Failure */
    LsppEchoMsg.u1LabelStackDepth = 1;
    LSPP_RELEASE_CRU_BUF (pMsg, 0);
    pMsg = NULL;

    if (LsppTxFrameAndSendPdu (&LsppEchoMsg, &LsppPingTraceEntry,
                               &EchoSequenceEntry, pMsg) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/* LsppTxBfdBtStrapReplyPkt */
INT4
LsppUtTx_7 (VOID)
{
    tLsppBfdInfo        LsppBfdInfo;
    UINT4               u4ContextId = 0;

    MEMSET (&LsppBfdInfo, 0, sizeof (tLsppBfdInfo));

    /* Invalid BUF CRU BUF Pointer */
    LsppBfdInfo.u4OffSet = 50;
    LsppBfdInfo.pBuf = NULL;
    if (LsppTxBfdBtStrapReplyPkt (u4ContextId, &LsppBfdInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case: Transmission on socket */
    LsppBfdInfo.u4BfdDiscriminator = 87;
    LsppBfdInfo.u4OffSet = 50;
    LsppBfdInfo.u1ReplyPath = LSPP_REPLY_VIA_IP_PATH;
    LsppBfdInfo.DestIpAddr.u4_addr[0] = 0;

    if (LsppTxBfdBtStrapReplyPkt (u4ContextId, &LsppBfdInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/* LsppTxEncapsulatePdu */
INT4
LsppUtTx_8 (VOID)
{
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;
    tLsppEchoMsg        LsppEchoMsg;

    MEMSET (&LsppEchoMsg, 0, sizeof (tLsppEchoMsg));
    pMsg = LSPP_ALLOC_CRU_BUF (100, 0);

    LsppEchoMsg.u1EncapType = LSPP_ENCAP_TYPE_ACH;
    LsppEchoMsg.LsppAchInfo.AchTlvHeader.u2TlvHdrLen = 0;
    LsppEchoMsg.u1LabelStackDepth = 0;

    if (LsppTxEncapsulatePdu (pMsg, &LsppEchoMsg) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/**************** Rx related UT cases ****************/

/* LsppRxVerifyCcCvCabability */
INT4
LsppUtRx_1 (VOID)
{
    UINT4               u4ContextId = 0;
    UINT1               u1CcReceived = 0;
    UINT1               u1CcSelected = 0;
    UINT1               u1CvSelected = 0;

    /* Success Case */
    u1CcReceived = LSPP_ENCAP_TYPE_ACH_IP;
    u1CcSelected = LSPP_ENCAP_TYPE_ACH_IP;
    u1CvSelected = 0x40;

    if (LsppRxVerifyCcCvCabability (u4ContextId, u1CcReceived,
                                    u1CcSelected, u1CvSelected) != OSIX_SUCCESS)
    {
        return OSIX_SUCCESS;
    }
    /* Failure Case- InValid CC */
    u1CcReceived = LSPP_ENCAP_TYPE_ACH_IP;
    u1CcSelected = LSPP_ENCAP_TYPE_ACH;
    u1CvSelected = 0x40;

    if (LsppRxVerifyCcCvCabability (u4ContextId, u1CcReceived,
                                    u1CcSelected, u1CvSelected) != OSIX_FAILURE)
    {
        return OSIX_SUCCESS;
    }

    /* Failure Case- InValid Cv */
    u1CcReceived = LSPP_ENCAP_TYPE_ACH_IP;
    u1CcSelected = LSPP_ENCAP_TYPE_ACH_IP;
    u1CvSelected = 0x80;

    if (LsppRxVerifyCcCvCabability (u4ContextId, u1CcReceived,
                                    u1CcSelected, u1CvSelected) != OSIX_FAILURE)
    {
        return OSIX_SUCCESS;
    }

    return OSIX_SUCCESS;
}

/* LsppRxGetAchTlv */
INT4
LsppUtRx_2 (VOID)
{
    tLsppAchInfo        LsppAchInfo;
    UINT1               au1Pkt[100];
    UINT1              *pu1Pkt = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2AchTlvLength = 0;
    UINT2               u2TlvType = 0;
    UINT2               u2TlvLength = 0;

    MEMSET (au1Pkt, 0, 100);
    MEMSET (&LsppAchInfo, 0, sizeof (tLsppAchInfo));

    pu1Pkt = au1Pkt;

    /* Failure Case -1, Lsp Mep Already Present */
    u2TlvType = 8;
    u2TlvLength = 12;
    u2TlvType = OSIX_HTONL (u2TlvType);
    u2TlvLength = OSIX_HTONL (u2TlvLength);

    MEMCPY (pu1Pkt, &u2TlvType, 2);
    pu1Pkt = pu1Pkt + 2;
    MEMCPY (pu1Pkt, &u2TlvLength, 2);
    pu1Pkt = pu1Pkt + 2;

    u2AchTlvLength = 100;

    LsppAchInfo.u1AchTlvPresent =
        LsppAchInfo.u1AchTlvPresent | LSPP_ACH_IP_LSP_MEP_TLV;

    if (LsppRxGetAchTlv (&pu1Pkt, &LsppAchInfo,
                         u2AchTlvLength, u4ContextId) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case -2, Pw Mep Already Present */
    u2TlvType = 9;
    u2TlvLength = 16;
    u2TlvType = OSIX_HTONL (u2TlvType);
    u2TlvLength = OSIX_HTONL (u2TlvLength);

    MEMCPY (pu1Pkt, &u2TlvType, 2);
    pu1Pkt = pu1Pkt + 2;
    MEMCPY (pu1Pkt, &u2TlvLength, 2);
    pu1Pkt = pu1Pkt + 2;

    u2AchTlvLength = 100;

    LsppAchInfo.u1AchTlvPresent =
        LsppAchInfo.u1AchTlvPresent | LSPP_ACH_IP_PW_MEP_TLV;

    if (LsppRxGetAchTlv (&pu1Pkt, &LsppAchInfo,
                         u2AchTlvLength, u4ContextId) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case -3, ICC Mep Already Present */
    u2TlvType = 10;
    u2TlvLength = 12;
    u2TlvType = OSIX_HTONL (u2TlvType);
    u2TlvLength = OSIX_HTONL (u2TlvLength);

    MEMCPY (pu1Pkt, &u2TlvType, 2);
    pu1Pkt = pu1Pkt + 2;
    MEMCPY (pu1Pkt, &u2TlvLength, 2);
    pu1Pkt = pu1Pkt + 2;

    u2AchTlvLength = 100;

    LsppAchInfo.u1AchTlvPresent =
        LsppAchInfo.u1AchTlvPresent | LSPP_ACH_ICC_MEP_TLV;

    if (LsppRxGetAchTlv (&pu1Pkt, &LsppAchInfo,
                         u2AchTlvLength, u4ContextId) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case -4, Mip Already Present */
    u2TlvType = 7;
    u2TlvLength = 12;
    u2TlvType = OSIX_HTONL (u2TlvType);
    u2TlvLength = OSIX_HTONL (u2TlvLength);

    MEMCPY (pu1Pkt, &u2TlvType, 2);
    pu1Pkt = pu1Pkt + 2;
    MEMCPY (pu1Pkt, &u2TlvLength, 2);
    pu1Pkt = pu1Pkt + 2;

    u2AchTlvLength = 100;
    LsppAchInfo.u1AchTlvPresent =
        LsppAchInfo.u1AchTlvPresent | LSPP_ACH_ICC_MEP_TLV;

    if (LsppRxGetAchTlv (&pu1Pkt, &LsppAchInfo,
                         u2AchTlvLength, u4ContextId) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case -5, Invalid Ach Tlv Type */
    u2TlvType = 1;
    u2TlvLength = 0;
    u2TlvType = OSIX_HTONL (u2TlvType);
    u2TlvLength = OSIX_HTONL (u2TlvLength);

    MEMCPY (pu1Pkt, &u2TlvType, 2);
    pu1Pkt = pu1Pkt + 2;
    MEMCPY (pu1Pkt, &u2TlvLength, 2);
    pu1Pkt = pu1Pkt + 2;

    u2AchTlvLength = 100;

    if (LsppRxGetAchTlv (&pu1Pkt, &LsppAchInfo,
                         u2AchTlvLength, u4ContextId) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/* LsppRxGetAndValidateIPHeader */
INT4
LsppUtRx_3 (VOID)
{
    UINT1               au1IpHdr[20];
    UINT1              *pIpHeader = NULL;
    t_IP_HEADER         LsppIpHeader;
    t_IP                RxLsppIpHeader;
    UINT4               u4ContextId = 0;

    MEMSET (&au1IpHdr, 0, 20);
    MEMSET (&LsppIpHeader, 0, sizeof (t_IP_HEADER));
    MEMSET (&RxLsppIpHeader, 0, sizeof (t_IP));

    pIpHeader = au1IpHdr;

    /* Case 1- Invalid Source Address */
    LsppIpHeader.u1Ver_hdrlen = IP_VERS_AND_HLEN (4, 5);
    LsppIpHeader.u1Tos = 200;
    LsppIpHeader.u2Totlen = OSIX_HTONS ((UINT2) (100 + IP_HDR_LEN +
                                                 LSPP_UDP_HDR_LEN));
    LsppIpHeader.u2Id = 0;
    LsppIpHeader.u2Fl_offs = 0;
    LsppIpHeader.u1Ttl = 1;
    LsppIpHeader.u1Proto = IPPROTO_UDP;
    LsppIpHeader.u2Cksum = 0;
    LsppIpHeader.u4Src = OSIX_HTONL (0x7f000001);
    LsppIpHeader.u4Dest = OSIX_HTONL (0x7f000001);
    MEMCPY (pIpHeader, &LsppIpHeader, 20);

    if (LsppRxGetAndValidateIPHeader (u4ContextId, &pIpHeader,
                                      &RxLsppIpHeader) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Case 2- Invalid Destination Address */

    MEMSET (&au1IpHdr, 0, 20);
    MEMSET (&LsppIpHeader, 0, sizeof (t_IP_HEADER));
    MEMSET (&RxLsppIpHeader, 0, sizeof (t_IP));

    pIpHeader = au1IpHdr;
    LsppIpHeader.u1Ver_hdrlen = IP_VERS_AND_HLEN (4, 5);
    LsppIpHeader.u1Tos = 200;
    LsppIpHeader.u2Totlen = OSIX_HTONS ((UINT2) (100 + IP_HDR_LEN +
                                                 LSPP_UDP_HDR_LEN));
    LsppIpHeader.u2Id = 0;
    LsppIpHeader.u2Fl_offs = 0;
    LsppIpHeader.u1Ttl = 1;
    LsppIpHeader.u1Proto = IPPROTO_UDP;
    LsppIpHeader.u2Cksum = 0;
    LsppIpHeader.u4Src = OSIX_HTONL (0x0a000001);
    LsppIpHeader.u4Dest = OSIX_HTONL (0x0a000001);
    MEMCPY (pIpHeader, &LsppIpHeader, 20);

    if (LsppRxGetAndValidateIPHeader (u4ContextId, &pIpHeader,
                                      &RxLsppIpHeader) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/* LsppRxGetAndValidateUdpHeader */
INT4
LsppUtRx_4 (VOID)
{
    UINT1               au1UdpHdr[8];
    UINT1              *pUdpHdr = NULL;
    tLsppUdpHeader      FrameUdpHdr;
    tLsppUdpHeader      RxUdpHdr;

    MEMSET (&au1UdpHdr, 0, 8);
    MEMSET (&FrameUdpHdr, 0, sizeof (tLsppUdpHeader));
    MEMSET (&RxUdpHdr, 0, sizeof (tLsppUdpHeader));

    pUdpHdr = au1UdpHdr;

    FrameUdpHdr.u2Dest_u_port = OSIX_HTONS (0x0d0d);
    FrameUdpHdr.u2Src_u_port = OSIX_HTONS (0x0d0d);
    FrameUdpHdr.u2Len = OSIX_HTONS ((UINT2) (100));

    MEMCPY (pUdpHdr, &FrameUdpHdr, 8);

    if (LsppRxGetAndValidateUdpHeader (&pUdpHdr, &RxUdpHdr) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/* LsppRxValidateIpUdpHeader */
INT4
LsppUtRx_5 (VOID)
{
    UINT1               au1EncapHdr[20];
    UINT1              *pu1EncapHdr = NULL;
    tLsppEchoMsg        LsppEchoMsg;

    t_IP_HEADER         LsppIpHeader;
    tLsppUdpHeader      FrameUdpHdr;

    MEMSET (&au1EncapHdr, 0, 20);
    MEMSET (&FrameUdpHdr, 0, sizeof (tLsppUdpHeader));
    MEMSET (&LsppIpHeader, 0, sizeof (t_IP_HEADER));
    MEMSET (&LsppEchoMsg, 0, sizeof (tLsppEchoMsg));

    pu1EncapHdr = au1EncapHdr;

    /* Case 1- Invalid IP header */
    LsppIpHeader.u1Ver_hdrlen = IP_VERS_AND_HLEN (4, 5);
    LsppIpHeader.u1Tos = 200;
    LsppIpHeader.u2Totlen = OSIX_HTONS ((UINT2) (100 + IP_HDR_LEN +
                                                 LSPP_UDP_HDR_LEN));
    LsppIpHeader.u2Id = 0;
    LsppIpHeader.u2Fl_offs = 0;
    LsppIpHeader.u1Ttl = 1;
    LsppIpHeader.u1Proto = IPPROTO_UDP;
    LsppIpHeader.u2Cksum = 0;
    LsppIpHeader.u4Src = OSIX_HTONL (0x7f000001);
    LsppIpHeader.u4Dest = OSIX_HTONL (0x7f000001);
    MEMCPY (pu1EncapHdr, &LsppIpHeader, 20);

    if (LsppRxValidateIpUdpHeader (&pu1EncapHdr,
                                   &(LsppEchoMsg)) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Case -2 Invalid UDP header */
    MEMSET (&au1EncapHdr, 0, 20);
    MEMSET (&LsppIpHeader, 0, sizeof (t_IP_HEADER));
    MEMSET (&LsppEchoMsg, 0, sizeof (tLsppEchoMsg));

    pu1EncapHdr = au1EncapHdr;

    LsppIpHeader.u1Ver_hdrlen = IP_VERS_AND_HLEN (4, 5);
    LsppIpHeader.u1Tos = 200;
    LsppIpHeader.u2Totlen = OSIX_HTONS ((UINT2) (100 + IP_HDR_LEN +
                                                 LSPP_UDP_HDR_LEN));
    LsppIpHeader.u2Id = 0;
    LsppIpHeader.u2Fl_offs = 0;
    LsppIpHeader.u1Ttl = 1;
    LsppIpHeader.u1Proto = IPPROTO_UDP;
    LsppIpHeader.u2Cksum = 0;
    LsppIpHeader.u4Src = OSIX_HTONL (0x0a000001);
    LsppIpHeader.u4Dest = OSIX_HTONL (0x7f000001);
    MEMCPY (pu1EncapHdr, &LsppIpHeader, 20);

    FrameUdpHdr.u2Dest_u_port = OSIX_HTONS (0x0d0d);
    FrameUdpHdr.u2Src_u_port = OSIX_HTONS (0x0d0d);
    FrameUdpHdr.u2Len = OSIX_HTONS ((UINT2) (100));

    MEMCPY (pu1EncapHdr, &FrameUdpHdr, 8);

    if (LsppRxValidateIpUdpHeader (&pu1EncapHdr, &(LsppEchoMsg)) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/* LsppRxValidateLspMepTlv */
INT4
LsppUtRx_6 (VOID)
{
    tMplsIpLspMepTlv    RxLspMepTlv;
    tLsppMegDetail      MegDetail;
    tLsppTnlLspInfo    *pTnlMepInfo = NULL;

    MEMSET (&RxLspMepTlv, 0, sizeof (tMplsIpLspMepTlv));
    MEMSET (&MegDetail, 0, sizeof (tLsppMegDetail));

    /* Failure Case -1, Invalid Global Id */
    pTnlMepInfo = &(MegDetail.unServiceInfo.ServiceTnlId);
    pTnlMepInfo->SrcNodeId.MplsGlobalNodeId.u4GlobalId = 1;
    RxLspMepTlv.GlobalNodeId.u4GlobalId = 2;

    if (LsppRxValidateLspMepTlv (&RxLspMepTlv, pTnlMepInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case -2, Invalid Node Id */
    MEMSET (&RxLspMepTlv, 0, sizeof (tMplsIpLspMepTlv));
    MEMSET (&MegDetail, 0, sizeof (tLsppMegDetail));

    pTnlMepInfo = &(MegDetail.unServiceInfo.ServiceTnlId);
    pTnlMepInfo->SrcNodeId.MplsGlobalNodeId.u4GlobalId = 1;
    pTnlMepInfo->SrcNodeId.MplsGlobalNodeId.u4NodeId = 101;
    pTnlMepInfo->u4SrcTnlNum = 100;
    pTnlMepInfo->u4LspNum = 1;

    RxLspMepTlv.GlobalNodeId.u4GlobalId = 1;
    RxLspMepTlv.GlobalNodeId.u4NodeId = 102;
    RxLspMepTlv.u2TnlNum = 100;
    RxLspMepTlv.u2LspNum = 1;

    if (LsppRxValidateLspMepTlv (&RxLspMepTlv, pTnlMepInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case -3, Invalid Tunnel Id */
    MEMSET (&RxLspMepTlv, 0, sizeof (tMplsIpLspMepTlv));
    MEMSET (&MegDetail, 0, sizeof (tLsppMegDetail));

    pTnlMepInfo = &(MegDetail.unServiceInfo.ServiceTnlId);
    pTnlMepInfo->SrcNodeId.MplsGlobalNodeId.u4GlobalId = 1;
    pTnlMepInfo->SrcNodeId.MplsGlobalNodeId.u4NodeId = 101;
    pTnlMepInfo->u4SrcTnlNum = 100;
    pTnlMepInfo->u4LspNum = 1;

    RxLspMepTlv.GlobalNodeId.u4GlobalId = 1;
    RxLspMepTlv.GlobalNodeId.u4NodeId = 101;
    RxLspMepTlv.u2TnlNum = 101;
    RxLspMepTlv.u2LspNum = 1;

    if (LsppRxValidateLspMepTlv (&RxLspMepTlv, pTnlMepInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case -4, Invalid Lsp Id */
    MEMSET (&RxLspMepTlv, 0, sizeof (tMplsIpLspMepTlv));
    MEMSET (&MegDetail, 0, sizeof (tLsppMegDetail));

    pTnlMepInfo = &(MegDetail.unServiceInfo.ServiceTnlId);
    pTnlMepInfo->SrcNodeId.MplsGlobalNodeId.u4GlobalId = 1;
    pTnlMepInfo->SrcNodeId.MplsGlobalNodeId.u4NodeId = 101;
    pTnlMepInfo->u4SrcTnlNum = 100;
    pTnlMepInfo->u4LspNum = 1;

    RxLspMepTlv.GlobalNodeId.u4GlobalId = 1;
    RxLspMepTlv.GlobalNodeId.u4NodeId = 101;
    RxLspMepTlv.u2TnlNum = 100;
    RxLspMepTlv.u2LspNum = 2;

    if (LsppRxValidateLspMepTlv (&RxLspMepTlv, pTnlMepInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/* LsppRxValidatePwMepTlv */
INT4
LsppUtRx_7 (VOID)
{
    tMplsIpPwMepTlv     RxIpPwMepTlv;
    tLsppPwDetail       PwDetail;
    tLsppPwFecInfo     *pPwMepInfo = NULL;

    pPwMepInfo = &(PwDetail.PwFecInfo);

    /* Updating Inputs */

    /* Failure Case 1- Invalid Pw Global Id */
    MEMSET (&RxIpPwMepTlv, 0, sizeof (tMplsIpPwMepTlv));
    MEMSET (&PwDetail, 0, sizeof (tLsppPwDetail));

    pPwMepInfo->DstNodeId.MplsGlobalNodeId.u4GlobalId = 1;
    pPwMepInfo->DstNodeId.MplsGlobalNodeId.u4NodeId = 101;
    pPwMepInfo->u4DstAcId = 1;
    pPwMepInfo->u1AgiLen = 8;
    pPwMepInfo->u1AgiType = 2;
    MEMCPY (pPwMepInfo->au1Agi, "SRCAGI", pPwMepInfo->u1AgiLen);

    RxIpPwMepTlv.u4GlobalId = 2;
    RxIpPwMepTlv.u4NodeId = 101;
    RxIpPwMepTlv.u4AcId = 1;
    RxIpPwMepTlv.u1AgiLen = 8;
    RxIpPwMepTlv.u1AgiType = 2;
    MEMCPY (RxIpPwMepTlv.au1Agi, "SRCAGI", RxIpPwMepTlv.u1AgiLen);

    if (LsppRxValidatePwMepTlv (&RxIpPwMepTlv, &PwDetail) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;

    }

    /* Failure Case 2- Invalid Pw Node Id */
    MEMSET (&RxIpPwMepTlv, 0, sizeof (tMplsIpPwMepTlv));
    MEMSET (&PwDetail, 0, sizeof (tLsppPwDetail));

    pPwMepInfo->DstNodeId.MplsGlobalNodeId.u4GlobalId = 1;
    pPwMepInfo->DstNodeId.MplsGlobalNodeId.u4NodeId = 101;
    pPwMepInfo->u4DstAcId = 1;
    pPwMepInfo->u1AgiLen = 8;
    pPwMepInfo->u1AgiType = 2;
    MEMCPY (pPwMepInfo->au1Agi, "SRCAGI", pPwMepInfo->u1AgiLen);

    RxIpPwMepTlv.u4GlobalId = 1;
    RxIpPwMepTlv.u4NodeId = 102;
    RxIpPwMepTlv.u4AcId = 1;
    RxIpPwMepTlv.u1AgiLen = 8;
    RxIpPwMepTlv.u1AgiType = 2;
    MEMCPY (RxIpPwMepTlv.au1Agi, "SRCAGI", RxIpPwMepTlv.u1AgiLen);

    if (LsppRxValidatePwMepTlv (&RxIpPwMepTlv, &PwDetail) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;

    }

    /* Failure Case 3- Invalid Destiantion AcId */
    MEMSET (&RxIpPwMepTlv, 0, sizeof (tMplsIpPwMepTlv));
    MEMSET (&PwDetail, 0, sizeof (tLsppPwDetail));

    pPwMepInfo->DstNodeId.MplsGlobalNodeId.u4GlobalId = 1;
    pPwMepInfo->DstNodeId.MplsGlobalNodeId.u4NodeId = 101;
    pPwMepInfo->u4DstAcId = 2;
    pPwMepInfo->u1AgiLen = 8;
    pPwMepInfo->u1AgiType = 2;
    MEMCPY (pPwMepInfo->au1Agi, "SRCAGI", pPwMepInfo->u1AgiLen);

    RxIpPwMepTlv.u4GlobalId = 1;
    RxIpPwMepTlv.u4NodeId = 101;
    RxIpPwMepTlv.u4AcId = 1;
    RxIpPwMepTlv.u1AgiLen = 8;
    RxIpPwMepTlv.u1AgiType = 2;
    MEMCPY (RxIpPwMepTlv.au1Agi, "SRCAGI", RxIpPwMepTlv.u1AgiLen);

    if (LsppRxValidatePwMepTlv (&RxIpPwMepTlv, &PwDetail) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;

    }

    /* Failure Case 4- Invalid Agi Type */
    MEMSET (&RxIpPwMepTlv, 0, sizeof (tMplsIpPwMepTlv));
    MEMSET (&PwDetail, 0, sizeof (tLsppPwDetail));

    pPwMepInfo->DstNodeId.MplsGlobalNodeId.u4GlobalId = 1;
    pPwMepInfo->DstNodeId.MplsGlobalNodeId.u4NodeId = 101;
    pPwMepInfo->u4DstAcId = 1;
    pPwMepInfo->u1AgiLen = 8;
    pPwMepInfo->u1AgiType = 1;
    MEMCPY (pPwMepInfo->au1Agi, "SRCAGI", pPwMepInfo->u1AgiLen);

    RxIpPwMepTlv.u4GlobalId = 1;
    RxIpPwMepTlv.u4NodeId = 101;
    RxIpPwMepTlv.u4AcId = 1;
    RxIpPwMepTlv.u1AgiLen = 8;
    RxIpPwMepTlv.u1AgiType = 2;
    MEMCPY (RxIpPwMepTlv.au1Agi, "SRCAGI", RxIpPwMepTlv.u1AgiLen);

    if (LsppRxValidatePwMepTlv (&RxIpPwMepTlv, &PwDetail) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;

    }

    /* Failure Case 5- Invalid Pw Agi len */
    MEMSET (&RxIpPwMepTlv, 0, sizeof (tMplsIpPwMepTlv));
    MEMSET (&PwDetail, 0, sizeof (tLsppPwDetail));

    pPwMepInfo->DstNodeId.MplsGlobalNodeId.u4GlobalId = 1;
    pPwMepInfo->DstNodeId.MplsGlobalNodeId.u4NodeId = 101;
    pPwMepInfo->u4DstAcId = 1;
    pPwMepInfo->u1AgiLen = 10;
    pPwMepInfo->u1AgiType = 2;
    MEMCPY (pPwMepInfo->au1Agi, "SRCAGI", pPwMepInfo->u1AgiLen);

    RxIpPwMepTlv.u4GlobalId = 1;
    RxIpPwMepTlv.u4NodeId = 101;
    RxIpPwMepTlv.u4AcId = 1;
    RxIpPwMepTlv.u1AgiLen = 8;
    RxIpPwMepTlv.u1AgiType = 2;
    MEMCPY (RxIpPwMepTlv.au1Agi, "SRCAGI", RxIpPwMepTlv.u1AgiLen);

    if (LsppRxValidatePwMepTlv (&RxIpPwMepTlv, &PwDetail) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;

    }

    /* Failure Case 6- Invalid Pw Agi Val */
    MEMSET (&RxIpPwMepTlv, 0, sizeof (tMplsIpPwMepTlv));
    MEMSET (&PwDetail, 0, sizeof (tLsppPwDetail));

    pPwMepInfo->DstNodeId.MplsGlobalNodeId.u4GlobalId = 1;
    pPwMepInfo->DstNodeId.MplsGlobalNodeId.u4NodeId = 101;
    pPwMepInfo->u4DstAcId = 1;
    pPwMepInfo->u1AgiLen = 8;
    pPwMepInfo->u1AgiType = 2;
    MEMCPY (pPwMepInfo->au1Agi, "SRCAGI", pPwMepInfo->u1AgiLen);

    RxIpPwMepTlv.u4GlobalId = 1;
    RxIpPwMepTlv.u4NodeId = 101;
    RxIpPwMepTlv.u4AcId = 1;
    RxIpPwMepTlv.u1AgiLen = 8;
    RxIpPwMepTlv.u1AgiType = 2;
    MEMCPY (RxIpPwMepTlv.au1Agi, "DSTAGI", RxIpPwMepTlv.u1AgiLen);

    if (LsppRxValidatePwMepTlv (&RxIpPwMepTlv, &PwDetail) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;

    }

    return OSIX_SUCCESS;
}

/* LsppRxValidateIccMepTlv */
INT4
LsppUtRx_8 (VOID)
{
    tMplsIccMepTlv      RxIccMepTlv;
    tLsppMegDetail      MegDetail;

    MEMSET (&RxIccMepTlv, 0, sizeof (tMplsIccMepTlv));
    MEMSET (&MegDetail, 0, sizeof (tLsppMegDetail));

    /* Failure Case 1 - Invalid Icc Id */
    MEMCPY (RxIccMepTlv.au1Icc, "ICCMEG", 7);
    MEMCPY (MegDetail.au1Icc, "ICC", 7);

    if (LsppRxValidateIccMepTlv (&RxIccMepTlv, &MegDetail) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case 2 - Invalid UMC Id */
    MEMSET (&RxIccMepTlv, 0, sizeof (tMplsIccMepTlv));
    MEMSET (&MegDetail, 0, sizeof (tLsppMegDetail));

    MEMCPY (RxIccMepTlv.au1Icc, "ICCMEG", 7);
    MEMCPY (MegDetail.au1Icc, "ICCMEG", 7);
    MEMCPY (RxIccMepTlv.au1Umc, "UMC", 3);
    MEMCPY (MegDetail.au1Umc, "CNU", 3);

    if (LsppRxValidateIccMepTlv (&RxIccMepTlv, &MegDetail) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case 3 - Invalid UMC Id */
    MEMSET (&RxIccMepTlv, 0, sizeof (tMplsIccMepTlv));
    MEMSET (&MegDetail, 0, sizeof (tLsppMegDetail));

    MEMCPY (RxIccMepTlv.au1Icc, "ICCMEG", 7);
    MEMCPY (MegDetail.au1Icc, "ICCMEG", 7);
    MEMCPY (RxIccMepTlv.au1Umc, "UMC", 3);
    MEMCPY (MegDetail.au1Umc, "UMC", 3);
    RxIccMepTlv.u2MepIndex = 1;
    MegDetail.u4IccSrcMepIndex = 2;

    if (LsppRxValidateIccMepTlv (&RxIccMepTlv, &MegDetail) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*LsppRxValidateMipTlv*/
INT4
LsppUtRx_9 (VOID)
{
    tMplsMipTlv         RxMipTlv;
    tLsppMegDetail      MegDetail;

    MEMSET (&RxMipTlv, 0, sizeof (tMplsMipTlv));
    MEMSET (&MegDetail, 0, sizeof (tLsppMegDetail));

    /* Failure 1- Invalid Global Id */
    RxMipTlv.GlobalNodeId.u4GlobalId = 1;
    MegDetail.MplsNodeId.u4GlobalId = 2;

    if (LsppRxValidateMipTlv (&RxMipTlv, &MegDetail) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure 2- Invalid Global Id */
    MEMSET (&RxMipTlv, 0, sizeof (tMplsMipTlv));
    MEMSET (&MegDetail, 0, sizeof (tLsppMegDetail));
    RxMipTlv.GlobalNodeId.u4GlobalId = 1;
    MegDetail.MplsNodeId.u4GlobalId = 1;

    RxMipTlv.GlobalNodeId.u4NodeId = 101;
    MegDetail.MplsNodeId.u4NodeId = 102;

    if (LsppRxValidateMipTlv (&RxMipTlv, &MegDetail) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure 3- Invalid IfNum */
    MEMSET (&RxMipTlv, 0, sizeof (tMplsMipTlv));
    MEMSET (&MegDetail, 0, sizeof (tLsppMegDetail));
    RxMipTlv.GlobalNodeId.u4GlobalId = 1;
    MegDetail.MplsNodeId.u4GlobalId = 1;

    RxMipTlv.GlobalNodeId.u4NodeId = 101;
    MegDetail.MplsNodeId.u4NodeId = 101;

    RxMipTlv.u4IfNum = 1;
    MegDetail.u4MpIfIndex = 2;

    if (LsppRxValidateMipTlv (&RxMipTlv, &MegDetail) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*LsppRxValidateAchTlvs */
INT4
LsppUtRx_10 (VOID)
{
    tLsppAchInfo        LsppAchInfo;
    tLsppPathInfo       LsppPathInfo;
    tMplsIpLspMepTlv   *pRxLspMepTlv = NULL;
    tLsppMegDetail     *pMegDetail = NULL;
    tLsppPwDetail      *pPwDetail = NULL;

    UINT4               u4ContextId = 0;

    /* Invalid LSP Mep Tlv */
    MEMSET (&LsppAchInfo, 0, sizeof (tLsppAchInfo));
    MEMSET (&LsppPathInfo, 0, sizeof (tLsppPathInfo));

    pRxLspMepTlv = &(LsppAchInfo.LsppMepTlv.unMepTlv.IpLspMepTlv);
    pMegDetail = &(LsppPathInfo.MegDetail);
    LsppAchInfo.u1AchTlvPresent =
        LsppAchInfo.u1AchTlvPresent | LSPP_ACH_IP_LSP_MEP_TLV;

    pRxLspMepTlv->GlobalNodeId.u4GlobalId = 2;
    pMegDetail->unServiceInfo.ServiceTnlId.SrcNodeId.
        MplsGlobalNodeId.u4GlobalId = 1;

    if (LsppRxValidateAchTlvs (u4ContextId, &LsppAchInfo, &LsppPathInfo) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Invalid Pw Mep Tlv */
    MEMSET (&LsppAchInfo, 0, sizeof (tLsppAchInfo));
    MEMSET (&LsppPathInfo, 0, sizeof (tLsppPathInfo));

    pPwDetail = &(LsppPathInfo.PwDetail);
    LsppAchInfo.u1AchTlvPresent =
        LsppAchInfo.u1AchTlvPresent | LSPP_ACH_IP_PW_MEP_TLV;

    LsppAchInfo.LsppMepTlv.unMepTlv.IpPwMepTlv.u4GlobalId = 1;
    pPwDetail->PwFecInfo.DstNodeId.MplsGlobalNodeId.u4GlobalId = 2;

    if (LsppRxValidateAchTlvs (u4ContextId, &LsppAchInfo, &LsppPathInfo) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Invalid ICC Mep Tlv */
    MEMSET (&LsppAchInfo, 0, sizeof (tLsppAchInfo));
    MEMSET (&LsppPathInfo, 0, sizeof (tLsppPathInfo));

    MEMCPY (LsppAchInfo.LsppMepTlv.unMepTlv.IccMepTlv.au1Icc, "ABCD", 4);
    MEMCPY (LsppPathInfo.MegDetail.au1Icc, "DCBA", 4);

    LsppAchInfo.u1AchTlvPresent =
        LsppAchInfo.u1AchTlvPresent | LSPP_ACH_ICC_MEP_TLV;

    if (LsppRxValidateAchTlvs (u4ContextId, &LsppAchInfo, &LsppPathInfo) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Invalid MIP TLV */

    MEMSET (&LsppAchInfo, 0, sizeof (tLsppAchInfo));
    MEMSET (&LsppPathInfo, 0, sizeof (tLsppPathInfo));

    LsppAchInfo.LsppMipTlv.GlobalNodeId.u4GlobalId = 1;
    LsppPathInfo.MegDetail.MplsNodeId.u4GlobalId = 2;

    LsppAchInfo.u1AchTlvPresent =
        LsppAchInfo.u1AchTlvPresent | LSPP_ACH_MIP_TLV;

    if (LsppRxValidateAchTlvs (u4ContextId, &LsppAchInfo, &LsppPathInfo) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/* LsppRxMatchEchoSequenceEntry */
INT4
LsppUtRx_11 (VOID)
{
    tLsppHeader         LsppHeader;
    tLsppFsLsppPingTraceTableEntry PingTraceEntry;
    tLsppFsLsppPingTraceTableEntry *pLsppPingTraceEntry = NULL;
    tLsppFsLsppEchoSequenceTableEntry EchoSequenceEntry;
    tLsppFsLsppEchoSequenceTableEntry *pLsppEchoSequenceEntry = NULL;
    UINT4               u4ContextId = 0;

    MEMSET (&LsppHeader, 0, sizeof (tLsppHeader));

    /* Invalid Senders Handle */
    LsppHeader.u4SenderHandle = 1;

    if (LsppRxMatchEchoSequenceEntry (u4ContextId, &LsppHeader,
                                      &pLsppPingTraceEntry,
                                      &pLsppEchoSequenceEntry) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Invalid Sequence number */
    MEMSET (&LsppHeader, 0, sizeof (tLsppHeader));
    MEMSET (&PingTraceEntry, 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Creating a node with Senders handle 1 */
    PingTraceEntry.MibObject.u4FsLsppContextId = 0;
    PingTraceEntry.MibObject.u4FsLsppSenderHandle = 1;

    /* Adding ping trace node */
    pLsppPingTraceEntry = LsppFsLsppPingTraceTableCreateApi (&PingTraceEntry);

    LsppHeader.u4SenderHandle = 1;
    LsppHeader.u4SequenceNum = 1;

    if (LsppRxMatchEchoSequenceEntry (u4ContextId, &LsppHeader,
                                      &pLsppPingTraceEntry,
                                      &pLsppEchoSequenceEntry) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    /* Invalid Sequence number */
    MEMSET (&LsppHeader, 0, sizeof (tLsppHeader));
    MEMSET (&PingTraceEntry, 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Creating A node in sequence tree */
    EchoSequenceEntry.MibObject.u4FsLsppContextId = 0;
    EchoSequenceEntry.MibObject.u4FsLsppSenderHandle = 1;
    EchoSequenceEntry.MibObject.u4FsLsppSequenceNumber = 1;
    EchoSequenceEntry.u1Status = LSPP_ECHO_SEQ_COMPLETED;

    pLsppEchoSequenceEntry = LsppFsLsppEchoSequenceTableCreateApi
        (&EchoSequenceEntry);

    LsppHeader.u4SenderHandle = 1;
    LsppHeader.u4SequenceNum = 1;

    if (LsppRxMatchEchoSequenceEntry (u4ContextId, &LsppHeader,
                                      &pLsppPingTraceEntry,
                                      &pLsppEchoSequenceEntry) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*LsppRxValidateLsppHeader */
INT4
LsppUtRx_12 (VOID)
{
    tLsppHeader         LsppHeader;
    UINT1               u1MsgTypeInvalid = 0;
    UINT4               u4ContextId = 0;

    /* Invalid version field */
    MEMSET (&LsppHeader, 0, sizeof (tLsppHeader));

    if (LsppRxValidateLsppHeader (&LsppHeader, &u1MsgTypeInvalid, u4ContextId)
        != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Invalid Global Flags */
    MEMSET (&LsppHeader, 0, sizeof (tLsppHeader));
    LsppHeader.u2Version = 1;
    LsppHeader.u2GlobalFlag = 5;

    if (LsppRxValidateLsppHeader (&LsppHeader, &u1MsgTypeInvalid, u4ContextId)
        != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Invalid Message type */
    MEMSET (&LsppHeader, 0, sizeof (tLsppHeader));
    LsppHeader.u2Version = 1;
    LsppHeader.u2GlobalFlag = 0;
    LsppHeader.u1MessageType = 5;

    if (LsppRxValidateLsppHeader (&LsppHeader, &u1MsgTypeInvalid, u4ContextId)
        != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Invalid Reply Mode */
    MEMSET (&LsppHeader, 0, sizeof (tLsppHeader));
    LsppHeader.u2Version = 1;
    LsppHeader.u2GlobalFlag = 0;
    LsppHeader.u1MessageType = 1;
    LsppHeader.u1ReplyMode = 7;

    if (LsppRxValidateLsppHeader (&LsppHeader, &u1MsgTypeInvalid, u4ContextId)
        != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Invalid Reply Mode */
    MEMSET (&LsppHeader, 0, sizeof (tLsppHeader));
    LsppHeader.u2Version = 1;
    LsppHeader.u2GlobalFlag = 0;
    LsppHeader.u1MessageType = 2;
    LsppHeader.u1ReplyMode = 3;
    LsppHeader.u1ReturnCode = 16;
    LsppHeader.u1ReturnSubCode = 99;

    if (LsppRxValidateLsppHeader (&LsppHeader, &u1MsgTypeInvalid, u4ContextId)
        != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Invalid Reply Mode */
    MEMSET (&LsppHeader, 0, sizeof (tLsppHeader));
    LsppHeader.u2Version = 1;
    LsppHeader.u2GlobalFlag = 0;
    LsppHeader.u1MessageType = 1;
    LsppHeader.u1ReplyMode = 3;
    LsppHeader.u1ReturnCode = 1;
    LsppHeader.u1ReturnSubCode = 1;

    if (LsppRxValidateLsppHeader (&LsppHeader, &u1MsgTypeInvalid, u4ContextId)
        != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/* LsppRxValidateDsmapTlv */
INT4
LsppUtRx_13 (VOID)
{
    tLsppDsMapTlv       LsppDsMapTlv;
    UINT4               u4ContextId = 0;
    UINT1               u1ReturnCode = 0;

    /* Invalid Address type */
    MEMSET (&LsppDsMapTlv, 0, sizeof (tLsppDsMapTlv));
    LsppDsMapTlv.u1AddrType = 6;

    if (LsppRxValidateDsmapTlv (&LsppDsMapTlv, &u1ReturnCode, u4ContextId)
        != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Invalid DS Flag */
    MEMSET (&LsppDsMapTlv, 0, sizeof (tLsppDsMapTlv));
    LsppDsMapTlv.u1AddrType = 2;
    LsppDsMapTlv.u1DsFlags = 8;

    if (LsppRxValidateDsmapTlv (&LsppDsMapTlv, &u1ReturnCode, u4ContextId)
        != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Invalid Multipath TYPE */
    MEMSET (&LsppDsMapTlv, 0, sizeof (tLsppDsMapTlv));
    LsppDsMapTlv.u1AddrType = 2;
    LsppDsMapTlv.u1DsFlags = 0;
    LsppDsMapTlv.u1MultipathType = 1;

    if (LsppRxValidateDsmapTlv (&LsppDsMapTlv, &u1ReturnCode, u4ContextId)
        != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Invalid Multipath TYPE Length */
    MEMSET (&LsppDsMapTlv, 0, sizeof (tLsppDsMapTlv));
    LsppDsMapTlv.u1AddrType = 2;
    LsppDsMapTlv.u1DsFlags = 0;
    LsppDsMapTlv.u1MultipathType = 0;
    LsppDsMapTlv.u2MultipathLength = 1;

    if (LsppRxValidateDsmapTlv (&LsppDsMapTlv, &u1ReturnCode, u4ContextId)
        != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Invalid DS label stack depth */
    MEMSET (&LsppDsMapTlv, 0, sizeof (tLsppDsMapTlv));
    LsppDsMapTlv.u1AddrType = 2;
    LsppDsMapTlv.u1DsFlags = 0;
    LsppDsMapTlv.u1MultipathType = 0;
    LsppDsMapTlv.u2MultipathLength = 0;
    LsppDsMapTlv.u1DsLblStackDepth = 98;

    if (LsppRxValidateDsmapTlv (&LsppDsMapTlv, &u1ReturnCode, u4ContextId)
        != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Invalid DS label protocol */
    MEMSET (&LsppDsMapTlv, 0, sizeof (tLsppDsMapTlv));
    LsppDsMapTlv.u1AddrType = 2;
    LsppDsMapTlv.u1DsFlags = 0;
    LsppDsMapTlv.u1MultipathType = 0;
    LsppDsMapTlv.u2MultipathLength = 0;
    LsppDsMapTlv.u1DsLblStackDepth = 1;
    LsppDsMapTlv.DsLblInfo[0].u1Protocol = 9;

    if (LsppRxValidateDsmapTlv (&LsppDsMapTlv, &u1ReturnCode, u4ContextId)
        != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*LsppRxValidateReplyTosTlv */
INT4
LsppUtRx_14 (VOID)
{
    tLsppReplyTosTlv    LsppReplyTosTlv;
    UINT4               u4ContextId = 0;

    MEMSET (&LsppReplyTosTlv, 0, sizeof (tLsppReplyTosTlv));

    if (LsppRxValidateReplyTosTlv (&LsppReplyTosTlv, u4ContextId) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/* LsppRxValidatePadTlv */
INT4
LsppUtRx_15 (VOID)
{
    tLsppPadTlv         LsppPadTlv;
    UINT4               u4ContextId = 0;
    UINT1               u1ReqType = 0;

    MEMSET (&LsppPadTlv, 0, sizeof (tLsppPadTlv));
    u1ReqType = LSPP_ECHO_REQUEST;
    LsppPadTlv.u1FirstOctet = 34;

    if (LsppRxValidatePadTlv (u1ReqType, &LsppPadTlv, u4ContextId) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (&LsppPadTlv, 0, sizeof (tLsppPadTlv));
    u1ReqType = LSPP_ECHO_REPLY;
    LsppPadTlv.u1FirstOctet = 1;

    if (LsppRxValidatePadTlv (u1ReqType, &LsppPadTlv, u4ContextId) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/* LsppRxValidateIfLblStkTlv */
INT4
LsppUtRx_16 (VOID)
{
    tLsppIfLblStkTlv    IfLblStkTlv;
    UINT4               u4ContextId = 0;

    /* Invalid Address Type */
    MEMSET (&IfLblStkTlv, 0, sizeof (tLsppIfLblStkTlv));

    IfLblStkTlv.u1AddrType = 9;

    if (LsppRxValidateIfLblStkTlv (&IfLblStkTlv, u4ContextId) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Invalid label depth */
    MEMSET (&IfLblStkTlv, 0, sizeof (tLsppIfLblStkTlv));

    IfLblStkTlv.u1AddrType = 3;
    IfLblStkTlv.u1LblStackDepth = 9;

    if (LsppRxValidateIfLblStkTlv (&IfLblStkTlv, u4ContextId) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Invalid label depth */
    MEMSET (&IfLblStkTlv, 0, sizeof (tLsppIfLblStkTlv));

    IfLblStkTlv.u1AddrType = 3;
    IfLblStkTlv.u1LblStackDepth = 1;
    IfLblStkTlv.RxLblInfo[0].u1Protocol = 9;

    if (LsppRxValidateIfLblStkTlv (&IfLblStkTlv, u4ContextId) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/* LsppRxProcessPdu */
INT4
LsppUtRx_17 (VOID)
{
    tLsppEchoMsg        LsppEchoMsg;
    tLsppFsLsppPingTraceTableEntry PingTraceEntry;
    tLsppFsLsppPingTraceTableEntry *pLsppPingTraceEntry = NULL;
    tLsppFsLsppEchoSequenceTableEntry EchoSequenceEntry;
    tLsppFsLsppEchoSequenceTableEntry *pLsppEchoSequenceEntry = NULL;
    UINT4               u4PktSize = 200;
    UINT1               au1RxPkt[100];
    UINT1              *pRxPkt = NULL;
    UINT4               u4Label = 0;
    UINT4               u4MplsHdr = 0;
    UINT1               u1Exp = 0;
    UINT1               u1SI = 0;
    UINT1               u1Ttl = 0;
    UINT1               u1Val = 0;
    UINT1               u4BfdDisc = 0;

    MEMSET (&PingTraceEntry, 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&EchoSequenceEntry, 0, sizeof (tLsppFsLsppEchoSequenceTableEntry));
    MEMSET (&LsppEchoMsg, 0, sizeof (tLsppEchoMsg));

    LsppEchoMsg.u4ContextId = 0;
    LsppEchoMsg.u4IfIndex = 1;

    /* Failure Case 1 - MPLS Packet */
    MEMSET (au1RxPkt, 0, 100);
    u4Label = 20010;
    u1Exp = 1;
    u1SI = 0;
    u1Ttl = 255;

    pRxPkt = au1RxPkt;
    u4MplsHdr =
        (((u4Label << 12) & 0xfffff000) | ((u1Exp << 9) & 0x00000e00) |
         ((u1SI << 8) & 0x00000100) | (u1Ttl & 0x000000ff));

    while (u1Val < 9)
    {
        MEMCPY (pRxPkt, &u4MplsHdr, 4);
        pRxPkt = pRxPkt + 4;
        u1Val++;
    }

    u4Label = 20010;
    u1Exp = 1;
    u1SI = 1;
    u1Ttl = 1;

    u4MplsHdr =
        (((u4Label << 12) & 0xfffff000) | ((u1Exp << 9) & 0x00000e00) |
         ((u1SI << 8) & 0x00000100) | (u1Ttl & 0x000000ff));

    u4MplsHdr = OSIX_HTONL (u4MplsHdr);

    MEMCPY (pRxPkt, &u4MplsHdr, 4);
    pRxPkt = pRxPkt + 4;

    pRxPkt = au1RxPkt;

    if (LsppRxProcessPdu (&LsppEchoMsg, pRxPkt, u4PktSize, OSIX_TRUE) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case 2 - Sanity Validation Failure */
    MEMSET (au1RxPkt, 0, 100);
    pRxPkt = au1RxPkt;

    LsppUtFrameHeader (&pRxPkt, LSPP_ECHO_REPLY, LSPP_INVALID_PKT);

    pRxPkt = au1RxPkt;

    if (LsppRxProcessPdu (&LsppEchoMsg, pRxPkt, u4PktSize, OSIX_FALSE) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case 3 - Match Entry not found */
    MEMSET (au1RxPkt, 0, 100);
    u4PktSize = 32;
    pRxPkt = au1RxPkt;

    LsppUtFrameHeader (&pRxPkt, LSPP_ECHO_REPLY, LSPP_VALID_PKT);
    pRxPkt = au1RxPkt;

    if (LsppRxProcessPdu (&LsppEchoMsg, pRxPkt, u4PktSize, OSIX_FALSE) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case 4 - Process Echo req Failed */
    MEMSET (au1RxPkt, 0, 100);
    u4PktSize = 32;
    pRxPkt = au1RxPkt;

    /* Creating a node with Senders handle 1 */
    PingTraceEntry.MibObject.u4FsLsppContextId = 0;
    PingTraceEntry.MibObject.u4FsLsppSenderHandle = 1;
    PingTraceEntry.MibObject.i4FsLsppStatus = LSPP_ECHO_SUCCESS;

    /* Adding ping trace node */
    pLsppPingTraceEntry = LsppFsLsppPingTraceTableCreateApi (&PingTraceEntry);

    MEMSET (&EchoSequenceEntry, 0, sizeof (tLsppFsLsppEchoSequenceTableEntry));

    /* Creating A node in sequence tree */
    EchoSequenceEntry.MibObject.u4FsLsppContextId = 0;
    EchoSequenceEntry.MibObject.u4FsLsppSenderHandle = 1;
    EchoSequenceEntry.MibObject.u4FsLsppSequenceNumber = 1;
    EchoSequenceEntry.u1Status = LSPP_ECHO_SEQ_IN_PROGRESS;

    pLsppEchoSequenceEntry = LsppFsLsppEchoSequenceTableCreateApi
        (&EchoSequenceEntry);

    LsppUtFrameHeader (&pRxPkt, LSPP_ECHO_REPLY, LSPP_VALID_PKT);
    pRxPkt = au1RxPkt;

    if (LsppRxProcessPdu (&LsppEchoMsg, pRxPkt, u4PktSize, OSIX_FALSE) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case 5 - Sending Packet TO BFD failure */

    /* Creating a node with Senders handle 1 */
    PingTraceEntry.MibObject.u4FsLsppContextId = 0;
    PingTraceEntry.MibObject.u4FsLsppSenderHandle = 1;
    PingTraceEntry.MibObject.i4FsLsppStatus = LSPP_ECHO_IN_PROGRESS;

    /* Adding ping trace node */
    pLsppPingTraceEntry = LsppFsLsppPingTraceTableCreateApi (&PingTraceEntry);

    MEMSET (&EchoSequenceEntry, 0, sizeof (tLsppFsLsppEchoSequenceTableEntry));

    /* Creating A node in sequence tree */
    EchoSequenceEntry.MibObject.u4FsLsppContextId = 0;
    EchoSequenceEntry.MibObject.u4FsLsppSenderHandle = 1;
    EchoSequenceEntry.MibObject.u4FsLsppSequenceNumber = 1;
    EchoSequenceEntry.u1Status = LSPP_ECHO_SEQ_IN_PROGRESS;

    pLsppEchoSequenceEntry = LsppFsLsppEchoSequenceTableCreateApi
        (&EchoSequenceEntry);

    /* Creating Input for the Funciton */
    MEMSET (au1RxPkt, 0, 100);
    u4PktSize = 40;
    pRxPkt = au1RxPkt;
    LsppUtFrameHeader (&pRxPkt, LSPP_ECHO_REPLY, LSPP_VALID_PKT);

    /* Updating BFD Related Inputs */
    u1Val = 17;
    MEMCPY (pRxPkt, &u1Val, 2);
    pRxPkt = pRxPkt + 2;

    u1Val = 4;
    MEMCPY (pRxPkt, &u1Val, 2);
    pRxPkt = pRxPkt + 2;

    u4BfdDisc = 255;
    MEMCPY (pRxPkt, &u4BfdDisc, 4);
    pRxPkt = pRxPkt + 4;

    if (LsppRxProcessPdu (&LsppEchoMsg, pRxPkt, u4PktSize, OSIX_FALSE) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

#if 1
/* LsppRxDecodeValidatePdu */
INT4
LsppUtRx_18 (VOID)
{
    UINT1               au1RxPkt[1000];
    UINT1              *pRxPkt;
    UINT2               u2TlvType = 0;
    UINT2               u2SubTlvType = 0;
    tLsppPduInfo        LsppPduInfo;
    UINT4               u4ContextId = 0;
    UINT4               u4MplsHdr = 0;
    UINT1               u1FecStackDepth = 0;
    UINT1               u1MsgType = 0;
    UINT2               u2TlvLength = 0;
    UINT1               u1Result = 0;
    UINT4               u4Label = 0;
    UINT1               u1Exp = 0;
    UINT1               u1SI = 0;
    UINT1               u1Ttl = 0;
    UINT1               u1Val = 0;
    UINT4               u4Val = 0;
    UINT1               Temp = 0;
    UINT4               u4Temp = 0;
    pRxPkt = au1RxPkt;

    /* Failure Case 1: Invalid Target Fec Stack Tlv */
    u2TlvType = 1;
    u2SubTlvType = LSPP_FEC_VPN_IPV4;
    u1FecStackDepth = 1;
    u1MsgType = 1;
    u2TlvLength = 22;

    if (LsppRxDecodeValidatePdu (&pRxPkt, u2TlvType, u2SubTlvType,
                                 &LsppPduInfo, u4ContextId, u1FecStackDepth,
                                 u1MsgType, &u2TlvLength, &u1Result) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;

    }

    if (u1Result != LSPP_TLV_NOT_UNDERSTOOD)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case 2: Invalid Target Fec Stack Tlv */
    u2TlvType = 1;
    u2SubTlvType = 31313;
    u1FecStackDepth = 1;
    u1MsgType = 1;
    u2TlvLength = 22;

    if (LsppRxDecodeValidatePdu (&pRxPkt, u2TlvType, u2SubTlvType,
                                 &LsppPduInfo, u4ContextId, u1FecStackDepth,
                                 u1MsgType, &u2TlvLength, &u1Result) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;

    }

    if (u1Result != LSPP_MALFORMED_ECHO_PACKET)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case 3 :Invalid DSMAP TLV Address Type */
    u2TlvType = 2;
    u2SubTlvType = 0;
    u1FecStackDepth = 1;
    u1MsgType = 1;
    u2TlvLength = 22;
    pRxPkt = au1RxPkt;

    /* Framing Dsmap Tlv */
    u4Val = 1500;
    MEMCPY (pRxPkt, &u4Val, 2);
    pRxPkt = pRxPkt + 2;
    u1Val = 6;
    MEMCPY (pRxPkt, &u1Val, 1);
    pRxPkt = pRxPkt + 1;
    u1Val = 0;

    if (LsppRxDecodeValidatePdu (&pRxPkt, u2TlvType, u2SubTlvType,
                                 &LsppPduInfo, u4ContextId, u1FecStackDepth,
                                 u1MsgType, &u2TlvLength, &u1Result) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;

    }

    /* Failure Case 4: Invalid Dsmap TLV */
    u2TlvType = 2;
    u2SubTlvType = 0;
    u1FecStackDepth = 1;
    u1MsgType = 1;
    u2TlvLength = 22;
    pRxPkt = au1RxPkt;

    /* Framing Dsmap Tlv */
    MEMCPY (pRxPkt, u4Val, 2);    /* MTU */
    pRxPkt = pRxPkt + 2;
    Temp = 1;
    MEMCPY (pRxPkt, &Temp, 1);    /* ADDR TTPE */
    pRxPkt = pRxPkt + 1;
    Temp = 5;
    MEMCPY (pRxPkt, &Temp, 1);    /* Gloabl FLAG */
    pRxPkt = pRxPkt + 1;
    u4Temp = 1234;
    MEMCPY (pRxPkt, &u4Temp, 4);    /* Ip addr */
    pRxPkt = pRxPkt + 4;
    MEMCPY (pRxPkt, &u4Temp, 4);    /* If Addr */
    pRxPkt = pRxPkt + 4;
    Temp = 1;
    MEMCPY (pRxPkt, &Temp, 1);    /* Multipath Type */
    pRxPkt = pRxPkt + 1;
    MEMCPY (pRxPkt, &Temp, 1);    /* Depth Limit */
    pRxPkt = pRxPkt + 1;

    MEMCPY (pRxPkt, &u1Val, 2);    /* Multipath Length */
    pRxPkt = pRxPkt + 2;

    u4Label = 20010;
    u1Exp = 1;
    u1SI = 1;
    u1Ttl = 1;

    u4MplsHdr =
        (((u4Label << 12) & 0xfffff000) | ((u1Exp << 9) & 0x00000e00) |
         ((u1SI << 8) & 0x00000100) | (u1Ttl & 0x000000ff));

    MEMCPY (pRxPkt, &u4MplsHdr, 4);
    pRxPkt = pRxPkt + 4;

    pRxPkt = au1RxPkt;

    if (LsppRxDecodeValidatePdu (&pRxPkt, u2TlvType, u2SubTlvType,
                                 &LsppPduInfo, u4ContextId, u1FecStackDepth,
                                 u1MsgType, &u2TlvLength, &u1Result) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;

    }

    /* Failure Case 5:Invalid If Lbk Stk TLV Address Type */
    Temp = 0;
    MEMSET (au1RxPkt, &Temp, 100);
    pRxPkt = au1RxPkt;

    u2TlvType = 7;
    u2SubTlvType = 0;
    u1FecStackDepth = 1;
    u1MsgType = 1;
    u2TlvLength = 22;

    /* Framing If lbl stk Tlv */
    Temp = 6;
    MEMCPY (pRxPkt, &Temp, 1);
    pRxPkt = pRxPkt + 1;

    pRxPkt = au1RxPkt;

    if (LsppRxDecodeValidatePdu (&pRxPkt, u2TlvType, u2SubTlvType,
                                 &LsppPduInfo, u4ContextId, u1FecStackDepth,
                                 u1MsgType, &u2TlvLength, &u1Result) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;

    }

    /* Failure Case 6: Invalid If lbl Stk TLV */
    Temp = 0;
    MEMSET (au1RxPkt, &Temp, 100);
    pRxPkt = au1RxPkt;

    u2TlvType = 7;
    u2SubTlvType = 0;
    u1FecStackDepth = 1;
    u1MsgType = 1;
    u2TlvLength = 22;

    /* Framing If lbl stk Tlv */
    Temp = 1;
    MEMCPY (pRxPkt, &Temp, 1);    /* ADDR TTPE */
    pRxPkt = pRxPkt + 1;

    pRxPkt = pRxPkt + 3;

    u4Temp = 1234;
    MEMCPY (pRxPkt, &u4Temp, 4);    /* Ip addr */
    pRxPkt = pRxPkt + 4;
    u4Temp = 1223;
    MEMCPY (pRxPkt, &u4Temp, 4);    /* If Addr */
    pRxPkt = pRxPkt + 4;

    u4Label = 20010;
    u1Exp = 1;
    u1SI = 0;
    u1Ttl = 1;

    u4MplsHdr =
        (((u4Label << 12) & 0xfffff000) | ((u1Exp << 9) & 0x00000e00) |
         ((u1SI << 8) & 0x00000100) | (u1Ttl & 0x000000ff));

    while (u1Val < 9)
    {

        MEMCPY (pRxPkt, &u4MplsHdr, 4);
        pRxPkt = pRxPkt + 4;

        u1Val++;
    }
    u1Val = 0;
    u4Label = 20010;
    u1Exp = 1;
    u1SI = 1;
    u1Ttl = 1;

    u4MplsHdr =
        (((u4Label << 12) & 0xfffff000) | ((u1Exp << 9) & 0x00000e00) |
         ((u1SI << 8) & 0x00000100) | (u1Ttl & 0x000000ff));

    pRxPkt = au1RxPkt;

    if (LsppRxDecodeValidatePdu (&pRxPkt, u2TlvType, u2SubTlvType,
                                 &LsppPduInfo, u4ContextId, u1FecStackDepth,
                                 u1MsgType, &u2TlvLength, &u1Result) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;

    }

    /* Failure case 7: Invalid Pad Tlv */
    MEMSET (au1RxPkt, 0, 100);
    pRxPkt = au1RxPkt;

    u2TlvType = 3;
    u2SubTlvType = 0;
    u1MsgType = 1;
    u2TlvLength = 50;

    /* Framing Pad Tlv */
    MEMCPY (pRxPkt, &u1Val, 1);    /* ADDR TTPE */
    pRxPkt = pRxPkt + 1;

    pRxPkt = au1RxPkt;
    if (LsppRxDecodeValidatePdu (&pRxPkt, u2TlvType, u2SubTlvType,
                                 &LsppPduInfo, u4ContextId, u1FecStackDepth,
                                 u1MsgType, &u2TlvLength, &u1Result) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;

    }

    /* Failure case 8: Invalid reply TOS */
    MEMSET (au1RxPkt, 0, 100);
    pRxPkt = au1RxPkt;

    u2TlvType = 10;
    u2SubTlvType = 0;
    u1MsgType = LSPP_ECHO_REPLY;
    u2TlvLength = 4;

    pRxPkt = au1RxPkt;
    if (LsppRxDecodeValidatePdu (&pRxPkt, u2TlvType, u2SubTlvType,
                                 &LsppPduInfo, u4ContextId, u1FecStackDepth,
                                 u1MsgType, &u2TlvLength, &u1Result) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;

    }

    /* Failure case 9: Invalid LSPP_TLV_TYPE_VENDOR_NUMBER */
    MEMSET (au1RxPkt, 0, 100);
    pRxPkt = au1RxPkt;

    u2TlvType = LSPP_TLV_TYPE_VENDOR_NUMBER;
    u2SubTlvType = 0;
    u1MsgType = LSPP_ECHO_REQUEST;
    u2TlvLength = 4;

    if (LsppRxDecodeValidatePdu (&pRxPkt, u2TlvType, u2SubTlvType,
                                 &LsppPduInfo, u4ContextId, u1FecStackDepth,
                                 u1MsgType, &u2TlvLength, &u1Result) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;

    }

    /* Failure case 10: Invalid LSPP_TLV_TYPE_VENDOR_NUMBER */
    MEMSET (au1RxPkt, 0, 100);
    pRxPkt = au1RxPkt;

    u2TlvType = 1223;
    u2SubTlvType = 0;
    u1MsgType = LSPP_ECHO_REQUEST;
    u2TlvLength = 4;

    if (LsppRxDecodeValidatePdu (&pRxPkt, u2TlvType, u2SubTlvType,
                                 &LsppPduInfo, u4ContextId, u1FecStackDepth,
                                 u1MsgType, &u2TlvLength, &u1Result) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*LsppRxSanityValidateLsppPdu */
INT4
LsppUtRx_19 (VOID)
{
    UINT1               au1RxPkt[100];
    tLsppPduInfo        LsppPduInfo;
    UINT4               u4PktSize = 200;
    UINT4               u4Offset = 86;
    UINT2               u2TlvType = 0;
    UINT2               u2TlvLen = 0;
    UINT4               u4ContextId = 0;
    UINT1               u1Result = 0;
    UINT1              *pRxPkt = NULL;
    UINT4               u4MplsHdr = 0;
    UINT4               u4Label = 0;
    UINT1               u1Exp = 0;
    UINT1               u1SI = 0;
    UINT1               u1Ttl = 0;
    UINT1               u1Val = 0;

    MEMSET (au1RxPkt, 0, sizeof (100));
    MEMSET (&LsppPduInfo, 0, sizeof (tLsppPduInfo));

    /* Failure Case 1: Invalid Ping Header in Reply */
    pRxPkt = au1RxPkt;
    LsppUtFrameHeader (&pRxPkt, LSPP_ECHO_REPLY, LSPP_INVALID_PKT);

    pRxPkt = au1RxPkt;

    if (LsppRxSanityValidateLsppPdu (&pRxPkt, &LsppPduInfo, u4ContextId,
                                     u4PktSize, u4Offset, &u1Result) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case 2: Invalid Ping Header in Request */
    pRxPkt = au1RxPkt;
    MEMSET (au1RxPkt, 0, sizeof (100));
    LsppUtFrameHeader (&pRxPkt, LSPP_ECHO_REQUEST, LSPP_INVALID_PKT);

    pRxPkt = au1RxPkt;

    if (LsppRxSanityValidateLsppPdu (&pRxPkt, &LsppPduInfo, u4ContextId,
                                     u4PktSize, u4Offset, &u1Result) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    if (u1Result != LSPP_MALFORMED_ECHO_PACKET)
    {
        return OSIX_FAILURE;
    }

    /* Failure case 3- Invalid Target Fec stack Tlv */

    MEMSET (au1RxPkt, 0, sizeof (100));
    pRxPkt = au1RxPkt;
    u2TlvType = 1;
    u2TlvLen = 28;

    LsppUtFrameHeader (&pRxPkt, LSPP_ECHO_REPLY, LSPP_VALID_PKT);
    MEMCPY (pRxPkt, &u2TlvType, 2);
    pRxPkt = pRxPkt + 2;

    MEMCPY (pRxPkt, &u2TlvLen, 2);
    pRxPkt = pRxPkt + 2;

    u2TlvType = 1234;
    u2TlvLen = 22;

    MEMCPY (pRxPkt, &u2TlvType, 2);
    pRxPkt = pRxPkt + 2;

    MEMCPY (pRxPkt, &u2TlvLen, 2);
    pRxPkt = pRxPkt + 2;

    pRxPkt = au1RxPkt;

    if (LsppRxSanityValidateLsppPdu (&pRxPkt, &LsppPduInfo, u4ContextId,
                                     u4PktSize, u4Offset, &u1Result) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure 4 -Invalid If Lbl Stack TLV in Request */
    MEMSET (au1RxPkt, 0, sizeof (100));
    pRxPkt = au1RxPkt;
    u2TlvType = 7;
    u2TlvLen = 28;

    LsppUtFrameHeader (&pRxPkt, LSPP_ECHO_REQUEST, LSPP_VALID_PKT);

    MEMCPY (pRxPkt, &u2TlvType, 2);
    pRxPkt = pRxPkt + 2;

    MEMCPY (pRxPkt, &u2TlvLen, 2);
    pRxPkt = pRxPkt + 2;
    pRxPkt = au1RxPkt;

    if (LsppRxSanityValidateLsppPdu (&pRxPkt, &LsppPduInfo, u4ContextId,
                                     u4PktSize, u4Offset, &u1Result) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure- 5 DSMAP TLV TWICE IN REQUEST */
    MEMSET (au1RxPkt, 0, sizeof (100));
    pRxPkt = au1RxPkt;
    u2TlvType = 2;
    u2TlvLen = 28;

    LsppUtFrameHeader (&pRxPkt, LSPP_ECHO_REQUEST, LSPP_VALID_PKT);

    /* Framing Dsmap Tlv */
    MEMCPY (pRxPkt, &u2TlvType, 2);    /* Tlv Type */
    pRxPkt = pRxPkt + 2;

    MEMCPY (pRxPkt, &u2TlvLen, 2);    /* Tlv Len */
    pRxPkt = pRxPkt + 2;

    MEMCPY (pRxPkt, 1500, 2);    /* MTU */
    pRxPkt = pRxPkt + 2;

    MEMCPY (pRxPkt, 1, 1);        /* ADDR TTPE */
    pRxPkt = pRxPkt + 1;

    MEMCPY (pRxPkt, 5, 1);        /* Gloabl FLAG */
    pRxPkt = pRxPkt + 1;

    MEMCPY (pRxPkt, 1234, 4);    /* Ip addr */
    pRxPkt = pRxPkt + 4;

    MEMCPY (pRxPkt, 1223, 4);    /* If Addr */
    pRxPkt = pRxPkt + 4;

    MEMCPY (pRxPkt, 1, 1);        /* Multipath Type */
    pRxPkt = pRxPkt + 1;

    MEMCPY (pRxPkt, 1, 1);        /* Depth Limit */
    pRxPkt = pRxPkt + 1;

    MEMCPY (pRxPkt, &u1Val, 2);    /* Multipath Length */
    pRxPkt = pRxPkt + 2;

    u4Label = 20010;
    u1Exp = 1;
    u1SI = 1;
    u1Ttl = 1;

    u4MplsHdr =
        (((u4Label << 12) & 0xfffff000) | ((u1Exp << 9) & 0x00000e00) |
         ((u1SI << 8) & 0x00000100) | (u1Ttl & 0x000000ff));

    MEMCPY (pRxPkt, &u4MplsHdr, 4);
    pRxPkt = pRxPkt + 4;

    /* Framing Second Dsmap Tlv */
    MEMCPY (pRxPkt, &u2TlvType, 2);    /* Tlv Type */
    pRxPkt = pRxPkt + 2;

    MEMCPY (pRxPkt, &u2TlvLen, 2);    /* Tlv Len */
    pRxPkt = pRxPkt + 2;

    pRxPkt = au1RxPkt;

    if (LsppRxSanityValidateLsppPdu (&pRxPkt, &LsppPduInfo, u4ContextId,
                                     u4PktSize, u4Offset, &u1Result) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure- 6 TLV other than FEC stack TLV decoding failure */
    MEMSET (au1RxPkt, 0, sizeof (100));
    pRxPkt = au1RxPkt;
    u2TlvType = 2;
    u2TlvLen = 28;

    LsppUtFrameHeader (&pRxPkt, LSPP_ECHO_REQUEST, LSPP_VALID_PKT);

    /* Framing Dsmap Tlv */
    MEMCPY (pRxPkt, &u2TlvType, 2);    /* Tlv Type */
    pRxPkt = pRxPkt + 2;

    MEMCPY (pRxPkt, &u2TlvLen, 2);    /* Tlv Len */
    pRxPkt = pRxPkt + 2;

    MEMCPY (pRxPkt, 1500, 2);    /* MTU */
    pRxPkt = pRxPkt + 2;

    MEMCPY (pRxPkt, 8, 1);        /* ADDR TTPE */
    pRxPkt = pRxPkt + 1;

    MEMCPY (pRxPkt, 5, 1);        /* Gloabl FLAG */
    pRxPkt = pRxPkt + 1;

    MEMCPY (pRxPkt, 1234, 4);    /* Ip addr */
    pRxPkt = pRxPkt + 4;

    MEMCPY (pRxPkt, 1223, 4);    /* If Addr */
    pRxPkt = pRxPkt + 4;

    MEMCPY (pRxPkt, 1, 1);        /* Multipath Type */
    pRxPkt = pRxPkt + 1;

    MEMCPY (pRxPkt, 1, 1);        /* Depth Limit */
    pRxPkt = pRxPkt + 1;

    MEMCPY (pRxPkt, &u1Val, 2);    /* Multipath Length */
    pRxPkt = pRxPkt + 2;

    u4Label = 20010;
    u1Exp = 1;
    u1SI = 1;
    u1Ttl = 1;

    u4MplsHdr =
        (((u4Label << 12) & 0xfffff000) | ((u1Exp << 9) & 0x00000e00) |
         ((u1SI << 8) & 0x00000100) | (u1Ttl & 0x000000ff));

    MEMCPY (pRxPkt, &u4MplsHdr, 4);
    pRxPkt = pRxPkt + 4;

    pRxPkt = au1RxPkt;

    if (LsppRxSanityValidateLsppPdu (&pRxPkt, &LsppPduInfo, u4ContextId,
                                     u4PktSize, u4Offset, &u1Result) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}
#endif
/* LsppRxProcessMplsPdu */
INT4
LsppUtRx_20 (VOID)
{
    tLsppEchoMsg        LsppEchoMsg;
    UINT1               au1RxPkt[100];
    UINT1              *pRxPkt = NULL;
    UINT4               u4Offset = 0;

    UINT4               u4MplsHdr = 0;
    UINT4               u4Label = 0;
    UINT1               u1Exp = 0;
    UINT1               u1SI = 0;
    UINT1               u1Ttl = 0;
    UINT1               u1Val = 0;

    UINT4               u4AchType = 1;
    UINT1               u1Version = 0;
    UINT1               u1Rsvd = 0;
    UINT2               u2ChannelType = 1;
    UINT4               u4SrcVal = 0;

    MEMSET (&LsppEchoMsg, 0, sizeof (tLsppEchoMsg));

    /* Failure Case 1 - Invalid Label Count */
    u4Label = 20010;
    u1Exp = 1;
    u1SI = 0;
    u1Ttl = 1;

    pRxPkt = au1RxPkt;
    u4MplsHdr =
        (((u4Label << 12) & 0xfffff000) | ((u1Exp << 9) & 0x00000e00) |
         ((u1SI << 8) & 0x00000100) | (u1Ttl & 0x000000ff));

    while (u1Val < 9)
    {
        MEMCPY (pRxPkt, &u4MplsHdr, 4);
        pRxPkt = pRxPkt + 4;
        u1Val++;
    }

    u4Label = 20010;
    u1Exp = 1;
    u1SI = 1;
    u1Ttl = 1;

    u4MplsHdr =
        (((u4Label << 12) & 0xfffff000) | ((u1Exp << 9) & 0x00000e00) |
         ((u1SI << 8) & 0x00000100) | (u1Ttl & 0x000000ff));

    MEMCPY (pRxPkt, &u4MplsHdr, 4);
    pRxPkt = pRxPkt + 4;

    pRxPkt = au1RxPkt;

    if (LsppRxProcessMplsPdu (&pRxPkt, &LsppEchoMsg, &u4Offset) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case 2- Fetch Path Id Failed */
    u4Label = 0;
    u1Exp = 1;
    u1SI = 0;
    u1Ttl = 1;

    pRxPkt = au1RxPkt;
    u4MplsHdr =
        (((u4Label << 12) & 0xfffff000) | ((u1Exp << 9) & 0x00000e00) |
         ((u1SI << 8) & 0x00000100) | (u1Ttl & 0x000000ff));

    MEMCPY (pRxPkt, &u4MplsHdr, 4);
    pRxPkt = pRxPkt + 4;

    pRxPkt = au1RxPkt;
    if (LsppRxProcessMplsPdu (&pRxPkt, &LsppEchoMsg, &u4Offset) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case 3 - Fetch Path Info Failed */
    u4Label = 2010;                /* Have to give Invalid Path Id for this label 
                                   so that fetch Path Info from Path Id failed */
    u1Exp = 1;
    u1SI = 0;
    u1Ttl = 1;

    pRxPkt = au1RxPkt;
    u4MplsHdr =
        (((u4Label << 12) & 0xfffff000) | ((u1Exp << 9) & 0x00000e00) |
         ((u1SI << 8) & 0x00000100) | (u1Ttl & 0x000000ff));

    MEMCPY (pRxPkt, &u4MplsHdr, 4);
    pRxPkt = pRxPkt + 4;

    pRxPkt = au1RxPkt;
    if (LsppRxProcessMplsPdu (&pRxPkt, &LsppEchoMsg, &u4Offset) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case 4- Invalid Channel type in ACH Header */
    u4Label = 200010;
    u1Exp = 1;
    u1SI = 0;
    u1Ttl = 1;

    pRxPkt = au1RxPkt;
    u4MplsHdr =
        (((u4Label << 12) & 0xfffff000) | ((u1Exp << 9) & 0x00000e00) |
         ((u1SI << 8) & 0x00000100) | (u1Ttl & 0x000000ff));

    MEMCPY (pRxPkt, &u4MplsHdr, 4);
    pRxPkt = pRxPkt + 4;

    /* Filling Ach Header */
    u4AchType = 1;
    u1Version = 0;
    u1Rsvd = 0;
    u2ChannelType = 1;

    u4SrcVal = ((u4AchType << 28) & 0xf0000000) |
        ((u1Version << 24) & 0x0f000000) |
        ((u1Rsvd << 16) & 0x00ff0000) | (u2ChannelType & 0x0000ffff);

    MEMCPY (pRxPkt, &u4SrcVal, 4);
    pRxPkt = pRxPkt + 4;

    pRxPkt = au1RxPkt;
    if (LsppRxProcessMplsPdu (&pRxPkt, &LsppEchoMsg, &u4Offset) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case 5- Invalid Ip UDP Header */
    MEMSET (au1RxPkt, 0, 100);

    u4Label = 200010;
    u1Exp = 1;
    u1SI = 0;
    u1Ttl = 1;

    pRxPkt = au1RxPkt;
    u4MplsHdr =
        (((u4Label << 12) & 0xfffff000) | ((u1Exp << 9) & 0x00000e00) |
         ((u1SI << 8) & 0x00000100) | (u1Ttl & 0x000000ff));

    MEMCPY (pRxPkt, &u4MplsHdr, 4);
    pRxPkt = pRxPkt + 4;

    /* Filling Ach Header */
    u4AchType = 1;
    u1Version = 0;
    u1Rsvd = 0;
    u2ChannelType = 0x21;

    u4SrcVal = ((u4AchType << 28) & 0xf0000000) |
        ((u1Version << 24) & 0x0f000000) |
        ((u1Rsvd << 16) & 0x00ff0000) | (u2ChannelType & 0x0000ffff);

    MEMCPY (pRxPkt, &u4SrcVal, 4);
    pRxPkt = pRxPkt + 4;

    LsppUtFrameIpUdpHeader (&pRxPkt, LSPP_INVALID_PKT);

    pRxPkt = au1RxPkt;
    if (LsppRxProcessMplsPdu (&pRxPkt, &LsppEchoMsg, &u4Offset) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case 6 - Invalid Get Ach Tlv */
    MEMSET (au1RxPkt, 0, 100);

    u4Label = 200010;
    u1Exp = 1;
    u1SI = 0;
    u1Ttl = 1;

    pRxPkt = au1RxPkt;
    u4MplsHdr =
        (((u4Label << 12) & 0xfffff000) | ((u1Exp << 9) & 0x00000e00) |
         ((u1SI << 8) & 0x00000100) | (u1Ttl & 0x000000ff));

    MEMCPY (pRxPkt, &u4MplsHdr, 4);
    pRxPkt = pRxPkt + 4;

    /* Filling Ach Header */
    u4AchType = 1;
    u1Version = 0;
    u1Rsvd = 0;
    u2ChannelType = 0x21;

    u4SrcVal = ((u4AchType << 28) & 0xf0000000) |
        ((u1Version << 24) & 0x0f000000) |
        ((u1Rsvd << 16) & 0x00ff0000) | (u2ChannelType & 0x0000ffff);

    MEMCPY (pRxPkt, &u4SrcVal, 4);
    pRxPkt = pRxPkt + 4;

    /* Updating ACH TVL Header */
    u4SrcVal = 16;
    MEMCPY (pRxPkt, &u4SrcVal, 4);
    pRxPkt = pRxPkt + 4;

    /* Rsvd field */
    pRxPkt = pRxPkt + 2;

    /* Ach Tlv type */
    u4SrcVal = 38;
    MEMCPY (pRxPkt, &u4SrcVal, 2);

    pRxPkt = au1RxPkt;
    if (LsppRxProcessMplsPdu (&pRxPkt, &LsppEchoMsg, &u4Offset) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case 7 - Ach Tlv Validation Failure */
    MEMSET (au1RxPkt, 0, 100);

    u4Label = 200010;
    u1Exp = 1;
    u1SI = 0;
    u1Ttl = 1;

    pRxPkt = au1RxPkt;
    u4MplsHdr =
        (((u4Label << 12) & 0xfffff000) | ((u1Exp << 9) & 0x00000e00) |
         ((u1SI << 8) & 0x00000100) | (u1Ttl & 0x000000ff));

    MEMCPY (pRxPkt, &u4MplsHdr, 4);
    pRxPkt = pRxPkt + 4;

    /* Filling Ach Header */
    u4AchType = 1;
    u1Version = 0;
    u1Rsvd = 0;
    u2ChannelType = 0x21;

    u4SrcVal = ((u4AchType << 28) & 0xf0000000) |
        ((u1Version << 24) & 0x0f000000) |
        ((u1Rsvd << 16) & 0x00ff0000) | (u2ChannelType & 0x0000ffff);

    MEMCPY (pRxPkt, &u4SrcVal, 4);
    pRxPkt = pRxPkt + 4;

    /* Updating ACH TVL Header */
    u4SrcVal = 16;
    MEMCPY (pRxPkt, &u4SrcVal, 4);
    pRxPkt = pRxPkt + 4;

    /* Rsvd field */
    pRxPkt = pRxPkt + 2;

    /* Ach Tlv type */
    u4SrcVal = 8;
    MEMCPY (pRxPkt, &u4SrcVal, 2);
    pRxPkt = pRxPkt + 2;

    u4SrcVal = 12;
    MEMCPY (pRxPkt, &u4SrcVal, 2);
    pRxPkt = pRxPkt + 2;

    /* Ach Tlv's */
    u4SrcVal = 4321;
    MEMCPY (pRxPkt, &u4SrcVal, 4);
    pRxPkt = pRxPkt + 4;

    u4SrcVal = 101;
    MEMCPY (pRxPkt, &u4SrcVal, 24);
    pRxPkt = pRxPkt + 4;

    u4SrcVal = 12;
    MEMCPY (pRxPkt, &u4SrcVal, 2);
    pRxPkt = pRxPkt + 2;

    u4SrcVal = 1;
    MEMCPY (pRxPkt, &u4SrcVal, 2);
    pRxPkt = pRxPkt + 2;

    pRxPkt = au1RxPkt;

    if (LsppRxProcessMplsPdu (&pRxPkt, &LsppEchoMsg, &u4Offset) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case 8 - Cv cc Validation */
    MEMSET (au1RxPkt, 0, 100);

    u4Label = 200010;            /* Pw Label consider and give CC and provide
                                   Ldp IP version as 1 */
    u1Exp = 1;
    u1SI = 0;
    u1Ttl = 1;

    pRxPkt = au1RxPkt;
    u4MplsHdr =
        (((u4Label << 12) & 0xfffff000) | ((u1Exp << 9) & 0x00000e00) |
         ((u1SI << 8) & 0x00000100) | (u1Ttl & 0x000000ff));

    MEMCPY (pRxPkt, &u4MplsHdr, 4);
    pRxPkt = pRxPkt + 4;

    /* Filling Ach Header */
    u4AchType = 1;
    u1Version = 0;
    u1Rsvd = 0;
    u2ChannelType = 0x21;

    u4SrcVal = ((u4AchType << 28) & 0xf0000000) |
        ((u1Version << 24) & 0x0f000000) |
        ((u1Rsvd << 16) & 0x00ff0000) | (u2ChannelType & 0x0000ffff);

    MEMCPY (pRxPkt, &u4SrcVal, 4);
    pRxPkt = pRxPkt + 4;

    LsppUtFrameIpUdpHeader (&pRxPkt, LSPP_INVALID_PKT);

    pRxPkt = au1RxPkt;
    if (LsppRxProcessMplsPdu (&pRxPkt, &LsppEchoMsg, &u4Offset) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case 9 - Cv cc Validation */
    MEMSET (au1RxPkt, 0, 100);

    u4Label = 200011;            /* Pw Label consider and give
                                   source as 1234 in port handle */
    u1Exp = 1;
    u1SI = 0;
    u1Ttl = 1;

    pRxPkt = au1RxPkt;
    u4MplsHdr =
        (((u4Label << 12) & 0xfffff000) | ((u1Exp << 9) & 0x00000e00) |
         ((u1SI << 8) & 0x00000100) | (u1Ttl & 0x000000ff));

    MEMCPY (pRxPkt, &u4MplsHdr, 4);
    pRxPkt = pRxPkt + 4;

    /* Filling Ach Header */
    u4AchType = 1;
    u1Version = 0;
    u1Rsvd = 0;
    u2ChannelType = 0x21;

    u4SrcVal = ((u4AchType << 28) & 0xf0000000) |
        ((u1Version << 24) & 0x0f000000) |
        ((u1Rsvd << 16) & 0x00ff0000) | (u2ChannelType & 0x0000ffff);

    MEMCPY (pRxPkt, &u4SrcVal, 4);
    pRxPkt = pRxPkt + 4;

    LsppUtFrameIpUdpHeader (&pRxPkt, LSPP_VALID_PKT);

    pRxPkt = au1RxPkt;
    if (LsppRxProcessMplsPdu (&pRxPkt, &LsppEchoMsg, &u4Offset) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Failure Case 9 - Cv cc Validation */
    MEMSET (au1RxPkt, 0, 100);

    u4Label = 200011;            /* Pw Label consider and give
                                   CC as TTL and CV for Pw as ICMP PING */
    u1Exp = 1;
    u1SI = 0;
    u1Ttl = 1;

    pRxPkt = au1RxPkt;
    u4MplsHdr =
        (((u4Label << 12) & 0xfffff000) | ((u1Exp << 9) & 0x00000e00) |
         ((u1SI << 8) & 0x00000100) | (u1Ttl & 0x000000ff));

    MEMCPY (pRxPkt, &u4MplsHdr, 4);
    pRxPkt = pRxPkt + 4;

    /* Filling Ach Header */
    u4AchType = 1;
    u1Version = 0;
    u1Rsvd = 0;
    u2ChannelType = 0x21;

    u4SrcVal = ((u4AchType << 28) & 0xf0000000) |
        ((u1Version << 24) & 0x0f000000) |
        ((u1Rsvd << 16) & 0x00ff0000) | (u2ChannelType & 0x0000ffff);

    MEMCPY (pRxPkt, &u4SrcVal, 4);
    pRxPkt = pRxPkt + 4;

    LsppUtFrameIpUdpHeader (&pRxPkt, LSPP_VALID_PKT);

    pRxPkt = au1RxPkt;
    if (LsppRxProcessMplsPdu (&pRxPkt, &LsppEchoMsg, &u4Offset) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/* LsppRxSendBfdBtInfo */
INT4
LsppUtRx_21 (VOID)
{
    tLsppFsLsppPingTraceTableEntry LsppPingTraceEntry;
    tLsppBfdDiscTlv     LsppBfdDiscTlv;
    UINT1               u1ReturnCode = 0;

    MEMSET (&LsppPingTraceEntry, 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&LsppBfdDiscTlv, 0, sizeof (tLsppBfdDiscTlv));

    LsppPingTraceEntry.MibObject.u4FsLsppContextId = 0;
    LsppBfdDiscTlv.u4Discriminator = 255;
    LsppPingTraceEntry.u4BfdSessionIndex = 100;

    if (LsppRxSendBfdBtInfo (&LsppPingTraceEntry, &LsppBfdDiscTlv,
                             u1ReturnCode) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;

}

/* LsppRxGetPathIdFromLabel  */
INT4
LsppUtRx_22 (VOID)
{
    tLsppPathId         LsppPathId;
    UINT4               u4ContextId = 0;
    UINT4               u4IfNum = 0;
    UINT4               u4Label = 0;

    MEMSET (&LsppPathId, 0, sizeof (tLsppPathId));

    if (LsppRxGetPathIdFromLabel (u4ContextId, u4IfNum, u4Label,
                                  &LsppPathId) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
LsppUtRx_23 (VOID)
{

    tLsppLdpFecTlv      LsppLdpFecTlv;
    UINT1               au1Pkt[10];
    UINT1              *pu1Pkt = NULL;
    UINT4               u4ContextId = 0;
    UINT4               u4Addr = 5767;
    UINT4               u1PrexixLen = 5;

    MEMSET (au1Pkt, 0, 10);
    MEMSET (&LsppLdpFecTlv, 0, sizeof (tLsppLdpFecTlv));
    pu1Pkt = malloc (10);
    pu1Pkt = au1Pkt;

    MEMCPY (pu1Pkt, &u4Addr, 4);
    pu1Pkt = pu1Pkt + 4;
    MEMCPY (pu1Pkt, &u1PrexixLen, 1);
    pu1Pkt = pu1Pkt + 1;

    pu1Pkt = au1Pkt;

    LsppRxDecodeIpv4LdpFec (&pu1Pkt, &LsppLdpFecTlv, u4ContextId);

    return OSIX_SUCCESS;
}

INT4
LsppUtRx_24 (VOID)
{

    tLsppLdpFecTlv      LsppLdpFecTlv;
    UINT1               au1Pkt[20];
    UINT1              *pu1Pkt = NULL;
    UINT4               u4ContextId = 0;
    UINT1               u1Addr = 0;
    UINT4               u1PrexixLen = 5;

    MEMSET (au1Pkt, 0, 20);
    MEMSET (&LsppLdpFecTlv, 0, sizeof (tLsppLdpFecTlv));

    pu1Pkt = au1Pkt;

    MEMCPY (pu1Pkt, &u1Addr, 16);
    pu1Pkt = pu1Pkt + 16;
    MEMCPY (pu1Pkt, &u1PrexixLen, 1);
    pu1Pkt = pu1Pkt + 1;

    pu1Pkt = au1Pkt;

    LsppRxDecodeIpv6LdpFec (&pu1Pkt, &LsppLdpFecTlv, u4ContextId);

    return OSIX_SUCCESS;
}

INT4
LsppUtRx_25 (VOID)
{
    tLsppNilFecTlv      LsppNilFecTlv;
    UINT1               au1Pkt[4];
    UINT1              *pu1Pkt = NULL;
    UINT4               u4ContextId = 0;
    UINT4               u4label = 101;

    MEMSET (&(LsppNilFecTlv), 0, sizeof (tLsppNilFecTlv));
    MEMSET (au1Pkt, 0, 4);

    pu1Pkt = au1Pkt;

    MEMCPY (pu1Pkt, &u4label, 4);
    pu1Pkt = au1Pkt;

    LsppRxDecodeNilFec (&pu1Pkt, &LsppNilFecTlv, u4ContextId);

    return OSIX_SUCCESS;
}

INT4
LsppUtRx_26 (VOID)
{
    tLsppReplyTosTlv    LsppReplyTosTlv;
    UINT1               au1Pkt[4];
    UINT1              *pu1Pkt = NULL;
    UINT4               u4ContextId = 0;
    UINT1               u1TosValue = 0;

    MEMSET (&(LsppReplyTosTlv), 0, sizeof (tLsppReplyTosTlv));
    MEMSET (au1Pkt, 0, 4);

    pu1Pkt = au1Pkt;

    MEMCPY (pu1Pkt, &u1TosValue, 4);

    pu1Pkt = au1Pkt;

    LsppRxDecodeReplyTosTlv (&pu1Pkt, &LsppReplyTosTlv, u4ContextId);

    return OSIX_SUCCESS;
}

INT4
LsppUtRx_27 (VOID)
{
    tLsppErrorTlv       LsppErrorTlv;
    UINT2               u2TlvLength = 8;
    UINT4               u4ContextId = 0;
    UINT1               au1Pkt[8];
    UINT1              *pu1Pkt = NULL;

    MEMSET (au1Pkt, 0, 8);
    MEMSET (&(LsppErrorTlv), 0, sizeof (tLsppErrorTlv));

    pu1Pkt = au1Pkt;

    LsppRxDecodeErrorTlv (&pu1Pkt, &LsppErrorTlv, u2TlvLength, u4ContextId);

    pu1Pkt = au1Pkt;

    return OSIX_SUCCESS;
}

INT4
LsppUtRx_28 (VOID)
{
    tLsppIfLblStkTlv    LsppIfLblStkTlv;
    UINT4               u4ContextId = 0;
    UINT1               au1Pkt[100];
    UINT1              *pu1Pkt = NULL;
    UINT1               AddrType = 1;
    UINT1               Ipv4Addr = 0;
    UINT4               u4MplsHdr = 0;
    UINT4               u4Label = 0;
    UINT1               u1Exp = 0;
    UINT1               u1SI = 0;
    UINT1               u1Ttl = 0;

    MEMSET (&(LsppIfLblStkTlv), 0, sizeof (tLsppIfLblStkTlv));
    MEMSET (au1Pkt, 0, 100);

    pu1Pkt = au1Pkt;

    MEMCPY (pu1Pkt, &AddrType, 1);
    pu1Pkt = pu1Pkt + 4;
    MEMCPY (pu1Pkt, &Ipv4Addr, 4);
    pu1Pkt = pu1Pkt + 4;

    u4Label = 20010;
    u1Exp = 255;
    u1SI = 1;
    u1Ttl = 255;

    u4MplsHdr =
        (((u4Label << 12) & 0xfffff000) | ((u1Exp << 9) & 0x00000e00) |
         ((u1SI << 8) & 0x00000100) | (u1Ttl & 0x000000ff));

    MEMCPY (pu1Pkt, &u4MplsHdr, 4);
    pu1Pkt = pu1Pkt + 4;

    pu1Pkt = au1Pkt;

    if (LsppRxDecodeIfLblStkTlv (&pu1Pkt, &LsppIfLblStkTlv, u4ContextId)
        != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (au1Pkt, 0, 100);

    pu1Pkt = au1Pkt;

    AddrType = 2;
    MEMCPY (pu1Pkt, &AddrType, 1);
    pu1Pkt = pu1Pkt + 4;
    MEMCPY (pu1Pkt, &Ipv4Addr, 4);
    pu1Pkt = pu1Pkt + 4;

    u4Label = 20010;
    u1Exp = 1;
    u1SI = 1;
    u1Ttl = 1;

    u4MplsHdr =
        (((u4Label << 12) & 0xfffff000) | ((u1Exp << 9) & 0x00000e00) |
         ((u1SI << 8) & 0x00000100) | (u1Ttl & 0x000000ff));

    MEMCPY (pu1Pkt, &u4MplsHdr, 4);
    pu1Pkt = pu1Pkt + 4;

    pu1Pkt = au1Pkt;
    if (LsppRxDecodeIfLblStkTlv (&pu1Pkt, &LsppIfLblStkTlv, u4ContextId)
        != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (au1Pkt, 0, 100);

    pu1Pkt = au1Pkt;

    AddrType = 3;
    MEMCPY (pu1Pkt, &AddrType, 4);
    pu1Pkt = pu1Pkt + 4;
    MEMCPY (pu1Pkt, &Ipv4Addr, 16);
    pu1Pkt = pu1Pkt + 16;

    u4Label = 20010;
    u1Exp = 1;
    u1SI = 1;
    u1Ttl = 1;

    u4MplsHdr =
        (((u4Label << 12) & 0xfffff000) | ((u1Exp << 9) & 0x00000e00) |
         ((u1SI << 8) & 0x00000100) | (u1Ttl & 0x000000ff));

    MEMCPY (pu1Pkt, &u4MplsHdr, 4);
    pu1Pkt = pu1Pkt + 4;

    pu1Pkt = au1Pkt;
    if (LsppRxDecodeIfLblStkTlv (&pu1Pkt, &LsppIfLblStkTlv, u4ContextId)
        != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    MEMSET (au1Pkt, 0, 100);

    pu1Pkt = au1Pkt;

    AddrType = 4;
    MEMCPY (pu1Pkt, &AddrType, 4);
    pu1Pkt = pu1Pkt + 4;
    MEMCPY (pu1Pkt, &Ipv4Addr, 16);
    pu1Pkt = pu1Pkt + 16;

    u4Label = 20010;
    u1Exp = 1;
    u1SI = 1;
    u1Ttl = 1;

    u4MplsHdr =
        (((u4Label << 12) & 0xfffff000) | ((u1Exp << 9) & 0x00000e00) |
         ((u1SI << 8) & 0x00000100) | (u1Ttl & 0x000000ff));

    MEMCPY (pu1Pkt, &u4MplsHdr, 4);
    pu1Pkt = pu1Pkt + 4;

    pu1Pkt = au1Pkt;

    if (LsppRxDecodeIfLblStkTlv (&pu1Pkt, &LsppIfLblStkTlv, u4ContextId)
        != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
LsppUtRx_29 (VOID)
{
    tLsppPwFec128Tlv    LsppPwFec128Tlv;
    UINT1               au1Pkt[20];
    UINT1              *pu1Pkt = NULL;
    UINT4               u4ContextId = 0;
    UINT4               u4Addr = 0;
    UINT4               PwInfo = 0;
    UINT2               PwType = 0;

    MEMSET (&(LsppPwFec128Tlv), 0, sizeof (tLsppPwFec128Tlv));
    MEMSET (au1Pkt, 0, 20);

    pu1Pkt = au1Pkt;

    MEMCPY (pu1Pkt, &u4Addr, 4);
    pu1Pkt = pu1Pkt + 4;
    MEMCPY (pu1Pkt, &u4Addr, 4);
    pu1Pkt = pu1Pkt + 4;
    MEMCPY (pu1Pkt, &PwInfo, 4);
    pu1Pkt = pu1Pkt + 4;
    MEMCPY (pu1Pkt, &PwType, 2);
    pu1Pkt = pu1Pkt + 2;

    pu1Pkt = au1Pkt;

    LsppRxDecodeFec128Pw (&pu1Pkt, &LsppPwFec128Tlv, u4ContextId);

    return OSIX_SUCCESS;
}

INT4
LsppUtRx_30 (VOID)
{
    tLsppPwFec129Tlv    LsppPwFec129Tlv;
    UINT1               au1Pkt[30];
    UINT1              *pu1Pkt = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2TlvLength = 0;

    MEMSET (au1Pkt, 0, 30);
    MEMSET (&(LsppPwFec129Tlv), 0, sizeof (tLsppPwFec129Tlv));

    pu1Pkt = au1Pkt;

    LsppRxDecodeFec129Pw (&(pu1Pkt), &LsppPwFec129Tlv, &u2TlvLength,
                          u4ContextId);
    return OSIX_SUCCESS;
}

INT4
LsppUtRx_31 (VOID)
{
    tLsppStaticPwFec    LsppStaticPwFec;
    UINT1               au1Pkt[30];
    UINT1              *pu1Pkt = NULL;
    UINT4               u4ContextId = 0;
    UINT2               u2TlvLength = 0;

    MEMSET (&(LsppStaticPwFec), 0, sizeof (tLsppStaticPwFec));
    pu1Pkt = au1Pkt;
    LsppRxDecodeStaticPwFec (&pu1Pkt, &LsppStaticPwFec, &u2TlvLength,
                             u4ContextId);
    return OSIX_SUCCESS;
}

INT4
LsppUtRx_32 (VOID)
{
    tMplsIccMepTlv      MplsIccMepTlv;
    UINT1               au1Pkt[30];
    UINT1              *pu1Pkt = NULL;

    MEMSET (&(MplsIccMepTlv), 0, sizeof (tMplsIccMepTlv));
    pu1Pkt = au1Pkt;

    LsppRxGetAchIccMepTlv (&pu1Pkt, &MplsIccMepTlv);

    return OSIX_SUCCESS;
}

INT4
LsppUtRx_33 (VOID)
{
    tMplsMipTlv         MplsMipTlv;
    UINT1               au1Pkt[30];
    UINT1              *pu1Pkt = NULL;

    MEMSET (&(MplsMipTlv), 0, sizeof (tMplsMipTlv));

    pu1Pkt = au1Pkt;

    LsppRxGetAchMipTlv (&pu1Pkt, &MplsMipTlv);

    return OSIX_SUCCESS;
}

INT4
LsppUtRx_34 (VOID)
{
    tMplsIpPwMepTlv     MplsIpPwMepTlv;
    UINT1               au1Pkt[30];
    UINT1              *pu1Pkt = NULL;
    UINT2               u2TlvLen = 0;

    MEMSET (&(MplsIpPwMepTlv), 0, sizeof (tMplsIpPwMepTlv));
    pu1Pkt = au1Pkt;

    LsppRxGetAchPwMepTlv (&pu1Pkt, &MplsIpPwMepTlv, &u2TlvLen);

    return OSIX_SUCCESS;
}

/* Routines for lsppcore.c */
INT4
LsppUtCore_1 (VOID)
{
    tLsppEchoMsg        pLsppEchoMsg;

    /* Case 1: to test the label stack depth not equal to 0 and get 
     * label failed condition */
    gu4LsppUtTestNumber = 11;
    pLsppEchoMsg.u1LabelStackDepth = 1;
    pLsppEchoMsg.LblInfo[0].u4Label = 1000;
    if (LsppCoreFecLabelValidate (&pLsppEchoMsg) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    pLsppEchoMsg.LblInfo[0].u4Label = 1;
    gu4LsppUtTestNumber = 12;
    pLsppEchoMsg.LsppPduInfo.LsppHeader.u1MessageType = LSPP_ECHO_REPLY;
    if (LsppCoreFecLabelValidate (&pLsppEchoMsg) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    gu4LsppUtTestNumber = 13;
    pLsppEchoMsg.LsppPduInfo.u2TlvsPresent = LSPP_INTERFACE_AND_LBL_STACK_TLV;
    pLsppEchoMsg.LsppPduInfo.LsppHeader.u1MessageType = LSPP_ECHO_REQUEST;
    pLsppEchoMsg.u1LabelStackDepth = -1;
    if (LsppCoreFecLabelValidate (&pLsppEchoMsg) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    gu4LsppUtTestNumber = 14;
    pLsppEchoMsg.LsppPduInfo.LsppHeader.u1MessageType = LSPP_ECHO_REQUEST;
    pLsppEchoMsg.u1LabelStackDepth = 0;
    pLsppEchoMsg.LsppPduInfo.LsppDsMapTlv.u1AddrType = LSPP_IPV4_NUMBERED;
    pLsppEchoMsg.LsppPduInfo.u2TlvsPresent = LSPP_DSMAP_TLV;
    if (LsppCoreFecLabelValidate (&pLsppEchoMsg) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    gu4LsppUtTestNumber = 15;
    pLsppEchoMsg.u1LabelStackDepth = 2;
    pLsppEchoMsg.LsppPduInfo.u2TlvsPresent = LSPP_INTERFACE_AND_LBL_STACK_TLV;
    if (LsppCoreFecLabelValidate (&pLsppEchoMsg) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
LsppUtCore_2 (VOID)
{
    return OSIX_SUCCESS;
}

INT4
LsppUtCore_3 (VOID)
{
    return OSIX_SUCCESS;
}

INT4
LsppUtCore_4 (VOID)
{
    tLsppFecTlv         pLsppFecTlv;
    UINT1               pu1FecStatus;
    UINT1               pu1ReturnCode;
    UINT4               u4RxLabelL;
    INT4                i4RetVal;

    pLsppFecTlv.u2TlvType = LSPP_FEC_NIL;
    u4RxLabelL = LSPP_LBL_EXPLICIT_NULL;
    if (LsppCoreFecValidate
        (0, &pLsppFecTlv, u4RxLabelL, &pu1FecStatus,
         &pu1ReturnCode) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    u4RxLabelL = LSPP_LBL_ROUTER_ALERT;
    if (LsppCoreFecValidate
        (0, &pLsppFecTlv, u4RxLabelL, &pu1FecStatus,
         &pu1ReturnCode) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    u4RxLabelL = LSPP_LBL_IMPLICIT_NULL;
    i4RetVal =
        LsppCoreFecValidate (0, &pLsppFecTlv, u4RxLabelL, &pu1FecStatus,
                             &pu1ReturnCode);
    if ((i4RetVal != OSIX_FAILURE)
        || ((pu1ReturnCode != LSPP_MAP_FAIL_FEC_TO_LBL_AT_STACK_DEPTH)
            && (pu1FecStatus == LSPP_FEC_VALIDATION_FAILED)))
    {
        return OSIX_FAILURE;
    }

    gu4LsppUtTestNumber = 41;
    pLsppFecTlv.u2TlvType = LSPP_FEC_GENERIC_IPV6;
    if (LsppCoreFecValidate
        (0, &pLsppFecTlv, u4RxLabelL, &pu1FecStatus,
         &pu1ReturnCode) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* 
       gu4LsppUtTestNumber = 42;
       pLsppFecTlv.u2TlvType = LSPP_FEC_GENERIC_IPV6 ;
       if ( LsppCoreFecValidate(0, &pLsppFecTlv,u4RxLabelL, &pu1FecStatus, &pu1ReturnCode) != OSIX_SUCCESS)
       {
       return OSIX_FAILURE;
       } */
    return OSIX_SUCCESS;

}

INT4
LsppUtCore_5 (VOID)
{
    tLsppFecTlv         pLsppFecTlv;
    UINT1               pu1FecStatus;
    UINT1               pu1ReturnCode;
    UINT4               u4RxLabelL;

    gu4LsppUtTestNumber = 51;
    pLsppFecTlv.u2TlvType = LSPP_FEC_LDP_IPV4;
    if (LsppCoreCheckFecLabelMap
        (0, &pLsppFecTlv, u4RxLabelL, &pu1FecStatus,
         &pu1ReturnCode) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    pLsppFecTlv.u2TlvType = LSPP_FEC_LDP_IPV6;
    if (LsppCoreCheckFecLabelMap
        (0, &pLsppFecTlv, u4RxLabelL, &pu1FecStatus,
         &pu1ReturnCode) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    pLsppFecTlv.u2TlvType = 100;
    if (LsppCoreCheckFecLabelMap
        (0, &pLsppFecTlv, u4RxLabelL, &pu1FecStatus,
         &pu1ReturnCode) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    gu4LsppUtTestNumber = 52;
    pLsppFecTlv.u2TlvType = LSPP_FEC_STATIC_PW;
    if (LsppCoreCheckFecLabelMap
        (0, &pLsppFecTlv, u4RxLabelL, &pu1FecStatus,
         &pu1ReturnCode) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    gu4LsppUtTestNumber = 53;
    if (LsppCoreCheckFecLabelMap
        (0, &pLsppFecTlv, u4RxLabelL, &pu1FecStatus,
         &pu1ReturnCode) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    gu4LsppUtTestNumber = 54;
    u4RxLabelL = LSPP_LBL_EXPLICIT_NULL;
    if (LsppCoreCheckFecLabelMap
        (0, &pLsppFecTlv, u4RxLabelL, &pu1FecStatus,
         &pu1ReturnCode) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    gu4LsppUtTestNumber = 55;
    u4RxLabelL = LSPP_LBL_EXPLICIT_NULL;
    if (LsppCoreCheckFecLabelMap
        (0, &pLsppFecTlv, u4RxLabelL, &pu1FecStatus,
         &pu1ReturnCode) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtCore_6 (VOID)
{
    UINT1               pu1EntryExists;
    UINT1               pu1LblAction;

    gu4LsppUtTestNumber = 61;
    if (LsppCoreGetLabelInfo (0, 1, 1, &pu1EntryExists, &pu1LblAction) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    gu4LsppUtTestNumber = 62;
    if (LsppCoreGetLabelInfo (0, 1, 1, &pu1EntryExists, &pu1LblAction) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    gu4LsppUtTestNumber = 63;
    if (LsppCoreGetLabelInfo (0, 1, 1, &pu1EntryExists, &pu1LblAction) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    gu4LsppUtTestNumber = 64;
    if (LsppCoreGetLabelInfo (0, 1, 1, &pu1EntryExists, &pu1LblAction) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    gu4LsppUtTestNumber = 65;
    if (LsppCoreGetLabelInfo (0, 1, 1, &pu1EntryExists, &pu1LblAction) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtCore_7 (VOID)
{
    return OSIX_SUCCESS;
}

INT4
LsppUtCore_8 (VOID)
{
    return OSIX_SUCCESS;
}

INT4
LsppUtCore_9 (VOID)
{
    tLsppEchoMsg        pLsppEchoMsg;

    pLsppEchoMsg.u1LabelStackDepth = 1;
    MEMSET (&pLsppEchoMsg.LblInfo, 0, sizeof (tMplsLblInfo));

    gu4LsppUtTestNumber = 91;
    pLsppEchoMsg.u1LabelStackDepth = 0;
    if (LsppCoreGetInfoForIfLblStkTlv (&pLsppEchoMsg) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    pLsppEchoMsg.u1LabelStackDepth = 100;
    if (LsppCoreGetInfoForIfLblStkTlv (&pLsppEchoMsg) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    gu4LsppUtTestNumber = 92;
    pLsppEchoMsg.u1LabelStackDepth = 1;
    if (LsppCoreGetInfoForIfLblStkTlv (&pLsppEchoMsg) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    gu4LsppUtTestNumber = 93;
    if (LsppCoreGetInfoForIfLblStkTlv (&pLsppEchoMsg) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    gu4LsppUtTestNumber = 94;
    if (LsppCoreGetInfoForIfLblStkTlv (&pLsppEchoMsg) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    gu4LsppUtTestNumber = 95;
    if (LsppCoreGetInfoForIfLblStkTlv (&pLsppEchoMsg) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtCore_10 (VOID)
{
    tLsppFsLsppEchoSequenceTableEntry pEchoSeqEntry;

    gu4LsppUtTestNumber = 101;
    pEchoSeqEntry.MibObject.u4FsLsppContextId = 0;
    pEchoSeqEntry.MibObject.u4FsLsppSenderHandle = 1;
    if (LsppCoreHandleReqTimedOut (&pEchoSeqEntry) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtCore_11 (VOID)
{
    UINT1               pu1RetCode;

    gu4LsppUtTestNumber = 111;
    LsppCoreUpdateGlobalStats (0, LSPP_REQ_TX, NULL);
    gu4LsppUtTestNumber = 111;
    LsppCoreUpdateGlobalStats (0, LSPP_REQ_RX, NULL);
    LsppCoreUpdateGlobalStats (0, LSPP_REQ_DROPPED, NULL);
    LsppCoreUpdateGlobalStats (0, LSPP_REQ_TIMEDOUT, NULL);
    LsppCoreUpdateGlobalStats (0, LSPP_REQ_UNSENT, NULL);
    LsppCoreUpdateGlobalStats (0, LSPP_REPLY_TX, NULL);
    LsppCoreUpdateGlobalStats (0, LSPP_REPLY_RX, NULL);
    pu1RetCode = LSPP_NO_RETURN_CODE;
    LsppCoreUpdateGlobalStats (0, LSPP_REPLY_RX, &pu1RetCode);
    pu1RetCode = LSPP_MALFORMED_ECHO_REQ_RECEIVED;
    LsppCoreUpdateGlobalStats (0, LSPP_REPLY_RX, &pu1RetCode);
    pu1RetCode = LSPP_ONE_OR_MORE_TLVS_NOT_UNDERSTOOD;
    LsppCoreUpdateGlobalStats (0, LSPP_REPLY_RX, &pu1RetCode);
    pu1RetCode = LSPP_EGRESS_FOR_FEC_AT_STACK_DEPTH;
    LsppCoreUpdateGlobalStats (0, LSPP_REPLY_RX, &pu1RetCode);
    pu1RetCode = LSPP_NO_MAPPING_FOR_FEC_AT_STACK_DEPTH;
    LsppCoreUpdateGlobalStats (0, LSPP_REPLY_RX, &pu1RetCode);
    pu1RetCode = LSPP_DOWNSTREAM_MAPPING_MISMATCH;
    LsppCoreUpdateGlobalStats (0, LSPP_REPLY_RX, &pu1RetCode);
    pu1RetCode = LSPP_UPSTREAM_IF_INDEX_UNKNOWN;
    LsppCoreUpdateGlobalStats (0, LSPP_REPLY_RX, &pu1RetCode);
    pu1RetCode = LSPP_RESERVED_RETURN_CODE;
    LsppCoreUpdateGlobalStats (0, LSPP_REPLY_RX, &pu1RetCode);
    pu1RetCode = LSPP_LBL_SWITCHED_AT_STACK_DEPTH;
    LsppCoreUpdateGlobalStats (0, LSPP_REPLY_RX, &pu1RetCode);
    pu1RetCode = LSPP_LBL_SWITCHED_BUT_NO_MPLS_FWD_AT_STACK_DEPTH;
    LsppCoreUpdateGlobalStats (0, LSPP_REPLY_RX, &pu1RetCode);
    pu1RetCode = LSPP_MAP_FAIL_FEC_TO_LBL_AT_STACK_DEPTH;
    LsppCoreUpdateGlobalStats (0, LSPP_REPLY_RX, &pu1RetCode);
    pu1RetCode = LSPP_NO_LBL_ENTRY_AT_STACK_DEPTH;
    LsppCoreUpdateGlobalStats (0, LSPP_REPLY_RX, &pu1RetCode);
    pu1RetCode = LSPP_PROTOCOL_MISMATCH;
    LsppCoreUpdateGlobalStats (0, LSPP_REPLY_RX, &pu1RetCode);
    pu1RetCode = LSPP_PRE_TERMINATED_REQUEST;
    LsppCoreUpdateGlobalStats (0, LSPP_REPLY_RX, &pu1RetCode);
    pu1RetCode = 100;
    LsppCoreUpdateGlobalStats (0, LSPP_REPLY_RX, &pu1RetCode);
    LsppCoreUpdateGlobalStats (0, LSPP_REPLY_DROPPED, NULL);
    LsppCoreUpdateGlobalStats (0, 100, NULL);
    return OSIX_SUCCESS;
}

INT4
LsppUtCore_12 (VOID)
{
    UINT4               pu4L3IfIndex;

    gu4LsppUtTestNumber = 121;

    if (LsppCoreGetL3If (0, 1, &pu4L3IfIndex) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    gu4LsppUtTestNumber = 122;

    if (LsppCoreGetL3If (0, 1, &pu4L3IfIndex) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtCore_13 (VOID)
{
    UINT4               pu4SrcIp;

    gu4LsppUtTestNumber = 131;
    if (LsppCoreGetSrcIpFromDestIp (0, 150, &pu4SrcIp) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    gu4LsppUtTestNumber = 132;
    if (LsppCoreGetSrcIpFromDestIp (0, 150, &pu4SrcIp) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtCore_14 (VOID)
{
    tLsppPathInfo       pLsppPathInfo;

    gu4LsppUtTestNumber = 141;
    if (LsppCoreGetRevPathFromDestIp (0, 150, &pLsppPathInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    gu4LsppUtTestNumber = 142;
    if (LsppCoreGetRevPathFromDestIp (0, 150, &pLsppPathInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    gu4LsppUtTestNumber = 143;
    if (LsppCoreGetRevPathFromDestIp (0, 150, &pLsppPathInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    gu4LsppUtTestNumber = 144;
    if (LsppCoreGetRevPathFromDestIp (0, 150, &pLsppPathInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    gu4LsppUtTestNumber = 145;
    if (LsppCoreGetRevPathFromDestIp (0, 150, &pLsppPathInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    gu4LsppUtTestNumber = 146;
    if (LsppCoreGetRevPathFromDestIp (0, 150, &pLsppPathInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    gu4LsppUtTestNumber = 147;
    if (LsppCoreGetRevPathFromDestIp (0, 150, &pLsppPathInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtCore_15 (VOID)
{
    UINT2               pu2Mtu;

    gu4LsppUtTestNumber = 151;
    if (LsppCoreGetIfMtu (0, 1, &pu2Mtu) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    gu4LsppUtTestNumber = 152;
    if (LsppCoreGetIfMtu (0, 1, &pu2Mtu) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtCore_16 (VOID)
{
    tLsppMepTlv         pMepTlv;
    tLsppPathInfo       pLsppPathInfo;

    pMepTlv.unMepTlv.IpLspMepTlv.GlobalNodeId.u4GlobalId = 100;
    pMepTlv.unMepTlv.IpLspMepTlv.GlobalNodeId.u4NodeId = 10;
    pMepTlv.unMepTlv.IpLspMepTlv.u2TnlNum = 1;
    pMepTlv.unMepTlv.IpLspMepTlv.u2LspNum = 1;

    gu4LsppUtTestNumber = 161;
    if (LsppCoreGetRevPathInfoFromMep
        (0, &pMepTlv, LSPP_ACH_IP_LSP_MEP_TLV, &pLsppPathInfo) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    gu4LsppUtTestNumber = 162;
    if (LsppCoreGetRevPathInfoFromMep
        (0, &pMepTlv, LSPP_ACH_IP_LSP_MEP_TLV, &pLsppPathInfo) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (LsppCoreGetRevPathInfoFromMep (0, &pMepTlv, 100, &pLsppPathInfo) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtCore_17 (VOID)
{
    tLsppEchoMsg        pEchoMsg;

    pEchoMsg.LsppPathInfo.MegDetail.u4MpType = LSPP_ME_MP_TYPE_MEP;
    pEchoMsg.LsppPathInfo.MegDetail.MplsNodeId.u4GlobalId = 100;

    pEchoMsg.LsppPathInfo.MegDetail.MplsNodeId.u4NodeId = 10;
    pEchoMsg.LsppPathInfo.MegDetail.u4MpIfIndex = 1;

    pEchoMsg.LsppPathInfo.MegDetail.au1Icc[0] = 1;
    pEchoMsg.LsppPathInfo.MegDetail.au1Icc[1] = 2;
    pEchoMsg.LsppPathInfo.MegDetail.au1Icc[2] = 3;
    pEchoMsg.LsppPathInfo.MegDetail.au1Icc[3] = 4;
    pEchoMsg.LsppPathInfo.MegDetail.au1Icc[4] = 5;
    pEchoMsg.LsppPathInfo.MegDetail.au1Icc[5] = 6;

    pEchoMsg.LsppPathInfo.MegDetail.au1Umc[0] = 1;
    pEchoMsg.LsppPathInfo.MegDetail.au1Umc[1] = 2;
    pEchoMsg.LsppPathInfo.MegDetail.au1Umc[2] = 3;
    pEchoMsg.LsppPathInfo.MegDetail.au1Umc[3] = 4;
    pEchoMsg.LsppPathInfo.MegDetail.au1Umc[4] = 5;
    pEchoMsg.LsppPathInfo.MegDetail.au1Umc[5] = 6;
    pEchoMsg.LsppPathInfo.MegDetail.au1Umc[6] = 7;
    pEchoMsg.LsppPathInfo.MegDetail.u4IccSrcMepIndex = 7;

    pEchoMsg.LsppPathInfo.PwDetail.PwFecInfo.u1AgiType = 1;
    pEchoMsg.LsppPathInfo.PwDetail.PwFecInfo.u1AgiLen = 1;
    pEchoMsg.LsppPathInfo.PwDetail.PwFecInfo.SrcNodeId.MplsGlobalNodeId.
        u4GlobalId = 100;
    pEchoMsg.LsppPathInfo.PwDetail.PwFecInfo.SrcNodeId.MplsGlobalNodeId.
        u4NodeId = 10;
    pEchoMsg.LsppPathInfo.PwDetail.PwFecInfo.u4SrcAcId = 1;

    pEchoMsg.LsppPathInfo.MegDetail.unServiceInfo.
        ServiceTnlId.SrcNodeId.MplsGlobalNodeId.u4GlobalId = 100;

    pEchoMsg.LsppPathInfo.MegDetail.unServiceInfo.
        ServiceTnlId.SrcNodeId.MplsGlobalNodeId.u4NodeId = 100;

    pEchoMsg.LsppPathInfo.MegDetail.unServiceInfo.ServiceTnlId.u4SrcTnlNum = 1;

    pEchoMsg.LsppPathInfo.MegDetail.unServiceInfo.ServiceTnlId.u4LspNum = 1;

    pEchoMsg.LsppPathInfo.MegDetail.u4OperatorType = LSPP_MEG_OPERATOR_TYPE_ICC;
    pEchoMsg.LsppPathInfo.MegDetail.u1ServiceType = 0;
    LsppCoreUpdateMepOrMipTlv (&pEchoMsg);

    pEchoMsg.LsppPathInfo.MegDetail.u4OperatorType = LSPP_MEG_OPERATOR_TYPE_IP;
    pEchoMsg.LsppPathInfo.MegDetail.u1ServiceType = LSPP_MPLS_PATH_TYPE_PW;
    LsppCoreUpdateMepOrMipTlv (&pEchoMsg);

    pEchoMsg.LsppPathInfo.MegDetail.u4OperatorType = LSPP_MEG_OPERATOR_TYPE_IP;
    pEchoMsg.LsppPathInfo.MegDetail.u1ServiceType = LSPP_MPLS_PATH_TYPE_TNL;
    LsppCoreUpdateMepOrMipTlv (&pEchoMsg);

    pEchoMsg.LsppPathInfo.MegDetail.u4MpType = LSPP_ME_MP_TYPE_MIP;
    pEchoMsg.LsppAchInfo.u1AchTlvPresent = LSPP_ACH_MIP_TLV;
    LsppCoreUpdateMepOrMipTlv (&pEchoMsg);
    return OSIX_SUCCESS;
}

INT4
LsppUtCore_18 (VOID)
{
    /* This function is covered in the above function itself */

    return OSIX_SUCCESS;
}

INT4
LsppUtCore_19 (VOID)
{
    tLsppEchoMsg        pLsppEchoMsg;
    tLsppFsLsppPingTraceTableEntry pLsppPingTraceEntry;
    tLsppFsLsppEchoSequenceTableEntry pLsppEchoSequenceEntry;
    tLsppSysTime        pLsppEchoPktRxTime;
    UINT1               u1RcvdMsgType = LSPP_ECHO_REQUEST;

    MEMSET (&pLsppPingTraceEntry, 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&pLsppEchoSequenceEntry, 0,
            sizeof (tLsppFsLsppEchoSequenceTableEntry));
    MEMSET (&pLsppEchoPktRxTime, 0, sizeof (tLsppSysTime));
    pLsppEchoMsg.LsppPduInfo.LsppHeader.u1MessageType = LSPP_ECHO_REQUEST;
    if (LsppCoreProcessRequest
        (&pLsppEchoMsg, &pLsppPingTraceEntry, &pLsppEchoSequenceEntry,
         &pLsppEchoPktRxTime, u1RcvdMsgType) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    gu4LsppUtTestNumber = 191;
    pLsppEchoMsg.LsppPduInfo.LsppHeader.u1ReturnCode = 10;
    if (LsppCoreProcessRequest
        (&pLsppEchoMsg, NULL, NULL, &pLsppEchoPktRxTime,
         u1RcvdMsgType) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    gu4LsppUtTestNumber = 192;
    pLsppEchoMsg.LsppPduInfo.u2TlvsPresent = LSPP_TARGET_FEC_TLV;
    pLsppEchoMsg.LsppPduInfo.LsppHeader.u1ReturnCode = 0;
    pLsppEchoMsg.LsppPduInfo.LsppHeader.u1MessageType = LSPP_ECHO_REPLY;
    u1RcvdMsgType = LSPP_ECHO_REPLY;
    pLsppPingTraceEntry.MibObject.i4FsLsppStatus = LSPP_ECHO_IN_PROGRESS;
    if (LsppCoreProcessRequest
        (&pLsppEchoMsg, &pLsppPingTraceEntry, &pLsppEchoSequenceEntry,
         &pLsppEchoPktRxTime, u1RcvdMsgType) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    gu4LsppUtTestNumber = 193;
    pLsppEchoMsg.LsppPduInfo.u2TlvsPresent = 10;
    pLsppEchoMsg.LsppPduInfo.LsppHeader.u1ReturnCode = 0;
    pLsppEchoMsg.LsppPduInfo.LsppHeader.u1MessageType = LSPP_ECHO_REPLY;
    pLsppPingTraceEntry.MibObject.i4FsLsppStatus = LSPP_ECHO_FAILURE;
    if (LsppCoreProcessRequest
        (&pLsppEchoMsg, &pLsppPingTraceEntry, &pLsppEchoSequenceEntry,
         &pLsppEchoPktRxTime, u1RcvdMsgType) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtCore_20 (VOID)
{
    return OSIX_SUCCESS;
}

INT4
LsppUtCore_21 (VOID)
{
    return OSIX_SUCCESS;
}

INT4
LsppUtCore_22 (VOID)
{
    return OSIX_SUCCESS;
}

INT4
LsppUtCore_23 (VOID)
{
    return OSIX_SUCCESS;
}

INT4
LsppUtCore_24 (VOID)
{
    return OSIX_SUCCESS;
}

INT4
LsppUtCore_25 (VOID)
{
    tLsppFsLsppPingTraceTableEntry pPingTraceEntry;
    tLsppEchoMsg        pLsppEchoMsg;

    gu4LsppUtTestNumber = 251;
    if (LsppCoreSendTraceRouteReq (&pPingTraceEntry, &pLsppEchoMsg) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    gu4LsppUtTestNumber = 252;
    if (LsppCoreSendTraceRouteReq (&pPingTraceEntry, &pLsppEchoMsg) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
/*
   if ( LsppCoreSendTraceRouteReq(&pPingTraceEntry,NULL) != OSIX_SUCCESS)
   {
        return OSIX_FAILURE;
   }
*/
    return OSIX_SUCCESS;
}

INT4
LsppUtCore_26 (VOID)
{
    tLsppFsLsppPingTraceTableEntry pLsppPingTraceEntry;
    tLsppEchoMsg        pLsppEchoMsg;
    tLsppEchoMsg       *pTempLsppEchoMsg[MAX_LSPP_FSLSPP_ECHOMSG_ENTRY];
    tLsppDsMapTlv       pRxDsMapTlv;
    UINT4               u4BfdDiscriminator = 1;
    INT4                i = 0;

    gu4LsppUtTestNumber = 261;
    if (LsppCoreSendEchoReq
        (&pLsppPingTraceEntry, &pLsppEchoMsg, &pRxDsMapTlv,
         u4BfdDiscriminator) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    gu4LsppUtTestNumber = 262;
    if (LsppCoreSendEchoReq
        (&pLsppPingTraceEntry, &pLsppEchoMsg, &pRxDsMapTlv,
         u4BfdDiscriminator) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    for (i = 0; i < MAX_LSPP_FSLSPP_ECHOMSG_ENTRY; i++)
    {
        if ((pTempLsppEchoMsg[i] = (tLsppEchoMsg *)
             MemAllocMemBlk (gLsppGlobals.LsppFsLsppEchoMsgPoolId)) == NULL)
        {
            return OSIX_FAILURE;
        }

    }

    gu4LsppUtTestNumber = 263;

    if (LsppCoreSendEchoReq
        (&pLsppPingTraceEntry, &pLsppEchoMsg, &pRxDsMapTlv,
         u4BfdDiscriminator) != OSIX_FAILURE)
    {
        for (i = 0; i < MAX_LSPP_FSLSPP_ECHOMSG_ENTRY; i++)
        {
            MemReleaseMemBlock (gLsppGlobals.LsppFsLsppEchoMsgPoolId,
                                (UINT1 *) pTempLsppEchoMsg[i]);

        }
        return OSIX_FAILURE;
    }

    for (i = 0; i < MAX_LSPP_FSLSPP_ECHOMSG_ENTRY; i++)
    {
        MemReleaseMemBlock (gLsppGlobals.LsppFsLsppEchoMsgPoolId,
                            (UINT1 *) pTempLsppEchoMsg[i]);

    }
    return OSIX_SUCCESS;
}

INT4
LsppUtCore_27 (VOID)
{
    tLsppFsLsppPingTraceTableEntry pLsppPingTraceEntry;
    tLsppEchoMsg        pLsppEchoMsg;

    gu4LsppUtTestNumber = 271;
    MEMSET (&pLsppEchoMsg, 0, sizeof (tLsppEchoMsg));
    pLsppEchoMsg.u1EncapType = LSPP_ENCAP_TYPE_IP;
    if (LsppCoreFillEncapHeader (&pLsppPingTraceEntry, &pLsppEchoMsg) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtCore_28 (VOID)
{
    UINT4               u4ContextId = 0;
    tLsppOutSegInfo     pOutSegInfo;
    tLsppDsMapTlv       pDsMapTlv;

    gu4LsppUtTestNumber = 281;
    pOutSegInfo.u4OutIf = 1;

    if (LsppCoreFillDsMapTlvInfo (u4ContextId, &pOutSegInfo, &pDsMapTlv) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtCore_29 (VOID)
{
    tLsppEchoMsg        pLsppEchoMsg;
    UINT1               u1PathType = 100;

    if (LsppCoreUpdateTgtFecInfo (&pLsppEchoMsg, u1PathType) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    u1PathType = LSPP_PATH_TYPE_MEP;

    pLsppEchoMsg.LsppPathInfo.u1PathType = LSPP_PATH_TYPE_STATIC_TNL;

    pLsppEchoMsg.LsppPathInfo.TeTnlInfo.TnlInfo.SrcNodeId.MplsGlobalNodeId.
        u4GlobalId = 100;

    pLsppEchoMsg.LsppPathInfo.TeTnlInfo.TnlInfo.SrcNodeId.MplsGlobalNodeId.
        u4NodeId = 10;

    pLsppEchoMsg.LsppPathInfo.TeTnlInfo.TnlInfo.DstNodeId.MplsGlobalNodeId.
        u4GlobalId = 200;

    pLsppEchoMsg.LsppPathInfo.TeTnlInfo.TnlInfo.DstNodeId.MplsGlobalNodeId.
        u4NodeId = 20;

    pLsppEchoMsg.LsppPathInfo.TeTnlInfo.TnlInfo.u4SrcTnlNum = 1;

    pLsppEchoMsg.LsppPathInfo.TeTnlInfo.TnlInfo.u4DstTnlNum = 1;

    pLsppEchoMsg.LsppPathInfo.TeTnlInfo.TnlInfo.u4LspNum = 1;

    gu4LsppUtTestNumber = 291;
    if (LsppCoreUpdateTgtFecInfo (&pLsppEchoMsg, u1PathType) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    if (LsppCoreUpdateTgtFecInfo (&pLsppEchoMsg, 100) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtCore_30 (VOID)
{
    return OSIX_SUCCESS;
}

INT4
LsppUtCore_31 (VOID)
{
    tLsppEchoMsg        pLsppEchoMsg;
    tLsppFsLsppPingTraceTableEntry pLsppPingTraceEntry;

    pLsppPingTraceEntry.MibObject.i4FsLsppRequestType = 2;

    gu4LsppUtTestNumber = 311;

    if (LsppCoreUpdateDsMapInfo (&pLsppEchoMsg, &pLsppPingTraceEntry) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtCore_32 (VOID)
{
    tLsppEchoMsg        pLsppEchoMsg;

    pLsppEchoMsg.u1LabelStackDepth = 8;

    if (LsppCoreUpdateNilFec (&pLsppEchoMsg) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
LsppUtCore_33 (VOID)
{
    return OSIX_SUCCESS;
}

INT4
LsppUtCore_34 (VOID)
{
    tLsppFsLsppPingTraceTableEntry pLsppPingTraceEntry;
    tLsppEchoMsg        pLsppEchoMsg;
    tLsppDsMapTlv       pRxDsMapTlv;
    UINT4               u4BfdDiscriminator = 1;

    gu4LsppUtTestNumber = 341;
    if (LsppCoreUpdateEchoMsgInfo
        (&pLsppPingTraceEntry, &pLsppEchoMsg, &pRxDsMapTlv,
         u4BfdDiscriminator) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    gu4LsppUtTestNumber = 342;
    pLsppPingTraceEntry.MibObject.i4FsLsppEncapType =
        LSPP_ENCAP_TYPE_VCCV_NEGOTIATED;
    pLsppEchoMsg.LsppPathInfo.u1PathType = LSPP_PATH_TYPE_FEC_128;
    pLsppEchoMsg.LsppPathInfo.PwDetail.u1CvSelected = 5;
    if (LsppCoreUpdateEchoMsgInfo
        (&pLsppPingTraceEntry, &pLsppEchoMsg, &pRxDsMapTlv,
         u4BfdDiscriminator) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    gu4LsppUtTestNumber = 343;
    pLsppPingTraceEntry.MibObject.i4FsLsppEncapType = LSPP_ENCAP_TYPE_ACH_IP;
    if (LsppCoreUpdateEchoMsgInfo
        (&pLsppPingTraceEntry, &pLsppEchoMsg, &pRxDsMapTlv,
         u4BfdDiscriminator) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
/*
   gu4LsppUtTestNumber = 344;
   if ( LsppCoreUpdateEchoMsgInfo (&pLsppPingTraceEntry, &pLsppEchoMsg, &pRxDsMapTlv, u4BfdDiscriminator) != OSIX_FAILURE)
   {
       return OSIX_FAILURE;
   }
   gu4LsppUtTestNumber = 345;
   if ( LsppCoreUpdateEchoMsgInfo (&pLsppPingTraceEntry, &pLsppEchoMsg, &pRxDsMapTlv, u4BfdDiscriminator) != OSIX_FAILURE)
   {
       return OSIX_FAILURE;
   }
*/
    return OSIX_SUCCESS;
}

INT4
LsppUtCore_35 (VOID)
{
    UINT4               u4ContextId = 1;
    UINT1               pu1IsRespReq;

    gu4LsppUtTestNumber = 351;
    if (LsppCoreGetBfdBtStrapRespReq (u4ContextId, &pu1IsRespReq) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;

}

INT4
LsppUtCore_37 (VOID)
{
    UINT4               u4ContextId = 1;
    tLsppFsLsppPingTraceTableEntry pLsppPingTraceEntry;

    if (LsppCoreStartAgeOutTimer (u4ContextId, &pLsppPingTraceEntry) !=
        OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtCore_39 (VOID)
{
    tLsppFsLsppPingTraceTableEntry LsppPingTraceEntry;

    MEMSET (&LsppPingTraceEntry, 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    if (LsppCoreHandleTraceUnsent (&LsppPingTraceEntry) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtCxt_1 (VOID)
{
    gu4LsppUtTestNumber = 411;
    if (LsppCxtCreateGlobalConfig (0) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    gu4LsppUtTestNumber = 412;
    if (LsppCxtCreateGlobalConfig (0) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    gu4LsppUtTestNumber = 413;
    if (LsppCxtCreateGlobalConfig (0) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    gu4LsppUtTestNumber = 414;
    if (LsppCxtCreateGlobalConfig (0) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtCxt_2 (VOID)
{

    gu4LsppUtTestNumber = 421;
    if (LsppCxtCreateGlobalStats (0) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    gu4LsppUtTestNumber = 422;
    if (LsppCxtCreateGlobalStats (0) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
LsppUtCxt_3 (VOID)
{

    gu4LsppUtTestNumber = 431;
    if (LsppCxtDeleteGlobalConfig (1) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtCxt_4 (VOID)
{
    if (LsppCxtDeleteGlobalStats (1) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtCxt_5 (VOID)
{
    return OSIX_SUCCESS;
}

INT4
LsppUtCxt_6 (VOID)
{
    return OSIX_SUCCESS;
}

INT4
LsppUtCxt_7 (VOID)
{
    return OSIX_SUCCESS;
}

INT4
LsppUtCxt_8 (VOID)
{
    return OSIX_SUCCESS;
}

INT4
LsppUtCxt_9 (VOID)
{
    tLsppFsLsppPingTraceTableEntry pLsppPingTraceEntry;

    return OSIX_SUCCESS;
    gu4LsppUtTestNumber = 491;
    if (LsppCxtDeletePingTraceInfo (&pLsppPingTraceEntry) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtCxt_10 (VOID)
{
    return OSIX_SUCCESS;
}

INT4
LsppUtCxt_11 (VOID)
{
    tLsppFsLsppPingTraceTableEntry pLsppPingTraceEntry;

    gu4LsppUtTestNumber = 511;
    pLsppPingTraceEntry.MibObject.u4FsLsppContextId = 0;
    if (LsppCxtDeletePingTraceNode (&pLsppPingTraceEntry) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtCxt_12 (VOID)
{
    if (LsppCxtCreateContext (1) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

INT4
LsppUtCxt_13 (VOID)
{
    gu4LsppUtTestNumber = 531;

    if (LsppCxtDeleteContext (0, OSIX_FALSE) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    gu4LsppUtTestNumber = 532;

    if (LsppCxtDeleteContext (0, OSIX_FALSE) != OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

INT4
LsppUtPw_1 (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    CliTakeAppContext ();
    MGMT_UNLOCK ();
    LSPP_UNLOCK;
    if (CliExecuteAppCmd
        ("ping mpls pseudowire 30.0.0.1 vc-id 1 global-id 1 node-id 1 if-num 1")
        != OSIX_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
    CliGiveAppContext ();
    LSPP_LOCK;
    MGMT_LOCK ();
    return i4RetVal;

}

INT4
LsppUtPw_2 (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    CliTakeAppContext ();
    LSPP_UNLOCK;
    MGMT_UNLOCK ();
    if (CliExecuteAppCmd ("ping mpls pseudowire 30.0.0.1 vc-id 1 lsp 1") !=
        OSIX_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
    CliGiveAppContext ();
    MGMT_LOCK ();
    LSPP_LOCK;
    return i4RetVal;

}

INT4
LsppUtPw_3 (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    CliTakeAppContext ();
    LSPP_UNLOCK;
    MGMT_UNLOCK ();
    if (CliExecuteAppCmd ("ping mpls pseudowire 30.0.0.1 vc-id 1 ttl 1") !=
        OSIX_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
    CliGiveAppContext ();
    MGMT_LOCK ();
    LSPP_LOCK;
    return i4RetVal;

}

INT4
LsppUtPw_4 (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    CliTakeAppContext ();
    LSPP_UNLOCK;
    MGMT_UNLOCK ();
    if (CliExecuteAppCmd
        ("ping mpls pseudowire 30.0.0.1 vc-id 1 reply mode ipv4") !=
        OSIX_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
    CliGiveAppContext ();
    MGMT_LOCK ();
    LSPP_LOCK;
    return i4RetVal;

}

INT4
LsppUtPw_5 (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    CliTakeAppContext ();
    LSPP_UNLOCK;
    MGMT_UNLOCK ();
    if (CliExecuteAppCmd
        ("ping mpls pseudowire 30.0.0.1 vc-id 1 reply mode router-alert") !=
        OSIX_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
    CliGiveAppContext ();
    MGMT_LOCK ();
    LSPP_LOCK;
    return i4RetVal;

}

INT4
LsppUtPw_6 (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    CliTakeAppContext ();
    LSPP_UNLOCK;
    MGMT_UNLOCK ();
    if (CliExecuteAppCmd
        ("ping mpls pseudowire 30.0.0.1 vc-id 1 reply mode ipv6") !=
        OSIX_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
    CliGiveAppContext ();
    MGMT_LOCK ();
    LSPP_LOCK;
    return i4RetVal;

}

INT4
LsppUtPw_7 (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    CliTakeAppContext ();
    LSPP_UNLOCK;
    MGMT_UNLOCK ();
    if (CliExecuteAppCmd
        ("ping mpls pseudowire 30.0.0.1 vc-id 1 reply mode control-channel") !=
        OSIX_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
    CliGiveAppContext ();
    MGMT_LOCK ();
    LSPP_LOCK;
    return i4RetVal;

}

INT4
LsppUtPw_8 (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    CliTakeAppContext ();
    LSPP_UNLOCK;
    MGMT_UNLOCK ();
    if (CliExecuteAppCmd
        ("ping mpls pseudowire 30.0.0.1 vc-id 1 reply mode no-reply") !=
        OSIX_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
    CliGiveAppContext ();
    MGMT_LOCK ();
    LSPP_LOCK;
    return i4RetVal;

}

INT4
LsppUtPw_9 (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    CliTakeAppContext ();
    LSPP_UNLOCK;
    MGMT_UNLOCK ();
    if (CliExecuteAppCmd
        ("ping mpls pseudowire 30.0.0.1 vc-id 1 force-explicit-null") !=
        OSIX_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
    CliGiveAppContext ();
    MGMT_LOCK ();
    LSPP_LOCK;
    return i4RetVal;

}

INT4
LsppUtPw_10 (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    CliTakeAppContext ();
    LSPP_UNLOCK;
    MGMT_UNLOCK ();
    if (CliExecuteAppCmd ("ping mpls pseudowire 30.0.0.1 vc-id 1 dsmap") !=
        OSIX_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
    CliGiveAppContext ();
    MGMT_LOCK ();
    LSPP_LOCK;
    return i4RetVal;

}

INT4
LsppUtPw_11 (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    CliTakeAppContext ();
    LSPP_UNLOCK;
    MGMT_UNLOCK ();
    if (CliExecuteAppCmd ("ping mpls pseudowire 30.0.0.1 vc-id 1 rx-intf-label")
        != OSIX_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
    CliGiveAppContext ();
    MGMT_LOCK ();
    LSPP_LOCK;
    return i4RetVal;

}

INT4
LsppUtPw_12 (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    CliTakeAppContext ();
    LSPP_UNLOCK;
    MGMT_UNLOCK ();
    if (CliExecuteAppCmd
        ("ping mpls pseudowire 30.0.0.1 vc-id 1 rev-path-verify") !=
        OSIX_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
    CliGiveAppContext ();
    MGMT_LOCK ();
    LSPP_LOCK;
    return i4RetVal;

}

INT4
LsppUtPw_13 (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    CliTakeAppContext ();
    LSPP_UNLOCK;
    MGMT_UNLOCK ();
    if (CliExecuteAppCmd
        ("ping mpls pseudowire 30.0.0.1 vc-id 1 encap-type mpls-ip") !=
        OSIX_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
    CliGiveAppContext ();
    MGMT_LOCK ();
    LSPP_LOCK;
    return i4RetVal;

}

INT4
LsppUtPw_14 (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    CliTakeAppContext ();
    LSPP_UNLOCK;
    MGMT_UNLOCK ();
    if (CliExecuteAppCmd
        ("ping mpls pseudowire 30.0.0.1 vc-id 1 encap-type mpls-ach") !=
        OSIX_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
    CliGiveAppContext ();
    MGMT_LOCK ();
    LSPP_LOCK;
    return i4RetVal;

}

INT4
LsppUtPw_15 (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    CliTakeAppContext ();
    LSPP_UNLOCK;
    MGMT_UNLOCK ();
    if (CliExecuteAppCmd
        ("ping mpls pseudowire 30.0.0.1 vc-id 1 encap-type mpls-ach-ip") !=
        OSIX_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
    CliGiveAppContext ();
    MGMT_LOCK ();
    LSPP_LOCK;
    return i4RetVal;

}

INT4
LsppUtPw_16 (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    CliTakeAppContext ();
    LSPP_UNLOCK;
    MGMT_UNLOCK ();
    if (CliExecuteAppCmd
        ("ping mpls pseudowire 30.0.0.1 vc-id 2 global-id 1 node-id 2 if-num 1")
        != OSIX_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
    CliGiveAppContext ();
    MGMT_LOCK ();
    LSPP_LOCK;
    return i4RetVal;

}

INT4
LsppUtPw_17 (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    CliTakeAppContext ();
    LSPP_UNLOCK;
    MGMT_UNLOCK ();
    if (CliExecuteAppCmd ("ping mpls pseudowire 30.0.0.1 vc-id 2 lsp 1") !=
        OSIX_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
    CliGiveAppContext ();
    MGMT_LOCK ();
    LSPP_LOCK;
    return i4RetVal;

}

INT4
LsppUtPw_18 (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    CliTakeAppContext ();
    LSPP_UNLOCK;
    MGMT_UNLOCK ();
    if (CliExecuteAppCmd ("ping mpls pseudowire 30.0.0.1 vc-id 2 ttl 1") !=
        OSIX_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
    CliGiveAppContext ();
    MGMT_LOCK ();
    LSPP_LOCK;
    return i4RetVal;

}

INT4
LsppUtPw_19 (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    CliTakeAppContext ();
    LSPP_UNLOCK;
    MGMT_UNLOCK ();
    if (CliExecuteAppCmd
        ("ping mpls pseudowire 30.0.0.1 vc-id 2 reply mode ipv4") !=
        OSIX_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
    CliGiveAppContext ();
    MGMT_LOCK ();
    LSPP_LOCK;
    return i4RetVal;

}

INT4
LsppUtPw_20 (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    CliTakeAppContext ();
    LSPP_UNLOCK;
    MGMT_UNLOCK ();
    if (CliExecuteAppCmd
        ("ping mpls pseudowire 30.0.0.1 vc-id 2 reply mode router-alert") !=
        OSIX_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
    CliGiveAppContext ();
    MGMT_LOCK ();
    LSPP_LOCK;
    return i4RetVal;

}

INT4
LsppUtPw_21 (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    CliTakeAppContext ();
    LSPP_UNLOCK;
    MGMT_UNLOCK ();
    if (CliExecuteAppCmd
        ("ping mpls pseudowire 30.0.0.1 vc-id 2 reply mode ipv6") !=
        OSIX_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
    CliGiveAppContext ();
    MGMT_LOCK ();
    LSPP_LOCK;
    return i4RetVal;

}

INT4
LsppUtPw_22 (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    CliTakeAppContext ();
    LSPP_UNLOCK;
    MGMT_UNLOCK ();
    if (CliExecuteAppCmd
        ("ping mpls pseudowire 30.0.0.1 vc-id 2 reply mode control-channel") !=
        OSIX_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
    CliGiveAppContext ();
    MGMT_LOCK ();
    LSPP_LOCK;
    return i4RetVal;

}

INT4
LsppUtPw_23 (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    CliTakeAppContext ();
    LSPP_UNLOCK;
    MGMT_UNLOCK ();
    if (CliExecuteAppCmd
        ("ping mpls pseudowire 30.0.0.1 vc-id 2 reply mode no-reply") !=
        OSIX_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
    CliGiveAppContext ();
    MGMT_LOCK ();
    LSPP_LOCK;
    return i4RetVal;

}

INT4
LsppUtPw_24 (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    CliTakeAppContext ();
    LSPP_UNLOCK;
    MGMT_UNLOCK ();
    if (CliExecuteAppCmd
        ("ping mpls pseudowire 30.0.0.1 vc-id 2 force-explicit-null") !=
        OSIX_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
    CliGiveAppContext ();
    MGMT_LOCK ();
    LSPP_LOCK;
    return i4RetVal;

}

INT4
LsppUtPw_25 (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    CliTakeAppContext ();
    LSPP_UNLOCK;
    MGMT_UNLOCK ();
    if (CliExecuteAppCmd ("ping mpls pseudowire 30.0.0.1 vc-id 2 dsmap") !=
        OSIX_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
    CliGiveAppContext ();
    MGMT_LOCK ();
    LSPP_LOCK;
    return i4RetVal;

}

INT4
LsppUtPw_26 (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    CliTakeAppContext ();
    LSPP_UNLOCK;
    MGMT_UNLOCK ();
    if (CliExecuteAppCmd ("ping mpls pseudowire 30.0.0.1 vc-id 2 rx-intf-label")
        != OSIX_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
    CliGiveAppContext ();
    MGMT_LOCK ();
    LSPP_LOCK;
    return i4RetVal;

}

INT4
LsppUtPw_27 (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    CliTakeAppContext ();
    LSPP_UNLOCK;
    MGMT_UNLOCK ();
    if (CliExecuteAppCmd
        ("ping mpls pseudowire 30.0.0.1 vc-id 2 rev-path-verify") !=
        OSIX_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
    CliGiveAppContext ();
    MGMT_LOCK ();
    LSPP_LOCK;
    return i4RetVal;

}

INT4
LsppUtPw_28 (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    CliTakeAppContext ();
    LSPP_UNLOCK;
    MGMT_UNLOCK ();
    if (CliExecuteAppCmd
        ("ping mpls pseudowire 30.0.0.1 vc-id 2 encap-type mpls-ip") !=
        OSIX_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
    CliGiveAppContext ();
    MGMT_LOCK ();
    LSPP_LOCK;
    return i4RetVal;

}

INT4
LsppUtPw_29 (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    CliTakeAppContext ();
    LSPP_UNLOCK;
    MGMT_UNLOCK ();
    if (CliExecuteAppCmd
        ("ping mpls pseudowire 30.0.0.1 vc-id 2 encap-type mpls-ach") !=
        OSIX_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
    CliGiveAppContext ();
    MGMT_LOCK ();
    LSPP_LOCK;
    return i4RetVal;

}

INT4
LsppUtPw_30 (VOID)
{
    INT4                i4RetVal = OSIX_SUCCESS;

    CliTakeAppContext ();
    LSPP_UNLOCK;
    MGMT_UNLOCK ();
    if (CliExecuteAppCmd
        ("ping mpls pseudowire 30.0.0.1 vc-id 2 encap-type mpls-ach-ip") !=
        OSIX_FAILURE)
    {
        i4RetVal = OSIX_FAILURE;
    }
    CliGiveAppContext ();
    MGMT_LOCK ();
    LSPP_LOCK;
    return i4RetVal;

}

INT4
LsppUtPw_31 (VOID)
{

#if 0
    INT4                i4RetVal = OSIX_SUCCESS;

    gu4LsppUtTestNumber = 1001;
    CliTakeAppContext ();
    LSPP_UNLOCK;
    MGMT_UNLOCK ();
    if (CliExecuteAppCmd ("ping mpls pseudowire 30.0.0.1 vc-id 1") !=
        OSIX_SUCCESS)
    {
        i4RetVal = OSIX_FAILURE;
    }
    CliGiveAppContext ();
    MGMT_LOCK ();
    LSPP_LOCK;
    return i4RetVal;
#endif
    return OSIX_FAILURE;
}

INT4
LsppUtPw_32 (VOID)
{
#if 0
    INT4                i4RetVal = OSIX_SUCCESS;

    gu4LsppUtTestNumber = 1002;

    CliTakeAppContext ();
    LSPP_UNLOCK;
    MGMT_UNLOCK ();
    if (CliExecuteAppCmd ("ping mpls pseudowire 30.0.0.1 vc-id 1") !=
        OSIX_SUCCESS)
    {
        i4RetVal = OSIX_FAILURE;
    }
    CliGiveAppContext ();
    MGMT_LOCK ();
    LSPP_LOCK;
    return i4RetVal;
#endif
    return OSIX_FAILURE;

}

INT4
LsppUtPw_33 (VOID)
{
#if 0
    INT4                i4RetVal = OSIX_SUCCESS;

    gu4LsppUtTestNumber = 1003;

    CliTakeAppContext ();
    LSPP_UNLOCK;
    MGMT_UNLOCK ();
    if (CliExecuteAppCmd ("ping mpls pseudowire 30.0.0.1 vc-id 1") !=
        OSIX_SUCCESS)
    {
        i4RetVal = OSIX_FAILURE;
    }
    CliGiveAppContext ();
    MGMT_LOCK ();
    LSPP_LOCK;
    return i4RetVal;
#endif
    return OSIX_FAILURE;
}

/* UT Stub function for external module interaction.*/

INT4
LsppUtPortHdlExtInteraction (tLsppExtInParams * pLsppExtInParams,
                             tLsppExtOutParams * pLsppExtOutParams)
{
    /*UINT1 u1CoRoutedLsp = 0; *
       tMplsApiInInfo      MplsInApiInfo;

       tMplsApiOutInfo     MplsOutApiInfo;

       MEMSET ((&MplsOutApiInfo), 0, sizeof (tMplsApiOutInfo));
       MEMSET ((&MplsInApiInfo), 0, sizeof (tMplsApiInInfo)); */

    if (gu4LsppUtTestNumber == 11)
    {
        return OSIX_FAILURE;
    }
    if (gu4LsppUtTestNumber == 12)
    {
        return OSIX_FAILURE;
    }
    if (gu4LsppUtTestNumber == 13)
    {
        return OSIX_FAILURE;
    }
    if (gu4LsppUtTestNumber == 14)
    {
        return OSIX_FAILURE;
    }
    if (gu4LsppUtTestNumber == 15)
    {
        if (pLsppExtInParams->u4RequestType == LSPP_CFA_GET_IF_ADDR)
        {
            return OSIX_FAILURE;
        }
        else
        {
            pLsppExtOutParams->OutTnlInfo.InSegInfo.u1LblAction =
                LSPP_MPLS_LBL_ACTION_POP;
            return OSIX_FAILURE;
        }
    }
    if (gu4LsppUtTestNumber == 52)
    {
        return OSIX_FAILURE;
    }
    else if (gu4LsppUtTestNumber == 53)
    {
        pLsppExtOutParams->OutPwDetail.u4InVcLabel = LSPP_INVALID_LABEL;
        return OSIX_SUCCESS;
    }
    else if (gu4LsppUtTestNumber == 54)
    {
        pLsppExtOutParams->OutPwDetail.u4InVcLabel = LSPP_LBL_ROUTER_ALERT;
        return OSIX_SUCCESS;
    }
    else if (gu4LsppUtTestNumber == 55)
    {
        pLsppExtOutParams->OutPwDetail.u4InVcLabel = LSPP_LBL_EXPLICIT_NULL;
        return OSIX_SUCCESS;
    }
    else if (gu4LsppUtTestNumber == 61)
    {
        return OSIX_FAILURE;
    }
    else if (gu4LsppUtTestNumber == 62)
    {
        pLsppExtOutParams->u1PathType = LSPP_MPLS_PATH_TYPE_LSP;
        return OSIX_SUCCESS;
    }
    else if (gu4LsppUtTestNumber == 63)
    {
        pLsppExtOutParams->u1PathType = LSPP_MPLS_PATH_TYPE_TNL;
        return OSIX_SUCCESS;
    }
    else if (gu4LsppUtTestNumber == 64)
    {
        pLsppExtOutParams->u1PathType = LSPP_MPLS_PATH_TYPE_PW;
        return OSIX_SUCCESS;
    }
    else if (gu4LsppUtTestNumber == 65)
    {
        pLsppExtOutParams->u1PathType = 100;
        return OSIX_SUCCESS;
    }
    else if (gu4LsppUtTestNumber == 92)
    {
        return OSIX_FAILURE;
    }
    else if (gu4LsppUtTestNumber == 93)
    {
        pLsppExtOutParams->OutLsppNodeId.MplsRouterId.u4_addr[0] = 4;
        return OSIX_SUCCESS;
    }
    else if (gu4LsppUtTestNumber == 94)
    {
        pLsppExtOutParams->OutLsppNodeId.MplsRouterId.u4_addr[0] = 0;
        return OSIX_SUCCESS;
    }
    else if (gu4LsppUtTestNumber == 95)
    {
        return OSIX_FAILURE;
    }
    else if (gu4LsppUtTestNumber == 121)
    {
        return OSIX_FAILURE;
    }
    else if (gu4LsppUtTestNumber == 122)
    {
        pLsppExtOutParams->unOutParams.u4IfIndex = 10;
        return OSIX_SUCCESS;
    }
    else if (gu4LsppUtTestNumber == 131)
    {
        return OSIX_FAILURE;
    }
    else if (gu4LsppUtTestNumber == 132)
    {
        pLsppExtOutParams->OutLsppNodeId.MplsRouterId.u4_addr[0] = 160;
        return OSIX_SUCCESS;
    }
    else if (gu4LsppUtTestNumber == 141)
    {
        return OSIX_FAILURE;
    }
    else if (gu4LsppUtTestNumber == 142)
    {
        pLsppExtOutParams->u1PathType = LSPP_MPLS_PATH_TYPE_TNL;
        pLsppExtOutParams->OutTnlInfo.TnlInfo.SrcNodeId.u4NodeType =
            LSPP_MPLS_ADDR_TYPE_IPV4;
        MEMSET (&(pLsppExtOutParams->OutTnlInfo), 0, sizeof (tMplsTnlLspId));
        return OSIX_SUCCESS;
    }
    else if (gu4LsppUtTestNumber == 143)
    {
        pLsppExtOutParams->u1PathType = LSPP_MPLS_PATH_TYPE_TNL;
        pLsppExtOutParams->OutTnlInfo.TnlInfo.SrcNodeId.u4NodeType =
            LSPP_MPLS_ADDR_TYPE_IPV6;
        MEMSET (&(pLsppExtOutParams->OutTnlInfo), 0, sizeof (tMplsTnlLspId));
        return OSIX_SUCCESS;
    }
    else if (gu4LsppUtTestNumber == 144)
    {
        pLsppExtOutParams->u1PathType = LSPP_MPLS_PATH_TYPE_TNL;
        pLsppExtOutParams->OutTnlInfo.TnlInfo.SrcNodeId.u4NodeType =
            LSPP_MPLS_ADDR_TYPE_GLOBAL_NODE_ID;
        MEMSET (&(pLsppExtOutParams->OutTnlInfo), 0, sizeof (tMplsTnlLspId));
        return OSIX_SUCCESS;
    }
    else if (gu4LsppUtTestNumber == 145)
    {
        pLsppExtOutParams->u1PathType = LSPP_MPLS_PATH_TYPE_LSP;
        pLsppExtOutParams->OutLdpInfo.LdpInfo.u4AddrType =
            LSPP_MPLS_ADDR_TYPE_IPV4;
        MEMSET (&(pLsppExtOutParams->OutLdpInfo), 0, sizeof (tLsppLdpInfo));
        return OSIX_SUCCESS;
    }
    else if (gu4LsppUtTestNumber == 146)
    {
        pLsppExtOutParams->u1PathType = LSPP_MPLS_PATH_TYPE_LSP;
        pLsppExtOutParams->OutLdpInfo.LdpInfo.u4AddrType =
            LSPP_PATH_TYPE_LDP_IPV6;
        MEMSET (&(pLsppExtOutParams->OutLdpInfo), 0, sizeof (tLsppLdpInfo));
        return OSIX_SUCCESS;
    }
    else if (gu4LsppUtTestNumber == 147)
    {
        pLsppExtOutParams->u1PathType = 100;
        pLsppExtOutParams->OutLdpInfo.LdpInfo.u4AddrType =
            LSPP_PATH_TYPE_LDP_IPV6;
        MEMSET (&(pLsppExtOutParams->OutLdpInfo), 0, sizeof (tLsppLdpInfo));
        return OSIX_SUCCESS;
    }
    else if (gu4LsppUtTestNumber == 151)
    {
        return OSIX_FAILURE;
    }
    else if (gu4LsppUtTestNumber == 152)
    {
        pLsppExtOutParams->unOutParams.u4Mtu = 1500;
        return OSIX_SUCCESS;
    }
    else if (gu4LsppUtTestNumber == 161)
    {
        return OSIX_FAILURE;
    }
    else if (gu4LsppUtTestNumber == 162)
    {
        MEMSET (&(pLsppExtOutParams->OutTnlInfo), 0,
                sizeof (sizeof (tMplsTnlLspId)));
        return OSIX_SUCCESS;
    }
    else if (gu4LsppUtTestNumber == 241)
    {
        return OSIX_FAILURE;
    }
    else if (gu4LsppUtTestNumber == 341)
    {
        return OSIX_FAILURE;
    }
    else if (gu4LsppUtTestNumber == 262)
    {
        return OSIX_FAILURE;
    }
    else if (gu4LsppUtTestNumber == 263)
    {
        return OSIX_SUCCESS;
    }
    else if (gu4LsppUtTestNumber == 271)
    {
        return OSIX_FAILURE;
    }
    else if (gu4LsppUtTestNumber == 281)
    {
        return OSIX_FAILURE;
    }
    else if (gu4LsppUtTestNumber == 412)
    {
        return OSIX_FAILURE;
    }
    else if (gu4LsppUtTestNumber == 413)
    {
        STRCPY (pLsppExtOutParams->unOutParams.au1ContextName, "default");
        return OSIX_SUCCESS;
    }
    else if (gu4LsppUtTestNumber == 414)
    {
        STRCPY (pLsppExtOutParams->unOutParams.au1ContextName, "default");
        return OSIX_SUCCESS;
    }
#if 0
    switch (pLsppExtInParams->u4RequestType)
    {
        case LSPP_CFA_GET_L3_IF:
            pLsppExtOutParams->unOutParams.u4IfIndex = 10;
            break;

        case LSPP_GET_PW_BASE_OID:
            MplsOutApiInfo.OutServiceOid.au4ServiceOidList[0] = 1;
            MplsOutApiInfo.OutServiceOid.au4ServiceOidList[1] = 1;
            MplsOutApiInfo.OutServiceOid.u2ServiceOidLen = 2;

            MEMCPY (&(pLsppExtOutParams->OutOid),
                    &(MplsOutApiInfo.OutServiceOid), sizeof (tLsppServiceOid));
            break;

        case LSPP_MPLSDB_GET_TNL_INFO:
            MplsOutApiInfo.OutTeTnlInfo.u4TnlType = 2;    /* KANI  - Need to get the proper MACRO */
            MplsOutApiInfo.OutTeTnlInfo.u1OperStatus = 1;
            MplsOutApiInfo.OutTeTnlInfo.u1TnlOwner = 1;    /* Need to fill the correct Macro */
            MplsOutApiInfo.OutTeTnlInfo.u1TnlMode = 2;
            if (gu4LsppUtTestNumber == 1001)
            {
                MplsOutApiInfo.OutTeTnlInfo.TnlLspId.SrcNodeId.u4NodeType =
                    MPLS_ADDR_TYPE_GLOBAL_NODE_ID;
                MplsOutApiInfo.OutTeTnlInfo.TnlLspId.SrcNodeId.MplsRouterId.
                    u4_addr[0] = 100;
                MplsOutApiInfo.OutTeTnlInfo.TnlLspId.DstNodeId.MplsRouterId.
                    u4_addr[0] = 200;

            }
            else
            {
                MplsOutApiInfo.OutTeTnlInfo.TnlLspId.SrcNodeId.u4NodeType =
                    MPLS_ADDR_TYPE_GLOBAL_NODE_ID;
                MplsOutApiInfo.OutTeTnlInfo.TnlLspId.SrcNodeId.MplsGlobalNodeId.
                    u4GlobalId = 100;
                MplsOutApiInfo.OutTeTnlInfo.TnlLspId.SrcNodeId.MplsGlobalNodeId.
                    u4NodeId = 10;
                MplsOutApiInfo.OutTeTnlInfo.TnlLspId.DstNodeId.u4NodeType =
                    MPLS_ADDR_TYPE_GLOBAL_NODE_ID;
                MplsOutApiInfo.OutTeTnlInfo.TnlLspId.DstNodeId.MplsGlobalNodeId.
                    u4GlobalId = 100;
                MplsOutApiInfo.OutTeTnlInfo.TnlLspId.DstNodeId.MplsGlobalNodeId.
                    u4NodeId = 20;
            }
            MplsOutApiInfo.OutTeTnlInfo.TnlLspId.u4SrcTnlNum = 1;
            MplsOutApiInfo.OutTeTnlInfo.TnlLspId.u4DstTnlNum = 1;
            MplsOutApiInfo.OutTeTnlInfo.TnlLspId.u4LspNum = 1;
            MplsOutApiInfo.OutTeTnlInfo.TnlLspId.u4SrcLocalMapNum = 10;
            MplsOutApiInfo.OutTeTnlInfo.TnlLspId.u4DstLocalMapNum = 20;

            MplsOutApiInfo.OutTeTnlInfo.ExtendedTnlId.u4_addr[0] = 10;
            MplsOutApiInfo.OutTeTnlInfo.ExtendedTnlId.u4_addr[1] = 0;
            MplsOutApiInfo.OutTeTnlInfo.ExtendedTnlId.u4_addr[2] = 0;
            MplsOutApiInfo.OutTeTnlInfo.ExtendedTnlId.u4_addr[3] = 10;

            MplsOutApiInfo.OutTeTnlInfo.au1NextHopMac[0] = 0;
            MplsOutApiInfo.OutTeTnlInfo.au1NextHopMac[1] = 1;
            MplsOutApiInfo.OutTeTnlInfo.au1NextHopMac[2] = 2;
            MplsOutApiInfo.OutTeTnlInfo.au1NextHopMac[3] = 3;
            MplsOutApiInfo.OutTeTnlInfo.au1NextHopMac[4] = 4;
            MplsOutApiInfo.OutTeTnlInfo.au1NextHopMac[5] = 6;

            MplsOutApiInfo.OutTeTnlInfo.MplsMegId.u4MegIndex = 1;
            MplsOutApiInfo.OutTeTnlInfo.MplsMegId.u4MeIndex = 1;
            MplsOutApiInfo.OutTeTnlInfo.MplsMegId.u4MpIndex = 1;

            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.FwdInSegInfo.LblInfo[0].
                u4Label = 1;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.FwdInSegInfo.LblInfo[0].
                u1Ttl = 1;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.FwdInSegInfo.LblInfo[0].
                u1Exp = 1;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.FwdInSegInfo.LblInfo[0].u1SI =
                1;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.FwdInSegInfo.LblInfo[0].
                u1Protocol = 1;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.FwdInSegInfo.u4InIf = 10;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.FwdInSegInfo.Direction = 1;    /* KANI  - Need to get the proper MACRO */
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.FwdInSegInfo.u2AddrFmly =
                MPLS_ADDR_TYPE_IPV4;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.FwdInSegInfo.u1LblAction = 11;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.FwdInSegInfo.u1LblStkCnt = 1;

            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.RevOutSegInfo.LblInfo[0].
                u4Label = 1;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.RevOutSegInfo.LblInfo[0].
                u1Ttl = 1;

            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.RevOutSegInfo.LblInfo[0].
                u1Exp = 1;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.RevOutSegInfo.LblInfo[0].
                u1SI = 1;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.RevOutSegInfo.LblInfo[0].
                u1Protocol = 1;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.RevOutSegInfo.NHAddr.
                u4_addr[0] = 1;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.RevOutSegInfo.NHAddr.
                u4_addr[1] = 2;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.RevOutSegInfo.NHAddr.
                u4_addr[2] = 3;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.RevOutSegInfo.NHAddr.
                u4_addr[3] = 4;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.RevOutSegInfo.u4OutIf = 10;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.RevOutSegInfo.Direction = 1;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.RevOutSegInfo.
                au1NextHopMac[0] = 0;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.RevOutSegInfo.
                au1NextHopMac[1] = 1;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.RevOutSegInfo.
                au1NextHopMac[2] = 2;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.RevOutSegInfo.
                au1NextHopMac[3] = 3;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.RevOutSegInfo.
                au1NextHopMac[4] = 4;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.RevOutSegInfo.
                au1NextHopMac[5] = 5;

            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.RevOutSegInfo.u1NHAddrType =
                MPLS_ADDR_TYPE_IPV4;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.RevOutSegInfo.u1LblStkCnt = 1;

            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.FwdOutSegInfo.LblInfo[0].
                u4Label = 1;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.FwdOutSegInfo.LblInfo[0].
                u1Ttl = 1;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.FwdOutSegInfo.LblInfo[0].
                u1Exp = 1;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.FwdOutSegInfo.LblInfo[0].
                u1SI = 1;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.FwdOutSegInfo.LblInfo[0].
                u1Protocol = 1;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.FwdOutSegInfo.NHAddr.
                u4_addr[0] = 1;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.FwdOutSegInfo.NHAddr.
                u4_addr[1] = 2;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.FwdOutSegInfo.NHAddr.
                u4_addr[2] = 3;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.FwdOutSegInfo.NHAddr.
                u4_addr[3] = 4;

            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.FwdOutSegInfo.u4OutIf = 10;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.FwdOutSegInfo.Direction = 1;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.FwdOutSegInfo.
                au1NextHopMac[0] = 0;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.FwdOutSegInfo.
                au1NextHopMac[1] = 1;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.FwdOutSegInfo.
                au1NextHopMac[2] = 2;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.FwdOutSegInfo.
                au1NextHopMac[3] = 3;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.FwdOutSegInfo.
                au1NextHopMac[4] = 4;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.FwdOutSegInfo.
                au1NextHopMac[5] = 6;

            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.FwdOutSegInfo.u1NHAddrType =
                MPLS_ADDR_TYPE_IPV4;
            MplsOutApiInfo.OutTeTnlInfo.XcApiInfo.FwdOutSegInfo.u1LblStkCnt = 1;
            LsppPortCopyTnlInfo (pLsppExtOutParams, &MplsOutApiInfo,
                                 u1CoRoutedLsp);
            break;

        case LSPP_L2VPN_GET_PW_OID:
            MplsOutApiInfo.OutServiceOid.au4ServiceOidList[0] = 1;
            MplsOutApiInfo.OutServiceOid.au4ServiceOidList[1] = 1;
            MplsOutApiInfo.OutServiceOid.au4ServiceOidList[2] =
                MplsInApiInfo.InPathId.PwId.u4VcId;
            MplsOutApiInfo.OutServiceOid.u2ServiceOidLen = 3;

            MEMCPY (&(pLsppExtOutParams->OutOid),
                    &(MplsOutApiInfo.OutServiceOid), sizeof (tLsppServiceOid));
            break;

        case LSPP_L2VPN_GET_PW_INFO_FRM_PW_INDEX:

            MplsOutApiInfo.u4ContextId = 0;
            MplsOutApiInfo.OutPathId.u4PathType = MPLS_PATH_TYPE_PW;
            MplsOutApiInfo.OutPathId.PwId.u4VcId =
                MplsInApiInfo.InPathId.PwId.u4PwIndex;
            MplsOutApiInfo.OutPathId.PwId.u4PwIndex =
                MplsInApiInfo.InPathId.PwId.u4PwIndex;
            MplsOutApiInfo.OutPathId.PwId.SrcNodeId.u4NodeType =
                MPLS_ADDR_TYPE_GLOBAL_NODE_ID;
            MplsOutApiInfo.OutPathId.PwId.SrcNodeId.MplsGlobalNodeId.
                u4GlobalId = 100;
            MplsOutApiInfo.OutPathId.PwId.SrcNodeId.MplsGlobalNodeId.u4NodeId =
                10;
            MplsOutApiInfo.OutPathId.PwId.u4SrcAcId = 1;
            MplsOutApiInfo.OutPathId.PwId.DstNodeId.u4NodeType =
                MPLS_ADDR_TYPE_GLOBAL_NODE_ID;
            MplsOutApiInfo.OutPathId.PwId.DstNodeId.MplsGlobalNodeId.
                u4GlobalId = 100;
            MplsOutApiInfo.OutPathId.PwId.DstNodeId.MplsGlobalNodeId.u4NodeId =
                20;
            MplsOutApiInfo.OutPathId.PwId.u4DstAcId = 1;
            MplsOutApiInfo.OutPathId.PwId.au1Agi[0] = 1;
            MplsOutApiInfo.OutPathId.PwId.u1AgiLen = 1;
            MplsOutApiInfo.OutPathId.PwId.u1AgiType = 2;    /* KANI - proper macro needs to be filled */
            MplsOutApiInfo.OutPwInfo.MplsPwPathId.u4VcId =
                MplsInApiInfo.InPathId.PwId.u4PwIndex;
            MplsOutApiInfo.OutPwInfo.MplsPwPathId.u4PwIndex = MplsInApiInfo.InPathId.PwId.u4PwIndex;    /* KANI - need to check with babu do we need to fill other information based on PW index */
            MplsOutApiInfo.OutPwInfo.MplsPwPathId.SrcNodeId.u4NodeType =
                MPLS_ADDR_TYPE_GLOBAL_NODE_ID;
            MplsOutApiInfo.OutPwInfo.MplsPwPathId.SrcNodeId.MplsGlobalNodeId.
                u4GlobalId = 100;
            MplsOutApiInfo.OutPwInfo.MplsPwPathId.SrcNodeId.MplsGlobalNodeId.
                u4NodeId = 10;
            MplsOutApiInfo.OutPwInfo.MplsPwPathId.u4SrcAcId = 1;
            MplsOutApiInfo.OutPwInfo.MplsPwPathId.DstNodeId.u4NodeType =
                MPLS_ADDR_TYPE_GLOBAL_NODE_ID;
            MplsOutApiInfo.OutPwInfo.MplsPwPathId.DstNodeId.MplsGlobalNodeId.
                u4GlobalId = 100;
            MplsOutApiInfo.OutPwInfo.MplsPwPathId.DstNodeId.MplsGlobalNodeId.
                u4NodeId = 20;
            MplsOutApiInfo.OutPwInfo.MplsPwPathId.u4DstAcId = 1;
            MplsOutApiInfo.OutPwInfo.MplsPwPathId.au1Agi[0] = 1;
            MplsOutApiInfo.OutPwInfo.MplsPwPathId.u1AgiLen = 1;
            MplsOutApiInfo.OutPwInfo.MplsPwPathId.u1AgiType = 2;

            MplsOutApiInfo.OutPathId.TnlId.SrcNodeId.u4NodeType =
                MPLS_ADDR_TYPE_GLOBAL_NODE_ID;
            MplsOutApiInfo.OutPathId.TnlId.SrcNodeId.MplsGlobalNodeId.
                u4GlobalId = 100;
            MplsOutApiInfo.OutPathId.TnlId.SrcNodeId.MplsGlobalNodeId.u4NodeId =
                10;
            MplsOutApiInfo.OutPathId.TnlId.u4SrcTnlNum = 1;
            MplsOutApiInfo.OutPathId.TnlId.DstNodeId.u4NodeType =
                MPLS_ADDR_TYPE_GLOBAL_NODE_ID;
            MplsOutApiInfo.OutPathId.TnlId.DstNodeId.MplsGlobalNodeId.
                u4GlobalId = 100;
            MplsOutApiInfo.OutPathId.TnlId.DstNodeId.MplsGlobalNodeId.u4NodeId =
                20;
            MplsOutApiInfo.OutPathId.TnlId.u4DstTnlNum = 1;
            MplsOutApiInfo.OutPathId.TnlId.u4LspNum = 1;
            MplsOutApiInfo.OutPathId.TnlId.u4SrcLocalMapNum = 10;
            MplsOutApiInfo.OutPathId.TnlId.u4DstLocalMapNum = 20;
            MplsOutApiInfo.OutPwInfo.MplsMegId.u4MegIndex = 1;
            MplsOutApiInfo.OutPwInfo.MplsMegId.u4MeIndex = 1;
            MplsOutApiInfo.OutPwInfo.MplsMegId.u4MpIndex = 2;
            MplsOutApiInfo.OutPwInfo.u4OutVcLabel = 2;
            MplsOutApiInfo.OutPwInfo.u4InVcLabel = 2;
            MplsOutApiInfo.OutPwInfo.u4LocalGroupID = 1;
            MplsOutApiInfo.OutPwInfo.u4RemoteGroupID = 2;
            MplsOutApiInfo.OutPwInfo.u4ProactiveSessIndex = 1;
            MplsOutApiInfo.OutPwInfo.u4PortVlan = 1;
            MplsOutApiInfo.OutPwInfo.i4PortIfIndex = 1;
            STRCPY (MplsOutApiInfo.OutPwInfo.au1PwVcName, "Vpn1");
            MplsOutApiInfo.OutPwInfo.u1CcSelected = 1;    /* KANI - what needs to be filled here */
            MplsOutApiInfo.OutPwInfo.u1CvSelected = LSPP_VCCV_CV_TYPE;    /* KANI - what needs to be filled here */
            MplsOutApiInfo.OutPwInfo.u1PwVcMode = 1;
            MplsOutApiInfo.OutPwInfo.u1MplsType = LSPP_MPLS_PATH_TYPE_TNL;    /* Proper Macro needs to be filled */
            MplsOutApiInfo.OutPwInfo.au1NextHopMac[0] = 0;
            MplsOutApiInfo.OutPwInfo.au1NextHopMac[1] = 1;
            MplsOutApiInfo.OutPwInfo.au1NextHopMac[2] = 2;
            MplsOutApiInfo.OutPwInfo.au1NextHopMac[3] = 3;
            MplsOutApiInfo.OutPwInfo.au1NextHopMac[4] = 4;
            MplsOutApiInfo.OutPwInfo.au1NextHopMac[5] = 5;

            MplsOutApiInfo.OutPwInfo.u1Ttl = 1;    /* KANI - need to check whether it is a correct value */
            MplsOutApiInfo.OutPwInfo.u1PwType = 1;    /* KANI - Need to fill the correct MACRO */
            MplsOutApiInfo.OutPwInfo.i1OperStatus = 1;    /* KANI - Need to fill the correct MACRO */
            MplsOutApiInfo.OutPwInfo.i1VlanMode = 1;
            MplsOutApiInfo.OutPwInfo.bOamEnable = TRUE;
            if (gu4LsppUtTestNumber == 1001)
            {
                MplsOutApiInfo.OutPwInfo.MplsPwPathId.u1PwVcOwner =
                    LSPP_PW_ID_FEC;
            }
            if (gu4LsppUtTestNumber == 1002)
            {
                MplsOutApiInfo.OutPwInfo.MplsPwPathId.u1PwVcOwner =
                    LSPP_GEN_FEC;
            }
            if (gu4LsppUtTestNumber == 1003)
            {
                MplsOutApiInfo.OutPwInfo.MplsPwPathId.u1PwVcOwner =
                    LSPP_GEN_FEC;
            }
            LsppPortCopyPwInfo (pLsppExtOutParams, &MplsOutApiInfo);

            break;

        case LSPP_L2VPN_GET_PW_INFO_FRM_VC_ID:

            MplsOutApiInfo.OutPwInfo.MplsPwPathId.u4VcId =
                MplsInApiInfo.InPathId.PwId.u4VcId;
            MplsOutApiInfo.OutPwInfo.MplsPwPathId.u4PwIndex = MplsInApiInfo.InPathId.PwId.u4PwIndex;    /* KANI - need to check with babu do we need to fill other information based on PW index */
            MplsOutApiInfo.OutPwInfo.MplsPwPathId.SrcNodeId.u4NodeType =
                MPLS_ADDR_TYPE_GLOBAL_NODE_ID;
            MplsOutApiInfo.OutPwInfo.MplsPwPathId.SrcNodeId.MplsGlobalNodeId.
                u4GlobalId = 100;
            MplsOutApiInfo.OutPwInfo.MplsPwPathId.SrcNodeId.MplsGlobalNodeId.
                u4NodeId = 10;
            MplsOutApiInfo.OutPwInfo.MplsPwPathId.u4SrcAcId = 1;
            MplsOutApiInfo.OutPwInfo.MplsPwPathId.DstNodeId.u4NodeType =
                MPLS_ADDR_TYPE_GLOBAL_NODE_ID;
            MplsOutApiInfo.OutPwInfo.MplsPwPathId.DstNodeId.MplsGlobalNodeId.
                u4GlobalId = 100;
            MplsOutApiInfo.OutPwInfo.MplsPwPathId.DstNodeId.MplsGlobalNodeId.
                u4NodeId = 20;
            MplsOutApiInfo.OutPwInfo.MplsPwPathId.u4DstAcId = 1;
            MplsOutApiInfo.OutPwInfo.MplsPwPathId.au1Agi[0] = 1;
            MplsOutApiInfo.OutPwInfo.MplsPwPathId.u1AgiLen = 1;
            MplsOutApiInfo.OutPwInfo.MplsPwPathId.u1AgiType = 2;
            MplsOutApiInfo.OutPwInfo.MplsPwPathId.u1LocalAIIType = 10;
            MplsOutApiInfo.OutPwInfo.MplsPwPathId.u1RemoteAIIType = 1;
            MplsOutApiInfo.OutPwInfo.MplsPwPathId.u1SaiiLen = 1;
            MplsOutApiInfo.OutPwInfo.MplsPwPathId.u1TaiiLen = 1;

            MplsOutApiInfo.OutPwInfo.MplsMegId.u4MegIndex = 1;
            MplsOutApiInfo.OutPwInfo.MplsMegId.u4MeIndex = 1;
            MplsOutApiInfo.OutPwInfo.MplsMegId.u4MpIndex = 2;
            MplsOutApiInfo.OutPwInfo.u4OutVcLabel = 2;
            MplsOutApiInfo.OutPwInfo.u4InVcLabel = 2;
            MplsOutApiInfo.OutPwInfo.u4LocalGroupID = 1;
            MplsOutApiInfo.OutPwInfo.u4RemoteGroupID = 2;
            MplsOutApiInfo.OutPwInfo.u4ProactiveSessIndex = 1;
            MplsOutApiInfo.OutPwInfo.u4PortVlan = 1;
            MplsOutApiInfo.OutPwInfo.i4PortIfIndex = 1;
            STRCPY (MplsOutApiInfo.OutPwInfo.au1PwVcName, "Vpn1");
            MplsOutApiInfo.OutPwInfo.u1CcSelected = 1;    /* KANI - what needs to be filled here */
            MplsOutApiInfo.OutPwInfo.u1CvSelected = LSPP_VCCV_CV_TYPE;    /* KANI - what needs to be filled here */
            MplsOutApiInfo.OutPwInfo.u1PwVcMode = 1;
            if (gu4LsppUtTestNumber == 1001)
            {
                MplsOutApiInfo.OutPwInfo.MplsPwPathId.u1PwVcOwner =
                    LSPP_PW_OWNER_PWIDFEC_SIGNALLING;
                MplsOutApiInfo.OutPwInfo.u1MplsType = LSPP_MPLS_PATH_TYPE_TNL;
                MplsOutApiInfo.OutPwInfo.MplsPwPathId.SrcNodeId.u4NodeType =
                    LSPP_MPLS_ADDR_TYPE_IPV4;
                MplsOutApiInfo.OutPwInfo.MplsPwPathId.SrcNodeId.MplsRouterId.
                    u4_addr[0] = 100;
                MplsOutApiInfo.OutPwInfo.MplsPwPathId.DstNodeId.MplsRouterId.
                    u4_addr[0] = 100;
            }
            if (gu4LsppUtTestNumber == 1002)
            {
                MplsOutApiInfo.OutPwInfo.MplsPwPathId.u1PwVcOwner =
                    LSPP_PW_OWNER_GENFEC_SIGNALLING;
                MplsOutApiInfo.OutPwInfo.u1MplsType = LSPP_MPLS_PATH_TYPE_TNL;
                MplsOutApiInfo.OutPwInfo.MplsPwPathId.SrcNodeId.u4NodeType =
                    LSPP_MPLS_ADDR_TYPE_IPV4;
                MplsOutApiInfo.OutPwInfo.MplsPwPathId.SrcNodeId.MplsRouterId.
                    u4_addr[0] = 100;
                MplsOutApiInfo.OutPwInfo.MplsPwPathId.DstNodeId.MplsRouterId.
                    u4_addr[0] = 100;
            }
            else if (gu4LsppUtTestNumber == 1003)
            {
                MplsOutApiInfo.OutPwInfo.MplsPwPathId.u1PwVcOwner =
                    LSPP_PW_OWNER_GENFEC_SIGNALLING;
                MplsOutApiInfo.OutPwInfo.u1MplsType = LSPP_MPLS_PATH_TYPE_LSP;
            }

            MplsOutApiInfo.OutPwInfo.au1NextHopMac[0] = 0;
            MplsOutApiInfo.OutPwInfo.au1NextHopMac[1] = 1;
            MplsOutApiInfo.OutPwInfo.au1NextHopMac[2] = 2;
            MplsOutApiInfo.OutPwInfo.au1NextHopMac[3] = 3;
            MplsOutApiInfo.OutPwInfo.au1NextHopMac[4] = 4;
            MplsOutApiInfo.OutPwInfo.au1NextHopMac[5] = 5;
            MplsOutApiInfo.OutPwInfo.u1Ttl = 1;
            MplsOutApiInfo.OutPwInfo.u1PwType = 1;
            MplsOutApiInfo.OutPwInfo.i1OperStatus = 1;
            MplsOutApiInfo.OutPwInfo.i1VlanMode = 1;
            MplsOutApiInfo.OutPwInfo.bOamEnable = TRUE;
            LsppPortCopyPwInfo (pLsppExtOutParams, &MplsOutApiInfo);    /* KANI_1 */
            break;

        default:
            return OSIX_FAILURE;

    }
#endif

    return OSIX_SUCCESS;
}
