/********************************************************************
 ** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 **
 ** $Id: lspptest.h,v 1.3 2010/11/23 04:53:24 siva Exp $
 **
 ** Description: LSPP  UT Test cases.
 ** NOTE: This file should not be included in release packaging.
 ** ***********************************************************************/

#ifndef _LSPPTEST_H_
#define _LSPPTEST_H_
#include "lsppinc.h"
#include "lsppcli.h"
#include "lsppclig.h"



#define LSPP_INGRESS              1
#define LSPP_TRANSIT              2
#define LSPP_EGRESS               3

#define LSPP_VALID_PKT            1
#define LSPP_INVALID_PKT          0

UINT1 gu1NodeType = 0;


VOID LsppExecuteUt(UINT4 u4FileNumber, UINT4 u4TestNumber);
VOID LsppExecuteUtAll (VOID);
VOID LsppExecuteUtFile (UINT4 u4File);

VOID LsppUtFillPathId (tLsppPathId *pLsppPathId, UINT1 u1PathType);
VOID LsppUtUdpateHeader(tLsppPduInfo *pLsppPduInfo);
VOID LsppUtUdpateTgtFec(tLsppPduInfo *pLsppPduInfo, UINT1 u1PktType);
VOID LsppUtUdpatePingTraceEntry(tLsppFsLsppPingTraceTableEntry 
                                *pLsppPingTraceEntry);
VOID LsppUtFrameHeader(UINT1 **pBuf, UINT1, UINT1 u1PktType);
VOID LsppUtFrameIpUdpHeader (UINT1 **pBuf, UINT1 u1PktType);


typedef struct
{
    UINT1     u1File;
    UINT1     u1Case;
}tTestCase;

tTestCase gTestCase[] = {
    {1,4},
    {2,13},
    {3,10},
    {4,51},
    {5,6},
    {6,10},
    {7,10},
    {8,3},
    {9,10},
    {10,2},
    {11,5},
    {12,10},
    {13,4},
    {14,3},
    {15,3},
    {16,34},
    {17,4},
    {18,1},
    {19,1},
    {20,5},
    {21,5},
    {22,5},
    {23,8},
    {24,5},
    {25,2},
    {26,10},
    {27,12},
    {28,32}

};



/* Routines for lsppapi.c */
INT4 LsppUtApi_1(VOID);

INT4 LsppUtApi_2(VOID);

INT4 LsppUtApi_3(VOID);

INT4 LsppUtApi_4(VOID);
/* Routines for lsppcli.c */
INT4 LsppUtCli_1(VOID);

INT4 LsppUtCli_2(VOID);

INT4 LsppUtCli_3(VOID);

INT4 LsppUtCli_4(VOID);

INT4 LsppUtCli_5(VOID);

INT4 LsppUtCli_6(VOID);

INT4 LsppUtCli_7(VOID);

INT4 LsppUtCli_8(VOID);

INT4 LsppUtCli_9(VOID);

INT4 LsppUtCli_10(VOID);

INT4 LsppUtCli_11(VOID);

INT4 LsppUtCli_12(VOID);

INT4 LsppUtCli_13(VOID);

INT4 LsppUtCli_14(VOID);

INT4 LsppUtCli_15(VOID);

INT4 LsppUtCli_16(VOID);

INT4 LsppUtCli_17(VOID);

INT4 LsppUtCli_18(VOID);

/* Routines for lsppclig.c */
INT4 LsppUtClig_1(VOID);

INT4 LsppUtClig_2(VOID);

/* Routines for lsppcore.c */
INT4 LsppUtCore_1(VOID);

INT4 LsppUtCore_2(VOID);

INT4 LsppUtCore_3(VOID);

INT4 LsppUtCore_4(VOID);

INT4 LsppUtCore_5(VOID);

INT4 LsppUtCore_6(VOID);

INT4 LsppUtCore_7(VOID);

INT4 LsppUtCore_8(VOID);

INT4 LsppUtCore_9(VOID);

INT4 LsppUtCore_10(VOID);

INT4 LsppUtCore_11(VOID);

INT4 LsppUtCore_12(VOID);

INT4 LsppUtCore_13(VOID);

INT4 LsppUtCore_14(VOID);

INT4 LsppUtCore_15(VOID);

INT4 LsppUtCore_16(VOID);

INT4 LsppUtCore_17(VOID);

INT4 LsppUtCore_18(VOID);

INT4 LsppUtCore_19(VOID);

INT4 LsppUtCore_20(VOID);

INT4 LsppUtCore_21(VOID);

INT4 LsppUtCore_22(VOID);

INT4 LsppUtCore_23(VOID);

INT4 LsppUtCore_24(VOID);

INT4 LsppUtCore_25(VOID);

INT4 LsppUtCore_26(VOID);

INT4 LsppUtCore_27(VOID);

INT4 LsppUtCore_28(VOID);

INT4 LsppUtCore_29(VOID);

INT4 LsppUtCore_30(VOID);

INT4 LsppUtCore_31(VOID);

INT4 LsppUtCore_32(VOID);

INT4 LsppUtCore_33(VOID);

INT4 LsppUtCore_34(VOID);

INT4 LsppUtCore_35(VOID);

INT4 LsppUtCore_36(VOID);

INT4 LsppUtCore_37(VOID);

INT4 LsppUtCore_38(VOID);

INT4 LsppUtCore_39(VOID);

INT4 LsppUtCore_40(VOID);

INT4 LsppUtCore_41(VOID);

INT4 LsppUtCore_42(VOID);

INT4 LsppUtCore_43(VOID);

INT4 LsppUtCore_44(VOID);

INT4 LsppUtCore_45(VOID);

INT4 LsppUtCore_46(VOID);

INT4 LsppUtCore_47(VOID);

INT4 LsppUtCore_48(VOID);

INT4 LsppUtCore_49(VOID);

INT4 LsppUtCore_50(VOID);

/* Routines for  lsppdb.c */
INT4 LsppUtDb_1(VOID);

INT4 LsppUtDb_2(VOID);

INT4 LsppUtDb_3 (VOID);

INT4 LsppUtDb_4 (VOID);

INT4 LsppUtDb_5 (VOID);

INT4 LsppUtDb_6 (VOID);


/* Routines for  lsppdbg.c */
INT4 LsppUtDbg_1(VOID);

INT4 LsppUtDbg_2(VOID);

INT4 LsppUtDbg_3(VOID);

INT4 LsppUtDbg_4(VOID);

INT4 LsppUtDbg_5(VOID);

INT4 LsppUtDbg_6(VOID);

INT4 LsppUtDbg_7(VOID);

/* Routines for  lsppdsg.c */
INT4 LsppUtDsg_1(VOID);

INT4 LsppUtDsg_2(VOID);

INT4 LsppUtDsg_3(VOID);

INT4 LsppUtDsg_4(VOID);

INT4 LsppUtDsg_5(VOID);

INT4 LsppUtDsg_6(VOID);

INT4 LsppUtDsg_7(VOID);

INT4 LsppUtDsg_8(VOID);

INT4 LsppUtDsg_9(VOID);

INT4 LsppUtDsg_10(VOID);

INT4 LsppUtDsg_11(VOID);

INT4 LsppUtDsg_12(VOID);

INT4 LsppUtDsg_13(VOID);

INT4 LsppUtDsg_14(VOID);

INT4 LsppUtDsg_15(VOID);

INT4 LsppUtDsg_16(VOID);

INT4 LsppUtDsg_17(VOID);

/* Routines for  lsppdval.c */
INT4 LsppUtDval_1(VOID);

INT4 LsppUtDval_2(VOID);

INT4 LsppUtDval_3(VOID);

/* Routines for  lsppdvalg.c */
INT4 LsppUtDvalg_1(VOID);

/* Routines for  lsppext.c */
INT4 LsppUtExt_1(VOID);

INT4 LsppUtExt_2(VOID);

/* Routines for  lspplw.c */
INT4 LsppUtLw_1(VOID);

INT4 LsppUtLw_2(VOID);

INT4 LsppUtLw_3(VOID);

INT4 LsppUtLw_4(VOID);

INT4 LsppUtLw_5(VOID);

INT4 LsppUtLw_6(VOID);

/* Routines for  lspplwg.c */
INT4 LsppUtLwg_1(VOID);

INT4 LsppUtLwg_2(VOID);

INT4 LsppUtLwg_3(VOID);

INT4 LsppUtLwg_4(VOID);

INT4 LsppUtLwg_5(VOID);

INT4 LsppUtLwg_6(VOID);

INT4 LsppUtLwg_7(VOID);

INT4 LsppUtLwg_8(VOID);

INT4 LsppUtLwg_9(VOID);

INT4 LsppUtLwg_10(VOID);

INT4 LsppUtLwg_11(VOID);

INT4 LsppUtLwg_12(VOID);

INT4 LsppUtLwg_13(VOID);

INT4 LsppUtLwg_14(VOID);

INT4 LsppUtLwg_15(VOID);

INT4 LsppUtLwg_16(VOID);

INT4 LsppUtLwg_17(VOID);

INT4 LsppUtLwg_18(VOID);

INT4 LsppUtLwg_19(VOID);

INT4 LsppUtLwg_20(VOID);

INT4 LsppUtLwg_21(VOID);

INT4 LsppUtLwg_22(VOID);

INT4 LsppUtLwg_23(VOID);

INT4 LsppUtLwg_24(VOID);

INT4 LsppUtLwg_25(VOID);

INT4 LsppUtLwg_26(VOID);

INT4 LsppUtLwg_27(VOID);

INT4 LsppUtLwg_28(VOID);

INT4 LsppUtLwg_29(VOID);


INT4 LsppUtLwg_30(VOID);

INT4 LsppUtLwg_31(VOID);

INT4 LsppUtLwg_32(VOID);

INT4 LsppUtLwg_33(VOID);

INT4 LsppUtLwg_34(VOID);

INT4 LsppUtLwg_35(VOID);

INT4 LsppUtLwg_36(VOID);

INT4 LsppUtLwg_37(VOID);

INT4 LsppUtLwg_38(VOID);

INT4 LsppUtLwg_39(VOID);

INT4 LsppUtLwg_40(VOID);

INT4 LsppUtLwg_41(VOID);

INT4 LsppUtLwg_42(VOID);

INT4 LsppUtLwg_43(VOID);

INT4 LsppUtLwg_44(VOID);

INT4 LsppUtLwg_45(VOID);

INT4 LsppUtLwg_46(VOID);

INT4 LsppUtLwg_47(VOID);

INT4 LsppUtLwg_48(VOID);

INT4 LsppUtLwg_49(VOID);

INT4 LsppUtLwg_50(VOID);

INT4 LsppUtLwg_51(VOID);

INT4 LsppUtLwg_52(VOID);

INT4 LsppUtLwg_53(VOID);

INT4 LsppUtLwg_54(VOID);

INT4 LsppUtLwg_55(VOID);

INT4 LsppUtLwg_56(VOID);

INT4 LsppUtLwg_57(VOID);

INT4 LsppUtLwg_58(VOID);

INT4 LsppUtLwg_59(VOID);

INT4 LsppUtLwg_60(VOID);

INT4 LsppUtLwg_61(VOID);

INT4 LsppUtLwg_62(VOID);

INT4 LsppUtLwg_63(VOID);

INT4 LsppUtLwg_64(VOID);

INT4 LsppUtLwg_65(VOID);

INT4 LsppUtLwg_66(VOID);

INT4 LsppUtLwg_67(VOID);

INT4 LsppUtLwg_68(VOID);

INT4 LsppUtLwg_69(VOID);

INT4 LsppUtLwg_70(VOID);

INT4 LsppUtLwg_71(VOID);

INT4 LsppUtLwg_72(VOID);

INT4 LsppUtLwg_73(VOID);

INT4 LsppUtLwg_74(VOID);

INT4 LsppUtLwg_75(VOID);

INT4 LsppUtLwg_76(VOID);

INT4 LsppUtLwg_77(VOID);

INT4 LsppUtLwg_78(VOID);

INT4 LsppUtLwg_79(VOID);

INT4 LsppUtLwg_80(VOID);

INT4 LsppUtLwg_81(VOID);

INT4 LsppUtLwg_82(VOID);

INT4 LsppUtLwg_83(VOID);

INT4 LsppUtLwg_84(VOID);

INT4 LsppUtLwg_85(VOID);

INT4 LsppUtLwg_86(VOID);

INT4 LsppUtLwg_87(VOID);

INT4 LsppUtLwg_88(VOID);

INT4 LsppUtLwg_89(VOID);

INT4 LsppUtLwg_90(VOID);

INT4 LsppUtLwg_91(VOID);

INT4 LsppUtLwg_92(VOID);

INT4 LsppUtLwg_93(VOID);

INT4 LsppUtLwg_94(VOID);

INT4 LsppUtLwg_95(VOID);

INT4 LsppUtLwg_96(VOID);

INT4 LsppUtLwg_97(VOID);

INT4 LsppUtLwg_98(VOID);

INT4 LsppUtLwg_99(VOID);

INT4 LsppUtLwg_100(VOID);

INT4 LsppUtLwg_101(VOID);

INT4 LsppUtLwg_102(VOID);

INT4 LsppUtLwg_103(VOID);

INT4 LsppUtLwg_104(VOID);

INT4 LsppUtLwg_105(VOID);

INT4 LsppUtLwg_106(VOID);

INT4 LsppUtLwg_107(VOID);

INT4 LsppUtLwg_108(VOID);

INT4 LsppUtLwg_109(VOID);

INT4 LsppUtLwg_110(VOID);

INT4 LsppUtLwg_111(VOID);

INT4 LsppUtLwg_112(VOID);

INT4 LsppUtLwg_113(VOID);

INT4 LsppUtLwg_114(VOID);

INT4 LsppUtLwg_115(VOID);

INT4 LsppUtLwg_116(VOID);

INT4 LsppUtLwg_117(VOID);

INT4 LsppUtLwg_118(VOID);

INT4 LsppUtLwg_119(VOID);

INT4 LsppUtLwg_120(VOID);

INT4 LsppUtLwg_121(VOID);

INT4 LsppUtLwg_122(VOID);

INT4 LsppUtLwg_123(VOID);

INT4 LsppUtLwg_124(VOID);

INT4 LsppUtLwg_125(VOID);

INT4 LsppUtLwg_126(VOID);

INT4 LsppUtLwg_127(VOID);

INT4 LsppUtLwg_128(VOID);

INT4 LsppUtLwg_129(VOID);

INT4 LsppUtLwg_130(VOID);

INT4 LsppUtLwg_131(VOID);

INT4 LsppUtLwg_132(VOID);

INT4 LsppUtLwg_133(VOID);

INT4 LsppUtLwg_134(VOID);


INT4 LsppUtLwg_135(VOID);

INT4 LsppUtLwg_136(VOID);

INT4 LsppUtLwg_137(VOID);

INT4 LsppUtLwg_138(VOID);

INT4 LsppUtLwg_139(VOID);

INT4 LsppUtLwg_140(VOID);

INT4 LsppUtLwg_141(VOID);

INT4 LsppUtLwg_142(VOID);

INT4 LsppUtLwg_143(VOID);

INT4 LsppUtLwg_144(VOID);

INT4 LsppUtLwg_145(VOID);

INT4 LsppUtLwg_146(VOID);

INT4 LsppUtLwg_147(VOID);

INT4 LsppUtLwg_148(VOID);

INT4 LsppUtLwg_149(VOID);

INT4 LsppUtLwg_150(VOID);

INT4 LsppUtLwg_151(VOID);

INT4 LsppUtLwg_152(VOID);
INT4 LsppUtLwg_153(VOID);

INT4 LsppUtLwg_154(VOID);

INT4 LsppUtLwg_155(VOID);

INT4 LsppUtLwg_156(VOID);

INT4 LsppUtLwg_157(VOID);

INT4 LsppUtLwg_158(VOID);

INT4 LsppUtLwg_159(VOID);

INT4 LsppUtLwg_160(VOID);

INT4 LsppUtLwg_161(VOID);

INT4 LsppUtLwg_162(VOID);

INT4 LsppUtLwg_163(VOID);

INT4 LsppUtLwg_164(VOID);

INT4 LsppUtLwg_165(VOID);

INT4 LsppUtLwg_166(VOID);

INT4 LsppUtLwg_167(VOID);

INT4 LsppUtLwg_168(VOID);

INT4 LsppUtLwg_169(VOID);

INT4 LsppUtLwg_170(VOID);

INT4 LsppUtLwg_171(VOID);

INT4 LsppUtLwg_172(VOID);

INT4 LsppUtLwg_173(VOID);

INT4 LsppUtLwg_174(VOID);

INT4 LsppUtLwg_175(VOID);

INT4 LsppUtLwg_176(VOID);

INT4 LsppUtLwg_177(VOID);

INT4 LsppUtLwg_178(VOID);

INT4 LsppUtLwg_179(VOID);

INT4 LsppUtLwg_180(VOID);

INT4 LsppUtLwg_181(VOID);

INT4 LsppUtLwg_182(VOID);

INT4 LsppUtLwg_183(VOID);

INT4 LsppUtLwg_184(VOID);

INT4 LsppUtLwg_185(VOID);

INT4 LsppUtLwg_186(VOID);

INT4 LsppUtLwg_187(VOID);

INT4 LsppUtLwg_188(VOID);

INT4 LsppUtLwg_189(VOID);

INT4 LsppUtLwg_190(VOID);

INT4 LsppUtLwg_191(VOID);

INT4 LsppUtLwg_192(VOID);

INT4 LsppUtLwg_193(VOID);

INT4 LsppUtLwg_194(VOID);

INT4 LsppUtLwg_195(VOID);

INT4 LsppUtLwg_196(VOID);

INT4 LsppUtLwg_197(VOID);

INT4 LsppUtLwg_198(VOID);

INT4 LsppUtLwg_199(VOID);

INT4 LsppUtLwg_200(VOID);

/* Routines for lsppmain.c */
INT4 LsppUtMain_1(VOID);

INT4 LsppUtMain_2(VOID);

INT4 LsppUtMain_3(VOID);

INT4 LsppUtMain_4(VOID);

INT4 LsppUtMain_5(VOID);

INT4 LsppUtMain_6(VOID);

INT4 LsppUtMain_7(VOID);

INT4 LsppUtMain_8(VOID);

/* Routines for  lsppport.c */
INT4 LsppUtPort_1(VOID);

INT4 LsppUtPort_2(VOID);

INT4 LsppUtPort_3(VOID);

INT4 LsppUtPort_4(VOID);

INT4 LsppUtPort_5(VOID);

INT4 LsppUtPort_6(VOID);

INT4 LsppUtPort_7(VOID);

INT4 LsppUtPort_8(VOID);

INT4 LsppUtPort_9(VOID);

/* Routines for  lsppque.c */
INT4 LsppUtQue_1(VOID);

INT4 LsppUtQue_2(VOID);

INT4 LsppUtQue_3(VOID);

/* Routines for  lspprx.c */
INT4 LsppUtRx_1(VOID);

INT4 LsppUtRx_2(VOID);

INT4 LsppUtRx_3(VOID);

INT4 LsppUtRx_4(VOID);

INT4 LsppUtRx_5(VOID);

INT4 LsppUtRx_6(VOID);

INT4 LsppUtRx_7(VOID);

INT4 LsppUtRx_8(VOID);

INT4 LsppUtRx_9(VOID);

INT4 LsppUtRx_10(VOID);

INT4 LsppUtRx_11(VOID);

INT4 LsppUtRx_12(VOID);

INT4 LsppUtRx_13(VOID);

INT4 LsppUtRx_14(VOID);

INT4 LsppUtRx_15(VOID);

INT4 LsppUtRx_16(VOID);

INT4 LsppUtRx_17(VOID);

INT4 LsppUtRx_18(VOID);

INT4 LsppUtRx_19(VOID);

INT4 LsppUtRx_20(VOID);

INT4 LsppUtRx_21(VOID);

INT4 LsppUtRx_22(VOID);

INT4 LsppUtRx_23(VOID);

INT4 LsppUtRx_24(VOID);

INT4 LsppUtRx_25(VOID);

INT4 LsppUtRx_26(VOID);

INT4 LsppUtRx_27(VOID);

INT4 LsppUtRx_28(VOID);

INT4 LsppUtRx_29(VOID);

INT4 LsppUtRx_30(VOID);

INT4 LsppUtRx_31(VOID);

INT4 LsppUtRx_32(VOID);

INT4 LsppUtRx_33(VOID);

INT4 LsppUtRx_34(VOID);

INT4 LsppUtRx_35(VOID);

INT4 LsppUtRx_36(VOID);

INT4 LsppUtRx_37(VOID);

INT4 LsppUtRx_38(VOID);

INT4 LsppUtRx_39(VOID);

INT4 LsppUtRx_40(VOID);

INT4 LsppUtRx_41(VOID);

INT4 LsppUtRx_42(VOID);

INT4 LsppUtRx_43(VOID);

INT4 LsppUtRx_44(VOID);

INT4 LsppUtRx_45(VOID);

INT4 LsppUtRx_46(VOID);

INT4 LsppUtRx_47(VOID);

INT4 LsppUtRx_48(VOID);

INT4 LsppUtRx_49(VOID);

INT4 LsppUtRx_50(VOID);

INT4 LsppUtRx_51(VOID);

INT4 LsppUtRx_52(VOID);

/* Routines for  lsppsock.c */
INT4 LsppUtSock_1(VOID);

INT4 LsppUtSock_2(VOID);

INT4 LsppUtSock_3(VOID);

INT4 LsppUtSock_4(VOID);

INT4 LsppUtSock_5(VOID);

INT4 LsppUtSock_6(VOID);

/* Routines for  lsppsz.c */
INT4 LsppUtSz_1(VOID);

INT4 LsppUtSz_2(VOID);

INT4 LsppUtSz_3(VOID);

INT4 LsppUtSz_4(VOID);

/* Routines for  lspptask.c */
INT4 LsppUtTask_1(VOID);

/* Routines for  lspptmr.c */
INT4 LsppUtTmr_1(VOID);

INT4 LsppUtTmr_2(VOID);

INT4 LsppUtTmr_3(VOID);

INT4 LsppUtTmr_4(VOID);

INT4 LsppUtTmr_5(VOID);

INT4 LsppUtTmr_6(VOID);

INT4 LsppUtTmr_7(VOID);

INT4 LsppUtTmr_8(VOID);

INT4 LsppUtTmr_9(VOID);

/* Routines for  lspptrap.c */
INT4 LsppUtTrap_1(VOID);

INT4 LsppUtTrap_2(VOID);

INT4 LsppUtTrap_3(VOID);

INT4 LsppUtTrap_4(VOID);

INT4 LsppUtTrap_5(VOID);

/* Routines for  lspptrc.c */
INT4 LsppUtTrc_1(VOID);

INT4 LsppUtTrc_2(VOID);

INT4 LsppUtTrc_3(VOID);

INT4 LsppUtTrc_4(VOID);

INT4 LsppUtTrc_5(VOID);

/* Routines for  lspptx.c */
INT4 LsppUtTx_1(VOID);

INT4 LsppUtTx_2(VOID);

INT4 LsppUtTx_3(VOID);

INT4 LsppUtTx_4(VOID);

INT4 LsppUtTx_5(VOID);

INT4 LsppUtTx_6(VOID);

INT4 LsppUtTx_7(VOID);

INT4 LsppUtTx_8(VOID);

INT4 LsppUtTx_9(VOID);

INT4 LsppUtTx_10(VOID);

INT4 LsppUtTx_11(VOID);

INT4 LsppUtTx_12(VOID);

INT4 LsppUtTx_13(VOID);

INT4 LsppUtTx_14(VOID);

INT4 LsppUtTx_15(VOID);

INT4 LsppUtTx_16(VOID);

INT4 LsppUtTx_17(VOID);

INT4 LsppUtTx_18(VOID);

INT4 LsppUtTx_19(VOID);

INT4 LsppUtTx_20(VOID);

INT4 LsppUtTx_21(VOID);

INT4 LsppUtTx_22(VOID);

INT4 LsppUtTx_23(VOID);

INT4 LsppUtTx_24(VOID);

INT4 LsppUtTx_25(VOID);

INT4 LsppUtTx_26(VOID);

INT4 LsppUtTx_27(VOID);

INT4 LsppUtTx_28(VOID);

INT4 LsppUtTx_29(VOID);

INT4 LsppUtTx_30(VOID);

INT4 LsppUtTx_31(VOID);

INT4 LsppUtTx_32(VOID);

INT4 LsppUtTx_33(VOID);

INT4 LsppUtTx_34(VOID);

INT4 LsppUtTx_35(VOID);

/* Routines for  lspputil.c */
INT4 LsppUtUtil_1(VOID);

INT4 LsppUtUtil_2(VOID);

INT4 LsppUtUtil_3(VOID);

INT4 LsppUtUtil_4(VOID);

INT4 LsppUtUtil_5(VOID);

INT4 LsppUtUtil_6(VOID);

INT4 LsppUtUtil_7(VOID);

INT4 LsppUtUtil_8(VOID);

/* Routines for  lspputl.c */
INT4 LsppUtUtl_1(VOID);

INT4 LsppUtUtl_2(VOID);

INT4 LsppUtUtl_3(VOID);

INT4 LsppUtUtl_4(VOID);

INT4 LsppUtUtl_5(VOID);

INT4 LsppUtUtl_6(VOID);

INT4 LsppUtUtl_7(VOID);

INT4 LsppUtUtl_8(VOID);

/* Routines for  lspputlg.c */
INT4 LsppUtUtlg_1(VOID);

INT4 LsppUtUtlg_2(VOID);

INT4 LsppUtUtlg_3(VOID);

INT4 LsppUtUtlg_4(VOID);

INT4 LsppUtUtlg_5(VOID);

INT4 LsppUtUtlg_6(VOID);

INT4 LsppUtUtlg_7(VOID);

INT4 LsppUtUtlg_8(VOID);

INT4 LsppUtUtlg_9(VOID);

INT4 LsppUtUtlg_10(VOID);

INT4 LsppUtUtlg_11(VOID);

INT4 LsppUtUtlg_12(VOID);

INT4 LsppUtUtlg_13(VOID);

INT4 LsppUtUtlg_14(VOID);

INT4 LsppUtUtlg_15(VOID);

INT4 LsppUtUtlg_16(VOID);

INT4 LsppUtUtlg_17(VOID);

INT4 LsppUtUtlg_18(VOID);

INT4 LsppUtUtlg_19(VOID);

INT4 LsppUtUtlg_20(VOID);

INT4 LsppUtUtlg_21(VOID);


/* Routines for lsppcore.c */
INT4 LsppUtCore_1(VOID);

INT4 LsppUtCore_2(VOID);

INT4 LsppUtCore_3(VOID);

INT4 LsppUtCore_4(VOID);

INT4 LsppUtCore_5(VOID);

INT4 LsppUtCore_6(VOID);

INT4 LsppUtCore_7(VOID);

INT4 LsppUtCore_8(VOID);

INT4 LsppUtCore_9(VOID);

INT4 LsppUtCore_10(VOID);

INT4 LsppUtCore_11(VOID);

INT4 LsppUtCore_12(VOID);

INT4 LsppUtCore_13(VOID);

INT4 LsppUtCore_14(VOID);

INT4 LsppUtCore_15(VOID);

INT4 LsppUtCore_16(VOID);

INT4 LsppUtCore_17(VOID);

INT4 LsppUtCore_18(VOID);

INT4 LsppUtCore_19(VOID);

INT4 LsppUtCore_20(VOID);

INT4 LsppUtCore_21(VOID);

INT4 LsppUtCore_22(VOID);

INT4 LsppUtCore_23(VOID);

INT4 LsppUtCore_24(VOID);

INT4 LsppUtCore_25(VOID);

INT4 LsppUtCore_26(VOID);

INT4 LsppUtCore_27(VOID);

INT4 LsppUtCore_28(VOID);

INT4 LsppUtCore_29(VOID);

INT4 LsppUtCore_30(VOID);

INT4 LsppUtCore_31(VOID);

INT4 LsppUtCore_32(VOID);

INT4 LsppUtCore_33(VOID);

INT4 LsppUtCore_34(VOID);

INT4 LsppUtCore_35(VOID);

INT4 LsppUtCore_36(VOID);

INT4 LsppUtCore_37(VOID);

INT4 LsppUtCore_38(VOID);

INT4 LsppUtCore_39(VOID);

INT4 LsppUtCore_40(VOID);

INT4 LsppUtCore_41(VOID);

INT4 LsppUtCore_42(VOID);

INT4 LsppUtCore_43(VOID);

INT4 LsppUtCore_44(VOID);

INT4 LsppUtCore_45(VOID);

INT4 LsppUtCore_46(VOID);

INT4 LsppUtCore_47(VOID);

INT4 LsppUtCore_48(VOID);

INT4 LsppUtCore_49(VOID);

INT4 LsppUtCore_50(VOID);

INT4 LsppUtCxt_1(VOID);
INT4 LsppUtCxt_2(VOID);
INT4 LsppUtCxt_3(VOID);
INT4 LsppUtCxt_4(VOID);
INT4 LsppUtCxt_5(VOID);
INT4 LsppUtCxt_6(VOID);
INT4 LsppUtCxt_7(VOID);
INT4 LsppUtCxt_8(VOID);
INT4 LsppUtCxt_9(VOID);
INT4 LsppUtCxt_10(VOID);
INT4 LsppUtCxt_11(VOID);
INT4 LsppUtCxt_12(VOID);
INT4 LsppUtCxt_13(VOID);

INT4 LsppUtPw_1(VOID);
INT4 LsppUtPw_2(VOID);
INT4 LsppUtPw_3(VOID);
INT4 LsppUtPw_4(VOID);
INT4 LsppUtPw_5(VOID);
INT4 LsppUtPw_6(VOID);
INT4 LsppUtPw_7(VOID);
INT4 LsppUtPw_8(VOID);
INT4 LsppUtPw_9(VOID);
INT4 LsppUtPw_10(VOID);
INT4 LsppUtPw_11(VOID);
INT4 LsppUtPw_12(VOID);
INT4 LsppUtPw_13(VOID);
INT4 LsppUtPw_14(VOID);
INT4 LsppUtPw_15(VOID);
INT4 LsppUtPw_16(VOID);
INT4 LsppUtPw_17(VOID);
INT4 LsppUtPw_18(VOID);
INT4 LsppUtPw_19(VOID);
INT4 LsppUtPw_20(VOID);
INT4 LsppUtPw_21(VOID);
INT4 LsppUtPw_22(VOID);
INT4 LsppUtPw_23(VOID);
INT4 LsppUtPw_24(VOID);
INT4 LsppUtPw_25(VOID);
INT4 LsppUtPw_26(VOID);
INT4 LsppUtPw_27(VOID);
INT4 LsppUtPw_28(VOID);
INT4 LsppUtPw_29(VOID);
INT4 LsppUtPw_30(VOID);
INT4 LsppUtPw_31(VOID);
INT4 LsppUtPw_32(VOID);
INT4 LsppUtPw_33(VOID);
#endif /* _LSPPTEST_H_ */ 
