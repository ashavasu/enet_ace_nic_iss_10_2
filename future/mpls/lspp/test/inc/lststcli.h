
/********************************************************************
 ** Copyright (C) 2010 Aricent Inc . All Rights Reserved
 **
 ** $Id: lststcli.h,v 1.1 2010/10/28 19:59:42 prabuc Exp $
 **
 ** Description: LSPP  UT Test cases.
 ** NOTE: This file should not be included in release packaging.
 ** ***********************************************************************/

#include "lr.h"
#include "cli.h"

INT4 cli_process_lspp_test_cmd (tCliHandle CliHandle, UINT4 u4Command,...);

INT4
LsppUt(INT4 u4GlobalId );


