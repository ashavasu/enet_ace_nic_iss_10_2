##########################################################################
# Copyright (C) Future Software Limited,2010			                 #
#                                                                        #
# $Id: make.h,v 1.1 2010/10/28 19:59:21 prabuc Exp $                                                                 #
#								                                         #
# Description : Contains information fro creating the make file          #
#		for this module      				                             #
#								                                         #
##########################################################################

#include the make.h and make.rule from LR
include ../../../LR/make.h
include ../../../LR/make.rule

#Compilation switches
COMP_SWITCHES = $(GENERAL_COMPILATION_SWITCHES)\
                $(SYSTEM_COMPILATION_SWITCHES)

#project directories
LSPP_INCL_DIR           = $(BASE_DIR)/mpls/lspp/inc
MPLS_UTIL_DIR           = $(BASE_DIR)/util/mpls
PROJECT_SOURCE_DIR      = ${PROJECT_BASE_DIR}/src
PROJECT_TEST_DIR        = ${PROJECT_BASE_DIR}/test
PROJECT_INCLUDE_DIR     = ${PROJECT_BASE_DIR}/inc
PROJECT_OBJECT_DIR      = ${PROJECT_BASE_DIR}/obj

LSPP_TEST_BASE_DIR  = ${BASE_DIR}/mpls/lspp/test
LSPP_TEST_SRC_DIR   = ${LSPP_TEST_BASE_DIR}/src
LSPP_TEST_INC_DIR   = ${LSPP_TEST_BASE_DIR}/inc
LSPP_TEST_OBJ_DIR   = ${LSPP_TEST_BASE_DIR}/obj

LSPP_TEST_INCLUDES  = -I$(LSPP_TEST_INC_DIR) \
                     -I$(PROJECT_INCLUDE_DIR) \
                     -I$(LSPP_INCL_DIR) \
                     -I$(MPLS_UTIL_DIR) \
                     $(COMMON_INCLUDE_DIRS)


##########################################################################
#                    End of file make.h					 #
##########################################################################
