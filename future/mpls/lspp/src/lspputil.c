/******************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lspputil.c,v 1.28 2014/12/12 11:56:49 siva Exp $
 *
 * Description : This file contains the utility 
 *               functions of the LSPP module
 *****************************************************************************/

#include "lsppinc.h"

/******************************************************************************
 * Function   : LsppUtilTestBaseOid
 *
 * Description: This function is to validate whether the OID given as input 
 *              is a valid one by comparing it with the corresponding tables's
 *              Base OID
 *
 * Input      : pLsppPingTraceEntry - PingTrace table with input OID
 *
 * Output     : None
 * Returns    : OSIX_SUCCESS/OSIX_FAILURE
 *     
 *****************************************************************************/
INT4
LsppUtilTestBaseOid (tLsppFsLsppPingTraceTableEntry * pLsppPingTraceEntry,
                     INT4 i4RowCreateOption)
{
    tLsppFsLsppPingTraceTableEntry *pLsppPingTraceEntryInput = NULL;
    tLsppExtInParams   *pLsppExtInParams = NULL;
    tLsppExtOutParams  *pLsppExtOutParams = NULL;
    UINT4               u4PathType = 0;

    if ((pLsppExtInParams = (tLsppExtInParams *)
         MemAllocMemBlk (LSPP_FSLSPPINPUTPARAMS_POOLID)) == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((pLsppExtOutParams = (tLsppExtOutParams *)
         MemAllocMemBlk (LSPP_FSLSPPOUTPUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        return OSIX_FAILURE;
    }
    if ((pLsppPingTraceEntryInput = (tLsppFsLsppPingTraceTableEntry *)
         MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);
        return OSIX_FAILURE;
    }

    MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
    MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));
    MEMSET (pLsppPingTraceEntryInput, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    if (i4RowCreateOption == OSIX_TRUE)
    {
        u4PathType = (UINT4) pLsppPingTraceEntry->MibObject.i4FsLsppPathType;
    }
    else
    {
        pLsppPingTraceEntryInput->MibObject.u4FsLsppContextId =
            pLsppPingTraceEntry->MibObject.u4FsLsppContextId;

        pLsppPingTraceEntryInput->MibObject.u4FsLsppSenderHandle =
            pLsppPingTraceEntry->MibObject.u4FsLsppSenderHandle;

        if (LsppGetAllFsLsppPingTraceTable (pLsppPingTraceEntryInput) !=
            OSIX_SUCCESS)
        {
            MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtInParams);
            MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtOutParams);
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceEntryInput);
            return OSIX_FAILURE;
        }

        u4PathType =
            (UINT4) pLsppPingTraceEntryInput->MibObject.i4FsLsppPathType;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppPingTraceEntryInput);

    pLsppExtInParams->u4ContextId =
        pLsppPingTraceEntry->MibObject.u4FsLsppContextId;

    switch (u4PathType)
    {
        case LSPP_PATH_TYPE_LDP_IPV4:
        case LSPP_PATH_TYPE_LDP_IPV6:

            pLsppExtInParams->u4RequestType = LSPP_GET_FTN_BASE_OID;
            break;

        case LSPP_PATH_TYPE_RSVP_IPV4:
        case LSPP_PATH_TYPE_RSVP_IPV6:

            pLsppExtInParams->u4RequestType = LSPP_GET_TNL_BASE_OID;
            break;

        case LSPP_PATH_TYPE_FEC_128:
        case LSPP_PATH_TYPE_FEC_129:

            pLsppExtInParams->u4RequestType = LSPP_GET_PW_BASE_OID;
            break;

        case LSPP_PATH_TYPE_MEP:

            pLsppExtInParams->u4RequestType = LSPP_GET_MEG_BASE_OID;
            break;

        default:
            LSPP_LOG (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
                      LSPP_INVALID_PATH_TYPE_TO_FETCH_OID,
                      "LsppUtilTestBaseOid");
            MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtInParams);
            MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtOutParams);
            return OSIX_FAILURE;
    }

    if (LsppPortHandleExtInteraction (pLsppExtInParams,
                                      pLsppExtOutParams) != OSIX_SUCCESS)
    {
        LSPP_LOG (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
                  LSPP_FETCH_FROM_EXT_MOD_FAILED, "LsppUtilTestBaseOid");
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);
        return OSIX_FAILURE;
    }

    if (pLsppExtOutParams->OutOid.u2ServiceOidLen > LSPP_MAX_OID_LEN)
    {
        LSPP_LOG (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
                  LSPP_INVALID_OID_LEN,
                  "LsppUtilTestBaseOid",
                  pLsppExtOutParams->OutOid.u2ServiceOidLen);
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);
        return OSIX_FAILURE;
    }

    /* Compare the OID given as input with the BASE OID of the 
     * corresponding table
     * */
    if (MEMCMP (pLsppExtOutParams->OutOid.au4ServiceOidList,
                pLsppPingTraceEntry->MibObject.au4FsLsppPathPointer,
                (sizeof (UINT4) * pLsppExtOutParams->OutOid.
                 u2ServiceOidLen)) != 0)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);
        return OSIX_FAILURE;
    }
    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtInParams);
    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtOutParams);
    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : LsppUtilGetMaxReqsToSend
 *
 * Description: This function is to get the maximum number of requests to be 
 *              sent for a particular ping trace entry
 *
 * Input      : pLsppPingTraceEntry - PingTrace Entry for which max number of
 *              requests has to be calculated
 * 
 * Output     : pu4MaxReqsToSend - Maximum number of requests to be sent
 * Returns    : None
 *     
 *****************************************************************************/
VOID
LsppUtilGetMaxReqsToSend (tLsppFsLsppPingTraceTableEntry * pLsppPingTraceEntry,
                          UINT4 *pu4MaxReqsToSend)
{
    /* If the request type is trace route set the maximum requests to the
     * value set in the max TTL */

    if (pLsppPingTraceEntry->MibObject.i4FsLsppRequestType ==
        LSPP_REQ_TYPE_TRACE_ROUTE)
    {
        *pu4MaxReqsToSend = pLsppPingTraceEntry->MibObject.u4FsLsppTTLValue;
    }

    else if (pLsppPingTraceEntry->MibObject.i4FsLsppRequestType ==
             LSPP_REQ_TYPE_PING)
    {
        /* If sweep option amd burst option are disabled, then
         * maximum requests = repeat count
         * */
        if ((pLsppPingTraceEntry->MibObject.i4FsLsppSweepOption ==
             LSPP_SNMP_FALSE)
            && (pLsppPingTraceEntry->MibObject.i4FsLsppBurstOption ==
                LSPP_SNMP_FALSE))
        {
            *pu4MaxReqsToSend = pLsppPingTraceEntry->MibObject.
                u4FsLsppRepeatCount;
        }
        else if (pLsppPingTraceEntry->MibObject.i4FsLsppSweepOption ==
                 LSPP_SNMP_TRUE)
        {
            /* If the Sweep option is enabled, then
             *                       (sweep max - sweep min)
             * maximum requests = (  ---------------------- + 1) * RepeatCount
             *                         sweep increment
             * */
            *pu4MaxReqsToSend =
                ((((pLsppPingTraceEntry->MibObject.u4FsLsppSweepMaximum -
                    pLsppPingTraceEntry->MibObject.u4FsLsppSweepMinimum) /
                   pLsppPingTraceEntry->MibObject.u4FsLsppSweepIncrement) + 1) *
                 pLsppPingTraceEntry->MibObject.u4FsLsppRepeatCount);
        }
        else if (pLsppPingTraceEntry->MibObject.i4FsLsppBurstOption ==
                 LSPP_SNMP_TRUE)
        {
            /* If the Burst option is enabled then
             * maximum requests = burst size * repeat count
             * */
            *pu4MaxReqsToSend =
                pLsppPingTraceEntry->MibObject.u4FsLsppRepeatCount *
                pLsppPingTraceEntry->MibObject.u4FsLsppBurstSize;
        }
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : LsppUtilCalcPingPduSize
 * DESCRIPTION      : This function is used to calculate the lenght of the 
 *                    LSP Ping PDU
 * Input(s)         : pLsppEchoMsg - Pointer to the echo messgae
 * OUTPUT           : pLsppEchoMsg - PDU length updated in echo message
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 **************************************************************************/
PUBLIC VOID
LsppUtilCalcPingPduSize (tLsppEchoMsg * pLsppEchoMsg)
{
    tLsppPduInfo       *pLsppPduInfo = NULL;
    UINT4               u4LspPingPduLen = 0;
    UINT4               u4PwTlvPadLen = 0;
    UINT4               u4TlvLen = 0;
    UINT1               u1FecStackDepth = 0;

    pLsppPduInfo = &(pLsppEchoMsg->LsppPduInfo);

    u4LspPingPduLen = LSPP_PROTOCOL_HEADER_LEN;

    /* Updating length if Target FEC stack TLV is present */
    if (pLsppPduInfo->u2TlvsPresent & LSPP_TARGET_FEC_TLV)
    {
        u1FecStackDepth = pLsppEchoMsg->LsppPduInfo.u1FecStackDepth;

        /* Updating TLV Header Length */
        u4LspPingPduLen = (u4LspPingPduLen + LSPP_TLV_HEADER_LEN);

        while (u1FecStackDepth > 0)
        {
            /* Adding the Sub TLV header length */
            u4TlvLen = (u4TlvLen + LSPP_TLV_HEADER_LEN);

            switch (pLsppPduInfo->LsppFecTlv[u1FecStackDepth - 1].u2TlvType)
            {
                case LSPP_FEC_LDP_IPV4:
                    u4TlvLen = (u4TlvLen +
                                LSPP_LDP_IPV4_TLV_LEN +
                                LSPP_FEC_LDP_IPV4_PAD_BYTES);
                    break;

                case LSPP_FEC_LDP_IPV6:
                    u4TlvLen = (u4TlvLen +
                                LSPP_LDP_IPV6_TLV_LEN +
                                LSPP_FEC_LDP_IPV6_PAD_BYTES);
                    break;

                case LSPP_FEC_RSVP_IPV4:
                    u4TlvLen = (u4TlvLen + LSPP_RSVP_IPV4_TLV_LEN);
                    break;

                case LSPP_FEC_RSVP_IPV6:
                    u4TlvLen = (u4TlvLen + LSPP_RSVP_IPV6_TLV_LEN);
                    break;

                case LSPP_FEC_128_DEPRECATED_PWFEC:
                    u4TlvLen = (u4TlvLen +
                                LSPP_FEC128_PW_DEPREC_TLV_LEN +
                                LSPP_FEC_128_DEPRECATED_PWFEC_PAD_BYTES);
                    break;

                case LSPP_FEC_128_PWFEC:
                    u4TlvLen = (u4TlvLen +
                                LSPP_FEC128_PW_TLV_LEN +
                                LSPP_FEC_128_PWFEC_PAD_BYTES);
                    break;

                case LSPP_FEC_129_PWFEC:
                    /* Calculate the number of bytes to be padded for 
                     * 4 byte alignment.*/
                    u4PwTlvPadLen = (UINT4)((LSPP_BYTE_PAD_MAX_LEN -
                                      (LSPP_FEC129_PW_TLV_LEN
                                       (u1FecStackDepth - 1) %
                                       LSPP_BYTE_PAD_MAX_LEN)) %
                                     LSPP_BYTE_PAD_MAX_LEN);
                    u4TlvLen = (u4TlvLen +
                                (UINT4)LSPP_FEC129_PW_TLV_LEN (u1FecStackDepth - 1) +
                                u4PwTlvPadLen);
                    break;

                case LSPP_FEC_NIL:
                    u4TlvLen = (u4TlvLen + LSPP_NIL_TLV_LEN);
                    break;

                case LSPP_FEC_STATIC_LSP:
                    u4TlvLen = (u4TlvLen +
                                LSPP_STATIC_LSP_TLV_LEN +
                                LSPP_FEC_STATIC_LSP_PAD_BYTES);
                    break;

                case LSPP_FEC_STATIC_PW:
                    /* Calculate the number of bytes to be padded for 
                     * 4 byte alignment.*/
                    u4PwTlvPadLen = (UINT4)((LSPP_BYTE_PAD_MAX_LEN -
                                      (LSPP_STATIC_PW_TLV_LEN
                                       (u1FecStackDepth - 1) %
                                       LSPP_BYTE_PAD_MAX_LEN)) %
                                     LSPP_BYTE_PAD_MAX_LEN);

                    u4TlvLen = (UINT4)(u4TlvLen +
                                LSPP_STATIC_PW_TLV_LEN
                                (u1FecStackDepth - 1) + u4PwTlvPadLen);
                    break;

                default:
                    LSPP_LOG (pLsppEchoMsg->u4ContextId,
                              LSPP_INVALID_FEC_TYPE_TO_CALC_PDU,
                              "LsppUtilCalcPingPduSize");
                    return;
            }
            /*Decerementing Target Fec Stack */
            u1FecStackDepth--;
        }

        /* Storing Fec Stack TLV Length */
        pLsppPduInfo->u2FecStackTlvLength = (UINT2) u4TlvLen;

        u4LspPingPduLen = u4LspPingPduLen + u4TlvLen;
    }

    /* Updating length if DSMAP TLV is present */
    if (pLsppPduInfo->u2TlvsPresent & LSPP_DSMAP_TLV)
    {
        if ((pLsppPduInfo->LsppDsMapTlv.u1AddrType == LSPP_IPV4_NUMBERED)
            || (pLsppPduInfo->LsppDsMapTlv.u1AddrType == LSPP_IPV4_UNNUMBERED))
        {
            u4TlvLen = (UINT4)LSPP_DSMAP_IPV4_TLV_LEN;
        }
        else if ((pLsppPduInfo->LsppDsMapTlv.u1AddrType ==
                  LSPP_IPV6_NUMBERED)
                 || (pLsppPduInfo->LsppDsMapTlv.u1AddrType ==
                     LSPP_IPV6_UNNUMBERED))
        {
            u4TlvLen = (UINT4)LSPP_DSMAP_IPV6_TLV_LEN;
        }
        else if (pLsppPduInfo->LsppDsMapTlv.u1AddrType ==
                 LSPP_ADDR_TYPE_NOT_APPLICABLE)
        {
            u4TlvLen =(UINT4) LSPP_DSMAP_NA_LEN;
        }

        pLsppPduInfo->LsppDsMapTlv.u2Length = (UINT2) u4TlvLen;
        u4LspPingPduLen = (u4LspPingPduLen + LSPP_TLV_HEADER_LEN + u4TlvLen);
    }

    /* Updating length if Interface and Label stack TLV is present */
    if (pLsppPduInfo->u2TlvsPresent & LSPP_INTERFACE_AND_LBL_STACK_TLV)
    {
        if ((pLsppPduInfo->LsppIfLblStkTlv.u1AddrType ==
             LSPP_IPV4_NUMBERED) ||
            (pLsppPduInfo->LsppIfLblStkTlv.u1AddrType == LSPP_IPV4_UNNUMBERED))
        {
            u4TlvLen = (UINT4)LSPP_IF_LBL_STK_TLV_IPV4_ADDR_LEN;
        }
        else if ((pLsppPduInfo->LsppIfLblStkTlv.u1AddrType ==
                  LSPP_IPV6_NUMBERED) ||
                 (pLsppPduInfo->LsppIfLblStkTlv.u1AddrType ==
                  LSPP_IPV6_UNNUMBERED))
        {
            u4TlvLen = (UINT4)LSPP_IF_LBL_STK_TLV_IPV4_ADDR_LEN;
        }
        else if (pLsppPduInfo->LsppIfLblStkTlv.u1AddrType ==
                 LSPP_ADDR_TYPE_NOT_APPLICABLE)
        {
            u4TlvLen = (UINT4)(LSPP_IF_LBL_STK_NA_LEN);
        }

        pLsppPduInfo->LsppIfLblStkTlv.u2Length = (UINT2) u4TlvLen;
        u4LspPingPduLen = (u4LspPingPduLen + LSPP_TLV_HEADER_LEN + u4TlvLen);
    }

    /* Updating length if BFD Disc is present */
    if (pLsppPduInfo->u2TlvsPresent & LSPP_BFD_DISCRIMINATOR_TLV)
    {
        pLsppEchoMsg->u4BfdTlvOffset = u4LspPingPduLen;
        u4LspPingPduLen = (UINT4) (u4LspPingPduLen +
                                   LSPP_TLV_HEADER_LEN + LSPP_BFD_DISC_TLV_LEN);
    }

    /* Updating length if pad tlv is present. This is applicable only 
     * for reply packet. The length for padding information in PAD Tlv 
     * in echo request will be calculated only after finding the complete 
     * packet length. */
    if (pLsppPduInfo->u2TlvsPresent & LSPP_PAD_TLV)
    {
        u4LspPingPduLen = (UINT4) (u4LspPingPduLen +
                                   LSPP_TLV_HEADER_LEN + LSPP_PAD_TLV_LEN);
    }

    /* Updating length if reply TOS is present */
    if (pLsppPduInfo->u2TlvsPresent & LSPP_REPLY_TOS_BYTE_TLV)
    {
        u4LspPingPduLen = (UINT4) (u4LspPingPduLen +
                                   LSPP_TLV_HEADER_LEN +
                                   LSPP_REPLY_TOS_TLV_LEN);
    }

    /* Updating length if Error TLV is present */
    if (pLsppPduInfo->u2TlvsPresent & LSPP_ERRORED_TLV)
    {
        u4LspPingPduLen = (UINT4) (u4LspPingPduLen +
                                   LSPP_TLV_HEADER_LEN + LSPP_ERRORED_TLV_LEN);
    }

    pLsppEchoMsg->u4LsppPduLen = u4LspPingPduLen;
    return;
}

/***************************************************************************
 * FUNCTION NAME    : LsppUtilCalculatePktSize
 * DESCRIPTION      : This function is used to calculate the packet size needed 
 *                    to frame LSP Ping PDU
 * Input(s)         : pLsppEchoMsg - Pointer to Echo message PDU which contains
 *                    details to frame the PDU
 * OUTPUT           : pLsppEchoMsg - Updated Echo message with packet size
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 **************************************************************************/
PUBLIC VOID
LsppUtilCalculatePktSize (tLsppEchoMsg * pLsppEchoMsg)
{
    UINT4               u4LabelLen = 0;
    UINT4               u4Offset = 0;
    UINT4               u4AchTlvLen = 0;
    UINT4               u4PwMepTlvPadLen = 0;
    UINT4               u4LblStkDepth = 0;
    UINT4               u4PathType = 0;

    u4LblStkDepth = (UINT4) pLsppEchoMsg->u1LabelStackDepth;
    u4PathType = (UINT4) pLsppEchoMsg->LsppPathInfo.u1PathType;

    /* Include length for RAL which gets added for PW when CC type is 
     * is RAL or the length for GAL which will be added for MSPL-TP 
     * Static Tunnel.
     */
    if ((((u4PathType == LSPP_PATH_TYPE_FEC_128) ||
          (u4PathType == LSPP_PATH_TYPE_FEC_129) ||
          (u4PathType == LSPP_PATH_TYPE_STATIC_PW)) &&
         ((pLsppEchoMsg->LsppPathInfo.PwDetail.u1CcSelected ==
           LSPP_VCCV_CC_RAL))) ||
        ((u4PathType == LSPP_PATH_TYPE_STATIC_TNL) &&
         ((pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_ACH) ||
          (pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_ACH_IP))))
    {
        u4LblStkDepth = u4LblStkDepth + 1;
    }

    u4LabelLen = (u4LblStkDepth * LSPP_MPLS_LABEL_LEN);

    /* Calcualting the offset */
    u4Offset = u4Offset + u4LabelLen;

    if (pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_ACH)
    {
        if (pLsppEchoMsg->LsppAchInfo.u1AchTlvPresent & LSPP_ACH_IP_LSP_MEP_TLV)
        {
            u4AchTlvLen = LSPP_ACH_TYPE_TUNNEL_MEP_LEN + LSPP_TLV_HEADER_LEN;
        }
        else if (pLsppEchoMsg->LsppAchInfo.u1AchTlvPresent &
                 LSPP_ACH_IP_PW_MEP_TLV)
        {
            u4PwMepTlvPadLen = (UINT4)((LSPP_BYTE_PAD_MAX_LEN -
                                 (LSPP_ACH_TYPE_PW_MEP_LEN %
                                  LSPP_BYTE_PAD_MAX_LEN)) %
                                LSPP_BYTE_PAD_MAX_LEN);

            u4AchTlvLen = (UINT4)(LSPP_ACH_TYPE_PW_MEP_LEN + u4PwMepTlvPadLen +
                           LSPP_TLV_HEADER_LEN);
        }
        else if (pLsppEchoMsg->LsppAchInfo.u1AchTlvPresent &
                 LSPP_ACH_ICC_MEP_TLV)
        {
            u4AchTlvLen = (LSPP_ACH_TYPE_ICC_MEP_LEN + LSPP_TLV_HEADER_LEN);
        }

        if (pLsppEchoMsg->LsppAchInfo.u1AchTlvPresent & LSPP_ACH_MIP_TLV)
        {
            u4AchTlvLen = (u4AchTlvLen + LSPP_ACH_TYPE_MIP_LEN +
                           LSPP_TLV_HEADER_LEN);
        }

        pLsppEchoMsg->LsppAchInfo.AchTlvHeader.u2TlvHdrLen =
            (UINT2) u4AchTlvLen;

        /* Adding Ach Header and ACH TLV header Length to the total length */
        u4AchTlvLen = (u4AchTlvLen + LSPP_ACH_HEADER_LEN +
                       LSPP_ACH_TLV_HEADER_LEN);

        /* Legnth of Header till ACH Tlvs */
        u4Offset = u4Offset + u4AchTlvLen;
    }
    else if ((pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_IP) ||
             (pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_ACH_IP))
    {
        if (pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_ACH_IP)
        {
            u4Offset = u4Offset + LSPP_ACH_HEADER_LEN;
        }

        /* Include the IP and UDP header length in calculating the
         * packet size only when the IP encapsulation is done by
         * LSPP module.*/
        if (!((pLsppEchoMsg->LsppPduInfo.LsppHeader.u1MessageType ==
               LSPP_ECHO_REPLY) &&
              ((pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReplyMode ==
                LSPP_REPLY_IP_UDP) ||
               ((pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReplyMode ==
                 LSPP_REPLY_IP_UDP_ROUTER_ALERT) &&
                (pLsppEchoMsg->u1IsRouteExists == OSIX_TRUE)))))
        {
            /* Updating the Ip Header offset */
            pLsppEchoMsg->u4IpHdrOffset = u4Offset;

            u4Offset = (UINT4)(u4Offset +
                (pLsppEchoMsg->LsppIpHeader.u1Hlen *
                 LSPP_IP_HEADER_LEN_IN_BYTES));

            /* Updating the Udp Header offset */
            pLsppEchoMsg->u4UdpHdrOffset = u4Offset;

            u4Offset = u4Offset + LSPP_UDP_HDR_LEN;
        }
    }

    /* Updating Header Length */
    pLsppEchoMsg->u4EncapHdrLen = u4Offset;

    LsppUtilCalcPingPduSize (pLsppEchoMsg);

    return;
}

/******************************************************************************
 * Function   : LsppUtilFillStaticLspFec 
 * Description: This function updates the Static LSP Fec from the Tunnel Info.
 * Input      : pTeTnlInfo - pointer to the tunnel information.
 *              pLsppStaticLspFec - Pointer to Static LSP FEC in Echo message.  
 * Output     : pLsppStaticLspFec - updated Static LSP FEC TLV structure.
 * Returns    : None 
 *****************************************************************************/
VOID
LsppUtilFillStaticLspFec (tLsppTeTnlInfo * pTeTnlInfo,
                          tLsppStaticLspFec * pLsppStaticLspFec)
{
    pLsppStaticLspFec->u4SrcGlobalId =
        pTeTnlInfo->TnlInfo.SrcNodeId.MplsGlobalNodeId.u4GlobalId;

    pLsppStaticLspFec->u4SrcNodeId =
        pTeTnlInfo->TnlInfo.SrcNodeId.MplsGlobalNodeId.u4NodeId;

    pLsppStaticLspFec->u4DstGlobalId =
        pTeTnlInfo->TnlInfo.DstNodeId.MplsGlobalNodeId.u4GlobalId;

    pLsppStaticLspFec->u4DstNodeId =
        pTeTnlInfo->TnlInfo.DstNodeId.MplsGlobalNodeId.u4NodeId;

    pLsppStaticLspFec->u2SrcTnlNum = (UINT2) pTeTnlInfo->TnlInfo.u4SrcTnlNum;

    pLsppStaticLspFec->u2DstTnlNum = (UINT2) pTeTnlInfo->TnlInfo.u4DstTnlNum;

    pLsppStaticLspFec->u2LspNum = (UINT2) pTeTnlInfo->TnlInfo.u4LspNum;

    return;
}

/******************************************************************************
 * Function   : LsppUtilFillStaticPwFec 
 * Description: This function fills the fetched Static Pw info
 * Input      : pLsppPwDetail - Pw details
 *              pLsppStaticPwFec - Pointer to PW FEC in Echo message  
 * Output     : pLsppStaticPwFec - pointer to the updated Static PW FEC 
 *              structure.
 * Returns    : None
 *****************************************************************************/
VOID
LsppUtilFillStaticPwFec (tLsppPwDetail * pLsppPwDetail,
                         tLsppStaticPwFec * pLsppStaticPwFec)
{

    pLsppStaticPwFec->u1AgiType = pLsppPwDetail->PwFecInfo.u1AgiType;

    pLsppStaticPwFec->u1AgiLen = pLsppPwDetail->PwFecInfo.u1AgiLen;

    MEMCPY (&(pLsppStaticPwFec->au1Agi), &(pLsppPwDetail->PwFecInfo.au1Agi),
            pLsppStaticPwFec->u1AgiLen);

    pLsppStaticPwFec->u4SrcGlobalId =
        pLsppPwDetail->PwFecInfo.SrcNodeId.MplsGlobalNodeId.u4GlobalId;

    pLsppStaticPwFec->u4SrcNodeId =
        pLsppPwDetail->PwFecInfo.SrcNodeId.MplsGlobalNodeId.u4NodeId;

    pLsppStaticPwFec->u4SrcAcId = pLsppPwDetail->PwFecInfo.u4SrcAcId;

    pLsppStaticPwFec->u4DstGlobalId =
        pLsppPwDetail->PwFecInfo.DstNodeId.MplsGlobalNodeId.u4GlobalId;

    pLsppStaticPwFec->u4DstNodeId =
        pLsppPwDetail->PwFecInfo.DstNodeId.MplsGlobalNodeId.u4NodeId;

    pLsppStaticPwFec->u4DstAcId = pLsppPwDetail->PwFecInfo.u4DstAcId;

    return;
}

/******************************************************************************
 * Function   : LsppUtilFillRsvpFecInfo 
 * Description: This function is to fill RSVP FEC details in echo message
 * Input      : pTnlInfo     - Pointer to Tnl Info Structure,
 *              pLsppEchoMsg - Echo Message Strucutre in which 
 *                             RSVP FEC TLv structure needs to be updated
 *              u1FecStackDepth - Stack Depth at which RSVP FEC TLv must be 
 *                                updated
 * Output     : LsppRsvpTeTlv - updated RSVP FEC TLv structure
 * Returns    : None
 *****************************************************************************/
VOID
LsppUtilFillRsvpFecInfo (tLsppTeTnlInfo * pLsppTeTnlInfo,
                         tLsppEchoMsg * pLsppEchoMsg, UINT1 u1FecStackDepth)
{
    tLsppRsvpTeFecTlv  *pLsppRsvpTeFecTlv = NULL;

    pLsppRsvpTeFecTlv = &(pLsppEchoMsg->LsppPduInfo.
                          LsppFecTlv[u1FecStackDepth].RsvpTeFecTlv);

    pLsppRsvpTeFecTlv->u1AddrType = (UINT1)
        pLsppTeTnlInfo->TnlInfo.SrcNodeId.u4NodeType;

    if (pLsppRsvpTeFecTlv->u1AddrType == LSPP_MPLS_ADDR_TYPE_IPV4)
    {
        pLsppEchoMsg->LsppPduInfo.LsppFecTlv[u1FecStackDepth].
            u2TlvType = LSPP_FEC_RSVP_IPV4;

        pLsppRsvpTeFecTlv->TunnelSenderAddr.u4_addr[0] =
            pLsppTeTnlInfo->TnlInfo.SrcNodeId.MplsRouterId.u4_addr[0];

        pLsppRsvpTeFecTlv->TunnelEndAddr.u4_addr[0] =
            pLsppTeTnlInfo->TnlInfo.DstNodeId.MplsRouterId.u4_addr[0];

        pLsppRsvpTeFecTlv->ExtendedTunnelId.u4_addr[0] =
            pLsppTeTnlInfo->ExtendedTnlId.u4_addr[0];
    }
    else if (pLsppRsvpTeFecTlv->u1AddrType == LSPP_MPLS_ADDR_TYPE_IPV6)
    {
        MEMCPY (pLsppRsvpTeFecTlv->TunnelSenderAddr.u1_addr,
                pLsppTeTnlInfo->TnlInfo.SrcNodeId.MplsRouterId.u1_addr,
                LSPP_IPV6_ADDR_LEN);

        MEMCPY (pLsppRsvpTeFecTlv->TunnelEndAddr.u1_addr,
                pLsppTeTnlInfo->TnlInfo.DstNodeId.MplsRouterId.u1_addr,
                LSPP_IPV6_ADDR_LEN);

        MEMCPY (pLsppRsvpTeFecTlv->ExtendedTunnelId.u1_addr,
                pLsppTeTnlInfo->ExtendedTnlId.u1_addr, LSPP_IPV6_ADDR_LEN);

        pLsppEchoMsg->LsppPduInfo.LsppFecTlv[u1FecStackDepth].
            u2TlvType = LSPP_FEC_RSVP_IPV6;
    }

    pLsppRsvpTeFecTlv->u2TunnelId = (UINT2) pLsppTeTnlInfo->TnlInfo.u4SrcTnlNum;

    pLsppRsvpTeFecTlv->u2LspId = (UINT2) pLsppTeTnlInfo->u4TnlPrimaryInstance;
}

/******************************************************************************
 * Function   : LsppUtilFillLdpFecInfo
 * Description: This function is to fill Ldp FEC details in echo message
 * Input      : pLdpInfo - LDP Info,
 *              pLsppEchoMsg - Echo Msg Strucutre where LDP FEC TLV needs
                               to be updated
                u1FecStackDepth - Depth at which LDP FEC TLV needs to be updated
 * Output     : pLsppEchoMsg - Echo Msg with Updated LDP FEC TLV
 * Returns    : None
 *****************************************************************************/
PUBLIC VOID
LsppUtilFillLdpFecInfo (tLsppNonTeInfo * pLsppNonTeInfo,
                        tLsppEchoMsg * pLsppEchoMsg, UINT1 u1FecStackDepth)
{
    tLsppLdpFecTlv     *pLsppLdpFecTlv = NULL;

    pLsppLdpFecTlv = &(pLsppEchoMsg->LsppPduInfo.
                       LsppFecTlv[u1FecStackDepth].LdpFecTlv);

    pLsppLdpFecTlv->u1PrefixType = (UINT1) pLsppNonTeInfo->LdpInfo.u4AddrType;

    if (pLsppLdpFecTlv->u1PrefixType == LSPP_MPLS_ADDR_TYPE_IPV4)
    {
        pLsppEchoMsg->LsppPduInfo.LsppFecTlv[u1FecStackDepth].
            u2TlvType = LSPP_FEC_LDP_IPV4;

        pLsppLdpFecTlv->Prefix.u4_addr[0] =
            pLsppNonTeInfo->LdpInfo.LdpPrefix.u4_addr[0];
    }
    else if (pLsppLdpFecTlv->u1PrefixType == LSPP_MPLS_ADDR_TYPE_IPV6)
    {
        pLsppEchoMsg->LsppPduInfo.LsppFecTlv[u1FecStackDepth].
            u2TlvType = LSPP_FEC_LDP_IPV6;

        MEMCPY (pLsppLdpFecTlv->Prefix.u1_addr,
                pLsppNonTeInfo->LdpInfo.LdpPrefix.u1_addr, LSPP_IPV6_ADDR_LEN);
    }

    pLsppLdpFecTlv->u1PrefixLength =
        (UINT1) pLsppNonTeInfo->LdpInfo.u4PrefixLength;

    return;
}

/******************************************************************************
 * Function   : LsppUtilFillFec128PwDetails
 * Description: This function is to fill the fetched info
 * Input      : pLsppPwDetail - Pw details
 *              pLsppPwFec128Tlv - Pointer to Pw FEc in Echo message
 * Output     : pLsppPwFec128Tlv - pointer to the updated PW FEC 128
 *            : TLV structure.
 * Returns    : None
 *****************************************************************************/
VOID
LsppUtilFillFec128PwDetails (tLsppPwDetail * pLsppPwDetail,
                             tLsppPwFec128Tlv * pLsppPwFec128Tlv)
{

    pLsppPwFec128Tlv->LsppPwInfo.u4PWId = pLsppPwDetail->PwFecInfo.u4VcId;
    pLsppPwFec128Tlv->LsppPwInfo.u2PWType = pLsppPwDetail->PwFecInfo.u2PwVcType;

    if (pLsppPwDetail->PwFecInfo.SrcNodeId.u4NodeType ==
        LSPP_MPLS_ADDR_TYPE_IPV4)
    {
        pLsppPwFec128Tlv->SenderPEAddr.u4_addr[0] =
            pLsppPwDetail->PwFecInfo.SrcNodeId.MplsRouterId.u4_addr[0];

        pLsppPwFec128Tlv->LsppPwInfo.RemotePEAddr.u4_addr[0] =
            pLsppPwDetail->PwFecInfo.DstNodeId.MplsRouterId.u4_addr[0];
    }
    else
    {
        /* Un supported Address type for 128 FEC PW. */
    }

    return;
}

/******************************************************************************
 * Function   : LsppUtilFillFec129PwDetails
 * Description: This function is to fill the fetched info
 * Input      : pLsppPwDetail - Pw details
 *              pLsppPwFec129Tlv - Pointer to Pw FEc in Echo message
 * Output     : pLsppPwFec129Tlv - pointer to the udpated PW FEC 129 TLV
 * Returns    : None
 *****************************************************************************/
VOID
LsppUtilFillFec129PwDetails (tLsppPwDetail * pLsppPwDetail,
                             tLsppPwFec129Tlv * pLsppPwFec129Tlv)
{

    pLsppPwFec129Tlv->u2PWType = pLsppPwDetail->PwFecInfo.u2PwVcType;

    pLsppPwFec129Tlv->u1AgiType = pLsppPwDetail->PwFecInfo.u1AgiType;

    pLsppPwFec129Tlv->u1AgiLen = pLsppPwDetail->PwFecInfo.u1AgiLen;

    MEMCPY (&(pLsppPwFec129Tlv->au1Agi), &(pLsppPwDetail->PwFecInfo.au1Agi),
            pLsppPwFec129Tlv->u1AgiLen);

    pLsppPwFec129Tlv->u1SaiiType = pLsppPwDetail->PwFecInfo.u1LocalAIIType;

    pLsppPwFec129Tlv->u1SaiiLen = pLsppPwDetail->PwFecInfo.u1SaiiLen;

    MEMCPY (&(pLsppPwFec129Tlv->au1Saii), &(pLsppPwDetail->PwFecInfo.au1Saii),
            pLsppPwFec129Tlv->u1SaiiLen);

    pLsppPwFec129Tlv->u1TaiiType = pLsppPwDetail->PwFecInfo.u1RemoteAIIType;

    pLsppPwFec129Tlv->u1TaiiLen = pLsppPwDetail->PwFecInfo.u1TaiiLen;

    MEMCPY (&(pLsppPwFec129Tlv->au1Taii), &(pLsppPwDetail->PwFecInfo.au1Taii),
            pLsppPwFec129Tlv->u1TaiiLen);

    if (pLsppPwDetail->PwFecInfo.SrcNodeId.u4NodeType ==
        LSPP_MPLS_ADDR_TYPE_IPV4)
    {
        pLsppPwFec129Tlv->SenderPEAddr.u4_addr[0] =
            pLsppPwDetail->PwFecInfo.SrcNodeId.MplsRouterId.u4_addr[0];

        pLsppPwFec129Tlv->RemotePEAddr.u4_addr[0] =
            pLsppPwDetail->PwFecInfo.DstNodeId.MplsRouterId.u4_addr[0];
    }
    else
    {
        /* Un supported Address type for 129 FEC PW. */
    }
}

/******************************************************************************
 * Function   : LsppUtilFillLblInfo
 * Description: This function fills the label info 
 * Input      : pLblInfo -  Pointer to label Info Structure where details
 *              u4Label - Label that needs to be filled
 *              u1Exp   - Exp value that needs to be filled in label
 *              u1Ttl   - TTL value that needs to be filled in label
 *              u1Protocol - protocol which distributed this label
 * Output     : pLblInfo - pointer to LblInfo with updated values
 * Returns    : None
 *****************************************************************************/

PUBLIC VOID
LsppUtilFillLblInfo (tLsppLblInfo * pLblInfo, UINT4 u4Label, UINT1 u1Exp,
                     UINT1 u1Si, UINT1 u1Ttl, UINT1 u1Protocol)
{
    pLblInfo->u4Label = u4Label;
    pLblInfo->u1Exp = u1Exp;
    pLblInfo->u1SI = u1Si;
    pLblInfo->u1Ttl = u1Ttl;
    pLblInfo->u1Protocol = u1Protocol;
}

/******************************************************************************
 * Function   : LsppUtilFillLsppHeader
 * Description: This function is to fill the Ping Protocol Header
 * Input      : pLsppPingTraceEntry - Ping trace entry
 *              pLsppHeader - Pointer to Lsp Ping Structure
 * Returns    : None
 * Output     : None
 *****************************************************************************/
VOID
LsppUtilFillLsppHeader (tLsppFsLsppPingTraceTableEntry * pLsppPingTraceEntry,
                        tLsppHeader * pLsppHeader)
{
    pLsppHeader->u4SenderHandle =
        pLsppPingTraceEntry->MibObject.u4FsLsppSenderHandle;

    pLsppHeader->u4SequenceNum = pLsppPingTraceEntry->u4LastGeneratedSeqNum;

    pLsppHeader->u1MessageType = LSPP_ECHO_REQUEST;

    pLsppHeader->u2Version = LSPP_HEADER_VERSION;

    if (pLsppPingTraceEntry->MibObject.i4FsLsppFecValidate == LSPP_SNMP_TRUE)
    {
        pLsppHeader->u2GlobalFlag = pLsppHeader->u2GlobalFlag |
            LSPP_FEC_VALIDATE;
    }

    if (pLsppPingTraceEntry->MibObject.i4FsLsppReversePathVerify ==
        LSPP_SNMP_TRUE)
    {
        pLsppHeader->u2GlobalFlag = pLsppHeader->u2GlobalFlag |
            LSPP_REVERSE_PATH_VALIDATE;
    }

    pLsppHeader->u1ReplyMode = (UINT1) pLsppPingTraceEntry->MibObject.
        i4FsLsppReplyMode;
}

/******************************************************************************
 * Function   : LsppUtilUpdateIccMepTlv 
 * Description: This function is to Update the MEP TLv
 * Input      : pIccMepTlv - Pointer to ICC MEP tlv where data needs to be 
 *                           filled
 *              pLsppMegDetail - Pointer to the Meg Detail Structure where 
 *                               information to fill are available
 *
 * Output     : pIccMepTlv - updated ICC MEP information 
 * Returns    : None
 *****************************************************************************/
VOID
LsppUtilUpdateIccMepTlv (tLsppMegDetail * pLsppMegDetail,
                         tMplsIccMepTlv * pIccMepTlv)
{
    MEMCPY (pIccMepTlv->au1Icc, pLsppMegDetail->au1Icc, LSPP_MPLS_ICC_LENGTH);

    MEMCPY (pIccMepTlv->au1Umc, pLsppMegDetail->au1Umc, LSPP_MPLS_UMC_LENGTH);

    pIccMepTlv->u2MepIndex = pLsppMegDetail->u2IccSrcMepIndex;
}

/******************************************************************************
 * Function   :  LsppUtilUpdateIpLspMepTlv
 * Description: This function is to Update the MEP TLv
 * Input      : pIpLspMepTlv - Pointer to IP LSP MEP tlv where data needs to be 
 *                           filled
 *              pLsppMegDetail - Pointer to the Meg Detail Structure where 
 *                               information to fill are available
 *
 * Output     : pIpLspMepTlv - updated pointer with the IP LSP MEG information
 * Returns    : None
 *****************************************************************************/
VOID
LsppUtilUpdateIpLspMepTlv (tLsppPathInfo * pLsppPathInfo,
                           tMplsIpLspMepTlv * pIpLspMepTlv)
{
    if ((pLsppPathInfo->TeTnlInfo.u1TnlRole == MPLS_TE_EGRESS) &&
        (pLsppPathInfo->TeTnlInfo.u1TnlMode ==
         TE_TNL_MODE_COROUTED_BIDIRECTIONAL))
    {
        pIpLspMepTlv->GlobalNodeId.u4GlobalId =
            pLsppPathInfo->MegDetail.unServiceInfo.ServiceTnlId.DstNodeId.
            MplsGlobalNodeId.u4GlobalId;

        pIpLspMepTlv->GlobalNodeId.u4NodeId =
            pLsppPathInfo->MegDetail.unServiceInfo.ServiceTnlId.DstNodeId.
            MplsGlobalNodeId.u4NodeId;
    }
    else
    {
        pIpLspMepTlv->GlobalNodeId.u4GlobalId =
            pLsppPathInfo->MegDetail.unServiceInfo.ServiceTnlId.SrcNodeId.
            MplsGlobalNodeId.u4GlobalId;

        pIpLspMepTlv->GlobalNodeId.u4NodeId =
            pLsppPathInfo->MegDetail.unServiceInfo.ServiceTnlId.SrcNodeId.
            MplsGlobalNodeId.u4NodeId;
    }

    pIpLspMepTlv->u2TnlNum = (UINT2) pLsppPathInfo->MegDetail.unServiceInfo.
        ServiceTnlId.u4SrcTnlNum;

    pIpLspMepTlv->u2LspNum = (UINT2) pLsppPathInfo->MegDetail.unServiceInfo.
        ServiceTnlId.u4LspNum;
}

/******************************************************************************
 * Function   : LsppUtilUpdateIpPwMepTlv 
 * Description: This function is to Update the MEP TLv
 * Input      : pLsppPwDetail - Pw details where Pw info are present
 *              pMplsIpPwMepTlv - Mpls Pw Mep TLv where information needs 
 *              to be copied
 * Output     :  pMplsIpPwMepTlv - Updated PW TLV info.
 * Returns    : None
 *****************************************************************************/
VOID
LsppUtilUpdateIpPwMepTlv (tLsppPwDetail * pLsppPwDetail,
                          tMplsIpPwMepTlv * pMplsIpPwMepTlv)
{
    tLsppPwFecInfo     *pPwMepInfo = NULL;

    pPwMepInfo = &(pLsppPwDetail->PwFecInfo);

    /* Updating Agi Type */
    pMplsIpPwMepTlv->u1AgiType = pPwMepInfo->u1AgiType;

    /* Updating Agi Length */
    pMplsIpPwMepTlv->u1AgiLen = pPwMepInfo->u1AgiLen;

    MEMCPY (pMplsIpPwMepTlv->au1Agi,
            pPwMepInfo->au1Agi, pMplsIpPwMepTlv->u1AgiLen);

    /* Updating Source Global Id */
    pMplsIpPwMepTlv->u4GlobalId = pPwMepInfo->SrcNodeId.MplsGlobalNodeId.
        u4GlobalId;

    /* Updating Source Node Id */
    pMplsIpPwMepTlv->u4NodeId = pPwMepInfo->SrcNodeId.MplsGlobalNodeId.u4NodeId;

    /* Updating Source Ac Id */
    pMplsIpPwMepTlv->u4AcId = pPwMepInfo->u4SrcAcId;
}

/*************************************************************************/
/* Function Name     : LsppUtilTmToSec                                   */
/* Description       : This procedure converts the time in tUtlTm struct */
/*                     to seconds since Sntp Base Year(1900)             */
/* Input(s)          : tm - Time in tUtlTm structure                     */
/* Output(s)         : None                                              */
/* Returns           : u4Secs -Time in seconds since Sntp Base Year(1900)*/
/*************************************************************************/
VOID
LsppUtilTmToSec (tUtlTm * tm, UINT4 *pu4Secs)
{
    UINT4               u4Year = LSPP_SNTP_REF_BASE_YEAR;

    while (u4Year < tm->tm_year)
    {
        *pu4Secs += LSPP_SECS_IN_YEAR (u4Year);
        ++u4Year;
    }

    *pu4Secs += (tm->tm_yday * LSPP_SECS_IN_DAY);
    *pu4Secs += (tm->tm_hour * LSPP_SECS_IN_HOUR);
    *pu4Secs += (tm->tm_min * LSPP_SECS_IN_MINUTE);
    *pu4Secs += (tm->tm_sec);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : LsppUtilGetSysTime
 * DESCRIPTION      : This function is to get the current time of system
 * Input(s)         : pu4TimeInSec - Time in Sec
 *                    pu4TimeInSec - Time in Micro Sec
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 **************************************************************************/
VOID
LsppUtilGetSysTime (UINT4 *pu4TimeInSec, UINT4 *pu4TimeInMSec)
{
    tUtlSysPreciseTime  SysPreciseTime;

    MEMSET (&SysPreciseTime, 0, sizeof (tUtlSysPreciseTime));

    /* Getting Sys up time */
    UtlGetPreciseSysTime (&SysPreciseTime);

    *pu4TimeInSec = SysPreciseTime.u4Sec;
    *pu4TimeInMSec = SysPreciseTime.u4NanoSec / 1000;
    return;
}

/***************************************************************************
 * FUNCTION NAME    : LsppUtilUpdateLblStk
 * DESCRIPTION      : This function is to convert label stack in to a string
 *                    separated by /
 * Input(s)         :  pLblInfo - Pointer to label stack
 *                     pu1LabelStackDepth - Specifies the Label stack depth
 *                     pu1LabelStack - Pointer where label needs to be stored
 *                     pu1ExpStack   - Pointer where exp needs to be stored
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 ***************************************************************************/
VOID
LsppUtilUpdateLblStk (tLsppLblInfo * pLblInfo, UINT1 u1LblStackDepth,
                      UINT1 *pu1LabelStack, INT4 *pi4LblStkLen,
                      UINT1 *pu1ExpStack, INT4 *pi4LblExpStkLen)
{
    UINT1               u1LabelCount = 1;
    UINT1               u1LblLen = 0;
    UINT1               u1ExpLen = 0;

    if ((u1LblStackDepth <= 0) || (u1LblStackDepth > LSPP_MAX_LABEL_STACK))
    {
        return;
    }

    while (u1LabelCount < u1LblStackDepth)
    {
        u1LblLen =
            (UINT1) SNPRINTF ((CHR1 *) pu1LabelStack,
                              (LSPP_MAX_OCTETSTRING_SIZE), "%u%s",
                              (pLblInfo->u4Label), "/");

        u1ExpLen =
            (UINT1) SNPRINTF ((CHR1 *) pu1ExpStack, (LSPP_MAX_OCTETSTRING_SIZE),
                              "%d%s", (pLblInfo->u1Exp), "/");

        pu1LabelStack = pu1LabelStack + u1LblLen;
        pu1ExpStack = pu1ExpStack + u1ExpLen;

        *pi4LblStkLen = (INT4) ((*pi4LblStkLen) + u1LblLen);
        *pi4LblExpStkLen = (INT4) ((*pi4LblExpStkLen) + u1ExpLen);

        u1LabelCount++;
        pLblInfo++;
    }

    u1LblLen =
        (UINT1) SNPRINTF ((CHR1 *) pu1LabelStack, (LSPP_MAX_OCTETSTRING_SIZE),
                          "%u", (pLblInfo->u4Label));
    u1ExpLen =
        (UINT1) SNPRINTF ((CHR1 *) pu1ExpStack, (LSPP_MAX_OCTETSTRING_SIZE),
                          "%d", (pLblInfo->u1Exp));

    pu1LabelStack = pu1LabelStack + u1LblLen;
    pu1ExpStack = pu1ExpStack + u1ExpLen;

    *pu1LabelStack = '\0';
    *pu1ExpStack = '\0';

    *pi4LblStkLen = ((*pi4LblStkLen) + u1LblLen);
    *pi4LblExpStkLen = ((*pi4LblExpStkLen) + u1ExpLen);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : LsppUtilGetTimeToRun
 * DESCRIPTION      : This function is to Calculate the time for which a 
 *                    timer should be started
 * Input(s)         : i4TmrUnit - Timer Unit
 *                    u4TmrInterval - Timer Value
 * OUTPUT           : pu4TimeInSec - Time in Sec
 *                    pu4TimeInMilliSec - Time in Milli Sec
 * RETURNS          : NONE
 **************************************************************************/
PUBLIC VOID
LsppUtilCalcTimeToStartTmr (INT4 i4TmrUnit, UINT4 u4TmrInterval,
                            UINT4 *pu4TimeInSec, UINT4 *pu4TimeMilliInSec)
{
    if (i4TmrUnit == LSPP_TMR_UNIT_MILLISEC)
    {
        *pu4TimeMilliInSec = u4TmrInterval;
    }
    else if (i4TmrUnit == LSPP_TMR_UNIT_SEC)
    {
        *pu4TimeInSec = u4TmrInterval;
    }
    else if (i4TmrUnit == LSPP_TMR_UNIT_MIN)
    {
        *pu4TimeInSec = (u4TmrInterval * LSPP_SECS_IN_MINUTE);
    }
    return;
}

 /******************************************************************************
 * Function   : LsppUtilGetPathPointer 
 *
 * Description: This function is to get the corresponding OID of the given path
 *              by calling LSP Ping Port function.
 *
 * Input      : ContextId
 *              PathInfo
 *
 * Output     : OID of the given Path
 * 
 * Returns    : OSIX_FAILURE/OSIX_SUCCESS
 *****************************************************************************/

INT4
LsppUtilGetPathPointer (UINT4 u4ContextId,
                        tLsppPathId * pLsppPathId, UINT4 *pu4ServiceOid,
                        INT4 *pi4OidLength)
{

    tLsppExtInParams   *pLsppExtInParams = NULL;
    tLsppExtOutParams  *pLsppExtOutParams = NULL;

    if ((pLsppExtInParams = (tLsppExtInParams *)
         MemAllocMemBlk (LSPP_FSLSPPINPUTPARAMS_POOLID)) == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((pLsppExtOutParams = (tLsppExtOutParams *)
         MemAllocMemBlk (LSPP_FSLSPPOUTPUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        return OSIX_FAILURE;
    }

    MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
    MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));

    pLsppExtInParams->u4ContextId = u4ContextId;

    switch (pLsppPathId->u1PathType)
    {
        case LSPP_PATH_TYPE_LDP_IPV4:
        case LSPP_PATH_TYPE_LDP_IPV6:

            pLsppExtInParams->u4RequestType = LSPP_MPLSDB_GET_FTN_OID;

            MEMCPY (&(pLsppExtInParams->InLdpInfo),
                    &(pLsppPathId->unPathId.LdpInfo), sizeof (tLsppLdpInfo));

            break;

        case LSPP_PATH_TYPE_RSVP_IPV4:
        case LSPP_PATH_TYPE_RSVP_IPV6:

            pLsppExtInParams->u4RequestType = LSPP_MPLSDB_GET_TUNNEL_OID;

            MEMCPY (&(pLsppExtInParams->InTnlInfo),
                    &(pLsppPathId->unPathId.TnlInfo), sizeof (tLsppTnlInfo));
            break;

        case LSPP_PATH_TYPE_PW:

            pLsppExtInParams->u4RequestType = LSPP_L2VPN_GET_PW_OID;

            MEMCPY (&(pLsppExtInParams->InPwFecInfo.DstNodeId.MplsRouterId),
                    &(pLsppPathId->unPathId.PwInfo.PeerAddr), sizeof (tIpAddr));

            pLsppExtInParams->InPwFecInfo.u4VcId =
                pLsppPathId->unPathId.PwInfo.u4PwVcId;
            break;

        case LSPP_PATH_TYPE_MEP:

            pLsppExtInParams->u4RequestType = LSPP_MPLS_OAM_GET_MEG_OID;

            MEMCPY (&(pLsppExtInParams->InMegInfo.MegName),
                    &(pLsppPathId->unPathId.MegName), sizeof (tLsppMegMeName));
            break;

        default:
            LSPP_LOG (u4ContextId, LSPP_INVALID_PATH_TYPE_TO_FETCH_OID,
                      "LsppUtilGetPathPointer");
            MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtInParams);
            MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtOutParams);
            return OSIX_FAILURE;
    }

    if (LsppPortHandleExtInteraction (pLsppExtInParams, pLsppExtOutParams) !=
        OSIX_SUCCESS)
    {
        LSPP_LOG (u4ContextId, LSPP_FETCH_FROM_EXT_MOD_FAILED,
                  "LsppUtilGetPathPointer");
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);
        return OSIX_FAILURE;
    }

    *pi4OidLength = pLsppExtOutParams->OutOid.u2ServiceOidLen;

    if ((*pi4OidLength) >= LSPP_MPLS_SERVICE_OID_LEN)
    {
        LSPP_LOG (u4ContextId, LSPP_INVALID_SERVICE_OID_LEN,
                  "LsppUtilGetPathPointer");
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);
        return OSIX_FAILURE;
    }

    MEMCPY (pu4ServiceOid, &(pLsppExtOutParams->OutOid.au4ServiceOidList),
            ((sizeof (UINT4)) * (*pi4OidLength)));

    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtInParams);
    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtOutParams);

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : LsppUtilDupBuf                                   */
/*                                                                           */
/*    Description         : This function takes care of duplicating the      */
/*                          specified chain buffer. This function allocates  */
/*                          a new chain buffer of the size equal to the size */
/*                          of the given buffer and copies the data to the   */
/*                          newly allocated buffer.                          */
/*                                                                           */
/*    Input(s)            : pSrc - Pointer to the source buffer.             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : Pointer to the allocated buffer OR NULL          */
/*                                                                           */
/*****************************************************************************/
tCRU_BUF_CHAIN_HEADER *
LsppUtilDupBuf (tCRU_BUF_CHAIN_HEADER * pSrc)
{
    tCRU_BUF_CHAIN_HEADER *pDupBuf = NULL;
    UINT4               u4PktSize = 0;

    u4PktSize = LSPP_GET_CRU_VALID_BYTE_COUNT (pSrc);

    pDupBuf = LSPP_ALLOC_CRU_BUF (u4PktSize, 0);
    if (pDupBuf == NULL)
    {
        return NULL;
    }

    MEMSET (gLsppGlobals.au1DupBuf, 0, LSPP_MAX_PDU_LEN);

    if ((LSPP_COPY_FROM_BUF_CHAIN (pSrc, gLsppGlobals.au1DupBuf, 0,
                                   u4PktSize) == CRU_FAILURE) ||
        (LSPP_COPY_OVER_BUF_CHAIN (pDupBuf, gLsppGlobals.au1DupBuf, 0,
                                   u4PktSize) == CRU_FAILURE))
    {
        LSPP_RELEASE_CRU_BUF (pDupBuf, 0);
        return NULL;
    }

    return pDupBuf;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : LsppUtilClearEchoStatistics                      */
/*                                                                           */
/*    Description         : This function is to clear all the global echo    */
/*                          statistics                                       */
/*                                                                           */
/*    Input(s)            : pLsppFsLsppGlobalStatsEntry - Pointer to the     */
/*                          Global Stats entry                               */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : None                                             */
/*                                                                           */
/*****************************************************************************/

VOID
LsppUtilClearEchoStatistics (tLsppFsLsppGlobalStatsTableEntry
                             * pLsppFsLsppGlobalStatsEntry)
{
    pLsppFsLsppGlobalStatsEntry->MibObject.u4FsLsppGlbStatReqTx = 0;
    pLsppFsLsppGlobalStatsEntry->MibObject.u4FsLsppGlbStatReqRx = 0;
    pLsppFsLsppGlobalStatsEntry->MibObject.u4FsLsppGlbStatReqTimedOut = 0;
    pLsppFsLsppGlobalStatsEntry->MibObject.u4FsLsppGlbStatReqUnSent = 0;
    pLsppFsLsppGlobalStatsEntry->MibObject.u4FsLsppGlbStatReplyTx = 0;
    pLsppFsLsppGlobalStatsEntry->MibObject.u4FsLsppGlbStatReplyRx = 0;
    pLsppFsLsppGlobalStatsEntry->MibObject.u4FsLsppGlbStatReplyDropped = 0;
    pLsppFsLsppGlobalStatsEntry->MibObject.u4FsLsppGlbStatReplyUnSent = 0;
    pLsppFsLsppGlobalStatsEntry->MibObject.u4FsLsppGlbStatReplyFromEgr = 0;
    pLsppFsLsppGlobalStatsEntry->MibObject.u4FsLsppGlbStatUnLbldOutIf = 0;
    pLsppFsLsppGlobalStatsEntry->MibObject.u4FsLsppGlbStatDsMapMismatch = 0;
    pLsppFsLsppGlobalStatsEntry->MibObject.u4FsLsppGlbStatFecLblMismatch = 0;
    pLsppFsLsppGlobalStatsEntry->MibObject.u4FsLsppGlbStatNoFecMapping = 0;
    pLsppFsLsppGlobalStatsEntry->MibObject.u4FsLsppGlbStatUnKUpstreamIf = 0;
    pLsppFsLsppGlobalStatsEntry->MibObject.u4FsLsppGlbStatReqLblSwitched = 0;
    pLsppFsLsppGlobalStatsEntry->MibObject.u4FsLsppGlbStatReqUnSupptdTlv = 0;
    pLsppFsLsppGlobalStatsEntry->MibObject.u4FsLsppGlbStatMalformedReq = 0;
    pLsppFsLsppGlobalStatsEntry->MibObject.u4FsLsppGlbStatNoLblEntry = 0;
    pLsppFsLsppGlobalStatsEntry->MibObject.u4FsLsppGlbStatPreTermReq = 0;
    pLsppFsLsppGlobalStatsEntry->MibObject.u4FsLsppGlbStatProtMismatch = 0;
    pLsppFsLsppGlobalStatsEntry->MibObject.u4FsLsppGlbStatRsvdRetCode = 0;
    pLsppFsLsppGlobalStatsEntry->MibObject.u4FsLsppGlbStatNoRetCode = 0;
    pLsppFsLsppGlobalStatsEntry->MibObject.u4FsLsppGlbStatUndefRetCode = 0;
    pLsppFsLsppGlobalStatsEntry->MibObject.u4FsLsppGlbStatInvalidPktDropped = 0;

}

/*****************************************************************************
 * Function           : LsppVerifyIpAddrRange
 * Input(s)           : u4Addr, u1Flag
 * Output(s)          : None
 * Descripiton        : Verifies if an address is a valid 
 * source/destination address.
 * Returns            : OSIX_SUCCESS, OSIX_FAILURE
*****************************************************************************/
PUBLIC INT4
LsppUtilVerifyIpAddrRange (UINT4 u4Addr)
{
    if (IP_IS_ADDR_CLASS_E (u4Addr) || ((u4Addr & 0xFF000000) == 0) ||
        (IP_IS_ADDR_CLASS_D (u4Addr))
        ||
        (!((IP_IS_ADDR_CLASS_A (u4Addr)) || (IP_IS_ADDR_CLASS_B (u4Addr))
           || (IP_IS_ADDR_CLASS_C (u4Addr)))))
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  lspputil.c                     */
/*-----------------------------------------------------------------------*/
