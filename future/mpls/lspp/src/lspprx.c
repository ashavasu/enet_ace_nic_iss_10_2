/***************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lspprx.c,v 1.34 2016/06/03 10:27:23 siva Exp $
 *
 * Description: This file contains the Lspp Rx related functions 
 ***************************************************************************/
#ifndef _LSPPRX_C_
#define _LSPPRX_C_

#include "lsppinc.h"

/***************************************************************************
 * FUNCTION NAME    : LsppRxProcessPdu   
 * DESCRIPTION      : This function is used to process the LSP Ping Pdu
 *                    received either from socket or from MPLS-RTR
 * Input(s)         : pPkt - Received Packet Buffer
 *                    pLsppEchoMsg  - Pointer to the Echo msg strucutre
 *                                    which contain the received informaiton
 *                    u4PktSize - Size of the received packet
 *                    bIsMpls - This is to specify whether packet is received
 *                              from MPLS-RTR or socket.  
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 **************************************************************************/
PUBLIC INT4
LsppRxProcessPdu (tLsppEchoMsg * pLsppEchoMsg, UINT1 *pPkt, UINT4 u4PktSize,
                  BOOL1 bIsMpls)
{
    tLsppFsLsppPingTraceTableEntry *pLsppPingTraceEntry = NULL;
    tLsppFsLsppEchoSequenceTableEntry *pLsppEchoSequenceEntry = NULL;
    tLsppSysTime        LsppEchoPktRxTime;
    UINT4               u4Offset = 0;
    INT4                i4Status = LSPP_SHUTDOWN;
    UINT1               u1ValidationResult = 0;
    UINT1               u1RcvdMsgType = 0;

    MEMSET (&LsppEchoPktRxTime, 0, sizeof (tLsppSysTime));

    if (LsppCoreGetModuleStatus (pLsppEchoMsg->u4ContextId,
                                 &i4Status) != OSIX_SUCCESS)
    {
        LSPP_CMN_TRC (pLsppEchoMsg->u4ContextId,
                      LSPP_CONTEXT_ID_INVALID, "LsppRxProcessPdu",
                      pLsppEchoMsg->u4ContextId);

        /* The context is not valid and hence drop the packet. */
        LsppCoreUpdateGlobalStats (pLsppEchoMsg->u4ContextId,
                                   LSPP_INVALID_PKT_DROPPED, NULL);

        return OSIX_FAILURE;
    }

    /*Check whether the module is enabled on the received context. */
    if (i4Status == LSPP_SHUTDOWN)
    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId,
                  LSPP_MODULE_STATUS_DOWN_IN_CXT,
                  "LsppRxProcessPdu", pLsppEchoMsg->u4ContextId);

        /* The module is down in the received context and hence 
         * drop the packet.*/
        LsppCoreUpdateGlobalStats (pLsppEchoMsg->u4ContextId,
                                   LSPP_INVALID_PKT_DROPPED, NULL);
        return OSIX_FAILURE;
    }

    LSPP_LOG (pLsppEchoMsg->u4ContextId, LSPP_PKT_RECEIVED, "LsppRxProcessPdu");

    LSPP_DUMP_PKT (pLsppEchoMsg->u4ContextId, pPkt, u4PktSize);

    /* Get the current system time for updating the received 
     * timestamp field in the echo reply packet.*/
    LsppUtilGetSysTime (&(LsppEchoPktRxTime.u4TimeInSec),
                        &(LsppEchoPktRxTime.u4TimeInMicroSec));

    /* Process the MPLS Labels when the packet is MPLS encapsulated. */
    if (bIsMpls == OSIX_TRUE)
    {
        if (LsppRxProcessMplsPdu (&pPkt, pLsppEchoMsg, &u4Offset) !=
            OSIX_SUCCESS)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_PROCESS_MPLS_PKT_FAILED, "LsppRxProcessPdu");

            /* The Encapsulation validation failed and hence increment the 
             * invalid packet dropped count and drop the packet. */
            LsppCoreUpdateGlobalStats (pLsppEchoMsg->u4ContextId,
                                       LSPP_INVALID_PKT_DROPPED, NULL);
            return OSIX_FAILURE;
        }
    }
    else
    {
        pLsppEchoMsg->u1EncapType = LSPP_ENCAP_TYPE_IP;
    }

    /* Validate the LSP Ping Header. */
    if (LsppRxDecodeValidateLsppHeader (pLsppEchoMsg, &pPkt,
                                        &u1ValidationResult) != OSIX_SUCCESS)
    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId,
                  LSPP_ECHO_MSG_VALIDATION_FAILED, "LsppRxProcessPdu");

        /* Drop the echo message when the validation fails for a 
         * reply packet. */
        if (u1ValidationResult == LSPP_DROP_PACKET)
        {
            if (pLsppEchoMsg->LsppPduInfo.LsppHeader.u1MessageType ==
                LSPP_ECHO_REPLY)
            {
                /* Incrementing Global Stats */
                LsppCoreUpdateGlobalStats (pLsppEchoMsg->u4ContextId,
                                           LSPP_REPLY_DROPPED, NULL);
            }
            else
            {
                /* The message type validation failed and hecne increment the 
                 * invalid packet dropped counter.*/
                LsppCoreUpdateGlobalStats (pLsppEchoMsg->u4ContextId,
                                           LSPP_INVALID_PKT_DROPPED, NULL);
            }

            return OSIX_FAILURE;
        }
    }

    u1RcvdMsgType = pLsppEchoMsg->LsppPduInfo.LsppHeader.u1MessageType;

    /* Recived Source and Destination IP addresses validation 
     * for echo request and reply packets are performed. */

    if ((pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_IP) ||
        (pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_ACH_IP))
    {
        if (LsppRxIpAddrValidate (pLsppEchoMsg, u1RcvdMsgType) != OSIX_SUCCESS)
        {
            LsppCoreUpdateGlobalStats (pLsppEchoMsg->u4ContextId,
                                       LSPP_INVALID_PKT_DROPPED, NULL);

            LSPP_LOG (pLsppEchoMsg->u4ContextId, LSPP_INVALID_IP_HDR,
                      "LsppRxProcessPdu");

            return OSIX_FAILURE;
        }
    }

    /* Match the Echo Sequence when the recive message is reply. 
     * If the match fails, drop the packet here itself. No need to parse the 
     * complete packet.*/
    if (u1RcvdMsgType == LSPP_ECHO_REPLY)
    {
        /* Check whether packet is destianted for this node. 
         * If not, fwd the packet to the destination*/
        if (pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReplyMode ==
            LSPP_REPLY_IP_UDP_ROUTER_ALERT)
        {
            if (NetIpv4IfIsOurAddressInCxt (pLsppEchoMsg->u4ContextId,
                                            pLsppEchoMsg->LsppIpHeader.u4Dest)
                != OSIX_SUCCESS)
            {
                LsppRxFwdPacket (pLsppEchoMsg);
                return OSIX_SUCCESS;
            }
        }

        if (LsppRxMatchEchoSequenceEntry (pLsppEchoMsg->u4ContextId,
                                          &(pLsppEchoMsg->LsppPduInfo.
                                            LsppHeader),
                                          &pLsppPingTraceEntry,
                                          &pLsppEchoSequenceEntry) !=
            OSIX_SUCCESS)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_ECHO_MATCH_ENTRY_NOT_FOUND, "LsppRxProcessPdu");

            LsppCoreUpdateGlobalStats (pLsppEchoMsg->u4ContextId,
                                       LSPP_REPLY_DROPPED, NULL);
            return OSIX_FAILURE;
        }
    }

    /* Updating the offset after parsing the LSPP Header. */
    u4Offset = u4Offset + sizeof (tLsppHeader);

    /* Validate the LSPP PDU only when the LSPP header validation succeeds. */
    if (u1ValidationResult != LSPP_MALFORMED_ECHO_PACKET)
    {
        if (LsppRxParseValidatePdu (&pPkt, &(pLsppEchoMsg->LsppPduInfo),
                                    pLsppEchoMsg->u4ContextId,
                                    u4PktSize, u4Offset,
                                    &u1ValidationResult) != OSIX_SUCCESS)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_ECHO_MSG_VALIDATION_FAILED, "LsppRxProcessPdu");

            /* Drop the packet when the validation fails for 
             * the reply packet. */
            if (pLsppEchoMsg->LsppPduInfo.LsppHeader.u1MessageType ==
                LSPP_ECHO_REPLY)
            {
                /* Incrementing Global Stats */
                LsppCoreUpdateGlobalStats (pLsppEchoMsg->u4ContextId,
                                           LSPP_REPLY_DROPPED, NULL);
                return OSIX_FAILURE;
            }
        }

        /* Validate whether the received reply for a trace route request
         * from the trasnit node conatins the Dsmap TLV or reply from egress 
         * contains the Dsmap TLV.*/
        if (u1RcvdMsgType == LSPP_ECHO_REPLY)
        {
            if ((pLsppPingTraceEntry != NULL) &&
                (pLsppPingTraceEntry->MibObject.i4FsLsppDsMap ==
                 LSPP_SNMP_TRUE) &&
                (pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReturnCode ==
                 LSPP_LBL_SWITCHED_AT_STACK_DEPTH) &&
                (!(pLsppEchoMsg->LsppPduInfo.u2TlvsPresent & LSPP_DSMAP_TLV)))
            {
                LSPP_LOG (pLsppEchoMsg->u4ContextId,
                          LSPP_DSMAP_NOT_PRESENT_IN_REPLY_FRM_TRANSIT,
                          "LsppRxProcessPdu");

                /* Incrementing Global Stats */
                LsppCoreUpdateGlobalStats (pLsppEchoMsg->u4ContextId,
                                           LSPP_REPLY_DROPPED, NULL);
                return OSIX_FAILURE;
            }

            if ((pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReturnCode ==
                 LSPP_EGRESS_FOR_FEC_AT_STACK_DEPTH) &&
                (pLsppEchoMsg->LsppPduInfo.u2TlvsPresent & LSPP_DSMAP_TLV))
            {
                LSPP_LOG (pLsppEchoMsg->u4ContextId,
                          LSPP_DSMAP_PRESENT_IN_REPLY_FRM_EGRESS,
                          "LsppRxProcessPdu");

                /* Incrementing Global Stats */
                LsppCoreUpdateGlobalStats (pLsppEchoMsg->u4ContextId,
                                           LSPP_REPLY_DROPPED, NULL);
                return OSIX_FAILURE;
            }
        }
    }

    if (u1RcvdMsgType == LSPP_ECHO_REQUEST)
    {
        /* Filling return code */
        pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReturnCode = u1ValidationResult;

        /* Filling Received packet time in SNTP Format */
        pLsppEchoMsg->LsppPduInfo.LsppHeader.u4TimeStampRxInSec =
            LsppEchoPktRxTime.u4TimeInSec;
        pLsppEchoMsg->LsppPduInfo.LsppHeader.u4TimeStampRxInMicroSec =
            LsppEchoPktRxTime.u4TimeInMicroSec;
    }

    /* Further processing will be done by Core module. */
    if (LsppCoreProcessRequest (pLsppEchoMsg, pLsppPingTraceEntry,
                                pLsppEchoSequenceEntry, &LsppEchoPktRxTime,
                                u1RcvdMsgType) != OSIX_SUCCESS)
    {
        /* Update the statistics. */
        if (u1RcvdMsgType == LSPP_ECHO_REPLY)
        {
            LsppCoreUpdateGlobalStats (pLsppEchoMsg->u4ContextId,
                                       LSPP_REPLY_DROPPED, NULL);
        }
        else if (u1RcvdMsgType == LSPP_ECHO_REQUEST)
        {
            LsppCoreUpdateGlobalStats (pLsppEchoMsg->u4ContextId,
                                       LSPP_REQ_RX, NULL);
        }

        LSPP_LOG (pLsppEchoMsg->u4ContextId,
                  LSPP_PROCESS_REQUEST_FAILED, "LsppRxProcessPdu",
                  u1RcvdMsgType);

        return OSIX_FAILURE;
    }

    /* Update the statistics. */
    if (u1RcvdMsgType == LSPP_ECHO_REPLY)
    {
        LsppCoreUpdateGlobalStats (pLsppEchoMsg->u4ContextId,
                                   LSPP_REPLY_RX,
                                   &(pLsppEchoMsg->LsppPduInfo.LsppHeader.
                                     u1ReturnCode));
    }
    else if (u1RcvdMsgType == LSPP_ECHO_REQUEST)
    {
        LsppCoreUpdateGlobalStats (pLsppEchoMsg->u4ContextId,
                                   LSPP_REQ_RX, NULL);
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxProcessMplsPdu    
 * DESCRIPTION      : This function is used to process the LSP Ping Pdu
 * Input(s)         : pPkt - Received Packet Buffer
 *                    pLsppEchoMsg  - Pointer to the Echo msg strucutre
 *                                    which contain the received informaiton
 *                    pu4Offset     - specifies the bytes till which 
 *                                    contents have been read
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 **************************************************************************/
PUBLIC INT4
LsppRxProcessMplsPdu (UINT1 **ppPkt, tLsppEchoMsg * pLsppEchoMsg,
                      UINT4 *pu4Offset)
{
    tLsppPathInfo      *pLsppPathInfo = NULL;
    UINT4               u4OuterLabel = MPLS_INVALID_LABEL;
    UINT2               u2AchTlvsLen = 0;
    UINT1               u1LabelCount = 0;
    UINT1               u1HdrType = 0;
    UINT1               u1CcReceived = 0;
    BOOL1               bValidLabel = OSIX_TRUE;

    /* Fetch MPLS Label */
    LsppRxGetMplsLabelStack (ppPkt, pLsppEchoMsg, &u1LabelCount,
                             &u4OuterLabel, pu4Offset);

    if (u1LabelCount > LSPP_MAX_LABEL_STACK)
    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId,
                  LSPP_PKT_RECEIVED_WITH_INVALID_LBL_COUNT,
                  "LsppRxProcessMplsPdu", u1LabelCount);
        return OSIX_FAILURE;
    }

    pLsppEchoMsg->u1LabelStackDepth = u1LabelCount;
    pLsppPathInfo = &(pLsppEchoMsg->LsppPathInfo);

    if ((u1LabelCount == 0) || (u4OuterLabel == MPLS_INVALID_LABEL))
    {
        bValidLabel = OSIX_FALSE;
    }

    /* Check whether the next header is IP or ACH */
    LSPP_GET_1BYTE (u1HdrType, *ppPkt);

    /* Move the pointer to 1 byte to get the original position */
    *ppPkt = ((*ppPkt) - 1);
    u1HdrType = (UINT1) ((u1HdrType & 0xf0) >> LSPP_HDR_TYPE_OFFSET_BITS);

    if (bValidLabel == OSIX_TRUE)
    {
        /* Get and Verify the CC and CV when the echo packet is for a PW. */
        if ((pLsppPathInfo->u1PathType == LSPP_PATH_TYPE_FEC_128) ||
            (pLsppPathInfo->u1PathType == LSPP_PATH_TYPE_FEC_129) ||
            (pLsppPathInfo->u1PathType == LSPP_PATH_TYPE_STATIC_PW))
        {
            if (u1HdrType == LSPP_ENCAP_ACH_HDR)
            {
                /* The received control channel is ACH. */
                u1CcReceived = LSPP_VCCV_CC_ACH;
            }

            /* Check whether the control channel is RAL for PW. */
            while (u1LabelCount > 0)
            {
                if (pLsppEchoMsg->LblInfo[--u1LabelCount].u4Label ==
                    LSPP_LBL_ROUTER_ALERT)
                {
                    u1CcReceived = LSPP_VCCV_CC_RAL;
                }
            }

            /* Update the label count to the original value for 
             * further processing.*/
            u1LabelCount = pLsppEchoMsg->u1LabelStackDepth;

            /* Check whether the control channel is PW TTL. */
            if ((pLsppEchoMsg->LblInfo[u1LabelCount - 1].u1SI == 1) &&
                (pLsppEchoMsg->LblInfo[u1LabelCount - 1].u1Ttl == 1))
            {
                u1CcReceived = LSPP_VCCV_CC_TTL;
            }

            /* Verify the CC and CV capabilities. */
            if (LsppRxVerifyCcCvCabability (pLsppEchoMsg->u4ContextId,
                                            u1CcReceived,
                                            pLsppPathInfo->PwDetail.
                                            u1CcSelected,
                                            pLsppPathInfo->PwDetail.
                                            u1CvSelected) == OSIX_FAILURE)
            {
                LSPP_LOG (pLsppEchoMsg->u4ContextId,
                          LSPP_VCCV_VERIFICATION_FAILED,
                          "LsppRxProcessMplsPdu");
                return OSIX_FAILURE;
            }
            pLsppEchoMsg->u1CcSelected = u1CcReceived;
        }
    }

    if (u1HdrType == LSPP_ENCAP_ACH_HDR)
    {
        /* Parse the ACH headers */
        LsppRxGetAchHdr (ppPkt, &(pLsppEchoMsg->LsppAchInfo.AchHeader));

        if ((pLsppEchoMsg->LsppAchInfo.AchHeader.u2ChannelType !=
             LSPP_ACH_CHNL_TYPE_LSPP) &&
            (pLsppEchoMsg->LsppAchInfo.AchHeader.u2ChannelType !=
             LSPP_ACH_CHNL_TYPE_IPV4))
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_INVALID_CHANNEL_TYPE,
                      "LsppRxProcessMplsPdu",
                      pLsppEchoMsg->LsppAchInfo.AchHeader.u2ChannelType);

            return OSIX_FAILURE;
        }

        /* Moving Read bytes */
        *pu4Offset = *pu4Offset + LSPP_ACH_HEADER_LEN;

        pLsppEchoMsg->u1EncapType = LSPP_ENCAP_TYPE_ACH;
    }

    /* Parse and validate the ACH TLVs present. */
    if ((pLsppEchoMsg->LsppAchInfo.AchHeader.u2ChannelType ==
         LSPP_ACH_CHNL_TYPE_LSPP) ||
        (pLsppEchoMsg->LsppAchInfo.AchHeader.u2ChannelType ==
         LSPP_ACH_CHNL_TYPE_IPV4))
    {
        /* Get the ACH TLV header length to get the ACH TLVs from the 
         * received packet*/
        LSPP_GET_2BYTE (u2AchTlvsLen, *ppPkt);

        /* In case of IP over ACH encapsulation, the packet may contain 
         * ACH TLVs also. So checking whether the packet has ACH TLV 
         * or not by fetching the first nibble of the next byte.*/
        if (LSPP_IS_IP_OR_ACH_TLV_LEN (u2AchTlvsLen) != LSPP_ENCAP_IPV4_HDR)
        {
            /* Skipping the reserved field */
            *ppPkt = *ppPkt + LSPP_ACH_TLV_HDR_RSVD;

            pLsppEchoMsg->LsppAchInfo.AchTlvHeader.u2TlvHdrLen = u2AchTlvsLen;

            if (pLsppEchoMsg->LsppAchInfo.AchTlvHeader.u2TlvHdrLen != 0)
            {
                if (LsppRxGetAchTlv (ppPkt, &(pLsppEchoMsg->LsppAchInfo),
                                     pLsppEchoMsg->LsppAchInfo.AchTlvHeader.
                                     u2TlvHdrLen, pLsppEchoMsg->u4ContextId) ==
                    OSIX_FAILURE)
                {
                    LSPP_LOG (pLsppEchoMsg->u4ContextId,
                              LSPP_GET_ACH_TLV_FAILED, "LsppRxProcessMplsPdu");
                    return OSIX_FAILURE;
                }

                if (bValidLabel == OSIX_TRUE)
                {
                    /* Validate ACH header if present */
                    if (LsppRxValidateAchTlvs (pLsppEchoMsg->u4ContextId,
                                               &(pLsppEchoMsg->LsppAchInfo),
                                               pLsppPathInfo) != OSIX_SUCCESS)
                    {
                        LSPP_LOG (pLsppEchoMsg->u4ContextId,
                                  LSPP_VALIDATE_ACH_TLV_FAILED,
                                  "LsppRxProcessMplsPdu");
                        return OSIX_FAILURE;
                    }
                }
            }

            /* Bytes Fetched */
            *pu4Offset = *pu4Offset + LSPP_ACH_TLV_HEADER_LEN +
                pLsppEchoMsg->LsppAchInfo.AchTlvHeader.u2TlvHdrLen;
        }
        else
        {
            /* Move back the packet pointer to the number of read bytes 
             * when there is no ACH TLVs in the packet.*/
            *ppPkt = *ppPkt - sizeof (u2AchTlvsLen);
        }
    }

    /* If the header type is IPv4 or the ACH channel type in ACH header is IP,
     * then process the received IP/UDP headers.*/
    if ((u1HdrType == LSPP_ENCAP_IPV4_HDR) ||
        (pLsppEchoMsg->LsppAchInfo.AchHeader.u2ChannelType ==
         LSPP_ACH_CHNL_TYPE_IPV4))
    {
        /* Get and Validate IP and UDP Header */
        if (LsppRxValidateIpUdpHeader (ppPkt, pLsppEchoMsg) != OSIX_SUCCESS)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_IP_UDP_HEADER_VALIDATION_FAILED,
                      "LsppRxProcessMplsPdu");
            return OSIX_FAILURE;
        }

        if (bValidLabel == OSIX_TRUE)
        {
            /* Verify whether the received IP packet version and 
             * Src IP matches with the version and IP in the PW info. */
            if ((pLsppPathInfo->u1PathType == LSPP_PATH_TYPE_FEC_128) ||
                (pLsppPathInfo->u1PathType == LSPP_PATH_TYPE_FEC_129) ||
                (pLsppPathInfo->u1PathType == LSPP_PATH_TYPE_STATIC_PW))
            {
                if (pLsppEchoMsg->LsppIpHeader.u1Version !=
                    pLsppPathInfo->PwDetail.u1IpVersion)
                {
                    LSPP_LOG (pLsppEchoMsg->u4ContextId,
                              LSPP_PW_IP_VERSION_VERIFICATION_FAILED,
                              "LsppRxProcessMplsPdu");
                    return OSIX_FAILURE;
                }

                if (pLsppEchoMsg->LsppIpHeader.u4Src !=
                    pLsppPathInfo->PwDetail.PwFecInfo.DstNodeId.
                    MplsRouterId.u4_addr[0])
                {
                    LSPP_LOG (pLsppEchoMsg->u4ContextId,
                              LSPP_PW_SRC_IP_VERIFICATION_FAILED,
                              "LsppRxProcessMplsPdu");
                    return OSIX_FAILURE;
                }
            }
        }

        /* Bytes moved from starting position */
        *pu4Offset = *pu4Offset + (pLsppEchoMsg->LsppIpHeader.u1Hlen +
                                   LSPP_UDP_HDR_LEN);

        if (u1HdrType == LSPP_ENCAP_IPV4_HDR)
        {
            pLsppEchoMsg->u1EncapType = LSPP_ENCAP_TYPE_IP;
        }
        else if (pLsppEchoMsg->LsppAchInfo.AchHeader.u2ChannelType ==
                 LSPP_ACH_CHNL_TYPE_IPV4)
        {
            pLsppEchoMsg->u1EncapType = LSPP_ENCAP_TYPE_ACH_IP;
        }
    }
    else if ((u1HdrType == LSPP_ENCAP_IPV6_HDR) ||
             (pLsppEchoMsg->LsppAchInfo.AchHeader.u2ChannelType ==
              LSPP_ACH_CHNL_TYPE_IPV6))
    {
        /* Currently IPv6 is not supported */
        LSPP_LOG (pLsppEchoMsg->u4ContextId,
                  LSPP_PKT_RECEVIED_WITH_INVALID_ENCAP, "LsppRxProcessMplsPdu");
        return OSIX_FAILURE;
    }

    /* The label Stack may contain RAL. This cannot be validated 
     * in LSP Ping Core. So updating the stack without RAL label.
     */
    LsppRxUpdateLabelStack (pLsppEchoMsg, &pLsppEchoMsg->u1LabelStackDepth);

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxDecodeValidateLsppHeader   
 * DESCRIPTION      : This function is to Parse and Validate LSP Ping Header 
 * Input(s)         : pLsppEchoMsg - pointer to the ecjo message structure.
 *                    pPkt - Packet buffer
 *                    pu1ValidationResult - Pointer to store Validation result
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 **************************************************************************/
PUBLIC INT4
LsppRxDecodeValidateLsppHeader (tLsppEchoMsg * pLsppEchoMsg, UINT1 **ppPkt,
                                UINT1 *pu1ValidationResult)
{
    UINT1               u1MsgInvalid = 0;

    /* Fetch LSP Ping Header */
    LsppRxDecodeHeader (ppPkt, &(pLsppEchoMsg->LsppPduInfo.LsppHeader));

    if (LsppRxValidateLsppHeader (pLsppEchoMsg, &u1MsgInvalid) != OSIX_SUCCESS)
    {
        /* This u1MsgInvalid flag will be set in the validate function 
         * only if message is invalid for both packet types. 
         * The reply packets will be dropped when the validation fails. 
         * On some failure conditions, the request packet need not be 
         * dropped but reply sent with 'malformed request' as return code.
         * */
        if ((u1MsgInvalid == 1) ||
            (pLsppEchoMsg->LsppPduInfo.LsppHeader.u1MessageType ==
             LSPP_ECHO_REPLY))
        {
            *pu1ValidationResult = LSPP_DROP_PACKET;
            return OSIX_FAILURE;
        }

        *pu1ValidationResult = LSPP_MALFORMED_ECHO_PACKET;

        LSPP_LOG (pLsppEchoMsg->u4ContextId, LSPP_HEADER_VALIDATION_FAILED,
                  "LsppRxDecodeValidateLsppHeader", *pu1ValidationResult);

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxIsTlvSupported  
 * DESCRIPTION      : This function is to verify whether the given TLV type  
 *                    is supported or not. 
 * Input(s)         : u4ContextId - uniquelly identifies the context
 *                    u2TlvType - Type of the TLV to be verified
 *                    u2SubTlvType - Type of the sub-tlv to be verified
 *                    pu2TlvLength - length of the TLV 
 * OUTPUT           : pu2TlvLength - updated length with pad bytes
 * RETURNS          : NONE
 **************************************************************************/
PUBLIC INT4
LsppRxIsTlvSupported (UINT4 u4ContextId, UINT2 u2TlvType, UINT2 u2SubTlvType,
                      UINT2 *pu2TlvLength)
{
    UINT1               u1PadBytesCount = 0;

    /* Calculating the Pad bytes that may be present in the packet.
     * If tlv length specifies that the tlv is 4 byte alligned,
     * then pad bytes will not be present. Else add the pad bytes count 
     * to the total tlv length */
    u1PadBytesCount = (UINT1) (*pu2TlvLength % LSPP_BYTE_PAD_MAX_LEN);
    if (u1PadBytesCount != 0)
    {
        /* Adding Pad Bytes to tlv length */
        *pu2TlvLength = (UINT2) ((*pu2TlvLength) +
                                 (LSPP_BYTE_PAD_MAX_LEN - u1PadBytesCount));
    }

    /* TLV types are verified. */
    switch (u2TlvType)
    {
        case LSPP_TLV_TYPE_TARGET_FEC_STACK:
            /* Sub-tlv types are verified. */
            switch (u2SubTlvType)
            {
                case LSPP_FEC_RSVP_IPV4:
                case LSPP_FEC_RSVP_IPV6:
                case LSPP_FEC_128_DEPRECATED_PWFEC:
                case LSPP_FEC_128_PWFEC:
                case LSPP_FEC_129_PWFEC:
                case LSPP_FEC_NIL:
                case LSPP_FEC_STATIC_LSP:
                case LSPP_FEC_STATIC_PW:

                    break;

                    /* Explicit Fall through. At present we are not supporting
                     * any of these TLVs. So if any TLV falls in these type
                     * request type is checked. If it is echo reply, the pakcet
                     * is droped. If it is a echo request, return code is
                     * set as TLV not understood and send */

                case LSPP_FEC_LDP_IPV4:
                case LSPP_FEC_LDP_IPV6:
                case LSPP_FEC_VPN_IPV4:
                case LSPP_FEC_VPN_IPV6:
                case LSPP_FEC_L2VPN_ENDPOINT:
                case LSPP_FEC_BGP_IPV4:
                case LSPP_FEC_BGP_IPV6:
                case LSPP_FEC_GENERIC_IPV4:
                case LSPP_FEC_GENERIC_IPV6:
                case LSPP_TLV_TYPE_VENDOR_NUMBER:

                default:

                    LSPP_LOG (u4ContextId, LSPP_UNKNOWN_SUBTLV_RECEIVED,
                              "LsppRxIsTlvSupported", u2TlvType);

                    if (u2SubTlvType >= LSPP_MIN_OPTIONAL_TLV_TYPE)
                    {
                        /* Ignore the optional TLVs that are not supported. */
                        return OSIX_SUCCESS;
                    }

                    /* Unsupported Mandatory TLVs in the request should be 
                     * reported in the echo reply and hence returning failure.*/
                    return OSIX_FAILURE;
            }

            break;

        case LSPP_TLV_TYPE_DSMAP:
        case LSPP_TLV_TYPE_IF_LBL_STACK:
        case LSPP_TLV_TYPE_PAD:
        case LSPP_TLV_TYPE_REPLY_TOS_BYTE:
        case LSPP_TLV_TYPE_ERRORED:
        case LSPP_TLV_TYPE_BFD_DISCRIMINATOR:
            break;

        default:
            LSPP_LOG (u4ContextId, LSPP_UNKNOWN_SUBTLV_RECEIVED,
                      "LsppRxIsTlvSupported", u2TlvType);
            if (u2TlvType >= LSPP_MIN_OPTIONAL_TLV_TYPE)
            {
                /* Ignore the optional TLVs that are not supported. */
                return OSIX_SUCCESS;
            }

            /* Unsupported Mandatory TLVs in the request should be 
             * reported in the echo reply and hence returning failure. */
            return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxCheckTlvsSupported  
 * DESCRIPTION      : This function is to verify whether all the TLVs 
 *                    received in the echo message are supported or not. 
 * Input(s)         : ppPkt - Packet buffer
 *                    u4ContextId - uniquelly identifies the context
 *                    u4PktSize - total packet size
 *                    u4Offset - Bytes read from starting position
 *                    pu1Result - Pointer to store Validation result
 *                    pLsppPduInfo - pointer to LSPP pdu info structure.
 * OUTPUT           : NONE
 * RETURNS          : NONE
 **************************************************************************/

PUBLIC VOID
LsppRxCheckTlvsSupported (UINT1 **ppPkt, UINT4 u4ContextId,
                          UINT4 u4PktSize, UINT4 u4Offset,
                          UINT1 *pu1Result, tLsppPduInfo * pLsppPduInfo)
{
    UINT2               u2TlvType = 0;
    UINT2               u2SubTlvType = 0;
    UINT2               u2SubTlvLength = 0;
    UINT2               u2TlvLength = 0;

    while (u4Offset < u4PktSize)
    {
        /* Reinitailizing the tlv type and TLV subtype and length 
         * on each iteration */
        u2TlvType = 0;
        u2TlvLength = 0;
        u2SubTlvType = 0;

        /* Fetch TLV Type and Length. */
        LSPP_GET_2BYTE (u2TlvType, *ppPkt);
        LSPP_GET_2BYTE (u2TlvLength, *ppPkt);

        /* Bytes moved from starting position */
        u4Offset = u4Offset + LSPP_TLV_HEADER_LEN + u2TlvLength;

        if (u2TlvType == LSPP_TLV_TYPE_TARGET_FEC_STACK)
        {
            /* Loop till target fec stack length */
            while (u2TlvLength > 0)
            {
                u2SubTlvLength = 0;
                u2SubTlvType = 0;

                /* Fetch Sub-Tlv Type and Length. */
                LSPP_GET_2BYTE (u2SubTlvType, *ppPkt);
                LSPP_GET_2BYTE (u2SubTlvLength, *ppPkt);

                if (LsppRxIsTlvSupported (u4ContextId, u2TlvType, u2SubTlvType,
                                          &u2SubTlvLength) != OSIX_SUCCESS)
                {
                    *ppPkt = *ppPkt - LSPP_TLV_HEADER_LEN;

                    *pu1Result = LSPP_TLV_NOT_UNDERSTOOD;

                    LSPP_LOG (u4ContextId, LSPP_UNKNOWN_SUBTLV_RECEIVED,
                              "LsppRxCheckTlvsSupported", u2SubTlvType);

                    LsppRxUpdateRcAndErrorTlv (ppPkt, u2SubTlvLength,
                                               &(pLsppPduInfo->LsppErrorTlv),
                                               &(pLsppPduInfo->u2TlvsPresent));
                }

                u2TlvLength = (UINT2) (u2TlvLength - (u2SubTlvLength +
                                                      LSPP_TLV_HEADER_LEN));

                /* Move the buffer pointer to read the next sub-TLV. */
                *ppPkt = *ppPkt + u2SubTlvLength;
            }
            /* Fetch the next TLV after the FEC Stack TLV and verify. */
            continue;
        }

        if (LsppRxIsTlvSupported (u4ContextId, u2TlvType, u2SubTlvType,
                                  &u2TlvLength) != OSIX_SUCCESS)
        {
            *ppPkt = *ppPkt - LSPP_TLV_HEADER_LEN;

            *pu1Result = LSPP_TLV_NOT_UNDERSTOOD;

            LSPP_LOG (u4ContextId, LSPP_UNKNOWN_SUBTLV_RECEIVED,
                      "LsppRxCheckTlvsSupported", u2TlvType);

            LsppRxUpdateRcAndErrorTlv (ppPkt, u2TlvLength,
                                       &(pLsppPduInfo->LsppErrorTlv),
                                       &(pLsppPduInfo->u2TlvsPresent));
        }
        /* Move the buffer pointer to read the next TLV. */
        *ppPkt = *ppPkt + u2TlvLength;
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxParseValidatePdu   
 * DESCRIPTION      : This function is to Parse and Validate LSP Ping Pdu 
 * Input(s)         : pPkt - Packet buffer
 *                    pLsppPduInfo - Pdu strucutre where tlv needs to be filled
 *                    u4ContextId - uniquelly identifies the context
 *                    u4PktSize - total packet size
 *                    u4Offset - Bytes read from starting position
 *                    pu1Result - Pointer to store Validation result
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 **************************************************************************/
PUBLIC INT4
LsppRxParseValidatePdu (UINT1 **ppPkt, tLsppPduInfo * pLsppPduInfo,
                        UINT4 u4ContextId, UINT4 u4PktSize,
                        UINT4 u4Offset, UINT1 *pu1Result)
{
    UINT1              *pPkt = NULL;
    UINT2               u2TlvType = 0;
    UINT2               u2SubTlvType = 0;
    UINT2               u2SubTlvLength = 0;
    UINT2               u2TlvLength = 0;
    UINT2               u2ReadBytes = 0;
    UINT1               u1FecStackDepth = 0;
    UINT1               u1ReturnCode = 0;
    UINT1               u1MsgType = 0;
    UINT1               u1DsmapPresent = OSIX_FALSE;

    u1MsgType = pLsppPduInfo->LsppHeader.u1MessageType;

    /*Get the base address of the received packet for 
     * further processing. */
    pPkt = *ppPkt;

    /* Check whether any tlv in the received echo message 
     * is not understood.*/
    LsppRxCheckTlvsSupported (ppPkt, u4ContextId,
                              u4PktSize, u4Offset, pu1Result, pLsppPduInfo);
    if (*pu1Result == LSPP_TLV_NOT_UNDERSTOOD)
    {
        return OSIX_FAILURE;
    }

    /* Reassign the original address for decoding the packet 
     * after the support of the received TLVs are verified.*/
    *ppPkt = pPkt;

    /* Decode and Validate the all the TLVs in the received message. */
    while (u4Offset < u4PktSize)
    {
        /* Reinitailizing the tlv type and TLV subtype and length 
         * on each iteration */

        u2TlvType = 0;
        u2TlvLength = 0;

        /* Fetch TLV Type */
        LSPP_GET_2BYTE (u2TlvType, *ppPkt);

        /* Fetch TLV Length */
        LSPP_GET_2BYTE (u2TlvLength, *ppPkt);
/*
        if ((u2TlvLength == 0) || (u2TlvType == 0))
        {
            *pu1Result = LSPP_DROP_PACKET;
            return OSIX_FAILURE;
        }
*/
        /* Bytes moved from starting position */
        u4Offset = u4Offset + LSPP_TLV_HEADER_LEN + u2TlvLength;

        if (u2TlvType == LSPP_TLV_TYPE_TARGET_FEC_STACK)
        {
            /* Filling Length Value in Structure */
            pLsppPduInfo->u2FecStackTlvLength = u2TlvLength;

            /* Loop till target fec stack length */
            while (u2TlvLength > 0)
            {
                u2SubTlvLength = 0;
                u2SubTlvType = 0;

                /* Fetch Sub-Tlv Type */
                LSPP_GET_2BYTE (u2SubTlvType, *ppPkt);

                /* Fetch Sub-Tlv Length */
                LSPP_GET_2BYTE (u2SubTlvLength, *ppPkt);

                if (LsppRxDecodeValidatePdu (ppPkt, u2TlvType, u2SubTlvType,
                                             pLsppPduInfo, u4ContextId,
                                             u1FecStackDepth,
                                             u1MsgType, &u2SubTlvLength,
                                             &u1ReturnCode, &u2ReadBytes) !=
                    OSIX_SUCCESS)
                {
                    *ppPkt = *ppPkt - u2ReadBytes;

                    LSPP_LOG (u4ContextId, LSPP_DECODING_TLV_FAILED,
                              "LsppRxParseValidatePdu");

                    *pu1Result = LSPP_MALFORMED_ECHO_PACKET;

                    LsppRxUpdateRcAndErrorTlv (ppPkt, u2SubTlvLength,
                                               &(pLsppPduInfo->LsppErrorTlv),
                                               &(pLsppPduInfo->u2TlvsPresent));

                    return OSIX_FAILURE;
                }

                u2TlvLength = (UINT2) (u2TlvLength - (u2SubTlvLength +
                                                      LSPP_TLV_HEADER_LEN));
                u1FecStackDepth++;
            }
            /* Fetch the next TLV after the FEC Stack TLV is decoded. */
            continue;
        }

        /* The packet is considered to be malformed when any of the 
         * following conditions are met.
         * 1. More than One DSMAP TLV present in the request.
         * 2. Interface and Label stack TLV present in the request.
         * */
        if (u1MsgType == LSPP_ECHO_REQUEST)
        {
            if (((u2TlvType == LSPP_TLV_TYPE_DSMAP) &&
                 (u1DsmapPresent != OSIX_FALSE)) ||
                (u2TlvType == LSPP_TLV_TYPE_IF_LBL_STACK))
            {
                *ppPkt = *ppPkt - LSPP_TLV_HEADER_LEN;

                /* Set Return Code as Malfromed */
                *pu1Result = LSPP_MALFORMED_ECHO_PACKET;

                LSPP_LOG (u4ContextId, LSPP_MALFORMED_REQ_RECEIVED,
                          "LsppRxParseValidatePdu");

                LsppRxUpdateRcAndErrorTlv (ppPkt, u2TlvLength,
                                           &(pLsppPduInfo->LsppErrorTlv),
                                           &(pLsppPduInfo->u2TlvsPresent));
                continue;
            }
        }

        /* Decode PDU and Validate */
        if (LsppRxDecodeValidatePdu (ppPkt, u2TlvType, u2SubTlvType,
                                     pLsppPduInfo, u4ContextId,
                                     u1FecStackDepth, u1MsgType,
                                     &u2TlvLength, &u1ReturnCode,
                                     &u2ReadBytes) != OSIX_SUCCESS)
        {
            *ppPkt = *ppPkt - u2ReadBytes;

            LSPP_LOG (u4ContextId, LSPP_DECODING_TLV_FAILED,
                      "LsppRxParseValidatePdu");

            /* Check whether any fields inside the TLV is not understood.
             * Update the validation result as TLV NOT UNDERSTOOD for 
             * unsupported fields in TLV else update result as MALFORMED PKT.*/
            if (u1ReturnCode == LSPP_TLV_NOT_UNDERSTOOD)
            {
                *pu1Result = LSPP_TLV_NOT_UNDERSTOOD;
            }
            else
            {
                *pu1Result = LSPP_MALFORMED_ECHO_PACKET;
            }

            LsppRxUpdateRcAndErrorTlv (ppPkt, u2TlvLength,
                                       &(pLsppPduInfo->LsppErrorTlv),
                                       &(pLsppPduInfo->u2TlvsPresent));
            return OSIX_FAILURE;
        }

        if (u2TlvType == LSPP_TLV_TYPE_DSMAP)
        {
            /* Indicates DSMAP TLV is already present */
            u1DsmapPresent = OSIX_TRUE;
        }
    }
    pLsppPduInfo->u1FecStackDepth = u1FecStackDepth;

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxUpdateRcAndErrorTlv
 * DESCRIPTION      : This funciton is to udpate the validation result and
 *                    fill error TLV 
 * Input(s)         : ppPkt - packet buffer 
 *                    u2TlvLength - length of the TLV to be copied in Errored
 *                    TLV
 *                    pErrorTlv   - pointer to error tlv structure.
 *                    pu2TlvPresent - bitmask to be set for Errored TLV 
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 **************************************************************************/
PUBLIC VOID
LsppRxUpdateRcAndErrorTlv (UINT1 **ppPkt, UINT2 u2TlvLength,
                           tLsppErrorTlv * pErrorTlv, UINT2 *pu2TlvPresent)
{
    u2TlvLength = (UINT2) (u2TlvLength + LSPP_TLV_HEADER_LEN);

    /* Copying the TLV content */
    LSPP_GET_NBYTE ((&(pErrorTlv->au1Value) +
                     pErrorTlv->u2ErrorTLVLength), *ppPkt, u2TlvLength);

    pErrorTlv->u2ErrorTLVLength =
        (UINT2) (pErrorTlv->u2ErrorTLVLength + u2TlvLength);

    *pu2TlvPresent = *pu2TlvPresent | LSPP_ERRORED_TLV;

}

/***************************************************************************
 * FUNCTION NAME    : LsppRxValidateIfLblStkTlv   
 * DESCRIPTION      : This function is to validate If and Label Stack TLV
 * Input(s)         : pLsppIfLblStkTlv - Pointer to interface and label stack 
 *                                       structure
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 **************************************************************************/
PUBLIC INT4
LsppRxValidateIfLblStkTlv (UINT4 u4ContextId,
                           tLsppIfLblStkTlv * pLsppIfLblStkTlv)
{
    UINT1               u1LabelCount = 0;
    /* Validating Label Stack TLV's Address Type */
    if (LSPP_IF_LBL_STK_ADDR_TYPE >= LSPP_MAX_ADDR_TYPE)
    {
        LSPP_LOG (u4ContextId, LSPP_INVALID_IF_LBL_STK_ADDR_TYPE,
                  "LsppRxValidateIfLblStkTlv", LSPP_IF_LBL_STK_ADDR_TYPE);

        return OSIX_FAILURE;
    }

    if (pLsppIfLblStkTlv->u1LblStackDepth > LSPP_MAX_LABEL_STACK)
    {
        return OSIX_FAILURE;
    }

    while (u1LabelCount < pLsppIfLblStkTlv->u1LblStackDepth)
    {
        if (pLsppIfLblStkTlv->RxLblInfo[u1LabelCount].u1Protocol >
            LSPP_PROTOCOL_RSVP_TE)
        {
            return OSIX_FAILURE;
        }
        u1LabelCount++;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxValidatePadTlv   
 *
 * DESCRIPTION      : This function is to validate Pad TLV
 *
 * Input(s)         : u4ContextId - Context Id
 *                    u1ReqType -   Echo Requset/Reply
 *                    pLsppPadTlv - Received Pad Tlv content
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 * 
 **************************************************************************/
PUBLIC INT4
LsppRxValidatePadTlv (UINT4 u4ContextId, UINT1 u1ReqType,
                      tLsppPadTlv * pLsppPadTlv)
{
    /* Validating First Octet of Pad TLV */
    if ((LSPP_PAD_TLV_FIRST_OCTET != LSPP_COPY_PAD_TLV) &&
        ((LSPP_PAD_TLV_FIRST_OCTET != LSPP_DROP_PAD_TLV)))
    {
        if (u1ReqType == LSPP_ECHO_REQUEST)
        {
            LSPP_LOG (u4ContextId, LSPP_INVALID_PAD_TLV_FIRST_OCTET_VAL,
                      "LsppRxValidatePadTlv", LSPP_PAD_TLV_FIRST_OCTET);
        }
        else if (u1ReqType == LSPP_ECHO_REPLY)
        {
            LSPP_LOG (u4ContextId, LSPP_INVALID_PAD_TLV_FIRST_OCTET_VAL_IN_RPLY,
                      "LsppRxValidatePadTlv", LSPP_PAD_TLV_FIRST_OCTET);

        }
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxValidateReplyTosTlv   
 *
 * DESCRIPTION      : This function is to validate Reply TOS TLV
 *
 * Input(s)         : u4ContextId - Context ID
 *                    pLsppReplyTosTlv - Received Reply TOS Tlv content
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 * 
 **************************************************************************/
PUBLIC INT4
LsppRxValidateReplyTosTlv (UINT4 u4ContextId,
                           tLsppReplyTosTlv * pLsppReplyTosTlv)
{
    /* Validating Repy tos value */
    if (LSPP_REPLY_TOS_VALUE > LSPP_MAX_REPLY_TOS_VALUE)
    {
        LSPP_LOG (u4ContextId, LSPP_INVALID_REPLY_TOS_VAL,
                  "LsppRxValidateReplyTosTlv", LSPP_REPLY_TOS_VALUE);
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxValidateDSMAPTlv   
 *
 * DESCRIPTION      : This function is to validate DSMAP TLV
 *
 * Input(s)         :      
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 * 
 **************************************************************************/
PUBLIC INT4
LsppRxValidateDsmapTlv (UINT4 u4ContextId, tLsppDsMapTlv * pLsppDsMapTlv,
                        UINT1 *pu1ReturnCode)
{
    UINT1               u1LabelCount = 0;

    /* Validating Address type */
    if (LSPP_DSMAP_TLV_ADDR_TYPE >= LSPP_MAX_ADDR_TYPE)
    {
        LSPP_LOG (u4ContextId, LSPP_INVALID_DSMAP_TLV_ADDR_TYPE,
                  "LsppRxValidateDsmapTlv", LSPP_DSMAP_TLV_ADDR_TYPE);
        return OSIX_FAILURE;
    }

    /* Validating DS Flag */
    if ((LSPP_DSMAP_TLV_DSFLAG != LSPP_INTERFACE_AND_LBL_STACK_REQ) &&
        (LSPP_DSMAP_TLV_DSFLAG != 0))
    {
        *pu1ReturnCode = LSPP_TLV_NOT_UNDERSTOOD;

        LSPP_LOG (u4ContextId, LSPP_INVALID_DSMAP_TLV_DSFLAG,
                  "LsppRxValidateDsmapTlv", LSPP_DSMAP_TLV_DSFLAG);
        return OSIX_FAILURE;
    }

    /* Validating Multipath TYPE */
    if (LSPP_DSMAP_TLV_MULTIPATH_TYPE != 0)
    {
        *pu1ReturnCode = LSPP_TLV_NOT_UNDERSTOOD;

        LSPP_LOG (u4ContextId, LSPP_INVALID_DSMAP_MULTIPATH_TYPE,
                  "LsppRxValidateDsmapTlv", LSPP_DSMAP_TLV_MULTIPATH_TYPE);
        return OSIX_FAILURE;
    }

    /* Validating Multipath TYPE Length */
    if (LSPP_DSMAP_TLV_MULTIPATH_LENGTH != 0)
    {
        *pu1ReturnCode = LSPP_TLV_NOT_UNDERSTOOD;

        LSPP_LOG (u4ContextId, LSPP_INVALID_DSMAP_MULTIPATH_LEN,
                  "LsppRxValidateDsmapTlv", LSPP_DSMAP_TLV_MULTIPATH_LENGTH);
        return OSIX_FAILURE;
    }

    if (pLsppDsMapTlv->u1DsLblStackDepth > LSPP_MAX_LABEL_STACK)
    {
        return OSIX_FAILURE;
    }

    while (u1LabelCount < pLsppDsMapTlv->u1DsLblStackDepth)
    {
        if (pLsppDsMapTlv->DsLblInfo[u1LabelCount].u1Protocol >
            LSPP_PROTOCOL_RSVP_TE)
        {
            return OSIX_FAILURE;
        }
        u1LabelCount++;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxGetMplsLabel   
 * DESCRIPTION      : This function is to parse the packet and fetch the 
 *                    MPLS Label
 * Input(s)         : pPkt - Linear buffer with received contents
 *                    pLsppLblInfo - Pointer to label Info Strucuture to store
 *                                   the received labels
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 **************************************************************************/
VOID
LsppRxGetMplsLabel (UINT1 **ppPkt, tLsppLblInfo * pLsppLblInfo)
{
    UINT4               u4Hdr;

    LSPP_GET_4BYTE (u4Hdr, *ppPkt);

    pLsppLblInfo->u4Label =
        (u4Hdr & LSPP_MPLS_LABEL_MASK) >> LSPP_MPLS_LABEL_SHIFT_FACTOR;

    pLsppLblInfo->u1Exp =
        (UINT1) ((u4Hdr & LSPP_MPLS_LABEL_EXP_MASK) >>
                 LSPP_MPLS_LABEL_EXP_SHIFT_FACTOR);

    pLsppLblInfo->u1SI =
        (UINT1) ((u4Hdr & LSPP_MPLS_LABEL_SI_MASK) >>
                 LSPP_MPLS_LABEL_SI_SHIFT_FACTOR);

    pLsppLblInfo->u1Ttl = (UINT1) (u4Hdr & LSPP_MPLS_LABEL_TTL_MASK);
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxValidateLsppHeader
 * DESCRIPTION      : This function is to validate LSP Ping Header 
 * Input(s)         : pLsppEchoMsg - Pointer to Echo message struture 
 *                                   containing LSP Ping Header.     
 *                    pu1MsgInvalid - This flag is to 
 *                    indicate if message is corrupted or not valid
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 **************************************************************************/
PUBLIC INT4
LsppRxValidateLsppHeader (tLsppEchoMsg * pLsppEchoMsg, UINT1 *pu1MsgInvalid)
{
    tLsppHeader        *pLsppHeader = NULL;

    pLsppHeader = &(pLsppEchoMsg->LsppPduInfo.LsppHeader);

    /* Validating Message Type */
    if ((pLsppHeader->u1MessageType != LSPP_ECHO_REQUEST) &&
        (pLsppHeader->u1MessageType != LSPP_ECHO_REPLY))
    {
        *pu1MsgInvalid = 1;

        LSPP_LOG (pLsppEchoMsg->u4ContextId, LSPP_INVALID_MESSAGE_TYPE,
                  "LsppRxValidateLsppHeader", pLsppHeader->u1MessageType);
        return OSIX_FAILURE;
    }

    /* Checking Version Field */
    if (pLsppHeader->u2Version != LSPP_HEADER_VERSION)
    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId, LSPP_INVALID_PROTOCOL_VERSION,
                  "LsppRxValidateLsppHeader", pLsppHeader->u2Version);
        return OSIX_FAILURE;
    }

    /* Validating Global Flags */
    if (!((pLsppHeader->u2GlobalFlag == 0) ||
          ((pLsppHeader->u1MessageType == LSPP_ECHO_REQUEST) &&
           (pLsppHeader->u2GlobalFlag & LSPP_FEC_VALIDATE)) ||
          ((pLsppHeader->u1MessageType == LSPP_ECHO_REQUEST) &&
           (pLsppHeader->u2GlobalFlag & LSPP_REVERSE_PATH_VALIDATE))))
    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId, LSPP_INVALID_GLOBAL_FLAG,
                  "LsppRxValidateLsppHeader", pLsppHeader->u2GlobalFlag);
        return OSIX_FAILURE;
    }

    /* Validating Reply Mode. */
    if ((pLsppHeader->u1ReplyMode != LSPP_NO_REPLY) &&
        (pLsppHeader->u1ReplyMode != LSPP_REPLY_IP_UDP) &&
        (pLsppHeader->u1ReplyMode != LSPP_REPLY_IP_UDP_ROUTER_ALERT) &&
        (pLsppHeader->u1ReplyMode != LSPP_REPLY_APPLICATION_CC))

    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId, LSPP_INVALID_REPLY_MODE,
                  "LsppRxValidateLsppHeader", pLsppHeader->u1ReplyMode);
        return OSIX_FAILURE;
    }

    /* Validate Reply mode against the received encapsulation type 
     * for the echo request packet. The packets received with invalid 
     * reply modes for the encapsulation are dropped.*/
    if ((pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_ACH) &&
        (pLsppHeader->u1MessageType == LSPP_ECHO_REQUEST) &&
        ((pLsppHeader->u1ReplyMode == LSPP_REPLY_IP_UDP) ||
         (pLsppHeader->u1ReplyMode == LSPP_REPLY_IP_UDP_ROUTER_ALERT)))
    {
        *pu1MsgInvalid = 1;

        LSPP_LOG (pLsppEchoMsg->u4ContextId, LSPP_INVALID_REPLY_MODE_FOR_ENCAP,
                  "LsppRxValidateLsppHeader", pLsppHeader->u1ReplyMode,
                  pLsppEchoMsg->u1EncapType);
        return OSIX_FAILURE;
    }

    /* Validating Return code and Return Subcode Validation. */
    if (pLsppHeader->u1MessageType == LSPP_ECHO_REPLY)
    {
        /* Check RC and RSC for Echo reply received. */
        if ((pLsppHeader->u1ReturnCode >= LSPP_MAX_RETURN_CODE) ||
            (pLsppHeader->u1ReturnSubCode > LSPP_MAX_LABEL_STACK))
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_INVALID_REPLY_WITH_RC_OR_RSC,
                      "LsppRxValidateLsppHeader",
                      pLsppHeader->u1ReturnCode, pLsppHeader->u1ReturnSubCode);
            return OSIX_FAILURE;
        }
    }
    else
    {
        /* Check RC and RSC for Echo request received. */
        if ((pLsppHeader->u1ReturnCode != 0) ||
            (pLsppHeader->u1ReturnSubCode != 0))
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_INVALID_REQ_WITH_RC_OR_RSC,
                      "LsppRxValidateLsppHeader",
                      pLsppHeader->u1ReturnCode, pLsppHeader->u1ReturnSubCode);
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxDecodeValidatePdu   
 * DESCRIPTION      : This function is used to Decode the LSP Ping Pdu
 * Input(s)         : pPkt - Packet Buffer
 *                    u2TlvType - Tlv type
 *                    u2SubTlvType - Target FEC stack TLV subtype
 *                    pLsppPduInfo - pointer to the PDU info structure 
 *                    u1FecStackDepth - Specifies the FEC stack depth
 *                    u2TlvLength - TLv length
 *                    pu1ReturnCode - validation result
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 **************************************************************************/
PUBLIC INT4
LsppRxDecodeValidatePdu (UINT1 **ppPkt, UINT2 u2TlvType, UINT2 u2SubTlvType,
                         tLsppPduInfo * pLsppPduInfo, UINT4 u4ContextId,
                         UINT1 u1FecStackDepth, UINT1 u1MsgType,
                         UINT2 *pu2TlvLength, UINT1 *pu1ReturnCode,
                         UINT2 *pu2ReadBytes)
{
    LSPP_LOG (u4ContextId, LSPP_DECODE_TLV,
              "LsppRxDecodeValidatePdu", u2TlvType, *pu2TlvLength);

    switch (u2TlvType)
    {
        case LSPP_TLV_TYPE_TARGET_FEC_STACK:

            /* Filling stack tlv length and type */
            pLsppPduInfo->LsppFecTlv[u1FecStackDepth].u2TlvType = u2SubTlvType;

            /* Set the bit to indicate that the Target FEC stack tlv is present 
             * in the received packet */
            pLsppPduInfo->u2TlvsPresent =
                (pLsppPduInfo->u2TlvsPresent | LSPP_TARGET_FEC_TLV);

            switch (u2SubTlvType)
            {

                case LSPP_FEC_LDP_IPV4:

                    LsppRxDecodeIpv4LdpFec (u4ContextId, ppPkt,
                                            LSPP_LDP_FEC_TLV_BASEADDR
                                            (u1FecStackDepth));

                    /* Adding the Pad Bytes also to the total TLV len */
                    *pu2TlvLength = (UINT2) ((*pu2TlvLength) +
                                             LSPP_FEC_LDP_IPV4_PAD_BYTES);
                    break;

                case LSPP_FEC_LDP_IPV6:

                    LsppRxDecodeIpv6LdpFec (u4ContextId, ppPkt,
                                            LSPP_LDP_FEC_TLV_BASEADDR
                                            (u1FecStackDepth));

                    /* Adding the Pad Bytes also to the total TLV len */
                    *pu2TlvLength = (UINT2) ((*pu2TlvLength) +
                                             LSPP_FEC_LDP_IPV6_PAD_BYTES);
                    break;

                case LSPP_FEC_RSVP_IPV4:

                    LsppRxDecodeIpv4RsvpFec (u4ContextId, ppPkt,
                                             LSPP_RSVP_FEC_TLV_BASEADDR
                                             (u1FecStackDepth));
                    break;

                case LSPP_FEC_RSVP_IPV6:

                    LsppRxDecodeIpv6RsvpFec (u4ContextId, ppPkt,
                                             LSPP_RSVP_FEC_TLV_BASEADDR
                                             (u1FecStackDepth));
                    break;

                case LSPP_FEC_128_DEPRECATED_PWFEC:

                    LsppRxDecodeFec128DeprecatePwFec
                        (u4ContextId, ppPkt,
                         LSPP_DEPREC_PW_TLV_BASEADDR (u1FecStackDepth));
                    /* Adding the Pad Bytes length also to the total 
                     * TLV length */
                    *pu2TlvLength =
                        (UINT2) (*pu2TlvLength +
                                 LSPP_FEC_128_DEPRECATED_PWFEC_PAD_BYTES);
                    break;

                case LSPP_FEC_128_PWFEC:

                    LsppRxDecodeFec128Pw (u4ContextId, ppPkt,
                                          LSPP_FEC128_PW_TLV_BASEADDR
                                          (u1FecStackDepth));
                    /* Adding the Pad Bytes also to the total TLV len */
                    *pu2TlvLength = (UINT2) (*pu2TlvLength +
                                             LSPP_FEC_128_PWFEC_PAD_BYTES);
                    break;

                case LSPP_FEC_129_PWFEC:

                    LsppRxDecodeFec129Pw (u4ContextId, ppPkt,
                                          LSPP_FEC129_PW_TLV_BASEADDR
                                          (u1FecStackDepth), pu2TlvLength);
                    break;

                case LSPP_FEC_NIL:

                    LsppRxDecodeNilFec (u4ContextId, ppPkt,
                                        LSPP_NIL_FEC_TLV_BASEADDR
                                        (u1FecStackDepth));
                    break;

                case LSPP_FEC_STATIC_LSP:

                    LsppRxDecodeStaticLspFec (u4ContextId, ppPkt,
                                              LSPP_STATIC_LSP_FEC_TLV_BASEADDR
                                              (u1FecStackDepth));

                    /* Adding the Pad Bytes also to the total TLV len */
                    *pu2TlvLength = (UINT2) ((*pu2TlvLength) +
                                             LSPP_FEC_STATIC_LSP_PAD_BYTES);

                    break;

                case LSPP_FEC_STATIC_PW:

                    LsppRxDecodeStaticPwFec (u4ContextId, ppPkt,
                                             LSPP_STATIC_PW_FEC_TLV_BASEADDR
                                             (u1FecStackDepth), pu2TlvLength);

                    break;

                default:
                    *pu2ReadBytes = LSPP_TLV_HEADER_LEN;

                    LSPP_LOG (u4ContextId, LSPP_INVALID_SUBTYPE_RECEIVED,
                              "LsppRxDecodeValidatePdu", u2SubTlvType);

                    if (u2SubTlvType >= LSPP_MIN_OPTIONAL_TLV_TYPE)
                    {
                        /* Ignore the optional TLVs that are not supported. */
                        return OSIX_SUCCESS;
                    }

                    return OSIX_FAILURE;
            }
            *pu2ReadBytes = *pu2TlvLength;

            break;

        case LSPP_TLV_TYPE_DSMAP:

            if (LsppRxDecodeDsmapTlv (u4ContextId, ppPkt,
                                      LSPP_DSMAP_TLV_BASEADDR,
                                      pu2ReadBytes) != OSIX_SUCCESS)
            {
                LSPP_LOG (u4ContextId, LSPP_INVALID_DSMAP_TLV_ADDR_TYPE,
                          "LsppRxValidateDsmapTlv",
                          pLsppPduInfo->LsppDsMapTlv.u1AddrType);

                return OSIX_FAILURE;
            }

            if (LsppRxValidateDsmapTlv (u4ContextId,
                                        LSPP_DSMAP_TLV_BASEADDR,
                                        pu1ReturnCode) != OSIX_SUCCESS)
            {
                LSPP_LOG (u4ContextId, LSPP_DSMAP_VALIDATION_FAILED,
                          "LsppRxDecodeValidatePdu", u2TlvType);
                return OSIX_FAILURE;
            }

            /* Indicates that dsmap tlv is present in the received packet */
            pLsppPduInfo->u2TlvsPresent =
                (pLsppPduInfo->u2TlvsPresent | LSPP_DSMAP_TLV);

            break;

        case LSPP_TLV_TYPE_IF_LBL_STACK:

            if (LsppRxDecodeIfLblStkTlv (u4ContextId, ppPkt,
                                         LSPP_IF_LBL_STK_TLV_BASEADDR) !=
                OSIX_SUCCESS)
            {
                LSPP_LOG (u4ContextId, LSPP_INVALID_IF_LBL_STK_ADDR_TYPE,
                          "LsppRxValidateIfLblStkTlv",
                          pLsppPduInfo->LsppIfLblStkTlv.u1AddrType);

                return OSIX_FAILURE;
            }

            if (LsppRxValidateIfLblStkTlv (u4ContextId,
                                           LSPP_IF_LBL_STK_TLV_BASEADDR) !=
                OSIX_SUCCESS)
            {
                LSPP_LOG (u4ContextId, LSPP_IF_LBL_STK_VALIDATION_FAILED,
                          "LsppRxDecodeValidatePdu");
                return OSIX_FAILURE;
            }

            /*Indicates that If lbl stk tlv is present in the received packet */
            pLsppPduInfo->u2TlvsPresent =
                (pLsppPduInfo->
                 u2TlvsPresent | LSPP_INTERFACE_AND_LBL_STACK_TLV);
            break;

        case LSPP_TLV_TYPE_PAD:

            if (*pu2TlvLength != 0)
            {
                LsppRxDecodePadTlv (u4ContextId, ppPkt,
                                    LSPP_PAD_TLV_BASEADDR, *pu2TlvLength);

                if (LsppRxValidatePadTlv (u4ContextId, u1MsgType,
                                          LSPP_PAD_TLV_BASEADDR) !=
                    OSIX_SUCCESS)
                {
                    LSPP_LOG (u4ContextId, LSPP_PAD_TLV_VALIDATION_FAILED,
                              "LsppRxDecodeValidatePdu");
                    return OSIX_FAILURE;
                }
            }

            /* Indicates that pad tlv is present in the received packet */
            pLsppPduInfo->u2TlvsPresent =
                (pLsppPduInfo->u2TlvsPresent | LSPP_PAD_TLV);
            break;

        case LSPP_TLV_TYPE_REPLY_TOS_BYTE:

            LsppRxDecodeReplyTosTlv (u4ContextId, ppPkt,
                                     LSPP_REPLY_TOS_TLV_BASEADDR);

            if (u1MsgType == LSPP_ECHO_REPLY)
            {
                LSPP_LOG (u4ContextId, LSPP_INVALID_REPLY_TOS_IN_REPLY,
                          "LsppRxDecodeValidatePdu");
                return OSIX_FAILURE;
            }

            if (LsppRxValidateReplyTosTlv (u4ContextId,
                                           LSPP_REPLY_TOS_TLV_BASEADDR) !=
                OSIX_SUCCESS)
            {
                LSPP_LOG (u4ContextId, LSPP_REPLY_TOS_TLV_VALDIATION_FAILED,
                          "LsppRxDecodeValidatePdu");
                return OSIX_FAILURE;
            }

            /* Indicates that reply tos tlv is present in the received packet */
            pLsppPduInfo->u2TlvsPresent =
                (pLsppPduInfo->u2TlvsPresent | LSPP_REPLY_TOS_BYTE_TLV);

            break;

        case LSPP_TLV_TYPE_ERRORED:

            LsppRxDecodeErrorTlv (u4ContextId, ppPkt,
                                  LSPP_ERROR_TLV_BASEADDR, *pu2TlvLength);

            /* Indicates that error tlv is present in the received packet */
            pLsppPduInfo->u2TlvsPresent =
                (pLsppPduInfo->u2TlvsPresent | LSPP_ERRORED_TLV);
            break;

        case LSPP_TLV_TYPE_BFD_DISCRIMINATOR:

            LsppRxDecodeBfdDiscTlv (u4ContextId, ppPkt,
                                    LSPP_BFD_FEC_TLV_BASEADDR);

            /* Indicates that bfd tlv is present in the received packet */
            pLsppPduInfo->u2TlvsPresent =
                (pLsppPduInfo->u2TlvsPresent | LSPP_BFD_DISCRIMINATOR_TLV);
            break;

        default:

            *pu2ReadBytes = LSPP_TLV_HEADER_LEN;

            LSPP_LOG (u4ContextId, LSPP_INVALID_TLV_TYPE,
                      "LsppRxDecodeValidatePdu", u2TlvType);

            if (u2SubTlvType >= LSPP_MIN_OPTIONAL_TLV_TYPE)
            {
                /* Ignore the optional TLVs that are not supported. */
                return OSIX_SUCCESS;
            }

            return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxDecodeIpv4LdpFec   
 * DESCRIPTION      : This function is to parse IPV4 LDP Prefix
 * Input(s)         : pPkt - Pointer to the buffer,
 *                    pLsppLdpFecTlv - Pointer to the LDP Target FEC stack
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 **************************************************************************/
PUBLIC VOID
LsppRxDecodeIpv4LdpFec (UINT4 u4ContextId, UINT1 **ppPkt,
                        tLsppLdpFecTlv * pLsppLdpFecTlv)
{
    /* Fill Address Type */
    pLsppLdpFecTlv->u1PrefixType = LSPP_MPLS_ADDR_TYPE_IPV4;

    /* Fetch Ip Address */
    LSPP_GET_4BYTE (pLsppLdpFecTlv->Prefix.u4_addr[0], *ppPkt);

    /* Fetch Prefix length */
    LSPP_GET_1BYTE (pLsppLdpFecTlv->u1PrefixLength, *ppPkt);

    /* Skipping Pad Bytes */
    *ppPkt = (*ppPkt + LSPP_FEC_LDP_IPV4_PAD_BYTES);

    LSPP_LOG (u4ContextId, LSPP_DECODE_IPV4_LDP_FEC,
              "LsppRxDecodeIpv4LdpFec",
              pLsppLdpFecTlv->Prefix.u4_addr[0],
              pLsppLdpFecTlv->u1PrefixLength);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxDecodeIpv6LdpFec   
 * DESCRIPTION      : This function is to parse IPV6 LDP Prefix
 * Input(s)         : pPkt - Pointer to the buffer,
 *                    pLsppLdpFecTlv - Pointer to the LDP Target FEC stack
 * OUTPUT           : NONE
 * RETURNS          : NONE 
 **************************************************************************/
PUBLIC VOID
LsppRxDecodeIpv6LdpFec (UINT4 u4ContextId, UINT1 **ppPkt,
                        tLsppLdpFecTlv * pLsppLdpFecTlv)
{
    /* Fill Address Type */
    pLsppLdpFecTlv->u1PrefixType = LSPP_MPLS_ADDR_TYPE_IPV6;

    /* Fetch Ip Address */
    LSPP_GET_NBYTE (pLsppLdpFecTlv->Prefix.u1_addr, *ppPkt,
                    (UINT4) LSPP_IPV6_ADDR_LEN);

    /* Fetch Prefix length */
    LSPP_GET_1BYTE (pLsppLdpFecTlv->u1PrefixLength, *ppPkt);

    /* Skipping Pad Bytes */
    *ppPkt = (*ppPkt + LSPP_FEC_LDP_IPV6_PAD_BYTES);

    LSPP_LOG (u4ContextId, LSPP_DECODE_IPV6_LDP_FEC,
              "LsppRxDecodeIpv6LdpFec",
              pLsppLdpFecTlv->Prefix.u1_addr, pLsppLdpFecTlv->u1PrefixLength);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxDecodeIpv4RsvpFec   
 * DESCRIPTION      : This function is to parse IPV4 RSVP TLV
 * Input(s)         : pPkt - Pointer to the buffer,
 *                    pLsppRsvpTeFecTlv - Pointer to the RSVP Structure
 * OUTPUT           : NONE
 * RETURNS          : NONE 
 **************************************************************************/
PUBLIC VOID
LsppRxDecodeIpv4RsvpFec (UINT4 u4ContextId, UINT1 **ppPkt,
                         tLsppRsvpTeFecTlv * pLsppRsvpTeFecTlv)
{
    UINT2               u2Dummy = 0;

    /* Fill Address Type */
    pLsppRsvpTeFecTlv->u1AddrType = LSPP_MPLS_ADDR_TYPE_IPV4;

    /* Fetch Tunnel End Point Address */
    LSPP_GET_4BYTE (pLsppRsvpTeFecTlv->TunnelEndAddr.u4_addr[0], *ppPkt);

    /* Skipping Must be zero elements in the TLV */
    LSPP_GET_2BYTE (u2Dummy, *ppPkt);

    /* Fetch Tunnel Id */
    LSPP_GET_2BYTE (pLsppRsvpTeFecTlv->u2TunnelId, *ppPkt);

    /* Fetch Extended Tunnel Address */
    LSPP_GET_4BYTE (pLsppRsvpTeFecTlv->ExtendedTunnelId.u4_addr[0], *ppPkt);

    /* Fetch IPV4 Tunnel Senders Address */
    LSPP_GET_4BYTE (pLsppRsvpTeFecTlv->TunnelSenderAddr.u4_addr[0], *ppPkt);

    /* Skipping Must be zero elements in the TLV */
    LSPP_GET_2BYTE (u2Dummy, *ppPkt);

    /* Fetch LSP Id */
    LSPP_GET_2BYTE (pLsppRsvpTeFecTlv->u2LspId, *ppPkt);

    LSPP_LOG (u4ContextId, LSPP_DECODE_IPV4_RSVP_FEC,
              "LsppRxDecodeIpv4RsvpFec",
              pLsppRsvpTeFecTlv->TunnelEndAddr.u4_addr[0],
              pLsppRsvpTeFecTlv->u2TunnelId,
              pLsppRsvpTeFecTlv->ExtendedTunnelId.u4_addr[0],
              pLsppRsvpTeFecTlv->TunnelSenderAddr.u4_addr[0],
              pLsppRsvpTeFecTlv->u2LspId);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxDecodeIpv6RsvpFec   
 * DESCRIPTION      : This function is to parse IPV6 RSVP TLV
 * Input(s)         : pPkt - Pointer to the buffer,
 *                    pLsppRsvpTeFecTlv - Pointer to the RSVP Structure
 * OUTPUT           : NONE
 * RETURNS          : NONE 
 **************************************************************************/
PUBLIC VOID
LsppRxDecodeIpv6RsvpFec (UINT4 u4ContextId, UINT1 **ppPkt,
                         tLsppRsvpTeFecTlv * pLsppRsvpTeFecTlv)
{
    UINT2               u2Dummy = 0;

    /* Fill Address Type */
    pLsppRsvpTeFecTlv->u1AddrType = LSPP_MPLS_ADDR_TYPE_IPV6;

    /* Fetch Tunnel End Point Address */
    LSPP_GET_NBYTE (pLsppRsvpTeFecTlv->TunnelEndAddr.u1_addr, *ppPkt,
                    (UINT4) LSPP_IPV6_ADDR_LEN);

    /* Skipping Must be zero elements in the TLV */
    LSPP_GET_2BYTE (u2Dummy, *ppPkt);

    /* Fetch Tunnel Id */
    LSPP_GET_2BYTE (pLsppRsvpTeFecTlv->u2TunnelId, *ppPkt);

    /* Fetch Extended Tunnel Address */
    LSPP_GET_NBYTE (pLsppRsvpTeFecTlv->ExtendedTunnelId.u1_addr, *ppPkt,
                    (UINT4) LSPP_IPV6_ADDR_LEN);

    /* Fetch IPV6 Tunnel Senders Address */
    LSPP_GET_NBYTE (pLsppRsvpTeFecTlv->TunnelSenderAddr.u1_addr, *ppPkt,
                    (UINT4) LSPP_IPV6_ADDR_LEN);

    /* Skipping Must be zero elements in the TLV */
    LSPP_GET_2BYTE (u2Dummy, *ppPkt);

    /* Fetch LSP Id */
    LSPP_GET_2BYTE (pLsppRsvpTeFecTlv->u2LspId, *ppPkt);

    LSPP_LOG (u4ContextId, LSPP_DECODE_IPV4_RSVP_FEC,
              "LsppRxDecodeIpv6RsvpFec",
              pLsppRsvpTeFecTlv->TunnelEndAddr.u1_addr,
              pLsppRsvpTeFecTlv->u2TunnelId,
              pLsppRsvpTeFecTlv->ExtendedTunnelId.u1_addr,
              pLsppRsvpTeFecTlv->TunnelSenderAddr.u1_addr,
              pLsppRsvpTeFecTlv->u2LspId);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxDecodeFec128DeprecatePwFec   
 * DESCRIPTION      : This function is to parse PW TLV
 * Input(s)         : pPkt - Pointer to the buffer,
 *                    pLsppPwFec128DeprecTlv - Pointer to the deprecate PW
 *                    Structure
 * OUTPUT           : NONE
 * RETURNS          : NONE 
 **************************************************************************/
PUBLIC VOID
LsppRxDecodeFec128DeprecatePwFec (UINT4 u4ContextId, UINT1 **ppPkt,
                                  tLsppPwFec128DeprecTlv *
                                  pLsppPwFec128DeprecTlv)
{
    /* Fetch Remote Peer Address */
    LSPP_GET_4BYTE (pLsppPwFec128DeprecTlv->LsppPwInfo.RemotePEAddr.u4_addr[0],
                    *ppPkt);

    /* Fetch PW ID */
    LSPP_GET_4BYTE (pLsppPwFec128DeprecTlv->LsppPwInfo.u4PWId, *ppPkt);

    /* Fetch PW TYPE */
    LSPP_GET_2BYTE (pLsppPwFec128DeprecTlv->LsppPwInfo.u2PWType, *ppPkt);

    /* Skipping Must be zero elements in the TLV */
    *ppPkt = *ppPkt + (LSPP_FEC_128_DEPRECATED_PWFEC_PAD_BYTES);

    LSPP_LOG (u4ContextId, LSPP_DECODE_FEC_PW_DEPREC,
              "LsppRxDecodeFec128DeprecatePwFec",
              pLsppPwFec128DeprecTlv->LsppPwInfo.RemotePEAddr.u4_addr[0],
              pLsppPwFec128DeprecTlv->LsppPwInfo.u4PWId,
              pLsppPwFec128DeprecTlv->LsppPwInfo.u2PWType);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxDecodeFec128Pw   
 * DESCRIPTION      : This function is to parse FEC 128 PW TLV
 * Input(s)         : pPkt - Pointer to the buffer,
 *                    pLsppPwFec128Tlv - Pointer to the deprecate PW
 *                    Structure
 * OUTPUT           : NONE
 * RETURNS          : NONE 
 **************************************************************************/
PUBLIC VOID
LsppRxDecodeFec128Pw (UINT4 u4ContextId, UINT1 **ppPkt,
                      tLsppPwFec128Tlv * pLsppPwFec128Tlv)
{
    /* Fetch Sender Peer Address */
    LSPP_GET_4BYTE (pLsppPwFec128Tlv->SenderPEAddr.u4_addr[0], *ppPkt);

    /* Fetch Remote Peer Address */
    LSPP_GET_4BYTE (pLsppPwFec128Tlv->LsppPwInfo.RemotePEAddr.u4_addr[0],
                    *ppPkt);

    /* Fetch PW ID */
    LSPP_GET_4BYTE (pLsppPwFec128Tlv->LsppPwInfo.u4PWId, *ppPkt);

    /* Fetch PW TYPE */
    LSPP_GET_2BYTE (pLsppPwFec128Tlv->LsppPwInfo.u2PWType, *ppPkt);

    /* Skipping Must be zero elements in the TLV */
    *ppPkt = *ppPkt + (LSPP_FEC_128_PWFEC_PAD_BYTES);

    LSPP_LOG (u4ContextId, LSPP_DECODE_FEC_128_PW,
              "LsppRxDecodeFec128Pw",
              pLsppPwFec128Tlv->SenderPEAddr.u4_addr[0],
              pLsppPwFec128Tlv->LsppPwInfo.RemotePEAddr.u4_addr[0],
              pLsppPwFec128Tlv->LsppPwInfo.u4PWId,
              pLsppPwFec128Tlv->LsppPwInfo.u2PWType);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxDecodeFec129Pw   
 * DESCRIPTION      : This function is to parse FEC 129 PW TLV
 * Input(s)         : pPkt - Pointer to the buffer,
 *                    pLsppPwFec129Tlv - Pointer to the PW
 *                    Structure
 * OUTPUT           : NONE
 * RETURNS          : NONE 
 **************************************************************************/
PUBLIC VOID
LsppRxDecodeFec129Pw (UINT4 u4ContextId, UINT1 **ppPkt,
                      tLsppPwFec129Tlv * pLsppPwFec129Tlv, UINT2 *pu2TlvLength)
{
    UINT1               u1PadBytesCount = 0;

    /* Fetch Sender Peer Address */
    LSPP_GET_4BYTE (pLsppPwFec129Tlv->SenderPEAddr.u4_addr[0], *ppPkt);

    /* Fetch Remote Peer Address */
    LSPP_GET_4BYTE (pLsppPwFec129Tlv->RemotePEAddr.u4_addr[0], *ppPkt);

    /* Fetch PW TYPE */
    LSPP_GET_2BYTE (pLsppPwFec129Tlv->u2PWType, *ppPkt);

    /* Fetch AGI TYPE */
    LSPP_GET_1BYTE (pLsppPwFec129Tlv->u1AgiType, *ppPkt);

    /* Fetch AGI Length */
    LSPP_GET_1BYTE (pLsppPwFec129Tlv->u1AgiLen, *ppPkt);

    /* Fetch AGI Value */
    LSPP_GET_NBYTE (&(pLsppPwFec129Tlv->au1Agi), *ppPkt,
                    (UINT4) pLsppPwFec129Tlv->u1AgiLen);

    /* Fetch SAII TYPE */
    LSPP_GET_1BYTE (pLsppPwFec129Tlv->u1SaiiType, *ppPkt);

    /* Fetch SAII Length */
    LSPP_GET_1BYTE (pLsppPwFec129Tlv->u1SaiiLen, *ppPkt);

    /* Fetch SAII Value */
    LSPP_GET_NBYTE (&(pLsppPwFec129Tlv->au1Saii), *ppPkt,
                    (UINT4) pLsppPwFec129Tlv->u1SaiiLen);

    /* Fetch TAII TYPE */
    LSPP_GET_1BYTE (pLsppPwFec129Tlv->u1TaiiType, *ppPkt);

    /* Fetch TAII Length */
    LSPP_GET_1BYTE (pLsppPwFec129Tlv->u1TaiiLen, *ppPkt);

    /* Fetch TAII Value */
    LSPP_GET_NBYTE (&(pLsppPwFec129Tlv->au1Taii), *ppPkt,
                    (UINT4) pLsppPwFec129Tlv->u1TaiiLen);

    /* Calculating the skip count. Skipping Must be zero 
     * elements in the TLV. */
    u1PadBytesCount = (UINT1) ((LSPP_BYTE_PAD_MAX_LEN -
                                (*pu2TlvLength % LSPP_BYTE_PAD_MAX_LEN))
                               % LSPP_BYTE_PAD_MAX_LEN);
    *ppPkt = *ppPkt + u1PadBytesCount;
    *pu2TlvLength = (UINT2) ((*pu2TlvLength) + u1PadBytesCount);

    LSPP_LOG (u4ContextId, LSPP_DECODE_FEC_129_PW,
              "LsppRxDecodeFec129Pw",
              pLsppPwFec129Tlv->SenderPEAddr.u4_addr[0],
              pLsppPwFec129Tlv->RemotePEAddr.u4_addr[0],
              pLsppPwFec129Tlv->u2PWType, pLsppPwFec129Tlv->u1AgiType,
              pLsppPwFec129Tlv->u1AgiLen, pLsppPwFec129Tlv->au1Agi,
              pLsppPwFec129Tlv->u1SaiiType, pLsppPwFec129Tlv->u1SaiiLen,
              pLsppPwFec129Tlv->au1Saii, pLsppPwFec129Tlv->u1TaiiType,
              pLsppPwFec129Tlv->u1TaiiLen, pLsppPwFec129Tlv->au1Taii);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxDecodeNilFec   
 * DESCRIPTION      : This function is to parse Nil FEC TLV
 * Input(s)         : pPkt - Pointer to the buffer,
 *                    pLsppNilFecTlv - Pointer to the Nil FEC Sructure
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 **************************************************************************/
PUBLIC VOID
LsppRxDecodeNilFec (UINT4 u4ContextId, UINT1 **ppPkt,
                    tLsppNilFecTlv * pLsppNilFecTlv)
{
    /* Fetch PW FEC TLV Length */
    LSPP_GET_4BYTE (pLsppNilFecTlv->u4Label, *ppPkt);

    pLsppNilFecTlv->u4Label = (pLsppNilFecTlv->u4Label &
                               LSPP_MPLS_LABEL_MASK) >> 12;

    LSPP_LOG (u4ContextId, LSPP_DECODE_NIL_FEC, "LsppRxDecodeNilFec",
              pLsppNilFecTlv->u4Label);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxDecodeStaticLspFec   
 * DESCRIPTION      : This function is to parse Static LSP FEC  TLV
 * Input(s)         : pPkt - Pointer to the buffer,
 *                    pStaticLspFec - Pointer to the static LSP Sructure
 * OUTPUT           : NONE
 * RETURNS          : NONE
 **************************************************************************/
PUBLIC VOID
LsppRxDecodeStaticLspFec (UINT4 u4ContextId, UINT1 **ppPkt,
                          tLsppStaticLspFec * pLsppStaticLspFec)
{
    /* Fetch Source Global Id */
    LSPP_GET_4BYTE (pLsppStaticLspFec->u4SrcGlobalId, *ppPkt);

    /* Fetch Source Node Id */
    LSPP_GET_4BYTE (pLsppStaticLspFec->u4SrcNodeId, *ppPkt);

    /* Fetch Source Tunnel Number */
    LSPP_GET_2BYTE (pLsppStaticLspFec->u2SrcTnlNum, *ppPkt);

    /* Fetch LSP Number */
    LSPP_GET_2BYTE (pLsppStaticLspFec->u2LspNum, *ppPkt);

    /* Fetch Destination Global Id */
    LSPP_GET_4BYTE (pLsppStaticLspFec->u4DstGlobalId, *ppPkt);

    /* Fetch Destination Node Id */
    LSPP_GET_4BYTE (pLsppStaticLspFec->u4DstNodeId, *ppPkt);

    /* Fetch Destination Tunnel Number */
    LSPP_GET_2BYTE (pLsppStaticLspFec->u2DstTnlNum, *ppPkt);

    /* Skipping must be zero elements */
    *ppPkt = *ppPkt + (LSPP_FEC_STATIC_LSP_PAD_BYTES);

    LSPP_LOG (u4ContextId, LSPP_DECODE_STATIC_LSP_FEC,
              "LsppRxDecodeStaticLspFec",
              pLsppStaticLspFec->u4SrcGlobalId,
              pLsppStaticLspFec->u4SrcNodeId, pLsppStaticLspFec->u2SrcTnlNum,
              pLsppStaticLspFec->u2LspNum, pLsppStaticLspFec->u4DstGlobalId,
              pLsppStaticLspFec->u4DstNodeId, pLsppStaticLspFec->u2DstTnlNum);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxDecodeStaticPwFec   
 * DESCRIPTION      : This function is to parse Static PW FEC  TLV
 * Input(s)         : pPkt - Pointer to the buffer,
 *                    pStaticPwFec - Pointer to the Static PW Sructure
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 **************************************************************************/
PUBLIC VOID
LsppRxDecodeStaticPwFec (UINT4 u4ContextId, UINT1 **ppPkt,
                         tLsppStaticPwFec * pLsppStaticPwFec,
                         UINT2 *pu2TlvLength)
{
    UINT1               u1PadBytesCount = 0;

    /* Fetch AGI TYPE */
    LSPP_GET_1BYTE (pLsppStaticPwFec->u1AgiType, *ppPkt);

    /* Fetch AGI Length */
    LSPP_GET_1BYTE (pLsppStaticPwFec->u1AgiLen, *ppPkt);

    /* Fetch AGI Value */
    LSPP_GET_NBYTE (&(pLsppStaticPwFec->au1Agi), *ppPkt,
                    (UINT4) pLsppStaticPwFec->u1AgiLen);

    /* Fetch Source Global Id */
    LSPP_GET_4BYTE (pLsppStaticPwFec->u4SrcGlobalId, *ppPkt);

    /* Fetch Source Node Id */
    LSPP_GET_4BYTE (pLsppStaticPwFec->u4SrcNodeId, *ppPkt);

    /* Fetch Source Ac Id */
    LSPP_GET_4BYTE (pLsppStaticPwFec->u4SrcAcId, *ppPkt);

    /* Fetch Destination Global Id */
    LSPP_GET_4BYTE (pLsppStaticPwFec->u4DstGlobalId, *ppPkt);

    /* Fetch Destination Node Id */
    LSPP_GET_4BYTE (pLsppStaticPwFec->u4DstNodeId, *ppPkt);

    /* Fetch Source Ac Id */
    LSPP_GET_4BYTE (pLsppStaticPwFec->u4DstAcId, *ppPkt);

    /* Calculate the number of bytes to be skipped which was 
     * added for 4 byte alignment.*/
    u1PadBytesCount = (UINT1) ((LSPP_BYTE_PAD_MAX_LEN -
                                (*pu2TlvLength % LSPP_BYTE_PAD_MAX_LEN))
                               % LSPP_BYTE_PAD_MAX_LEN);

    /* Skipping Pad Count */
    *ppPkt = *ppPkt + u1PadBytesCount;
    *pu2TlvLength = (UINT2) ((*pu2TlvLength) + u1PadBytesCount);

    LSPP_LOG (u4ContextId, LSPP_DECODE_STATIC_PW_FEC,
              "LsppRxDecodeStaticPwFec",
              pLsppStaticPwFec->u1AgiType, pLsppStaticPwFec->u1AgiLen,
              pLsppStaticPwFec->au1Agi, pLsppStaticPwFec->u4SrcGlobalId,
              pLsppStaticPwFec->u4SrcNodeId, pLsppStaticPwFec->u4SrcAcId,
              pLsppStaticPwFec->u4DstGlobalId, pLsppStaticPwFec->u4DstNodeId,
              pLsppStaticPwFec->u4DstAcId);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxDecodePadTlv
 * DESCRIPTION      : This function is to parse Pad  TLV
 * Input(s)         : pPkt - Pointer to the buffer,
 *                    pLsppPadTlv - Pointer to the PAD TLV Sructure
 * OUTPUT           : NONE
 * RETURNS          : NONE 
 **************************************************************************/
PUBLIC VOID
LsppRxDecodePadTlv (UINT4 u4ContextId, UINT1 **ppPkt,
                    tLsppPadTlv * pLsppPadTlv, UINT2 u2TlvLength)
{
    /* Fetch Pad TLV length */
    pLsppPadTlv->u2Length = u2TlvLength;

    /* Fetch First Octet */
    LSPP_GET_1BYTE (pLsppPadTlv->u1FirstOctet, *ppPkt);

    LSPP_LOG (u4ContextId, LSPP_DECODE_PAD_TLV,
              "LsppRxDecodePadTlv", pLsppPadTlv->u1FirstOctet);

    /* Fetch Padded bytes */
    LSPP_GET_NBYTE (pLsppPadTlv->au1PadInfo, *ppPkt, u2TlvLength);

    LSPP_DUMP_TLV (u4ContextId, pLsppPadTlv->au1PadInfo, (u2TlvLength - 1));
    return;
}

/***************************************************************************
 * FUNCTION NAME    : LsppReplyTosTlv
 * DESCRIPTION      : This function is to Reply Tos  TLV
 * Input(s)         : pPkt - Pointer to the buffer,
 *                    pLsppReplyTosTlv- Pointer to the Reply Tos Sructure
 * OUTPUT           : NONE
 * RETURNS          : NONE 
 **************************************************************************/
PUBLIC VOID
LsppRxDecodeReplyTosTlv (UINT4 u4ContextId, UINT1 **ppPkt,
                         tLsppReplyTosTlv * pLsppReplyTosTlv)
{
    /* Fetch TOS length */
    LSPP_GET_1BYTE (pLsppReplyTosTlv->u1TosValue, *ppPkt);

    /* Skipping the rest of 3zero bytes */
    *ppPkt = (*ppPkt + LSPP_TLV_REPLY_TOS_PAD_BYTES);

    LSPP_LOG (u4ContextId, LSPP_DECODE_REPLY_TOS_TLV,
              "LsppRxDecodeReplyTosTlv", pLsppReplyTosTlv->u1TosValue);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxDecodeErrorTlv
 * DESCRIPTION      : This function is to parse Error TLV
 * Input(s)         : pPkt - Pointer to the buffer,
 *                    pLsppErrorTlv- Pointer to the Error Sructure
 * OUTPUT           : NONE
 * RETURNS          : NONE 
 **************************************************************************/
PUBLIC VOID
LsppRxDecodeErrorTlv (UINT4 u4ContextId, UINT1 **ppPkt,
                      tLsppErrorTlv * pLsppErrorTlv, UINT2 u2TlvLength)
{
    /* Fetch the whole errored tlv */
    LSPP_GET_NBYTE (pLsppErrorTlv->au1Value, *ppPkt, (UINT4) u2TlvLength);

    LSPP_LOG (u4ContextId, LSPP_DECODE_ERROR_TLV, "LsppRxDecodeErrorTlv");

    LSPP_DUMP_TLV (u4ContextId, pLsppErrorTlv->au1Value, u2TlvLength);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxDecodeBfdDiscTlv
 * DESCRIPTION      : This function is to parse Bfd Disc TLV
 * Input(s)         : pPkt - Pointer to the buffer,
 *                    pLsppBfdDiscTlv - Pointer to the Error Sructure
 * OUTPUT           : NONE
 * RETURNS          : NONE
 **************************************************************************/
PUBLIC VOID
LsppRxDecodeBfdDiscTlv (UINT4 u4ContextId, UINT1 **ppPkt,
                        tLsppBfdDiscTlv * pLsppBfdDiscTlv)
{
    /* Fetch Discrminator */
    LSPP_GET_4BYTE (pLsppBfdDiscTlv->u4Discriminator, *ppPkt);

    LSPP_LOG (u4ContextId, LSPP_DECODE_BFD_DISC_TLV,
              "LsppRxDecodeBfdDiscTlv", pLsppBfdDiscTlv->u4Discriminator);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxDecodeHeader
 * DESCRIPTION      : This function is to parse LSP Ping Header
 * Input(s)         : pPkt - Pointer to the buffer,
 *                    pLsppHeader - Pointer to the Header Sructure
 * OUTPUT           : NONE
 * RETURNS          : NONE 
 **************************************************************************/
PUBLIC VOID
LsppRxDecodeHeader (UINT1 **ppPkt, tLsppHeader * pLsppHeader)
{
    /* Fetch Version */
    LSPP_GET_2BYTE (pLsppHeader->u2Version, *ppPkt);

    /* Fetch Global Flag */
    LSPP_GET_2BYTE (pLsppHeader->u2GlobalFlag, *ppPkt);

    /* Fetch Message Type */
    LSPP_GET_1BYTE (pLsppHeader->u1MessageType, *ppPkt);

    /* Fetch Reply mode */
    LSPP_GET_1BYTE (pLsppHeader->u1ReplyMode, *ppPkt);

    /* Fetch Return code */
    LSPP_GET_1BYTE (pLsppHeader->u1ReturnCode, *ppPkt);

    /* Fetch Return sub code */
    LSPP_GET_1BYTE (pLsppHeader->u1ReturnSubCode, *ppPkt);

    /* Fetch Senders handle */
    LSPP_GET_4BYTE (pLsppHeader->u4SenderHandle, *ppPkt);

    /* Fetch Sequence number */
    LSPP_GET_4BYTE (pLsppHeader->u4SequenceNum, *ppPkt);

    /* Fetch Timestamp sent in sec */
    LSPP_GET_4BYTE (pLsppHeader->u4TimeStampSentInSec, *ppPkt);

    /* Fetch  Timestamp sent in microsec */
    LSPP_GET_4BYTE (pLsppHeader->u4TimeStampSentInMicroSec, *ppPkt);

    /* Fetch  Timestamp receive in sec */
    LSPP_GET_4BYTE (pLsppHeader->u4TimeStampRxInSec, *ppPkt);

    /* Fetch  Timestamp receive in microsec */
    LSPP_GET_4BYTE (pLsppHeader->u4TimeStampRxInMicroSec, *ppPkt);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxDecodeIfLblStkTlv
 * DESCRIPTION      : This function is to parse Interface and label Stack TLV
 * Input(s)         : pPkt - Pointer to the buffer,
 *                    pLsppIfLblStkTlv- Pointer to the Header Sructure
 * OUTPUT           : NONE
 * RETURNS          : NONE 
 **************************************************************************/
PUBLIC UINT4
LsppRxDecodeIfLblStkTlv (UINT4 u4ContextId, UINT1 **ppPkt,
                         tLsppIfLblStkTlv * pLsppIfLblStkTlv)
{
    UINT1               u1Index = 0;
    UINT1               au1RxLabelStack[LSPP_MAX_OCTETSTRING_SIZE];
    UINT1               au1RxLabelExp[LSPP_MAX_OCTETSTRING_SIZE];
    INT4                i4RxLblStkLen = 0;
    INT4                i4RxLblExpStkLen = 0;

    /* Fetch AddressType */
    LSPP_GET_1BYTE (pLsppIfLblStkTlv->u1AddrType, *ppPkt);

    /* Skipping 3 bytes as the TLV has zero value */
    *ppPkt = (*ppPkt + LSPP_TLV_IF_LBL_STACK_PAD_BYTES);

    /* Fetch Rx Ip Address */
    if ((pLsppIfLblStkTlv->u1AddrType == LSPP_IPV4_NUMBERED) ||
        (pLsppIfLblStkTlv->u1AddrType == LSPP_IPV4_UNNUMBERED))
    {
        LSPP_GET_4BYTE (pLsppIfLblStkTlv->RxIpAddr.u4_addr[0], *ppPkt);
        LSPP_GET_4BYTE (pLsppIfLblStkTlv->RxIfAddr.u4_addr[0], *ppPkt);
    }
    else if ((pLsppIfLblStkTlv->u1AddrType == LSPP_IPV6_NUMBERED) ||
             (pLsppIfLblStkTlv->u1AddrType == LSPP_IPV6_UNNUMBERED))
    {
        LSPP_GET_NBYTE (pLsppIfLblStkTlv->RxIpAddr.u1_addr, *ppPkt,
                        (UINT4) LSPP_IPV6_ADDR_LEN);

        LSPP_GET_NBYTE (pLsppIfLblStkTlv->RxIfAddr.u1_addr, *ppPkt,
                        (UINT4) LSPP_IPV6_ADDR_LEN);
    }
    else if (pLsppIfLblStkTlv->u1AddrType == LSPP_ADDR_TYPE_NOT_APPLICABLE)
    {
        /* IP and Interface addresses will not be there in this TLV 
         * for this case.
         */
    }
    else
    {
        return OSIX_FAILURE;
    }

    do
    {
        if (u1Index >= LSPP_MAX_LABEL_STACK)
        {
            return OSIX_FAILURE;
        }

        LsppRxGetMplsLabel (ppPkt, &(pLsppIfLblStkTlv->RxLblInfo[u1Index]));
    }
    while (pLsppIfLblStkTlv->RxLblInfo[u1Index++].u1SI != 1);

    pLsppIfLblStkTlv->u1LblStackDepth = u1Index;

    LsppUtilUpdateLblStk (pLsppIfLblStkTlv->RxLblInfo, u1Index,
                          au1RxLabelStack, &i4RxLblStkLen,
                          au1RxLabelExp, &i4RxLblExpStkLen);

    if (pLsppIfLblStkTlv->u1AddrType == LSPP_IPV4_NUMBERED)
    {
        LSPP_LOG (u4ContextId, LSPP_DECODE_IPV4_IF_LBL_STK_TLV,
                  "LsppRxDecodeIfLblStkTlv",
                  pLsppIfLblStkTlv->u1AddrType,
                  pLsppIfLblStkTlv->RxIpAddr.u4_addr[0],
                  pLsppIfLblStkTlv->RxIfAddr.u4_addr[0], au1RxLabelStack);
    }
    else if (pLsppIfLblStkTlv->u1AddrType == LSPP_IPV6_NUMBERED)
    {
        LSPP_LOG (u4ContextId, LSPP_DECODE_IPV6_IF_LBL_STK_TLV,
                  "LsppRxDecodeIfLblStkTlv",
                  pLsppIfLblStkTlv->u1AddrType,
                  pLsppIfLblStkTlv->RxIpAddr.u1_addr,
                  pLsppIfLblStkTlv->RxIfAddr.u1_addr, au1RxLabelStack);
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxDecodeDsmapTlv
 * DESCRIPTION      : This function is to Parse DSMAP TLV
 * Input(s)         : pPkt - Pointer to the buffer,
 *                    pLsppDsMapTlv - Pointer to the Header Sructure
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 **************************************************************************/
PUBLIC UINT4
LsppRxDecodeDsmapTlv (UINT4 u4ContextId, UINT1 **ppPkt,
                      tLsppDsMapTlv * pLsppDsMapTlv, UINT2 *pu2ReadBytes)
{
    UINT1               u1Index = 0;
    UINT1               au1DsLabelStack[LSPP_MAX_OCTETSTRING_SIZE];
    UINT1               au1DsLabelExp[LSPP_MAX_OCTETSTRING_SIZE];
    INT4                i4DsLblStkLen = 0;
    INT4                i4DsLblExpStkLen = 0;

    /* Fetch MTU */
    LSPP_GET_2BYTE (pLsppDsMapTlv->u2Mtu, *ppPkt);

    /* Fetch Address Type */
    LSPP_GET_1BYTE (pLsppDsMapTlv->u1AddrType, *ppPkt);

    /* Fetch DS Flag */
    LSPP_GET_1BYTE (pLsppDsMapTlv->u1DsFlags, *ppPkt);

    /* Fetch DS Ip Address */
    if ((pLsppDsMapTlv->u1AddrType == LSPP_IPV4_NUMBERED) ||
        (pLsppDsMapTlv->u1AddrType == LSPP_IPV4_UNNUMBERED))
    {
        LSPP_GET_4BYTE (pLsppDsMapTlv->DsIpAddr.u4_addr[0], *ppPkt);
        LSPP_GET_4BYTE (pLsppDsMapTlv->DsIfAddr.u4_addr[0], *ppPkt);
    }
    else if ((pLsppDsMapTlv->u1AddrType == LSPP_IPV6_NUMBERED) ||
             (pLsppDsMapTlv->u1AddrType == LSPP_IPV6_UNNUMBERED))
    {
        LSPP_GET_NBYTE (pLsppDsMapTlv->DsIpAddr.u1_addr, *ppPkt,
                        (UINT4) LSPP_IPV6_ADDR_LEN);
        LSPP_GET_NBYTE (pLsppDsMapTlv->DsIfAddr.u1_addr, *ppPkt,
                        (UINT4) LSPP_IPV6_ADDR_LEN);
    }
    else if (pLsppDsMapTlv->u1AddrType == LSPP_ADDR_TYPE_NOT_APPLICABLE)
    {
        /* Downstream IP and Interface addresses will not be there in the
         * packet.
         */
    }
    else
    {
        *pu2ReadBytes = LSPP_DSMAP_ADDR_TYPE_OFFSET;
        return OSIX_FAILURE;
    }

    /* Fetch Multipath Type */
    LSPP_GET_1BYTE (pLsppDsMapTlv->u1MultipathType, *ppPkt);

    /* Fetch Depth Limit */
    LSPP_GET_1BYTE (pLsppDsMapTlv->u1DepthLimit, *ppPkt);

    /* Fetch Multipath Length */
    LSPP_GET_2BYTE (pLsppDsMapTlv->u2MultipathLength, *ppPkt);

    /* Fetch Multipath Information */
    LSPP_GET_NBYTE (pLsppDsMapTlv->au1MultipathInfo, *ppPkt,
                    (UINT4) pLsppDsMapTlv->u2MultipathLength);

    if (pLsppDsMapTlv->DsIpAddr.u4_addr[0] != LSPP_ALLROUTERS_MULTICAST_ADDRESS)
    {
        do
        {
            if (u1Index >= LSPP_MAX_LABEL_STACK)
            {
                return OSIX_FAILURE;
            }

            LsppRxGetMplsLabel (ppPkt, &(pLsppDsMapTlv->DsLblInfo[u1Index]));

            /* Protocol field will be in place of TTL . So using the same 
             * function to get the label information and interchange protocol 
             * field alone */

            pLsppDsMapTlv->DsLblInfo[u1Index].u1Protocol =
                pLsppDsMapTlv->DsLblInfo[u1Index].u1Ttl;

            pLsppDsMapTlv->DsLblInfo[u1Index].u1Ttl = 0;

        }
        while (pLsppDsMapTlv->DsLblInfo[u1Index++].u1SI != 1);

        pLsppDsMapTlv->u1DsLblStackDepth = u1Index;

        LsppUtilUpdateLblStk (pLsppDsMapTlv->DsLblInfo, u1Index,
                              au1DsLabelStack, &i4DsLblStkLen,
                              au1DsLabelExp, &i4DsLblExpStkLen);
    }

    if ((pLsppDsMapTlv->u1AddrType == LSPP_IPV4_NUMBERED) ||
        (pLsppDsMapTlv->u1AddrType == LSPP_IPV4_UNNUMBERED))
    {
        LSPP_LOG (u4ContextId, LSPP_DECODE_IPV4_DSMAP_TLV,
                  "LsppRxDecodeDsmapTlv",
                  pLsppDsMapTlv->u2Mtu, pLsppDsMapTlv->u1AddrType,
                  pLsppDsMapTlv->u1DsFlags,
                  pLsppDsMapTlv->DsIpAddr.u4_addr[0],
                  pLsppDsMapTlv->DsIfAddr.u4_addr[0],
                  pLsppDsMapTlv->u1MultipathType,
                  pLsppDsMapTlv->u1DepthLimit,
                  pLsppDsMapTlv->u2MultipathLength,
                  pLsppDsMapTlv->au1MultipathInfo, au1DsLabelStack);
    }
    else if (pLsppDsMapTlv->u1AddrType == LSPP_IPV6_NUMBERED)
    {
        LSPP_LOG (u4ContextId, LSPP_DECODE_IPV6_DSMAP_TLV,
                  "LsppRxDecodeDsmapTlv",
                  pLsppDsMapTlv->u2Mtu, pLsppDsMapTlv->u1AddrType,
                  pLsppDsMapTlv->u1DsFlags,
                  pLsppDsMapTlv->DsIpAddr.u1_addr,
                  pLsppDsMapTlv->DsIfAddr.u1_addr,
                  pLsppDsMapTlv->u1MultipathType,
                  pLsppDsMapTlv->u1DepthLimit,
                  pLsppDsMapTlv->u2MultipathLength,
                  pLsppDsMapTlv->au1MultipathInfo, au1DsLabelStack);
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxMatchEchoSequenceEntry
 * DESCRIPTION      : This function is to match senders handle and sequecen 
 *                    number
 * Input(s)         : u4ContextId - Context In which packet is received
 *                    pLsppHeader - Protcol Header Information
 *                    pLsppPingTraceEntry - Ping trace entry Information
 *                    pLsppEchoSequenceEntry - Echo Sequecnce Entry information
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 **************************************************************************/
PUBLIC INT4
LsppRxMatchEchoSequenceEntry (UINT4 u4ContextId, tLsppHeader * pLsppHeader,
                              tLsppFsLsppPingTraceTableEntry
                              ** ppLsppPingTraceEntry,
                              tLsppFsLsppEchoSequenceTableEntry
                              ** ppLsppEchoSequenceEntry)
{
    tLsppFsLsppEchoSequenceTableEntry LsppEchoSequenceEntry;
    tLsppFsLsppPingTraceTableEntry *pLsppPingTraceEntry = NULL;

    pLsppPingTraceEntry = (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppPingTraceEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pLsppPingTraceEntry, 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&LsppEchoSequenceEntry, 0,
            sizeof (tLsppFsLsppEchoSequenceTableEntry));

    /* Fetch Ping Trace Entry */
    pLsppPingTraceEntry->MibObject.u4FsLsppContextId = u4ContextId;

    pLsppPingTraceEntry->MibObject.u4FsLsppSenderHandle =
        pLsppHeader->u4SenderHandle;

    /* Fetch PingTraceEntry */
    *ppLsppPingTraceEntry = LsppGetFsLsppPingTraceTable (pLsppPingTraceEntry);

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppPingTraceEntry);

    if (*ppLsppPingTraceEntry == NULL)
    {
        LSPP_LOG (u4ContextId, LSPP_PINGTRACE_ENTRY_NOT_FOUND,
                  "LsppRxMatchEchoSequenceEntry", pLsppHeader->u4SenderHandle);
        return OSIX_FAILURE;
    }

    /* If Senders Handle is Present, Check for Sequence number */
    LsppEchoSequenceEntry.MibObject.u4FsLsppContextId = u4ContextId;

    LsppEchoSequenceEntry.MibObject.u4FsLsppSenderHandle =
        pLsppHeader->u4SenderHandle;

    LsppEchoSequenceEntry.MibObject.u4FsLsppSequenceNumber =
        pLsppHeader->u4SequenceNum;

    *ppLsppEchoSequenceEntry =
        LsppGetFsLsppEchoSequenceTable (&LsppEchoSequenceEntry);

    if (*ppLsppEchoSequenceEntry == NULL)
    {
        LSPP_LOG (u4ContextId, LSPP_ECHO_SEQ_ENTRY_NOT_FOUND,
                  "LsppRxMatchEchoSequenceEntry", pLsppHeader->u4SenderHandle,
                  pLsppHeader->u4SequenceNum);
        return OSIX_FAILURE;
    }

    if ((*ppLsppEchoSequenceEntry)->u1Status != LSPP_ECHO_SEQ_IN_PROGRESS)
    {
        (*ppLsppPingTraceEntry)->u4SuccessCount = (*ppLsppPingTraceEntry)->u4SuccessCount + 1;
		LSPP_LOG (u4ContextId, LSPP_ECHO_STATUS_ALREADY_COMPLETED,
                  "LsppRxMatchEchoSequenceEntry", pLsppHeader->u4SenderHandle,
                  pLsppHeader->u4SequenceNum);
        return OSIX_FAILURE;
    }

    LSPP_LOG (u4ContextId, LSPP_ECHO_SEQ_ENTRY_FOUND,
              "LsppRxMatchEchoSequenceEntry", u4ContextId,
              pLsppHeader->u4SenderHandle, pLsppHeader->u4SequenceNum);

    return OSIX_SUCCESS;
}

 /***************************************************************************
 * FUNCTION NAME    : LsppRxValidateLspMepTlv
 *
 * DESCRIPTION      : This function is to Validate Mep Tlv
 *
 * Input(s)         : pRxLspMepTlv - Pointer to Lsp Mep TLv
 *                    pTnlMepInfo  - Pointer to the Te Tunnel info    
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 * 
 **************************************************************************/
PUBLIC INT4
LsppRxValidateLspMepTlv (tMplsIpLspMepTlv * pRxLspMepTlv,
                         tLsppTeTnlInfo * pTeTnlInfo)
{
    if ((pTeTnlInfo->u1TnlRole == MPLS_TE_INGRESS) &&
        (pTeTnlInfo->u1TnlMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL))
    {
        if ((pRxLspMepTlv->GlobalNodeId.u4GlobalId !=
             pTeTnlInfo->TnlInfo.DstNodeId.MplsGlobalNodeId.u4GlobalId) ||
            (pRxLspMepTlv->GlobalNodeId.u4NodeId !=
             pTeTnlInfo->TnlInfo.DstNodeId.MplsGlobalNodeId.u4NodeId) ||
            (pRxLspMepTlv->u2TnlNum != pTeTnlInfo->TnlInfo.u4SrcTnlNum) ||
            (pRxLspMepTlv->u2LspNum != pTeTnlInfo->TnlInfo.u4LspNum))
        {
            return OSIX_FAILURE;

        }
    }
    else if ((pTeTnlInfo->u1TnlRole == MPLS_TE_INTERMEDIATE) &&
             (pTeTnlInfo->u1TnlMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL) &&
             (pTeTnlInfo->InSegInfo.Direction == LSPP_MPLS_DIRECTION_REVERSE))
    {
        if ((pRxLspMepTlv->GlobalNodeId.u4GlobalId !=
             pTeTnlInfo->TnlInfo.DstNodeId.MplsGlobalNodeId.u4GlobalId) ||
            (pRxLspMepTlv->GlobalNodeId.u4NodeId !=
             pTeTnlInfo->TnlInfo.DstNodeId.MplsGlobalNodeId.u4NodeId) ||
            (pRxLspMepTlv->u2TnlNum != pTeTnlInfo->TnlInfo.u4SrcTnlNum) ||
            (pRxLspMepTlv->u2LspNum != pTeTnlInfo->TnlInfo.u4LspNum))
        {
            return OSIX_FAILURE;

        }
    }
    else
    {
        if ((pRxLspMepTlv->GlobalNodeId.u4GlobalId !=
             pTeTnlInfo->TnlInfo.SrcNodeId.MplsGlobalNodeId.u4GlobalId) ||
            (pRxLspMepTlv->GlobalNodeId.u4NodeId !=
             pTeTnlInfo->TnlInfo.SrcNodeId.MplsGlobalNodeId.u4NodeId) ||
            (pRxLspMepTlv->u2TnlNum != pTeTnlInfo->TnlInfo.u4SrcTnlNum) ||
            (pRxLspMepTlv->u2LspNum != pTeTnlInfo->TnlInfo.u4LspNum))
        {
            return OSIX_FAILURE;

        }
    }

    return OSIX_SUCCESS;
}

 /***************************************************************************
 * FUNCTION NAME    : LsppRxValidatePwMepTlv 
 *
 * DESCRIPTION      : This function is to match Pw MEP
 *
 * Input(s)         : pRxIpPwMepTlv - Pointer to Lsp Mep TLv
 *                    pMegDetail         - Meg Detail Fetched from Label    
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 * 
 **************************************************************************/
PUBLIC INT4
LsppRxValidatePwMepTlv (tMplsIpPwMepTlv * pRxIpPwMepTlv,
                        tLsppPwDetail * pPwDetail)
{
    tLsppPwFecInfo     *pPwMepInfo = NULL;

    pPwMepInfo = &(pPwDetail->PwFecInfo);

    if ((pRxIpPwMepTlv->u4GlobalId != pPwMepInfo->DstNodeId.
         MplsGlobalNodeId.u4GlobalId) ||
        (pRxIpPwMepTlv->u4NodeId != pPwMepInfo->DstNodeId.
         MplsGlobalNodeId.u4NodeId) ||
        (pRxIpPwMepTlv->u4AcId != pPwMepInfo->u4DstAcId))
    {
        return OSIX_FAILURE;
    }
    /* AGI VALIDATION */
    if ((pRxIpPwMepTlv->u1AgiLen != pPwMepInfo->u1AgiLen) ||
        (pRxIpPwMepTlv->u1AgiType != pPwMepInfo->u1AgiType) ||
        (STRNCMP (pRxIpPwMepTlv->au1Agi, pPwMepInfo->au1Agi,
                  pRxIpPwMepTlv->u1AgiLen) != 0))
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxValidateIccMepTlv 
 *
 * DESCRIPTION      : This function is to match Icc MEP
 *
 * Input(s)         : pRxIccMepTlv - Pointer to ICc Mep TLv
 *                    pMegDetail    - Meg Detail Fetched from Label    
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 * 
 **************************************************************************/
PUBLIC INT4
LsppRxValidateIccMepTlv (tMplsIccMepTlv * pRxIccMepTlv,
                         tLsppMegDetail * pMegDetail, UINT1 u1PathDirection)
{

    UINT1              *pu1MegInfo = NULL;
    UINT1               au1MegInfo[MPLS_MEG_ID_LENGTH] = { 0 };

    pu1MegInfo = au1MegInfo;

    /* Extracting the ICC and UMC from the packet */
    LSPP_PUT_NBYTE (pu1MegInfo, pRxIccMepTlv->au1Icc, MPLS_ICC_LENGTH);

    LSPP_PUT_NBYTE (pu1MegInfo, pRxIccMepTlv->au1Umc, MPLS_UMC_LENGTH);

    /* Resetting the offset */
    pu1MegInfo = au1MegInfo;

    if ((STRNCMP (pu1MegInfo, pMegDetail->au1Icc,
                  STRLEN (pMegDetail->au1Icc)) != 0) ||
        (STRNCMP ((pu1MegInfo + STRLEN (pMegDetail->au1Icc)),
                  pMegDetail->au1Umc, STRLEN (pMegDetail->au1Umc)) != 0))
    {
        return OSIX_FAILURE;
    }

    if ((pMegDetail->u1MpType == LSPP_ME_MP_TYPE_MEP) ||
        ((pMegDetail->u1MpType == LSPP_ME_MP_TYPE_MIP) &&
         (u1PathDirection == LSPP_MPLS_DIRECTION_REVERSE)))
    {
        if (pRxIccMepTlv->u2MepIndex != pMegDetail->u2IccSinkMepIndex)
        {
            return OSIX_FAILURE;

        }
    }
    else if ((pMegDetail->u1MpType == LSPP_ME_MP_TYPE_MIP) &&
             (u1PathDirection == LSPP_MPLS_DIRECTION_FORWARD))
    {
        if (pRxIccMepTlv->u2MepIndex != pMegDetail->u2IccSrcMepIndex)
        {
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxValidateMipTlv 
 *
 * DESCRIPTION      : This function is to match Mip Tlv
 *
 * Input(s)         : pRxMipTlv - Pointer to Mip TLv
 *                    pMegDetail    - Meg Detail Fetched from Label    
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 * 
 **************************************************************************/
PUBLIC INT4
LsppRxValidateMipTlv (tMplsMipTlv * pRxMipTlv, tLsppMegDetail * pMegDetail)
{
    if ((pRxMipTlv->GlobalNodeId.u4GlobalId !=
         pMegDetail->MplsNodeId.u4GlobalId) ||
        (pRxMipTlv->GlobalNodeId.u4NodeId !=
         pMegDetail->MplsNodeId.u4NodeId) ||
        (pRxMipTlv->u4IfNum != pMegDetail->u4MpIfIndex))

    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

 /***************************************************************************
 * FUNCTION NAME    : LsppRxValidateAchTlvs
 * DESCRIPTION      : This function is to  Match ACH header
 * Input(s)         : u4contextId - uniquelly identifies the context.
 *                    pLsppAchInfo - Pointer to the Header Sructure.
 *                    pLsppMegApiInfo - Pointer to Lsp Meg Info Structure.
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 **************************************************************************/
PUBLIC INT4
LsppRxValidateAchTlvs (UINT4 u4ContextId,
                       tLsppAchInfo * pLsppAchInfo,
                       tLsppPathInfo * pLsppPathInfo)
{
    tLsppMegDetail     *pMegDetail = NULL;
    tLsppPwDetail      *pPwDetail = NULL;
    tLsppTeTnlInfo     *pTeTnlDetail = NULL;
    UINT1               u1PathDirection = 0;

    pMegDetail = &(pLsppPathInfo->MegDetail);
    pPwDetail = &(pLsppPathInfo->PwDetail);
    pTeTnlDetail = &(pLsppPathInfo->TeTnlInfo);

    if (pLsppAchInfo->u1AchTlvPresent & LSPP_ACH_IP_LSP_MEP_TLV)
    {
        if (LsppRxValidateLspMepTlv (&(pLsppAchInfo->LsppMepTlv.unMepTlv.
                                       IpLspMepTlv), pTeTnlDetail) ==
            OSIX_FAILURE)
        {
            LSPP_LOG (u4ContextId, LSPP_ACH_LSP_MEP_VALIDATION_FAILED,
                      "LsppRxValidateAchTlvs");
            return OSIX_FAILURE;
        }
    }
    else if (pLsppAchInfo->u1AchTlvPresent & LSPP_ACH_IP_PW_MEP_TLV)
    {
        if (LsppRxValidatePwMepTlv
            (&(pLsppAchInfo->LsppMepTlv.unMepTlv.IpPwMepTlv), pPwDetail) !=
            OSIX_SUCCESS)
        {
            LSPP_LOG (u4ContextId, LSPP_ACH_PW_MEP_VALIDATION_FAILED,
                      "LsppRxValidateAchTlvs");
            return OSIX_FAILURE;
        }
    }
    else if (pLsppAchInfo->u1AchTlvPresent & LSPP_ACH_ICC_MEP_TLV)
    {
        u1PathDirection = (UINT1) pLsppPathInfo->TeTnlInfo.InSegInfo.Direction;

        if (LsppRxValidateIccMepTlv
            (&(pLsppAchInfo->LsppMepTlv.unMepTlv.IccMepTlv), pMegDetail,
             u1PathDirection) != OSIX_SUCCESS)
        {
            LSPP_LOG (u4ContextId, LSPP_ACH_ICC_MEP_VALIDATION_FAILED,
                      "LsppRxValidateAchTlvs");
            return OSIX_FAILURE;
        }
    }

    if (pMegDetail->u1MpType == LSPP_ME_MP_TYPE_MIP)
    {
        if (pLsppAchInfo->u1AchTlvPresent & LSPP_ACH_MIP_TLV)
        {
            if (LsppRxValidateMipTlv
                (&(pLsppAchInfo->LsppMipTlv), pMegDetail) != OSIX_SUCCESS)
            {
                LSPP_LOG (u4ContextId, LSPP_ACH_MIP_VALIDATION_FAILED,
                          "LsppRxValidateAchTlvs");
                return OSIX_FAILURE;
            }
        }
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxGetAndValidateIpHeader
 *
 * DESCRIPTION      : This function is to Parse and validate Ip Header
 *
 * Input(s)         : u4ContextId - identifier which uniquelly identifies 
 *                                  the context.
 *                    ppPkt       - pointer to the packet buffer.              
 *                    pLsppIpHeader - Pointer to the Ip Sructure
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 * 
 **************************************************************************/
PUBLIC INT4
LsppRxGetAndValidateIPHeader (UINT4 u4ContextId,
                              UINT1 **ppPkt, t_IP * pRxLsppIpHeader)
{
    t_IP_HEADER         LsppIpHeader;
    UINT1               u1Tmp = 0;

    MEMSET (&LsppIpHeader, 0, sizeof (t_IP_HEADER));

    /* Fetch IP Header */
    LSPP_GET_NBYTE (&(LsppIpHeader), *ppPkt, (UINT4) IP_HDR_LEN);

    u1Tmp = LsppIpHeader.u1Ver_hdrlen;

    pRxLsppIpHeader->u1Version = (UINT1) (LSPP_IP_VERS (u1Tmp));
    pRxLsppIpHeader->u1Hlen = (UINT1) ((u1Tmp & IP_HEADER_LEN_MASK) << IP_TWO);
    pRxLsppIpHeader->u1Tos = LsppIpHeader.u1Tos;
    pRxLsppIpHeader->u2Len = OSIX_NTOHS (LsppIpHeader.u2Totlen);

    if ((pRxLsppIpHeader->u1Hlen < IP_HDR_LEN) ||
        (pRxLsppIpHeader->u2Len < pRxLsppIpHeader->u1Hlen))
    {
        LSPP_LOG (u4ContextId, LSPP_INVALID_IP_HDR_LEN,
                  "LsppRxGetAndValidateIPHeader");
        return IP_FAILURE;
    }

    /* IP header options are not processed by LSP Ping. */
    if (pRxLsppIpHeader->u1Hlen > IP_HDR_LEN)
    {
        LSPP_GET_NBYTE (pRxLsppIpHeader->au1Options, *ppPkt,
                        (UINT4) (pRxLsppIpHeader->u1Hlen - IP_HDR_LEN));
    }

    pRxLsppIpHeader->u2Olen = (UINT2) (IP_OLEN (u1Tmp));
    pRxLsppIpHeader->u2Id = OSIX_NTOHS (LsppIpHeader.u2Id);
    pRxLsppIpHeader->u2Fl_offs = OSIX_NTOHS (LsppIpHeader.u2Fl_offs);
    pRxLsppIpHeader->u1Ttl = LsppIpHeader.u1Ttl;
    pRxLsppIpHeader->u1Proto = LsppIpHeader.u1Proto;
    pRxLsppIpHeader->u2Cksum = OSIX_NTOHS (LsppIpHeader.u2Cksum);
    pRxLsppIpHeader->u4Src = OSIX_NTOHL (LsppIpHeader.u4Src);
    pRxLsppIpHeader->u4Dest = OSIX_NTOHL (LsppIpHeader.u4Dest);

    *ppPkt = *ppPkt - (IP_HDR_LEN + pRxLsppIpHeader->u2Olen);

    if (UtlIpCSumLinBuf ((CONST INT1 *) *ppPkt, (IP_HDR_LEN +
                                                 pRxLsppIpHeader->u2Olen)) != 0)
    {
        LSPP_LOG (u4ContextId, LSPP_IP_CHECKSUM_VALIDATION_FAILED,
                  "LsppRxGetAndValidateIPHeader");
        return OSIX_FAILURE;
    }

    *ppPkt = *ppPkt + (IP_HDR_LEN + pRxLsppIpHeader->u2Olen);

    return OSIX_SUCCESS;
}

 /***************************************************************************
 * FUNCTION NAME    : LsppRxGetAndValidateUdpHeader
 *
 * DESCRIPTION      : This function is to Parse and validate Up Header
 *
 * Input(s)         : pPkt - Pointer to the buffer,
 *                    pLsppUdpHeader - Pointer to the Udp Sructure
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 * 
 **************************************************************************/
PUBLIC INT4
LsppRxGetAndValidateUdpHeader (UINT1 **ppPkt, UINT4 u4ContextId,
                               tLsppUdpHeader * pLsppUdpHeader)
{
    /* Fetch UDP Header */
    LSPP_GET_NBYTE (pLsppUdpHeader, *ppPkt, (UINT4) sizeof (tLsppUdpHeader));

    pLsppUdpHeader->u2Src_u_port = OSIX_NTOHS (pLsppUdpHeader->u2Src_u_port);
    pLsppUdpHeader->u2Dest_u_port = OSIX_NTOHS (pLsppUdpHeader->u2Dest_u_port);
    pLsppUdpHeader->u2Len = OSIX_NTOHS (pLsppUdpHeader->u2Len);
    pLsppUdpHeader->u2Cksum = OSIX_NTOHS (pLsppUdpHeader->u2Cksum);

    /* Checksum validation is not required. */

    if ((pLsppUdpHeader->u2Dest_u_port != LSPP_UDP_DEST_PORT))
    {
        LSPP_LOG (u4ContextId, LSPP_UDP_VALIDATION_FAILED,
                  "LsppRxGetAndValidateUdpHeader");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

 /***************************************************************************
 * FUNCTION NAME    : LsppRxGetAchHdr
 * DESCRIPTION      : This function is to Parse Ach Header
 * Input(s)         : pPkt - Pointer to the buffer,
 *                    pAchHeader - Pointer to the Udp Sructure
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 **************************************************************************/
VOID
LsppRxGetAchHdr (UINT1 **ppPkt, tLsppAchHeader * pAchHeader)
{
    UINT4               u4Hdr = 0;
    LSPP_GET_4BYTE (u4Hdr, *ppPkt);
    pAchHeader->u4AchType = (u4Hdr & LSPP_MPLS_ACH_TYPE_MASK) >>
        LSPP_MPLS_ACH_TYPE_OFFSET_BITS;
    pAchHeader->u1Version = (UINT1) ((u4Hdr &
                                      LSPP_MPLS_ACH_VERSION_MASK) >>
                                     LSPP_MPLS_ACH_VERSION_OFFSET_BITS);
    pAchHeader->u1Rsvd = (UINT1) ((u4Hdr & LSPP_MPLS_ACH_RSVD_MASK) >>
                                  LSPP_MPLS_ACH_RSVD_OFFSET_BITS);
    pAchHeader->u2ChannelType = (UINT2) (u4Hdr & LSPP_MPLS_ACH_CHNL_TYPE_MASK);
}

/******************************************************************************
 * Function   : LsppRxUdpHdrCheckSumValidation
 * Description: This function is to validate the UDP checksum
 * Input      : pBuf   - Packet Buffer
 *              i2Len - Length of the Udp Payload packet
 *              u4Dest - Destination Ip address
 *              u4Src  - Source Ip address
 *              pUdpHdr - Udp Header Format
 * Output     : OSIX_SUCCESS or OSIX_FAILURE
 * Returns    : Updated checksum
 *****************************************************************************/
PRIVATE UINT2
LsppRxUdpHdrCheckSumValidate (UINT1 *pBuf, INT2 i2Len, UINT4 u4Dest,
                              UINT4 u4Src, tLsppUdpHeader * pUdpHdr)
{
    UINT4               u4Sum = 0;
    UINT2               u2Tmp;

    /* Adding the Pseudo Header */
    u4Sum += (u4Src >> 16);
    u4Sum += (u4Src & IP_BIT_ALL);
    u4Sum += (u4Dest >> 16);
    u4Sum += (u4Dest & IP_BIT_ALL);
    u4Sum += (UINT2) IPPROTO_UDP;    /* 00-PROTO */
    u4Sum += (UINT2) i2Len;

    /* Adding the Udp Header. */
    u4Sum += (pUdpHdr->u2Src_u_port);
    u4Sum += (pUdpHdr->u2Dest_u_port);
    u4Sum += (pUdpHdr->u2Len);
    u4Sum += (pUdpHdr->u2Cksum);

    /* Calculating the CheckSum for the Udp DATA Alone */
    u2Tmp = (UINT2) (~(UtlIpCSumLinBuf ((CONST INT1 *) pBuf,
                                        i2Len - LSPP_UDP_HDR_LEN)));

    /* Converting to Host Order is not required. as it is in the host order */
    u4Sum += u2Tmp;

    /* One's Complement Addition */
    u4Sum = (UINT4) ((u4Sum >> 16) + (u4Sum & 0xffffL));
    u2Tmp = (UINT2) ((u4Sum >> 16) + (u4Sum & 0xffffL));

    return ((UINT2) (~u2Tmp));
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxValidateIpUdpHeader
 *
 * DESCRIPTION      : This function is to validate IP and UDP Header
 *
 * Input(s)         : pPkt - Pointer to received buffer
 *                    pLsppEchoMsg - Pointer to Echo message
 *
 * OUTPUT           : NONE
 * 
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 * 
 **************************************************************************/
PUBLIC INT4
LsppRxValidateIpUdpHeader (UINT1 **ppPkt, tLsppEchoMsg * pLsppEchoMsg)
{
    if (LsppRxGetAndValidateIPHeader (pLsppEchoMsg->u4ContextId, ppPkt,
                                      &(pLsppEchoMsg->LsppIpHeader))
        != OSIX_SUCCESS)
    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId, LSPP_IP_HDR_VALIDATION_FAILED,
                  "LsppRxGetAndValidateIPUdpHeader");
        return OSIX_FAILURE;
    }

    if (LsppRxGetAndValidateUdpHeader (ppPkt, pLsppEchoMsg->u4ContextId,
                                       &(pLsppEchoMsg->LsppUdpHeader))
        != OSIX_SUCCESS)
    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId, LSPP_UDP_VALIDATION_FAILED,
                  "LsppRxGetAndValidateIPUdpHeader");
        return OSIX_FAILURE;
    }

    if (LsppRxUdpHdrCheckSumValidate (*ppPkt, pLsppEchoMsg->LsppUdpHeader.u2Len,
                                      pLsppEchoMsg->LsppIpHeader.u4Dest,
                                      pLsppEchoMsg->LsppIpHeader.u4Src,
                                      &(pLsppEchoMsg->LsppUdpHeader)) != 0)
    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId,
                  LSPP_UDP_CHECKSUM_VALIDATION_FAILED,
                  "LsppRxValidateIpUdpHeader");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxVerifyCcCvCabability 
 * DESCRIPTION      : This function is to validate CC and CV capability
 * Input(s)         : u1CcReceived - Cc as in the receivd packet
 *                    u1CcSelected - Configured CC for this Pw
 *                    u1CvSelected - Configured Cv for this Pw 
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 **************************************************************************/
PUBLIC INT4
LsppRxVerifyCcCvCabability (UINT4 u4ContextId, UINT1 u1CcReceived,
                            UINT1 u1CcSelected, UINT1 u1CvSelected)
{
    if (!(u1CvSelected & LSPP_VCCV_CV_TYPE))
    {
        /* Lsp Ping is assigned CV type. So returning Failure */
        LSPP_LOG (u4ContextId, LSPP_CV_VALIDATION_FAILED,
                  "LsppRxVerifyCcCvCabability");
        return OSIX_FAILURE;
    }

    if (u1CcReceived != u1CcSelected)
    {
        /* Mismatch in CC received and Configured for that Pw. So
         * returning Failure*/
        LSPP_LOG (u4ContextId, LSPP_CC_VALIDATION_FAILED,
                  "LsppRxVerifyCcCvCabability");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxGetAchTunnelMepTlv
 * DESCRIPTION      : This function is to get the ACH LSP Mep TLV's
 * Input(s)         : pPkt - Linear Buffer with Receivd contents
 *                    pLsppIpLspMepTlv - To store the LSP IP MEP tlv 
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 **************************************************************************/
VOID
LsppRxGetAchTunnelMepTlv (UINT1 **ppPkt, tMplsIpLspMepTlv * pLsppIpLspMepTlv)
{
    /* Extract Global ID */
    LSPP_GET_4BYTE (pLsppIpLspMepTlv->GlobalNodeId.u4GlobalId, *ppPkt);

    /* Extract Node ID */
    LSPP_GET_4BYTE (pLsppIpLspMepTlv->GlobalNodeId.u4NodeId, *ppPkt);

    /* Extract Tunnel Number */
    LSPP_GET_2BYTE (pLsppIpLspMepTlv->u2TnlNum, *ppPkt);

    /* Extract LSP ID */
    LSPP_GET_2BYTE (pLsppIpLspMepTlv->u2LspNum, *ppPkt);

    return;

}

/***************************************************************************
 * FUNCTION NAME    : LsppRxGetAchPwMepTlv 
 * DESCRIPTION      : This function is to get the Pw Ach Mep TLV
 * Input(s)         : pPkt - Linear Buffer with Receivd contents
 *                    pLsppIpPwMepTlv - To store the Pw IP MEP tlv 
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 **************************************************************************/
VOID
LsppRxGetAchPwMepTlv (UINT1 **ppPkt,
                      tMplsIpPwMepTlv * pIpPwMepTlv, UINT2 *pu2TlvLen)
{
    UINT1               u1PadCount = 0;

    /* Extract AGI Type */
    LSPP_GET_1BYTE (pIpPwMepTlv->u1AgiType, *ppPkt);

    /* Extract AGI Len */
    LSPP_GET_1BYTE (pIpPwMepTlv->u1AgiLen, *ppPkt);

    /* Extract AGI Value */
    LSPP_GET_NBYTE (pIpPwMepTlv->au1Agi, *ppPkt, (UINT4) pIpPwMepTlv->u1AgiLen);

    /* Extract Global ID */
    LSPP_GET_4BYTE (pIpPwMepTlv->u4GlobalId, *ppPkt);

    /* Extract Node ID */
    LSPP_GET_4BYTE (pIpPwMepTlv->u4NodeId, *ppPkt);

    /* Extract AC_ID */
    LSPP_GET_4BYTE (pIpPwMepTlv->u4AcId, *ppPkt);

    /* Calculate the number of bytes to be skipped which was 
     * added for 4 byte alignment.*/
    u1PadCount = (UINT1) ((LSPP_BYTE_PAD_MAX_LEN -
                           (*pu2TlvLen % LSPP_BYTE_PAD_MAX_LEN))
                          % LSPP_BYTE_PAD_MAX_LEN);

    *ppPkt += u1PadCount;

    *pu2TlvLen = (UINT2) ((*pu2TlvLen) + u1PadCount);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxGetAchIccMepTlv 
 * DESCRIPTION      : This function is to get the ICC Mep TLV
 * Input(s)         : pPkt - Linear Buffer with Receivd contents
 *                    pMplsIccMepTlv - To store the Icc MEP tlv 
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 **************************************************************************/
VOID
LsppRxGetAchIccMepTlv (UINT1 **ppPkt, tMplsIccMepTlv * pIccMepTlv)
{
    /* Extract MEG ID */
    LSPP_GET_NBYTE (pIccMepTlv->au1Icc, *ppPkt, (UINT4) MPLS_ICC_LENGTH);
    LSPP_GET_NBYTE (pIccMepTlv->au1Umc, *ppPkt, (UINT4) MPLS_UMC_LENGTH);

    /* Extract MEP Index */
    LSPP_GET_2BYTE (pIccMepTlv->u2MepIndex, *ppPkt);

    /* Remove padded 1 byte */
    *ppPkt += 1;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxGetAchMipTlv 
 * DESCRIPTION      : This function is to get the ICC Mip TLV
 * Input(s)         : pPkt - Linear Buffer with Receivd contents
 *                    pMplsMipTlv - Pointer to store Mip Tlv
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 **************************************************************************/
VOID
LsppRxGetAchMipTlv (UINT1 **ppPkt, tMplsMipTlv * pMplsMipTlv)
{
    /* Extract Global ID */
    LSPP_GET_4BYTE (pMplsMipTlv->GlobalNodeId.u4GlobalId, *ppPkt);

    /* Extract Node ID */
    LSPP_GET_4BYTE (pMplsMipTlv->GlobalNodeId.u4NodeId, *ppPkt);

    /* Extract If Num */
    LSPP_GET_4BYTE (pMplsMipTlv->u4IfNum, *ppPkt);

    return;
}

 /***************************************************************************
 * FUNCTION NAME    : LsppRxGetAchTlv
 * DESCRIPTION      : This function is to get the ALV TLV's
 * Input(s)         : pPkt - Linear Buffer with received packet
 *                    pLsppAchInfo  - Pointer to ACH Info Strucutre to store
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 **************************************************************************/
UINT4
LsppRxGetAchTlv (UINT1 **ppPkt, tLsppAchInfo * pLsppAchInfo,
                 UINT2 u2AchTlvLength, UINT4 u4ContextId)
{
    UINT2               u2TlvType = 0;
    UINT2               u2TlvLen = 0;

    /* Fetch all ACH TLVs */
    while (u2AchTlvLength > 0)
    {
        /* Extract Tlv type and length */
        LSPP_GET_2BYTE (u2TlvType, *ppPkt);
        LSPP_GET_2BYTE (u2TlvLen, *ppPkt);

        if (u2TlvType == LSPP_ACH_TYPE_IP_TUNNEL_MEP)
        {
            if (pLsppAchInfo->u1AchTlvPresent & LSPP_ACH_IP_LSP_MEP_TLV)
            {
                LSPP_LOG (u4ContextId, LSPP_LSP_MEP_ALREADY_PRESENT,
                          "LsppRxGetAchTlv");
                return OSIX_FAILURE;
            }

            LsppRxGetAchTunnelMepTlv (ppPkt,
                                      &(pLsppAchInfo->LsppMepTlv.unMepTlv.
                                        IpLspMepTlv));

            /* Updating Mep Tlv is present */
            pLsppAchInfo->u1AchTlvPresent =
                (pLsppAchInfo->u1AchTlvPresent | LSPP_ACH_IP_LSP_MEP_TLV);
        }
        else if (u2TlvType == LSPP_ACH_TYPE_IP_PW_MEP)
        {
            if (pLsppAchInfo->u1AchTlvPresent & LSPP_ACH_IP_PW_MEP_TLV)
            {
                LSPP_LOG (u4ContextId, LSPP_PW_MEP_ALREADY_PRESENT,
                          "LsppRxGetAchTlv");
                return OSIX_FAILURE;
            }

            LsppRxGetAchPwMepTlv (ppPkt,
                                  &(pLsppAchInfo->LsppMepTlv.unMepTlv.
                                    IpPwMepTlv), &u2TlvLen);

            /* Updating Mep Tlv is present */
            pLsppAchInfo->u1AchTlvPresent =
                (pLsppAchInfo->u1AchTlvPresent | LSPP_ACH_IP_PW_MEP_TLV);
        }
        else if (u2TlvType == LSPP_ACH_TYPE_ICC_MEP)
        {
            if (pLsppAchInfo->u1AchTlvPresent & LSPP_ACH_ICC_MEP_TLV)
            {
                LSPP_LOG (u4ContextId, LSPP_ICC_MEP_ALREADY_PRESENT,
                          "LsppRxGetAchTlv");
                return OSIX_FAILURE;
            }

            LsppRxGetAchIccMepTlv (ppPkt,
                                   &(pLsppAchInfo->LsppMepTlv.unMepTlv.
                                     IccMepTlv));

            /* Updating Mep Tlv is present */
            pLsppAchInfo->u1AchTlvPresent =
                (pLsppAchInfo->u1AchTlvPresent | LSPP_ACH_ICC_MEP_TLV);
        }
        else if (u2TlvType == LSPP_ACH_TYPE_MIP)
        {
            if (pLsppAchInfo->u1AchTlvPresent & LSPP_ACH_MIP_TLV)
            {
                LSPP_LOG (u4ContextId, LSPP_MIP_ALREADY_PRESENT,
                          "LsppRxGetAchTlv");
                return OSIX_FAILURE;
            }

            LsppRxGetAchMipTlv (ppPkt, &(pLsppAchInfo->LsppMipTlv));

            /* Updating Mip Tlv is present */
            pLsppAchInfo->u1AchTlvPresent =
                (UINT1) (pLsppAchInfo->u1AchTlvPresent | LSPP_ACH_MIP_TLV);
        }
        else
        {
            LSPP_LOG (u4ContextId, LSPP_INVALID_ACH_TLV_PRESENT,
                      "LsppRxGetAchTlv");
            return OSIX_FAILURE;
        }

        /* Decrementing Total ACH Tlv length */
        u2AchTlvLength = (UINT2) (u2AchTlvLength -
                                  (u2TlvLen + LSPP_TLV_HEADER_LEN));
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxGetPathIdFromLabel
 * DESCRIPTION      : This function is to get the Path Info from the Incoming 
 *                    label
 * Input(s)         : u4ContextId - Received Context
 *                    u4IfNum     - Received Interface Index
 *                    u4Label     - Received Label
 *                    pLsppPathId - Pointer to Path ID structure to store the 
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 ***************************************************************************/
PUBLIC INT4
LsppRxGetPathIdFromLabel (UINT4 u4ContextId, UINT4 u4IfNum, UINT4 u4Label,
                          tLsppPathId * pLsppPathId)
{
    tLsppExtInParams   *pLsppExtInParams = NULL;
    tLsppExtOutParams  *pLsppExtOutParams = NULL;

    if ((pLsppExtInParams = (tLsppExtInParams *)
         MemAllocMemBlk (LSPP_FSLSPPINPUTPARAMS_POOLID)) == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((pLsppExtOutParams = (tLsppExtOutParams *)
         MemAllocMemBlk (LSPP_FSLSPPOUTPUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        return OSIX_FAILURE;
    }

    MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
    MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));

    /* Fill the LsppPort function's input structure */
    pLsppExtInParams->u4RequestType = LSPP_GET_PATH_FRM_INLBL_INFO;
    pLsppExtInParams->u4ContextId = u4ContextId;

    pLsppExtInParams->InLblInfo.u4Inlabel = u4Label;
    pLsppExtInParams->InLblInfo.u4InIf = u4IfNum;

    if (LsppPortHandleExtInteraction (pLsppExtInParams,
                                      pLsppExtOutParams) != OSIX_SUCCESS)
    {
        LSPP_LOG (u4ContextId, LSPP_FETCH_FROM_EXT_MOD_FAILED,
                  "LsppRxGetPathIdFromLabel");
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);
        return OSIX_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtInParams);

    pLsppPathId->u1PathType = pLsppExtOutParams->u1PathType;

    switch (pLsppExtOutParams->u1PathType)
    {
        case LSPP_MPLS_PATH_TYPE_TNL:

            pLsppPathId->unPathId.TnlInfo.u4SrcTnlId =
                pLsppExtOutParams->OutTnlInfo.TnlInfo.u4SrcTnlNum;

            pLsppPathId->unPathId.TnlInfo.u4LspId =
                pLsppExtOutParams->OutTnlInfo.TnlInfo.u4LspNum;

            if (pLsppExtOutParams->OutTnlInfo.TnlInfo.SrcNodeId.u4NodeType ==
                LSPP_MPLS_ADDR_TYPE_IPV4)
            {
                pLsppPathId->unPathId.TnlInfo.SrcNodeId.
                    MplsRouterId.u4_addr[0] =
                    pLsppExtOutParams->OutTnlInfo.TnlInfo.SrcNodeId.
                    MplsRouterId.u4_addr[0];

                pLsppPathId->unPathId.TnlInfo.DstNodeId.
                    MplsRouterId.u4_addr[0] =
                    pLsppExtOutParams->OutTnlInfo.TnlInfo.DstNodeId.
                    MplsRouterId.u4_addr[0];
                pLsppPathId->u1PathType = LSPP_PATH_TYPE_RSVP_IPV4;
            }
            else if (pLsppExtOutParams->OutTnlInfo.TnlInfo.SrcNodeId.
                     u4NodeType == LSPP_MPLS_ADDR_TYPE_IPV6)
            {

                MEMCPY (pLsppPathId->unPathId.TnlInfo.SrcNodeId.
                        MplsRouterId.u1_addr,
                        pLsppExtOutParams->OutTnlInfo.TnlInfo.SrcNodeId.
                        MplsRouterId.u1_addr, LSPP_IPV6_ADDR_LEN);

                MEMCPY (pLsppPathId->unPathId.TnlInfo.DstNodeId.
                        MplsRouterId.u1_addr,
                        pLsppExtOutParams->OutTnlInfo.TnlInfo.DstNodeId.
                        MplsRouterId.u1_addr, LSPP_IPV6_ADDR_LEN);

                pLsppPathId->u1PathType = LSPP_PATH_TYPE_RSVP_IPV6;
            }
            else if (pLsppExtOutParams->OutTnlInfo.u1TnlSgnlPrtcl
                     == LSPP_MPLS_TE_SIGPROTO_NONE)
            {
                pLsppPathId->u1PathType = LSPP_PATH_TYPE_STATIC_TNL;

                if (pLsppExtOutParams->OutTnlInfo.TnlInfo.SrcNodeId.u4NodeType
                    != LSPP_MPLS_ADDR_TYPE_GLOBAL_NODE_ID)
                {
                    pLsppPathId->unPathId.TnlInfo.SrcNodeId.
                        MplsRouterId.u4_addr[0] =
                        pLsppExtOutParams->OutTnlInfo.TnlInfo.SrcNodeId.
                        MplsRouterId.u4_addr[0];

                    pLsppPathId->unPathId.TnlInfo.DstNodeId.
                        MplsRouterId.u4_addr[0] =
                        pLsppExtOutParams->OutTnlInfo.TnlInfo.DstNodeId.
                        MplsRouterId.u4_addr[0];
                }
            }

            break;

        case LSPP_MPLS_PATH_TYPE_LSP:

            pLsppPathId->unPathId.LdpInfo.u4AddrType =
                pLsppExtOutParams->OutLdpInfo.LdpInfo.u4AddrType;

            pLsppPathId->unPathId.LdpInfo.u4PrefixLength =
                pLsppExtOutParams->OutLdpInfo.LdpInfo.u4PrefixLength;

            if (pLsppPathId->unPathId.LdpInfo.u4AddrType ==
                LSPP_MPLS_ADDR_TYPE_IPV4)
            {
                pLsppPathId->unPathId.LdpInfo.LdpPrefix.u4_addr[0] =
                    pLsppExtOutParams->OutLdpInfo.LdpInfo.LdpPrefix.u4_addr[0];

                pLsppPathId->u1PathType = LSPP_PATH_TYPE_LDP_IPV4;
            }
            else if (pLsppPathId->unPathId.LdpInfo.u4AddrType ==
                     LSPP_MPLS_ADDR_TYPE_IPV4)
            {
                MEMCPY (pLsppPathId->unPathId.LdpInfo.LdpPrefix.u1_addr,
                        pLsppExtOutParams->OutLdpInfo.LdpInfo.LdpPrefix.u1_addr,
                        LSPP_IPV6_ADDR_LEN);

                pLsppPathId->u1PathType = LSPP_PATH_TYPE_LDP_IPV6;
            }
            break;

        case LSPP_MPLS_PATH_TYPE_PW:

            if (pLsppExtOutParams->OutPwDetail.PwFecInfo.DstNodeId.u4NodeType ==
                LSPP_MPLS_ADDR_TYPE_IPV4)
            {
                pLsppPathId->unPathId.PwInfo.PeerAddr.u4_addr[0] =
                    pLsppExtOutParams->OutPwDetail.PwFecInfo.DstNodeId.
                    MplsRouterId.u4_addr[0];

                if (pLsppExtOutParams->OutPwDetail.PwFecInfo.u1PwVcOwner ==
                    LSPP_PW_OWNER_PWIDFEC_SIGNALLING)
                {
                    pLsppPathId->u1PathType = LSPP_PATH_TYPE_FEC_128;
                }
                else if (pLsppExtOutParams->OutPwDetail.PwFecInfo.u1PwVcOwner ==
                         LSPP_PW_OWNER_GENFEC_SIGNALLING)
                {
                    pLsppPathId->u1PathType = LSPP_PATH_TYPE_FEC_129;
                }
                else if (pLsppExtOutParams->OutPwDetail.PwFecInfo.u1PwVcOwner ==
                         LSPP_PW_OWNER_MANUAL)
                {
                    pLsppPathId->u1PathType = LSPP_PATH_TYPE_STATIC_PW;
                }
            }
            else if (pLsppExtOutParams->OutPwDetail.PwFecInfo.DstNodeId.
                     u4NodeType == LSPP_MPLS_ADDR_TYPE_IPV6)
            {
                MEMCPY (pLsppPathId->unPathId.PwInfo.PeerAddr.u1_addr,
                        pLsppExtOutParams->OutPwDetail.PwFecInfo.DstNodeId.
                        MplsRouterId.u1_addr, LSPP_IPV6_ADDR_LEN);
            }

            pLsppPathId->unPathId.PwInfo.u4PwVcId =
                pLsppExtOutParams->OutPwDetail.PwFecInfo.u4VcId;

            break;
        case LSPP_MPLS_PATH_TYPE_MEG:
            pLsppPathId->unPathId.MepIndices.u4MegIndex =
                pLsppExtOutParams->OutMegDetail.MegInfo.MegIndices.u4MegIndex;
            pLsppPathId->unPathId.MepIndices.u4MeIndex =
                pLsppExtOutParams->OutMegDetail.MegInfo.MegIndices.u4MeIndex;
            pLsppPathId->unPathId.MepIndices.u4MpIndex =
                pLsppExtOutParams->OutMegDetail.MegInfo.MegIndices.u4MpIndex;

            pLsppPathId->u1PathType = LSPP_PATH_TYPE_MEP;
            break;

        default:

            LSPP_LOG (u4ContextId, LSPP_INVALID_PATH_ID_TYPE,
                      "LsppRxGetPathIdFromLabel");
            MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtOutParams);
            return OSIX_FAILURE;
    }
    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtOutParams);
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxGetMplsLabelStack
 * DESCRIPTION      : This function is to fill the MPLS Label Stack
 * Input(s)         : pPkt          - Pointer to Linear Buffer with 
 *                                    received contents
 *                    pLsppEchoMsg  - Pointer to Echo message strucutre
 *                    pu1LableCount - To store the label count
 *                    pu4MplsLabel  - To store the bottom most label
 * OUTPUT           : NONE
 * RETURNS          : NONE 
 ***************************************************************************/
PUBLIC VOID
LsppRxGetMplsLabelStack (UINT1 **ppPkt, tLsppEchoMsg * pLsppEchoMsg,
                         UINT1 *pu1LabelCount, UINT4 *pu4OuterLabel,
                         UINT4 *pu4Offset)
{
    INT1                i1Count = 0;
    UINT4               u4Label = MPLS_INVALID_LABEL;
    BOOL1               bValidLabel = OSIX_TRUE;
    UINT1               u1PathType = 0;
    UINT1               u1LblAction = 0;
    UINT1               u1PrevLblAction = 0;
    UINT4               u4PrevLabel = MPLS_INVALID_LABEL;

    *pu4OuterLabel = MPLS_INVALID_LABEL;
    *pu1LabelCount = 0;

    /* Get the bottom most label to get the path information
     * if GAL Label present ingnore and get the previous label */
    do
    {
        bValidLabel = OSIX_TRUE;

        if (i1Count >= LSPP_MAX_LABEL_STACK)
        {
            return;
        }

        LsppRxGetMplsLabel (ppPkt, &(pLsppEchoMsg->LblInfo[i1Count]));

        *pu4Offset = (UINT4) (*pu4Offset + LSPP_MPLS_LABEL_LEN);

        if (pLsppEchoMsg->LblInfo[i1Count].u4Label == LSPP_GAL_LABEL)
        {
            if (u1PrevLblAction == MPLS_LABEL_ACTION_POP)
            {
                *pu4OuterLabel = u4PrevLabel;
            }
            break;
        }

        u4Label = pLsppEchoMsg->LblInfo[i1Count].u4Label;

        /* Fetch Path Id from Label */
        if ((*pu4OuterLabel != MPLS_INVALID_LABEL)
            || (LSPP_LBL_ROUTER_ALERT == u4Label))
        {
            continue;
        }

        MEMSET ((UINT1 *) &(pLsppEchoMsg->LsppPathId), 0, sizeof (tLsppPathId));
        MEMSET ((UINT1 *) &(pLsppEchoMsg->LsppPathInfo), 0,
                sizeof (tLsppPathInfo));

        if (LsppRxGetPathIdFromLabel (pLsppEchoMsg->u4ContextId,
                                      pLsppEchoMsg->u4IfIndex,
                                      u4Label, &(pLsppEchoMsg->LsppPathId))
            != OSIX_SUCCESS)
        {
            bValidLabel = OSIX_FALSE;
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_GET_PATH_ID_FROM_LABEL_FAILED,
                      "LsppRxGetMplsLabelStack", u4Label);
        }

        if (bValidLabel == OSIX_TRUE)
        {
            /* Fetch Fec Info */
            if (LsppCoreGetPathInfo (pLsppEchoMsg->u4ContextId,
                                     &(pLsppEchoMsg->LsppPathId),
                                     &(pLsppEchoMsg->LsppPathInfo),
                                     pLsppEchoMsg->u4IfIndex) != OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppEchoMsg->u4ContextId,
                          LSPP_GET_PATH_INFO_FROM_PATH_ID_FAILED,
                          "LsppRxGetMplsLabelStack");

                continue;
            }

            u1PathType = pLsppEchoMsg->LsppPathInfo.u1PathType;
            u1LblAction =
                pLsppEchoMsg->LsppPathInfo.TeTnlInfo.InSegInfo.u1LblAction;

            if ((pLsppEchoMsg->LblInfo[i1Count].u1SI == 1) ||
                (((u1PathType == LSPP_PATH_TYPE_RSVP_IPV4) ||
                  (u1PathType == LSPP_PATH_TYPE_STATIC_TNL)) &&
                 (u1LblAction == MPLS_LABEL_ACTION_SWAP)))
            {
                *pu4OuterLabel = u4Label;
            }

            if (u1LblAction == MPLS_LABEL_ACTION_POP)
            {
                u1PrevLblAction = u1LblAction;
                u4PrevLabel = u4Label;
            }
        }
    }
    while (pLsppEchoMsg->LblInfo[i1Count++].u1SI != 1);

    *pu1LabelCount = i1Count;

    return;
}

/***************************************************************************
 * FUNCTION NAME    : LsppRxUpdateLabelStack
 * DESCRIPTION      : This function is to update the label stack without 
 *                    RAL Label before handing over to LSP core. 
 * Input(s)         : pLsppEchoMsg - Echo message where Label stack to modify 
 *                                   is present
 *                    pu1LabelStackDepth - Specifies the Label stack depth
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 ***************************************************************************/
VOID
LsppRxUpdateLabelStack (tLsppEchoMsg * pLsppEchoMsg, UINT1 *pu1LabelStackDepth)
{
    UINT1               u1LabelCount = 0;

    /* The RAL used for OAM encapsulation purpose are removed since it 
     * need not be validated.
     */

    if (*pu1LabelStackDepth > LSPP_MAX_LABEL_STACK)
    {
        return;
    }

    while (u1LabelCount < (*pu1LabelStackDepth))
    {
        if ((pLsppEchoMsg->LblInfo[u1LabelCount].u4Label) ==
            LSPP_LBL_ROUTER_ALERT)
        {
            (*pu1LabelStackDepth)--;
            break;
        }

        u1LabelCount++;
    }

    /* Udpate the remaining labels in the stack. */
    while (u1LabelCount < (*pu1LabelStackDepth))
    {
        pLsppEchoMsg->LblInfo[u1LabelCount].u4Label =
            pLsppEchoMsg->LblInfo[u1LabelCount + 1].u4Label;

        u1LabelCount++;
    }
}

/******************************************************************************
 * Function   : LsppRxSendBfdBtInfo
 * Description: This function is to Send the next trace route request on WFR 
 *              timer or on receiving a reply.
 * Input      : pLsppPingTraceEntry - Ping Trace Entry
 *              pDsMapTlv           - Pointer to DSMAP TLV is present
 * Output     : OSIX_SUCCESS or OSIX_FAILURE
 * Returns    : None
 *****************************************************************************/
UINT4
LsppRxSendBfdBtInfo (tLsppFsLsppPingTraceTableEntry * pLsppPingTraceEntry,
                     tLsppBfdDiscTlv * pLsppBfdDiscTlv, UINT1 u1ReturnCode)
{
    tLsppExtInParams   *pLsppExtInParams = NULL;
    tLsppExtOutParams  *pLsppExtOutParams = NULL;

    if ((pLsppExtInParams = (tLsppExtInParams *)
         MemAllocMemBlk (LSPP_FSLSPPINPUTPARAMS_POOLID)) == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((pLsppExtOutParams = (tLsppExtOutParams *)
         MemAllocMemBlk (LSPP_FSLSPPOUTPUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        return OSIX_FAILURE;
    }

    MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
    MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));

    pLsppExtInParams->u4RequestType = LSPP_BFD_PROCESS_BTSTRAP_INFO;
    pLsppExtInParams->u4ContextId =
        pLsppPingTraceEntry->MibObject.u4FsLsppContextId;

    pLsppExtInParams->InBfdInfo.bIsEgress = OSIX_FALSE;

    pLsppExtInParams->InBfdInfo.u4SessionIndex =
        pLsppPingTraceEntry->u4BfdSessionIndex;

    if (u1ReturnCode == LSPP_EGRESS_FOR_FEC_AT_STACK_DEPTH)
    {
        pLsppExtInParams->InBfdInfo.u4BfdDiscriminator =
            pLsppBfdDiscTlv->u4Discriminator;

        pLsppExtInParams->InBfdInfo.eErrorCode = (tLsppErrorCode) OSIX_FALSE;
    }
    else
    {
        pLsppExtInParams->InBfdInfo.eErrorCode = (tLsppErrorCode) OSIX_TRUE;
    }

    MEMCPY (&(pLsppExtInParams->InBfdInfo.PathId),
            &(pLsppPingTraceEntry->PathId), sizeof (tLsppPathId));

    /* Call the LSPP exit function to give the BFD disc to BFD module */
    if (LsppPortHandleExtInteraction
        (pLsppExtInParams, pLsppExtOutParams) != OSIX_SUCCESS)
    {
        LSPP_LOG (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
                  LSPP_FETCH_FROM_EXT_MOD_FAILED, "LsppRxSendBfdBtInfo");
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);
        return OSIX_FAILURE;
    }
    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtInParams);
    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtOutParams);

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : LsppRxIpAddrValidate
 * Description: This function is to verify the Src and Destination Ip address 
 *              value and other IP header options
 * Input      : pLsppEchoMsg - Echo Message with Ip Header
 *              u1RcvdMsgType - Echo Reply / Request
 * Output     : OSIX_SUCCESS or OSIX_FAILURE
 * Returns    : Validation Result
 *****************************************************************************/
PUBLIC UINT4
LsppRxIpAddrValidate (tLsppEchoMsg * pLsppEchoMsg, UINT1 u1RcvdMsgType)
{
    tLsppIpRouterAlertOpt LsppIpRouterAlertOpt;
    UINT1              *pIpOtion = NULL;

    MEMSET (&LsppIpRouterAlertOpt, 0, sizeof (tLsppIpRouterAlertOpt));

    /* Verifying Source Ip range */
    if (LsppUtilVerifyIpAddrRange (pLsppEchoMsg->LsppIpHeader.u4Src)
        != OSIX_SUCCESS)
    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId,
                  LSPP_SRC_IP_IN_INVALID_RANGE, "LsppRxIpAddrValidate");
        return OSIX_FAILURE;
    }

    /* Verifying Destination Ip range. When packet is received from
     * socket this verification is skipped as that is taken taken at 
     * socket layer*/
    if ((u1RcvdMsgType == LSPP_ECHO_REPLY) &&
        (pLsppEchoMsg->u1EncapType != LSPP_ENCAP_TYPE_IP))
    {
        if (LsppUtilVerifyIpAddrRange (pLsppEchoMsg->LsppIpHeader.u4Dest)
            != OSIX_SUCCESS)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_DEST_IP_IN_INVALID_RANGE, "LsppRxIpAddrValidate");
            return OSIX_FAILURE;

        }
    }
    if (u1RcvdMsgType == LSPP_ECHO_REQUEST)
    {
        /* Check Destination Ip Address in the range 127.0.0.0 */
        if (((pLsppEchoMsg->LsppIpHeader.u4Dest & 0xff000000) !=
             LSPP_IP_LOOPBACK_PREFIX) ||
            ((pLsppEchoMsg->LsppIpHeader.u4Src & 0xff000000) ==
             LSPP_IP_LOOPBACK_PREFIX) ||
            (pLsppEchoMsg->LsppIpHeader.u1Ttl != 1))
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_INVALID_IP_HEADER_IN_REQ, "LsppRxIpAddrValidate");
            return OSIX_FAILURE;
        }

        /* Fetching Router Alert option Values */
        pIpOtion = pLsppEchoMsg->LsppIpHeader.au1Options;
        MEMCPY (&(LsppIpRouterAlertOpt.u1Type), pIpOtion, 1);
        pIpOtion = pIpOtion + 1;
        MEMCPY (&(LsppIpRouterAlertOpt.u1Len), pIpOtion, 1);

        if ((LsppIpRouterAlertOpt.u1Type !=
             LSPP_IP_ROUTER_ALERT_OPTION_TYPE) ||
            (LsppIpRouterAlertOpt.u1Len != LSPP_IP_ROUTER_ALERT_OPTION_LEN))
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_INVALID_IP_HEADER_IN_REQ, "LsppRxIpAddrValidate");
            return OSIX_FAILURE;
        }
    }
    else if (u1RcvdMsgType == LSPP_ECHO_REPLY)
    {
        if ((pLsppEchoMsg->LsppIpHeader.u4Src & 0xff000000) ==
            LSPP_IP_LOOPBACK_PREFIX)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_INVALID_SRC_IP_IN_REPLY, "LsppRxIpAddrValidate");
            return OSIX_FAILURE;
        }

        if (pLsppEchoMsg->u1EncapType != LSPP_ENCAP_TYPE_IP)
        {
            if (NetIpv4IfIsOurAddressInCxt (pLsppEchoMsg->u4ContextId,
                                            pLsppEchoMsg->LsppIpHeader.u4Dest)
                != OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppEchoMsg->u4ContextId,
                          LSPP_INVALID_DEST_IP_IN_REPLY,
                          "LsppRxIpAddrValidate");
                return OSIX_FAILURE;

            }
        }
    }
    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : LsppRxFwdPacket
 * Description: This function is to forward the packet to Ingress node 
 * Input      : pLsppEchoMsg - Echo Message with Ip Header
 * Output     : NONE
 * Returns    : OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/
PUBLIC UINT4
LsppRxFwdPacket (tLsppEchoMsg * pLsppEchoMsg)
{
    tLsppIpRouterAlertOpt LsppIpRouterAlertOpt;
    INT4                i4RetVal = 0;
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;

    MEMSET (&LsppIpRouterAlertOpt, 0, sizeof (tLsppIpRouterAlertOpt));

    /* Including Router Alert to the packet */
    pLsppEchoMsg->LsppIpHeader.u1Hlen =
        (IP_HDR_LEN / LSPP_IP_HEADER_LEN_IN_BYTES) + 1;

    LsppIpRouterAlertOpt.u1Type = LSPP_IP_ROUTER_ALERT_OPTION_TYPE;
    LsppIpRouterAlertOpt.u1Len = LSPP_IP_ROUTER_ALERT_OPTION_LEN;
    pLsppEchoMsg->u1IsRouteExists = OSIX_TRUE;

    MEMCPY (pLsppEchoMsg->LsppIpHeader.au1Options,
            &LsppIpRouterAlertOpt, sizeof (tLsppIpRouterAlertOpt));

    /* Filling UDP Header params */
    pLsppEchoMsg->LsppUdpHeader.u2Src_u_port = LSPP_UDP_SRC_PORT;
    pLsppEchoMsg->LsppUdpHeader.u2Dest_u_port = LSPP_UDP_DEST_PORT;

    /* Calculating packet size */
    LsppUtilCalculatePktSize (pLsppEchoMsg);

    /* Tx packet. */
    /* Allocate buffer for Tx Packet. */
    pMsg = LSPP_ALLOC_CRU_BUF ((pLsppEchoMsg->u4EncapHdrLen +
                                pLsppEchoMsg->u4LsppPduLen +
                                MPLS_MAX_L2HEADER_LEN +
                                CFA_ENET_V2_HEADER_SIZE),
                               pLsppEchoMsg->u4EncapHdrLen +
                               CFA_ENET_V2_HEADER_SIZE + MPLS_MAX_L2HEADER_LEN);
    if (pMsg == NULL)
    {
        LSPP_CMN_TRC (pLsppEchoMsg->u4ContextId,
                      LSPP_CRU_BUF_ALLOC_FAILED, "LsppCoreSendEchoReply");
        return OSIX_FAILURE;
    }

    i4RetVal = LsppTxFrameAndSendPdu (pLsppEchoMsg, NULL, NULL, pMsg);
    if (i4RetVal == OSIX_FAILURE)
    {
        LSPP_RELEASE_CRU_BUF (pMsg, 0);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

#endif
