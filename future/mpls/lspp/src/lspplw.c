
/******************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lspplw.c,v 1.5 2012/03/26 13:51:13 siva Exp $
 * 
 *****************************************************************************/
#include "lsppinc.h"

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsLsppGlobalConfigTable
 Input       :  The Indices
                FsLsppContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsLsppGlobalConfigTable (UINT4 u4FsLsppContextId)
{
    tLsppFsLsppGlobalConfigTableEntry LsppGlobalConfigEntry;
    tLsppFsLsppGlobalConfigTableEntry *pLsppGlobalConfigEntry = NULL;

    MEMSET (&LsppGlobalConfigEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));

    LsppGlobalConfigEntry.MibObject.u4FsLsppContextId = u4FsLsppContextId;
    pLsppGlobalConfigEntry =
        LsppGetFsLsppGlobalConfigTable (&LsppGlobalConfigEntry);

    if (pLsppGlobalConfigEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsLsppGlobalStatsTable
 Input       :  The Indices
                FsLsppContextId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsLsppGlobalStatsTable (UINT4 u4FsLsppContextId)
{
    tLsppFsLsppGlobalStatsTableEntry LsppGlobalStatsEntry;
    tLsppFsLsppGlobalStatsTableEntry *pLsppGlobalStatsEntry = NULL;

    MEMSET (&LsppGlobalStatsEntry, 0,
            sizeof (tLsppFsLsppGlobalStatsTableEntry));

    LsppGlobalStatsEntry.MibObject.u4FsLsppContextId = u4FsLsppContextId;
    pLsppGlobalStatsEntry =
        LsppGetFsLsppGlobalStatsTable (&LsppGlobalStatsEntry);

    if (pLsppGlobalStatsEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsLsppPingTraceTable
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsLsppPingTraceTable (UINT4 u4FsLsppContextId,
                                              UINT4 u4FsLsppSenderHandle)
{
    tLsppFsLsppPingTraceTableEntry LsppPingTraceEntry;
    tLsppFsLsppPingTraceTableEntry *pLsppPingTraceEntry = NULL;

    MEMSET (&LsppPingTraceEntry, 0, sizeof (tLsppFsLsppPingTraceTableEntry));

    if ((u4FsLsppSenderHandle < 1) ||
        (u4FsLsppSenderHandle > LSPP_MAX_SENDERS_HANDLE))
    {
        return SNMP_FAILURE;
    }

    LsppPingTraceEntry.MibObject.u4FsLsppContextId = u4FsLsppContextId;
    LsppPingTraceEntry.MibObject.u4FsLsppSenderHandle = u4FsLsppSenderHandle;

    pLsppPingTraceEntry = LsppGetFsLsppPingTraceTable (&LsppPingTraceEntry);

    if (pLsppPingTraceEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsLsppEchoSequenceTable
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
                FsLsppSequenceNumber
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsLsppEchoSequenceTable (UINT4 u4FsLsppContextId,
                                                 UINT4 u4FsLsppSenderHandle,
                                                 UINT4 u4FsLsppSequenceNumber)
{
    tLsppFsLsppEchoSequenceTableEntry LsppEchoSequenceEntry;
    tLsppFsLsppEchoSequenceTableEntry *pLsppEchoSequenceEntry = NULL;

    MEMSET (&LsppEchoSequenceEntry, 0,
            sizeof (tLsppFsLsppEchoSequenceTableEntry));

    if ((u4FsLsppSenderHandle < 1) ||
        (u4FsLsppSenderHandle > LSPP_MAX_SENDERS_HANDLE))
    {
        return SNMP_FAILURE;
    }

    if ((u4FsLsppSequenceNumber < 1) ||
        (u4FsLsppSequenceNumber > LSPP_MAX_SEQUENCE_ENTRY))
    {
        return SNMP_FAILURE;
    }

    LsppEchoSequenceEntry.MibObject.u4FsLsppContextId = u4FsLsppContextId;
    LsppEchoSequenceEntry.MibObject.u4FsLsppSenderHandle = u4FsLsppSenderHandle;
    LsppEchoSequenceEntry.MibObject.u4FsLsppSequenceNumber =
        u4FsLsppSequenceNumber;

    pLsppEchoSequenceEntry =
        LsppGetFsLsppEchoSequenceTable (&LsppEchoSequenceEntry);

    if (pLsppEchoSequenceEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsLsppHopTable
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
                FsLsppHopIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsLsppHopTable (UINT4 u4FsLsppContextId,
                                        UINT4 u4FsLsppSenderHandle,
                                        UINT4 u4FsLsppHopIndex)
{
    tLsppFsLsppHopTableEntry *pLsppHopEntryIn = NULL;
    tLsppFsLsppHopTableEntry *pLsppHopEntry = NULL;

    if ((pLsppHopEntryIn = MemAllocMemBlk (LSPP_FSLSPPHOPTABLE_POOLID)) == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppHopEntryIn, 0, sizeof (tLsppFsLsppHopTableEntry));

    if ((u4FsLsppSenderHandle < 1) ||
        (u4FsLsppSenderHandle > LSPP_MAX_SENDERS_HANDLE))
    {
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppHopEntryIn);
        return SNMP_FAILURE;
    }

    if ((u4FsLsppHopIndex < 1) || (u4FsLsppHopIndex > LSPP_MAX_HOP_ENTRY))
    {
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppHopEntryIn);
        return SNMP_FAILURE;
    }

    pLsppHopEntryIn->MibObject.u4FsLsppContextId = u4FsLsppContextId;
    pLsppHopEntryIn->MibObject.u4FsLsppSenderHandle = u4FsLsppSenderHandle;
    pLsppHopEntryIn->MibObject.u4FsLsppHopIndex = u4FsLsppHopIndex;

    pLsppHopEntry = LsppGetFsLsppHopTable (pLsppHopEntryIn);

    if (pLsppHopEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppHopEntryIn);
        return SNMP_FAILURE;
    }
    MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID, (UINT1 *) pLsppHopEntryIn);
    return SNMP_SUCCESS;

}
