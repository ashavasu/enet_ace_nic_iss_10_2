/******************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lsppport.c,v 1.28 2014/02/18 10:30:40 siva Exp $
 *
 * Description : This file contains the utility
 *               functions of the LSPP module
 *****************************************************************************/

#include "lsppinc.h"
#include "bfd.h"

#ifdef LSPP_TEST_WANTED
extern UINT4        gu4LsppUtTestNumber;
extern UINT4        gu4LsppUtStubsReqd;
#endif

/******************************************************************************
 * Function   : LsppPortHandleExtInteraction
 *
 * Description: This is the common function to call the external API's
 *              For calling a external API this function should be called with 
 *              required parameters. This function will identify the 
 *              Request type in the input paramaters and call the appropriate 
 *              API.
 *
 *Input       : pLsppExtInParams
 *
 * Output     : pLsppExtOutParams
 *
 * Returns    : OSIX_FAILURE/OSIX_SUCCESS
 *
 *****************************************************************************/

INT4
LsppPortHandleExtInteraction (tLsppExtInParams * pLsppExtInParams,
                              tLsppExtOutParams * pLsppExtOutParams)
{
    tBfdRespParams      BfdOutParams;
    tIpConfigInfo       IpInfo;
    tFmFaultMsg         FmFaultMsg;
    tCfaIfInfo          CfaIfInfo;
    tVcmRegInfo         VcmRegInfo;
    tMplsApiInInfo     *pMplsInApiInfo = NULL;
    tMplsApiOutInfo    *pMplsOutApiInfo = NULL;
    tBfdReqParams      *pBfdInParams = NULL;
    UINT1               au1ContextName[LSPP_MAX_CONTEXT_NAME_LEN];
    UINT4               u4ContextId = 0;
    UINT4               u4IfIndex = 0;
    UINT4               u4RequestType = 0;
    UINT4               u4L3IfIndex = 0;
    UINT4               u4MplsIfIndex = 0;
    UINT4               u4SrcAddr = 0;
    INT4                i4RetVal = 0;
    UINT1               u1CoRoutedLsp = 0;
    UINT1               u1PathDirection = LSPP_MPLS_DIRECTION_FORWARD;

    if ((pMplsInApiInfo = (tMplsApiInInfo *)
         MemAllocMemBlk (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID)) == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((pMplsOutApiInfo = (tMplsApiOutInfo *)
         MemAllocMemBlk (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                            (UINT1 *) pMplsInApiInfo);
        return OSIX_FAILURE;
    }
    if ((pBfdInParams = (tBfdReqParams *)
         MemAllocMemBlk (LSPP_FSLSPPBFDREQPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                            (UINT1 *) pMplsInApiInfo);
        MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                            (UINT1 *) pMplsOutApiInfo);
        return OSIX_FAILURE;
    }
    MEMSET (pMplsInApiInfo, 0, sizeof (tMplsApiInInfo));
    MEMSET (pMplsOutApiInfo, 0, sizeof (tMplsApiOutInfo));
    MEMSET (pBfdInParams, 0, sizeof (tBfdReqParams));
    MEMSET ((&BfdOutParams), 0, sizeof (tBfdRespParams));
    MEMSET ((&IpInfo), 0, sizeof (tIpConfigInfo));
    MEMSET ((&CfaIfInfo), 0, sizeof (tCfaIfInfo));
    MEMSET ((&FmFaultMsg), 0, sizeof (tFmFaultMsg));
    MEMSET (&VcmRegInfo, 0, sizeof (tVcmRegInfo));
    MEMSET (au1ContextName, 0, LSPP_MAX_CONTEXT_NAME_LEN);

#ifdef LSPP_TEST_WANTED
    if (gu4LsppUtStubsReqd == OSIX_TRUE)
    {
        MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                            (UINT1 *) pMplsInApiInfo);
        MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                            (UINT1 *) pMplsOutApiInfo);
        MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                            (UINT1 *) pBfdInParams);
        return (LsppUtPortHdlExtInteraction
                (pLsppExtInParams, pLsppExtOutParams));
    }
#endif

    switch (pLsppExtInParams->u4RequestType)
    {
        case LSPP_BFD_PROCESS_BTSTRAP_INFO:

            pBfdInParams->u4ContextId = pLsppExtInParams->u4ContextId;
            pBfdInParams->u4ReqType = BFD_PROCESS_BOOTSTRAP_INFO;

            pBfdInParams->unReqInfo.BfdLsppInfo.u4BfdDiscriminator =
                pLsppExtInParams->InBfdInfo.u4BfdDiscriminator;

            pBfdInParams->unReqInfo.BfdLsppInfo.u4SessionIndex =
                pLsppExtInParams->InBfdInfo.u4SessionIndex;

            pBfdInParams->unReqInfo.BfdLsppInfo.u4ErrorCode =
                (UINT4) pLsppExtInParams->InBfdInfo.eErrorCode;

            if (pLsppExtInParams->InBfdInfo.PathId.u1PathType ==
                LSPP_PATH_TYPE_MEP)
            {
                pBfdInParams->unReqInfo.BfdLsppInfo.PathId.ePathType =
                    BFD_PATH_TYPE_MEP;
            }
            else
            {
                pBfdInParams->unReqInfo.BfdLsppInfo.PathId.ePathType =
                    (tBfdPathType) pLsppExtInParams->InBfdInfo.PathId.
                    u1PathType;

            }

            if ((pLsppExtInParams->InBfdInfo.PathId.u1PathType ==
                 LSPP_PATH_TYPE_LDP_IPV4) ||
                (pLsppExtInParams->InBfdInfo.PathId.u1PathType ==
                 LSPP_PATH_TYPE_LDP_IPV6))
            {
                pBfdInParams->unReqInfo.BfdLsppInfo.PathId.
                    SessNonTeParams.u1AddrType =
                    (UINT1) pLsppExtInParams->InBfdInfo.PathId.
                    unPathId.LdpInfo.u4AddrType;

                pBfdInParams->unReqInfo.BfdLsppInfo.PathId.
                    SessNonTeParams.u1AddrLen =
                    (UINT1) pLsppExtInParams->InBfdInfo.PathId.
                    unPathId.LdpInfo.u4PrefixLength;

                MEMCPY (&(pBfdInParams->unReqInfo.BfdLsppInfo.PathId.
                          SessNonTeParams.PeerAddr),
                        &(pLsppExtInParams->InBfdInfo.PathId.unPathId.LdpInfo.
                          LdpPrefix), sizeof (tIpAddr));

            }
            else if ((pLsppExtInParams->InBfdInfo.PathId.u1PathType ==
                      LSPP_PATH_TYPE_RSVP_IPV4) ||
                     (pLsppExtInParams->InBfdInfo.PathId.u1PathType ==
                      LSPP_PATH_TYPE_RSVP_IPV6))
            {
                pBfdInParams->unReqInfo.BfdLsppInfo.PathId.SessTeParams.
                    u4TunnelId =
                    pLsppExtInParams->InBfdInfo.PathId.unPathId.TnlInfo.
                    u4SrcTnlId;

                pBfdInParams->unReqInfo.BfdLsppInfo.PathId.SessTeParams.
                    u4TunnelInst =
                    pLsppExtInParams->InBfdInfo.PathId.unPathId.TnlInfo.u4LspId;

                pBfdInParams->unReqInfo.BfdLsppInfo.PathId.SessTeParams.
                    u4AddrType =
                    pLsppExtInParams->InBfdInfo.PathId.unPathId.TnlInfo.
                    SrcNodeId.u4NodeType;

                if ((pLsppExtInParams->InBfdInfo.PathId.unPathId.TnlInfo.
                     SrcNodeId.u4NodeType == LSPP_MPLS_ADDR_TYPE_IPV4)
                    || (pLsppExtInParams->InBfdInfo.PathId.unPathId.TnlInfo.
                        SrcNodeId.u4NodeType == LSPP_MPLS_ADDR_TYPE_IPV6))
                {
                    MEMCPY (&(pBfdInParams->unReqInfo.BfdLsppInfo.PathId.
                              SessTeParams.SrcIpAddr),
                            &(pLsppExtInParams->InBfdInfo.PathId.unPathId.
                              TnlInfo.SrcNodeId.MplsRouterId),
                            sizeof (tIpAddr));

                    MEMCPY (&(pBfdInParams->unReqInfo.BfdLsppInfo.PathId.
                              SessTeParams.DstIpAddr),
                            &(pLsppExtInParams->InBfdInfo.PathId.unPathId.
                              TnlInfo.DstNodeId.MplsRouterId),
                            sizeof (tIpAddr));
                }
                else if (pLsppExtInParams->InBfdInfo.PathId.unPathId.TnlInfo.
                         SrcNodeId.u4NodeType ==
                         LSPP_MPLS_ADDR_TYPE_GLOBAL_NODE_ID)
                {
                    pBfdInParams->unReqInfo.BfdLsppInfo.PathId.SessTeParams.
                        SrcIpAddr.u4_addr[0] =
                        pLsppExtInParams->InBfdInfo.PathId.unPathId.TnlInfo.
                        SrcNodeId.MplsGlobalNodeId.u4GlobalId;

                    pBfdInParams->unReqInfo.BfdLsppInfo.PathId.SessTeParams.
                        SrcIpAddr.u4_addr[1] =
                        pLsppExtInParams->InBfdInfo.PathId.unPathId.TnlInfo.
                        SrcNodeId.MplsGlobalNodeId.u4NodeId;

                    pBfdInParams->unReqInfo.BfdLsppInfo.PathId.SessTeParams.
                        DstIpAddr.u4_addr[0] =
                        pLsppExtInParams->InBfdInfo.PathId.unPathId.TnlInfo.
                        DstNodeId.MplsGlobalNodeId.u4GlobalId;

                    pBfdInParams->unReqInfo.BfdLsppInfo.PathId.SessTeParams.
                        DstIpAddr.u4_addr[1] =
                        pLsppExtInParams->InBfdInfo.PathId.unPathId.TnlInfo.
                        DstNodeId.MplsGlobalNodeId.u4NodeId;
                }
            }

            else if ((pLsppExtInParams->InBfdInfo.PathId.u1PathType ==
                      LSPP_PATH_TYPE_PW))
            {
                MEMCPY (&
                        (pBfdInParams->unReqInfo.BfdLsppInfo.PathId.
                         SessPwParams.PeerAddr),
                        &(pLsppExtInParams->InBfdInfo.PathId.unPathId.PwInfo.
                          PeerAddr), sizeof (tIpAddr));

                pBfdInParams->unReqInfo.BfdLsppInfo.PathId.SessPwParams.u4VcId =
                    pLsppExtInParams->InBfdInfo.PathId.unPathId.PwInfo.u4PwVcId;
            }

            else if ((pLsppExtInParams->InBfdInfo.PathId.u1PathType ==
                      LSPP_PATH_TYPE_MEP))
            {
                pBfdInParams->unReqInfo.BfdLsppInfo.PathId.SessMeParams.
                    u4MegId =
                    pLsppExtInParams->InBfdInfo.PathId.unPathId.MepIndices.
                    u4MegIndex;

                pBfdInParams->unReqInfo.BfdLsppInfo.PathId.SessMeParams.u4MeId =
                    pLsppExtInParams->InBfdInfo.PathId.unPathId.
                    MepIndices.u4MeIndex;

                pBfdInParams->unReqInfo.BfdLsppInfo.PathId.SessMeParams.u4MpId =
                    pLsppExtInParams->InBfdInfo.PathId.unPathId.
                    MepIndices.u4MpIndex;
            }
            else
            {
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }

            pBfdInParams->unReqInfo.BfdLsppInfo.bIsEgress =
                pLsppExtInParams->InBfdInfo.bIsEgress;

            MEMCPY (&(pBfdInParams->unReqInfo.BfdLsppInfo.DestIpAddr),
                    &(pLsppExtInParams->InBfdInfo.DestIpAddr),
                    sizeof (tIpAddr));

            pBfdInParams->unReqInfo.BfdLsppInfo.pBuf =
                pLsppExtInParams->InBfdInfo.pBuf;

            pBfdInParams->unReqInfo.BfdLsppInfo.u4OffSet =
                pLsppExtInParams->InBfdInfo.u4OffSet;

            pBfdInParams->unReqInfo.BfdLsppInfo.u4SrcUdpPort =
                pLsppExtInParams->InBfdInfo.u4SrcUdpPort;

            pBfdInParams->unReqInfo.BfdLsppInfo.u4OutIfIndex =
                pLsppExtInParams->InBfdInfo.u4OutIfIndex;

            MEMCPY (pBfdInParams->unReqInfo.BfdLsppInfo.au1NextHopMac,
                    pLsppExtInParams->InBfdInfo.au1NextHopMac, MAC_ADDR_LEN);

            pBfdInParams->unReqInfo.BfdLsppInfo.u1ReplyPath =
                pLsppExtInParams->InBfdInfo.u1ReplyPath;

            pBfdInParams->unReqInfo.BfdLsppInfo.NextHopIpAddrType =
                pLsppExtInParams->InBfdInfo.NextHopIpAddrType;

            if (BfdApiHandleExtRequest (pLsppExtInParams->u4ContextId,
                                        pBfdInParams, &BfdOutParams) !=
                OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId, LSPP_BFD_API_FAILED,
                          "LsppPortHandleExtInteraction");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;

            }
            break;

        case LSPP_SYSLOG_REGISTER:
#ifdef SYSLOG_WANTED
            gLsppGlobals.i4SysLogId = SYS_LOG_REGISTER ((UINT1 *) LSPP_NAME,
                                                        SYSLOG_CRITICAL_LEVEL);
            if (gLsppGlobals.i4SysLogId <= 0)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId,
                          LSPP_SYSLOG_REGISTER_FAILED,
                          "LsppPortHandleExtInteraction");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }
#endif
            break;

        case LSPP_SYSLOG_DEREGISTER:

#ifdef SYSLOG_WANTED
            SYS_LOG_DEREGISTER (gLsppGlobals.i4SysLogId);
#endif
            break;

        case LSPP_SYSLOG_MSG:

#ifdef SYSLOG_WANTED
            SysLogMsg (pLsppExtInParams->LsppSyslogMsg.u4SyslogLevel,
                       LSPP_SYSLOG_ID, pLsppExtInParams->LsppSyslogMsg.
                       ac1SyslogMsg);
#endif
            break;

        case LSPP_VCM_REGISTER_WITH_VCM:

            VcmRegInfo.pIfMapChngAndCxtChng = LsppApiVcmCallback;
            VcmRegInfo.u1InfoMask |= (VCM_CONTEXT_CREATE | VCM_CONTEXT_DELETE);
            VcmRegInfo.u1ProtoId = LSPP_PROTOCOL_ID;

            if (VcmRegisterHLProtocol (&VcmRegInfo) != VCM_SUCCESS)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId,
                          LSPP_VCM_REGISTER_FAILED,
                          "LsppPortHandleExtInteraction");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }
            break;

            /* This request type is to get the context name from the given 
             * context ID by calling the VCM module API
             * */
        case LSPP_VCM_DEREGISTER_WITH_VCM:

            if (VcmDeRegisterHLProtocol (LSPP_PROTOCOL_ID) != VCM_SUCCESS)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId,
                          LSPP_VCM_DEREGISTER_FAILED,
                          "LsppPortHandleExtInteraction");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }
            break;

        case LSPP_VCM_GET_CXT_NAME_FRM_ID:

            if (VcmGetAliasName (pLsppExtInParams->u4ContextId,
                                 au1ContextName) != VCM_SUCCESS)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId,
                          LSPP_GET_CXT_NAME_FRM_ID_FAILED,
                          "LsppPortHandleExtInteraction");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }

            STRNCPY (pLsppExtOutParams->unOutParams.au1ContextName,
                     au1ContextName,
                     (sizeof (pLsppExtOutParams->unOutParams.au1ContextName) -
                      1));
            pLsppExtOutParams->unOutParams.
                au1ContextName[(sizeof
                                (pLsppExtOutParams->unOutParams.
                                 au1ContextName) - 1)] = '\0';

            break;

            /* This request type is to get the context Id from the Interface 
             * index by calling VCM Module API
             * */
        case LSPP_VCM_GET_CXT_ID_FRM_IF_INDEX:

            if (VcmGetContextIdFromCfaIfIndex (pLsppExtInParams->uInParams.
                                               u4IfIndex, &u4ContextId) !=
                VCM_SUCCESS)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId,
                          LSPP_GET_CXT_ID_FROM_IF_INDEX_FAILED,
                          "LsppPortHandleExtInteraction");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }
            pLsppExtOutParams->unOutParams.u4ContextId = u4ContextId;

            break;

            /* To get the Context Id from the given context name
             * If the context with the given name not exists, then failure 
             * will be returned
             * */
        case LSPP_VCM_GET_CXT_ID_FRM_NAME:

            if (VcmIsSwitchExist (pLsppExtInParams->uInParams.au1ContextName,
                                  &u4ContextId) != VCM_TRUE)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId, LSPP_CONTEXT_NOT_EXIST,
                          "LsppPortHandleExtInteraction",
                          pLsppExtInParams->uInParams.au1ContextName);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }
            pLsppExtOutParams->unOutParams.u4ContextId = u4ContextId;

            break;

            /* To get the Maximum transfer unit of the given 
             * L3 interface index */
        case LSPP_CFA_GET_MTU:

            if (CfaGetIfInfo (pLsppExtInParams->uInParams.u4IfIndex, &CfaIfInfo)
                != CFA_SUCCESS)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId,
                          LSPP_GET_INTF_MTU_FAILED,
                          "LsppPortHandleExtInteraction");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }
            pLsppExtOutParams->unOutParams.u4Mtu = CfaIfInfo.u4IfMtu;

            break;

            /* To get the L3 interface from the given MPLS or MPLS Tunnel 
             * interface
             * */
        case LSPP_CFA_GET_L3_IF:

            /* Get the type of the interface given */
            if (CfaGetIfInfo (pLsppExtInParams->uInParams.u4IfIndex, &CfaIfInfo)
                != CFA_SUCCESS)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId,
                          LSPP_GET_INTF_TYPE_FAILED,
                          "LsppPortHandleExtInteraction");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }

            /* If the given interface itself L3 interface, then give the same
             * as output
             * */
            if ((CfaIfInfo.u1IfType == CFA_L3IPVLAN) ||
                ((CfaIfInfo.u1IfType == CFA_ENET) &&
                 (CfaIfInfo.u1BridgedIface == CFA_DISABLED)))
            {
                pLsppExtOutParams->unOutParams.u4IfIndex =
                    pLsppExtInParams->uInParams.u4IfIndex;
                break;
            }

            /* If the interface type is MPLS Tunnel, get the underlying
             * MPLS interface from the MPLS Tunnel interface
             * */
            else if (CfaIfInfo.u1IfType == CFA_MPLS_TUNNEL)
            {
                if (CfaUtilGetMplsIfFromMplsTnlIf (pLsppExtInParams->uInParams.
                                                   u4IfIndex,
                                                   &u4MplsIfIndex, FALSE) !=
                    CFA_SUCCESS)
                {
                    LSPP_LOG (pLsppExtInParams->u4ContextId,
                              LSPP_GET_MPLS_INTF_FAILED,
                              "LsppPortHandleExtInteraction");
                    MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                        (UINT1 *) pMplsInApiInfo);
                    MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                        (UINT1 *) pMplsOutApiInfo);
                    MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                        (UINT1 *) pBfdInParams);
                    return OSIX_FAILURE;
                }
            }

            /* If the type of the given interface is MPLS, then assign the
             * MPLS interface index to a local varaiable
             * */
            else if (CfaIfInfo.u1IfType == CFA_MPLS)
            {
                u4MplsIfIndex = pLsppExtInParams->uInParams.u4IfIndex;
            }

            /* Get the L3 interface from the MPSL interface */
            if (CfaUtilGetL3IfFromMplsIf (u4MplsIfIndex, &u4L3IfIndex, FALSE) !=
                CFA_SUCCESS)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId,
                          LSPP_GET_L3_INTF_FAILED,
                          "LsppPortHandleExtInteraction");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }

            pLsppExtOutParams->unOutParams.u4IfIndex = u4L3IfIndex;

            break;

            /* To get the IP address corresponds to the given interface index */
        case LSPP_CFA_GET_IF_ADDR:

            if (CfaIpIfGetIfInfo (pLsppExtInParams->uInParams.u4IfIndex,
                                  &IpInfo) != CFA_SUCCESS)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId,
                          LSPP_GET_IP_ADDR_FRM_INTF_FAILED,
                          "LsppPortHandleExtInteraction");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }
            pLsppExtOutParams->OutLsppNodeId.u4NodeType =
                LSPP_MPLS_ADDR_TYPE_IPV4;
            pLsppExtOutParams->OutLsppNodeId.MplsRouterId.u4_addr[0] =
                IpInfo.u4Addr;

            break;

            /* To get the Interface index from the given Ip address */
        case LSPP_NETIP_GET_IF_INDEX:

            if (pLsppExtInParams->InLsppNodeId.u4NodeType ==
                LSPP_MPLS_ADDR_TYPE_IPV4)
            {
                if (NetIpv4GetIfIndexFromAddr (pLsppExtInParams->InLsppNodeId.
                                               MplsRouterId.u4_addr[0],
                                               &u4IfIndex) != NETIPV4_SUCCESS)
                {
                    LSPP_LOG (pLsppExtInParams->u4ContextId,
                              LSPP_GET_IF_INDEX_FROM_IP_FAILED,
                              "LsppPortHandleExtInteraction");
                    MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                        (UINT1 *) pMplsInApiInfo);
                    MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                        (UINT1 *) pMplsOutApiInfo);
                    MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                        (UINT1 *) pBfdInParams);
                    return OSIX_FAILURE;
                }
                pLsppExtOutParams->unOutParams.u4IfIndex = u4IfIndex;
            }
            else if (pLsppExtInParams->InLsppNodeId.u4NodeType ==
                     LSPP_MPLS_ADDR_TYPE_IPV6)
            {
                /* Currently it is not supported */
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }
            break;

            /* To get the Source IP address to be used from the given 
             * Destination IP address
             * */
        case LSPP_NETIP_GET_SRC_IP_FRM_DEST_IP:

            if (pLsppExtInParams->InLsppNodeId.u4NodeType ==
                LSPP_MPLS_ADDR_TYPE_IPV4)
            {
                if (NetIpv4GetSrcAddrToUseForDestInCxt (pLsppExtInParams->
                                                        u4ContextId,
                                                        pLsppExtInParams->
                                                        InLsppNodeId.
                                                        MplsRouterId.u4_addr[0],
                                                        &u4SrcAddr) !=
                    NETIPV4_SUCCESS)
                {
                    LSPP_LOG (pLsppExtInParams->u4ContextId,
                              LSPP_GET_SRC_IP_FRM_DEST_IP_FAILED,
                              "LsppPortHandleExtInteraction");
                    MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                        (UINT1 *) pMplsInApiInfo);
                    MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                        (UINT1 *) pMplsOutApiInfo);
                    MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                        (UINT1 *) pBfdInParams);
                    return OSIX_FAILURE;
                }
                pLsppExtOutParams->OutLsppNodeId.u4NodeType =
                    LSPP_MPLS_ADDR_TYPE_IPV4;

                pLsppExtOutParams->OutLsppNodeId.MplsRouterId.u4_addr[0] =
                    u4SrcAddr;
            }
            else if (pLsppExtInParams->InLsppNodeId.u4NodeType ==
                     LSPP_MPLS_ADDR_TYPE_IPV6)
            {
                /* Currently it is not supported */
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }
            break;

            /* To hand over the LSP Ping packet to the MPLS-RTR module for 
             * transmission
             * */
        case LSPP_MPLSRTR_TX_OAM_PKT:

            u4RequestType = MPLS_PACKET_HANDLE_FROM_APP;

            pMplsInApiInfo->u4ContextId = pLsppExtInParams->u4ContextId;
            pMplsInApiInfo->u4SubReqType = 0;
            pMplsInApiInfo->u4SrcModId = LSPPING_MODULE;

            MEMCPY (&(pMplsInApiInfo->InPktInfo),
                    &(pLsppExtInParams->InPktTxInfo), sizeof (tLsppPktTxInfo));

            if (MplsApiHandleExternalRequest (u4RequestType, pMplsInApiInfo,
                                              pMplsOutApiInfo) != OSIX_SUCCESS)
            {
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }

            break;

            /* To get the Meg and Me names from the given MEP indices */
        case LSPP_MPLS_OAM_GET_MEG_ME_NAME:

            u4RequestType = MPLS_OAM_GET_MEG_INFO;

            pMplsInApiInfo->u4ContextId = pLsppExtInParams->u4ContextId;
            pMplsInApiInfo->u4SubReqType = MPLS_GET_MEG_NAME_FROM_MEG_ID;
            pMplsInApiInfo->u4SrcModId = LSPPING_MODULE;

            MEMCPY (&(pMplsInApiInfo->InPathId.MegId), &(pLsppExtInParams->
                                                         InMegInfo.MegIndices),
                    sizeof (tLsppMegIndices));

            if (MplsApiHandleExternalRequest (u4RequestType, pMplsInApiInfo,
                                              pMplsOutApiInfo) != OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId, LSPP_MPLS_API_FAILED,
                          "LsppPortHandleExtInteraction", "Meg-Me name");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }

            MEMCPY (&(pLsppExtOutParams->OutMegDetail.MegInfo.MegName),
                    &(pMplsOutApiInfo->OutPathId.MegMeName),
                    sizeof (tLsppMegMeName));
            break;

            /* To get the MEG table OID from the given MEG and ME name */
        case LSPP_MPLS_OAM_GET_MEG_OID:

            u4RequestType = MPLS_GET_SERVICE_POINTER_OID;

            pMplsInApiInfo->u4ContextId = pLsppExtInParams->u4ContextId;
            pMplsInApiInfo->u4SubReqType = MPLS_GET_BASE_OID_FROM_MEG_NAME;
            pMplsInApiInfo->u4SrcModId = LSPPING_MODULE;

            MEMCPY (&(pMplsInApiInfo->InPathId.MegMeName),
                    &(pLsppExtInParams->InMegInfo.MegName),
                    sizeof (tLsppMegMeName));

            if (MplsApiHandleExternalRequest (u4RequestType, pMplsInApiInfo,
                                              pMplsOutApiInfo) != OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId, LSPP_MPLS_API_FAILED,
                          "LsppPortHandleExtInteraction", "Meg-OID");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }

            MEMCPY (&(pLsppExtOutParams->OutOid),
                    &(pMplsOutApiInfo->OutServiceOid),
                    sizeof (tLsppServiceOid));

            pLsppExtOutParams->OutOid.u2ServiceOidLen =
                pMplsOutApiInfo->OutServiceOid.u2ServiceOidLen;

            break;

            /* To get the all Meg related infromations from the given MEP 
             * indices 
             */
        case LSPP_MPLS_OAM_GET_MEG_INFO:
        case LSPP_MPLS_OAM_GET_MEG_INFO_FROM_ICC_MEG:

            u4RequestType = MPLS_OAM_GET_MEG_INFO;
            pMplsInApiInfo->u4ContextId = pLsppExtInParams->u4ContextId;
            pMplsInApiInfo->u4SubReqType = MPLS_GET_MEG_INFO_FROM_MEG_ID;
            pMplsInApiInfo->u4SrcModId = LSPPING_MODULE;

            if (pLsppExtInParams->u4RequestType == LSPP_MPLS_OAM_GET_MEG_INFO)
            {
                pMplsInApiInfo->u4SubReqType = MPLS_GET_MEG_INFO_FROM_MEG_ID;
                MEMCPY (&(pMplsInApiInfo->InPathId.MegId),
                        &(pLsppExtInParams->InMegInfo.MegIndices),
                        sizeof (tLsppMegIndices));
            }
            else if (pLsppExtInParams->u4RequestType ==
                     LSPP_MPLS_OAM_GET_MEG_INFO_FROM_ICC_MEG)
            {
                pMplsInApiInfo->u4SubReqType = MPLS_GET_MEG_FROM_SRC_ICC_MEP;
                MEMCPY (&(pMplsInApiInfo->InPathId.unPathId.MplsIccMep),
                        &(pLsppExtInParams->InIccMep), sizeof (tLsppIccMep));
            }

            if (MplsApiHandleExternalRequest (u4RequestType, pMplsInApiInfo,
                                              pMplsOutApiInfo) != OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId, LSPP_MPLS_API_FAILED,
                          "LsppPortHandleExtInteraction", "Meg Info ");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }

            LsppPortCopyMegInfo (pLsppExtOutParams, pMplsOutApiInfo);

            if (pMplsOutApiInfo->OutMegInfo.u1ServiceType ==
                OAM_SERVICE_TYPE_LSP)
            {
                /* Get the Source Global Id and Node Id from Local Src Num. */
                MEMSET (pMplsInApiInfo, 0, sizeof (tMplsApiInInfo));
                MEMSET (pMplsOutApiInfo, 0, sizeof (tMplsApiOutInfo));

                u4RequestType = MPLS_GET_NODE_ID;
                pMplsInApiInfo->u4ContextId = pLsppExtInParams->u4ContextId;
                pMplsInApiInfo->u4SubReqType = MPLS_GET_GLBNODEID_FROM_LOCALNUM;
                pMplsInApiInfo->u4SrcModId = LSPPING_MODULE;

                pMplsInApiInfo->InLocalNum =
                    pLsppExtOutParams->OutMegDetail.
                    unServiceInfo.ServiceTnlId.u4SrcLocalMapNum;

                if (MplsApiHandleExternalRequest (u4RequestType, pMplsInApiInfo,
                                                  pMplsOutApiInfo) !=
                    OSIX_SUCCESS)
                {
                    LSPP_LOG (pLsppExtInParams->u4ContextId,
                              LSPP_MPLS_API_FAILED,
                              "LsppPortHandleExtInteraction");
                    MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                        (UINT1 *) pMplsInApiInfo);
                    MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                        (UINT1 *) pMplsOutApiInfo);
                    MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                        (UINT1 *) pBfdInParams);
                    return OSIX_FAILURE;
                }

                pLsppExtOutParams->OutMegDetail.unServiceInfo.ServiceTnlId.
                    SrcNodeId.MplsGlobalNodeId.u4GlobalId =
                    pMplsOutApiInfo->OutNodeId.u4GlobalId;

                pLsppExtOutParams->OutMegDetail.unServiceInfo.ServiceTnlId.
                    SrcNodeId.MplsGlobalNodeId.u4NodeId =
                    pMplsOutApiInfo->OutNodeId.u4NodeId;

                pLsppExtOutParams->OutMegDetail.unServiceInfo.ServiceTnlId.
                    SrcNodeId.u4NodeType = LSPP_MPLS_ADDR_TYPE_GLOBAL_NODE_ID;

                /* Get the Destination Global Id and Node Id from 
                 * Local Dst Num. */
                MEMSET (pMplsInApiInfo, 0, sizeof (tMplsApiInInfo));
                MEMSET (pMplsOutApiInfo, 0, sizeof (tMplsApiOutInfo));

                u4RequestType = MPLS_GET_NODE_ID;
                pMplsInApiInfo->u4ContextId = pLsppExtInParams->u4ContextId;
                pMplsInApiInfo->u4SubReqType = MPLS_GET_GLBNODEID_FROM_LOCALNUM;
                pMplsInApiInfo->u4SrcModId = LSPPING_MODULE;

                pMplsInApiInfo->InLocalNum =
                    pLsppExtOutParams->OutMegDetail.
                    unServiceInfo.ServiceTnlId.u4DstLocalMapNum;

                if (MplsApiHandleExternalRequest (u4RequestType, pMplsInApiInfo,
                                                  pMplsOutApiInfo) !=
                    OSIX_SUCCESS)
                {
                    LSPP_LOG (pLsppExtInParams->u4ContextId,
                              LSPP_MPLS_API_FAILED,
                              "LsppPortHandleExtInteraction");
                    MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                        (UINT1 *) pMplsInApiInfo);
                    MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                        (UINT1 *) pMplsOutApiInfo);
                    MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                        (UINT1 *) pBfdInParams);
                    return OSIX_FAILURE;
                }

                pLsppExtOutParams->OutMegDetail.unServiceInfo.ServiceTnlId.
                    DstNodeId.MplsGlobalNodeId.u4GlobalId =
                    pMplsOutApiInfo->OutNodeId.u4GlobalId;

                pLsppExtOutParams->OutMegDetail.unServiceInfo.ServiceTnlId.
                    DstNodeId.MplsGlobalNodeId.u4NodeId =
                    pMplsOutApiInfo->OutNodeId.u4NodeId;

                pLsppExtOutParams->OutMegDetail.unServiceInfo.ServiceTnlId.
                    DstNodeId.u4NodeType = LSPP_MPLS_ADDR_TYPE_GLOBAL_NODE_ID;
            }

            break;

            /* TO get the reverse path info from the given IP address or 
             * the Global Id and Node Id
             * */
        case LSPP_MPLS_OAM_GET_REV_PATH_INFO:

            u4RequestType = MPLS_GET_REV_PATH_FROM_FWD_PATH;

            pMplsInApiInfo->u4ContextId = pLsppExtInParams->u4ContextId;
            pMplsInApiInfo->u4SubReqType = 0;
            pMplsInApiInfo->u4SrcModId = LSPPING_MODULE;

            MEMCPY (&(pMplsInApiInfo->InNodeId),
                    &(pLsppExtInParams->InLsppNodeId), sizeof (tLsppNodeId));

            if (MplsApiHandleExternalRequest (u4RequestType, pMplsInApiInfo,
                                              pMplsOutApiInfo) != OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId, LSPP_MPLS_API_FAILED,
                          "LsppPortHandleExtInteraction", "reverse-path info");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }

            if (pMplsOutApiInfo->u4PathType == LSPP_MPLS_PATH_TYPE_TNL)
            {
                LsppPortCopyTnlInfo (pLsppExtOutParams, pMplsOutApiInfo,
                                     u1CoRoutedLsp, u1PathDirection);
            }

            if (pMplsOutApiInfo->u4PathType == LSPP_MPLS_PATH_TYPE_LSP)
            {
                LsppPortCopyLspInfo (pLsppExtOutParams, pMplsOutApiInfo);
            }

            break;

            /* To get the router Id of the router from the MPLS-RTR module */
        case LSPP_MPLS_OAM_GET_ROUTER_ID:
        case LSPP_MPLS_OAM_GET_NODE_ID:

            u4RequestType = MPLS_GET_NODE_ID;

            pMplsInApiInfo->u4ContextId = pLsppExtInParams->u4ContextId;

            if (pLsppExtInParams->u4RequestType == LSPP_MPLS_OAM_GET_ROUTER_ID)
            {
                pMplsInApiInfo->u4SubReqType = MPLS_GET_ROUTER_ID;
            }
            else if (pLsppExtInParams->u4RequestType ==
                     LSPP_MPLS_OAM_GET_NODE_ID)
            {
                pMplsInApiInfo->u4SubReqType = MPLS_GET_GLOBAL_NODE_ID;
            }

            pMplsInApiInfo->u4SrcModId = LSPPING_MODULE;

            if (MplsApiHandleExternalRequest (u4RequestType, pMplsInApiInfo,
                                              pMplsOutApiInfo) != OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId, LSPP_MPLS_API_FAILED,
                          "LsppPortHandleExtInteraction", "Router-Id");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }

            if (pLsppExtInParams->u4RequestType == LSPP_MPLS_OAM_GET_ROUTER_ID)
            {
                pLsppExtOutParams->OutLsppNodeId.u4NodeType =
                    LSPP_MPLS_ADDR_TYPE_IPV4;

                pLsppExtOutParams->OutLsppNodeId.MplsRouterId.u4_addr[0] =
                    pMplsOutApiInfo->OutNodeId.u4NodeId;

            }
            else if (pLsppExtInParams->u4RequestType ==
                     LSPP_MPLS_OAM_GET_NODE_ID)
            {
                MEMCPY (&(pLsppExtOutParams->OutLsppNodeId.MplsGlobalNodeId),
                        &(pMplsOutApiInfo->OutNodeId),
                        sizeof (tLsppGlobalNodeId));
            }
            break;

            /* TO get the Path info from the Label in the received echo 
             * request
             */
        case LSPP_GET_PATH_FRM_INLBL_INFO:

            u4RequestType = MPLS_GET_PATH_FROM_INLBL_INFO;

            pMplsInApiInfo->u4ContextId = pLsppExtInParams->u4ContextId;
            pMplsInApiInfo->u4SubReqType = 0;
            pMplsInApiInfo->u4SrcModId = LSPPING_MODULE;

            MEMCPY (&(pMplsInApiInfo->InInLblInfo),
                    &(pLsppExtInParams->InLblInfo), sizeof (tLsppInLblInfo));

            if (MplsApiHandleExternalRequest (u4RequestType, pMplsInApiInfo,
                                              pMplsOutApiInfo) != OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId, LSPP_MPLS_API_FAILED,
                          "LsppPortHandleExtInteraction",
                          "Path Info from In-Label");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }

            if (pMplsOutApiInfo->u4PathType == LSPP_MPLS_PATH_TYPE_TNL)
            {
                if (pMplsOutApiInfo->OutTeTnlInfo.u1TnlMode ==
                    LSPP_COROUTED_LSP)
                {
                    u1CoRoutedLsp = OSIX_TRUE;
                }
                LsppPortCopyTnlInfo (pLsppExtOutParams, pMplsOutApiInfo,
                                     u1CoRoutedLsp, u1PathDirection);
            }

            if (pMplsOutApiInfo->u4PathType == LSPP_MPLS_PATH_TYPE_LSP)
            {
                LsppPortCopyLspInfo (pLsppExtOutParams, pMplsOutApiInfo);
            }

            if (pMplsOutApiInfo->u4PathType == LSPP_MPLS_PATH_TYPE_PW)
            {
                LsppPortCopyPwInfo (pLsppExtOutParams, pMplsOutApiInfo);
            }

            if (pMplsOutApiInfo->u4PathType == LSPP_MPLS_PATH_TYPE_MEG)
            {
                LsppPortCopyMegInfo (pLsppExtOutParams, pMplsOutApiInfo);
            }

            pLsppExtOutParams->u1PathType = (UINT1) pMplsOutApiInfo->u4PathType;

            break;

            /* To get the Base OID of the FTN table */
        case LSPP_GET_FTN_BASE_OID:

            u4RequestType = MPLS_GET_SERVICE_POINTER_OID;

            pMplsInApiInfo->u4ContextId = pLsppExtInParams->u4ContextId;
            pMplsInApiInfo->u4SubReqType = MPLS_GET_BASE_FTN_OID;
            pMplsInApiInfo->u4SrcModId = LSPPING_MODULE;

            if (MplsApiHandleExternalRequest (u4RequestType, pMplsInApiInfo,
                                              pMplsOutApiInfo) != OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId, LSPP_MPLS_API_FAILED,
                          "LsppPortHandleExtInteraction", "FTN Base OID");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }

            MEMCPY (&(pLsppExtOutParams->OutOid),
                    &(pMplsOutApiInfo->OutServiceOid),
                    sizeof (tLsppServiceOid));

            break;

            /* To get the Base OID of the Pseudowire table */
        case LSPP_GET_PW_BASE_OID:

            u4RequestType = MPLS_GET_SERVICE_POINTER_OID;

            pMplsInApiInfo->u4ContextId = pLsppExtInParams->u4ContextId;
            pMplsInApiInfo->u4SubReqType = MPLS_GET_BASE_PW_OID;
            pMplsInApiInfo->u4SrcModId = LSPPING_MODULE;

            if (MplsApiHandleExternalRequest (u4RequestType, pMplsInApiInfo,
                                              pMplsOutApiInfo) != OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId, LSPP_MPLS_API_FAILED,
                          "LsppPortHandleExtInteraction", "PW Base OID");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }

            MEMCPY (&(pLsppExtOutParams->OutOid),
                    &(pMplsOutApiInfo->OutServiceOid),
                    sizeof (tLsppServiceOid));
            break;

            /* To get the Base OID of the Tunnel table */
        case LSPP_GET_TNL_BASE_OID:

            u4RequestType = MPLS_GET_SERVICE_POINTER_OID;

            pMplsInApiInfo->u4ContextId = pLsppExtInParams->u4ContextId;
            pMplsInApiInfo->u4SubReqType = MPLS_GET_BASE_TNL_OID;
            pMplsInApiInfo->u4SrcModId = LSPPING_MODULE;

            if (MplsApiHandleExternalRequest (u4RequestType, pMplsInApiInfo,
                                              pMplsOutApiInfo) != OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId, LSPP_MPLS_API_FAILED,
                          "LsppPortHandleExtInteraction", "Tunnel Base OID");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }

            MEMCPY (&(pLsppExtOutParams->OutOid),
                    &(pMplsOutApiInfo->OutServiceOid),
                    sizeof (tLsppServiceOid));
            break;

            /* To get the Base OID of the MEG table */
        case LSPP_GET_MEG_BASE_OID:

            u4RequestType = MPLS_GET_SERVICE_POINTER_OID;

            pMplsInApiInfo->u4ContextId = pLsppExtInParams->u4ContextId;
            pMplsInApiInfo->u4SubReqType = MPLS_GET_BASE_MEG_OID;
            pMplsInApiInfo->u4SrcModId = LSPPING_MODULE;

            if (MplsApiHandleExternalRequest (u4RequestType, pMplsInApiInfo,
                                              pMplsOutApiInfo) != OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId, LSPP_MPLS_API_FAILED,
                          "LsppPortHandleExtInteraction", "MEG Base OID");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }

            MEMCPY (&(pLsppExtOutParams->OutOid),
                    &(pMplsOutApiInfo->OutServiceOid),
                    sizeof (tLsppServiceOid));
            break;

            /* To get the Base OID of the FTN table corresponds to the given 
             * LDP prefix
             * */
        case LSPP_MPLSDB_GET_FTN_OID:

            u4RequestType = MPLS_GET_SERVICE_POINTER_OID;

            pMplsInApiInfo->u4ContextId = pLsppExtInParams->u4ContextId;
            pMplsInApiInfo->u4SubReqType = MPLS_GET_BASE_OID_FROM_FEC;
            pMplsInApiInfo->u4SrcModId = LSPPING_MODULE;

            MEMCPY (&(pMplsInApiInfo->InPathId.LspId.PeerAddr),
                    &(pLsppExtInParams->InLdpInfo.LdpPrefix),
                    sizeof (pLsppExtInParams->InLdpInfo.LdpPrefix));

            pMplsInApiInfo->InPathId.LspId.u4AddrType =
                pLsppExtInParams->InLdpInfo.u4AddrType;

            pMplsInApiInfo->InPathId.LspId.u4AddrLen =
                pLsppExtInParams->InLdpInfo.u4PrefixLength;

            if (MplsApiHandleExternalRequest (u4RequestType, pMplsInApiInfo,
                                              pMplsOutApiInfo) != OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId, LSPP_MPLS_API_FAILED,
                          "LsppPortHandleExtInteraction",
                          "FTN table OID from FEC");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }

            MEMCPY (&(pLsppExtOutParams->OutOid),
                    &(pMplsOutApiInfo->OutServiceOid),
                    sizeof (tLsppServiceOid));

            pLsppExtOutParams->OutOid.u2ServiceOidLen =
                pMplsOutApiInfo->OutServiceOid.u2ServiceOidLen;
            break;

            /* To get the Tunnel table OID from the givrn tunnel indices */
        case LSPP_MPLSDB_GET_TUNNEL_OID:

            u4RequestType = MPLS_GET_SERVICE_POINTER_OID;

            pMplsInApiInfo->u4ContextId = pLsppExtInParams->u4ContextId;
            pMplsInApiInfo->u4SubReqType = MPLS_GET_BASE_OID_FROM_TNL_INDEX;
            pMplsInApiInfo->u4SrcModId = LSPPING_MODULE;

            pMplsInApiInfo->InPathId.TnlId.u4SrcTnlNum =
                pLsppExtInParams->InTnlInfo.u4SrcTnlId;

            pMplsInApiInfo->InPathId.TnlId.u4LspNum =
                pLsppExtInParams->InTnlInfo.u4LspId;

            if (MplsApiHandleExternalRequest (u4RequestType, pMplsInApiInfo,
                                              pMplsOutApiInfo) != OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId, LSPP_MPLS_API_FAILED,
                          "LsppPortHandleExtInteraction",
                          "Tunnel OID from Tunnel indices");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }

            MEMCPY (&(pLsppExtOutParams->OutOid),
                    &(pMplsOutApiInfo->OutServiceOid),
                    sizeof (tLsppServiceOid));

            pLsppExtOutParams->OutOid.u2ServiceOidLen =
                pMplsOutApiInfo->OutServiceOid.u2ServiceOidLen;
            break;

            /* To get the LDP informations from the FEC received in the 
             * echo request
             *  Currently it is not supported
             case LSPP_MPLSDB_GET_LDP_INFO_FRM_FEC:

             u4RequestType = MPLS_GET_LSP_INFO;

             pMplsInApiInfo->u4ContextId = pLsppExtInParams->u4ContextId;
             pMplsInApiInfo->u4SubReqType = MPLS_GET_LSP_INFO_FROM_FEC;
             pMplsInApiInfo->u4SrcModId = LSPPING_MODULE;

             MEMCPY (&(pMplsInApiInfo->InPathId.LspId.PeerAddr),
             &(pLsppExtInParams->InLdpInfo.LdpPrefix),
             sizeof (tIpAddr));

             pMplsInApiInfo->InPathId.LspId.u4AddrType = 
             pLsppExtInParams->InLdpInfo.u4AddrType;

             pMplsInApiInfo->InPathId.LspId.u4AddrLen = 
             pLsppExtInParams->InLdpInfo.u4PrefixLength;

             if (MplsApiHandleExternalRequest (u4RequestType, pMplsInApiInfo,
             pMplsOutApiInfo)
             != OSIX_SUCCESS )
             {
             LSPP_LOG (pLsppExtInParams->u4ContextId, LSPP_MPLS_API_FAILED, 
             "LsppPortHandleExtInteraction", 
             "LDP info from LDP FEC");
             MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID, (UINT1 *) pMplsInApiInfo);
             MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID, (UINT1 *) pMplsOutApiInfo);
             MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID, (UINT1 *) pBfdInParams);
             return OSIX_FAILURE;
             }

             LsppPortCopyLspInfo (pLsppExtOutParams, &pMplsOutApiInfo->;

             break; */

            /* To get the LDP Info from the XC index
             * This will be used when finding the underlying path of the PW
             * */
        case LSPP_MPLSDB_GET_LDP_INFO_FRM_XC_INDEX:

            u4RequestType = MPLS_GET_LSP_INFO;

            pMplsInApiInfo->u4ContextId = pLsppExtInParams->u4ContextId;
            pMplsInApiInfo->u4SubReqType = MPLS_GET_LSP_INFO_FROM_XC_INDEX;
            pMplsInApiInfo->u4SrcModId = LSPPING_MODULE;

            pMplsInApiInfo->InPathId.LspId.u4XCIndex =
                pLsppExtInParams->uInParams.u4XcIndex;

            if (MplsApiHandleExternalRequest (u4RequestType, pMplsInApiInfo,
                                              pMplsOutApiInfo) != OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId, LSPP_MPLS_API_FAILED,
                          "LsppPortHandleExtInteraction",
                          "LDP info from XC index");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }

            LsppPortCopyLspInfo (pLsppExtOutParams, pMplsOutApiInfo);

            break;

            /* To get the LDP informations from the given FTN table index */
        case LSPP_MPLSDB_GET_LDP_INFO_FRM_FTN_INDEX:

            u4RequestType = MPLS_GET_LSP_INFO;

            pMplsInApiInfo->u4ContextId = pLsppExtInParams->u4ContextId;
            pMplsInApiInfo->u4SubReqType = MPLS_GET_LSP_INFO_FROM_FTN_INDEX;
            pMplsInApiInfo->u4SrcModId = LSPPING_MODULE;

            pMplsInApiInfo->InPathId.LspId.u4FtnIndex =
                pLsppExtInParams->uInParams.u4FtnIndex;

            if (MplsApiHandleExternalRequest (u4RequestType, pMplsInApiInfo,
                                              pMplsOutApiInfo) != OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId, LSPP_MPLS_API_FAILED,
                          "LsppPortHandleExtInteraction",
                          "LDP info from FTN index");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }

            MEMCPY (&(pLsppExtOutParams->OutLdpInfo.LdpInfo.LdpPrefix),
                    &(pMplsOutApiInfo->OutPathId.LspId.PeerAddr),
                    sizeof (tIpAddr));

            pLsppExtOutParams->OutLdpInfo.LdpInfo.u4AddrType =
                pMplsOutApiInfo->OutPathId.LspId.u4AddrType;

            pLsppExtOutParams->OutLdpInfo.LdpInfo.u4PrefixLength =
                pMplsOutApiInfo->OutPathId.LspId.u4AddrLen;

            break;

            /* To get the Tunnel informations from the given Tunnel indices */
        case LSPP_MPLSDB_GET_TNL_INFO:

            u4RequestType = MPLS_GET_TUNNEL_INFO;

            pMplsInApiInfo->u4ContextId = pLsppExtInParams->u4ContextId;
            pMplsInApiInfo->u4SubReqType = 0;
            pMplsInApiInfo->u4SrcModId = LSPPING_MODULE;

            MEMCPY (&(pMplsInApiInfo->InPathId.TnlId.SrcNodeId),
                    &(pLsppExtInParams->InTnlInfo.SrcNodeId),
                    sizeof (tLsppNodeId));

            MEMCPY (&(pMplsInApiInfo->InPathId.TnlId.DstNodeId),
                    &(pLsppExtInParams->InTnlInfo.DstNodeId),
                    sizeof (tLsppNodeId));

            pMplsInApiInfo->InPathId.TnlId.u4SrcTnlNum =
                pLsppExtInParams->InTnlInfo.u4SrcTnlId;

            pMplsInApiInfo->InPathId.TnlId.u4LspNum =
                pLsppExtInParams->InTnlInfo.u4LspId;

            if (MplsApiHandleExternalRequest (u4RequestType, pMplsInApiInfo,
                                              pMplsOutApiInfo) != OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId, LSPP_MPLS_API_FAILED,
                          "LsppPortHandleExtInteraction",
                          "Tunnel info from FEC");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }

            if (pMplsOutApiInfo->OutTeTnlInfo.u1TnlMode == LSPP_COROUTED_LSP)
            {
                u1CoRoutedLsp = OSIX_TRUE;
            }

            if (((pMplsOutApiInfo->OutTeTnlInfo.u1TnlRole) ==
                 MPLS_TE_INTERMEDIATE) && (u1CoRoutedLsp == OSIX_TRUE))
            {
                /* If the node is an intermediate node for that LSP,
                 * then check whether the request came from Ingress node or
                 * Egreess node based on the received interface. If the
                 * packet is received from Ingress node copy Fwd out
                 * tunnel details to the outsegment Info. If the packet is
                 * received from Egress, copy reverse out XC in the outsegement
                 * Info */

                i4RetVal = LsppCoreGetL3If (pLsppExtInParams->u4ContextId,
                                            pMplsOutApiInfo->OutTeTnlInfo.
                                            XcApiInfo.RevInSegInfo.u4InIf,
                                            &u4IfIndex);

                if (i4RetVal == OSIX_FAILURE)
                {
                    LSPP_LOG (pLsppExtInParams->u4ContextId,
                              LSPP_GET_L3_IF_FAILED,
                              "LsppPortHandleExtInteraction");
                    MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                        (UINT1 *) pMplsInApiInfo);
                    MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                        (UINT1 *) pMplsOutApiInfo);
                    MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                        (UINT1 *) pBfdInParams);
                    return OSIX_FAILURE;
                }

                if (u4IfIndex == pLsppExtInParams->InTnlInfo.u4InIf)
                {
                    u1PathDirection = LSPP_MPLS_DIRECTION_REVERSE;
                }
            }

            LsppPortCopyTnlInfo (pLsppExtOutParams, pMplsOutApiInfo,
                                 u1CoRoutedLsp, u1PathDirection);
            break;

        case LSPP_MPLSDB_GET_REV_TNL_INFO:

            u4RequestType = MPLS_GET_TUNNEL_INFO;

            pMplsInApiInfo->u4ContextId = pLsppExtInParams->u4ContextId;
            pMplsInApiInfo->u4SubReqType = 0;
            pMplsInApiInfo->u4SrcModId = LSPPING_MODULE;

            MEMCPY (&(pMplsInApiInfo->InPathId.TnlId.SrcNodeId),
                    &(pLsppExtInParams->InTnlInfo.SrcNodeId),
                    sizeof (tLsppNodeId));

            pMplsInApiInfo->InPathId.TnlId.u4SrcTnlNum =
                pLsppExtInParams->InTnlInfo.u4SrcTnlId;

            pMplsInApiInfo->InPathId.TnlId.u4LspNum =
                pLsppExtInParams->InTnlInfo.u4LspId;

            MEMCPY (&(pMplsInApiInfo->InPathId.TnlId.DstNodeId),
                    &(pLsppExtInParams->InTnlInfo.DstNodeId),
                    sizeof (tLsppNodeId));

            pMplsInApiInfo->InPathId.TnlId.u4DstTnlNum =
                pLsppExtInParams->InTnlInfo.u4DstTnlId;

            if (MplsApiHandleExternalRequest (u4RequestType, pMplsInApiInfo,
                                              pMplsOutApiInfo) != OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId, LSPP_MPLS_API_FAILED,
                          "LsppPortHandleExtInteraction",
                          "reverse tunnel Info");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }

            if (pMplsOutApiInfo->OutTeTnlInfo.u1TnlMode == LSPP_COROUTED_LSP)
            {
                u1CoRoutedLsp = OSIX_TRUE;

                if (((pMplsOutApiInfo->OutTeTnlInfo.u1TnlRole) ==
                     MPLS_TE_INTERMEDIATE))
                {

                    /* If the node is an intermediate node for that LSP,
                     * then check whether the request came from Ingress node or
                     * Egreess node based on the received interface. If the 
                     * packet is received from Ingress node copy reverse out 
                     * tunnel details to the outsegment Info. If the packet is
                     * received from Egress, copy Fwd oud XC in the outsegement
                     * Info */

                    /* Filling OutGoing L3 Interface */
                    i4RetVal = LsppCoreGetL3If (pLsppExtInParams->u4ContextId,
                                                pMplsOutApiInfo->OutTeTnlInfo.
                                                XcApiInfo.FwdInSegInfo.u4InIf,
                                                &u4IfIndex);

                    if (i4RetVal == OSIX_FAILURE)
                    {
                        LSPP_LOG (pLsppExtInParams->u4ContextId,
                                  LSPP_GET_L3_IF_FAILED,
                                  "LsppPortHandleExtInteraction");
                        MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                            (UINT1 *) pMplsInApiInfo);
                        MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                            (UINT1 *) pMplsOutApiInfo);
                        MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                            (UINT1 *) pBfdInParams);
                        return OSIX_FAILURE;
                    }

                    if (u4IfIndex == pLsppExtInParams->InTnlInfo.u4InIf)
                    {
                        u1PathDirection = LSPP_MPLS_DIRECTION_REVERSE;
                    }
                }

                LsppPortCopyTnlInfo (pLsppExtOutParams, pMplsOutApiInfo,
                                     u1CoRoutedLsp, u1PathDirection);
            }

            /* If the tunnel is associated bi-directional, then the
             * Destination node ID and Destination tunnel number in the obtained
             * tunnel info will be given as input to Port function to get the
             * reverse path info
             * */

            else if ((pMplsOutApiInfo->OutTeTnlInfo.u1TnlMode ==
                      LSPP_ASSOCIATED_LSP) ||
                     (pMplsOutApiInfo->OutTeTnlInfo.u1TnlMode ==
                      LSPP_UNIDIRECTIONAL_LSP))
            {
                u4RequestType = MPLS_GET_TUNNEL_INFO;

                pMplsInApiInfo->u4ContextId = pLsppExtInParams->u4ContextId;
                pMplsInApiInfo->u4SubReqType = 0;
                pMplsInApiInfo->u4SrcModId = LSPPING_MODULE;

                MEMCPY (&(pMplsInApiInfo->InPathId.TnlId.SrcNodeId),
                        &(pMplsOutApiInfo->OutTeTnlInfo.TnlLspId.DstNodeId),
                        sizeof (tLsppNodeId));

                pMplsInApiInfo->InPathId.TnlId.DstNodeId.
                    MplsGlobalNodeId.u4GlobalId = 0;

                pMplsInApiInfo->InPathId.TnlId.DstNodeId.
                    MplsGlobalNodeId.u4NodeId = 0;

                pMplsInApiInfo->InPathId.TnlId.u4SrcTnlNum =
                    pMplsOutApiInfo->OutTeTnlInfo.TnlLspId.u4DstTnlNum;

                pMplsInApiInfo->InPathId.TnlId.u4LspNum =
                    pMplsOutApiInfo->OutTeTnlInfo.TnlLspId.u4DstLspNum;

                if (MplsApiHandleExternalRequest (u4RequestType, pMplsInApiInfo,
                                                  pMplsOutApiInfo)
                    != OSIX_SUCCESS)
                {
                    LSPP_LOG (pLsppExtInParams->u4ContextId,
                              LSPP_MPLS_API_FAILED,
                              "LsppPortHandleExtInteraction",
                              "reverse tunnel info (associated)");
                    MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                        (UINT1 *) pMplsInApiInfo);
                    MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                        (UINT1 *) pMplsOutApiInfo);
                    MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                        (UINT1 *) pBfdInParams);
                    return OSIX_FAILURE;
                }

                LsppPortCopyTnlInfo (pLsppExtOutParams, pMplsOutApiInfo,
                                     u1CoRoutedLsp, u1PathDirection);
            }
            break;

            /* To get the Pseudowire table OID from the given VC-Id and the
             * Target Address
             * */
        case LSPP_L2VPN_GET_PW_OID:

            u4RequestType = MPLS_GET_SERVICE_POINTER_OID;

            pMplsInApiInfo->u4ContextId = pLsppExtInParams->u4ContextId;
            pMplsInApiInfo->u4SubReqType = MPLS_GET_BASE_OID_FROM_VCID;
            pMplsInApiInfo->u4SrcModId = LSPPING_MODULE;

            MEMCPY (&(pMplsInApiInfo->InPathId.PwId.DstNodeId),
                    &(pLsppExtInParams->InPwFecInfo.DstNodeId),
                    sizeof (tLsppNodeId));

            pMplsInApiInfo->InPathId.PwId.u4VcId =
                pLsppExtInParams->InPwFecInfo.u4VcId;

            if (MplsApiHandleExternalRequest (u4RequestType, pMplsInApiInfo,
                                              pMplsOutApiInfo) != OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId, LSPP_MPLS_API_FAILED,
                          "LsppPortHandleExtInteraction", "PW OID from VC-Id");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }

            MEMCPY (&(pLsppExtOutParams->OutOid),
                    &(pMplsOutApiInfo->OutServiceOid),
                    sizeof (tLsppServiceOid));

            pLsppExtOutParams->OutOid.u2ServiceOidLen =
                pMplsOutApiInfo->OutServiceOid.u2ServiceOidLen;
            break;

            /* To get the Vc-Id and the target Address of the pseudowire from
             * the givem Pseudowire index
             * */
        case LSPP_L2VPN_GET_VCID_FRM_PW_INDEX:

            u4RequestType = MPLS_GET_PW_INFO;

            pMplsInApiInfo->u4ContextId = pLsppExtInParams->u4ContextId;
            pMplsInApiInfo->u4SubReqType = MPLS_GET_PW_INFO_FROM_PW_INDEX;
            pMplsInApiInfo->u4SrcModId = LSPPING_MODULE;

            pMplsInApiInfo->InPathId.PwId.u4PwIndex =
                pLsppExtInParams->InPwFecInfo.u4PwIndex;

            if (MplsApiHandleExternalRequest (u4RequestType, pMplsInApiInfo,
                                              pMplsOutApiInfo) != OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId, LSPP_MPLS_API_FAILED,
                          "LsppPortHandleExtInteraction",
                          "VC-Id from Pw index");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }

            pLsppExtOutParams->OutPwDetail.PwFecInfo.u4VcId =
                pMplsOutApiInfo->OutPwInfo.MplsPwPathId.u4VcId;

            MEMCPY (&(pLsppExtOutParams->OutPwDetail.PwFecInfo.DstNodeId),
                    &(pMplsOutApiInfo->OutPwInfo.MplsPwPathId.DstNodeId),
                    sizeof (tLsppNodeId));

            break;

            /* To get all the pseudowire informations from the givem Pseudowire 
             * index
             * */
        case LSPP_L2VPN_GET_PW_INFO_FRM_PW_INDEX:

            u4RequestType = MPLS_GET_PW_INFO;

            pMplsInApiInfo->u4ContextId = pLsppExtInParams->u4ContextId;
            pMplsInApiInfo->u4SubReqType = MPLS_GET_PW_INFO_FROM_PW_INDEX;
            pMplsInApiInfo->u4SrcModId = LSPPING_MODULE;

            pMplsInApiInfo->InPathId.PwId.u4PwIndex =
                pLsppExtInParams->InPwFecInfo.u4PwIndex;

            if (MplsApiHandleExternalRequest (u4RequestType, pMplsInApiInfo,
                                              pMplsOutApiInfo) != OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId, LSPP_MPLS_API_FAILED,
                          "LsppPortHandleExtInteraction",
                          "Pw info from Pw index");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }

            LsppPortCopyPwInfo (pLsppExtOutParams, pMplsOutApiInfo);

            break;

            /* To get the pseudowire infromations from the givem Vc-Id and the 
             * target address of the Pseudowire
             * */
        case LSPP_L2VPN_GET_PW_INFO_FRM_VC_ID:

            u4RequestType = MPLS_GET_PW_INFO;
            pMplsInApiInfo->u4ContextId = pLsppExtInParams->u4ContextId;
            pMplsInApiInfo->u4SubReqType = MPLS_GET_PW_INFO_FROM_VCID;
            pMplsInApiInfo->u4SrcModId = LSPPING_MODULE;

            MEMCPY (&(pMplsInApiInfo->InPathId.PwId.DstNodeId),
                    &(pLsppExtInParams->InPwFecInfo.DstNodeId),
                    sizeof (tLsppNodeId));

            pMplsInApiInfo->InPathId.PwId.u4VcId =
                pLsppExtInParams->InPwFecInfo.u4VcId;

            if (MplsApiHandleExternalRequest (u4RequestType, pMplsInApiInfo,
                                              pMplsOutApiInfo) != OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId, LSPP_MPLS_API_FAILED,
                          "LsppPortHandleExtInteraction", "PW info from VC-Id");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }

            LsppPortCopyPwInfo (pLsppExtOutParams, pMplsOutApiInfo);
            break;

            /* To get the pseudowire Informations from the PWID FEC (FEC128) 
             * received in the echo request
             * */
        case LSPP_L2VPN_GET_PW_INFO_FRM_PWID_FEC:

            u4RequestType = MPLS_GET_PW_INFO;

            pMplsInApiInfo->u4ContextId = pLsppExtInParams->u4ContextId;
            pMplsInApiInfo->u4SubReqType = MPLS_GET_PW_INFO_FROM_PWIDFEC;
            pMplsInApiInfo->u4SrcModId = LSPPING_MODULE;

            pMplsInApiInfo->InPathId.PwId.u4VcId =
                pLsppExtInParams->InPwFecInfo.u4VcId;

            pMplsInApiInfo->InPathId.PwId.u2PwVcType =
                pLsppExtInParams->InPwFecInfo.u2PwVcType;

            pMplsInApiInfo->InPathId.PwId.u1PwVcOwner =
                pLsppExtInParams->InPwFecInfo.u1PwVcOwner;

            MEMCPY (&(pMplsInApiInfo->InPathId.PwId.SrcNodeId),
                    &(pLsppExtInParams->InPwFecInfo.SrcNodeId),
                    sizeof (tLsppNodeId));

            MEMCPY (&(pMplsInApiInfo->InPathId.PwId.DstNodeId),
                    &(pLsppExtInParams->InPwFecInfo.DstNodeId),
                    sizeof (tLsppNodeId));

            if (MplsApiHandleExternalRequest (u4RequestType, pMplsInApiInfo,
                                              pMplsOutApiInfo) != OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId, LSPP_MPLS_API_FAILED,
                          "LsppPortHandleExtInteraction",
                          "PW info from PW-Id FEC");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }

            LsppPortCopyPwInfo (pLsppExtOutParams, pMplsOutApiInfo);
            break;

            /* To get the pseudowire Informations from the GEN FEC Type 1(FEC129) 
             * received in the echo request
             * */
        case LSPP_L2VPN_GET_PW_INFO_FRM_GENFEC_TYPE1:

            u4RequestType = MPLS_GET_PW_INFO;

            pMplsInApiInfo->u4ContextId = pLsppExtInParams->u4ContextId;
            pMplsInApiInfo->u4SubReqType = MPLS_GET_PW_INFO_FROM_GENFEC;
            pMplsInApiInfo->u4SrcModId = LSPPING_MODULE;

            MEMCPY (&(pMplsInApiInfo->InPathId.PwId),
                    &(pLsppExtInParams->InPwFecInfo), sizeof (tLsppPwFecInfo));

            if (MplsApiHandleExternalRequest (u4RequestType, pMplsInApiInfo,
                                              pMplsOutApiInfo) != OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId, LSPP_MPLS_API_FAILED,
                          "LsppPortHandleExtInteraction",
                          "PW info from GEN-FEC Type 1");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }

            LsppPortCopyPwInfo (pLsppExtOutParams, pMplsOutApiInfo);
            break;

            /* To get the pseudowire Informations from the GEN FEC Type 2
             * (Static LSP FEC) received in the echo request
             * */
        case LSPP_L2VPN_GET_PW_INFO_FRM_GENFEC_TYPE2:

            u4RequestType = MPLS_GET_PW_INFO;

            pMplsInApiInfo->u4ContextId = pLsppExtInParams->u4ContextId;
            pMplsInApiInfo->u4SubReqType = MPLS_GET_PW_INFO_FROM_GENTYPE2;
            pMplsInApiInfo->u4SrcModId = LSPPING_MODULE;

            MEMCPY (&(pMplsInApiInfo->InPathId.PwId),
                    &(pLsppExtInParams->InPwFecInfo), sizeof (tLsppPwFecInfo));

            if (MplsApiHandleExternalRequest (u4RequestType, pMplsInApiInfo,
                                              pMplsOutApiInfo) != OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId, LSPP_MPLS_API_FAILED,
                          "LsppPortHandleExtInteraction",
                          "PW info from GEN-FEC Type 2");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }

            LsppPortCopyPwInfo (pLsppExtOutParams, pMplsOutApiInfo);
            break;

            /* To get the CC and Cv capability of the pseudowire from the 
             * pseudowire index
             * */
        case LSPP_L2VPN_GET_CCCV_CAPABILITY:

            u4RequestType = MPLS_GET_PW_INFO;

            pMplsInApiInfo->u4ContextId = pLsppExtInParams->u4ContextId;
            pMplsInApiInfo->u4SubReqType = MPLS_GET_PW_INFO_FROM_PW_INDEX;
            pMplsInApiInfo->u4SrcModId = LSPPING_MODULE;

            pMplsInApiInfo->InPathId.PwId.u4PwIndex =
                pLsppExtInParams->InPwFecInfo.u4PwIndex;

            if (MplsApiHandleExternalRequest (u4RequestType, pMplsInApiInfo,
                                              pMplsOutApiInfo) != OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId, LSPP_MPLS_API_FAILED,
                          "LsppPortHandleExtInteraction",
                          "CC and CV capability");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }

            pLsppExtOutParams->OutPwDetail.u1CcSelected =
                pMplsOutApiInfo->OutPwInfo.u1CcSelected;

            pLsppExtOutParams->OutPwDetail.u1CvSelected =
                pMplsOutApiInfo->OutPwInfo.u1CvSelected;

            break;

        case LSPP_FM_SEND_TRAP:

            MEMCPY (&FmFaultMsg, &(pLsppExtInParams->uInParams.FmFaultMsg),
                    sizeof (tFmFaultMsg));

            if (FmApiNotifyFaults (&FmFaultMsg) == OSIX_FAILURE)
            {
                LSPP_LOG (pLsppExtInParams->u4ContextId,
                          LSPP_FM_TRAP_SEND_FAILED,
                          "LsppPortHandleExtInteraction");
                MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsInApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pMplsOutApiInfo);
                MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                    (UINT1 *) pBfdInParams);
                return OSIX_FAILURE;
            }
            break;

        default:
            LSPP_LOG (pLsppExtInParams->u4ContextId,
                      LSPP_UNKNOWN_REQ_TYPE_IN_PORT_FUNC,
                      "LsppPortHandleExtInteraction");
            MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                                (UINT1 *) pMplsInApiInfo);
            MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                                (UINT1 *) pMplsOutApiInfo);
            MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID,
                                (UINT1 *) pBfdInParams);
            return OSIX_FAILURE;
    }
    MemReleaseMemBlock (LSPP_FSLSPPMPLSINPUTPARAMS_POOLID,
                        (UINT1 *) pMplsInApiInfo);
    MemReleaseMemBlock (LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID,
                        (UINT1 *) pMplsOutApiInfo);
    MemReleaseMemBlock (LSPP_FSLSPPBFDREQPARAMS_POOLID, (UINT1 *) pBfdInParams);
    return OSIX_SUCCESS;
}

 /******************************************************************************
 * Function   : LsppPortCopyTnlInfo 
 *
 * Description: This function is to Copy the Tunnel info in the MPLS 
 *              API output structure to the Output structure of LSP Ping Port 
 *              function.
 *
 * Input      : pMplsOutApiInfo
 *
 * Output     : pLsppExtOutParams
 * 
 * Returns    : OSIX_FAILURE/OSIX_SUCCESS
 *****************************************************************************/
VOID
LsppPortCopyTnlInfo (tLsppExtOutParams * pLsppExtOutParams,
                     tMplsApiOutInfo * pMplsOutApiInfo, UINT1 u1CoRoutedLsp,
                     UINT1 u1PathDirection)
{
    pLsppExtOutParams->u1PathType = LSPP_MPLS_PATH_TYPE_TNL;

    pLsppExtOutParams->OutTnlInfo.u4TnlType =
        pMplsOutApiInfo->OutTeTnlInfo.u4TnlType;

    pLsppExtOutParams->OutTnlInfo.u1OperStatus =
        pMplsOutApiInfo->OutTeTnlInfo.u1OperStatus;

    pLsppExtOutParams->OutTnlInfo.u1TnlOwner =
        pMplsOutApiInfo->OutTeTnlInfo.u1TnlOwner;

    pLsppExtOutParams->OutTnlInfo.u1TnlMode =
        pMplsOutApiInfo->OutTeTnlInfo.u1TnlMode;

    pLsppExtOutParams->OutTnlInfo.u1TnlRole =
        pMplsOutApiInfo->OutTeTnlInfo.u1TnlRole;

    MEMCPY (&(pLsppExtOutParams->OutTnlInfo.TnlInfo),
            &(pMplsOutApiInfo->OutTeTnlInfo.TnlLspId),
            sizeof (tLsppTnlLspInfo));

    MEMCPY (&(pLsppExtOutParams->OutTnlInfo.ExtendedTnlId),
            &(pMplsOutApiInfo->OutTeTnlInfo.ExtendedTnlId), sizeof (tIpAddr));

    MEMCPY (&(pLsppExtOutParams->OutTnlInfo.TnlInfo),
            &(pMplsOutApiInfo->OutTeTnlInfo.TnlLspId),
            sizeof (tLsppTnlLspInfo));

    MEMCPY (&(pLsppExtOutParams->OutTnlInfo.au1NextHopMac),
            &(pMplsOutApiInfo->OutTeTnlInfo.au1NextHopMac), MAC_ADDR_LEN);

    MEMCPY (&(pLsppExtOutParams->OutTnlInfo.MegIndices),
            &(pMplsOutApiInfo->OutTeTnlInfo.MplsMegId),
            sizeof (tLsppMegIndices));

    if ((u1CoRoutedLsp == OSIX_TRUE) &&
        (pMplsOutApiInfo->OutTeTnlInfo.u1TnlRole == MPLS_TE_INGRESS))
    {
        MEMCPY (&(pLsppExtOutParams->OutTnlInfo.InSegInfo),
                &(pMplsOutApiInfo->OutTeTnlInfo.XcApiInfo.RevInSegInfo),
                sizeof (tLsppInSegInfo));
    }
    else if ((u1CoRoutedLsp == OSIX_TRUE) &&
             (pMplsOutApiInfo->OutTeTnlInfo.u1TnlRole ==
              MPLS_TE_INTERMEDIATE) &&
             (u1PathDirection == LSPP_MPLS_DIRECTION_REVERSE))
    {
        MEMCPY (&(pLsppExtOutParams->OutTnlInfo.InSegInfo),
                &(pMplsOutApiInfo->OutTeTnlInfo.XcApiInfo.RevInSegInfo),
                sizeof (tLsppInSegInfo));
    }
    else
    {
        MEMCPY (&(pLsppExtOutParams->OutTnlInfo.InSegInfo),
                &(pMplsOutApiInfo->OutTeTnlInfo.XcApiInfo.FwdInSegInfo),
                sizeof (tLsppInSegInfo));
    }

    /* If the Tunnel is Co-routed bidirectional, then the reverse path
     * information will be present in the Reverse OUT segment info of the
     * Tunnel informations
     * */

    if ((u1CoRoutedLsp == OSIX_TRUE) &&
        (pMplsOutApiInfo->OutTeTnlInfo.u1TnlRole == MPLS_TE_EGRESS))
    {
        MEMCPY (&(pLsppExtOutParams->OutTnlInfo.OutSegInfo),
                &(pMplsOutApiInfo->OutTeTnlInfo.XcApiInfo.RevOutSegInfo),
                sizeof (tLsppOutSegInfo));
    }
    else if ((u1CoRoutedLsp == OSIX_TRUE) &&
             (pMplsOutApiInfo->OutTeTnlInfo.u1TnlRole ==
              MPLS_TE_INTERMEDIATE) &&
             (u1PathDirection == LSPP_MPLS_DIRECTION_REVERSE))
    {
        MEMCPY (&(pLsppExtOutParams->OutTnlInfo.OutSegInfo),
                &(pMplsOutApiInfo->OutTeTnlInfo.XcApiInfo.RevOutSegInfo),
                sizeof (tLsppOutSegInfo));
    }
    else
    {
        MEMCPY (&(pLsppExtOutParams->OutTnlInfo.OutSegInfo),
                &(pMplsOutApiInfo->OutTeTnlInfo.XcApiInfo.FwdOutSegInfo),
                sizeof (tLsppOutSegInfo));
    }

    pLsppExtOutParams->OutTnlInfo.u4TnlPrimaryInstance =
        pMplsOutApiInfo->OutTeTnlInfo.u4TnlPrimaryInstance;

    pLsppExtOutParams->OutTnlInfo.u1TnlSgnlPrtcl
        = pMplsOutApiInfo->OutTeTnlInfo.u1TnlSgnlPrtcl;

    LsppPortConvertProtocolVal (pLsppExtOutParams,
                                pMplsOutApiInfo->
                                OutTeTnlInfo.u1TnlSgnlPrtcl,
                                pMplsOutApiInfo->OutTeTnlInfo.u4TnlType);
}

 /******************************************************************************
 * Function   : LsppPortCopyLspInfo 
 *
 * Description: This function is to Copy the LSP info in the MPLS 
 *              API output structure to the Output structure of LSP Ping Port 
 *              function.
 *
 * Input      : pMplsOutApiInfo
 *
 * Output     : pLsppExtOutParams
 * 
 * Returns    : OSIX_FAILURE/OSIX_SUCCESS
 *****************************************************************************/
VOID
LsppPortCopyLspInfo (tLsppExtOutParams * pLsppExtOutParams,
                     tMplsApiOutInfo * pMplsOutApiInfo)
{

    pLsppExtOutParams->u1PathType = LSPP_MPLS_PATH_TYPE_LSP;

    pLsppExtOutParams->OutLdpInfo.LdpInfo.u4AddrType =
        pMplsOutApiInfo->OutNonTeTnlInfo.MplsLspId.u4AddrType;

    pLsppExtOutParams->OutLdpInfo.LdpInfo.u4PrefixLength =
        pMplsOutApiInfo->OutNonTeTnlInfo.MplsLspId.u4AddrLen;

    MEMCPY (&(pLsppExtOutParams->OutLdpInfo.LdpInfo.LdpPrefix),
            &(pMplsOutApiInfo->OutNonTeTnlInfo.MplsLspId.PeerAddr),
            sizeof (tIpAddr));

    MEMCPY (&(pLsppExtOutParams->OutLdpInfo.OutSegInfo),
            &(pMplsOutApiInfo->OutNonTeTnlInfo.XcApiInfo.FwdOutSegInfo),
            sizeof (tLsppOutSegInfo));

    MEMCPY (&(pLsppExtOutParams->OutLdpInfo.InSegInfo),
            &(pMplsOutApiInfo->OutNonTeTnlInfo.XcApiInfo.FwdInSegInfo),
            sizeof (tLsppInSegInfo));

    LsppPortConvertProtocolVal (pLsppExtOutParams, 0, 0);
}

 /******************************************************************************
 * Function   : LsppPortCopyPwInfo 
 *
 * Description: This function is to Copy the Pseudowire info in the MPLS 
 *              API output structure to the Output structure of LSP Ping Port 
 *              function.
 *
 * Input      : pMplsOutApiInfo
 *
 * Output     : pLsppExtOutParams
 * 
 * Returns    : OSIX_FAILURE/OSIX_SUCCESS
 *****************************************************************************/

VOID
LsppPortCopyPwInfo (tLsppExtOutParams * pLsppExtOutParams,
                    tMplsApiOutInfo * pMplsOutApiInfo)
{

    MEMCPY (&(pLsppExtOutParams->OutPwDetail.PwFecInfo),
            &(pMplsOutApiInfo->OutPwInfo.MplsPwPathId),
            sizeof (tLsppPwFecInfo));

    MEMCPY (&(pLsppExtOutParams->OutPwDetail.unOutTnlInfo.TnlIndices),
            &(pMplsOutApiInfo->OutPwInfo.TeTnlId), sizeof (tLsppTnlLspInfo));

    MEMCPY (&(pLsppExtOutParams->OutPwDetail.au1NextHopMac),
            &(pMplsOutApiInfo->OutPwInfo.au1NextHopMac), MAC_ADDR_LEN);

    MEMCPY (&(pLsppExtOutParams->OutPwDetail.MegIndices),
            &(pMplsOutApiInfo->OutPwInfo.MplsMegId), sizeof (tLsppMegIndices));

    pLsppExtOutParams->OutPwDetail.unOutTnlInfo.u4NonTeXcIndex =
        pMplsOutApiInfo->OutPwInfo.NonTeTnlId;

    pLsppExtOutParams->OutPwDetail.unOutTnlInfo.u4OutIfIndex =
        pMplsOutApiInfo->OutPwInfo.PwOnlyIfIndex;

    pLsppExtOutParams->OutPwDetail.u4OutVcLabel =
        pMplsOutApiInfo->OutPwInfo.u4OutVcLabel;

    pLsppExtOutParams->OutPwDetail.u4InVcLabel =
        pMplsOutApiInfo->OutPwInfo.u4InVcLabel;

    /*  pLsppExtOutParams->OutPwDetail.u1InVcLabelAction = 
       pMplsOutApiInfo->OutPwInfo.u1InVcLabelAction; */

    pLsppExtOutParams->OutPwDetail.u1CcSelected =
        pMplsOutApiInfo->OutPwInfo.u1CcSelected;

    pLsppExtOutParams->OutPwDetail.u1CvSelected =
        pMplsOutApiInfo->OutPwInfo.u1CvSelected;

    pLsppExtOutParams->OutPwDetail.u1MplsType =
        pMplsOutApiInfo->OutPwInfo.u1MplsType;

    pLsppExtOutParams->OutPwDetail.u1Ttl = pMplsOutApiInfo->OutPwInfo.u1Ttl;

    pLsppExtOutParams->OutPwDetail.u1PwType =
        pMplsOutApiInfo->OutPwInfo.u1PwType;

    pLsppExtOutParams->OutPwDetail.u1IpVersion =
        pMplsOutApiInfo->OutPwInfo.u1IpVersion;

    pLsppExtOutParams->OutPwDetail.i1OperStatus =
        pMplsOutApiInfo->OutPwInfo.i1OperStatus;

}

 /******************************************************************************
 * Function   : LsppPortCopyMegInfo 
 *
 * Description: This function is to Copy the Meg info in the MPLS API output
 *              structure to the Output structure of LSP Ping Port function.
 *
 * Input      : pMplsOutApiInfo
 *
 * Output     : pLsppExtOutParams
 * 
 * Returns    : OSIX_FAILURE/OSIX_SUCCESS
 *****************************************************************************/

VOID
LsppPortCopyMegInfo (tLsppExtOutParams * pLsppExtOutParams,
                     tMplsApiOutInfo * pMplsOutApiInfo)
{

    MEMCPY (&(pLsppExtOutParams->OutMegDetail.MegInfo.MegIndices),
            &(pMplsOutApiInfo->OutMegInfo.MplsMegId), sizeof (tLsppMegIndices));

    MEMCPY (&(pLsppExtOutParams->OutMegDetail.MegInfo.MegName),
            &(pMplsOutApiInfo->OutMegInfo.MplsMegMeName),
            sizeof (tLsppMegMeName));

    MEMCPY (pLsppExtOutParams->OutMegDetail.au1Icc,
            pMplsOutApiInfo->OutMegInfo.au1Icc, LSPP_MPLS_ICC_LENGTH);

    MEMCPY (pLsppExtOutParams->OutMegDetail.au1Umc,
            pMplsOutApiInfo->OutMegInfo.au1Umc, LSPP_MPLS_UMC_LENGTH);

    MEMCPY (&(pLsppExtOutParams->OutMegDetail.MplsNodeId),
            &(pMplsOutApiInfo->OutMegInfo.MplsNodeId),
            sizeof (tLsppGlobalNodeId));

    MEMCPY (&(pLsppExtOutParams->OutMegDetail.unServiceInfo.ServiceTnlId),
            &(pMplsOutApiInfo->OutMegInfo.MegMplsTnlId),
            sizeof (tLsppTnlLspInfo));

    pLsppExtOutParams->OutMegDetail.unServiceInfo.ServicePwId.u4PwIndex =
        pMplsOutApiInfo->OutMegInfo.MegPwId;
    pLsppExtOutParams->OutMegDetail.u1OperatorType =
        (UINT1) pMplsOutApiInfo->OutMegInfo.u4OperatorType;

    pLsppExtOutParams->OutMegDetail.u4MpIfIndex =
        pMplsOutApiInfo->OutMegInfo.u4MpIfIndex;

    pLsppExtOutParams->OutMegDetail.u2IccSrcMepIndex =
        (UINT2) pMplsOutApiInfo->OutMegInfo.u4IccSrcMepIndex;

    pLsppExtOutParams->OutMegDetail.u2IccSinkMepIndex =
        (UINT2) pMplsOutApiInfo->OutMegInfo.u4IccSinkMepIndex;

    pLsppExtOutParams->OutMegDetail.u1MpType =
        (UINT1) pMplsOutApiInfo->OutMegInfo.u4MpType;

    pLsppExtOutParams->OutMegDetail.u1MepDirection =
        (UINT1) pMplsOutApiInfo->OutMegInfo.u4MepDirection;

    pLsppExtOutParams->OutMegDetail.u4OnDemandTCValue =
        pMplsOutApiInfo->OutMegInfo.u4OnDemandTCValue;

    pLsppExtOutParams->OutMegDetail.u1MeState =
        (UINT1) pMplsOutApiInfo->OutMegInfo.u4MeOperStatus;

    pLsppExtOutParams->OutMegDetail.u1ServiceType =
        pMplsOutApiInfo->OutMegInfo.u1ServiceType;

    pLsppExtOutParams->OutMegDetail.u1ServiceLocation =
        pMplsOutApiInfo->OutMegInfo.u1ServiceLocation;

}

/******************************************************************************
 * Function   : LsppPortGetMegMeName 
 *
 * Description: This function is to get the corresponding MEG name and ME name 
 *              from the given MEG, ME, MP indices by calling the LSP Ping 
 *              Port Function.
 *
 * Input      : ContextId
 *              MegIndices
 *
 * Output     : MegMeName
 *
 * Returns    : OSIX_FAILURE/OSIX_SUCCESS
 *
 *****************************************************************************/

INT4
LsppPortGetMegMeName (UINT4 u4ContextId, tLsppMegIndices * pMegIndices,
                      tLsppMegMeName * pMegMeName)
{
    tLsppExtInParams   *pLsppExtInParams = NULL;
    tLsppExtOutParams  *pLsppExtOutParams = NULL;

    if ((pLsppExtInParams = (tLsppExtInParams *)
         MemAllocMemBlk (LSPP_FSLSPPINPUTPARAMS_POOLID)) == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((pLsppExtOutParams = (tLsppExtOutParams *)
         MemAllocMemBlk (LSPP_FSLSPPOUTPUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        return OSIX_FAILURE;
    }

    MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
    MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));

    pLsppExtInParams->u4ContextId = u4ContextId;
    pLsppExtInParams->u4RequestType = LSPP_MPLS_OAM_GET_MEG_ME_NAME;

    MEMCPY (&(pLsppExtInParams->InMegInfo.MegIndices),
            pMegIndices, sizeof (tLsppMegIndices));

    if (LsppPortHandleExtInteraction (pLsppExtInParams, pLsppExtOutParams) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);
        return OSIX_FAILURE;
    }

    MEMCPY (pMegMeName, &(pLsppExtOutParams->OutMegDetail.MegInfo.MegName),
            sizeof (tLsppMegMeName));

    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtInParams);
    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtOutParams);

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : LsppPortMapProtocol 
 *
 * Description:  
 *
 * Input      : pLblInfo - pointer to the label info in the Out segment or 
 *                         In segment info present in the External out params.
 *              u1LblStkCnt - label stack count in Outsegment or Insegment.
 * Output     : 
 *
 * Returns    : None 
 *
 *****************************************************************************/

VOID
LsppPortMapProtocol (tMplsLblInfo * pLblInfo, UINT1 u1LblStkCnt,
                     UINT1 u1TnlSgnlPrtcl, UINT4 u4TnlType)
{

    while (u1LblStkCnt > 0)
    {
        if ((pLblInfo[u1LblStkCnt - 1].u1Protocol ==
             LSPP_MPLS_OWNER_RSVPTE) ||
            ((u1TnlSgnlPrtcl == LSPP_MPLS_TE_SIGPROTO_RSVP) ||
             ((u1TnlSgnlPrtcl == LSPP_MPLS_TE_SIGPROTO_NONE) &&
              (u4TnlType == LSPP_TE_TNL_TYPE_MPLS))))
        {
            pLblInfo[u1LblStkCnt - 1].u1Protocol = LSPP_PROTOCOL_RSVP_TE;
        }
        else if (pLblInfo[u1LblStkCnt - 1].u1Protocol == LSPP_MPLS_OWNER_LDP)
        {
            pLblInfo[u1LblStkCnt - 1].u1Protocol = LSPP_PROTOCOL_LDP;
        }
        else if ((pLblInfo[u1LblStkCnt - 1].u1Protocol ==
                  LSPP_MPLS_OWNER_STATIC) ||
                 ((u1TnlSgnlPrtcl == LSPP_MPLS_TE_SIGPROTO_NONE) &&
                  (u4TnlType == LSPP_TE_TNL_TYPE_MPLSTP)))
        {
            pLblInfo[u1LblStkCnt - 1].u1Protocol = LSPP_PROTOCOL_STATIC;
        }
        else if (pLblInfo[u1LblStkCnt - 1].u1Protocol ==
                 LSPP_MPLS_OWNER_UNKNOWN)
        {
            pLblInfo[u1LblStkCnt - 1].u1Protocol = LSPP_PROTOCOL_UNKNOWN;
        }

        u1LblStkCnt--;
    }
}

/******************************************************************************
 * Function   : LsppPortConvertProtocolVal 
 *
 * Description: 
 *
 * Input      : pLsppExtOutParams - Pointer to the ExternalOut Params
 *
 * Output     : 
 *
 * Returns    : None 
 *
 *****************************************************************************/

VOID
LsppPortConvertProtocolVal (tLsppExtOutParams * pLsppExtOutParams,
                            UINT1 u1SgnlPrtcl, UINT4 u4TnlType)
{
    tLsppInSegInfo     *pInSegInfo = NULL;
    tLsppOutSegInfo    *pOutSegInfo = NULL;

    if (pLsppExtOutParams->u1PathType == LSPP_MPLS_PATH_TYPE_TNL)
    {
        pInSegInfo = &(pLsppExtOutParams->OutTnlInfo.InSegInfo);
        pOutSegInfo = &(pLsppExtOutParams->OutTnlInfo.OutSegInfo);

    }
    else if (pLsppExtOutParams->u1PathType == LSPP_MPLS_PATH_TYPE_LSP)
    {
        pInSegInfo = &(pLsppExtOutParams->OutLdpInfo.InSegInfo);
        pOutSegInfo = &(pLsppExtOutParams->OutLdpInfo.OutSegInfo);
    }
    else
    {
        return;
    }

    LsppPortMapProtocol (pInSegInfo->LblInfo, pInSegInfo->u1LblStkCnt,
                         u1SgnlPrtcl, u4TnlType);
    LsppPortMapProtocol (pOutSegInfo->LblInfo, pOutSegInfo->u1LblStkCnt,
                         u1SgnlPrtcl, u4TnlType);
    return;
}

#ifndef BFD_WANTED
/* Stub function added for BFD. Need to be removed when BFD is enabled.*/
INT4
BfdApiHandleExtRequest (UINT4 u4, tBfdReqParams * Req, tBfdRespParams * Resp)
{

    UNUSED_PARAM (u4);
    UNUSED_PARAM (Req);
    UNUSED_PARAM (Resp);
    return OSIX_SUCCESS;
}
#endif

/*----------------------------------------------------------------------------
 *
 *                       End of lsppport.c file
 * 
 * ---------------------------------------------------------------------------*/
