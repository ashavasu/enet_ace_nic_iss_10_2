/***************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lspptx.c,v 1.27 2016/10/25 06:32:36 siva Exp $
 *
 * Description: This file contains the Lspp TX related functions
 ****************************************************************************/
#ifndef _LSPPTX_C_
#define _LSPPTX_C_

#include "lsppinc.h"
#include "../../l2vpn/inc/l2vpincs.h" 
/***************************************************************************
 * FUNCTION NAME    : LsppTxFrameHeader
 * DESCRIPTION      : This function is used to frame the header
 * Input(s)         : pBuf - Pointer to Buffer where Header details need 
 *                                 to be filled
 *                    pu4BufOffset - Offset in which data needs to be filled
 *                    pLsppHeader - Pointer to the header structure where 
 *                                  Header information needs to be fetched to 
 *                                  fill in the buffer
 * OUTPUT           : NONE
 * RETURNS          : NONE
 **************************************************************************/
VOID
LsppTxFrameHeader (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4BufOffset,
                   tLsppHeader * pLsppHeader)
{
    /* Fill Version */
    LSPP_CRU_PUT_2BYTE (pBuf, pu4BufOffset, pLsppHeader->u2Version);

    /* Fill Global Flag */
    LSPP_CRU_PUT_2BYTE (pBuf, pu4BufOffset, pLsppHeader->u2GlobalFlag);

    /* Fill Message Type */
    LSPP_CRU_PUT_1BYTE (pBuf, pu4BufOffset, pLsppHeader->u1MessageType);

    /* Fill Reply mode */
    LSPP_CRU_PUT_1BYTE (pBuf, pu4BufOffset, pLsppHeader->u1ReplyMode);

    /* Fill Return code */
    LSPP_CRU_PUT_1BYTE (pBuf, pu4BufOffset, pLsppHeader->u1ReturnCode);

    /* Fill Return sub code */
    LSPP_CRU_PUT_1BYTE (pBuf, pu4BufOffset, pLsppHeader->u1ReturnSubCode);

    /* Fill Senders handle */
    LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset, pLsppHeader->u4SenderHandle);

    /* Fill Sequence number */
    LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset, pLsppHeader->u4SequenceNum);

    /* Fill Timestamp sent in sec */
    LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset, pLsppHeader->u4TimeStampSentInSec);

    /* Fill  Timestamp sent in microsec */
    LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset,
                        pLsppHeader->u4TimeStampSentInMicroSec);

    /* Fill Received Timestamp in sec */
    LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset, pLsppHeader->u4TimeStampRxInSec);

    /* Fill Received Timestamp in microsec */
    LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset,
                        pLsppHeader->u4TimeStampRxInMicroSec);
}

/***************************************************************************
 * FUNCTION NAME    : LsppTxFrameLdpFec
 * DESCRIPTION      : This function is used to frame the IPV4 TLV 
 * Input(s)         : pBuf - Pointer to Buffer where details need 
 *                                 to be filled
 *                    pu4BufOffset - Offset in which data needs to be filled
 *                    pLsppLdpFecTlv - Pointer to the LDP strucutre
 * OUTPUT           : NONE
 * RETURNS          : NONE
 **************************************************************************/
VOID
LsppTxFrameLdpIpV4Fec (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4BufOffset,
                       tLsppLdpFecTlv * pLsppLdpFecTlv, UINT4 u4ContextId)
{
    /* Fill Ip Address */
    LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset, pLsppLdpFecTlv->Prefix.u4_addr[0]);

    /* Fill Prefix length */
    LSPP_CRU_PUT_1BYTE (pBuf, pu4BufOffset, pLsppLdpFecTlv->u1PrefixLength);

    /* Filling 3 bytes of  Pad Bytes */
    LSPP_CRU_PUT_2BYTE (pBuf, pu4BufOffset, 0);
    LSPP_CRU_PUT_1BYTE (pBuf, pu4BufOffset, 0);

    LSPP_LOG (u4ContextId, LSPP_DECODE_IPV4_LDP_FEC,
              "LsppTxFrameLdpIpV4Fec",
              pLsppLdpFecTlv->Prefix.u4_addr[0],
              pLsppLdpFecTlv->u1PrefixLength);
    return;
}

/***************************************************************************
 * FUNCTION NAME    : LsppTxFrameLdpIpV6Fec
 * DESCRIPTION      : This function is used to frame the IPV6 TLV 
 * Input(s)         : pBuf - Pointer to Buffer where RSVP TLV 
 *                                 details need to be filled
 *                    pu4BufOffset - Offset in which data needs to be filled
 *                    pLsppLdpFecTlv - Pointer to the LDP strucutre
 * OUTPUT           : NONE
 * RETURNS          : NONE
 **************************************************************************/
VOID
LsppTxFrameLdpIpV6Fec (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4BufOffset,
                       tLsppLdpFecTlv * pLsppLdpFecTlv, UINT4 u4ContextId)
{
    /* Fill Ip Address */
    LSPP_CRU_PUT_NBYTE (pBuf, pLsppLdpFecTlv->Prefix.u1_addr,
                        pu4BufOffset, LSPP_IPV6_ADDR_LEN);

    /* Filling Prefix length */
    LSPP_CRU_PUT_1BYTE (pBuf, pu4BufOffset, pLsppLdpFecTlv->u1PrefixLength);

    /* Filling zero elements */
    LSPP_CRU_PUT_2BYTE (pBuf, pu4BufOffset, 0);
    LSPP_CRU_PUT_1BYTE (pBuf, pu4BufOffset, 0);

    LSPP_LOG (u4ContextId, LSPP_DECODE_IPV6_LDP_FEC,
              "LsppTxFrameLdpIpV6Fec",
              pLsppLdpFecTlv->Prefix.u1_addr, pLsppLdpFecTlv->u1PrefixLength);

    return;
}

/***************************************************************************
 * FUNCTION NAME    : LsppTxFrameRsvpIpV4Fec
 * DESCRIPTION      : This function is used to frame the RSVP Ipv4 TLV 
 * Input(s)         : pBuf - Pointer to Buffer where TLV details need 
 *                                 to be filled
 *                    pu4BufOffset - Offset in which data needs to be filled
 *                    pLsppRsvpTeFecTlv - Pointer to the RSVP strucutre
 * OUTPUT           : NONE
 * RETURNS          : NONE
 **************************************************************************/
VOID
LsppTxFrameRsvpIpV4Fec (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4BufOffset,
                        tLsppRsvpTeFecTlv * pLsppRsvpTeFecTlv,
                        UINT4 u4ContextId)
{
    /* Filling Tunnel End Point Address */
    LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset,
                        pLsppRsvpTeFecTlv->TunnelEndAddr.u4_addr[0]);

    /* Filling zero elements in the TLV */
    LSPP_CRU_PUT_2BYTE (pBuf, pu4BufOffset, 0);

    /* Filling Tunnel Id */
    LSPP_CRU_PUT_2BYTE (pBuf, pu4BufOffset, pLsppRsvpTeFecTlv->u2TunnelId);

    /* Filling Extended Tunnel Address */
    LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset,
                        pLsppRsvpTeFecTlv->ExtendedTunnelId.u4_addr[0]);

    /* Filling IPV4 Tunnel Senders Address */
    LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset,
                        pLsppRsvpTeFecTlv->TunnelSenderAddr.u4_addr[0]);

    /* Filling zero elements in the TLV */
    LSPP_CRU_PUT_2BYTE (pBuf, pu4BufOffset, 0);

    /* Filling LSP Id */
    LSPP_CRU_PUT_2BYTE (pBuf, pu4BufOffset, pLsppRsvpTeFecTlv->u2LspId);

    LSPP_LOG (u4ContextId, LSPP_DECODE_IPV4_RSVP_FEC,
              "LsppTxFrameRsvpIpV4Fec",
              pLsppRsvpTeFecTlv->TunnelEndAddr.u4_addr[0],
              pLsppRsvpTeFecTlv->u2TunnelId,
              pLsppRsvpTeFecTlv->ExtendedTunnelId.u4_addr[0],
              pLsppRsvpTeFecTlv->TunnelSenderAddr.u4_addr[0],
              pLsppRsvpTeFecTlv->u2LspId);
}

/***************************************************************************
 * FUNCTION NAME    : LsppTxFrameRsvpIpV6Fec
 * DESCRIPTION      : This function is used to frame the RSVP Ipv6 TLV 
 * Input(s)         : pBuf - Pointer to Buffer where TLV details need 
 *                                 to be filled
 *                    pu4BufOffset - Offset in which data needs to be filled
 *                    pLsppRsvpTeFecTlv - Pointer to the RSVP strucutre
 * OUTPUT           : NONE
 * RETURNS          : NONE
 **************************************************************************/
VOID
LsppTxFrameRsvpIpV6Fec (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4BufOffset,
                        tLsppRsvpTeFecTlv * pLsppRsvpTeFecTlv,
                        UINT4 u4ContextId)
{
    /* Filling Tunnel End Point Address */
    LSPP_CRU_PUT_NBYTE (pBuf, pLsppRsvpTeFecTlv->TunnelEndAddr.u1_addr,
                        pu4BufOffset, LSPP_IPV6_ADDR_LEN);

    /* Filling zero elements in the TLV */
    LSPP_CRU_PUT_2BYTE (pBuf, pu4BufOffset, 0);

    /* Filling Tunnel Id */
    LSPP_CRU_PUT_2BYTE (pBuf, pu4BufOffset, pLsppRsvpTeFecTlv->u2TunnelId);

    /* Filling Extended Tunnel Address */
    LSPP_CRU_PUT_NBYTE (pBuf, pLsppRsvpTeFecTlv->ExtendedTunnelId.u1_addr,
                        pu4BufOffset, LSPP_IPV6_ADDR_LEN);

    /* Filling IPV6 Tunnel Senders Address */
    LSPP_CRU_PUT_NBYTE (pBuf, pLsppRsvpTeFecTlv->TunnelSenderAddr.u1_addr,
                        pu4BufOffset, LSPP_IPV6_ADDR_LEN);

    /* Filling zero elements in the TLV */
    LSPP_CRU_PUT_2BYTE (pBuf, pu4BufOffset, 0);

    /* Filling LSP Id */
    LSPP_CRU_PUT_2BYTE (pBuf, pu4BufOffset, pLsppRsvpTeFecTlv->u2LspId);

    LSPP_LOG (u4ContextId, LSPP_DECODE_IPV4_RSVP_FEC,
              "LsppTxFrameRsvpIpV6Fec",
              pLsppRsvpTeFecTlv->TunnelEndAddr.u1_addr,
              pLsppRsvpTeFecTlv->u2TunnelId,
              pLsppRsvpTeFecTlv->ExtendedTunnelId.u1_addr,
              pLsppRsvpTeFecTlv->TunnelSenderAddr.u1_addr,
              pLsppRsvpTeFecTlv->u2LspId);
}

/***************************************************************************
 * FUNCTION NAME    : LsppTxFrameFec128DeprecatePwFec
 * DESCRIPTION      : This function is used to frame the pw Deprecate TLV 
 * Input(s)         : pBuf - Pointer to Buffer where TLV details need 
 *                                 to be filled
 *                    pu4BufOffset - Offset in which data needs to be filled
 *                     pLsppPwFec128DeprecTlv - Pointer to the Pw strucutre
 * OUTPUT           : NONE
 * RETURNS          : NONE 
 **************************************************************************/
VOID
LsppTxFrameFec128DeprecatePwFec (tCRU_BUF_CHAIN_HEADER * pBuf,
                                 UINT4 *pu4BufOffset,
                                 tLsppPwFec128DeprecTlv *
                                 pLsppPwFec128DeprecTlv, UINT4 u4ContextId)
{
    /* Filling Remote Peer Address */
    LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset,
                        pLsppPwFec128DeprecTlv->LsppPwInfo.RemotePEAddr.
                        u4_addr[0]);

    /* Filling PW ID */
    LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset,
                        pLsppPwFec128DeprecTlv->LsppPwInfo.u4PWId);

    /* Filling PW TYPE */
    LSPP_CRU_PUT_2BYTE (pBuf, pu4BufOffset,
                        pLsppPwFec128DeprecTlv->LsppPwInfo.u2PWType);

    /* Filling zero elements in the TLV */
    LSPP_CRU_PUT_2BYTE (pBuf, pu4BufOffset, 0);

    LSPP_LOG (u4ContextId, LSPP_DECODE_FEC_PW_DEPREC,
              "LsppTxFrameFec128DeprecatePwFec",
              pLsppPwFec128DeprecTlv->LsppPwInfo.
              RemotePEAddr.u4_addr[0],
              pLsppPwFec128DeprecTlv->LsppPwInfo.u4PWId,
              pLsppPwFec128DeprecTlv->LsppPwInfo.u2PWType);
}

/***************************************************************************
 * FUNCTION NAME    : LsppTxFrameFec128Pw
 * DESCRIPTION      : This function is used to frame the Fec 128 Pw TLV 
 * Input(s)         : pBuf - Pointer to Buffer where TLV details need 
 *                                 to be filled
 *                    pu4BufOffset - Offset in which data needs to be filled
 *                    pLsppPwFec128Tlv - Pointer to the Pw strucutre
 * OUTPUT           : NONE
 * RETURNS          : NONE
 **************************************************************************/
VOID
LsppTxFrameFec128Pw (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4BufOffset,
                     tLsppPwFec128Tlv * pLsppPwFec128Tlv, UINT4 u4ContextId)
{
    /* Filling Sender Peer Address */
    LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset,
                        pLsppPwFec128Tlv->SenderPEAddr.u4_addr[0]);

    /* Filling Remote Peer Address */
    LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset,
                        pLsppPwFec128Tlv->LsppPwInfo.RemotePEAddr.u4_addr[0]);

    /* Filling PW ID */
    LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset,
                        pLsppPwFec128Tlv->LsppPwInfo.u4PWId);

    /* Filling PW TYPE */
    LSPP_CRU_PUT_2BYTE (pBuf, pu4BufOffset,
                        pLsppPwFec128Tlv->LsppPwInfo.u2PWType);

    /* Filling zero elements in the TLV */
    LSPP_CRU_PUT_2BYTE (pBuf, pu4BufOffset, 0);

    LSPP_LOG (u4ContextId, LSPP_DECODE_FEC_128_PW,
              "LsppTxFrameFec128Pw",
              pLsppPwFec128Tlv->SenderPEAddr.u4_addr[0],
              pLsppPwFec128Tlv->LsppPwInfo.
              RemotePEAddr.u4_addr[0],
              pLsppPwFec128Tlv->LsppPwInfo.u4PWId,
              pLsppPwFec128Tlv->LsppPwInfo.u2PWType);
}

/***************************************************************************
 * FUNCTION NAME    : LsppTxFrameFec129Pw
 * DESCRIPTION      : This function is used to frame the Fec 129 Pw TLV 
 * Input(s)         : pBuf - Pointer to Buffer where TLV details need 
 *                                 to be filled
 *                    pu4BufOffset - Offset in which data needs to be filled
 *                    pLsppPwFec129Tlv - Pointer to the Pw strucutre
 * OUTPUT           : NONE
 * RETURNS          : NONE
 **************************************************************************/
VOID
LsppTxFrameFec129Pw (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4BufOffset,
                     tLsppPwFec129Tlv * pLsppPwFec129Tlv, UINT1 u1SkipCount,
                     UINT4 u4ContextId)
{
    /* Filling Sender Peer Address */
    LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset,
                        pLsppPwFec129Tlv->SenderPEAddr.u4_addr[0]);

    /* Filling Remote Peer Address */
    LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset,
                        pLsppPwFec129Tlv->RemotePEAddr.u4_addr[0]);

    /* Filling PW TYPE */
    LSPP_CRU_PUT_2BYTE (pBuf, pu4BufOffset, pLsppPwFec129Tlv->u2PWType);

    /* Filling AGI TYPE */
    LSPP_CRU_PUT_1BYTE (pBuf, pu4BufOffset, pLsppPwFec129Tlv->u1AgiType);

    /* Filling AGI Length */
    LSPP_CRU_PUT_1BYTE (pBuf, pu4BufOffset, pLsppPwFec129Tlv->u1AgiLen);

    /* Filling AGI Value */
    LSPP_CRU_PUT_NBYTE (pBuf, pLsppPwFec129Tlv->au1Agi, pu4BufOffset,
                        pLsppPwFec129Tlv->u1AgiLen);

    /* Filling SAII TYPE */
    LSPP_CRU_PUT_1BYTE (pBuf, pu4BufOffset, pLsppPwFec129Tlv->u1SaiiType);

    /* Filling SAII Length */
    LSPP_CRU_PUT_1BYTE (pBuf, pu4BufOffset, pLsppPwFec129Tlv->u1SaiiLen);

    /* Filling SAII Value */
    LSPP_CRU_PUT_NBYTE (pBuf, pLsppPwFec129Tlv->au1Saii, pu4BufOffset,
                        pLsppPwFec129Tlv->u1SaiiLen);

    /* Filling TAII TYPE */
    LSPP_CRU_PUT_1BYTE (pBuf, pu4BufOffset, pLsppPwFec129Tlv->u1TaiiType);

    /* Filling TAII Length */
    LSPP_CRU_PUT_1BYTE (pBuf, pu4BufOffset, pLsppPwFec129Tlv->u1TaiiLen);

    /* Filling TAII Value */
    LSPP_CRU_PUT_NBYTE (pBuf, pLsppPwFec129Tlv->au1Taii, pu4BufOffset,
                        pLsppPwFec129Tlv->u1TaiiLen);

    /* Filling Pad bytes */
    while (u1SkipCount > 0)
    {
        LSPP_CRU_PUT_1BYTE (pBuf, pu4BufOffset, 0);
        u1SkipCount--;
    }

    LSPP_LOG (u4ContextId, LSPP_DECODE_FEC_129_PW,
              "LsppTxFrameFec129Pw",
              pLsppPwFec129Tlv->SenderPEAddr.u4_addr[0],
              pLsppPwFec129Tlv->RemotePEAddr.u4_addr[0],
              pLsppPwFec129Tlv->u2PWType, pLsppPwFec129Tlv->u1AgiType,
              pLsppPwFec129Tlv->u1AgiLen, pLsppPwFec129Tlv->au1Agi,
              pLsppPwFec129Tlv->u1SaiiType, pLsppPwFec129Tlv->u1SaiiLen,
              pLsppPwFec129Tlv->au1Saii, pLsppPwFec129Tlv->u1TaiiType,
              pLsppPwFec129Tlv->u1TaiiLen, pLsppPwFec129Tlv->au1Taii);
}

/***************************************************************************
 * FUNCTION NAME    : LsppTxFrameNilFec
 * DESCRIPTION      : This function is used to frame the Nil Fec Tlv 
 * Input(s)         : pBuf - Pointer to Buffer where TLV details need 
 *                                 to be filled
 *                    pu4BufOffset - Offset in which data needs to be filled
 *                   pLsppNilFecTlv - Pointer to the Nil Fec strucutre
 * OUTPUT           : NONE
 * RETURNS          : NONE
 **************************************************************************/
VOID
LsppTxFrameNilFec (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4BufOffset,
                   tLsppNilFecTlv * pLsppNilFecTlv, UINT4 u4ContextId)
{
    pLsppNilFecTlv->u4Label = (pLsppNilFecTlv->u4Label << 12);

    LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset, pLsppNilFecTlv->u4Label);

    LSPP_LOG (u4ContextId, LSPP_DECODE_NIL_FEC, "LsppTxFrameNilFec",
              pLsppNilFecTlv->u4Label);
}

/***************************************************************************
 * FUNCTION NAME    : LsppTxFrameStaticLspFec
 * DESCRIPTION      : This function is used to frame the Static LSP Fec 
 * Input(s)         : pBuf - Pointer to Buffer where TLV details need 
 *                                 to be filled
 *                    pu4BufOffset - Offset in which data needs to be filled
 *                    pLsppStaticLspFec - Pointer to the Static LSP 
 *                    Fec strucutre 
 * OUTPUT           : NONE
 * RETURNS          : NONE
 **************************************************************************/
VOID
LsppTxFrameStaticLspFec (tCRU_BUF_CHAIN_HEADER * pBuf,
                         UINT4 *pu4BufOffset,
                         tLsppStaticLspFec * pLsppStaticLspFec,
                         UINT4 u4ContextId)
{
    /* Filling Source Global Id */
    LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset, pLsppStaticLspFec->u4SrcGlobalId);

    /* Filling Source Node Id */
    LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset, pLsppStaticLspFec->u4SrcNodeId);

    /* Filling Source Tunnel Number */
    LSPP_CRU_PUT_2BYTE (pBuf, pu4BufOffset, pLsppStaticLspFec->u2SrcTnlNum);

    /* Filling LSP Number */
    LSPP_CRU_PUT_2BYTE (pBuf, pu4BufOffset, pLsppStaticLspFec->u2LspNum);

    /* Filling Destination Global Id */
    LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset, pLsppStaticLspFec->u4DstGlobalId);

    /* Filling Destination Node Id */
    LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset, pLsppStaticLspFec->u4DstNodeId);

    /* Filling Destination Tunnel Number */
    LSPP_CRU_PUT_2BYTE (pBuf, pu4BufOffset, pLsppStaticLspFec->u2DstTnlNum);

    /* Filling zero elements */
    LSPP_CRU_PUT_2BYTE (pBuf, pu4BufOffset, 0);

    LSPP_LOG (u4ContextId, LSPP_DECODE_STATIC_LSP_FEC,
              "LsppTxFrameStaticLspFec",
              pLsppStaticLspFec->u4SrcGlobalId,
              pLsppStaticLspFec->u4SrcNodeId, pLsppStaticLspFec->u2SrcTnlNum,
              pLsppStaticLspFec->u2LspNum, pLsppStaticLspFec->u4DstGlobalId,
              pLsppStaticLspFec->u4DstNodeId, pLsppStaticLspFec->u2DstTnlNum);
}

/***************************************************************************
 * FUNCTION NAME    : LsppTxFrameStaticPwFec
 * DESCRIPTION      : This function is used to frame the Static Pw Fec 
 * Input(s)         : pBuf - Pointer to Buffer where TLV details need 
 *                                 to be filled
 *                    pu4BufOffset - Offset in which data needs to be filled
 *                    pLsppStaticPwFec - Pointer to the Static Pw
 *                    u1SkipCount  - Pad Count
 *                    Fec strucutre 
 * OUTPUT           : NONE
 * RETURNS          : NONE
 **************************************************************************/
VOID
LsppTxFrameStaticPwFec (tCRU_BUF_CHAIN_HEADER * pBuf,
                        UINT4 *pu4BufOffset,
                        tLsppStaticPwFec * pLsppStaticPwFec, UINT1 u1SkipCount,
                        UINT4 u4ContextId)
{
    /* Filling AGI TYPE */
    LSPP_CRU_PUT_1BYTE (pBuf, pu4BufOffset, pLsppStaticPwFec->u1AgiType);

    /* Filling AGI Length */
    LSPP_CRU_PUT_1BYTE (pBuf, pu4BufOffset, pLsppStaticPwFec->u1AgiLen);

    /* Filling AGI Value */
    LSPP_CRU_PUT_NBYTE (pBuf, pLsppStaticPwFec->au1Agi, pu4BufOffset,
                        pLsppStaticPwFec->u1AgiLen);

    /* Filling Source Global Id */
    LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset, pLsppStaticPwFec->u4SrcGlobalId);

    /* Filling Source Node Id */
    LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset, pLsppStaticPwFec->u4SrcNodeId);

    /* Filling Source Ac Id */
    LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset, pLsppStaticPwFec->u4SrcAcId);

    /* Filling Destination Global Id */
    LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset, pLsppStaticPwFec->u4DstGlobalId);

    /* Filling Destination Node Id */
    LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset, pLsppStaticPwFec->u4DstNodeId);

    /* Filling Source Ac Id */
    LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset, pLsppStaticPwFec->u4DstAcId);

    /* Filling Pad bytes */
    while (u1SkipCount > 0)
    {
        LSPP_CRU_PUT_1BYTE (pBuf, pu4BufOffset, 0);
        u1SkipCount--;
    }

    LSPP_LOG (u4ContextId, LSPP_DECODE_STATIC_PW_FEC,
              "LsppTxFrameStaticPwFec",
              pLsppStaticPwFec->u1AgiType, pLsppStaticPwFec->u1AgiLen,
              pLsppStaticPwFec->au1Agi, pLsppStaticPwFec->u4SrcGlobalId,
              pLsppStaticPwFec->u4SrcNodeId, pLsppStaticPwFec->u4SrcAcId,
              pLsppStaticPwFec->u4DstGlobalId, pLsppStaticPwFec->u4DstNodeId,
              pLsppStaticPwFec->u4DstAcId);
}

/***************************************************************************
 * FUNCTION NAME    : LsppTxFramePadTlv
 * DESCRIPTION      : This function is used to frame the Pad Tlv 
 * Input(s)         : pBuf - Pointer to Buffer where TLV details need 
 *                                 to be filled
 *                    pu4BufOffset - Offset in CRU BUF
 *                    pLsppPadTlv - Pointer to the Pad Tlv structure
 *                    u2TlvLength - Pad Tlv Length 
 * OUTPUT           : NONE
 * RETURNS          : NONE
 **************************************************************************/
VOID
LsppTxFramePadTlv (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4BufOffset,
                   tLsppPadTlv * pLsppPadTlv, UINT2 u2TlvLength,
                   UINT4 u4ContextId)
{
    UNUSED_PARAM (u4ContextId);

    /* Filling Pad Pattern bytes. Decrement the size by 1 as the first octet
     * is copied already.*/

    if (u2TlvLength != 0)
    {
        /* Filling First Octet of Pad TLV which specifies whether the PAD 
         * patterns should be copied in reply or not */

        LSPP_CRU_PUT_1BYTE (pBuf, pu4BufOffset, pLsppPadTlv->u1FirstOctet);
        LSPP_CRU_PUT_NBYTE (pBuf, pLsppPadTlv->au1PadInfo, pu4BufOffset,
                            (u2TlvLength - 1));
    }
}

/***************************************************************************
 * FUNCTION NAME    : LsppTxFrameBfdDiscTlv
 * DESCRIPTION      : This function is used to frame the Bfd discriminator TLV
 * Input(s)         : pBuf - Pointer to Buffer where TLV details need 
 *                                 to be filled
 *                    pLsppErrorTlv - Pointer to the Bfd Disc TLV structure
 * OUTPUT           : NONE
 * RETURNS          : NONE
 **************************************************************************/
VOID
LsppTxFrameBfdDiscTlv (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4BufOffset,
                       tLsppBfdDiscTlv * pLsppBfdDiscTlv, UINT4 u4ContextId)
{
    /* Filling BFD Discrminator */
    LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset, pLsppBfdDiscTlv->u4Discriminator);

    LSPP_LOG (u4ContextId, LSPP_DECODE_BFD_DISC_TLV,
              "LsppTxFrameBfdDiscTlv", pLsppBfdDiscTlv->u4Discriminator);
}

/***************************************************************************
 * FUNCTION NAME    : LsppTxFrameReplyTosTlv
 * DESCRIPTION      : This function is used to frame Reply Tos TLV
 * Input(s)         : pBuf - Pointer to Buffer where TLV details need 
 *                                 to be filled
 *                    pLsppReplyTosTlv - Pointer to the Reply Tos TLV structure
 * OUTPUT           : NONE
 * RETURNS          : NONE
 **************************************************************************/
VOID
LsppTxFrameReplyTosTlv (tCRU_BUF_CHAIN_HEADER * pBuf,
                        UINT4 *pu4BufOffset,
                        tLsppReplyTosTlv * pLsppReplyTosTlv, UINT4 u4ContextId)
{
    /* Filling TOS Value */
    LSPP_CRU_PUT_1BYTE (pBuf, pu4BufOffset, pLsppReplyTosTlv->u1TosValue);

    /* Fillinf  3 zero bytes */
    LSPP_CRU_PUT_2BYTE (pBuf, pu4BufOffset, 0);
    LSPP_CRU_PUT_1BYTE (pBuf, pu4BufOffset, 0);

    LSPP_LOG (u4ContextId, LSPP_DECODE_REPLY_TOS_TLV,
              "LsppTxFrameReplyTosTlv", pLsppReplyTosTlv->u1TosValue);
}

/***************************************************************************
 * FUNCTION NAME    : LsppTxFrameMplsLabel
 * DESCRIPTION      : This function is used to frame Mpls Label
 * Input(s)         : u4Label - Label to fill in first 20 bits
 *                    u1Exp   - Exp of the label
 *                    u1SI    - SI for the label
 *                    u1Ttl   - Ttl for the label
 *                    pu4MplsHdr   - Mpls Label Header
 * OUTPUT           : NONE
 * RETURNS          : NONE
 **************************************************************************/
VOID
LsppTxFrameMplsLabel (UINT4 u4Label, UINT1 u1Exp, UINT1 u1SI, UINT1 u1Ttl,
                      UINT4 *pu4MplsHdr)
{
    /* Framing Label */
    *pu4MplsHdr =
        (((u4Label << LSPP_LBL_OFFSET_BITS) & 0xfffff000) |
         ((u1Exp << LSPP_EXP_OFFSET_BITS) & 0x00000e00) |
         ((u1SI << LSPP_SI_OFFSET_BITS) & 0x00000100) | (u1Ttl & 0x000000ff));
}

/***************************************************************************
 * FUNCTION NAME    : LsppTxFrameIfLblStkTlv
 * DESCRIPTION      : This function is used to frame Interface and label TLV
 * Input(s)         : pBuf - Pointer to Buffer where TLV details need 
 *                                 to be filled
 *                    pu4BufOffset - Pointer to the location where packet needs
 *                                   to be copied
 *
 *                    pLsppIfLblStkTlv - Pointer to If and Label Stk pointer
 * OUTPUT           : NONE
 * RETURNS          : NONE
 **************************************************************************/
VOID
LsppTxFrameIfLblStkTlv (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4BufOffset,
                        tLsppIfLblStkTlv * pLsppIfLblStkTlv, UINT4 u4ContextId)
{
    INT4                i4LblStkLen = 0;
    INT4                i4ExpStkLen = 0;
    UINT4               u4Label = 0;
    UINT1               au1LabelStack[LSPP_MAX_OCTETSTRING_SIZE];
    UINT1               au1LabelExp[LSPP_MAX_OCTETSTRING_SIZE];
    UINT1               u1LabelCount = 0;
    UINT1               u1RxLabelCount = 0;

    /* Filling AddressType */
    LSPP_CRU_PUT_1BYTE (pBuf, pu4BufOffset, pLsppIfLblStkTlv->u1AddrType);

    /* Filling 3 zero value */
    LSPP_CRU_PUT_2BYTE (pBuf, pu4BufOffset, 0);
    LSPP_CRU_PUT_1BYTE (pBuf, pu4BufOffset, 0);

    /* Filling Rx Ip Address */
    if ((pLsppIfLblStkTlv->u1AddrType == LSPP_IPV4_NUMBERED) ||
        (pLsppIfLblStkTlv->u1AddrType == LSPP_IPV4_UNNUMBERED))
    {
        LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset,
                            pLsppIfLblStkTlv->RxIpAddr.u4_addr[0]);

        LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset,
                            pLsppIfLblStkTlv->RxIfAddr.u4_addr[0]);
    }
    else if ((pLsppIfLblStkTlv->u1AddrType == LSPP_IPV6_NUMBERED) ||
             (pLsppIfLblStkTlv->u1AddrType == LSPP_IPV6_UNNUMBERED))
    {
        LSPP_CRU_PUT_NBYTE (pBuf, pLsppIfLblStkTlv->RxIpAddr.u1_addr,
                            pu4BufOffset, LSPP_IPV6_ADDR_LEN);

        LSPP_CRU_PUT_NBYTE (pBuf, pLsppIfLblStkTlv->RxIfAddr.u1_addr,
                            pu4BufOffset, LSPP_IPV6_ADDR_LEN);
    }
    else if (pLsppIfLblStkTlv->u1AddrType == LSPP_ADDR_TYPE_NOT_APPLICABLE)
    {
        /* Skip the Address fields. */
    }
    else
    {
        return;
    }

    /* Update Rx Label Stack SI before Framing the label */
    u1RxLabelCount = pLsppIfLblStkTlv->u1LblStackDepth;

    if ((u1RxLabelCount <= 0) || (u1RxLabelCount > LSPP_MAX_LABEL_STACK))
    {
        return;
    }

    pLsppIfLblStkTlv->RxLblInfo[u1RxLabelCount - 1].u1SI = 1;

    while (u1LabelCount < pLsppIfLblStkTlv->u1LblStackDepth)
    {
        LsppTxFrameMplsLabel
            (pLsppIfLblStkTlv->RxLblInfo[u1LabelCount].u4Label,
             pLsppIfLblStkTlv->RxLblInfo[u1LabelCount].u1Exp,
             pLsppIfLblStkTlv->RxLblInfo[u1LabelCount].u1SI,
             pLsppIfLblStkTlv->RxLblInfo[u1LabelCount].u1Ttl, &u4Label);

        LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset, u4Label);

        u1LabelCount++;
    }

    LsppUtilUpdateLblStk (pLsppIfLblStkTlv->RxLblInfo,
                          pLsppIfLblStkTlv->u1LblStackDepth,
                          au1LabelStack, &i4LblStkLen,
                          au1LabelExp, &i4ExpStkLen);

    if (pLsppIfLblStkTlv->u1AddrType == LSPP_IPV4_NUMBERED)
    {
        LSPP_LOG (u4ContextId, LSPP_DECODE_IPV4_IF_LBL_STK_TLV,
                  "LsppTxFrameIfLblStkTlv",
                  pLsppIfLblStkTlv->u1AddrType,
                  pLsppIfLblStkTlv->RxIpAddr.u4_addr[0],
                  pLsppIfLblStkTlv->RxIfAddr.u4_addr[0], au1LabelStack);
    }
    else if (pLsppIfLblStkTlv->u1AddrType == LSPP_IPV6_NUMBERED)
    {
        LSPP_LOG (u4ContextId, LSPP_DECODE_IPV6_IF_LBL_STK_TLV,
                  "LsppTxFrameIfLblStkTlv",
                  pLsppIfLblStkTlv->u1AddrType,
                  pLsppIfLblStkTlv->RxIpAddr.u1_addr,
                  pLsppIfLblStkTlv->RxIfAddr.u1_addr, au1LabelStack);
    }

}

/***************************************************************************
 * FUNCTION NAME    : LsppTxFrameDsmapTlv
 * DESCRIPTION      : This function is used to frame Dsmap TLV
 * Input(s)         : pBuf - Pointer to Buffer where TLV details need 
 *                           to be filled
 *                    pu4BufOffset - Pointer to the location where 
 *                                   packet needs to be copied
 *                    pLsppDsMapTlv - Pointer to Dsmap TLv strcuture
 * OUTPUT           : NONE
 * RETURNS          : NONE
 **************************************************************************/
VOID
LsppTxFrameDsmapTlv (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 *pu4BufOffset,
                     tLsppDsMapTlv * pLsppDsMapTlv, UINT4 u4ContextId)
{
    INT4                i4LblStkLen = 0;
    INT4                i4ExpStkLen = 0;
    UINT4               u4Label = 0;
    UINT1               u1LabelCount = 0;
    UINT1               u1DsLabelCount = 0;
    UINT1               au1LabelStack[LSPP_MAX_OCTETSTRING_SIZE];
    UINT1               au1LabelExp[LSPP_MAX_OCTETSTRING_SIZE];

    /* Filling MTU */
    LSPP_CRU_PUT_2BYTE (pBuf, pu4BufOffset, pLsppDsMapTlv->u2Mtu);

    /* Filling Address Type */
    LSPP_CRU_PUT_1BYTE (pBuf, pu4BufOffset, pLsppDsMapTlv->u1AddrType);

    /* Filling DS Flag */
    LSPP_CRU_PUT_1BYTE (pBuf, pu4BufOffset, pLsppDsMapTlv->u1DsFlags);

    /* Filling DS Ip and If Address */
    if ((pLsppDsMapTlv->u1AddrType == LSPP_IPV4_NUMBERED) ||
        (pLsppDsMapTlv->u1AddrType == LSPP_IPV4_UNNUMBERED))
    {
        LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset,
                            pLsppDsMapTlv->DsIpAddr.u4_addr[0]);
        LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset,
                            pLsppDsMapTlv->DsIfAddr.u4_addr[0]);
    }
    else if ((pLsppDsMapTlv->u1AddrType == LSPP_IPV6_NUMBERED) ||
             (pLsppDsMapTlv->u1AddrType == LSPP_IPV6_UNNUMBERED))
    {
        LSPP_CRU_PUT_NBYTE (pBuf, pLsppDsMapTlv->DsIpAddr.u1_addr,
                            pu4BufOffset, LSPP_IPV6_ADDR_LEN);
        LSPP_CRU_PUT_NBYTE (pBuf, pLsppDsMapTlv->DsIfAddr.u1_addr,
                            pu4BufOffset, LSPP_IPV6_ADDR_LEN);
    }
    else if (pLsppDsMapTlv->u1AddrType == LSPP_ADDR_TYPE_NOT_APPLICABLE)
    {
        /* Skip the Address fields. */
    }
    else
    {
        return;
    }

    /* Filling Multipath Type */
    LSPP_CRU_PUT_1BYTE (pBuf, pu4BufOffset, pLsppDsMapTlv->u1MultipathType);

    /* Filling Depth Limit */
    LSPP_CRU_PUT_1BYTE (pBuf, pu4BufOffset, pLsppDsMapTlv->u1DepthLimit);

    /* Filling Multipath Length */
    LSPP_CRU_PUT_2BYTE (pBuf, pu4BufOffset, pLsppDsMapTlv->u2MultipathLength);

    /* Filling Multipath Information */
    LSPP_CRU_PUT_NBYTE (pBuf, pLsppDsMapTlv->au1MultipathInfo,
                        pu4BufOffset, pLsppDsMapTlv->u2MultipathLength);

    /* Update SI in the bottom most label of the DSMAP Label Stack 
     * before Framing the label.
     */
    u1DsLabelCount = pLsppDsMapTlv->u1DsLblStackDepth;

    if ((u1DsLabelCount > LSPP_MAX_LABEL_STACK) || (u1DsLabelCount == 0))
    {
        return;
    }

    pLsppDsMapTlv->DsLblInfo[u1DsLabelCount - 1].u1SI = 1;

    while (u1LabelCount < u1DsLabelCount)
    {
        LsppTxFrameMplsLabel
            (pLsppDsMapTlv->DsLblInfo[u1LabelCount].u4Label,
             pLsppDsMapTlv->DsLblInfo[u1LabelCount].u1Exp,
             pLsppDsMapTlv->DsLblInfo[u1LabelCount].u1SI,
             pLsppDsMapTlv->DsLblInfo[u1LabelCount].u1Protocol, &u4Label);

        LSPP_CRU_PUT_4BYTE (pBuf, pu4BufOffset, u4Label);

        u1LabelCount++;
    }

    LsppUtilUpdateLblStk (pLsppDsMapTlv->DsLblInfo, u1DsLabelCount,
                          au1LabelStack, &i4LblStkLen,
                          au1LabelExp, &i4ExpStkLen);

    if (pLsppDsMapTlv->u1AddrType == LSPP_IPV4_NUMBERED ||
        pLsppDsMapTlv->u1AddrType == LSPP_IPV4_UNNUMBERED)
    {
        LSPP_LOG (u4ContextId, LSPP_DECODE_IPV4_DSMAP_TLV,
                  "LsppTxFrameDsmapTlv",
                  pLsppDsMapTlv->u2Mtu, pLsppDsMapTlv->u1AddrType,
                  pLsppDsMapTlv->u1DsFlags,
                  pLsppDsMapTlv->DsIpAddr.u4_addr[0],
                  pLsppDsMapTlv->DsIfAddr.u4_addr[0],
                  pLsppDsMapTlv->u1MultipathType,
                  pLsppDsMapTlv->u1DepthLimit,
                  pLsppDsMapTlv->u2MultipathLength,
                  pLsppDsMapTlv->au1MultipathInfo, au1LabelStack);
    }
    else if (pLsppDsMapTlv->u1AddrType == LSPP_IPV6_NUMBERED)
    {
        LSPP_LOG (u4ContextId, LSPP_DECODE_IPV6_DSMAP_TLV,
                  "LsppTxFrameDsmapTlv",
                  pLsppDsMapTlv->u2Mtu, pLsppDsMapTlv->u1AddrType,
                  pLsppDsMapTlv->u1DsFlags,
                  pLsppDsMapTlv->DsIpAddr.u1_addr,
                  pLsppDsMapTlv->DsIfAddr.u1_addr,
                  pLsppDsMapTlv->u1MultipathType,
                  pLsppDsMapTlv->u1DepthLimit,
                  pLsppDsMapTlv->u2MultipathLength,
                  pLsppDsMapTlv->au1MultipathInfo, au1LabelStack);
    }
}

/***************************************************************************
 * FUNCTION NAME    : LsppTxFillIpUdpHdr 
 * DESCRIPTION      : This function is used to frame Ip and UDP HEADER
 * 
 * Input(s)         : pBuf -   Pointer to CRU Buffer where 
 *                                   packet needs to be framed.
 *                    pLsppEchoMsg - Pointer to the structure
 *                                   where data to be filled is available
 *                    u4HeaderLen  - Encap Header length
 * OUTPUT           : NONE
 *
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 **************************************************************************/
INT4
LsppTxFillIpUdpHdr (tCRU_BUF_CHAIN_HEADER * pBuf, tLsppEchoMsg * pLsppEchoMsg)
{
    tLsppUdpHeader      TxUdpHdr;
    t_IP_HEADER         TxIpHdr;
    tLsppUdpIpPseudoHdr TxUdpPseudoHdr;
    UINT4               u4BufLen = 0;
    UINT4               u4IpHdrLen = 0;
    UINT4               u4UdpPseudoHdrLen = 0;
    UINT2               u2Cksum = 0;

    MEMSET (&TxUdpHdr, 0, sizeof (tLsppUdpHeader));
    MEMSET (&TxIpHdr, 0, sizeof (t_IP_HEADER));
    MEMSET (&TxUdpPseudoHdr, 0, sizeof (tLsppUdpIpPseudoHdr));

    u4BufLen = CRU_BUF_Get_ChainValidByteCount (pBuf);

    /* Fill UDP Header Info */
    TxUdpHdr.u2Dest_u_port = OSIX_HTONS (pLsppEchoMsg->LsppUdpHeader.
                                         u2Dest_u_port);
    TxUdpHdr.u2Src_u_port = OSIX_HTONS (pLsppEchoMsg->LsppUdpHeader.
                                        u2Src_u_port);
    TxUdpHdr.u2Len = OSIX_HTONS ((UINT2) (u4BufLen + LSPP_UDP_HDR_LEN));

    /* Fill UDP IP Pseudo Header Info */
    TxUdpPseudoHdr.u4Src = OSIX_HTONL (pLsppEchoMsg->LsppIpHeader.u4Src);
    TxUdpPseudoHdr.u4Dest = OSIX_HTONL (pLsppEchoMsg->LsppIpHeader.u4Dest);
    TxUdpPseudoHdr.u1Zero = 0;
    TxUdpPseudoHdr.u1Proto = IPPROTO_UDP;
    TxUdpPseudoHdr.u2UdpLen =
        OSIX_HTONS ((UINT2) (u4BufLen + LSPP_UDP_HDR_LEN));

    /* Fill IP Header Info */

    if (pLsppEchoMsg->LsppIpHeader.u1Hlen >
        (IP_HDR_LEN / LSPP_IP_HEADER_LEN_IN_BYTES))
    {
        u4IpHdrLen = sizeof (t_IP_HEADER);
        TxIpHdr.u1Ver_hdrlen = (UINT1)
            IP_VERS_AND_HLEN (pLsppEchoMsg->LsppIpHeader.u1Version,
                              LSPP_IP_OPTION_FILED_LEN);

        TxIpHdr.u2Totlen = OSIX_HTONS ((UINT2) (u4BufLen + u4IpHdrLen +
                                                LSPP_UDP_HDR_LEN));

        MEMCPY (TxIpHdr.u1Options, pLsppEchoMsg->LsppIpHeader.au1Options,
                LSPP_IP_OPTION_FILED_LEN);
    }
    else
    {
        u4IpHdrLen = IP_HDR_LEN;
        TxIpHdr.u1Ver_hdrlen = (UINT1)
            IP_VERS_AND_HLEN (pLsppEchoMsg->LsppIpHeader.u1Version, 0);
        TxIpHdr.u2Totlen =
            OSIX_HTONS ((UINT2) (u4BufLen + u4IpHdrLen + LSPP_UDP_HDR_LEN));

    }
    TxIpHdr.u1Tos = pLsppEchoMsg->LsppIpHeader.u1Tos;
    TxIpHdr.u2Id = 0;
    TxIpHdr.u2Fl_offs = 0;
    TxIpHdr.u1Ttl = 1;
    TxIpHdr.u1Proto = IPPROTO_UDP;
    TxIpHdr.u2Cksum = 0;
    TxIpHdr.u4Src = OSIX_HTONL (pLsppEchoMsg->LsppIpHeader.u4Src);
    TxIpHdr.u4Dest = OSIX_HTONL (pLsppEchoMsg->LsppIpHeader.u4Dest);

    if (CRU_BUF_Prepend_BufChain
        (pBuf, (UINT1 *) &TxUdpHdr, sizeof (tLsppUdpHeader)) != CRU_SUCCESS)
    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId,
                  LSPP_CRU_BUF_UDP_HDR_PREPEND_FAILED, "LsppTxFillIpUdpHdr");
        return OSIX_FAILURE;
    }

    /* TODO: u4UdpPseudoHdrLen is set to size of struct, assuming that
     * the struct is packed. Assuming that the sizeof will return the value 12.
     * Following prpend is also using the copy of structure. 
     * If the struct is not aligned and packed and if the value returned is not
     * equal to 12, this piece of code is a problem. */
    u4UdpPseudoHdrLen = sizeof (tLsppUdpIpPseudoHdr);

    if (CRU_BUF_Prepend_BufChain
        (pBuf, (UINT1 *) &TxUdpPseudoHdr, u4UdpPseudoHdrLen) != CRU_SUCCESS)
    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId,
                  LSPP_CRU_BUF_UDP_HDR_PREPEND_FAILED, "LsppTxFillIpUdpHdr");
        return OSIX_FAILURE;
    }

    /* Get the CRU Buf Length with UDP Header */
    u4BufLen = LSPP_GET_CRU_VALID_BYTE_COUNT (pBuf);

    /* Calculate checksum for UDP */
    u2Cksum = UtlIpCSumCruBuf (pBuf, u4BufLen, 0);
    u2Cksum = OSIX_HTONS (u2Cksum);

    if (CRU_BUF_Copy_OverBufChain
        (pBuf, (UINT1 *) &u2Cksum, u4UdpPseudoHdrLen + LSPP_UDP_CKSUM_OFFSET,
         sizeof (UINT2)) != CRU_SUCCESS)
    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId,
                  LSPP_CRU_UDP_CHCKSUM_UPDATION_FAILED, "LsppTxFillIpUdpHdr");
        return OSIX_FAILURE;
    }

    /* Since we want to prepend the IP header before UDP header, move the valid
     * offset to the beginning of UDP header */
    if (LSPP_CRU_MOVE_VALID_OFFSET (pBuf, u4UdpPseudoHdrLen) != CRU_SUCCESS)
    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId, LSPP_CRU_BUF_IP_HDR_PREPEND_FAILED,
                  "LsppTxFillIpUdpHdr");
        return OSIX_FAILURE;
    }

    if (CRU_BUF_Prepend_BufChain
        (pBuf, (UINT1 *) &TxIpHdr, u4IpHdrLen) != CRU_SUCCESS)
    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId, LSPP_CRU_BUF_IP_HDR_PREPEND_FAILED,
                  "LsppTxFillIpUdpHdr");
        return OSIX_FAILURE;
    }

    /* Get the CRU Buf Length with IP Header */
    u4BufLen = LSPP_GET_CRU_VALID_BYTE_COUNT (pBuf);

    /* Calculate checksum for IP */
    u2Cksum = UtlIpCSumCruBuf (pBuf, u4IpHdrLen, 0);
    u2Cksum = OSIX_HTONS (u2Cksum);

    if (CRU_BUF_Copy_OverBufChain
        (pBuf, (UINT1 *) &u2Cksum, (LSPP_IP_CKSUM_OFFSET),
         sizeof (UINT2)) != CRU_SUCCESS)
    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId,
                  LSPP_CRU_IP_CHCKSUM_UPDATION_FAILED, "LsppTxFillIpUdpHdr");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppTxFrameLspMepTlv
 * DESCRIPTION      : This function is used to frame the Mep TLV
 * INPUT            : pMplsAchTlv - ACH TLV Info
 *                    pLsppMepTlv - Lsp Mep Info where MEP details are avail
 * OUTPUT           : NONE
 * RETURNS          : NONE
 **************************************************************************/
VOID
LsppTxFrameLspMepTlv (tLsppMepTlv * pLsppMepTlv, tLsppAchTlv * pMplsAchTlv)
{
    pMplsAchTlv->u2TlvType = LSPP_ACH_TYPE_IP_TUNNEL_MEP;

    pMplsAchTlv->u2TlvLen = LSPP_ACH_TYPE_TUNNEL_MEP_LEN;

    MEMCPY (&(pMplsAchTlv->unAchValue.MepTlv.unMepTlv.IpLspMepTlv),
            &(pLsppMepTlv->unMepTlv.IpLspMepTlv), sizeof (tMplsIpLspMepTlv));
}

/***************************************************************************
 * FUNCTION NAME    : LsppTxFrameLspPwTlv
 * DESCRIPTION      : This function is used to frame the Pw Mep TLV
 * INPUT            : pMplsAchTlv - ACH TLV Info
 * OUTPUT           : NONE
 * RETURNS          : NONE
 **************************************************************************/
VOID
LsppTxFramePwMepTlv (tLsppMepTlv * pLsppMepTlv, tLsppAchTlv * pMplsAchTlv)
{
    pMplsAchTlv->u2TlvType = LSPP_ACH_TYPE_IP_PW_MEP;

    pMplsAchTlv->u2TlvLen = (UINT2) (pLsppMepTlv->unMepTlv.IpPwMepTlv.u1AgiLen +
                                     LSPP_ACH_PW_MEP_MANDATORY_FIELD_LEN);

    MEMCPY (&(pMplsAchTlv->unAchValue.MepTlv.unMepTlv.IpPwMepTlv),
            &(pLsppMepTlv->unMepTlv.IpPwMepTlv), sizeof (tMplsIpPwMepTlv));
}

/***************************************************************************
 * FUNCTION NAME    : LsppTxFrameICCMepTlv
 * DESCRIPTION      : This function is used to frame the ICC Mep TLV
 * INPUT            : pMplsAchTlv - ACH TLV Info
 * OUTPUT           : NONE
 * RETURNS          : NONE
 **************************************************************************/
VOID
LsppTxFrameIccMepTlv (tLsppMepTlv * pLsppMepTlv, tLsppAchTlv * pMplsAchTlv)
{
    pMplsAchTlv->u2TlvType = LSPP_ACH_TYPE_ICC_MEP;

    pMplsAchTlv->u2TlvLen = LSPP_ACH_TYPE_ICC_MEP_LEN;

    MEMCPY (&(pMplsAchTlv->unAchValue.MepTlv.unMepTlv.IccMepTlv),
            &(pLsppMepTlv->unMepTlv.IccMepTlv), sizeof (tMplsIccMepTlv));
}

/***************************************************************************
 * FUNCTION NAME    : LsppTxFrameMipTlv
 * DESCRIPTION      : This function is used to frame the Mip TLV
 * INPUT            : pMplsAchTlv - ACH TLV Info                    
 * OUTPUT           : NONE
 * RETURNS          : NONE
 **************************************************************************/
VOID
LsppTxFrameMipTlv (tLsppMipTlv * pMplsMipTlv, tLsppAchTlv * pMplsAchTlv)
{
    pMplsAchTlv->u2TlvType = LSPP_ACH_TYPE_MIP;

    pMplsAchTlv->u2TlvLen = LSPP_ACH_TYPE_MIP_LEN;

    MEMCPY (&(pMplsAchTlv->unAchValue.MipTlv), pMplsMipTlv,
            sizeof (tLsppMipTlv));
}

/***************************************************************************
 * FUNCTION NAME    : LsppTxEchoPkt
 * DESCRIPTION      : This function is to transmit the packet to MPLSRTR module
 * INPUT            : pBuf - Cru Buf which contain packet to transmit
 *                    pLsppEchoMsg - Echo msg structure where Information to 
 *                    fill IP and UDP header are available
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 **************************************************************************/
UINT4
LsppTxEchoPkt (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4ContextId,
               tLsppOutSegInfo * pOutSegInfo)
{
    tLsppExtInParams   *pLsppExtInParams = NULL;
    tLsppExtOutParams  *pLsppExtOutParams = NULL;

    if ((pLsppExtInParams = (tLsppExtInParams *)
         MemAllocMemBlk (LSPP_FSLSPPINPUTPARAMS_POOLID)) == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((pLsppExtOutParams = (tLsppExtOutParams *)
         MemAllocMemBlk (LSPP_FSLSPPOUTPUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        return OSIX_FAILURE;
    }

    MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
    MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));

    if ((pOutSegInfo == NULL) || (pBuf == NULL))
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);
        return OSIX_FAILURE;
    }

    pLsppExtInParams->u4RequestType = LSPP_MPLSRTR_TX_OAM_PKT;
    pLsppExtInParams->u4ContextId = u4ContextId;

    pLsppExtInParams->InPktTxInfo.pBuf = pBuf;

    pLsppExtInParams->InPktTxInfo.NextHopIpAddrType = pOutSegInfo->u1NHAddrType;

    if (pOutSegInfo->u1NHAddrType == LSPP_MPLS_ADDR_TYPE_IPV4)
    {
        pLsppExtInParams->InPktTxInfo.NextHopIpAddr.u4_addr[0] =
            pOutSegInfo->NHAddr.u4_addr[0];
    }
    else if (pOutSegInfo->u1NHAddrType == LSPP_MPLS_ADDR_TYPE_IPV6)
    {
        MEMCPY (pLsppExtInParams->InPktTxInfo.NextHopIpAddr.u1_addr,
                pOutSegInfo->NHAddr.u1_addr, LSPP_IPV6_ADDR_LEN);
    }

    pLsppExtInParams->InPktTxInfo.u4OutIfIndex = pOutSegInfo->u4OutIf;

    MEMCPY (&(pLsppExtInParams->InPktTxInfo.au1DstMac),
            pOutSegInfo->au1NextHopMac, MAC_ADDR_LEN);

    pLsppExtInParams->InPktTxInfo.bIsMplsLabelledPacket = OSIX_TRUE;

    /* Un-Numbered is not supported */
    pLsppExtInParams->InPktTxInfo.bIsUnNumberedIf = OSIX_FALSE;

    /* Call the LSPP exit function to Tx to packet to MPLSRTR */
    if (LsppPortHandleExtInteraction
        (pLsppExtInParams, pLsppExtOutParams) != OSIX_SUCCESS)
    {
        LSPP_LOG (u4ContextId, LSPP_FETCH_FROM_EXT_MOD_FAILED, "LsppTxEchoPkt");
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);
        return OSIX_FAILURE;
    }
    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtInParams);
    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtOutParams);
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppTxModifyEchoPkt
 * DESCRIPTION      : This function is used to change the sequence number of
 *                    each request and to change the checksum of IP and UDP 
 *                    header
 * INPUT            : pBuf - Cru Buf which contain packet to transmit
 *                    pLsppEchoMsg - Echo msg structure where Information to 
 *                    fill IP and UDP header are available
 *                    u4LastSentSeqNum - Contains the last sent sequence 
 *                    number Value
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 **************************************************************************/
UINT4
LsppTxModifyEchoPkt (tCRU_BUF_CHAIN_HEADER * pBuf,
                     tLsppEchoMsg * pLsppEchoMsg, UINT4 u4LastSentSeqNum)
{
    UINT4               u4OffSet = 0;

    u4OffSet = pLsppEchoMsg->u4EncapHdrLen + LSPP_SEQ_NUM_OFFSET;

    /* Incrementing the last sent seq number */
/*    (u4LastSentSeqNum)++; */

    /* Fill Sequence number */
    LSPP_CRU_PUT_4BYTE (pBuf, &u4OffSet, (u4LastSentSeqNum));

    if ((pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_IP) ||
        (pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_ACH_IP))
    {
        if (LSPP_CRU_MOVE_VALID_OFFSET (pBuf, pLsppEchoMsg->u4EncapHdrLen) !=
            CRU_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        if (LsppTxEncapsulatePdu (pBuf, pLsppEchoMsg) != OSIX_SUCCESS)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId, LSPP_ENCAP_PDU_FAILED,
                      "LsppTxModifyEchoPkt");
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : LsppTxUpdateOutGoingLblStk 
 * Description: This function is to fill the label stack with Alert label      
 * Input      : pLsppEchomsg - Pointer to Lspp Echo message structure 
 * Output     : OSIX_SUCCESS or OSIX_FAILURE 
 * Returns    : None
 *****************************************************************************/
UINT4
LsppTxUpdateOutGoingLblStk (tLsppEchoMsg * pLsppEchoMsg)
{
    tLsppLblInfo        LsppLblInfo;
    UINT1               u1LabelCount = 0;

    MEMSET (&LsppLblInfo, 0, sizeof (tLsppLblInfo));

    u1LabelCount = pLsppEchoMsg->u1LabelStackDepth;

    if ((u1LabelCount <= 0) || (u1LabelCount > LSPP_MAX_LABEL_STACK))
    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId,
                  LSPP_INVALID_LABEL_COUNT, "LsppTxUpdateOutGoingLblStk");
        return OSIX_FAILURE;
    }

    if ((pLsppEchoMsg->LsppPathInfo.u1PathType == LSPP_PATH_TYPE_STATIC_PW) ||
        (pLsppEchoMsg->LsppPathInfo.u1PathType == LSPP_PATH_TYPE_FEC_128) ||
        (pLsppEchoMsg->LsppPathInfo.u1PathType == LSPP_PATH_TYPE_FEC_129))
    {
        if (pLsppEchoMsg->u1CcSelected == LSPP_VCCV_CC_TTL)
        {
            pLsppEchoMsg->LblInfo[u1LabelCount - 1].u1Ttl = 1;
        }
        else if (pLsppEchoMsg->u1CcSelected == LSPP_VCCV_CC_RAL)
        {
            /* Adding RAL to the Label Stack */
            MEMCPY (&LsppLblInfo, &(pLsppEchoMsg->LblInfo[u1LabelCount - 1]),
                    sizeof (tLsppLblInfo));

            /* Updating RAL Label */
            pLsppEchoMsg->LblInfo[u1LabelCount - 1].u4Label =
                LSPP_LBL_ROUTER_ALERT;
            pLsppEchoMsg->LblInfo[u1LabelCount - 1].u1Ttl = 1;
            pLsppEchoMsg->LblInfo[u1LabelCount - 1].u1SI = 0;
            (u1LabelCount)++;

            if (u1LabelCount > LSPP_MAX_LABEL_STACK)
            {
                LSPP_LOG (pLsppEchoMsg->u4ContextId,
                          LSPP_UPDATING_LBL_STK_FAILED,
                          "LsppTxUpdateOutGoingLblStk", LSPP_LBL_ROUTER_ALERT);
                return OSIX_FAILURE;
            }

            /* Updating the Pw Label */
            MEMCPY (&(pLsppEchoMsg->LblInfo[u1LabelCount - 1]), &LsppLblInfo,
                    sizeof (tLsppLblInfo));
        }
    }
    else if ((pLsppEchoMsg->LsppPathInfo.u1PathType ==
              LSPP_PATH_TYPE_STATIC_TNL) &&
             ((pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_ACH) ||
              (pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_ACH_IP)))
    {
        /* Updating GAL Label */
        (u1LabelCount)++;

        if (u1LabelCount > LSPP_MAX_LABEL_STACK)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_UPDATING_LBL_STK_FAILED,
                      "LsppTxUpdateOutGoingLblStk", LSPP_GAL_LABEL);
            return OSIX_FAILURE;
        }

        pLsppEchoMsg->LblInfo[u1LabelCount - 1].u4Label = LSPP_GAL_LABEL;
        pLsppEchoMsg->LblInfo[u1LabelCount - 1].u1Ttl = 1;
        pLsppEchoMsg->LblInfo[u1LabelCount - 1].u1SI = 0;
    }

    if (u1LabelCount > LSPP_MAX_LABEL_STACK)
    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId,
                  LSPP_INVALID_LABEL_COUNT, "LsppTxUpdateOutGoingLblStk");
        return OSIX_FAILURE;
    }

    pLsppEchoMsg->LblInfo[u1LabelCount - 1].u1SI = 1;
    pLsppEchoMsg->u1LabelStackDepth = u1LabelCount;

    return OSIX_SUCCESS;
}

 /***************************************************************************
 * FUNCTION NAME    : LsppTxEncapsulatePdu 
 * DESCRIPTION      : This function is used to frame encapsulation
 * Input(s)         : pMsg         -   Pointer to CRU Buffer where 
 *                                   packet needs to be framed.
 *                                   packet needs to be framed
 *                    pLsppEchoMsg - Pointer to the structure
 *                                   where data to be filled is available *
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 **************************************************************************/
INT4
LsppTxEncapsulatePdu (tCRU_BUF_CHAIN_HEADER * pMsg, tLsppEchoMsg * pLsppEchoMsg)
{

    tLsppAchTlv         MplsAchTlv;
    tLsppAchTlvHeader   MplsAchTlvHdr;
    tLsppAchHeader      MplsAchHdr;
    tLsppMplsHdr        MplsHdr;
    tPwVcEntry          *pPwVcEntry = NULL;
    UINT1               au1Pad[4];
    UINT1               u1LabelCount = 0;
    UINT1               u1PadCount = 0;
   UINT1               u1SetAchFlag = OSIX_TRUE;
    MEMSET (&MplsAchHdr, 0, sizeof (tLsppAchHeader));
    MEMSET (&MplsAchTlv, 0, sizeof (tLsppAchTlv));
    MEMSET (&MplsAchTlvHdr, 0, sizeof (tLsppAchTlvHeader));
    MEMSET (&MplsHdr, 0, sizeof (tLsppMplsHdr));
    MEMSET (au1Pad, 0, sizeof (au1Pad));

    /* Filling IP UDP Header */
    if ((pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_IP) ||
        (pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_ACH_IP))
    {
        if (LsppTxFillIpUdpHdr (pMsg, pLsppEchoMsg) != OSIX_SUCCESS)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_UPDATING_IP_UDP_HDR_FAILED, "LsppTxEncapsulatePdu");
            return OSIX_FAILURE;
        }

    }

    if ((pLsppEchoMsg->LsppPathInfo.u1PathType == LSPP_PATH_TYPE_STATIC_PW) ||
            (pLsppEchoMsg->LsppPathInfo.u1PathType == LSPP_PATH_TYPE_FEC_128) ||
            (pLsppEchoMsg->LsppPathInfo.u1PathType == LSPP_PATH_TYPE_FEC_129))
    {
        pPwVcEntry = L2VpnGetPwVcEntryFromIndex(pLsppEchoMsg->LsppPathInfo.
                PwDetail.PwFecInfo.u4PwIndex);
        if(NULL == pPwVcEntry)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                    LSPP_UPDATING_IP_UDP_HDR_FAILED, "Vc entry not found");
            return OSIX_FAILURE;
        }
    }

    /* Filling ACH TLVs and ACH TLV Header */
    if (pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_ACH)
    {
        if (pLsppEchoMsg->LsppAchInfo.AchTlvHeader.u2TlvHdrLen != 0)
        {
            if (pLsppEchoMsg->LsppAchInfo.u1AchTlvPresent & LSPP_ACH_MIP_TLV)
            {
                LsppTxFrameMipTlv (&(pLsppEchoMsg->LsppAchInfo.LsppMipTlv),
                                   &MplsAchTlv);
                MplsUtlFillMplsAchTlv (&MplsAchTlv, pMsg);
            }

            if (pLsppEchoMsg->LsppAchInfo.u1AchTlvPresent &
                LSPP_ACH_IP_LSP_MEP_TLV)
            {
                LsppTxFrameLspMepTlv (&(pLsppEchoMsg->LsppAchInfo.LsppMepTlv),
                                      &MplsAchTlv);
                MplsUtlFillMplsAchTlv (&MplsAchTlv, pMsg);
            }
            else if (pLsppEchoMsg->LsppAchInfo.u1AchTlvPresent &
                     LSPP_ACH_IP_PW_MEP_TLV)
            {
                LsppTxFramePwMepTlv (&(pLsppEchoMsg->LsppAchInfo.LsppMepTlv),
                                     &MplsAchTlv);

                u1PadCount = (UINT1) ((LSPP_BYTE_PAD_MAX_LEN -
                                       (MplsAchTlv.u2TlvLen %
                                        LSPP_BYTE_PAD_MAX_LEN))
                                      % LSPP_BYTE_PAD_MAX_LEN);

                if (CRU_BUF_Prepend_BufChain
                    (pMsg, (UINT1 *) &au1Pad, u1PadCount) != CRU_SUCCESS)
                {
                    /* CRU Buf Prepend Failed */
                    return OSIX_FAILURE;
                }

                MplsUtlFillMplsAchTlv (&MplsAchTlv, pMsg);
            }
            else if (pLsppEchoMsg->LsppAchInfo.u1AchTlvPresent &
                     LSPP_ACH_ICC_MEP_TLV)
            {
                LsppTxFrameIccMepTlv (&(pLsppEchoMsg->LsppAchInfo.LsppMepTlv),
                                      &MplsAchTlv);

                MplsUtlFillMplsAchTlv (&MplsAchTlv, pMsg);
            }
        }

        MplsAchTlvHdr.u2TlvHdrLen =
            pLsppEchoMsg->LsppAchInfo.AchTlvHeader.u2TlvHdrLen;

        MplsUtlFillMplsAchTlvHdr (&MplsAchTlvHdr, pMsg);
    }

      if ((pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_ACH) ||
        (pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_ACH_IP))
    {
   if ((pPwVcEntry != NULL) && (pPwVcEntry->i1ControlWord == L2VPN_DISABLED))
   {
       u1SetAchFlag = OSIX_FALSE;
   }
   if (u1SetAchFlag == OSIX_TRUE)
   {
        /* Filling Ach Header */
        MplsAchHdr.u4AchType = 1;
        MplsAchHdr.u1Version = 0;
        MplsAchHdr.u1Rsvd = 0;

        if (pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_ACH)
        {
            MplsAchHdr.u2ChannelType = LSPP_ACH_CHNL_TYPE_LSPP;
        }
        else if ((pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_ACH_IP) &&
                 (pLsppEchoMsg->LsppIpHeader.u1Version == LSPP_ENCAP_IPV4_HDR))
        {
            MplsAchHdr.u2ChannelType = LSPP_ACH_CHNL_TYPE_IPV4;
        }
        else if ((pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_ACH_IP) &&
                 (pLsppEchoMsg->LsppIpHeader.u1Version == LSPP_ENCAP_IPV6_HDR))
        {
            MplsAchHdr.u2ChannelType = LSPP_ACH_CHNL_TYPE_IPV6;
        }
        MplsUtlFillMplsAchHdr (&MplsAchHdr, pMsg); 
   }
    }

    /* This function is to update Echo Message Label Information.
     * In case of TNL - GAL Label is added and in case of 
     * Pw - Label are modified as per Cc configured 
     */
    if (LsppTxUpdateOutGoingLblStk (pLsppEchoMsg) != OSIX_SUCCESS)
    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId,
                  LSPP_UPDATING_OUTGOING_LBL_STK_FAILED,
                  "LsppTxEncapsulatePdu");
        return OSIX_FAILURE;
    }

    u1LabelCount = pLsppEchoMsg->u1LabelStackDepth;

    while (u1LabelCount > 0)
    {
        /* Updating MplsHdr */
        MplsHdr.u4Lbl = pLsppEchoMsg->LblInfo[u1LabelCount - 1].u4Label;
        MplsHdr.Ttl = pLsppEchoMsg->LblInfo[u1LabelCount - 1].u1Ttl;
        MplsHdr.Exp = pLsppEchoMsg->LblInfo[u1LabelCount - 1].u1Exp;
        MplsHdr.SI = pLsppEchoMsg->LblInfo[u1LabelCount - 1].u1SI;

        MplsUtlFillMplsHdr (&MplsHdr, pMsg);
        u1LabelCount--;
    }

    return OSIX_SUCCESS;
}

 /***************************************************************************
 * FUNCTION NAME    : LsppTxFramePingPayload
 * DESCRIPTION      : This function is used to frame the ping Payload
 * Input(s)         : pMsg -   Pointer to CRU Buffer where 
 *                                   packet needs to be framed.
 *                    pu4BufOffset - Valid Offset from where 
 *                                   packet needs to be framed
 *                    pLsppPduInfo - Pointer to the structure
 *                                   where data to be filled is available 
 * OUTPUT           :  pMsg - Updated Cru Buffer with Ping Payload 
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 **************************************************************************/
INT4
LsppTxFramePingPayload (tCRU_BUF_CHAIN_HEADER * pMsg, UINT4 *pu4BufOffset,
                        tLsppPduInfo * pLsppPduInfo, UINT4 u4ContextId)
{
    tLsppSysTime        LsppEchoPktTxTime;
    UINT1               u1FecStackDepth = 0;
    UINT2               u2TlvLength = 0;
    UINT1               u1SkipCount = 0;

    MEMSET (&LsppEchoPktTxTime, 0, sizeof (tLsppSysTime));

    /* Fetching the current time from FSAP to fill the Timestamp 
     * in the echo packet only in case of request. In case of echo reply
     * Sent Timestamp will already to present. 
     */
    if (pLsppPduInfo->LsppHeader.u1MessageType == LSPP_ECHO_REQUEST)
    {
        LsppUtilGetSysTime (&(LsppEchoPktTxTime.u4TimeInSec),
                            &(LsppEchoPktTxTime.u4TimeInMicroSec));

        pLsppPduInfo->LsppHeader.u4TimeStampSentInSec =
            LsppEchoPktTxTime.u4TimeInSec;

        pLsppPduInfo->LsppHeader.u4TimeStampSentInMicroSec =
            LsppEchoPktTxTime.u4TimeInMicroSec;
    }

    /* Frame LSP Ping Header */
    LsppTxFrameHeader (pMsg, pu4BufOffset, &(pLsppPduInfo->LsppHeader));

    /* Framing Ping Payload with Target FEC stack TLV. */
    if (pLsppPduInfo->u2TlvsPresent & LSPP_TARGET_FEC_TLV)
    {
        LSPP_CRU_PUT_2BYTE (pMsg, pu4BufOffset, LSPP_TLV_TYPE_TARGET_FEC_STACK);

        LSPP_CRU_PUT_2BYTE (pMsg, pu4BufOffset,
                            pLsppPduInfo->u2FecStackTlvLength);

        while (u1FecStackDepth < pLsppPduInfo->u1FecStackDepth)
        {
            /* Filling the Type Field */
            LSPP_CRU_PUT_2BYTE (pMsg, pu4BufOffset,
                                pLsppPduInfo->LsppFecTlv[u1FecStackDepth].
                                u2TlvType);

            switch (pLsppPduInfo->LsppFecTlv[u1FecStackDepth].u2TlvType)
            {
                case LSPP_FEC_LDP_IPV4:

                    LSPP_CRU_PUT_2BYTE (pMsg, pu4BufOffset,
                                        LSPP_LDP_IPV4_TLV_LEN);

                    LsppTxFrameLdpIpV4Fec (pMsg, pu4BufOffset,
                                           LSPP_LDP_FEC_TLV_BASEADDR
                                           (u1FecStackDepth), u4ContextId);
                    break;

                case LSPP_FEC_LDP_IPV6:

                    LSPP_CRU_PUT_2BYTE (pMsg, pu4BufOffset,
                                        LSPP_LDP_IPV6_TLV_LEN);

                    LsppTxFrameLdpIpV6Fec (pMsg, pu4BufOffset,
                                           LSPP_LDP_FEC_TLV_BASEADDR
                                           (u1FecStackDepth), u4ContextId);
                    break;

                case LSPP_FEC_RSVP_IPV4:

                    LSPP_CRU_PUT_2BYTE (pMsg, pu4BufOffset,
                                        LSPP_RSVP_IPV4_TLV_LEN);

                    LsppTxFrameRsvpIpV4Fec (pMsg, pu4BufOffset,
                                            LSPP_RSVP_FEC_TLV_BASEADDR
                                            (u1FecStackDepth), u4ContextId);
                    break;

                case LSPP_FEC_RSVP_IPV6:

                    LSPP_CRU_PUT_2BYTE (pMsg, pu4BufOffset,
                                        LSPP_RSVP_IPV6_TLV_LEN);

                    LsppTxFrameRsvpIpV6Fec (pMsg, pu4BufOffset,
                                            LSPP_RSVP_FEC_TLV_BASEADDR
                                            (u1FecStackDepth), u4ContextId);
                    break;

                case LSPP_FEC_128_DEPRECATED_PWFEC:

                    LSPP_CRU_PUT_2BYTE (pMsg, pu4BufOffset,
                                        LSPP_FEC128_PW_DEPREC_TLV_LEN);

                    LsppTxFrameFec128DeprecatePwFec (pMsg, pu4BufOffset,
                                                     LSPP_DEPREC_PW_TLV_BASEADDR
                                                     (u1FecStackDepth),
                                                     u4ContextId);
                    break;

                case LSPP_FEC_128_PWFEC:

                    LSPP_CRU_PUT_2BYTE (pMsg, pu4BufOffset,
                                        LSPP_FEC128_PW_TLV_LEN);

                    LsppTxFrameFec128Pw (pMsg, pu4BufOffset,
                                         LSPP_FEC128_PW_TLV_BASEADDR
                                         (u1FecStackDepth), u4ContextId);
                    break;

                case LSPP_FEC_129_PWFEC:

                    u2TlvLength =
                        (UINT1) (LSPP_FEC129_PW_TLV_LEN (u1FecStackDepth));

                    /* Calculating the pad bytes count */
                    u1SkipCount = (UINT1) ((LSPP_BYTE_PAD_MAX_LEN -
                                            (u2TlvLength %
                                             LSPP_BYTE_PAD_MAX_LEN)) %
                                           LSPP_BYTE_PAD_MAX_LEN);

                    LSPP_CRU_PUT_2BYTE (pMsg, pu4BufOffset, u2TlvLength);

                    LsppTxFrameFec129Pw (pMsg, pu4BufOffset,
                                         LSPP_FEC129_PW_TLV_BASEADDR
                                         (u1FecStackDepth), u1SkipCount,
                                         u4ContextId);
                    break;

                case LSPP_FEC_NIL:

                    LSPP_CRU_PUT_2BYTE (pMsg, pu4BufOffset, LSPP_NIL_TLV_LEN);

                    LsppTxFrameNilFec (pMsg, pu4BufOffset,
                                       LSPP_NIL_FEC_TLV_BASEADDR
                                       (u1FecStackDepth), u4ContextId);
                    break;

                case LSPP_FEC_STATIC_LSP:

                    LSPP_CRU_PUT_2BYTE (pMsg, pu4BufOffset,
                                        LSPP_STATIC_LSP_TLV_LEN);

                    LsppTxFrameStaticLspFec (pMsg, pu4BufOffset,
                                             LSPP_STATIC_LSP_FEC_TLV_BASEADDR
                                             (u1FecStackDepth), u4ContextId);
                    break;

                case LSPP_FEC_STATIC_PW:

                    u2TlvLength =
                        (UINT1) (LSPP_STATIC_PW_TLV_LEN (u1FecStackDepth));

                    /* Calculating the pad bytes count */
                    u1SkipCount = (UINT1) ((LSPP_BYTE_PAD_MAX_LEN -
                                            (u2TlvLength %
                                             LSPP_BYTE_PAD_MAX_LEN))
                                           % LSPP_BYTE_PAD_MAX_LEN);

                    LSPP_CRU_PUT_2BYTE (pMsg, pu4BufOffset, u2TlvLength);

                    LsppTxFrameStaticPwFec (pMsg, pu4BufOffset,
                                            LSPP_STATIC_PW_FEC_TLV_BASEADDR
                                            (u1FecStackDepth), u1SkipCount,
                                            u4ContextId);
                    break;

                default:

                    LSPP_LOG (u4ContextId,
                              LSPP_INVALID_TLV_SUBTYPE_TO_FRAME,
                              "LsppTxEncapsulatePdu");
                    return OSIX_FAILURE;
            }
            u1FecStackDepth++;
        }
    }

    /* Framing Payload with DSMAP TLV. */
    if (pLsppPduInfo->u2TlvsPresent & LSPP_DSMAP_TLV)
    {
        LSPP_CRU_PUT_2BYTE (pMsg, pu4BufOffset, LSPP_TLV_TYPE_DSMAP);

        LSPP_CRU_PUT_2BYTE (pMsg, pu4BufOffset,
                            pLsppPduInfo->LsppDsMapTlv.u2Length);

        LsppTxFrameDsmapTlv (pMsg, pu4BufOffset, &(pLsppPduInfo->LsppDsMapTlv),
                             u4ContextId);
    }

    /* Framing Payload with Interface and Label stack TLV. */
    if (pLsppPduInfo->u2TlvsPresent & LSPP_INTERFACE_AND_LBL_STACK_TLV)
    {
        LSPP_CRU_PUT_2BYTE (pMsg, pu4BufOffset, LSPP_TLV_TYPE_IF_LBL_STACK);

        LSPP_CRU_PUT_2BYTE (pMsg, pu4BufOffset,
                            pLsppPduInfo->LsppIfLblStkTlv.u2Length);

        LsppTxFrameIfLblStkTlv (pMsg, pu4BufOffset,
                                LSPP_IF_LBL_STK_TLV_BASEADDR, u4ContextId);
    }

    /* Framing Payload with BDF Dicriminator TLV. */
    if (pLsppPduInfo->u2TlvsPresent & LSPP_BFD_DISCRIMINATOR_TLV)
    {
        LSPP_CRU_PUT_2BYTE (pMsg, pu4BufOffset,
                            LSPP_TLV_TYPE_BFD_DISCRIMINATOR);

        LSPP_CRU_PUT_2BYTE (pMsg, pu4BufOffset, LSPP_BFD_DISC_TLV_LEN);

        LsppTxFrameBfdDiscTlv (pMsg, pu4BufOffset, LSPP_BFD_FEC_TLV_BASEADDR,
                               u4ContextId);
    }

    if ((pLsppPduInfo->LsppHeader.u1MessageType == LSPP_ECHO_REQUEST) &&
        (pLsppPduInfo->u2TlvsPresent & LSPP_REPLY_TOS_BYTE_TLV))
    {
        LSPP_CRU_PUT_2BYTE (pMsg, pu4BufOffset, LSPP_TLV_TYPE_REPLY_TOS_BYTE);

        LSPP_CRU_PUT_2BYTE (pMsg, pu4BufOffset, LSPP_REPLY_TOS_TLV_LEN);

        LsppTxFrameReplyTosTlv (pMsg, pu4BufOffset,
                                LSPP_REPLY_TOS_TLV_BASEADDR, u4ContextId);
    }

    /*  Framing Payload with Errored Tlv. */
    if (pLsppPduInfo->u2TlvsPresent & LSPP_ERRORED_TLV)
    {
        LSPP_CRU_PUT_2BYTE (pMsg, pu4BufOffset, LSPP_TLV_TYPE_ERRORED);

        LSPP_CRU_PUT_2BYTE (pMsg, pu4BufOffset, LSPP_ERRORED_TLV_LEN);

        /* Framing the Errored tlv */
        LSPP_CRU_PUT_NBYTE (pMsg, pLsppPduInfo->LsppErrorTlv.au1Value,
                            pu4BufOffset, LSPP_ERRORED_TLV_LEN);
    }

    /*  Framing Payload with Pad Tlv. */
    if (pLsppPduInfo->u2TlvsPresent & LSPP_PAD_TLV)
    {
        LSPP_CRU_PUT_2BYTE (pMsg, pu4BufOffset, LSPP_TLV_TYPE_PAD);

        LSPP_CRU_PUT_2BYTE (pMsg, pu4BufOffset, LSPP_PAD_TLV_LEN);

        LsppTxFramePadTlv (pMsg, pu4BufOffset, LSPP_PAD_TLV_BASEADDR,
                           LSPP_PAD_TLV_LEN, u4ContextId);
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppTxSendPacketToBfd
 * DESCRIPTION      : This function is used to sene the framed LSP Ping 
 *                    Payload to BFD
 * Input(s)         : pMsg         - Lsp Ping Message
 *                    pLsppEchoMsg - Pointer to Echo message pdu info strucutre
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 **************************************************************************/
PUBLIC UINT4
LsppTxSendPacketToBfd (tCRU_BUF_CHAIN_HEADER * pMsg,
                       tLsppEchoMsg * pLsppEchoMsg, UINT1 u1ReplyPath)
{
    tLsppExtInParams   *pLsppExtInParams = NULL;
    tLsppExtOutParams  *pLsppExtOutParams = NULL;

    if ((pLsppExtInParams = (tLsppExtInParams *)
         MemAllocMemBlk (LSPP_FSLSPPINPUTPARAMS_POOLID)) == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((pLsppExtOutParams = (tLsppExtOutParams *)
         MemAllocMemBlk (LSPP_FSLSPPOUTPUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        return OSIX_FAILURE;
    }

    MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
    MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));

    pLsppExtInParams->u4RequestType = LSPP_BFD_PROCESS_BTSTRAP_INFO;
    pLsppExtInParams->u4ContextId = pLsppEchoMsg->u4ContextId;

    pLsppExtInParams->InBfdInfo.u4BfdDiscriminator =
        pLsppEchoMsg->LsppPduInfo.LsppBfdDiscTlv.u4Discriminator;

    pLsppExtInParams->InBfdInfo.bIsEgress = OSIX_TRUE;

    pLsppExtInParams->InBfdInfo.u1ReplyPath = u1ReplyPath;

    MEMCPY (&(pLsppExtInParams->InBfdInfo.PathId), &(pLsppEchoMsg->LsppPathId),
            sizeof (tLsppPathId));

    if (pLsppExtInParams->InBfdInfo.u1ReplyPath != LSPP_NO_REPLY)
    {
        pLsppEchoMsg->u4BfdTlvOffset = (pLsppEchoMsg->u4BfdTlvOffset +
                                        pLsppEchoMsg->u4EncapHdrLen);

        pLsppExtInParams->InBfdInfo.u4OffSet = pLsppEchoMsg->u4BfdTlvOffset;
        pLsppExtInParams->InBfdInfo.pBuf = pMsg;

        if (pLsppExtInParams->InBfdInfo.u1ReplyPath == LSPP_REPLY_VIA_IP_PATH)
        {
            pLsppExtInParams->InBfdInfo.DestIpAddr.u4_addr[0] =
                pLsppEchoMsg->LsppIpHeader.u4Dest;

            pLsppExtInParams->InBfdInfo.u4SrcUdpPort =
                pLsppEchoMsg->LsppUdpHeader.u2Dest_u_port;
        }
        else if (pLsppExtInParams->InBfdInfo.u1ReplyPath == LSPP_REPLY_VIA_LSP)
        {
            MEMCPY (pLsppExtInParams->InBfdInfo.au1NextHopMac,
                    pLsppEchoMsg->pOutSegInfo->au1NextHopMac, MAC_ADDR_LEN);

            pLsppExtInParams->InBfdInfo.u4OutIfIndex =
                pLsppEchoMsg->pOutSegInfo->u4OutIf;
            pLsppExtInParams->InBfdInfo.NextHopIpAddrType =
                pLsppEchoMsg->pOutSegInfo->u1NHAddrType;
        }
    }

    /* Call the LSPP exit function to Tx to packet to MPLSRTR */
    if (LsppPortHandleExtInteraction
        (pLsppExtInParams, pLsppExtOutParams) != OSIX_SUCCESS)
    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId,
                  LSPP_FETCH_FROM_EXT_MOD_FAILED, "LsppTxSendPacketToBfd");
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);
        return OSIX_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtInParams);
    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtOutParams);

    LSPP_LOG (pLsppEchoMsg->u4ContextId,
              LSPP_BFD_DISCRIMINATOR_FROM_REMOTE,
              "LsppTxSendPacketToBfd: ",
              pLsppEchoMsg->u4ContextId,
              pLsppEchoMsg->LsppPduInfo.LsppBfdDiscTlv.u4Discriminator);

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppTxBfdBtStrapReplyPkt
 * DESCRIPTION      : This function is to send the BFD Bootstrap reply 
 * Input(s)         : u4ContextId - Context Id
 *                    pLsppBfdInfo - Bfd Information 
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 **************************************************************************/
PUBLIC INT4
LsppTxBfdBtStrapReplyPkt (UINT4 u4ContextId, tLsppBfdInfo * pLsppBfdInfo)
{
    tLsppOutSegInfo     OutSegInfo;
    UINT4               u4OffSet = 0;
    INT4                i4Status = LSPP_SHUTDOWN;
    UINT1               u1DscpVal = 0;

    MEMSET (&OutSegInfo, 0, sizeof (tLsppOutSegInfo));

    if (LsppCoreGetModuleStatus (u4ContextId, &i4Status) != OSIX_SUCCESS)
    {
        LSPP_CMN_TRC (u4ContextId, LSPP_CONTEXT_ID_INVALID,
                      "LsppTxBfdBtStrapReplyPkt", u4ContextId);
        return OSIX_FAILURE;
    }

    /*Check whether the module is enabled on the received context. */
    if (i4Status == LSPP_SHUTDOWN)
    {
        LSPP_LOG (u4ContextId, LSPP_MODULE_STATUS_DOWN_IN_CXT,
                  "LsppTxBfdBtStrapReplyPkt", u4ContextId);
        return OSIX_FAILURE;
    }

    u4OffSet = pLsppBfdInfo->u4OffSet + LSPP_TLV_HEADER_LEN;

    if (pLsppBfdInfo->pBuf == NULL)
    {
        return OSIX_FAILURE;
    }

    /* Filling the BFD Discriminator Value */
    LSPP_CRU_PUT_4BYTE (pLsppBfdInfo->pBuf,
                        &u4OffSet, pLsppBfdInfo->u4BfdDiscriminator);

    if (pLsppBfdInfo->u1ReplyPath == LSPP_REPLY_VIA_IP_PATH)
    {
        if (LsppSockTransmitEchoRplyPkt (u4ContextId,
                                         pLsppBfdInfo->pBuf,
                                         pLsppBfdInfo->DestIpAddr.
                                         u4_addr[0],
                                         &u1DscpVal,
                                         pLsppBfdInfo->u4SrcUdpPort,
                                         0) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        LSPP_RELEASE_CRU_BUF (pLsppBfdInfo->pBuf, FALSE);
    }
    else
    {
        OutSegInfo.u1NHAddrType = pLsppBfdInfo->NextHopIpAddrType;

        if (pLsppBfdInfo->NextHopIpAddrType == LSPP_MPLS_ADDR_TYPE_IPV4)
        {
            OutSegInfo.NHAddr.u4_addr[0] = pLsppBfdInfo->DestIpAddr.u4_addr[0];
        }
        else
        {
            MEMCPY (&(OutSegInfo.NHAddr.u1_addr),
                    &(pLsppBfdInfo->DestIpAddr.u1_addr), LSPP_IPV6_ADDR_LEN);
        }

        OutSegInfo.u4OutIf = pLsppBfdInfo->u4OutIfIndex;

        MEMCPY (&(OutSegInfo.au1NextHopMac), &(pLsppBfdInfo->au1NextHopMac),
                MAC_ADDR_LEN);

        if (LsppTxEchoPkt (pLsppBfdInfo->pBuf, u4ContextId, &OutSegInfo) !=
            OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
    }

    LSPP_LOG (u4ContextId,
              LSPP_BFD_LOCAL_DISCRIMINATOR_FOR_REPLY,
              "LsppTxBfdBtStrapReplyPkt: ",
              u4ContextId, pLsppBfdInfo->u4BfdDiscriminator);

    /* Increment the reply transmitted counter. */
    LsppCoreUpdateGlobalStats (u4ContextId, LSPP_REPLY_TX, NULL);
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppTxGetReplyPath
 * DESCRIPTION      : This function is Fetch reply path based on the reply mode
 * Input(s)         : pLsppEchoMsg - Pointer to Echo message pdu info strucutre
 *                    pu1DscpVal   - Pointer to store the Dscp Value
 *                    pbRouterAlertOpt - This specifies whether to add
 *                    Router alert option
 * OUTPUT           : pu1DscpVal - Updated Dscp Value
 *                    pbRouterAlertOpt - Updated Router Alert option presence 
 *                    flag
 *                    Reply Path - Ip/LSP
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 **************************************************************************/
UINT1
LsppTxGetReplyPath (tLsppEchoMsg * pLsppEchoMsg, UINT1 *pu1DscpValue,
                    BOOL1 * pbRouterAlertOpt)
{
    /* Router Alert Option is by default false. The router alert option
     * is only set when the reply mode is LSPP_REPLY_IP_UDP_ROUTER_ALERT */
    *pbRouterAlertOpt = OSIX_FALSE;

    if (pLsppEchoMsg->LsppPduInfo.u2TlvsPresent & LSPP_REPLY_TOS_BYTE_TLV)
    {
        *pu1DscpValue = pLsppEchoMsg->LsppPduInfo.LsppReplyTosTlv.u1TosValue;
    }

    if (pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReplyMode == LSPP_REPLY_IP_UDP)
    {
        return LSPP_REPLY_VIA_IP_PATH;
    }
    else if (pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReplyMode ==
             LSPP_REPLY_IP_UDP_ROUTER_ALERT)
    {
        if (pLsppEchoMsg->u1IsRouteExists == OSIX_FALSE)
        {
            return LSPP_REPLY_VIA_LSP;
        }
        else
        {
            *pbRouterAlertOpt = OSIX_TRUE;
            return LSPP_REPLY_VIA_IP_PATH;

        }
    }
    else if (pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReplyMode ==
             LSPP_REPLY_APPLICATION_CC)
    {
        if (pLsppEchoMsg->u1IsRouteExists == OSIX_TRUE)
        {
            return LSPP_REPLY_VIA_IP_PATH;
        }
        else
        {
            return LSPP_REPLY_VIA_LSP;
        }
    }

    return LSPP_NO_REPLY;
}

/***************************************************************************
 * FUNCTION NAME    : LsppTxFrameAndSendPdu
 * DESCRIPTION      : This function is used to frame and transmit 
 *                    the LSP Ping Pdu
 * Input(s)         : pLsppEchoMsg - Pointer to Echo message pdu info strucutre
 *                    pLsppPingTraceEntry - Pointer to Ping trace Entry
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 **************************************************************************/
PUBLIC UINT4
LsppTxFrameAndSendPdu (tLsppEchoMsg * pLsppEchoMsg,
                       tLsppFsLsppPingTraceTableEntry * pLsppPingTraceEntry,
                       tLsppFsLsppEchoSequenceTableEntry * pEchoSequenceEntry,
                       tCRU_BUF_CHAIN_HEADER * pMsg)
{
    tCRU_BUF_CHAIN_HEADER *pPkt = NULL;
    UINT4               u4BufOffset = 0;
    UINT4               u4RepeatCount = 0;
    UINT4               u4TimeInSec = 0;
    UINT4               u4TimeInMilliSec = 0;
    UINT4               u4MaxReqsToSend = 0;
    UINT1               u1DscpVal = 0;
    UINT1               u1ReplyPath = 0;
    BOOL1               bIsWTSTimerStarted = OSIX_FALSE;
    BOOL1               bIsWFRTmrNeedToStart = OSIX_TRUE;
    BOOL1               bRouterAlertOpt = OSIX_FALSE;

    if ((pLsppEchoMsg->LsppPduInfo.LsppHeader.u1MessageType ==
         LSPP_ECHO_REQUEST) &&
        ((pLsppPingTraceEntry == NULL) || (pEchoSequenceEntry == NULL)))
    {
        return OSIX_FAILURE;
    }

    if (LsppTxFramePingPayload (pMsg, &u4BufOffset,
                                &(pLsppEchoMsg->LsppPduInfo),
                                pLsppEchoMsg->u4ContextId) != OSIX_SUCCESS)
    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId, LSPP_FRAMING_PING_PAYLOAD_FAILED,
                  "LsppTxFrameAndSendPdu");
        return OSIX_FAILURE;
    }

    /* If BFD TLV is present then send the CRU BUF and BFD packet offset to 
     * BFD module along with path type */
    if (pLsppEchoMsg->LsppPduInfo.LsppHeader.u1MessageType == LSPP_ECHO_REPLY)
    {
        u1ReplyPath = LsppTxGetReplyPath (pLsppEchoMsg, &u1DscpVal,
                                          &bRouterAlertOpt);
    }

    if ((pLsppEchoMsg->LsppPduInfo.LsppHeader.u1MessageType ==
         LSPP_ECHO_REQUEST) ||
        ((pLsppEchoMsg->LsppPduInfo.LsppHeader.u1MessageType ==
          LSPP_ECHO_REPLY) && (u1ReplyPath != LSPP_REPLY_VIA_IP_PATH)))
    {
        if (LsppTxEncapsulatePdu (pMsg, pLsppEchoMsg) != OSIX_SUCCESS)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId, LSPP_ENCAP_PDU_FAILED,
                      "LsppTxFrameAndSendPdu");
            return OSIX_FAILURE;
        }
    }

    if ((pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReplyMode ==
         LSPP_REPLY_IP_UDP_ROUTER_ALERT) &&
        (pLsppEchoMsg->u1IsRouteExists == OSIX_TRUE)
        && (pLsppEchoMsg->LsppPduInfo.LsppHeader.u1MessageType ==
            LSPP_ECHO_REPLY))
    {
        /* In this case updating Ip/UDP header alone as 
         * we have to send the packet in raw socket. As
         * we are set IP Header option in Raw socket,
         * Framing IP/UDP Header here */
        if (LsppTxFillIpUdpHdr (pMsg, pLsppEchoMsg) != OSIX_SUCCESS)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_UPDATING_IP_UDP_HDR_FAILED, "LsppTxEncapsulatePdu");
            return OSIX_FAILURE;
        }
    }

    if ((pLsppEchoMsg->LsppPduInfo.LsppHeader.u1MessageType ==
         LSPP_ECHO_REPLY) &&
        (pLsppEchoMsg->LsppPduInfo.u2TlvsPresent & LSPP_BFD_DISCRIMINATOR_TLV))
    {
        LsppTrcCruBufDump (pLsppEchoMsg->u4ContextId, pMsg);

        if (LsppTxSendPacketToBfd (pMsg, pLsppEchoMsg, u1ReplyPath) !=
            OSIX_SUCCESS)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_SENDING_PKT_TO_BFD_FAILED, "LsppTxFrameAndSendPdu");
            return OSIX_FAILURE;
        }
        return OSIX_SUCCESS;
    }

    if ((pLsppEchoMsg->LsppPduInfo.LsppHeader.u1MessageType ==
         LSPP_ECHO_REPLY) && (u1ReplyPath == LSPP_REPLY_VIA_IP_PATH))
    {
        LsppTrcCruBufDump (pLsppEchoMsg->u4ContextId, pMsg);

        if (LsppSockTransmitEchoRplyPkt (pLsppEchoMsg->u4ContextId,
                                         pMsg,
                                         pLsppEchoMsg->LsppIpHeader.u4Dest,
                                         &u1DscpVal,
                                         pLsppEchoMsg->LsppUdpHeader.
                                         u2Dest_u_port, bRouterAlertOpt) !=
            OSIX_SUCCESS)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_SENDING_PKT_ON_SOCK_FAILED, "LsppTxFrameAndSendPdu");
            return OSIX_FAILURE;
        }

        LSPP_RELEASE_CRU_BUF (pMsg, 0);

        /* Increment the reply transmitted counter. */
        LsppCoreUpdateGlobalStats (pLsppEchoMsg->u4ContextId,
                                   LSPP_REPLY_TX, NULL);
        return OSIX_SUCCESS;
    }

    if (pLsppEchoMsg->LsppPduInfo.LsppHeader.u1MessageType == LSPP_ECHO_REPLY)
    {
        LsppTrcCruBufDump (pLsppEchoMsg->u4ContextId, pMsg);

        if (LsppTxEchoPkt (pMsg, pLsppEchoMsg->u4ContextId,
                           pLsppEchoMsg->pOutSegInfo) != OSIX_SUCCESS)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId, LSPP_TRANSMIT_PDU_FAILED,
                      "LsppTxFrameAndSendPdu");
            return OSIX_FAILURE;
        }

        /* Increment the reply transmitted counter. */
        LsppCoreUpdateGlobalStats (pLsppEchoMsg->u4ContextId,
                                   LSPP_REPLY_TX, NULL);
    }
    else if (pLsppEchoMsg->LsppPduInfo.LsppHeader.u1MessageType ==
             LSPP_ECHO_REQUEST)
    {
        /* Get the repeat count for various options. */
        if (pLsppPingTraceEntry->MibObject.i4FsLsppSameSeqNumOption ==
            LSPP_SNMP_TRUE)
        {
            u4RepeatCount = pLsppPingTraceEntry->MibObject.u4FsLsppRepeatCount;
        }
        else if (pLsppPingTraceEntry->MibObject.i4FsLsppBurstOption ==
                 LSPP_SNMP_TRUE)
        {
            u4RepeatCount = pLsppPingTraceEntry->MibObject.u4FsLsppBurstSize;
        }
        else
        {
            u4RepeatCount = 1;
        }

        /* Getting Max Request to send */
        LsppUtilGetMaxReqsToSend (pLsppPingTraceEntry, &u4MaxReqsToSend);

        /* Send the configured number of packets when the option is 
         * Burst or SameSeqNum.
         */
        while (u4RepeatCount > 0)
        {
            /* Get a copy of a buffer to send. */
            pPkt = LsppUtilDupBuf (pMsg);
            if (pPkt == NULL)
            {
                LSPP_CMN_TRC (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
                              LSPP_MEM_ALLOC_FAIL, "LsppTxFrameAndSendPdu");
                return OSIX_FAILURE;
            }

            LsppTrcCruBufDump (pLsppEchoMsg->u4ContextId, pMsg);

            if (LsppTxEchoPkt (pPkt, pLsppEchoMsg->u4ContextId,
                               pLsppEchoMsg->pOutSegInfo) != OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppEchoMsg->u4ContextId, LSPP_TRANSMIT_PDU_FAILED,
                          "LsppTxFrameAndSendPdu");
                LSPP_RELEASE_CRU_BUF (pPkt, 0);
                return OSIX_FAILURE;
            }

            if (pLsppPingTraceEntry->MibObject.i4FsLsppReplyMode !=
                LSPP_NO_REPLY)
            {
                pEchoSequenceEntry->u1Status = LSPP_ECHO_SEQ_IN_PROGRESS;
            }
            else
            {
                pEchoSequenceEntry->u1Status = LSPP_ECHO_SEQ_COMPLETED;
            }

            /* Incrementing Gloabl Stats */
            LsppCoreUpdateGlobalStats (pLsppEchoMsg->u4ContextId,
                                       LSPP_REQ_TX, NULL);

            /* Incrementing Echo Instance Tx Count */
            (pLsppPingTraceEntry->MibObject.u4FsLsppPktsTx)++;

            if (bIsWTSTimerStarted == OSIX_FALSE)
            {
                if ((pLsppPingTraceEntry->MibObject.i4FsLsppRequestType ==
                     LSPP_REQ_TYPE_PING))
                {
                    if ((pLsppPingTraceEntry->MibObject.
                         i4FsLsppSameSeqNumOption != LSPP_SNMP_TRUE) &&
                        ((pLsppPingTraceEntry->MibObject.u4FsLsppPktsTx +
                          pLsppPingTraceEntry->MibObject.u4FsLsppPktsUnSent) <
                         u4MaxReqsToSend))
                    {
                        /*Start Wait to send timer */
                        LsppUtilCalcTimeToStartTmr
                            (pLsppPingTraceEntry->MibObject.i4FsLsppWTSTmrUnit,
                             pLsppPingTraceEntry->MibObject.u4FsLsppWTSInterval,
                             &u4TimeInSec, &u4TimeInMilliSec);

                        if ((u4TimeInSec) != 0 || (u4TimeInMilliSec != 0))
                        {
                            LsppTmrStartTmr (&(pLsppPingTraceEntry->WTSTimer),
                                             LSPP_WTS_TMR, u4TimeInSec,
                                             u4TimeInMilliSec);
		    	    u4TimeInSec = 0;
			    u4TimeInMilliSec = 0;

                            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                                      LSPP_WTS_TIMER_STARTED,
                                      "LsppTxFrameAndSendPdu");
                        }
                    }
                }
                bIsWTSTimerStarted = OSIX_TRUE;
            }

            if (bIsWFRTmrNeedToStart == OSIX_TRUE)
            {
                if (((pLsppPingTraceEntry->MibObject.i4FsLsppReplyMode !=
                      LSPP_NO_REPLY) &&
                     (pLsppPingTraceEntry->MibObject.i4FsLsppRequestType ==
                      LSPP_REQ_TYPE_PING)) ||
                    (pLsppPingTraceEntry->MibObject.i4FsLsppRequestType ==
                     LSPP_REQ_TYPE_TRACE_ROUTE))
                {
                    /*Start Wait for reply timer */
                    LsppUtilCalcTimeToStartTmr (pLsppPingTraceEntry->MibObject.
                                                i4FsLsppWFRTmrUnit,
                                                pLsppPingTraceEntry->MibObject.
                                                u4FsLsppWFRInterval,
                                                &u4TimeInSec,
                                                &u4TimeInMilliSec);

                    if ((u4TimeInSec) != 0 || (u4TimeInMilliSec != 0))
                    {
                        LsppTmrStartTmr (&(pEchoSequenceEntry->WFRTimer),
                                         LSPP_WFR_TMR, u4TimeInSec,
                                         u4TimeInMilliSec);

                        LSPP_LOG (pLsppEchoMsg->u4ContextId,
                                  LSPP_WFR_TIMER_STARTED,
                                  "LsppTxFrameAndSendPdu");
                    }
                }
                if (pLsppPingTraceEntry->MibObject.i4FsLsppSameSeqNumOption ==
                    LSPP_SNMP_TRUE)
                {
                    bIsWFRTmrNeedToStart = OSIX_FALSE;
                }
            }

            u4RepeatCount--;

            if (u4RepeatCount == 0)
            {
                break;
            }

            if (pLsppPingTraceEntry->MibObject.i4FsLsppBurstOption ==
                LSPP_SNMP_TRUE)
            {
                /* If Burst Option is enabled, then Repeat count of 
                 * packets must be sent at a stretch by incrementing 
                 * sequecne number by 1. This fucntion is called to 
                 * modify the pMsg buffer to update the pMsg with 
                 * next sequence number */

                pEchoSequenceEntry = LsppDbCreateEchoSeqNode
                    (pLsppPingTraceEntry);

                if (pEchoSequenceEntry == NULL)
                {
                    LSPP_LOG (pLsppEchoMsg->u4ContextId,
                              LSPP_ECHO_SEQ_NODE_CREATE_FAILED,
                              "LsppTxFrameAndSendPdu");
                    return OSIX_FAILURE;
                }

                LsppTxModifyEchoPkt (pMsg, pLsppEchoMsg,
                                     (pLsppPingTraceEntry->
                                      u4LastGeneratedSeqNum));
            }
        }

        /* Release the Msg buffer. */
        LSPP_RELEASE_CRU_BUF (pMsg, 0);

        if ((pLsppPingTraceEntry->MibObject.i4FsLsppRequestType ==
             LSPP_REQ_TYPE_PING) &&
            (pLsppPingTraceEntry->MibObject.i4FsLsppReplyMode ==
             LSPP_NO_REPLY) &&
            ((pLsppPingTraceEntry->MibObject.u4FsLsppPktsTx +
              pLsppPingTraceEntry->MibObject.u4FsLsppPktsUnSent) ==
             u4MaxReqsToSend))
        {
            if (pLsppPingTraceEntry->MibObject.u4FsLsppPktsTx ==
                u4MaxReqsToSend)
            {
                pLsppPingTraceEntry->MibObject.i4FsLsppStatus =
                    LSPP_ECHO_SUCCESS;
            }
            else
            {
                pLsppPingTraceEntry->MibObject.i4FsLsppStatus =
                    LSPP_ECHO_FAILURE;
            }

            LSPP_LOG (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
                      LSPP_TRAP_PING_COMPLETED, pLsppPingTraceEntry->
                      MibObject.i4FsLsppStatus);

            LsppCoreHandleAgeOut (pLsppPingTraceEntry);
        }
    }

    return OSIX_SUCCESS;
}

#endif /* _LSPPTX_C_ */
