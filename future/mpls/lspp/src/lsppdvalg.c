/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: lsppdvalg.c,v 1.3 2010/11/23 04:53:22 siva Exp $
*
* Description: This file contains the routines to initialize the
*              mib objects for the module Lspp 
*********************************************************************/

#include "lsppinc.h"

/****************************************************************************
* Function    : LsppInitializeMibFsLsppPingTraceTable
* Input       : pLsppFsLsppPingTraceTableEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
LsppInitializeMibFsLsppPingTraceTable (tLsppFsLsppPingTraceTableEntry *
                                       pLsppFsLsppPingTraceTableEntry)
{

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle = 0;

    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRequestType = 0;

    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppPathType = 0;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipGlobalId = 0;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipNodeId = 0;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipIfNum = 0;

    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppReplyMode = 0;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppRepeatCount = 5;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppPacketSize = 200;

    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppPadPatternLen = 2;

    pLsppFsLsppPingTraceTableEntry->MibObject.au1FsLsppPadPattern[0] = 0xab;
    pLsppFsLsppPingTraceTableEntry->MibObject.au1FsLsppPadPattern[1] = 0xcd;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTTLValue = 0;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppWFRInterval = 2;

    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppWFRTmrUnit = 2;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppWTSInterval = 1;

    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppWTSTmrUnit = 2;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppReplyDscpValue = 0;

    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppSweepOption = 2;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepMinimum = 100;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepMaximum = 17986;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepIncrement = 100;

    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppBurstOption = 2;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppBurstSize = 3;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppEXPValue = 0;

    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppDsMap = 2;

    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppFecValidate = 1;

    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppReplyPadTlv = 2;

    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppForceExplicitNull = 2;

    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppInterfaceLabelTlv = 2;

    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppSameSeqNumOption = 2;

    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppVerbose = 2;

    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppReversePathVerify = 2;

    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppEncapType = 0;

    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRowStatus = 0;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId = 0;

    return OSIX_SUCCESS;
}
