/**************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lsppcore.c,v 1.46 2016/07/19 10:08:09 siva Exp $
 *
 * Description: This file contains the Lspp Core Protocol related routines
 *************************************************************************/
#ifndef _LSPPCORE_C_
#define _LSPPCORE_C_

#include "lsppinc.h"
#include "mplcmndb.h"

/*******************************************************************************
 * *                                                                           *
 * * Function     : LsppCoreFecLabelValidate                                   *
 * *                                                                           *
 * * Description  : This function performs label validation, label operation   *
 * *                check, Downstream Mapping Validation, Egress Processing and*
 * *                and FEC validation.                                        *
 * *                                                                           *
 * * Input        : pLsppEchoMsg - pointer to the structure containing the     *
 * *                received packet information.                               *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * *                                                                           *
 * ****************************************************************************/

PUBLIC INT4
LsppCoreFecLabelValidate (tLsppEchoMsg * pLsppEchoMsg)
{
    tLsppLblInfo       *pRxLblStackR = NULL;
    tLsppLblInfo       *pRxLblStackD = NULL;
    tLsppFecTlv        *pLsppFecTlv = NULL;
    UINT4               u4RxLabelL = 0;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT1               u1EntryExists = OSIX_FALSE;
    UINT1               u1LabelStackDepth = 0;
    UINT1               u1DsLblStackDepth = 0;
    UINT1               u1FecStackDepth = 0;
    UINT1               u1FecStatus = 0;
    UINT1               u1FecRetCode = 0;
    UINT1               u1LblIndex = 0;
    UINT1               u1LblAction = 0;
    UINT1               u1LblCount = 0;

    /* Get the Label Stack Depth from the received message. */
    u1LabelStackDepth = pLsppEchoMsg->u1LabelStackDepth;
    pLsppEchoMsg->u1LblSwapAtDepth = LSPP_INVALID_INDEX;

    /* As per section 4.4 in RFC 4379, the bottom of stack is considered 
     * to be stack-depth of 1. The bottom most label will be at the 
     * Label-Stack-Depth of the received label information array. 
     */

    /* Label Validation. */
    while (u1LabelStackDepth != 0)
    {
        /* The Label-L is set to the value extracted from the received label 
         * stack at Label-Stack-Depth which is the outermost label since the 
         * bottom most label is stack-depth of 1. 
         *
         * Hence the label validation is performed starting from the outermost 
         * label that is StackR[0] (received label information array).
         */
        pRxLblStackR = &(pLsppEchoMsg->LblInfo[u1LblIndex]);

        u4RxLabelL = pRxLblStackR->u4Label;

        i4RetVal = LsppCoreGetLabelInfo (pLsppEchoMsg->u4ContextId,
                                         pLsppEchoMsg->u4IfIndex,
                                         u4RxLabelL, &u1EntryExists,
                                         &u1LblAction);
        if (i4RetVal == OSIX_FAILURE)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId, LSPP_GET_LABEL_INFO_FAILED,
                      "LsppCoreFecLabelValidate");
        }

        if (u1EntryExists == OSIX_FALSE)
        {
            pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReturnCode =
                LSPP_NO_LBL_ENTRY_AT_STACK_DEPTH;
            pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReturnSubCode =
                u1LabelStackDepth;

            LSPP_LOG (pLsppEchoMsg->u4ContextId, LSPP_LABEL_ENTRY_NOT_FOUND,
                      "LsppCoreFecLabelValidate", u4RxLabelL,
                      u1LabelStackDepth);

            if (pLsppEchoMsg->LsppPduInfo.LsppHeader.u1MessageType ==
                LSPP_ECHO_REQUEST)
            {
                /* Send the echo reply as the label validation failed. */
                i4RetVal = LsppCoreSendEchoReply (pLsppEchoMsg);
                if (i4RetVal == OSIX_FAILURE)
                {
                    LSPP_LOG (pLsppEchoMsg->u4ContextId,
                              LSPP_SEND_ECHO_REPLY_FAILED,
                              "LsppCoreFecLabelValidate");

                    LsppCoreUpdateGlobalStats (pLsppEchoMsg->u4ContextId,
                                               LSPP_REPLY_UNSENT, NULL);
                }
                return OSIX_SUCCESS;
            }
            else
            {
                LSPP_LOG (pLsppEchoMsg->u4ContextId,
                          LSPP_LABEL_ENTRY_NOT_FOUND_FOR_ECHO_REPLY,
                          "LsppCoreFecLabelValidate");
                return OSIX_FAILURE;
            }
        }

        if (u1LblAction == LSPP_MPLS_LBL_ACTION_SWAP)
        {
            /* The action could be "Swap or Pop and Switch based 
             * on Popped Label. 
             */
            pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReturnCode =
                LSPP_LBL_SWITCHED_AT_STACK_DEPTH;
            pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReturnSubCode =
                u1LabelStackDepth;
            pLsppEchoMsg->u1LblSwapAtDepth = u1LblIndex;
            break;
        }
        else if (u1LblAction != LSPP_MPLS_LBL_ACTION_POP)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_LABEL_ACTION_INVALID,
                      "LsppCoreFecLabelValidate",
                      u4RxLabelL, u1LabelStackDepth);
        }

        u1LabelStackDepth--;
        u1LblIndex++;
    }

    if (u1LabelStackDepth == 0)
    {
        /* The LSR is the tail-end for the LSP. */
        pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReturnCode =
            LSPP_EGRESS_FOR_FEC_AT_STACK_DEPTH;
        pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReturnSubCode = 1;

        /* Egress Processing. */
        i4RetVal = LsppCoreEgressProcess (pLsppEchoMsg);
        if (i4RetVal == OSIX_FAILURE)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_EGRESS_PROCESSING_FAILED,
                      "LsppCoreFecLabelValidate");
            return OSIX_FAILURE;
        }
    }
    else
    {
        i4RetVal = OSIX_FAILURE;

        /* Check whether the Downstream Mapping TLV is present in the packet. */
        if (pLsppEchoMsg->LsppPduInfo.u2TlvsPresent & LSPP_DSMAP_TLV)
        {
            i4RetVal = LsppCoreVerifyDsmap (pLsppEchoMsg);
        }

        if ((pLsppEchoMsg->LsppPduInfo.LsppHeader.u2GlobalFlag &
             LSPP_FEC_VALIDATE) && (i4RetVal != OSIX_FAILURE))
        {

            /* Validate the Target FEC Stack in the received echo request 
             * when the validate flag is set.
             * 
             * First determine FEC-stack-depth from the Downstream Mapping TLV.
             * This is done by walking through Stack-D (the Downstream labels) 
             * from the bottom, decrementing the number of labels for each 
             * non-Implicit Null label, while incrementing FEC-stack-depth for 
             * each label. If the Downstream Mapping TLV contains one or more 
             * Implicit Null labels, FEC-stack-depth may be greater than 
             * Label-stack-depth. To be consistent with the above stack-depths, 
             * the bottom is considered to entry 1.
             */

            u1FecStackDepth = 0;
            u1LblCount = u1LabelStackDepth;
            u1LblIndex = 1;

            u1DsLblStackDepth =
                pLsppEchoMsg->LsppPduInfo.LsppDsMapTlv.u1DsLblStackDepth;

            while (u1LblCount > 0)
            {
                pRxLblStackD =
                    &(pLsppEchoMsg->LsppPduInfo.LsppDsMapTlv.
                      DsLblInfo[u1DsLblStackDepth - u1LblIndex]);

                u1FecStackDepth++;

                if (pRxLblStackD->u4Label != LSPP_LBL_IMPLICIT_NULL)
                {
                    u1LblCount--;
                }

                u1LblIndex++;
            }

            if (pLsppEchoMsg->LsppPduInfo.u1FecStackDepth >= u1FecStackDepth)
            {
                /* As per section 4.4 in RFC 4379, the bottom of stack is 
                 * considered to be stack-depth of 1. The corresponding FEC 
                 * is retrieved starting from the bottom most label for the 
                 * FEC stack depth. 
                 */
                pLsppFecTlv =
                    &(LSPP_TGT_FEC_STACK[LSPP_TGT_FEC_STACK_DEPTH -
                                         u1FecStackDepth]);

                /* Validate the FEC at the identifed FEC stack depth. */
                i4RetVal = LsppCoreFecValidate (pLsppEchoMsg->u4ContextId,
                                                pLsppFecTlv, u4RxLabelL,
                                                pLsppEchoMsg->u4IfIndex,
                                                &u1FecStatus, &u1FecRetCode);
                if (i4RetVal == OSIX_FAILURE)
                {
                    LSPP_LOG (pLsppEchoMsg->u4ContextId,
                              LSPP_VALIDATING_FEC_FAILED,
                              "LsppCoreFecLabelValidate");
                }

                if (u1FecStatus == LSPP_FEC_LBL_MAP_IMPLICIT_NULL)
                {
                    pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReturnCode =
                        LSPP_MAP_FAIL_FEC_TO_LBL_AT_STACK_DEPTH;
                }
                else if (u1FecStatus == LSPP_FEC_VALIDATION_FAILED)
                {
                    pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReturnCode =
                        u1FecRetCode;
                }
                pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReturnSubCode =
                    u1FecStackDepth;
            }
        }

        i4RetVal = LsppCoreSendEchoReply (pLsppEchoMsg);
        if (i4RetVal == OSIX_FAILURE)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_SEND_ECHO_REPLY_FAILED, "LsppCoreFecLabelValidate");
            LsppCoreUpdateGlobalStats (pLsppEchoMsg->u4ContextId,
                                       LSPP_REPLY_UNSENT, NULL);
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/*******************************************************************************
 * *                                                                           *
 * * Function     : LsppCoreVerifyDsmap                                        *
 * *                                                                           *
 * * Description  : This function verfies the Downstream Mapping information   *
 * *                against the received interface and Stack-R.                *
 * *                                                                           *
 * * Input        : pLsppEchoMsg - pointer to the structure containing the     *
 * *                received packet information.                               *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * *                                                                           *
 * ****************************************************************************/

PUBLIC INT4
LsppCoreVerifyDsmap (tLsppEchoMsg * pLsppEchoMsg)
{
    tLsppExtInParams   *pLsppExtInParams = NULL;
    tLsppExtOutParams  *pLsppExtOutParams = NULL;
    tLsppDsMapTlv      *pLsppDsMapTlv = NULL;
    UINT1              *pu1ReturnCode = NULL;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT4               u4IpAddr = 0;
    UINT1               u1IntfValidate = OSIX_FALSE;
    UINT1               u1StackDValidate = OSIX_FALSE;
    UINT1               u1RxLblIndex = 0;
    UINT1               u1DsLblIndex = 0;

    if ((pLsppExtInParams = (tLsppExtInParams *)
         MemAllocMemBlk (LSPP_FSLSPPINPUTPARAMS_POOLID)) == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((pLsppExtOutParams = (tLsppExtOutParams *)
         MemAllocMemBlk (LSPP_FSLSPPOUTPUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        return OSIX_FAILURE;
    }

    MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
    MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));

    pLsppDsMapTlv = &(pLsppEchoMsg->LsppPduInfo.LsppDsMapTlv);

    /* Check whether the initiator has requested for the Interface and 
     * Label Stack TLV in the reply message. If requested, set the 
     * corresponding flag so that the TLV will be included in the reply 
     * message.
     */
    if (pLsppDsMapTlv->u1DsFlags & LSPP_INTERFACE_AND_LBL_STACK_REQ)
    {
        pLsppEchoMsg->LsppPduInfo.u2TlvsPresent |=
            LSPP_INTERFACE_AND_LBL_STACK_TLV;
    }

    pu1ReturnCode = &(pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReturnCode);

    switch (pLsppDsMapTlv->u1AddrType)
    {
        case LSPP_IPV4_NUMBERED:
        case LSPP_IPV6_NUMBERED:
        {
            u1IntfValidate = OSIX_TRUE;
            u1StackDValidate = OSIX_TRUE;
            break;
        }
        case LSPP_IPV4_UNNUMBERED:
        {
            if (pLsppDsMapTlv->DsIpAddr.u4_addr[0] == LSPP_IP_LOOPBACK_ADDRESS)
            {
                *pu1ReturnCode = LSPP_UPSTREAM_IF_INDEX_UNKNOWN;
                /* Interface and Label stack TLV should be included in 
                 * the reply packet with Received Interface and Stack-R.
                 */
                pLsppEchoMsg->LsppPduInfo.u2TlvsPresent |=
                    LSPP_INTERFACE_AND_LBL_STACK_TLV;

                /* Skip IP address and Interface validation. */
                u1StackDValidate = OSIX_TRUE;
            }
            else if (pLsppDsMapTlv->DsIpAddr.u4_addr[0] ==
                     LSPP_ALLROUTERS_MULTICAST_ADDRESS)
            {
                /* Skip IP address, interface address and label stack 
                 * validations. 
                 */
            }
            else
            {
                u1IntfValidate = OSIX_TRUE;
                u1StackDValidate = OSIX_TRUE;
            }
            break;

        }
        case LSPP_IPV6_UNNUMBERED:
            /* IPv6 related checks to be added. */

        case LSPP_ADDR_TYPE_NOT_APPLICABLE:
        {
            u1StackDValidate = OSIX_TRUE;
            break;
        }

        default:
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_INVALID_DSMAP_TLV_ADDR_TYPE,
                      pLsppDsMapTlv->u1AddrType);
    }

    /* Validate the IP and interface address when the flag is set. */
    if (u1IntfValidate == OSIX_TRUE)
    {
        pLsppExtInParams->u4ContextId = pLsppEchoMsg->u4ContextId;
        pLsppExtInParams->u4RequestType = LSPP_CFA_GET_IF_ADDR;
        pLsppExtInParams->uInParams.u4IfIndex = pLsppEchoMsg->u4IfIndex;

        /* Get the IP address for the received interface. */
        i4RetVal = LsppPortHandleExtInteraction (pLsppExtInParams,
                                                 pLsppExtOutParams);
        if (i4RetVal == OSIX_FAILURE)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_FETCH_IF_INFO_FAILED, "LsppCoreVerifyDsmap");
        }

        if ((pLsppDsMapTlv->u1AddrType == LSPP_IPV4_NUMBERED) ||
            (pLsppDsMapTlv->u1AddrType == LSPP_IPV4_UNNUMBERED))
        {
            u4IpAddr = pLsppExtOutParams->OutLsppNodeId.MplsRouterId.u4_addr[0];

            pLsppExtInParams->u4RequestType = LSPP_MPLS_OAM_GET_ROUTER_ID;

            /* Get the Router Id from MPLS module. */
            i4RetVal = LsppPortHandleExtInteraction (pLsppExtInParams,
                                                     pLsppExtOutParams);
            if (i4RetVal == OSIX_FAILURE)
            {
                LSPP_LOG (pLsppEchoMsg->u4ContextId,
                          LSPP_FETCH_ROUTER_ID_FAILED, "LsppCoreVerifyDsmap");
            }

            if (pLsppDsMapTlv->u1AddrType == LSPP_IPV4_NUMBERED)
            {
                if (((pLsppDsMapTlv->DsIpAddr.u4_addr[0] !=
                      pLsppExtOutParams->OutLsppNodeId.MplsRouterId.u4_addr[0])
                     && (pLsppDsMapTlv->DsIpAddr.u4_addr[0] != u4IpAddr)) &&
                    (pLsppDsMapTlv->DsIfAddr.u4_addr[0] != u4IpAddr))
                {
                    *pu1ReturnCode = LSPP_DOWNSTREAM_MAPPING_MISMATCH;
                }
            }
            else if (pLsppDsMapTlv->u1AddrType == LSPP_IPV4_UNNUMBERED)
            {
                if (pLsppDsMapTlv->DsIpAddr.u4_addr[0] !=
                    pLsppExtOutParams->OutLsppNodeId.MplsRouterId.u4_addr[0])
                {
                    *pu1ReturnCode = LSPP_DOWNSTREAM_MAPPING_MISMATCH;
                }
            }

            if (*pu1ReturnCode == LSPP_DOWNSTREAM_MAPPING_MISMATCH)
            {
                pLsppEchoMsg->LsppPduInfo.u2TlvsPresent |=
                    LSPP_INTERFACE_AND_LBL_STACK_TLV;

                LSPP_LOG (pLsppEchoMsg->u4ContextId,
                          LSPP_MISMATCH_IN_DSMAP_IP,
                          "LsppCoreVerifyDsmap",
                          pLsppDsMapTlv->DsIpAddr.u4_addr[0]);
                MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                    (UINT1 *) pLsppExtInParams);
                MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pLsppExtOutParams);
                return OSIX_FAILURE;
            }
        }
        else
        {
            /* For IPv6 Numbered and UnNumbered need to be done. */
        }
    }

    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtInParams);
    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtOutParams);

    /* Validate the label stack in Dsmap TLV against received MPLS label 
     * stack.
     * */
    if ((u1StackDValidate == OSIX_TRUE) &&
        (pLsppDsMapTlv->u1DsLblStackDepth != 0))
    {
        /* The Dsmap label stack may contain Implicit Null label also and 
         * hence it may not always be equal to the received label stack.
         */
        if (pLsppEchoMsg->u1LabelStackDepth <= pLsppDsMapTlv->u1DsLblStackDepth)
        {
            u1RxLblIndex = pLsppEchoMsg->u1LabelStackDepth;
            u1DsLblIndex = pLsppDsMapTlv->u1DsLblStackDepth;

            while (u1DsLblIndex)
            {
                if ((u1RxLblIndex == 0) ||
                    (u1RxLblIndex > LSPP_MAX_LABEL_STACK) ||
                    (u1DsLblIndex == 0) ||
                    (u1DsLblIndex > LSPP_MAX_LABEL_STACK))
                {
                    *pu1ReturnCode = LSPP_DOWNSTREAM_MAPPING_MISMATCH;

                    pLsppEchoMsg->LsppPduInfo.u2TlvsPresent |=
                        LSPP_INTERFACE_AND_LBL_STACK_TLV;

                    LSPP_LOG (pLsppEchoMsg->u4ContextId,
                              LSPP_INVALID_LABEL_COUNT, "LsppCoreVerifyDsmap");

                    return OSIX_FAILURE;
                }

                if (pLsppDsMapTlv->DsLblInfo[u1DsLblIndex - 1].u4Label !=
                    LSPP_LBL_IMPLICIT_NULL)
                {
                    if ((pLsppEchoMsg->LblInfo[u1RxLblIndex - 1].u4Label !=
                         pLsppDsMapTlv->DsLblInfo[u1DsLblIndex - 1].u4Label) ||
                        (pLsppEchoMsg->LblInfo[u1RxLblIndex - 1].u1Exp !=
                         pLsppDsMapTlv->DsLblInfo[u1DsLblIndex - 1].u1Exp))
                    {
                        *pu1ReturnCode = LSPP_DOWNSTREAM_MAPPING_MISMATCH;

                        pLsppEchoMsg->LsppPduInfo.u2TlvsPresent |=
                            LSPP_INTERFACE_AND_LBL_STACK_TLV;

                        LSPP_LOG (pLsppEchoMsg->u4ContextId,
                                  LSPP_MISMATCH_IN_DSMAP_LABEL,
                                  "LsppCoreVerifyDsmap");

                        return OSIX_FAILURE;
                    }
                    u1RxLblIndex--;
                }
                u1DsLblIndex--;
            }

            /* Verify the Protocol in the recieved Dsmap Label Stack. */
            if (LsppCoreVerifyDsmapProtocol (pLsppEchoMsg) != OSIX_SUCCESS)
            {
                *pu1ReturnCode = LSPP_DOWNSTREAM_MAPPING_MISMATCH;

                pLsppEchoMsg->LsppPduInfo.u2TlvsPresent |=
                    LSPP_INTERFACE_AND_LBL_STACK_TLV;

                LSPP_LOG (pLsppEchoMsg->u4ContextId,
                          LSPP_MISMATCH_IN_DSMAP_LBL_STK_PROTOCOL,
                          "LsppCoreVerifyDsmap");
                return OSIX_FAILURE;
            }

        }
        else
        {
            *pu1ReturnCode = LSPP_DOWNSTREAM_MAPPING_MISMATCH;

            pLsppEchoMsg->LsppPduInfo.u2TlvsPresent |=
                LSPP_INTERFACE_AND_LBL_STACK_TLV;

            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_MISMATCH_IN_DSMAP_LBL_AND_RX_LBL_COUNT,
                      "LsppCoreVerifyDsmap",
                      pLsppEchoMsg->u1LabelStackDepth,
                      pLsppDsMapTlv->u1DsLblStackDepth);
            return OSIX_FAILURE;
        }
    }

    /* Logging Trace Message whethere Interface or Label Validation
     * Skipped */
    if ((u1IntfValidate == OSIX_FALSE) && (u1StackDValidate == OSIX_FALSE))
    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId,
                  LSPP_SKIPPING_DSMAP_LBL_IF_VALIDATION, "LsppCoreVerifyDsmap");
    }
    else if (u1StackDValidate == OSIX_FALSE)
    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId,
                  LSPP_SKIPPING_DSMAP_LBL_VALIDATION, "LsppCoreVerifyDsmap");
    }
    else if (u1IntfValidate == OSIX_FALSE)
    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId,
                  LSPP_SKIPPING_DSMAP_IF_VALIDATION, "LsppCoreVerifyDsmap");
    }

    return OSIX_SUCCESS;
}

/*******************************************************************************
 * *                                                                           *
 * * Function     : LsppCoreVerifyDsmapProtocol                                *
 * *                                                                           *
 * * Description  : This function validates the received protocol in the label *
 * *                stack(StackD) of DSMAP TLV against the received            *
 * *                label stack (StackR).                                      *
 * *                                                                           *
 * * Input        : pLsppEchoMsg - pointer to the structure containing the     *
 * *                received packet information.                               *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * *                                                                           *
 * ****************************************************************************/
PUBLIC INT4
LsppCoreVerifyDsmapProtocol (tLsppEchoMsg * pLsppEchoMsg)
{
    tLsppDsMapTlv      *pLsppDsMapTlv = NULL;
    UINT1               u1Protocol = 0;
    UINT1               u1LblStkCnt = 0;

    pLsppDsMapTlv = &(pLsppEchoMsg->LsppPduInfo.LsppDsMapTlv);

    switch (pLsppEchoMsg->LsppPathInfo.u1PathType)
    {
        case LSPP_PATH_TYPE_RSVP_IPV4:
        case LSPP_PATH_TYPE_RSVP_IPV6:
        case LSPP_PATH_TYPE_STATIC_TNL:
            u1LblStkCnt = pLsppEchoMsg->LsppPathInfo.TeTnlInfo.InSegInfo.
                u1LblStkCnt;

            if ((u1LblStkCnt > 0) && (u1LblStkCnt <= LSPP_MAX_LABEL_STACK))
            {
                u1Protocol = pLsppEchoMsg->LsppPathInfo.TeTnlInfo.
                    InSegInfo.LblInfo[u1LblStkCnt - 1].u1Protocol;
            }
            break;
        case LSPP_PATH_TYPE_FEC_128:
        case LSPP_PATH_TYPE_FEC_129:
        case LSPP_PATH_TYPE_STATIC_PW:
            return OSIX_SUCCESS;
        default:
            return OSIX_FAILURE;
    }

    if (pLsppDsMapTlv->DsLblInfo[0].u1Protocol != u1Protocol)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*******************************************************************************
 * *                                                                           *
 * * Function     : LsppCoreEgressProcess                                      *
 * *                                                                           *
 * * Description  : This function performs Downstream Mapping TLV validation   *
 * *                and FEC stack validation.                                  *
 * *                                                                           *
 * * Input        : pLsppEchoMsg - pointer to the structure containing the     *
 * *                received packet information.                               *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * *                                                                           *
 * ****************************************************************************/

PUBLIC INT4
LsppCoreEgressProcess (tLsppEchoMsg * pLsppEchoMsg)
{
    UINT1              *pu1ReturnCode = NULL;
    UINT1              *pu1ReturnSubCode = NULL;
    tLsppFecTlv        *pLsppFecTlv = NULL;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT4               u4RxLabelL = LSPP_LBL_IMPLICIT_NULL;
    UINT1               u1FecStackDepth = 1;
    UINT1               u1LabelStackDepth = 1;
    UINT1               u1FecStatus = 0;

    pu1ReturnCode = &(pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReturnCode);
    pu1ReturnSubCode = &(pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReturnSubCode);

    if ((pLsppEchoMsg->u1LabelStackDepth > LSPP_MAX_LABEL_STACK) ||
        (pLsppEchoMsg->LsppPduInfo.u1FecStackDepth > LSPP_MAX_FEC_STACK_DEPTH))
    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId,
                  LSPP_INVALID_FEC_OR_LBL_STK_DEPTH,
                  "LsppCoreEgressProcess",
                  pLsppEchoMsg->u1LabelStackDepth,
                  pLsppEchoMsg->LsppPduInfo.u1FecStackDepth);
        return OSIX_FAILURE;
    }

    /* Check whether the Downstream Mapping TLV is present in the packet. */
    if (pLsppEchoMsg->LsppPduInfo.u2TlvsPresent & LSPP_DSMAP_TLV)
    {
        i4RetVal = LsppCoreVerifyDsmap (pLsppEchoMsg);
        if (i4RetVal == OSIX_FAILURE)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_DSMAP_VALIDATION_FAILED, "LsppCoreEgressProcess");

            i4RetVal = LsppCoreSendEchoReply (pLsppEchoMsg);
            if (i4RetVal == OSIX_FAILURE)
            {
                LSPP_LOG (pLsppEchoMsg->u4ContextId,
                          LSPP_SEND_ECHO_REPLY_FAILED, "LsppCoreEgressProcess");

                LsppCoreUpdateGlobalStats (pLsppEchoMsg->u4ContextId,
                                           LSPP_REPLY_UNSENT, NULL);
            }
            return OSIX_SUCCESS;
        }
    }

    /* FEC Stack Validation. */
    while (1)
    {

        if (u1LabelStackDepth > LSPP_MAX_LABEL_STACK)
        {
            break;
        }

        pLsppFecTlv = &(LSPP_TGT_FEC_STACK[u1FecStackDepth - 1]);

        u4RxLabelL = LSPP_RX_LBL_STACK[u1LabelStackDepth - 1].u4Label;

        /* Validate the FEC and FEC to Label mapping. */
        i4RetVal = LsppCoreFecValidate (pLsppEchoMsg->u4ContextId,
                                        pLsppFecTlv, u4RxLabelL,
                                        pLsppEchoMsg->u4IfIndex,
                                        &u1FecStatus, pu1ReturnCode);
        if (i4RetVal == OSIX_FAILURE)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_VALIDATING_FEC_FAILED, "LsppCoreEgressProcess");
        }

        *pu1ReturnSubCode = u1FecStackDepth;

        if (u1FecStatus != LSPP_FEC_VALIDATION_FAILED)
        {
            ++u1FecStackDepth;

            LSPP_LOG (pLsppEchoMsg->u4ContextId, LSPP_FEC_VALIDATION_SUCCEEDED,
                      "LsppCoreEgressProcess");

            if (u1FecStackDepth <= LSPP_TGT_FEC_STACK_DEPTH)
            {
                ++u1LabelStackDepth;
                if (u1LabelStackDepth <= LSPP_RX_LBL_STACK_DEPTH)
                {
                    if (u1FecStatus == LSPP_FEC_LBL_MAP_IMPLICIT_NULL)
                    {
                        /* The received label stack will not contain 
                         * Implicit Null label and hence iterate to the 
                         * next FEC with the same label.
                         */
                        --u1LabelStackDepth;
                    }
                    continue;
                }
            }
        }
        break;
    }

    /* Send the reply packet. */
    if (pLsppEchoMsg->LsppPduInfo.LsppHeader.u1MessageType == LSPP_ECHO_REQUEST)
    {
        /* Send the echo reply with FEC validation result */
        i4RetVal = LsppCoreSendEchoReply (pLsppEchoMsg);
        if (i4RetVal == OSIX_FAILURE)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_SEND_ECHO_REPLY_FAILED, "LsppCoreEgressProcess");
            LsppCoreUpdateGlobalStats (pLsppEchoMsg->u4ContextId,
                                       LSPP_REPLY_UNSENT, NULL);
            return OSIX_SUCCESS;
        }
    }
    else
    {
        if (u1FecStatus == LSPP_FEC_VALIDATION_FAILED)
        {
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/*******************************************************************************
 * *                                                                           *
 * * Function     : LsppCoreFecValidate                                        *
 * *                                                                           *
 * * Description  : This function validates the given FEC                      *
 * *                                                                           *
 * * Input        : u4ContextId - identifier to the context                    *
 * *              : pLsppFecTlv - pointer to the structure containing the      *
 * *                              FEC information.                             *
 * *                u4RxLabelL  - Label from the received Stack R at           *
 * *                              u1LabelStackDepth.                           *
 * *                u4RxIfIndex - echo packet received interface index         *
 * *                pu1FecStatus- Fec Validation result                        *
 * *                pu1ReturnCode-Validation failure error codes to send in    *
 * *                              reply packet.                                *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * *                                                                           *
 * ****************************************************************************/

INT4
LsppCoreFecValidate (UINT4 u4ContextId, tLsppFecTlv * pLsppFecTlv,
                     UINT4 u4RxLabelL, UINT4 u4RxIfIndex, UINT1 *pu1FecStatus,
                     UINT1 *pu1ReturnCode)
{
    INT4                i4RetVal = OSIX_FAILURE;

    /* Check whether the FEC is NIL FEC and match the label. */
    if (pLsppFecTlv->u2TlvType == LSPP_FEC_NIL)
    {
        if ((u4RxLabelL == LSPP_LBL_EXPLICIT_NULL) ||
            (u4RxLabelL == LSPP_LBL_ROUTER_ALERT))
        {
            return OSIX_SUCCESS;
        }
        else
        {
            *pu1ReturnCode = LSPP_MAP_FAIL_FEC_TO_LBL_AT_STACK_DEPTH;
            *pu1FecStatus = LSPP_FEC_VALIDATION_FAILED;
            return OSIX_FAILURE;
        }
    }

    /* FEC to Label mapping. */
    i4RetVal = LsppCoreCheckFecLabelMap (u4ContextId, pLsppFecTlv, u4RxLabelL,
                                         u4RxIfIndex, pu1FecStatus,
                                         pu1ReturnCode);
    if (i4RetVal == OSIX_FAILURE)
    {
        LSPP_LOG (u4ContextId, LSPP_FEC_LBL_MAPPING_FAILED,
                  "LsppCoreFecValidate");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*******************************************************************************
 * *                                                                           *
 * * Function     : LsppCoreCheckFecLabelMap                                   *
 * *                                                                           *
 * * Description  : This function peforms FEC to Label mapping and protocol    *
 * *              : validation.                                                *
 * *                                                                           *
 * * Input        : pLsppEchoMsg - pointer to the structure containing the     *
 * *                received packet information.                               *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * *                                                                           *
 * ****************************************************************************/

INT4
LsppCoreCheckFecLabelMap (UINT4 u4ContextId, tLsppFecTlv * pLsppFecTlv,
                          UINT4 u4RxLabelL, UINT4 u4RxIfIndex,
                          UINT1 *pu1FecStatus, UINT1 *pu1ReturnCode)
{
    tLsppExtInParams   *pLsppExtInParams = NULL;
    tLsppExtOutParams  *pLsppExtOutParams = NULL;
    UINT4               u4Label = LSPP_INVALID_LABEL;
    UINT4               u4L3IfIndex = 0;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT1               u1ProtValid = OSIX_FALSE;
    UINT1               u1IfValid = OSIX_FALSE;
    UINT1               u1LblStkCnt = 0;

    if ((pLsppExtInParams = (tLsppExtInParams *)
         MemAllocMemBlk (LSPP_FSLSPPINPUTPARAMS_POOLID)) == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((pLsppExtOutParams = (tLsppExtOutParams *)
         MemAllocMemBlk (LSPP_FSLSPPOUTPUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        return OSIX_FAILURE;
    }

    MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
    MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));

    pLsppExtInParams->u4ContextId = u4ContextId;

    switch (pLsppFecTlv->u2TlvType)
    {
        case LSPP_FEC_LDP_IPV4:
        case LSPP_FEC_LDP_IPV6:
            /* Currently not supported. */
            MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtInParams);
            MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtOutParams);
            return OSIX_FAILURE;

        case LSPP_FEC_RSVP_IPV4:
        {
            pLsppExtInParams->u4RequestType = LSPP_MPLSDB_GET_TNL_INFO;

            pLsppExtInParams->uInParams.TnlInfo.SrcNodeId.
                u4NodeType = LSPP_MPLS_ADDR_TYPE_IPV4;

            pLsppExtInParams->uInParams.TnlInfo.SrcNodeId.
                MplsRouterId.u4_addr[0] =
                pLsppFecTlv->RsvpTeFecTlv.TunnelSenderAddr.u4_addr[0];

            pLsppExtInParams->uInParams.TnlInfo.DstNodeId.
                u4NodeType = LSPP_MPLS_ADDR_TYPE_IPV4;

            pLsppExtInParams->uInParams.TnlInfo.DstNodeId.
                MplsRouterId.u4_addr[0] =
                pLsppFecTlv->RsvpTeFecTlv.TunnelEndAddr.u4_addr[0];

            pLsppExtInParams->uInParams.TnlInfo.u4SrcTnlId =
                pLsppFecTlv->RsvpTeFecTlv.u2TunnelId;

            pLsppExtInParams->uInParams.TnlInfo.u4LspId =
                pLsppFecTlv->RsvpTeFecTlv.u2LspId;

            break;
        }

        case LSPP_FEC_RSVP_IPV6:
        {
            pLsppExtInParams->u4RequestType = LSPP_MPLSDB_GET_TNL_INFO;

            pLsppExtInParams->uInParams.TnlInfo.SrcNodeId.
                u4NodeType = LSPP_MPLS_ADDR_TYPE_IPV6;

            MEMCPY (pLsppExtInParams->uInParams.TnlInfo.SrcNodeId.
                    MplsRouterId.u1_addr,
                    pLsppFecTlv->RsvpTeFecTlv.TunnelSenderAddr.u1_addr,
                    LSPP_IPV6_ADDR_LEN);

            pLsppExtInParams->uInParams.TnlInfo.DstNodeId.
                u4NodeType = LSPP_MPLS_ADDR_TYPE_IPV6;

            MEMCPY (pLsppExtInParams->uInParams.TnlInfo.DstNodeId.
                    MplsRouterId.u1_addr,
                    pLsppFecTlv->RsvpTeFecTlv.TunnelEndAddr.u1_addr,
                    LSPP_IPV6_ADDR_LEN);

            pLsppExtInParams->uInParams.TnlInfo.u4SrcTnlId =
                pLsppFecTlv->RsvpTeFecTlv.u2TunnelId;

            pLsppExtInParams->uInParams.TnlInfo.u4LspId =
                pLsppFecTlv->RsvpTeFecTlv.u2LspId;

            break;
        }

        case LSPP_FEC_128_DEPRECATED_PWFEC:
        {
            pLsppExtInParams->u4RequestType =
                LSPP_L2VPN_GET_PW_INFO_FRM_PWID_FEC;

            pLsppExtInParams->uInParams.PwFecInfo.SrcNodeId.
                u4NodeType = LSPP_MPLS_ADDR_TYPE_IPV4;

            pLsppExtInParams->uInParams.PwFecInfo.SrcNodeId.
                MplsRouterId.u4_addr[0] =
                pLsppFecTlv->PwFec128DeprecTlv.LsppPwInfo.
                RemotePEAddr.u4_addr[0];

            pLsppExtInParams->uInParams.PwFecInfo.u4VcId =
                pLsppFecTlv->PwFec128DeprecTlv.LsppPwInfo.u4PWId;

            pLsppExtInParams->uInParams.PwFecInfo.u2PwVcType =
                pLsppFecTlv->PwFec128DeprecTlv.LsppPwInfo.u2PWType;

            break;
        }

        case LSPP_FEC_128_PWFEC:
        {
            pLsppExtInParams->u4RequestType =
                LSPP_L2VPN_GET_PW_INFO_FRM_PWID_FEC;

            pLsppExtInParams->uInParams.PwFecInfo.SrcNodeId.
                u4NodeType = LSPP_MPLS_ADDR_TYPE_IPV4;

            pLsppExtInParams->uInParams.PwFecInfo.SrcNodeId.
                MplsRouterId.u4_addr[0] =
                pLsppFecTlv->PwFec128Tlv.LsppPwInfo.RemotePEAddr.u4_addr[0];

            pLsppExtInParams->uInParams.PwFecInfo.DstNodeId.
                u4NodeType = LSPP_MPLS_ADDR_TYPE_IPV4;

            pLsppExtInParams->uInParams.PwFecInfo.DstNodeId.
                MplsRouterId.u4_addr[0] =
                pLsppFecTlv->PwFec128Tlv.SenderPEAddr.u4_addr[0];

            pLsppExtInParams->uInParams.PwFecInfo.u4VcId =
                pLsppFecTlv->PwFec128Tlv.LsppPwInfo.u4PWId;

            pLsppExtInParams->uInParams.PwFecInfo.u2PwVcType =
                pLsppFecTlv->PwFec128Tlv.LsppPwInfo.u2PWType;

            break;
        }

        case LSPP_FEC_129_PWFEC:
        {
            pLsppExtInParams->u4RequestType =
                LSPP_L2VPN_GET_PW_INFO_FRM_GENFEC_TYPE1;

            pLsppExtInParams->uInParams.PwFecInfo.SrcNodeId.
                u4NodeType = LSPP_MPLS_ADDR_TYPE_IPV4;

            pLsppExtInParams->uInParams.PwFecInfo.SrcNodeId.
                MplsRouterId.u4_addr[0] =
                pLsppFecTlv->PwFec129Tlv.RemotePEAddr.u4_addr[0];

            pLsppExtInParams->uInParams.PwFecInfo.DstNodeId.
                u4NodeType = LSPP_MPLS_ADDR_TYPE_IPV4;

            pLsppExtInParams->uInParams.PwFecInfo.DstNodeId.
                MplsRouterId.u4_addr[0] =
                pLsppFecTlv->PwFec129Tlv.SenderPEAddr.u4_addr[0];

            pLsppExtInParams->uInParams.PwFecInfo.u2PwVcType =
                pLsppFecTlv->PwFec129Tlv.u2PWType;

            pLsppExtInParams->uInParams.PwFecInfo.u1AgiType =
                pLsppFecTlv->PwFec129Tlv.u1AgiType;

            pLsppExtInParams->uInParams.PwFecInfo.u1AgiLen =
                pLsppFecTlv->PwFec129Tlv.u1AgiLen;

            MEMCPY (pLsppExtInParams->uInParams.PwFecInfo.au1Agi,
                    pLsppFecTlv->PwFec129Tlv.au1Agi,
                    pLsppFecTlv->PwFec129Tlv.u1AgiLen);

            pLsppExtInParams->uInParams.PwFecInfo.u1LocalAIIType =
                pLsppFecTlv->PwFec129Tlv.u1TaiiType;

            pLsppExtInParams->uInParams.PwFecInfo.u1SaiiLen =
                pLsppFecTlv->PwFec129Tlv.u1TaiiLen;

            MEMCPY (pLsppExtInParams->uInParams.PwFecInfo.au1Saii,
                    pLsppFecTlv->PwFec129Tlv.au1Taii,
                    pLsppFecTlv->PwFec129Tlv.u1TaiiLen);

            pLsppExtInParams->uInParams.PwFecInfo.u1RemoteAIIType =
                pLsppFecTlv->PwFec129Tlv.u1SaiiType;

            pLsppExtInParams->uInParams.PwFecInfo.u1TaiiLen =
                pLsppFecTlv->PwFec129Tlv.u1SaiiLen;

            MEMCPY (pLsppExtInParams->uInParams.PwFecInfo.au1Taii,
                    pLsppFecTlv->PwFec129Tlv.au1Saii,
                    pLsppFecTlv->PwFec129Tlv.u1SaiiLen);

            break;
        }

        case LSPP_FEC_STATIC_LSP:
        {
            pLsppExtInParams->u4RequestType = LSPP_MPLSDB_GET_TNL_INFO;

            pLsppExtInParams->uInParams.TnlInfo.SrcNodeId.
                u4NodeType = LSPP_MPLS_ADDR_TYPE_GLOBAL_NODE_ID;

            pLsppExtInParams->uInParams.TnlInfo.SrcNodeId.
                MplsGlobalNodeId.u4GlobalId =
                pLsppFecTlv->StaticLspFec.u4SrcGlobalId;

            pLsppExtInParams->uInParams.TnlInfo.SrcNodeId.
                MplsGlobalNodeId.u4NodeId =
                pLsppFecTlv->StaticLspFec.u4SrcNodeId;

            pLsppExtInParams->uInParams.TnlInfo.u4SrcTnlId =
                pLsppFecTlv->StaticLspFec.u2SrcTnlNum;

            pLsppExtInParams->uInParams.TnlInfo.u4LspId =
                pLsppFecTlv->StaticLspFec.u2LspNum;

            pLsppExtInParams->uInParams.TnlInfo.DstNodeId.
                u4NodeType = LSPP_MPLS_ADDR_TYPE_GLOBAL_NODE_ID;

            pLsppExtInParams->uInParams.TnlInfo.DstNodeId.
                MplsGlobalNodeId.u4GlobalId =
                pLsppFecTlv->StaticLspFec.u4DstGlobalId;

            pLsppExtInParams->uInParams.TnlInfo.DstNodeId.
                MplsGlobalNodeId.u4NodeId =
                pLsppFecTlv->StaticLspFec.u4DstNodeId;

            pLsppExtInParams->uInParams.TnlInfo.u4DstTnlId =
                pLsppFecTlv->StaticLspFec.u2DstTnlNum;

            pLsppExtInParams->uInParams.TnlInfo.u4InIf = u4RxIfIndex;
            break;
        }

        case LSPP_FEC_STATIC_PW:
        {
            pLsppExtInParams->u4RequestType =
                LSPP_L2VPN_GET_PW_INFO_FRM_GENFEC_TYPE2;

            pLsppExtInParams->uInParams.PwFecInfo.u1AgiType =
                pLsppFecTlv->StaticPwFec.u1AgiType;

            pLsppExtInParams->uInParams.PwFecInfo.u1AgiLen =
                pLsppFecTlv->StaticPwFec.u1AgiLen;

            MEMCPY (pLsppExtInParams->uInParams.PwFecInfo.au1Agi,
                    pLsppFecTlv->StaticPwFec.au1Agi,
                    pLsppFecTlv->StaticPwFec.u1AgiLen);

            pLsppExtInParams->uInParams.PwFecInfo.SrcNodeId.
                u4NodeType = LSPP_MPLS_ADDR_TYPE_GLOBAL_NODE_ID;

            pLsppExtInParams->uInParams.PwFecInfo.SrcNodeId.
                MplsGlobalNodeId.u4GlobalId =
                pLsppFecTlv->StaticPwFec.u4DstGlobalId;

            pLsppExtInParams->uInParams.PwFecInfo.SrcNodeId.
                MplsGlobalNodeId.u4NodeId =
                pLsppFecTlv->StaticPwFec.u4DstNodeId;

            pLsppExtInParams->uInParams.PwFecInfo.u4SrcAcId =
                pLsppFecTlv->StaticPwFec.u4DstAcId;

            pLsppExtInParams->uInParams.PwFecInfo.DstNodeId.
                u4NodeType = LSPP_MPLS_ADDR_TYPE_GLOBAL_NODE_ID;

            pLsppExtInParams->uInParams.PwFecInfo.DstNodeId.
                MplsGlobalNodeId.u4GlobalId =
                pLsppFecTlv->StaticPwFec.u4SrcGlobalId;

            pLsppExtInParams->uInParams.PwFecInfo.DstNodeId.
                MplsGlobalNodeId.u4NodeId =
                pLsppFecTlv->StaticPwFec.u4SrcNodeId;

            pLsppExtInParams->uInParams.PwFecInfo.u4DstAcId =
                pLsppFecTlv->StaticPwFec.u4SrcAcId;

            break;
        }
        default:
        {
            LSPP_LOG (u4ContextId,
                      LSPP_INVALID_FEC_TYPE_FOR_LABEL_MAPPING,
                      "LsppCoreCheckFecLabelMap");
            MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtInParams);
            MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtOutParams);
            return OSIX_FAILURE;
        }
    }
    /* Fetch FEC related info based on the FEC type */
    i4RetVal = LsppPortHandleExtInteraction (pLsppExtInParams,
                                             pLsppExtOutParams);
    if (i4RetVal == OSIX_FAILURE)
    {
        LSPP_LOG (u4ContextId, LSPP_FETCH_FROM_EXT_MOD_FAILED,
                  "LsppCoreCheckFecLabelMap");

        /* Update the return code. */
        *pu1ReturnCode = LSPP_NO_MAPPING_FOR_FEC_AT_STACK_DEPTH;
        *pu1FecStatus = LSPP_FEC_VALIDATION_FAILED;

        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);
        return OSIX_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtInParams);

    switch (pLsppFecTlv->u2TlvType)
    {
        case LSPP_FEC_RSVP_IPV4:
        case LSPP_FEC_RSVP_IPV6:
        {
            if (pLsppExtOutParams->OutTnlInfo.InSegInfo.u1LblStkCnt != 0)
            {
                /* The bottom most label will be the respective label for
                 * the given FEC.
                 */
                u1LblStkCnt =
                    pLsppExtOutParams->OutTnlInfo.InSegInfo.u1LblStkCnt;

                u4Label =
                    pLsppExtOutParams->OutTnlInfo.InSegInfo.
                    LblInfo[u1LblStkCnt - 1].u4Label;

                if (pLsppExtOutParams->OutTnlInfo.InSegInfo.
                    LblInfo[u1LblStkCnt - 1].u1Protocol ==
                    LSPP_PROTOCOL_RSVP_TE)
                {
                    u1ProtValid = OSIX_TRUE;
                }

                i4RetVal = LsppCoreGetL3If (u4ContextId,
                                            pLsppExtOutParams->OutTnlInfo.
                                            InSegInfo.u4InIf, &u4L3IfIndex);
                if (i4RetVal == OSIX_FAILURE)
                {
                    LSPP_LOG (u4ContextId,
                              LSPP_GET_L3_IF_FAILED, "LsppCoreGetDsmapInfo");
                    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                        (UINT1 *) pLsppExtOutParams);
                    return OSIX_FAILURE;
                }

                if (u4L3IfIndex == u4RxIfIndex)
                {
                    u1IfValid = OSIX_TRUE;
                }
            }

            break;
        }

        case LSPP_FEC_128_DEPRECATED_PWFEC:
        case LSPP_FEC_128_PWFEC:
        case LSPP_FEC_129_PWFEC:
        case LSPP_FEC_STATIC_PW:
        {
            u4Label = pLsppExtOutParams->OutPwDetail.u4InVcLabel;

            /* Protocol validation to be done once it is available. */
            u1ProtValid = OSIX_TRUE;
            /*Interface validation is made true as of now.It will be done if MPLS is\
             * providing the IN interface details*/
            u1IfValid = OSIX_TRUE;
            break;
        }

        case LSPP_FEC_STATIC_LSP:
        {
            if (pLsppExtOutParams->OutTnlInfo.InSegInfo.u1LblStkCnt != 0)
            {
                /* The bottom most label will be the respective label for
                 * the given FEC.
                 */
                u1LblStkCnt =
                    pLsppExtOutParams->OutTnlInfo.InSegInfo.u1LblStkCnt;

                u4Label =
                    pLsppExtOutParams->OutTnlInfo.InSegInfo.
                    LblInfo[u1LblStkCnt - 1].u4Label;

                if (pLsppExtOutParams->OutTnlInfo.InSegInfo.
                    LblInfo[u1LblStkCnt - 1].u1Protocol == LSPP_PROTOCOL_STATIC)
                {
                    u1ProtValid = OSIX_TRUE;
                }

                i4RetVal = LsppCoreGetL3If (u4ContextId,
                                            pLsppExtOutParams->OutTnlInfo.
                                            InSegInfo.u4InIf, &u4L3IfIndex);
                if (i4RetVal == OSIX_FAILURE)
                {
                    LSPP_LOG (u4ContextId,
                              LSPP_GET_L3_IF_FAILED, "LsppCoreGetDsmapInfo");
                    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                        (UINT1 *) pLsppExtOutParams);
                    return OSIX_FAILURE;
                }

                if (u4L3IfIndex == u4RxIfIndex)
                {
                    u1IfValid = OSIX_TRUE;
                }
            }

            break;
        }
        default:
            LSPP_LOG (u4ContextId, LSPP_INVALID_FEC_TYPE_FOR_LABEL_MAPPING,
                      "LsppCoreCheckFecLabelMap");
    }

    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtOutParams);

    if (u4Label == LSPP_INVALID_LABEL)
    {
        LSPP_LOG (u4ContextId, LSPP_INVALID_LABEL_FOR_FEC_MAPPING,
                  "LsppCoreCheckFecLabelMap", pLsppFecTlv->u2TlvType);
        *pu1ReturnCode = LSPP_NO_MAPPING_FOR_FEC_AT_STACK_DEPTH;
        *pu1FecStatus = LSPP_FEC_VALIDATION_FAILED;
        return OSIX_FAILURE;
    }
    else
    {
        if (u4Label == LSPP_LBL_IMPLICIT_NULL)
        {
            *pu1FecStatus = LSPP_FEC_LBL_MAP_IMPLICIT_NULL;
        }
        else if (u4Label != u4RxLabelL)
        {
            LSPP_LOG (u4ContextId, LSPP_INVALID_RX_LABEL_FOR_FEC,
                      "LsppCoreCheckFecLabelMap", u4Label, u4RxLabelL);
            *pu1ReturnCode = LSPP_MAP_FAIL_FEC_TO_LBL_AT_STACK_DEPTH;
            *pu1FecStatus = LSPP_FEC_VALIDATION_FAILED;
            return OSIX_FAILURE;
        }

        /* Validate the interace by fetching the underlying 
         * interface index from CFA. 
         * */

        if ((u1ProtValid == OSIX_FALSE) || (u1IfValid == OSIX_FALSE))
        {
            LSPP_LOG (u4ContextId, LSPP_PROTOCOL_IF_VALIDAITON_FAILED,
                      "LsppCoreCheckFecLabelMap");
            *pu1ReturnCode = LSPP_PROTOCOL_MISMATCH;
            *pu1FecStatus = LSPP_FEC_VALIDATION_FAILED;
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/*******************************************************************************
 * *                                                                           *
 * * Function     : LsppCoreGetLabelInfo                                       *
 * *                                                                           *
 * * Description  : This function fetches the label information validation.    *
 * *                                                                           *
 * * Input        : pLsppEchoMsg - pointer to the structure containing the     *
 * *                received packet information.                               *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * *                                                                           *
 * ****************************************************************************/

INT4
LsppCoreGetLabelInfo (UINT4 u4ContextId, UINT4 u4RxIfIndex,
                      UINT4 u4RxLabel, UINT1 *pu1EntryExists,
                      UINT1 *pu1LblAction)
{
    tLsppExtInParams   *pLsppExtInParams = NULL;
    tLsppExtOutParams  *pLsppExtOutParams = NULL;
    tLsppTnlLspInfo    *pTnlId = NULL;
    INT4                i4RetVal = OSIX_FAILURE;

    if ((pLsppExtInParams = (tLsppExtInParams *)
         MemAllocMemBlk (LSPP_FSLSPPINPUTPARAMS_POOLID)) == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((pLsppExtOutParams = (tLsppExtOutParams *)
         MemAllocMemBlk (LSPP_FSLSPPOUTPUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        return OSIX_FAILURE;
    }

    MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
    MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));

    /* Initializing label Entry present to false and this is 
     * set to true if ILM mapping is present */
    *pu1EntryExists = OSIX_FALSE;

    pLsppExtInParams->u4ContextId = u4ContextId;
    pLsppExtInParams->u4RequestType = LSPP_GET_PATH_FRM_INLBL_INFO;
    pLsppExtInParams->InLblInfo.u4Inlabel = u4RxLabel;
    pLsppExtInParams->InLblInfo.u4InIf = u4RxIfIndex;

    /* To fetch received path info from received label */
    i4RetVal = LsppPortHandleExtInteraction (pLsppExtInParams,
                                             pLsppExtOutParams);
    if (i4RetVal == OSIX_FAILURE)
    {
        LSPP_LOG (u4ContextId, LSPP_FETCH_FROM_EXT_MOD_FAILED,
                  "LsppCoreGetLabelInfo");
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);
        return OSIX_FAILURE;
    }

    switch (pLsppExtOutParams->u1PathType)
    {
        case LSPP_MPLS_PATH_TYPE_LSP:
        {
            /* Currently not supported. */
            break;
        }
        case LSPP_MPLS_PATH_TYPE_TNL:
        {
            *pu1LblAction = pLsppExtOutParams->OutTnlInfo.InSegInfo.u1LblAction;
            break;
        }

        case LSPP_MPLS_PATH_TYPE_PW:
        {
            /* If Pseudowire entry exist Action for PW label is always POP. */
            *pu1LblAction = LSPP_MPLS_LBL_ACTION_POP;
            break;
        }
        case LSPP_MPLS_PATH_TYPE_MEG:
        {
            if (pLsppExtOutParams->OutMegDetail.u1ServiceType ==
                OAM_SERVICE_TYPE_LSP)
            {
                MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));

                pLsppExtInParams->u4ContextId = u4ContextId;
                pLsppExtInParams->u4RequestType = LSPP_MPLS_OAM_GET_MEG_INFO;

                pLsppExtInParams->uInParams.MegInfo.MegIndices.u4MegIndex =
                    pLsppExtOutParams->OutMegDetail.MegInfo.MegIndices.
                    u4MegIndex;

                pLsppExtInParams->uInParams.MegInfo.MegIndices.u4MeIndex =
                    pLsppExtOutParams->OutMegDetail.MegInfo.MegIndices.
                    u4MeIndex;

                pLsppExtInParams->uInParams.MegInfo.MegIndices.u4MpIndex =
                    pLsppExtOutParams->OutMegDetail.MegInfo.MegIndices.
                    u4MpIndex;

                MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));

                /* To get Meg Information from MEG indices */
                if (LsppPortHandleExtInteraction
                    (pLsppExtInParams, pLsppExtOutParams) != OSIX_SUCCESS)
                {
                    LSPP_LOG (u4ContextId, LSPP_FETCH_MEG_INFO_FAILED,
                              "LsppCoreGetLabelInfo");
                    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                        (UINT1 *) pLsppExtInParams);
                    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                        (UINT1 *) pLsppExtOutParams);
                    return OSIX_FAILURE;
                }

                pTnlId =
                    &(pLsppExtOutParams->OutMegDetail.unServiceInfo.
                      ServiceTnlId);

                MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));

                pLsppExtInParams->uInParams.TnlInfo.SrcNodeId.
                    MplsGlobalNodeId.u4GlobalId =
                    pTnlId->SrcNodeId.MplsGlobalNodeId.u4GlobalId;

                pLsppExtInParams->uInParams.TnlInfo.SrcNodeId.
                    MplsGlobalNodeId.u4NodeId =
                    pTnlId->SrcNodeId.MplsGlobalNodeId.u4NodeId;

                pLsppExtInParams->uInParams.TnlInfo.DstNodeId.
                    MplsGlobalNodeId.u4GlobalId =
                    pTnlId->DstNodeId.MplsGlobalNodeId.u4GlobalId;

                pLsppExtInParams->uInParams.TnlInfo.DstNodeId.
                    MplsGlobalNodeId.u4NodeId =
                    pTnlId->DstNodeId.MplsGlobalNodeId.u4NodeId;

                pLsppExtInParams->uInParams.TnlInfo.SrcNodeId.u4NodeType =
                    pTnlId->SrcNodeId.u4NodeType;

                pLsppExtInParams->uInParams.TnlInfo.DstNodeId.u4NodeType =
                    pTnlId->DstNodeId.u4NodeType;

                pLsppExtInParams->uInParams.TnlInfo.u4SrcTnlId =
                    pTnlId->u4SrcTnlNum;

                pLsppExtInParams->uInParams.TnlInfo.u4LspId = pTnlId->u4LspNum;

                pLsppExtInParams->u4ContextId = u4ContextId;
                pLsppExtInParams->u4RequestType = LSPP_MPLSDB_GET_TNL_INFO;

                MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));

                /* Get the Tunnel information from the MPLS DB module. */
                if (LsppPortHandleExtInteraction
                    (pLsppExtInParams, pLsppExtOutParams) != OSIX_SUCCESS)
                {
                    LSPP_LOG (u4ContextId, LSPP_FETCH_TNL_INFO_FAILED,
                              "LsppCoreGetLabelInfo");
                    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                        (UINT1 *) pLsppExtInParams);
                    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                        (UINT1 *) pLsppExtOutParams);
                    return OSIX_FAILURE;
                }

                *pu1LblAction =
                    pLsppExtOutParams->OutTnlInfo.InSegInfo.u1LblAction;
            }
            else if (pLsppExtOutParams->OutMegDetail.u1ServiceType ==
                     OAM_SERVICE_TYPE_PW)
            {
                /* If Pseudowire entry exist Action for PW label is 
                 * always POP. */
                *pu1LblAction = LSPP_MPLS_LBL_ACTION_POP;
            }
            break;
        }

        default:
            LSPP_LOG (u4ContextId, LSPP_INVALID_MPLS_PATH_TYPE,
                      "LsppCoreGetLabelInfo",
                      pLsppExtOutParams->u1PathType, u4RxLabel);
            MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtInParams);
            MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtOutParams);
            return OSIX_FAILURE;
    }

    *pu1EntryExists = OSIX_TRUE;

    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtInParams);
    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtOutParams);

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : LsppCoreUpdatePwSrcAddr 
 * Description: This function will update the Pw Peer Address
 * Input      : u4ContextId      - Context Id
 * Returns    : OSIX_SUCCESS or OSIX_FAILURE 
 * Output     : pPwDetail - Pw Detail with Source Ip Address Updated
 *****************************************************************************/
PRIVATE UINT4
LsppCoreUpdatePwSrcAddr (UINT4 u4ContextId, tLsppPwDetail * pPwDetail)
{
    tLsppExtInParams   *pLsppExtInParams = NULL;
    tLsppExtOutParams  *pLsppExtOutParams = NULL;
    INT4                i4RetVal = OSIX_FAILURE;

    if ((pLsppExtInParams = (tLsppExtInParams *)
         MemAllocMemBlk (LSPP_FSLSPPINPUTPARAMS_POOLID)) == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((pLsppExtOutParams = (tLsppExtOutParams *)
         MemAllocMemBlk (LSPP_FSLSPPOUTPUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        return OSIX_FAILURE;
    }

    MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
    MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));

    pLsppExtInParams->u4RequestType = LSPP_MPLS_OAM_GET_ROUTER_ID;

    /* To fetch PW Source Address. */
    i4RetVal = LsppPortHandleExtInteraction (pLsppExtInParams,
                                             pLsppExtOutParams);
    if (i4RetVal == OSIX_FAILURE)
    {
        LSPP_LOG (u4ContextId, LSPP_FETCH_ROUTER_ID_FAILED,
                  "LsppCoreUpdatePwSrcAddr");
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);
        return OSIX_FAILURE;
    }

    if (pLsppExtOutParams->OutLsppNodeId.u4NodeType == LSPP_MPLS_ADDR_TYPE_IPV4)
    {
        pPwDetail->PwFecInfo.SrcNodeId.MplsRouterId.u4_addr[0] =
            pLsppExtOutParams->OutLsppNodeId.MplsRouterId.u4_addr[0];
    }
    else
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);
        return OSIX_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtInParams);
    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtOutParams);
    return OSIX_SUCCESS;
}

/*******************************************************************************
 *  Function     : LsppCoreUpdateDefaultReplyMode                             *
 *  Description  : This function is to update the reply mode with default     *
 *                 value when request is received with invalid reply mode     *
 *  Input        : pLsppEchoMsg - pointer to the structure containing the     *
 *                 received packet information.                               *
 *  Output       : None                                                       *
 *  Returns      : None                                                       *
 * ****************************************************************************/ VOID
LsppCoreUpdateDefaultReplyMode (tLsppEchoMsg * pLsppEchoMsg)
{
    if (pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_ACH)
    {
        pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReplyMode =
            LSPP_REPLY_APPLICATION_CC;
    }
    else
    {
        pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReplyMode = LSPP_REPLY_IP_UDP;
    }
    return;
}

/*******************************************************************************
 * *                                                                           *
 * * Function     : LsppCoreSendEchoReply                                      *
 * *                                                                           *
 * * Description  : This function sends the reply packet for the request.      *
 * *                                                                           *
 * * Input        : pLsppEchoMsg - pointer to the structure containing the     *
 * *                received packet information.                               *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * *                                                                           *
 * ****************************************************************************/

INT4
LsppCoreSendEchoReply (tLsppEchoMsg * pLsppEchoMsg)
{
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT1               u1HeaderLen = 0;

    /* Updating Reply Mode with Default Reply Mode if 
     * echo request is received with invalid reply mode */
    if (pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReplyMode >
        LSPP_REPLY_APPLICATION_CC)
    {
        LsppCoreUpdateDefaultReplyMode (pLsppEchoMsg);
    }

    /* Do not send the reply when the reply mode in the received 
     * echo request is set to "No reply". Also check whether the
     * sanity validation is succeeded before giving the remote 
     * discriminator to BFD module.*/
    if (pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReplyMode == LSPP_NO_REPLY)
    {
        if ((pLsppEchoMsg->LsppPduInfo.u2TlvsPresent &
             LSPP_BFD_DISCRIMINATOR_TLV) &&
            (pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReturnCode != 0))
        {
            if (LsppTxSendPacketToBfd (NULL, pLsppEchoMsg, LSPP_NO_REPLY) !=
                OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppEchoMsg->u4ContextId,
                          LSPP_SENDING_PKT_TO_BFD_FAILED,
                          "LsppCoreSendEchoReply");

                return OSIX_FAILURE;
            }
        }
        /* Update any statistics if applicable. */
        return OSIX_SUCCESS;
    }

    pLsppEchoMsg->LsppPduInfo.LsppHeader.u1MessageType = LSPP_ECHO_REPLY;
    pLsppEchoMsg->LsppPduInfo.LsppHeader.u2Version = LSPP_HEADER_VERSION;

    /* Reset the Target FEC Tlv bit as the reply need not contain FEC stack. */
    pLsppEchoMsg->LsppPduInfo.u2TlvsPresent =
        (UINT2) (pLsppEchoMsg->LsppPduInfo.u2TlvsPresent &
                 (~(LSPP_TARGET_FEC_TLV)));

    /* Get the information for Interface and Label Stack TLV 
     * when the flag is set.*/
    if (pLsppEchoMsg->LsppPduInfo.u2TlvsPresent &
        LSPP_INTERFACE_AND_LBL_STACK_TLV)
    {
        i4RetVal = LsppCoreGetIfLblStkTlvInfo (pLsppEchoMsg);
        if (i4RetVal == OSIX_FAILURE)
        {
            /* Update the statistics. */
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_GET_IF_LBL_STK_INFO_FAILED, "LsppCoreSendEchoReply");
            return OSIX_FAILURE;
        }
    }

    /* Get Downstream information if the node is a transit node 
     * for the FEC.*/
    if ((pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReturnCode ==
         LSPP_LBL_SWITCHED_AT_STACK_DEPTH) &&
        (pLsppEchoMsg->LsppPduInfo.u2TlvsPresent & LSPP_DSMAP_TLV))
    {
        i4RetVal = LsppCoreGetDsmapInfo (pLsppEchoMsg);
        if (i4RetVal == OSIX_FAILURE)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_GET_DSMAP_INFO_FAILED, "LsppCoreSendEchoReply");
            return OSIX_FAILURE;
        }
    }
    else
    {
        /* Reset the Dsmap Tlv bit */
        pLsppEchoMsg->LsppPduInfo.u2TlvsPresent =
            (UINT2) (pLsppEchoMsg->LsppPduInfo.u2TlvsPresent &
                     (~(LSPP_DSMAP_TLV)));
    }

    /* Update the Destination IP and Port from the received IP/UDP header. */
    if (((pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReplyMode ==
          LSPP_REPLY_IP_UDP) ||
         (pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReplyMode ==
          LSPP_REPLY_IP_UDP_ROUTER_ALERT)) ||
        ((pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReplyMode ==
          LSPP_REPLY_APPLICATION_CC) &&
         ((pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_IP) ||
          (pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_ACH_IP))))
    {
        pLsppEchoMsg->LsppIpHeader.u4Dest = pLsppEchoMsg->LsppIpHeader.u4Src;
        pLsppEchoMsg->LsppUdpHeader.u2Dest_u_port =
            pLsppEchoMsg->LsppUdpHeader.u2Src_u_port;
        pLsppEchoMsg->LsppUdpHeader.u2Src_u_port = LSPP_UDP_DEST_PORT;
        u1HeaderLen = pLsppEchoMsg->LsppIpHeader.u1Hlen;

        pLsppEchoMsg->LsppIpHeader.u1Hlen =
            (IP_HDR_LEN / LSPP_IP_HEADER_LEN_IN_BYTES);
    }

    if (pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReturnCode !=
        LSPP_NO_LBL_ENTRY_AT_STACK_DEPTH)
    {
        if ((pLsppEchoMsg->LsppPathInfo.u1PathType ==
             LSPP_PATH_TYPE_FEC_128) ||
            (pLsppEchoMsg->LsppPathInfo.u1PathType ==
             LSPP_PATH_TYPE_FEC_129) ||
            (pLsppEchoMsg->LsppPathInfo.u1PathType == LSPP_PATH_TYPE_STATIC_PW))
        {
            pLsppEchoMsg->LsppIpHeader.u1Version =
                pLsppEchoMsg->LsppPathInfo.PwDetail.u1IpVersion;

            pLsppEchoMsg->LsppIpHeader.u1Hlen =
                (IP_HDR_LEN / LSPP_IP_HEADER_LEN_IN_BYTES);

            if (((pLsppEchoMsg->LsppPathInfo.u1PathType ==
                 LSPP_PATH_TYPE_FEC_128) ||
                (pLsppEchoMsg->LsppPathInfo.u1PathType ==
                 LSPP_PATH_TYPE_FEC_129) ||
                (pLsppEchoMsg->LsppPathInfo.u1PathType == 
                 LSPP_PATH_TYPE_STATIC_PW)) &&
                (pLsppEchoMsg->LsppPathInfo.PwDetail.PwFecInfo.SrcNodeId.MplsRouterId.u4_addr[0] == 0))
            {
                pLsppEchoMsg->LsppIpHeader.u4Dest =
                    pLsppEchoMsg->LsppIpHeader.u4Src;

                if (LsppCoreUpdatePwSrcAddr (pLsppEchoMsg->u4ContextId,
                                             &(pLsppEchoMsg->LsppPathInfo.
                                               PwDetail)) != OSIX_SUCCESS)
                {
                    LSPP_LOG (pLsppEchoMsg->u4ContextId,
                              LSPP_FETCH_PW_SRC_IP_FAILED,
                              "LsppCoreSendEchoReply");
                    return OSIX_FAILURE;
                }
            }

            if (pLsppEchoMsg->LsppPathInfo.PwDetail.PwFecInfo.
                SrcNodeId.MplsRouterId.u4_addr[0] == 0)
            {
                return OSIX_FAILURE;
            }

            pLsppEchoMsg->LsppIpHeader.u4Src =
                pLsppEchoMsg->LsppPathInfo.PwDetail.PwFecInfo.
                SrcNodeId.MplsRouterId.u4_addr[0];
        }
    }

    if (pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReplyMode ==
        LSPP_REPLY_IP_UDP_ROUTER_ALERT)
    {
        i4RetVal =
            LsppCoreGetRevPathFromDestIp (pLsppEchoMsg->u4ContextId,
                                          pLsppEchoMsg->LsppIpHeader.
                                          u4Dest,
                                          &(pLsppEchoMsg->LsppIpHeader.
                                            u4Src),
                                          &(pLsppEchoMsg->LsppPathInfo));
        if (i4RetVal == OSIX_FAILURE)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_NO_REVERSE_PATH, "LsppCoreSendEchoReply");

            /* LSP doesn't exists for sending the reply packet on LSP and
             * hence check whether the IP path exists.*/
            i4RetVal =
                LsppCoreGetSrcIpFromDestIp (pLsppEchoMsg->u4ContextId,
                                            pLsppEchoMsg->LsppIpHeader.
                                            u4Dest,
                                            &(pLsppEchoMsg->LsppIpHeader.
                                              u4Src));

            if ((i4RetVal == OSIX_FAILURE) ||
                (pLsppEchoMsg->LsppIpHeader.u4Src == 0))
            {
                LSPP_LOG (pLsppEchoMsg->u4ContextId,
                          LSPP_NO_IP_ROUTE_EXIST, "LsppCoreSendEchoReply");
                return OSIX_FAILURE;
            }
            /* Route exists to reach the remote node. */
            pLsppEchoMsg->u1IsRouteExists = OSIX_TRUE;
            pLsppEchoMsg->LsppIpHeader.u1Hlen = u1HeaderLen;
            pLsppEchoMsg->u1EncapType = LSPP_ENCAP_TYPE_IP;
        }
        pLsppEchoMsg->LsppIpHeader.u1Version = (UINT1) IP_VERSION_4;
    }

    if (pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReplyMode == LSPP_REPLY_IP_UDP)
    {
        pLsppEchoMsg->u1EncapType = LSPP_ENCAP_TYPE_IP;
    }

    /* Get the reverse path from the Label or Source MEP received in the 
     * request packet.*/
    if (pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReplyMode ==
        LSPP_REPLY_APPLICATION_CC)
    {
        i4RetVal = LsppCoreGetRevPathFromFwd (pLsppEchoMsg);
        if (i4RetVal == OSIX_FAILURE)
        {
            /* Drop the packet as the reverse path is not available. */
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_GET_REVERSE_PATH_FROM_FWD_FAILED,
                      "LsppCoreSendEchoReply");

            return OSIX_FAILURE;
        }
    }

    /* Include the Target FEC for the reverse path when it is requested
     * by the ingress node. This is applicable only for bi-directional LSPs.
     */
    if (pLsppEchoMsg->LsppPduInfo.LsppHeader.u2GlobalFlag &
        LSPP_REVERSE_PATH_VALIDATE)
    {
        i4RetVal = LsppCoreUpdateTgtFecInfo (pLsppEchoMsg, LSPP_PATH_TYPE_MEP);
        if (i4RetVal == OSIX_FAILURE)
        {
            /* Drop the packet as the reverse path is not available. */
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_UPDATE_TARGET_FEC_STACK_FAILED,
                      "LsppCoreSendEchoReply");
            return OSIX_FAILURE;
        }
    }

    /* Reset the Pad TLV in the TLVs to be framed list when the padding 
     * received need not be copied in the reply.
     */
    if (pLsppEchoMsg->LsppPduInfo.u2TlvsPresent & LSPP_PAD_TLV)
    {
        if ((pLsppEchoMsg->LsppPduInfo.LsppPadTlv.u1FirstOctet ==
             LSPP_DROP_PAD_TLV) ||
            (pLsppEchoMsg->LsppPduInfo.LsppPadTlv.u2Length == 0))
        {
            pLsppEchoMsg->LsppPduInfo.u2TlvsPresent = (UINT2)
                (pLsppEchoMsg->LsppPduInfo.u2TlvsPresent & (~(LSPP_PAD_TLV)));
        }
    }

    if (((pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReplyMode ==
          LSPP_REPLY_IP_UDP_ROUTER_ALERT) ||
         (pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReplyMode ==
          LSPP_REPLY_APPLICATION_CC)) &&
        (pLsppEchoMsg->u1IsRouteExists == OSIX_FALSE))
    {

        /* Update the Out segment info for out path. */
        i4RetVal = LsppCoreUpdateOutPathInfo (pLsppEchoMsg,
                                              pLsppEchoMsg->LsppPathInfo.
                                              u1PathType);
        if (i4RetVal != OSIX_SUCCESS)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_UPDATE_OUTSEG_INFO_STACK_FAILED,
                      "LsppCoreSendEchoReply");
            return OSIX_FAILURE;
        }

        /* Update the label stack for the reverse path from the identified 
         * path information.
         */
        i4RetVal = LsppCoreUpdateOutLblInfo (pLsppEchoMsg,
                                             pLsppEchoMsg->LblInfo,
                                             &(pLsppEchoMsg->
                                               u1LabelStackDepth));
        if (i4RetVal == OSIX_FAILURE)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_UPDATE_OUT_LABEL_FAILED, "LsppCoreSendEchoReply");
            /* Drop the packet as the reverse path is not available. */
            return OSIX_FAILURE;
        }

        /* Get the Source MEP if the reply node is egress for the path or 
         * Source MIP when the node is an intermediate node for the path.
         */
        if ((pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_ACH) &&
            ((pLsppEchoMsg->LsppPathInfo.u1PathType ==
              LSPP_PATH_TYPE_STATIC_TNL) ||
             (pLsppEchoMsg->LsppPathInfo.u1PathType ==
              LSPP_PATH_TYPE_STATIC_PW)))
        {
            pLsppEchoMsg->LsppAchInfo.u1AchTlvPresent = 0;
            LsppCoreUpdateMepOrMipTlv (pLsppEchoMsg);
        }

    }

    /* Reset the received Echo message encapsulation information 
     * before calcualing the packet size to be sent via IP path. */
    if ((pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReplyMode ==
         LSPP_REPLY_IP_UDP) ||
        ((pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReplyMode ==
          LSPP_REPLY_IP_UDP_ROUTER_ALERT) &&
         (pLsppEchoMsg->u1IsRouteExists == OSIX_TRUE)))
    {
        pLsppEchoMsg->LsppAchInfo.u1AchTlvPresent = 0;
        pLsppEchoMsg->u1LabelStackDepth = 0;
    }

    pLsppEchoMsg->LsppPduInfo.LsppHeader.u2GlobalFlag = 0;

    /* Calculating packet size */
    LsppUtilCalculatePktSize (pLsppEchoMsg);

    /* Tx packet. */
    /* Allocate buffer for Tx Packet. */
    pMsg = LSPP_ALLOC_CRU_BUF ((pLsppEchoMsg->u4EncapHdrLen +
                                pLsppEchoMsg->u4LsppPduLen +
                                MPLS_MAX_L2HEADER_LEN +
                                CFA_ENET_V2_HEADER_SIZE),
                               pLsppEchoMsg->u4EncapHdrLen +
                               CFA_ENET_V2_HEADER_SIZE + MPLS_MAX_L2HEADER_LEN);
    if (pMsg == NULL)
    {
        LSPP_CMN_TRC (pLsppEchoMsg->u4ContextId,
                      LSPP_CRU_BUF_ALLOC_FAILED, "LsppCoreSendEchoReply");
        return OSIX_FAILURE;
    }

    i4RetVal = LsppTxFrameAndSendPdu (pLsppEchoMsg, NULL, NULL, pMsg);
    if (i4RetVal == OSIX_FAILURE)
    {
        LSPP_RELEASE_CRU_BUF (pMsg, 0);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*******************************************************************************
 * *                                                                           *
 * * Function     : LsppCoreGetDsmapInfo                                       *
 * *                                                                           *
 * * Description  : This function fetches the Downstream information for the   *
 * *                incoming label at a transit node.                          *
 * *                                                                           *
 * * Input        : pLsppEchoMsg - pointer to the structure containing the     *
 * *                received packet information.                               *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * *                                                                           *
 * ****************************************************************************/

INT4
LsppCoreGetDsmapInfo (tLsppEchoMsg * pLsppEchoMsg)
{
    tLsppExtInParams   *pLsppExtInParams = NULL;
    tLsppExtOutParams  *pLsppExtOutParams = NULL;
    tMplsOutSegInfo    *pOutSegInfo = NULL;
    tLsppDsMapTlv      *pLsppDsMapTlv = NULL;
    tLsppTnlLspInfo    *pTnlId = NULL;
    UINT4               u4L3IfIndex = 0;
    UINT4               u4Label = 0;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT1               u1DsmapLblCnt = 0;
    UINT1               u1RxLblCnt = 0;

    if ((pLsppExtInParams = (tLsppExtInParams *)
         MemAllocMemBlk (LSPP_FSLSPPINPUTPARAMS_POOLID)) == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((pLsppExtOutParams = (tLsppExtOutParams *)
         MemAllocMemBlk (LSPP_FSLSPPOUTPUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        return OSIX_FAILURE;
    }

    MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
    MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));

    pLsppDsMapTlv = &(pLsppEchoMsg->LsppPduInfo.LsppDsMapTlv);
    MEMSET (pLsppDsMapTlv, 0, sizeof (tLsppDsMapTlv));

    if (pLsppEchoMsg->u1LblSwapAtDepth == LSPP_INVALID_INDEX)
    {
        /* Downstream information at an egress is not valid. */
        LSPP_LOG (pLsppEchoMsg->u4ContextId,
                  LSPP_INVALID_SWAP_DEPTH, "LsppCoreGetDsmapInfo");
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);
        return OSIX_FAILURE;
    }

    u4Label = pLsppEchoMsg->LblInfo[pLsppEchoMsg->u1LblSwapAtDepth].u4Label;

    pLsppExtInParams->u4ContextId = pLsppEchoMsg->u4ContextId;
    pLsppExtInParams->u4RequestType = LSPP_GET_PATH_FRM_INLBL_INFO;
    pLsppExtInParams->InLblInfo.u4Inlabel = u4Label;
    pLsppExtInParams->InLblInfo.u4InIf = pLsppEchoMsg->u4IfIndex;

    /* This external module fetch is to get Received path info
     * from Received label */
    i4RetVal = LsppPortHandleExtInteraction (pLsppExtInParams,
                                             pLsppExtOutParams);
    if (i4RetVal == OSIX_FAILURE)
    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId, LSPP_FETCH_FROM_EXT_MOD_FAILED,
                  "LsppCoreGetDsmapInfo");
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);
        return OSIX_FAILURE;
    }

    switch (pLsppExtOutParams->u1PathType)
    {
        case LSPP_MPLS_PATH_TYPE_TNL:
        {
            pOutSegInfo = &(pLsppExtOutParams->OutTnlInfo.OutSegInfo);
            break;
        }

        case LSPP_MPLS_PATH_TYPE_LSP:
        {
            /* Currently not supported. */
            MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtInParams);
            MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtOutParams);
            return OSIX_FAILURE;
        }

        case LSPP_MPLS_PATH_TYPE_PW:
        {
            /* Not applicable for a transit node. */
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_INVALID_PW_PATH_TYPE, "LsppCoreGetDsmapInfo");

            MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtInParams);
            MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtOutParams);
            return OSIX_FAILURE;
        }

        case LSPP_MPLS_PATH_TYPE_MEG:
        {
            if (pLsppExtOutParams->OutMegDetail.u1ServiceType ==
                OAM_SERVICE_TYPE_LSP)
            {
                MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));

                pLsppExtInParams->u4ContextId = pLsppEchoMsg->u4ContextId;
                pLsppExtInParams->u4RequestType = LSPP_MPLS_OAM_GET_MEG_INFO;

                pLsppExtInParams->uInParams.MegInfo.MegIndices.u4MegIndex =
                    pLsppExtOutParams->OutMegDetail.MegInfo.MegIndices.
                    u4MegIndex;

                pLsppExtInParams->uInParams.MegInfo.MegIndices.u4MeIndex =
                    pLsppExtOutParams->OutMegDetail.MegInfo.MegIndices.
                    u4MeIndex;

                pLsppExtInParams->uInParams.MegInfo.MegIndices.u4MpIndex =
                    pLsppExtOutParams->OutMegDetail.MegInfo.MegIndices.
                    u4MpIndex;

                MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));

                /* This external module call is to fetch MEG information from
                 * MEG Indices */
                if (LsppPortHandleExtInteraction
                    (pLsppExtInParams, pLsppExtOutParams) != OSIX_SUCCESS)
                {
                    LSPP_LOG (pLsppEchoMsg->u4ContextId,
                              LSPP_FETCH_MEG_INFO_FAILED,
                              "LsppCoreGetLabelInfo");
                    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                        (UINT1 *) pLsppExtInParams);
                    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                        (UINT1 *) pLsppExtOutParams);
                    return OSIX_FAILURE;
                }

                pTnlId =
                    &(pLsppExtOutParams->OutMegDetail.unServiceInfo.
                      ServiceTnlId);

                MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));

                pLsppExtInParams->uInParams.TnlInfo.SrcNodeId.
                    MplsGlobalNodeId.u4GlobalId =
                    pTnlId->SrcNodeId.MplsGlobalNodeId.u4GlobalId;

                pLsppExtInParams->uInParams.TnlInfo.SrcNodeId.
                    MplsGlobalNodeId.u4NodeId =
                    pTnlId->SrcNodeId.MplsGlobalNodeId.u4NodeId;

                pLsppExtInParams->uInParams.TnlInfo.DstNodeId.
                    MplsGlobalNodeId.u4GlobalId =
                    pTnlId->DstNodeId.MplsGlobalNodeId.u4GlobalId;

                pLsppExtInParams->uInParams.TnlInfo.DstNodeId.
                    MplsGlobalNodeId.u4NodeId =
                    pTnlId->DstNodeId.MplsGlobalNodeId.u4NodeId;

                pLsppExtInParams->uInParams.TnlInfo.SrcNodeId.u4NodeType =
                    pTnlId->SrcNodeId.u4NodeType;

                pLsppExtInParams->uInParams.TnlInfo.DstNodeId.u4NodeType =
                    pTnlId->DstNodeId.u4NodeType;

                pLsppExtInParams->uInParams.TnlInfo.u4SrcTnlId =
                    pTnlId->u4SrcTnlNum;

                pLsppExtInParams->uInParams.TnlInfo.u4LspId = pTnlId->u4LspNum;

                pLsppExtInParams->uInParams.TnlInfo.u4InIf =
                    pLsppEchoMsg->u4IfIndex;

                pLsppExtInParams->u4ContextId = pLsppEchoMsg->u4ContextId;
                pLsppExtInParams->u4RequestType = LSPP_MPLSDB_GET_TNL_INFO;

                MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));

                /* Get the Tunnel information from the MPLS DB module. */
                if (LsppPortHandleExtInteraction
                    (pLsppExtInParams, pLsppExtOutParams) != OSIX_SUCCESS)
                {
                    LSPP_LOG (pLsppEchoMsg->u4ContextId,
                              LSPP_FETCH_TNL_INFO_FAILED,
                              "LsppCoreGetDsmapInfo");
                    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                        (UINT1 *) pLsppExtInParams);
                    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                        (UINT1 *) pLsppExtOutParams);
                    return OSIX_FAILURE;
                }
            }
            else if (pLsppExtOutParams->OutMegDetail.u1ServiceType ==
                     OAM_SERVICE_TYPE_PW)
            {
                /* Not applicable for a transit node. */
                LSPP_LOG (pLsppEchoMsg->u4ContextId,
                          LSPP_INVALID_PW_PATH_TYPE, "LsppCoreGetDsmapInfo");
                MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                    (UINT1 *) pLsppExtInParams);
                MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pLsppExtOutParams);
                return OSIX_FAILURE;
            }
            else
            {
                LSPP_LOG (pLsppEchoMsg->u4ContextId,
                          LSPP_INVALID_MEG_SERVICE_TYPE,
                          "LsppCoreGetDsmapInfo");
                MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                    (UINT1 *) pLsppExtInParams);
                MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pLsppExtOutParams);
                return OSIX_FAILURE;
            }

            pOutSegInfo = &(pLsppExtOutParams->OutTnlInfo.OutSegInfo);
            break;
        }
        default:
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_INVALID_DSMAP_PATH_TYPE, "LsppCoreGetDsmapInfo");

            MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtInParams);
            MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtOutParams);

            return OSIX_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtInParams);
    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtOutParams);

    if (pOutSegInfo->NHAddr.u4_addr[0] == 0)
    {
        if (pOutSegInfo->u1LblStkCnt == 0)
        {
            pLsppEchoMsg->LsppPduInfo.u2TlvsPresent =
                (UINT2) (pLsppEchoMsg->LsppPduInfo.u2TlvsPresent &
                         (~(LSPP_DSMAP_TLV)));
            pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReturnCode =
                LSPP_LBL_SWITCHED_BUT_NO_MPLS_FWD_AT_STACK_DEPTH;

            return OSIX_SUCCESS;
        }
        else if (pOutSegInfo->u1NHAddrType == LSR_ADDR_IPV4)
        {
            pLsppDsMapTlv->u1AddrType = LSPP_IPV4_UNNUMBERED;
            pLsppDsMapTlv->DsIpAddr.u4_addr[0] = LSPP_IP_LOOPBACK_ADDRESS;
            pLsppDsMapTlv->DsIfAddr.u4_addr[0] = 0;
        }
        else if (pOutSegInfo->u1NHAddrType == LSR_ADDR_IPV6)
        {
            /* Currently not supported. */
        }
    }

    else if (pOutSegInfo->u1NHAddrType == LSR_ADDR_IPV4)
    {
        pLsppDsMapTlv->u1AddrType = LSPP_IPV4_NUMBERED;
        pLsppDsMapTlv->DsIpAddr.u4_addr[0] = pOutSegInfo->NHAddr.u4_addr[0];
        pLsppDsMapTlv->DsIfAddr.u4_addr[0] = pOutSegInfo->NHAddr.u4_addr[0];
    }
    else if (pOutSegInfo->u1NHAddrType == LSR_ADDR_IPV6)
    {
        /* Currently not supported. */
    }

    while ((u1DsmapLblCnt < pOutSegInfo->u1LblStkCnt) &&
           (u1DsmapLblCnt < LSPP_MAX_LABEL_STACK))
    {

        pLsppDsMapTlv->DsLblInfo[u1DsmapLblCnt].u4Label =
            pOutSegInfo->LblInfo[u1DsmapLblCnt].u4Label;
        pLsppDsMapTlv->DsLblInfo[u1DsmapLblCnt].u1Ttl =
            pOutSegInfo->LblInfo[u1DsmapLblCnt].u1Ttl;
        pLsppDsMapTlv->DsLblInfo[u1DsmapLblCnt].u1Exp =
            pOutSegInfo->LblInfo[u1DsmapLblCnt].u1Exp;
        pLsppDsMapTlv->DsLblInfo[u1DsmapLblCnt].u1SI = 0;
        pLsppDsMapTlv->DsLblInfo[u1DsmapLblCnt].u1Protocol =
            pOutSegInfo->LblInfo[u1DsmapLblCnt].u1Protocol;

        u1DsmapLblCnt++;
    }

    pLsppDsMapTlv->u1DsLblStackDepth = pOutSegInfo->u1LblStkCnt;

    u1RxLblCnt = (UINT1) (pLsppEchoMsg->u1LblSwapAtDepth + 1);

    while ((u1RxLblCnt < pLsppEchoMsg->u1LabelStackDepth)
           && (u1DsmapLblCnt < LSPP_MAX_LABEL_STACK))
    {

        pLsppDsMapTlv->DsLblInfo[u1DsmapLblCnt].u4Label =
            pLsppEchoMsg->LblInfo[u1RxLblCnt].u4Label;
        pLsppDsMapTlv->DsLblInfo[u1DsmapLblCnt].u1Ttl =
            pLsppEchoMsg->LblInfo[u1RxLblCnt].u1Ttl;
        pLsppDsMapTlv->DsLblInfo[u1DsmapLblCnt].u1Exp =
            pLsppEchoMsg->LblInfo[u1RxLblCnt].u1Exp;
        pLsppDsMapTlv->DsLblInfo[u1DsmapLblCnt].u1SI =
            pLsppEchoMsg->LblInfo[u1RxLblCnt].u1SI;
        pLsppDsMapTlv->DsLblInfo[u1DsmapLblCnt].u1Protocol =
            pLsppEchoMsg->LblInfo[u1RxLblCnt].u1Protocol;

        u1RxLblCnt++;
        u1DsmapLblCnt++;

        (pLsppDsMapTlv->u1DsLblStackDepth)++;

        if (pLsppDsMapTlv->u1DsLblStackDepth > LSPP_MAX_LABEL_STACK)
        {
            return OSIX_FAILURE;
        }
    }

    /* Update the bottom most label SI bit to 1. */
    if (u1DsmapLblCnt != 0)
    {
        pLsppDsMapTlv->DsLblInfo[u1DsmapLblCnt - 1].u1SI = 1;
    }

    /* Get the underlying L3 interface for fetching the MTU. */
    i4RetVal = LsppCoreGetL3If (pLsppEchoMsg->u4ContextId,
                                pOutSegInfo->u4OutIf, &u4L3IfIndex);
    if (i4RetVal == OSIX_FAILURE)
    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId,
                  LSPP_GET_L3_IF_FAILED, "LsppCoreGetDsmapInfo");
        return OSIX_FAILURE;
    }

    /* Get the MTU for the Outgoing Interface */
    i4RetVal = LsppCoreGetIfMtu (pLsppEchoMsg->u4ContextId,
                                 u4L3IfIndex, &(pLsppDsMapTlv->u2Mtu));
    if (i4RetVal == OSIX_FAILURE)
    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId,
                  LSPP_GET_INTF_MTU_FAILED, "LsppCoreGetDsmapInfo");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*******************************************************************************
 * *                                                                           *
 * * Function     : LsppCoreGetIfLblStkTlvInfo                              *
 * *                                                                           *
 * * Description  : This function fetches the information required for         *
 * *                Interface and Label Satck TLV.                             *
 * *                                                                           *
 * * Input        : pLsppEchoMsg - pointer to the structure containing the     *
 * *                received packet information.                               *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * *                                                                           *
 * ****************************************************************************/

PUBLIC INT4
LsppCoreGetIfLblStkTlvInfo (tLsppEchoMsg * pLsppEchoMsg)
{
    tLsppExtInParams   *pLsppExtInParams = NULL;
    tLsppExtOutParams  *pLsppExtOutParams = NULL;
    tLsppIfLblStkTlv   *pLsppIfLblStkTlv = NULL;
    INT4                i4RetVal = OSIX_FAILURE;

    if ((pLsppExtInParams = (tLsppExtInParams *)
         MemAllocMemBlk (LSPP_FSLSPPINPUTPARAMS_POOLID)) == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((pLsppExtOutParams = (tLsppExtOutParams *)
         MemAllocMemBlk (LSPP_FSLSPPOUTPUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        return OSIX_FAILURE;
    }

    MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
    MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));

    pLsppIfLblStkTlv = &pLsppEchoMsg->LsppPduInfo.LsppIfLblStkTlv;

    if ((pLsppEchoMsg->u1LabelStackDepth <= 0) ||
        (pLsppEchoMsg->u1LabelStackDepth > LSPP_MAX_LABEL_STACK))
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);
        return OSIX_FAILURE;
    }

    pLsppExtInParams->u4ContextId = pLsppEchoMsg->u4ContextId;
    pLsppExtInParams->u4RequestType = LSPP_CFA_GET_IF_ADDR;
    pLsppExtInParams->uInParams.u4IfIndex = pLsppEchoMsg->u4IfIndex;

    /* Get the IP address for the received interface. */
    i4RetVal = LsppPortHandleExtInteraction (pLsppExtInParams,
                                             pLsppExtOutParams);
    if (i4RetVal == OSIX_FAILURE)
    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId,
                  LSPP_FETCH_FROM_EXT_MOD_FAILED, "LsppCoreGetIfLblStkTlvInfo");
    }

    /* This needs to be updated for IPv6 cases. 
     * As of now it is considerd as IPv4 by default.
     */

    /* Check whether the interface has any address assigned to it. */
    if (pLsppExtOutParams->OutLsppNodeId.MplsRouterId.u4_addr[0] != 0)
    {
        pLsppIfLblStkTlv->u1AddrType = LSPP_IPV4_NUMBERED;

        pLsppIfLblStkTlv->RxIpAddr.u4_addr[0] =
            pLsppExtOutParams->OutLsppNodeId.MplsRouterId.u4_addr[0];

        pLsppIfLblStkTlv->RxIfAddr.u4_addr[0] =
            pLsppExtOutParams->OutLsppNodeId.MplsRouterId.u4_addr[0];
    }
    else
    {
        /* The interface is considered to be unnumbered as it does not 
         * have any address assigned to it. 
         */
        pLsppIfLblStkTlv->u1AddrType = LSPP_IPV4_UNNUMBERED;
    }

    if (pLsppIfLblStkTlv->u1AddrType == LSPP_IPV4_UNNUMBERED)
    {
        pLsppExtInParams->u4RequestType = LSPP_MPLS_OAM_GET_ROUTER_ID;

        /* Get the Router Id from MPLS module. */
        i4RetVal = LsppPortHandleExtInteraction (pLsppExtInParams,
                                                 pLsppExtOutParams);
        if (i4RetVal == OSIX_FAILURE)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_FETCH_FROM_EXT_MOD_FAILED,
                      "LsppCoreGetIfLblStkTlvInfo");
        }

        pLsppIfLblStkTlv->RxIpAddr.u4_addr[0] =
            pLsppExtOutParams->OutLsppNodeId.MplsRouterId.u4_addr[0];

        pLsppIfLblStkTlv->RxIfAddr.u4_addr[0] = pLsppEchoMsg->u4IfIndex;
    }

    /* Copy the Received Label stack in the Interface & Label Stack TLV. */
    MEMCPY (pLsppIfLblStkTlv->RxLblInfo, pLsppEchoMsg->LblInfo,
            (sizeof (tLsppLblInfo) * (pLsppEchoMsg->u1LabelStackDepth)));

    pLsppIfLblStkTlv->u1LblStackDepth = pLsppEchoMsg->u1LabelStackDepth;

    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtInParams);
    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtOutParams);

    return OSIX_SUCCESS;
}

/*******************************************************************************
 *                                                                            *
 *  Function     : LsppCoreHandleReqTimedOut                                  *
 *                                                                            *
 *  Description  : This function handles the WFR timer expiry. Updates the    * 
 *                 ping trace entry, global statistics, releases the sem      * 
 *                 when the request is triggered from CLI and sends the next  * 
 *                 traceroute request.                                        * 
 *                                                                            *
 *  Input        : pEchoSeqEntry - pointer to the structure containing the    *
 *                 echo sequence information.                                 *
 *                                                                            *
 *  Output       : None                                                       *
 *                                                                            *
 *  Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                            *
 *****************************************************************************/

PUBLIC INT4
LsppCoreHandleReqTimedOut (tLsppFsLsppEchoSequenceTableEntry * pEchoSeqEntry)
{
    tLsppFsLsppPingTraceTableEntry *pLsppPingTraceEntryInput = NULL;
    tLsppFsLsppPingTraceTableEntry *pPingTraceEntry = NULL;
    tLsppEchoMsg       *pLsppEchoMsg = NULL;

    pLsppPingTraceEntryInput = (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppPingTraceEntryInput == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pLsppPingTraceEntryInput, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    pLsppPingTraceEntryInput->MibObject.u4FsLsppContextId =
        pEchoSeqEntry->MibObject.u4FsLsppContextId;
    pLsppPingTraceEntryInput->MibObject.u4FsLsppSenderHandle =
        pEchoSeqEntry->MibObject.u4FsLsppSenderHandle;

    pPingTraceEntry = LsppGetFsLsppPingTraceTable (pLsppPingTraceEntryInput);

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppPingTraceEntryInput);

    if (pPingTraceEntry == NULL)
    {
        LSPP_LOG (pEchoSeqEntry->MibObject.u4FsLsppContextId,
                  LSPP_PINGTRACE_ENTRY_NOT_FOUND,
                  "LsppCoreHandleReqTimedOut",
                  pEchoSeqEntry->MibObject.u4FsLsppSenderHandle);
        return OSIX_FAILURE;
    }

    if (pEchoSeqEntry->MibObject.u4FsLsppSequenceNumber >
        pPingTraceEntry->u4LastUpdatedSeqIndex)
    {
        pPingTraceEntry->u4LastUpdatedSeqIndex =
            pEchoSeqEntry->MibObject.u4FsLsppSequenceNumber;
    }

    LsppCoreUpdateGlobalStats (pPingTraceEntry->MibObject.u4FsLsppContextId,
                               LSPP_REQ_TIMEDOUT, NULL);

    /* Allocate memory to store the details needed to frame the packet. */
    if ((pLsppEchoMsg = (tLsppEchoMsg *)
         MemAllocMemBlk (gLsppGlobals.LsppFsLsppEchoMsgPoolId)) == NULL)
    {
        LSPP_CMN_TRC (pPingTraceEntry->MibObject.u4FsLsppContextId,
                      LSPP_MEM_ALLOC_FAIL, "LsppCoreHandleReqTimedOut");
        return OSIX_FAILURE;
    }

    MEMSET (pLsppEchoMsg, 0, sizeof (tLsppEchoMsg));

    /* This function releases the Semaphore for printing the output 
     * on the console when the request is triggered from CLI and
     * Initiate a request when the request type is traceroute.
     */
    LsppCoreUpdatePingTraceResults (pPingTraceEntry, pLsppEchoMsg,
                                    LSPP_REQ_TIMEDOUT);

    MemReleaseMemBlock (gLsppGlobals.LsppFsLsppEchoMsgPoolId,
                        (UINT1 *) pLsppEchoMsg);

    return OSIX_SUCCESS;
}

/*******************************************************************************
 * *                                                                           *
 * * Function     : LsppCoreUpdateGlobalStats                                  *
 * *                                                                           *
 * * Description  : This function increments the corresponding                 *
 * *                counter based on the given type and the return code.       *
 * *                                                                           *
 * * Input        : u4ContextId - unique identifier for context.               *
 * *                u1ParamType - specifies the counter which has to be        *
 * *                              incremented.                                 *
 * *                pu1RetCode  - pointer to the return code which should be a *
 * *                              valid pointer containing the return code     *
 * *                              of the received reply packet.                *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * *                                                                           *
 * ****************************************************************************/

VOID
LsppCoreUpdateGlobalStats (UINT4 u4ContextId, UINT1 u1ParamType,
                           UINT1 *pu1RetCode)
{
    tLsppFsLsppGlobalStatsTableEntry LsppGlobalStatsEntry;
    tLsppFsLsppGlobalStatsTableEntry *pGlobalStatsEntry = NULL;

    MEMSET (&LsppGlobalStatsEntry, 0,
            sizeof (tLsppFsLsppGlobalStatsTableEntry));

    /* Get the global stats entry for the corresponding context. */
    LsppGlobalStatsEntry.MibObject.u4FsLsppContextId = u4ContextId;

    pGlobalStatsEntry = LsppGetFsLsppGlobalStatsTable (&LsppGlobalStatsEntry);
    if (pGlobalStatsEntry == NULL)
    {
        LSPP_LOG (LSPP_INVALID_CONTEXT, LSPP_INVALID_CONTEXT_ID,
                  "LsppCoreUpdateGlobalStats", u4ContextId);
        return;
    }

    switch (u1ParamType)
    {
        case LSPP_REQ_TX:
            (pGlobalStatsEntry->MibObject.u4FsLsppGlbStatReqTx)++;
            break;
        case LSPP_REQ_RX:
            (pGlobalStatsEntry->MibObject.u4FsLsppGlbStatReqRx)++;
            break;
        case LSPP_REQ_TIMEDOUT:
            (pGlobalStatsEntry->MibObject.u4FsLsppGlbStatReqTimedOut)++;
            break;
        case LSPP_REQ_UNSENT:
            (pGlobalStatsEntry->MibObject.u4FsLsppGlbStatReqUnSent)++;
            break;
        case LSPP_REPLY_TX:
            (pGlobalStatsEntry->MibObject.u4FsLsppGlbStatReplyTx)++;
            break;
        case LSPP_REPLY_RX:
            (pGlobalStatsEntry->MibObject.u4FsLsppGlbStatReplyRx)++;
            if (pu1RetCode == NULL)
            {
                LSPP_LOG (u4ContextId, LSPP_INVALID_RETURN_CODE,
                          "LsppCoreUpdateGlobalStats");
                return;
            }

            switch (*pu1RetCode)
            {
                case LSPP_NO_RETURN_CODE:
                    (pGlobalStatsEntry->MibObject.u4FsLsppGlbStatNoRetCode)++;
                    break;
                case LSPP_MALFORMED_ECHO_REQ_RECEIVED:
                    (pGlobalStatsEntry->MibObject.
                     u4FsLsppGlbStatMalformedReq)++;
                    break;
                case LSPP_ONE_OR_MORE_TLVS_NOT_UNDERSTOOD:
                    (pGlobalStatsEntry->MibObject.
                     u4FsLsppGlbStatReqUnSupptdTlv)++;
                    break;
                case LSPP_EGRESS_FOR_FEC_AT_STACK_DEPTH:
                    (pGlobalStatsEntry->MibObject.
                     u4FsLsppGlbStatReplyFromEgr)++;
                    break;
                case LSPP_NO_MAPPING_FOR_FEC_AT_STACK_DEPTH:
                    (pGlobalStatsEntry->MibObject.
                     u4FsLsppGlbStatNoFecMapping)++;
                    break;
                case LSPP_DOWNSTREAM_MAPPING_MISMATCH:
                    (pGlobalStatsEntry->MibObject.
                     u4FsLsppGlbStatDsMapMismatch)++;
                    break;
                case LSPP_UPSTREAM_IF_INDEX_UNKNOWN:
                    (pGlobalStatsEntry->MibObject.
                     u4FsLsppGlbStatUnKUpstreamIf)++;
                    break;
                case LSPP_RESERVED_RETURN_CODE:
                    (pGlobalStatsEntry->MibObject.u4FsLsppGlbStatRsvdRetCode)++;
                    break;
                case LSPP_LBL_SWITCHED_AT_STACK_DEPTH:
                    (pGlobalStatsEntry->MibObject.
                     u4FsLsppGlbStatReqLblSwitched)++;
                    break;
                case LSPP_LBL_SWITCHED_BUT_NO_MPLS_FWD_AT_STACK_DEPTH:
                    (pGlobalStatsEntry->MibObject.u4FsLsppGlbStatUnLbldOutIf)++;
                    break;
                case LSPP_MAP_FAIL_FEC_TO_LBL_AT_STACK_DEPTH:
                    (pGlobalStatsEntry->MibObject.
                     u4FsLsppGlbStatFecLblMismatch)++;
                    break;
                case LSPP_NO_LBL_ENTRY_AT_STACK_DEPTH:
                    (pGlobalStatsEntry->MibObject.u4FsLsppGlbStatNoLblEntry)++;
                    break;
                case LSPP_PROTOCOL_MISMATCH:
                    (pGlobalStatsEntry->MibObject.
                     u4FsLsppGlbStatProtMismatch)++;
                    break;
                case LSPP_PRE_TERMINATED_REQUEST:
                    (pGlobalStatsEntry->MibObject.u4FsLsppGlbStatPreTermReq)++;
                    break;
                default:
                    /* Undefined return code received. */
                    (pGlobalStatsEntry->MibObject.
                     u4FsLsppGlbStatUndefRetCode)++;
                    break;
            }

            break;
        case LSPP_REPLY_DROPPED:
            (pGlobalStatsEntry->MibObject.u4FsLsppGlbStatReplyDropped)++;
            break;
        case LSPP_REPLY_UNSENT:
            (pGlobalStatsEntry->MibObject.u4FsLsppGlbStatReplyUnSent)++;
            break;
        case LSPP_INVALID_PKT_DROPPED:
            (pGlobalStatsEntry->MibObject.u4FsLsppGlbStatInvalidPktDropped)++;
            break;

        default:
            LSPP_LOG (u4ContextId, LSPP_INVALID_UPDATE_PARAM,
                      "LsppCoreUpdateGlobalStats");
    }
}

/******************************************************************************
 * Function   : LsppCoreGetL3If
 *
 * Description: This function is to get the L3 interface index from mpls 
 *              interface or Mpls tunnel interface by calling the external 
 *              APIS through LSPP Port function
 *
 * Input      : ContextId
 *              u4IfIndex - It may be a Mpls interface or Mpls tunnel interface
 *
 * Output     : pu4L3IfIndex - pointer to the L3 interface corresponds to the
 *                             given interface index
 *
 * Returns    : OSIX_FAILURE/OSIX_SUCCESS
 *
 *****************************************************************************/
INT4
LsppCoreGetL3If (UINT4 u4ContextId, UINT4 u4IfIndex, UINT4 *pu4L3IfIndex)
{

    tLsppExtInParams   *pLsppExtInParams = NULL;
    tLsppExtOutParams  *pLsppExtOutParams = NULL;

    if ((pLsppExtInParams = (tLsppExtInParams *)
         MemAllocMemBlk (LSPP_FSLSPPINPUTPARAMS_POOLID)) == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((pLsppExtOutParams = (tLsppExtOutParams *)
         MemAllocMemBlk (LSPP_FSLSPPOUTPUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        return OSIX_FAILURE;
    }

    MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
    MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));

    pLsppExtInParams->u4ContextId = u4ContextId;
    pLsppExtInParams->u4RequestType = LSPP_CFA_GET_L3_IF;

    pLsppExtInParams->uInParams.u4IfIndex = u4IfIndex;

    /* This external module call is to fetch L3 interface index */
    if (LsppPortHandleExtInteraction (pLsppExtInParams, pLsppExtOutParams) !=
        OSIX_SUCCESS)
    {
        LSPP_LOG (u4ContextId, LSPP_FETCH_FROM_EXT_MOD_FAILED,
                  "LsppCoreGetL3If");
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);
        return OSIX_FAILURE;
    }

    *pu4L3IfIndex = pLsppExtOutParams->unOutParams.u4IfIndex;

    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtInParams);
    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtOutParams);

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : LsppCoreGetSrcIpFromDestIp
 *
 * Description: This function is to get the Source IP address from the 
 *              Destination IP address by calling external APIs through
 *              LSPP Port function.
 *
 * Input      : ContextId
 *              u4DestIp - destination IP address
 *
 * Output     : pu4SrcIp - Pointer to the source IP address
 *
 * Returns    : OSIX_FAILURE/OSIX_SUCCESS
 *
 *****************************************************************************/
INT4
LsppCoreGetSrcIpFromDestIp (UINT4 u4ContextId, UINT4 u4DestIp, UINT4 *pu4SrcIp)
{
    tLsppExtInParams   *pLsppExtInParams = NULL;
    tLsppExtOutParams  *pLsppExtOutParams = NULL;

    if ((pLsppExtInParams = (tLsppExtInParams *)
         MemAllocMemBlk (LSPP_FSLSPPINPUTPARAMS_POOLID)) == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((pLsppExtOutParams = (tLsppExtOutParams *)
         MemAllocMemBlk (LSPP_FSLSPPOUTPUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        return OSIX_FAILURE;
    }

    MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
    MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));

    pLsppExtInParams->u4ContextId = u4ContextId;
    pLsppExtInParams->u4RequestType = LSPP_NETIP_GET_SRC_IP_FRM_DEST_IP;

    pLsppExtInParams->InLsppNodeId.u4NodeType = LSPP_MPLS_ADDR_TYPE_IPV4;
    pLsppExtInParams->InLsppNodeId.MplsRouterId.u4_addr[0] = u4DestIp;

    /* This external module call is to fetch Source Ip to be used for
     * a destination IP */
    if (LsppPortHandleExtInteraction (pLsppExtInParams, pLsppExtOutParams) !=
        OSIX_SUCCESS)
    {
        LSPP_LOG (u4ContextId, LSPP_FETCH_FROM_EXT_MOD_FAILED,
                  "LsppCoreGetSrcIpFromDestIp");
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);
        return OSIX_FAILURE;
    }

    *pu4SrcIp = pLsppExtOutParams->OutLsppNodeId.MplsRouterId.u4_addr[0];

    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtInParams);
    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtOutParams);

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : LsppCoreGetRevPathFromDestIp
 *
 * Description: This function is to get the reverse path info (Tunnel or LSP)
 *              from the destination IP address by calling the external APIS 
 *              through LSPP Port function
 *
 * Input      : ContextId
 *              DestIP -  Destination IP address 
 *              
 * Output     : pLsppPathInfo - pointer to the structure which containing 
 *              informations about the reverse path. (Tunnel or LSP)
 *
 * Returns    : OSIX_FAILURE/OSIX_SUCCESS
 *
 *****************************************************************************/
INT4
LsppCoreGetRevPathFromDestIp (UINT4 u4ContextId, UINT4 u4DestIp,
                              UINT4 *pu4SrcIp, tLsppPathInfo * pLsppPathInfo)
{
    tLsppExtInParams   *pLsppExtInParams = NULL;
    tLsppExtOutParams  *pLsppExtOutParams = NULL;

    if ((pLsppExtInParams = (tLsppExtInParams *)
         MemAllocMemBlk (LSPP_FSLSPPINPUTPARAMS_POOLID)) == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((pLsppExtOutParams = (tLsppExtOutParams *)
         MemAllocMemBlk (LSPP_FSLSPPOUTPUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        return OSIX_FAILURE;
    }

    MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
    MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));

    pLsppExtInParams->u4ContextId = u4ContextId;
    pLsppExtInParams->u4RequestType = LSPP_MPLS_OAM_GET_REV_PATH_INFO;

    pLsppExtInParams->InLsppNodeId.u4NodeType = LSPP_MPLS_ADDR_TYPE_IPV4;
    pLsppExtInParams->InLsppNodeId.MplsRouterId.u4_addr[0] = u4DestIp;

    /* This external module call is to fetch Reverse path information from
     *  Destination IP address */
    if (LsppPortHandleExtInteraction (pLsppExtInParams, pLsppExtOutParams) !=
        OSIX_SUCCESS)
    {
        LSPP_LOG (u4ContextId, LSPP_FETCH_FROM_EXT_MOD_FAILED,
                  "LsppCoreGetRevPathFromDestIp");
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);
        return OSIX_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtInParams);

    if (pLsppExtOutParams->u1PathType == LSPP_MPLS_PATH_TYPE_TNL)
    {
        if (pLsppExtOutParams->OutTnlInfo.TnlInfo.SrcNodeId.u4NodeType ==
            LSPP_MPLS_ADDR_TYPE_IPV4)
        {
            pLsppPathInfo->u1PathType = LSPP_PATH_TYPE_RSVP_IPV4;
            *pu4SrcIp = pLsppExtOutParams->OutTnlInfo.TnlInfo.SrcNodeId.
                MplsRouterId.u4_addr[0];
        }
        else if (pLsppExtOutParams->OutTnlInfo.TnlInfo.SrcNodeId.u4NodeType ==
                 LSPP_MPLS_ADDR_TYPE_IPV6)
        {
            pLsppPathInfo->u1PathType = LSPP_PATH_TYPE_RSVP_IPV6;
            /* Not Supported */
        }
        else if (pLsppExtOutParams->OutTnlInfo.TnlInfo.SrcNodeId.u4NodeType ==
                 LSPP_MPLS_ADDR_TYPE_GLOBAL_NODE_ID)
        {
            pLsppPathInfo->u1PathType = LSPP_PATH_TYPE_STATIC_TNL;
            *pu4SrcIp = pLsppExtOutParams->OutTnlInfo.TnlInfo.SrcNodeId.
                MplsGlobalNodeId.u4NodeId;
        }
        /* Check Tunnel State */
        if (pLsppExtOutParams->unOutParams.TnlInfo.u1OperStatus != MPLS_OPER_UP)
        {
            *pu4SrcIp = 0;
            LSPP_LOG (u4ContextId, LSPP_UNDERLYING_PATH_DOWN,
                      "LsppCoreGetPathInfo");
            MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtOutParams);
            return OSIX_FAILURE;
        }

        MEMCPY (&(pLsppPathInfo->TeTnlInfo), &(pLsppExtOutParams->OutTnlInfo),
                sizeof (tLsppTeTnlInfo));
    }

    else if (pLsppExtOutParams->u1PathType == LSPP_MPLS_PATH_TYPE_LSP)
    {
        if (pLsppExtOutParams->OutLdpInfo.LdpInfo.u4AddrType ==
            LSPP_MPLS_ADDR_TYPE_IPV4)
        {
            pLsppPathInfo->u1PathType = LSPP_PATH_TYPE_LDP_IPV4;
            *pu4SrcIp = pLsppExtOutParams->OutLdpInfo.
                LdpInfo.LdpPrefix.u4_addr[0];
        }
        else if (pLsppExtOutParams->OutLdpInfo.LdpInfo.u4AddrType ==
                 LSPP_MPLS_ADDR_TYPE_IPV6)
        {
            pLsppPathInfo->u1PathType = LSPP_PATH_TYPE_LDP_IPV6;
            /* Not Supported */
        }

        MEMCPY (&(pLsppPathInfo->NonTeInfo), &(pLsppExtOutParams->OutLdpInfo),
                sizeof (tLsppNonTeInfo));
    }
    else
    {
        *pu4SrcIp = 0;
        LSPP_LOG (u4ContextId, LSPP_INVALID_PATH_TYPE,
                  "LsppCoreGetRevPathFromDestIp");
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);
        return OSIX_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtOutParams);
    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : LsppCoreGetIfMtu
 *
 * Description: This function is to get the Maximum transfer unit of the given
 *              L3 interface index by calling the external APIS through 
 *              LSPP port function
 *
 * Input      : ContextId
 *              IfIndex and pMtu
 *
 * Output     : MTU
 *
 * Returns    : OSIX_FAILURE/OSIX_SUCCESS
 *
 *****************************************************************************/
INT4
LsppCoreGetIfMtu (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 *pu2Mtu)
{
    tLsppExtInParams   *pLsppExtInParams = NULL;
    tLsppExtOutParams  *pLsppExtOutParams = NULL;

    if ((pLsppExtInParams = (tLsppExtInParams *)
         MemAllocMemBlk (LSPP_FSLSPPINPUTPARAMS_POOLID)) == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((pLsppExtOutParams = (tLsppExtOutParams *)
         MemAllocMemBlk (LSPP_FSLSPPOUTPUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        return OSIX_FAILURE;
    }

    MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
    MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));

    pLsppExtInParams->u4ContextId = u4ContextId;
    pLsppExtInParams->u4RequestType = LSPP_CFA_GET_MTU;

    pLsppExtInParams->uInParams.u4IfIndex = u4IfIndex;

    /*This external call is to obtain MTU of the interface index */
    if (LsppPortHandleExtInteraction (pLsppExtInParams, pLsppExtOutParams) !=
        OSIX_SUCCESS)
    {
        LSPP_LOG (u4ContextId,
                  LSPP_FETCH_FROM_EXT_MOD_FAILED, "LsppCoreGetIfMtu");
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);
        return OSIX_FAILURE;
    }

    *pu2Mtu = (UINT2) pLsppExtOutParams->unOutParams.u4Mtu;

    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtInParams);
    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtOutParams);

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : LsppCoreGetRevPathFromFwd
 *
 * Description: This function is to get the Reverse path information from the 
 *              received forward path information.
 *
 * Input      : pLsppEchoMsg - pointer to the echo message containing 
 *                             the received information.
 *
 * Output     : pLsppEchoMsg - updated with reverse path information
 *
 * Returns    : OSIX_FAILURE/OSIX_SUCCESS
 *
 *****************************************************************************/
INT4
LsppCoreGetRevPathFromFwd (tLsppEchoMsg * pLsppEchoMsg)
{
    tLsppPathId         LsppPathId;
    tLsppNodeId         LsppNodeId;
    tLsppPathInfo      *pLsppPathInfo = NULL;
    tLsppMepTlv        *pMepTlv = NULL;
    tLsppExtInParams   *pLsppExtInParams = NULL;
    tLsppExtOutParams  *pLsppExtOutParams = NULL;
    UINT1               u1ReturnCode = 0;

    if ((pLsppExtInParams = (tLsppExtInParams *)
         MemAllocMemBlk (LSPP_FSLSPPINPUTPARAMS_POOLID)) == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((pLsppExtOutParams = (tLsppExtOutParams *)
         MemAllocMemBlk (LSPP_FSLSPPOUTPUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        return OSIX_FAILURE;
    }

    MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
    MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));
    MEMSET (&(LsppPathId), 0, sizeof (tLsppPathId));
    MEMSET (&(LsppNodeId), 0, sizeof (tLsppNodeId));

    pLsppPathInfo = &(pLsppEchoMsg->LsppPathInfo);
    pMepTlv = &(pLsppEchoMsg->LsppAchInfo.LsppMepTlv);
    u1ReturnCode = pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReturnCode;

    pLsppExtInParams->u4ContextId = pLsppEchoMsg->u4ContextId;

    /* Get the reverse path from the forward path info when the forward 
     * validation is successful. The path info structure will contain the
     * valid inforamtion when the received label information is valid. 
     * If the received packet sanity or label validation failed. get the
     * reverse path from the forward meg information.*/
    if ((u1ReturnCode != LSPP_MALFORMED_ECHO_PACKET) &&
        (u1ReturnCode != LSPP_TLV_NOT_UNDERSTOOD) &&
        (u1ReturnCode != LSPP_NO_LBL_ENTRY_AT_STACK_DEPTH))
    {
        if (pLsppPathInfo->u1PathType == LSPP_PATH_TYPE_STATIC_TNL)
        {
            pLsppExtInParams->u4RequestType = LSPP_MPLSDB_GET_REV_TNL_INFO;

            pLsppExtInParams->InTnlInfo.SrcNodeId.u4NodeType =
                pLsppPathInfo->TeTnlInfo.TnlInfo.SrcNodeId.u4NodeType;

            MEMCPY (&(pLsppExtInParams->InTnlInfo.SrcNodeId.MplsGlobalNodeId),
                    &(pLsppPathInfo->TeTnlInfo.TnlInfo.SrcNodeId.
                      MplsGlobalNodeId), sizeof (tLsppGlobalNodeId));

            pLsppExtInParams->InTnlInfo.u4SrcTnlId =
                pLsppPathInfo->TeTnlInfo.TnlInfo.u4SrcTnlNum;

            pLsppExtInParams->InTnlInfo.u4LspId =
                pLsppPathInfo->TeTnlInfo.TnlInfo.u4LspNum;

            pLsppExtInParams->InTnlInfo.u4InIf = pLsppEchoMsg->u4IfIndex;

            MEMCPY (&(pLsppExtInParams->InTnlInfo.DstNodeId.MplsGlobalNodeId),
                    &(pLsppPathInfo->TeTnlInfo.TnlInfo.DstNodeId.
                      MplsGlobalNodeId), sizeof (tLsppGlobalNodeId));

            pLsppExtInParams->InTnlInfo.u4DstTnlId =
                pLsppPathInfo->TeTnlInfo.TnlInfo.u4DstTnlNum;
        }
        /* In Case of PW we have already fetched the reverse path Info
         * from the incoming label. This case is hit only if label is valid
         * and is Pseudowire positive scenarios*/
        else if ((pLsppPathInfo->u1PathType == LSPP_PATH_TYPE_STATIC_PW) ||
                 (pLsppPathInfo->u1PathType == LSPP_PATH_TYPE_FEC_128) ||
                 (pLsppPathInfo->u1PathType == LSPP_PATH_TYPE_FEC_129))
        {
            MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtInParams);
            MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtOutParams);
            return OSIX_SUCCESS;
        }
        else
        {
            MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtInParams);
            MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtOutParams);
            return OSIX_FAILURE;
        }
    }
    else
    {
        if (pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_ACH)
        {
            if (pLsppEchoMsg->LsppAchInfo.u1AchTlvPresent &
                LSPP_ACH_IP_LSP_MEP_TLV)
            {
                pLsppExtInParams->u4RequestType = LSPP_MPLSDB_GET_REV_TNL_INFO;

                pLsppExtInParams->InTnlInfo.SrcNodeId.u4NodeType =
                    LSPP_MPLS_ADDR_TYPE_GLOBAL_NODE_ID;

                MEMCPY (&
                        (pLsppExtInParams->InTnlInfo.SrcNodeId.
                         MplsGlobalNodeId),
                        &(pMepTlv->unMepTlv.IpLspMepTlv.GlobalNodeId),
                        sizeof (tLsppGlobalNodeId));

                pLsppExtInParams->InTnlInfo.u4SrcTnlId =
                    (UINT4) pMepTlv->unMepTlv.IpLspMepTlv.u2TnlNum;

                pLsppExtInParams->InTnlInfo.u4LspId =
                    (UINT4) pMepTlv->unMepTlv.IpLspMepTlv.u2LspNum;

                pLsppPathInfo->u1PathType = LSPP_PATH_TYPE_STATIC_TNL;
            }
            else if (pLsppEchoMsg->LsppAchInfo.u1AchTlvPresent &
                     LSPP_ACH_IP_PW_MEP_TLV)
            {
                pLsppExtInParams->u4RequestType =
                    LSPP_L2VPN_GET_PW_INFO_FRM_GENFEC_TYPE2;

                pLsppExtInParams->uInParams.PwFecInfo.u1AgiType =
                    pMepTlv->unMepTlv.IpPwMepTlv.u1AgiType;

                pLsppExtInParams->uInParams.PwFecInfo.u1AgiLen =
                    pMepTlv->unMepTlv.IpPwMepTlv.u1AgiLen;

                MEMCPY (pLsppExtInParams->uInParams.PwFecInfo.au1Agi,
                        pMepTlv->unMepTlv.IpPwMepTlv.au1Agi,
                        pMepTlv->unMepTlv.IpPwMepTlv.u1AgiLen);

                pLsppExtInParams->uInParams.PwFecInfo.DstNodeId.
                    u4NodeType = LSPP_MPLS_ADDR_TYPE_GLOBAL_NODE_ID;

                pLsppExtInParams->uInParams.PwFecInfo.DstNodeId.
                    MplsGlobalNodeId.u4GlobalId =
                    pMepTlv->unMepTlv.IpPwMepTlv.u4GlobalId;

                pLsppExtInParams->uInParams.PwFecInfo.DstNodeId.
                    MplsGlobalNodeId.u4NodeId =
                    pMepTlv->unMepTlv.IpPwMepTlv.u4NodeId;

                pLsppExtInParams->uInParams.PwFecInfo.u4SrcAcId =
                    pMepTlv->unMepTlv.IpPwMepTlv.u4AcId;

                if (LsppPortHandleExtInteraction (pLsppExtInParams,
                                                  pLsppExtOutParams) !=
                    OSIX_SUCCESS)
                {
                    LSPP_LOG (pLsppEchoMsg->u4ContextId,
                              LSPP_FETCH_FROM_EXT_MOD_FAILED,
                              "LsppCoreGetRevPathFromFwd");
                    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                        (UINT1 *) pLsppExtInParams);
                    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                        (UINT1 *) pLsppExtOutParams);
                    return OSIX_FAILURE;
                }

                LsppPathId.u1PathType = LSPP_PATH_TYPE_MEP;

                LsppPathId.unPathId.MepIndices.u4MegIndex =
                    pLsppExtOutParams->OutPwDetail.MegIndices.u4MegIndex;
                LsppPathId.unPathId.MepIndices.u4MeIndex =
                    pLsppExtOutParams->OutPwDetail.MegIndices.u4MeIndex;
                LsppPathId.unPathId.MepIndices.u4MpIndex =
                    pLsppExtOutParams->OutPwDetail.MegIndices.u4MpIndex;

                if (LsppCoreGetPathInfo (pLsppEchoMsg->u4ContextId,
                                         &(LsppPathId), pLsppPathInfo, 0) !=
                    OSIX_SUCCESS)
                {
                    LSPP_LOG (pLsppEchoMsg->u4ContextId,
                              LSPP_GET_PATH_INFO_FROM_PATH_ID_FAILED,
                              "LsppCoreGetRevPathFromFwd");
                    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                        (UINT1 *) pLsppExtInParams);
                    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                        (UINT1 *) pLsppExtOutParams);
                    return OSIX_FAILURE;
                }

                /* Update CC for control channel encapsulation. */
                pLsppEchoMsg->u1CcSelected =
                    pLsppPathInfo->PwDetail.u1CcSelected;

                MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                    (UINT1 *) pLsppExtInParams);
                MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pLsppExtOutParams);
                return OSIX_SUCCESS;
            }
            else if (pLsppEchoMsg->LsppAchInfo.u1AchTlvPresent &
                     LSPP_ACH_ICC_MEP_TLV)
            {
                pLsppExtInParams->u4RequestType =
                    LSPP_MPLS_OAM_GET_MEG_INFO_FROM_ICC_MEG;

                MEMCPY (&(pLsppExtInParams->uInParams.IccMep.au1Icc),
                        &(pLsppEchoMsg->LsppAchInfo.LsppMepTlv.
                          unMepTlv.IccMepTlv.au1Icc), LSPP_MPLS_ICC_LENGTH);

                pLsppExtInParams->uInParams.IccMep.u2MepIndex =
                    pLsppEchoMsg->LsppAchInfo.LsppMepTlv.
                    unMepTlv.IccMepTlv.u2MepIndex;

                MEMCPY (&(pLsppExtInParams->uInParams.IccMep.au1Umc),
                        &(pLsppEchoMsg->LsppAchInfo.LsppMepTlv.
                          unMepTlv.IccMepTlv.au1Umc), LSPP_MPLS_UMC_LENGTH);

                /* This external module call is to fetch
                 * MEG Information from ICC based MEP TLV informations */
                if (LsppPortHandleExtInteraction (pLsppExtInParams,
                                                  pLsppExtOutParams) !=
                    OSIX_SUCCESS)
                {
                    LSPP_LOG (pLsppEchoMsg->u4ContextId,
                              LSPP_FETCH_FROM_EXT_MOD_FAILED,
                              "LsppCoreGetRevPathFromFwd");
                    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                        (UINT1 *) pLsppExtInParams);
                    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                        (UINT1 *) pLsppExtOutParams);
                    return OSIX_FAILURE;
                }
                if (pLsppExtOutParams->OutMegDetail.u1ServiceType ==
                    OAM_SERVICE_TYPE_PW)
                {
                    MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));

                    pLsppExtInParams->u4RequestType =
                        LSPP_L2VPN_GET_PW_INFO_FRM_PW_INDEX;

                    pLsppExtInParams->uInParams.PwFecInfo.u4PwIndex =
                        pLsppExtOutParams->OutMegDetail.unServiceInfo.
                        ServicePwId.u4PwIndex;

                    /* This external module call is to fetch
                     * pseudowire information from PW index */
                    if (LsppPortHandleExtInteraction (pLsppExtInParams,
                                                      pLsppExtOutParams) !=
                        OSIX_SUCCESS)
                    {
                        LSPP_LOG (pLsppEchoMsg->u4ContextId,
                                  LSPP_FETCH_PW_INFO_FAILED,
                                  "LsppCoreGetRevPathFromFwd");
                        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                            (UINT1 *) pLsppExtInParams);
                        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                            (UINT1 *) pLsppExtOutParams);
                        return OSIX_FAILURE;
                    }

                    MEMCPY (&(pLsppPathInfo->PwDetail),
                            &(pLsppExtOutParams->unOutParams.PwDetail),
                            sizeof (tLsppPwDetail));

                    LsppPathId.u1PathType = LSPP_PATH_TYPE_MEP;

                    LsppPathId.unPathId.MepIndices.u4MegIndex =
                        pLsppExtOutParams->OutPwDetail.MegIndices.u4MegIndex;
                    LsppPathId.unPathId.MepIndices.u4MeIndex =
                        pLsppExtOutParams->OutPwDetail.MegIndices.u4MeIndex;
                    LsppPathId.unPathId.MepIndices.u4MpIndex =
                        pLsppExtOutParams->OutPwDetail.MegIndices.u4MpIndex;

                    /* Get PW Path, MEG, underlying tunnel/LSP
                     * info from the configured MEG Indicies */
                    if (LsppCoreGetPathInfo (pLsppEchoMsg->u4ContextId,
                                             &(LsppPathId), pLsppPathInfo, 0) !=
                        OSIX_SUCCESS)
                    {
                        LSPP_LOG (pLsppEchoMsg->u4ContextId,
                                  LSPP_GET_PATH_INFO_FROM_PATH_ID_FAILED,
                                  "LsppCoreGetRevPathFromFwd");
                        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                            (UINT1 *) pLsppExtInParams);
                        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                            (UINT1 *) pLsppExtOutParams);
                        return OSIX_FAILURE;
                    }

                    /* Update CC for control channel encapsulation. */
                    pLsppEchoMsg->u1CcSelected =
                        pLsppPathInfo->PwDetail.u1CcSelected;

                    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                        (UINT1 *) pLsppExtInParams);
                    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                        (UINT1 *) pLsppExtOutParams);

                    return OSIX_SUCCESS;

                }
                else if (pLsppExtOutParams->OutMegDetail.u1ServiceType ==
                         OAM_SERVICE_TYPE_LSP)
                {
                    pLsppExtInParams->u4RequestType =
                        LSPP_MPLSDB_GET_REV_TNL_INFO;

                    pLsppExtInParams->InTnlInfo.SrcNodeId.u4NodeType =
                        pLsppExtOutParams->OutMegDetail.unServiceInfo.
                        ServiceTnlId.SrcNodeId.u4NodeType;

                    MEMCPY (&
                            (pLsppExtInParams->InTnlInfo.SrcNodeId.
                             MplsGlobalNodeId),
                            &(pLsppExtOutParams->OutMegDetail.unServiceInfo.
                              ServiceTnlId.SrcNodeId.MplsGlobalNodeId),
                            sizeof (tLsppGlobalNodeId));

                    pLsppExtInParams->InTnlInfo.u4SrcTnlId =
                        pLsppExtOutParams->OutMegDetail.unServiceInfo.
                        ServiceTnlId.u4SrcTnlNum;

                    pLsppExtInParams->InTnlInfo.u4LspId =
                        pLsppExtOutParams->OutMegDetail.unServiceInfo.
                        ServiceTnlId.u4LspNum;

                    pLsppExtInParams->InTnlInfo.u4InIf =
                        pLsppEchoMsg->u4IfIndex;

                    MEMCPY (&
                            (pLsppExtInParams->InTnlInfo.DstNodeId.
                             MplsGlobalNodeId),
                            &(pLsppExtOutParams->OutMegDetail.unServiceInfo.
                              ServiceTnlId.DstNodeId.MplsGlobalNodeId),
                            sizeof (tLsppGlobalNodeId));
                    pLsppExtInParams->InTnlInfo.u4DstTnlId =
                        pLsppExtOutParams->OutMegDetail.unServiceInfo.
                        ServiceTnlId.u4DstTnlNum;
                }
            }
        }
        else if ((pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_IP) ||
                 (pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_ACH_IP))
        {
            pLsppExtInParams->u4ContextId = pLsppEchoMsg->u4ContextId;
            pLsppExtInParams->u4RequestType = LSPP_MPLS_OAM_GET_REV_PATH_INFO;

            pLsppExtInParams->InLsppNodeId.u4NodeType =
                LSPP_MPLS_ADDR_TYPE_IPV4;
            pLsppExtInParams->InLsppNodeId.MplsRouterId.u4_addr[0] =
                pLsppEchoMsg->LsppIpHeader.u4Dest;
        }
    }

    /* This external module call is to fetch tunnel information
     * from Tunnel FEC info or from Source Tunnel Mep Information */
    if (LsppPortHandleExtInteraction (pLsppExtInParams,
                                      pLsppExtOutParams) != OSIX_SUCCESS)
    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId,
                  LSPP_FETCH_FROM_EXT_MOD_FAILED, "LsppCoreGetRevPathFromFwd");
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);
        return OSIX_FAILURE;
    }

    if (pLsppExtOutParams->u1PathType == LSPP_MPLS_PATH_TYPE_TNL)
    {
        if (pLsppExtOutParams->OutTnlInfo.TnlInfo.SrcNodeId.u4NodeType ==
            LSPP_MPLS_ADDR_TYPE_IPV4)
        {
            pLsppPathInfo->u1PathType = LSPP_PATH_TYPE_RSVP_IPV4;
        }
        else if (pLsppExtOutParams->OutTnlInfo.TnlInfo.SrcNodeId.u4NodeType ==
                 LSPP_MPLS_ADDR_TYPE_IPV6)
        {
            pLsppPathInfo->u1PathType = LSPP_PATH_TYPE_RSVP_IPV6;
        }
        else if (pLsppExtOutParams->OutTnlInfo.TnlInfo.SrcNodeId.u4NodeType ==
                 LSPP_MPLS_ADDR_TYPE_GLOBAL_NODE_ID)
        {
            pLsppPathInfo->u1PathType = LSPP_PATH_TYPE_STATIC_TNL;
        }
        /* Check Tunnel State */
        if (pLsppExtOutParams->unOutParams.TnlInfo.u1OperStatus != MPLS_OPER_UP)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId, LSPP_UNDERLYING_PATH_DOWN,
                      "LsppCoreGetRevPathFromFwd");
            MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtInParams);
            MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtOutParams);
            return OSIX_FAILURE;
        }

        MEMCPY (&(pLsppPathInfo->TeTnlInfo), &(pLsppExtOutParams->OutTnlInfo),
                sizeof (tLsppTeTnlInfo));
    }

    /* Get the reverse path MEG information when the path is associated 
     * or unidirectional. The forward path MEG will be used for co-routed 
     * bidirectional LSP.*/

    if (pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_ACH)
    {
        if ((pLsppExtOutParams->OutTnlInfo.u1TnlMode == LSPP_ASSOCIATED_LSP) ||
            (pLsppExtOutParams->OutTnlInfo.u1TnlMode == LSPP_UNIDIRECTIONAL_LSP)
            || (pLsppExtOutParams->OutTnlInfo.u1TnlMode == LSPP_COROUTED_LSP))
        {
            MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
            MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));

            pLsppExtInParams->u4RequestType = LSPP_MPLS_OAM_GET_MEG_INFO;

            MEMCPY (&(pLsppExtInParams->InMegInfo.MegIndices),
                    &(pLsppPathInfo->TeTnlInfo.MegIndices),
                    sizeof (tLsppMegIndices));

            if (LsppPortHandleExtInteraction (pLsppExtInParams,
                                              pLsppExtOutParams) !=
                OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppEchoMsg->u4ContextId,
                          LSPP_FETCH_FROM_EXT_MOD_FAILED,
                          "LsppCoreGetRevPathFromFwd");
                MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                    (UINT1 *) pLsppExtInParams);
                MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pLsppExtOutParams);
                return OSIX_FAILURE;
            }
            /* Check the Meg State */
            if ((pLsppExtOutParams->unOutParams.MegDetail.u1MeState &
                 OAM_ME_OPER_STATUS_MEG_DOWN) ||
                (pLsppExtOutParams->unOutParams.MegDetail.u1MeState &
                 OAM_ME_OPER_STATUS_ME_DOWN) ||
                (pLsppExtOutParams->unOutParams.MegDetail.u1MeState &
                 OAM_ME_OPER_STATUS_PATH_DOWN))
            {
                LSPP_LOG (pLsppEchoMsg->u4ContextId, LSPP_UNDERLYING_PATH_DOWN,
                          "LsppCoreGetRevPathFromFwd");
                MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                    (UINT1 *) pLsppExtInParams);
                MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pLsppExtOutParams);
                return OSIX_FAILURE;
            }

            MEMCPY (&(pLsppPathInfo->MegDetail),
                    &(pLsppExtOutParams->OutMegDetail),
                    sizeof (tLsppMegDetail));
        }
    }

    /* Updating Source Ip Address as Node-Id */

    if (pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_IP ||
        pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_ACH_IP)
    {
        if (LsppCoreGetNodeId (pLsppEchoMsg->u4ContextId,
                               &LsppNodeId) != OSIX_SUCCESS)
        {
            MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtInParams);
            MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtOutParams);
            return OSIX_FAILURE;
        }

        pLsppEchoMsg->LsppIpHeader.u4Src = LsppNodeId.MplsGlobalNodeId.u4NodeId;
    }

    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtInParams);
    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtOutParams);

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : LsppCoreUpdateMepOrMipTlv
 *
 * Description: This function is to update the MEP or MIP TLV in the
 *              EchoMsg structure.
 * 
 * Input      : pLsppEchoMsg - with Path informations
 *
 * Output     : pLsppEchoMsg - with updated MEP or MIP TLV informations
 *
 * Returns    : OSIX_FAILURE/OSIX_SUCCESS=
 *****************************************************************************/
VOID
LsppCoreUpdateMepOrMipTlv (tLsppEchoMsg * pEchoMsg)
{
    pEchoMsg->LsppAchInfo.u1AchTlvPresent = 0;

    if (pEchoMsg->LsppPathInfo.MegDetail.u1MpType == LSPP_ME_MP_TYPE_MEP)
    {
        LsppCoreUpdateMepTlv
            ((pEchoMsg->LsppPathInfo.MegDetail.u1OperatorType),
             pEchoMsg->LsppPathInfo.MegDetail.u1ServiceType,
             &(pEchoMsg->LsppPathInfo), &(pEchoMsg->LsppAchInfo));
    }
    else if (pEchoMsg->LsppPathInfo.MegDetail.u1MpType == LSPP_ME_MP_TYPE_MIP)
    {
        if (pEchoMsg->LsppPathInfo.MegDetail.u1OperatorType ==
            OAM_OPERATOR_TYPE_IP)
        {
            MEMCPY (&(pEchoMsg->LsppAchInfo.LsppMipTlv.GlobalNodeId),
                    &(pEchoMsg->LsppPathInfo.MegDetail.MplsNodeId),
                    sizeof (tMplsGlobalNodeId));

            pEchoMsg->LsppAchInfo.LsppMipTlv.u4IfNum =
                pEchoMsg->LsppPathInfo.MegDetail.u4MpIfIndex;

            pEchoMsg->LsppAchInfo.u1AchTlvPresent =
                (UINT1) (pEchoMsg->LsppAchInfo.
                         u1AchTlvPresent | LSPP_ACH_MIP_TLV);
        }

    }

    return;
}

/******************************************************************************
 * Function   : LsppCoreUpdateMepTlv
 *
 * Description: This function is to update the MEP TLV in the
 *              EchoMsg structure.
 * 
 * Input      : u1OperatorType - Operator type of the MEG (ICC/IP)
 *              u1ServiceType  - Tunnel or Pseudowire
 *              pMegDetail     - Meg Informations
 *
 * Output     : pLsppAchInfo -  with updated MEP TLV informations
 *
 * Returns    : None
 *****************************************************************************/
VOID
LsppCoreUpdateMepTlv (UINT1 u1OperatorType, UINT1 u1ServiceType,
                      tLsppPathInfo * pLsppPathInfo,
                      tLsppAchInfo * pLsppAchInfo)
{
    tLsppMegDetail     *pMegDetail = NULL;
    tLsppPwDetail      *pPwDetail = NULL;

    pMegDetail = &(pLsppPathInfo->MegDetail);
    pPwDetail = &(pLsppPathInfo->PwDetail);

    if (u1OperatorType == OAM_OPERATOR_TYPE_ICC)
    {
        LsppUtilUpdateIccMepTlv (pMegDetail,
                                 &(pLsppAchInfo->LsppMepTlv.
                                   unMepTlv.IccMepTlv));

        pLsppAchInfo->u1AchTlvPresent =
            pLsppAchInfo->u1AchTlvPresent | LSPP_ACH_ICC_MEP_TLV;
    }
    else if ((u1OperatorType == OAM_OPERATOR_TYPE_IP) &&
             (u1ServiceType == OAM_SERVICE_TYPE_PW))
    {

        LsppUtilUpdateIpPwMepTlv (pPwDetail, &(pLsppAchInfo->LsppMepTlv.
                                               unMepTlv.IpPwMepTlv));

        pLsppAchInfo->u1AchTlvPresent =
            (pLsppAchInfo->u1AchTlvPresent | LSPP_ACH_IP_PW_MEP_TLV);
    }
    else if ((u1OperatorType == OAM_OPERATOR_TYPE_IP) &&
             (u1ServiceType == OAM_SERVICE_TYPE_LSP))
    {
        LsppUtilUpdateIpLspMepTlv (pLsppPathInfo, &(pLsppAchInfo->LsppMepTlv.
                                                    unMepTlv.IpLspMepTlv));

        pLsppAchInfo->u1AchTlvPresent =
            (pLsppAchInfo->u1AchTlvPresent | LSPP_ACH_IP_LSP_MEP_TLV);
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : LsppCoreProcessRequest
 * DESCRIPTION      : This function is to process the request and reply
 * Input(s)         : pLsppEchoMsg - Echo message
 *                    pLsppPingTraceEntry - Ping trace entry obtained based on 
 *                    received Pkt info
 *                     pLsppEchoSequenceEntry - Echo Seq entry obtained 
 *                     based on received Pkt info
 *                     pLsppEchoPktRxTime - Packet Received Time 
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 ***************************************************************************/
PUBLIC UINT4
LsppCoreProcessRequest (tLsppEchoMsg * pLsppEchoMsg,
                        tLsppFsLsppPingTraceTableEntry * pLsppPingTraceEntry,
                        tLsppFsLsppEchoSequenceTableEntry
                        * pLsppEchoSequenceEntry,
                        tLsppSysTime * pLsppEchoPktRxTime, UINT1 u1RcvdMsgType)
{
    if ((u1RcvdMsgType == LSPP_ECHO_REQUEST) &&
        ((pLsppPingTraceEntry != NULL) && (pLsppEchoSequenceEntry != NULL)))
    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId,
                  LSPP_INVALID_PING_ENTRY_FOR_ECHO_REQUEST,
                  "LsppCoreProcessRequest");
        return OSIX_FAILURE;
    }

    if (u1RcvdMsgType == LSPP_ECHO_REQUEST)
    {
        /*Sanity validation failure */
        if (pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReturnCode != 0)
        {
            if (LsppCoreSendEchoReply (pLsppEchoMsg) != OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppEchoMsg->u4ContextId,
                          LSPP_SEND_ECHO_REPLY_FAILED,
                          "LsppCoreProcessRequest");
                LsppCoreUpdateGlobalStats (pLsppEchoMsg->u4ContextId,
                                           LSPP_REPLY_UNSENT, NULL);
                return OSIX_FAILURE;
            }
            return OSIX_SUCCESS;
        }
    }

    /* Peform FEC and Label validation when the Target FEC is present in 
     * the Echo packet.
     * */
    if (pLsppEchoMsg->LsppPduInfo.u2TlvsPresent & LSPP_TARGET_FEC_TLV)
    {
        if ((pLsppEchoMsg->LsppPduInfo.LsppHeader.u1MessageType ==
             LSPP_ECHO_REQUEST) ||
            ((pLsppEchoMsg->LsppPduInfo.LsppHeader.u1MessageType ==
              LSPP_ECHO_REPLY) &&
             (pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReturnCode ==
              LSPP_EGRESS_FOR_FEC_AT_STACK_DEPTH)))
        {
            if ((u1RcvdMsgType == LSPP_ECHO_REPLY) &&
                (pLsppPingTraceEntry != NULL))
            {
                if (LsppCoreReversePathValidate (pLsppEchoMsg,
                                                 pLsppPingTraceEntry) !=
                    OSIX_SUCCESS)
                {
                    /* Specifying Validaiton of reverse direction has failed */
                    pLsppPingTraceEntry->MibObject.
                        i4FsLsppStatusPathDirection =
                        LSPP_VALIDATION_DIRECTION_REVERSE;

                    LSPP_LOG (pLsppEchoMsg->u4ContextId,
                              LSPP_REVERSE_PATH_VALIDATION_FAILED,
                              "LsppCoreProcessRequest");
                    return OSIX_FAILURE;
                }
            }

            if (LsppCoreFecLabelValidate (pLsppEchoMsg) != OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppEchoMsg->u4ContextId,
                          LSPP_FEC_LBL_VALIDATE_FAILED,
                          "LsppCoreProcessRequest");

                if ((u1RcvdMsgType == LSPP_ECHO_REPLY) &&
                    (pLsppPingTraceEntry != NULL))
                {
                    /* Specifying Validaiton of reverse direction has failed */
                    pLsppPingTraceEntry->MibObject.
                        i4FsLsppStatusPathDirection =
                        LSPP_VALIDATION_DIRECTION_REVERSE;

                    LSPP_LOG (pLsppEchoMsg->u4ContextId,
                              LSPP_REVERSE_PATH_VALIDATION_FAILED,
                              "LsppCoreProcessRequest");
                }

                return OSIX_FAILURE;
            }
        }
    }

    /* Update the ping or traceroute results in the database. */
    if ((u1RcvdMsgType == LSPP_ECHO_REPLY) && (pLsppPingTraceEntry != NULL))
    {
        /* 1. Update Sequence number table,
         * 2. Update Hop Info if DSMAP And If LBL STK TLV Is present
         * 3. Update Pingtrace node*/

        if (pLsppPingTraceEntry->MibObject.i4FsLsppStatus ==
            LSPP_ECHO_IN_PROGRESS)
        {
            LsppCoreUpdateEchoReplyStats (pLsppEchoMsg, pLsppPingTraceEntry,
                                          pLsppEchoSequenceEntry,
                                          pLsppEchoPktRxTime);
        }
        else
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_ECHO_ALREADY_COMPLETED,
                      "LsppCoreProcessRequest",
                      pLsppPingTraceEntry->MibObject.u4FsLsppSenderHandle);
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppCoreUpdateEchoHopInfo
 * DESCRIPTION      : This function is to update echo hop info
 * Input(s)         : 
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 ***************************************************************************/
PRIVATE UINT4
LsppCoreUpdateHopInfo (tLsppEchoMsg * pLsppEchoMsg,
                       tLsppFsLsppPingTraceTableEntry * pPingTraceEntry,
                       UINT4 u4Rtt)
{
    tLsppFsLsppHopTableEntry *pLsppHopTable = NULL;
    tLsppFsLsppHopTableEntry *pLsppHopInfoEntry = NULL;
    tLsppDsMapTlv      *pLsppDsMapTlv = NULL;
    tLsppIfLblStkTlv   *pLsppIfLblStkTlv = NULL;
    tLsppMipTlv        *pLsppMipTlv = NULL;
    UINT4               u4TempAddr = 0;
    UINT1              *pu1LabelStack = NULL;
    UINT1              *pu1ExpStack = NULL;

    pLsppHopTable =
        (tLsppFsLsppHopTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPHOPTABLE_POOLID);

    if (pLsppHopTable == NULL)
    {
        LSPP_CMN_TRC (pLsppEchoMsg->u4ContextId,
                      LSPP_MEM_ALLOC_FAIL, "LsppCoreUpdateHopInfo");
        return OSIX_FAILURE;
    }

    MEMSET (pLsppHopTable, 0, sizeof (tLsppFsLsppHopTableEntry));

    pLsppHopTable->MibObject.u4FsLsppContextId = pLsppEchoMsg->u4ContextId;

    pLsppHopTable->MibObject.u4FsLsppSenderHandle =
        pLsppEchoMsg->LsppPduInfo.LsppHeader.u4SenderHandle;

    if (pPingTraceEntry->MibObject.i4FsLsppRequestType == LSPP_REQ_TYPE_PING)
    {
        /* There will always be a single Hop entry when the request type is 
         * ping and it is to an intermediate node with Dsmap request. 
         * The same entry will get updated for the repeat count.
         */
        pLsppHopTable->MibObject.u4FsLsppHopIndex = 1;
    }
    else if (pPingTraceEntry->MibObject.i4FsLsppRequestType ==
             LSPP_REQ_TYPE_TRACE_ROUTE)
    {
        pLsppHopTable->MibObject.u4FsLsppHopIndex =
            pLsppEchoMsg->LsppPduInfo.LsppHeader.u4SequenceNum;
    }

    pLsppHopInfoEntry = LsppGetFsLsppHopTable (pLsppHopTable);

    if (pLsppHopInfoEntry == NULL)
    {
        pLsppHopInfoEntry = LsppFsLsppHopTableCreateApi (pLsppHopTable);

        if (pLsppHopInfoEntry == NULL)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_ALLOC_HOP_ENTRY_FAILED,
                      "LsppCoreUpdateHopInfo",
                      pLsppHopTable->MibObject.u4FsLsppHopIndex);
            MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                                (UINT1 *) pLsppHopTable);
            return OSIX_FAILURE;
        }
    }

    MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID, (UINT1 *) pLsppHopTable);

    if (pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_IP ||
        pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_ACH_IP)
    {
        if (pLsppEchoMsg->LsppIpHeader.u1Version == LSPP_ENCAP_IPV4_HDR)
        {
            pLsppHopInfoEntry->MibObject.i4FsLsppHopAddrType =
                LSPP_IPV4_NUMBERED;

            /* This conversion is required to copy UINT4 type variable 
             * into an UINT1 array variable.*/
            u4TempAddr = OSIX_NTOHL (pLsppEchoMsg->LsppIpHeader.u4Src);

            MEMCPY (pLsppHopInfoEntry->MibObject.au1FsLsppHopAddr,
                    &(u4TempAddr), LSPP_IPV4_ADDR_LEN);

            pLsppHopInfoEntry->MibObject.
                i4FsLsppHopAddrLen = LSPP_IPV4_ADDR_LEN;
        }
    }
    else if (pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_ACH)
    {
        pLsppHopInfoEntry->MibObject.i4FsLsppHopAddrType =
            LSPP_ADDR_TYPE_NOT_APPLICABLE;

        /* Updating Replying Router Details */
        if (pLsppEchoMsg->LsppAchInfo.u1AchTlvPresent & LSPP_ACH_MIP_TLV)
        {
            pLsppMipTlv = &(pLsppEchoMsg->LsppAchInfo.LsppMipTlv);

            pLsppHopInfoEntry->MibObject.u4FsLsppHopGlobalId =
                pLsppMipTlv->GlobalNodeId.u4GlobalId;

            pLsppHopInfoEntry->MibObject.u4FsLsppHopId =
                pLsppMipTlv->GlobalNodeId.u4NodeId;

            pLsppHopInfoEntry->MibObject.u4FsLsppHopIfNum =
                pLsppMipTlv->u4IfNum;
        }

        if (pLsppEchoMsg->LsppAchInfo.u1AchTlvPresent & LSPP_ACH_IP_LSP_MEP_TLV)
        {
            pLsppHopInfoEntry->MibObject.u4FsLsppHopGlobalId =
                pLsppEchoMsg->LsppAchInfo.LsppMepTlv.unMepTlv.
                IpLspMepTlv.GlobalNodeId.u4GlobalId;

            pLsppHopInfoEntry->MibObject.u4FsLsppHopId =
                pLsppEchoMsg->LsppAchInfo.LsppMepTlv.unMepTlv.
                IpLspMepTlv.GlobalNodeId.u4NodeId;
        }
        else if (pLsppEchoMsg->LsppAchInfo.u1AchTlvPresent &
                 LSPP_ACH_IP_PW_MEP_TLV)
        {
            pLsppHopInfoEntry->MibObject.u4FsLsppHopGlobalId =
                pLsppEchoMsg->LsppAchInfo.LsppMepTlv.unMepTlv.
                IpPwMepTlv.u4GlobalId;

            pLsppHopInfoEntry->MibObject.u4FsLsppHopId =
                pLsppEchoMsg->LsppAchInfo.LsppMepTlv.unMepTlv.
                IpPwMepTlv.u4NodeId;
        }
        else if (pLsppEchoMsg->LsppAchInfo.u1AchTlvPresent &
                 LSPP_ACH_ICC_MEP_TLV)
        {
            MEMCPY (pLsppHopInfoEntry->MibObject.au1FsLsppHopIcc,
                    pLsppEchoMsg->LsppAchInfo.LsppMepTlv.
                    unMepTlv.IccMepTlv.au1Icc, LSPP_MPLS_ICC_LENGTH);

            pLsppHopInfoEntry->MibObject.i4FsLsppHopIccLen =
                LSPP_MPLS_ICC_LENGTH;

            MEMCPY (pLsppHopInfoEntry->MibObject.au1FsLsppHopUMC,
                    pLsppEchoMsg->LsppAchInfo.LsppMepTlv.
                    unMepTlv.IccMepTlv.au1Umc, LSPP_MPLS_UMC_LENGTH);

            pLsppHopInfoEntry->MibObject.i4FsLsppHopUMCLen =
                LSPP_MPLS_UMC_LENGTH;

            pLsppHopInfoEntry->MibObject.u4FsLsppHopMepIndex =
                pLsppEchoMsg->LsppAchInfo.LsppMepTlv.
                unMepTlv.IccMepTlv.u2MepIndex;
        }
    }

    /* Updating Replying Router DownStream Details */
    if ((pLsppEchoMsg->LsppPduInfo.u2TlvsPresent & LSPP_DSMAP_TLV))
    {
        pLsppDsMapTlv = &(pLsppEchoMsg->LsppPduInfo.LsppDsMapTlv);

        pLsppHopInfoEntry->MibObject.u4FsLsppHopDsMtu =
            (UINT4) pLsppDsMapTlv->u2Mtu;

        pLsppHopInfoEntry->MibObject.i4FsLsppHopDsAddrType =
            pLsppDsMapTlv->u1AddrType;

        if ((pLsppDsMapTlv->u1AddrType == LSPP_IPV4_NUMBERED) ||
            (pLsppDsMapTlv->u1AddrType == LSPP_IPV4_UNNUMBERED))
        {
            /* This conversion is required to copy UINT4 type variable 
             * into an UINT1 array variable.*/
            u4TempAddr = OSIX_NTOHL (pLsppDsMapTlv->DsIpAddr.u4_addr[0]);

            MEMCPY (pLsppHopInfoEntry->MibObject.au1FsLsppHopDsIPAddr,
                    &(u4TempAddr), LSPP_IPV4_ADDR_LEN);

            pLsppHopInfoEntry->MibObject.
                i4FsLsppHopDsIPAddrLen = LSPP_IPV4_ADDR_LEN;

            /* This conversion is required to copy UINT4 type variable 
             * into an UINT1 array variable.*/
            u4TempAddr = OSIX_NTOHL (pLsppDsMapTlv->DsIfAddr.u4_addr[0]);

            MEMCPY (pLsppHopInfoEntry->MibObject.au1FsLsppHopDsIfAddr,
                    &(u4TempAddr), LSPP_IPV4_ADDR_LEN);

            pLsppHopInfoEntry->MibObject.
                i4FsLsppHopDsIfAddrLen = LSPP_IPV4_ADDR_LEN;
        }
        else if ((pLsppDsMapTlv->u1AddrType == LSPP_IPV6_NUMBERED) ||
                 (pLsppDsMapTlv->u1AddrType == LSPP_IPV6_UNNUMBERED))
        {
            MEMCPY (pLsppHopInfoEntry->MibObject.au1FsLsppHopDsIPAddr,
                    pLsppDsMapTlv->DsIpAddr.u1_addr, LSPP_IPV6_ADDR_LEN);

            pLsppHopInfoEntry->MibObject.
                i4FsLsppHopDsIPAddrLen = LSPP_IPV6_ADDR_LEN;

            MEMCPY (pLsppHopInfoEntry->MibObject.au1FsLsppHopDsIfAddr,
                    pLsppDsMapTlv->DsIfAddr.u1_addr, LSPP_IPV6_ADDR_LEN);

            pLsppHopInfoEntry->MibObject.
                i4FsLsppHopDsIfAddrLen = LSPP_IPV6_ADDR_LEN;
        }
        else if (pLsppDsMapTlv->u1AddrType == LSPP_ADDR_TYPE_NOT_APPLICABLE)
        {
            /*IP addresses are not applcable for this type. */
        }

        pu1LabelStack = pLsppHopInfoEntry->MibObject.au1FsLsppHopDsLabelStack;
        pu1ExpStack = pLsppHopInfoEntry->MibObject.au1FsLsppHopDsLabelExp;

        LsppUtilUpdateLblStk (pLsppDsMapTlv->DsLblInfo,
                              pLsppDsMapTlv->u1DsLblStackDepth,
                              pu1LabelStack,
                              &(pLsppHopInfoEntry->MibObject.
                                i4FsLsppHopDsLabelStackLen),
                              pu1ExpStack,
                              &(pLsppHopInfoEntry->MibObject.
                                i4FsLsppHopDsLabelExpLen));

    }

    /* Updating Replying Router Received Information details */
    if (pLsppEchoMsg->LsppPduInfo.u2TlvsPresent &
        LSPP_INTERFACE_AND_LBL_STACK_TLV)
    {
        pLsppIfLblStkTlv = &(pLsppEchoMsg->LsppPduInfo.LsppIfLblStkTlv);

        pLsppHopInfoEntry->MibObject.i4FsLsppHopRxAddrType =
            pLsppIfLblStkTlv->u1AddrType;

        if ((pLsppIfLblStkTlv->u1AddrType == LSPP_IPV4_NUMBERED) ||
            (pLsppIfLblStkTlv->u1AddrType == LSPP_IPV4_UNNUMBERED))
        {

            /* This conversion is required to copy UINT4 type variable 
             * into an UINT1 array variable.*/
            u4TempAddr = OSIX_NTOHL (pLsppIfLblStkTlv->RxIpAddr.u4_addr[0]);

            MEMCPY (pLsppHopInfoEntry->MibObject.au1FsLsppHopRxIPAddr,
                    &(u4TempAddr), LSPP_IPV4_ADDR_LEN);

            pLsppHopInfoEntry->MibObject.
                i4FsLsppHopRxIPAddrLen = LSPP_IPV4_ADDR_LEN;

            /* This conversion is required to copy UINT4 type variable 
             * into an UINT1 array variable.*/
            u4TempAddr = OSIX_NTOHL (pLsppIfLblStkTlv->RxIfAddr.u4_addr[0]);

            MEMCPY (pLsppHopInfoEntry->MibObject.au1FsLsppHopRxIfAddr,
                    &(u4TempAddr), LSPP_IPV4_ADDR_LEN);

            pLsppHopInfoEntry->MibObject.
                i4FsLsppHopRxIfAddrLen = LSPP_IPV4_ADDR_LEN;
        }
        else if ((pLsppIfLblStkTlv->u1AddrType == LSPP_IPV6_NUMBERED) ||
                 (pLsppIfLblStkTlv->u1AddrType == LSPP_IPV6_UNNUMBERED))
        {
            MEMCPY (pLsppHopInfoEntry->MibObject.au1FsLsppHopRxIPAddr,
                    pLsppIfLblStkTlv->RxIpAddr.u1_addr, LSPP_IPV6_ADDR_LEN);

            pLsppHopInfoEntry->MibObject.
                i4FsLsppHopRxIPAddrLen = LSPP_IPV6_ADDR_LEN;

            MEMCPY (pLsppHopInfoEntry->MibObject.au1FsLsppHopRxIfAddr,
                    pLsppIfLblStkTlv->RxIfAddr.u1_addr, LSPP_IPV6_ADDR_LEN);

            pLsppHopInfoEntry->MibObject.
                i4FsLsppHopRxIfAddrLen = LSPP_IPV6_ADDR_LEN;
        }
        else if (pLsppIfLblStkTlv->u1AddrType == LSPP_ADDR_TYPE_NOT_APPLICABLE)
        {
            /*IP addresses are not applcable for this type. */
        }

        pu1LabelStack = pLsppHopInfoEntry->MibObject.au1FsLsppHopRxLabelStack;
        pu1ExpStack = pLsppHopInfoEntry->MibObject.au1FsLsppHopRxLabelExp;

        LsppUtilUpdateLblStk (pLsppIfLblStkTlv->RxLblInfo,
                              pLsppIfLblStkTlv->u1LblStackDepth,
                              pu1LabelStack,
                              &(pLsppHopInfoEntry->MibObject.
                                i4FsLsppHopRxLabelStackLen),
                              pu1ExpStack,
                              &(pLsppHopInfoEntry->MibObject.
                                i4FsLsppHopRxLabelExpLen));
    }

    /* Updating the Round trip time for this Instance */
    pLsppHopInfoEntry->MibObject.u4FsLsppHopRtt = u4Rtt;

    pPingTraceEntry->u1LastUpdatedHopIndex =
        (UINT1) (pLsppHopInfoEntry->MibObject.u4FsLsppHopIndex);

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppCoreUpdateEchoSeqTableInfo
 * DESCRIPTION      : This function is to update the echo sequence table Info
 * Input(s)         : pLsppEchoMsg - Echo message where received packet Info is
 *                                   present
 *                    pLsppEchoSequenceEntry - Echo Seq Entry corresponding to 
 *                                          the received reply
 *                    
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 ***************************************************************************/
VOID
LsppCoreUpdateEchoSeqTableInfo (tLsppEchoMsg * pLsppEchoMsg,
                                tLsppFsLsppEchoSequenceTableEntry *
                                pLsppEchoSequenceEntry)
{
    UINT1               u1ReturnCode = 0;
    UINT1               u1ReturnSubCode = 0;
    UINT1               u1ReturnCodeStrnLen = 0;

    /* Updating Statistics in Sequence number table */
    /* Fetch Return Code and return Sub Code */
    u1ReturnCode = pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReturnCode;
    u1ReturnSubCode = pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReturnSubCode;

    /* Updating Return Code and Return Sub-Code */
    pLsppEchoSequenceEntry->MibObject.u4FsLsppReturnCode = (UINT4) u1ReturnCode;

    pLsppEchoSequenceEntry->MibObject.u4FsLsppReturnSubCode =
        (UINT4) u1ReturnSubCode;

    u1ReturnCodeStrnLen = sizeof (pLsppEchoSequenceEntry->MibObject.
                                  au1FsLsppReturnCodeStr);

    /* Filling Return Code String */
    if (u1ReturnSubCode != 0)
    {
        SNPRINTF ((CHR1 *)
                  pLsppEchoSequenceEntry->MibObject.au1FsLsppReturnCodeStr,
                  (u1ReturnCodeStrnLen),
                  gapReturnCodeString[u1ReturnCode], u1ReturnSubCode);
    }
    else
    {
        MEMCPY (pLsppEchoSequenceEntry->MibObject.au1FsLsppReturnCodeStr,
                gapReturnCodeString[u1ReturnCode], (u1ReturnCodeStrnLen - 1));
    }

    pLsppEchoSequenceEntry->MibObject.i4FsLsppReturnCodeStrLen =
        u1ReturnCodeStrnLen - 1;

    pLsppEchoSequenceEntry->u1Status = LSPP_ECHO_SEQ_COMPLETED;

    LsppTmrStopTmr (&(pLsppEchoSequenceEntry->WFRTimer));

    LSPP_LOG (pLsppEchoMsg->u4ContextId,
              LSPP_WFR_TIMER_STOPPED, "LsppCoreUpdateEchoSeqTableInfo");
}

/***************************************************************************
 * FUNCTION NAME    : LsppCoreUpdatePingTraceInfo
 * DESCRIPTION      : This function is to update the Ping trace table Info
 * Input(s)         : pLsppEchoMsg - Echo message where received packet Info is
 *                                   present
 *                    pPingTraceEntry - Ping trace Entry corresponding to 
 *                                          the received reply
 *                    
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE 
 ***************************************************************************/
VOID
LsppCoreUpdatePingTraceInfo (tLsppEchoMsg * pLsppEchoMsg,
                             tLsppFsLsppPingTraceTableEntry *
                             pLsppPingTraceEntry)
{
    tLsppMipTlv        *pLsppMipTlv = NULL;
    UINT4               u4TempAddr = 0;

    /* Incrementing Success Count */
    if ((pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReturnCode ==
         LSPP_LBL_SWITCHED_AT_STACK_DEPTH) ||
        (pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReturnCode ==
         LSPP_EGRESS_FOR_FEC_AT_STACK_DEPTH)||
	(pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReturnCode == 
	 LSPP_NO_LBL_ENTRY_AT_STACK_DEPTH))
    {
        pLsppPingTraceEntry->u4SuccessCount =
            pLsppPingTraceEntry->u4SuccessCount + 1;
    }

    /* Incrementing Received packet Count */
    pLsppPingTraceEntry->MibObject.u4FsLsppPktsRx =
        pLsppPingTraceEntry->MibObject.u4FsLsppPktsRx + 1;

    if ((pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_IP) ||
        (pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_ACH_IP))
    {
        if (pLsppEchoMsg->LsppIpHeader.u1Version == LSPP_ENCAP_IPV4_HDR)
        {
            pLsppPingTraceEntry->MibObject.
                i4FsLsppResponderAddrType = LSPP_IPV4_NUMBERED;

            /* This conversion is required to copy UINT4 type variable 
             * into an UINT1 array variable.*/
            u4TempAddr = OSIX_NTOHL (pLsppEchoMsg->LsppIpHeader.u4Src);

            MEMCPY (pLsppPingTraceEntry->MibObject.au1FsLsppResponderAddr,
                    &(u4TempAddr), LSPP_IPV4_ADDR_LEN);

            pLsppPingTraceEntry->MibObject.
                i4FsLsppResponderAddrLen = LSPP_IPV4_ADDR_LEN;
        }
    }
    else if (pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_ACH)
    {
        /* Assinging Not Applicable Address type */
        pLsppPingTraceEntry->MibObject.i4FsLsppResponderAddrType = 0;

        /* Assigning Node Id and Global Id */
        if (pLsppEchoMsg->LsppAchInfo.u1AchTlvPresent & LSPP_ACH_IP_LSP_MEP_TLV)
        {
            pLsppPingTraceEntry->MibObject.u4FsLsppResponderGlobalId =
                pLsppEchoMsg->LsppAchInfo.LsppMepTlv.unMepTlv.
                IpLspMepTlv.GlobalNodeId.u4GlobalId;

            pLsppPingTraceEntry->MibObject.u4FsLsppResponderId =
                pLsppEchoMsg->LsppAchInfo.LsppMepTlv.unMepTlv.
                IpLspMepTlv.GlobalNodeId.u4NodeId;
        }
        else if (pLsppEchoMsg->LsppAchInfo.u1AchTlvPresent &
                 LSPP_ACH_IP_PW_MEP_TLV)
        {
            pLsppPingTraceEntry->MibObject.u4FsLsppResponderGlobalId =
                pLsppEchoMsg->LsppAchInfo.LsppMepTlv.unMepTlv.
                IpPwMepTlv.u4GlobalId;

            pLsppPingTraceEntry->MibObject.u4FsLsppResponderId =
                pLsppEchoMsg->LsppAchInfo.LsppMepTlv.unMepTlv.
                IpPwMepTlv.u4NodeId;
        }
        else if (pLsppEchoMsg->LsppAchInfo.u1AchTlvPresent &
                 LSPP_ACH_ICC_MEP_TLV)
        {
            MEMCPY (pLsppPingTraceEntry->MibObject.au1FsLsppResponderIcc,
                    pLsppEchoMsg->LsppAchInfo.LsppMepTlv.
                    unMepTlv.IccMepTlv.au1Icc, LSPP_MPLS_ICC_LENGTH);

            pLsppPingTraceEntry->MibObject.i4FsLsppResponderIccLen =
                LSPP_MPLS_ICC_LENGTH;

            MEMCPY (pLsppPingTraceEntry->MibObject.au1FsLsppResponderUMC,
                    pLsppEchoMsg->LsppAchInfo.LsppMepTlv.
                    unMepTlv.IccMepTlv.au1Umc, LSPP_MPLS_UMC_LENGTH);

            pLsppPingTraceEntry->MibObject.i4FsLsppResponderUMCLen =
                LSPP_MPLS_UMC_LENGTH;

            pLsppPingTraceEntry->MibObject.u4FsLsppResponderMepIndex =
                pLsppEchoMsg->LsppAchInfo.LsppMepTlv.
                unMepTlv.IccMepTlv.u2MepIndex;
        }

        if (pLsppEchoMsg->LsppAchInfo.u1AchTlvPresent & LSPP_ACH_MIP_TLV)
        {
            pLsppMipTlv = &(pLsppEchoMsg->LsppAchInfo.LsppMipTlv);

            pLsppPingTraceEntry->MibObject.u4FsLsppResponderGlobalId =
                pLsppMipTlv->GlobalNodeId.u4GlobalId;

            pLsppPingTraceEntry->MibObject.u4FsLsppResponderId =
                pLsppMipTlv->GlobalNodeId.u4NodeId;
        }
    }

    if (pLsppPingTraceEntry->MibObject.i4FsLsppRequestType ==
        LSPP_REQ_TYPE_TRACE_ROUTE)
    {
        pLsppPingTraceEntry->MibObject.u4FsLsppActualHopCount =
            pLsppEchoMsg->LsppPduInfo.LsppHeader.u4SequenceNum;
    }

    if (pLsppEchoMsg->LsppPduInfo.LsppHeader.u4SequenceNum >
        pLsppPingTraceEntry->u4LastUpdatedSeqIndex)
    {
        pLsppPingTraceEntry->u4LastUpdatedSeqIndex =
            pLsppEchoMsg->LsppPduInfo.LsppHeader.u4SequenceNum;
    }
}

/******************************************************************************
 * Function   : LsppCoreUpdateEchoRttInfo
 * Description: This funciton is to scan the echo sequence table to get the 
 *              status of the ping 
 * Input      : pLsppPingTraceEntry - Ping Trace Entry
 *              pu1LsppStatus - Variable to store Ping operation status 
 * Output     : OSIX_SUCCESS or OSIX_FAILURE
 * Returns    : None
 *****************************************************************************/
VOID
LsppCoreUpdateEchoRttInfo (tLsppFsLsppPingTraceTableEntry * pLsppPingTraceEntry,
                           UINT4 u4Rtt)
{
    /* Updating Min RTT */
    if ((u4Rtt < pLsppPingTraceEntry->MibObject.u4FsLsppMinRtt) ||
        (pLsppPingTraceEntry->MibObject.u4FsLsppMinRtt == 0))
    {
        pLsppPingTraceEntry->MibObject.u4FsLsppMinRtt = u4Rtt;
    }

    /* Updating Max RTT */
    if (u4Rtt > pLsppPingTraceEntry->MibObject.u4FsLsppMaxRtt)
    {
        pLsppPingTraceEntry->MibObject.u4FsLsppMaxRtt = u4Rtt;
    }

    if (pLsppPingTraceEntry->u4SuccessCount != 0)
    {
        /* Updating Average RTT */
        pLsppPingTraceEntry->MibObject.u4FsLsppAverageRtt =
            ((pLsppPingTraceEntry->MibObject.u4FsLsppAverageRtt *
              (pLsppPingTraceEntry->u4SuccessCount - 1)) + u4Rtt) /
            pLsppPingTraceEntry->u4SuccessCount;
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : LsppCoreUpdateEchoReplyStats
 * DESCRIPTION      : This function is to update the echo reply stats
 * Input(s)         : pLsppEchoMsg - Echo message where received packet Info is
 *                                   present
 *                    pLsppPingTraceEntry - Ping Trace Entry corresponding to 
 *                                          the received reply
 *                    pLsppEchoSequenceEntry - Echo Seq Entry corresponding to 
 *                                          the received reply
 *                    pLsppEchoPktRxTime - Specifies the packet received time
 * OUTPUT           : NONE
 * RETURNS          : NONE 
 ***************************************************************************/
PUBLIC VOID
LsppCoreUpdateEchoReplyStats (tLsppEchoMsg * pLsppEchoMsg,
                              tLsppFsLsppPingTraceTableEntry * pPingTraceEntry,
                              tLsppFsLsppEchoSequenceTableEntry *
                              pEchoSequenceEntry,
                              tLsppSysTime * pLsppEchoPktRxTime)
{
    UINT4               u4Rtt = 0;

    /* Updating Echo Sequence table */
    if (pEchoSequenceEntry != NULL)
    {
        LsppCoreUpdateEchoSeqTableInfo (pLsppEchoMsg, pEchoSequenceEntry);
    }

    LsppCoreCalcRtt (pLsppEchoMsg, pLsppEchoPktRxTime, &u4Rtt);

    if ((pPingTraceEntry->MibObject.i4FsLsppRequestType ==
         LSPP_REQ_TYPE_TRACE_ROUTE) ||
        ((pPingTraceEntry->MibObject.i4FsLsppRequestType ==
          LSPP_REQ_TYPE_PING) &&
         (pPingTraceEntry->MibObject.i4FsLsppDsMap == LSPP_SNMP_TRUE)))
    {
        LsppCoreUpdateHopInfo (pLsppEchoMsg, pPingTraceEntry, u4Rtt);
    }

    LsppCoreUpdatePingTraceInfo (pLsppEchoMsg, pPingTraceEntry);
    LsppCoreUpdateEchoRttInfo (pPingTraceEntry, u4Rtt);
    LsppCoreUpdatePingTraceResults (pPingTraceEntry, pLsppEchoMsg,
                                    pLsppEchoMsg->LsppPduInfo.
                                    LsppHeader.u1ReturnCode);

    return;
}

/******************************************************************************
 * Function   : LsppCoreSendTraceRequest
 * Description: This function is to Send the next trace route request on WFR 
 *              timer or on receiving a reply.
 * Input      : pLsppPingTraceEntry - Ping Trace Entry
 *              pDsMapTlv           - Pointer to DSMAP TLV is present
 * Output     : OSIX_SUCCESS or OSIX_FAILURE
 * Returns    : None
 *****************************************************************************/
PUBLIC UINT4
LsppCoreSendTraceRouteReq (tLsppFsLsppPingTraceTableEntry * pPingTraceEntry,
                           tLsppEchoMsg * pLsppEchoMsg)
{
    tLsppDsMapTlv       DsMapTlv;

    MEMSET (&DsMapTlv, 0, sizeof (tLsppDsMapTlv));

    if (pLsppEchoMsg->LsppPduInfo.u2TlvsPresent & LSPP_DSMAP_TLV)
    {
        MEMCPY (&DsMapTlv, &(pLsppEchoMsg->LsppPduInfo.LsppDsMapTlv),
                sizeof (tLsppDsMapTlv));
    }
    else
    {
        DsMapTlv.u1AddrType = LSPP_IPV4_UNNUMBERED;
        DsMapTlv.DsIpAddr.u4_addr[0] = LSPP_ALLROUTERS_MULTICAST_ADDRESS;
    }

    MEMSET (pLsppEchoMsg, 0, sizeof (tLsppEchoMsg));

    if (LsppCoreInitiateRequest (pPingTraceEntry, pLsppEchoMsg, &DsMapTlv, 0) !=
        OSIX_SUCCESS)
    {
        LSPP_LOG (pPingTraceEntry->MibObject.u4FsLsppContextId,
                  LSPP_SEND_ECHO_REQ_FAILED, "LsppCoreSendTraceRouteReq");

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : LsppCoreSendEchoReq 
 * Description: This function frames the echo request.
 *              This is called from Configuraiton handler, External Event 
 *              Handler and for sending next trace rotue request.
 * Input      : pLsppPingTraceEntry - Ping Trace Entry
 *              pLsppEchoMsg        - Echo Sequence message strucutre used to 
 *                                    frame the request
 *              pRxDsMapTlv         - Received downstream TLV details
 * Output     : pu4TxResult         - Pointer to Cause of failure 
 * Returns    : OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/
PUBLIC UINT4
LsppCoreSendEchoReq (tLsppFsLsppPingTraceTableEntry * pLsppPingTraceEntry,
                     tLsppEchoMsg * pLsppEchoMsg, tLsppDsMapTlv * pRxDsMapTlv,
                     UINT4 u4BfdDiscriminator, UINT4 *pu4TxResult)
{
    tCRU_BUF_CHAIN_HEADER *pMsg = NULL;
    tLsppFsLsppEchoSequenceTableEntry *pEchoSequenceEntry = NULL;
    INT4                i4RetVal = OSIX_FAILURE;

    /* Create Echo Sequence node */
    pEchoSequenceEntry = LsppDbCreateEchoSeqNode (pLsppPingTraceEntry);
    if (pEchoSequenceEntry == NULL)
    {
        *pu4TxResult = LSPP_NODE_CREATION_FAILED;
        LSPP_LOG (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
                  LSPP_ECHO_SEQ_NODE_CREATE_FAILED, "LsppCoreSendEchoReq");
        return OSIX_FAILURE;
    }

    /* Update Echo Message Strucutre with the required details to frame the 
     * echo request.*/
    i4RetVal = LsppCoreUpdateEchoMsgInfo (pLsppPingTraceEntry, pLsppEchoMsg,
                                          pRxDsMapTlv, u4BfdDiscriminator);
    if (i4RetVal == OSIX_FAILURE)
    {
        *pu4TxResult = LSPP_UPDATING_ECHO_MSG_FAILED;
        LSPP_LOG (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
                  LSPP_UPDATE_ECHO_MSG_FAILED, "LsppCoreSendEchoReq");

        return OSIX_FAILURE;
    }

    /* Allocate buffer for Tx Packet. */
    pMsg = LSPP_ALLOC_CRU_BUF ((pLsppEchoMsg->u4EncapHdrLen +
                                pLsppEchoMsg->u4LsppPduLen +
                                MPLS_MAX_L2HEADER_LEN) +
                               CFA_ENET_V2_HEADER_SIZE,
                               pLsppEchoMsg->u4EncapHdrLen +
                               CFA_ENET_V2_HEADER_SIZE + MPLS_MAX_L2HEADER_LEN);
    if (pMsg == NULL)
    {
        *pu4TxResult = LSPP_CRU_BUF_ALLOC_FAILED;
        LSPP_CMN_TRC (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
                      LSPP_CRU_BUF_ALLOC_FAILED, "LsppCoreSendEchoReq");
        return OSIX_FAILURE;
    }

    i4RetVal = LsppTxFrameAndSendPdu (pLsppEchoMsg, pLsppPingTraceEntry,
                                      pEchoSequenceEntry, pMsg);
    if (i4RetVal == OSIX_FAILURE)
    {
        *pu4TxResult = LSPP_TX_PKT_FAILED;

        LSPP_RELEASE_CRU_BUF (pMsg, 0);

        LSPP_LOG (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
                  LSPP_TX_PKT_FAIL, "LsppCoreSendEchoReq");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : LsppCoreFillEncapHeader 
 * Description: This function fetches the encapsulation related params and 
 *            : updates the echo message.
 * Input      : pLsppPingTraceEntry - pointer to the ping trace entry based on 
 *            :                    which the encapsulation is done.
 *              pLsppEchoMsg - Pointer to echo message structure
 * Returns    : OSIX_SUCCESS or OSIX_FAILURE 
 * Output     : pLsppEchoMsg - updated echo message with the encapsulations.
 *****************************************************************************/
PUBLIC UINT4
LsppCoreFillEncapHeader (tLsppFsLsppPingTraceTableEntry * pLsppPingTraceEntry,
                         tLsppEchoMsg * pLsppEchoMsg)
{
    tLsppIpRouterAlertOpt LsppIpRouterAlertOpt;
    tLsppNodeId         LsppNodeId;
    tLsppPathInfo      *pLsppPathInfo = NULL;
    UINT4               u4SrcIp = 0;
    UINT1               u1OperatorType = 0;
    UINT1               u1ServiceType = 0;
    UINT1               u1Version = 0;

    MEMSET (&LsppIpRouterAlertOpt, 0, sizeof (tLsppIpRouterAlertOpt));
    MEMSET (&(LsppNodeId), 0, sizeof (tLsppNodeId));

    pLsppPathInfo = &(pLsppEchoMsg->LsppPathInfo);
    u1OperatorType = pLsppPathInfo->MegDetail.u1OperatorType;
    u1ServiceType = (UINT1) pLsppPathInfo->MegDetail.u1ServiceType;

    if ((pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_IP) ||
        (pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_ACH_IP))
    {

        u1Version = (UINT1) IP_VERSION_4;

        /* Get the Router Id since the source IP is not available
         * in PW Info. */
        if ((pLsppPathInfo->u1PathType == LSPP_PATH_TYPE_FEC_128) ||
            (pLsppPathInfo->u1PathType == LSPP_PATH_TYPE_FEC_129) ||
            (pLsppPathInfo->u1PathType == LSPP_PATH_TYPE_STATIC_PW))
        {
            if (pLsppPathInfo->PwDetail.PwFecInfo.SrcNodeId.u4NodeType ==
                LSPP_MPLS_ADDR_TYPE_IPV4)
            {
                u4SrcIp = pLsppPathInfo->PwDetail.PwFecInfo.SrcNodeId.
                    MplsRouterId.u4_addr[0];
            }
            u1Version = pLsppPathInfo->PwDetail.u1IpVersion;
        }
        else if ((pLsppPathInfo->u1PathType == LSPP_PATH_TYPE_RSVP_IPV4) ||
                 (pLsppPathInfo->u1PathType == LSPP_PATH_TYPE_RSVP_IPV6))
        {
            if (pLsppPathInfo->TeTnlInfo.TnlInfo.SrcNodeId.u4NodeType ==
                LSPP_MPLS_ADDR_TYPE_IPV4)
            {
                u4SrcIp = pLsppPathInfo->TeTnlInfo.TnlInfo.SrcNodeId.
                    MplsRouterId.u4_addr[0];
            }
        }
        else if (pLsppPathInfo->u1PathType == LSPP_PATH_TYPE_STATIC_TNL)
        {
            if (LsppCoreGetNodeId (pLsppEchoMsg->u4ContextId,
                                   &LsppNodeId) != OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }
            u4SrcIp = LsppNodeId.MplsGlobalNodeId.u4NodeId;
        }

        if (u4SrcIp != 0 && u1Version != 0)
        {
            pLsppEchoMsg->LsppIpHeader.u1Version = u1Version;
            pLsppEchoMsg->LsppIpHeader.u4Src = u4SrcIp;
            pLsppEchoMsg->LsppIpHeader.u4Dest = LSPP_IP_LOOPBACK_ADDRESS;
            pLsppEchoMsg->LsppIpHeader.u1Tos = 1;

            /* IP header length including the Option length. */
            pLsppEchoMsg->LsppIpHeader.u1Hlen =
                (IP_HDR_LEN / LSPP_IP_HEADER_LEN_IN_BYTES) + 1;

            LsppIpRouterAlertOpt.u1Type = LSPP_IP_ROUTER_ALERT_OPTION_TYPE;
            LsppIpRouterAlertOpt.u1Len = LSPP_IP_ROUTER_ALERT_OPTION_LEN;

            MEMCPY (pLsppEchoMsg->LsppIpHeader.au1Options,
                    &LsppIpRouterAlertOpt, sizeof (tLsppIpRouterAlertOpt));
        }
        else
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_ENCAP_SRC_IP_OR_IP_VERSION_INVALID,
                      "LsppCoreFillEncapHeader");
            return OSIX_FAILURE;
        }

        /* Filling UDP Header params */
        pLsppEchoMsg->LsppUdpHeader.u2Src_u_port = LSPP_UDP_SRC_PORT;
        pLsppEchoMsg->LsppUdpHeader.u2Dest_u_port = LSPP_UDP_DEST_PORT;
    }

    if (pLsppEchoMsg->u1EncapType == LSPP_ENCAP_TYPE_ACH)
    {
        pLsppPingTraceEntry->u1MegOperatorType = u1OperatorType;

        LsppCoreUpdateMepTlv (u1OperatorType, u1ServiceType,
                              &(pLsppEchoMsg->LsppPathInfo),
                              &(pLsppEchoMsg->LsppAchInfo));

        /* Update the MIP TLV parameters when the request type is PING and the 
         * request is to verify connectivity to an Intermediate node. 
         */
        if ((pLsppPingTraceEntry->MibObject.i4FsLsppRequestType ==
             LSPP_REQ_TYPE_PING) &&
            (pLsppPingTraceEntry->MibObject.u4FsLsppTgtMipNodeId != 0))
        {
            pLsppEchoMsg->LsppAchInfo.LsppMipTlv.GlobalNodeId.u4GlobalId =
                pLsppPingTraceEntry->MibObject.u4FsLsppTgtMipGlobalId;

            pLsppEchoMsg->LsppAchInfo.LsppMipTlv.GlobalNodeId.u4NodeId =
                pLsppPingTraceEntry->MibObject.u4FsLsppTgtMipNodeId;

            pLsppEchoMsg->LsppAchInfo.LsppMipTlv.u4IfNum =
                pLsppPingTraceEntry->MibObject.u4FsLsppTgtMipIfNum;

            pLsppEchoMsg->LsppAchInfo.u1AchTlvPresent =
                (pLsppEchoMsg->LsppAchInfo.u1AchTlvPresent | LSPP_ACH_MIP_TLV);
        }
    }

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : LsppCoreFillDsmapTlvDetails
 * Description: This function is to fill the DSMAP Details 
                fetched Path Info to echo msg strcutre
 * Input      : pOutSegInfo - Pointer to Outsegment details where DSMAP TLV
 *                            requires details are available
 *              pDsMapTlv   - pointer to Echo message strucutre where echo 
 *                            message is filled
 * Returns     : OSIX_SUCCESS or OSIX_FAILURE
 * Output      : None
 *****************************************************************************/
PUBLIC UINT4
LsppCoreFillDsMapTlvInfo (UINT4 u4ContextId,
                          tLsppOutSegInfo * pOutSegInfo,
                          tLsppDsMapTlv * pDsMapTlv)
{
    tLsppExtInParams   *pLsppExtInParams = NULL;
    tLsppExtOutParams  *pLsppExtOutParams = NULL;

    if ((pLsppExtInParams = (tLsppExtInParams *)
         MemAllocMemBlk (LSPP_FSLSPPINPUTPARAMS_POOLID)) == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((pLsppExtOutParams = (tLsppExtOutParams *)
         MemAllocMemBlk (LSPP_FSLSPPOUTPUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        return OSIX_FAILURE;
    }

    MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
    MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));

    pLsppExtInParams->u4ContextId = u4ContextId;
    pLsppExtInParams->u4RequestType = LSPP_CFA_GET_MTU;
    pLsppExtInParams->uInParams.u4IfIndex = pOutSegInfo->u4OutIf;

    /* call the LSPP exit function to fetch MTU of Outgoing Interface */
    if (LsppPortHandleExtInteraction
        (pLsppExtInParams, pLsppExtOutParams) != OSIX_SUCCESS)
    {
        LSPP_LOG (u4ContextId, LSPP_FETCH_FROM_EXT_MOD_FAILED,
                  "LsppCoreFillDsMapTlvInfo");
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);
        return OSIX_FAILURE;
    }

    /* Filling Downstream Interface MTU */
    pDsMapTlv->u2Mtu = (UINT2) pLsppExtOutParams->unOutParams.u4Mtu;

    if (pOutSegInfo->u1NHAddrType == LSR_ADDR_IPV4)
    {
        pDsMapTlv->u1AddrType = LSPP_IPV4_NUMBERED;
        pDsMapTlv->DsIpAddr.u4_addr[0] = pOutSegInfo->NHAddr.u4_addr[0];
        pDsMapTlv->DsIfAddr.u4_addr[0] = pOutSegInfo->NHAddr.u4_addr[0];
    }
    else if (pOutSegInfo->u1NHAddrType == LSR_ADDR_IPV6)
    {
        pDsMapTlv->u1AddrType = LSPP_IPV6_NUMBERED;

        MEMCPY (&(pDsMapTlv->DsIpAddr.u1_addr),
                &(pOutSegInfo->NHAddr.u1_addr), LSPP_IPV6_ADDR_LEN);

        MEMCPY (&(pDsMapTlv->DsIfAddr.u1_addr),
                &(pOutSegInfo->NHAddr.u1_addr), LSPP_IPV6_ADDR_LEN);
    }

    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtInParams);
    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtOutParams);

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : LsppCoreUpdateOutPathInfo  
 * Description: This function will update the fetched out segment info from 
 *              External Module in to the echo msg structure
 * Input      : pLsppEchoMsg     - Echo message Strucutre where information 
 *                                 needs to be filled
 *              u1PathType       - specifies the path for which Ping/Trace is 
 *                                 triggered
 * Returns    : OSIX_SUCCESS or OSIX_FAILURE 
 * Output     : pLsppEchoMsg     - updated OutSegInfo in echo message
 *****************************************************************************/
PUBLIC UINT4
LsppCoreUpdateOutPathInfo (tLsppEchoMsg * pLsppEchoMsg, UINT1 u1PathType)
{
    tLsppOutSegInfo    *pOutSegInfo = NULL;
    tLsppPathInfo      *pLsppPathInfo = NULL;
    INT4                i4RetVal = OSIX_FAILURE;

    pLsppPathInfo = &(pLsppEchoMsg->LsppPathInfo);

    switch (u1PathType)
    {
        case LSPP_PATH_TYPE_STATIC_TNL:
        case LSPP_PATH_TYPE_STATIC_PW:
        {
            pOutSegInfo = &(pLsppPathInfo->TeTnlInfo.OutSegInfo);
            MEMCPY (pOutSegInfo->au1NextHopMac,
                    pLsppPathInfo->TeTnlInfo.au1NextHopMac, MAC_ADDR_LEN);
        }
            break;

        case LSPP_PATH_TYPE_FEC_128:
        case LSPP_PATH_TYPE_FEC_129:
        {
            if (pLsppPathInfo->PwDetail.u1MplsType == LSPP_L2VPN_MPLS_TYPE_TE)
            {
                pOutSegInfo = &(pLsppPathInfo->TeTnlInfo.OutSegInfo);
                MEMCPY (pOutSegInfo->au1NextHopMac,
                        pLsppPathInfo->TeTnlInfo.au1NextHopMac, MAC_ADDR_LEN);
            }
            else if (pLsppPathInfo->PwDetail.u1MplsType ==
                     LSPP_L2VPN_MPLS_TYPE_NONTE)
            {
                pOutSegInfo = &(pLsppPathInfo->NonTeInfo.OutSegInfo);
            }
            else
            {
                return OSIX_FAILURE;
            }
        }
            break;

        case LSPP_PATH_TYPE_RSVP_IPV4:
        case LSPP_PATH_TYPE_RSVP_IPV6:
        {
            pOutSegInfo = &(pLsppPathInfo->TeTnlInfo.OutSegInfo);
            MEMCPY (pOutSegInfo->au1NextHopMac,
                    pLsppPathInfo->TeTnlInfo.au1NextHopMac, MAC_ADDR_LEN);
        }
            break;

        case LSPP_PATH_TYPE_LDP_IPV4:
        case LSPP_PATH_TYPE_LDP_IPV6:
        {
            pOutSegInfo = &(pLsppPathInfo->NonTeInfo.OutSegInfo);
        }
            break;

        default:
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_INVALID_PATH_TYPE_FOR_TARGET_FEC_UPDATAION,
                      "LsppCoreUpdateOutPathInfo");
            return OSIX_FAILURE;
    }

    /* Filling OutGoing L3 Interface */
    i4RetVal = LsppCoreGetL3If (pLsppEchoMsg->u4ContextId,
                                pOutSegInfo->u4OutIf, &(pOutSegInfo->u4OutIf));
    if (i4RetVal == OSIX_FAILURE)
    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId,
                  LSPP_GET_L3_IF_FAILED, "LsppCoreUpdateOutPathInfo");
        return OSIX_FAILURE;
    }

    pLsppEchoMsg->u4IfIndex = pOutSegInfo->u4OutIf;
    pLsppEchoMsg->pOutSegInfo = pOutSegInfo;

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : LsppCoreUpdateTgtFecInfo  
 * Description: This function will update the Target FEC fetched info from 
 *              External Module in to the echo msg structure
 * Input      : pLsppEchoMsg     - Echo message Strucutre where information 
 *                                 needs to be filled
 *              u1PathType       - specifies the path for which Ping/Trace is 
 *                                 triggered
 * Returns    : OSIX_SUCCESS or OSIX_FAILURE 
 * Output     : pLsppEchoMsg     - updated Target FEC stack in echo message
 *****************************************************************************/
PUBLIC UINT4
LsppCoreUpdateTgtFecInfo (tLsppEchoMsg * pLsppEchoMsg, UINT1 u1PathType)
{
    tLsppPathInfo      *pLsppPathInfo = NULL;
    tLsppPduInfo       *pLsppPduInfo = NULL;
    UINT1               u1FecStackDepth = 0;

    pLsppPathInfo = &(pLsppEchoMsg->LsppPathInfo);
    pLsppPduInfo = &(pLsppEchoMsg->LsppPduInfo);

    switch (u1PathType)
    {
        case LSPP_PATH_TYPE_MEP:
        {
            if ((pLsppPathInfo->u1PathType == LSPP_PATH_TYPE_STATIC_TNL) ||
                ((pLsppPathInfo->u1PathType == LSPP_PATH_TYPE_STATIC_PW) &&
                 (pLsppPathInfo->PwDetail.u1MplsType ==
                  LSPP_L2VPN_MPLS_TYPE_TE) &&
                 (pLsppPathInfo->TeTnlInfo.u4TnlType ==
                  MPLS_TE_TNL_TYPE_MPLSTP)))
            {
                LsppUtilFillStaticLspFec (&(pLsppPathInfo->TeTnlInfo),
                                          LSPP_STATIC_LSP_FEC_TLV_BASEADDR
                                          (u1FecStackDepth));

                pLsppEchoMsg->LsppPduInfo.LsppFecTlv[u1FecStackDepth].
                    u2TlvType = LSPP_FEC_STATIC_LSP;

                u1FecStackDepth++;
            }

            if (pLsppPathInfo->u1PathType == LSPP_PATH_TYPE_STATIC_PW)
            {
                if ((pLsppPathInfo->PwDetail.u1MplsType ==
                     LSPP_L2VPN_MPLS_TYPE_TE) &&
                    (pLsppPathInfo->TeTnlInfo.u4TnlType ==
                     MPLS_TE_TNL_TYPE_MPLS))
                {
                    /* Tlv type and Fec details  should be filled based on 
                     * interface type as either IPV4/IPV6. 
                     * So pointer to pLsppEchoMsg is passed to this 
                     * function and the interface type check is done 
                     * with in the calling function and FEC details and 
                     * u2TlvType are updated accordingly*/

                    LsppUtilFillRsvpFecInfo (&(pLsppPathInfo->TeTnlInfo),
                                             pLsppEchoMsg, u1FecStackDepth);
                    u1FecStackDepth++;
                }
                else if ((pLsppPathInfo->PwDetail.u1MplsType ==
                          LSPP_L2VPN_MPLS_TYPE_NONTE) &&
                         (pLsppPathInfo->TeTnlInfo.u4TnlType ==
                          MPLS_TE_TNL_TYPE_MPLS))
                {
                    /* FEC details and u2TlvType are updated in 
                     * called function */
                    LsppUtilFillLdpFecInfo (&(pLsppPathInfo->NonTeInfo),
                                            pLsppEchoMsg, u1FecStackDepth);
                    u1FecStackDepth++;
                }

                LsppUtilFillStaticPwFec (&(pLsppPathInfo->PwDetail),
                                         LSPP_STATIC_PW_FEC_TLV_BASEADDR
                                         (u1FecStackDepth));

                pLsppEchoMsg->LsppPduInfo.LsppFecTlv[u1FecStackDepth].
                    u2TlvType = LSPP_FEC_STATIC_PW;

                u1FecStackDepth++;
            }
        }
            break;

        case LSPP_PATH_TYPE_FEC_128:
        case LSPP_PATH_TYPE_FEC_129:
        {
            /* Update the underlying tunnel or LSP for PW before adding 
             * PW Fec in Target Fec Tlv*/
            if ((pLsppPathInfo->PwDetail.u1MplsType ==
                 LSPP_L2VPN_MPLS_TYPE_TE) &&
                (pLsppPathInfo->TeTnlInfo.u4TnlType == MPLS_TE_TNL_TYPE_MPLS))
            {
                /* Tlv type and Fec details  should be filled based on 
                 * interface type as either IPV4/IPV6. 
                 * So pointer to pLsppEchoMsg is passed to this function 
                 * and the interface type check is done with in the 
                 * calling function and FEC details and u2TlvType are 
                 * updated accordingly*/

                LsppUtilFillRsvpFecInfo (&(pLsppPathInfo->TeTnlInfo),
                                         pLsppEchoMsg, u1FecStackDepth);
            }
            else if ((pLsppPathInfo->PwDetail.u1MplsType ==
                      LSPP_L2VPN_MPLS_TYPE_NONTE) &&
                     (pLsppPathInfo->TeTnlInfo.u4TnlType ==
                      MPLS_TE_TNL_TYPE_MPLS))
            {
                /* FEC details and u2TlvType are updated in 
                 * called function */
                LsppUtilFillLdpFecInfo (&(pLsppPathInfo->NonTeInfo),
                                        pLsppEchoMsg, u1FecStackDepth);
            }
            else
            {
                return OSIX_FAILURE;
            }

            u1FecStackDepth++;

            if (LsppCoreUpdatePwSrcAddr (pLsppEchoMsg->u4ContextId,
                                         &(pLsppPathInfo->PwDetail)) !=
                OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppEchoMsg->u4ContextId,
                          LSPP_FETCH_PW_SRC_IP_FAILED,
                          "LsppCoreUpdateTgtFecInfo");
                return OSIX_FAILURE;
            }

            if (pLsppPathInfo->PwDetail.PwFecInfo.u1PwVcOwner ==
                LSPP_PW_OWNER_PWIDFEC_SIGNALLING)
            {
                LsppUtilFillFec128PwDetails (&(pLsppPathInfo->PwDetail),
                                             LSPP_FEC128_PW_TLV_BASEADDR
                                             (u1FecStackDepth));

                pLsppEchoMsg->LsppPduInfo.LsppFecTlv[u1FecStackDepth].
                    u2TlvType = LSPP_FEC_128_PWFEC;
            }
            else if (pLsppPathInfo->PwDetail.PwFecInfo.u1PwVcOwner ==
                     LSPP_PW_OWNER_GENFEC_SIGNALLING)
            {
                LsppUtilFillFec129PwDetails (&(pLsppPathInfo->PwDetail),
                                             LSPP_FEC129_PW_TLV_BASEADDR
                                             (u1FecStackDepth));

                pLsppEchoMsg->LsppPduInfo.LsppFecTlv[u1FecStackDepth].
                    u2TlvType = LSPP_FEC_129_PWFEC;
            }
            else
            {
                return OSIX_FAILURE;
            }

            u1FecStackDepth++;
        }
            break;

        case LSPP_PATH_TYPE_RSVP_IPV4:
        case LSPP_PATH_TYPE_RSVP_IPV6:
        {
            /* FEC details and u2TlvType are updated in 
             * called function */
            LsppUtilFillRsvpFecInfo (&(pLsppPathInfo->TeTnlInfo),
                                     pLsppEchoMsg, u1FecStackDepth);
            u1FecStackDepth++;
        }
            break;

        case LSPP_PATH_TYPE_LDP_IPV4:
        case LSPP_PATH_TYPE_LDP_IPV6:
        {
            /* FEC details and u2TlvType are updated in 
             * called function */
            LsppUtilFillLdpFecInfo (&(pLsppPathInfo->NonTeInfo),
                                    pLsppEchoMsg, u1FecStackDepth);
            u1FecStackDepth++;
        }
            break;

        default:
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_INVALID_PATH_TYPE_FOR_TARGET_FEC_UPDATAION,
                      "LsppCoreUpdateTgtFecInfo");
            return OSIX_FAILURE;
    }

    pLsppEchoMsg->LsppPduInfo.u2TlvsPresent =
        (pLsppEchoMsg->LsppPduInfo.u2TlvsPresent | LSPP_TARGET_FEC_TLV);

    /* Total FEC stack Depth */
    pLsppEchoMsg->LsppPduInfo.u1FecStackDepth = u1FecStackDepth;

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : LsppCoreUpdateOutLblInfo
 * Description: This function will update the outgoing labels in to the 
 *              Echo Msg structure
 * Input      : pLsppEchoMsg - Pointer to Echo message in which labels 
 *              needs to be updated
 * Returns    : OSIX_SUCCESS or OSIX_FAILURE 
 * Output     : Updated LabelInfo.
 *****************************************************************************/
PUBLIC INT4
LsppCoreUpdateOutLblInfo (tLsppEchoMsg * pLsppEchoMsg, tLsppLblInfo * pLblInfo,
                          UINT1 *pu1LblStkDepth)
{
    tLsppOutSegInfo    *pOutSegInfo = NULL;
    tLsppPathInfo      *pLsppPathInfo = NULL;
    UINT1               u1LabelCount = 0;

    *pu1LblStkDepth = 0;

    pOutSegInfo = pLsppEchoMsg->pOutSegInfo;
    pLsppPathInfo = &(pLsppEchoMsg->LsppPathInfo);

    if ((pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReplyMode ==
         LSPP_REPLY_IP_UDP_ROUTER_ALERT) &&
        (pLsppEchoMsg->u1IsRouteExists == OSIX_FALSE)
        && (pLsppEchoMsg->LsppPduInfo.LsppHeader.u1MessageType ==
            LSPP_ECHO_REPLY))
    {
        /* RAL Label */
        pLblInfo->u4Label = LSPP_LBL_ROUTER_ALERT;
        pLblInfo->u1Ttl = 1;
        pLblInfo->u1SI = 0;

        pLblInfo++;
        (*pu1LblStkDepth)++;
    }

    while (u1LabelCount < pOutSegInfo->u1LblStkCnt)
    {
        LsppUtilFillLblInfo (pLblInfo,
                             pOutSegInfo->LblInfo[u1LabelCount].u4Label,
                             pOutSegInfo->LblInfo[u1LabelCount].u1Exp, 0,
                             pOutSegInfo->LblInfo[u1LabelCount].u1Ttl,
                             pOutSegInfo->LblInfo[u1LabelCount].u1Protocol);

        u1LabelCount++;
        pLblInfo++;

        if (u1LabelCount >= LSPP_MAX_LABEL_STACK)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_UPDATING_LBL_STK_FAILED, "LsppCoreUpdateOutLblInfo");
            return OSIX_FAILURE;
        }
    }

    if ((pLsppPathInfo->u1PathType == LSPP_PATH_TYPE_FEC_128) ||
        (pLsppPathInfo->u1PathType == LSPP_PATH_TYPE_FEC_129) ||
        (pLsppPathInfo->u1PathType == LSPP_PATH_TYPE_STATIC_PW))
    {
        /* Updating Pw label and Ttl */
        pLblInfo->u4Label = pLsppPathInfo->PwDetail.u4OutVcLabel;
        pLblInfo->u1Ttl = LSPP_PW_DEFAULT_TTL;
        pLblInfo->u1SI = 0;

        u1LabelCount++;
        pLblInfo++;

        if (u1LabelCount >= LSPP_MAX_LABEL_STACK)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_UPDATING_LBL_STK_FAILED, "LsppCoreUpdateOutLblInfo");
            return OSIX_FAILURE;
        }
    }

    *pu1LblStkDepth = (UINT1) ((*pu1LblStkDepth) + u1LabelCount);

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : LsppCoreUpdateDsMapInfo
 * Description: This function will update the Downstream Informations
 * Input      : pLsppEchoMsg     - Echo message Strucutre where DSMAP 
 *              information needs to be filled
 * Returns    : OSIX_SUCCESS or OSIX_FAILURE 
 * Output     : pLblInfo - Echo Message strucutre updated with 
 *              DSMAP Information
 *****************************************************************************/

PUBLIC INT4
LsppCoreUpdateDsMapInfo (tLsppEchoMsg * pLsppEchoMsg,
                         tLsppFsLsppPingTraceTableEntry * pLsppPingTraceEntry)
{
    INT4                i4RetVal = OSIX_FAILURE;

    /* Set the Interface & Label Stack Flag when it is requested. */
    if (pLsppPingTraceEntry->MibObject.i4FsLsppInterfaceLabelTlv ==
        LSPP_SNMP_TRUE)
    {
        pLsppEchoMsg->LsppPduInfo.LsppDsMapTlv.u1DsFlags =
            LSPP_INTERFACE_AND_LBL_STACK_REQ;
    }

    if (pLsppPingTraceEntry->MibObject.i4FsLsppRequestType ==
        LSPP_REQ_TYPE_PING)
    {
        pLsppEchoMsg->LsppPduInfo.LsppDsMapTlv.u1AddrType =
            LSPP_IPV4_UNNUMBERED;
        pLsppEchoMsg->LsppPduInfo.LsppDsMapTlv.DsIpAddr.u4_addr[0] =
            LSPP_ALLROUTERS_MULTICAST_ADDRESS;

        pLsppEchoMsg->LsppPduInfo.LsppDsMapTlv.DsIfAddr.u4_addr[0] = 0;

        pLsppEchoMsg->LsppPduInfo.u2TlvsPresent =
            (pLsppEchoMsg->LsppPduInfo.u2TlvsPresent | LSPP_DSMAP_TLV);

        return OSIX_SUCCESS;
    }

    i4RetVal = LsppCoreUpdateOutLblInfo (pLsppEchoMsg,
                                         pLsppEchoMsg->LsppPduInfo.LsppDsMapTlv.
                                         DsLblInfo,
                                         &(pLsppEchoMsg->LsppPduInfo.
                                           LsppDsMapTlv.u1DsLblStackDepth));
    if (i4RetVal == OSIX_FAILURE)
    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId,
                  LSPP_UPDATE_OUT_LABEL_FAILED, "LsppCoreUpdateDsMapInfo");
        return OSIX_FAILURE;
    }

    LsppCoreFillDsMapTlvInfo (pLsppEchoMsg->u4ContextId,
                              pLsppEchoMsg->pOutSegInfo,
                              &(pLsppEchoMsg->LsppPduInfo.LsppDsMapTlv));

    pLsppEchoMsg->LsppPduInfo.u2TlvsPresent =
        (pLsppEchoMsg->LsppPduInfo.u2TlvsPresent | LSPP_DSMAP_TLV);

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : LsppCoreUpdateNilFec
 * Description: This function will update the FEC stack with Nil Fec and updated
 *               Label Stack with Explicit label
 * Input      : pLsppEchoMsg     - Echo message Strucutre where NIL FEC 
 *                                 needs to be filled
 * Returns    : OSIX_SUCCESS or OSIX_FAILURE 
 * Output     : pLblInfo - Echo Message strucutre updated with 
 *              Nil FEC Information
 *****************************************************************************/
PUBLIC INT4
LsppCoreUpdateNilFec (tLsppEchoMsg * pLsppEchoMsg)
{
    UINT1               u1FecStackDepth = 0;
    UINT1               u1LabelCount = 0;

    u1FecStackDepth = pLsppEchoMsg->LsppPduInfo.u1FecStackDepth;
    u1LabelCount = pLsppEchoMsg->u1LabelStackDepth;

    /* Filling FEC stack with Nil FEC for FORCE EXPLICIT NULL */
    pLsppEchoMsg->LsppPduInfo.LsppFecTlv[u1FecStackDepth].
        NilFecTlv.u4Label = LSPP_LBL_EXPLICIT_NULL;

    /* Specifying that Nil Fec type as this depth is Pw */
    pLsppEchoMsg->LsppPduInfo.LsppFecTlv[u1FecStackDepth].u2TlvType =
        LSPP_FEC_NIL;

    if (u1LabelCount >= LSPP_MAX_LABEL_STACK)
    {
        LSPP_LOG (pLsppEchoMsg->u4ContextId,
                  LSPP_UPDATING_LBL_STK_FAILED, "LsppCoreUpdateNilFec");
        return OSIX_FAILURE;
    }
    /* Filling Label Stack with force explicit null */
    pLsppEchoMsg->LblInfo[u1LabelCount].u4Label = LSPP_LBL_EXPLICIT_NULL;
    pLsppEchoMsg->LblInfo[u1LabelCount].u1Ttl = 1;

    u1FecStackDepth++;
    u1LabelCount++;

    pLsppEchoMsg->u1LabelStackDepth = u1LabelCount;
    pLsppEchoMsg->LsppPduInfo.u1FecStackDepth = u1FecStackDepth;

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function    : LsppCoreUpdatePadTlvInfo 
 * Description : This funciton is to update the Pad Tlv Info
 * Input       : pPadPattern - Pad Pattern
 *               u1PadPatternLen - Pad Pattern Length
 *               pLsppPadTlv - Pointer to the Pad TLv 
 * Output      : pLsppPadTlv - Updated Pad Tlv
 * Returns     : OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
VOID
LsppCoreUpdatePadTlvInfo (UINT1 *pPadPattern,
                          UINT1 u1PadPatternLen, tLsppPadTlv * pLsppPadTlv)
{
    UINT1              *pu1Offset = NULL;
    UINT4               u4PadCount = 0;

    pu1Offset = pLsppPadTlv->au1PadInfo;

    u4PadCount = ((pLsppPadTlv->u2Length - 1) / u1PadPatternLen);

    while (u4PadCount > 0)
    {
        MEMCPY (pu1Offset, pPadPattern, u1PadPatternLen);

        pu1Offset = pu1Offset + u1PadPatternLen;
        u4PadCount--;
    }

    u4PadCount = ((pLsppPadTlv->u2Length - 1) % u1PadPatternLen);

    if (u4PadCount != 0)
    {
        MEMCPY (pu1Offset, pPadPattern, u4PadCount);
    }

    return;
}

/******************************************************************************
 * Function   : LsppCoreUpdateEchoMsgInfo
 * Description: This function fetches the information from external modules
 *              and updates the Echo Msg struture.
 * Input      : pLsppPingTraceEntry - Pointer to ping trace entry
 *              pLsppEchoMsg        - Pointer to Echo Msg structure
 *              pRxDsMapTlv         - Received downstream TLV details from the
 *                                    traceroute reply message.
 *              u4BfdDiscriminator  - Local Discriminator from BFD for 
 *                                    bootstrapping.                      
 * Output     : None
 * Returns    : OSIX_SUCCESS or OSIX_FAILURE 
 *****************************************************************************/
PUBLIC UINT4
LsppCoreUpdateEchoMsgInfo (tLsppFsLsppPingTraceTableEntry * pLsppPingTraceEntry,
                           tLsppEchoMsg * pLsppEchoMsg,
                           tLsppDsMapTlv * pRxDsMapTlv,
                           UINT4 u4BfdDiscriminator)
{
    tLsppFsLsppGlobalConfigTableEntry LsppGlobalConfigTable;
    tLsppPathInfo      *pLsppPathInfo = NULL;
    UINT4               u4LsppPduLen = 0;
    UINT4               u4PktSize = 0;
    INT4                i4RetVal = OSIX_FAILURE;
    UINT1               u1IsRespReq = 0;
    BOOL1               bDsmapOption = OSIX_TRUE;

    MEMSET (&(LsppGlobalConfigTable), 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));

    pLsppPathInfo = &(pLsppEchoMsg->LsppPathInfo);

    pLsppEchoMsg->u4ContextId =
        pLsppPingTraceEntry->MibObject.u4FsLsppContextId;

    pLsppEchoMsg->u1EncapType =
        (UINT1) (pLsppPingTraceEntry->MibObject.i4FsLsppEncapType);

    /* Get the Path information from the path id. */
    i4RetVal = LsppCoreGetPathInfo (pLsppPingTraceEntry->MibObject.
                                    u4FsLsppContextId,
                                    &(pLsppPingTraceEntry->PathId),
                                    pLsppPathInfo, 0);
    if (i4RetVal != OSIX_SUCCESS)
    {
        LSPP_LOG (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
                  LSPP_GET_PATH_INFO_FROM_PATH_ID_FAILED,
                  "LsppCoreUpdateEchoMsgInfo");
        return OSIX_FAILURE;
    }

    /* Check Whether Ping/Trace is initatied from MEP/MIP */
    if ((pLsppPathInfo->u1PathType == LSPP_PATH_TYPE_STATIC_PW) ||
        (pLsppPathInfo->u1PathType == LSPP_PATH_TYPE_STATIC_TNL))
    {
        if (pLsppPathInfo->MegDetail.u1MpType != LSPP_ME_MP_TYPE_MEP)
        {
            return OSIX_FAILURE;
        }
    }

    /* Check whether the CV negotiated for the path is LSP Ping when 
     * the path type is PW and return failure when there is a msimatch.
     */
    if ((pLsppPingTraceEntry->MibObject.i4FsLsppEncapType ==
         LSPP_ENCAP_TYPE_VCCV_NEGOTIATED) &&
        ((pLsppPathInfo->u1PathType == LSPP_PATH_TYPE_FEC_128) ||
         (pLsppPathInfo->u1PathType == LSPP_PATH_TYPE_FEC_129) ||
         (pLsppPathInfo->u1PathType == LSPP_PATH_TYPE_STATIC_PW)))
    {
        if (!(pLsppPathInfo->PwDetail.u1CvSelected & LSPP_VCCV_CV_TYPE))
        {
            LSPP_LOG (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
                      LSPP_INVALID_PW_CV, "LsppCoreUpdateEchoMsgInfo");
            return OSIX_FAILURE;
        }

        /* CC selected is copied to a varaible in Echo Message strucutre.
         * Based on the configured CC the Lsp Ping Echo Packets 
         * will be encapsulted for PW. The encap type is set to ACH in 
         * case of MPLS-TP PW and ACH-IP in case of MPLS PW. 
         * This encap type is used while calculating the packet size. */

        pLsppEchoMsg->u1CcSelected = pLsppPathInfo->PwDetail.u1CcSelected;

        if (pLsppPathInfo->u1PathType != LSPP_PATH_TYPE_STATIC_PW)
        {
            pLsppEchoMsg->u1EncapType = (UINT1) LSPP_ENCAP_TYPE_ACH_IP;
        }
        else
        {
            pLsppEchoMsg->u1EncapType = (UINT1) LSPP_ENCAP_TYPE_ACH;
        }
    }

    if (pRxDsMapTlv != NULL)
    {
        MEMCPY (&(pLsppEchoMsg->LsppPduInfo.LsppDsMapTlv), pRxDsMapTlv,
                sizeof (tLsppDsMapTlv));

        pLsppEchoMsg->LsppPduInfo.u2TlvsPresent =
            (pLsppEchoMsg->LsppPduInfo.u2TlvsPresent | LSPP_DSMAP_TLV);

        /* Set the Interface & Label Stack Flag when it is requested. */
        if (pLsppPingTraceEntry->MibObject.i4FsLsppInterfaceLabelTlv ==
            LSPP_SNMP_TRUE)
        {
            pLsppEchoMsg->LsppPduInfo.LsppDsMapTlv.u1DsFlags =
                LSPP_INTERFACE_AND_LBL_STACK_REQ;
        }

        bDsmapOption = OSIX_FALSE;
    }
    else if (pLsppPingTraceEntry->MibObject.i4FsLsppDsMap != LSPP_SNMP_TRUE)
    {
        bDsmapOption = OSIX_FALSE;
    }

    /* Update the Target Fec info from the Path Info in Echo message. */
    i4RetVal = LsppCoreUpdateTgtFecInfo (pLsppEchoMsg,
                                         pLsppPingTraceEntry->PathId.
                                         u1PathType);
    if (i4RetVal != OSIX_SUCCESS)
    {
        LSPP_LOG (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
                  LSPP_UPDATE_TARGET_FEC_STACK_FAILED,
                  "LsppCoreUpdateEchoMsgInfo");
        return OSIX_FAILURE;
    }

    /* Update the Out segment info for out path. */
    i4RetVal = LsppCoreUpdateOutPathInfo (pLsppEchoMsg,
                                          pLsppEchoMsg->LsppPathInfo.
                                          u1PathType);
    if (i4RetVal != OSIX_SUCCESS)
    {
        LSPP_LOG (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
                  LSPP_UPDATE_OUTSEG_INFO_STACK_FAILED,
                  "LsppCoreUpdateEchoMsgInfo");
        return OSIX_FAILURE;
    }

    /* Update the Outgoing Label stack in the echo message. */
    i4RetVal = LsppCoreUpdateOutLblInfo (pLsppEchoMsg,
                                         pLsppEchoMsg->LblInfo,
                                         &(pLsppEchoMsg->u1LabelStackDepth));
    if (i4RetVal != OSIX_SUCCESS)
    {
        LSPP_LOG (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
                  LSPP_UPDATE_OUT_LABEL_FAILED, "LsppCoreUpdateEchoMsgInfo");
        return OSIX_FAILURE;
    }

    if (bDsmapOption == OSIX_TRUE)
    {
        i4RetVal = LsppCoreUpdateDsMapInfo (pLsppEchoMsg, pLsppPingTraceEntry);
        if (i4RetVal == OSIX_FAILURE)
        {
            LSPP_LOG (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
                      LSPP_UPDATE_DSMAP_FAILED, "LsppCoreUpdateEchoMsgInfo");
            return OSIX_FAILURE;
        }
    }

    /* Update the Fec and Label Stack with NIL Fec and Explicit Null when the 
     * force explicit null option is enabled.
     */
    if (pLsppPingTraceEntry->MibObject.i4FsLsppForceExplicitNull ==
        LSPP_SNMP_TRUE)
    {
        i4RetVal = LsppCoreUpdateNilFec (pLsppEchoMsg);
        if (i4RetVal == OSIX_FAILURE)
        {
            LSPP_LOG (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
                      LSPP_UPDATE_NIL_FEC_FAILED, "LsppCoreUpdateEchoMsgInfo");
            return OSIX_FAILURE;
        }
    }

    /* Fill Ping Header with required details */
    LsppUtilFillLsppHeader (pLsppPingTraceEntry,
                            &(pLsppEchoMsg->LsppPduInfo.LsppHeader));

    /* Fill Encapsulation Details. */
    if (LsppCoreFillEncapHeader (pLsppPingTraceEntry, pLsppEchoMsg) !=
        OSIX_SUCCESS)
    {
        LSPP_LOG (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
                  LSPP_UPDATE_ENCAP_HEADER_FAILED, "LsppCoreUpdateEchoMsgInfo");
        return OSIX_FAILURE;
    }

    /* Update the configured EXP value in the outermost label. */
    pLsppEchoMsg->LblInfo[0].u1Exp =
        (UINT1) (pLsppPingTraceEntry->MibObject.u4FsLsppEXPValue);
    pLsppEchoMsg->LsppPduInfo.LsppDsMapTlv.DsLblInfo[0].u1Exp =
        (UINT1) (pLsppPingTraceEntry->MibObject.u4FsLsppEXPValue);

    /* Update the TTL based on the request type. For ping request, the default 
     * is 255. If ping is to an internediate node, then it is set to the 
     * configured value. For traceroute request, the TTL will be incremented 
     * by 1 for each request.
     */
    if (pLsppPingTraceEntry->MibObject.i4FsLsppRequestType ==
        LSPP_REQ_TYPE_PING)
    {
        pLsppEchoMsg->LblInfo[0].u1Ttl =
            (UINT1) pLsppPingTraceEntry->MibObject.u4FsLsppTTLValue;
    }
    else if (pLsppPingTraceEntry->MibObject.i4FsLsppRequestType ==
             LSPP_REQ_TYPE_TRACE_ROUTE)
    {
        pLsppEchoMsg->LblInfo[0].u1Ttl =
            (++(pLsppPingTraceEntry->u1LastSentTtl));
    }

    if (pLsppPingTraceEntry->MibObject.u4FsLsppReplyDscpValue != 0)
    {
        /* Filling Reply TOS value */
        pLsppEchoMsg->LsppPduInfo.LsppReplyTosTlv.u1TosValue =
            (UINT1) pLsppPingTraceEntry->MibObject.u4FsLsppReplyDscpValue;

        pLsppEchoMsg->LsppPduInfo.u2TlvsPresent =
            (UINT2) (pLsppEchoMsg->LsppPduInfo.u2TlvsPresent |
                     LSPP_REPLY_TOS_BYTE_TLV);
    }

    if (pLsppPingTraceEntry->MibObject.i4FsLsppRequestOwner ==
        LSPP_REQ_OWNER_BFD)
    {
        pLsppEchoMsg->LsppPduInfo.LsppBfdDiscTlv.
            u4Discriminator = u4BfdDiscriminator;

        pLsppEchoMsg->LsppPduInfo.u2TlvsPresent =
            (pLsppEchoMsg->LsppPduInfo.u2TlvsPresent |
             LSPP_BFD_DISCRIMINATOR_TLV);

        i4RetVal = LsppCoreGetBfdBtStrapRespReq
            (pLsppPingTraceEntry->MibObject.u4FsLsppContextId, &u1IsRespReq);

        if (i4RetVal == OSIX_FAILURE)
        {
            LSPP_LOG (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
                      LSPP_GET_BFD_BOOTSTRAP_RESP_REQ_FAILED,
                      "LsppCoreUpdateEchoMsgInfo");
            return OSIX_FAILURE;
        }

        /* Check whether Echo reply is required or not for this bootstrap 
         * request and updated the reply mode based on the configured value.
         */
        if (u1IsRespReq == LSPP_SNMP_FALSE)
        {
            pLsppEchoMsg->LsppPduInfo.LsppHeader.u1ReplyMode = LSPP_NO_REPLY;
        }
    }

    /* Calculating packet size */
    LsppUtilCalculatePktSize (pLsppEchoMsg);

    /* Total Lspp Packet Size Calculation */
    u4LsppPduLen = pLsppEchoMsg->u4EncapHdrLen + pLsppEchoMsg->u4LsppPduLen;

    /* Check for Sweep Option and update the next packet size to be sent. */
    if ((pLsppPingTraceEntry->MibObject.i4FsLsppSweepOption ==
         LSPP_SNMP_TRUE) &&
        (pLsppPingTraceEntry->MibObject.i4FsLsppRequestType ==
         LSPP_REQ_TYPE_PING))
    {
        if (pLsppPingTraceEntry->MibObject.u4FsLsppPktsTx != 0)
        {
            u4PktSize = pLsppPingTraceEntry->MibObject.u4FsLsppPacketSize +
                pLsppPingTraceEntry->MibObject.u4FsLsppSweepIncrement;

            if (u4PktSize > pLsppPingTraceEntry->MibObject.u4FsLsppSweepMaximum)
            {
                u4PktSize = pLsppPingTraceEntry->MibObject.u4FsLsppSweepMinimum;
            }
            pLsppPingTraceEntry->MibObject.u4FsLsppPacketSize = u4PktSize;
        }
        else
        {
            /* First ping request will be sent with the sweep minimum packet 
             * size.
             */
            pLsppPingTraceEntry->MibObject.u4FsLsppPacketSize =
                pLsppPingTraceEntry->MibObject.u4FsLsppSweepMinimum;
        }
    }

    /* Filling Pad Tlv Size */
    if ((u4LsppPduLen + LSPP_TLV_HEADER_LEN) <=
        pLsppPingTraceEntry->MibObject.u4FsLsppPacketSize)
    {
        /* Filling Pad Tlv Length */
        pLsppEchoMsg->LsppPduInfo.LsppPadTlv.u2Length =
            (UINT2) (pLsppPingTraceEntry->MibObject.u4FsLsppPacketSize -
                     (u4LsppPduLen + LSPP_TLV_HEADER_LEN));

        if (pLsppEchoMsg->LsppPduInfo.LsppPadTlv.u2Length != 0)
        {
            if (pLsppPingTraceEntry->MibObject.i4FsLsppReplyPadTlv ==
                LSPP_SNMP_TRUE)
            {
                pLsppEchoMsg->LsppPduInfo.LsppPadTlv.u1FirstOctet =
                    LSPP_COPY_PAD_TLV;
            }
            else
            {
                pLsppEchoMsg->LsppPduInfo.LsppPadTlv.u1FirstOctet =
                    LSPP_DROP_PAD_TLV;
            }

            LsppCoreUpdatePadTlvInfo (pLsppPingTraceEntry->MibObject.
                                      au1FsLsppPadPattern,
                                      (UINT1) pLsppPingTraceEntry->MibObject.
                                      i4FsLsppPadPatternLen,
                                      &(pLsppEchoMsg->LsppPduInfo.LsppPadTlv));
        }
        else
        {
            if (pLsppPingTraceEntry->MibObject.i4FsLsppReplyPadTlv ==
                LSPP_SNMP_TRUE)
            {
                LSPP_LOG (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
                          LSPP_INVALID_PACKET_SIZE_CONFIGURED,
                          "LsppCoreUpdateEchoMsgInfo");
                return OSIX_FAILURE;
            }
        }

        pLsppEchoMsg->u4LsppPduLen = pLsppEchoMsg->u4LsppPduLen +
            (pLsppEchoMsg->LsppPduInfo.LsppPadTlv.u2Length +
             LSPP_TLV_HEADER_LEN);

        pLsppEchoMsg->LsppPduInfo.u2TlvsPresent =
            (pLsppEchoMsg->LsppPduInfo.u2TlvsPresent | LSPP_PAD_TLV);
    }
    else
    {
        LSPP_LOG (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
                  LSPP_INVALID_PACKET_SIZE_CONFIGURED,
                  "LsppCoreUpdateEchoMsgInfo");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*******************************************************************************
 * FUNCTION NAME    : LsppCoreGetBfdBtStrapRespReq                             *
 * DESCRIPTION      : This function is used to get the option to check whether *
 *                    the echo reply is required for bootstrap echo request.   *
 * INPUT            : u4ContextId   - Virtual Context Identifier               *
 *                    pu1IsRespReq  - Pointer to RespReq Flag                  *
 * OUTPUT           : pu1IsRespReq  - updated vlaue                            *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                                *
 ******************************************************************************/
PUBLIC INT4
LsppCoreGetBfdBtStrapRespReq (UINT4 u4ContextId, UINT1 *pu1IsRespReq)
{
    tLsppFsLsppGlobalConfigTableEntry LsppGlobalConfigTable;
    tLsppFsLsppGlobalConfigTableEntry *pLsppGlobalConfigTable = NULL;

    MEMSET (&LsppGlobalConfigTable, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));

    LsppGlobalConfigTable.MibObject.u4FsLsppContextId = u4ContextId;

    pLsppGlobalConfigTable = LsppGetFsLsppGlobalConfigTable
        (&LsppGlobalConfigTable);

    if (pLsppGlobalConfigTable == NULL)
    {
        LSPP_LOG (LSPP_INVALID_CONTEXT,
                  LSPP_INVALID_CONTEXT_ID,
                  "LsppCoreGetBfdBtStrapRespReq", u4ContextId);
        return OSIX_FAILURE;
    }

    *pu1IsRespReq =
        (UINT1) pLsppGlobalConfigTable->MibObject.i4FsLsppBfdBtStrapRespReq;

    return OSIX_SUCCESS;
}

/*******************************************************************************
 * FUNCTION NAME    : LsppCoreGetPathInfo                                      *
 * DESCRIPTION      : This function is used to get the complete path           *
 *                    information from the respective modules based on         *
 *                    given path type.                                         *
 * INPUT            : u4ContextId   - Virtual Context Identifier               *
 *                    pLsppPathId   - Pointer to path id struture              *
 *                    u4IfIndex     - This object specifies the received       *
 *                                    interface index                          *
 * OUTPUT           : pLsppPathInfo - pointer to the Path Info structure       *
 *                                    which gets filled based on the path      *
 *                                    type.                                    *
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE                                *
 ******************************************************************************/
PUBLIC INT4
LsppCoreGetPathInfo (UINT4 u4ContextId,
                     tLsppPathId * pLsppPathId, tLsppPathInfo * pLsppPathInfo,
                     UINT4 u4IfIndex)
{
    tLsppTnlLspInfo     TeTunnelId;
    tLsppExtInParams   *pLsppExtInParams = NULL;
    tLsppExtOutParams  *pLsppExtOutParams = NULL;
    tLsppTnlLspInfo    *pTnlId = NULL;

    if ((pLsppExtInParams = (tLsppExtInParams *)
         MemAllocMemBlk (LSPP_FSLSPPINPUTPARAMS_POOLID)) == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((pLsppExtOutParams = (tLsppExtOutParams *)
         MemAllocMemBlk (LSPP_FSLSPPOUTPUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        return OSIX_FAILURE;
    }

    MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
    MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));
    MEMSET (&TeTunnelId, 0, sizeof (tLsppTnlLspInfo));

    if ((pLsppPathId == NULL) || (pLsppPathInfo == NULL))
    {
        LSPP_LOG (u4ContextId, LSPP_INVALID_PATH_ID_OR_PATH_INFO,
                  "LsppCoreGetPathInfo");
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);
        return OSIX_FAILURE;
    }

    /* Get the Meg details when the path type is MEP and also update 
     * the identified underlying path type. */
    if (pLsppPathId->u1PathType == LSPP_PATH_TYPE_MEP)
    {
        pLsppExtInParams->u4ContextId = u4ContextId;
        pLsppExtInParams->u4RequestType = LSPP_MPLS_OAM_GET_MEG_INFO;

        pLsppExtInParams->uInParams.MegInfo.MegIndices.u4MegIndex =
            pLsppPathId->unPathId.MepIndices.u4MegIndex;

        pLsppExtInParams->uInParams.MegInfo.MegIndices.u4MeIndex =
            pLsppPathId->unPathId.MepIndices.u4MeIndex;

        pLsppExtInParams->uInParams.MegInfo.MegIndices.u4MpIndex =
            pLsppPathId->unPathId.MepIndices.u4MpIndex;

        if (LsppPortHandleExtInteraction
            (pLsppExtInParams, pLsppExtOutParams) != OSIX_SUCCESS)
        {
            LSPP_LOG (u4ContextId, LSPP_FETCH_MEG_INFO_FAILED,
                      "LsppCoreGetPathInfo");
            MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtInParams);
            MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtOutParams);
            return OSIX_FAILURE;
        }
        /* Check the Meg State */
        if ((pLsppExtOutParams->unOutParams.MegDetail.u1MeState &
             OAM_ME_OPER_STATUS_MEG_DOWN) ||
            (pLsppExtOutParams->unOutParams.MegDetail.u1MeState &
             OAM_ME_OPER_STATUS_ME_DOWN) ||
            (pLsppExtOutParams->unOutParams.MegDetail.u1MeState &
             OAM_ME_OPER_STATUS_PATH_DOWN))
        {
            LSPP_LOG (u4ContextId, LSPP_UNDERLYING_PATH_DOWN,
                      "LsppCoreGetPathInfo");
            MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtInParams);
            MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtOutParams);
            return OSIX_FAILURE;
        }

        if (pLsppExtOutParams->unOutParams.MegDetail.u1ServiceType
            == OAM_SERVICE_TYPE_LSP)
        {
            pLsppPathInfo->u1PathType = LSPP_PATH_TYPE_STATIC_TNL;
        }
        else if (pLsppExtOutParams->unOutParams.MegDetail.u1ServiceType
                 == OAM_SERVICE_TYPE_PW)
        {
            pLsppPathInfo->u1PathType = LSPP_PATH_TYPE_STATIC_PW;
        }

        MEMCPY (&(pLsppPathInfo->MegDetail),
                &(pLsppExtOutParams->unOutParams.MegDetail),
                sizeof (tLsppMegDetail));

        MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
        MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));
    }

    /* Get the PW details when the path type is PW and update the 
     * path information.*/
    if ((pLsppPathId->u1PathType == LSPP_PATH_TYPE_FEC_128) ||
        (pLsppPathId->u1PathType == LSPP_PATH_TYPE_FEC_129) ||
        (pLsppPathInfo->u1PathType == LSPP_PATH_TYPE_STATIC_PW))
    {
        pLsppExtInParams->u4ContextId = u4ContextId;

        /* Update the ExtInParams to get the PW details from the L2VPN
         * module. 
         */
        if ((pLsppPathId->u1PathType == LSPP_PATH_TYPE_FEC_128) ||
            (pLsppPathId->u1PathType == LSPP_PATH_TYPE_FEC_129))
        {
            pLsppPathInfo->u1PathType = pLsppPathId->u1PathType;

            pLsppExtInParams->u4RequestType = LSPP_L2VPN_GET_PW_INFO_FRM_VC_ID;

            pLsppExtInParams->InPwFecInfo.u4VcId =
                pLsppPathId->unPathId.PwInfo.u4PwVcId;
            pLsppExtInParams->InPwFecInfo.SrcNodeId.u4NodeType =
                LSPP_MPLS_ADDR_TYPE_IPV4;
            pLsppExtInParams->InPwFecInfo.SrcNodeId.MplsRouterId.u4_addr[0] =
                pLsppPathId->unPathId.PwInfo.PeerAddr.u4_addr[0];
        }
        else
        {
            pLsppExtInParams->u4RequestType =
                LSPP_L2VPN_GET_PW_INFO_FRM_PW_INDEX;

            pLsppExtInParams->uInParams.PwFecInfo.u4PwIndex =
                pLsppPathInfo->MegDetail.unServiceInfo.ServicePwId.u4PwIndex;
        }

        if (LsppPortHandleExtInteraction (pLsppExtInParams,
                                          pLsppExtOutParams) != OSIX_SUCCESS)
        {
            LSPP_LOG (u4ContextId, LSPP_FETCH_PW_INFO_FAILED,
                      "LsppCoreGetPathInfo");
            MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtInParams);
            MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtOutParams);
            return OSIX_FAILURE;
        }
        /* Check PW State */
        if (pLsppExtOutParams->unOutParams.PwDetail.i1OperStatus !=
            MPLS_OPER_UP)
        {
            LSPP_LOG (u4ContextId, LSPP_UNDERLYING_PATH_DOWN,
                      "LsppCoreGetPathInfo");
            MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtInParams);
            MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtOutParams);
            return OSIX_FAILURE;
        }

        MEMCPY (&(pLsppPathInfo->PwDetail),
                &(pLsppExtOutParams->unOutParams.PwDetail),
                sizeof (tLsppPwDetail));

        MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
        MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));
    }

    /* Check the path type is RSVP Tunnel or MPLS-TP Static Tunnel 
     * or an underlying tunnel(RSVP-TE or Static) for a PW as path 
     * type and get the Tunnel information. */
    if ((pLsppPathId->u1PathType == LSPP_PATH_TYPE_RSVP_IPV4) ||
        (pLsppPathId->u1PathType == LSPP_PATH_TYPE_RSVP_IPV6) ||
        (pLsppPathInfo->u1PathType == LSPP_PATH_TYPE_STATIC_TNL) ||
        (pLsppPathInfo->PwDetail.u1MplsType == LSPP_L2VPN_MPLS_TYPE_TE))
    {

        pLsppExtInParams->u4ContextId = u4ContextId;
        pLsppExtInParams->u4RequestType = LSPP_MPLSDB_GET_TNL_INFO;

        /* Update the ExtInParams from the path id 
         * when the type is RSVP-TE 
         * or from Meg Path info when the path type is Static Tunnel
         * or from PW path info in case of tunnel underlying to a PW.
         */
        if (pLsppPathInfo->u1PathType == LSPP_PATH_TYPE_STATIC_TNL)
        {
            pTnlId = &(pLsppPathInfo->MegDetail.unServiceInfo.ServiceTnlId);
        }
        else if (pLsppPathInfo->PwDetail.u1MplsType == LSPP_L2VPN_MPLS_TYPE_TE)
        {
            pTnlId = &(pLsppPathInfo->PwDetail.unOutTnlInfo.TnlIndices);
        }
        else if (pLsppPathId->u1PathType == LSPP_PATH_TYPE_RSVP_IPV4)
        {
            TeTunnelId.SrcNodeId.MplsRouterId.u4_addr[0] =
                pLsppPathId->unPathId.TnlInfo.SrcNodeId.MplsRouterId.u4_addr[0];
            TeTunnelId.SrcNodeId.u4NodeType = LSPP_MPLS_ADDR_TYPE_IPV4;

            TeTunnelId.DstNodeId.MplsRouterId.u4_addr[0] =
                pLsppPathId->unPathId.TnlInfo.DstNodeId.MplsRouterId.u4_addr[0];
            TeTunnelId.DstNodeId.u4NodeType = LSPP_MPLS_ADDR_TYPE_IPV4;

            pLsppPathInfo->u1PathType = pLsppPathId->u1PathType;
            TeTunnelId.u4SrcTnlNum = pLsppPathId->unPathId.TnlInfo.u4SrcTnlId;
            TeTunnelId.u4LspNum = pLsppPathId->unPathId.TnlInfo.u4LspId;

            pTnlId = &TeTunnelId;
        }
        else if (pLsppPathId->u1PathType == LSPP_PATH_TYPE_RSVP_IPV6)
        {
            MEMCPY (TeTunnelId.SrcNodeId.MplsRouterId.u1_addr,
                    pLsppPathId->unPathId.TnlInfo.SrcNodeId.MplsRouterId.
                    u1_addr, LSPP_IPV6_ADDR_LEN);
            TeTunnelId.SrcNodeId.u4NodeType = LSPP_MPLS_ADDR_TYPE_IPV6;

            MEMCPY (TeTunnelId.DstNodeId.MplsRouterId.u1_addr,
                    pLsppPathId->unPathId.TnlInfo.DstNodeId.MplsRouterId.
                    u1_addr, LSPP_IPV6_ADDR_LEN);
            TeTunnelId.DstNodeId.u4NodeType = LSPP_MPLS_ADDR_TYPE_IPV6;

            TeTunnelId.u4SrcTnlNum = pLsppPathId->unPathId.TnlInfo.u4SrcTnlId;
            TeTunnelId.u4LspNum = pLsppPathId->unPathId.TnlInfo.u4LspId;

            pTnlId = &TeTunnelId;
        }
        else
        {
            MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtInParams);
            MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtOutParams);
            return OSIX_FAILURE;
        }

        if (pTnlId->SrcNodeId.u4NodeType == LSPP_MPLS_ADDR_TYPE_GLOBAL_NODE_ID)
        {
            pLsppExtInParams->uInParams.TnlInfo.SrcNodeId.
                MplsGlobalNodeId.u4GlobalId =
                pTnlId->SrcNodeId.MplsGlobalNodeId.u4GlobalId;

            pLsppExtInParams->uInParams.TnlInfo.SrcNodeId.
                MplsGlobalNodeId.u4NodeId =
                pTnlId->SrcNodeId.MplsGlobalNodeId.u4NodeId;

            pLsppExtInParams->uInParams.TnlInfo.DstNodeId.
                MplsGlobalNodeId.u4GlobalId =
                pTnlId->DstNodeId.MplsGlobalNodeId.u4GlobalId;

            pLsppExtInParams->uInParams.TnlInfo.DstNodeId.
                MplsGlobalNodeId.u4NodeId =
                pTnlId->DstNodeId.MplsGlobalNodeId.u4NodeId;
        }
        else if (pTnlId->SrcNodeId.u4NodeType == LSPP_MPLS_ADDR_TYPE_IPV4)
        {
            pLsppExtInParams->uInParams.TnlInfo.SrcNodeId.
                MplsRouterId.u4_addr[0] =
                pTnlId->SrcNodeId.MplsRouterId.u4_addr[0];

            pLsppExtInParams->uInParams.TnlInfo.DstNodeId.
                MplsRouterId.u4_addr[0] =
                pTnlId->DstNodeId.MplsRouterId.u4_addr[0];
        }
        else if (pTnlId->SrcNodeId.u4NodeType == LSPP_MPLS_ADDR_TYPE_IPV6)
        {
            MEMCPY (pLsppExtInParams->uInParams.TnlInfo.SrcNodeId.
                    MplsRouterId.u1_addr,
                    pTnlId->SrcNodeId.MplsRouterId.u1_addr, LSPP_IPV6_ADDR_LEN);

            MEMCPY (pLsppExtInParams->uInParams.TnlInfo.DstNodeId.
                    MplsRouterId.u1_addr,
                    pTnlId->DstNodeId.MplsRouterId.u1_addr, LSPP_IPV6_ADDR_LEN);
        }

        pLsppExtInParams->uInParams.TnlInfo.SrcNodeId.u4NodeType =
            pTnlId->SrcNodeId.u4NodeType;

        pLsppExtInParams->uInParams.TnlInfo.DstNodeId.u4NodeType =
            pTnlId->DstNodeId.u4NodeType;

        pLsppExtInParams->uInParams.TnlInfo.u4SrcTnlId = pTnlId->u4SrcTnlNum;

        pLsppExtInParams->uInParams.TnlInfo.u4LspId = pTnlId->u4LspNum;

        pLsppExtInParams->uInParams.TnlInfo.u4InIf = u4IfIndex;

        /* Get the Tunnel information from the MPLS DB module. */
        if (LsppPortHandleExtInteraction
            (pLsppExtInParams, pLsppExtOutParams) != OSIX_SUCCESS)
        {
            LSPP_LOG (u4ContextId, LSPP_FETCH_TNL_INFO_FAILED,
                      "LsppCoreGetPathInfo");
            MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtInParams);
            MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtOutParams);
            return OSIX_FAILURE;
        }
        /* Check Tunnel State */
        if (pLsppExtOutParams->unOutParams.TnlInfo.u1OperStatus != MPLS_OPER_UP)
        {
            LSPP_LOG (u4ContextId, LSPP_UNDERLYING_PATH_DOWN,
                      "LsppCoreGetPathInfo");
            MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtInParams);
            MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtOutParams);
            return OSIX_FAILURE;
        }

        MEMCPY (&(pLsppPathInfo->TeTnlInfo),
                &(pLsppExtOutParams->unOutParams.TnlInfo),
                sizeof (tLsppTeTnlInfo));

        MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
        MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));
    }

    /* Get the LSP information when the path type is LSP or an 
     * underlying path for a PW.
     */
    if ((pLsppPathId->u1PathType == LSPP_PATH_TYPE_LDP_IPV4) ||
        (pLsppPathId->u1PathType == LSPP_PATH_TYPE_LDP_IPV6) ||
        (pLsppPathInfo->PwDetail.u1MplsType == LSPP_L2VPN_MPLS_TYPE_NONTE))
    {

        pLsppExtInParams->u4ContextId = u4ContextId;

        /* Update the ExtInParams from the path id 
         * when the type is LSP 
         * or from PW path info in case of LSP underlying to a PW.
         */
        if ((pLsppPathId->u1PathType == LSPP_PATH_TYPE_LDP_IPV4) ||
            (pLsppPathId->u1PathType == LSPP_PATH_TYPE_LDP_IPV6))
        {
            pLsppPathInfo->u1PathType = pLsppPathId->u1PathType;

            if (pLsppPathId->u1PathType == LSPP_PATH_TYPE_LDP_IPV4)
            {
                pLsppExtInParams->uInParams.LdpInfo.LdpPrefix.u4_addr[0] =
                    pLsppPathId->unPathId.LdpInfo.LdpPrefix.u4_addr[0];
            }
            else if (pLsppPathId->u1PathType == LSPP_PATH_TYPE_LDP_IPV6)
            {
                MEMCPY (&
                        (pLsppExtInParams->uInParams.LdpInfo.LdpPrefix.u1_addr),
                        &(pLsppPathId->unPathId.LdpInfo.LdpPrefix.u1_addr),
                        LSPP_IPV6_ADDR_LEN);
            }

            pLsppExtInParams->u4RequestType = LSPP_MPLSDB_GET_LDP_INFO_FRM_FEC;

            pLsppExtInParams->uInParams.LdpInfo.u4AddrType =
                pLsppPathId->unPathId.LdpInfo.u4AddrType;

            pLsppExtInParams->uInParams.LdpInfo.u4PrefixLength =
                pLsppPathId->unPathId.LdpInfo.u4PrefixLength;
        }
        else if (pLsppPathInfo->PwDetail.u1MplsType ==
                 LSPP_L2VPN_MPLS_TYPE_NONTE)
        {
            LSPP_LOG (u4ContextId, LSPP_NONTE_LSP_FOR_PW_INVALID,
                      "LsppCoreGetPathInfo");
            MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtInParams);
            MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtOutParams);
            return OSIX_FAILURE;
            /*
               pLsppExtInParams->u4RequestType =
               LSPP_MPLSDB_GET_LDP_INFO_FRM_XC_INDEX;

               pLsppExtInParams->uInParams.u4XcIndex =
               pLsppPathInfo->PwDetail.unOutTnlInfo.u4NonTeXcIndex;
             */
        }

        /* Get the LSP information from the MPLS DB module. */
        if (LsppPortHandleExtInteraction (pLsppExtInParams,
                                          pLsppExtOutParams) != OSIX_SUCCESS)
        {
            LSPP_LOG (u4ContextId, LSPP_FETCH_LSP_INFO_FAILED,
                      "LsppCoreGetPathInfo");
            MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtInParams);
            MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtOutParams);
            return OSIX_FAILURE;
        }

        MEMCPY (&(pLsppPathInfo->NonTeInfo),
                &(pLsppExtOutParams->unOutParams.LdpInfo),
                sizeof (tLsppNonTeInfo));
    }

    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtInParams);
    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtOutParams);
    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : LsppCoreStartAgeOutTimer 
 * Description: This function is to start Age Out timer 
 * Input      : u4ContextId - Virtual Context Id
 *              pLsppPingTraceEntry - Ping Trace Entry
 *              u4AgeOutTmrValue - AgeOut Timer Value
 *              i4AgeOutTmrUnit - AgeOut timer Unit
 * Output     : NONE
 * Returns    : OSIX_SUCCESS or OSIX_FAILURE
 *****************************************************************************/
UINT4
LsppCoreStartAgeOutTimer (UINT4 u4ContextId,
                          tLsppFsLsppPingTraceTableEntry * pLsppPingTraceEntry,
                          UINT4 u4AgeOutTmrValue, INT4 i4AgeOutTmrUnit)
{
    UINT4               u4TimeInSec = 0;
    UINT4               u4TimeInMilliSec = 0;

    UNUSED_PARAM (u4ContextId);

    LsppUtilCalcTimeToStartTmr (i4AgeOutTmrUnit, u4AgeOutTmrValue,
                                &u4TimeInSec, &u4TimeInMilliSec);

    LsppTmrStartTmr (&(pLsppPingTraceEntry->AgeOutTimer),
                     LSPP_AGE_OUT_TMR, u4TimeInSec, u4TimeInMilliSec);

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : LsppCoreUpdatePingTraceResults  
 * Description: This function is to change the status of ping/trace route  
 * Input      : pLsppPingTraceEntry - Ping Trace Entry
 *              pLsppEchoMsg        - Pointer to Echo message strucutre
 *              u1ReturnCode        - u1Return Code for that ping instance
 * Output     : OSIX_SUCCESS or OSIX_FAILURE
 * Returns    : None
 *****************************************************************************/
UINT4
LsppCoreUpdatePingTraceResults (tLsppFsLsppPingTraceTableEntry *
                                pLsppPingTraceEntry,
                                tLsppEchoMsg * pLsppEchoMsg, UINT1 u1ReturnCode)
{
    UINT4               u4MaxReq = 0;

    /* Updating PingTraceTable Stats */
    pLsppPingTraceEntry->u4ReqCompletedCount =
        pLsppPingTraceEntry->u4ReqCompletedCount + 1;

    /* Calculating Max request to send */
    LsppUtilGetMaxReqsToSend (pLsppPingTraceEntry, &u4MaxReq);

    /* Checking whether ping/trace opertaion is completed */
    if (((pLsppPingTraceEntry->u4ReqCompletedCount +
          pLsppPingTraceEntry->MibObject.u4FsLsppPktsUnSent) == u4MaxReq) ||
        ((pLsppPingTraceEntry->MibObject.i4FsLsppRequestType ==
          LSPP_REQ_TYPE_TRACE_ROUTE) &&
         (u1ReturnCode == LSPP_EGRESS_FOR_FEC_AT_STACK_DEPTH)) ||
        ((pLsppPingTraceEntry->MibObject.i4FsLsppRequestType ==
          LSPP_REQ_TYPE_PING) &&
         (pLsppPingTraceEntry->MibObject.i4FsLsppSameSeqNumOption ==
          LSPP_SNMP_TRUE)))
    {
        if (pLsppPingTraceEntry->MibObject.u4FsLsppPktsTx ==
            pLsppPingTraceEntry->u4SuccessCount ||
            ((pLsppPingTraceEntry->MibObject.i4FsLsppSameSeqNumOption ==
              LSPP_SNMP_TRUE) && (pLsppPingTraceEntry->u4SuccessCount == 1)))
        {
            pLsppPingTraceEntry->MibObject.i4FsLsppStatus = LSPP_ECHO_SUCCESS;
        }
        else if ((pLsppPingTraceEntry->MibObject.i4FsLsppRequestType ==
                  LSPP_REQ_TYPE_TRACE_ROUTE) &&
                 (pLsppPingTraceEntry->MibObject.i4FsLsppReplyMode ==
                  LSPP_NO_REPLY) &&
                 (pLsppPingTraceEntry->MibObject.u4FsLsppPktsTx == u4MaxReq))
        {
            pLsppPingTraceEntry->MibObject.i4FsLsppStatus = LSPP_ECHO_SUCCESS;
        }
        else
        {
            pLsppPingTraceEntry->MibObject.i4FsLsppStatus = LSPP_ECHO_FAILURE;
        }

        if (pLsppPingTraceEntry->MibObject.i4FsLsppRequestType ==
            LSPP_REQ_TYPE_TRACE_ROUTE)
        {
            LSPP_LOG (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
                      LSPP_TRAP_TRACE_COMPLETED, pLsppPingTraceEntry->
                      MibObject.i4FsLsppStatus,
                      pLsppPingTraceEntry->MibObject.u4FsLsppActualHopCount);
        }
        else if (pLsppPingTraceEntry->MibObject.i4FsLsppRequestType ==
                 LSPP_REQ_TYPE_PING)
        {
            LSPP_LOG (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
                      LSPP_TRAP_PING_COMPLETED, pLsppPingTraceEntry->
                      MibObject.i4FsLsppStatus);
        }

        /* Pass the received remote discriminator to BFD module. */
        if (pLsppPingTraceEntry->MibObject.i4FsLsppRequestOwner ==
            LSPP_REQ_OWNER_BFD)

        {
            if (LsppRxSendBfdBtInfo (pLsppPingTraceEntry,
                                     &(pLsppEchoMsg->LsppPduInfo.
                                       LsppBfdDiscTlv),
                                     pLsppEchoMsg->LsppPduInfo.LsppHeader.
                                     u1ReturnCode) != OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppEchoMsg->u4ContextId,
                          LSPP_SEND_BFD_RESPONSE_FAILURE,
                          "LsppCoreUpdatePingTraceResults");
                return OSIX_FAILURE;
            }

            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_BFD_DISCRIMINATOR_FROM_REMOTE_FOR_SESSION,
                      "LsppCoreUpdatePingTraceResults: ",
                      pLsppEchoMsg->u4ContextId,
                      pLsppPingTraceEntry->u4BfdSessionIndex,
                      pLsppEchoMsg->LsppPduInfo.LsppBfdDiscTlv.u4Discriminator);

        }

        /* Specifying Validaiton of forward/reverse direction has passed */
        if ((pLsppPingTraceEntry->MibObject.i4FsLsppStatus ==
             LSPP_ECHO_SUCCESS) &&
            (pLsppPingTraceEntry->MibObject.i4FsLsppReversePathVerify ==
             LSPP_SNMP_TRUE))
        {
            pLsppPingTraceEntry->MibObject.
                i4FsLsppStatusPathDirection = LSPP_VALIDATION_DIRECTION_BOTH;
        }

        LsppCoreHandleAgeOut (pLsppPingTraceEntry);
    }
    else
    {
        /* Release the Semaphore for printing the output on the console
         * when the request is triggered from CLI.
         */
        if ((pLsppPingTraceEntry->MibObject.i4FsLsppRequestOwner ==
             LSPP_REQ_OWNER_MGMT) &&
            (pLsppPingTraceEntry->u1RequestOwnerSubType ==
             LSPP_REQ_OWNER_MGMT_CLI))
        {
            OsixSemGive (pLsppPingTraceEntry->SemId);
        }

        /* Initiate a request when the request type is traceroute. */
        if (pLsppPingTraceEntry->MibObject.i4FsLsppRequestType ==
            LSPP_REQ_TYPE_TRACE_ROUTE &&
            (pLsppPingTraceEntry->MibObject.i4FsLsppStatus ==
             LSPP_ECHO_IN_PROGRESS))
        {
            if (LsppCoreSendTraceRouteReq (pLsppPingTraceEntry, pLsppEchoMsg) !=
                OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppEchoMsg->u4ContextId,
                          LSPP_SEND_TRACEROUTE_REQ_FAILED,
                          "LsppCoreUpdatePingTraceResults");
                return OSIX_FAILURE;
            }
        }
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppCoreHandleTraceUnsent
 * DESCRIPTION      : This function is used to start WFR timer on Trace route 
 *                    unsent case 
 * Input(s)         : pLsppPingTraceEntry - Pointer to Ping trace Entry
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 **************************************************************************/
PUBLIC UINT4
LsppCoreHandleTraceUnsent (tLsppFsLsppPingTraceTableEntry * pLsppPingTraceEntry)
{
    tLsppFsLsppEchoSequenceTableEntry LsppEchoSequenceEntry;
    tLsppFsLsppEchoSequenceTableEntry *pLsppEchoSequenceEntry = NULL;
    UINT4               u4TimeInSec = 0;
    UINT4               u4TimeInMilliSec = 0;

    MEMSET (&LsppEchoSequenceEntry, 0,
            sizeof (tLsppFsLsppEchoSequenceTableEntry));

    LsppEchoSequenceEntry.MibObject.u4FsLsppContextId =
        pLsppPingTraceEntry->MibObject.u4FsLsppContextId;

    LsppEchoSequenceEntry.MibObject.u4FsLsppSenderHandle =
        pLsppPingTraceEntry->MibObject.u4FsLsppSenderHandle;

    LsppEchoSequenceEntry.MibObject.u4FsLsppSequenceNumber =
        pLsppPingTraceEntry->u4LastGeneratedSeqNum;

    pLsppEchoSequenceEntry =
        LsppGetFsLsppEchoSequenceTable (&LsppEchoSequenceEntry);

    if (pLsppEchoSequenceEntry == NULL)
    {
        LSPP_LOG (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
                  LSPP_ECHO_SEQ_ENTRY_NOT_FOUND,
                  "LsppRxMatchEchoSequenceEntry",
                  pLsppPingTraceEntry->MibObject.u4FsLsppSenderHandle,
                  pLsppPingTraceEntry->u4LastGeneratedSeqNum);
        return OSIX_FAILURE;
    }

    LsppUtilCalcTimeToStartTmr (pLsppPingTraceEntry->MibObject.
                                i4FsLsppWFRTmrUnit,
                                pLsppPingTraceEntry->MibObject.
                                u4FsLsppWFRInterval, &u4TimeInSec,
                                &u4TimeInMilliSec);

    LsppTmrStartTmr (&(pLsppEchoSequenceEntry->WFRTimer),
                     LSPP_WFR_TMR, u4TimeInSec, u4TimeInMilliSec);

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppCoreInitiateRequest
 * DESCRIPTION      : This function is used to frame and transmit 
 *                    the LSP Ping Pdu
 * Input(s)         : pLsppEchoMsg - Pointer to Echo message pdu info strucutre
 *                    pLsppPingTraceEntry - Pointer to Ping trace Entry
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 **************************************************************************/
PUBLIC UINT4
LsppCoreInitiateRequest (tLsppFsLsppPingTraceTableEntry * pLsppPingTraceEntry,
                         tLsppEchoMsg * pLsppEchoMsg,
                         tLsppDsMapTlv * pRxDsMapTlv, UINT4 u4BfdDiscriminator)
{
    UINT4               u4TimeInSec = 0;
    UINT4               u4TimeInMilliSec = 0;
    UINT4               u4MaxReqsToSend = 0;
    UINT4               u4RepeatCount = 0;
    UINT4               u4TxPktResult = 0;

    if (LsppCoreSendEchoReq (pLsppPingTraceEntry, pLsppEchoMsg,
                             pRxDsMapTlv, u4BfdDiscriminator, &u4TxPktResult) !=
        OSIX_SUCCESS)
    {
        /* Increment the packet unsent counter since the packet framing and
         * sending failed.*/
        if (pLsppPingTraceEntry->MibObject.i4FsLsppSameSeqNumOption ==
            LSPP_SNMP_TRUE)
        {
            u4RepeatCount = pLsppPingTraceEntry->MibObject.u4FsLsppRepeatCount;
        }
        else if (pLsppPingTraceEntry->MibObject.i4FsLsppBurstOption ==
                 LSPP_SNMP_TRUE)
        {
            u4RepeatCount = pLsppPingTraceEntry->MibObject.u4FsLsppBurstSize;
        }
        else
        {
            u4RepeatCount = 1;
        }

        if (u4TxPktResult == LSPP_TX_PKT_FAILED)
        {
            u4RepeatCount = u4RepeatCount -
                (pLsppPingTraceEntry->MibObject.u4FsLsppPktsTx % u4RepeatCount);
        }

        /* Updating Unsent packet count for this ping Instance and 
         * Global Unsent Counter */

        pLsppPingTraceEntry->MibObject.u4FsLsppPktsUnSent =
            pLsppPingTraceEntry->MibObject.u4FsLsppPktsUnSent + u4RepeatCount;

        while (u4RepeatCount > 0)
        {
            LsppCoreUpdateGlobalStats (pLsppEchoMsg->u4ContextId,
                                       LSPP_REQ_UNSENT, NULL);
            u4RepeatCount--;
        }

        LsppUtilGetMaxReqsToSend (pLsppPingTraceEntry, &u4MaxReqsToSend);

        if ((pLsppPingTraceEntry->u4ReqCompletedCount +
             pLsppPingTraceEntry->MibObject.u4FsLsppPktsUnSent) ==
            u4MaxReqsToSend)
        {
            pLsppPingTraceEntry->MibObject.i4FsLsppStatus = LSPP_ECHO_FAILURE;

            LsppCoreHandleAgeOut (pLsppPingTraceEntry);
        }

        else
        {
            if (pLsppPingTraceEntry->MibObject.i4FsLsppRequestType ==
                LSPP_REQ_TYPE_PING)
            {
                if ((pLsppPingTraceEntry->MibObject.
                     i4FsLsppSameSeqNumOption != LSPP_SNMP_TRUE) &&
                    ((pLsppPingTraceEntry->MibObject.u4FsLsppPktsTx +
                      pLsppPingTraceEntry->MibObject.u4FsLsppPktsUnSent) <
                     u4MaxReqsToSend))
                {
                    /*Start WTS timer */
                    LsppUtilCalcTimeToStartTmr (pLsppPingTraceEntry->MibObject.
                                                i4FsLsppWTSTmrUnit,
                                                pLsppPingTraceEntry->MibObject.
                                                u4FsLsppWTSInterval,
                                                &u4TimeInSec,
                                                &u4TimeInMilliSec);

                    LsppTmrStartTmr (&(pLsppPingTraceEntry->WTSTimer),
                                     LSPP_WTS_TMR, u4TimeInSec,
                                     u4TimeInMilliSec);
                }

                pLsppPingTraceEntry->u4LastUpdatedSeqIndex =
                    pLsppPingTraceEntry->u4LastGeneratedSeqNum;
            }
            else if (pLsppPingTraceEntry->MibObject.i4FsLsppRequestType ==
                     LSPP_REQ_TYPE_TRACE_ROUTE)
            {
                /* Start WFR Timer */
                if (LsppCoreHandleTraceUnsent (pLsppPingTraceEntry) !=
                    OSIX_SUCCESS)
                {
                    return OSIX_FAILURE;
                }
            }

            /* Release the Semaphore for Unsend result on the console
             * when the request is triggered from CLI.
             */
            if ((pLsppPingTraceEntry->MibObject.i4FsLsppRequestOwner ==
                 LSPP_REQ_OWNER_MGMT) &&
                (pLsppPingTraceEntry->u1RequestOwnerSubType ==
                 LSPP_REQ_OWNER_MGMT_CLI))
            {
                OsixSemGive (pLsppPingTraceEntry->SemId);
            }
        }
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppCoreGetModuleStatus
 * DESCRIPTION      : This function is used to get the module status for the 
 *                    given context.
 * Input(s)         : u4contextId - identifier to uniquely identify the 
 *                                  context.
 *                    pi4Status  - pointer to the status variable.
 * OUTPUT           : pi4Status  - updated status
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 **************************************************************************/
PUBLIC INT4
LsppCoreGetModuleStatus (UINT4 u4ContextId, INT4 *pi4Status)
{
    tLsppFsLsppGlobalConfigTableEntry LsppGlobalConfigEntry;

    MEMSET (&LsppGlobalConfigEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));

    LsppGlobalConfigEntry.MibObject.u4FsLsppContextId = u4ContextId;

    if (LsppGetAllFsLsppGlobalConfigTable (&LsppGlobalConfigEntry) !=
        OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    *pi4Status = LsppGlobalConfigEntry.MibObject.i4FsLsppSystemControl;

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppCoreCreateAndTriggerEcho
 * DESCRIPTION      : This function is used to allocate the Echo msg and 
 *                    triffer the LSP Ping Echo Request.
 * Input(s)         : pPingTraceEntry - Pointer to Ping trace entry
 *                    u4BfdDiscriminator - BFD dicriminator
 * OUTPUT           : NONE
 * RETURNS          : OSIX_SUCCESS / OSIX_FAILURE
 **************************************************************************/
PUBLIC INT4
LsppCoreCreateAndTriggerEcho (tLsppFsLsppPingTraceTableEntry * pPingTraceEntry,
                              UINT4 u4BfdDiscriminator)
{
    tLsppEchoMsg       *pLsppEchoMsg = NULL;
    tLsppDsMapTlv      *pDsMapTlv = NULL;
    UINT4               u4TempAddr = 0;
    UINT1               au1IpAddr[LSPP_MAX_IP_ADDR_LEN];
 
    /* Allocate memory to store the details needed to frame the packet. */
    if ((pLsppEchoMsg =
         (tLsppEchoMsg *) MemAllocMemBlk
         (gLsppGlobals.LsppFsLsppEchoMsgPoolId)) == NULL)
    {
        LSPP_CMN_TRC (pPingTraceEntry->MibObject.u4FsLsppContextId,
                      LSPP_MEM_ALLOC_FAIL, "LsppCoreCreateAndTriggerEcho");
        return OSIX_FAILURE;
    }

    MEMSET (pLsppEchoMsg, 0, sizeof (tLsppEchoMsg));
    MEMSET (au1IpAddr, 0, LSPP_MAX_IP_ADDR_LEN);

    if (LsppCoreInitiateRequest (pPingTraceEntry, pLsppEchoMsg,
                                 pDsMapTlv, u4BfdDiscriminator) != OSIX_SUCCESS)
    {
        LSPP_LOG (pPingTraceEntry->MibObject.u4FsLsppContextId,
                  LSPP_SEND_ECHO_REQ_FAILED, "LsppCoreCreateAndTriggerEcho");

        MemReleaseMemBlock (gLsppGlobals.LsppFsLsppEchoMsgPoolId,
                            (UINT1 *) pLsppEchoMsg);
        return OSIX_FAILURE;
    }
    u4TempAddr = OSIX_NTOHL (pLsppEchoMsg->LsppPduInfo.LsppDsMapTlv.DsIpAddr.ip6_addr_u.u4WordAddr[0]);
    MEMCPY (au1IpAddr, &(u4TempAddr), LSPP_IPV4_ADDR_LEN);
 
    MEMCPY (pPingTraceEntry->u1SrcTnlDsIPAddr, au1IpAddr, LSPP_IPV4_ADDR_LEN);
    pPingTraceEntry->u4SrcTnlDsLabel = pLsppEchoMsg->LsppPduInfo.LsppDsMapTlv.DsLblInfo[0].u4Label;
    pPingTraceEntry->u2SrcTnlDsMtu = pLsppEchoMsg->LsppPduInfo.LsppDsMapTlv.u2Mtu;
    pPingTraceEntry->u1SrcTnlDsLabelExp = pLsppEchoMsg->LsppPduInfo.LsppDsMapTlv.DsLblInfo[0].u1Exp;

    MemReleaseMemBlock (gLsppGlobals.LsppFsLsppEchoMsgPoolId,
                        (UINT1 *) pLsppEchoMsg);

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppCoreHandleAgeOut
 * DESCRIPTION      : This function is to Start the age-out timer once the 
 *                    status of the ping is completed or to delete the entry
 *                    if the age-out is configured to zero.
 * Input(s)         : pPingTraceEntry - Pointer to Ping trace entry
 * OUTPUT           : NONE
 * RETURNS          : NONE
 **************************************************************************/
VOID
LsppCoreHandleAgeOut (tLsppFsLsppPingTraceTableEntry * pLsppPingTraceEntry)
{
    tLsppFsLsppGlobalConfigTableEntry LsppGlobalConfigEntry;
    tLsppFsLsppGlobalConfigTableEntry *pLsppGlobalConfigEntry = NULL;
    UINT4               u4AgeOutTmrValue = 0;
    INT4                i4RetVal = OSIX_FAILURE;
    INT4                i4AgeOutTmrUnit = 0;

    MEMSET (&LsppGlobalConfigEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));

    LsppGlobalConfigEntry.MibObject.u4FsLsppContextId =
        pLsppPingTraceEntry->MibObject.u4FsLsppContextId;

    pLsppGlobalConfigEntry =
        LsppGetFsLsppGlobalConfigTable (&LsppGlobalConfigEntry);

    if (pLsppGlobalConfigEntry == NULL)
    {
        LSPP_LOG (LSPP_INVALID_CONTEXT,
                  LSPP_INVALID_CONTEXT_ID,
                  "LsppCoreHandleAgeOut",
                  pLsppPingTraceEntry->MibObject.u4FsLsppContextId);
        return;
    }

    if (pLsppPingTraceEntry->MibObject.i4FsLsppRequestOwner ==
        LSPP_REQ_OWNER_BFD)
    {
        u4AgeOutTmrValue =
            pLsppGlobalConfigEntry->MibObject.u4FsLsppBfdBtStrapAgeOutTime;

        i4AgeOutTmrUnit =
            pLsppGlobalConfigEntry->MibObject.i4FsLsppBfdBtStrapAgeOutTmrUnit;
    }
    else if (pLsppPingTraceEntry->MibObject.i4FsLsppRequestOwner ==
             LSPP_REQ_OWNER_MGMT)
    {
        u4AgeOutTmrValue = pLsppGlobalConfigEntry->MibObject.u4FsLsppAgeOutTime;

        i4AgeOutTmrUnit =
            pLsppGlobalConfigEntry->MibObject.i4FsLsppAgeOutTmrUnit;
    }

    if (u4AgeOutTmrValue != 0)
    {
        if (LsppCoreStartAgeOutTimer
            (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
             pLsppPingTraceEntry, u4AgeOutTmrValue, i4AgeOutTmrUnit) !=
            OSIX_SUCCESS)
        {

            LSPP_LOG (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
                      LSPP_AGEOUT_TIMER_START_FAILED, "LsppCoreHandleAgeOut");
            return;
        }

        LSPP_LOG (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
                  LSPP_AGEOUT_TIMER_STARTED,
                  "LsppCoreHandleAgeOut",
                  pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
                  pLsppPingTraceEntry->MibObject.u4FsLsppSenderHandle);
    }
    else
    {
        /* Since the AgeOut time is 0, delete the PingTrace entry from 
         * the database when the owner is from external trigger or 
         * SNMP manager. When the request is initiated from CLI, it 
         * will wait on the semaphore for the result, so the entry 
         * should not be deleted here in that case.
         */

        if (((pLsppPingTraceEntry->MibObject.i4FsLsppRequestOwner ==
              LSPP_REQ_OWNER_MGMT) &&
             (pLsppPingTraceEntry->u1RequestOwnerSubType ==
              LSPP_REQ_OWNER_MGMT_SNMP)) ||
            (pLsppPingTraceEntry->MibObject.i4FsLsppRequestOwner ==
             LSPP_REQ_OWNER_BFD))
        {
            /* Delete the Echo Sequences and Hop Info of this 
             * Ping Trace entry. 
             */
            i4RetVal = LsppCxtDeletePingTraceInfo (pLsppPingTraceEntry);
            if (i4RetVal == OSIX_FAILURE)
            {
                LSPP_LOG (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
                          LSPP_PING_TRACE_INFO_DELETE_FAIL,
                          "LsppCoreHandleAgeOut");
                return;
            }

            /* Delete the Ping Trace Node from the database. */
            i4RetVal = LsppCxtDeletePingTraceNode (pLsppPingTraceEntry);
            if (i4RetVal == OSIX_FAILURE)
            {
                LSPP_LOG (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
                          LSPP_PING_TRACE_NODE_DELETE_FAIL,
                          "LsppCoreHandleAgeOut");
                return;
            }
            return;
        }
    }

    /* Release the Semaphore for printing the output on the console
     * when the request is triggered from CLI.
     */
    if ((pLsppPingTraceEntry->MibObject.i4FsLsppRequestOwner ==
         LSPP_REQ_OWNER_MGMT) &&
        (pLsppPingTraceEntry->u1RequestOwnerSubType == LSPP_REQ_OWNER_MGMT_CLI))
    {
        OsixSemGive (pLsppPingTraceEntry->SemId);
    }
}

/***************************************************************************
 * FUNCTION NAME    : LsppCoreReversePathValidate
 * DESCRIPTION      : This function is to test whether the reply has come in 
 *                    reverse path of the bidirectional tunnel
 * Input(s)         : pLsppEchoMsg - Pointer to Echo Message Structure;
 *                    pPingTraceEntry - Pointer to Ping trace entry
 * OUTPUT           : NONE
 * RETURNS          : NONE
 **************************************************************************/
PUBLIC UINT4
LsppCoreReversePathValidate (tLsppEchoMsg * pLsppEchoMsg,
                             tLsppFsLsppPingTraceTableEntry * pPingTraceEntry)
{
    tLsppExtInParams   *pLsppExtInParams = NULL;
    tLsppExtOutParams  *pLsppExtOutParams = NULL;
    tLsppFecTlv        *pLsppFecTlv = NULL;
    UINT4               u4MegIndex = 0;
    UINT4               u4MeIndex = 0;
    UINT4               u4MpIndex = 0;

    if ((pLsppExtInParams = (tLsppExtInParams *)
         MemAllocMemBlk (LSPP_FSLSPPINPUTPARAMS_POOLID)) == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((pLsppExtOutParams = (tLsppExtOutParams *)
         MemAllocMemBlk (LSPP_FSLSPPOUTPUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        return OSIX_FAILURE;
    }

    MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
    MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));

    /* This piece of code may need to get modified
     * when support for H-LSP is provided */
    pLsppFecTlv = &(pLsppEchoMsg->LsppPduInfo.
                    LsppFecTlv[pLsppEchoMsg->LsppPduInfo.u1FecStackDepth - 1]);

    pLsppExtInParams->u4ContextId = pLsppEchoMsg->u4ContextId;

    if (pLsppFecTlv->u2TlvType == LSPP_FEC_STATIC_LSP)
    {
        pLsppExtInParams->u4RequestType = LSPP_MPLSDB_GET_REV_TNL_INFO;

        pLsppExtInParams->InTnlInfo.SrcNodeId.u4NodeType =
            LSPP_MPLS_ADDR_TYPE_GLOBAL_NODE_ID;

        pLsppExtInParams->uInParams.TnlInfo.SrcNodeId.
            MplsGlobalNodeId.u4GlobalId =
            pLsppFecTlv->StaticLspFec.u4SrcGlobalId;

        pLsppExtInParams->uInParams.TnlInfo.SrcNodeId.
            MplsGlobalNodeId.u4NodeId = pLsppFecTlv->StaticLspFec.u4SrcNodeId;

        pLsppExtInParams->uInParams.TnlInfo.u4SrcTnlId =
            pLsppFecTlv->StaticLspFec.u2SrcTnlNum;

        pLsppExtInParams->uInParams.TnlInfo.u4LspId =
            pLsppFecTlv->StaticLspFec.u2LspNum;

        pLsppExtInParams->uInParams.TnlInfo.DstNodeId.
            MplsGlobalNodeId.u4GlobalId =
            pLsppFecTlv->StaticLspFec.u4DstGlobalId;

        pLsppExtInParams->uInParams.TnlInfo.DstNodeId.
            MplsGlobalNodeId.u4NodeId = pLsppFecTlv->StaticLspFec.u4DstNodeId;

        pLsppExtInParams->uInParams.TnlInfo.u4DstTnlId =
            pLsppFecTlv->StaticLspFec.u2DstTnlNum;

        if (LsppPortHandleExtInteraction (pLsppExtInParams,
                                          pLsppExtOutParams) != OSIX_SUCCESS)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_FETCH_FROM_EXT_MOD_FAILED,
                      "LsppCoreReversePathValidate");
            MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtInParams);
            MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtOutParams);
            return OSIX_FAILURE;
        }

        u4MegIndex = pLsppExtOutParams->OutTnlInfo.MegIndices.u4MegIndex;
        u4MeIndex = pLsppExtOutParams->OutTnlInfo.MegIndices.u4MeIndex;
        u4MpIndex = pLsppExtOutParams->OutTnlInfo.MegIndices.u4MpIndex;
    }
    else if (pLsppFecTlv->u2TlvType == LSPP_FEC_STATIC_PW)
    {
        pLsppExtInParams->u4RequestType =
            LSPP_L2VPN_GET_PW_INFO_FRM_GENFEC_TYPE2;

        pLsppExtInParams->uInParams.PwFecInfo.u1AgiType =
            pLsppFecTlv->StaticPwFec.u1AgiType;

        pLsppExtInParams->uInParams.PwFecInfo.u1AgiLen =
            pLsppFecTlv->StaticPwFec.u1AgiLen;

        MEMCPY (pLsppExtInParams->uInParams.PwFecInfo.au1Agi,
                pLsppFecTlv->StaticPwFec.au1Agi,
                pLsppFecTlv->StaticPwFec.u1AgiLen);

        pLsppExtInParams->uInParams.PwFecInfo.SrcNodeId.
            u4NodeType = LSPP_MPLS_ADDR_TYPE_GLOBAL_NODE_ID;

        pLsppExtInParams->uInParams.PwFecInfo.SrcNodeId.
            MplsGlobalNodeId.u4GlobalId =
            pLsppFecTlv->StaticPwFec.u4DstGlobalId;

        pLsppExtInParams->uInParams.PwFecInfo.SrcNodeId.
            MplsGlobalNodeId.u4NodeId = pLsppFecTlv->StaticPwFec.u4DstNodeId;

        pLsppExtInParams->uInParams.PwFecInfo.u4SrcAcId =
            pLsppFecTlv->StaticPwFec.u4DstAcId;

        pLsppExtInParams->uInParams.PwFecInfo.DstNodeId.
            MplsGlobalNodeId.u4GlobalId =
            pLsppFecTlv->StaticPwFec.u4SrcGlobalId;

        pLsppExtInParams->uInParams.PwFecInfo.DstNodeId.
            MplsGlobalNodeId.u4NodeId = pLsppFecTlv->StaticPwFec.u4SrcNodeId;

        pLsppExtInParams->uInParams.PwFecInfo.u4DstAcId =
            pLsppFecTlv->StaticPwFec.u4SrcAcId;

        if (LsppPortHandleExtInteraction (pLsppExtInParams,
                                          pLsppExtOutParams) != OSIX_SUCCESS)
        {
            LSPP_LOG (pLsppEchoMsg->u4ContextId,
                      LSPP_FETCH_FROM_EXT_MOD_FAILED,
                      "LsppCoreReversePathValidate");
            MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtInParams);
            MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtOutParams);
            return OSIX_FAILURE;
        }

        u4MegIndex = pLsppExtOutParams->OutPwDetail.MegIndices.u4MegIndex;
        u4MeIndex = pLsppExtOutParams->OutPwDetail.MegIndices.u4MeIndex;
        u4MpIndex = pLsppExtOutParams->OutPwDetail.MegIndices.u4MpIndex;
    }

    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtInParams);
    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtOutParams);

    if ((pPingTraceEntry->PathId.unPathId.MepIndices.u4MegIndex !=
         u4MegIndex) ||
        (pPingTraceEntry->PathId.unPathId.MepIndices.u4MeIndex !=
         u4MeIndex) ||
        (pPingTraceEntry->PathId.unPathId.MepIndices.u4MpIndex != u4MpIndex))
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppCoreGetNodeId
 * DESCRIPTION      : This function is to get Self Node ID
 * Input(s)         : u4ContextId - Context Id
 * OUTPUT           : pu4SrcIp - Update Source Ip
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 **************************************************************************/
PUBLIC UINT4
LsppCoreGetNodeId (UINT4 u4ContextId, tLsppNodeId * pLsppNodeId)
{
    tLsppExtInParams   *pLsppExtInParams = NULL;
    tLsppExtOutParams  *pLsppExtOutParams = NULL;
    INT4                i4RetVal = 0;

    if ((pLsppExtInParams = (tLsppExtInParams *)
         MemAllocMemBlk (LSPP_FSLSPPINPUTPARAMS_POOLID)) == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((pLsppExtOutParams = (tLsppExtOutParams *)
         MemAllocMemBlk (LSPP_FSLSPPOUTPUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        return OSIX_FAILURE;
    }

    MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
    MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));

    pLsppExtInParams->u4ContextId = u4ContextId;
    pLsppExtInParams->u4RequestType = LSPP_MPLS_OAM_GET_NODE_ID;

    /* Get the Node Id from MPLS module. */
    i4RetVal = LsppPortHandleExtInteraction (pLsppExtInParams,
                                             pLsppExtOutParams);
    if (i4RetVal == OSIX_FAILURE)
    {
        LSPP_LOG (u4ContextId,
                  LSPP_FETCH_ROUTER_ID_FAILED, "LsppCoreVerifyDsmap");
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);

        return OSIX_FAILURE;
    }

    MEMCPY (pLsppNodeId, &(pLsppExtOutParams->OutLsppNodeId),
            sizeof (tLsppNodeId));

    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtInParams);
    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtOutParams);

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppCoreCalcRtt
 * DESCRIPTION      : This function is to Calculate the Round Trip Time
 * Input(s)         : pLsppEchoPktRxTime - Reply Packet Received Time 
 *                    pLsppEchoMsg - Echo message where received packet Info is
 *                                   present  
 * OUTPUT           : pu4Rtt- RTT value
 * RETURNS          : OSIX_SUCCESS/OSIX_FAILURE
 **************************************************************************/
PUBLIC UINT4
LsppCoreCalcRtt (tLsppEchoMsg * pLsppEchoMsg,
                 tLsppSysTime * pLsppEchoPktRxTime, UINT4 *pu4Rtt)
{
    UINT4               u4LsppTxTimeInSec = 0;
    UINT4               u4LsppTxTimeInMSec = 0;
    UINT4               u4LsppRxTimeInSec = 0;
    UINT4               u4LsppRxTimeInMSec = 0;
    UINT4               u4RttInSec = 0;
    UINT4               u4RttInMSec = 0;

    u4LsppTxTimeInSec = pLsppEchoMsg->LsppPduInfo.
        LsppHeader.u4TimeStampSentInSec;
    u4LsppTxTimeInMSec = pLsppEchoMsg->LsppPduInfo.LsppHeader.
        u4TimeStampSentInMicroSec;
    u4LsppRxTimeInSec = pLsppEchoPktRxTime->u4TimeInSec;
    u4LsppRxTimeInMSec = pLsppEchoPktRxTime->u4TimeInMicroSec;

    if (u4LsppRxTimeInSec == u4LsppTxTimeInSec)
    {
        if (u4LsppRxTimeInMSec < u4LsppTxTimeInMSec)
        {
            return OSIX_FAILURE;
        }
        else
        {
            u4RttInMSec = u4LsppRxTimeInMSec - u4LsppTxTimeInMSec;
        }
    }
    else if (u4LsppRxTimeInSec > u4LsppTxTimeInSec)
    {
        if (u4LsppRxTimeInMSec >= u4LsppTxTimeInMSec)
        {
            u4RttInSec = u4LsppRxTimeInSec - u4LsppTxTimeInSec;
            u4RttInMSec = u4LsppRxTimeInMSec - u4LsppTxTimeInMSec;
        }
        else
        {
            u4RttInSec = (u4LsppRxTimeInSec - u4LsppTxTimeInSec) - 1;
            u4RttInMSec = ((LSPP_MAX_MICRO_SECONDS - u4LsppTxTimeInMSec) +
                           u4LsppRxTimeInMSec);
        }
    }
    else if (u4LsppRxTimeInSec < u4LsppTxTimeInSec)
    {
        if (u4LsppRxTimeInMSec >= u4LsppTxTimeInMSec)
        {
            u4RttInSec = (LSPP_MAX_SECONDS - u4LsppTxTimeInSec) +
                u4LsppRxTimeInSec;
            u4RttInMSec = u4LsppRxTimeInMSec - u4LsppTxTimeInMSec;
        }
        else
        {
            u4RttInSec = ((LSPP_MAX_SECONDS - u4LsppTxTimeInSec) +
                          u4LsppRxTimeInSec) - 1;
            u4RttInMSec = ((LSPP_MAX_MICRO_SECONDS - u4LsppTxTimeInMSec) +
                           u4LsppRxTimeInMSec);
        }
    }

    *pu4Rtt = ((u4RttInSec * LSPP_NO_OF_MILLISECS_IN_SEC) +
               (u4RttInMSec / LSPP_NO_OF_MICROSECS_IN_MILLSEC));

    return OSIX_SUCCESS;
}

#endif /* _LSPPCORE_C_ */
