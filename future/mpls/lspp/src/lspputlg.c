/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: lspputlg.c,v 1.8 2012/01/24 13:27:26 siva Exp $
*
* Description: This file contains utility functions used by protocol Lspp
*********************************************************************/

#include "lsppinc.h"

/****************************************************************************
 Function    :  LsppUtlCreateRBTree
 Input       :  None
 Description :  This function creates all 
                the RBTree required.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
PUBLIC INT4
LsppUtlCreateRBTree ()
{

    if (LsppFsLsppGlobalConfigTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (LsppFsLsppGlobalStatsTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (LsppFsLsppPingTraceTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (LsppFsLsppEchoSequenceTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }

    if (LsppFsLsppHopTableCreate () == OSIX_FAILURE)

    {
        return (OSIX_FAILURE);
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  LsppFsLsppGlobalConfigTableCreate
 Input       :  None
 Description :  This function creates the FsLsppGlobalConfigTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
LsppFsLsppGlobalConfigTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tLsppFsLsppGlobalConfigTableEntry,
                       MibObject.FsLsppGlobalConfigTableNode);

    if ((gLsppGlobals.LsppGlbMib.FsLsppGlobalConfigTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsLsppGlobalConfigTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  LsppFsLsppGlobalStatsTableCreate
 Input       :  None
 Description :  This function creates the FsLsppGlobalStatsTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
LsppFsLsppGlobalStatsTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tLsppFsLsppGlobalStatsTableEntry,
                       MibObject.FsLsppGlobalStatsTableNode);

    if ((gLsppGlobals.LsppGlbMib.FsLsppGlobalStatsTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsLsppGlobalStatsTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  LsppFsLsppPingTraceTableCreate
 Input       :  None
 Description :  This function creates the FsLsppPingTraceTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
LsppFsLsppPingTraceTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tLsppFsLsppPingTraceTableEntry,
                       MibObject.FsLsppPingTraceTableNode);

    if ((gLsppGlobals.LsppGlbMib.FsLsppPingTraceTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsLsppPingTraceTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  LsppFsLsppEchoSequenceTableCreate
 Input       :  None
 Description :  This function creates the FsLsppEchoSequenceTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
LsppFsLsppEchoSequenceTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tLsppFsLsppEchoSequenceTableEntry,
                       MibObject.FsLsppEchoSequenceTableNode);

    if ((gLsppGlobals.LsppGlbMib.FsLsppEchoSequenceTable =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               FsLsppEchoSequenceTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  LsppFsLsppHopTableCreate
 Input       :  None
 Description :  This function creates the FsLsppHopTable
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/

PUBLIC INT4
LsppFsLsppHopTableCreate ()
{
    UINT4               u4RBNodeOffset = 0;
    u4RBNodeOffset =
        FSAP_OFFSETOF (tLsppFsLsppHopTableEntry, MibObject.FsLsppHopTableNode);

    if ((gLsppGlobals.LsppGlbMib.FsLsppHopTable =
         RBTreeCreateEmbedded (u4RBNodeOffset, FsLsppHopTableRBCmp)) == NULL)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  FsLsppGlobalConfigTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsLsppGlobalConfigTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsLsppGlobalConfigTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tLsppFsLsppGlobalConfigTableEntry *pFsLsppGlobalConfigTableEntry1 =
        (tLsppFsLsppGlobalConfigTableEntry *) pRBElem1;
    tLsppFsLsppGlobalConfigTableEntry *pFsLsppGlobalConfigTableEntry2 =
        (tLsppFsLsppGlobalConfigTableEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsLsppGlobalConfigTableEntry1->MibObject.u4FsLsppContextId >
        pFsLsppGlobalConfigTableEntry2->MibObject.u4FsLsppContextId)
    {
        return 1;
    }
    else if (pFsLsppGlobalConfigTableEntry1->MibObject.u4FsLsppContextId <
             pFsLsppGlobalConfigTableEntry2->MibObject.u4FsLsppContextId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsLsppGlobalStatsTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsLsppGlobalStatsTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsLsppGlobalStatsTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tLsppFsLsppGlobalStatsTableEntry *pFsLsppGlobalStatsTableEntry1 =
        (tLsppFsLsppGlobalStatsTableEntry *) pRBElem1;
    tLsppFsLsppGlobalStatsTableEntry *pFsLsppGlobalStatsTableEntry2 =
        (tLsppFsLsppGlobalStatsTableEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsLsppGlobalStatsTableEntry1->MibObject.u4FsLsppContextId >
        pFsLsppGlobalStatsTableEntry2->MibObject.u4FsLsppContextId)
    {
        return 1;
    }
    else if (pFsLsppGlobalStatsTableEntry1->MibObject.u4FsLsppContextId <
             pFsLsppGlobalStatsTableEntry2->MibObject.u4FsLsppContextId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsLsppPingTraceTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsLsppPingTraceTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsLsppPingTraceTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tLsppFsLsppPingTraceTableEntry *pFsLsppPingTraceTableEntry1 =
        (tLsppFsLsppPingTraceTableEntry *) pRBElem1;
    tLsppFsLsppPingTraceTableEntry *pFsLsppPingTraceTableEntry2 =
        (tLsppFsLsppPingTraceTableEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsLsppPingTraceTableEntry1->MibObject.u4FsLsppSenderHandle >
        pFsLsppPingTraceTableEntry2->MibObject.u4FsLsppSenderHandle)
    {
        return 1;
    }
    else if (pFsLsppPingTraceTableEntry1->MibObject.u4FsLsppSenderHandle <
             pFsLsppPingTraceTableEntry2->MibObject.u4FsLsppSenderHandle)
    {
        return -1;
    }

    if (pFsLsppPingTraceTableEntry1->MibObject.u4FsLsppContextId >
        pFsLsppPingTraceTableEntry2->MibObject.u4FsLsppContextId)
    {
        return 1;
    }
    else if (pFsLsppPingTraceTableEntry1->MibObject.u4FsLsppContextId <
             pFsLsppPingTraceTableEntry2->MibObject.u4FsLsppContextId)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsLsppEchoSequenceTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsLsppEchoSequenceTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsLsppEchoSequenceTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tLsppFsLsppEchoSequenceTableEntry *pFsLsppEchoSequenceTableEntry1 =
        (tLsppFsLsppEchoSequenceTableEntry *) pRBElem1;
    tLsppFsLsppEchoSequenceTableEntry *pFsLsppEchoSequenceTableEntry2 =
        (tLsppFsLsppEchoSequenceTableEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsLsppEchoSequenceTableEntry1->MibObject.u4FsLsppContextId >
        pFsLsppEchoSequenceTableEntry2->MibObject.u4FsLsppContextId)
    {
        return 1;
    }
    else if (pFsLsppEchoSequenceTableEntry1->MibObject.u4FsLsppContextId <
             pFsLsppEchoSequenceTableEntry2->MibObject.u4FsLsppContextId)
    {
        return -1;
    }

    if (pFsLsppEchoSequenceTableEntry1->MibObject.u4FsLsppSenderHandle >
        pFsLsppEchoSequenceTableEntry2->MibObject.u4FsLsppSenderHandle)
    {
        return 1;
    }
    else if (pFsLsppEchoSequenceTableEntry1->MibObject.u4FsLsppSenderHandle <
             pFsLsppEchoSequenceTableEntry2->MibObject.u4FsLsppSenderHandle)
    {
        return -1;
    }

    if (pFsLsppEchoSequenceTableEntry1->MibObject.u4FsLsppSequenceNumber >
        pFsLsppEchoSequenceTableEntry2->MibObject.u4FsLsppSequenceNumber)
    {
        return 1;
    }
    else if (pFsLsppEchoSequenceTableEntry1->MibObject.u4FsLsppSequenceNumber <
             pFsLsppEchoSequenceTableEntry2->MibObject.u4FsLsppSequenceNumber)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsLsppHopTableRBCmp
 Input       :  pRBElem1 - Pointer to the node1 for comparision
                pRBElem2 - Pointer to the node2 for comparision
 Description :  RBTree Compare function for
                FsLsppHopTable table
                Table i.e. a RBTree.
 Output      :  None
 Returns     :  1,-1 0
****************************************************************************/

PUBLIC INT4
FsLsppHopTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tLsppFsLsppHopTableEntry *pFsLsppHopTableEntry1 =
        (tLsppFsLsppHopTableEntry *) pRBElem1;
    tLsppFsLsppHopTableEntry *pFsLsppHopTableEntry2 =
        (tLsppFsLsppHopTableEntry *) pRBElem2;
    INT4                i4MaxLength = 0;

    if (pFsLsppHopTableEntry1->MibObject.u4FsLsppContextId >
        pFsLsppHopTableEntry2->MibObject.u4FsLsppContextId)
    {
        return 1;
    }
    else if (pFsLsppHopTableEntry1->MibObject.u4FsLsppContextId <
             pFsLsppHopTableEntry2->MibObject.u4FsLsppContextId)
    {
        return -1;
    }

    if (pFsLsppHopTableEntry1->MibObject.u4FsLsppSenderHandle >
        pFsLsppHopTableEntry2->MibObject.u4FsLsppSenderHandle)
    {
        return 1;
    }
    else if (pFsLsppHopTableEntry1->MibObject.u4FsLsppSenderHandle <
             pFsLsppHopTableEntry2->MibObject.u4FsLsppSenderHandle)
    {
        return -1;
    }

    if (pFsLsppHopTableEntry1->MibObject.u4FsLsppHopIndex >
        pFsLsppHopTableEntry2->MibObject.u4FsLsppHopIndex)
    {
        return 1;
    }
    else if (pFsLsppHopTableEntry1->MibObject.u4FsLsppHopIndex <
             pFsLsppHopTableEntry2->MibObject.u4FsLsppHopIndex)
    {
        return -1;
    }

    UNUSED_PARAM (i4MaxLength);

    return 0;
}

/****************************************************************************
 Function    :  FsLsppGlobalConfigTableFilterInputs
 Input       :  The Indices
                pLsppFsLsppGlobalConfigTableEntry
                pLsppSetFsLsppGlobalConfigTableEntry
                pLsppIsSetFsLsppGlobalConfigTableEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsLsppGlobalConfigTableFilterInputs (tLsppFsLsppGlobalConfigTableEntry *
                                     pLsppFsLsppGlobalConfigTableEntry,
                                     tLsppFsLsppGlobalConfigTableEntry *
                                     pLsppSetFsLsppGlobalConfigTableEntry,
                                     tLsppIsSetFsLsppGlobalConfigTableEntry *
                                     pLsppIsSetFsLsppGlobalConfigTableEntry)
{
    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppContextId == OSIX_TRUE)
    {
        if (pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId ==
            pLsppSetFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId)
            pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppContextId =
                OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppSystemControl ==
        OSIX_TRUE)
    {
        if (pLsppFsLsppGlobalConfigTableEntry->MibObject.
            i4FsLsppSystemControl ==
            pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
            i4FsLsppSystemControl)
            pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppSystemControl =
                OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppTrapStatus == OSIX_TRUE)
    {
        if (pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppTrapStatus ==
            pLsppSetFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppTrapStatus)
            pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppTrapStatus =
                OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppTraceLevel == OSIX_TRUE)
    {
        if (pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppTraceLevel ==
            pLsppSetFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppTraceLevel)
            pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppTraceLevel =
                OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppAgeOutTime == OSIX_TRUE)
    {
        if (pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppAgeOutTime ==
            pLsppSetFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppAgeOutTime)
            pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppAgeOutTime =
                OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppAgeOutTmrUnit ==
        OSIX_TRUE)
    {
        if (pLsppFsLsppGlobalConfigTableEntry->MibObject.
            i4FsLsppAgeOutTmrUnit ==
            pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
            i4FsLsppAgeOutTmrUnit)
            pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppAgeOutTmrUnit =
                OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppClearEchoStats ==
        OSIX_TRUE)
    {
        if (pLsppFsLsppGlobalConfigTableEntry->MibObject.
            i4FsLsppClearEchoStats ==
            pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
            i4FsLsppClearEchoStats)
            pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppClearEchoStats =
                OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppBfdBtStrapRespReq ==
        OSIX_TRUE)
    {
        if (pLsppFsLsppGlobalConfigTableEntry->MibObject.
            i4FsLsppBfdBtStrapRespReq ==
            pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
            i4FsLsppBfdBtStrapRespReq)
            pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppBfdBtStrapRespReq =
                OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppBfdBtStrapAgeOutTime ==
        OSIX_TRUE)
    {
        if (pLsppFsLsppGlobalConfigTableEntry->MibObject.
            u4FsLsppBfdBtStrapAgeOutTime ==
            pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
            u4FsLsppBfdBtStrapAgeOutTime)
            pLsppIsSetFsLsppGlobalConfigTableEntry->
                bFsLsppBfdBtStrapAgeOutTime = OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppGlobalConfigTableEntry->
        bFsLsppBfdBtStrapAgeOutTmrUnit == OSIX_TRUE)
    {
        if (pLsppFsLsppGlobalConfigTableEntry->MibObject.
            i4FsLsppBfdBtStrapAgeOutTmrUnit ==
            pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
            i4FsLsppBfdBtStrapAgeOutTmrUnit)
            pLsppIsSetFsLsppGlobalConfigTableEntry->
                bFsLsppBfdBtStrapAgeOutTmrUnit = OSIX_FALSE;
    }
    if ((pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppContextId == OSIX_FALSE)
        && (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppSystemControl ==
            OSIX_FALSE)
        && (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppTrapStatus ==
            OSIX_FALSE)
        && (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppTraceLevel ==
            OSIX_FALSE)
        && (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppAgeOutTime ==
            OSIX_FALSE)
        && (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppAgeOutTmrUnit ==
            OSIX_FALSE)
        && (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppClearEchoStats ==
            OSIX_FALSE)
        && (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppBfdBtStrapRespReq ==
            OSIX_FALSE)
        && (pLsppIsSetFsLsppGlobalConfigTableEntry->
            bFsLsppBfdBtStrapAgeOutTime == OSIX_FALSE)
        && (pLsppIsSetFsLsppGlobalConfigTableEntry->
            bFsLsppBfdBtStrapAgeOutTmrUnit == OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  FsLsppPingTraceTableFilterInputs
 Input       :  The Indices
                pLsppFsLsppPingTraceTableEntry
                pLsppSetFsLsppPingTraceTableEntry
                pLsppIsSetFsLsppPingTraceTableEntry
 Description :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsLsppPingTraceTableFilterInputs (tLsppFsLsppPingTraceTableEntry *
                                  pLsppFsLsppPingTraceTableEntry,
                                  tLsppFsLsppPingTraceTableEntry *
                                  pLsppSetFsLsppPingTraceTableEntry,
                                  tLsppIsSetFsLsppPingTraceTableEntry *
                                  pLsppIsSetFsLsppPingTraceTableEntry)
{
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle == OSIX_TRUE)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle ==
            pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle)
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle =
                OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRequestType == OSIX_TRUE)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRequestType ==
            pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppRequestType)
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRequestType =
                OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPathType == OSIX_TRUE)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppPathType ==
            pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppPathType)
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPathType = OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPathPointer == OSIX_TRUE)
    {
        if ((MEMCMP
             (pLsppFsLsppPingTraceTableEntry->MibObject.au4FsLsppPathPointer,
              pLsppSetFsLsppPingTraceTableEntry->MibObject.au4FsLsppPathPointer,
              pLsppSetFsLsppPingTraceTableEntry->MibObject.
              i4FsLsppPathPointerLen * sizeof (UINT4)) == 0)
            && (pLsppFsLsppPingTraceTableEntry->MibObject.
                i4FsLsppPathPointerLen ==
                pLsppSetFsLsppPingTraceTableEntry->MibObject.
                i4FsLsppPathPointerLen))
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPathPointer =
                OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTgtMipGlobalId == OSIX_TRUE)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipGlobalId ==
            pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipGlobalId)
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTgtMipGlobalId =
                OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTgtMipNodeId == OSIX_TRUE)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipNodeId ==
            pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipNodeId)
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTgtMipNodeId =
                OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTgtMipIfNum == OSIX_TRUE)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipIfNum ==
            pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipIfNum)
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTgtMipIfNum =
                OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReplyMode == OSIX_TRUE)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppReplyMode ==
            pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppReplyMode)
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReplyMode = OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRepeatCount == OSIX_TRUE)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppRepeatCount ==
            pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppRepeatCount)
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRepeatCount =
                OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPacketSize == OSIX_TRUE)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppPacketSize ==
            pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppPacketSize)
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPacketSize = OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPadPattern == OSIX_TRUE)
    {
        if ((MEMCMP
             (pLsppFsLsppPingTraceTableEntry->MibObject.au1FsLsppPadPattern,
              pLsppSetFsLsppPingTraceTableEntry->MibObject.au1FsLsppPadPattern,
              pLsppSetFsLsppPingTraceTableEntry->MibObject.
              i4FsLsppPadPatternLen) == 0)
            && (pLsppFsLsppPingTraceTableEntry->MibObject.
                i4FsLsppPadPatternLen ==
                pLsppSetFsLsppPingTraceTableEntry->MibObject.
                i4FsLsppPadPatternLen))
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPadPattern = OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTTLValue == OSIX_TRUE)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTTLValue ==
            pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppTTLValue)
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTTLValue = OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWFRInterval == OSIX_TRUE)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppWFRInterval ==
            pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppWFRInterval)
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWFRInterval =
                OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWFRTmrUnit == OSIX_TRUE)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppWFRTmrUnit ==
            pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppWFRTmrUnit)
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWFRTmrUnit = OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWTSInterval == OSIX_TRUE)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppWTSInterval ==
            pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppWTSInterval)
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWTSInterval =
                OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWTSTmrUnit == OSIX_TRUE)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppWTSTmrUnit ==
            pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppWTSTmrUnit)
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWTSTmrUnit = OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReplyDscpValue == OSIX_TRUE)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppReplyDscpValue ==
            pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppReplyDscpValue)
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReplyDscpValue =
                OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepOption == OSIX_TRUE)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppSweepOption ==
            pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppSweepOption)
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepOption =
                OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepMinimum == OSIX_TRUE)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepMinimum ==
            pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepMinimum)
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepMinimum =
                OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepMaximum == OSIX_TRUE)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepMaximum ==
            pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepMaximum)
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepMaximum =
                OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepIncrement == OSIX_TRUE)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepIncrement ==
            pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepIncrement)
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepIncrement =
                OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppBurstOption == OSIX_TRUE)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppBurstOption ==
            pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppBurstOption)
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppBurstOption =
                OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppBurstSize == OSIX_TRUE)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppBurstSize ==
            pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppBurstSize)
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppBurstSize = OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppEXPValue == OSIX_TRUE)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppEXPValue ==
            pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppEXPValue)
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppEXPValue = OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppDsMap == OSIX_TRUE)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppDsMap ==
            pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppDsMap)
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppDsMap = OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppFecValidate == OSIX_TRUE)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppFecValidate ==
            pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppFecValidate)
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppFecValidate =
                OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReplyPadTlv == OSIX_TRUE)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppReplyPadTlv ==
            pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppReplyPadTlv)
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReplyPadTlv =
                OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppForceExplicitNull ==
        OSIX_TRUE)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.
            i4FsLsppForceExplicitNull ==
            pLsppSetFsLsppPingTraceTableEntry->MibObject.
            i4FsLsppForceExplicitNull)
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppForceExplicitNull =
                OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppInterfaceLabelTlv ==
        OSIX_TRUE)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.
            i4FsLsppInterfaceLabelTlv ==
            pLsppSetFsLsppPingTraceTableEntry->MibObject.
            i4FsLsppInterfaceLabelTlv)
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppInterfaceLabelTlv =
                OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSameSeqNumOption ==
        OSIX_TRUE)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.
            i4FsLsppSameSeqNumOption ==
            pLsppSetFsLsppPingTraceTableEntry->MibObject.
            i4FsLsppSameSeqNumOption)
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSameSeqNumOption =
                OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppVerbose == OSIX_TRUE)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppVerbose ==
            pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppVerbose)
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppVerbose = OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReversePathVerify ==
        OSIX_TRUE)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.
            i4FsLsppReversePathVerify ==
            pLsppSetFsLsppPingTraceTableEntry->MibObject.
            i4FsLsppReversePathVerify)
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReversePathVerify =
                OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppEncapType == OSIX_TRUE)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppEncapType ==
            pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppEncapType)
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppEncapType = OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRowStatus == OSIX_TRUE)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRowStatus ==
            pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppRowStatus)
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRowStatus = OSIX_FALSE;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId == OSIX_TRUE)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId ==
            pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId)
            pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_FALSE;
    }
    if ((pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle == OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRequestType ==
            OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPathType == OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPathPointer ==
            OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTgtMipGlobalId ==
            OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTgtMipNodeId ==
            OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTgtMipIfNum ==
            OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReplyMode == OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRepeatCount ==
            OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPacketSize ==
            OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPadPattern ==
            OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTTLValue == OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWFRInterval ==
            OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWFRTmrUnit ==
            OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWTSInterval ==
            OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWTSTmrUnit ==
            OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReplyDscpValue ==
            OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepOption ==
            OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepMinimum ==
            OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepMaximum ==
            OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepIncrement ==
            OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppBurstOption ==
            OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppBurstSize == OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppEXPValue == OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppDsMap == OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppFecValidate ==
            OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReplyPadTlv ==
            OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppForceExplicitNull ==
            OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppInterfaceLabelTlv ==
            OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSameSeqNumOption ==
            OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppVerbose == OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReversePathVerify ==
            OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppEncapType == OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRowStatus == OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 Function    :  LsppSetAllFsLsppGlobalConfigTableTrigger
 Input       :  The Indices
                pLsppSetFsLsppGlobalConfigTableEntry
                pLsppIsSetFsLsppGlobalConfigTableEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
LsppSetAllFsLsppGlobalConfigTableTrigger (tLsppFsLsppGlobalConfigTableEntry *
                                          pLsppSetFsLsppGlobalConfigTableEntry,
                                          tLsppIsSetFsLsppGlobalConfigTableEntry
                                          *
                                          pLsppIsSetFsLsppGlobalConfigTableEntry,
                                          INT4 i4SetOption)
{

    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppContextId == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppContextId, 13, LsppMainTaskLock, LsppMainTaskUnLock,
                      0, 0, 1, i4SetOption, "%u %u",
                      pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
                      u4FsLsppContextId);
    }
    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppSystemControl ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppSystemControl, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
                      i4FsLsppSystemControl);
    }
    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppTrapStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppTrapStatus, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
                      i4FsLsppTrapStatus);
    }
    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppTraceLevel == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppTraceLevel, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
                      i4FsLsppTraceLevel);
    }
    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppAgeOutTime == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppAgeOutTime, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
                      u4FsLsppAgeOutTime);
    }
    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppAgeOutTmrUnit ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppAgeOutTmrUnit, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
                      i4FsLsppAgeOutTmrUnit);
    }
    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppClearEchoStats ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppClearEchoStats, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
                      i4FsLsppClearEchoStats);
    }
    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppBfdBtStrapRespReq ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppBfdBtStrapRespReq, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
                      i4FsLsppBfdBtStrapRespReq);
    }
    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppBfdBtStrapAgeOutTime ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppBfdBtStrapAgeOutTime, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %u",
                      pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
                      u4FsLsppBfdBtStrapAgeOutTime);
    }
    if (pLsppIsSetFsLsppGlobalConfigTableEntry->
        bFsLsppBfdBtStrapAgeOutTmrUnit == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppBfdBtStrapAgeOutTmrUnit, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 1, i4SetOption, "%u %i",
                      pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
                      i4FsLsppBfdBtStrapAgeOutTmrUnit);
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  LsppSetAllFsLsppPingTraceTableTrigger
 Input       :  The Indices
                pLsppSetFsLsppPingTraceTableEntry
                pLsppIsSetFsLsppPingTraceTableEntry
 Description :  This Routine is used to send 
                MSR and RM indication
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
LsppSetAllFsLsppPingTraceTableTrigger (tLsppFsLsppPingTraceTableEntry *
                                       pLsppSetFsLsppPingTraceTableEntry,
                                       tLsppIsSetFsLsppPingTraceTableEntry *
                                       pLsppIsSetFsLsppPingTraceTableEntry,
                                       INT4 i4SetOption)
{
    tSNMP_OID_TYPE     *pFsLsppPathPointerVal = NULL;
    tSNMP_OCTET_STRING_TYPE FsLsppPadPatternVal;
    UINT1               au1FsLsppPadPatternVal[256];

    pFsLsppPathPointerVal = alloc_oid (MAX_OID_LEN);

    if (pFsLsppPathPointerVal == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (au1FsLsppPadPatternVal, 0, sizeof (au1FsLsppPadPatternVal));
    FsLsppPadPatternVal.pu1_OctetList = au1FsLsppPadPatternVal;
    FsLsppPadPatternVal.i4_Length = 0;

    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppSenderHandle, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %u",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRequestType == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppRequestType, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %i",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      i4FsLsppRequestType);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPathType == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppPathType, 13, LsppMainTaskLock, LsppMainTaskUnLock,
                      0, 0, 2, i4SetOption, "%u %u %i",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      i4FsLsppPathType);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPathPointer == OSIX_TRUE)
    {
        MEMCPY (pFsLsppPathPointerVal->pu4_OidList,
                pLsppSetFsLsppPingTraceTableEntry->MibObject.
                au4FsLsppPathPointer,
                (pLsppSetFsLsppPingTraceTableEntry->MibObject.
                 i4FsLsppPathPointerLen * sizeof (UINT4)));
        pFsLsppPathPointerVal->u4_Length =
            (UINT4) pLsppSetFsLsppPingTraceTableEntry->MibObject.
            i4FsLsppPathPointerLen;

        nmhSetCmnNew (FsLsppPathPointer, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %o",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle, pFsLsppPathPointerVal);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTgtMipGlobalId == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppTgtMipGlobalId, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %u",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppTgtMipGlobalId);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTgtMipNodeId == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppTgtMipNodeId, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %u",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppTgtMipNodeId);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTgtMipIfNum == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppTgtMipIfNum, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %u",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppTgtMipIfNum);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReplyMode == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppReplyMode, 13, LsppMainTaskLock, LsppMainTaskUnLock,
                      0, 0, 2, i4SetOption, "%u %u %i",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      i4FsLsppReplyMode);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRepeatCount == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppRepeatCount, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %u",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppRepeatCount);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPacketSize == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppPacketSize, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %u",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppPacketSize);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPadPattern == OSIX_TRUE)
    {
        MEMCPY (FsLsppPadPatternVal.pu1_OctetList,
                pLsppSetFsLsppPingTraceTableEntry->MibObject.
                au1FsLsppPadPattern,
                pLsppSetFsLsppPingTraceTableEntry->MibObject.
                i4FsLsppPadPatternLen);
        FsLsppPadPatternVal.i4_Length =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppPadPatternLen;

        nmhSetCmnNew (FsLsppPadPattern, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %s",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle, &FsLsppPadPatternVal);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTTLValue == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppTTLValue, 13, LsppMainTaskLock, LsppMainTaskUnLock,
                      0, 0, 2, i4SetOption, "%u %u %u",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppTTLValue);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWFRInterval == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppWFRInterval, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %u",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppWFRInterval);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWFRTmrUnit == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppWFRTmrUnit, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %i",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      i4FsLsppWFRTmrUnit);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWTSInterval == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppWTSInterval, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %u",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppWTSInterval);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWTSTmrUnit == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppWTSTmrUnit, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %i",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      i4FsLsppWTSTmrUnit);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReplyDscpValue == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppReplyDscpValue, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %u",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppReplyDscpValue);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepOption == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppSweepOption, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %i",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      i4FsLsppSweepOption);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepMinimum == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppSweepMinimum, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %u",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSweepMinimum);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepMaximum == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppSweepMaximum, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %u",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSweepMaximum);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepIncrement == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppSweepIncrement, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %u",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSweepIncrement);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppBurstOption == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppBurstOption, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %i",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      i4FsLsppBurstOption);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppBurstSize == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppBurstSize, 13, LsppMainTaskLock, LsppMainTaskUnLock,
                      0, 0, 2, i4SetOption, "%u %u %u",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppBurstSize);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppEXPValue == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppEXPValue, 13, LsppMainTaskLock, LsppMainTaskUnLock,
                      0, 0, 2, i4SetOption, "%u %u %u",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppEXPValue);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppDsMap == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppDsMap, 13, LsppMainTaskLock, LsppMainTaskUnLock, 0,
                      0, 2, i4SetOption, "%u %u %i",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      i4FsLsppDsMap);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppFecValidate == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppFecValidate, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %i",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      i4FsLsppFecValidate);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReplyPadTlv == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppReplyPadTlv, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %i",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      i4FsLsppReplyPadTlv);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppForceExplicitNull ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppForceExplicitNull, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %i",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      i4FsLsppForceExplicitNull);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppInterfaceLabelTlv ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppInterfaceLabelTlv, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %i",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      i4FsLsppInterfaceLabelTlv);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSameSeqNumOption ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppSameSeqNumOption, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %i",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      i4FsLsppSameSeqNumOption);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppVerbose == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppVerbose, 13, LsppMainTaskLock, LsppMainTaskUnLock,
                      0, 0, 2, i4SetOption, "%u %u %i",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      i4FsLsppVerbose);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReversePathVerify ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppReversePathVerify, 13, LsppMainTaskLock,
                      LsppMainTaskUnLock, 0, 0, 2, i4SetOption, "%u %u %i",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      i4FsLsppReversePathVerify);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppEncapType == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppEncapType, 13, LsppMainTaskLock, LsppMainTaskUnLock,
                      0, 0, 2, i4SetOption, "%u %u %i",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      i4FsLsppEncapType);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppRowStatus, 13, LsppMainTaskLock, LsppMainTaskUnLock,
                      0, 1, 2, i4SetOption, "%u %u %i",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      i4FsLsppRowStatus);
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId == OSIX_TRUE)
    {
        nmhSetCmnNew (FsLsppContextId, 13, LsppMainTaskLock, LsppMainTaskUnLock,
                      0, 0, 2, i4SetOption, "%u %u %u",
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle,
                      pLsppSetFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId);
    }

    SNMP_FreeOid (pFsLsppPathPointerVal);
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  LsppFsLsppGlobalConfigTableCreateApi
 Input       :  pLsppFsLsppGlobalConfigTableEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tLsppFsLsppGlobalConfigTableEntry *
LsppFsLsppGlobalConfigTableCreateApi (tLsppFsLsppGlobalConfigTableEntry *
                                      pSetLsppFsLsppGlobalConfigTableEntry)
{
    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigTableEntry = NULL;

    if (pSetLsppFsLsppGlobalConfigTableEntry == NULL)
    {
        LSPP_TRC ((LSPP_UTIL_TRC,
                   "LsppFsLsppGlobalConfigTableCreatApi: pSetLsppFsLsppGlobalConfigTableEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pLsppFsLsppGlobalConfigTableEntry =
        (tLsppFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);
    if (pLsppFsLsppGlobalConfigTableEntry == NULL)
    {
        LSPP_TRC ((LSPP_UTIL_TRC,
                   "LsppFsLsppGlobalConfigTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pLsppFsLsppGlobalConfigTableEntry,
                pSetLsppFsLsppGlobalConfigTableEntry,
                sizeof (tLsppFsLsppGlobalConfigTableEntry));
        if (RBTreeAdd
            (gLsppGlobals.LsppGlbMib.FsLsppGlobalConfigTable,
             (tRBElem *) pLsppFsLsppGlobalConfigTableEntry) != RB_SUCCESS)
        {
            LSPP_TRC ((LSPP_UTIL_TRC,
                       "LsppFsLsppGlobalConfigTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                                (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
            return NULL;
        }
        return pLsppFsLsppGlobalConfigTableEntry;
    }
}

/****************************************************************************
 Function    :  LsppFsLsppGlobalStatsTableCreateApi
 Input       :  pLsppFsLsppGlobalStatsTableEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tLsppFsLsppGlobalStatsTableEntry *
LsppFsLsppGlobalStatsTableCreateApi (tLsppFsLsppGlobalStatsTableEntry *
                                     pSetLsppFsLsppGlobalStatsTableEntry)
{
    tLsppFsLsppGlobalStatsTableEntry *pLsppFsLsppGlobalStatsTableEntry = NULL;

    if (pSetLsppFsLsppGlobalStatsTableEntry == NULL)
    {
        LSPP_TRC ((LSPP_UTIL_TRC,
                   "LsppFsLsppGlobalStatsTableCreatApi: pSetLsppFsLsppGlobalStatsTableEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pLsppFsLsppGlobalStatsTableEntry =
        (tLsppFsLsppGlobalStatsTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID);
    if (pLsppFsLsppGlobalStatsTableEntry == NULL)
    {
        LSPP_TRC ((LSPP_UTIL_TRC,
                   "LsppFsLsppGlobalStatsTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pLsppFsLsppGlobalStatsTableEntry,
                pSetLsppFsLsppGlobalStatsTableEntry,
                sizeof (tLsppFsLsppGlobalStatsTableEntry));
        if (RBTreeAdd
            (gLsppGlobals.LsppGlbMib.FsLsppGlobalStatsTable,
             (tRBElem *) pLsppFsLsppGlobalStatsTableEntry) != RB_SUCCESS)
        {
            LSPP_TRC ((LSPP_UTIL_TRC,
                       "LsppFsLsppGlobalStatsTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                                (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
            return NULL;
        }
        return pLsppFsLsppGlobalStatsTableEntry;
    }
}

/****************************************************************************
 Function    :  LsppFsLsppPingTraceTableCreateApi
 Input       :  pLsppFsLsppPingTraceTableEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tLsppFsLsppPingTraceTableEntry *
LsppFsLsppPingTraceTableCreateApi (tLsppFsLsppPingTraceTableEntry *
                                   pSetLsppFsLsppPingTraceTableEntry)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    if (pSetLsppFsLsppPingTraceTableEntry == NULL)
    {
        LSPP_TRC ((LSPP_UTIL_TRC,
                   "LsppFsLsppPingTraceTableCreatApi: pSetLsppFsLsppPingTraceTableEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        LSPP_TRC ((LSPP_UTIL_TRC,
                   "LsppFsLsppPingTraceTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pLsppFsLsppPingTraceTableEntry,
                pSetLsppFsLsppPingTraceTableEntry,
                sizeof (tLsppFsLsppPingTraceTableEntry));
        if (RBTreeAdd
            (gLsppGlobals.LsppGlbMib.FsLsppPingTraceTable,
             (tRBElem *) pLsppFsLsppPingTraceTableEntry) != RB_SUCCESS)
        {
            LSPP_TRC ((LSPP_UTIL_TRC,
                       "LsppFsLsppPingTraceTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppFsLsppPingTraceTableEntry);
            return NULL;
        }
        return pLsppFsLsppPingTraceTableEntry;
    }
}

/****************************************************************************
 Function    :  LsppFsLsppEchoSequenceTableCreateApi
 Input       :  pLsppFsLsppEchoSequenceTableEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tLsppFsLsppEchoSequenceTableEntry *
LsppFsLsppEchoSequenceTableCreateApi (tLsppFsLsppEchoSequenceTableEntry *
                                      pSetLsppFsLsppEchoSequenceTableEntry)
{
    tLsppFsLsppEchoSequenceTableEntry *pLsppFsLsppEchoSequenceTableEntry = NULL;

    if (pSetLsppFsLsppEchoSequenceTableEntry == NULL)
    {
        LSPP_TRC ((LSPP_UTIL_TRC,
                   "LsppFsLsppEchoSequenceTableCreatApi: pSetLsppFsLsppEchoSequenceTableEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pLsppFsLsppEchoSequenceTableEntry =
        (tLsppFsLsppEchoSequenceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPECHOSEQUENCETABLE_POOLID);
    if (pLsppFsLsppEchoSequenceTableEntry == NULL)
    {
        LSPP_TRC ((LSPP_UTIL_TRC,
                   "LsppFsLsppEchoSequenceTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pLsppFsLsppEchoSequenceTableEntry,
                pSetLsppFsLsppEchoSequenceTableEntry,
                sizeof (tLsppFsLsppEchoSequenceTableEntry));
        if (RBTreeAdd
            (gLsppGlobals.LsppGlbMib.FsLsppEchoSequenceTable,
             (tRBElem *) pLsppFsLsppEchoSequenceTableEntry) != RB_SUCCESS)
        {
            LSPP_TRC ((LSPP_UTIL_TRC,
                       "LsppFsLsppEchoSequenceTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (LSPP_FSLSPPECHOSEQUENCETABLE_POOLID,
                                (UINT1 *) pLsppFsLsppEchoSequenceTableEntry);
            return NULL;
        }
        return pLsppFsLsppEchoSequenceTableEntry;
    }
}

/****************************************************************************
 Function    :  LsppFsLsppHopTableCreateApi
 Input       :  pLsppFsLsppHopTableEntry
 Description :  This Routine Creates the memory 
 Output      :  None
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/

tLsppFsLsppHopTableEntry *
LsppFsLsppHopTableCreateApi (tLsppFsLsppHopTableEntry *
                             pSetLsppFsLsppHopTableEntry)
{
    tLsppFsLsppHopTableEntry *pLsppFsLsppHopTableEntry = NULL;

    if (pSetLsppFsLsppHopTableEntry == NULL)
    {
        LSPP_TRC ((LSPP_UTIL_TRC,
                   "LsppFsLsppHopTableCreatApi: pSetLsppFsLsppHopTableEntry is NULL.\r\n"));
        return NULL;
    }
    /* Allocate memory for the new node */
    pLsppFsLsppHopTableEntry =
        (tLsppFsLsppHopTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPHOPTABLE_POOLID);
    if (pLsppFsLsppHopTableEntry == NULL)
    {
        LSPP_TRC ((LSPP_UTIL_TRC,
                   "LsppFsLsppHopTableCreatApi: Fail to Allocate Memory.\r\n"));
        return NULL;
    }
    else
    {
        MEMCPY (pLsppFsLsppHopTableEntry, pSetLsppFsLsppHopTableEntry,
                sizeof (tLsppFsLsppHopTableEntry));
        if (RBTreeAdd
            (gLsppGlobals.LsppGlbMib.FsLsppHopTable,
             (tRBElem *) pLsppFsLsppHopTableEntry) != RB_SUCCESS)
        {
            LSPP_TRC ((LSPP_UTIL_TRC,
                       "LsppFsLsppHopTableCreatApi: Fail to Add the node.\r\n"));
            MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                                (UINT1 *) pLsppFsLsppHopTableEntry);
            return NULL;
        }
        return pLsppFsLsppHopTableEntry;
    }
}
