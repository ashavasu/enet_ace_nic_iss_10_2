/******************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lsppque.c,v 1.12 2013/06/27 12:23:21 siva Exp $
 *
 * Description : This file contains the processing of 
 *               messages queued
 *****************************************************************************/
#include "lsppinc.h"

/****************************************************************************
*                                                                           *
* Function     : LsppQueProcessMsgs                                         *
*                                                                           *
* Description  : Deques and Process the msgs                                *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
LsppQueProcessMsgs ()
{
    tLsppQMsg          *pLsppQueMsg = NULL;

    while (OsixQueRecv (gLsppGlobals.lsppQueId,
                        (UINT1 *) &pLsppQueMsg, OSIX_DEF_MSG_LEN,
                        OSIX_NO_WAIT) == OSIX_SUCCESS)
    {
        switch (pLsppQueMsg->u1MsgType)
        {
            case LSPP_CREATE_CONTEXT_MSG:

                /* From VCM to create a context */
                if (LsppCxtCreateContext (pLsppQueMsg->u4ContextId) !=
                    OSIX_SUCCESS)
                {
                    LSPP_LOG (LSPP_INVALID_CONTEXT,
                              LSPP_CXT_CREATION_FAILED,
                              "LsppQueProcessMsgs", pLsppQueMsg->u4ContextId);
                }
                break;

            case LSPP_DELETE_CONTEXT_MSG:

                /* From VCM to delete a context */
                if (LsppCxtDeleteContext (pLsppQueMsg->u4ContextId, OSIX_TRUE)
                    != OSIX_SUCCESS)
                {
                    LSPP_LOG (LSPP_INVALID_CONTEXT,
                              LSPP_CXT_DELETION_FAILED,
                              "LsppQueProcessMsgs", pLsppQueMsg->u4ContextId);
                }
                break;

            case LSPP_RX_PDU_MSG:

                /* Message from MPLS-RTR to recive the PDU */
                if (LsppQueRxPdu (pLsppQueMsg->u4ContextId,
                                  &(pLsppQueMsg->unMsgParam.LsppRxPduInfo)) !=
                    OSIX_SUCCESS)
                {
                    LSPP_LOG (pLsppQueMsg->u4ContextId,
                              LSPP_PROCESS_RX_PKT_FAIL, "LsppQueProcessMsgs");
                }

                /* Release the received CRU buffer. */
                LSPP_RELEASE_CRU_BUF (pLsppQueMsg->unMsgParam.
                                      LsppRxPduInfo.pBuf, 0);
                break;

            case LSPP_BFD_BOOTSTRAP_ECHO_REQ:

                /* Message from BFD to bootstrap Ping request. */
                if (LsppExtTriggerPing (pLsppQueMsg->u4ContextId,
                                        &(pLsppQueMsg->unMsgParam.
                                          LsppExtTrigInfo)) != OSIX_SUCCESS)
                {
                    LSPP_LOG (pLsppQueMsg->u4ContextId, LSPP_EXT_TRIG_FAILED,
                              "LsppQueProcessMsgs");
                }
                break;

            case LSPP_BFD_BOOTSTRAP_ECHO_REPLY:

                if (LsppTxBfdBtStrapReplyPkt (pLsppQueMsg->u4ContextId,
                                              &(pLsppQueMsg->unMsgParam.
                                                LsppBfdInfo)) != OSIX_SUCCESS)
                {
                    LSPP_RELEASE_CRU_BUF
                        (pLsppQueMsg->unMsgParam.LsppBfdInfo.pBuf, FALSE);
                }

                break;

            default:
                LSPP_LOG (pLsppQueMsg->u4ContextId,
                          LSPP_INVALID_MSG_TYPE_IN_QUE, "LsppQueProcessMsgs");
                break;
        }

        /* Releasing the memory after dequeue */
        if (MemReleaseMemBlock (gLsppGlobals.LsppFsLsppQMsgPoolId,
                                (UINT1 *) pLsppQueMsg) == MEM_FAILURE)
        {
            LSPP_CMN_TRC (pLsppQueMsg->u4ContextId, LSPP_MEM_RELEASE_FAILED,
                          "LsppQueProcessMsgs", LSPP_QUEUE_NAME);
            return;
        }
    }
    return;
}

/****************************************************************************
*                                                                           *
* Function     : LsppQueEnqMsg                                              *
*                                                                           *
* Description  : This function is to Enqueue the mesage and send an event   * 
*                to the module                                              *
*                                                                           *
* Input        : pMsg - Pointer to the message                              *
*                pu1ErrorCode - pointer to the error code variable where the*
*                               error code is set on failure.               *
*                                                                           *
* Output       : pu1ErrorCode - updated pointer with error code.            *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*****************************************************************************/
INT4
LsppQueEnqMsg (tLsppQMsg * pMsg, UINT1 *pu1ErrorCode)
{
    if (OsixQueSend (gLsppGlobals.lsppQueId, (UINT1 *) &pMsg,
                     OSIX_DEF_MSG_LEN) == OSIX_FAILURE)
    {
        LSPP_CMN_TRC (pMsg->u4ContextId, LSPP_QUEUE_SEND_FAILED,
                      "LsppQueEnqMsg");
        *pu1ErrorCode = LSPP_MSG_ENQUEUE_FAILED;
        return OSIX_FAILURE;
    }

    if (OsixEvtSend (gLsppGlobals.lsppTaskId, LSPP_QMSG_EVENT) != OSIX_SUCCESS)
    {
        LSPP_CMN_TRC (pMsg->u4ContextId, LSPP_EVENT_SEND_FAILED,
                      " System Task failed\n");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : LsppQueRxPdu                                               *
*                                                                           *
* Description  : This function copies the received packet to linear buffer, *
*                allocates memory for Echo message structure and gives the  *
*                packet to Rx sub-module for further processing.            *
*                                                                           *
* Input        : u4ContextId                                                *
*                pLsppRxPduInfo - Pointer to the received Pdu Information   *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*****************************************************************************/
INT4
LsppQueRxPdu (UINT4 u4ContextId, tLsppRxPduInfo * pLsppRxPduInfo)
{
    tLsppEchoMsg       *pLsppEchoMsg = NULL;
    UINT1              *pu1RxPktTmp = NULL;
    UINT1              *pu1RxPkt = NULL;
    UINT4               u4PktSize = 0;

    if ((pu1RxPktTmp =
         (UINT1 *) MemAllocMemBlk (LSPP_FSLSPP_PDU_BUFFER_POOLID)) == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pu1RxPktTmp, 0, LSPP_MAX_PDU_LEN);

    if (pLsppRxPduInfo->pBuf == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPP_PDU_BUFFER_POOLID,
                            (UINT1 *) pu1RxPktTmp);
        return OSIX_FAILURE;
    }

    u4PktSize = LSPP_GET_CRU_VALID_BYTE_COUNT (pLsppRxPduInfo->pBuf);

    pu1RxPkt = (UINT1 *) CRU_BUF_Get_DataPtr_IfLinear (pLsppRxPduInfo->pBuf,
                                                       0, u4PktSize);
    if (pu1RxPkt == NULL)
    {
        /* Copy the received packet to linear buffer. */
        if (CRU_BUF_Copy_FromBufChain (pLsppRxPduInfo->pBuf, pu1RxPktTmp,
                                       0, u4PktSize) < ((INT4) u4PktSize))
        {
            MemReleaseMemBlock (LSPP_FSLSPP_PDU_BUFFER_POOLID,
                                (UINT1 *) pu1RxPktTmp);
            return OSIX_FAILURE;
        }
        pu1RxPkt = pu1RxPktTmp;
    }

    if ((pLsppEchoMsg = (tLsppEchoMsg *)
         MemAllocMemBlk (gLsppGlobals.LsppFsLsppEchoMsgPoolId)) == NULL)
    {
        LSPP_CMN_TRC (u4ContextId, LSPP_MEM_ALLOC_FAIL, "LsppQueRxPdu");
        MemReleaseMemBlock (LSPP_FSLSPP_PDU_BUFFER_POOLID,
                            (UINT1 *) pu1RxPktTmp);
        return OSIX_FAILURE;
    }

    MEMSET (pLsppEchoMsg, 0, sizeof (tLsppEchoMsg));

    pLsppEchoMsg->u4ContextId = u4ContextId;
    pLsppEchoMsg->u4IfIndex = pLsppRxPduInfo->u4IfIndex;

    /* Give the packet to Rx sub-module for further packet processing. */
    if (LsppRxProcessPdu (pLsppEchoMsg, pu1RxPkt, u4PktSize, OSIX_TRUE) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (gLsppGlobals.LsppFsLsppEchoMsgPoolId,
                            (UINT1 *) pLsppEchoMsg);
        MemReleaseMemBlock (LSPP_FSLSPP_PDU_BUFFER_POOLID,
                            (UINT1 *) pu1RxPktTmp);
        return OSIX_FAILURE;
    }

    /* Release the Echo message memory. */
    MemReleaseMemBlock (gLsppGlobals.LsppFsLsppEchoMsgPoolId,
                        (UINT1 *) pLsppEchoMsg);
    MemReleaseMemBlock (LSPP_FSLSPP_PDU_BUFFER_POOLID, (UINT1 *) pu1RxPktTmp);
    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  lsppque.c                      */
/*-----------------------------------------------------------------------*/
