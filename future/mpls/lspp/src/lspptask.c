/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lspptask.c,v 1.3 2010/10/22 13:10:57 prabuc Exp $
 *
 * Description:This file contains procedures related to
 *             LSPP - Task Initialization
 *******************************************************************/

#include "lsppinc.h"

/*****************************************************************************/
/*                                                                           */
/* Function     : LsppTaskSpawnLsppTask()                                    */
/*                                                                           */
/* Description  : This procedure is provided to Spawn Lspp Task              */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/

PUBLIC VOID
LsppTaskSpawnLsppTask (INT1 *pu1Dummy)
{
    UNUSED_PARAM (pu1Dummy);

    LSPP_TRC_FUNC ((LSPP_FN_ENTRY, "FUNC:LsppTaskSpawnLsppTask\n"));

    /* task initializations */
    if (LsppMainTaskInit () == OSIX_FAILURE)
    {
        LSPP_TRC ((LSPP_TASK_TRC, " !!!!! LSPP TASK INIT FAILURE  !!!!! \n"));
        LsppMainDeInit ();
        lrInitComplete (OSIX_FAILURE);
        return;
    }

    /* Register the protocol MIB with SNMP */
    RegisterFSLSPP ();
    
    lrInitComplete (OSIX_SUCCESS);

    LsppMainTask ();

    LSPP_TRC_FUNC ((LSPP_FN_EXIT, "EXIT:LsppTaskSpawnLsppTask\n"));

    return;
}

/*------------------------------------------------------------------------*/
/*                        End of the file  lspptask.c                     */
/*------------------------------------------------------------------------*/
