
 /******************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 *  $Id: lspptrap.c,v 1.10 2013/06/27 12:23:22 siva Exp $
 *
 * Description : This file contains the functions to send trap to the manager
 *                
 * ****************************************************************************/

#include "lsppinc.h"
#include "fslspp.h"

PRIVATE tSNMP_OID_TYPE *LsppTrapMakeObjIdFrmString PROTO ((INT1 *, UINT1 *));

PRIVATE INT4 LsppTrapParseSubIdNew PROTO ((UINT1 **, UINT4 *));

/******************************************************************************
 * Function   : LsppTrapSendTrapNotifications
 *
 * Description: 
 * 
 * Input      : 
 * Output     : None
 * Returns    : 
 * 
 *****************************************************************************/

VOID
LsppTrapSendTrapNotifications (va_list vTrapArgs, UINT4 u4ContextId,
                               UINT1 *pu1ContextName, INT4 i4TrapType)
{
    tLsppExtInParams    LsppExtInParams;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    UINT4               au4SnmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    tLsppExtOutParams  *pLsppExtOutParams = NULL;
    tSNMP_VAR_BIND     *pVbList = NULL;
    tSNMP_VAR_BIND     *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL;
    tSNMP_OID_TYPE     *pSnmpOid = NULL;
    tSNMP_OID_TYPE     *pEnterpriseOid = NULL;
    tSNMP_OCTET_STRING_TYPE *pOstring = NULL;
    UINT4               u4GenTrapType = ENTERPRISE_SPECIFIC;
    UINT4               u4TrapObjVal = 0;
    UINT4               u4OidLen = 0;
    UINT1               au1Buf[LSPP_MAX_NAME_LEN];

    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    MEMSET (&LsppExtInParams, 0, sizeof (tLsppExtInParams));

    u4OidLen = (sizeof (au4SnmpTrapOid) / sizeof (UINT4));
    pSnmpOid = alloc_oid (u4OidLen);

    if (pSnmpOid == NULL)
    {
        return;
    }

    MEMCPY (pSnmpOid->pu4_OidList, au4SnmpTrapOid, sizeof (au4SnmpTrapOid));
    pSnmpOid->u4_Length = u4OidLen;

    MEMSET (au1Buf, 0, sizeof (au1Buf));
    SNPRINTF ((CHR1 *) au1Buf, (LSPP_MAX_NAME_LEN), "fsLsppTraps");

    pEnterpriseOid = LsppTrapMakeObjIdFrmString ((INT1 *) au1Buf,
                                                 (UINT1 *)
                                                 fs_lspp_orig_mib_oid_table);

    if (pEnterpriseOid == NULL)
    {
        SNMP_FreeOid (pSnmpOid);
        return;
    }

    switch (i4TrapType)
    {
        case LSPP_PING_COMPLETION_TRAP:

            pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] =
                LSPP_PING_COMPLETION_TRAP_OID;
            break;

        case LSPP_TRACE_ROUTE_COMPLETION_TRAP:

            pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] =
                LSPP_TRACE_ROUTE_COMPLETION_TRAP_OID;
            break;

        case LSPP_BFD_BTSTRAP_TRAP:

            pEnterpriseOid->pu4_OidList[pEnterpriseOid->u4_Length++] =
                LSPP_BFD_BTSTRAP_TRAP_OID;
            break;

        default:
            SNMP_FreeOid (pSnmpOid);
            SNMP_FreeOid (pEnterpriseOid);
            return;
    }

    pVbList = SNMP_AGT_FormVarBind (pSnmpOid, SNMP_DATA_TYPE_OBJECT_ID,
                                    0L, 0, NULL, pEnterpriseOid, SnmpCnt64Type);

    if (pVbList == NULL)
    {
        SNMP_FreeOid (pSnmpOid);
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }

    pStartVb = pVbList;

    /* Filling the Context-Name */
    MEMSET (au1Buf, 0, sizeof (au1Buf));
    SNPRINTF ((CHR1 *) au1Buf, LSPP_MAX_NAME_LEN, "fsLsppTrapContextName");

    pOid = LsppTrapMakeObjIdFrmString ((INT1 *) au1Buf,
                                       (UINT1 *) fs_lspp_orig_mib_oid_table);

    if (pOid == NULL)
    {
        SNMP_FreeOid (pSnmpOid);
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    pOstring = SNMP_AGT_FormOctetString (pu1ContextName,
                                         (LSPP_MAX_CONTEXT_NAME_LEN - 1));

    if (pOstring == NULL)
    {
        SNMP_FreeOid (pSnmpOid);
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    pVbList->pNextVarBind = SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_OCTET_PRIM,
                                                  0, 0, pOstring, NULL,
                                                  SnmpCnt64Type);

    if (pVbList->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pSnmpOid);
        SNMP_AGT_FreeOctetString (pOstring);
        SNMP_FreeOid (pOid);
        SNMP_FreeOid (pEnterpriseOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        return;
    }

    pVbList = pVbList->pNextVarBind;

    MEMSET (au1Buf, 0, sizeof (au1Buf));
    switch (i4TrapType)
    {
        case LSPP_PING_COMPLETION_TRAP:

            u4TrapObjVal = va_arg (vTrapArgs, UINT4);
            SNPRINTF ((CHR1 *) au1Buf, LSPP_MAX_NAME_LEN, "fsLsppStatus");
            break;

        case LSPP_TRACE_ROUTE_COMPLETION_TRAP:

            u4TrapObjVal = va_arg (vTrapArgs, UINT4);
            SNPRINTF ((CHR1 *) au1Buf, LSPP_MAX_NAME_LEN, "fsLsppStatus");
            if (LsppTrapFormVarBindList (au1Buf, u4TrapObjVal, &pVbList,
                                         SnmpCnt64Type) != OSIX_SUCCESS)
            {
                SNMP_FreeOid (pSnmpOid);
                SNMP_AGT_FreeVarBindList (pStartVb);
                SNMP_FreeOid (pEnterpriseOid);
                return;
            }

            MEMSET (au1Buf, 0, sizeof (au1Buf));
            u4TrapObjVal = va_arg (vTrapArgs, UINT4);
            SNPRINTF ((CHR1 *) au1Buf, LSPP_MAX_NAME_LEN,
                      "fsLsppActualHopCount");
            break;

        case LSPP_BFD_BTSTRAP_TRAP:

            u4TrapObjVal = va_arg (vTrapArgs, UINT4);
            SNPRINTF ((CHR1 *) au1Buf, LSPP_MAX_NAME_LEN, "fsLsppRequestOwner");
            break;

    }

    if (LsppTrapFormVarBindList (au1Buf, u4TrapObjVal, &pVbList,
                                 SnmpCnt64Type) != OSIX_SUCCESS)
    {
        SNMP_FreeOid (pSnmpOid);
        SNMP_AGT_FreeVarBindList (pStartVb);
        SNMP_FreeOid (pEnterpriseOid);
        return;
    }
    pVbList->pNextVarBind = NULL;

    LsppExtInParams.u4ContextId = u4ContextId;
    LsppExtInParams.u4RequestType = LSPP_FM_SEND_TRAP;

    LsppExtInParams.uInParams.FmFaultMsg.pTrapMsg = pStartVb;
    LsppExtInParams.uInParams.FmFaultMsg.u4GenTrapType = u4GenTrapType;
    LsppExtInParams.uInParams.FmFaultMsg.u4SpecTrapType = (UINT4) i4TrapType;
    LsppExtInParams.uInParams.FmFaultMsg.u4ModuleId = FM_NOTIFY_MOD_ID_LSPP;

    if (LsppPortHandleExtInteraction (&LsppExtInParams, pLsppExtOutParams) !=
        OSIX_SUCCESS)
    {
        return;
    }

    return;
}

/******************************************************************************
 * * Function :   LsppTrapMakeObjIdFrmString
 * *
 * * Description: This Function retuns the OID  of the given string for the
 * *              proprietary MIB.
 * *
 * * Input    :   pi1TextStr - pointer to the string.
 * *              pTableName - TableName has to be fetched.
 * *
 * * Output   :   None.
 * *
 * * Returns  :   pOidPtr or NULL
 * ****************************************************************************/

PRIVATE tSNMP_OID_TYPE *
LsppTrapMakeObjIdFrmString (INT1 *pi1TextStr, UINT1 *pu1TableName)
{
    tSNMP_OID_TYPE     *pOidPtr = NULL;
    INT1               *pi1DotPtr = NULL;
    UINT2               u2Index = 0;
    UINT2               u2DotCount = 0;
    INT1                ai1TempBuffer[LSPP_MAX_NAME_LEN];
    UINT1              *pu1TempPtr = NULL;
    struct MIB_OID     *pTableName = NULL;
    UINT2               u2BufferLen = 0;

    pTableName = (struct MIB_OID *) (VOID *) pu1TableName;

    MEMSET (ai1TempBuffer, 0, sizeof (ai1TempBuffer));

    /* see if there is an alpha descriptor at begining */
    if (ISALPHA (*pi1TextStr) != 0)
    {
        pi1DotPtr = (INT1 *) STRCHR ((INT1 *) pi1TextStr, '.');

        /* if no dot, point to end of string */
        if (pi1DotPtr == NULL)
        {
            pi1DotPtr = pi1TextStr + STRLEN ((INT1 *) pi1TextStr);
        }
        pu1TempPtr = (UINT1 *) pi1TextStr;

        for (u2Index = 0;
             ((pu1TempPtr < (UINT1 *) pi1DotPtr) &&
              (u2Index < (LSPP_MAX_NAME_LEN - 1))); u2Index++)
        {
            ai1TempBuffer[u2Index] = *pu1TempPtr++;
        }
        ai1TempBuffer[u2Index] = '\0';
        for (u2Index = 0; pTableName[u2Index].pName != NULL; u2Index++)
        {
            if ((STRCMP (pTableName[u2Index].pName, (INT1 *) ai1TempBuffer)
                 == 0) && (STRLEN ((INT1 *) ai1TempBuffer) ==
                           STRLEN (pTableName[u2Index].pName)))
            {
                STRNCPY ((INT1 *) ai1TempBuffer, pTableName[u2Index].pNumber,
                         (sizeof (ai1TempBuffer) - 1));
                ai1TempBuffer[(sizeof (ai1TempBuffer) - 1)] = '\0';

                break;
            }
        }

        if (pTableName[u2Index].pName == NULL)
        {
            return (NULL);
        }
        /* now concatenate the non-alpha part to the begining */
        u2BufferLen = sizeof (ai1TempBuffer) - STRLEN (ai1TempBuffer);
        STRNCAT ((INT1 *) ai1TempBuffer, (INT1 *) pi1DotPtr,
                 (u2BufferLen <
                  STRLEN (pi1DotPtr) ? u2BufferLen : STRLEN (pi1DotPtr)));

    }
    else
    {                            /* is not alpha, so just copy into ai1TempBuffer */
        STRNCPY ((INT1 *) ai1TempBuffer, (INT1 *) pi1TextStr,
                 (sizeof (ai1TempBuffer) - 1));
        ai1TempBuffer[(sizeof (ai1TempBuffer) - 1)] = '\0';

    }

    /* Now we've got something with numbers instead of an alpha header */

    /* count the dots.  num +1 is the number of SID's */
    for (u2Index = 0; ((u2Index < LSPP_MAX_NAME_LEN) &&
                       (ai1TempBuffer[u2Index] != '\0')); u2Index++)
    {
        if (ai1TempBuffer[u2Index] == '.')
        {
            u2DotCount++;
        }
    }
    pOidPtr = alloc_oid (SNMP_MAX_OID_LENGTH);
    if (pOidPtr == NULL)
    {
        return (NULL);
    }

    pOidPtr->u4_Length = u2DotCount + 1;

    /* now we convert number.number.... strings */
    pu1TempPtr = (UINT1 *) ai1TempBuffer;
    for (u2Index = 0; u2Index < u2DotCount + 1; u2Index++)
    {
        if (LsppTrapParseSubIdNew
            (&pu1TempPtr, &(pOidPtr->pu4_OidList[u2Index])) == OSIX_FAILURE)
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }

        if (*pu1TempPtr == '.')
        {
            pu1TempPtr++;        /* to skip over dot */
        }
        else if (*pu1TempPtr != '\0')
        {
            free_oid (pOidPtr);
            pOidPtr = NULL;
            return (NULL);
        }
    }                            /* end of for loop */

    return (pOidPtr);
}

/******************************************************************************
 * * Function :   LsppTrapParseSubIdNew
 * *
 * * Description : Parse the string format in number.number..format.
 * *
 * * Input       : ppu1TempPtr - pointer to the string.
 * *               pu4Value    - Pointer the OID List value.
 * *
 * * Output      : value of ppu1TempPtr
 * *
 * * Returns     : OSIX_SUCCESS or OSIX_FAILURE
 * ***************************************************************************/
PRIVATE INT4
LsppTrapParseSubIdNew (UINT1 **ppu1TempPtr, UINT4 *pu4Value)
{
    UINT4               u4Value = 0;
    UINT1              *pu1Tmp = NULL;
    INT4                i4RetVal = OSIX_SUCCESS;

    for (pu1Tmp = *ppu1TempPtr; (((*pu1Tmp >= '0') && (*pu1Tmp <= '9')) ||
                                 ((*pu1Tmp >= 'a') && (*pu1Tmp <= 'f')) ||
                                 ((*pu1Tmp >= 'A') && (*pu1Tmp <= 'F')));
         pu1Tmp++)
    {
        u4Value = (u4Value * 10) + (*pu1Tmp & 0xf);
    }

    if (*ppu1TempPtr == pu1Tmp)
    {
        i4RetVal = OSIX_FAILURE;
    }
    *ppu1TempPtr = pu1Tmp;
    *pu4Value = u4Value;
    return (i4RetVal);
}

/*****************************************************************************/
/* Function Name      : LsppTrapFormVarBindList                              */
/*                                                                           */
/* Description        : This function will form the varbind list for the     */
/*                      trap objects.                                        */
/*                                                                           */
/* Input(s)           : au1Buf- Pointer to the Trap obj name                 */
/*                      u4TrapObjVal- Trap object value                      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : VOID                                                 */
/*****************************************************************************/

PUBLIC INT4
LsppTrapFormVarBindList (UINT1 *au1Buf, UINT4 u4TrapObjVal,
                         tSNMP_VAR_BIND ** ppVbList,
                         tSNMP_COUNTER64_TYPE SnmpCnt64Type)
{

    tSNMP_OID_TYPE     *pOid = NULL;

    pOid = LsppTrapMakeObjIdFrmString ((INT1 *) au1Buf,
                                       (UINT1 *) fs_lspp_orig_mib_oid_table);

    if (pOid == NULL)
    {
        return OSIX_FAILURE;
    }
    (*ppVbList)->pNextVarBind = SNMP_AGT_FormVarBind
        (pOid, SNMP_DATA_TYPE_UNSIGNED32, u4TrapObjVal, 0,
         NULL, NULL, SnmpCnt64Type);

    if ((*ppVbList)->pNextVarBind == NULL)
    {
        SNMP_FreeOid (pOid);
        return OSIX_FAILURE;
    }

    *ppVbList = (*ppVbList)->pNextVarBind;
    return OSIX_SUCCESS;
}
