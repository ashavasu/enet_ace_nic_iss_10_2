
/******************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lspptmr.c,v 1.11 2014/12/12 11:56:49 siva Exp $
 *
 * Description : This file contains procedures containing Lspp Timer
 *               related operations
 *****************************************************************************/
#include "lsppinc.h"

PRIVATE tLsppTmrDesc gaLsppTmrDesc[LSPP_MAX_TMRS];

/****************************************************************************
*                                                                           *
* Function     : LsppTmrInitTmrDesc                                         *
*                                                                           *
* Description  : Initialize Lspp Timer Descriptors                          *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
LsppTmrInitTmrDesc ()
{
    /* Wait-To-Send Timer */
    gaLsppTmrDesc[LSPP_WTS_TMR].pTmrExpFunc = LsppTmrWTSTmrExp;
    gaLsppTmrDesc[LSPP_WTS_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tLsppFsLsppPingTraceTableEntry, WTSTimer);

    /* Wait-For-Reply Timer */
    gaLsppTmrDesc[LSPP_WFR_TMR].pTmrExpFunc = LsppTmrWFRTmrExp;
    gaLsppTmrDesc[LSPP_WFR_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tLsppFsLsppEchoSequenceTableEntry, WFRTimer);

    /* Age-Out Timer */
    gaLsppTmrDesc[LSPP_AGE_OUT_TMR].pTmrExpFunc = LsppTmrAgeOutTmrExp;
    gaLsppTmrDesc[LSPP_AGE_OUT_TMR].i2Offset =
        (INT2) FSAP_OFFSETOF (tLsppFsLsppPingTraceTableEntry, AgeOutTimer);
}

/****************************************************************************
*                                                                           *
* Function     : LsppTmrInit                                                *
*                                                                           *
* Description  : Creates and initializes the Lspp Timer List                *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*****************************************************************************/

PUBLIC INT4
LsppTmrInit (VOID)
{
    if (TmrCreateTimerList ((CONST UINT1 *) LSPP_TASK_NAME, LSPP_TIMER_EVENT,
                            NULL, (tTimerListId *) & (gLsppGlobals.lsppTmrLst))
        == TMR_FAILURE)
    {
        LSPP_TRC ((LSPP_MAIN_TRC, "LSPP Timer List Creation Failed\n"));
        return OSIX_FAILURE;
    }

    LsppTmrInitTmrDesc ();

    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : LsppTmrDeInit                                              *
*                                                                           *
* Description  : Deletes the Lspp Timer List                                *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
PUBLIC VOID
LsppTmrDeInit (VOID)
{
    if (gLsppGlobals.lsppTmrLst != NULL)
    {
        TmrDeleteTimerList (gLsppGlobals.lsppTmrLst);
    }

    MEMSET (gaLsppTmrDesc, 0, sizeof (gaLsppTmrDesc));

    return;
}

/****************************************************************************
*                                                                           *
* Function     : LsppTmrStartTmr                                            *
*                                                                           *
* Description  : Starts Lspp Timer                                          *
*                                                                           *
* Input        : pLsppTmr - pointer to tTmrBlk structure                    *
*                u1TmrId  - LSPP timer ID                                   *
*                u4Secs   - seconds                                         *
*                u4MilliSecs - MilliSeconds                                 *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*****************************************************************************/
PUBLIC VOID
LsppTmrStartTmr (tTmrBlk * pLsppTmr, UINT1 u1TmrId, UINT4 u4Secs,
                 UINT4 u4MilliSecs)
{
    if (TmrStart (gLsppGlobals.lsppTmrLst, pLsppTmr, u1TmrId, u4Secs,
                  u4MilliSecs) == TMR_FAILURE)
    {
        LSPP_CMN_TRC (LSPP_INVALID_CONTEXT, LSPP_TMR_START_FAILED, u1TmrId);
    }
    return;
}

/****************************************************************************
*                                                                           *
* Function     : LsppTmrRestartTmr                                          *
*                                                                           *
* Description  :  ReStarts Lspp Timer                                       *
*                                                                           *
* Input        : pLsppTmr - pointer to tTmrBlk structure                    *
*                u1TmrId  - LSPP timer ID                                   *
*                u4Secs   - seconds                                         *
*                u4MilliSecs - MilliSeconds                                 *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
PUBLIC VOID
LsppTmrRestartTmr (tTmrBlk * pLsppTmr, UINT1 u1TimerId, UINT4 u4Secs,
                   UINT4 u4MilliSecs)
{
    TmrStop (gLsppGlobals.lsppTmrLst, pLsppTmr);
    LsppTmrStartTmr (pLsppTmr, u1TimerId, u4Secs, u4MilliSecs);

    return;
}

/****************************************************************************
*                                                                           *
* Function     : LsppTmrStopTmr                                             *
*                                                                           *
* Description  : Restarts Lspp Timer                                        *
*                                                                           *
* Input        : pLsppTmr - pointer to tTmrBlk structure                    *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
PUBLIC VOID
LsppTmrStopTmr (tTmrBlk * pLsppTmr)
{
    TmrStop (gLsppGlobals.lsppTmrLst, pLsppTmr);
    return;
}

/****************************************************************************
*                                                                           *
* Function     : LsppTmrHandleExpiry                                        *
*                                                                           *
* Description  : This procedure is invoked when the event indicating a time *
*                expiry occurs. This procedure finds the expired timers and *
*                invokes the corresponding timer routines.                  *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*                                                                           *
*****************************************************************************/
PUBLIC VOID
LsppTmrHandleExpiry (VOID)
{
    tTmrAppTimer       *pExpiredTimers = NULL;
    UINT1               u1TmrId = 0;
    INT2                i2Offset = 0;

    while ((pExpiredTimers = TmrGetNextExpiredTimer (gLsppGlobals.lsppTmrLst))
           != NULL)
    {
        u1TmrId = ((tTmrBlk *) pExpiredTimers)->u1TimerId;
        i2Offset = gaLsppTmrDesc[u1TmrId].i2Offset;

        if (i2Offset < 0)
        {
            /* The timer function does not take any parameter */
            (*(gaLsppTmrDesc[u1TmrId].pTmrExpFunc)) (NULL);
        }
        else
        {
            /* The timer function requires a parameter */
            (*(gaLsppTmrDesc[u1TmrId].pTmrExpFunc))
                ((UINT1 *) pExpiredTimers - i2Offset);
        }
    }
    return;
}

/****************************************************************************
*                                                                           *
* Function     : LsppTmrWTSTmrExp                                           *
*                                                                           *
* Description  : This function will be invoked in the case of WTS timer     *
*                expiry. It will performs the operations needed on the      *
*                expiry of the WTS timer                                    *
*                                                                           *
* Input        : pointer to the PingTrace table                             *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
VOID
LsppTmrWTSTmrExp (VOID *pArg)
{
    tLsppFsLsppPingTraceTableEntry *pPingTraceEntry =
        (tLsppFsLsppPingTraceTableEntry *) pArg;
    UINT4               u4MaxReqsToSend = 0;

    LSPP_LOG (pPingTraceEntry->MibObject.u4FsLsppContextId,
              LSPP_WTS_TIMER_EXPIRED, "LsppTmrWTSTmrExp",
              pPingTraceEntry->MibObject.u4FsLsppContextId,
              pPingTraceEntry->MibObject.u4FsLsppSenderHandle);

    /* Get the maximun number of echo requests to send for this 
     * ping trace entry.
     */
    LsppUtilGetMaxReqsToSend (pPingTraceEntry, &u4MaxReqsToSend);

    if ((u4MaxReqsToSend > 0) &&
        ((pPingTraceEntry->MibObject.u4FsLsppPktsTx +
          pPingTraceEntry->MibObject.u4FsLsppPktsUnSent) < u4MaxReqsToSend))
    {
        /* Send the next Echo request and update the Ping Trace entry */
        if (LsppCoreCreateAndTriggerEcho (pPingTraceEntry, 0) != OSIX_SUCCESS)
        {
            LSPP_LOG (pPingTraceEntry->MibObject.u4FsLsppContextId,
                      LSPP_UNABLE_TO_SEND_ECHO_REQ, "LsppTmrWTSTmrExp");
        }

    }

    return;
}

/****************************************************************************
*                                                                           *
* Function     : LsppTmrWFRTmrExp                                           *
*                                                                           *
* Description  : This function will be invoked in the case of WFR timer     *
*                expiry. It will performs the operations needed on the      *
*                expiry of the WFR timer                                    *
*                                                                           *
* Input        : pointer to the PingTrace table                             *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
VOID
LsppTmrWFRTmrExp (VOID *pArg)
{
    tLsppFsLsppEchoSequenceTableEntry *pEchoSeqEntry = NULL;
    pEchoSeqEntry = (tLsppFsLsppEchoSequenceTableEntry *) pArg;

    LSPP_LOG (pEchoSeqEntry->MibObject.u4FsLsppContextId,
              LSPP_REQ_TIMED_OUT_WFR_TIMER, "LsppTmrWFRTmrExp",
              pEchoSeqEntry->MibObject.u4FsLsppContextId,
              pEchoSeqEntry->MibObject.u4FsLsppSenderHandle,
              pEchoSeqEntry->MibObject.u4FsLsppSequenceNumber);

    /* Set the Status of the Echo sequence to TIMED_OUT */
    if (pEchoSeqEntry->u1Status == LSPP_ECHO_SEQ_IN_PROGRESS)
    {
        pEchoSeqEntry->u1Status = LSPP_ECHO_SEQ_TIMEDOUT;
    }

    /* Update the Ping Trace table and initiate the request when the entry 
     * belongs to traceroute.*/
    if (LsppCoreHandleReqTimedOut (pEchoSeqEntry) != OSIX_SUCCESS)
    {
        LSPP_LOG (pEchoSeqEntry->MibObject.u4FsLsppContextId,
                  LSPP_REQ_TIMED_OUT_PROCESS_FAILED, "LsppTmrWFRTmrExp");
    }

    return;
}

/****************************************************************************
*                                                                           *
* Function     : LsppTmrAgeOutTmrExp                                        *
*                                                                           *
* Description  : This function will be invoked in the case of AgeOut timer  *
*                expiry. The corresponding ping trace entry and the         *
*                sequences will be deleted.                                 *
*                                                                           *
* Input        : pointer to the PingTrace entry                             *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
VOID
LsppTmrAgeOutTmrExp (VOID *pArg)
{
    INT4                i4RetVal = OSIX_FAILURE;

    tLsppFsLsppPingTraceTableEntry *pPingTraceEntry =
        (tLsppFsLsppPingTraceTableEntry *) pArg;

    LSPP_LOG (pPingTraceEntry->MibObject.u4FsLsppContextId,
              LSPP_AGE_OUT_TIMER_EXPIRED,
              "LsppTmrAgeOutTmrExp",
              pPingTraceEntry->MibObject.u4FsLsppContextId,
              pPingTraceEntry->MibObject.u4FsLsppSenderHandle);

    /* Delete the Echo Sequence and Hop Info table that belongs 
     * to this ping trace entry.
     */
    i4RetVal = LsppCxtDeletePingTraceInfo (pPingTraceEntry);
    if (i4RetVal == OSIX_FAILURE)
    {
        LSPP_LOG (pPingTraceEntry->MibObject.u4FsLsppContextId,
                  LSPP_UNABLE_TO_DELETE_PING_TRACE_INFO,
                  "LsppTmrAgeOutTmrExp", pPingTraceEntry->MibObject.
                  u4FsLsppSenderHandle);
        return;
    }

    /* Delete the Ping Trace Node from the database. */
    LsppCxtDeletePingTraceNode (pPingTraceEntry);

    return;
}

/****************************************************************************
*                                                                           *
* Function     : LsppTmrRestartAgeOutTimer                                  *
*                                                                           *
* Description  : This function will be invoked if a new age-out time is     *
*                configured in the Global Config Table.                     *
*                This function will restart the age-out timer for the newly *
*                configured time.                                           *
*                                                                           *
* Input        : Global Config table - with new age-out time                *
*                Global Config table - with old age-out time                *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
INT4
LsppTmrRestartAgeOutTimer (tLsppFsLsppGlobalConfigTableEntry *
                           pLsppGlobalConfigTableEntry,
                           tLsppFsLsppGlobalConfigTableEntry *
                           pLsppOldGlobalConfigTableEntry)
{
    tLsppFsLsppPingTraceTableEntry *pLsppPingTraceTableEntryInput = NULL;
    tLsppFsLsppPingTraceTableEntry *pLsppPingTraceTableEntry = NULL;
    UINT4               u4RemainingTime = 0;
    UINT4               u4OldAgeOutTime = 0;
    UINT4               u4NewAgeOutTime = 0;
    INT4                i4ReStartTime = 0;

    pLsppPingTraceTableEntryInput = (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppPingTraceTableEntryInput == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pLsppPingTraceTableEntryInput, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the Context Id to the input structure to get the first Ping trace
     * entry in that context
     * */
    pLsppPingTraceTableEntryInput->MibObject.u4FsLsppContextId =
        pLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId;

    pLsppPingTraceTableEntryInput->MibObject.u4FsLsppSenderHandle = 0;

    while ((pLsppPingTraceTableEntry = LsppGetNextFsLsppPingTraceTable
            (pLsppPingTraceTableEntryInput)) != NULL)
    {
        if (pLsppPingTraceTableEntry->MibObject.u4FsLsppContextId !=
            pLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId)
        {
            /* No more ping trace entry in the given context */
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTableEntryInput);
            return OSIX_SUCCESS;
        }

        /* If the Status of the Ping trace entry is not IN_PROGRESS (COMPLETED)
         * then, Get the remaining age-out time
         * */
        if (pLsppPingTraceTableEntry->MibObject.i4FsLsppStatus !=
            LSPP_ECHO_IN_PROGRESS)
        {
            if (TmrGetRemainingTime (gLsppGlobals.lsppTmrLst,
                                     (tTmrAppTimer *)
                                     & (pLsppPingTraceTableEntry->AgeOutTimer),
                                     &u4RemainingTime) != TMR_SUCCESS)
            {
                LSPP_CMN_TRC (pLsppPingTraceTableEntry->MibObject.
                              u4FsLsppContextId,
                              LSPP_GET_REMAINING_TIME_FAILED,
                              "LsppTmrRestartAgeOutTimer");
                MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                    (UINT1 *) pLsppPingTraceTableEntryInput);
                return OSIX_FAILURE;
            }

            /* Converting the remaining time obtained in number of Ticks to
             * Milliseconds
             * */
            u4RemainingTime = (u4RemainingTime / NO_OF_TICKS_PER_SEC) *
                LSPP_NO_OF_MILLISECS_IN_SEC;

            /* Convert the Old Age out time into MilliSeconds */

            if (pLsppOldGlobalConfigTableEntry->MibObject.
                i4FsLsppAgeOutTmrUnit == LSPP_TMR_UNIT_MILLISEC)
            {
                u4OldAgeOutTime =
                    pLsppOldGlobalConfigTableEntry->MibObject.
                    u4FsLsppAgeOutTime;
            }
            else if (pLsppOldGlobalConfigTableEntry->MibObject.
                     i4FsLsppAgeOutTmrUnit == LSPP_TMR_UNIT_SEC)
            {
                u4OldAgeOutTime =
                    pLsppOldGlobalConfigTableEntry->MibObject.
                    u4FsLsppAgeOutTime * LSPP_NO_OF_MILLISECS_IN_SEC;
            }
            else if (pLsppOldGlobalConfigTableEntry->MibObject.
                     i4FsLsppAgeOutTmrUnit == LSPP_TMR_UNIT_MIN)
            {
                u4OldAgeOutTime =
                    pLsppOldGlobalConfigTableEntry->MibObject.
                    u4FsLsppAgeOutTime * LSPP_NO_OF_MILLISECS_IN_MIN;
            }

            /* Convert the New Age out time into MilliSeconds */

            if (pLsppGlobalConfigTableEntry->MibObject.i4FsLsppAgeOutTmrUnit ==
                LSPP_TMR_UNIT_MILLISEC)
            {
                u4NewAgeOutTime = pLsppGlobalConfigTableEntry->MibObject.
                    u4FsLsppAgeOutTime;
            }
            else if (pLsppGlobalConfigTableEntry->MibObject.
                     i4FsLsppAgeOutTmrUnit == LSPP_TMR_UNIT_SEC)
            {
                u4NewAgeOutTime =
                    pLsppGlobalConfigTableEntry->MibObject.
                    u4FsLsppAgeOutTime * LSPP_NO_OF_MILLISECS_IN_SEC;
            }
            else if (pLsppGlobalConfigTableEntry->MibObject.
                     i4FsLsppAgeOutTmrUnit == LSPP_TMR_UNIT_MIN)
            {
                u4NewAgeOutTime = pLsppGlobalConfigTableEntry->MibObject.
                    u4FsLsppAgeOutTime * LSPP_NO_OF_MILLISECS_IN_MIN;
            }

            /* Time for which the age out timer has to be restarted */
            i4ReStartTime =(INT4)( u4NewAgeOutTime -
                (u4OldAgeOutTime - u4RemainingTime));

            if (i4ReStartTime <= 0)
            {
                /* If the timer already expired for the new age out time
                 * then, then delete the entry
                 * */
                if (LsppCxtDeletePingTraceInfo (pLsppPingTraceTableEntry) !=
                    OSIX_SUCCESS)
                {
                    LSPP_LOG (pLsppPingTraceTableEntry->MibObject.
                              u4FsLsppContextId,
                              LSPP_UNABLE_TO_DELETE_PING_TRACE_INFO,
                              "LsppTmrRestartAgeOutTimer",
                              pLsppPingTraceTableEntry->MibObject.
                              u4FsLsppSenderHandle);
                    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                        (UINT1 *)
                                        pLsppPingTraceTableEntryInput);

                    return OSIX_FAILURE;
                }

                if (LsppCxtDeletePingTraceNode (pLsppPingTraceTableEntry) !=
                    OSIX_SUCCESS)
                {
                    LSPP_LOG (pLsppPingTraceTableEntry->MibObject.
                              u4FsLsppContextId,
                              LSPP_PING_TRACE_NODE_DELETE_FAIL,
                              "LsppTmrRestartAgeOutTimer");
                    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                        (UINT1 *)
                                        pLsppPingTraceTableEntryInput);

                    return OSIX_FAILURE;
                }
            }
            else
            {
                /* Restart the Age Out timer */
                LsppTmrRestartTmr (&(pLsppPingTraceTableEntry->AgeOutTimer),
                                   LSPP_AGE_OUT_TMR, 0, (UINT4) i4ReStartTime);
            }
        }

        pLsppPingTraceTableEntryInput->MibObject.u4FsLsppContextId =
            pLsppPingTraceTableEntry->MibObject.u4FsLsppContextId;

        pLsppPingTraceTableEntryInput->MibObject.u4FsLsppSenderHandle =
            pLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppPingTraceTableEntryInput);
    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  lspptmr.c                      */
/*-----------------------------------------------------------------------*/
