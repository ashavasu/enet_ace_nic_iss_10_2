/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: lsppdsg.c,v 1.2 2010/11/23 04:53:22 siva Exp $
*
* Description: This file contains the routines for DataStructure access for the module Lspp 
*********************************************************************/

#include "lsppinc.h"

/****************************************************************************
 Function    :  LsppGetFirstFsLsppGlobalConfigTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pLsppFsLsppGlobalConfigTable or NULL
****************************************************************************/
tLsppFsLsppGlobalConfigTableEntry *
LsppGetFirstFsLsppGlobalConfigTable ()
{
    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigTableEntry = NULL;

    pLsppFsLsppGlobalConfigTableEntry =
        (tLsppFsLsppGlobalConfigTableEntry *) RBTreeGetFirst (gLsppGlobals.
                                                              LsppGlbMib.
                                                              FsLsppGlobalConfigTable);

    return pLsppFsLsppGlobalConfigTableEntry;
}

/****************************************************************************
 Function    :  LsppGetNextFsLsppGlobalConfigTable
 Input       :  pCurrentLsppFsLsppGlobalConfigTable
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextLsppFsLsppGlobalConfigTable or NULL
****************************************************************************/
tLsppFsLsppGlobalConfigTableEntry *
LsppGetNextFsLsppGlobalConfigTable (tLsppFsLsppGlobalConfigTableEntry *
                                    pCurrentLsppFsLsppGlobalConfigTableEntry)
{
    tLsppFsLsppGlobalConfigTableEntry *pNextLsppFsLsppGlobalConfigTableEntry =
        NULL;

    pNextLsppFsLsppGlobalConfigTableEntry =
        (tLsppFsLsppGlobalConfigTableEntry *) RBTreeGetNext (gLsppGlobals.
                                                             LsppGlbMib.
                                                             FsLsppGlobalConfigTable,
                                                             (tRBElem *)
                                                             pCurrentLsppFsLsppGlobalConfigTableEntry,
                                                             NULL);

    return pNextLsppFsLsppGlobalConfigTableEntry;
}

/****************************************************************************
 Function    :  LsppGetFsLsppGlobalConfigTable
 Input       :  pLsppFsLsppGlobalConfigTable
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetLsppFsLsppGlobalConfigTable or NULL
****************************************************************************/
tLsppFsLsppGlobalConfigTableEntry *
LsppGetFsLsppGlobalConfigTable (tLsppFsLsppGlobalConfigTableEntry *
                                pLsppFsLsppGlobalConfigTableEntry)
{
    tLsppFsLsppGlobalConfigTableEntry *pGetLsppFsLsppGlobalConfigTableEntry =
        NULL;

    pGetLsppFsLsppGlobalConfigTableEntry =
        (tLsppFsLsppGlobalConfigTableEntry *) RBTreeGet (gLsppGlobals.
                                                         LsppGlbMib.
                                                         FsLsppGlobalConfigTable,
                                                         (tRBElem *)
                                                         pLsppFsLsppGlobalConfigTableEntry);

    return pGetLsppFsLsppGlobalConfigTableEntry;
}

/****************************************************************************
 Function    :  LsppGetFirstFsLsppGlobalStatsTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pLsppFsLsppGlobalStatsTable or NULL
****************************************************************************/
tLsppFsLsppGlobalStatsTableEntry *
LsppGetFirstFsLsppGlobalStatsTable ()
{
    tLsppFsLsppGlobalStatsTableEntry *pLsppFsLsppGlobalStatsTableEntry = NULL;

    pLsppFsLsppGlobalStatsTableEntry =
        (tLsppFsLsppGlobalStatsTableEntry *) RBTreeGetFirst (gLsppGlobals.
                                                             LsppGlbMib.
                                                             FsLsppGlobalStatsTable);

    return pLsppFsLsppGlobalStatsTableEntry;
}

/****************************************************************************
 Function    :  LsppGetNextFsLsppGlobalStatsTable
 Input       :  pCurrentLsppFsLsppGlobalStatsTable
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextLsppFsLsppGlobalStatsTable or NULL
****************************************************************************/
tLsppFsLsppGlobalStatsTableEntry *
LsppGetNextFsLsppGlobalStatsTable (tLsppFsLsppGlobalStatsTableEntry *
                                   pCurrentLsppFsLsppGlobalStatsTableEntry)
{
    tLsppFsLsppGlobalStatsTableEntry *pNextLsppFsLsppGlobalStatsTableEntry =
        NULL;

    pNextLsppFsLsppGlobalStatsTableEntry =
        (tLsppFsLsppGlobalStatsTableEntry *) RBTreeGetNext (gLsppGlobals.
                                                            LsppGlbMib.
                                                            FsLsppGlobalStatsTable,
                                                            (tRBElem *)
                                                            pCurrentLsppFsLsppGlobalStatsTableEntry,
                                                            NULL);

    return pNextLsppFsLsppGlobalStatsTableEntry;
}

/****************************************************************************
 Function    :  LsppGetFsLsppGlobalStatsTable
 Input       :  pLsppFsLsppGlobalStatsTable
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetLsppFsLsppGlobalStatsTable or NULL
****************************************************************************/
tLsppFsLsppGlobalStatsTableEntry *
LsppGetFsLsppGlobalStatsTable (tLsppFsLsppGlobalStatsTableEntry *
                               pLsppFsLsppGlobalStatsTableEntry)
{
    tLsppFsLsppGlobalStatsTableEntry *pGetLsppFsLsppGlobalStatsTableEntry =
        NULL;

    pGetLsppFsLsppGlobalStatsTableEntry =
        (tLsppFsLsppGlobalStatsTableEntry *) RBTreeGet (gLsppGlobals.LsppGlbMib.
                                                        FsLsppGlobalStatsTable,
                                                        (tRBElem *)
                                                        pLsppFsLsppGlobalStatsTableEntry);

    return pGetLsppFsLsppGlobalStatsTableEntry;
}

/****************************************************************************
 Function    :  LsppGetFirstFsLsppPingTraceTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pLsppFsLsppPingTraceTable or NULL
****************************************************************************/
tLsppFsLsppPingTraceTableEntry *
LsppGetFirstFsLsppPingTraceTable ()
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *) RBTreeGetFirst (gLsppGlobals.
                                                           LsppGlbMib.
                                                           FsLsppPingTraceTable);

    return pLsppFsLsppPingTraceTableEntry;
}

/****************************************************************************
 Function    :  LsppGetNextFsLsppPingTraceTable
 Input       :  pCurrentLsppFsLsppPingTraceTable
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextLsppFsLsppPingTraceTable or NULL
****************************************************************************/
tLsppFsLsppPingTraceTableEntry *
LsppGetNextFsLsppPingTraceTable (tLsppFsLsppPingTraceTableEntry *
                                 pCurrentLsppFsLsppPingTraceTableEntry)
{
    tLsppFsLsppPingTraceTableEntry *pNextLsppFsLsppPingTraceTableEntry = NULL;

    pNextLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *) RBTreeGetNext (gLsppGlobals.
                                                          LsppGlbMib.
                                                          FsLsppPingTraceTable,
                                                          (tRBElem *)
                                                          pCurrentLsppFsLsppPingTraceTableEntry,
                                                          NULL);

    return pNextLsppFsLsppPingTraceTableEntry;
}

/****************************************************************************
 Function    :  LsppGetFsLsppPingTraceTable
 Input       :  pLsppFsLsppPingTraceTable
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetLsppFsLsppPingTraceTable or NULL
****************************************************************************/
tLsppFsLsppPingTraceTableEntry *
LsppGetFsLsppPingTraceTable (tLsppFsLsppPingTraceTableEntry *
                             pLsppFsLsppPingTraceTableEntry)
{
    tLsppFsLsppPingTraceTableEntry *pGetLsppFsLsppPingTraceTableEntry = NULL;

    pGetLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *) RBTreeGet (gLsppGlobals.LsppGlbMib.
                                                      FsLsppPingTraceTable,
                                                      (tRBElem *)
                                                      pLsppFsLsppPingTraceTableEntry);

    return pGetLsppFsLsppPingTraceTableEntry;
}

/****************************************************************************
 Function    :  LsppGetFirstFsLsppEchoSequenceTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pLsppFsLsppEchoSequenceTable or NULL
****************************************************************************/
tLsppFsLsppEchoSequenceTableEntry *
LsppGetFirstFsLsppEchoSequenceTable ()
{
    tLsppFsLsppEchoSequenceTableEntry *pLsppFsLsppEchoSequenceTableEntry = NULL;

    pLsppFsLsppEchoSequenceTableEntry =
        (tLsppFsLsppEchoSequenceTableEntry *) RBTreeGetFirst (gLsppGlobals.
                                                              LsppGlbMib.
                                                              FsLsppEchoSequenceTable);

    return pLsppFsLsppEchoSequenceTableEntry;
}

/****************************************************************************
 Function    :  LsppGetNextFsLsppEchoSequenceTable
 Input       :  pCurrentLsppFsLsppEchoSequenceTable
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextLsppFsLsppEchoSequenceTable or NULL
****************************************************************************/
tLsppFsLsppEchoSequenceTableEntry *
LsppGetNextFsLsppEchoSequenceTable (tLsppFsLsppEchoSequenceTableEntry *
                                    pCurrentLsppFsLsppEchoSequenceTableEntry)
{
    tLsppFsLsppEchoSequenceTableEntry *pNextLsppFsLsppEchoSequenceTableEntry =
        NULL;

    pNextLsppFsLsppEchoSequenceTableEntry =
        (tLsppFsLsppEchoSequenceTableEntry *) RBTreeGetNext (gLsppGlobals.
                                                             LsppGlbMib.
                                                             FsLsppEchoSequenceTable,
                                                             (tRBElem *)
                                                             pCurrentLsppFsLsppEchoSequenceTableEntry,
                                                             NULL);

    return pNextLsppFsLsppEchoSequenceTableEntry;
}

/****************************************************************************
 Function    :  LsppGetFsLsppEchoSequenceTable
 Input       :  pLsppFsLsppEchoSequenceTable
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetLsppFsLsppEchoSequenceTable or NULL
****************************************************************************/
tLsppFsLsppEchoSequenceTableEntry *
LsppGetFsLsppEchoSequenceTable (tLsppFsLsppEchoSequenceTableEntry *
                                pLsppFsLsppEchoSequenceTableEntry)
{
    tLsppFsLsppEchoSequenceTableEntry *pGetLsppFsLsppEchoSequenceTableEntry =
        NULL;

    pGetLsppFsLsppEchoSequenceTableEntry =
        (tLsppFsLsppEchoSequenceTableEntry *) RBTreeGet (gLsppGlobals.
                                                         LsppGlbMib.
                                                         FsLsppEchoSequenceTable,
                                                         (tRBElem *)
                                                         pLsppFsLsppEchoSequenceTableEntry);

    return pGetLsppFsLsppEchoSequenceTableEntry;
}

/****************************************************************************
 Function    :  LsppGetFirstFsLsppHopTable
 Input       :  NONE
 Description :  This Routine is used to take
                first index from a table
 Returns     :  pLsppFsLsppHopTable or NULL
****************************************************************************/
tLsppFsLsppHopTableEntry *
LsppGetFirstFsLsppHopTable ()
{
    tLsppFsLsppHopTableEntry *pLsppFsLsppHopTableEntry = NULL;

    pLsppFsLsppHopTableEntry =
        (tLsppFsLsppHopTableEntry *) RBTreeGetFirst (gLsppGlobals.LsppGlbMib.
                                                     FsLsppHopTable);

    return pLsppFsLsppHopTableEntry;
}

/****************************************************************************
 Function    :  LsppGetNextFsLsppHopTable
 Input       :  pCurrentLsppFsLsppHopTable
 Description :  This Routine is used to take
                next index from a table
 Returns     :  pNextLsppFsLsppHopTable or NULL
****************************************************************************/
tLsppFsLsppHopTableEntry *
LsppGetNextFsLsppHopTable (tLsppFsLsppHopTableEntry *
                           pCurrentLsppFsLsppHopTableEntry)
{
    tLsppFsLsppHopTableEntry *pNextLsppFsLsppHopTableEntry = NULL;

    pNextLsppFsLsppHopTableEntry =
        (tLsppFsLsppHopTableEntry *) RBTreeGetNext (gLsppGlobals.LsppGlbMib.
                                                    FsLsppHopTable,
                                                    (tRBElem *)
                                                    pCurrentLsppFsLsppHopTableEntry,
                                                    NULL);

    return pNextLsppFsLsppHopTableEntry;
}

/****************************************************************************
 Function    :  LsppGetFsLsppHopTable
 Input       :  pLsppFsLsppHopTable
 Description :  This Routine is used to take
                Row for the index from a table
 Returns     :  pGetLsppFsLsppHopTable or NULL
****************************************************************************/
tLsppFsLsppHopTableEntry *
LsppGetFsLsppHopTable (tLsppFsLsppHopTableEntry * pLsppFsLsppHopTableEntry)
{
    tLsppFsLsppHopTableEntry *pGetLsppFsLsppHopTableEntry = NULL;

    pGetLsppFsLsppHopTableEntry =
        (tLsppFsLsppHopTableEntry *) RBTreeGet (gLsppGlobals.LsppGlbMib.
                                                FsLsppHopTable,
                                                (tRBElem *)
                                                pLsppFsLsppHopTableEntry);

    return pGetLsppFsLsppHopTableEntry;
}

/****************************************************************************
 Function    :  LsppGetFsLsppTrapContextName
 Input       :  The Indices
 Input       :  pFsLsppTrapContextName
 Descritpion :  This Routine will Get 
                the Value accordingly.
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
LsppGetFsLsppTrapContextName (UINT1 *pFsLsppTrapContextName)
{
    MEMCPY (pFsLsppTrapContextName,
            gLsppGlobals.LsppGlbMib.au1FsLsppTrapContextName,
            gLsppGlobals.LsppGlbMib.i4FsLsppTrapContextNameLen);

    return OSIX_SUCCESS;
}
