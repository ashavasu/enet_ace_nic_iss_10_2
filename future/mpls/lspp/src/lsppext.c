
/******************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lsppext.c,v 1.15 2012/02/22 12:31:47 siva Exp $
 *
 * Description : This file is for processing the external requests coming 
 *               from other modules.
 *      
 *****************************************************************************/
#include "lsppinc.h"

/****************************************************************************
*                                                                           *
* Function     : LsppExtTriggerPing                                         *
*                                                                           *
* Description  : This function creates an entry in ping trace table, fills  *
*                the mandatory fields required and triggers the ping request*
*                on receiving a request from external modules.              *
*                                                                           *
* Input        : u4ContextId -                                              *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*****************************************************************************/
INT4
LsppExtTriggerPing (UINT4 u4ContextId, tLsppExtTrigInfo * pLsppExtTrigInfo)
{
    tLsppFsLsppGlobalConfigTableEntry LsppGlobalConfigEntry;
    tLsppPathId         LsppPathId;
    tLsppFsLsppPingTraceTableEntry *pLsppPingTraceEntryInput = NULL;
    tLsppFsLsppPingTraceTableEntry *pLsppPingTraceEntry = NULL;
    UINT4               au4PathPointer[LSPP_MAX_OID_LEN];
    UINT4               u4SendersHandle = 0;
    INT4                i4OidLength = 0;
    UINT4               u4LsppEncap = 0;

    pLsppPingTraceEntryInput = (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppPingTraceEntryInput == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (&LsppPathId, 0, sizeof (tLsppPathId));
    MEMSET (pLsppPingTraceEntryInput, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&LsppGlobalConfigEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (au4PathPointer, 0, sizeof (au4PathPointer));

    LSPP_LOG (u4ContextId, LSPP_EXT_TRIG_RCVD_FROM_BFD,
              "LsppExtTriggerPing", u4ContextId,
              pLsppExtTrigInfo->BfdBtStrapInfo.u4BfdSessionIndex,
              pLsppExtTrigInfo->BfdBtStrapInfo.u4BfdDiscriminator);

    /* Check whether the Context specified by the external module is 
     * available.
     */
    LsppGlobalConfigEntry.MibObject.u4FsLsppContextId = u4ContextId;

    if (LsppGetAllFsLsppGlobalConfigTable (&LsppGlobalConfigEntry) !=
        OSIX_SUCCESS)
    {
        LSPP_LOG (u4ContextId, LSPP_EXT_TRIG_CXT_NOT_EXIST,
                  "LsppExtTriggerPing");
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppPingTraceEntryInput);
        return OSIX_FAILURE;
    }

    if (LsppGlobalConfigEntry.MibObject.i4FsLsppSystemControl == LSPP_SHUTDOWN)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppPingTraceEntryInput);
        return OSIX_FAILURE;
    }

    /* Get a Senders Handle for this entry from the Index Manager */
    if (IndexManagerSetNextFreeIndex (LsppGlobalConfigEntry.IndexMgrId,
                                      &u4SendersHandle) != INDEX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppPingTraceEntryInput);
        return OSIX_FAILURE;
    }

    /* Initialize the Ping trace table to default values */
    if ((LsppInitializeFsLsppPingTraceTable (pLsppPingTraceEntryInput))
        == OSIX_FAILURE)
    {
        LSPP_LOG (u4ContextId, LSPP_PINGTRACE_TABLE_INIT_FAILED,
                  "LsppExtTriggerPing");
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppPingTraceEntryInput);
        return OSIX_FAILURE;
    }

    pLsppPingTraceEntryInput->MibObject.u4FsLsppContextId = u4ContextId;
    pLsppPingTraceEntryInput->MibObject.u4FsLsppSenderHandle = u4SendersHandle;

    /* Path type for which the ping request has to be sent */
    LsppPathId.u1PathType = pLsppExtTrigInfo->LsppPathId.u1PathType;

    /* Set the Owner of the request as BFD */
    pLsppPingTraceEntryInput->MibObject.i4FsLsppRequestOwner =
        LSPP_REQ_OWNER_BFD;
    pLsppPingTraceEntryInput->MibObject.i4FsLsppRequestType =
        LSPP_REQ_TYPE_PING;
    pLsppPingTraceEntryInput->MibObject.i4FsLsppPathType =
        LsppPathId.u1PathType;

    /* Store the BFD session index in the ping trace entry */
    pLsppPingTraceEntryInput->u4BfdSessionIndex =
        pLsppExtTrigInfo->BfdBtStrapInfo.u4BfdSessionIndex;

    /* Setting WFR Interval */
    pLsppPingTraceEntryInput->MibObject.u4FsLsppWFRInterval =
        (pLsppExtTrigInfo->BfdBtStrapInfo.u4WFRInterval /
         LSPP_NO_OF_MICROSECS_IN_MILLSEC);
    pLsppPingTraceEntryInput->MibObject.i4FsLsppWFRTmrUnit =
        (INT4) LSPP_TMR_UNIT_MILLISEC;

    /* Setting Encapsulation to use */
    if (pLsppExtTrigInfo->BfdBtStrapInfo.u4LsppEncapType >= LSPP_ENCAP_TYPE_MAX)
    {
        /* Wrong Encap Type by the external module */
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppPingTraceEntryInput);
        return OSIX_FAILURE;
    }
    else if (pLsppExtTrigInfo->BfdBtStrapInfo.u4LsppEncapType ==
             LSPP_ENCAP_TYPE_DEFAULT)
    {
        /* External module has not specified encap type, use default */
        u4LsppEncap = LSPP_ENCAP_TYPE_DEFAULT;
    }
    else
    {
        /* Use the Encap given by the external module */
        u4LsppEncap = pLsppExtTrigInfo->BfdBtStrapInfo.u4LsppEncapType;
    }

    pLsppPingTraceEntryInput->MibObject.u4FsLsppRepeatCount = 1;
    pLsppPingTraceEntryInput->MibObject.u4FsLsppTTLValue = LSPP_MAX_TTL;
    pLsppPingTraceEntryInput->MibObject.i4FsLsppStatus = LSPP_ECHO_IN_PROGRESS;

    /* copy the PathInfo received in the Queue message to the PingTrace table 
     * for protocol access
     * */
    MEMCPY (&(pLsppPingTraceEntryInput->PathId),
            &(pLsppExtTrigInfo->LsppPathId), sizeof (tLsppPathId));

    /* Depends on path type set the reply mode and encapsulation type in the 
     * ping trace table
     * */
    switch (LsppPathId.u1PathType)
    {
        case LSPP_PATH_TYPE_LDP_IPV4:
        case LSPP_PATH_TYPE_LDP_IPV6:

            if (u4LsppEncap == LSPP_ENCAP_TYPE_DEFAULT)
            {
                pLsppPingTraceEntryInput->MibObject.i4FsLsppEncapType =
                    LSPP_ENCAP_TYPE_IP;
            }
            else
            {
                pLsppPingTraceEntryInput->MibObject.i4FsLsppEncapType =
                    u4LsppEncap;
            }

            /* copy the LdpInfo from the pingTrace table to the PathInfo 
             * structure 
             * */
            MEMCPY (&(LsppPathId.unPathId.LdpInfo),
                    &(pLsppExtTrigInfo->LsppPathId.unPathId.LdpInfo),
                    sizeof (tLsppLdpInfo));
            break;

        case LSPP_PATH_TYPE_RSVP_IPV4:
        case LSPP_PATH_TYPE_RSVP_IPV6:

            if (u4LsppEncap == LSPP_ENCAP_TYPE_DEFAULT)
            {
                pLsppPingTraceEntryInput->MibObject.i4FsLsppEncapType =
                    LSPP_ENCAP_TYPE_IP;
            }
            else
            {
                pLsppPingTraceEntryInput->MibObject.i4FsLsppEncapType =
                    u4LsppEncap;
            }
            /* copy the Tunnnel Info from the pingTrace table to the PathInfo 
             * structure
             * */
            MEMCPY (&(LsppPathId.unPathId.TnlInfo),
                    &(pLsppExtTrigInfo->LsppPathId.unPathId.TnlInfo),
                    sizeof (tLsppTnlInfo));
            break;

        case LSPP_PATH_TYPE_FEC_128:
        case LSPP_PATH_TYPE_FEC_129:

            if (u4LsppEncap == LSPP_ENCAP_TYPE_DEFAULT)
            {
                pLsppPingTraceEntryInput->MibObject.i4FsLsppEncapType =
                    LSPP_ENCAP_TYPE_VCCV_NEGOTIATED;
            }
            else
            {
                pLsppPingTraceEntryInput->MibObject.i4FsLsppEncapType =
                    u4LsppEncap;
            }

            /* copy the PseudoWire Info from the pingTrace table to the 
             * PathInfo structure 
             * */
            MEMCPY (&(LsppPathId.unPathId.PwInfo),
                    &(pLsppExtTrigInfo->LsppPathId.unPathId.PwInfo),
                    sizeof (tLsppPwInfo));
            break;

        case LSPP_PATH_TYPE_MEP:

            if (u4LsppEncap == LSPP_ENCAP_TYPE_DEFAULT)
            {
                pLsppPingTraceEntryInput->MibObject.i4FsLsppEncapType =
                    LSPP_ENCAP_TYPE_ACH;
            }
            else
            {
                pLsppPingTraceEntryInput->MibObject.i4FsLsppEncapType =
                    u4LsppEncap;
            }

            /* If the PathType is MEP, get the Meg name and ME name by calling
             * external API's.
             * Meg name and ME name is neede to get the OID of the MEG table
             * which needs to be filled in the PingTrace Table
             * */
            if (LsppPortGetMegMeName (u4ContextId, &(pLsppExtTrigInfo->
                                                     LsppPathId.unPathId.
                                                     MepIndices),
                                      &(LsppPathId.unPathId.MegName)) !=
                OSIX_SUCCESS)
            {
                LSPP_LOG (u4ContextId, LSPP_EXT_TRIG_GET_MEG_NAME_FAILED,
                          "LsppExtTriggerPing");
                MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                    (UINT1 *) pLsppPingTraceEntryInput);
                return OSIX_FAILURE;
            }

            break;

        default:
            LSPP_LOG (u4ContextId, LSPP_INVALID_PATH_TYPE,
                      "LsppExtTriggerPing");
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceEntryInput);
            return OSIX_FAILURE;
    }

    if ((pLsppPingTraceEntryInput->MibObject.i4FsLsppEncapType ==
         LSPP_ENCAP_TYPE_ACH) ||
        (pLsppPingTraceEntryInput->MibObject.i4FsLsppEncapType ==
         LSPP_ENCAP_TYPE_VCCV_NEGOTIATED))
    {
        pLsppPingTraceEntryInput->MibObject.i4FsLsppReplyMode =
            LSPP_REPLY_APPLICATION_CC;
    }
    else
    {
        pLsppPingTraceEntryInput->MibObject.i4FsLsppReplyMode =
            LSPP_REPLY_IP_UDP;
    }

    if (LsppUtilGetPathPointer (u4ContextId, &LsppPathId, au4PathPointer,
                                &i4OidLength) != OSIX_SUCCESS)
    {
        LSPP_LOG (u4ContextId, LSPP_EXT_TRIG_GET_OID_FAILED,
                  "LsppExtTriggerPing");
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppPingTraceEntryInput);
        return OSIX_FAILURE;
    }

    /* Fill the OID in the PingTrace entry */
    MEMCPY (pLsppPingTraceEntryInput->MibObject.au4FsLsppPathPointer,
            au4PathPointer, i4OidLength);

    pLsppPingTraceEntryInput->MibObject.i4FsLsppPathPointerLen = i4OidLength;

    /* Allocate memory for the new node and add it in the data base */
    pLsppPingTraceEntry =
        LsppFsLsppPingTraceTableCreateApi (pLsppPingTraceEntryInput);

    if (pLsppPingTraceEntry == NULL)
    {
        LSPP_LOG (u4ContextId, LSPP_PINGTRACE_ENTRY_CREATION_FAILED,
                  "LsppExtTriggerPing");
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppPingTraceEntryInput);
        return OSIX_FAILURE;
    }

    /* Trigger Ping request for the external module */
    if (LsppCoreCreateAndTriggerEcho (pLsppPingTraceEntry,
                                      pLsppExtTrigInfo->BfdBtStrapInfo.
                                      u4BfdDiscriminator) != OSIX_SUCCESS)
    {
        LSPP_LOG (u4ContextId, LSPP_EXT_TRIG_ECHO_SEND_FAILED,
                  "LsppExtTriggerPing");
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppPingTraceEntryInput);
        return OSIX_FAILURE;
    }

    /* Sending Trap if Module Owner is BFD */
    LSPP_LOG (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
              LSPP_TRAP_BFD_BOOTSTRAP_REQ, pLsppPingTraceEntry->
              MibObject.i4FsLsppRequestOwner);

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppPingTraceEntryInput);
    return OSIX_SUCCESS;
}
