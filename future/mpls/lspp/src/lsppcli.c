/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: lsppcli.c,v 1.34 2016/06/14 12:29:22 siva Exp $
*
* Description: This file contains the Lspp CLI related routines 
*********************************************************************/
#define __LSPPCLI_C__

#include "lsppinc.h"
#include "lsppcli.h"
#include "lsppclig.h"
#include "lsppext.h"
PRIVATE UINT1       gau1ReturnCode[LSPP_MAX_RETURN_CODE] =
    { 'x', 'M', 'm', '!', 'F',
    'D', 'I', 'U', 'R', 'B',
    'f', 'N', 'P', 'p'
};

/****************************************************************************
 * Function    :  cli_process_Lspp_show_cmd
 * Description :  This function is exported to CLI module to handle the
                  LSPP cli show commands to display the details.

 * Input       :  Variable arguments

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
cli_process_Lspp_show_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[LSPP_CLI_MAX_ARGS];
    INT1                i1Argno = 0;
    UINT4               u4ErrCode = 0;
    INT4                i4RetStatus = CLI_FAILURE;
    INT4                i4Inst = 0;
    UINT1               u1RequestType = 0;

    CliRegisterLock (CliHandle, LsppMainTaskLock, LsppMainTaskUnLock);
    LSPP_LOCK;

    va_start (ap, u4Command);

    i4Inst = va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        u4ErrCode = (UINT4) i4Inst;
    }

    while (LSPP_FOR_EVER)
    {
        args[i1Argno++] = va_arg (ap, UINT4 *);
        if (i1Argno == LSPP_CLI_MAX_ARGS)
        {
            break;
        }
    }

    va_end (ap);
    switch (u4Command)
    {
        case CLI_LSPP_SHOW_GLOBAL_STATS:
            i4RetStatus = LsppCliShowGlobalStats (CliHandle, args[0],
                                                  (UINT1 *) args[1]);
            break;

        case CLI_LSPP_SHOW_GLOBAL_CFGS:
            i4RetStatus = LsppCliShowGlobalConfig (CliHandle, args[0]);
            break;

        case CLI_LSPP_SHOW_PING_TABLE:
            u1RequestType = LSPP_REQ_TYPE_PING;
            i4RetStatus = LsppCliShowPingTraceTable (CliHandle, args[0],
                                                     (UINT1 *) args[1],
                                                     (UINT1 *) args[2],
                                                     u1RequestType);
            break;

        case CLI_LSPP_SHOW_TRACE_TABLE:
            u1RequestType = LSPP_REQ_TYPE_TRACE_ROUTE;
            i4RetStatus = LsppCliShowPingTraceTable (CliHandle,
                                                     args[0], NULL,
                                                     (UINT1 *) args[1],
                                                     u1RequestType);
            break;

        default:
            CliPrintf (CliHandle, "Invalid command");
            break;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < LSPP_CLI_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%% %s", gaLsppCliErrorString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetStatus);

    CliUnRegisterLock (CliHandle);
    LSPP_UNLOCK;

    return i4RetStatus;
}

/****************************************************************************
 * Function    :  LsppCliShowGlobalStats
 * 
 * Description :  This function is to fetch the Global Statistics table whose 
 *                details needs to be dispayed.
 *
 * Input       :  ContextId 
 *                SummaryOption - To inform whether summary of the statistics 
 *                is enough or full detail is needed.
 *
 * Output      :  None 
 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/

INT4
LsppCliShowGlobalStats (tCliHandle CliHandle, UINT4 *pu4ContextId,
                        UINT1 *pu1IsSetSummary)
{

    tLsppFsLsppGlobalStatsTableEntry LsppFsLsppGlobalStatsTableEntry;
    tLsppFsLsppGlobalStatsTableEntry *pLsppGlobalStatsTable = NULL;

    MEMSET (&LsppFsLsppGlobalStatsTableEntry, 0,
            sizeof (tLsppFsLsppGlobalStatsTableEntry));

    /* If the Context name is specified in the show command, display only the
     * table corresponding to that context
     * */

    if (pu4ContextId != NULL)
    {
        LsppFsLsppGlobalStatsTableEntry.MibObject.u4FsLsppContextId =
            *pu4ContextId;

        /* Get the Global Stats table of the given context to display */
        if (LsppGetAllFsLsppGlobalStatsTable
            (&LsppFsLsppGlobalStatsTableEntry) != OSIX_SUCCESS)
        {
            return CLI_SUCCESS;
        }

        /* Print the global statistics */
        LsppCliPrintGlobalStats (CliHandle, &LsppFsLsppGlobalStatsTableEntry,
                                 pu1IsSetSummary);
    }

    /* If the context is not specified in the show command, display 
     * Global stats tables for all the contexts available in the data base
     * */

    else
    {
        /* Get the First Global Stats table from the data base */
        pLsppGlobalStatsTable = LsppGetFirstFsLsppGlobalStatsTable ();

        if (pLsppGlobalStatsTable == NULL)
        {
            return CLI_SUCCESS;
        }

        do
        {
            /* Print the Statistics of first Global Stats table */
            LsppCliPrintGlobalStats (CliHandle, pLsppGlobalStatsTable,
                                     pu1IsSetSummary);

            /* Get the next Global Stats table */
            pLsppGlobalStatsTable =
                LsppGetNextFsLsppGlobalStatsTable (pLsppGlobalStatsTable);

            if (pLsppGlobalStatsTable == NULL)
            {
                /* No more Global Stats tables are available */
                break;
            }

        }
        while (LSPP_FOR_EVER);
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  LsppCliPrintGlobalStats
 * Description :  This function is to show the Global statistics of a
 *                particular table
 *
 * Input       :  GlobalStatsTable - to be printed
 *                SummaryOption - to inform wheteher summary of the statistics
 *                is enough or full deatil is needed
 *
 * Output      :  None 
 *
 * Returns     :  NOne
 * ****************************************************************************/

VOID
LsppCliPrintGlobalStats (tCliHandle CliHandle,
                         tLsppFsLsppGlobalStatsTableEntry
                         * pLsppGlobalStatsTable, UINT1 *pu1IsSetSummary)
{

    CliPrintf (CliHandle, "\r\nContext Id : %d\n",
               pLsppGlobalStatsTable->MibObject.u4FsLsppContextId);

    /* If summary option is NULL, then print the entire statistics */
    if (pu1IsSetSummary == NULL)
    {
        LsppCliPrintGlobalStatsDetail (CliHandle, pLsppGlobalStatsTable);
    }

    /* Print the summmary of the statistics */
    CliPrintf (CliHandle, "\r\nEcho Requests: sent (%d)/received (%d)/"
               "timedout (%d)/unsent (%d) ",
               pLsppGlobalStatsTable->MibObject.u4FsLsppGlbStatReqTx,
               pLsppGlobalStatsTable->MibObject.u4FsLsppGlbStatReqRx,
               pLsppGlobalStatsTable->MibObject.u4FsLsppGlbStatReqTimedOut,
               pLsppGlobalStatsTable->MibObject.u4FsLsppGlbStatReqUnSent);

    CliPrintf (CliHandle, "\r\nEcho Replies: sent (%d)/received (%d)/"
               "dropped (%d)/unsent (%d)\n",
               pLsppGlobalStatsTable->MibObject.u4FsLsppGlbStatReplyTx,
               pLsppGlobalStatsTable->MibObject.u4FsLsppGlbStatReplyRx,
               pLsppGlobalStatsTable->MibObject.u4FsLsppGlbStatReplyDropped,
               pLsppGlobalStatsTable->MibObject.u4FsLsppGlbStatReplyUnSent);

    CliPrintf (CliHandle, "\r\nInvalid packets dropped  - %d\n\n",
               pLsppGlobalStatsTable->MibObject.
               u4FsLsppGlbStatInvalidPktDropped);

    return;
}

/****************************************************************************
 * Function    :  LsppCliShowGlobalConfig
 *
 * Description :  This function is to fetch the Global Config table whose 
 *                detail needs to be displayed.
 *
 * Input       :  ContextId
 *
 * Output      :  None 
 *
 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/

INT4
LsppCliShowGlobalConfig (tCliHandle CliHandle, UINT4 *pu4ContextId)
{
    tLsppFsLsppGlobalConfigTableEntry LsppFsLsppGlobalConfigTable;
    tLsppFsLsppGlobalConfigTableEntry *pLsppGlobalConfigTable = NULL;

    MEMSET (&LsppFsLsppGlobalConfigTable, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));

    /* If the Context is specified in the show command, display only the
     * the Global Config table corresponds to that context */
    if (pu4ContextId != NULL)
    {
        LsppFsLsppGlobalConfigTable.MibObject.u4FsLsppContextId = *pu4ContextId;

        /* Get the Global Config table of the given context to display */
        if (LsppGetAllFsLsppGlobalConfigTable (&LsppFsLsppGlobalConfigTable) !=
            OSIX_SUCCESS)
        {
            return CLI_SUCCESS;
        }

        /* Print the Global Config table */
        LsppCliPrintGlobalConfig (CliHandle, &LsppFsLsppGlobalConfigTable);
    }

    /* If the context is not specified in the show command, diaplay all the 
     * tables for all the contexts available in the data base */
    else
    {
        /* Get the First Global Config table from the data base */
        pLsppGlobalConfigTable = LsppGetFirstFsLsppGlobalConfigTable ();

        if (pLsppGlobalConfigTable == NULL)
        {
            return CLI_SUCCESS;
        }

        do
        {
            /*Print the Global Config table */
            LsppCliPrintGlobalConfig (CliHandle, pLsppGlobalConfigTable);

            /* Get the next Global Config table from the data base */
            pLsppGlobalConfigTable =
                LsppGetNextFsLsppGlobalConfigTable (pLsppGlobalConfigTable);

            if (pLsppGlobalConfigTable == NULL)
            {
                /* No more Global Config table is avilable */
                break;
            }
        }
        while (LSPP_FOR_EVER);
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  LsppCliPrintGlobalConfig
 *
 * Description :  This function is to display the configurations of the global 
 *                parameters of the given Global Config Table
 *
 * Input       :  Global Config Table - to be printed
 *
 * Output      :  None 
 *
 * Returns     :  None
****************************************************************************/
VOID
LsppCliPrintGlobalConfig (tCliHandle CliHandle,
                          tLsppFsLsppGlobalConfigTableEntry
                          * pLsppFsLsppGlobalConfigTable)
{

    CliPrintf (CliHandle, "\r\nContext Name                  : %s",
               pLsppFsLsppGlobalConfigTable->au1ContextName);

    CliPrintf (CliHandle, "\r\nContext Id                    : %d",
               pLsppFsLsppGlobalConfigTable->MibObject.u4FsLsppContextId);

    if (pLsppFsLsppGlobalConfigTable->MibObject.i4FsLsppSystemControl ==
        LSPP_START)
    {
        CliPrintf (CliHandle, "\r\nSystem Control                : START");
    }

    else if (pLsppFsLsppGlobalConfigTable->MibObject.i4FsLsppSystemControl ==
             LSPP_SHUTDOWN)
    {
        CliPrintf (CliHandle, "\r\nSystem Control                : SHUTDOWN");
    }

    CliPrintf (CliHandle, "\r\nTrap Status                   : ");

    if (pLsppFsLsppGlobalConfigTable->MibObject.i4FsLsppTrapStatus ==
        LSPP_ALL_TRAP_DISABLED)
    {
        CliPrintf (CliHandle, "All Traps disabled");
    }

    if (pLsppFsLsppGlobalConfigTable->MibObject.i4FsLsppTrapStatus &
        LSPP_PING_COMPLETION_TRAP)
    {
        CliPrintf (CliHandle, "Ping Completion Trap,");
    }

    if (pLsppFsLsppGlobalConfigTable->MibObject.i4FsLsppTrapStatus &
        LSPP_TRACE_ROUTE_COMPLETION_TRAP)
    {
        CliPrintf (CliHandle, "Trace Route Completion Trap,");
    }

    if (pLsppFsLsppGlobalConfigTable->MibObject.i4FsLsppTrapStatus &
        LSPP_BFD_BTSTRAP_TRAP)
    {
        CliPrintf (CliHandle, "BFD Bootstrap Trap ");
    }

    CliPrintf (CliHandle, "\r\nTrace Level                   : ");

    if (pLsppFsLsppGlobalConfigTable->MibObject.i4FsLsppTraceLevel ==
        LSPP_ALL_TRACES_ENABLED)
    {
        CliPrintf (CliHandle, "All Traces enabled");
    }
    else if (pLsppFsLsppGlobalConfigTable->MibObject.i4FsLsppTraceLevel ==
             LSPP_ALL_TRACES_DISABLED)
    {
        CliPrintf (CliHandle, "All Traces disabled");
    }
    else
    {
        if (pLsppFsLsppGlobalConfigTable->MibObject.i4FsLsppTraceLevel &
            LSPP_INIT_SHUT_TRC)
        {
            CliPrintf (CliHandle, "Init-Shut Trace,");
        }

        if (pLsppFsLsppGlobalConfigTable->MibObject.i4FsLsppTraceLevel &
            LSPP_MGMT_TRC)
        {
            CliPrintf (CliHandle, "Management Trace,");
        }

        if (pLsppFsLsppGlobalConfigTable->MibObject.i4FsLsppTraceLevel &
            LSPP_DATA_TRC)
        {
            CliPrintf (CliHandle, "Data Trace,");
        }

        if (pLsppFsLsppGlobalConfigTable->MibObject.i4FsLsppTraceLevel &
            LSPP_CONTROL_PLANE_TRC)
        {
            CliPrintf (CliHandle, "Control-Plane Trace,");
        }

        if (pLsppFsLsppGlobalConfigTable->MibObject.i4FsLsppTraceLevel &
            LSPP_PKT_DUMP_TRC)
        {
            CliPrintf (CliHandle, "Packet-Dump Trace,");
        }

        if (pLsppFsLsppGlobalConfigTable->MibObject.i4FsLsppTraceLevel &
            LSPP_RESOURCE_TRC)
        {
            CliPrintf (CliHandle, "Resource Trace,");
        }

        if (pLsppFsLsppGlobalConfigTable->MibObject.i4FsLsppTraceLevel &
            LSPP_ALL_FAILURE_TRC)
        {
            CliPrintf (CliHandle, "All-Failure Trace,");
        }

        if (pLsppFsLsppGlobalConfigTable->MibObject.i4FsLsppTraceLevel &
            LSPP_BUF_TRC)
        {
            CliPrintf (CliHandle, "Buffer Trace,");
        }

        if (pLsppFsLsppGlobalConfigTable->MibObject.i4FsLsppTraceLevel &
            LSPP_TLV_TRC)
        {
            CliPrintf (CliHandle, "TLV Trace,");
        }

        if (pLsppFsLsppGlobalConfigTable->MibObject.i4FsLsppTraceLevel &
            LSPP_ERROR_TRC)
        {
            CliPrintf (CliHandle, "Error Trace,");
        }

        if (pLsppFsLsppGlobalConfigTable->MibObject.i4FsLsppTraceLevel &
            LSPP_EVENT_TRC)
        {
            CliPrintf (CliHandle, "Event Trace,");
        }

        if (pLsppFsLsppGlobalConfigTable->MibObject.i4FsLsppTraceLevel &
            LSPP_PKT_ERROR_TRC)
        {
            CliPrintf (CliHandle, "Packet-Error Trace,");
        }

        if (pLsppFsLsppGlobalConfigTable->MibObject.i4FsLsppTraceLevel &
            LSPP_PATH_TRC)
        {
            CliPrintf (CliHandle, "Path Trace");
        }
    }

    CliPrintf (CliHandle, "\r\nPing/Trace Age Out Time       : %d",
               pLsppFsLsppGlobalConfigTable->MibObject.u4FsLsppAgeOutTime);

    if (pLsppFsLsppGlobalConfigTable->MibObject.i4FsLsppAgeOutTmrUnit ==
        LSPP_TMR_UNIT_MILLISEC)
    {
        CliPrintf (CliHandle,
                   "\r\nPing/Trace Age Out Unit       : Milli seconds");
    }
    else if (pLsppFsLsppGlobalConfigTable->MibObject.i4FsLsppAgeOutTmrUnit ==
             LSPP_TMR_UNIT_SEC)
    {
        CliPrintf (CliHandle, "\r\nPing/Trace Age Out Unit       : Seconds");
    }
    else if (pLsppFsLsppGlobalConfigTable->MibObject.i4FsLsppAgeOutTmrUnit ==
             LSPP_TMR_UNIT_MIN)
    {
        CliPrintf (CliHandle, "\r\nPing/Trace Age Out Unit       : Minutes");
    }

    if (pLsppFsLsppGlobalConfigTable->MibObject.i4FsLsppClearEchoStats ==
        LSPP_SNMP_FALSE)
    {
        CliPrintf (CliHandle, "\r\nClear Echo Stats              : DISABLED");
    }
    else if (pLsppFsLsppGlobalConfigTable->MibObject.i4FsLsppClearEchoStats ==
             LSPP_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "\r\nClear Echo Stats              : ENABLED");
    }

    if (pLsppFsLsppGlobalConfigTable->MibObject.i4FsLsppBfdBtStrapRespReq ==
        LSPP_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "\r\nBfd BootStrap Reply Required  : YES");
    }
    else if (pLsppFsLsppGlobalConfigTable->MibObject.
             i4FsLsppBfdBtStrapRespReq == LSPP_SNMP_FALSE)
    {
        CliPrintf (CliHandle, "\r\nBfd BootStrap Reply Required  : NO");
    }

    CliPrintf (CliHandle, "\r\nBfd Bootstrap Age Out Time    : %d",
               pLsppFsLsppGlobalConfigTable->MibObject.
               u4FsLsppBfdBtStrapAgeOutTime);

    if (pLsppFsLsppGlobalConfigTable->MibObject.
        i4FsLsppBfdBtStrapAgeOutTmrUnit == LSPP_TMR_UNIT_MILLISEC)
    {
        CliPrintf (CliHandle,
                   "\r\nBfd Bootstrap Age Out Unit    : Milli seconds\n");
    }
    else if (pLsppFsLsppGlobalConfigTable->MibObject.
             i4FsLsppBfdBtStrapAgeOutTmrUnit == LSPP_TMR_UNIT_SEC)
    {
        CliPrintf (CliHandle, "\r\nBfd Bootstrap Age Out Unit    : Seconds\n");
    }
    else if (pLsppFsLsppGlobalConfigTable->MibObject.
             i4FsLsppBfdBtStrapAgeOutTmrUnit == LSPP_TMR_UNIT_MIN)
    {
        CliPrintf (CliHandle, "\r\nBfd Bootstrap Age Out Unit    : Minutes\n");
    }
    return;
}

/****************************************************************************
 * Function    : LsppCliShowPingTraceTable
 *
 * Description : This function is fetch the each PingTrace node which needs to
 *               be displayed.
 *
 * Input       : ContextId
 *                
 *               IsSeqenceInfo - Flag to inform whether Echo Sequence table
 *               associated with the PingTrace table needs to be printed or not
 *
 *               IsHopInfo - Flag to inform whether HopInfo Table associated
 *               with the PingTrace table needs to be printed or not
 *
 *               u1RequestType - Ping/Trace route
 *
 * Output      : None 
 *
 * Returns     : CLI_SUCCESS/CLI_FAILURE
****************************************************************************/

INT4
LsppCliShowPingTraceTable (tCliHandle CliHandle,
                           UINT4 *pu4ContextId, UINT1 *pu1IsSequenceInfo,
                           UINT1 *pu1IsHopInfo, UINT1 u1RequestType)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableInput = NULL;
    tLsppFsLsppPingTraceTableEntry *pLsppPingTraceTable = NULL;

    pLsppFsLsppPingTraceTableInput = (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableInput == NULL)
    {
        return CLI_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableInput, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* If the Context is specified in the command, then display only the 
     * ping trace tables corresponds to that context
     * */
    if (pu4ContextId != NULL)
    {
        /* Assign values to get the first Ping trace entry in the given
         * context
         * */
        pLsppFsLsppPingTraceTableInput->MibObject.u4FsLsppContextId =
            *pu4ContextId;
        pLsppFsLsppPingTraceTableInput->MibObject.u4FsLsppSenderHandle = 0;

        while (LSPP_FOR_EVER)
        {
            /* Get the Ping trace entry in the given context */
            pLsppPingTraceTable = LsppGetNextFsLsppPingTraceTable
                (pLsppFsLsppPingTraceTableInput);

            if (pLsppPingTraceTable == NULL)
            {
                /* No more ping trace entries are avilable */
                MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                    (UINT1 *) pLsppFsLsppPingTraceTableInput);
                return CLI_SUCCESS;
            }

            if (pLsppPingTraceTable->MibObject.u4FsLsppContextId !=
                *pu4ContextId)
            {
                /*No more Ping Trace tables are available for the given 
                 * context
                 * */
                MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                    (UINT1 *) pLsppFsLsppPingTraceTableInput);
                return CLI_SUCCESS;
            }
            else
            {
                /* Print the Ping trace table available in the given context */
                LsppCliPrintPingTraceTable (CliHandle, pLsppPingTraceTable,
                                            pu1IsSequenceInfo, pu1IsHopInfo,
                                            u1RequestType);
            }

            /* Assign the previous table's sender handle to the input structure
             * to get the next Ping trace table
             * */
            pLsppFsLsppPingTraceTableInput->MibObject.u4FsLsppContextId =
                pLsppPingTraceTable->MibObject.u4FsLsppContextId;

            pLsppFsLsppPingTraceTableInput->MibObject.u4FsLsppSenderHandle =
                pLsppPingTraceTable->MibObject.u4FsLsppSenderHandle;
        }
    }

    /* If the Context name is not specified in the command, then display all 
     * the Ping trace tables in the data base
     * */

    else
    {
        /* Get the first PingTrace table */
        pLsppPingTraceTable = LsppGetFirstFsLsppPingTraceTable ();

        if (pLsppPingTraceTable == NULL)
        {
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppFsLsppPingTraceTableInput);
            return CLI_SUCCESS;
        }

        do
        {
            /* Print the Ping Trace table */
            LsppCliPrintPingTraceTable (CliHandle, pLsppPingTraceTable,
                                        pu1IsSequenceInfo, pu1IsHopInfo,
                                        u1RequestType);
            /* Get the next PingTrace table from the data base */
            pLsppPingTraceTable =
                LsppGetNextFsLsppPingTraceTable (pLsppPingTraceTable);

            if (pLsppPingTraceTable == NULL)
            {
                /* No more ping trace table in the database */
                MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                    (UINT1 *) pLsppFsLsppPingTraceTableInput);
                return CLI_SUCCESS;
            }

        }
        while (LSPP_FOR_EVER);
    }
}

/****************************************************************************
 * Function    : LsppCliPrintPingTraceTable
 *
 * Description : This function is to display the PingTrace node and decide 
 *               about whether Sequence Table and HopInfo table associated 
 *               with the PingTrace 
 *               node needs to printed or not.
 *
 * Input       : PingTrace Table - to be printed
 *             
 *               IsSeqenceInfo - Flag to inform whether Echo Sequence table
 *               associated with the PingTrace table needs to be printed or not
 *
 *               IsHopInfo - Flag to inform whether HopInfo Table associated
 *               with the PingTrace table needs to be printed or not
 *
 *               u1RequestType - Ping/Trace route
 *
 * Output      : None 
 *
 * Returns     : None
 ****************************************************************************/
VOID
LsppCliPrintPingTraceTable (tCliHandle CliHandle,
                            tLsppFsLsppPingTraceTableEntry
                            * pLsppPingTraceTable,
                            UINT1 *pu1IsSequenceInfo,
                            UINT1 *pu1IsHopInfo, UINT1 u1RequestType)
{

    /* If the request type is Ping, display only the objects related to ping */
    if (u1RequestType == LSPP_REQ_TYPE_PING)
    {
        LsppCliDisplayPingTable (CliHandle, pLsppPingTraceTable);
    }

    /* If the request type is trace route, display only the objects 
     * related to traceroute */
    else
    {
        LsppCliDisplayTraceRouteTable (CliHandle, pLsppPingTraceTable);
    }

    /* Print all the Sequence table belongs to this Ping table,
     * if the option is provided */
    if (pu1IsSequenceInfo != NULL)
    {
        LsppCliPrintEchoSequenceTable (CliHandle, pLsppPingTraceTable);
    }

    /* Print the HopInfo table belongs to this Ping table,
     * if the option is provided */
    if (pu1IsHopInfo != NULL)
    {
        LsppCliPrintHopInfoTable (CliHandle, pLsppPingTraceTable);
    }
    return;
}

/****************************************************************************
 * Function    : LsppCliDisplayPingTable
 *
 * Description : This function is to display the PingTrace Table

 * Input       : PingTrace Table

 * Output      : None 

 * Returns     : None 
 ***************************************************************************/
VOID
LsppCliDisplayPingTable (tCliHandle CliHandle,
                         tLsppFsLsppPingTraceTableEntry * pLsppPingTable)
{
    tUtlIn6Addr         Ip6Addr;
    UINT1               au1ResponderAddr[LSPP_MAX_IP_ADDR_LEN];
    UINT1               au1Icc[LSPP_MPLS_ICC_LENGTH + 1];
    UINT1               au1Umc[LSPP_MPLS_UMC_LENGTH + 1];
    UINT1               u1PadLen = 0;

    MEMSET (&Ip6Addr, 0, sizeof (tUtlIn6Addr));
    MEMSET (au1ResponderAddr, 0, LSPP_MAX_IP_ADDR_LEN);
    MEMSET (&au1Icc, 0, (LSPP_MPLS_ICC_LENGTH + 1));
    MEMSET (&au1Umc, 0, (LSPP_MPLS_UMC_LENGTH + 1));

    if (pLsppPingTable->MibObject.i4FsLsppRequestType != LSPP_REQ_TYPE_PING)
    {
        return;
    }
    CliPrintf (CliHandle, "\r\n\nPING TABLE\n");

    CliPrintf (CliHandle, "\r\nContext Id                           : %d ",
               pLsppPingTable->MibObject.u4FsLsppContextId);
    CliPrintf (CliHandle, "\r\nSenders Handle                       : %d",
               pLsppPingTable->MibObject.u4FsLsppSenderHandle);
    CliPrintf (CliHandle, "\r\nRepeat Count                         : %d",
               pLsppPingTable->MibObject.u4FsLsppRepeatCount);
    CliPrintf (CliHandle, "\r\npacket Size                          : %d",
               pLsppPingTable->MibObject.u4FsLsppPacketSize);
    CliPrintf (CliHandle, "\r\nTTL value                            : %d",
               pLsppPingTable->MibObject.u4FsLsppTTLValue);
    CliPrintf (CliHandle, "\r\nWFR interval                         : %d",
               pLsppPingTable->MibObject.u4FsLsppWFRInterval);

    CliPrintf (CliHandle, "\r\nTarget MIP Global Id                 : %d",
               pLsppPingTable->MibObject.u4FsLsppTgtMipGlobalId);
    CliPrintf (CliHandle, "\r\nTarget MIP Node Id                   : %d",
               pLsppPingTable->MibObject.u4FsLsppTgtMipNodeId);
    CliPrintf (CliHandle, "\r\nTarget MIP Intf Num                  : %d",
               pLsppPingTable->MibObject.u4FsLsppTgtMipIfNum);

    /* WFR timer unit */
    if (pLsppPingTable->MibObject.i4FsLsppWFRTmrUnit == LSPP_TMR_UNIT_MILLISEC)
    {
        CliPrintf (CliHandle,
                   "\r\nWFR time Unit                        : Milli seconds");
    }
    else if (pLsppPingTable->MibObject.i4FsLsppWFRTmrUnit == LSPP_TMR_UNIT_SEC)
    {
        CliPrintf (CliHandle,
                   "\r\nWFR time Unit                        : Seconds");
    }
    else if (pLsppPingTable->MibObject.i4FsLsppWFRTmrUnit == LSPP_TMR_UNIT_MIN)
    {
        CliPrintf (CliHandle,
                   "\r\nWFR time Unit                        : Minutes");
    }

    CliPrintf (CliHandle, "\r\nWTS interval                         : %d",
               pLsppPingTable->MibObject.u4FsLsppWTSInterval);

    /* WTS timer unit */
    if (pLsppPingTable->MibObject.i4FsLsppWTSTmrUnit == LSPP_TMR_UNIT_MILLISEC)
    {
        CliPrintf (CliHandle,
                   "\r\nWTS time Unit                        : Milli seconds");
    }
    else if (pLsppPingTable->MibObject.i4FsLsppWTSTmrUnit == LSPP_TMR_UNIT_SEC)
    {
        CliPrintf (CliHandle,
                   "\r\nWTS time Unit                        : Seconds");
    }
    else if (pLsppPingTable->MibObject.i4FsLsppWTSTmrUnit == LSPP_TMR_UNIT_MIN)
    {
        CliPrintf (CliHandle,
                   "\r\nWTS time Unit                        : Minutes");
    }

    CliPrintf (CliHandle, "\r\nReply DSCP Value                     : %d",
               pLsppPingTable->MibObject.u4FsLsppReplyDscpValue);

    if (pLsppPingTable->MibObject.i4FsLsppSweepOption == LSPP_SNMP_TRUE)
    {
        CliPrintf (CliHandle,
                   "\r\nSweep Option                         : Enabled");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\nSweep Option                         : Disabled");
    }

    CliPrintf (CliHandle, "\r\nSweep Minimum Range                  : %d",
               pLsppPingTable->MibObject.u4FsLsppSweepMinimum);
    CliPrintf (CliHandle, "\r\nSweep Maximum Range                  : %d",
               pLsppPingTable->MibObject.u4FsLsppSweepMaximum);
    CliPrintf (CliHandle, "\r\nSweep Incremental Value              : %d",
               pLsppPingTable->MibObject.u4FsLsppSweepIncrement);

    if (pLsppPingTable->MibObject.i4FsLsppBurstOption == LSPP_SNMP_TRUE)
    {
        CliPrintf (CliHandle,
                   "\r\nBurst Option                         : Enabled");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\nBurst Option                         : Disabled");
    }

    CliPrintf (CliHandle, "\r\nBurst Size                           : %d",
               pLsppPingTable->MibObject.u4FsLsppBurstSize);
    CliPrintf (CliHandle, "\r\nEXP Value                            : %d",
               pLsppPingTable->MibObject.u4FsLsppEXPValue);
    CliPrintf (CliHandle, "\r\nMax Round Trip Time                  : %d",
               pLsppPingTable->MibObject.u4FsLsppMaxRtt);
    CliPrintf (CliHandle, "\r\nMin Round Trip Time                  : %d",
               pLsppPingTable->MibObject.u4FsLsppMinRtt);
    CliPrintf (CliHandle, "\r\nAverage Round Trip Time              : %d",
               pLsppPingTable->MibObject.u4FsLsppAverageRtt);
    CliPrintf (CliHandle, "\r\nPackets Tx                           : %d",
               pLsppPingTable->MibObject.u4FsLsppPktsTx);
    CliPrintf (CliHandle, "\r\nPackets Rx                           : %d",
               pLsppPingTable->MibObject.u4FsLsppPktsRx);
    CliPrintf (CliHandle, "\r\nPackets UnSent                       : %d",
               pLsppPingTable->MibObject.u4FsLsppPktsUnSent);
    CliPrintf (CliHandle, "\r\nRequest Owner                        : ");

    if (pLsppPingTable->MibObject.i4FsLsppRequestOwner == LSPP_REQ_OWNER_MGMT)
    {
        if (pLsppPingTable->u1RequestOwnerSubType == LSPP_REQ_OWNER_MGMT_CLI)
        {
            CliPrintf (CliHandle, "CLI");
        }
        else if (pLsppPingTable->u1RequestOwnerSubType ==
                 LSPP_REQ_OWNER_MGMT_SNMP)
        {
            CliPrintf (CliHandle, "SNMP");
        }
        else
        {
            CliPrintf (CliHandle, "Unknown");
        }
    }
    else if (pLsppPingTable->MibObject.i4FsLsppRequestOwner ==
             LSPP_REQ_OWNER_BFD)
    {
        CliPrintf (CliHandle, "BFD");
    }
    else
    {
        CliPrintf (CliHandle, "Unknown");
    }

    /* Path type */
    if (pLsppPingTable->MibObject.i4FsLsppPathType == LSPP_PATH_TYPE_LDP_IPV4)
    {
        CliPrintf (CliHandle,
                   "\r\nPath Type                            : LDP IPv4");
    }
    else if (pLsppPingTable->MibObject.i4FsLsppPathType ==
             LSPP_PATH_TYPE_LDP_IPV6)
    {
        CliPrintf (CliHandle,
                   "\r\nPath Type                            : LDP IPv6");
    }
    else if (pLsppPingTable->MibObject.i4FsLsppPathType ==
             LSPP_PATH_TYPE_RSVP_IPV4)
    {
        CliPrintf (CliHandle,
                   "\r\nPath Type                            : RSVP-TE IPv4");
    }
    else if (pLsppPingTable->MibObject.i4FsLsppPathType ==
             LSPP_PATH_TYPE_RSVP_IPV6)
    {
        CliPrintf (CliHandle,
                   "\r\nPath Type                            : RSVP-TE IPv6");
    }
    else if (pLsppPingTable->MibObject.i4FsLsppPathType ==
             LSPP_PATH_TYPE_FEC_128)
    {
        CliPrintf (CliHandle,
                   "\r\nPath Type                            : FEC 128 PW");
    }
    else if (pLsppPingTable->MibObject.i4FsLsppPathType ==
             LSPP_PATH_TYPE_FEC_129)
    {
        CliPrintf (CliHandle,
                   "\r\nPath Type                            : FEC 129 PW");
    }
    else if (pLsppPingTable->MibObject.i4FsLsppPathType == LSPP_PATH_TYPE_MEP)
    {
        CliPrintf (CliHandle, "\r\nPath Type                            : MEP");
    }

    CliPrintf (CliHandle, "\r\nReply Mode                           : %d",
               pLsppPingTable->MibObject.i4FsLsppReplyMode);

    if (pLsppPingTable->MibObject.i4FsLsppDsMap == LSPP_SNMP_TRUE)
    {
        CliPrintf (CliHandle,
                   "\r\nDsMap Option                         : Enabled");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\nDsMap Option                         : Disabled");
    }

    if (pLsppPingTable->MibObject.i4FsLsppFecValidate == LSPP_SNMP_TRUE)
    {
        CliPrintf (CliHandle,
                   "\r\nFec Validate Option                  : Enabled");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\nFec Validate Option                  : Disabled");
    }

    if (pLsppPingTable->MibObject.i4FsLsppReplyPadTlv == LSPP_SNMP_TRUE)
    {
        CliPrintf (CliHandle,
                   "\r\nReply PAD TLV                        : Enabled");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\nReply PAD TLV                        : Disabled");
    }

    if (pLsppPingTable->MibObject.i4FsLsppForceExplicitNull == LSPP_SNMP_TRUE)
    {
        CliPrintf (CliHandle,
                   "\r\nForce Explicit NULL                  : Enabled");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\nForce Explicit NULL                  : Disabled");
    }

    if (pLsppPingTable->MibObject.i4FsLsppInterfaceLabelTlv == LSPP_SNMP_TRUE)
    {
        CliPrintf (CliHandle,
                   "\r\nRx Interface Label TLV               : Enabled");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\nRx Interface Label TLV               : Disabled");
    }

    if (pLsppPingTable->MibObject.i4FsLsppSameSeqNumOption == LSPP_SNMP_TRUE)
    {
        CliPrintf (CliHandle,
                   "\r\nSame Sequence-Num Option             : Enabled");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\nSame Sequence-Num Option             : Disabled");
    }

    if (pLsppPingTable->MibObject.i4FsLsppVerbose == LSPP_SNMP_TRUE)
    {
        CliPrintf (CliHandle,
                   "\r\nVerbose                              : Enabled");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\nVerbose                              : Disabled");
    }

    if (pLsppPingTable->MibObject.i4FsLsppReversePathVerify == LSPP_SNMP_TRUE)
    {
        CliPrintf (CliHandle,
                   "\r\nRev-Path Verify                      : Enabled");
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\nRev-Path Verify                      : Disabled");
    }

    /* Encapsulation type */
    if (pLsppPingTable->MibObject.i4FsLsppEncapType ==
        LSPP_ENCAP_TYPE_VCCV_NEGOTIATED)
    {
        CliPrintf (CliHandle,
                   "\r\nEncapsulation Type                   :VCCV-NEGOTIATED ");
    }
    else if (pLsppPingTable->MibObject.i4FsLsppEncapType == LSPP_ENCAP_TYPE_IP)
    {
        CliPrintf (CliHandle,
                   "\r\nEncapsulation Type                   : MPLS-IP");
    }
    else if (pLsppPingTable->MibObject.i4FsLsppEncapType ==
             LSPP_ENCAP_TYPE_ACH_IP)
    {
        CliPrintf (CliHandle,
                   "\r\nEncapsulation Type                   : MPLS-ACH-IP");
    }
    else if (pLsppPingTable->MibObject.i4FsLsppEncapType == LSPP_ENCAP_TYPE_ACH)
    {
        CliPrintf (CliHandle,
                   "\r\nEncapsulation Type                   : MPLS_ACH");
    }

    /* Status of ping */
    if (pLsppPingTable->MibObject.i4FsLsppStatus == LSPP_ECHO_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\nStatus of Ping                       : Success");
    }
    else if (pLsppPingTable->MibObject.i4FsLsppStatus == LSPP_ECHO_IN_PROGRESS)
    {
        CliPrintf (CliHandle,
                   "\r\nStatus of Ping                       : In-Progress");
    }
    else if (pLsppPingTable->MibObject.i4FsLsppStatus == LSPP_ECHO_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\nStatus of Ping                       : Failure");
    }

    /* Direction of Validation */
    if (pLsppPingTable->MibObject.i4FsLsppStatusPathDirection ==
        LSPP_VALIDATION_DIRECTION_FORWARD)
    {
        CliPrintf (CliHandle,
                   "\r\nDirection of Validation              : Forward");
    }
    else if (pLsppPingTable->MibObject.i4FsLsppStatusPathDirection ==
             LSPP_VALIDATION_DIRECTION_REVERSE)
    {
        CliPrintf (CliHandle,
                   "\r\nDirection of Validation              : Reverse");
    }
    else if (pLsppPingTable->MibObject.i4FsLsppStatusPathDirection ==
             LSPP_VALIDATION_DIRECTION_BOTH)
    {
        CliPrintf (CliHandle,
                   "\r\nDirection of Validation              : Both");
    }

    CliPrintf (CliHandle, "\r\nPAD Pattern                          : ");
    for (u1PadLen = 0;
         u1PadLen < pLsppPingTable->MibObject.i4FsLsppPadPatternLen; u1PadLen++)
    {
        CliPrintf (CliHandle, "%x", pLsppPingTable->MibObject.
                   au1FsLsppPadPattern[u1PadLen]);
    }

    if (pLsppPingTable->MibObject.i4FsLsppReplyMode != LSPP_NO_REPLY)
    {
        if (pLsppPingTable->MibObject.i4FsLsppResponderAddrType ==
            LSPP_IPV4_NUMBERED)
        {
            MEMCPY (au1ResponderAddr, pLsppPingTable->MibObject.
                    au1FsLsppResponderAddr, LSPP_IPV4_ADDR_LEN);

            CliPrintf (CliHandle,
                       "\r\nResponder Address                    :%d.%d.%d.%d",
                       au1ResponderAddr[0], au1ResponderAddr[1],
                       au1ResponderAddr[2], au1ResponderAddr[3]);
            CliPrintf (CliHandle,
                       "\r\nResponder Address Type              : IPv4");
        }
        else if (pLsppPingTable->MibObject.i4FsLsppResponderAddrType ==
                 LSPP_IPV6_NUMBERED)
        {
            MEMCPY (&Ip6Addr, pLsppPingTable->MibObject.au1FsLsppResponderAddr,
                    LSPP_IPV6_ADDR_LEN);

            STRNCPY (au1ResponderAddr, INET_NTOA6 (Ip6Addr),
                     (sizeof (au1ResponderAddr) - 1));
            au1ResponderAddr[sizeof (au1ResponderAddr) - 1] = '\0';

            CliPrintf (CliHandle, "%s", au1ResponderAddr);
            CliPrintf (CliHandle,
                       "\r\nResponder Address Type               : IPv6");
        }

        else if (pLsppPingTable->MibObject.i4FsLsppResponderAddrType ==
                 LSPP_ADDR_TYPE_NOT_APPLICABLE)
        {
            if (pLsppPingTable->u1MegOperatorType == OAM_OPERATOR_TYPE_IP)
            {
                CliPrintf (CliHandle,
                           "\r\nResponder Global Id                  : %d",
                           pLsppPingTable->MibObject.u4FsLsppResponderGlobalId);
                CliPrintf (CliHandle,
                           "\r\nResponder Id                         : %d",
                           pLsppPingTable->MibObject.u4FsLsppResponderId);
            }
            else if (pLsppPingTable->u1MegOperatorType == OAM_OPERATOR_TYPE_ICC)
            {
                MEMCPY (&au1Icc, pLsppPingTable->MibObject.
                        au1FsLsppResponderIcc, LSPP_MPLS_ICC_LENGTH);

                MEMCPY (&au1Umc, pLsppPingTable->MibObject.
                        au1FsLsppResponderUMC, LSPP_MPLS_UMC_LENGTH);

                CliPrintf (CliHandle,
                           "\r\nResponder ICC                        : %s",
                           au1Icc);

                CliPrintf (CliHandle,
                           "\r\nResponder UMC                        : %s",
                           au1Umc);

                CliPrintf (CliHandle,
                           "\r\nResponder MepIndex                   : %d",
                           pLsppPingTable->MibObject.u4FsLsppResponderMepIndex);
            }

            CliPrintf (CliHandle,
                       "\r\nResponder Address Type                    :"
                       "Not Applicable");
        }
    }
    return;
}

/****************************************************************************
 * Function    : LsppCliDisplayTraceRouteTable
 *
 * Description : This function is to display the PingTrace Table.
 *               It will display only the objects corresponds to Trace route
 *
 * Input       : PingTrace Table - to be printed
 *
 * Output      :  None 
 *
 * Returns     : None 
 ***************************************************************************/
VOID
LsppCliDisplayTraceRouteTable (tCliHandle CliHandle,
                               tLsppFsLsppPingTraceTableEntry
                               * pLsppTraceRouteTable)
{
    UINT1               u1PadLen = 0;

    if (pLsppTraceRouteTable->MibObject.i4FsLsppRequestType !=
        LSPP_REQ_TYPE_TRACE_ROUTE)
    {
        return;
    }
    CliPrintf (CliHandle, "\r\n");

    CliPrintf (CliHandle, "\r\nTRACE ROUTE TABLE\n");

    CliPrintf (CliHandle, "\r\n");

    CliPrintf (CliHandle, "\r\nContext Id                    : %d ",
               pLsppTraceRouteTable->MibObject.u4FsLsppContextId);

    CliPrintf (CliHandle, "\r\nSenders Handle                : %d",
               pLsppTraceRouteTable->MibObject.u4FsLsppSenderHandle);

    CliPrintf (CliHandle, "\r\nMax TTL value                 : %d",
               pLsppTraceRouteTable->MibObject.u4FsLsppTTLValue);
    CliPrintf (CliHandle, "\r\nActual Hop Count              : %d",
               pLsppTraceRouteTable->MibObject.u4FsLsppActualHopCount);

    CliPrintf (CliHandle, "\r\nWFR interval                  : %d",
               pLsppTraceRouteTable->MibObject.u4FsLsppWFRInterval);

    /* WFR timer unit */
    if (pLsppTraceRouteTable->MibObject.i4FsLsppWFRTmrUnit ==
        LSPP_TMR_UNIT_MILLISEC)
    {
        CliPrintf (CliHandle,
                   "\r\nWFR time Unit                 : Milli seconds");
    }
    else if (pLsppTraceRouteTable->MibObject.i4FsLsppWFRTmrUnit ==
             LSPP_TMR_UNIT_SEC)
    {
        CliPrintf (CliHandle, "\r\nWFR time Unit                 : Seconds");
    }
    else if (pLsppTraceRouteTable->MibObject.i4FsLsppWFRTmrUnit ==
             LSPP_TMR_UNIT_MIN)
    {
        CliPrintf (CliHandle, "\r\nWFR time Unit                 : Minutes");
    }

    CliPrintf (CliHandle, "\r\nReply DSCP Value              : %d",
               pLsppTraceRouteTable->MibObject.u4FsLsppReplyDscpValue);

    CliPrintf (CliHandle, "\r\nEXP Value                     : %d",
               pLsppTraceRouteTable->MibObject.u4FsLsppEXPValue);
    CliPrintf (CliHandle, "\r\nPackets Tx                    : %d",
               pLsppTraceRouteTable->MibObject.u4FsLsppPktsTx);
    CliPrintf (CliHandle, "\r\nPackets Rx                    : %d",
               pLsppTraceRouteTable->MibObject.u4FsLsppPktsRx);
    CliPrintf (CliHandle, "\r\nPackets UnSent                : %d",
               pLsppTraceRouteTable->MibObject.u4FsLsppPktsUnSent);

    CliPrintf (CliHandle, "\r\nRequest Owner                 : ");

    if (pLsppTraceRouteTable->MibObject.i4FsLsppRequestOwner ==
        LSPP_REQ_OWNER_MGMT)
    {
        if (pLsppTraceRouteTable->u1RequestOwnerSubType ==
            LSPP_REQ_OWNER_MGMT_CLI)
        {
            CliPrintf (CliHandle, "CLI");
        }

        else if (pLsppTraceRouteTable->u1RequestOwnerSubType ==
                 LSPP_REQ_OWNER_MGMT_SNMP)
        {
            CliPrintf (CliHandle, "SNMP");
        }
        else
        {
            CliPrintf (CliHandle, "Unknown");
        }
    }
    else if (pLsppTraceRouteTable->MibObject.i4FsLsppRequestOwner ==
             LSPP_REQ_OWNER_BFD)
    {
        CliPrintf (CliHandle, "BFD");
    }
    else
    {
        CliPrintf (CliHandle, "Unknown");
    }

    /* Path type */
    if (pLsppTraceRouteTable->MibObject.i4FsLsppPathType ==
        LSPP_PATH_TYPE_LDP_IPV4)
    {
        CliPrintf (CliHandle, "\r\nPath Type                     : LDP IPV4");
    }
    else if (pLsppTraceRouteTable->MibObject.i4FsLsppPathType ==
             LSPP_PATH_TYPE_LDP_IPV6)
    {
        CliPrintf (CliHandle, "\r\nPath Type                     : LDP IPV6");
    }
    else if (pLsppTraceRouteTable->MibObject.i4FsLsppPathType ==
             LSPP_PATH_TYPE_RSVP_IPV4)
    {
        CliPrintf (CliHandle,
                   "\r\nPath Type                     : RSVP-TE IPv4");
    }
    else if (pLsppTraceRouteTable->MibObject.i4FsLsppPathType ==
             LSPP_PATH_TYPE_RSVP_IPV6)
    {
        CliPrintf (CliHandle,
                   "\r\nPath Type                     : RSVP-TE IPv6");
    }
    else if (pLsppTraceRouteTable->MibObject.i4FsLsppPathType ==
             LSPP_PATH_TYPE_FEC_128)
    {
        CliPrintf (CliHandle, "\r\nPath Type                     : FEC 128 PW");
    }
    else if (pLsppTraceRouteTable->MibObject.i4FsLsppPathType ==
             LSPP_PATH_TYPE_FEC_129)
    {
        CliPrintf (CliHandle, "\r\nPath Type                     : FEC 129 PW");
    }
    else if (pLsppTraceRouteTable->MibObject.i4FsLsppPathType ==
             LSPP_PATH_TYPE_MEP)
    {
        CliPrintf (CliHandle, "\r\nPath Type                     : MEP");
    }

    CliPrintf (CliHandle, "\r\nReply Mode                    : %d",
               pLsppTraceRouteTable->MibObject.i4FsLsppReplyMode);

    if (pLsppTraceRouteTable->MibObject.i4FsLsppDsMap == LSPP_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "\r\nDsMap Option                  : Enabled");
    }
    else
    {
        CliPrintf (CliHandle, "\r\nDsMap Option                  : Disabled");
    }

    if (pLsppTraceRouteTable->MibObject.i4FsLsppFecValidate == LSPP_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "\r\nFec Validate Option           : Enabled");
    }
    else
    {
        CliPrintf (CliHandle, "\r\nFec Validate Option           : Disabled");
    }

    if (pLsppTraceRouteTable->MibObject.i4FsLsppReplyPadTlv == LSPP_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "\r\nReply PAD TLV                 : Enabled");
    }
    else
    {
        CliPrintf (CliHandle, "\r\nReply PAD TLV                 : Disabled");
    }

    if (pLsppTraceRouteTable->MibObject.i4FsLsppForceExplicitNull ==
        LSPP_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "\r\nForce Explicit NULL           : Enabled");
    }
    else
    {
        CliPrintf (CliHandle, "\r\nForce Explicit NULL           : Disabled");
    }

    if (pLsppTraceRouteTable->MibObject.i4FsLsppInterfaceLabelTlv ==
        LSPP_SNMP_TRUE)
    {
        CliPrintf (CliHandle, "\r\nRx Interface Label TLV        : Enabled");
    }
    else
    {
        CliPrintf (CliHandle, "\r\nRx Interface Label TLV        : Disabled");
    }

    /* Encapsulation type */
    if (pLsppTraceRouteTable->MibObject.i4FsLsppEncapType ==
        LSPP_ENCAP_TYPE_VCCV_NEGOTIATED)
    {
        CliPrintf (CliHandle,
                   "\r\nEncapsulation Type            :VCCV-NEGOTIATED ");
    }
    else if (pLsppTraceRouteTable->MibObject.i4FsLsppEncapType ==
             LSPP_ENCAP_TYPE_IP)
    {
        CliPrintf (CliHandle, "\r\nEncapsulation Type            : MPLS-IP");
    }
    else if (pLsppTraceRouteTable->MibObject.i4FsLsppEncapType ==
             LSPP_ENCAP_TYPE_ACH_IP)
    {
        CliPrintf (CliHandle,
                   "\r\nEncapsulation Type            : MPLS-ACH-IP");
    }
    else if (pLsppTraceRouteTable->MibObject.i4FsLsppEncapType ==
             LSPP_ENCAP_TYPE_ACH)
    {
        CliPrintf (CliHandle, "\r\nEncapsulation Type            : MPLS_ACH");
    }
    CliPrintf (CliHandle, "\r\nPAD Pattern                   : ");

    for (u1PadLen = 0;
         u1PadLen < pLsppTraceRouteTable->MibObject.i4FsLsppPadPatternLen;
         u1PadLen++)
    {
        CliPrintf (CliHandle, "%x", pLsppTraceRouteTable->MibObject.
                   au1FsLsppPadPattern[u1PadLen]);
    }

    /* Status of Trace  route */
    if (pLsppTraceRouteTable->MibObject.i4FsLsppStatus == LSPP_ECHO_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nStatus of Trace Route         : Success");
    }
    else if (pLsppTraceRouteTable->MibObject.i4FsLsppStatus ==
             LSPP_ECHO_IN_PROGRESS)
    {
        CliPrintf (CliHandle,
                   "\r\nStatus of Trace Route         : In-Progress");
    }
    else if (pLsppTraceRouteTable->MibObject.i4FsLsppStatus ==
             LSPP_ECHO_FAILURE)
    {
        CliPrintf (CliHandle, "\r\nStatus of Trace Route         : Failure");
    }

    /* Direction of Validation */
    if (pLsppTraceRouteTable->MibObject.i4FsLsppStatusPathDirection ==
        LSPP_VALIDATION_DIRECTION_FORWARD)
    {
        CliPrintf (CliHandle, "\r\nDirection of Validation          : Forward");
    }
    else if (pLsppTraceRouteTable->MibObject.i4FsLsppStatusPathDirection ==
             LSPP_VALIDATION_DIRECTION_REVERSE)
    {
        CliPrintf (CliHandle, "\r\nDirection of Validation          : Reverse");
    }
    else if (pLsppTraceRouteTable->MibObject.i4FsLsppStatusPathDirection ==
             LSPP_VALIDATION_DIRECTION_BOTH)
    {
        CliPrintf (CliHandle, "\r\nDirection of Validation          : Both");
    }
    return;
}

/****************************************************************************
 * Function    : LsppCliPrintEchoSequenceTable
 *
 * Description : This function is to fetch the Echo sequence Table corresponds 
 *               to the given PingTrace Entry.
 *
 * Input       : PingTrace Table
 *
 * Output      : None 
 *
 * Returns     : None
 ***************************************************************************/
VOID
LsppCliPrintEchoSequenceTable (tCliHandle CliHandle,
                               tLsppFsLsppPingTraceTableEntry
                               * pLsppPingTraceTable)
{
    tLsppFsLsppEchoSequenceTableEntry LsppEchoSequenceTable;
    tLsppFsLsppEchoSequenceTableEntry *pLsppEchoSequenceTable = NULL;

    MEMSET (&LsppEchoSequenceTable, 0,
            sizeof (tLsppFsLsppEchoSequenceTableEntry));

    /* Assign the values to get the first Echo sequence entry for the given 
     * ping trace entry
     * */
    LsppEchoSequenceTable.MibObject.u4FsLsppContextId =
        pLsppPingTraceTable->MibObject.u4FsLsppContextId;

    LsppEchoSequenceTable.MibObject.u4FsLsppSenderHandle =
        pLsppPingTraceTable->MibObject.u4FsLsppSenderHandle;

    LsppEchoSequenceTable.MibObject.u4FsLsppSequenceNumber = 0;

    while (LSPP_FOR_EVER)
    {
        /* Get the Echo sequence table in the given Ping trace entry */
        pLsppEchoSequenceTable =
            LsppGetNextFsLsppEchoSequenceTable (&LsppEchoSequenceTable);

        if (pLsppEchoSequenceTable == NULL)
        {
            /*No more Echo sequence entries present */
            return;
        }
        if ((pLsppEchoSequenceTable->MibObject.u4FsLsppContextId !=
             pLsppPingTraceTable->MibObject.u4FsLsppContextId) ||
            (pLsppEchoSequenceTable->MibObject.u4FsLsppSenderHandle !=
             pLsppPingTraceTable->MibObject.u4FsLsppSenderHandle))
        {
            /* No more Echo sequence entries present for the given PingTrace
             * entry
             * */
            return;
        }
        else
        {
            /* Display the echo sequence table */
            LsppCliDisplayEchoSequence (CliHandle, pLsppEchoSequenceTable);
        }

        /* Assign the previous table's sendersHandle and Sequence number to
         * the input structure to get the next table
         * */
        LsppEchoSequenceTable.MibObject.u4FsLsppContextId =
            pLsppEchoSequenceTable->MibObject.u4FsLsppContextId;

        LsppEchoSequenceTable.MibObject.u4FsLsppSenderHandle =
            pLsppEchoSequenceTable->MibObject.u4FsLsppSenderHandle;

        LsppEchoSequenceTable.MibObject.u4FsLsppSequenceNumber =
            pLsppEchoSequenceTable->MibObject.u4FsLsppSequenceNumber;
    }
}

/****************************************************************************
 * Function    : LsppCliDisplayEchoSequence
 *
 * Description : This function is to display the EchoSequence Table
 *
 * Input       : Echo Sequence table - to be printed
 *
 * Output      : None 
 *
 * Returns     : None
 ***************************************************************************/
VOID
LsppCliDisplayEchoSequence (tCliHandle CliHandle,
                            tLsppFsLsppEchoSequenceTableEntry *
                            pLsppEchoSequenceTable)
{
    CliPrintf (CliHandle, "\r\n");

    CliPrintf (CliHandle, "\r\nEcho Sequence Table\n");

    CliPrintf (CliHandle, "\r\nContext Id         : %d",
               pLsppEchoSequenceTable->MibObject.u4FsLsppContextId);
    CliPrintf (CliHandle, "\r\nSenders Handle     : %d",
               pLsppEchoSequenceTable->MibObject.u4FsLsppSenderHandle);
    CliPrintf (CliHandle, "\r\nSequence Number    : %d",
               pLsppEchoSequenceTable->MibObject.u4FsLsppSequenceNumber);
    CliPrintf (CliHandle, "\r\nReturn Code        : %d",
               pLsppEchoSequenceTable->MibObject.u4FsLsppReturnCode);
    CliPrintf (CliHandle, "\r\nReturn Code String : %s",
               pLsppEchoSequenceTable->MibObject.au1FsLsppReturnCodeStr);
    CliPrintf (CliHandle, "\r\nReturn Sub Code    : %d\n",
               pLsppEchoSequenceTable->MibObject.u4FsLsppReturnSubCode);

}

/****************************************************************************
 * Function    : LsppCliPrintHopInfoTable
 *
 * Description : This function is to fetch the HopInfo table corresponds to the
 *               given Ping Trace entry
 *
 * Input       : PingTrace Table
 *
 * Output      : None 
 *
 * Returns     : None
 ***************************************************************************/
VOID
LsppCliPrintHopInfoTable (tCliHandle CliHandle,
                          tLsppFsLsppPingTraceTableEntry * pLsppPingTraceTable)
{
    tLsppFsLsppHopTableEntry *pLsppHopInfoTableInput = NULL;
    tLsppFsLsppHopTableEntry *pLsppHopInfoTable = NULL;

    pLsppHopInfoTableInput = (tLsppFsLsppHopTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPHOPTABLE_POOLID);
    if (pLsppHopInfoTableInput == NULL)
    {
        return;
    }
    MEMSET (pLsppHopInfoTableInput, 0, sizeof (tLsppFsLsppHopTableEntry));

    /* Assign values to get the first HopInfo entry for the given 
     * Ping Trace entry
     * */
    pLsppHopInfoTableInput->MibObject.u4FsLsppContextId =
        pLsppPingTraceTable->MibObject.u4FsLsppContextId;

    pLsppHopInfoTableInput->MibObject.u4FsLsppSenderHandle =
        pLsppPingTraceTable->MibObject.u4FsLsppSenderHandle;

    pLsppHopInfoTableInput->MibObject.u4FsLsppHopIndex = 0;

    while (LSPP_FOR_EVER)
    {
        /* get the HopInfo table */
        pLsppHopInfoTable = LsppGetNextFsLsppHopTable (pLsppHopInfoTableInput);

        if (pLsppHopInfoTable == NULL)
        {
            /* No more HopInfo table present */
            MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                                (UINT1 *) pLsppHopInfoTableInput);
            return;
        }
        if ((pLsppHopInfoTable->MibObject.u4FsLsppContextId !=
             pLsppPingTraceTable->MibObject.u4FsLsppContextId) ||
            (pLsppHopInfoTable->MibObject.u4FsLsppSenderHandle !=
             pLsppPingTraceTable->MibObject.u4FsLsppSenderHandle))
        {
            /* No more Hop Info entry present in the given Ping Trace entry */
            MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                                (UINT1 *) pLsppHopInfoTableInput);
            return;
        }
        else
        {
            /* Display the Hop Info table */
            LsppCliDisplayHopInfo (CliHandle, pLsppHopInfoTable,
                                   pLsppPingTraceTable);
        }

        /* Assign the previous table's Hop Index to the input structure to 
         * get the next Hop Info table
         * */
        pLsppHopInfoTableInput->MibObject.u4FsLsppContextId =
            pLsppHopInfoTable->MibObject.u4FsLsppContextId;

        pLsppHopInfoTableInput->MibObject.u4FsLsppSenderHandle =
            pLsppHopInfoTable->MibObject.u4FsLsppSenderHandle;

        pLsppHopInfoTableInput->MibObject.u4FsLsppHopIndex =
            pLsppHopInfoTable->MibObject.u4FsLsppHopIndex;
    }
}

/****************************************************************************
 * Function    : LsppCliDisplayHopInfo
 *
 * Description : This function is to disply the HopInfo table
 *
 * Input       : HopInfo table
 *
 * Output      : None 
 *
 * Returns     : None 
****************************************************************************/
VOID
LsppCliDisplayHopInfo (tCliHandle CliHandle,
                       tLsppFsLsppHopTableEntry * pLsppHopInfoTable,
                       tLsppFsLsppPingTraceTableEntry * pLsppPingTraceTable)
{
    tUtlIn6Addr         Ip6Addr;
    UINT1               au1IpAddr[LSPP_MAX_IP_ADDR_LEN];
    UINT1               au1Icc[LSPP_MPLS_ICC_LENGTH + 1];
    UINT1               au1Umc[LSPP_MPLS_UMC_LENGTH + 1];

    MEMSET (au1IpAddr, 0, LSPP_MAX_IP_ADDR_LEN);
    MEMSET (&Ip6Addr, 0, sizeof (tUtlIn6Addr));
    MEMSET (&au1Icc, 0, (LSPP_MPLS_ICC_LENGTH + 1));
    MEMSET (&au1Umc, 0, (LSPP_MPLS_UMC_LENGTH + 1));

    CliPrintf (CliHandle, "\r\n");

    CliPrintf (CliHandle, "\r\nHop Info Table\n");

    CliPrintf (CliHandle, "\r\n");

    CliPrintf (CliHandle, "\r\nContext Id                        : %d",
               pLsppHopInfoTable->MibObject.u4FsLsppContextId);

    CliPrintf (CliHandle, "\r\nSenders Handle                    : %d",
               pLsppHopInfoTable->MibObject.u4FsLsppSenderHandle);

    CliPrintf (CliHandle, "\r\nHop Index                         : %d",
               pLsppHopInfoTable->MibObject.u4FsLsppHopIndex);

    CliPrintf (CliHandle, "\r\nRound Trip Time                   : %d",
               pLsppHopInfoTable->MibObject.u4FsLsppHopRtt);

    if (pLsppHopInfoTable->MibObject.i4FsLsppHopAddrType == LSPP_IPV4_NUMBERED)
    {
        MEMCPY (au1IpAddr, pLsppHopInfoTable->MibObject.au1FsLsppHopAddr,
                LSPP_IPV4_ADDR_LEN);

        CliPrintf (CliHandle,
                   "Hop Address                           : %d.%d.%d.%d",
                   au1IpAddr[0], au1IpAddr[1], au1IpAddr[2], au1IpAddr[3]);

        CliPrintf (CliHandle, "\r\nHop Address Type                  : IPv4");
    }
    else if (pLsppHopInfoTable->MibObject.i4FsLsppHopAddrType ==
             LSPP_IPV6_NUMBERED)
    {
        MEMCPY (&Ip6Addr, pLsppHopInfoTable->MibObject.au1FsLsppHopAddr,
                LSPP_IPV6_ADDR_LEN);
        STRNCPY (au1IpAddr, INET_NTOA6 (Ip6Addr), (sizeof (au1IpAddr) - 1));
        au1IpAddr[(sizeof (au1IpAddr) - 1)] = '\0';

        CliPrintf (CliHandle, "Hop Address                           : %s",
                   au1IpAddr);
        CliPrintf (CliHandle, "\r\nHop Address Type                  : IPv6");
    }
    else if (pLsppHopInfoTable->MibObject.i4FsLsppHopAddrType ==
             LSPP_ADDR_TYPE_NOT_APPLICABLE)
    {
        if (pLsppPingTraceTable->u1MegOperatorType == OAM_OPERATOR_TYPE_IP)
        {
            CliPrintf (CliHandle,
                       "\r\nHop Global Id                     : %d",
                       pLsppHopInfoTable->MibObject.u4FsLsppHopGlobalId);

            CliPrintf (CliHandle,
                       "\r\nHop Id                            : %d",
                       pLsppHopInfoTable->MibObject.u4FsLsppHopId);

            CliPrintf (CliHandle,
                       "\r\nHop Intf Num                      : %d",
                       pLsppHopInfoTable->MibObject.u4FsLsppHopIfNum);
        }
        else if (pLsppPingTraceTable->u1MegOperatorType ==
                 OAM_OPERATOR_TYPE_ICC)
        {
            MEMCPY (&au1Icc, pLsppHopInfoTable->MibObject.
                    au1FsLsppHopIcc, LSPP_MPLS_ICC_LENGTH);

            MEMCPY (&au1Umc, pLsppHopInfoTable->MibObject.
                    au1FsLsppHopUMC, LSPP_MPLS_UMC_LENGTH);

            CliPrintf (CliHandle,
                       "\r\nHop ICC                           : %s", au1Icc);
            CliPrintf (CliHandle,
                       "\r\nHop UMC                           : %s", au1Umc);
            CliPrintf (CliHandle,
                       "\r\nHop MepIndex                      : %d",
                       pLsppHopInfoTable->MibObject.u4FsLsppHopMepIndex);
        }

        CliPrintf (CliHandle,
                   "\r\nHop Address Type                  : Not Applicable");
    }

    if (pLsppPingTraceTable->MibObject.i4FsLsppInterfaceLabelTlv ==
        LSPP_SNMP_TRUE)
    {
        /* Address type in the Received Interface Label Stack TLV */
        MEMSET (au1IpAddr, 0, LSPP_MAX_IP_ADDR_LEN);
        MEMSET (&Ip6Addr, 0, sizeof (tUtlIn6Addr));

        if (pLsppHopInfoTable->MibObject.i4FsLsppHopRxAddrType ==
            LSPP_IPV4_NUMBERED)
        {
            MEMCPY (au1IpAddr,
                    pLsppHopInfoTable->MibObject.au1FsLsppHopRxIPAddr,
                    LSPP_IPV4_ADDR_LEN);

            CliPrintf (CliHandle,
                       "\r\nReceiver IP address               : %d.%d.%d.%d",
                       au1IpAddr[0], au1IpAddr[1], au1IpAddr[2], au1IpAddr[3]);

            MEMSET (au1IpAddr, 0, LSPP_MAX_IP_ADDR_LEN);

            MEMCPY (au1IpAddr,
                    pLsppHopInfoTable->MibObject.au1FsLsppHopRxIfAddr,
                    LSPP_IPV4_ADDR_LEN);

            CliPrintf (CliHandle,
                       "\r\nReceiver Intf Address             : %d.%d.%d.%d",
                       au1IpAddr[0], au1IpAddr[1], au1IpAddr[2], au1IpAddr[3]);

            CliPrintf (CliHandle,
                       "\r\nReceiver Address Type             : IPv4 Numbered");

        }
        else if (pLsppHopInfoTable->MibObject.i4FsLsppHopRxAddrType ==
                 LSPP_IPV4_UNNUMBERED)
        {
            CliPrintf (CliHandle, "\r\nReceiver Intf Num                 : %d",
                       pLsppHopInfoTable->MibObject.u4FsLsppHopRxIfNum);
            CliPrintf (CliHandle,
                       "\r\nReceiver Address Type             : IPv4 UnNumbered");
        }
        else if (pLsppHopInfoTable->MibObject.i4FsLsppHopRxAddrType ==
                 LSPP_IPV6_NUMBERED)
        {
            MEMCPY (&Ip6Addr, pLsppHopInfoTable->MibObject.au1FsLsppHopRxIPAddr,
                    LSPP_IPV6_ADDR_LEN);

            STRNCPY (au1IpAddr, INET_NTOA6 (Ip6Addr), (sizeof (au1IpAddr) - 1));
            au1IpAddr[(sizeof (au1IpAddr) - 1)] = '\0';

            CliPrintf (CliHandle, "\r\nReceiver IP address               : %s",
                       au1IpAddr);

            MEMSET (au1IpAddr, 0, LSPP_MAX_IP_ADDR_LEN);
            MEMSET (&Ip6Addr, 0, sizeof (tUtlIn6Addr));

            MEMCPY (&Ip6Addr, pLsppHopInfoTable->MibObject.au1FsLsppHopRxIfAddr,
                    LSPP_IPV6_ADDR_LEN);

            STRNCPY (au1IpAddr, INET_NTOA6 (Ip6Addr), (sizeof (au1IpAddr) - 1));
            au1IpAddr[(sizeof (au1IpAddr) - 1)] = '\0';

            CliPrintf (CliHandle, "\r\nReceiver Intf Address            : %s",
                       au1IpAddr);
            CliPrintf (CliHandle,
                       "\r\nReceiver Address Type            : IPv6 Numbered");
        }
        else if (pLsppHopInfoTable->MibObject.i4FsLsppHopRxAddrType ==
                 LSPP_IPV6_UNNUMBERED)
        {
            CliPrintf (CliHandle, "\r\nReceiver Intf Num                : %d",
                       pLsppHopInfoTable->MibObject.u4FsLsppHopRxIfNum);

            CliPrintf (CliHandle,
                       "\r\nReceiver Address Type            : IPv6 UnNumbered");
        }
        CliPrintf (CliHandle, "\r\nReceiver Label Stack : %s",
                   pLsppHopInfoTable->MibObject.au1FsLsppHopRxLabelStack);
    }

    /* Down stream Address type */
    MEMSET (au1IpAddr, 0, LSPP_MAX_IP_ADDR_LEN);
    MEMSET (&Ip6Addr, 0, sizeof (tUtlIn6Addr));

    CliPrintf (CliHandle, "\r\nDown Stream MTU                   : %d",
               pLsppHopInfoTable->MibObject.u4FsLsppHopDsMtu);

    if (pLsppHopInfoTable->MibObject.i4FsLsppHopDsAddrType ==
        LSPP_IPV4_NUMBERED)
    {
        MEMCPY (au1IpAddr, pLsppHopInfoTable->MibObject.au1FsLsppHopDsIPAddr,
                LSPP_IPV4_ADDR_LEN);

        CliPrintf (CliHandle,
                   "\r\nDown Stream Address Type          : IPv4 Numbered");

        CliPrintf (CliHandle,
                   "\r\nDown Stream IP address           : %d.%d.%d.%d",
                   au1IpAddr[0], au1IpAddr[1], au1IpAddr[2], au1IpAddr[3]);

        MEMSET (au1IpAddr, 0, LSPP_MAX_IP_ADDR_LEN);

        MEMCPY (au1IpAddr, pLsppHopInfoTable->MibObject.au1FsLsppHopDsIfAddr,
                LSPP_IPV4_ADDR_LEN);

        CliPrintf (CliHandle,
                   "\r\nDown Stream Intf Address          : %d.%d.%d.%d",
                   au1IpAddr[0], au1IpAddr[1], au1IpAddr[2], au1IpAddr[3]);

    }
    else if (pLsppHopInfoTable->MibObject.i4FsLsppHopDsAddrType ==
             LSPP_IPV4_UNNUMBERED)
    {
        CliPrintf (CliHandle,
                   "\r\nDown Stream Address Type          : IPv4 UnNumbered");

        CliPrintf (CliHandle, "\r\nDown Stream Intf Num             : %d",
                   pLsppHopInfoTable->MibObject.u4FsLsppHopDsIfNum);
    }
    else if (pLsppHopInfoTable->MibObject.i4FsLsppHopDsAddrType ==
             LSPP_IPV6_NUMBERED)
    {
        MEMCPY (&Ip6Addr, pLsppHopInfoTable->MibObject.au1FsLsppHopDsIPAddr,
                LSPP_IPV6_ADDR_LEN);

        STRNCPY (au1IpAddr, INET_NTOA6 (Ip6Addr), (sizeof (au1IpAddr) - 1));
        au1IpAddr[(sizeof (au1IpAddr) - 1)] = '\0';

        CliPrintf (CliHandle,
                   "\r\nDown Stream Address Type          : IPv6 Numbered");

        CliPrintf (CliHandle, "\r\nDown Stream IP address            : %s",
                   au1IpAddr);

        MEMSET (au1IpAddr, 0, LSPP_MAX_IP_ADDR_LEN);
        MEMSET (&Ip6Addr, 0, sizeof (tUtlIn6Addr));

        MEMCPY (&Ip6Addr, pLsppHopInfoTable->MibObject.au1FsLsppHopDsIfAddr,
                LSPP_IPV6_ADDR_LEN);

        STRNCPY (au1IpAddr, INET_NTOA6 (Ip6Addr), (sizeof (au1IpAddr) - 1));
        au1IpAddr[(sizeof (au1IpAddr) - 1)] = '\0';

        CliPrintf (CliHandle, "\r\nDown Stream Intf Address          : %s",
                   au1IpAddr);

    }
    else if (pLsppHopInfoTable->MibObject.i4FsLsppHopDsAddrType ==
             LSPP_IPV6_UNNUMBERED)
    {
        CliPrintf (CliHandle,
                   "\r\nDownStream Address Type          : IPv6 UnNumbered");
        CliPrintf (CliHandle, "\r\nReceiver Intf Num                 : %d",
                   pLsppHopInfoTable->MibObject.u4FsLsppHopDsIfNum);
    }
    else if (pLsppHopInfoTable->MibObject.i4FsLsppHopDsAddrType ==
             LSPP_ADDR_TYPE_NOT_APPLICABLE)
    {
        CliPrintf (CliHandle,
                   "\r\nDownStream Address Type           : Not Applicable");
    }

    CliPrintf (CliHandle, "\r\nDown Stream Label Stack           : %s",
               pLsppHopInfoTable->MibObject.au1FsLsppHopDsLabelStack);

    CliPrintf (CliHandle, "\r\nDown Stream EXP Value             : %s\n",
               pLsppHopInfoTable->MibObject.au1FsLsppHopDsLabelExp);
    return;
}

/****************************************************************************
 * Function    : LsppCliPrintGlobalStatsDetail
 *
 * Description : This function is to display the full Global Statistics details 
 *               if the summary option is not provided
 *
 * Input       : GlobalStats Table
 *
 * Output      : None 
 *
 * Returns     : None 
****************************************************************************/
VOID
LsppCliPrintGlobalStatsDetail (tCliHandle CliHandle,
                               tLsppFsLsppGlobalStatsTableEntry *
                               pLsppGlobalStatsTable)
{
    CliPrintf (CliHandle, "\r\nReturn code distribution:\n");
    CliPrintf (CliHandle, "\r\n!-Success (3) - %d",
               pLsppGlobalStatsTable->MibObject.u4FsLsppGlbStatReplyFromEgr);

    CliPrintf (CliHandle, "\r\nB-Unlabeled output interface (9) - %d",
               pLsppGlobalStatsTable->MibObject.u4FsLsppGlbStatUnLbldOutIf);

    CliPrintf (CliHandle, "\r\nD-DS map mismatch (5) - %d",
               pLsppGlobalStatsTable->MibObject.u4FsLsppGlbStatDsMapMismatch);

    CliPrintf (CliHandle, "\r\nf-Forward Error Correction (FEC) mismatch (10)"
               "- %d",
               pLsppGlobalStatsTable->MibObject.u4FsLsppGlbStatFecLblMismatch);

    CliPrintf (CliHandle, "\r\nF-No FEC mapping (4)  - %d",
               pLsppGlobalStatsTable->MibObject.u4FsLsppGlbStatNoFecMapping);

    CliPrintf (CliHandle, "\r\nI-Unknown upstream interface index (6) - %d",
               pLsppGlobalStatsTable->MibObject.u4FsLsppGlbStatUnKUpstreamIf);

    CliPrintf (CliHandle, "\r\nL-Labeled output interface (8) - %d",
               pLsppGlobalStatsTable->MibObject.u4FsLsppGlbStatReqLblSwitched);

    CliPrintf (CliHandle, "\r\nm-Unsupported TLVs (2)  - %d",
               pLsppGlobalStatsTable->MibObject.u4FsLsppGlbStatReqUnSupptdTlv);

    CliPrintf (CliHandle, "\r\nM-Malformed echo request (1)  - %d",
               pLsppGlobalStatsTable->MibObject.u4FsLsppGlbStatMalformedReq);

    CliPrintf (CliHandle, "\r\nN-No label entry (11) - %d",
               pLsppGlobalStatsTable->MibObject.u4FsLsppGlbStatNoLblEntry);

    CliPrintf (CliHandle, "\r\np-Premature termination of link-state packet"
               "(LSP) (13) - %d",
               pLsppGlobalStatsTable->MibObject.u4FsLsppGlbStatPreTermReq);

    CliPrintf (CliHandle, "\r\nP-No receive interface label protocol (12) - %d",
               pLsppGlobalStatsTable->MibObject.u4FsLsppGlbStatProtMismatch);

    CliPrintf (CliHandle, "\r\nU-Reserved (7)  - %d",
               pLsppGlobalStatsTable->MibObject.u4FsLsppGlbStatRsvdRetCode);

    CliPrintf (CliHandle, "\r\nx-No return code (0)  - %d",
               pLsppGlobalStatsTable->MibObject.u4FsLsppGlbStatNoRetCode);

    CliPrintf (CliHandle, "\r\nX-Undefined return code  - %d\n",
               pLsppGlobalStatsTable->MibObject.u4FsLsppGlbStatUndefRetCode);

}

/****************************************************************************
 * Function    : LsppCliSendEchoRqstShowOutput
 *
 * Description : This function is to send the first echo request in CLI task
 *               and make the CLI Task wait for displaying the results.
 * 
 * Input       : PingTrace Table
 *
 * Output      : None 
 *
 * Returns     : CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
LsppCliSendEchoRqstShowOutput (tCliHandle CliHandle,
                               tLsppFsLsppPingTraceTableEntry *
                               pLsppSetPingTraceTable)
{

    tLsppFsLsppGlobalConfigTableEntry LsppGlobalConfigTable;
    tLsppIsSetFsLsppPingTraceTableEntry LsppIsSetPingTraceTable;
    tLsppFsLsppEchoSequenceTableEntry LsppEchoSequenceTable;
    tLsppFsLsppHopTableEntry *pLsppHopInfoTable = NULL;
    tLsppFsLsppPingTraceTableEntry *pLsppPingTraceTable = NULL;
    tLsppFsLsppPingTraceTableEntry *pLsppPingTraceEntry = NULL;
    UINT4               u4Repeat = 0;
    UINT4               u4MaxRqst = 0;    /* Maximum number of echo requests */
    UINT4               u4SuccessRate = 0;
    UINT4               u4LastPrintedSeq = 0;
    UINT1               au1IpAddr[LSPP_MAX_IP_ADDR_LEN];

    pLsppPingTraceTable = (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppPingTraceTable == NULL)
    {
        return CLI_FAILURE;
    }
    pLsppHopInfoTable = (tLsppFsLsppHopTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPHOPTABLE_POOLID);
    if (pLsppHopInfoTable == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppPingTraceTable);
        return CLI_FAILURE;
    }

    MEMSET (pLsppPingTraceTable, 0, sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&LsppIsSetPingTraceTable, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));
    MEMSET (&LsppGlobalConfigTable, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (&LsppEchoSequenceTable, 0,
            sizeof (tLsppFsLsppEchoSequenceTableEntry));
    MEMSET (pLsppHopInfoTable, 0, sizeof (tLsppFsLsppHopTableEntry));
    MEMSET (au1IpAddr, 0, LSPP_MAX_IP_ADDR_LEN);

    pLsppPingTraceTable->MibObject.u4FsLsppContextId = pLsppSetPingTraceTable->
        MibObject.u4FsLsppContextId;

    pLsppPingTraceTable->MibObject.u4FsLsppSenderHandle =
        pLsppSetPingTraceTable->MibObject.u4FsLsppSenderHandle;

    /*Get the Ping Trace entry */
    pLsppPingTraceEntry = LsppGetFsLsppPingTraceTable (pLsppPingTraceTable);
    if (pLsppPingTraceEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppPingTraceTable);
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppHopInfoTable);
        return CLI_FAILURE;
    }

    if (LsppCoreCreateAndTriggerEcho (pLsppPingTraceEntry, 0) != OSIX_SUCCESS)
    {
	CLI_SET_ERR(CLI_LSPP_NOT_SUPPORT_ERR);
 	MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppPingTraceTable);
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppHopInfoTable);

	return CLI_FAILURE;
    }

    if (LsppGetAllFsLsppPingTraceTable (pLsppPingTraceTable) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppPingTraceTable);
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppHopInfoTable);
        return CLI_FAILURE;
    }

    /* Print the return codes and their meaning */
    if (LsppCliPrintOutputHeader (CliHandle, pLsppPingTraceTable) !=
        CLI_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppPingTraceTable);
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppHopInfoTable);
        return CLI_FAILURE;
    }
    if (pLsppPingTraceTable->MibObject.i4FsLsppReplyMode == LSPP_NO_REPLY)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppPingTraceTable);
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppHopInfoTable);
        return CLI_SUCCESS;
    }
    mmi_printf ("\r\n");

    LsppUtilGetMaxReqsToSend (pLsppPingTraceTable, &u4MaxRqst);

    for (u4Repeat = 1; u4Repeat <= u4MaxRqst; u4Repeat++)
    {
        MEMSET (pLsppHopInfoTable, 0, sizeof (tLsppFsLsppHopTableEntry));
        LSPP_UNLOCK;
        if (OsixSemTake (pLsppPingTraceTable->SemId) != OSIX_SUCCESS)
        {
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTable);
            MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                                (UINT1 *) pLsppHopInfoTable);
            CLI_SET_ERR (LSPP_CLI_SEM_TAKE_FAILED);
            LSPP_LOCK;
            return CLI_FAILURE;
        }

        LSPP_LOCK;
        if (LsppGetAllFsLsppPingTraceTable (pLsppPingTraceTable) !=
            OSIX_SUCCESS)
        {
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTable);
            MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                                (UINT1 *) pLsppHopInfoTable);
            CLI_SET_ERR (LSPP_CLI_GET_PING_TRACE_ENTRY_FAILED);
            return CLI_FAILURE;
        }

        if ((pLsppPingTraceTable->MibObject.i4FsLsppDsMap !=
             LSPP_SNMP_FALSE) &&
            (pLsppPingTraceTable->u1LastUpdatedHopIndex != 0))
        {
            pLsppHopInfoTable->MibObject.u4FsLsppContextId =
                pLsppPingTraceTable->MibObject.u4FsLsppContextId;

            pLsppHopInfoTable->MibObject.u4FsLsppSenderHandle =
                pLsppPingTraceTable->MibObject.u4FsLsppSenderHandle;

            pLsppHopInfoTable->MibObject.u4FsLsppHopIndex =
                pLsppPingTraceTable->u1LastUpdatedHopIndex;

            if (LsppGetAllFsLsppHopTable (pLsppHopInfoTable) != OSIX_SUCCESS)
            {
                MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                    (UINT1 *) pLsppPingTraceTable);
                MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                                    (UINT1 *) pLsppHopInfoTable);
                CLI_SET_ERR (LSPP_CLI_GET_HOP_ENTRY_FAILED);
                return CLI_FAILURE;
            }
        }

        /* Assign values to get the Echo sequence table to be printed */
        LsppEchoSequenceTable.MibObject.u4FsLsppContextId =
            pLsppPingTraceTable->MibObject.u4FsLsppContextId;

        LsppEchoSequenceTable.MibObject.u4FsLsppSenderHandle =
            pLsppPingTraceTable->MibObject.u4FsLsppSenderHandle;

        while (u4LastPrintedSeq < pLsppPingTraceTable->u4LastUpdatedSeqIndex)
        {
            LsppEchoSequenceTable.MibObject.u4FsLsppSequenceNumber =
                (++u4LastPrintedSeq);

            /* Get the Echo sequence table to be printed */
            if (LsppGetAllFsLsppEchoSequenceTable
                (&LsppEchoSequenceTable) != OSIX_SUCCESS)
            {
                MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                    (UINT1 *) pLsppPingTraceTable);
                MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                                    (UINT1 *) pLsppHopInfoTable);
                CLI_SET_ERR (LSPP_CLI_GET_ECHO_SEQUENCE_ENTRY_FAILED);
                return CLI_FAILURE;
            }
            if (pLsppPingTraceTable->MibObject.i4FsLsppRequestType ==
                LSPP_REQ_TYPE_PING)
            {
                LsppCliShowPingOutput (CliHandle, pLsppPingTraceTable,
                                       &LsppEchoSequenceTable,
                                       pLsppHopInfoTable);
            }
        }

        if (pLsppPingTraceTable->MibObject.i4FsLsppRequestType ==
            LSPP_REQ_TYPE_TRACE_ROUTE)
        {
            if (pLsppHopInfoTable->MibObject.i4FsLsppHopAddrType ==
                    LSPP_IPV4_NUMBERED)
            {
               if (pLsppHopInfoTable->MibObject.u4FsLsppHopIndex == 1)
               {
	            mmi_printf ("R 1 ");
                    MEMCPY (au1IpAddr, pLsppPingTraceEntry->
                    u1SrcTnlDsIPAddr, LSPP_IPV4_ADDR_LEN);
                    mmi_printf ("%d.%d.%d.%d,", au1IpAddr[0],
                        au1IpAddr[1], au1IpAddr[2], au1IpAddr[3]);
                    mmi_printf (" mtu %d", pLsppPingTraceEntry->u2SrcTnlDsMtu);

                    mmi_printf (" [Labels: %d Exp: %d] ",
                           pLsppPingTraceEntry->u4SrcTnlDsLabel,
                           pLsppPingTraceEntry->u1SrcTnlDsLabelExp);
                    mmi_printf ("\r\n");
                }
                if (pLsppHopInfoTable->MibObject.au1FsLsppHopDsIPAddr[0] != 0)
                {
                    LsppCliShowTraceRouteOutput (CliHandle, pLsppPingTraceTable,
                                                 &LsppEchoSequenceTable,
                                                 pLsppHopInfoTable);
                }
            }
            else
            {
		LsppCliShowTraceRouteOutput (CliHandle, pLsppPingTraceTable,
                                             &LsppEchoSequenceTable,
                                             pLsppHopInfoTable);
            }
        }

        /* If the ping or trace route status finished in between, then exit 
         * from the loop. In case of same-sequence number option, after 
         * receiving one reply, the status will be success and no more requests
         * will be sent
         * */
        if ((pLsppPingTraceTable->MibObject.i4FsLsppStatus ==
             LSPP_ECHO_SUCCESS) ||
            (pLsppPingTraceTable->MibObject.i4FsLsppStatus ==
             LSPP_ECHO_FAILURE))
        {
            break;
        }
    }

    /* If the request type is PING, display the packets Tx, Rx and success 
     * rate
     * */
    if (pLsppPingTraceTable->MibObject.i4FsLsppRequestType ==
        LSPP_REQ_TYPE_PING)
    {
        if (pLsppPingTraceTable->MibObject.u4FsLsppPktsTx != 0)
        {
                u4SuccessRate = (pLsppPingTraceTable->u4SuccessCount * 100) /
                    pLsppPingTraceTable->MibObject.u4FsLsppPktsTx;
        }

        CliPrintf (CliHandle, "\r\r\r\nSuccess Rate is %d percent (%d/%d), "
                   "round-trip min/avg/max = %d/%d/%d ms\n\n",
                   u4SuccessRate, pLsppPingTraceTable->u4SuccessCount,
                   pLsppPingTraceTable->MibObject.u4FsLsppPktsTx,
                   pLsppPingTraceTable->MibObject.u4FsLsppMinRtt,
                   pLsppPingTraceTable->MibObject.u4FsLsppAverageRtt,
                   pLsppPingTraceTable->MibObject.u4FsLsppMaxRtt);
    }

    LsppGlobalConfigTable.MibObject.u4FsLsppContextId =
        pLsppPingTraceTable->MibObject.u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalConfigTable (&LsppGlobalConfigTable) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppPingTraceTable);
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppHopInfoTable);
        CLI_SET_ERR (LSPP_CLI_GET_GLOBAL_CONFIG_ENTRY_FAILED);
        return CLI_FAILURE;
    }

    if (LsppGlobalConfigTable.MibObject.u4FsLsppAgeOutTime == 0)
    {
        pLsppPingTraceTable->MibObject.i4FsLsppRowStatus = DESTROY;

        LsppIsSetPingTraceTable.bFsLsppContextId = OSIX_TRUE;
        LsppIsSetPingTraceTable.bFsLsppSenderHandle = OSIX_TRUE;
        LsppIsSetPingTraceTable.bFsLsppRowStatus = OSIX_TRUE;

        if (LsppSetAllFsLsppPingTraceTable (pLsppPingTraceTable,
                                            &LsppIsSetPingTraceTable,
                                            OSIX_FALSE,
                                            OSIX_FALSE) != OSIX_SUCCESS)
        {
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTable);
            MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                                (UINT1 *) pLsppHopInfoTable);
            CLI_SET_ERR (LSPP_CLI_DESTROY_PINGTRACE_ENTRY_FAILED);
            return CLI_FAILURE;
        }
    }
    MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                        (UINT1 *) pLsppHopInfoTable);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppPingTraceTable);
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    : LsppCliPrintOutputHeader
 *
 * Description : This function is to display the summary of the request for 
 *               which path the request is sent, the packet size and the 
 *               time-out value
 *
 * Input       : PingTrace Table
 *
 * Output      : None 
 *
 * Returns     : CLI_SUCCESS?CLI_FAILURE
****************************************************************************/
INT4
LsppCliPrintOutputHeader (tCliHandle CliHandle,
                          tLsppFsLsppPingTraceTableEntry * pLsppPingTraceTable)
{
    tLsppMegMeName      MegName;
    tUtlIn6Addr         Ip6Addr;
    tUtlInAddr          IpAddr;
    CHR1               *pc1IpAddr = NULL;

    MEMSET (&MegName, 0, sizeof (tLsppMegMeName));
    MEMSET (&Ip6Addr, 0, sizeof (tUtlIn6Addr));
    MEMSET (&IpAddr, 0, sizeof (tUtlInAddr));

    /* For Ping */

    if (pLsppPingTraceTable->MibObject.i4FsLsppRequestType ==
        LSPP_REQ_TYPE_PING)
    {
        mmi_printf ("\r\nSending %d, ",
                    pLsppPingTraceTable->MibObject.u4FsLsppRepeatCount);

        /* To specify the size of packets */

        if (pLsppPingTraceTable->MibObject.i4FsLsppSweepOption ==
            LSPP_SNMP_TRUE)
        {
            mmi_printf ("[%d..%d]-byte MPLS Echos to",
                        pLsppPingTraceTable->MibObject.u4FsLsppSweepMinimum,
                        pLsppPingTraceTable->MibObject.u4FsLsppSweepMaximum);
        }
        else
        {
            mmi_printf ("%d-byte MPLS Echos to",
                        pLsppPingTraceTable->MibObject.u4FsLsppPacketSize);
        }

        /* To specify the monitoring path */

        if (pLsppPingTraceTable->MibObject.i4FsLsppPathType ==
            LSPP_PATH_TYPE_LDP_IPV4)
        {
            MEMCPY (&IpAddr, &(pLsppPingTraceTable->PathId.unPathId.LdpInfo.
                               LdpPrefix.u4_addr[0]), LSPP_IPV4_ADDR_LEN);
            pc1IpAddr = UtlInetNtoa (IpAddr);

            mmi_printf (" %s", pc1IpAddr);
        }
        else if (pLsppPingTraceTable->MibObject.i4FsLsppPathType ==
                 LSPP_PATH_TYPE_LDP_IPV6)
        {
            MEMCPY (&Ip6Addr, pLsppPingTraceTable->PathId.unPathId.LdpInfo.
                    LdpPrefix.u1_addr, LSPP_IPV6_ADDR_LEN);
            pc1IpAddr = UtlInetNtoa6 (Ip6Addr);
            mmi_printf (" %s", pc1IpAddr);
        }

        else if ((pLsppPingTraceTable->MibObject.i4FsLsppPathType ==
                  LSPP_PATH_TYPE_RSVP_IPV4) || (pLsppPingTraceTable->MibObject.
                                                i4FsLsppPathType ==
                                                LSPP_PATH_TYPE_RSVP_IPV6))
        {
            mmi_printf (" Tunnel%d,", pLsppPingTraceTable->PathId.
                        unPathId.TnlInfo.u4SrcTnlId);
        }
        else if ((pLsppPingTraceTable->MibObject.i4FsLsppPathType ==
                  LSPP_PATH_TYPE_FEC_128) || (pLsppPingTraceTable->MibObject.
                                              i4FsLsppPathType ==
                                              LSPP_PATH_TYPE_FEC_129))
        {
            MEMCPY (&IpAddr, &(pLsppPingTraceTable->PathId.unPathId.PwInfo.
                               PeerAddr.u4_addr[0]), LSPP_IPV4_ADDR_LEN);
            pc1IpAddr = UtlInetNtoa (IpAddr);

            mmi_printf (" %s", pc1IpAddr);
        }
        else if (pLsppPingTraceTable->MibObject.i4FsLsppPathType ==
                 LSPP_PATH_TYPE_MEP)
        {
            if (LsppPortGetMegMeName (pLsppPingTraceTable->MibObject.
                                      u4FsLsppContextId,
                                      &(pLsppPingTraceTable->PathId.unPathId.
                                        MepIndices), &MegName) != CLI_SUCCESS)
            {
                CLI_SET_ERR (LSPP_CLI_GET_MEG_NAME_FAILED);
                return CLI_FAILURE;
            }
            mmi_printf (" meg %s and me %s,", MegName.au1MegName,
                        MegName.au1MeName);
        }
    }

    /* For trace route */

    else
    {
        mmi_printf ("\r\nTracing MPLS");

        if (pLsppPingTraceTable->MibObject.i4FsLsppPathType ==
            LSPP_PATH_TYPE_LDP_IPV4)
        {

            MEMCPY (&IpAddr, &(pLsppPingTraceTable->PathId.unPathId.LdpInfo.
                               LdpPrefix.u4_addr[0]), LSPP_IPV4_ADDR_LEN);

            pc1IpAddr = UtlInetNtoa (IpAddr);
            mmi_printf (" Label Switched Path to %s,", pc1IpAddr);
        }
        else if (pLsppPingTraceTable->MibObject.i4FsLsppPathType ==
                 LSPP_PATH_TYPE_LDP_IPV6)
        {

            MEMCPY (&Ip6Addr, pLsppPingTraceTable->PathId.unPathId.LdpInfo.
                    LdpPrefix.u1_addr, LSPP_IPV6_ADDR_LEN);
            pc1IpAddr = UtlInetNtoa6 (Ip6Addr);

            mmi_printf (" Label Switched Path to %s,", pc1IpAddr);
        }

        else if ((pLsppPingTraceTable->MibObject.i4FsLsppPathType ==
                  LSPP_PATH_TYPE_RSVP_IPV4) || (pLsppPingTraceTable->MibObject.
                                                i4FsLsppPathType ==
                                                LSPP_PATH_TYPE_RSVP_IPV6))
        {
            mmi_printf ("-TE Label Switched Path on Tunnel%d,",
                        pLsppPingTraceTable->PathId.unPathId.TnlInfo.
                        u4SrcTnlId);
        }
        else if (pLsppPingTraceTable->MibObject.i4FsLsppPathType ==
                 LSPP_PATH_TYPE_MEP)
        {

            if (LsppPortGetMegMeName (pLsppPingTraceTable->MibObject.
                                      u4FsLsppContextId,
                                      &(pLsppPingTraceTable->PathId.
                                        unPathId.MepIndices), &MegName) !=
                CLI_SUCCESS)
            {
                CLI_SET_ERR (LSPP_CLI_GET_MEG_NAME_FAILED);
                return CLI_FAILURE;
            }
            mmi_printf ("-TP Label Switched Path on meg %s and me %s,",
                        MegName.au1MegName, MegName.au1MeName);
        }
    }

    /* To specify the time out for all the requests */
    mmi_printf (" timeout is %d ", pLsppPingTraceTable->
                MibObject.u4FsLsppWFRInterval);

    /* Timeout interval unit */
    if (pLsppPingTraceTable->MibObject.i4FsLsppWFRTmrUnit ==
        LSPP_TMR_UNIT_MILLISEC)
    {
        mmi_printf ("milliseconds\n");
    }
    else if (pLsppPingTraceTable->MibObject.i4FsLsppWFRTmrUnit ==
             LSPP_TMR_UNIT_SEC)
    {
        mmi_printf ("seconds\n");
    }
    if (pLsppPingTraceTable->MibObject.i4FsLsppWFRTmrUnit == LSPP_TMR_UNIT_MIN)
    {
        mmi_printf ("minutes\n");
    }
    if (pLsppPingTraceTable->MibObject.i4FsLsppReplyMode != LSPP_NO_REPLY)
    {
        /* Print the codes and the meaning that will be displayed in the output */
        LsppCliPrintCodes (CliHandle);
    }

    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    : LsppCliPrintCodes
 *
 * Description : This function is to print the codes and their meanings that 
 *               will be displayed during the result.
 *
 * Input       : None
 *
 * Output      : None 
 *
 * Returns     : None 
****************************************************************************/
VOID
LsppCliPrintCodes (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    mmi_printf ("\r\n Codes :\n");
    mmi_printf ("\r\n '!' - success, 'Q' - request not sent, "
                "'.' - timeout,");
    mmi_printf ("\r\n 'L' - labeled output interface, "
                "'B' - unlabeled output interface,");
    mmi_printf ("\r\n 'D' - DS Map mismatch, 'F' - no FEC mapping, "
                " m - FEC mismatch,");
    mmi_printf ("\r\n 'M' - malformed request, 'm' - unsupported"
                " tlvs, 'N' - no rx label,");
    mmi_printf ("\r\n 'P' - no rx intf label prot, "
                "'p' - premature termination of LSP,");
    mmi_printf ("\r\n 'R' - transit router, 'X' - unknown return code,"
                " 'x' - return code 0,");
    mmi_printf ("\r\n 'I' - Unknown upstream interface index, "
                "'U' - Reserved\n");
}

/****************************************************************************
 * Function    : LsppCliShowPingOutput
 * Description : This function is to display the result of the Ping
 * 
 * Input       : PingTrace Table and the EchoSequence Table

 * Output      : None 

 * Returns     : None
****************************************************************************/
VOID
LsppCliShowPingOutput (tCliHandle CliHandle,
                       tLsppFsLsppPingTraceTableEntry * pLsppPingTraceTable,
                       tLsppFsLsppEchoSequenceTableEntry *
                       pLsppEchoSequenceTable,
                       tLsppFsLsppHopTableEntry * pLsppHopInfoTable)
{
    tUtlIn6Addr         Ip6Addr;
    UINT1               au1IpAddr[LSPP_MAX_IP_ADDR_LEN];
    UINT1               au1Icc[LSPP_MPLS_ICC_LENGTH + 1];
    UINT1               au1Umc[LSPP_MPLS_UMC_LENGTH + 1];

    MEMSET (au1IpAddr, 0, LSPP_MAX_IP_ADDR_LEN);
    MEMSET (&Ip6Addr, 0, sizeof (tUtlIn6Addr));
    MEMSET (&au1Icc, 0, (LSPP_MPLS_ICC_LENGTH + 1));
    MEMSET (&au1Umc, 0, (LSPP_MPLS_UMC_LENGTH + 1));

    /* Verbose is not enabled */
    if (pLsppPingTraceTable->MibObject.i4FsLsppVerbose != LSPP_SNMP_TRUE)
    {
        if (pLsppEchoSequenceTable->u1Status == LSPP_ECHO_SEQ_COMPLETED)
        {
            if (pLsppEchoSequenceTable->MibObject.u4FsLsppReturnCode >=
                LSPP_MAX_RETURN_CODE)
            {
                CliPrintf (CliHandle, "X");
            }

            else
            {
                CliPrintf (CliHandle, "%c ",
                           gau1ReturnCode[pLsppEchoSequenceTable->MibObject.
                                          u4FsLsppReturnCode]);
            }

        }
        else if (pLsppEchoSequenceTable->u1Status == LSPP_ECHO_SEQ_TIMEDOUT)
        {
            CliPrintf (CliHandle, ". ");
        }
        else if (pLsppEchoSequenceTable->u1Status == LSPP_ECHO_SEQ_UNSENT)
        {
            CliPrintf (CliHandle, "Q ");
        }
    }

    /* If verbose is enabled */

    else if (pLsppPingTraceTable->MibObject.i4FsLsppVerbose == LSPP_SNMP_TRUE)
    {
        if (pLsppEchoSequenceTable->u1Status == LSPP_ECHO_SEQ_COMPLETED)
        {
            if (pLsppEchoSequenceTable->MibObject.u4FsLsppReturnCode >=
                LSPP_MAX_RETURN_CODE)
            {
                CliPrintf (CliHandle, "X");
            }
            else
            {

                CliPrintf (CliHandle, "\r\n%c ",
                           gau1ReturnCode[pLsppEchoSequenceTable->MibObject.
                                          u4FsLsppReturnCode]);
            }

            if (pLsppPingTraceTable->MibObject.i4FsLsppResponderAddrType ==
                LSPP_IPV4_NUMBERED)
            {
                MEMCPY (au1IpAddr, pLsppPingTraceTable->MibObject.
                        au1FsLsppResponderAddr, LSPP_IPV4_ADDR_LEN);

                CliPrintf (CliHandle, "%d.%d.%d.%d,", au1IpAddr[0],
                           au1IpAddr[1], au1IpAddr[2], au1IpAddr[3]);
            }
            else if (pLsppPingTraceTable->MibObject.i4FsLsppResponderAddrType ==
                     LSPP_IPV6_NUMBERED)
            {
                MEMCPY (&Ip6Addr, pLsppPingTraceTable->MibObject.
                        au1FsLsppResponderAddr, LSPP_IPV4_ADDR_LEN);

                STRNCPY (au1IpAddr, INET_NTOA6 (Ip6Addr),
                         (sizeof (au1IpAddr) - 1));
                au1IpAddr[(sizeof (au1IpAddr) - 1)] = '\0';
                CliPrintf (CliHandle, "%s,", au1IpAddr);
            }
            else if (pLsppPingTraceTable->MibObject.i4FsLsppResponderAddrType ==
                     LSPP_ADDR_TYPE_NOT_APPLICABLE)
            {
                if (pLsppPingTraceTable->u1MegOperatorType ==
                    OAM_OPERATOR_TYPE_IP)
                {
                    CliPrintf (CliHandle, "Global Id %d Node ID %d,",
                               pLsppPingTraceTable->MibObject.
                               u4FsLsppResponderGlobalId,
                               pLsppPingTraceTable->MibObject.
                               u4FsLsppResponderId);
                }
                else if (pLsppPingTraceTable->u1MegOperatorType ==
                         OAM_OPERATOR_TYPE_ICC)
                {

                    MEMCPY (&au1Icc, pLsppPingTraceTable->MibObject.
                            au1FsLsppResponderIcc, LSPP_MPLS_ICC_LENGTH);

                    MEMCPY (&au1Umc,
                            pLsppPingTraceTable->MibObject.
                            au1FsLsppResponderUMC, LSPP_MPLS_UMC_LENGTH);

                    CliPrintf (CliHandle, "ICC:%s UMC: %s MepIndex:%d",
                               au1Icc, au1Umc,
                               pLsppPingTraceTable->MibObject.
                               u4FsLsppResponderMepIndex);
                }

            }

            CliPrintf (CliHandle, " return code %d",
                       pLsppEchoSequenceTable->MibObject.u4FsLsppReturnCode);

            if ((pLsppPingTraceTable->MibObject.i4FsLsppInterfaceLabelTlv ==
                 LSPP_SNMP_TRUE) && (pLsppHopInfoTable != NULL))
            {
                if (pLsppHopInfoTable->MibObject.i4FsLsppHopRxAddrType ==
                    LSPP_IPV4_NUMBERED)
                {
                    MEMCPY (au1IpAddr, pLsppHopInfoTable->MibObject.
                            au1FsLsppHopRxIPAddr, LSPP_IPV4_ADDR_LEN);

                    CliPrintf (CliHandle, "\r\n\tRx-IP :"
                               "%d.%d.%d.%d", au1IpAddr[0], au1IpAddr[1],
                               au1IpAddr[2], au1IpAddr[3]);

                    MEMSET (au1IpAddr, 0, LSPP_MAX_IP_ADDR_LEN);

                    MEMCPY (au1IpAddr, pLsppHopInfoTable->MibObject.
                            au1FsLsppHopRxIfAddr, LSPP_IPV4_ADDR_LEN);

                    CliPrintf (CliHandle, "Rx-Intf :"
                               "%d.%d.%d.%d", au1IpAddr[0], au1IpAddr[1],
                               au1IpAddr[2], au1IpAddr[3]);
                }

                else if ((pLsppHopInfoTable->MibObject.i4FsLsppHopRxAddrType ==
                          LSPP_IPV4_UNNUMBERED) ||
                         (pLsppHopInfoTable->MibObject.i4FsLsppHopRxAddrType ==
                          LSPP_IPV6_UNNUMBERED))
                {
                    CliPrintf (CliHandle, "\r\n\tRx-Intf : %d",
                               pLsppHopInfoTable->MibObject.u4FsLsppHopRxIfNum);
                }
                else if (pLsppHopInfoTable->MibObject.i4FsLsppHopRxAddrType ==
                         LSPP_IPV6_NUMBERED)
                {
                    MEMCPY (&Ip6Addr, pLsppHopInfoTable->MibObject.
                            au1FsLsppHopRxIPAddr, LSPP_IPV6_ADDR_LEN);

                    STRNCPY (au1IpAddr, INET_NTOA6 (Ip6Addr),
                             (sizeof (au1IpAddr) - 1));
                    au1IpAddr[(sizeof (au1IpAddr) - 1)] = '\0';

                    CliPrintf (CliHandle, "\r\n\tRx-IP : %s", au1IpAddr);

                    MEMSET (au1IpAddr, 0, LSPP_MAX_IP_ADDR_LEN);

                    MEMCPY (&Ip6Addr, pLsppHopInfoTable->MibObject.
                            au1FsLsppHopRxIfAddr, LSPP_IPV6_ADDR_LEN);

                    STRNCPY (au1IpAddr, INET_NTOA6 (Ip6Addr),
                             (sizeof (au1IpAddr) - 1));
                    au1IpAddr[(sizeof (au1IpAddr) - 1)] = '\0';

                    CliPrintf (CliHandle, "Rx-Intf : %s", au1IpAddr);
                }
                CliPrintf (CliHandle, "Rx-Labels : %s Rx-Labels Exp : %s",
                           pLsppHopInfoTable->MibObject.
                           au1FsLsppHopRxLabelStack,
                           pLsppHopInfoTable->MibObject.au1FsLsppHopRxLabelExp);
            }
        }
        else if (pLsppEchoSequenceTable->u1Status == LSPP_ECHO_SEQ_TIMEDOUT)
        {
            mmi_printf (". - Timed out for echo request with sequence number"
                        "%d\n", pLsppEchoSequenceTable->MibObject.
                        u4FsLsppSequenceNumber);
        }
        else if (pLsppEchoSequenceTable->u1Status == LSPP_ECHO_SEQ_UNSENT)
        {
            mmi_printf ("Q - Echo Request unsent for the sequence %d\n",
                        pLsppEchoSequenceTable->MibObject.
                        u4FsLsppSequenceNumber);
        }
    }
    return;
}

/****************************************************************************
 * Function    : LsppCliShowTraceRouteOutput
 *
 * Description : This function is to display the tarce route result
 *
 * Input       : PingTrace Table, EchoSequence Table, HopInfo Table
 *
 * Output      :  None 
 *
 * Returns     :  None 
****************************************************************************/
VOID
LsppCliShowTraceRouteOutput (tCliHandle CliHandle,
                             tLsppFsLsppPingTraceTableEntry
                             * pLsppPingTraceTable,
                             tLsppFsLsppEchoSequenceTableEntry *
                             pLsppEchoSequenceTable,
                             tLsppFsLsppHopTableEntry * pLsppHopInfoTable)
{
    tUtlIn6Addr         Ip6Addr;
    UINT1               au1IpAddr[LSPP_MAX_IP_ADDR_LEN];
    UINT1               au1Icc[LSPP_MPLS_ICC_LENGTH + 1];
    UINT1               au1Umc[LSPP_MPLS_UMC_LENGTH + 1];
    UNUSED_PARAM (CliHandle);

    MEMSET (au1IpAddr, 0, LSPP_MAX_IP_ADDR_LEN);
    MEMSET (&Ip6Addr, 0, sizeof (tUtlIn6Addr));
    MEMSET (&au1Icc, 0, (LSPP_MPLS_ICC_LENGTH + 1));
    MEMSET (&au1Umc, 0, (LSPP_MPLS_UMC_LENGTH + 1));

    if (pLsppEchoSequenceTable->u1Status == LSPP_ECHO_SEQ_COMPLETED)
    {
        if (pLsppEchoSequenceTable->MibObject.u4FsLsppReturnCode >=
            LSPP_MAX_RETURN_CODE)
        {
            CliPrintf (CliHandle, "X");
        }
        else
        {
            if (pLsppHopInfoTable->MibObject.i4FsLsppHopAddrType ==
            LSPP_IPV4_NUMBERED)
            { 
                 mmi_printf ("%c %d ",
                             gau1ReturnCode[pLsppEchoSequenceTable->MibObject.
                                            u4FsLsppReturnCode],
                             pLsppHopInfoTable->MibObject.u4FsLsppHopIndex+1);
            }
            else
            {
		mmi_printf ("%c %d ",
                             gau1ReturnCode[pLsppEchoSequenceTable->MibObject.
                                            u4FsLsppReturnCode],
                             pLsppHopInfoTable->MibObject.u4FsLsppHopIndex);

            }
        }

        if (pLsppHopInfoTable->MibObject.i4FsLsppHopAddrType ==
            LSPP_IPV4_NUMBERED)
        {
              MEMCPY (au1IpAddr, pLsppHopInfoTable->MibObject.
                    au1FsLsppHopDsIPAddr, LSPP_IPV4_ADDR_LEN);
            
              mmi_printf ("%d.%d.%d.%d,", au1IpAddr[0],
                        au1IpAddr[1], au1IpAddr[2], au1IpAddr[3]);
        }
        else if (pLsppHopInfoTable->MibObject.i4FsLsppHopAddrType ==
                 LSPP_IPV6_NUMBERED)
        {
            MEMCPY (&Ip6Addr, pLsppHopInfoTable->MibObject.au1FsLsppHopAddr,
                    LSPP_IPV6_ADDR_LEN);
            STRNCPY (au1IpAddr, INET_NTOA6 (Ip6Addr), (sizeof (au1IpAddr) - 1));
            au1IpAddr[(sizeof (au1IpAddr) - 1)] = '\0';

            mmi_printf ("%s,", au1IpAddr);
        }
        else if (pLsppHopInfoTable->MibObject.i4FsLsppHopAddrType ==
                 LSPP_ADDR_TYPE_NOT_APPLICABLE)
        {

            if (pLsppPingTraceTable->u1MegOperatorType == OAM_OPERATOR_TYPE_IP)
            {
                mmi_printf ("Global-Id %d Node-Id %d",
                            pLsppHopInfoTable->MibObject.u4FsLsppHopGlobalId,
                            pLsppHopInfoTable->MibObject.u4FsLsppHopId);

                if (pLsppHopInfoTable->MibObject.u4FsLsppHopIfNum != 0)
                {
                    mmi_printf (" If-Num %d,",
                                pLsppHopInfoTable->MibObject.u4FsLsppHopIfNum);
                }
                else
                {
                    mmi_printf (",");
                }
            }
            else if (pLsppPingTraceTable->u1MegOperatorType ==
                     OAM_OPERATOR_TYPE_ICC)
            {
                MEMCPY (&au1Icc, pLsppHopInfoTable->MibObject.
                        au1FsLsppHopIcc, LSPP_MPLS_ICC_LENGTH);

                MEMCPY (&au1Umc, pLsppHopInfoTable->MibObject.
                        au1FsLsppHopUMC, LSPP_MPLS_UMC_LENGTH);

                mmi_printf ("ICC:%s UMC: %s MepIndex:%d",
                            au1Icc, au1Umc,
                            pLsppHopInfoTable->MibObject.u4FsLsppHopMepIndex);
            }
        }

        if (pLsppEchoSequenceTable->MibObject.u4FsLsppReturnCode !=
            LSPP_EGRESS_FOR_FEC_AT_STACK_DEPTH)
        {
            mmi_printf (" mtu %d", pLsppHopInfoTable->MibObject.
                        u4FsLsppHopDsMtu);

            mmi_printf (" [Labels: %s Exp: %s] ",
                        pLsppHopInfoTable->MibObject.au1FsLsppHopDsLabelStack,
                        pLsppHopInfoTable->MibObject.au1FsLsppHopDsLabelExp);
        }

        if (pLsppPingTraceTable->MibObject.i4FsLsppInterfaceLabelTlv ==
            LSPP_SNMP_TRUE)
        {

            if (pLsppHopInfoTable->MibObject.i4FsLsppHopRxAddrType ==
                LSPP_IPV4_NUMBERED)
            {
                MEMCPY (au1IpAddr, pLsppHopInfoTable->MibObject.
                        au1FsLsppHopRxIPAddr, LSPP_IPV4_ADDR_LEN);

                mmi_printf ("\r\n\tRx-IP :"
                            "%d.%d.%d.%d", au1IpAddr[0], au1IpAddr[1],
                            au1IpAddr[2], au1IpAddr[3]);

                MEMSET (au1IpAddr, 0, LSPP_MAX_IP_ADDR_LEN);

                MEMCPY (au1IpAddr, pLsppHopInfoTable->MibObject.
                        au1FsLsppHopRxIfAddr, LSPP_IPV4_ADDR_LEN);

                mmi_printf ("  Rx-Intf :"
                            "%d.%d.%d.%d", au1IpAddr[0], au1IpAddr[1],
                            au1IpAddr[2], au1IpAddr[3]);
            }

            else if ((pLsppHopInfoTable->MibObject.i4FsLsppHopRxAddrType ==
                      LSPP_IPV4_UNNUMBERED) ||
                     (pLsppHopInfoTable->MibObject.i4FsLsppHopRxAddrType ==
                      LSPP_IPV6_UNNUMBERED))
            {
                mmi_printf ("\r\n\tRx-Intf : %d",
                            pLsppHopInfoTable->MibObject.u4FsLsppHopRxIfNum);
            }
            else if (pLsppHopInfoTable->MibObject.i4FsLsppHopRxAddrType ==
                     LSPP_IPV6_NUMBERED)
            {
                MEMCPY (&Ip6Addr, pLsppHopInfoTable->MibObject.
                        au1FsLsppHopRxIPAddr, LSPP_IPV6_ADDR_LEN);

                STRNCPY (au1IpAddr, INET_NTOA6 (Ip6Addr),
                         (sizeof (au1IpAddr) - 1));
                au1IpAddr[(sizeof (au1IpAddr) - 1)] = '\0';

                mmi_printf ("\r\n\tRx-IP : %s", au1IpAddr);

                MEMSET (au1IpAddr, 0, LSPP_MAX_IP_ADDR_LEN);

                MEMCPY (&Ip6Addr, pLsppHopInfoTable->MibObject.
                        au1FsLsppHopRxIfAddr, LSPP_IPV6_ADDR_LEN);

                STRNCPY (au1IpAddr, INET_NTOA6 (Ip6Addr),
                         (sizeof (au1IpAddr) - 1));
                au1IpAddr[(sizeof (au1IpAddr) - 1)] = '\0';

                mmi_printf ("  Rx-Intf : %s", au1IpAddr);
            }
            else if (pLsppHopInfoTable->MibObject.i4FsLsppHopRxAddrType ==
                     LSPP_ADDR_TYPE_NOT_APPLICABLE)
            {
                mmi_printf ("\r\n\tRx-GlobalId : %d",
                            pLsppHopInfoTable->MibObject.u4FsLsppHopGlobalId);

                mmi_printf ("  Rx-NodeId : %d",
                            pLsppHopInfoTable->MibObject.u4FsLsppHopId);

                mmi_printf ("  Rx-Intf : %d",
                            pLsppHopInfoTable->MibObject.u4FsLsppHopRxIfNum);
            }

            mmi_printf ("  Rx-Labels : %s   Rx-Labels Exp : %s",
                        pLsppHopInfoTable->MibObject.au1FsLsppHopRxLabelStack,
                        pLsppHopInfoTable->MibObject.au1FsLsppHopRxLabelExp);
        }

        mmi_printf (" %d ms\n", pLsppHopInfoTable->MibObject.u4FsLsppHopRtt);
    }

    else if (pLsppEchoSequenceTable->u1Status == LSPP_ECHO_SEQ_TIMEDOUT)
    {
        mmi_printf (".\n");
    }

    else if (pLsppEchoSequenceTable->u1Status == LSPP_ECHO_SEQ_UNSENT)
    {
        mmi_printf ("Q \n");
    }
    return;
}

/******************************************************************************
 * Function   : LsppCliGetSendersHandle
 *
 * Description: This function is to get the Sender handle from the Index 
                manager 
 *
 * Input      : ContextName
 *
 * Output     : ContextId
 *
 * Returns    : CLI_FAILURE/CLI_SUCCESS
 *****************************************************************************/

INT4
LsppCliGetSendersHandle (UINT4 u4ContextId, UINT4 *pu4SendersHandle)
{

    tLsppFsLsppGlobalConfigTableEntry LsppFsLsppGlobalConfigTable;

    MEMSET (&LsppFsLsppGlobalConfigTable, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));

    LsppFsLsppGlobalConfigTable.MibObject.u4FsLsppContextId = u4ContextId;

    if (LsppGetAllFsLsppGlobalConfigTable (&LsppFsLsppGlobalConfigTable) !=
        OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (IndexManagerSetNextFreeIndex (LsppFsLsppGlobalConfigTable.IndexMgrId,
                                      pu4SendersHandle) != INDEX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function   : LsppCliFreeSendersHandle
 * Description: This function is to free the Sender handle from the Index 
                manager 
 * Input      : u4ContextId
 *              u4SendersHandle
 * Output     : ContextId
 * Returns    : CLI_FAILURE/CLI_SUCCESS
 *****************************************************************************/

INT4
LsppCliFreeSendersHandle (UINT4 u4ContextId, UINT4 u4SenderHandle)
{
    tLsppFsLsppGlobalConfigTableEntry LsppFsLsppGlobalConfigTable;

    MEMSET (&LsppFsLsppGlobalConfigTable, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));

    LsppFsLsppGlobalConfigTable.MibObject.u4FsLsppContextId = u4ContextId;

    if (LsppGetAllFsLsppGlobalConfigTable (&LsppFsLsppGlobalConfigTable) !=
        OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    IndexManagerFreeIndex (LsppFsLsppGlobalConfigTable.IndexMgrId,
                           u4SenderHandle);
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function   : LsppCliGetCxtIdFromName 
 *
 * Description: This function is to get the Context Id from the given
 *              Context name by calling LSP Ping Port function.
 *
 * Input      : ContextName
 *
 * Output     : ContextId
 *
 * Returns    : CLI_FAILURE/CLI_SUCCESS
 *****************************************************************************/
INT4
LsppCliGetCxtIdFromName (UINT1 *pu1ContextName, UINT4 *pu4ContextId)
{
    tLsppExtInParams   *pLsppExtInParams = NULL;
    tLsppExtOutParams  *pLsppExtOutParams = NULL;

    if ((pLsppExtInParams = (tLsppExtInParams *)
         MemAllocMemBlk (LSPP_FSLSPPINPUTPARAMS_POOLID)) == NULL)
    {
        return CLI_FAILURE;
    }
    if ((pLsppExtOutParams = (tLsppExtOutParams *)
         MemAllocMemBlk (LSPP_FSLSPPOUTPUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        return CLI_FAILURE;
    }

    MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
    MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));

    pLsppExtInParams->u4RequestType = LSPP_VCM_GET_CXT_ID_FRM_NAME;

    STRNCPY (pLsppExtInParams->uInParams.au1ContextName, pu1ContextName,
             (sizeof (pLsppExtInParams->uInParams.au1ContextName) - 1));
    pLsppExtInParams->uInParams.
        au1ContextName[(sizeof (pLsppExtInParams->uInParams.au1ContextName) -
                        1)] = '\0';

    if (LsppPortHandleExtInteraction (pLsppExtInParams, pLsppExtOutParams) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);
        return CLI_FAILURE;
    }

    *pu4ContextId = pLsppExtOutParams->unOutParams.u4ContextId;

    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtInParams);
    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtOutParams);

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function   : LsppCliGetDebugLevel 
 *
 * Description: This function is to get the old debug level configured
 *
 * Input      : ContextId 
 *
 * Output     : pi4OldDebugLevel - pointer to the old debug level fetched from
 *              the Global Config table
 *
 * Returns    : CLI_FAILURE/CLI_SUCCESS
 *****************************************************************************/
INT4
LsppCliGetDebugLevel (UINT4 u4ContextId, INT4 *pi4OldDebugLevel)
{
    tLsppFsLsppGlobalConfigTableEntry LsppFsLsppGlobalConfigTable;

    MEMSET (&LsppFsLsppGlobalConfigTable, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));

    LsppFsLsppGlobalConfigTable.MibObject.u4FsLsppContextId = u4ContextId;

    if (LsppGetAllFsLsppGlobalConfigTable (&LsppFsLsppGlobalConfigTable) !=
        OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    *pi4OldDebugLevel = LsppFsLsppGlobalConfigTable.MibObject.
        i4FsLsppTraceLevel;

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function   : LsppCliGetTrapLevel 
 *
 * Description: This function is to get the old trap level configured
 *
 * Input      : ContextId
 *
 * Output     : pi4OldTrapLevel - pointer to the old trap level fetched from
 *              the Global Config table
 *
 * Returns    : CLI_FAILURE/CLI_SUCCESS
 *****************************************************************************/

INT4
LsppCliGetTrapLevel (UINT4 u4ContextId, INT4 *pi4OldTrapLevel)
{
    tLsppFsLsppGlobalConfigTableEntry LsppFsLsppGlobalConfigTable;

    MEMSET (&LsppFsLsppGlobalConfigTable, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));

    LsppFsLsppGlobalConfigTable.MibObject.u4FsLsppContextId = u4ContextId;

    if (LsppGetAllFsLsppGlobalConfigTable (&LsppFsLsppGlobalConfigTable) !=
        OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    *pi4OldTrapLevel = LsppFsLsppGlobalConfigTable.MibObject.i4FsLsppTrapStatus;

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function   : LsppCliGetLdpIpv4FecAndMask 
 *
 * Description: This function is to extract the LDP IPv4 FEC and the 
 *              Prefix length (mask) from the string which contains
 *              ipv4 address and the mask sepearated by a "/" between them
 *
 * Input      : CliHandle 
 *              pu1String - string with IPv4 address and mask
 *
 * Output     : pLsppPathId - pointer to the structure where LDP FEC and
 *              Prefix length has to be extracted and filled
 *
 * Returns    : CLI_FAILURE/CLI_SUCCESS
 *****************************************************************************/
INT4
LsppCliGetLdpIpv4FecAndMask (tCliHandle CliHandle, UINT1 *pu1String,
                             tLsppPathId * pLsppPathId)
{
    tUtlInAddr          IpAddr;
    CHR1               *pu1Token = NULL;

    MEMSET (&IpAddr, 0, sizeof (tUtlInAddr));

    pu1Token = STRTOK (pu1String, "/");

    if (pu1Token == NULL)
    {
        CliPrintf (CliHandle, "\r\nIp address/mask invalid \n");
        return CLI_FAILURE;
    }

    if (UtlInetAton ((CONST CHR1 *) pu1Token, &IpAddr) == 0)
    {
        CliPrintf (CliHandle, "\r\nIp address invalid\n");
        return CLI_FAILURE;
    }

    pLsppPathId->unPathId.LdpInfo.LdpPrefix.u4_addr[0] = IpAddr.u4Addr;

    pu1Token = STRTOK (NULL, "/");

    if (pu1Token == NULL)
    {
        CliPrintf (CliHandle, "\r\nIp address/mask invalid \n");
        return CLI_FAILURE;
    }

    MEMCPY (&(pLsppPathId->unPathId.LdpInfo.u4PrefixLength), pu1Token,
            sizeof (UINT4));

    if (pLsppPathId->unPathId.LdpInfo.u4PrefixLength >
        CLI_LSPP_MAX_IPV4_MASK_LEN)
    {
        CliPrintf (CliHandle, "\r\nIpv4 Mask invalid\n");
        return CLI_FAILURE;
    }

    pu1Token = STRTOK (NULL, "/");

    if (pu1Token != NULL)
    {
        CliPrintf (CliHandle, "\r\nIp address and mask invalid\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function   : LsppCliGetLdpIpv6FecAndMask
 *
 * Description: This function is to extract the LDP IPv6 FEC and the 
 *              Prefix length (mask) from the string which contains
 *              ipv4 address and the mask sepearated by a "/" between them
 *
 * Input      : CliHandle 
 *              pu1String - string with IPv6 address and mask
 *
 * Output     : pLsppPathId - pointer to the structure where LDP FEC and
 *              Prefix length has to be extracted and filled
 *
 * Returns    : CLI_FAILURE/CLI_SUCCESS
 *****************************************************************************/
INT4
LsppCliGetLdpIpv6FecAndMask (tCliHandle CliHandle, UINT1 *pu1String,
                             tLsppPathId * pLsppPathId)
{
    tUtlIn6Addr         Ip6Addr;
    CHR1               *pu1Token = NULL;

    MEMSET (&Ip6Addr, 0, sizeof (tUtlIn6Addr));

    pu1Token = STRTOK (pu1String, "/");

    if (pu1Token == NULL)
    {
        CliPrintf (CliHandle, "\r\nIp address/mask invalid \n");
        return CLI_FAILURE;
    }

    if (UtlInetAton6 ((CONST CHR1 *) pu1Token, &Ip6Addr) == 0)
    {
        CliPrintf (CliHandle, "\r\nIp address invalid\n");
        return CLI_FAILURE;
    }

    MEMCPY (&(pLsppPathId->unPathId.LdpInfo.LdpPrefix.u1_addr),
            Ip6Addr.u1addr, sizeof (tUtlIn6Addr));

    pu1Token = STRTOK (NULL, "/");

    if (pu1Token == NULL)
    {
        CliPrintf (CliHandle, "\r\nIp address/mask invalid \n");
        return CLI_FAILURE;
    }

    MEMCPY (&(pLsppPathId->unPathId.LdpInfo.u4PrefixLength), pu1Token,
            sizeof (UINT4));

    if (pLsppPathId->unPathId.LdpInfo.u4PrefixLength <
        CLI_LSPP_MAX_IPV6_MASK_LEN)
    {
        CliPrintf (CliHandle, "\r\nIpv6 Mask invalid\n");
        return CLI_FAILURE;
    }

    pu1Token = STRTOK (NULL, "/");

    if (pu1Token != NULL)
    {
        CliPrintf (CliHandle, "\r\nIp address and mask invalid\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function   : LsppCliConvertStrToHex
 *
 * Description: This function is to validate whether the OID given as input 
 *              is a valid one by comparing it with the corresponding tables's
 *              Base OID
 *
 * Input      : pu1PadPattern - Pointerc with pad pattern string
 *
 * Output     : pu1HexVal - Hexa decimal value for the given pad string
 *
 * Returns    : None
 *     
 *****************************************************************************/

VOID
LsppCliConvertStrToHex (UINT1 *pu1PadPattern, UINT1 *pu1HexVal)
{
    UINT1              *pu1Temp = pu1PadPattern;
    UINT4               u4Value = 0;
    UINT1               u1Index = 0;
    UINT1               u1HexIndex = 0;
    UINT1               u1Char = 0;

    for (u1Index = 0; (*pu1Temp != '\0'); u1Index++)
    {
        u4Value = 0;
        u1HexIndex = 0;
        u1Char = 0;

        for (u1HexIndex = 0; ((*pu1Temp != '\0') &&
                              (u1HexIndex < LSPP_MAX_STR_TO_HEX_INDEX));
             u1HexIndex++)
        {
            if (ISDIGIT (*pu1Temp) == 0)
            {
                u1Char = (UINT1) (LSPP_MAX_DIGIT_VAL + ((*pu1Temp) - 'a'));
            }
            else
            {
                u1Char = *pu1Temp;
            }
            u4Value = (u4Value * LSPP_MAX_HEX_VAL) + (0x0f & u1Char);
            pu1Temp++;
        }
        if ((u1HexIndex == 1) && (*pu1Temp == '\0'))
        {
            pu1HexVal[u1Index] = (UINT1) (u4Value * LSPP_MAX_HEX_VAL);
            return;
        }
        pu1HexVal[u1Index] = (UINT1) u4Value;
    }

    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IssMplsOamEchoShowDebugging                         */
/*                                                                         */
/*     Description   : This function is used to display debug level for    */
/*                     MPLS OAM ECHO  module                               */
/*                                                                         */
/*     INPUT         : CliHandle                                           */
/*                                                                         */
/*     OUTPUT        : None                                                */
/*                                                                         */
/*     RETURNS       : None                                                */
/*                                                                         */
/***************************************************************************/

VOID
IssMplsOamEchoShowDebugging (tCliHandle CliHandle)
{
    INT4                i4DebugLevel = 0;
    UINT4               u4ContextId = CLI_LSPP_DEFAULT_CONTEXT;

    CliRegisterLock (CliHandle, LsppMainTaskLock, LsppMainTaskUnLock);
    LSPP_LOCK;

    LsppCliGetDebugLevel (u4ContextId, &i4DebugLevel);

    CliUnRegisterLock (CliHandle);
    LSPP_UNLOCK;

    if (i4DebugLevel == 0)
    {
        return;
    }

    CliPrintf (CliHandle, "\rMPLS OAM ECHO :");

    if ((i4DebugLevel & CLI_LSPP_DBG_INIT) != 0)
    {
        CliPrintf (CliHandle, "\r\n  MPLS oam echo init-shut debugging is on");
    }
    if ((i4DebugLevel & CLI_LSPP_DBG_MGMT) != 0)
    {
        CliPrintf (CliHandle, "\r\n  MPLS oam echo management debugging is on");
    }
    if ((i4DebugLevel & CLI_LSPP_DBG_DATA) != 0)
    {
        CliPrintf (CliHandle, "\r\n  MPLS oam echo data debugging is on");
    }
    if ((i4DebugLevel & CLI_LSPP_DBG_CTRL) != 0)
    {
        CliPrintf (CliHandle, "\r\n  MPLS oam echo control debugging is on");
    }
    if ((i4DebugLevel & CLI_LSPP_DBG_PKT_DUMP) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  MPLS oam echo packet dump debugging is on");
    }
    if ((i4DebugLevel & CLI_LSPP_DBG_RESOURCE) != 0)
    {
        CliPrintf (CliHandle, "\r\n  MPLS oam echo resource debugging is on");
    }
    if ((i4DebugLevel & CLI_LSPP_DBG_FAIL) != 0)
    {
        CliPrintf (CliHandle, "\r\n  MPLS oam echo all-fail debugging is on");
    }
    if ((i4DebugLevel & CLI_LSPP_DBG_BUF) != 0)
    {
        CliPrintf (CliHandle, "\r\n  MPLS oam echo buffer debugging is on");
    }
    if ((i4DebugLevel & CLI_LSPP_DBG_TLV) != 0)
    {
        CliPrintf (CliHandle, "\r\n  MPLS oam echo tlv debugging is on");
    }
    if ((i4DebugLevel & CLI_LSPP_DBG_ERROR) != 0)
    {
        CliPrintf (CliHandle, "\r\n  MPLS oam echo error debugging is on");
    }
    if ((i4DebugLevel & CLI_LSPP_DBG_EVENT) != 0)
    {
        CliPrintf (CliHandle, "\r\n  MPLS oam echo event debugging is on");
    }
    if ((i4DebugLevel & CLI_LSPP_DBG_PKT_ERROR) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  MPLS oam echo packet error debugging is on");
    }
    if ((i4DebugLevel & CLI_LSPP_DBG_PATH) != 0)
    {
        CliPrintf (CliHandle,
                   "\r\n  MPLS oam echo path discovery debugging is on");
    }
    CliPrintf (CliHandle, "\r\n");

    return;
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IssMplsOamEchoSrcGlobalConfig                       */
/*                                                                         */
/*     Description   : This function is used to display global             */
/*                     configuration for MPLS OAM ECHO  module             */
/*                                                                         */
/*     INPUT         : CliHandle                                           */
/*                                                                         */
/*     OUTPUT        : None                                                */
/*                                                                         */
/*     RETURNS       : None                                                */
/*                                                                         */
/***************************************************************************/

VOID
IssMplsOamEchoSrcGlobalConfig (tCliHandle CliHandle)
{
    tLsppFsLsppGlobalConfigTableEntry *pLsppOamEchoGlobalConfigTable = NULL;

    /* Get the First Global Config table from the data base */
    pLsppOamEchoGlobalConfigTable = LsppGetFirstFsLsppGlobalConfigTable ();

    if (pLsppOamEchoGlobalConfigTable == NULL)
    {
        return;
    }

    do
    {
        /* by default LsppSystemControl is enabled */
        if (pLsppOamEchoGlobalConfigTable->MibObject.i4FsLsppSystemControl ==
            LSPP_SHUTDOWN)
        {
            CliPrintf (CliHandle, "\r\nshutdown mpls oam echo\r\n");

            if (pLsppOamEchoGlobalConfigTable->MibObject.u4FsLsppContextId !=
                CLI_LSPP_DEFAULT_CONTEXT)
            {
                CliPrintf (CliHandle, " vrf %s\r\n",
                           pLsppOamEchoGlobalConfigTable->au1ContextName);
            }

        }

        /* by default all trap is disable */
        if (pLsppOamEchoGlobalConfigTable->MibObject.i4FsLsppTrapStatus !=
            LSPP_ALL_TRAP_DISABLED)
        {
            CliPrintf (CliHandle, "\nmpls oam echo trap");

            if (pLsppOamEchoGlobalConfigTable->MibObject.i4FsLsppTrapStatus ==
                CLI_LSPP_ENABLE_ALL_TRAP)
            {
                CliPrintf (CliHandle, " all");
            }

            if ((pLsppOamEchoGlobalConfigTable->MibObject.
                 i4FsLsppTrapStatus & LSPP_PING_COMPLETION_TRAP)
                && (pLsppOamEchoGlobalConfigTable->MibObject.
                    i4FsLsppTrapStatus != CLI_LSPP_ENABLE_ALL_TRAP))

            {
                CliPrintf (CliHandle, " ping");
            }

            if ((pLsppOamEchoGlobalConfigTable->MibObject.
                 i4FsLsppTrapStatus & LSPP_TRACE_ROUTE_COMPLETION_TRAP)
                && (pLsppOamEchoGlobalConfigTable->MibObject.
                    i4FsLsppTrapStatus != CLI_LSPP_ENABLE_ALL_TRAP))
            {
                CliPrintf (CliHandle, " traceroute");
            }

            if ((pLsppOamEchoGlobalConfigTable->MibObject.
                 i4FsLsppTrapStatus & LSPP_BFD_BTSTRAP_TRAP)
                && (pLsppOamEchoGlobalConfigTable->MibObject.
                    i4FsLsppTrapStatus != CLI_LSPP_ENABLE_ALL_TRAP))
            {
                CliPrintf (CliHandle, " bfdbootstrap");
            }

            if (pLsppOamEchoGlobalConfigTable->MibObject.u4FsLsppContextId !=
                CLI_LSPP_DEFAULT_CONTEXT)
            {
                CliPrintf (CliHandle, " vrf %s",
                           pLsppOamEchoGlobalConfigTable->au1ContextName);
            }

            CliPrintf (CliHandle, "\r\n");
        }

        if(0 == ((pLsppOamEchoGlobalConfigTable->MibObject.u4FsLsppAgeOutTime == LSPP_DEFAULT_AGEOUT_TIMER_VAL) && 
                 (pLsppOamEchoGlobalConfigTable->MibObject.i4FsLsppAgeOutTmrUnit == LSPP_TMR_UNIT_MIN)))
        {
            CliPrintf (CliHandle, "\nmpls oam echo age-out %u",
                       pLsppOamEchoGlobalConfigTable->MibObject.
                       u4FsLsppAgeOutTime);
        } 

        if (pLsppOamEchoGlobalConfigTable->MibObject.
            i4FsLsppAgeOutTmrUnit == LSPP_TMR_UNIT_MILLISEC)
        {
            CliPrintf (CliHandle, " msec");
        }
        else if (pLsppOamEchoGlobalConfigTable->MibObject.
                i4FsLsppAgeOutTmrUnit == LSPP_TMR_UNIT_SEC)
        {
            CliPrintf (CliHandle, " sec");
        }

        if (pLsppOamEchoGlobalConfigTable->MibObject.u4FsLsppContextId !=
            CLI_LSPP_DEFAULT_CONTEXT)
        {
            CliPrintf (CliHandle, " vrf %s",
                       pLsppOamEchoGlobalConfigTable->au1ContextName);
        }

        CliPrintf (CliHandle, "\r\n");
                 

        /* by default brf root reply is enabled */
        if ( 0 == ((pLsppOamEchoGlobalConfigTable->MibObject.i4FsLsppBfdBtStrapRespReq == LSPP_SNMP_TRUE) &&
                  (pLsppOamEchoGlobalConfigTable->MibObject.u4FsLsppBfdBtStrapAgeOutTime == 0)))
            
        {
            CliPrintf (CliHandle,"\nmpls oam echo require-bfd-bootstrap-reply");
            
            if (pLsppOamEchoGlobalConfigTable->MibObject.i4FsLsppBfdBtStrapRespReq == LSPP_SNMP_TRUE) 
            {
                CliPrintf (CliHandle," true");
            }
            else 
            {
                CliPrintf (CliHandle," false");
            }

            if (pLsppOamEchoGlobalConfigTable->MibObject.u4FsLsppBfdBtStrapAgeOutTime != 0)
            {
                CliPrintf (CliHandle,
                           " bfd-bootstrap-age-out %u",pLsppOamEchoGlobalConfigTable->MibObject.
                            u4FsLsppBfdBtStrapAgeOutTime);

                if (pLsppOamEchoGlobalConfigTable->MibObject.
                    i4FsLsppBfdBtStrapAgeOutTmrUnit == LSPP_TMR_UNIT_MILLISEC)
                {
                    CliPrintf (CliHandle, " msec");
                }
                else if (pLsppOamEchoGlobalConfigTable->MibObject.
                         i4FsLsppBfdBtStrapAgeOutTmrUnit == LSPP_TMR_UNIT_SEC)
                {
                    CliPrintf (CliHandle, " sec");
                }
            }

            if (pLsppOamEchoGlobalConfigTable->MibObject.u4FsLsppContextId !=
                CLI_LSPP_DEFAULT_CONTEXT)
            {
                CliPrintf (CliHandle, " vrf %s",
                           pLsppOamEchoGlobalConfigTable->au1ContextName);
            }

            CliPrintf (CliHandle, "\r\n");    
        }   
       
        /* Get the next Global Config table from the data base */
        pLsppOamEchoGlobalConfigTable =
        LsppGetNextFsLsppGlobalConfigTable (pLsppOamEchoGlobalConfigTable);

        if (pLsppOamEchoGlobalConfigTable == NULL)
        {
            /* No more Global Config table is available */
            break;
        }
    }
    while (LSPP_FOR_EVER);

    return;
}

/******************************************************************************
 *                       End of lsppcli.c file
 * ***************************************************************************/
