/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: lsppclig.c,v 1.12 2016/06/03 10:27:23 siva Exp $
*
* Description: This file contains the Lspp CLI related routines 
*********************************************************************/

#define __LSPPCLIG_C__
#include "lsppinc.h"
#include "lsppclig.h"

/****************************************************************************
 * Function    :  cli_process_Lspp_cmd
 * Description :  This function is exported to CLI module to handle the
                LSPP cli commands to take the corresponding action.

 * Input       :  Variable arguments

 * Output      :  None 

 * Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
cli_process_Lspp_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *args[CLI_LSPP_MAX_ARGS];
    INT1                argno = 0;
    UINT4               u4ErrCode = CLI_SUCCESS;
    INT4                i4RetStatus = CLI_FAILURE;
    UINT4               u4CmdType = 0;
    INT4                i4Inst;
    INT4                i4Args = 0;

    tLsppFsLsppGlobalConfigTableEntry *pLsppSetFsLsppGlobalConfigTableEntry =
        NULL;
    tLsppIsSetFsLsppGlobalConfigTableEntry
        * pLsppIsSetFsLsppGlobalConfigTableEntry = NULL;
    tLsppFsLsppPingTraceTableEntry *pLsppSetFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    UNUSED_PARAM (u4CmdType);
    CliRegisterLock (CliHandle, LsppMainTaskLock, LsppMainTaskUnLock);
    LSPP_LOCK;

    va_start (ap, u4Command);

    i4Inst = va_arg (ap, INT4);

    if (i4Inst != 0)
    {
        u4ErrCode = (UINT4) i4Inst;
    }
    MEMSET (args, 0, sizeof (args));
    while (1)
    {
        args[argno++] = va_arg (ap, UINT4 *);
        if (argno == CLI_LSPP_MAX_ARGS)
        {
            break;
        }
    }

    va_end (ap);

    switch (u4Command)
    {
        case CLI_LSPP_FSLSPPGLOBALCONFIGTABLE:

            if (args[10] != NULL)
            {
                i4Args = CLI_PTR_TO_I4 (args[10]);
                LsppCliSetDebugLevel (CliHandle, i4Args);
            }

            pLsppSetFsLsppGlobalConfigTableEntry =
                (tLsppFsLsppGlobalConfigTableEntry *)
                MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);

            if (pLsppSetFsLsppGlobalConfigTableEntry == NULL)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }
            MEMSET (pLsppSetFsLsppGlobalConfigTableEntry, 0,
                    sizeof (tLsppFsLsppGlobalConfigTableEntry));

            pLsppIsSetFsLsppGlobalConfigTableEntry =
                (tLsppIsSetFsLsppGlobalConfigTableEntry *)
                MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);

            if (pLsppIsSetFsLsppGlobalConfigTableEntry == NULL)
            {
                MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                                    (UINT1 *)
                                    pLsppSetFsLsppGlobalConfigTableEntry);

                i4RetStatus = CLI_FAILURE;
                break;
            }
            MEMSET (pLsppIsSetFsLsppGlobalConfigTableEntry, 0,
                    sizeof (tLsppIsSetFsLsppGlobalConfigTableEntry));

            LSPP_FILL_FSLSPPGLOBALCONFIGTABLE_ARGS ((pLsppSetFsLsppGlobalConfigTableEntry), (pLsppIsSetFsLsppGlobalConfigTableEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9]);

            i4RetStatus =
                LsppCliSetFsLsppGlobalConfigTable (CliHandle,
                                                   (pLsppSetFsLsppGlobalConfigTableEntry),
                                                   (pLsppIsSetFsLsppGlobalConfigTableEntry));
            MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                                (UINT1 *) pLsppSetFsLsppGlobalConfigTableEntry);
            MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                                (UINT1 *)
                                pLsppIsSetFsLsppGlobalConfigTableEntry);

            break;

        case CLI_LSPP_FSLSPPPINGTRACETABLE:

            pLsppSetFsLsppPingTraceTableEntry =
                (tLsppFsLsppPingTraceTableEntry *)
                MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

            if (pLsppSetFsLsppPingTraceTableEntry == NULL)
            {
                i4RetStatus = CLI_FAILURE;
                break;
            }
            MEMSET (pLsppSetFsLsppPingTraceTableEntry, 0,
                    sizeof (tLsppFsLsppPingTraceTableEntry));

            pLsppIsSetFsLsppPingTraceTableEntry =
                (tLsppIsSetFsLsppPingTraceTableEntry *)
                MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

            if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
            {
                MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                    (UINT1 *)
                                    pLsppSetFsLsppPingTraceTableEntry);

                i4RetStatus = CLI_FAILURE;
                break;
            }
            MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
                    sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

            LSPP_FILL_FSLSPPPINGTRACETABLE_ARGS ((pLsppSetFsLsppPingTraceTableEntry), (pLsppIsSetFsLsppPingTraceTableEntry), args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9], args[10], args[11], args[12], args[13], args[14], args[15], args[16], args[17], args[18], args[19], args[20], args[21], args[22], args[23], args[24], args[25], args[26], args[27], args[28], args[29], args[30], args[31], args[32], args[33], args[34], args[35], args[36]);

            if (LsppCliSetFsLsppPingTraceTable (CliHandle,
                                                (pLsppSetFsLsppPingTraceTableEntry),
                                                (pLsppIsSetFsLsppPingTraceTableEntry))
                != CLI_SUCCESS)
            {
                i4RetStatus = CLI_FAILURE;
                LsppCliFreeSendersHandle (pLsppSetFsLsppPingTraceTableEntry->
                                          MibObject.u4FsLsppContextId,
                                          pLsppSetFsLsppPingTraceTableEntry->
                                          MibObject.u4FsLsppSenderHandle);
                break;
            }
            i4RetStatus = LsppCliSendEchoRqstShowOutput (CliHandle,
                                                         pLsppSetFsLsppPingTraceTableEntry);

            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppSetFsLsppPingTraceTableEntry);
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);

            break;
    }
    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode > 0) && (u4ErrCode < CLI_LSPP_MAX_ERR))
        {
            CliPrintf (CliHandle, "\r%s", LsppCliErrString[u4ErrCode]);
        }
        CLI_SET_ERR (0);
    }
    CLI_SET_CMD_STATUS (i4RetStatus);

    CliUnRegisterLock (CliHandle);

    LSPP_UNLOCK;

    return i4RetStatus;

}

/****************************************************************************
* Function    :  LsppCliSetDebugLevel
* Description :
* Input       :  CliHandle, i4CliDebugLevel
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/

INT4
LsppCliSetDebugLevel (tCliHandle CliHandle, INT4 i4CliDebugLevel)
{
    LSPP_TRC_LVL = 0;

    UNUSED_PARAM (CliHandle);
    if (i4CliDebugLevel == DEBUG_DEBUG_LEVEL)
    {
        LSPP_TRC_LVL = LSPP_FN_ENTRY |
            LSPP_FN_EXIT |
            LSPP_CLI_TRC |
            LSPP_MAIN_TRC |
            LSPP_PKT_TRC |
            LSPP_QUE_TRC | LSPP_TASK_TRC | LSPP_TMR_TRC | LSPP_UTIL_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_INFO_LEVEL)
    {
        LSPP_TRC_LVL =
            LSPP_FN_ENTRY | LSPP_FN_EXIT | LSPP_CLI_TRC | LSPP_MAIN_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_NOTICE_LEVEL)
    {
        LSPP_TRC_LVL = LSPP_UTIL_TRC | LSPP_FN_ENTRY | LSPP_FN_EXIT;
    }
    else if (i4CliDebugLevel == DEBUG_WARN_LEVEL)
    {
        LSPP_TRC_LVL = LSPP_UTIL_TRC | LSPP_FN_ENTRY | LSPP_FN_EXIT;
    }
    else if (i4CliDebugLevel == DEBUG_ERROR_LEVEL)
    {
        LSPP_TRC_LVL = LSPP_MAIN_TRC |
            LSPP_PKT_TRC | LSPP_QUE_TRC | LSPP_TASK_TRC | LSPP_TMR_TRC;
    }
    else if ((i4CliDebugLevel == DEBUG_CRITICAL_LEVEL)
             || (i4CliDebugLevel == DEBUG_ALERT_LEVEL)
             || (i4CliDebugLevel == DEBUG_EMERG_LEVEL))
    {
        LSPP_TRC_LVL = LSPP_MAIN_TRC |
            LSPP_PKT_TRC |
            LSPP_QUE_TRC | LSPP_TASK_TRC | LSPP_TMR_TRC | LSPP_UTIL_TRC;
    }
    else if (i4CliDebugLevel == DEBUG_DEF_LVL_FLAG)
    {
        return CLI_SUCCESS;
    }
    else
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  LsppCliSetFsLsppGlobalConfigTable
* Description :
* Input       :  CliHandle 
*            pLsppSetFsLsppGlobalConfigTable
*            pLsppIsSetFsLsppGlobalConfigTable
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
LsppCliSetFsLsppGlobalConfigTable (tCliHandle CliHandle,
                                   tLsppFsLsppGlobalConfigTableEntry *
                                   pLsppSetFsLsppGlobalConfigTableEntry,
                                   tLsppIsSetFsLsppGlobalConfigTableEntry *
                                   pLsppIsSetFsLsppGlobalConfigTableEntry)
{
    UINT4               u4ErrorCode;

    if (LsppTestAllFsLsppGlobalConfigTable
        (&u4ErrorCode, pLsppSetFsLsppGlobalConfigTableEntry,
         pLsppIsSetFsLsppGlobalConfigTableEntry) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }

    if (LsppSetAllFsLsppGlobalConfigTable
        (pLsppSetFsLsppGlobalConfigTableEntry,
         pLsppIsSetFsLsppGlobalConfigTableEntry) != OSIX_SUCCESS)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/****************************************************************************
* Function    :  LsppCliSetFsLsppPingTraceTable
* Description :
* Input       :  CliHandle 
*            pLsppSetFsLsppPingTraceTable
*            pLsppIsSetFsLsppPingTraceTable
* Output      :  None 
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
LsppCliSetFsLsppPingTraceTable (tCliHandle CliHandle,
                                tLsppFsLsppPingTraceTableEntry *
                                pLsppSetFsLsppPingTraceTableEntry,
                                tLsppIsSetFsLsppPingTraceTableEntry *
                                pLsppIsSetFsLsppPingTraceTableEntry)
{
    UINT4               u4ErrorCode;
	UNUSED_PARAM(CliHandle);
    if (LsppTestAllFsLsppPingTraceTable
        (&u4ErrorCode, pLsppSetFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_TRUE,
         OSIX_TRUE) != OSIX_SUCCESS)
    {
        return CLI_FAILURE;
    }
    
    u4ErrorCode = (UINT4)LsppSetAllFsLsppPingTraceTable
        (pLsppSetFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_TRUE, OSIX_TRUE);
    if(u4ErrorCode != OSIX_SUCCESS) 
    {
        CLI_SET_ERR (u4ErrorCode);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}
