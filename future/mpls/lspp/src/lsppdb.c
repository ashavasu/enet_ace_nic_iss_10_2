/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: lsppdb.c,v 1.23 2016/02/25 08:12:59 siva Exp $
*
* Description: This file contains the routines for the protocol Database Access for the module Lspp 
*********************************************************************/

#include "lsppinc.h"
#include "lsppclig.h"
#include "lsppcli.h"

/****************************************************************************
 Function    :  LsppTestAllFsLsppGlobalConfigTable
 Input       :  pu4ErrorCode
                pLsppSetFsLsppGlobalConfigTableEntry
                pLsppIsSetFsLsppGlobalConfigTableEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
LsppTestAllFsLsppGlobalConfigTable (UINT4 *pu4ErrorCode,
                                    tLsppFsLsppGlobalConfigTableEntry *
                                    pLsppSetFsLsppGlobalConfigTableEntry,
                                    tLsppIsSetFsLsppGlobalConfigTableEntry *
                                    pLsppIsSetFsLsppGlobalConfigTableEntry)
{
    if (LsppDbGlobalConfigInitialTest
        (pLsppSetFsLsppGlobalConfigTableEntry,
         pLsppIsSetFsLsppGlobalConfigTableEntry) != OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return OSIX_FAILURE;
    }

    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppSystemControl !=
        OSIX_FALSE)
    {
        if ((pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
             i4FsLsppSystemControl != LSPP_SHUTDOWN) &&
            (pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
             i4FsLsppSystemControl != LSPP_START))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }

    }

    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppTrapStatus != OSIX_FALSE)
    {
        if ((pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
             i4FsLsppTrapStatus < LSPP_ALL_TRAP_DISABLED) ||
            (pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
             i4FsLsppTrapStatus > LSPP_ALL_TRAP_ENABLED))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }

    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppTraceLevel != OSIX_FALSE)
    {
        if ((pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
             i4FsLsppTraceLevel < LSPP_ALL_TRACES_DISABLED) ||
            (pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
             i4FsLsppTraceLevel > LSPP_ALL_TRACES_ENABLED))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }

    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppAgeOutTime != OSIX_FALSE)
    {
        if ((pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
             u4FsLsppAgeOutTime > LSPP_MAX_AGE_OUT_TIME))
        {
            CLI_SET_ERR (CLI_LSPP_INVALID_MAX_AGE_OUT_TIME);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }

    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppAgeOutTmrUnit !=
        OSIX_FALSE)
    {
        if ((pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
             i4FsLsppAgeOutTmrUnit < LSPP_TMR_UNIT_MILLISEC) ||
            (pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
             i4FsLsppAgeOutTmrUnit > LSPP_TMR_UNIT_MIN))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }

    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppClearEchoStats !=
        OSIX_FALSE)
    {
        if ((pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
             i4FsLsppClearEchoStats != LSPP_SNMP_FALSE) &&
            (pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
             i4FsLsppClearEchoStats != LSPP_SNMP_TRUE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }

    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppBfdBtStrapRespReq !=
        OSIX_FALSE)
    {
        if ((pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
             i4FsLsppBfdBtStrapRespReq != LSPP_SNMP_FALSE) &&
            (pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
             i4FsLsppBfdBtStrapRespReq != LSPP_SNMP_TRUE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }

    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppBfdBtStrapAgeOutTime !=
        OSIX_FALSE)
    {
        if ((pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
             u4FsLsppBfdBtStrapAgeOutTime > LSPP_MAX_AGE_OUT_TIME))
        {
            CLI_SET_ERR (CLI_LSPP_INVALID_BFD_AGEOUT_VALUE);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }

    if (pLsppIsSetFsLsppGlobalConfigTableEntry->
        bFsLsppBfdBtStrapAgeOutTmrUnit != OSIX_FALSE)
    {
        if ((pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
             i4FsLsppBfdBtStrapAgeOutTmrUnit < LSPP_TMR_UNIT_MILLISEC) ||
            (pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
             i4FsLsppBfdBtStrapAgeOutTmrUnit > LSPP_TMR_UNIT_MIN))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  LsppTestAllFsLsppPingTraceTable
 Input       :  pu4ErrorCode
                pLsppSetFsLsppPingTraceTableEntry
                pLsppIsSetFsLsppPingTraceTableEntry
 Description :  This Routine Take the Indices &
                Test the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
LsppTestAllFsLsppPingTraceTable (UINT4 *pu4ErrorCode,
                                 tLsppFsLsppPingTraceTableEntry *
                                 pLsppSetFsLsppPingTraceTableEntry,
                                 tLsppIsSetFsLsppPingTraceTableEntry *
                                 pLsppIsSetFsLsppPingTraceTableEntry,
                                 INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tLsppFsLsppPingTraceTableEntry *pLsppPingTraceTableEntry = NULL;
    tLsppExtOutParams   LsppExtOutParams;

    UNUSED_PARAM (i4RowStatusLogic);
    pLsppPingTraceTableEntry = (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppPingTraceTableEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (&(LsppExtOutParams), 0, sizeof (tLsppExtOutParams));

    if ((pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRowStatus != OSIX_FALSE)
        && (pLsppSetFsLsppPingTraceTableEntry->MibObject.
            i4FsLsppRowStatus == ACTIVE))
    {
        pLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId;

        pLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle;

        if (LsppGetAllFsLsppPingTraceTable (pLsppPingTraceTableEntry) !=
            OSIX_SUCCESS)
        {
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }

        if (pLsppPingTraceTableEntry->MibObject.i4FsLsppRowStatus == ACTIVE)
        {
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTableEntry);
            return OSIX_SUCCESS;
        }
    }

    if (LsppDbPingTraceInitialTest (pLsppSetFsLsppPingTraceTableEntry,
                                    pLsppIsSetFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppPingTraceTableEntry);
        return OSIX_FAILURE;
    }

    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRequestType != OSIX_FALSE)
    {
        if ((pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppRequestType != LSPP_REQ_TYPE_PING) &&
            (pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppRequestType != LSPP_REQ_TYPE_TRACE_ROUTE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }
    }

    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPathType != OSIX_FALSE)
    {
        if ((pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppPathType < LSPP_PATH_TYPE_LDP_IPV4) ||
            (pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppPathType > LSPP_PATH_TYPE_MEP))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }
    }

    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPathPointer != OSIX_FALSE)
    {
        if (LsppUtilTestBaseOid (pLsppSetFsLsppPingTraceTableEntry,
                                 i4RowCreateOption) != OSIX_SUCCESS)
        {
            CLI_SET_ERR (CLI_LSPP_INVALID_BASE_OID);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }
    }

    if ((pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTgtMipNodeId != OSIX_FALSE)
        && (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTgtMipGlobalId !=
            OSIX_FALSE))
    {
        if ((pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppPathType) == LSPP_PATH_TYPE_MEP)
        {
            if (LsppUtilGetGlobalNodeId (pLsppSetFsLsppPingTraceTableEntry,
                                         &LsppExtOutParams) == OSIX_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                    (UINT1 *) pLsppPingTraceTableEntry);
                return OSIX_FAILURE;
            }
            if ((LsppExtOutParams.OutMegDetail.unServiceInfo.ServiceTnlId.
                 SrcNodeId.MplsGlobalNodeId.u4GlobalId !=
                 pLsppSetFsLsppPingTraceTableEntry->MibObject.
                 u4FsLsppTgtMipGlobalId)
                || (pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    u4FsLsppTgtMipGlobalId == 0))
            {
                CLI_SET_ERR (CLI_LSPP_INVALID_MIP_GLOBAL_ID);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                    (UINT1 *) pLsppPingTraceTableEntry);
                return OSIX_FAILURE;
            }
            if ((pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    u4FsLsppTgtMipNodeId) == 0)
            {
		
                CLI_SET_ERR (CLI_LSPP_INVALID_MIP_NODE_ID);
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                    (UINT1 *) pLsppPingTraceTableEntry);
                return OSIX_FAILURE;
            }

        }
    }

    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReplyMode != OSIX_FALSE)
    {
        if ((pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppReplyMode < LSPP_NO_REPLY) ||
            (pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppReplyMode > LSPP_REPLY_APPLICATION_CC))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }
    }

    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRepeatCount != OSIX_FALSE)
    {
        if ((pLsppSetFsLsppPingTraceTableEntry->MibObject.
             u4FsLsppRepeatCount < LSPP_MIN_REPEAT_COUNT) ||
            (pLsppSetFsLsppPingTraceTableEntry->MibObject.
             u4FsLsppRepeatCount > LSPP_MAX_REPEAT_COUNT))
        {
            CLI_SET_ERR (CLI_LSPP_INVALID_REPEAT_COUNT);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }
    }

    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPacketSize != OSIX_FALSE)
    {
        if ((pLsppSetFsLsppPingTraceTableEntry->MibObject.
             u4FsLsppPacketSize < LSPP_MIN_PACKET_SIZE) ||
            (pLsppSetFsLsppPingTraceTableEntry->MibObject.
             u4FsLsppPacketSize > LSPP_MAX_PACKET_SIZE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }

        /*if (i4RowCreateOption == OSIX_FALSE)
           {
           pLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
           pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId;

           pLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
           pLsppSetFsLsppPingTraceTableEntry->MibObject.
           u4FsLsppSenderHandle;

           LsppGetAllFsLsppPingTraceTable (pLsppPingTraceTableEntry);

           if (pLsppPingTraceTableEntry->MibObject.i4FsLsppSweepOption ==
           LSPP_SNMP_TRUE)
           {
           *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
           return OSIX_FAILURE;
           }
           } */
    }

    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTTLValue != OSIX_FALSE)
    {
        if ((pLsppSetFsLsppPingTraceTableEntry->MibObject.
             u4FsLsppTTLValue < LSPP_MIN_TTL) ||
            (pLsppSetFsLsppPingTraceTableEntry->MibObject.
             u4FsLsppTTLValue > LSPP_MAX_TTL))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }
    }

    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWFRInterval != OSIX_FALSE)
    {
        if ((pLsppSetFsLsppPingTraceTableEntry->MibObject.
             u4FsLsppWFRInterval < LSPP_MIN_WFR_TIME) ||
            (pLsppSetFsLsppPingTraceTableEntry->MibObject.
             u4FsLsppWFRInterval > LSPP_MAX_WFR_TIME))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWFRTmrUnit != OSIX_FALSE)
    {
        if ((pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppWFRTmrUnit < LSPP_TMR_UNIT_MILLISEC) ||
            (pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppWFRTmrUnit > LSPP_TMR_UNIT_MIN))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWTSInterval != OSIX_FALSE)
    {
        if (pLsppSetFsLsppPingTraceTableEntry->MibObject.
            u4FsLsppWTSInterval > LSPP_MAX_WTS_TIME)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }

        if (i4RowCreateOption == OSIX_FALSE)
        {
            pLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
                pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId;

            pLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
                pLsppSetFsLsppPingTraceTableEntry->MibObject.
                u4FsLsppSenderHandle;

            if (LsppGetAllFsLsppPingTraceTable (pLsppPingTraceTableEntry) !=
                OSIX_SUCCESS)
            {
                MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                    (UINT1 *) pLsppPingTraceTableEntry);
                return OSIX_FAILURE;
            }

            if (pLsppPingTraceTableEntry->MibObject.i4FsLsppRequestType ==
                LSPP_REQ_TYPE_TRACE_ROUTE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                    (UINT1 *) pLsppPingTraceTableEntry);
                return OSIX_FAILURE;
            }
        }
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWTSTmrUnit != OSIX_FALSE)
    {
        if ((pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppWTSTmrUnit < LSPP_TMR_UNIT_MILLISEC) ||
            (pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppWTSTmrUnit > LSPP_TMR_UNIT_MIN))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }
    }

    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReplyDscpValue !=
        OSIX_FALSE)
    {
        if (pLsppSetFsLsppPingTraceTableEntry->MibObject.
            u4FsLsppReplyDscpValue > LSPP_MAX_DSCP_VALUE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }
    }

    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPadPattern != OSIX_FALSE)
    {
        if ((pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppPadPatternLen <= 0) ||
            (pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppPadPatternLen > LSPP_MAX_PAD_PATTERN_LEN))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }
    }

    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepOption != OSIX_FALSE)
    {
        if ((pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppSweepOption != LSPP_SNMP_FALSE) &&
            (pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppSweepOption != LSPP_SNMP_TRUE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }
    }

    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepMinimum != OSIX_FALSE)
    {
        if ((pLsppSetFsLsppPingTraceTableEntry->MibObject.
             u4FsLsppSweepMinimum < LSPP_MIN_SWEEP_START_VALUE) ||
            (pLsppSetFsLsppPingTraceTableEntry->MibObject.
             u4FsLsppSweepMinimum > LSPP_MAX_SWEEP_START_VALUE))
        {
            CLI_SET_ERR (CLI_LSPP_INVALID_SWEEP_START_VALUE);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }
    }

    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepMaximum != OSIX_FALSE)
    {
        if ((pLsppSetFsLsppPingTraceTableEntry->MibObject.
             u4FsLsppSweepMaximum < LSPP_MIN_SWEEP_END_VALUE) ||
            (pLsppSetFsLsppPingTraceTableEntry->MibObject.
             u4FsLsppSweepMaximum > LSPP_MAX_SWEEP_END_VALUE))
        {
            CLI_SET_ERR (CLI_LSPP_INVALID_SWEEP_END_VALUE);
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }
    }

    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepIncrement !=
        OSIX_FALSE)
    {
        if ((pLsppSetFsLsppPingTraceTableEntry->MibObject.
             u4FsLsppSweepIncrement < LSPP_MIN_SWEEP_INCREMENT_VALUE) ||
            (pLsppSetFsLsppPingTraceTableEntry->MibObject.
             u4FsLsppSweepIncrement > LSPP_MAX_SWEEP_INCREMENT_VALUE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppBurstOption != OSIX_FALSE)
    {
        if ((pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppBurstOption != LSPP_SNMP_FALSE) &&
            (pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppBurstOption != LSPP_SNMP_TRUE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }
    }

    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppBurstSize != OSIX_FALSE)
    {
        if ((pLsppSetFsLsppPingTraceTableEntry->MibObject.
             u4FsLsppBurstSize < LSPP_MIN_BURST_SIZE) ||
            (pLsppSetFsLsppPingTraceTableEntry->MibObject.
             u4FsLsppBurstSize > LSPP_MAX_BURST_SIZE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }
    }

    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppEXPValue != OSIX_FALSE)
    {
        if (pLsppSetFsLsppPingTraceTableEntry->MibObject.
            u4FsLsppEXPValue > LSPP_MAX_EXP_VALUE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }
    }

    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppDsMap != OSIX_FALSE)
    {
        if ((pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppDsMap != LSPP_SNMP_FALSE) &&
            (pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppDsMap != LSPP_SNMP_TRUE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }
    }

    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppFecValidate != OSIX_FALSE)
    {
        if ((pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppFecValidate != LSPP_SNMP_FALSE) &&
            (pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppFecValidate != LSPP_SNMP_TRUE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }
    }

    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReplyPadTlv != OSIX_FALSE)
    {
        if ((pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppReplyPadTlv != LSPP_SNMP_FALSE) &&
            (pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppReplyPadTlv != LSPP_SNMP_TRUE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }
    }

    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppForceExplicitNull !=
        OSIX_FALSE)
    {
        if ((pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppForceExplicitNull != LSPP_SNMP_FALSE) &&
            (pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppForceExplicitNull != LSPP_SNMP_TRUE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }
    }

    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppInterfaceLabelTlv !=
        OSIX_FALSE)
    {
        if ((pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppInterfaceLabelTlv != LSPP_SNMP_FALSE) &&
            (pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppInterfaceLabelTlv != LSPP_SNMP_TRUE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }
    }

    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSameSeqNumOption !=
        OSIX_FALSE)
    {
        if ((pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppSameSeqNumOption != LSPP_SNMP_FALSE) &&
            (pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppSameSeqNumOption != LSPP_SNMP_TRUE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }
    }

    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppVerbose != OSIX_FALSE)
    {
        if ((pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppVerbose != LSPP_SNMP_FALSE) &&
            (pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppVerbose != LSPP_SNMP_TRUE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }
    }

    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReversePathVerify !=
        OSIX_FALSE)
    {
        if ((pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppReversePathVerify != LSPP_SNMP_FALSE) &&
            (pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppReversePathVerify != LSPP_SNMP_TRUE))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }
    }

    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppEncapType != OSIX_FALSE)
    {
        if ((pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppEncapType < LSPP_ENCAP_TYPE_IP) ||
            (pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppEncapType > LSPP_ENCAP_TYPE_VCCV_NEGOTIATED))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }
    }

    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRowStatus != OSIX_FALSE)
    {
        if ((pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppRowStatus < ACTIVE) ||
            (pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppRowStatus > DESTROY))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }

        /* An Lsp Ping Entry Created by Bfd Cannot be deleted by management */
        if (pLsppSetFsLsppPingTraceTableEntry->MibObject.
            i4FsLsppRowStatus == DESTROY)
        {
            pLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
                pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId;

            pLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
                pLsppSetFsLsppPingTraceTableEntry->MibObject.
                u4FsLsppSenderHandle;

            if (LsppGetAllFsLsppPingTraceTable (pLsppPingTraceTableEntry) !=
                OSIX_SUCCESS)
            {
                MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                    (UINT1 *) pLsppPingTraceTableEntry);
                return OSIX_FAILURE;
            }

            if (pLsppPingTraceTableEntry->MibObject.i4FsLsppRequestOwner ==
                LSPP_REQ_OWNER_BFD)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                    (UINT1 *) pLsppPingTraceTableEntry);
                return OSIX_FAILURE;
            }
        }
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppPingTraceTableEntry);

    if (LsppDbPingTraceDependencyTest (pLsppSetFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : LsppDbPingTraceInitialTest
 *
 * Description: This function is to perform initial checkings and validations 
 *              when an new PingTrace entry is created.
 *
 * Input      : pLsppSetFsLsppPingTraceTableEntry - PingTrace structure with 
 *              values that is going to be set.
 *
 *              pLsppIsSetPingTraceTable - A boolean structure informs about 
 *              each object, wheteher it is going to be set or not.
 * Output     : None
 * Returns    : OSIX_SUCCESS/OSIX_FAILURE
 *     
 *****************************************************************************/
INT4
LsppDbPingTraceInitialTest (tLsppFsLsppPingTraceTableEntry *
                            pLsppSetFsLsppPingTraceTableEntry,
                            tLsppIsSetFsLsppPingTraceTableEntry *
                            pLsppIsSetFsLsppPingTraceTableEntry)
{
    tLsppFsLsppGlobalConfigTableEntry LsppFsLsppGlobalConfigTableEntry;
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry = (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (&LsppFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));

    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle;

    /* Check for the presence of ping trace entry */

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        /* An entry should be present already if any of the object is going to 
         * be configured expect Row status CREATE_AND_GO and CREATE_AND_WAIT
         */
        if ((pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppRowStatus != CREATE_AND_WAIT) &&
            (pLsppSetFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppRowStatus != CREATE_AND_GO))
        {
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppFsLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }
    }

    /* If the entry is present and its ROWSTATUS is ACTIVE,
     * then changing the row status object should not be allowed
     * except for the value DESTROY
     * */
    else if ((pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRowStatus !=
              OSIX_FALSE) &&
             (pLsppFsLsppPingTraceTableEntry->MibObject.
              i4FsLsppRowStatus == ACTIVE) &&
             (pLsppSetFsLsppPingTraceTableEntry->MibObject.
              i4FsLsppRowStatus != DESTROY))
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return OSIX_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);

    LsppFsLsppGlobalConfigTableEntry.MibObject.u4FsLsppContextId =
        pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalConfigTable (&LsppFsLsppGlobalConfigTableEntry) !=
        OSIX_SUCCESS)
    {
        CLI_SET_ERR (CLI_LSPP_CONTEXT_NOT_EXIST);
        return OSIX_FAILURE;
    }

    if (LsppFsLsppGlobalConfigTableEntry.MibObject.i4FsLsppSystemControl ==
        LSPP_SHUTDOWN)
    {
        CLI_SET_ERR (CLI_LSPP_MODULE_SHUTDOWN);
        return OSIX_FAILURE;
    }

    if (pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppRowStatus ==
        CREATE_AND_WAIT)
    {
        /* Check whether the given SendersHandle (second index of 
         * PingTracetable) is not present already */
        if (IndexManagerCheckIndexIsFree (LsppFsLsppGlobalConfigTableEntry.
                                          IndexMgrId,
                                          pLsppSetFsLsppPingTraceTableEntry->
                                          MibObject.u4FsLsppSenderHandle) !=
            OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
        if (IndexManagerSetIndex (LsppFsLsppGlobalConfigTableEntry.IndexMgrId,
                                  pLsppSetFsLsppPingTraceTableEntry->MibObject.
                                  u4FsLsppSenderHandle) != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
    }

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : LsppDbPingTraceDependencyTest
 *
 * Description: This utility function is to check the dependencies between the 
 *              inputs in the newly created PingTrace entry.
 *
 * Input      : pLsppSetPingTraceTableEntry - PingTrace structure with 
 *              values that is going to be set.
 *
 * Output     : None
 * Returns    : OSIX_FAILURE/OSIX_SUCCESS
 *     
 *****************************************************************************/
INT4
LsppDbPingTraceDependencyTest (tLsppFsLsppPingTraceTableEntry *
                               pLsppSetPingTraceTableEntry)
{
    tLsppFsLsppPingTraceTableEntry *pLsppSetPingTraceTableEntryInput = NULL;

    pLsppSetPingTraceTableEntryInput = (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppSetPingTraceTableEntryInput == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (pLsppSetPingTraceTableEntryInput, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* When the ROW STATUS is going to be set as ACTIVE from SNMP,
     * get the Ping Trace Entry from the data base for dependency checking 
     * */
    if (pLsppSetPingTraceTableEntry->MibObject.i4FsLsppRowStatus == ACTIVE)
    {
        pLsppSetPingTraceTableEntryInput->MibObject.u4FsLsppContextId =
            pLsppSetPingTraceTableEntry->MibObject.u4FsLsppContextId;

        pLsppSetPingTraceTableEntryInput->MibObject.u4FsLsppSenderHandle =
            pLsppSetPingTraceTableEntry->MibObject.u4FsLsppSenderHandle;

        if (LsppGetAllFsLsppPingTraceTable (pLsppSetPingTraceTableEntryInput) !=
            OSIX_SUCCESS)
        {
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppSetPingTraceTableEntryInput);
            return OSIX_FAILURE;
        }

        /* Call the function to check the inter-dependencies between the 
         * inputs
         * */
        if (LsppDbPingTraceCheckDependency (pLsppSetPingTraceTableEntryInput) !=
            OSIX_SUCCESS)
        {
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppSetPingTraceTableEntryInput);
            return OSIX_FAILURE;
        }
    }

    /* When the ping is initiated from CLI, check the dependencies between the
     * input parameters in the passed input structure
     * */
    else if (pLsppSetPingTraceTableEntry->MibObject.i4FsLsppRowStatus ==
             CREATE_AND_GO)
    {
        /* Check the inter-dependencies between the 
         * inputs
         * */
        if (LsppDbPingTraceCheckDependency (pLsppSetPingTraceTableEntry) !=
            OSIX_SUCCESS)
        {
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppSetPingTraceTableEntryInput);
            return OSIX_FAILURE;
        }
    }
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppSetPingTraceTableEntryInput);
    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : LsppDbGlobalConfigInitialTest
 * 
 * Description: This function is to perform initial checkings when an object is
 *              going to be set in the Global Conrfig Table
 *               
 * Input      : pLsppSetGlobalConfigTable - GlobalConfig structure with values 
 *              that is going to be set.
 *
 * Output     : None
 * Returns    : OSIX_SUCCESS/OSIX_FAILURE
 *     
 *****************************************************************************/
INT4
LsppDbGlobalConfigInitialTest (tLsppFsLsppGlobalConfigTableEntry
                               * pLsppSetFsLsppGlobalConfigTableEntry,
                               tLsppIsSetFsLsppGlobalConfigTableEntry
                               * pLsppIsSetFsLsppGlobalConfigTableEntry)
{
    tLsppFsLsppGlobalConfigTableEntry LsppGlobalConfigTableEntry;

    MEMSET (&LsppGlobalConfigTableEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    /* When an object other than SYSTEM CONTROL - START is going to be 
     * configured for a particular context, an entry must be present already
     * */
    LsppGlobalConfigTableEntry.MibObject.u4FsLsppContextId =
        pLsppSetFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId;

    /* Check whether the node is already present */
    if (LsppGetAllFsLsppGlobalConfigTable (&LsppGlobalConfigTableEntry) !=
        OSIX_SUCCESS)
    {
        CLI_SET_ERR (CLI_LSPP_GLOBAL_CONFIG_ENTRY_NOT_EXIST);
        return OSIX_FAILURE;
    }

    if ((LsppGlobalConfigTableEntry.MibObject.
         i4FsLsppSystemControl == LSPP_SHUTDOWN) &&
        (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppSystemControl !=
         OSIX_TRUE))
    {
        CLI_SET_ERR (CLI_LSPP_MODULE_SHUTDOWN);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : LsppDbPingTraceCheckDependency
 *
 * Description: Function to check the dependencies in the newly created 
 *              pingTrace entry
 *
 * Input      : pLsppFsLsppPingTraceTableEntry - PingTrace structure for which 
 *              the dependencies has to be checked
 * Output     : None
 * Returns    : OSIX_SUCCESS/OSIX_FAILURE
 *     
 *****************************************************************************/
INT4
LsppDbPingTraceCheckDependency (tLsppFsLsppPingTraceTableEntry *
                                pLsppFsLsppPingTraceTableEntry)
{
    /* Dependencies between inputs if the request type is TRACE ROUTE */
    if (pLsppFsLsppPingTraceTableEntry->MibObject.
        i4FsLsppRequestType == LSPP_REQ_TYPE_TRACE_ROUTE)
    {
        if ((pLsppFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppPathType == LSPP_PATH_TYPE_FEC_128) ||
            (pLsppFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppPathType == LSPP_PATH_TYPE_FEC_129))
        {
            return OSIX_FAILURE;
        }
        /*  Reverse Path verify should not be set to true for trace route */
        if (pLsppFsLsppPingTraceTableEntry->MibObject.
            i4FsLsppReversePathVerify == LSPP_SNMP_TRUE)
        {
            return OSIX_FAILURE;
        }

        /* Verbose should not be set to true for trace route */
        if (pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppVerbose ==
            LSPP_SNMP_TRUE)
        {
            return OSIX_FAILURE;
        }

        /* Sweep, burst and same sequence number should not be configured for 
         * trace route
         * */
        if ((pLsppFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppSameSeqNumOption == LSPP_SNMP_TRUE) ||
            (pLsppFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppSweepOption == LSPP_SNMP_TRUE) ||
            (pLsppFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppBurstOption == LSPP_SNMP_TRUE))
        {
            return OSIX_FAILURE;
        }

        /* Global-id, Node-id, Interface number should not be configured while 
         * performing trace route. It can be configured only for Ping request
         * when connectivity verification to the intermediate node is required
         * */
        if ((pLsppFsLsppPingTraceTableEntry->MibObject.
             u4FsLsppTgtMipGlobalId != 0) ||
            (pLsppFsLsppPingTraceTableEntry->MibObject.
             u4FsLsppTgtMipNodeId != 0) ||
            (pLsppFsLsppPingTraceTableEntry->MibObject.
             u4FsLsppTgtMipIfNum != 0))
        {
            return OSIX_FAILURE;
        }

    }

    /* Dependency checks when the request type is PING */

    if (pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRequestType ==
        LSPP_REQ_TYPE_PING)
    {
        /* Interface and Label stack TLV can be set to true only if DSMAP is
         * set to true
         * */
        if ((pLsppFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppDsMap == LSPP_SNMP_FALSE) &&
            (pLsppFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppInterfaceLabelTlv == LSPP_SNMP_TRUE))
        {
            CLI_SET_ERR (CLI_LSPP_INTF_LBL_TLV_CANNOT_SET);
            return OSIX_FAILURE;
        }

        /* Same-sequence number option should not be configured in conjuction
         * with BURST and SWEEP option */
        if (pLsppFsLsppPingTraceTableEntry->MibObject.
            i4FsLsppSameSeqNumOption == LSPP_SNMP_TRUE)
        {
            if ((pLsppFsLsppPingTraceTableEntry->MibObject.
                 i4FsLsppSweepOption == LSPP_SNMP_TRUE) ||
                (pLsppFsLsppPingTraceTableEntry->MibObject.
                 i4FsLsppBurstOption == LSPP_SNMP_TRUE))
            {
                CLI_SET_ERR (CLI_LSPP_SAMESEQ_IN_CONJUCTION);
                return OSIX_FAILURE;
            }
        }

        /* SWEEP option should not be configured in conjuction
         * with BURST and Same-sequence number option */
        if (pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppSweepOption ==
            LSPP_SNMP_TRUE)
        {
            if ((pLsppFsLsppPingTraceTableEntry->MibObject.
                 i4FsLsppSameSeqNumOption == LSPP_SNMP_TRUE) ||
                (pLsppFsLsppPingTraceTableEntry->MibObject.
                 i4FsLsppBurstOption == LSPP_SNMP_TRUE))
            {
                CLI_SET_ERR (CLI_LSPP_SWEEP_IN_CONJUCTION);
                return OSIX_FAILURE;
            }
        }

        /* Burst option should not be configured in conjuction
         * with same-sequence number and SWEEP option */
        if (pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppBurstOption ==
            LSPP_SNMP_TRUE)
        {
            if ((pLsppFsLsppPingTraceTableEntry->MibObject.
                 i4FsLsppSweepOption == LSPP_SNMP_TRUE) ||
                (pLsppFsLsppPingTraceTableEntry->MibObject.
                 i4FsLsppSameSeqNumOption == LSPP_SNMP_TRUE))
            {
                CLI_SET_ERR (CLI_LSPP_BURST_IN_CONJUCTION);
                return OSIX_FAILURE;
            }
        }

        /* Reverse path verification can be done only for MPLS-TP LSPs */
        if ((pLsppFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppPathType != LSPP_PATH_TYPE_MEP) &&
            (pLsppFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppReversePathVerify == LSPP_SNMP_TRUE))
        {
            CLI_SET_ERR (CLI_LSPP_REV_PATH_VERIFY_INVALID);
            return OSIX_FAILURE;
        }

        /* Reverse path verification cant be done if reply mode is no-reply */
        if ((pLsppFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppReplyMode == LSPP_NO_REPLY) &&
            (pLsppFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppReversePathVerify == LSPP_SNMP_TRUE))
        {
            CLI_SET_ERR (CLI_LSPP_REV_PATH_VERIFY_INVALID);
            return OSIX_FAILURE;
        }

        /* Reverse Path verification cannot be done for MPLS-TP LSPs, if the
         * reverse path is not LSP
         * */
        if ((pLsppFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppPathType == LSPP_PATH_TYPE_MEP) &&
            (pLsppFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppReversePathVerify == LSPP_SNMP_TRUE) &&
            (pLsppFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppReplyMode == LSPP_REPLY_IP_UDP))
        {
            CLI_SET_ERR (CLI_LSPP_REV_PATH_VERIFY_INVALID);
            return OSIX_FAILURE;
        }

        /* Force-explicit NULL and TTL value should not be configured while
         * monitoring pseudowires
         * */
        if (((pLsppFsLsppPingTraceTableEntry->MibObject.
              i4FsLsppPathType == LSPP_PATH_TYPE_FEC_128) ||
             (pLsppFsLsppPingTraceTableEntry->MibObject.
              i4FsLsppPathType == LSPP_PATH_TYPE_FEC_129)) &&
            ((pLsppFsLsppPingTraceTableEntry->MibObject.
              i4FsLsppForceExplicitNull == LSPP_SNMP_TRUE) ||
             (pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTTLValue != 0)))
        {
            CLI_SET_ERR (CLI_LSPP_INVALID_OPTION_FOR_PW);
            return OSIX_FAILURE;
        }

        /* Global Id, Node Id, Interface number can be configured for 
         * intermediate node connectivity verification onlty if the path type 
         * is MEP 
         * */
        if ((pLsppFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppPathType != LSPP_PATH_TYPE_MEP) &&
            ((pLsppFsLsppPingTraceTableEntry->MibObject.
              u4FsLsppTgtMipGlobalId != 0) ||
             (pLsppFsLsppPingTraceTableEntry->MibObject.
              u4FsLsppTgtMipNodeId != 0) ||
             (pLsppFsLsppPingTraceTableEntry->MibObject.
              u4FsLsppTgtMipIfNum != 0)))
        {
            CLI_SET_ERR (CLI_LSPP_INTERMEDIATE_CV_INVALID);
            return OSIX_FAILURE;
        }

    }

    /* Encapsulation type should be VCCV NEGOTIATED only for PWs */
    if ((pLsppFsLsppPingTraceTableEntry->MibObject.
         i4FsLsppPathType != LSPP_PATH_TYPE_FEC_128) &&
        (pLsppFsLsppPingTraceTableEntry->MibObject.
         i4FsLsppPathType != LSPP_PATH_TYPE_FEC_129) &&
        (pLsppFsLsppPingTraceTableEntry->MibObject.
         i4FsLsppPathType != LSPP_PATH_TYPE_MEP) &&
        (pLsppFsLsppPingTraceTableEntry->MibObject.
         i4FsLsppEncapType == LSPP_ENCAP_TYPE_VCCV_NEGOTIATED))
    {
        CLI_SET_ERR (CLI_LSPP_ENCAP_VALID_FOR_PW_ONLY);
        return OSIX_FAILURE;
    }

    /* Encapsulation type must be VCCV NEGOTIATED for PWs */
    if (((pLsppFsLsppPingTraceTableEntry->MibObject.
          i4FsLsppPathType == LSPP_PATH_TYPE_FEC_128) ||
         (pLsppFsLsppPingTraceTableEntry->MibObject.
          i4FsLsppPathType == LSPP_PATH_TYPE_FEC_129)) &&
        ((pLsppFsLsppPingTraceTableEntry->MibObject.
          i4FsLsppEncapType == LSPP_ENCAP_TYPE_IP) ||
         (pLsppFsLsppPingTraceTableEntry->MibObject.
          i4FsLsppEncapType == LSPP_ENCAP_TYPE_ACH_IP) ||
         (pLsppFsLsppPingTraceTableEntry->MibObject.
          i4FsLsppEncapType == LSPP_ENCAP_TYPE_ACH)))
    {
        CLI_SET_ERR (CLI_LSPP_INVALID_ENCAP_FOR_PW);
        return OSIX_FAILURE;
    }

    /* ACH and ACH-IP encapsulations are not applicable 
     * for RSVP-TE established path.*/
    if (((pLsppFsLsppPingTraceTableEntry->MibObject.
          i4FsLsppPathType == LSPP_PATH_TYPE_RSVP_IPV4) ||
         (pLsppFsLsppPingTraceTableEntry->MibObject.
          i4FsLsppPathType == LSPP_PATH_TYPE_RSVP_IPV6)) &&
        ((pLsppFsLsppPingTraceTableEntry->MibObject.
          i4FsLsppEncapType == LSPP_ENCAP_TYPE_ACH) ||
         (pLsppFsLsppPingTraceTableEntry->MibObject.
          i4FsLsppEncapType == LSPP_ENCAP_TYPE_ACH_IP)))
    {
        CLI_SET_ERR (CLI_LSPP_ENCAP_INVALID_FOR_RSVP);
        return OSIX_FAILURE;
    }

    /* Reply mode Control channel is not applicable for  
     * for RSVP-TE established path.*/
    if (((pLsppFsLsppPingTraceTableEntry->MibObject.
          i4FsLsppPathType == LSPP_PATH_TYPE_RSVP_IPV4) ||
         (pLsppFsLsppPingTraceTableEntry->MibObject.
          i4FsLsppPathType == LSPP_PATH_TYPE_RSVP_IPV6)) &&
        (pLsppFsLsppPingTraceTableEntry->MibObject.
         i4FsLsppReplyMode == LSPP_REPLY_APPLICATION_CC))
    {
        CLI_SET_ERR (CLI_LSPP_INVALID_REPLY_MODE_FOR_RSVP);
        return OSIX_FAILURE;
    }

    /* Reply mode cannot be IP or IP with Router Alert 
     * when the request doen not contain IP/UDP header.*/
    if ((pLsppFsLsppPingTraceTableEntry->MibObject.
         i4FsLsppEncapType == LSPP_ENCAP_TYPE_ACH) &&
        ((pLsppFsLsppPingTraceTableEntry->MibObject.
          i4FsLsppReplyMode == LSPP_REPLY_IP_UDP) ||
         (pLsppFsLsppPingTraceTableEntry->MibObject.
          i4FsLsppReplyMode == LSPP_REPLY_IP_UDP_ROUTER_ALERT)))
    {
        CLI_SET_ERR (CLI_LSPP_INVALID_REPLY_MODE_FOR_ENCAP);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : LsppDbCreateEchoSeqNode
 * DESCRIPTION      : This function will create and add a node in Echo 
 *                    Sequecne table.
 * Input(s)         : pLsppPingTraceEntry - Pointer to Ping trace Entry
 * OUTPUT           : pEchoSequenceEntry - pointer to the allocated node.
 * RETURNS          : pEchoSequenceEntry / NULL
 **************************************************************************/
tLsppFsLsppEchoSequenceTableEntry *
LsppDbCreateEchoSeqNode (tLsppFsLsppPingTraceTableEntry * pLsppPingTraceEntry)
{
    tLsppFsLsppEchoSequenceTableEntry EchoSequenceEntry;
    tLsppFsLsppEchoSequenceTableEntry *pEchoSequenceEntry = NULL;

    MEMSET (&EchoSequenceEntry, 0, sizeof (tLsppFsLsppEchoSequenceTableEntry));

    EchoSequenceEntry.MibObject.u4FsLsppContextId =
        pLsppPingTraceEntry->MibObject.u4FsLsppContextId;

    EchoSequenceEntry.MibObject.u4FsLsppSenderHandle =
        pLsppPingTraceEntry->MibObject.u4FsLsppSenderHandle;

    /* The last sent sequence number is updated while 
     * framing the packet. */
    EchoSequenceEntry.MibObject.u4FsLsppSequenceNumber =
        (++(pLsppPingTraceEntry->u4LastGeneratedSeqNum));

    pEchoSequenceEntry =
        LsppFsLsppEchoSequenceTableCreateApi (&EchoSequenceEntry);
    if (pEchoSequenceEntry == NULL)
    {
        --(pLsppPingTraceEntry->u4LastGeneratedSeqNum);
        return NULL;
    }

    pEchoSequenceEntry->u1Status = LSPP_ECHO_SEQ_UNSENT;

    return pEchoSequenceEntry;
}
