
/******************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lsppapi.c,v 1.7 2013/06/27 12:23:22 siva Exp $
 *
 * Description : This file contains the entry function of the LSP Ping module
 *               which will be called by the extrenal modules
 *               messages queued
 *****************************************************************************/
#include "lsppinc.h"

/****************************************************************************
*                                                                           *
* Function     : LsppApiHandleExtRequest                                    *
*                                                                           *
* Description  : This function is to intimate the LSP Ping module to        *
*                perform the necesary action needed by the external modules.*
*                                                                           *
* Input        : pLsppReqParams - Pointer to the input structure with the   *
*                necessary input parameters filled.                         *
*                                                                           *
* Output       : pLsppRespParams - It contain the error code to inform the  *
*                external module about the failures if any.                 *
*                                                                           *
* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
*                                                                           *
*****************************************************************************/
INT4
LsppApiHandleExtRequest (tLsppReqParams * pLsppReqParams,
                         tLsppRespParams * pLsppRespParams)
{
    tLsppQMsg          *pLsppQMsg = NULL;

    /* Allocate memory for queue message. */
    if ((pLsppQMsg = (tLsppQMsg *)
         MemAllocMemBlk (gLsppGlobals.LsppFsLsppQMsgPoolId)) == NULL)
    {
        LSPP_CMN_TRC (pLsppReqParams->u4ContextId, LSPP_MEM_ALLOC_FAIL,
                      "LsppApiHandleExtRequest");

        pLsppRespParams->u1ErrorCode = LSPP_QMSG_MEM_ALLOC_FAILED;
        return OSIX_FAILURE;
    }

    MEMSET (pLsppQMsg, 0, sizeof (tLsppQMsg));

    pLsppQMsg->u1MsgType = pLsppReqParams->u1MsgType;
    pLsppQMsg->u4ContextId = pLsppReqParams->u4ContextId;

    switch (pLsppQMsg->u1MsgType)
    {
        case LSPP_CREATE_CONTEXT_MSG:
        case LSPP_DELETE_CONTEXT_MSG:
            break;

        case LSPP_BFD_BOOTSTRAP_ECHO_REQ:

            /* If the msg type is bfd bootstrap, then copy the path info 
             * structure from the input to the Queue message, also fill the
             * BFD discriminator value in the Queue message
             * */
            MEMCPY (&(pLsppQMsg->unMsgParam.LsppExtTrigInfo.LsppPathId),
                    &(pLsppReqParams->unMsgParam.LsppExtTrigInfo.LsppPathId),
                    sizeof (tLsppPathId));

            MEMCPY (&(pLsppQMsg->unMsgParam.LsppExtTrigInfo.BfdBtStrapInfo),
                    &(pLsppReqParams->unMsgParam.LsppExtTrigInfo.
                      BfdBtStrapInfo), sizeof (tLsppBfdBtStrapInfo));
            break;

        case LSPP_BFD_BOOTSTRAP_ECHO_REPLY:

            MEMCPY (&(pLsppQMsg->unMsgParam.LsppBfdInfo),
                    &(pLsppReqParams->unMsgParam.LsppBfdInfo),
                    sizeof (tLsppBfdInfo));
            break;

        case LSPP_RX_PDU_MSG:

            pLsppQMsg->unMsgParam.LsppRxPduInfo.pBuf =
                pLsppReqParams->unMsgParam.LsppRxPduInfo.pBuf;

            pLsppQMsg->unMsgParam.LsppRxPduInfo.u4IfIndex =
                pLsppReqParams->unMsgParam.LsppRxPduInfo.u4IfIndex;

            break;

        default:
            LSPP_LOG (pLsppReqParams->u4ContextId, LSPP_INVALID_MSG_TYPE,
                      "LsppApiHandleExtRequest");
            pLsppRespParams->u1ErrorCode = LSPP_MSG_TYPE_INVALID;

            /* Release the Queue Message allocated since the message type 
             * is invalid.
             * */
            if (MemReleaseMemBlock (gLsppGlobals.LsppFsLsppQMsgPoolId,
                                    (UINT1 *) pLsppQMsg) == MEM_FAILURE)
            {
                LSPP_CMN_TRC (pLsppReqParams->u4ContextId,
                              LSPP_MEM_RELEASE_FAILED,
                              "LsppApiHandleExtRequest", LSPP_QUEUE_NAME);
            }
            return OSIX_FAILURE;
    }

    if (LsppQueEnqMsg (pLsppQMsg, &(pLsppRespParams->u1ErrorCode)) !=
        OSIX_SUCCESS)
    {
        LSPP_CMN_TRC (pLsppReqParams->u4ContextId,
                      LSPP_ENQUEUE_FAILED, "LsppApiHandleExtRequest");

        /* Release the received packet buffer when the enqueue is failed. */
        if (pLsppQMsg->u1MsgType == LSPP_RX_PDU_MSG)
        {
            LSPP_RELEASE_CRU_BUF (pLsppQMsg->unMsgParam.LsppRxPduInfo.pBuf,
                                  FALSE);
        }

        if (MemReleaseMemBlock (gLsppGlobals.LsppFsLsppQMsgPoolId,
                                (UINT1 *) pLsppQMsg) == MEM_FAILURE)
        {
            LSPP_CMN_TRC (pLsppReqParams->u4ContextId,
                          LSPP_MEM_RELEASE_FAILED,
                          "LsppApiHandleExtRequest", LSPP_QUEUE_NAME);
        }
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/****************************************************************************
*                                                                           *
* Function     : LsppApiPktRcvdOnSocket                                     *
*                                                                           *
* Description  : This is the call back function registered with SELECT      *
*                utility. This function will get called when a request or   *
*                reply packet is received in the socket registered with     *
*                the SELECT function.                                       *
*                This function will send a event to LSP Ping indicating the *
*                packet reception in the socket                             *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
LsppApiPktRcvdOnSocket (INT4 i4SockId)
{
    UNUSED_PARAM (i4SockId);

    if (OsixEvtSend (gLsppGlobals.lsppTaskId, LSPP_IP_PKT_RX_EVENT) !=
        OSIX_SUCCESS)
    {
        return;
    }

}

/****************************************************************************
*                                                                           *
* Function     : LsppApiVcmCallback                                         *
*                                                                           *
* Description  : This is the call back function registered with VCM Module. *
*                This call back will get called when a context is created   *
*                or deleted.                                                *
*                This is the notification to the LSP PING module            *
*                to delete or create the context.                           *
*                                                                           *
* Input        : IfIndex- Interface index                                   *
*                u4VcmCxtId - Context Id which context has to be created or *
*                deleted.                                                   *
*                u1Event    - To specify Whether to create or delete a      *
*                             context                                       *
* Output       : None                                                       *
*                                                                           *
* Returns      : None                                                       *
*                                                                           *
*****************************************************************************/
VOID
LsppApiVcmCallback (UINT4 u4IpIfIndex, UINT4 u4VcmCxtId, UINT1 u1Event)
{
    tLsppReqParams      LsppReqParams;
    tLsppRespParams     LsppRespParams;
    UNUSED_PARAM (u4IpIfIndex);

    MEMSET (&LsppReqParams, 0, sizeof (tLsppReqParams));
    MEMSET (&LsppRespParams, 0, sizeof (tLsppRespParams));

    if (u1Event & VCM_CONTEXT_CREATE)
    {
        LsppReqParams.u1MsgType = LSPP_CREATE_CONTEXT_MSG;
        LsppReqParams.u4ContextId = u4VcmCxtId;
    }
    else if (u1Event & VCM_CONTEXT_DELETE)
    {
        LsppReqParams.u1MsgType = LSPP_DELETE_CONTEXT_MSG;
        LsppReqParams.u4ContextId = u4VcmCxtId;
    }
    else
    {
        return;
    }

    if (LsppApiHandleExtRequest (&LsppReqParams, &LsppRespParams) !=
        OSIX_SUCCESS)
    {
        LSPP_LOG (LSPP_INVALID_CONTEXT, LSPP_API_HANDLE_FAILED,
                  "LsppApiVcmCallback");
    }
    return;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  lsppque.c                      */
/*-----------------------------------------------------------------------*/
