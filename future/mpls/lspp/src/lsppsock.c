/*****************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved                      *
 *                                                                           *
 * $Id: lsppsock.c,v 1.17 2013/12/20 03:49:48 siva Exp $                                                                    *
 *                                                                           *
 * Description: This file contains the LSP Ping socket related utilities     *
 *              implementation.                                              *
 *****************************************************************************/

#include "lsppinc.h"

/*****************************************************************************
 *                                                                           *
 * Function     : LsppSockInit                                               *
 *                                                                           *
 * Description  : This routine initialises the socket related parameters.    *
 *                This function should be invoked during LSP Ping module     *
 *                boot up.                                                   *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
PUBLIC INT4
LsppSockInit (VOID)
{
    struct sockaddr_in  LsppLocalAddr;
    MEMSET (&LsppLocalAddr, 0, sizeof (struct sockaddr_in));

    gLsppGlobals.i4LsppSockId = socket (AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    if (gLsppGlobals.i4LsppSockId < 0)
    {
        /* Failure in opening the UDP socket */
        LSPP_CMN_TRC (LSPP_INVALID_CONTEXT, LSPP_SOCK_CREATION_FAILED,
                      "LsppSockInitUdpSock");
        return OSIX_FAILURE;
    }

    LsppLocalAddr.sin_family = AF_INET;
    LsppLocalAddr.sin_addr.s_addr = OSIX_HTONS (INADDR_ANY);
    LsppLocalAddr.sin_port = OSIX_HTONS (LSPP_UDP_DEST_PORT);

    /* Bind with the socket */
    if (bind (gLsppGlobals.i4LsppSockId, (struct sockaddr *) &LsppLocalAddr,
              sizeof (struct sockaddr_in)) < 0)
    {
        /* Failure in binding the UDP socket */
        LSPP_CMN_TRC (LSPP_INVALID_CONTEXT, LSPP_SOCK_BIND_FAILED,
                      "LsppSockInitUdpSock");
        close (gLsppGlobals.i4LsppSockId);
        return OSIX_FAILURE;
    }

    if (LsppSockInitSockParams (gLsppGlobals.i4LsppSockId) != OSIX_SUCCESS)
    {
        LSPP_LOG (LSPP_INVALID_CONTEXT, LSPP_SOCK_INIT_FAILED,
                  "LsppSockInitUdpSock");
        close (gLsppGlobals.i4LsppSockId);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : LsppSockInitSockParams                                      *
 *                                                                           *
 * Description  : This routine initialises the socket related parameters for *
 *                the provided socket.                                       *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
LsppSockInitSockParams (INT4 i4SockId)
{
    INT4                i4Flags = 0;
    UINT1               u1OpnVal = 1;

    if ((i4Flags = fcntl (i4SockId, F_GETFL, 0)) < 0)
    {
        LSPP_CMN_TRC (LSPP_INVALID_CONTEXT, LSPP_SOCK_FCNTL_SET_FAILED,
                      "LsppSockInitSockParams");
        return OSIX_FAILURE;
    }

    i4Flags |= O_NONBLOCK;

    if (fcntl (i4SockId, F_SETFL, i4Flags) < 0)
    {
        LSPP_CMN_TRC (LSPP_INVALID_CONTEXT, LSPP_SOCK_FCNTL_SET_FAILED,
                      "LsppSockInitSockParams");
        return OSIX_FAILURE;
    }

    if (setsockopt (i4SockId, IPPROTO_IP, IP_PKTINFO,
                    &u1OpnVal, sizeof (UINT1)) < 0)
    {
        return OSIX_FAILURE;
    }

    if (LsppSockAddFd () != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : LsppSockTransmitEchoRplyPkt                                     *
 * Description  : This function is to transmit the Ip echo reply packet to   *
 *                the ingress router through socket.                         *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
LsppSockTransmitEchoRplyPkt (UINT4 u4ContextId, tCRU_BUF_CHAIN_HEADER * pBuf,
                             UINT4 u4DestAddr, UINT1 *pu1DscpValue,
                             UINT4 u4DstPort, BOOL1 bIsRouterAlert)
{
    struct sockaddr_in  PeerAddr;
    UINT1              *pu1TxBuf = NULL;
    UINT4               u4PktLen = 0;
    UINT4               u4ifAddr = 0;
    INT4                i4OpnVal = 1;
    INT4                i4RawSockFd = 0;
    INT4                i4OptnVal = 1;
#ifndef IP_WANTED
    UNUSED_PARAM (u4ContextId);
#endif

    if ((pu1TxBuf =
         (UINT1 *) MemAllocMemBlk (LSPP_FSLSPP_PDU_BUFFER_POOLID)) == NULL)
    {
        return OSIX_FAILURE;
    }
    MEMSET (&PeerAddr, 0, sizeof (struct sockaddr_in));
    MEMSET (pu1TxBuf, 0, LSPP_MAX_PDU_LEN);

    PeerAddr.sin_family = AF_INET;
    PeerAddr.sin_addr.s_addr = OSIX_HTONL (u4DestAddr);
    PeerAddr.sin_port = (UINT2) OSIX_HTONS (u4DstPort);

    u4PktLen = LSPP_GET_CRU_VALID_BYTE_COUNT (pBuf);
    if (CRU_BUF_Copy_FromBufChain (pBuf, pu1TxBuf, 0, u4PktLen) == CRU_FAILURE)
    {
        LSPP_CMN_TRC (LSPP_INVALID_CONTEXT, LSPP_CRU_BUF_ALLOC_FAILED,
                      "\n Error in LsppSockTransmitEchoRplyPkt - Copy from Buf failed.\n");
        MemReleaseMemBlock (LSPP_FSLSPP_PDU_BUFFER_POOLID, (UINT1 *) pu1TxBuf);
        return OSIX_FAILURE;
    }

    if (*pu1DscpValue != 0)
    {
        /* Shift the Value to the TOS bits (7-2) set TOS bits (1,0) as 0  */
        i4OpnVal = (INT4) (*pu1DscpValue << 2);
        if (setsockopt (gLsppGlobals.i4LsppSockId, IPPROTO_IP, IP_TOS,
                        &i4OpnVal, sizeof (INT4)) < 0)
        {
            LSPP_CMN_TRC (LSPP_INVALID_CONTEXT, LSPP_SET_SOCK_OPT_FAILED,
                          "LsppSockTransmitEchoRplyPkt", "Dscp value");
            MemReleaseMemBlock (LSPP_FSLSPP_PDU_BUFFER_POOLID,
                                (UINT1 *) pu1TxBuf);
            return OSIX_FAILURE;
        }
    }
#ifdef IP_WANTED
    if (setsockopt (gLsppGlobals.i4LsppSockId, IPPROTO_IP, IP_PKT_TX_CXTID,
                    &u4ContextId, sizeof (UINT4)) < 0)
    {
        LSPP_CMN_TRC (LSPP_INVALID_CONTEXT, LSPP_SET_SOCK_OPT_FAILED,
                      "LsppSockTransmitEchoRplyPkt", "Tx-Context");
        MemReleaseMemBlock (LSPP_FSLSPP_PDU_BUFFER_POOLID, (UINT1 *) pu1TxBuf);
        return OSIX_FAILURE;
    }
#endif

    if (bIsRouterAlert == OSIX_TRUE)
    {
        i4RawSockFd = socket (AF_INET, SOCK_RAW, 0);
        if (i4RawSockFd < 0)
        {
            close (i4RawSockFd);
            MemReleaseMemBlock (LSPP_FSLSPP_PDU_BUFFER_POOLID,
                                (UINT1 *) pu1TxBuf);
            return OSIX_FAILURE;
        }

        /* IP Header is included in the packet construction */
        if (setsockopt (i4RawSockFd, IPPROTO_IP, IP_HDRINCL,
                        (&i4OptnVal), (sizeof (INT4))) < 0)
        {
            close (i4RawSockFd);
            MemReleaseMemBlock (LSPP_FSLSPP_PDU_BUFFER_POOLID,
                                (UINT1 *) pu1TxBuf);
            return OSIX_FAILURE;
        }

        if ((sendto (i4RawSockFd, pu1TxBuf, u4PktLen, 0,
                     (struct sockaddr *) &PeerAddr,
                     sizeof (struct sockaddr_in))) < 0)
        {
            LSPP_CMN_TRC (LSPP_INVALID_CONTEXT, LSPP_SOCK_SEND_TO_FAILED,
                          "LsppSockTransmitEchoRplyPkt");
            close (i4RawSockFd);
            MemReleaseMemBlock (LSPP_FSLSPP_PDU_BUFFER_POOLID,
                                (UINT1 *) pu1TxBuf);
            return OSIX_FAILURE;
        }

        if (close (i4RawSockFd) != 0)
        {
            MemReleaseMemBlock (LSPP_FSLSPP_PDU_BUFFER_POOLID,
                                (UINT1 *) pu1TxBuf);
            return OSIX_FAILURE;
        }

    }
    /* Send the LSP PING echo reply in the socket */
    else
    {

        if (LsppCoreGetSrcIpFromDestIp (u4ContextId, u4DestAddr, &u4ifAddr) !=
            OSIX_SUCCESS)
        {
            LSPP_LOG (u4ContextId,
                      LSPP_NO_IP_ROUTE_EXIST, "LsppCoreSendEchoReply");
            MemReleaseMemBlock (LSPP_FSLSPP_PDU_BUFFER_POOLID,
                                (UINT1 *) pu1TxBuf);
            return OSIX_FAILURE;
        }

        if (LsppSockSendMessage (pu1TxBuf, u4PktLen, u4DestAddr,
                                 u4DstPort, u4ifAddr) != OSIX_SUCCESS)
        {
            LSPP_CMN_TRC (LSPP_INVALID_CONTEXT, LSPP_SOCK_SEND_TO_FAILED,
                          "LsppSockTransmitEchoRplyPkt");
            MemReleaseMemBlock (LSPP_FSLSPP_PDU_BUFFER_POOLID,
                                (UINT1 *) pu1TxBuf);
            return OSIX_FAILURE;
        }

        if (sendto (gLsppGlobals.i4LsppSockId, pu1TxBuf, (UINT4) u4PktLen, 0,
                    (struct sockaddr *) &PeerAddr,
                    sizeof (struct sockaddr_in)) < 0)
        {
            LSPP_CMN_TRC (LSPP_INVALID_CONTEXT, LSPP_SOCK_SEND_TO_FAILED,
                          "LsppSockTransmitEchoRplyPkt");
            MemReleaseMemBlock (LSPP_FSLSPP_PDU_BUFFER_POOLID,
                                (UINT1 *) pu1TxBuf);
            return OSIX_FAILURE;
        }
    }

    i4OpnVal = 0;

#if 0

    if (bIsRouterAlert == OSIX_TRUE)
    {
        /* Reset the IP_ROUTER_ALERT option in the socket */
        if (setsockopt (gLsppGlobals.i4LsppSockId, IPPROTO_IP, IP_ROUTER_ALERT,
                        (UINT1 *) &u1OpnVal, sizeof (UINT1)) < 0)
        {
            LSPP_CMN_TRC (LSPP_INVALID_CONTEXT, LSPP_SET_SOCK_OPT_FAILED,
                          "LsppSockTransmitEchoRplyPkt", "Router-Alert option");
            return OSIX_FAILURE;
        }
    }
#endif

    /* Reset the Dscp Value */
    if (setsockopt (gLsppGlobals.i4LsppSockId, IPPROTO_IP, IP_TOS,
                    &i4OpnVal, sizeof (INT4)) < 0)
    {
        LSPP_CMN_TRC (LSPP_INVALID_CONTEXT, LSPP_SET_SOCK_OPT_FAILED,
                      "LsppSockTransmitEchoRplyPkt", "Dscp value");
        MemReleaseMemBlock (LSPP_FSLSPP_PDU_BUFFER_POOLID, (UINT1 *) pu1TxBuf);
        return OSIX_FAILURE;
    }
    MemReleaseMemBlock (LSPP_FSLSPP_PDU_BUFFER_POOLID, (UINT1 *) pu1TxBuf);
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : LsppSockAddFd                                              *
 *                                                                           *
 * Description  : This function is to register the call back function with   *
 *                the select utility when a echo reply packet is received    *
 *                in the socket                                              *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/

PUBLIC INT4
LsppSockAddFd (VOID)
{
    if (SelAddFd (gLsppGlobals.i4LsppSockId, LsppApiPktRcvdOnSocket) !=
        OSIX_SUCCESS)
    {
        /* FD Add failed */
        LSPP_CMN_TRC (LSPP_INVALID_CONTEXT, LSPP_ADD_FD_IN_SELECT_FAILED,
                      "LsppSockAddOrRemoveFd");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : LsppSockDeInit                                             *
 *                                                                           *
 * Description  : This routine de-initializes the UDP data that are needed   *
 *                by the LSPP module. This should be invoked whenever LSPP   *
 *                module is shutdown. This close all the socktes and reset   *
 *                the options                                                *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/

PUBLIC VOID
LsppSockDeInit (VOID)
{
    /* Remove the FD association from SLI */
    SelRemoveFd (gLsppGlobals.i4LsppSockId);

    /* Close the socket identifier. */
    close (gLsppGlobals.i4LsppSockId);

    return;
}

/*****************************************************************************
 *                                                                           *
 * Function     : LsppSockReceiveEchoPkt                                     *
 *                                                                           *
 * Description  : This function is to receive the Ip Echo reply packet from  *
 *                the socket and call the corresponding function to process  *
 *                the received reply packet                                  *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : None                                                       *
 *                                                                           *
 *****************************************************************************/

VOID
LsppSockReceiveEchoPkt (VOID)
{
    struct sockaddr_in  PeerAddr;
    struct msghdr       PktInfo;
    struct in_pktinfo  *pIpPktInfo = NULL;
#ifdef BSDCOMP_SLI_WANTED
    struct cmsghdr     *pCmsg = NULL;
    UINT1               au1Cmsg[LSPP_MPLS_MAX_IP_HDR_LEN];
    struct iovec        Iov;
#else
    struct cmsghdr      CmsgInfo;
#endif
    tLsppEchoMsg       *pLsppEchoMsg = NULL;
    UINT1              *pu1RxBuf = NULL;
    UINT4               u4IpPktDAddress = 0;
    INT4                i4PktLen = 0;
#ifndef BSDCOMP_SLI_WANTED
    INT4                i4AddrLen = 0;
#endif
    INT1                i1OptVal = 0;

    if ((pu1RxBuf =
         (UINT1 *) MemAllocMemBlk (LSPP_FSLSPP_PDU_BUFFER_POOLID)) == NULL)
    {
        return;
    }
    MEMSET (&PeerAddr, 0, sizeof (struct sockaddr_in));
    MEMSET (pu1RxBuf, 0, LSPP_MAX_PDU_LEN);

#ifdef BSDCOMP_SLI_WANTED
    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));

    PktInfo.msg_name = (void *) &PeerAddr;
    PktInfo.msg_namelen = sizeof (struct sockaddr_in);
    Iov.iov_base = pu1RxBuf;
    Iov.iov_len = LSPP_MAX_PDU_LEN;
    PktInfo.msg_iov = &Iov;
    PktInfo.msg_iovlen = 1;
    PktInfo.msg_control = (void *) au1Cmsg;
    PktInfo.msg_controllen = sizeof (au1Cmsg);

    pCmsg = CMSG_FIRSTHDR (&PktInfo);
    pCmsg->cmsg_level = SOL_IP;
    pCmsg->cmsg_type = IP_PKTINFO;
    pCmsg->cmsg_len = sizeof (au1Cmsg);

    i4PktLen = recvmsg (gLsppGlobals.i4LsppSockId, &PktInfo, 0);

    if (i4PktLen > 0)
    {
        pIpPktInfo =
            (struct in_pktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));
        u4IpPktDAddress = pIpPktInfo->ipi_spec_dst.s_addr;
    }
    else
    {
        LSPP_CMN_TRC (LSPP_INVALID_CONTEXT, LSPP_SOCK_RECV_FROM_FAILED,
                      "LsppSockReceiveEchoRplyPkt");
        MemReleaseMemBlock (LSPP_FSLSPP_PDU_BUFFER_POOLID, (UINT1 *) pu1RxBuf);
        return;
    }
#else
    i4PktLen = recvfrom (gLsppGlobals.i4LsppSockId, pu1RxBuf,
                         LSPP_MAX_PDU_LEN, 0,
                         (struct sockaddr *) &PeerAddr, &i4AddrLen);

    if (i4PktLen <= 0)
    {
        LSPP_CMN_TRC (LSPP_INVALID_CONTEXT, LSPP_SOCK_RECV_FROM_FAILED,
                      "LsppSockReceiveEchoRplyPkt");
        MemReleaseMemBlock (LSPP_FSLSPP_PDU_BUFFER_POOLID, (UINT1 *) pu1RxBuf);
        return;
    }

    PktInfo.msg_control = (VOID *) &CmsgInfo;
    PktInfo.msg_controllen = sizeof (CmsgInfo);

    if (recvmsg (gLsppGlobals.i4LsppSockId, &PktInfo, 0) < 0)
    {
        MemReleaseMemBlock (LSPP_FSLSPP_PDU_BUFFER_POOLID, (UINT1 *) pu1RxBuf);
        return;
    }

    pIpPktInfo = (tInPktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));
    u4IpPktDAddress = pIpPktInfo->ipi_addr.s_addr;
#endif

    /*if (getsockopt (gLsppGlobals.i4LsppSockId, IPPROTO_IP,
       perror ("recvmsg failed\r\n");
       IP_PKT_RX_CXTID, (void *) &i1OptVal, sizeof (INT1)) !=
       SLI_SUCCESS)
       {
       LSPP_CMN_TRC (LSPP_INVALID_CONTEXT, LSPP_GET_SOCK_OPT_FAILED,
       "LsppSockReceiveEchoRplyPkt");
       return;
       } */

    if ((pLsppEchoMsg = (tLsppEchoMsg *)
         MemAllocMemBlk (gLsppGlobals.LsppFsLsppEchoMsgPoolId)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPP_PDU_BUFFER_POOLID, (UINT1 *) pu1RxBuf);
        return;
    }

    MEMSET (pLsppEchoMsg, 0, sizeof (tLsppEchoMsg));

    pLsppEchoMsg->u4ContextId = (UINT4) i1OptVal;
    pLsppEchoMsg->LsppIpHeader.u4Src = OSIX_NTOHL (PeerAddr.sin_addr.s_addr);
    pLsppEchoMsg->LsppIpHeader.u4Dest = u4IpPktDAddress;
    pLsppEchoMsg->LsppIpHeader.u1Version = LSPP_ENCAP_IPV4_HDR;
    pLsppEchoMsg->u4IfIndex = 0;

    if (LsppRxProcessPdu (pLsppEchoMsg, pu1RxBuf, (UINT4) i4PktLen,
                          OSIX_FALSE) != OSIX_SUCCESS)
    {
        LSPP_LOG ((UINT4) i1OptVal, LSPP_PROCESS_RX_PKT_FAIL,
                  "LsppSockReceiveEchoPkt");
    }

    if (MemReleaseMemBlock (gLsppGlobals.LsppFsLsppEchoMsgPoolId,
                            (UINT1 *) pLsppEchoMsg) != MEM_SUCCESS)
    {
        LSPP_CMN_TRC (LSPP_INVALID_CONTEXT, LSPP_MEM_RELEASE_FAILED,
                      "LsppSockReceiveEchoRplyPkt", "Ping Trace Table");
        MemReleaseMemBlock (LSPP_FSLSPP_PDU_BUFFER_POOLID, (UINT1 *) pu1RxBuf);
        return;
    }

    if (LsppSockAddFd () != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPP_PDU_BUFFER_POOLID, (UINT1 *) pu1RxBuf);
        return;
    }
    MemReleaseMemBlock (LSPP_FSLSPP_PDU_BUFFER_POOLID, (UINT1 *) pu1RxBuf);

}

/*****************************************************************************/
/* Function Name : LsppSoclSendMessage                                       */
/* Description   : Function to set IP_INFO for UDP Send                      */
/* Input(s)      : pu1Data - Buffer to Send                                  */
/* Input(s)      : u4BufLen - Buffer Size                                    */
/*                 u4DestAddr - DestinationIP                                */
/*                 u4DestPort - Destination port                             */
/*                 u4IfAddr - outgoing Interface IP Address                  */
/* Output(s)     : None                                                      */
/* Return(s)     : OSIX_SUCCESS/OSIX_FAILURE                                 */
/*****************************************************************************/
PUBLIC UINT4
LsppSockSendMessage (UINT1 *pu1Data, UINT4 u4BufLen,
                     UINT4 u4DestAddr, UINT4 u4DestPort, UINT4 u4IfAddr)
{
    struct msghdr       PktInfo;
    struct in_pktinfo  *pIpPktInfo;
    struct cmsghdr     *pCmsgInfo;
    UINT1               au1Cmsg[24];
    struct sockaddr_in  PeerAddr;

    MEMSET (&PktInfo, 0, sizeof (struct msghdr));
#ifdef BSDCOMP_SLI_WANTED
    struct iovec        Iov;
    Iov.iov_base = pu1Data;
    Iov.iov_len = u4BufLen;
    PktInfo.msg_iov = &Iov;
    PktInfo.msg_iovlen = 1;
#else
    UNUSED_PARAM (pu1Data);
    UNUSED_PARAM (u4BufLen);
#endif

    MEMSET (au1Cmsg, 0, sizeof (au1Cmsg));

#ifdef BSDCOMP_SLI_WANTED
    return OSIX_SUCCESS;
#endif

    PeerAddr.sin_family = AF_INET;
    PeerAddr.sin_addr.s_addr = OSIX_HTONL (u4DestAddr);
    PeerAddr.sin_port = (UINT2) OSIX_HTONS (u4DestPort);

    PktInfo.msg_name = (VOID *) &PeerAddr;
    PktInfo.msg_namelen = sizeof (struct sockaddr_in);
    PktInfo.msg_control = (VOID *) au1Cmsg;
    PktInfo.msg_controllen = sizeof (au1Cmsg);
    pCmsgInfo = CMSG_FIRSTHDR (&PktInfo);
    pCmsgInfo->cmsg_level = SOL_IP;
    pCmsgInfo->cmsg_type = IP_PKTINFO;
    pCmsgInfo->cmsg_len = sizeof (au1Cmsg);

    pIpPktInfo =
        (struct in_pktinfo *) (VOID *) CMSG_DATA (CMSG_FIRSTHDR (&PktInfo));

    pIpPktInfo->ipi_ifindex = LSPP_INVALID_IF_INDEX;
    if ((u4IfAddr != LSPP_INVALID_IF_INDEX) &&
        (NetIpv4GetIfIndexFromAddr (u4IfAddr,
                                    (UINT4 *) &pIpPktInfo->
                                    ipi_ifindex) == NETIPV4_FAILURE))
    {
        return OSIX_FAILURE;
    }

    pIpPktInfo->ipi_addr.s_addr = u4DestAddr;
    if (sendmsg (gLsppGlobals.i4LsppSockId, &PktInfo, 0) < 0)
    {
        LSPP_CMN_TRC (LSPP_INVALID_CONTEXT, LSPP_SOCK_SEND_MSG_FAILED,
                      "LsppSockTransmitEchoRplyPkt");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*----------------------------------------------------------------------------*/
 /*                       End of lsppsock.c file                              */
/*----------------------------------------------------------------------------*/
