/**************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lsppcxt.c,v 1.12 2012/02/29 09:11:02 siva Exp $
 *
 * Description: This file contains the Lspp Context related functions
 *************************************************************************/
#ifndef _LSPPCXT_C_
#define _LSPPCXT_C_

#include "lsppinc.h"

/*******************************************************************************
 *                                                                           *
 * Function     : LsppCxtCreateGlobalConfig                                  *
 *                                                                           *
 * Description  : Creates the entry in Global config for maintaining the     *
 *                context specific global configurations.                    *
 *                                                                           *
 * Input        : u4ContextId - unique identifier for the context.           *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/

PUBLIC INT4
LsppCxtCreateGlobalConfig (UINT4 u4ContextId)
{
    tLsppFsLsppGlobalConfigTableEntry LsppGlobalConfigEntry;
    tLsppExtInParams   *pLsppExtInParams = NULL;
    tLsppExtOutParams  *pLsppExtOutParams = NULL;
    tLsppFsLsppGlobalConfigTableEntry *pLsppGlobalConfigEntry = NULL;
    INT4                i4RetVal = OSIX_FAILURE;

    if ((pLsppExtInParams = (tLsppExtInParams *)
         MemAllocMemBlk (LSPP_FSLSPPINPUTPARAMS_POOLID)) == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((pLsppExtOutParams = (tLsppExtOutParams *)
         MemAllocMemBlk (LSPP_FSLSPPOUTPUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        return OSIX_FAILURE;
    }

    MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
    MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));
    MEMSET (&LsppGlobalConfigEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));

    /*Initialize the Global Config Entry. */
    LsppInitFsLsppGlobalConfigTable (&LsppGlobalConfigEntry);

    /* Get the context name from the VCM module. */
    pLsppExtInParams->u4ContextId = u4ContextId;
    pLsppExtInParams->u4RequestType = LSPP_VCM_GET_CXT_NAME_FRM_ID;

    i4RetVal = LsppPortHandleExtInteraction (pLsppExtInParams,
                                             pLsppExtOutParams);
    if (i4RetVal == OSIX_FAILURE)
    {
        LSPP_LOG (u4ContextId, LSPP_FETCH_FROM_EXT_MOD_FAILED,
                  "LsppCxtCreateGlobalConfig");
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);
        return OSIX_FAILURE;
    }

    MEMCPY (LsppGlobalConfigEntry.au1ContextName,
            pLsppExtOutParams->unOutParams.au1ContextName,
            sizeof (LsppGlobalConfigEntry.au1ContextName));

    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtInParams);
    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtOutParams);

    /* Allocate memory for Global Config Entry for the new context */
    LsppGlobalConfigEntry.MibObject.u4FsLsppContextId = u4ContextId;

    pLsppGlobalConfigEntry =
        LsppFsLsppGlobalConfigTableCreateApi (&LsppGlobalConfigEntry);

    if (pLsppGlobalConfigEntry == NULL)
    {
        LSPP_LOG (u4ContextId, LSPP_GLOBAL_CONFIG_CREATION_FAILED,
                  "LsppCxtCreateGlobalConfig");
        return OSIX_FAILURE;
    }

    /* Initializing with Index Manager */
    i4RetVal = IndexManagerInitList (1, LSPP_MAX_SENDERS_HANDLE,
                                     pLsppGlobalConfigEntry->au1IndexBitMapList,
                                     INDEX_MGR_NON_INCR_TYPE,
                                     &(pLsppGlobalConfigEntry->IndexMgrId));

    if (i4RetVal != INDEX_SUCCESS)
    {
        LSPP_LOG (u4ContextId, LSPP_INDEX_MGR_INIT_FAILED,
                  "LsppCxtCreateGlobalConfig");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*******************************************************************************
 * *                                                                           *
 * * Function     : LsppCxtCreateGlobalStats                                  *
 * *                                                                           *
 * * Description  : Creates the entry in Global Statistics table for           *
 * *                maintaining the context specific global statistics         *
 * *                                                                           *
 * * Input        : u4ContextId - unique identifier for the context.           *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * *                                                                           *
 * ****************************************************************************/

PUBLIC INT4
LsppCxtCreateGlobalStats (UINT4 u4ContextId)
{
    tLsppFsLsppGlobalStatsTableEntry LspppGlobalStatsEntry;
    tLsppFsLsppGlobalStatsTableEntry *pLsppFsLsppGlobalStatsTable = NULL;

    MEMSET (&LspppGlobalStatsEntry, 0,
            sizeof (tLsppFsLsppGlobalStatsTableEntry));

    /*Initialize the Global Stats Entry. */
    LsppInitFsLsppGlobalStatsTable (&LspppGlobalStatsEntry);

    LspppGlobalStatsEntry.MibObject.u4FsLsppContextId = u4ContextId;

    /* Allocate memory for Global Stats Entry for the new context */
    pLsppFsLsppGlobalStatsTable =
        LsppFsLsppGlobalStatsTableCreateApi (&LspppGlobalStatsEntry);

    if (pLsppFsLsppGlobalStatsTable == NULL)
    {
        LSPP_LOG (u4ContextId, LSPP_GLOBAL_STAT_CREATE_FAILED,
                  "LsppCxtCreateGlobalStats");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*******************************************************************************
 * *                                                                           *
 * * Function     : LsppCxtDeleteGlobalConfig                                 *
 * *                                                                           *
 * * Description  : Deletes the entry in Global config Table                   *
 * *                                                                           *
 * * Input        : u4ContextId - unique identifier for the context.           *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * *                                                                           *
 * ****************************************************************************/

PUBLIC INT4
LsppCxtDeleteGlobalConfig (UINT4 u4ContextId)
{
    tLsppFsLsppGlobalConfigTableEntry LsppGlobalConfigEntry;
    tLsppFsLsppGlobalConfigTableEntry *pLsppGlobalConfigEntry = NULL;
    INT4                i4RetVal = (INT4) MEM_FAILURE;

    MEMSET (&LsppGlobalConfigEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));

    LsppGlobalConfigEntry.MibObject.u4FsLsppContextId = u4ContextId;

    pLsppGlobalConfigEntry =
        LsppGetFsLsppGlobalConfigTable (&LsppGlobalConfigEntry);
    if (pLsppGlobalConfigEntry == NULL)
    {
        LSPP_LOG (u4ContextId, LSPP_INVALID_CONTEXT_ID,
                  "LsppCxtDeleteGlobalConfig", u4ContextId);
        return OSIX_FAILURE;
    }
    /* De-Init with Index Manager */
    IndexManagerDeInitList (pLsppGlobalConfigEntry->IndexMgrId);

    /* Remove the entry in the Global Config table. */
    RBTreeRem (gLsppGlobals.LsppGlbMib.FsLsppGlobalConfigTable,
               (tRBElem *) pLsppGlobalConfigEntry);

    i4RetVal = MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                                   (UINT1 *) pLsppGlobalConfigEntry);

    if (i4RetVal == (INT4) MEM_FAILURE)
    {
        LSPP_CMN_TRC (u4ContextId, LSPP_MEM_RELEASE_FAILED,
                      "LsppCxtDeleteGlobalConfig", "Global Config Table");
    }

    return OSIX_SUCCESS;
}

/*******************************************************************************
 * *                                                                           *
 * * Function     : LsppCxtDeleteGlobalStats                                  *
 * *                                                                           *
 * * Description  : Deletes the entry in Global Statistics Table               *
 * *                                                                           *
 * * Input        : u4ContextId - unique identifier for the context.           *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * *                                                                           *
 * ****************************************************************************/

PUBLIC INT4
LsppCxtDeleteGlobalStats (UINT4 u4ContextId)
{
    tLsppFsLsppGlobalStatsTableEntry LsppGlobalStatsEntry;
    tLsppFsLsppGlobalStatsTableEntry *pLsppGlobalStatsEntry = NULL;
    INT4                i4RetVal = (INT4) MEM_FAILURE;

    MEMSET (&LsppGlobalStatsEntry, 0, sizeof (LsppGlobalStatsEntry));

    LsppGlobalStatsEntry.MibObject.u4FsLsppContextId = u4ContextId;

    pLsppGlobalStatsEntry =
        LsppGetFsLsppGlobalStatsTable (&LsppGlobalStatsEntry);
    if (pLsppGlobalStatsEntry == NULL)
    {
        LSPP_LOG (u4ContextId, LSPP_INVALID_CONTEXT_ID,
                  "LsppCxtDeleteGlobalStats", u4ContextId);
        return OSIX_FAILURE;
    }

    /* Remove the entry in the Global Stats table. */
    RBTreeRem (gLsppGlobals.LsppGlbMib.FsLsppGlobalStatsTable,
               (tRBElem *) pLsppGlobalStatsEntry);

    i4RetVal = MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                                   (UINT1 *) pLsppGlobalStatsEntry);
    if (i4RetVal == (INT4) MEM_FAILURE)
    {
        LSPP_CMN_TRC (u4ContextId, LSPP_MEM_RELEASE_FAILED,
                      "LsppCxtDeleteGlobalStats", "Global stats Table");
    }

    return OSIX_SUCCESS;
}

/*******************************************************************************
 * *                                                                           *
 * * Function     : LsppCxtDeleteHopInfoNode                                  *
 * *                                                                           *
 * * Description  : Delete the Hop Info in the database.                       * * *                                                                           *
 * * Input        : pLsppHopInfoEntry - pointer to the Hop Info Entry.         *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * *                                                                           *
 * ****************************************************************************/

PUBLIC VOID
LsppCxtDeleteHopInfoNode (tLsppFsLsppHopTableEntry * pLsppHopInfoEntry)
{
    INT4                i4RetVal = (INT4) MEM_FAILURE;

    RBTreeRem (gLsppGlobals.LsppGlbMib.FsLsppHopTable,
               (tRBElem *) pLsppHopInfoEntry);

    i4RetVal = MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                                   (UINT1 *) pLsppHopInfoEntry);
    if (i4RetVal == (INT4) MEM_FAILURE)
    {
        LSPP_CMN_TRC (pLsppHopInfoEntry->MibObject.u4FsLsppContextId,
                      LSPP_MEM_RELEASE_FAILED, "LsppCxtDeleteHopInfoNode",
                      "Hop Info Table");
    }

    return;
}

/*******************************************************************************
 * *                                                                           *
 * * Function     : LsppCxtDeleteHopInfoEntries                                *
 * *                                                                           *
 * * Description  : Deletes the Path information for the given Ping Trace      *
 * *                Instance.                                                  *
 * * Input        : u4ContextId - unique identifier for the context.           *
 * *                u4SenderHandle - identifer which uniquelly identifies a    *
 * *                                 ping or traceroute instance in a context. *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : None                                                       *
 * *                                                                           *
 * ****************************************************************************/

PUBLIC VOID
LsppCxtDeleteHopInfoEntries (UINT4 u4ContextId, UINT4 u4SenderHandle)
{
    tLsppFsLsppHopTableEntry *pLsppCurrentHopEntry = NULL;
    tLsppFsLsppHopTableEntry *pLsppNextHopEntry = NULL;

    pLsppCurrentHopEntry = (tLsppFsLsppHopTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPHOPTABLE_POOLID);
    if (pLsppCurrentHopEntry == NULL)
    {
        return;
    }
    MEMSET (pLsppCurrentHopEntry, 0, sizeof (tLsppFsLsppHopTableEntry));

    pLsppCurrentHopEntry->MibObject.u4FsLsppContextId = u4ContextId;
    pLsppCurrentHopEntry->MibObject.u4FsLsppSenderHandle = u4SenderHandle;

    while ((pLsppNextHopEntry =
            LsppGetNextFsLsppHopTable (pLsppCurrentHopEntry)) != NULL)
    {
        if ((pLsppNextHopEntry->MibObject.u4FsLsppContextId != u4ContextId) ||
            (pLsppNextHopEntry->MibObject.u4FsLsppSenderHandle !=
             u4SenderHandle))
        {
            /* No more entry exists for this PingTrace instance in the 
             * database. 
             */
            break;
        }

        pLsppCurrentHopEntry->MibObject.u4FsLsppContextId =
            pLsppNextHopEntry->MibObject.u4FsLsppContextId;
        pLsppCurrentHopEntry->MibObject.u4FsLsppSenderHandle =
            pLsppNextHopEntry->MibObject.u4FsLsppSenderHandle;
        pLsppCurrentHopEntry->MibObject.u4FsLsppHopIndex =
            pLsppNextHopEntry->MibObject.u4FsLsppHopIndex;

        /* Delete the Echo Sequence Node in the Echo Sequence Table. */
        LsppCxtDeleteHopInfoNode (pLsppNextHopEntry);

        pLsppNextHopEntry = NULL;

    }

    MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                        (UINT1 *) pLsppCurrentHopEntry);
    return;
}

/*******************************************************************************
 * *                                                                           *
 * * Function     : LsppCxtDeleteEchoSeqNode                                  *
 * *                                                                           *
 * * Description  : Delete the Echo Sequence Node in the database.             * * *                                                                           *
 * * Input        : pLsppEchoSeqEntry - pointer to the Echo Sequence Entry.    *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * *                                                                           *
 * ****************************************************************************/

PUBLIC VOID
LsppCxtDeleteEchoSeqNode (tLsppFsLsppEchoSequenceTableEntry * pLsppEchoSeqEntry)
{
    INT4                i4RetVal = (INT4) MEM_FAILURE;

    RBTreeRem (gLsppGlobals.LsppGlbMib.FsLsppEchoSequenceTable,
               (tRBElem *) pLsppEchoSeqEntry);

    i4RetVal = MemReleaseMemBlock (LSPP_FSLSPPECHOSEQUENCETABLE_POOLID,
                                   (UINT1 *) pLsppEchoSeqEntry);
    if (i4RetVal == (INT4) MEM_FAILURE)
    {
        LSPP_CMN_TRC (pLsppEchoSeqEntry->MibObject.u4FsLsppContextId,
                      LSPP_MEM_RELEASE_FAILED, "LsppCxtDeleteEchoSeqNode",
                      "Echo Seq Table");
    }

    return;
}

/*******************************************************************************
 * *                                                                           *
 * * Function     : LsppCxtDeleteEchoSequences                                *
 * *                                                                           *
 * * Description  : Deletes the Echo sequences in the Echo Sequence Table for  *
 * *                the given Ping Trace Instance.                             *
 * * Input        : u4ContextId - unique identifier for the context.           *
 * *                u4SenderHandle - identifer which uniquelly identifies a    *
 * *                                 ping or traceroute instance in a context. *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : None                                                       *
 * *                                                                           *
 * ****************************************************************************/
PUBLIC VOID
LsppCxtDeleteEchoSequences (UINT4 u4ContextId, UINT4 u4SenderHandle)
{
    tLsppFsLsppEchoSequenceTableEntry LsppCurrentEchoSeqEntry;
    tLsppFsLsppEchoSequenceTableEntry *pLsppNextEchoSeqEntry = NULL;

    MEMSET (&LsppCurrentEchoSeqEntry, 0, sizeof (LsppCurrentEchoSeqEntry));

    LsppCurrentEchoSeqEntry.MibObject.u4FsLsppContextId = u4ContextId;
    LsppCurrentEchoSeqEntry.MibObject.u4FsLsppSenderHandle = u4SenderHandle;

    while ((pLsppNextEchoSeqEntry = LsppGetNextFsLsppEchoSequenceTable
            (&LsppCurrentEchoSeqEntry)) != NULL)
    {
        if ((pLsppNextEchoSeqEntry->MibObject.u4FsLsppContextId !=
             u4ContextId) ||
            (pLsppNextEchoSeqEntry->MibObject.u4FsLsppSenderHandle !=
             u4SenderHandle))
        {
            /* No more entry exists for this PingTrace instance in the 
             * database. 
             */
            break;
        }

        if (pLsppNextEchoSeqEntry->u1Status == LSPP_ECHO_SEQ_IN_PROGRESS)
        {
            /*Stop the Wait-For-Reply Timer running for this echo sequence. */
            LsppTmrStopTmr (&pLsppNextEchoSeqEntry->WFRTimer);
        }

        LsppCurrentEchoSeqEntry.MibObject.u4FsLsppContextId =
            pLsppNextEchoSeqEntry->MibObject.u4FsLsppContextId;
        LsppCurrentEchoSeqEntry.MibObject.u4FsLsppSenderHandle =
            pLsppNextEchoSeqEntry->MibObject.u4FsLsppSenderHandle;
        LsppCurrentEchoSeqEntry.MibObject.u4FsLsppSequenceNumber =
            pLsppNextEchoSeqEntry->MibObject.u4FsLsppSequenceNumber;

        /* Delete the Echo Sequence Node in the Echo Sequence Table. */
        LsppCxtDeleteEchoSeqNode (pLsppNextEchoSeqEntry);

        pLsppNextEchoSeqEntry = NULL;

    }

    return;
}

/*******************************************************************************
 * *                                                                           *
 * * Function     : LsppCxtDeletePingTraceInfo                                *
 * *                                                                           *
 * * Description  : Deletes the Ping Trace Echo Sequences and Hop Information. * * *                                                                           *
 * * Input        : pLsppPingTraceEntry - pointer to the Ping Trace Entry.     *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * *                                                                           *
 * ****************************************************************************/

PUBLIC INT4
LsppCxtDeletePingTraceInfo (tLsppFsLsppPingTraceTableEntry *
                            pLsppPingTraceEntry)
{

    /* Delete all the echo sequences of this ping or traceroute instance. */
    LsppCxtDeleteEchoSequences
        (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
         pLsppPingTraceEntry->MibObject.u4FsLsppSenderHandle);

    /* Delete all the Hop information of this ping or traceroute instance. */
    LsppCxtDeleteHopInfoEntries
        (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
         pLsppPingTraceEntry->MibObject.u4FsLsppSenderHandle);

    if (pLsppPingTraceEntry->MibObject.i4FsLsppStatus == LSPP_ECHO_IN_PROGRESS)
    {
        /* Stop the Wait-To-Send timer for this Ping/Traceroute instance. */
        LsppTmrStopTmr (&(pLsppPingTraceEntry->WTSTimer));
    }
    else
    {
        /* Need to check whether Ageout is configured to a valid value. */

        /* Stop the Age-Out timer for this Ping/Traceroute instance. */
        LsppTmrStopTmr (&pLsppPingTraceEntry->AgeOutTimer);
    }

    if ((pLsppPingTraceEntry->MibObject.i4FsLsppRequestOwner ==
         LSPP_REQ_OWNER_MGMT) &&
        (pLsppPingTraceEntry->u1RequestOwnerSubType == LSPP_REQ_OWNER_MGMT_CLI))
    {
        OsixSemDel (pLsppPingTraceEntry->SemId);
    }

    return OSIX_SUCCESS;
}

/*******************************************************************************
 * *                                                                           *
 * * Function     : LsppCxtDeletePingTraceNode                                *
 * *                                                                           *
 * * Description  : Delete the Ping Trace Node in the database.                * * *                                                                           *
 * * Input        : pLsppPingTraceEntry - pointer to the Ping Trace Entry.     *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * *                                                                           *
 * ****************************************************************************/

PUBLIC INT4
LsppCxtDeletePingTraceNode (tLsppFsLsppPingTraceTableEntry *
                            pLsppPingTraceEntry)
{
    tLsppFsLsppGlobalConfigTableEntry LsppGlobalConfigEntry;
    tLsppFsLsppGlobalConfigTableEntry *pLsppGlobalConfigEntry = NULL;
    INT4                i4RetVal = (INT4) MEM_FAILURE;

    MEMSET (&LsppGlobalConfigEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));

    LsppGlobalConfigEntry.MibObject.u4FsLsppContextId =
        pLsppPingTraceEntry->MibObject.u4FsLsppContextId;

    pLsppGlobalConfigEntry =
        LsppGetFsLsppGlobalConfigTable (&LsppGlobalConfigEntry);

    if (pLsppGlobalConfigEntry == NULL)
    {
        LSPP_LOG (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
                  LSPP_INVALID_CONTEXT_ID,
                  "LsppCxtDeletePingTraceNode",
                  pLsppPingTraceEntry->MibObject.u4FsLsppContextId);
        return OSIX_FAILURE;
    }

    IndexManagerFreeIndex (pLsppGlobalConfigEntry->IndexMgrId,
                           pLsppPingTraceEntry->MibObject.u4FsLsppSenderHandle);

    RBTreeRem (gLsppGlobals.LsppGlbMib.FsLsppPingTraceTable,
               (tRBElem *) pLsppPingTraceEntry);

    i4RetVal = MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                   (UINT1 *) pLsppPingTraceEntry);
    if (i4RetVal == (INT4) MEM_FAILURE)
    {
        LSPP_CMN_TRC (pLsppPingTraceEntry->MibObject.u4FsLsppContextId,
                      LSPP_MEM_RELEASE_FAILED, "LsppCxtDeletePingTraceNode",
                      "Ping Trace Node");

        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*******************************************************************************
 * *                                                                           *
 * * Function     : LsppCxtDeletePingTraceEntries                             *
 * *                                                                           *
 * * Description  : Deletes all the Ping Trace Entries in the Ping Trace Table.*
 * *                                                                           *
 * * Input        : u4ContextId - unique identifier for the context.           *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : None                                                       *
 * *                                                                           *
 * ****************************************************************************/

PUBLIC VOID
LsppCxtDeletePingTraceEntries (UINT4 u4ContextId)
{
    tLsppFsLsppPingTraceTableEntry *pLsppCurrentPingTraceEntry = NULL;
    tLsppFsLsppPingTraceTableEntry *pLsppNextPingTraceEntry = NULL;
    INT4                i4RetVal = OSIX_FAILURE;

    pLsppCurrentPingTraceEntry = (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppCurrentPingTraceEntry == NULL)
    {
        return;
    }

    MEMSET (pLsppCurrentPingTraceEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    pLsppCurrentPingTraceEntry->MibObject.u4FsLsppContextId = u4ContextId;

    while ((pLsppNextPingTraceEntry = LsppGetNextFsLsppPingTraceTable
            (pLsppCurrentPingTraceEntry)) != NULL)
    {
        if (pLsppNextPingTraceEntry->MibObject.u4FsLsppContextId != u4ContextId)
        {
            /* No more entry exists for this context in the database. */
            break;
        }

        /* Delete the Echo Sequences and Hop Info of this Ping Trace entry. */
        i4RetVal = LsppCxtDeletePingTraceInfo (pLsppNextPingTraceEntry);
        if (i4RetVal == OSIX_FAILURE)
        {
            LSPP_LOG (pLsppNextPingTraceEntry->MibObject.u4FsLsppContextId,
                      LSPP_PING_TRACE_INFO_DELETE_FAIL,
                      "LsppCxtDeletePingTraceEntries");
        }

        pLsppCurrentPingTraceEntry->MibObject.u4FsLsppContextId =
            pLsppNextPingTraceEntry->MibObject.u4FsLsppContextId;

        pLsppCurrentPingTraceEntry->MibObject.u4FsLsppSenderHandle =
            pLsppNextPingTraceEntry->MibObject.u4FsLsppSenderHandle;

        /* Delete the Ping Trace Node from the database. */
        LsppCxtDeletePingTraceNode (pLsppNextPingTraceEntry);

        pLsppNextPingTraceEntry = NULL;

    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppCurrentPingTraceEntry);
    return;
}

/*******************************************************************************
 * *                                                                           *
 * * Function     : LsppCxtCreateContext                                      *
 * *                                                                           *
 * * Description  : Creates the context in the LSP Ping module                 *
 * *                                                                           *
 * * Input        : u4ContextId - unique identifier for the context.           *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * *                                                                           *
 * ****************************************************************************/

PUBLIC INT4
LsppCxtCreateContext (UINT4 u4ContextId)
{
    INT4                i4RetVal = OSIX_FAILURE;

    /*Create an entry in Global Config table. */
    i4RetVal = LsppCxtCreateGlobalConfig (u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        LSPP_LOG (u4ContextId, LSPP_CREATE_GLOBAL_CONFIG_FAILED,
                  "LsppCxtCreateContext");
        return OSIX_FAILURE;
    }

    /*Create an entry in Global stats table. */
    i4RetVal = LsppCxtCreateGlobalStats (u4ContextId);
    if (i4RetVal == OSIX_FAILURE)
    {
        LSPP_LOG (u4ContextId, LSPP_CREATE_GLOBAL_STATS_FAILED,
                  "LsppCxtCreateContext");
        LsppCxtDeleteGlobalConfig (u4ContextId);
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}

/*******************************************************************************
 * *                                                                           *
 * * Function     : LsppCxtDeleteContext                                      *
 * *                                                                           *
 * * Description  : Deletes the context in the LSP Ping module                 *
 * *                                                                           *
 * * Input        : u4ContextId - unique identifier for the context.           *
 * *                                                                           *
 * * Output       : None                                                       *
 * *                                                                           *
 * * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 * *                                                                           *
 * ****************************************************************************/

PUBLIC INT4
LsppCxtDeleteContext (UINT4 u4ContextId, BOOL1 bIsVcm)
{
    tLsppFsLsppGlobalConfigTableEntry LsppGlobalConfigEntry;
    tLsppFsLsppGlobalStatsTableEntry LsppGlobalStatsEntry;
    tLsppFsLsppGlobalConfigTableEntry *pLsppGlobalConfigEntry = NULL;
    tLsppFsLsppGlobalStatsTableEntry *pLsppGlobalStatsEntry = NULL;
    INT4                i4RetVal = OSIX_FAILURE;

    MEMSET (&LsppGlobalConfigEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (&LsppGlobalStatsEntry, 0,
            sizeof (tLsppFsLsppGlobalStatsTableEntry));

    /*Delete the PingTrace Entries for the context. */
    LsppCxtDeletePingTraceEntries (u4ContextId);

    if (bIsVcm == OSIX_TRUE)
    {
        /*Delete the Global Config Entry for the context. */
        i4RetVal = LsppCxtDeleteGlobalConfig (u4ContextId);
        if (i4RetVal == OSIX_FAILURE)
        {
            LSPP_LOG (u4ContextId, LSPP_DELETE_GLOBAL_CONFIG_FAIL,
                      "LsppCxtDeleteContext");
            return OSIX_FAILURE;
        }

        /*Delete the Global stats Entry for the context. */
        i4RetVal = LsppCxtDeleteGlobalStats (u4ContextId);
        if (i4RetVal == OSIX_FAILURE)
        {
            LSPP_LOG (u4ContextId, LSPP_DELETE_GLOBAL_STAS_FAIL,
                      "LsppCxtDeleteContext");
            return OSIX_FAILURE;
        }
    }

    else
    {
        LsppGlobalConfigEntry.MibObject.u4FsLsppContextId = u4ContextId;

        pLsppGlobalConfigEntry =
            LsppGetFsLsppGlobalConfigTable (&LsppGlobalConfigEntry);

        if (pLsppGlobalConfigEntry == NULL)
        {
            return OSIX_FAILURE;
        }

        /* ReInitializing Global Config objects to default value */
        LsppInitMibGlobalConfigTable (pLsppGlobalConfigEntry);
        pLsppGlobalConfigEntry->MibObject.i4FsLsppSystemControl = LSPP_SHUTDOWN;

        /* ReInitializing Global Stats Value */
        LsppGlobalStatsEntry.MibObject.u4FsLsppContextId = u4ContextId;
        pLsppGlobalStatsEntry =
            LsppGetFsLsppGlobalStatsTable (&LsppGlobalStatsEntry);

        if (pLsppGlobalStatsEntry == NULL)
        {
            return OSIX_FAILURE;
        }

        LsppUtilClearEchoStatistics (pLsppGlobalStatsEntry);
    }

    return OSIX_SUCCESS;
}

#endif /* _LSPPCXT_C_ */
