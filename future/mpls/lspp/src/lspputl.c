/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: lspputl.c,v 1.20 2016/06/03 10:27:23 siva Exp $
*
* Description: This file contains utility functions used by protocol Lspp
*********************************************************************/

#include "lsppinc.h"
#include "lsppclig.h"
/****************************************************************************
 Function    :  LsppGetAllUtlFsLsppGlobalConfigTable
 Input       :  pLsppGetFsLsppGlobalConfigTableEntry
                pLsppdsFsLsppGlobalConfigTableEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
LsppGetAllUtlFsLsppGlobalConfigTable (tLsppFsLsppGlobalConfigTableEntry *
                                      pLsppGetFsLsppGlobalConfigTableEntry,
                                      tLsppFsLsppGlobalConfigTableEntry *
                                      pLsppdsFsLsppGlobalConfigTableEntry)
{
    MEMCPY (&(pLsppGetFsLsppGlobalConfigTableEntry->IndexMgrId),
            &(pLsppdsFsLsppGlobalConfigTableEntry->IndexMgrId),
            sizeof (tIndexMgrId));

    MEMCPY (&(pLsppGetFsLsppGlobalConfigTableEntry->au1IndexBitMapList),
            &(pLsppdsFsLsppGlobalConfigTableEntry->au1IndexBitMapList),
            LSPP_SENDER_HANDLE_INDEX_MGR_BITMAP_LEN);

    MEMCPY (&(pLsppGetFsLsppGlobalConfigTableEntry->au1ContextName),
            &(pLsppdsFsLsppGlobalConfigTableEntry->au1ContextName),
            LSPP_MAX_CONTEXT_NAME_LEN);

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  LsppUtilUpdateFsLsppGlobalConfigTable
 * Input       :   pLsppOldFsLsppGlobalConfigTableEntry
                   pLsppFsLsppGlobalConfigTableEntry
                   pLsppIsSetFsLsppGlobalConfigTableEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
LsppUtilUpdateFsLsppGlobalConfigTable (tLsppFsLsppGlobalConfigTableEntry *
                                       pLsppOldFsLsppGlobalConfigTableEntry,
                                       tLsppFsLsppGlobalConfigTableEntry *
                                       pLsppFsLsppGlobalConfigTableEntry,
                                       tLsppIsSetFsLsppGlobalConfigTableEntry *
                                       pLsppIsSetFsLsppGlobalConfigTableEntry)
{
    tLsppFsLsppGlobalStatsTableEntry LsppGlobalStatsTableEntry;
    tLsppFsLsppGlobalStatsTableEntry *pLsppFsLsppGlobalStatsTableEntry = NULL;

    MEMSET (&LsppGlobalStatsTableEntry, 0,
            sizeof (tLsppFsLsppGlobalStatsTableEntry));

    LsppGlobalStatsTableEntry.MibObject.u4FsLsppContextId =
        pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId;

    /* Check the clear echo stats value */
    if (pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppClearEchoStats ==
        LSPP_SNMP_TRUE)
    {
        pLsppFsLsppGlobalStatsTableEntry = LsppGetFsLsppGlobalStatsTable
            (&LsppGlobalStatsTableEntry);

        if (pLsppFsLsppGlobalStatsTableEntry == NULL)
        {
            LSPP_LOG (pLsppFsLsppGlobalConfigTableEntry->MibObject.
                      u4FsLsppContextId,
                      LSPP_GLOBAL_STATS_ENTRY_NOT_EXIST,
                      "LsppUtilUpdateFsLsppGlobalConfigTable",
                      pLsppFsLsppGlobalConfigTableEntry->MibObject.
                      u4FsLsppContextId);
            return OSIX_FAILURE;
        }

        LsppUtilClearEchoStatistics (pLsppFsLsppGlobalStatsTableEntry);

        /* After cleared the global statistics, set the Clear echo stats to
         * false again
         * */
        pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppClearEchoStats =
            LSPP_SNMP_FALSE;
    }

    /* check the new age-out time with old age-out time 
     * If new time is different from old one, restart the age-out time with
     * new time value */

    if ((pLsppOldFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppAgeOutTime !=
         pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppAgeOutTime) ||
        (pLsppOldFsLsppGlobalConfigTableEntry->MibObject.
         i4FsLsppAgeOutTmrUnit !=
         pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppAgeOutTmrUnit))
    {
        if (LsppTmrRestartAgeOutTimer (pLsppFsLsppGlobalConfigTableEntry,
                                       pLsppOldFsLsppGlobalConfigTableEntry) !=
            OSIX_SUCCESS)
        {
            LSPP_LOG (pLsppFsLsppGlobalConfigTableEntry->MibObject.
                      u4FsLsppContextId,
                      LSPP_RESTART_AGE_OUT_TIMER_FAILED,
                      "LsppUtilUpdateFsLsppGlobalConfigTable");
            return OSIX_FAILURE;
        }
    }

    /* Delete all the pingTrace entries, EchoSequence, HopInfo entries, 
     * and initialize the GLobal Config table and Global Stats table to default
     * values, if system control is SHUTDOWN */

    if ((pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppSystemControl ==
         OSIX_TRUE) &&
        (pLsppFsLsppGlobalConfigTableEntry->MibObject.
         i4FsLsppSystemControl == LSPP_SHUTDOWN))
    {
        if (LsppCxtDeleteContext (pLsppFsLsppGlobalConfigTableEntry->MibObject.
                                  u4FsLsppContextId, OSIX_FALSE) !=
            OSIX_SUCCESS)
        {
            LSPP_LOG (pLsppFsLsppGlobalConfigTableEntry->MibObject.
                      u4FsLsppContextId, LSPP_CXT_DELETION_FAILED,
                      "LsppUtilUpdateFsLsppGlobalConfigTable");
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  LsppGetAllUtlFsLsppGlobalStatsTable
 Input       :  pLsppGetFsLsppGlobalStatsTable
                pLsppdsFsLsppGlobalStatsTable
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
LsppGetAllUtlFsLsppGlobalStatsTable (tLsppFsLsppGlobalStatsTableEntry *
                                     pLsppGetFsLsppGlobalStatsTableEntry,
                                     tLsppFsLsppGlobalStatsTableEntry *
                                     pLsppdsFsLsppGlobalStatsTableEntry)
{
    UNUSED_PARAM (pLsppGetFsLsppGlobalStatsTableEntry);
    UNUSED_PARAM (pLsppdsFsLsppGlobalStatsTableEntry);
    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : LsppUtlConvertStringtoOid
* Input       : pLsppFsLsppPingTraceTableEntry,
*               pau1FsLsppPathPointer,
*               pau1FsLsppPathPointerLen
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/

INT4
LsppUtlConvertStringtoOid (tLsppFsLsppPingTraceTableEntry *
                           pLsppFsLsppPingTraceTableEntry,
                           UINT4 *pau1FsLsppPathPointer,
                           INT4 *pau1FsLsppPathPointerLen)
{

    MEMCPY (pLsppFsLsppPingTraceTableEntry->MibObject.au4FsLsppPathPointer,
            pau1FsLsppPathPointer,
            ((sizeof (UINT4)) * (*pau1FsLsppPathPointerLen)));

    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppPathPointerLen =
        *pau1FsLsppPathPointerLen;
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  LsppGetAllUtlFsLsppPingTraceTable
 Input       :  pLsppGetFsLsppPingTraceTableEntry
                pLsppdsFsLsppPingTraceTableEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
LsppGetAllUtlFsLsppPingTraceTable (tLsppFsLsppPingTraceTableEntry *
                                   pLsppGetFsLsppPingTraceTableEntry,
                                   tLsppFsLsppPingTraceTableEntry *
                                   pLsppdsFsLsppPingTraceTableEntry)
{
    MEMCPY (&(pLsppGetFsLsppPingTraceTableEntry->SemId),
            &(pLsppdsFsLsppPingTraceTableEntry->SemId), sizeof (tOsixSemId));

    MEMCPY (&(pLsppGetFsLsppPingTraceTableEntry->AgeOutTimer),
            &(pLsppdsFsLsppPingTraceTableEntry->AgeOutTimer), sizeof (tTmrBlk));

    MEMCPY (&(pLsppGetFsLsppPingTraceTableEntry->WTSTimer),
            &(pLsppdsFsLsppPingTraceTableEntry->WTSTimer), sizeof (tTmrBlk));

    MEMCPY (&(pLsppGetFsLsppPingTraceTableEntry->PathId),
            &(pLsppdsFsLsppPingTraceTableEntry->PathId), sizeof (tLsppPathId));

    pLsppGetFsLsppPingTraceTableEntry->u4BfdSessionIndex =
        pLsppdsFsLsppPingTraceTableEntry->u4BfdSessionIndex;

    pLsppGetFsLsppPingTraceTableEntry->u4LastGeneratedSeqNum =
        pLsppdsFsLsppPingTraceTableEntry->u4LastGeneratedSeqNum;

    pLsppGetFsLsppPingTraceTableEntry->u4LastUpdatedSeqIndex =
        pLsppdsFsLsppPingTraceTableEntry->u4LastUpdatedSeqIndex;

    pLsppGetFsLsppPingTraceTableEntry->u4SuccessCount =
        pLsppdsFsLsppPingTraceTableEntry->u4SuccessCount;

    pLsppGetFsLsppPingTraceTableEntry->u4ReqCompletedCount =
        pLsppdsFsLsppPingTraceTableEntry->u4ReqCompletedCount;

    pLsppGetFsLsppPingTraceTableEntry->u1LastUpdatedHopIndex =
        pLsppdsFsLsppPingTraceTableEntry->u1LastUpdatedHopIndex;

    pLsppGetFsLsppPingTraceTableEntry->u1LastSentTtl =
        pLsppdsFsLsppPingTraceTableEntry->u1LastSentTtl;

    pLsppGetFsLsppPingTraceTableEntry->u1RequestOwnerSubType =
        pLsppdsFsLsppPingTraceTableEntry->u1RequestOwnerSubType;

    pLsppGetFsLsppPingTraceTableEntry->u1MegOperatorType =
        pLsppdsFsLsppPingTraceTableEntry->u1MegOperatorType;

    return OSIX_SUCCESS;
}

/****************************************************************************
 * Function    :  LsppUtilUpdateFsLsppPingTraceTable
 * Input       :   pLsppOldFsLsppPingTraceTableEntry
                   pLsppFsLsppPingTraceTableEntry
                   pLsppIsSetFsLsppPingTraceTableEntry
 * Descritpion :  This Routine checks set value 
                  with that of the value in database
                  and do the necessary protocol operation
 * Output      :  None
 * Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
LsppUtilUpdateFsLsppPingTraceTable (tLsppFsLsppPingTraceTableEntry *
                                    pLsppOldFsLsppPingTraceTableEntry,
                                    tLsppFsLsppPingTraceTableEntry *
                                    pLsppFsLsppPingTraceTableEntry,
                                    tLsppIsSetFsLsppPingTraceTableEntry *
                                    pLsppIsSetFsLsppPingTraceTableEntry)
{
    tLsppFsLsppGlobalConfigTableEntry LsppGlobalConfigEntry;
    tLsppExtInParams   *pLsppExtInParams = NULL;
    tLsppExtOutParams  *pLsppExtOutParams = NULL;
    UINT1               au1SemName[LSPP_MAX_NAME_LEN];
   	UINT4		u4ErrFlag = FALSE;
    UNUSED_PARAM (pLsppIsSetFsLsppPingTraceTableEntry);

    if ((pLsppExtInParams = (tLsppExtInParams *)
         MemAllocMemBlk (LSPP_FSLSPPINPUTPARAMS_POOLID)) == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((pLsppExtOutParams = (tLsppExtOutParams *)
         MemAllocMemBlk (LSPP_FSLSPPOUTPUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        return OSIX_FAILURE;
    }

    MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
    MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));
    MEMSET (&(LsppGlobalConfigEntry), 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (au1SemName, 0, LSPP_MAX_NAME_LEN);

    /* When the Row Status is Active, Check whether mandatory fields are 
     * present and send a trigger to send echo request */

    if (pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRowStatus == ACTIVE)
    {
        /* Check whether mandatory fields are present in the table */

        if ((pLsppFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppRequestType == 0) ||
            (pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppPathType == 0))
        {
            LSPP_LOG (pLsppFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      LSPP_MANDATORY_FIELDS_NOT_PRESENT,
                      "LsppUtilUpdateFsLsppPingTraceTable");
            MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtInParams);
            MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtOutParams);
            return OSIX_FAILURE;
        }

        if (pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppSweepOption ==
            LSPP_SNMP_TRUE)
        {
            if (pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepMinimum >
                pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepMaximum)
            {
                LSPP_LOG (pLsppFsLsppPingTraceTableEntry->MibObject.
                          u4FsLsppContextId, LSPP_SWEEP_MIN_GREATER_THAN_MAX,
                          "LsppUtilUpdateFsLsppPingTraceTable");
                MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                    (UINT1 *) pLsppExtInParams);
                MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pLsppExtOutParams);
                return OSIX_FAILURE;
            }
        }

        /* Depends on the path type call external API's and fill the
         * PathInfo's in data base */

        pLsppFsLsppPingTraceTableEntry->PathId.u1PathType =
            (UINT1) pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppPathType;

        switch (pLsppFsLsppPingTraceTableEntry->PathId.u1PathType)
        {
            case LSPP_PATH_TYPE_LDP_IPV4:
            case LSPP_PATH_TYPE_LDP_IPV6:

                pLsppExtInParams->u4ContextId =
                    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId;
                pLsppExtInParams->u4RequestType =
                    LSPP_MPLSDB_GET_LDP_INFO_FRM_FTN_INDEX;

                pLsppExtInParams->uInParams.u4FtnIndex =
                    pLsppFsLsppPingTraceTableEntry->MibObject.
                    au4FsLsppPathPointer[pLsppFsLsppPingTraceTableEntry->
                                         MibObject.i4FsLsppPathPointerLen - 1];

                if (LsppPortHandleExtInteraction (pLsppExtInParams,
                                                  pLsppExtOutParams) !=
                    OSIX_SUCCESS)
                {
                    LSPP_LOG (pLsppFsLsppPingTraceTableEntry->MibObject.
                              u4FsLsppContextId, LSPP_GET_PATH_ID_FAILED,
                              "LsppUtilUpdateFsLsppPingTraceTable", "LDP");
                    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                        (UINT1 *) pLsppExtInParams);
                    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                        (UINT1 *) pLsppExtOutParams);
                    return OSIX_FAILURE;
                }

                pLsppFsLsppPingTraceTableEntry->PathId.unPathId.LdpInfo.
                    u4AddrType =
                    pLsppExtOutParams->OutLdpInfo.LdpInfo.u4AddrType;

                MEMCPY (&(pLsppFsLsppPingTraceTableEntry->PathId.unPathId.
                          LdpInfo.LdpPrefix),
                        &(pLsppExtOutParams->OutLdpInfo.LdpInfo.LdpPrefix),
                        sizeof (tIpAddr));

                pLsppFsLsppPingTraceTableEntry->PathId.unPathId.LdpInfo.
                    u4PrefixLength =
                    pLsppExtOutParams->OutLdpInfo.LdpInfo.u4PrefixLength;

                if (pLsppFsLsppPingTraceTableEntry->MibObject.
                    i4FsLsppEncapType == 0)
                {
                    pLsppFsLsppPingTraceTableEntry->MibObject.
                        i4FsLsppEncapType = LSPP_ENCAP_TYPE_IP;
                }

                if (pLsppFsLsppPingTraceTableEntry->MibObject.
                    i4FsLsppReplyMode == 0)
                {
                    pLsppFsLsppPingTraceTableEntry->MibObject.
                        i4FsLsppReplyMode = LSPP_REPLY_IP_UDP;
                }

                break;

            case LSPP_PATH_TYPE_RSVP_IPV4:

                pLsppFsLsppPingTraceTableEntry->PathId.unPathId.TnlInfo.
                    DstNodeId.MplsRouterId.u4_addr[0] =
                    pLsppFsLsppPingTraceTableEntry->MibObject.
                    au4FsLsppPathPointer[pLsppFsLsppPingTraceTableEntry->
                                         MibObject.i4FsLsppPathPointerLen -
                                         LSPP_OID_OFFSET_RSVPTE_EGRESS_ID];

                pLsppFsLsppPingTraceTableEntry->PathId.unPathId.TnlInfo.
                    SrcNodeId.MplsRouterId.u4_addr[0] =
                    pLsppFsLsppPingTraceTableEntry->MibObject.
                    au4FsLsppPathPointer[pLsppFsLsppPingTraceTableEntry->
                                         MibObject.i4FsLsppPathPointerLen -
                                         LSPP_OID_OFFSET_RSVPTE_INGRESS_ID];

                pLsppFsLsppPingTraceTableEntry->PathId.unPathId.TnlInfo.
                    u4LspId =
                    pLsppFsLsppPingTraceTableEntry->MibObject.
                    au4FsLsppPathPointer[pLsppFsLsppPingTraceTableEntry->
                                         MibObject.i4FsLsppPathPointerLen -
                                         LSPP_OID_OFFSET_RSVPTE_LSP_ID];

                pLsppFsLsppPingTraceTableEntry->PathId.unPathId.TnlInfo.
                    u4SrcTnlId =
                    pLsppFsLsppPingTraceTableEntry->MibObject.
                    au4FsLsppPathPointer[pLsppFsLsppPingTraceTableEntry->
                                         MibObject.i4FsLsppPathPointerLen -
                                         LSPP_OID_OFFSET_RSVPTE_TNL_ID];

                pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppPathType =
                    LSPP_PATH_TYPE_RSVP_IPV4;

                pLsppFsLsppPingTraceTableEntry->PathId.u1PathType =
                    LSPP_PATH_TYPE_RSVP_IPV4;

                if (pLsppFsLsppPingTraceTableEntry->MibObject.
                    i4FsLsppEncapType == 0)
                {
                    pLsppFsLsppPingTraceTableEntry->MibObject.
                        i4FsLsppEncapType = LSPP_ENCAP_TYPE_IP;
                }

                if (pLsppFsLsppPingTraceTableEntry->MibObject.
                    i4FsLsppReplyMode == 0)
                {
                    pLsppFsLsppPingTraceTableEntry->MibObject.
                        i4FsLsppReplyMode = LSPP_REPLY_IP_UDP;
                }
                break;

            case LSPP_PATH_TYPE_RSVP_IPV6:

                /* Currently it is not suppoertde */
                break;

            case LSPP_PATH_TYPE_FEC_128:
            case LSPP_PATH_TYPE_FEC_129:

                pLsppExtInParams->u4ContextId =
                    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId;

                pLsppExtInParams->u4RequestType =
                    LSPP_L2VPN_GET_PW_INFO_FRM_PW_INDEX;

                pLsppExtInParams->InPwFecInfo.u4PwIndex =
                    pLsppFsLsppPingTraceTableEntry->MibObject.
                    au4FsLsppPathPointer[pLsppFsLsppPingTraceTableEntry->
                                         MibObject.i4FsLsppPathPointerLen - 1];

                if (LsppPortHandleExtInteraction (pLsppExtInParams,
                                                  pLsppExtOutParams) !=
                    OSIX_SUCCESS)
                {
                    LSPP_LOG (pLsppFsLsppPingTraceTableEntry->MibObject.
                              u4FsLsppContextId, LSPP_GET_PATH_ID_FAILED,
                              "LsppUtilUpdateFsLsppPingTraceTable", "PW");
                    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                        (UINT1 *) pLsppExtInParams);
                    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                        (UINT1 *) pLsppExtOutParams);
                    return OSIX_FAILURE;
                }

                MEMCPY (&(pLsppFsLsppPingTraceTableEntry->PathId.unPathId.
                          PwInfo.PeerAddr),
                        &(pLsppExtOutParams->OutPwDetail.PwFecInfo.DstNodeId.
                          MplsRouterId), sizeof (tIpAddr));

                pLsppFsLsppPingTraceTableEntry->PathId.
                    unPathId.PwInfo.u4PwVcId =
                    pLsppExtOutParams->OutPwDetail.PwFecInfo.u4VcId;

                if (pLsppExtOutParams->OutPwDetail.PwFecInfo.u1PwVcOwner ==
                    LSPP_PW_OWNER_PWIDFEC_SIGNALLING)
                {
                    pLsppFsLsppPingTraceTableEntry->PathId.u1PathType =
                        LSPP_PATH_TYPE_FEC_128;
                    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppPathType =
                        LSPP_PATH_TYPE_FEC_128;
                }

                else if (pLsppExtOutParams->OutPwDetail.PwFecInfo.u1PwVcOwner ==
                         LSPP_PW_OWNER_GENFEC_SIGNALLING)
                {
                    pLsppFsLsppPingTraceTableEntry->PathId.u1PathType =
                        LSPP_PATH_TYPE_FEC_129;
                    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppPathType =
                        LSPP_PATH_TYPE_FEC_129;
                }

                if (pLsppFsLsppPingTraceTableEntry->MibObject.
                    i4FsLsppEncapType == 0)
                {
                    pLsppFsLsppPingTraceTableEntry->MibObject.
                        i4FsLsppEncapType = LSPP_ENCAP_TYPE_VCCV_NEGOTIATED;
                }

                if (pLsppFsLsppPingTraceTableEntry->MibObject.
                    i4FsLsppReplyMode == 0)
                {
                    pLsppFsLsppPingTraceTableEntry->MibObject.
                        i4FsLsppReplyMode = LSPP_REPLY_APPLICATION_CC;
                }
                break;

            case LSPP_PATH_TYPE_MEP:

                pLsppFsLsppPingTraceTableEntry->PathId.unPathId.MepIndices.
                    u4MpIndex =
                    pLsppFsLsppPingTraceTableEntry->MibObject.
                    au4FsLsppPathPointer[pLsppFsLsppPingTraceTableEntry->
                                         MibObject.i4FsLsppPathPointerLen -
                                         LSPP_OID_OFFSET_MEG_MP_INDEX];

                pLsppFsLsppPingTraceTableEntry->PathId.unPathId.MepIndices.
                    u4MeIndex =
                    pLsppFsLsppPingTraceTableEntry->MibObject.
                    au4FsLsppPathPointer[pLsppFsLsppPingTraceTableEntry->
                                         MibObject.i4FsLsppPathPointerLen -
                                         LSPP_OID_OFFSET_MEG_ME_INDEX];

                pLsppFsLsppPingTraceTableEntry->PathId.unPathId.MepIndices.
                    u4MegIndex =
                    pLsppFsLsppPingTraceTableEntry->MibObject.
                    au4FsLsppPathPointer[pLsppFsLsppPingTraceTableEntry->
                                         MibObject.i4FsLsppPathPointerLen -
                                         LSPP_OID_OFFSET_MEG_INDEX];

                /*  Check the service type of the MEG */
                pLsppExtInParams->u4ContextId =
                    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId;

                pLsppExtInParams->u4RequestType = LSPP_MPLS_OAM_GET_MEG_INFO;

                MEMCPY (&(pLsppExtInParams->InMegInfo.MegIndices),
                        &(pLsppFsLsppPingTraceTableEntry->PathId.unPathId.
                          MepIndices), sizeof (tLsppMegIndices));

                if (LsppPortHandleExtInteraction (pLsppExtInParams,
                                                  pLsppExtOutParams) !=
                    OSIX_SUCCESS)
                {
                    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                        (UINT1 *) pLsppExtInParams);
                    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                        (UINT1 *) pLsppExtOutParams);
                    return OSIX_FAILURE;
                }

                /* If the service type is pseudowire, then Force explicit null,
                 * TTl and GlobalId, NodeId, Interface number should not
                 * be configured
                 */

                if (pLsppFsLsppPingTraceTableEntry->MibObject.
                    i4FsLsppReplyMode == 0)
                {
                    pLsppFsLsppPingTraceTableEntry->MibObject.
                        i4FsLsppReplyMode = LSPP_REPLY_APPLICATION_CC;
                }

                if (pLsppExtOutParams->OutMegDetail.u1ServiceType ==
                    OAM_SERVICE_TYPE_PW)
                {
                    if (pLsppFsLsppPingTraceTableEntry->MibObject.
                        i4FsLsppReplyMode != LSPP_REPLY_APPLICATION_CC)
                    {
                        LSPP_LOG (pLsppFsLsppPingTraceTableEntry->MibObject.
                                  u4FsLsppContextId,
                                  LSPP_CFG_INVALID_FOR_PW,
                                  "LsppUtilUpdateFsLsppPingTraceTable");
                        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                            (UINT1 *) pLsppExtInParams);
                        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                            (UINT1 *) pLsppExtOutParams);
                        return OSIX_FAILURE;
                    }

                    if ((pLsppFsLsppPingTraceTableEntry->MibObject.
                         i4FsLsppEncapType != 0) &&
                        (pLsppFsLsppPingTraceTableEntry->MibObject.
                         i4FsLsppEncapType != LSPP_ENCAP_TYPE_VCCV_NEGOTIATED))
                    {
                        LSPP_LOG (pLsppFsLsppPingTraceTableEntry->MibObject.
                                  u4FsLsppContextId,
                                  LSPP_CFG_INVALID_FOR_PW,
                                  "LsppUtilUpdateFsLsppPingTraceTable");
                        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                            (UINT1 *) pLsppExtInParams);
                        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                            (UINT1 *) pLsppExtOutParams);
                        return OSIX_FAILURE;
                    }
                    else if (pLsppFsLsppPingTraceTableEntry->MibObject.
                             i4FsLsppEncapType == 0)
                    {
                        pLsppFsLsppPingTraceTableEntry->MibObject.
                            i4FsLsppEncapType = LSPP_ENCAP_TYPE_VCCV_NEGOTIATED;
                    }

                    if ((pLsppFsLsppPingTraceTableEntry->MibObject.
                         i4FsLsppForceExplicitNull == LSPP_SNMP_TRUE) ||
                        (pLsppFsLsppPingTraceTableEntry->MibObject.
                         u4FsLsppTTLValue != 0) ||
                        (pLsppFsLsppPingTraceTableEntry->MibObject.
                         u4FsLsppTgtMipGlobalId != 0) ||
                        (pLsppFsLsppPingTraceTableEntry->MibObject.
                         u4FsLsppTgtMipNodeId != 0) ||
                        (pLsppFsLsppPingTraceTableEntry->MibObject.
                         u4FsLsppTgtMipIfNum != 0))
                    {
                        LSPP_LOG (pLsppFsLsppPingTraceTableEntry->MibObject.
                                  u4FsLsppContextId,
                                  LSPP_CFG_INVALID_FOR_PW,
                                  "LsppUtilUpdateFsLsppPingTraceTable");
                        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                            (UINT1 *) pLsppExtInParams);
                        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                            (UINT1 *) pLsppExtOutParams);
                        return OSIX_FAILURE;
                    }
                    if (pLsppFsLsppPingTraceTableEntry->MibObject.
                        i4FsLsppRequestType == LSPP_REQ_TYPE_TRACE_ROUTE)
                    {
                        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                            (UINT1 *) pLsppExtInParams);
                        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                            (UINT1 *) pLsppExtOutParams);
                        return OSIX_FAILURE;
                    }

                }
                else
                {
                    if ((pLsppFsLsppPingTraceTableEntry->MibObject.
                         u4FsLsppTgtMipNodeId != 0) &&
                        (pLsppFsLsppPingTraceTableEntry->MibObject.
                         u4FsLsppTTLValue == 0))
                    {
                        LSPP_LOG (pLsppFsLsppPingTraceTableEntry->MibObject.
                                  u4FsLsppContextId,
                                  LSPP_INTER_CV_CFG_INVALID,
                                  "LsppUtilUpdateFsLsppPingTraceTable");
                        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                            (UINT1 *) pLsppExtInParams);
                        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                            (UINT1 *) pLsppExtOutParams);
                        return OSIX_FAILURE;
                    }

                    if (pLsppFsLsppPingTraceTableEntry->MibObject.
                        i4FsLsppEncapType == LSPP_ENCAP_TYPE_VCCV_NEGOTIATED)
                    {
                        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                            (UINT1 *) pLsppExtInParams);
                        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                            (UINT1 *) pLsppExtOutParams);
                        return OSIX_FAILURE;
                    }
                    else if (pLsppFsLsppPingTraceTableEntry->MibObject.
                             i4FsLsppEncapType == 0)
                    {
                        pLsppFsLsppPingTraceTableEntry->MibObject.
                            i4FsLsppEncapType = LSPP_ENCAP_TYPE_ACH;
                    }

                }

                break;

            default:
                LSPP_LOG (pLsppFsLsppPingTraceTableEntry->MibObject.
                          u4FsLsppContextId, LSPP_INVALID_PATH_TYPE_IN_ENTRY,
                          "LsppUtilUpdateFsLsppPingTraceTable");
                MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                    (UINT1 *) pLsppExtInParams);
                MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pLsppExtOutParams);
                return OSIX_FAILURE;
        }

        if (pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTTLValue == 0)
        {
            if (pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRequestType ==
                LSPP_REQ_TYPE_TRACE_ROUTE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTTLValue =
                    LSPP_DEFAULT_MAX_HOP_COUNT;
            }
            else
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTTLValue =
                    LSPP_MAX_TTL;
            }
        }

        if ((pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppReplyMode ==
             LSPP_NO_REPLY) &&
            (pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRequestType ==
             LSPP_REQ_TYPE_PING))
        {
            pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppWFRInterval = 0;
        }

        if (pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRequestType ==
            LSPP_REQ_TYPE_TRACE_ROUTE)
        {
            pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppDsMap =
                LSPP_SNMP_TRUE;
        }

        /* Reply mode should be either via IP/UDP or IP/UDP with router alert
         * when DSCP value is configured
         */
        if ((pLsppFsLsppPingTraceTableEntry->MibObject.
             u4FsLsppReplyDscpValue != 0) &&
            ((pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppReplyMode !=
              LSPP_REPLY_IP_UDP) &&
             (pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppReplyMode !=
              LSPP_REPLY_IP_UDP_ROUTER_ALERT)))
        {
            LSPP_LOG (pLsppFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      LSPP_REPLY_MODE_INVALID_FOR_DSCP,
                      "LsppUtilUpdateFsLsppPingTraceTable");
            MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtInParams);
            MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtOutParams);
            return OSIX_FAILURE;
        }

        if ((pLsppFsLsppPingTraceTableEntry->MibObject.
             i4FsLsppEncapType == LSPP_ENCAP_TYPE_ACH) &&
            ((pLsppFsLsppPingTraceTableEntry->MibObject.
              i4FsLsppReplyMode == LSPP_REPLY_IP_UDP) ||
             (pLsppFsLsppPingTraceTableEntry->MibObject.
              i4FsLsppReplyMode == LSPP_REPLY_IP_UDP_ROUTER_ALERT)))
        {
            LSPP_LOG (pLsppFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      LSPP_INVALID_REPLY_MODE_FOR_ENCAP,
                      "LsppUtilUpdateFsLsppPingTraceTable",
                      pLsppFsLsppPingTraceTableEntry->MibObject.
                      i4FsLsppReplyMode,
                      pLsppFsLsppPingTraceTableEntry->MibObject.
                      i4FsLsppEncapType);
            MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtInParams);
            MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                (UINT1 *) pLsppExtOutParams);
	    u4ErrFlag = CLI_LSPP_INVALID_REPLY_MODE_FOR_ENCAP;
            return (INT4)u4ErrFlag;
        }

        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRequestOwner =
            LSPP_REQ_OWNER_MGMT;
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppStatus =
            LSPP_ECHO_IN_PROGRESS;

        if (pLsppOldFsLsppPingTraceTableEntry == NULL)
        {
            pLsppFsLsppPingTraceTableEntry->u1RequestOwnerSubType =
                LSPP_REQ_OWNER_MGMT_CLI;

            SNPRINTF ((CHR1 *) au1SemName, LSPP_MAX_NAME_LEN, "L%d%d",
                      pLsppFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      pLsppFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppSenderHandle);

            if (OsixSemCrt (au1SemName,
                            &(pLsppFsLsppPingTraceTableEntry->SemId))
                == OSIX_FAILURE)
            {
                LSPP_CMN_TRC (pLsppFsLsppPingTraceTableEntry->MibObject.
                              u4FsLsppContextId, LSPP_SEM_CREATION_FAILED,
                              "LsppUtilUpdateFsLsppPingTraceTable");
                MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                    (UINT1 *) pLsppExtInParams);
                MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pLsppExtOutParams);
                return OSIX_FAILURE;
            }
        }
        else
        {
            pLsppFsLsppPingTraceTableEntry->u1RequestOwnerSubType =
                LSPP_REQ_OWNER_MGMT_SNMP;

            /* Trigger to frame and send echo request for SNMP */
            if (LsppCoreCreateAndTriggerEcho
                (pLsppFsLsppPingTraceTableEntry, 0) != OSIX_SUCCESS)
            {
                /* Here, return value is sucess even after the failure of 
                 * sending first echo request. Because after the Timer expiry,
                 * second request packet will be sent. So return status
                 * should not be failure
                 */
                MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                                    (UINT1 *) pLsppExtInParams);
                MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                                    (UINT1 *) pLsppExtOutParams);
                return OSIX_SUCCESS;
            }
        }

        /* Specifying the path validation direction is forward by default */
        pLsppFsLsppPingTraceTableEntry->MibObject.
            i4FsLsppStatusPathDirection = LSPP_VALIDATION_DIRECTION_FORWARD;
    }

    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtInParams);
    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtOutParams);

    /* When the row status is DESTROY, delete all the EchoSequence and HopInfo
     * entries corresponds to this PingTrace entry */
    if (pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRowStatus == DESTROY)
    {
        if (pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRequestOwner ==
            LSPP_REQ_OWNER_BFD)
        {
            LSPP_LOG (pLsppFsLsppPingTraceTableEntry->MibObject.
                      u4FsLsppContextId,
                      LSPP_DEL_PINGTRACE_NODE_NOT_ALLOWED,
                      "LsppUtilUpdateFsLsppPingTraceTable", "BFD");
            return OSIX_FAILURE;
        }
        else
        {
            if (LsppCxtDeletePingTraceInfo (pLsppFsLsppPingTraceTableEntry) !=
                OSIX_SUCCESS)
            {
                LSPP_LOG (pLsppFsLsppPingTraceTableEntry->MibObject.
                          u4FsLsppContextId,
                          LSPP_UNABLE_TO_DELETE_PING_TRACE_INFO,
                          "LsppUtilUpdateFsLsppPingTraceTable");
                return OSIX_FAILURE;
            }

            LsppGlobalConfigEntry.MibObject.u4FsLsppContextId =
                pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId;

            if (LsppGetAllFsLsppGlobalConfigTable (&LsppGlobalConfigEntry) !=
                OSIX_SUCCESS)
            {
                return OSIX_FAILURE;
            }

            IndexManagerFreeIndex (LsppGlobalConfigEntry.IndexMgrId,
                                   pLsppFsLsppPingTraceTableEntry->MibObject.
                                   u4FsLsppSenderHandle);
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  LsppGetAllUtlFsLsppEchoSequenceTable
 Input       :  pLsppGetFsLsppEchoSequenceTableEntry
                pLsppdsFsLsppEchoSequenceTableEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
LsppGetAllUtlFsLsppEchoSequenceTable (tLsppFsLsppEchoSequenceTableEntry *
                                      pLsppGetFsLsppEchoSequenceTableEntry,
                                      tLsppFsLsppEchoSequenceTableEntry *
                                      pLsppdsFsLsppEchoSequenceTableEntry)
{
    MEMCPY (&(pLsppGetFsLsppEchoSequenceTableEntry->WFRTimer),
            &(pLsppdsFsLsppEchoSequenceTableEntry->WFRTimer), sizeof (tTmrBlk));

    pLsppGetFsLsppEchoSequenceTableEntry->u1Status =
        pLsppdsFsLsppEchoSequenceTableEntry->u1Status;

    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  LsppGetAllUtlFsLsppHopTable
 Input       :  pLsppGetFsLsppHopTableEntry
                pLsppdsFsLsppHopTableEntry
 Description :  This routine Gets the 
                value of needed.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
LsppGetAllUtlFsLsppHopTable (tLsppFsLsppHopTableEntry *
                             pLsppGetFsLsppHopTableEntry,
                             tLsppFsLsppHopTableEntry *
                             pLsppdsFsLsppHopTableEntry)
{
    UNUSED_PARAM (pLsppGetFsLsppHopTableEntry);
    UNUSED_PARAM (pLsppdsFsLsppHopTableEntry);
    return OSIX_SUCCESS;
}

 /******************************************************************************
 * Function   : LsppUtilGetGlobalNodeId
 *
 * Description: This function is to get the Out params for the given input
 *
 * Input      : pLsppExtOutParams
 *
 * Output     : OID of the given Path
 *
 * Returns    : OSIX_FAILURE/OSIX_SUCCESS
 *****************************************************************************/

INT4
LsppUtilGetGlobalNodeId (tLsppFsLsppPingTraceTableEntry
                         * pLsppFsLsppPingTraceTableEntry,
                         tLsppExtOutParams * pLsppExtOutParams)
{

    tLsppExtInParams    LsppExtInParams;

    MEMSET (&(LsppExtInParams), 0, sizeof (tLsppExtInParams));

    LsppExtInParams.u4ContextId =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->PathId.unPathId.MepIndices.
        u4MpIndex =
        pLsppFsLsppPingTraceTableEntry->MibObject.
        au4FsLsppPathPointer[pLsppFsLsppPingTraceTableEntry->
                             MibObject.i4FsLsppPathPointerLen -
                             LSPP_OID_OFFSET_MEG_MP_INDEX];

    pLsppFsLsppPingTraceTableEntry->PathId.unPathId.MepIndices.
        u4MeIndex =
        pLsppFsLsppPingTraceTableEntry->MibObject.
        au4FsLsppPathPointer[pLsppFsLsppPingTraceTableEntry->
                             MibObject.i4FsLsppPathPointerLen -
                             LSPP_OID_OFFSET_MEG_ME_INDEX];

    pLsppFsLsppPingTraceTableEntry->PathId.unPathId.MepIndices.
        u4MegIndex =
        pLsppFsLsppPingTraceTableEntry->MibObject.
        au4FsLsppPathPointer[pLsppFsLsppPingTraceTableEntry->
                             MibObject.i4FsLsppPathPointerLen -
                             LSPP_OID_OFFSET_MEG_INDEX];

    LsppExtInParams.u4RequestType = LSPP_MPLS_OAM_GET_MEG_INFO;

    MEMCPY (&(LsppExtInParams.InMegInfo.MegIndices),
            &(pLsppFsLsppPingTraceTableEntry->PathId.unPathId.
              MepIndices), sizeof (tLsppMegIndices));

    if (LsppPortHandleExtInteraction (&LsppExtInParams, pLsppExtOutParams) !=
        OSIX_SUCCESS)
    {
        LSPP_LOG (pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId,
                  LSPP_FETCH_FROM_EXT_MOD_FAILED, "LsppUtilTestGlobalNodeId");
        return OSIX_FAILURE;
    }

    return OSIX_SUCCESS;
}
