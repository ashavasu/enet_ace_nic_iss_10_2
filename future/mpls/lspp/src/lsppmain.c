/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lsppmain.c,v 1.9 2013/06/27 12:23:22 siva Exp $
 *
 * Description: This file contains the lspp task main loop
 *              and the initialization routines.
 *
 *******************************************************************/
#define __LSPPMAIN_C__
#include "lsppinc.h"

/* Proto types of the functions private to this file only */

PRIVATE UINT4 LsppMainMemInit PROTO ((VOID));
PRIVATE VOID LsppMainMemClear PROTO ((VOID));

/****************************************************************************
*                                                                           *
* Function     : LsppMainTask                                               *
*                                                                           *
* Description  : Main function of LSPP.                                     *
*                                                                           *
* Input        : pTaskId                                                    *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : VOID                                                       *
*                                                                           *
*****************************************************************************/

PUBLIC VOID
LsppMainTask ()
{
    UINT4               u4Event = 0;

    LSPP_TRC_FUNC ((LSPP_FN_ENTRY, "FUNC:LsppTaskMain\n"));

    while (OSIX_TRUE)
    {

        if (OsixReceiveEvent ((LSPP_TIMER_EVENT | LSPP_QMSG_EVENT |
                               LSPP_IP_PKT_RX_EVENT), OSIX_WAIT, (UINT4) 0,
                              (UINT4 *) &(u4Event)) != OSIX_SUCCESS)
        {
            continue;
        }

        LsppMainTaskLock ();

        LSPP_TRC ((LSPP_MAIN_TRC, "Rx Event %d\n", u4Event));

        if ((u4Event & LSPP_TIMER_EVENT) == LSPP_TIMER_EVENT)
        {
            LSPP_TRC ((LSPP_MAIN_TRC, "LSPP_TIMER_EVENT\n"));

            LsppTmrHandleExpiry ();
        }

        if ((u4Event & LSPP_QMSG_EVENT) == LSPP_QMSG_EVENT)
        {
            LSPP_TRC ((LSPP_MAIN_TRC, "LSPP_QUEUE_EVENT\n"));

            LsppQueProcessMsgs ();
        }

        if ((u4Event & LSPP_IP_PKT_RX_EVENT) == LSPP_IP_PKT_RX_EVENT)
        {
            LsppSockReceiveEchoPkt ();
        }

        /* Mutual exclusion flag OFF */
        LsppMainTaskUnLock ();
    }
}

/****************************************************************************
*                                                                           *
* Function     : LsppMainTaskInit                                           *
*                                                                           *
* Description  : LSPP initialization routine.                               *
*                                                                           *
* Input        : None                                                       *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : OSIX_SUCCESS, if initialization succeeds                   *
*                OSIX_FAILURE, otherwise                                    *
*                                                                           *
*****************************************************************************/

PUBLIC UINT4
LsppMainTaskInit (VOID)
{

    LSPP_TRC_FUNC ((LSPP_FN_ENTRY, "FUNC:LsppMainTaskInit\n"));
#ifdef LSPP_TRACE_WANTED
    LSPP_TRC_FLAG = ~((UINT4) 0);
#endif

    MEMSET (&gLsppGlobals, 0, sizeof (gLsppGlobals));
    MEMCPY (gLsppGlobals.au1TaskSemName, LSPP_MUT_EXCL_SEM_NAME, OSIX_NAME_LEN);

    /* Create the LSPP task semaphore. */
    if (OsixCreateSem (LSPP_MUT_EXCL_SEM_NAME, LSPP_SEM_CREATE_INIT_CNT, 0,
                       &gLsppGlobals.lsppTaskSemId) == OSIX_FAILURE)
    {
        LSPP_TRC ((LSPP_MAIN_TRC, "Semaphore Creation failure for %s \n",
                   LSPP_MUT_EXCL_SEM_NAME));
        return OSIX_FAILURE;
    }

    /* Create the LSPP task Q. */
    if (OsixCreateQ (LSPP_QUEUE_NAME, LSPP_QUEUE_DEPTH, (UINT4) 0,
                     (tOsixQId *) & (gLsppGlobals.lsppQueId)) == OSIX_FAILURE)
    {
        LSPP_TRC ((LSPP_MAIN_TRC, "LSPP Queue Creation Failed\n"));
        return OSIX_FAILURE;
    }

    /* Create RBTree for all the tables in LSPP module. */
    if (LsppUtlCreateRBTree () == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }

    /* Create buffer pools for data structures */
    if (LsppMainMemInit () == OSIX_FAILURE)
    {
        LSPP_TRC ((LSPP_MAIN_TRC, "Memory Pool Creation Failed\n"));
        return OSIX_FAILURE;
    }

    /* Initialize the Timer list for all the timers. */
    if (LsppTmrInit () == OSIX_FAILURE)
    {
        LSPP_TRC ((LSPP_MAIN_TRC, "Timer List Initialization Failed\n"));
        return OSIX_FAILURE;
    }

    /* Create the socket for transmitting and receving the LSPP 
     * echo messages.
     */
    if (LsppSockInit () != OSIX_SUCCESS)
    {
        LSPP_TRC ((LSPP_MAIN_TRC, "LSPP Socket Creation Failed\n"));
        return OSIX_FAILURE;
    }

    /* Register with SysLog */

    if (LsppMainSysLogRegister () != OSIX_SUCCESS)
    {
        LSPP_TRC ((LSPP_MAIN_TRC, "Syslog Registration Failed \r\n"));
        return OSIX_FAILURE;
    }

    /* Register with VCM module. */
    if (LsppMainVcmRegister () != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    /* Create the default context */
    if (LsppCxtCreateContext (LSPP_DEFAULT_CONTEXT_ID) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }

    if (OsixTskIdSelf (&gLsppGlobals.lsppTaskId) != OSIX_SUCCESS)
    {
        LSPP_TRC ((LSPP_MAIN_TRC, "Getting Module Task Id Failed\n"));
        return OSIX_FAILURE;
    }

    LSPP_TRC_FUNC ((LSPP_FN_EXIT, "FUNC:LsppMainTaskInit\n"));

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsppMainDeInit                                             */
/*                                                                           */
/* Description  : Deleting the resources when task init fails.               */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*                                                                           */
/*****************************************************************************/
PUBLIC VOID
LsppMainDeInit (VOID)
{
    if (gLsppGlobals.lsppTaskSemId)
    {
        OsixSemDel (gLsppGlobals.lsppTaskSemId);
    }
    if (gLsppGlobals.lsppQueId)
    {
        OsixQueDel (gLsppGlobals.lsppQueId);
    }
    LsppTmrDeInit ();
    LsppSockDeInit ();
    LsppMainSysLogDeRegister ();
    LsppMainVcmDeRegister ();
    LsppCxtDeleteContext (LSPP_DEFAULT_CONTEXT_ID, OSIX_TRUE);
    LsppMainDestroyAllTables ();
    LsppMainMemClear ();
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsppMainTaskLock                                           */
/*                                                                           */
/* Description  : Lock the Lspp Main Task                                    */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
LsppMainTaskLock (VOID)
{
    LSPP_TRC_FUNC ((LSPP_FN_ENTRY, "FUNC:LsppMainTaskLock\n"));

    if (OsixSemTake (gLsppGlobals.lsppTaskSemId) == OSIX_FAILURE)
    {
        LSPP_TRC ((LSPP_MAIN_TRC,
                   "TakeSem failure for %s \n", LSPP_MUT_EXCL_SEM_NAME));
        return SNMP_FAILURE;
    }

    LSPP_TRC_FUNC ((LSPP_FN_EXIT, "EXIT:LsppMainTaskLock\n"));
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsppMainTaskUnLock                                         */
/*                                                                           */
/* Description  : UnLock the LSPP Task                                       */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : SNMP_SUCCESS or SNMP_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PUBLIC INT4
LsppMainTaskUnLock (VOID)
{
    LSPP_TRC_FUNC ((LSPP_FN_ENTRY, "FUNC:LsppMainTaskUnLock\n"));

    OsixSemGive (gLsppGlobals.lsppTaskSemId);

    LSPP_TRC_FUNC ((LSPP_FN_EXIT, "EXIT:LsppMainTaskUnLock\n"));
    return SNMP_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsppMainMemInit                                            */
/*                                                                           */
/* Description  : Allocates all the memory that is required for LSPP         */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS or OSIX_FAILURE                               */
/*                                                                           */
/*****************************************************************************/
PRIVATE UINT4
LsppMainMemInit (VOID)
{
    if (LsppSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        return OSIX_FAILURE;
    }
    gLsppGlobals.LsppFsLsppQMsgPoolId = LSPP_FSLSPP_QUEUE_MSG_ENTRY_POOLID;
    gLsppGlobals.LsppFsLsppEchoMsgPoolId = LSPP_FSLSPP_ECHOMSG_ENTRY_POOLID;

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : LsppMainMemClear                                           */
/*                                                                           */
/* Description  : Clears all the Memory                                      */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
PRIVATE VOID
LsppMainMemClear (VOID)
{
    LsppSizingMemDeleteMemPools ();

    gLsppGlobals.LsppFsLsppQMsgPoolId = 0;
    gLsppGlobals.LsppFsLsppEchoMsgPoolId = 0;
}

/*****************************************************************************/
/* Function     : LsppMainVcmRegister                                        */
/*                                                                           */
/* Description  : To register with VCM module, so that VCM will intimate     */
/*                about the context creation or deletion to LSP Ping module  */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  */
/*****************************************************************************/
INT4
LsppMainVcmRegister (VOID)
{
    tLsppExtInParams   *pLsppExtInParams = NULL;
    tLsppExtOutParams  *pLsppExtOutParams = NULL;

    if ((pLsppExtInParams = (tLsppExtInParams *)
         MemAllocMemBlk (LSPP_FSLSPPINPUTPARAMS_POOLID)) == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((pLsppExtOutParams = (tLsppExtOutParams *)
         MemAllocMemBlk (LSPP_FSLSPPOUTPUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        return OSIX_FAILURE;
    }

    MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
    MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));

    pLsppExtInParams->u4RequestType = LSPP_VCM_REGISTER_WITH_VCM;
    pLsppExtInParams->u4ContextId = LSPP_DEFAULT_CONTEXT_ID;

    if (LsppPortHandleExtInteraction (pLsppExtInParams, pLsppExtOutParams) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);
        return OSIX_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtInParams);
    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtOutParams);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : LsppMainVcmDeRegister                                      */
/*                                                                           */
/* Description  : To de-register with the VCM module                         */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
LsppMainVcmDeRegister (VOID)
{
    tLsppExtInParams   *pLsppExtInParams = NULL;
    tLsppExtOutParams  *pLsppExtOutParams = NULL;

    if ((pLsppExtInParams = (tLsppExtInParams *)
         MemAllocMemBlk (LSPP_FSLSPPINPUTPARAMS_POOLID)) == NULL)
    {
        return;
    }
    if ((pLsppExtOutParams = (tLsppExtOutParams *)
         MemAllocMemBlk (LSPP_FSLSPPOUTPUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        return;
    }

    MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
    MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));

    pLsppExtInParams->u4RequestType = LSPP_VCM_DEREGISTER_WITH_VCM;
    pLsppExtInParams->u4ContextId = LSPP_DEFAULT_CONTEXT_ID;

    if (LsppPortHandleExtInteraction (pLsppExtInParams, pLsppExtOutParams) !=
        OSIX_SUCCESS)
    {
        LSPP_LOG (LSPP_INVALID_CONTEXT, LSPP_DE_REGISTER_WITH_VCM_FAILED,
                  "LsppMainDeRegisterWithVcm");
    }
    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtInParams);
    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtOutParams);
}

/*****************************************************************************/
/* Function     : LsppMainSysLogRegister                                     */
/*                                                                           */
/* Description  : To de-register with the VCM module                         */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
INT4
LsppMainSysLogRegister (VOID)
{
    tLsppExtInParams   *pLsppExtInParams = NULL;
    tLsppExtOutParams  *pLsppExtOutParams = NULL;

    if ((pLsppExtInParams = (tLsppExtInParams *)
         MemAllocMemBlk (LSPP_FSLSPPINPUTPARAMS_POOLID)) == NULL)
    {
        return OSIX_FAILURE;
    }
    if ((pLsppExtOutParams = (tLsppExtOutParams *)
         MemAllocMemBlk (LSPP_FSLSPPOUTPUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        return OSIX_FAILURE;
    }

    MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
    MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));

    pLsppExtInParams->u4RequestType = LSPP_SYSLOG_REGISTER;
    pLsppExtInParams->u4ContextId = LSPP_DEFAULT_CONTEXT_ID;

    if (LsppPortHandleExtInteraction (pLsppExtInParams, pLsppExtOutParams) !=
        OSIX_SUCCESS)
    {
        LSPP_LOG (LSPP_INVALID_CONTEXT, LSPP_DE_REGISTER_WITH_VCM_FAILED,
                  "LsppMainSysLogRegister");
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);
        return OSIX_FAILURE;
    }
    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtInParams);
    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtOutParams);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function     : LsppMainSysLogDeRegister                                   */
/*                                                                           */
/* Description  : To de-register with the VCM module                         */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
LsppMainSysLogDeRegister (VOID)
{
    tLsppExtInParams   *pLsppExtInParams = NULL;
    tLsppExtOutParams  *pLsppExtOutParams = NULL;

    if ((pLsppExtInParams = (tLsppExtInParams *)
         MemAllocMemBlk (LSPP_FSLSPPINPUTPARAMS_POOLID)) == NULL)
    {
        return;
    }
    if ((pLsppExtOutParams = (tLsppExtOutParams *)
         MemAllocMemBlk (LSPP_FSLSPPOUTPUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        return;
    }

    MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
    MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));

    pLsppExtInParams->u4RequestType = LSPP_SYSLOG_DEREGISTER;
    pLsppExtInParams->u4ContextId = LSPP_DEFAULT_CONTEXT_ID;

    if (LsppPortHandleExtInteraction (pLsppExtInParams, pLsppExtOutParams) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);
        return;
    }

    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtInParams);
    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtOutParams);
}

/*****************************************************************************/
/* Function     : LsppMainDestroyAllTables                                   */
/*                                                                           */
/* Description  : To destroy all the tables created                          */
/*                                                                           */
/* Input        : None                                                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : VOID                                                       */
/*                                                                           */
/*****************************************************************************/
VOID
LsppMainDestroyAllTables (VOID)
{
    RBTreeDelete (gLsppGlobals.LsppGlbMib.FsLsppGlobalConfigTable);
    RBTreeDelete (gLsppGlobals.LsppGlbMib.FsLsppGlobalStatsTable);
    RBTreeDelete (gLsppGlobals.LsppGlbMib.FsLsppPingTraceTable);
    RBTreeDelete (gLsppGlobals.LsppGlbMib.FsLsppEchoSequenceTable);
    RBTreeDelete (gLsppGlobals.LsppGlbMib.FsLsppHopTable);

}

/*-----------------------------------------------------------------------*/
/*                       End of the file  lsppmain.c                     */
/*-----------------------------------------------------------------------*/
