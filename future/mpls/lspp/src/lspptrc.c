 /******************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lspptrc.c,v 1.15 2015/02/05 11:29:58 siva Exp $
 *
 * Description : This file contains the functions to print the trace messages,
 *               send trap to the manager and syslog related functionalities
 *                
 * ****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include "lsppinc.h"

/******************************************************************************
 * Function   : LsppTrcIssEventLogNotify
 *
 * Description: This function is to print the trace messages and send syslog 
 *              messages common to all the modules.
 * 
 * Input      : u4ContextId
 *              u4ArrayIndex - Index of an array containing the trace levels
 *                             and the trace messages
 *              Variable args
 *
 * Output     : Trace messages will be printed if it us enabled
 * Returns    : None 
 * 
 *****************************************************************************/

PUBLIC VOID
LsppTrcIssEventLogNotify (UINT4 u4ContextId, UINT4 u4ArrayIndex, ...)
{
    va_list             vTrcArgs;
    tLsppFsLsppGlobalConfigTableEntry LsppGlobalConfigTableEntry;
    tLsppFsLsppGlobalConfigTableEntry *pLsppGlobalConfigTableEntry = NULL;
    INT4                i4TraceLevel = 0;
    UINT4               u4SyslogLevel = 0;
    UINT4               u4OffSet = 0;
    UINT1               au1ContextName[LSPP_MAX_CONTEXT_NAME_LEN];
    CHR1                ac1Buf[LSPP_MAX_SYSLOG_MSG_LEN];
    CHR1                ac1LogMsg[LSPP_MAX_SYSLOG_MSG_LEN];

    MEMSET (ac1Buf, 0, sizeof (ac1Buf));
    MEMSET (au1ContextName, 0, sizeof (LSPP_MAX_CONTEXT_NAME_LEN));
    MEMSET (ac1LogMsg, 0, sizeof (LSPP_MAX_SYSLOG_MSG_LEN));
    MEMSET (&vTrcArgs, 0, sizeof (va_list));

    /*Identify the message and related information from the ArrayIndex */
    i4TraceLevel = (INT4)gaLsppIssEventLogNotify[u4ArrayIndex].u4TraceLevel;
    u4SyslogLevel = gaLsppIssEventLogNotify[u4ArrayIndex].u4SyslogLevel;
    STRNCPY (ac1LogMsg, gaLsppIssEventLogNotify[u4ArrayIndex].ac1LogMsg,
             (sizeof (ac1LogMsg) - 1));
    ac1LogMsg[(sizeof (ac1LogMsg) - 1)] = '\0';

    if (u4ContextId == LSPP_INVALID_CONTEXT)
    {
        if (gLsppGlobals.u4LsppTrc == OSIX_TRUE)
        {
            SNPRINTF (ac1Buf, LSPP_MAX_SYSLOG_MSG_LEN, "LSPP: ");
        }
        else
        {
            return;
        }
    }
    else
    {
        MEMSET (&LsppGlobalConfigTableEntry, 0,
                sizeof (tLsppFsLsppGlobalConfigTableEntry));

        /*Get Context (Global Config) info */
        LsppGlobalConfigTableEntry.MibObject.u4FsLsppContextId = u4ContextId;

        pLsppGlobalConfigTableEntry =
            LsppGetFsLsppGlobalConfigTable (&LsppGlobalConfigTableEntry);

        if (pLsppGlobalConfigTableEntry == NULL)
        {
            return;
        }

        if ((i4TraceLevel & pLsppGlobalConfigTableEntry->MibObject.
             i4FsLsppTraceLevel))
        {
            STRNCPY (au1ContextName,
                     pLsppGlobalConfigTableEntry->au1ContextName,
                     (sizeof (au1ContextName) - 1));
            au1ContextName[(sizeof (au1ContextName) - 1)] = '\0';

            SNPRINTF (ac1Buf, LSPP_MAX_SYSLOG_MSG_LEN,
                      (CONST CHR1 *) (au1ContextName));
        }
        else
        {
            return;
        }

    }

    u4OffSet = STRLEN (ac1Buf);

    va_start (vTrcArgs, u4ArrayIndex);

    vsprintf (&ac1Buf[u4OffSet], ac1LogMsg, vTrcArgs);

    LsppTrcSysLogMsg (u4ContextId, u4SyslogLevel, ac1Buf);

    va_end (vTrcArgs);

    UtlTrcPrint (ac1Buf);

    return;
}

/******************************************************************************
 * Function   : LsppTrcLsppEventLogNotify
 *
 * Description: This function is to print the trace messages,send syslog 
 *              messages and send trap to the SNMP manager.
 * 
 * Input      : u4ContextId
 *              u4ArrayIndex - Index of an array containing the trace levels,
 *                             race messages, trap levels and syslog levels
 *              Variable args
 *
 * Output     : Trace message will be printed, syslog messages will be sent to 
 *              the syslog serrver and trap will be sent to the SNMP manager.
 * Returns    : None 
 * 
 *****************************************************************************/

PUBLIC VOID
LsppTrcLsppEventLogNotify (UINT4 u4ContextId, UINT4 u4ArrayIndex, ...)
{
    va_list             vTrcArgs;
    va_list             vTrapArgs;
    tLsppFsLsppGlobalConfigTableEntry LsppGlobalConfigTableEntry;
    UINT1               au1ContextName[LSPP_MAX_CONTEXT_NAME_LEN];
    CHR1                ac1Buf[LSPP_MAX_SYSLOG_MSG_LEN];
    INT4                i4TraceLevel = 0;
    INT4                i4TrapType = 0;
    UINT4               u4SyslogLevel = 0;
    UINT4               u4OffSet = 0;
    CHR1                ac1LogMsg[LSPP_MAX_SYSLOG_MSG_LEN];

    MEMSET (&LsppGlobalConfigTableEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (&au1ContextName, 0, sizeof (au1ContextName));
    MEMSET (&ac1Buf, 0, sizeof (ac1Buf));

    MEMSET (&ac1LogMsg, 0, sizeof (ac1LogMsg));
    MEMSET (&vTrapArgs, 0, sizeof (va_list));
    MEMSET (&vTrcArgs, 0, sizeof (va_list));

    /*Identify the message and related information from the ArrayIndex */
    i4TraceLevel = (INT4)(gaLsppEventLogNotify[u4ArrayIndex].u4TraceLevel);
    u4SyslogLevel = gaLsppEventLogNotify[u4ArrayIndex].u4SyslogLevel;
    i4TrapType = gaLsppEventLogNotify[u4ArrayIndex].u4TrapType;
    STRNCPY (ac1LogMsg, gaLsppEventLogNotify[u4ArrayIndex].ac1LogMsg,
             (sizeof (ac1LogMsg) - 1));
    ac1LogMsg[(sizeof (ac1LogMsg) - 1)] = '\0';

    if (u4ContextId == LSPP_INVALID_CONTEXT)
    {
        if (gLsppGlobals.u4LsppTrc == OSIX_TRUE)
        {
            SNPRINTF (ac1Buf, LSPP_MAX_SYSLOG_MSG_LEN, "LSPP: ");
        }
        else
        {
            return;
        }
    }
    else
    {
        MEMSET (&LsppGlobalConfigTableEntry, 0,
                sizeof (tLsppFsLsppGlobalConfigTableEntry));

        /*Get Context (Global Config) info */
        LsppGlobalConfigTableEntry.MibObject.u4FsLsppContextId = u4ContextId;

        if (LsppGetAllFsLsppGlobalConfigTable (&LsppGlobalConfigTableEntry) !=
            OSIX_SUCCESS)
        {
            return;
        }

        STRNCPY (au1ContextName, LsppGlobalConfigTableEntry.au1ContextName,
                 (sizeof (au1ContextName) - 1));
        au1ContextName[(sizeof (au1ContextName) - 1)] = '\0';

        if ((i4TraceLevel &
             LsppGlobalConfigTableEntry.MibObject.i4FsLsppTraceLevel))
        {
            SNPRINTF (ac1Buf, LSPP_MAX_SYSLOG_MSG_LEN,
                      (CONST CHR1 *) au1ContextName);
        }
    }

    u4OffSet = STRLEN (ac1Buf);

    va_start (vTrcArgs, u4ArrayIndex);

    vsprintf (&ac1Buf[u4OffSet], ac1LogMsg, vTrcArgs);

    LsppTrcSysLogMsg (u4ContextId, u4SyslogLevel, ac1Buf);

    va_end (vTrcArgs);

    if ((u4ContextId == LSPP_INVALID_CONTEXT) ||
        (i4TraceLevel &
         LsppGlobalConfigTableEntry.MibObject.i4FsLsppTraceLevel))
    {
        UtlTrcPrint (ac1Buf);
    }

    va_start (vTrapArgs, u4ArrayIndex);

    if ((u4ContextId != LSPP_INVALID_CONTEXT) &&
        (i4TrapType & LsppGlobalConfigTableEntry.MibObject.i4FsLsppTrapStatus))
    {
        LsppTrapSendTrapNotifications (vTrapArgs, u4ContextId, au1ContextName,
                                       i4TrapType);
    }
    va_end (vTrapArgs);
}

/****************************************************************************
*                                                                           *
* Function     : LsppTrcPrint                                               *
*                                                                           *
* Description  :  prints the trace - with filename and line no              *
*                                                                           *
* Input        : fname   - File name                                        *
*                u4Line  - Line no                                          *
*                s       - strong to be printed                             *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Returns string                                             *
*                                                                           *
*****************************************************************************/

VOID
LsppTrcPrint (const char *fname, UINT4 u4Line, const char *s)
{

    tOsixSysTime        sysTime = 0;
    const char         *pc1Fname = fname;

    if (s == NULL)
    {
        return;
    }
    while (*fname != '\0')
    {
        if (*fname == '/')
            pc1Fname = (fname + 1);
        fname++;
    }
    OsixGetSysTime (&sysTime);
    printf ("LSPP: %d:%s:%d:   %s", sysTime, pc1Fname, u4Line, s);
}

/****************************************************************************
*                                                                           *
* Function     : LsppTrcWrite                                               *
*                                                                           *
* Description  :  prints the trace - without filename and line no ,         *
*                 Useful for dumping packets                                *
*                                                                           *
* Input        : s - string to be printed                                   *
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Returns string                                             *
*                                                                           *
*****************************************************************************/

VOID
LsppTrcWrite (CHR1 * s)
{
    if (s != NULL)
    {
        printf ("%s", s);
    }
}

/****************************************************************************
*                                                                           *
* Function     : LsppTrc                                                    *
*                                                                           *
* Description  : converts variable argument in to string depending on flag  *
*                                                                           *
* Input        : u4Flags  - Trace flag                                      *
*                fmt  - format strong, variable argument
*                                                                           *
* Output       : None                                                       *
*                                                                           *
* Returns      : Returns string                                             *
*                                                                           *
*****************************************************************************/

CHR1               *
LsppTrc (UINT4 u4Flags, const char *fmt, ...)
{
    va_list             ap;
#define LSPP_TRC_BUF_SIZE    2000
    static CHR1         buf[LSPP_TRC_BUF_SIZE];
    MEMSET (&ap, 0, sizeof (va_list));
    if ((u4Flags & LSPP_TRC_FLAG) > 0)
    {
        return (NULL);
    }
    va_start (ap, fmt);
    vsprintf (&buf[0], fmt, ap);
    va_end (ap);

    return (&buf[0]);
}

/******************************************************************************
 * Function   : LsppTrcSysLogMsg 
 *
 * Description: This function is to send the syslog message by calling the 
 *              LSP extranal port function
 * 
 * Input      : u4ContextId
 *              u4SyslogLevel - syslog level
 *              pc1SyslogMsg  - Pointer containing syslog message
 *
 * Output     : Syslog messages will be sent to the syslog server
 * Returns    : None 
 * 
 *****************************************************************************/

VOID
LsppTrcSysLogMsg (UINT4 u4ContextId, UINT4 u4SyslogLevel, CHR1 * pc1SyslogMsg)
{
    tLsppExtInParams   *pLsppExtInParams = NULL;
    tLsppExtOutParams  *pLsppExtOutParams = NULL;

    if ((pLsppExtInParams = (tLsppExtInParams *)
         MemAllocMemBlk (LSPP_FSLSPPINPUTPARAMS_POOLID)) == NULL)
    {
        return;
    }
    if ((pLsppExtOutParams = (tLsppExtOutParams *)
         MemAllocMemBlk (LSPP_FSLSPPOUTPUTPARAMS_POOLID)) == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        return;
    }

    MEMSET (pLsppExtInParams, 0, sizeof (tLsppExtInParams));
    MEMSET (pLsppExtOutParams, 0, sizeof (tLsppExtOutParams));

    pLsppExtInParams->u4RequestType = LSPP_SYSLOG_MSG;
    pLsppExtInParams->u4ContextId = u4ContextId;

    pLsppExtInParams->LsppSyslogMsg.u4SyslogLevel = u4SyslogLevel;
    STRNCPY (pLsppExtInParams->LsppSyslogMsg.ac1SyslogMsg, pc1SyslogMsg,
             (sizeof (pLsppExtInParams->LsppSyslogMsg.ac1SyslogMsg) - 1));
    pLsppExtInParams->LsppSyslogMsg.
        ac1SyslogMsg[(sizeof (pLsppExtInParams->LsppSyslogMsg.ac1SyslogMsg) -
                      1)] = '\0';

    if (LsppPortHandleExtInteraction (pLsppExtInParams, pLsppExtOutParams) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtInParams);
        MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                            (UINT1 *) pLsppExtOutParams);
        return;
    }

    MemReleaseMemBlock (LSPP_FSLSPPINPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtInParams);
    MemReleaseMemBlock (LSPP_FSLSPPOUTPUTPARAMS_POOLID,
                        (UINT1 *) pLsppExtOutParams);
}

/******************************************************************************
 * Function   : LsppTrcDump 
 * Description: This function prints the given buffer in bytes
 * Input      : pu1Buf - Buffer to print
 *              i4Len  - Length of the buffer to print
 * Output     : None
 * Returns    : None 
 *****************************************************************************/
PUBLIC VOID
LsppTrcDump (UINT1 *pu1Buf, UINT4 i4Len)
{
    UINT4               u4Tmp;
    if (!pu1Buf)
        return;
    for (u4Tmp = 0; u4Tmp < i4Len; u4Tmp++)
    {
        if (u4Tmp)
        {
            if (!(u4Tmp % LSPP_MAX_BYTES_IN_A_LINE))
            {
                printf ("\n");
            }
            else
            {
                if (!(u4Tmp % LSPP_MAX_BYTES_IN_A_TOKEN))
                {
                    printf ("  ");
                }
                else
                {
                    if (u4Tmp)
                    {
                        printf (" ");
                    }
                }
            }
        }
        printf ("%02X", pu1Buf[u4Tmp]);
    }
    printf ("\n");
}

/******************************************************************************
 * Function   : LsppTrcGetTraceLevel
 * Description: This function to get the confiugred trace level
 * Input      : u4ContextId - Context Id for which trace level is to be fetched
 *              pi4TraceLevel - Pointer to store the fetched trace level
 * Output     : pi4TraceLevel - Updated trace level
 * Returns    : None 
 *****************************************************************************/
PUBLIC UINT4
LsppTrcGetTraceLevel (UINT4 u4ContextId, INT4 *pi4TraceLevel)
{

    tLsppFsLsppGlobalConfigTableEntry LsppGlobalConfigTableEntry;
    tLsppFsLsppGlobalConfigTableEntry *pLsppGlobalConfigTableEntry = NULL;

    MEMSET (&LsppGlobalConfigTableEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));

    LsppGlobalConfigTableEntry.MibObject.u4FsLsppContextId = u4ContextId;

    pLsppGlobalConfigTableEntry =
        LsppGetFsLsppGlobalConfigTable (&LsppGlobalConfigTableEntry);

    if (pLsppGlobalConfigTableEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    *pi4TraceLevel = pLsppGlobalConfigTableEntry->MibObject.i4FsLsppTraceLevel;

    return OSIX_SUCCESS;
}

/******************************************************************************
 * Function   : LsppTrcCruBufDump 
 * Description: This function dumps the cru bbuf content
 * Input      : u4ContextId - Context Id for which trace level is to be fetched
 *              pMsg - Cru buffer to be dumped
 * Output     : None
 * Returns    : None 
 *****************************************************************************/
PUBLIC VOID
LsppTrcCruBufDump (UINT4 u4ContextId, tCRU_BUF_CHAIN_HEADER * pMsg)
{
    INT4                i4TraceLevel = 0;
    UINT4               u4PktLen = 0;

    if (LsppTrcGetTraceLevel (u4ContextId, &i4TraceLevel) != OSIX_SUCCESS)
    {
        return;
    }

    if (i4TraceLevel & LSPP_PKT_DUMP_TRC)
    {
        u4PktLen = LSPP_GET_CRU_VALID_BYTE_COUNT (pMsg);
        LSPP_CRUBUF_DUMP_PKT (pMsg, u4PktLen);
    }
}

/*-----------------------------------------------------------------------*/
/*                       End of the file  lspptrc.c                      */
/*-----------------------------------------------------------------------*/
