
/******************************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
 * $Id: lspplwg.c,v 1.9 2014/12/12 11:56:49 siva Exp $
*
* Description: This file contains the low level routines
*               for the module Lspp 
*********************************************************************/

#include "lsppinc.h"

/****************************************************************************
 Function    :  nmhGetFirstIndexFsLsppGlobalConfigTable
 Input       :  The Indices
                FsLsppContextId
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsLsppGlobalConfigTable (UINT4 *pu4FsLsppContextId)
{

    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigTableEntry = NULL;

    pLsppFsLsppGlobalConfigTableEntry = LsppGetFirstFsLsppGlobalConfigTable ();

    if (pLsppFsLsppGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pu4FsLsppContextId =
        pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsLsppGlobalStatsTable
 Input       :  The Indices
                FsLsppContextId
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsLsppGlobalStatsTable (UINT4 *pu4FsLsppContextId)
{

    tLsppFsLsppGlobalStatsTableEntry *pLsppFsLsppGlobalStatsTableEntry = NULL;

    pLsppFsLsppGlobalStatsTableEntry = LsppGetFirstFsLsppGlobalStatsTable ();

    if (pLsppFsLsppGlobalStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pu4FsLsppContextId =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppContextId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsLsppPingTraceTable
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsLsppPingTraceTable (UINT4 *pu4FsLsppContextId,
                                      UINT4 *pu4FsLsppSenderHandle)
{

    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry = LsppGetFirstFsLsppPingTraceTable ();

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pu4FsLsppContextId =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId;

    *pu4FsLsppSenderHandle =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsLsppEchoSequenceTable
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
                FsLsppSequenceNumber
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsLsppEchoSequenceTable (UINT4 *pu4FsLsppContextId,
                                         UINT4 *pu4FsLsppSenderHandle,
                                         UINT4 *pu4FsLsppSequenceNumber)
{

    tLsppFsLsppEchoSequenceTableEntry *pLsppFsLsppEchoSequenceTableEntry = NULL;

    pLsppFsLsppEchoSequenceTableEntry = LsppGetFirstFsLsppEchoSequenceTable ();

    if (pLsppFsLsppEchoSequenceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pu4FsLsppContextId =
        pLsppFsLsppEchoSequenceTableEntry->MibObject.u4FsLsppContextId;

    *pu4FsLsppSenderHandle =
        pLsppFsLsppEchoSequenceTableEntry->MibObject.u4FsLsppSenderHandle;

    *pu4FsLsppSequenceNumber =
        pLsppFsLsppEchoSequenceTableEntry->MibObject.u4FsLsppSequenceNumber;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsLsppHopTable
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
                FsLsppHopIndex
 Output      :  The Get First Routines gets the Lexicographically
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsLsppHopTable (UINT4 *pu4FsLsppContextId,
                                UINT4 *pu4FsLsppSenderHandle,
                                UINT4 *pu4FsLsppHopIndex)
{

    tLsppFsLsppHopTableEntry *pLsppFsLsppHopTableEntry = NULL;

    pLsppFsLsppHopTableEntry = LsppGetFirstFsLsppHopTable ();

    if (pLsppFsLsppHopTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pu4FsLsppContextId = pLsppFsLsppHopTableEntry->MibObject.u4FsLsppContextId;

    *pu4FsLsppSenderHandle =
        pLsppFsLsppHopTableEntry->MibObject.u4FsLsppSenderHandle;

    *pu4FsLsppHopIndex = pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopIndex;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsLsppGlobalConfigTable
 Input       :  The Indices
                FsLsppContextId
                nextFsLsppContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsLsppGlobalConfigTable (UINT4 u4FsLsppContextId,
                                        UINT4 *pu4NextFsLsppContextId)
{

    tLsppFsLsppGlobalConfigTableEntry LsppFsLsppGlobalConfigTableEntry;
    tLsppFsLsppGlobalConfigTableEntry *pNextLsppFsLsppGlobalConfigTableEntry =
        NULL;

    MEMSET (&LsppFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));

    /* Assign the index */
    LsppFsLsppGlobalConfigTableEntry.MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pNextLsppFsLsppGlobalConfigTableEntry =
        LsppGetNextFsLsppGlobalConfigTable (&LsppFsLsppGlobalConfigTableEntry);

    if (pNextLsppFsLsppGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pu4NextFsLsppContextId =
        pNextLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsLsppGlobalStatsTable
 Input       :  The Indices
                FsLsppContextId
                nextFsLsppContextId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsLsppGlobalStatsTable (UINT4 u4FsLsppContextId,
                                       UINT4 *pu4NextFsLsppContextId)
{

    tLsppFsLsppGlobalStatsTableEntry LsppFsLsppGlobalStatsTableEntry;
    tLsppFsLsppGlobalStatsTableEntry *pNextLsppFsLsppGlobalStatsTableEntry =
        NULL;

    MEMSET (&LsppFsLsppGlobalStatsTableEntry, 0,
            sizeof (tLsppFsLsppGlobalStatsTableEntry));

    /* Assign the index */
    LsppFsLsppGlobalStatsTableEntry.MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pNextLsppFsLsppGlobalStatsTableEntry =
        LsppGetNextFsLsppGlobalStatsTable (&LsppFsLsppGlobalStatsTableEntry);

    if (pNextLsppFsLsppGlobalStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pu4NextFsLsppContextId =
        pNextLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppContextId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsLsppPingTraceTable
 Input       :  The Indices
                FsLsppContextId
                nextFsLsppContextId
                FsLsppSenderHandle
                nextFsLsppSenderHandle
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsLsppPingTraceTable (UINT4 u4FsLsppContextId,
                                     UINT4 *pu4NextFsLsppContextId,
                                     UINT4 u4FsLsppSenderHandle,
                                     UINT4 *pu4NextFsLsppSenderHandle)
{

    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    tLsppFsLsppPingTraceTableEntry *pNextLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    pNextLsppFsLsppPingTraceTableEntry =
        LsppGetNextFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry);

    if (pNextLsppFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pu4NextFsLsppContextId =
        pNextLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId;

    *pu4NextFsLsppSenderHandle =
        pNextLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsLsppEchoSequenceTable
 Input       :  The Indices
                FsLsppContextId
                nextFsLsppContextId
                FsLsppSenderHandle
                nextFsLsppSenderHandle
                FsLsppSequenceNumber
                nextFsLsppSequenceNumber
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsLsppEchoSequenceTable (UINT4 u4FsLsppContextId,
                                        UINT4 *pu4NextFsLsppContextId,
                                        UINT4 u4FsLsppSenderHandle,
                                        UINT4 *pu4NextFsLsppSenderHandle,
                                        UINT4 u4FsLsppSequenceNumber,
                                        UINT4 *pu4NextFsLsppSequenceNumber)
{

    tLsppFsLsppEchoSequenceTableEntry LsppFsLsppEchoSequenceTableEntry;
    tLsppFsLsppEchoSequenceTableEntry *pNextLsppFsLsppEchoSequenceTableEntry =
        NULL;

    MEMSET (&LsppFsLsppEchoSequenceTableEntry, 0,
            sizeof (tLsppFsLsppEchoSequenceTableEntry));

    /* Assign the index */
    LsppFsLsppEchoSequenceTableEntry.MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    LsppFsLsppEchoSequenceTableEntry.MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    LsppFsLsppEchoSequenceTableEntry.MibObject.u4FsLsppSequenceNumber =
        u4FsLsppSequenceNumber;

    pNextLsppFsLsppEchoSequenceTableEntry =
        LsppGetNextFsLsppEchoSequenceTable (&LsppFsLsppEchoSequenceTableEntry);

    if (pNextLsppFsLsppEchoSequenceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pu4NextFsLsppContextId =
        pNextLsppFsLsppEchoSequenceTableEntry->MibObject.u4FsLsppContextId;

    *pu4NextFsLsppSenderHandle =
        pNextLsppFsLsppEchoSequenceTableEntry->MibObject.u4FsLsppSenderHandle;

    *pu4NextFsLsppSequenceNumber =
        pNextLsppFsLsppEchoSequenceTableEntry->MibObject.u4FsLsppSequenceNumber;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsLsppHopTable
 Input       :  The Indices
                FsLsppContextId
                nextFsLsppContextId
                FsLsppSenderHandle
                nextFsLsppSenderHandle
                FsLsppHopIndex
                nextFsLsppHopIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine. */

INT1
nmhGetNextIndexFsLsppHopTable (UINT4 u4FsLsppContextId,
                               UINT4 *pu4NextFsLsppContextId,
                               UINT4 u4FsLsppSenderHandle,
                               UINT4 *pu4NextFsLsppSenderHandle,
                               UINT4 u4FsLsppHopIndex,
                               UINT4 *pu4NextFsLsppHopIndex)
{

    tLsppFsLsppHopTableEntry *pLsppFsLsppHopTableEntry = NULL;

    tLsppFsLsppHopTableEntry *pNextLsppFsLsppHopTableEntry = NULL;

    pLsppFsLsppHopTableEntry =
        (tLsppFsLsppHopTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPHOPTABLE_POOLID);

    if (pLsppFsLsppHopTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppHopTableEntry, 0, sizeof (tLsppFsLsppHopTableEntry));

    /* Assign the index */
    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppContextId = u4FsLsppContextId;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopIndex = u4FsLsppHopIndex;

    pNextLsppFsLsppHopTableEntry =
        LsppGetNextFsLsppHopTable (pLsppFsLsppHopTableEntry);

    if (pNextLsppFsLsppHopTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppHopTableEntry);
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pu4NextFsLsppContextId =
        pNextLsppFsLsppHopTableEntry->MibObject.u4FsLsppContextId;

    *pu4NextFsLsppSenderHandle =
        pNextLsppFsLsppHopTableEntry->MibObject.u4FsLsppSenderHandle;

    *pu4NextFsLsppHopIndex =
        pNextLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopIndex;

    MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppHopTableEntry);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppSystemControl
 Input       :  The Indices
                FsLsppContextId

                The Object 
                INT4 *pi4RetValFsLsppSystemControl
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppSystemControl (UINT4 u4FsLsppContextId,
                           INT4 *pi4RetValFsLsppSystemControl)
{
    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigTableEntry = NULL;

    pLsppFsLsppGlobalConfigTableEntry =
        (tLsppFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);

    if (pLsppFsLsppGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalConfigTable (pLsppFsLsppGlobalConfigTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsLsppSystemControl =
        pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppSystemControl;

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppTrapStatus
 Input       :  The Indices
                FsLsppContextId

                The Object 
                INT4 *pi4RetValFsLsppTrapStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppTrapStatus (UINT4 u4FsLsppContextId,
                        INT4 *pi4RetValFsLsppTrapStatus)
{
    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigTableEntry = NULL;

    pLsppFsLsppGlobalConfigTableEntry =
        (tLsppFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);

    if (pLsppFsLsppGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalConfigTable (pLsppFsLsppGlobalConfigTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsLsppTrapStatus =
        pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppTrapStatus;

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppTraceLevel
 Input       :  The Indices
                FsLsppContextId

                The Object 
                INT4 *pi4RetValFsLsppTraceLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppTraceLevel (UINT4 u4FsLsppContextId,
                        INT4 *pi4RetValFsLsppTraceLevel)
{
    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigTableEntry = NULL;

    pLsppFsLsppGlobalConfigTableEntry =
        (tLsppFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);

    if (pLsppFsLsppGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalConfigTable (pLsppFsLsppGlobalConfigTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsLsppTraceLevel =
        pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppTraceLevel;

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppAgeOutTime
 Input       :  The Indices
                FsLsppContextId

                The Object 
                UINT4 *pu4RetValFsLsppAgeOutTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppAgeOutTime (UINT4 u4FsLsppContextId,
                        UINT4 *pu4RetValFsLsppAgeOutTime)
{
    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigTableEntry = NULL;

    pLsppFsLsppGlobalConfigTableEntry =
        (tLsppFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);

    if (pLsppFsLsppGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalConfigTable (pLsppFsLsppGlobalConfigTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppAgeOutTime =
        pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppAgeOutTime;

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppAgeOutTmrUnit
 Input       :  The Indices
                FsLsppContextId

                The Object 
                INT4 *pi4RetValFsLsppAgeOutTmrUnit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppAgeOutTmrUnit (UINT4 u4FsLsppContextId,
                           INT4 *pi4RetValFsLsppAgeOutTmrUnit)
{
    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigTableEntry = NULL;

    pLsppFsLsppGlobalConfigTableEntry =
        (tLsppFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);

    if (pLsppFsLsppGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalConfigTable (pLsppFsLsppGlobalConfigTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsLsppAgeOutTmrUnit =
        pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppAgeOutTmrUnit;

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppClearEchoStats
 Input       :  The Indices
                FsLsppContextId

                The Object 
                INT4 *pi4RetValFsLsppClearEchoStats
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppClearEchoStats (UINT4 u4FsLsppContextId,
                            INT4 *pi4RetValFsLsppClearEchoStats)
{
    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigTableEntry = NULL;

    pLsppFsLsppGlobalConfigTableEntry =
        (tLsppFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);

    if (pLsppFsLsppGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalConfigTable (pLsppFsLsppGlobalConfigTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsLsppClearEchoStats =
        pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppClearEchoStats;

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppBfdBtStrapRespReq
 Input       :  The Indices
                FsLsppContextId

                The Object 
                INT4 *pi4RetValFsLsppBfdBtStrapRespReq
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppBfdBtStrapRespReq (UINT4 u4FsLsppContextId,
                               INT4 *pi4RetValFsLsppBfdBtStrapRespReq)
{
    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigTableEntry = NULL;

    pLsppFsLsppGlobalConfigTableEntry =
        (tLsppFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);

    if (pLsppFsLsppGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalConfigTable (pLsppFsLsppGlobalConfigTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsLsppBfdBtStrapRespReq =
        pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppBfdBtStrapRespReq;

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppBfdBtStrapAgeOutTime
 Input       :  The Indices
                FsLsppContextId

                The Object 
                UINT4 *pu4RetValFsLsppBfdBtStrapAgeOutTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppBfdBtStrapAgeOutTime (UINT4 u4FsLsppContextId,
                                  UINT4 *pu4RetValFsLsppBfdBtStrapAgeOutTime)
{
    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigTableEntry = NULL;

    pLsppFsLsppGlobalConfigTableEntry =
        (tLsppFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);

    if (pLsppFsLsppGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalConfigTable (pLsppFsLsppGlobalConfigTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppBfdBtStrapAgeOutTime =
        pLsppFsLsppGlobalConfigTableEntry->MibObject.
        u4FsLsppBfdBtStrapAgeOutTime;

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppBfdBtStrapAgeOutTmrUnit
 Input       :  The Indices
                FsLsppContextId

                The Object 
                INT4 *pi4RetValFsLsppBfdBtStrapAgeOutTmrUnit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppBfdBtStrapAgeOutTmrUnit (UINT4 u4FsLsppContextId,
                                     INT4
                                     *pi4RetValFsLsppBfdBtStrapAgeOutTmrUnit)
{
    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigTableEntry = NULL;

    pLsppFsLsppGlobalConfigTableEntry =
        (tLsppFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);

    if (pLsppFsLsppGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalConfigTable (pLsppFsLsppGlobalConfigTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsLsppBfdBtStrapAgeOutTmrUnit =
        pLsppFsLsppGlobalConfigTableEntry->MibObject.
        i4FsLsppBfdBtStrapAgeOutTmrUnit;

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppGlbStatReqTx
 Input       :  The Indices
                FsLsppContextId

                The Object 
                UINT4 *pu4RetValFsLsppGlbStatReqTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppGlbStatReqTx (UINT4 u4FsLsppContextId,
                          UINT4 *pu4RetValFsLsppGlbStatReqTx)
{
    tLsppFsLsppGlobalStatsTableEntry *pLsppFsLsppGlobalStatsTableEntry = NULL;

    pLsppFsLsppGlobalStatsTableEntry =
        (tLsppFsLsppGlobalStatsTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID);

    if (pLsppFsLsppGlobalStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalStatsTableEntry, 0,
            sizeof (tLsppFsLsppGlobalStatsTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalStatsTable (pLsppFsLsppGlobalStatsTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppGlbStatReqTx =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatReqTx;

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppGlbStatReqRx
 Input       :  The Indices
                FsLsppContextId

                The Object 
                UINT4 *pu4RetValFsLsppGlbStatReqRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppGlbStatReqRx (UINT4 u4FsLsppContextId,
                          UINT4 *pu4RetValFsLsppGlbStatReqRx)
{
    tLsppFsLsppGlobalStatsTableEntry *pLsppFsLsppGlobalStatsTableEntry = NULL;

    pLsppFsLsppGlobalStatsTableEntry =
        (tLsppFsLsppGlobalStatsTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID);

    if (pLsppFsLsppGlobalStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalStatsTableEntry, 0,
            sizeof (tLsppFsLsppGlobalStatsTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalStatsTable (pLsppFsLsppGlobalStatsTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppGlbStatReqRx =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatReqRx;

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppGlbStatReqTimedOut
 Input       :  The Indices
                FsLsppContextId

                The Object 
                UINT4 *pu4RetValFsLsppGlbStatReqTimedOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppGlbStatReqTimedOut (UINT4 u4FsLsppContextId,
                                UINT4 *pu4RetValFsLsppGlbStatReqTimedOut)
{
    tLsppFsLsppGlobalStatsTableEntry *pLsppFsLsppGlobalStatsTableEntry = NULL;

    pLsppFsLsppGlobalStatsTableEntry =
        (tLsppFsLsppGlobalStatsTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID);

    if (pLsppFsLsppGlobalStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalStatsTableEntry, 0,
            sizeof (tLsppFsLsppGlobalStatsTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalStatsTable (pLsppFsLsppGlobalStatsTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppGlbStatReqTimedOut =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatReqTimedOut;

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppGlbStatReqUnSent
 Input       :  The Indices
                FsLsppContextId

                The Object 
                UINT4 *pu4RetValFsLsppGlbStatReqUnSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppGlbStatReqUnSent (UINT4 u4FsLsppContextId,
                              UINT4 *pu4RetValFsLsppGlbStatReqUnSent)
{
    tLsppFsLsppGlobalStatsTableEntry *pLsppFsLsppGlobalStatsTableEntry = NULL;

    pLsppFsLsppGlobalStatsTableEntry =
        (tLsppFsLsppGlobalStatsTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID);

    if (pLsppFsLsppGlobalStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalStatsTableEntry, 0,
            sizeof (tLsppFsLsppGlobalStatsTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalStatsTable (pLsppFsLsppGlobalStatsTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppGlbStatReqUnSent =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatReqUnSent;

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppGlbStatReplyTx
 Input       :  The Indices
                FsLsppContextId

                The Object 
                UINT4 *pu4RetValFsLsppGlbStatReplyTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppGlbStatReplyTx (UINT4 u4FsLsppContextId,
                            UINT4 *pu4RetValFsLsppGlbStatReplyTx)
{
    tLsppFsLsppGlobalStatsTableEntry *pLsppFsLsppGlobalStatsTableEntry = NULL;

    pLsppFsLsppGlobalStatsTableEntry =
        (tLsppFsLsppGlobalStatsTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID);

    if (pLsppFsLsppGlobalStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalStatsTableEntry, 0,
            sizeof (tLsppFsLsppGlobalStatsTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalStatsTable (pLsppFsLsppGlobalStatsTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppGlbStatReplyTx =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatReplyTx;

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppGlbStatReplyRx
 Input       :  The Indices
                FsLsppContextId

                The Object 
                UINT4 *pu4RetValFsLsppGlbStatReplyRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppGlbStatReplyRx (UINT4 u4FsLsppContextId,
                            UINT4 *pu4RetValFsLsppGlbStatReplyRx)
{
    tLsppFsLsppGlobalStatsTableEntry *pLsppFsLsppGlobalStatsTableEntry = NULL;

    pLsppFsLsppGlobalStatsTableEntry =
        (tLsppFsLsppGlobalStatsTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID);

    if (pLsppFsLsppGlobalStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalStatsTableEntry, 0,
            sizeof (tLsppFsLsppGlobalStatsTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalStatsTable (pLsppFsLsppGlobalStatsTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppGlbStatReplyRx =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatReplyRx;

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppGlbStatReplyDropped
 Input       :  The Indices
                FsLsppContextId

                The Object 
                UINT4 *pu4RetValFsLsppGlbStatReplyDropped
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppGlbStatReplyDropped (UINT4 u4FsLsppContextId,
                                 UINT4 *pu4RetValFsLsppGlbStatReplyDropped)
{
    tLsppFsLsppGlobalStatsTableEntry *pLsppFsLsppGlobalStatsTableEntry = NULL;

    pLsppFsLsppGlobalStatsTableEntry =
        (tLsppFsLsppGlobalStatsTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID);

    if (pLsppFsLsppGlobalStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalStatsTableEntry, 0,
            sizeof (tLsppFsLsppGlobalStatsTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalStatsTable (pLsppFsLsppGlobalStatsTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppGlbStatReplyDropped =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatReplyDropped;

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppGlbStatReplyUnSent
 Input       :  The Indices
                FsLsppContextId

                The Object 
                UINT4 *pu4RetValFsLsppGlbStatReplyUnSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppGlbStatReplyUnSent (UINT4 u4FsLsppContextId,
                                UINT4 *pu4RetValFsLsppGlbStatReplyUnSent)
{
    tLsppFsLsppGlobalStatsTableEntry *pLsppFsLsppGlobalStatsTableEntry = NULL;

    pLsppFsLsppGlobalStatsTableEntry =
        (tLsppFsLsppGlobalStatsTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID);

    if (pLsppFsLsppGlobalStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalStatsTableEntry, 0,
            sizeof (tLsppFsLsppGlobalStatsTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalStatsTable (pLsppFsLsppGlobalStatsTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppGlbStatReplyUnSent =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatReplyUnSent;

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppGlbStatReplyFromEgr
 Input       :  The Indices
                FsLsppContextId

                The Object 
                UINT4 *pu4RetValFsLsppGlbStatReplyFromEgr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppGlbStatReplyFromEgr (UINT4 u4FsLsppContextId,
                                 UINT4 *pu4RetValFsLsppGlbStatReplyFromEgr)
{
    tLsppFsLsppGlobalStatsTableEntry *pLsppFsLsppGlobalStatsTableEntry = NULL;

    pLsppFsLsppGlobalStatsTableEntry =
        (tLsppFsLsppGlobalStatsTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID);

    if (pLsppFsLsppGlobalStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalStatsTableEntry, 0,
            sizeof (tLsppFsLsppGlobalStatsTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalStatsTable (pLsppFsLsppGlobalStatsTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppGlbStatReplyFromEgr =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatReplyFromEgr;

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppGlbStatUnLbldOutIf
 Input       :  The Indices
                FsLsppContextId

                The Object 
                UINT4 *pu4RetValFsLsppGlbStatUnLbldOutIf
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppGlbStatUnLbldOutIf (UINT4 u4FsLsppContextId,
                                UINT4 *pu4RetValFsLsppGlbStatUnLbldOutIf)
{
    tLsppFsLsppGlobalStatsTableEntry *pLsppFsLsppGlobalStatsTableEntry = NULL;

    pLsppFsLsppGlobalStatsTableEntry =
        (tLsppFsLsppGlobalStatsTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID);

    if (pLsppFsLsppGlobalStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalStatsTableEntry, 0,
            sizeof (tLsppFsLsppGlobalStatsTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalStatsTable (pLsppFsLsppGlobalStatsTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppGlbStatUnLbldOutIf =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatUnLbldOutIf;

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppGlbStatDsMapMismatch
 Input       :  The Indices
                FsLsppContextId

                The Object 
                UINT4 *pu4RetValFsLsppGlbStatDsMapMismatch
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppGlbStatDsMapMismatch (UINT4 u4FsLsppContextId,
                                  UINT4 *pu4RetValFsLsppGlbStatDsMapMismatch)
{
    tLsppFsLsppGlobalStatsTableEntry *pLsppFsLsppGlobalStatsTableEntry = NULL;

    pLsppFsLsppGlobalStatsTableEntry =
        (tLsppFsLsppGlobalStatsTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID);

    if (pLsppFsLsppGlobalStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalStatsTableEntry, 0,
            sizeof (tLsppFsLsppGlobalStatsTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalStatsTable (pLsppFsLsppGlobalStatsTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppGlbStatDsMapMismatch =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.
        u4FsLsppGlbStatDsMapMismatch;

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppGlbStatFecLblMismatch
 Input       :  The Indices
                FsLsppContextId

                The Object 
                UINT4 *pu4RetValFsLsppGlbStatFecLblMismatch
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppGlbStatFecLblMismatch (UINT4 u4FsLsppContextId,
                                   UINT4 *pu4RetValFsLsppGlbStatFecLblMismatch)
{
    tLsppFsLsppGlobalStatsTableEntry *pLsppFsLsppGlobalStatsTableEntry = NULL;

    pLsppFsLsppGlobalStatsTableEntry =
        (tLsppFsLsppGlobalStatsTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID);

    if (pLsppFsLsppGlobalStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalStatsTableEntry, 0,
            sizeof (tLsppFsLsppGlobalStatsTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalStatsTable (pLsppFsLsppGlobalStatsTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppGlbStatFecLblMismatch =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.
        u4FsLsppGlbStatFecLblMismatch;

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppGlbStatNoFecMapping
 Input       :  The Indices
                FsLsppContextId

                The Object 
                UINT4 *pu4RetValFsLsppGlbStatNoFecMapping
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppGlbStatNoFecMapping (UINT4 u4FsLsppContextId,
                                 UINT4 *pu4RetValFsLsppGlbStatNoFecMapping)
{
    tLsppFsLsppGlobalStatsTableEntry *pLsppFsLsppGlobalStatsTableEntry = NULL;

    pLsppFsLsppGlobalStatsTableEntry =
        (tLsppFsLsppGlobalStatsTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID);

    if (pLsppFsLsppGlobalStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalStatsTableEntry, 0,
            sizeof (tLsppFsLsppGlobalStatsTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalStatsTable (pLsppFsLsppGlobalStatsTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppGlbStatNoFecMapping =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatNoFecMapping;

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppGlbStatUnKUpstreamIf
 Input       :  The Indices
                FsLsppContextId

                The Object 
                UINT4 *pu4RetValFsLsppGlbStatUnKUpstreamIf
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppGlbStatUnKUpstreamIf (UINT4 u4FsLsppContextId,
                                  UINT4 *pu4RetValFsLsppGlbStatUnKUpstreamIf)
{
    tLsppFsLsppGlobalStatsTableEntry *pLsppFsLsppGlobalStatsTableEntry = NULL;

    pLsppFsLsppGlobalStatsTableEntry =
        (tLsppFsLsppGlobalStatsTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID);

    if (pLsppFsLsppGlobalStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalStatsTableEntry, 0,
            sizeof (tLsppFsLsppGlobalStatsTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalStatsTable (pLsppFsLsppGlobalStatsTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppGlbStatUnKUpstreamIf =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.
        u4FsLsppGlbStatUnKUpstreamIf;

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppGlbStatReqLblSwitched
 Input       :  The Indices
                FsLsppContextId

                The Object 
                UINT4 *pu4RetValFsLsppGlbStatReqLblSwitched
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppGlbStatReqLblSwitched (UINT4 u4FsLsppContextId,
                                   UINT4 *pu4RetValFsLsppGlbStatReqLblSwitched)
{
    tLsppFsLsppGlobalStatsTableEntry *pLsppFsLsppGlobalStatsTableEntry = NULL;

    pLsppFsLsppGlobalStatsTableEntry =
        (tLsppFsLsppGlobalStatsTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID);

    if (pLsppFsLsppGlobalStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalStatsTableEntry, 0,
            sizeof (tLsppFsLsppGlobalStatsTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalStatsTable (pLsppFsLsppGlobalStatsTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppGlbStatReqLblSwitched =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.
        u4FsLsppGlbStatReqLblSwitched;

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppGlbStatReqUnSupptdTlv
 Input       :  The Indices
                FsLsppContextId

                The Object 
                UINT4 *pu4RetValFsLsppGlbStatReqUnSupptdTlv
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppGlbStatReqUnSupptdTlv (UINT4 u4FsLsppContextId,
                                   UINT4 *pu4RetValFsLsppGlbStatReqUnSupptdTlv)
{
    tLsppFsLsppGlobalStatsTableEntry *pLsppFsLsppGlobalStatsTableEntry = NULL;

    pLsppFsLsppGlobalStatsTableEntry =
        (tLsppFsLsppGlobalStatsTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID);

    if (pLsppFsLsppGlobalStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalStatsTableEntry, 0,
            sizeof (tLsppFsLsppGlobalStatsTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalStatsTable (pLsppFsLsppGlobalStatsTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppGlbStatReqUnSupptdTlv =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.
        u4FsLsppGlbStatReqUnSupptdTlv;

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppGlbStatMalformedReq
 Input       :  The Indices
                FsLsppContextId

                The Object 
                UINT4 *pu4RetValFsLsppGlbStatMalformedReq
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppGlbStatMalformedReq (UINT4 u4FsLsppContextId,
                                 UINT4 *pu4RetValFsLsppGlbStatMalformedReq)
{
    tLsppFsLsppGlobalStatsTableEntry *pLsppFsLsppGlobalStatsTableEntry = NULL;

    pLsppFsLsppGlobalStatsTableEntry =
        (tLsppFsLsppGlobalStatsTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID);

    if (pLsppFsLsppGlobalStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalStatsTableEntry, 0,
            sizeof (tLsppFsLsppGlobalStatsTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalStatsTable (pLsppFsLsppGlobalStatsTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppGlbStatMalformedReq =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatMalformedReq;

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppGlbStatNoLblEntry
 Input       :  The Indices
                FsLsppContextId

                The Object 
                UINT4 *pu4RetValFsLsppGlbStatNoLblEntry
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppGlbStatNoLblEntry (UINT4 u4FsLsppContextId,
                               UINT4 *pu4RetValFsLsppGlbStatNoLblEntry)
{
    tLsppFsLsppGlobalStatsTableEntry *pLsppFsLsppGlobalStatsTableEntry = NULL;

    pLsppFsLsppGlobalStatsTableEntry =
        (tLsppFsLsppGlobalStatsTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID);

    if (pLsppFsLsppGlobalStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalStatsTableEntry, 0,
            sizeof (tLsppFsLsppGlobalStatsTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalStatsTable (pLsppFsLsppGlobalStatsTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppGlbStatNoLblEntry =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatNoLblEntry;

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppGlbStatPreTermReq
 Input       :  The Indices
                FsLsppContextId

                The Object 
                UINT4 *pu4RetValFsLsppGlbStatPreTermReq
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppGlbStatPreTermReq (UINT4 u4FsLsppContextId,
                               UINT4 *pu4RetValFsLsppGlbStatPreTermReq)
{
    tLsppFsLsppGlobalStatsTableEntry *pLsppFsLsppGlobalStatsTableEntry = NULL;

    pLsppFsLsppGlobalStatsTableEntry =
        (tLsppFsLsppGlobalStatsTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID);

    if (pLsppFsLsppGlobalStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalStatsTableEntry, 0,
            sizeof (tLsppFsLsppGlobalStatsTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalStatsTable (pLsppFsLsppGlobalStatsTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppGlbStatPreTermReq =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatPreTermReq;

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppGlbStatProtMismatch
 Input       :  The Indices
                FsLsppContextId

                The Object 
                UINT4 *pu4RetValFsLsppGlbStatProtMismatch
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppGlbStatProtMismatch (UINT4 u4FsLsppContextId,
                                 UINT4 *pu4RetValFsLsppGlbStatProtMismatch)
{
    tLsppFsLsppGlobalStatsTableEntry *pLsppFsLsppGlobalStatsTableEntry = NULL;

    pLsppFsLsppGlobalStatsTableEntry =
        (tLsppFsLsppGlobalStatsTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID);

    if (pLsppFsLsppGlobalStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalStatsTableEntry, 0,
            sizeof (tLsppFsLsppGlobalStatsTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalStatsTable (pLsppFsLsppGlobalStatsTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppGlbStatProtMismatch =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatProtMismatch;

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppGlbStatRsvdRetCode
 Input       :  The Indices
                FsLsppContextId

                The Object 
                UINT4 *pu4RetValFsLsppGlbStatRsvdRetCode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppGlbStatRsvdRetCode (UINT4 u4FsLsppContextId,
                                UINT4 *pu4RetValFsLsppGlbStatRsvdRetCode)
{
    tLsppFsLsppGlobalStatsTableEntry *pLsppFsLsppGlobalStatsTableEntry = NULL;

    pLsppFsLsppGlobalStatsTableEntry =
        (tLsppFsLsppGlobalStatsTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID);

    if (pLsppFsLsppGlobalStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalStatsTableEntry, 0,
            sizeof (tLsppFsLsppGlobalStatsTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalStatsTable (pLsppFsLsppGlobalStatsTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppGlbStatRsvdRetCode =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatRsvdRetCode;

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppGlbStatNoRetCode
 Input       :  The Indices
                FsLsppContextId

                The Object 
                UINT4 *pu4RetValFsLsppGlbStatNoRetCode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppGlbStatNoRetCode (UINT4 u4FsLsppContextId,
                              UINT4 *pu4RetValFsLsppGlbStatNoRetCode)
{
    tLsppFsLsppGlobalStatsTableEntry *pLsppFsLsppGlobalStatsTableEntry = NULL;

    pLsppFsLsppGlobalStatsTableEntry =
        (tLsppFsLsppGlobalStatsTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID);

    if (pLsppFsLsppGlobalStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalStatsTableEntry, 0,
            sizeof (tLsppFsLsppGlobalStatsTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalStatsTable (pLsppFsLsppGlobalStatsTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppGlbStatNoRetCode =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatNoRetCode;

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppGlbStatUndefRetCode
 Input       :  The Indices
                FsLsppContextId

                The Object 
                UINT4 *pu4RetValFsLsppGlbStatUndefRetCode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppGlbStatUndefRetCode (UINT4 u4FsLsppContextId,
                                 UINT4 *pu4RetValFsLsppGlbStatUndefRetCode)
{
    tLsppFsLsppGlobalStatsTableEntry *pLsppFsLsppGlobalStatsTableEntry = NULL;

    pLsppFsLsppGlobalStatsTableEntry =
        (tLsppFsLsppGlobalStatsTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID);

    if (pLsppFsLsppGlobalStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalStatsTableEntry, 0,
            sizeof (tLsppFsLsppGlobalStatsTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalStatsTable (pLsppFsLsppGlobalStatsTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppGlbStatUndefRetCode =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatUndefRetCode;

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppGlbStatInvalidPktDropped
 Input       :  The Indices
                FsLsppContextId

                The Object 
                UINT4 *pu4RetValFsLsppGlbStatInvalidPktDropped
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppGlbStatInvalidPktDropped (UINT4 u4FsLsppContextId,
                                      UINT4
                                      *pu4RetValFsLsppGlbStatInvalidPktDropped)
{
    tLsppFsLsppGlobalStatsTableEntry *pLsppFsLsppGlobalStatsTableEntry = NULL;

    pLsppFsLsppGlobalStatsTableEntry =
        (tLsppFsLsppGlobalStatsTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID);

    if (pLsppFsLsppGlobalStatsTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalStatsTableEntry, 0,
            sizeof (tLsppFsLsppGlobalStatsTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    if (LsppGetAllFsLsppGlobalStatsTable (pLsppFsLsppGlobalStatsTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppGlbStatInvalidPktDropped =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.
        u4FsLsppGlbStatInvalidPktDropped;

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALSTATSTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalStatsTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppRequestType
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                INT4 *pi4RetValFsLsppRequestType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppRequestType (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                         INT4 *pi4RetValFsLsppRequestType)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsLsppRequestType =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRequestType;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppRequestOwner
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                INT4 *pi4RetValFsLsppRequestOwner
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppRequestOwner (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                          INT4 *pi4RetValFsLsppRequestOwner)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsLsppRequestOwner =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRequestOwner;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppPathType
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                INT4 *pi4RetValFsLsppPathType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppPathType (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                      INT4 *pi4RetValFsLsppPathType)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsLsppPathType =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppPathType;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppPathPointer
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                tSNMP_OID_TYPE * pRetValFsLsppPathPointer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppPathPointer (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                         tSNMP_OID_TYPE * pRetValFsLsppPathPointer)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsLsppPathPointer->pu4_OidList,
            pLsppFsLsppPingTraceTableEntry->MibObject.au4FsLsppPathPointer,
            pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppPathPointerLen *
            sizeof (UINT4));
    pRetValFsLsppPathPointer->u4_Length =
       (UINT4)( pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppPathPointerLen);

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppTgtMipGlobalId
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                UINT4 *pu4RetValFsLsppTgtMipGlobalId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppTgtMipGlobalId (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                            UINT4 *pu4RetValFsLsppTgtMipGlobalId)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppTgtMipGlobalId =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipGlobalId;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppTgtMipNodeId
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                UINT4 *pu4RetValFsLsppTgtMipNodeId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppTgtMipNodeId (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                          UINT4 *pu4RetValFsLsppTgtMipNodeId)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppTgtMipNodeId =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipNodeId;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppTgtMipIfNum
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                UINT4 *pu4RetValFsLsppTgtMipIfNum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppTgtMipIfNum (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                         UINT4 *pu4RetValFsLsppTgtMipIfNum)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppTgtMipIfNum =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipIfNum;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppReplyMode
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                INT4 *pi4RetValFsLsppReplyMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppReplyMode (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                       INT4 *pi4RetValFsLsppReplyMode)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsLsppReplyMode =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppReplyMode;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppRepeatCount
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                UINT4 *pu4RetValFsLsppRepeatCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppRepeatCount (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                         UINT4 *pu4RetValFsLsppRepeatCount)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppRepeatCount =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppRepeatCount;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppPacketSize
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                UINT4 *pu4RetValFsLsppPacketSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppPacketSize (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                        UINT4 *pu4RetValFsLsppPacketSize)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppPacketSize =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppPacketSize;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppPadPattern
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsLsppPadPattern
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppPadPattern (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                        tSNMP_OCTET_STRING_TYPE * pRetValFsLsppPadPattern)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsLsppPadPattern->pu1_OctetList,
            pLsppFsLsppPingTraceTableEntry->MibObject.au1FsLsppPadPattern,
            pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppPadPatternLen);
    pRetValFsLsppPadPattern->i4_Length =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppPadPatternLen;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppTTLValue
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                UINT4 *pu4RetValFsLsppTTLValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppTTLValue (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                      UINT4 *pu4RetValFsLsppTTLValue)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppTTLValue =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTTLValue;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppWFRInterval
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                UINT4 *pu4RetValFsLsppWFRInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppWFRInterval (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                         UINT4 *pu4RetValFsLsppWFRInterval)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppWFRInterval =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppWFRInterval;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppWFRTmrUnit
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                INT4 *pi4RetValFsLsppWFRTmrUnit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppWFRTmrUnit (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                        INT4 *pi4RetValFsLsppWFRTmrUnit)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsLsppWFRTmrUnit =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppWFRTmrUnit;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppWTSInterval
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                UINT4 *pu4RetValFsLsppWTSInterval
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppWTSInterval (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                         UINT4 *pu4RetValFsLsppWTSInterval)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppWTSInterval =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppWTSInterval;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppWTSTmrUnit
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                INT4 *pi4RetValFsLsppWTSTmrUnit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppWTSTmrUnit (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                        INT4 *pi4RetValFsLsppWTSTmrUnit)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsLsppWTSTmrUnit =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppWTSTmrUnit;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppReplyDscpValue
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                UINT4 *pu4RetValFsLsppReplyDscpValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppReplyDscpValue (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                            UINT4 *pu4RetValFsLsppReplyDscpValue)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppReplyDscpValue =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppReplyDscpValue;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppSweepOption
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                INT4 *pi4RetValFsLsppSweepOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppSweepOption (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                         INT4 *pi4RetValFsLsppSweepOption)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsLsppSweepOption =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppSweepOption;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppSweepMinimum
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                UINT4 *pu4RetValFsLsppSweepMinimum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppSweepMinimum (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                          UINT4 *pu4RetValFsLsppSweepMinimum)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppSweepMinimum =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepMinimum;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppSweepMaximum
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                UINT4 *pu4RetValFsLsppSweepMaximum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppSweepMaximum (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                          UINT4 *pu4RetValFsLsppSweepMaximum)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppSweepMaximum =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepMaximum;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppSweepIncrement
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                UINT4 *pu4RetValFsLsppSweepIncrement
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppSweepIncrement (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                            UINT4 *pu4RetValFsLsppSweepIncrement)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppSweepIncrement =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepIncrement;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppBurstOption
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                INT4 *pi4RetValFsLsppBurstOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppBurstOption (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                         INT4 *pi4RetValFsLsppBurstOption)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsLsppBurstOption =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppBurstOption;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppBurstSize
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                UINT4 *pu4RetValFsLsppBurstSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppBurstSize (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                       UINT4 *pu4RetValFsLsppBurstSize)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppBurstSize =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppBurstSize;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppEXPValue
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                UINT4 *pu4RetValFsLsppEXPValue
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppEXPValue (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                      UINT4 *pu4RetValFsLsppEXPValue)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppEXPValue =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppEXPValue;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppDsMap
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                INT4 *pi4RetValFsLsppDsMap
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppDsMap (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                   INT4 *pi4RetValFsLsppDsMap)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsLsppDsMap =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppDsMap;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppFecValidate
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                INT4 *pi4RetValFsLsppFecValidate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppFecValidate (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                         INT4 *pi4RetValFsLsppFecValidate)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsLsppFecValidate =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppFecValidate;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppReplyPadTlv
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                INT4 *pi4RetValFsLsppReplyPadTlv
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppReplyPadTlv (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                         INT4 *pi4RetValFsLsppReplyPadTlv)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsLsppReplyPadTlv =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppReplyPadTlv;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppForceExplicitNull
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                INT4 *pi4RetValFsLsppForceExplicitNull
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppForceExplicitNull (UINT4 u4FsLsppContextId,
                               UINT4 u4FsLsppSenderHandle,
                               INT4 *pi4RetValFsLsppForceExplicitNull)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsLsppForceExplicitNull =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppForceExplicitNull;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppInterfaceLabelTlv
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                INT4 *pi4RetValFsLsppInterfaceLabelTlv
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppInterfaceLabelTlv (UINT4 u4FsLsppContextId,
                               UINT4 u4FsLsppSenderHandle,
                               INT4 *pi4RetValFsLsppInterfaceLabelTlv)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsLsppInterfaceLabelTlv =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppInterfaceLabelTlv;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppSameSeqNumOption
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                INT4 *pi4RetValFsLsppSameSeqNumOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppSameSeqNumOption (UINT4 u4FsLsppContextId,
                              UINT4 u4FsLsppSenderHandle,
                              INT4 *pi4RetValFsLsppSameSeqNumOption)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsLsppSameSeqNumOption =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppSameSeqNumOption;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppVerbose
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                INT4 *pi4RetValFsLsppVerbose
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppVerbose (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                     INT4 *pi4RetValFsLsppVerbose)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsLsppVerbose =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppVerbose;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppReversePathVerify
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                INT4 *pi4RetValFsLsppReversePathVerify
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppReversePathVerify (UINT4 u4FsLsppContextId,
                               UINT4 u4FsLsppSenderHandle,
                               INT4 *pi4RetValFsLsppReversePathVerify)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsLsppReversePathVerify =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppReversePathVerify;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppEncapType
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                INT4 *pi4RetValFsLsppEncapType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppEncapType (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                       INT4 *pi4RetValFsLsppEncapType)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsLsppEncapType =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppEncapType;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppStatus
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                INT4 *pi4RetValFsLsppStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppStatus (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                    INT4 *pi4RetValFsLsppStatus)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsLsppStatus =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppStatus;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppActualHopCount
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                UINT4 *pu4RetValFsLsppActualHopCount
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppActualHopCount (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                            UINT4 *pu4RetValFsLsppActualHopCount)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppActualHopCount =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppActualHopCount;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppResponderAddrType
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                INT4 *pi4RetValFsLsppResponderAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppResponderAddrType (UINT4 u4FsLsppContextId,
                               UINT4 u4FsLsppSenderHandle,
                               INT4 *pi4RetValFsLsppResponderAddrType)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsLsppResponderAddrType =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppResponderAddrType;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppResponderAddr
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsLsppResponderAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppResponderAddr (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                           tSNMP_OCTET_STRING_TYPE * pRetValFsLsppResponderAddr)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsLsppResponderAddr->pu1_OctetList,
            pLsppFsLsppPingTraceTableEntry->MibObject.au1FsLsppResponderAddr,
            pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppResponderAddrLen);
    pRetValFsLsppResponderAddr->i4_Length =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppResponderAddrLen;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppResponderGlobalId
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                UINT4 *pu4RetValFsLsppResponderGlobalId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppResponderGlobalId (UINT4 u4FsLsppContextId,
                               UINT4 u4FsLsppSenderHandle,
                               UINT4 *pu4RetValFsLsppResponderGlobalId)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppResponderGlobalId =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppResponderGlobalId;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppResponderId
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                UINT4 *pu4RetValFsLsppResponderId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppResponderId (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                         UINT4 *pu4RetValFsLsppResponderId)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppResponderId =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppResponderId;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppMaxRtt
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                UINT4 *pu4RetValFsLsppMaxRtt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppMaxRtt (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                    UINT4 *pu4RetValFsLsppMaxRtt)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppMaxRtt =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppMaxRtt;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppMinRtt
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                UINT4 *pu4RetValFsLsppMinRtt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppMinRtt (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                    UINT4 *pu4RetValFsLsppMinRtt)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppMinRtt =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppMinRtt;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppAverageRtt
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                UINT4 *pu4RetValFsLsppAverageRtt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppAverageRtt (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                        UINT4 *pu4RetValFsLsppAverageRtt)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppAverageRtt =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppAverageRtt;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppPktsTx
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                UINT4 *pu4RetValFsLsppPktsTx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppPktsTx (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                    UINT4 *pu4RetValFsLsppPktsTx)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppPktsTx =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppPktsTx;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppPktsRx
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                UINT4 *pu4RetValFsLsppPktsRx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppPktsRx (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                    UINT4 *pu4RetValFsLsppPktsRx)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppPktsRx =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppPktsRx;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppPktsUnSent
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                UINT4 *pu4RetValFsLsppPktsUnSent
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppPktsUnSent (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                        UINT4 *pu4RetValFsLsppPktsUnSent)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppPktsUnSent =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppPktsUnSent;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppRowStatus
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                INT4 *pi4RetValFsLsppRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppRowStatus (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                       INT4 *pi4RetValFsLsppRowStatus)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsLsppRowStatus =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRowStatus;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppStatusPathDirection
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                INT4 *pi4RetValFsLsppStatusPathDirection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppStatusPathDirection (UINT4 u4FsLsppContextId,
                                 UINT4 u4FsLsppSenderHandle,
                                 INT4 *pi4RetValFsLsppStatusPathDirection)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsLsppStatusPathDirection =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppStatusPathDirection;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppResponderIcc
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsLsppResponderIcc
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppResponderIcc (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                          tSNMP_OCTET_STRING_TYPE * pRetValFsLsppResponderIcc)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsLsppResponderIcc->pu1_OctetList,
            pLsppFsLsppPingTraceTableEntry->MibObject.au1FsLsppResponderIcc,
            pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppResponderIccLen);
    pRetValFsLsppResponderIcc->i4_Length =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppResponderIccLen;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppResponderUMC
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsLsppResponderUMC
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppResponderUMC (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                          tSNMP_OCTET_STRING_TYPE * pRetValFsLsppResponderUMC)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsLsppResponderUMC->pu1_OctetList,
            pLsppFsLsppPingTraceTableEntry->MibObject.au1FsLsppResponderUMC,
            pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppResponderUMCLen);
    pRetValFsLsppResponderUMC->i4_Length =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppResponderUMCLen;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppResponderMepIndex
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                UINT4 *pu4RetValFsLsppResponderMepIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppResponderMepIndex (UINT4 u4FsLsppContextId,
                               UINT4 u4FsLsppSenderHandle,
                               UINT4 *pu4RetValFsLsppResponderMepIndex)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    if (LsppGetAllFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppResponderMepIndex =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppResponderMepIndex;

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppReturnCode
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
                FsLsppSequenceNumber

                The Object 
                UINT4 *pu4RetValFsLsppReturnCode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppReturnCode (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                        UINT4 u4FsLsppSequenceNumber,
                        UINT4 *pu4RetValFsLsppReturnCode)
{
    tLsppFsLsppEchoSequenceTableEntry *pLsppFsLsppEchoSequenceTableEntry = NULL;

    pLsppFsLsppEchoSequenceTableEntry =
        (tLsppFsLsppEchoSequenceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPECHOSEQUENCETABLE_POOLID);

    if (pLsppFsLsppEchoSequenceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppEchoSequenceTableEntry, 0,
            sizeof (tLsppFsLsppEchoSequenceTableEntry));

    /* Assign the index */
    pLsppFsLsppEchoSequenceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppEchoSequenceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    pLsppFsLsppEchoSequenceTableEntry->MibObject.u4FsLsppSequenceNumber =
        u4FsLsppSequenceNumber;

    if (LsppGetAllFsLsppEchoSequenceTable (pLsppFsLsppEchoSequenceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPECHOSEQUENCETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppEchoSequenceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppReturnCode =
        pLsppFsLsppEchoSequenceTableEntry->MibObject.u4FsLsppReturnCode;

    MemReleaseMemBlock (LSPP_FSLSPPECHOSEQUENCETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppEchoSequenceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppReturnSubCode
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
                FsLsppSequenceNumber

                The Object 
                UINT4 *pu4RetValFsLsppReturnSubCode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppReturnSubCode (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                           UINT4 u4FsLsppSequenceNumber,
                           UINT4 *pu4RetValFsLsppReturnSubCode)
{
    tLsppFsLsppEchoSequenceTableEntry *pLsppFsLsppEchoSequenceTableEntry = NULL;

    pLsppFsLsppEchoSequenceTableEntry =
        (tLsppFsLsppEchoSequenceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPECHOSEQUENCETABLE_POOLID);

    if (pLsppFsLsppEchoSequenceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppEchoSequenceTableEntry, 0,
            sizeof (tLsppFsLsppEchoSequenceTableEntry));

    /* Assign the index */
    pLsppFsLsppEchoSequenceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppEchoSequenceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    pLsppFsLsppEchoSequenceTableEntry->MibObject.u4FsLsppSequenceNumber =
        u4FsLsppSequenceNumber;

    if (LsppGetAllFsLsppEchoSequenceTable (pLsppFsLsppEchoSequenceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPECHOSEQUENCETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppEchoSequenceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppReturnSubCode =
        pLsppFsLsppEchoSequenceTableEntry->MibObject.u4FsLsppReturnSubCode;

    MemReleaseMemBlock (LSPP_FSLSPPECHOSEQUENCETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppEchoSequenceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppReturnCodeStr
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
                FsLsppSequenceNumber

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsLsppReturnCodeStr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppReturnCodeStr (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                           UINT4 u4FsLsppSequenceNumber,
                           tSNMP_OCTET_STRING_TYPE * pRetValFsLsppReturnCodeStr)
{
    tLsppFsLsppEchoSequenceTableEntry *pLsppFsLsppEchoSequenceTableEntry = NULL;

    pLsppFsLsppEchoSequenceTableEntry =
        (tLsppFsLsppEchoSequenceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPECHOSEQUENCETABLE_POOLID);

    if (pLsppFsLsppEchoSequenceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppEchoSequenceTableEntry, 0,
            sizeof (tLsppFsLsppEchoSequenceTableEntry));

    /* Assign the index */
    pLsppFsLsppEchoSequenceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;

    pLsppFsLsppEchoSequenceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    pLsppFsLsppEchoSequenceTableEntry->MibObject.u4FsLsppSequenceNumber =
        u4FsLsppSequenceNumber;

    if (LsppGetAllFsLsppEchoSequenceTable (pLsppFsLsppEchoSequenceTableEntry) !=
        OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPECHOSEQUENCETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppEchoSequenceTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsLsppReturnCodeStr->pu1_OctetList,
            pLsppFsLsppEchoSequenceTableEntry->MibObject.au1FsLsppReturnCodeStr,
            pLsppFsLsppEchoSequenceTableEntry->MibObject.
            i4FsLsppReturnCodeStrLen);
    pRetValFsLsppReturnCodeStr->i4_Length =
        pLsppFsLsppEchoSequenceTableEntry->MibObject.i4FsLsppReturnCodeStrLen;

    MemReleaseMemBlock (LSPP_FSLSPPECHOSEQUENCETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppEchoSequenceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppHopAddrType
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
                FsLsppHopIndex

                The Object 
                INT4 *pi4RetValFsLsppHopAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppHopAddrType (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                         UINT4 u4FsLsppHopIndex,
                         INT4 *pi4RetValFsLsppHopAddrType)
{
    tLsppFsLsppHopTableEntry *pLsppFsLsppHopTableEntry = NULL;

    pLsppFsLsppHopTableEntry =
        (tLsppFsLsppHopTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPHOPTABLE_POOLID);

    if (pLsppFsLsppHopTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppHopTableEntry, 0, sizeof (tLsppFsLsppHopTableEntry));

    /* Assign the index */
    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppContextId = u4FsLsppContextId;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopIndex = u4FsLsppHopIndex;

    if (LsppGetAllFsLsppHopTable (pLsppFsLsppHopTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppHopTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsLsppHopAddrType =
        pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopAddrType;

    MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppHopTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppHopAddr
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
                FsLsppHopIndex

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsLsppHopAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppHopAddr (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                     UINT4 u4FsLsppHopIndex,
                     tSNMP_OCTET_STRING_TYPE * pRetValFsLsppHopAddr)
{
    tLsppFsLsppHopTableEntry *pLsppFsLsppHopTableEntry = NULL;

    pLsppFsLsppHopTableEntry =
        (tLsppFsLsppHopTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPHOPTABLE_POOLID);

    if (pLsppFsLsppHopTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppHopTableEntry, 0, sizeof (tLsppFsLsppHopTableEntry));

    /* Assign the index */
    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppContextId = u4FsLsppContextId;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopIndex = u4FsLsppHopIndex;

    if (LsppGetAllFsLsppHopTable (pLsppFsLsppHopTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppHopTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsLsppHopAddr->pu1_OctetList,
            pLsppFsLsppHopTableEntry->MibObject.au1FsLsppHopAddr,
            pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopAddrLen);
    pRetValFsLsppHopAddr->i4_Length =
        pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopAddrLen;

    MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppHopTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppHopGlobalId
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
                FsLsppHopIndex

                The Object 
                UINT4 *pu4RetValFsLsppHopGlobalId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppHopGlobalId (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                         UINT4 u4FsLsppHopIndex,
                         UINT4 *pu4RetValFsLsppHopGlobalId)
{
    tLsppFsLsppHopTableEntry *pLsppFsLsppHopTableEntry = NULL;

    pLsppFsLsppHopTableEntry =
        (tLsppFsLsppHopTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPHOPTABLE_POOLID);

    if (pLsppFsLsppHopTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppHopTableEntry, 0, sizeof (tLsppFsLsppHopTableEntry));

    /* Assign the index */
    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppContextId = u4FsLsppContextId;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopIndex = u4FsLsppHopIndex;

    if (LsppGetAllFsLsppHopTable (pLsppFsLsppHopTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppHopTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppHopGlobalId =
        pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopGlobalId;

    MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppHopTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppHopId
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
                FsLsppHopIndex

                The Object 
                UINT4 *pu4RetValFsLsppHopId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppHopId (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                   UINT4 u4FsLsppHopIndex, UINT4 *pu4RetValFsLsppHopId)
{
    tLsppFsLsppHopTableEntry *pLsppFsLsppHopTableEntry = NULL;

    pLsppFsLsppHopTableEntry =
        (tLsppFsLsppHopTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPHOPTABLE_POOLID);

    if (pLsppFsLsppHopTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppHopTableEntry, 0, sizeof (tLsppFsLsppHopTableEntry));

    /* Assign the index */
    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppContextId = u4FsLsppContextId;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopIndex = u4FsLsppHopIndex;

    if (LsppGetAllFsLsppHopTable (pLsppFsLsppHopTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppHopTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppHopId = pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopId;

    MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppHopTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppHopIfNum
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
                FsLsppHopIndex

                The Object 
                UINT4 *pu4RetValFsLsppHopIfNum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppHopIfNum (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                      UINT4 u4FsLsppHopIndex, UINT4 *pu4RetValFsLsppHopIfNum)
{
    tLsppFsLsppHopTableEntry *pLsppFsLsppHopTableEntry = NULL;

    pLsppFsLsppHopTableEntry =
        (tLsppFsLsppHopTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPHOPTABLE_POOLID);

    if (pLsppFsLsppHopTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppHopTableEntry, 0, sizeof (tLsppFsLsppHopTableEntry));

    /* Assign the index */
    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppContextId = u4FsLsppContextId;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopIndex = u4FsLsppHopIndex;

    if (LsppGetAllFsLsppHopTable (pLsppFsLsppHopTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppHopTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppHopIfNum =
        pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopIfNum;

    MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppHopTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppHopReturnCode
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
                FsLsppHopIndex

                The Object 
                UINT4 *pu4RetValFsLsppHopReturnCode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppHopReturnCode (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                           UINT4 u4FsLsppHopIndex,
                           UINT4 *pu4RetValFsLsppHopReturnCode)
{
    tLsppFsLsppHopTableEntry *pLsppFsLsppHopTableEntry = NULL;

    pLsppFsLsppHopTableEntry =
        (tLsppFsLsppHopTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPHOPTABLE_POOLID);

    if (pLsppFsLsppHopTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppHopTableEntry, 0, sizeof (tLsppFsLsppHopTableEntry));

    /* Assign the index */
    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppContextId = u4FsLsppContextId;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopIndex = u4FsLsppHopIndex;

    if (LsppGetAllFsLsppHopTable (pLsppFsLsppHopTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppHopTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppHopReturnCode =
        pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopReturnCode;

    MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppHopTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppHopReturnSubCode
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
                FsLsppHopIndex

                The Object 
                UINT4 *pu4RetValFsLsppHopReturnSubCode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppHopReturnSubCode (UINT4 u4FsLsppContextId,
                              UINT4 u4FsLsppSenderHandle,
                              UINT4 u4FsLsppHopIndex,
                              UINT4 *pu4RetValFsLsppHopReturnSubCode)
{
    tLsppFsLsppHopTableEntry *pLsppFsLsppHopTableEntry = NULL;

    pLsppFsLsppHopTableEntry =
        (tLsppFsLsppHopTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPHOPTABLE_POOLID);

    if (pLsppFsLsppHopTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppHopTableEntry, 0, sizeof (tLsppFsLsppHopTableEntry));

    /* Assign the index */
    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppContextId = u4FsLsppContextId;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopIndex = u4FsLsppHopIndex;

    if (LsppGetAllFsLsppHopTable (pLsppFsLsppHopTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppHopTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppHopReturnSubCode =
        pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopReturnSubCode;

    MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppHopTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppHopReturnCodeStr
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
                FsLsppHopIndex

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsLsppHopReturnCodeStr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppHopReturnCodeStr (UINT4 u4FsLsppContextId,
                              UINT4 u4FsLsppSenderHandle,
                              UINT4 u4FsLsppHopIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValFsLsppHopReturnCodeStr)
{
    tLsppFsLsppHopTableEntry *pLsppFsLsppHopTableEntry = NULL;

    pLsppFsLsppHopTableEntry =
        (tLsppFsLsppHopTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPHOPTABLE_POOLID);

    if (pLsppFsLsppHopTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppHopTableEntry, 0, sizeof (tLsppFsLsppHopTableEntry));

    /* Assign the index */
    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppContextId = u4FsLsppContextId;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopIndex = u4FsLsppHopIndex;

    if (LsppGetAllFsLsppHopTable (pLsppFsLsppHopTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppHopTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsLsppHopReturnCodeStr->pu1_OctetList,
            pLsppFsLsppHopTableEntry->MibObject.au1FsLsppHopReturnCodeStr,
            pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopReturnCodeStrLen);
    pRetValFsLsppHopReturnCodeStr->i4_Length =
        pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopReturnCodeStrLen;

    MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppHopTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppHopRxAddrType
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
                FsLsppHopIndex

                The Object 
                INT4 *pi4RetValFsLsppHopRxAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppHopRxAddrType (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                           UINT4 u4FsLsppHopIndex,
                           INT4 *pi4RetValFsLsppHopRxAddrType)
{
    tLsppFsLsppHopTableEntry *pLsppFsLsppHopTableEntry = NULL;

    pLsppFsLsppHopTableEntry =
        (tLsppFsLsppHopTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPHOPTABLE_POOLID);

    if (pLsppFsLsppHopTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppHopTableEntry, 0, sizeof (tLsppFsLsppHopTableEntry));

    /* Assign the index */
    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppContextId = u4FsLsppContextId;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopIndex = u4FsLsppHopIndex;

    if (LsppGetAllFsLsppHopTable (pLsppFsLsppHopTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppHopTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsLsppHopRxAddrType =
        pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopRxAddrType;

    MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppHopTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppHopRxIPAddr
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
                FsLsppHopIndex

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsLsppHopRxIPAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppHopRxIPAddr (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                         UINT4 u4FsLsppHopIndex,
                         tSNMP_OCTET_STRING_TYPE * pRetValFsLsppHopRxIPAddr)
{
    tLsppFsLsppHopTableEntry *pLsppFsLsppHopTableEntry = NULL;

    pLsppFsLsppHopTableEntry =
        (tLsppFsLsppHopTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPHOPTABLE_POOLID);

    if (pLsppFsLsppHopTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppHopTableEntry, 0, sizeof (tLsppFsLsppHopTableEntry));

    /* Assign the index */
    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppContextId = u4FsLsppContextId;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopIndex = u4FsLsppHopIndex;

    if (LsppGetAllFsLsppHopTable (pLsppFsLsppHopTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppHopTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsLsppHopRxIPAddr->pu1_OctetList,
            pLsppFsLsppHopTableEntry->MibObject.au1FsLsppHopRxIPAddr,
            pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopRxIPAddrLen);
    pRetValFsLsppHopRxIPAddr->i4_Length =
        pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopRxIPAddrLen;

    MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppHopTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppHopRxIfAddr
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
                FsLsppHopIndex

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsLsppHopRxIfAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppHopRxIfAddr (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                         UINT4 u4FsLsppHopIndex,
                         tSNMP_OCTET_STRING_TYPE * pRetValFsLsppHopRxIfAddr)
{
    tLsppFsLsppHopTableEntry *pLsppFsLsppHopTableEntry = NULL;

    pLsppFsLsppHopTableEntry =
        (tLsppFsLsppHopTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPHOPTABLE_POOLID);

    if (pLsppFsLsppHopTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppHopTableEntry, 0, sizeof (tLsppFsLsppHopTableEntry));

    /* Assign the index */
    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppContextId = u4FsLsppContextId;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopIndex = u4FsLsppHopIndex;

    if (LsppGetAllFsLsppHopTable (pLsppFsLsppHopTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppHopTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsLsppHopRxIfAddr->pu1_OctetList,
            pLsppFsLsppHopTableEntry->MibObject.au1FsLsppHopRxIfAddr,
            pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopRxIfAddrLen);
    pRetValFsLsppHopRxIfAddr->i4_Length =
        pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopRxIfAddrLen;

    MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppHopTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppHopRxIfNum
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
                FsLsppHopIndex

                The Object 
                UINT4 *pu4RetValFsLsppHopRxIfNum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppHopRxIfNum (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                        UINT4 u4FsLsppHopIndex,
                        UINT4 *pu4RetValFsLsppHopRxIfNum)
{
    tLsppFsLsppHopTableEntry *pLsppFsLsppHopTableEntry = NULL;

    pLsppFsLsppHopTableEntry =
        (tLsppFsLsppHopTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPHOPTABLE_POOLID);

    if (pLsppFsLsppHopTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppHopTableEntry, 0, sizeof (tLsppFsLsppHopTableEntry));

    /* Assign the index */
    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppContextId = u4FsLsppContextId;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopIndex = u4FsLsppHopIndex;

    if (LsppGetAllFsLsppHopTable (pLsppFsLsppHopTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppHopTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppHopRxIfNum =
        pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopRxIfNum;

    MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppHopTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppHopRxLabelStack
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
                FsLsppHopIndex

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsLsppHopRxLabelStack
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppHopRxLabelStack (UINT4 u4FsLsppContextId,
                             UINT4 u4FsLsppSenderHandle, UINT4 u4FsLsppHopIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsLsppHopRxLabelStack)
{
    tLsppFsLsppHopTableEntry *pLsppFsLsppHopTableEntry = NULL;

    pLsppFsLsppHopTableEntry =
        (tLsppFsLsppHopTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPHOPTABLE_POOLID);

    if (pLsppFsLsppHopTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppHopTableEntry, 0, sizeof (tLsppFsLsppHopTableEntry));

    /* Assign the index */
    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppContextId = u4FsLsppContextId;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopIndex = u4FsLsppHopIndex;

    if (LsppGetAllFsLsppHopTable (pLsppFsLsppHopTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppHopTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsLsppHopRxLabelStack->pu1_OctetList,
            pLsppFsLsppHopTableEntry->MibObject.au1FsLsppHopRxLabelStack,
            pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopRxLabelStackLen);
    pRetValFsLsppHopRxLabelStack->i4_Length =
        pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopRxLabelStackLen;

    MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppHopTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppHopRxLabelExp
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
                FsLsppHopIndex

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsLsppHopRxLabelExp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppHopRxLabelExp (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                           UINT4 u4FsLsppHopIndex,
                           tSNMP_OCTET_STRING_TYPE * pRetValFsLsppHopRxLabelExp)
{
    tLsppFsLsppHopTableEntry *pLsppFsLsppHopTableEntry = NULL;

    pLsppFsLsppHopTableEntry =
        (tLsppFsLsppHopTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPHOPTABLE_POOLID);

    if (pLsppFsLsppHopTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppHopTableEntry, 0, sizeof (tLsppFsLsppHopTableEntry));

    /* Assign the index */
    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppContextId = u4FsLsppContextId;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopIndex = u4FsLsppHopIndex;

    if (LsppGetAllFsLsppHopTable (pLsppFsLsppHopTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppHopTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsLsppHopRxLabelExp->pu1_OctetList,
            pLsppFsLsppHopTableEntry->MibObject.au1FsLsppHopRxLabelExp,
            pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopRxLabelExpLen);
    pRetValFsLsppHopRxLabelExp->i4_Length =
        pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopRxLabelExpLen;

    MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppHopTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppHopRtt
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
                FsLsppHopIndex

                The Object 
                UINT4 *pu4RetValFsLsppHopRtt
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppHopRtt (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                    UINT4 u4FsLsppHopIndex, UINT4 *pu4RetValFsLsppHopRtt)
{
    tLsppFsLsppHopTableEntry *pLsppFsLsppHopTableEntry = NULL;

    pLsppFsLsppHopTableEntry =
        (tLsppFsLsppHopTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPHOPTABLE_POOLID);

    if (pLsppFsLsppHopTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppHopTableEntry, 0, sizeof (tLsppFsLsppHopTableEntry));

    /* Assign the index */
    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppContextId = u4FsLsppContextId;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopIndex = u4FsLsppHopIndex;

    if (LsppGetAllFsLsppHopTable (pLsppFsLsppHopTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppHopTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppHopRtt = pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopRtt;

    MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppHopTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppHopDsMtu
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
                FsLsppHopIndex

                The Object 
                UINT4 *pu4RetValFsLsppHopDsMtu
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppHopDsMtu (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                      UINT4 u4FsLsppHopIndex, UINT4 *pu4RetValFsLsppHopDsMtu)
{
    tLsppFsLsppHopTableEntry *pLsppFsLsppHopTableEntry = NULL;

    pLsppFsLsppHopTableEntry =
        (tLsppFsLsppHopTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPHOPTABLE_POOLID);

    if (pLsppFsLsppHopTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppHopTableEntry, 0, sizeof (tLsppFsLsppHopTableEntry));

    /* Assign the index */
    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppContextId = u4FsLsppContextId;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopIndex = u4FsLsppHopIndex;

    if (LsppGetAllFsLsppHopTable (pLsppFsLsppHopTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppHopTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppHopDsMtu =
        pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopDsMtu;

    MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppHopTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppHopDsAddrType
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
                FsLsppHopIndex

                The Object 
                INT4 *pi4RetValFsLsppHopDsAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppHopDsAddrType (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                           UINT4 u4FsLsppHopIndex,
                           INT4 *pi4RetValFsLsppHopDsAddrType)
{
    tLsppFsLsppHopTableEntry *pLsppFsLsppHopTableEntry = NULL;

    pLsppFsLsppHopTableEntry =
        (tLsppFsLsppHopTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPHOPTABLE_POOLID);

    if (pLsppFsLsppHopTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppHopTableEntry, 0, sizeof (tLsppFsLsppHopTableEntry));

    /* Assign the index */
    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppContextId = u4FsLsppContextId;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopIndex = u4FsLsppHopIndex;

    if (LsppGetAllFsLsppHopTable (pLsppFsLsppHopTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppHopTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsLsppHopDsAddrType =
        pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopDsAddrType;

    MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppHopTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppHopDsIPAddr
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
                FsLsppHopIndex

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsLsppHopDsIPAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppHopDsIPAddr (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                         UINT4 u4FsLsppHopIndex,
                         tSNMP_OCTET_STRING_TYPE * pRetValFsLsppHopDsIPAddr)
{
    tLsppFsLsppHopTableEntry *pLsppFsLsppHopTableEntry = NULL;

    pLsppFsLsppHopTableEntry =
        (tLsppFsLsppHopTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPHOPTABLE_POOLID);

    if (pLsppFsLsppHopTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppHopTableEntry, 0, sizeof (tLsppFsLsppHopTableEntry));

    /* Assign the index */
    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppContextId = u4FsLsppContextId;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopIndex = u4FsLsppHopIndex;

    if (LsppGetAllFsLsppHopTable (pLsppFsLsppHopTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppHopTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsLsppHopDsIPAddr->pu1_OctetList,
            pLsppFsLsppHopTableEntry->MibObject.au1FsLsppHopDsIPAddr,
            pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopDsIPAddrLen);
    pRetValFsLsppHopDsIPAddr->i4_Length =
        pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopDsIPAddrLen;

    MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppHopTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppHopDsIfAddr
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
                FsLsppHopIndex

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsLsppHopDsIfAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppHopDsIfAddr (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                         UINT4 u4FsLsppHopIndex,
                         tSNMP_OCTET_STRING_TYPE * pRetValFsLsppHopDsIfAddr)
{
    tLsppFsLsppHopTableEntry *pLsppFsLsppHopTableEntry = NULL;

    pLsppFsLsppHopTableEntry =
        (tLsppFsLsppHopTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPHOPTABLE_POOLID);

    if (pLsppFsLsppHopTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppHopTableEntry, 0, sizeof (tLsppFsLsppHopTableEntry));

    /* Assign the index */
    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppContextId = u4FsLsppContextId;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopIndex = u4FsLsppHopIndex;

    if (LsppGetAllFsLsppHopTable (pLsppFsLsppHopTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppHopTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsLsppHopDsIfAddr->pu1_OctetList,
            pLsppFsLsppHopTableEntry->MibObject.au1FsLsppHopDsIfAddr,
            pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopDsIfAddrLen);
    pRetValFsLsppHopDsIfAddr->i4_Length =
        pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopDsIfAddrLen;

    MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppHopTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppHopDsIfNum
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
                FsLsppHopIndex

                The Object 
                UINT4 *pu4RetValFsLsppHopDsIfNum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppHopDsIfNum (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                        UINT4 u4FsLsppHopIndex,
                        UINT4 *pu4RetValFsLsppHopDsIfNum)
{
    tLsppFsLsppHopTableEntry *pLsppFsLsppHopTableEntry = NULL;

    pLsppFsLsppHopTableEntry =
        (tLsppFsLsppHopTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPHOPTABLE_POOLID);

    if (pLsppFsLsppHopTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppHopTableEntry, 0, sizeof (tLsppFsLsppHopTableEntry));

    /* Assign the index */
    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppContextId = u4FsLsppContextId;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopIndex = u4FsLsppHopIndex;

    if (LsppGetAllFsLsppHopTable (pLsppFsLsppHopTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppHopTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppHopDsIfNum =
        pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopDsIfNum;

    MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppHopTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppHopDsLabelStack
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
                FsLsppHopIndex

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsLsppHopDsLabelStack
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppHopDsLabelStack (UINT4 u4FsLsppContextId,
                             UINT4 u4FsLsppSenderHandle, UINT4 u4FsLsppHopIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsLsppHopDsLabelStack)
{
    tLsppFsLsppHopTableEntry *pLsppFsLsppHopTableEntry = NULL;

    pLsppFsLsppHopTableEntry =
        (tLsppFsLsppHopTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPHOPTABLE_POOLID);

    if (pLsppFsLsppHopTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppHopTableEntry, 0, sizeof (tLsppFsLsppHopTableEntry));

    /* Assign the index */
    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppContextId = u4FsLsppContextId;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopIndex = u4FsLsppHopIndex;

    if (LsppGetAllFsLsppHopTable (pLsppFsLsppHopTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppHopTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsLsppHopDsLabelStack->pu1_OctetList,
            pLsppFsLsppHopTableEntry->MibObject.au1FsLsppHopDsLabelStack,
            pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopDsLabelStackLen);
    pRetValFsLsppHopDsLabelStack->i4_Length =
        pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopDsLabelStackLen;

    MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppHopTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppHopDsLabelExp
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
                FsLsppHopIndex

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsLsppHopDsLabelExp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppHopDsLabelExp (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                           UINT4 u4FsLsppHopIndex,
                           tSNMP_OCTET_STRING_TYPE * pRetValFsLsppHopDsLabelExp)
{
    tLsppFsLsppHopTableEntry *pLsppFsLsppHopTableEntry = NULL;

    pLsppFsLsppHopTableEntry =
        (tLsppFsLsppHopTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPHOPTABLE_POOLID);

    if (pLsppFsLsppHopTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppHopTableEntry, 0, sizeof (tLsppFsLsppHopTableEntry));

    /* Assign the index */
    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppContextId = u4FsLsppContextId;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopIndex = u4FsLsppHopIndex;

    if (LsppGetAllFsLsppHopTable (pLsppFsLsppHopTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppHopTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsLsppHopDsLabelExp->pu1_OctetList,
            pLsppFsLsppHopTableEntry->MibObject.au1FsLsppHopDsLabelExp,
            pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopDsLabelExpLen);
    pRetValFsLsppHopDsLabelExp->i4_Length =
        pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopDsLabelExpLen;

    MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppHopTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppHopIcc
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
                FsLsppHopIndex

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsLsppHopIcc
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppHopIcc (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                    UINT4 u4FsLsppHopIndex,
                    tSNMP_OCTET_STRING_TYPE * pRetValFsLsppHopIcc)
{
    tLsppFsLsppHopTableEntry *pLsppFsLsppHopTableEntry = NULL;

    pLsppFsLsppHopTableEntry =
        (tLsppFsLsppHopTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPHOPTABLE_POOLID);

    if (pLsppFsLsppHopTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppHopTableEntry, 0, sizeof (tLsppFsLsppHopTableEntry));

    /* Assign the index */
    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppContextId = u4FsLsppContextId;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopIndex = u4FsLsppHopIndex;

    if (LsppGetAllFsLsppHopTable (pLsppFsLsppHopTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppHopTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsLsppHopIcc->pu1_OctetList,
            pLsppFsLsppHopTableEntry->MibObject.au1FsLsppHopIcc,
            pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopIccLen);
    pRetValFsLsppHopIcc->i4_Length =
        pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopIccLen;

    MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppHopTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppHopUMC
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
                FsLsppHopIndex

                The Object 
                tSNMP_OCTET_STRING_TYPE * pRetValFsLsppHopUMC
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppHopUMC (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                    UINT4 u4FsLsppHopIndex,
                    tSNMP_OCTET_STRING_TYPE * pRetValFsLsppHopUMC)
{
    tLsppFsLsppHopTableEntry *pLsppFsLsppHopTableEntry = NULL;

    pLsppFsLsppHopTableEntry =
        (tLsppFsLsppHopTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPHOPTABLE_POOLID);

    if (pLsppFsLsppHopTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppHopTableEntry, 0, sizeof (tLsppFsLsppHopTableEntry));

    /* Assign the index */
    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppContextId = u4FsLsppContextId;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopIndex = u4FsLsppHopIndex;

    if (LsppGetAllFsLsppHopTable (pLsppFsLsppHopTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppHopTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValFsLsppHopUMC->pu1_OctetList,
            pLsppFsLsppHopTableEntry->MibObject.au1FsLsppHopUMC,
            pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopUMCLen);
    pRetValFsLsppHopUMC->i4_Length =
        pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopUMCLen;

    MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppHopTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsLsppHopMepIndex
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
                FsLsppHopIndex

                The Object 
                UINT4 *pu4RetValFsLsppHopMepIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsLsppHopMepIndex (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                         UINT4 u4FsLsppHopIndex,
                         UINT4 *pu4RetValFsLsppHopMepIndex)
{
    tLsppFsLsppHopTableEntry *pLsppFsLsppHopTableEntry = NULL;

    pLsppFsLsppHopTableEntry =
        (tLsppFsLsppHopTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPHOPTABLE_POOLID);

    if (pLsppFsLsppHopTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppHopTableEntry, 0, sizeof (tLsppFsLsppHopTableEntry));

    /* Assign the index */
    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppContextId = u4FsLsppContextId;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;

    pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopIndex = u4FsLsppHopIndex;

    if (LsppGetAllFsLsppHopTable (pLsppFsLsppHopTableEntry) != OSIX_SUCCESS)
    {
        MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppHopTableEntry);
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsLsppHopMepIndex =
        pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopMepIndex;

    MemReleaseMemBlock (LSPP_FSLSPPHOPTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppHopTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppSystemControl
 Input       :  The Indices
                FsLsppContextId

                The Object 
             :  INT4 i4SetValFsLsppSystemControl
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppSystemControl (UINT4 u4FsLsppContextId,
                           INT4 i4SetValFsLsppSystemControl)
{
    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigTableEntry = NULL;
    tLsppIsSetFsLsppGlobalConfigTableEntry
        * pLsppIsSetFsLsppGlobalConfigTableEntry = NULL;

    pLsppFsLsppGlobalConfigTableEntry =
        (tLsppFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);
    if (pLsppFsLsppGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppGlobalConfigTableEntry =
        (tLsppIsSetFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);
    if (pLsppIsSetFsLsppGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (pLsppIsSetFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppIsSetFsLsppGlobalConfigTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppContextId = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppSystemControl =
        i4SetValFsLsppSystemControl;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppSystemControl = OSIX_TRUE;

    if (LsppSetAllFsLsppGlobalConfigTable
        (pLsppFsLsppGlobalConfigTableEntry,
         pLsppIsSetFsLsppGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppTrapStatus
 Input       :  The Indices
                FsLsppContextId

                The Object 
             :  INT4 i4SetValFsLsppTrapStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppTrapStatus (UINT4 u4FsLsppContextId, INT4 i4SetValFsLsppTrapStatus)
{
    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigTableEntry = NULL;
    tLsppIsSetFsLsppGlobalConfigTableEntry
        * pLsppIsSetFsLsppGlobalConfigTableEntry = NULL;

    pLsppFsLsppGlobalConfigTableEntry =
        (tLsppFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);
    if (pLsppFsLsppGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppGlobalConfigTableEntry =
        (tLsppIsSetFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);
    if (pLsppIsSetFsLsppGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (pLsppIsSetFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppIsSetFsLsppGlobalConfigTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppContextId = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppTrapStatus =
        i4SetValFsLsppTrapStatus;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppTrapStatus = OSIX_TRUE;

    if (LsppSetAllFsLsppGlobalConfigTable
        (pLsppFsLsppGlobalConfigTableEntry,
         pLsppIsSetFsLsppGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppTraceLevel
 Input       :  The Indices
                FsLsppContextId

                The Object 
             :  INT4 i4SetValFsLsppTraceLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppTraceLevel (UINT4 u4FsLsppContextId, INT4 i4SetValFsLsppTraceLevel)
{
    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigTableEntry = NULL;
    tLsppIsSetFsLsppGlobalConfigTableEntry
        * pLsppIsSetFsLsppGlobalConfigTableEntry = NULL;

    pLsppFsLsppGlobalConfigTableEntry =
        (tLsppFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);
    if (pLsppFsLsppGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppGlobalConfigTableEntry =
        (tLsppIsSetFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);
    if (pLsppIsSetFsLsppGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (pLsppIsSetFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppIsSetFsLsppGlobalConfigTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppContextId = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppTraceLevel =
        i4SetValFsLsppTraceLevel;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppTraceLevel = OSIX_TRUE;

    if (LsppSetAllFsLsppGlobalConfigTable
        (pLsppFsLsppGlobalConfigTableEntry,
         pLsppIsSetFsLsppGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppAgeOutTime
 Input       :  The Indices
                FsLsppContextId

                The Object 
             :  UINT4 u4SetValFsLsppAgeOutTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppAgeOutTime (UINT4 u4FsLsppContextId, UINT4 u4SetValFsLsppAgeOutTime)
{
    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigTableEntry = NULL;
    tLsppIsSetFsLsppGlobalConfigTableEntry
        * pLsppIsSetFsLsppGlobalConfigTableEntry = NULL;

    pLsppFsLsppGlobalConfigTableEntry =
        (tLsppFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);
    if (pLsppFsLsppGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppGlobalConfigTableEntry =
        (tLsppIsSetFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);
    if (pLsppIsSetFsLsppGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (pLsppIsSetFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppIsSetFsLsppGlobalConfigTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppContextId = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppAgeOutTime =
        u4SetValFsLsppAgeOutTime;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppAgeOutTime = OSIX_TRUE;

    if (LsppSetAllFsLsppGlobalConfigTable
        (pLsppFsLsppGlobalConfigTableEntry,
         pLsppIsSetFsLsppGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppAgeOutTmrUnit
 Input       :  The Indices
                FsLsppContextId

                The Object 
             :  INT4 i4SetValFsLsppAgeOutTmrUnit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppAgeOutTmrUnit (UINT4 u4FsLsppContextId,
                           INT4 i4SetValFsLsppAgeOutTmrUnit)
{
    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigTableEntry = NULL;
    tLsppIsSetFsLsppGlobalConfigTableEntry
        * pLsppIsSetFsLsppGlobalConfigTableEntry = NULL;

    pLsppFsLsppGlobalConfigTableEntry =
        (tLsppFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);
    if (pLsppFsLsppGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppGlobalConfigTableEntry =
        (tLsppIsSetFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);
    if (pLsppIsSetFsLsppGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (pLsppIsSetFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppIsSetFsLsppGlobalConfigTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppContextId = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppAgeOutTmrUnit =
        i4SetValFsLsppAgeOutTmrUnit;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppAgeOutTmrUnit = OSIX_TRUE;

    if (LsppSetAllFsLsppGlobalConfigTable
        (pLsppFsLsppGlobalConfigTableEntry,
         pLsppIsSetFsLsppGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppClearEchoStats
 Input       :  The Indices
                FsLsppContextId

                The Object 
             :  INT4 i4SetValFsLsppClearEchoStats
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppClearEchoStats (UINT4 u4FsLsppContextId,
                            INT4 i4SetValFsLsppClearEchoStats)
{
    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigTableEntry = NULL;
    tLsppIsSetFsLsppGlobalConfigTableEntry
        * pLsppIsSetFsLsppGlobalConfigTableEntry = NULL;

    pLsppFsLsppGlobalConfigTableEntry =
        (tLsppFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);
    if (pLsppFsLsppGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppGlobalConfigTableEntry =
        (tLsppIsSetFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);
    if (pLsppIsSetFsLsppGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (pLsppIsSetFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppIsSetFsLsppGlobalConfigTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppContextId = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppClearEchoStats =
        i4SetValFsLsppClearEchoStats;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppClearEchoStats = OSIX_TRUE;

    if (LsppSetAllFsLsppGlobalConfigTable
        (pLsppFsLsppGlobalConfigTableEntry,
         pLsppIsSetFsLsppGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppBfdBtStrapRespReq
 Input       :  The Indices
                FsLsppContextId

                The Object 
             :  INT4 i4SetValFsLsppBfdBtStrapRespReq
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppBfdBtStrapRespReq (UINT4 u4FsLsppContextId,
                               INT4 i4SetValFsLsppBfdBtStrapRespReq)
{
    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigTableEntry = NULL;
    tLsppIsSetFsLsppGlobalConfigTableEntry
        * pLsppIsSetFsLsppGlobalConfigTableEntry = NULL;

    pLsppFsLsppGlobalConfigTableEntry =
        (tLsppFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);
    if (pLsppFsLsppGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppGlobalConfigTableEntry =
        (tLsppIsSetFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);
    if (pLsppIsSetFsLsppGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (pLsppIsSetFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppIsSetFsLsppGlobalConfigTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppContextId = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppBfdBtStrapRespReq =
        i4SetValFsLsppBfdBtStrapRespReq;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppBfdBtStrapRespReq =
        OSIX_TRUE;

    if (LsppSetAllFsLsppGlobalConfigTable
        (pLsppFsLsppGlobalConfigTableEntry,
         pLsppIsSetFsLsppGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppBfdBtStrapAgeOutTime
 Input       :  The Indices
                FsLsppContextId

                The Object 
             :  UINT4 u4SetValFsLsppBfdBtStrapAgeOutTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppBfdBtStrapAgeOutTime (UINT4 u4FsLsppContextId,
                                  UINT4 u4SetValFsLsppBfdBtStrapAgeOutTime)
{
    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigTableEntry = NULL;
    tLsppIsSetFsLsppGlobalConfigTableEntry
        * pLsppIsSetFsLsppGlobalConfigTableEntry = NULL;

    pLsppFsLsppGlobalConfigTableEntry =
        (tLsppFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);
    if (pLsppFsLsppGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppGlobalConfigTableEntry =
        (tLsppIsSetFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);
    if (pLsppIsSetFsLsppGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (pLsppIsSetFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppIsSetFsLsppGlobalConfigTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppContextId = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppBfdBtStrapAgeOutTime =
        u4SetValFsLsppBfdBtStrapAgeOutTime;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppBfdBtStrapAgeOutTime =
        OSIX_TRUE;

    if (LsppSetAllFsLsppGlobalConfigTable
        (pLsppFsLsppGlobalConfigTableEntry,
         pLsppIsSetFsLsppGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppBfdBtStrapAgeOutTmrUnit
 Input       :  The Indices
                FsLsppContextId

                The Object 
             :  INT4 i4SetValFsLsppBfdBtStrapAgeOutTmrUnit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppBfdBtStrapAgeOutTmrUnit (UINT4 u4FsLsppContextId,
                                     INT4 i4SetValFsLsppBfdBtStrapAgeOutTmrUnit)
{
    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigTableEntry = NULL;
    tLsppIsSetFsLsppGlobalConfigTableEntry
        * pLsppIsSetFsLsppGlobalConfigTableEntry = NULL;

    pLsppFsLsppGlobalConfigTableEntry =
        (tLsppFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);
    if (pLsppFsLsppGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppGlobalConfigTableEntry =
        (tLsppIsSetFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);
    if (pLsppIsSetFsLsppGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (pLsppIsSetFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppIsSetFsLsppGlobalConfigTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppContextId = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.
        i4FsLsppBfdBtStrapAgeOutTmrUnit = i4SetValFsLsppBfdBtStrapAgeOutTmrUnit;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppBfdBtStrapAgeOutTmrUnit =
        OSIX_TRUE;

    if (LsppSetAllFsLsppGlobalConfigTable
        (pLsppFsLsppGlobalConfigTableEntry,
         pLsppIsSetFsLsppGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppRequestType
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
             :  INT4 i4SetValFsLsppRequestType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppRequestType (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                         INT4 i4SetValFsLsppRequestType)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRequestType =
        i4SetValFsLsppRequestType;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRequestType = OSIX_TRUE;

    if (LsppSetAllFsLsppPingTraceTable
        (pLsppFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppPathType
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
             :  INT4 i4SetValFsLsppPathType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppPathType (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                      INT4 i4SetValFsLsppPathType)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppPathType =
        i4SetValFsLsppPathType;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPathType = OSIX_TRUE;

    if (LsppSetAllFsLsppPingTraceTable
        (pLsppFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppPathPointer
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
             :  tSNMP_OID_TYPE *pSetValFsLsppPathPointer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppPathPointer (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                         tSNMP_OID_TYPE * pSetValFsLsppPathPointer)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pLsppFsLsppPingTraceTableEntry->MibObject.au4FsLsppPathPointer,
            pSetValFsLsppPathPointer->pu4_OidList,
            pSetValFsLsppPathPointer->u4_Length * sizeof (UINT4));
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppPathPointerLen =
        (INT4)(pSetValFsLsppPathPointer->u4_Length);

    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPathPointer = OSIX_TRUE;

    if (LsppSetAllFsLsppPingTraceTable
        (pLsppFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppTgtMipGlobalId
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
             :  UINT4 u4SetValFsLsppTgtMipGlobalId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppTgtMipGlobalId (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                            UINT4 u4SetValFsLsppTgtMipGlobalId)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipGlobalId =
        u4SetValFsLsppTgtMipGlobalId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTgtMipGlobalId = OSIX_TRUE;

    if (LsppSetAllFsLsppPingTraceTable
        (pLsppFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppTgtMipNodeId
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
             :  UINT4 u4SetValFsLsppTgtMipNodeId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppTgtMipNodeId (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                          UINT4 u4SetValFsLsppTgtMipNodeId)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipNodeId =
        u4SetValFsLsppTgtMipNodeId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTgtMipNodeId = OSIX_TRUE;

    if (LsppSetAllFsLsppPingTraceTable
        (pLsppFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppTgtMipIfNum
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
             :  UINT4 u4SetValFsLsppTgtMipIfNum
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppTgtMipIfNum (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                         UINT4 u4SetValFsLsppTgtMipIfNum)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipIfNum =
        u4SetValFsLsppTgtMipIfNum;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTgtMipIfNum = OSIX_TRUE;

    if (LsppSetAllFsLsppPingTraceTable
        (pLsppFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppReplyMode
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
             :  INT4 i4SetValFsLsppReplyMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppReplyMode (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                       INT4 i4SetValFsLsppReplyMode)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppReplyMode =
        i4SetValFsLsppReplyMode;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReplyMode = OSIX_TRUE;

    if (LsppSetAllFsLsppPingTraceTable
        (pLsppFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppRepeatCount
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
             :  UINT4 u4SetValFsLsppRepeatCount
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppRepeatCount (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                         UINT4 u4SetValFsLsppRepeatCount)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppRepeatCount =
        u4SetValFsLsppRepeatCount;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRepeatCount = OSIX_TRUE;

    if (LsppSetAllFsLsppPingTraceTable
        (pLsppFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppPacketSize
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
             :  UINT4 u4SetValFsLsppPacketSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppPacketSize (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                        UINT4 u4SetValFsLsppPacketSize)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppPacketSize =
        u4SetValFsLsppPacketSize;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPacketSize = OSIX_TRUE;

    if (LsppSetAllFsLsppPingTraceTable
        (pLsppFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppPadPattern
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
             :  tSNMP_OCTET_STRING_TYPE *pSetValFsLsppPadPattern
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppPadPattern (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                        tSNMP_OCTET_STRING_TYPE * pSetValFsLsppPadPattern)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pLsppFsLsppPingTraceTableEntry->MibObject.au1FsLsppPadPattern,
            pSetValFsLsppPadPattern->pu1_OctetList,
            pSetValFsLsppPadPattern->i4_Length);
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppPadPatternLen =
        pSetValFsLsppPadPattern->i4_Length;

    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPadPattern = OSIX_TRUE;

    if (LsppSetAllFsLsppPingTraceTable
        (pLsppFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppTTLValue
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
             :  UINT4 u4SetValFsLsppTTLValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppTTLValue (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                      UINT4 u4SetValFsLsppTTLValue)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTTLValue =
        u4SetValFsLsppTTLValue;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTTLValue = OSIX_TRUE;

    if (LsppSetAllFsLsppPingTraceTable
        (pLsppFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppWFRInterval
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
             :  UINT4 u4SetValFsLsppWFRInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppWFRInterval (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                         UINT4 u4SetValFsLsppWFRInterval)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppWFRInterval =
        u4SetValFsLsppWFRInterval;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWFRInterval = OSIX_TRUE;

    if (LsppSetAllFsLsppPingTraceTable
        (pLsppFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppWFRTmrUnit
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
             :  INT4 i4SetValFsLsppWFRTmrUnit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppWFRTmrUnit (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                        INT4 i4SetValFsLsppWFRTmrUnit)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppWFRTmrUnit =
        i4SetValFsLsppWFRTmrUnit;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWFRTmrUnit = OSIX_TRUE;

    if (LsppSetAllFsLsppPingTraceTable
        (pLsppFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppWTSInterval
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
             :  UINT4 u4SetValFsLsppWTSInterval
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppWTSInterval (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                         UINT4 u4SetValFsLsppWTSInterval)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppWTSInterval =
        u4SetValFsLsppWTSInterval;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWTSInterval = OSIX_TRUE;

    if (LsppSetAllFsLsppPingTraceTable
        (pLsppFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppWTSTmrUnit
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
             :  INT4 i4SetValFsLsppWTSTmrUnit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppWTSTmrUnit (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                        INT4 i4SetValFsLsppWTSTmrUnit)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppWTSTmrUnit =
        i4SetValFsLsppWTSTmrUnit;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWTSTmrUnit = OSIX_TRUE;

    if (LsppSetAllFsLsppPingTraceTable
        (pLsppFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppReplyDscpValue
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
             :  UINT4 u4SetValFsLsppReplyDscpValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppReplyDscpValue (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                            UINT4 u4SetValFsLsppReplyDscpValue)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppReplyDscpValue =
        u4SetValFsLsppReplyDscpValue;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReplyDscpValue = OSIX_TRUE;

    if (LsppSetAllFsLsppPingTraceTable
        (pLsppFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppSweepOption
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
             :  INT4 i4SetValFsLsppSweepOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppSweepOption (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                         INT4 i4SetValFsLsppSweepOption)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppSweepOption =
        i4SetValFsLsppSweepOption;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepOption = OSIX_TRUE;

    if (LsppSetAllFsLsppPingTraceTable
        (pLsppFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppSweepMinimum
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
             :  UINT4 u4SetValFsLsppSweepMinimum
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppSweepMinimum (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                          UINT4 u4SetValFsLsppSweepMinimum)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepMinimum =
        u4SetValFsLsppSweepMinimum;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepMinimum = OSIX_TRUE;

    if (LsppSetAllFsLsppPingTraceTable
        (pLsppFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppSweepMaximum
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
             :  UINT4 u4SetValFsLsppSweepMaximum
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppSweepMaximum (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                          UINT4 u4SetValFsLsppSweepMaximum)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepMaximum =
        u4SetValFsLsppSweepMaximum;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepMaximum = OSIX_TRUE;

    if (LsppSetAllFsLsppPingTraceTable
        (pLsppFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppSweepIncrement
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
             :  UINT4 u4SetValFsLsppSweepIncrement
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppSweepIncrement (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                            UINT4 u4SetValFsLsppSweepIncrement)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepIncrement =
        u4SetValFsLsppSweepIncrement;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepIncrement = OSIX_TRUE;

    if (LsppSetAllFsLsppPingTraceTable
        (pLsppFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppBurstOption
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
             :  INT4 i4SetValFsLsppBurstOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppBurstOption (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                         INT4 i4SetValFsLsppBurstOption)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppBurstOption =
        i4SetValFsLsppBurstOption;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppBurstOption = OSIX_TRUE;

    if (LsppSetAllFsLsppPingTraceTable
        (pLsppFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppBurstSize
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
             :  UINT4 u4SetValFsLsppBurstSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppBurstSize (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                       UINT4 u4SetValFsLsppBurstSize)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppBurstSize =
        u4SetValFsLsppBurstSize;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppBurstSize = OSIX_TRUE;

    if (LsppSetAllFsLsppPingTraceTable
        (pLsppFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppEXPValue
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
             :  UINT4 u4SetValFsLsppEXPValue
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppEXPValue (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                      UINT4 u4SetValFsLsppEXPValue)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppEXPValue =
        u4SetValFsLsppEXPValue;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppEXPValue = OSIX_TRUE;

    if (LsppSetAllFsLsppPingTraceTable
        (pLsppFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppDsMap
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
             :  INT4 i4SetValFsLsppDsMap
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppDsMap (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                   INT4 i4SetValFsLsppDsMap)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppDsMap =
        i4SetValFsLsppDsMap;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppDsMap = OSIX_TRUE;

    if (LsppSetAllFsLsppPingTraceTable
        (pLsppFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppFecValidate
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
             :  INT4 i4SetValFsLsppFecValidate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppFecValidate (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                         INT4 i4SetValFsLsppFecValidate)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppFecValidate =
        i4SetValFsLsppFecValidate;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppFecValidate = OSIX_TRUE;

    if (LsppSetAllFsLsppPingTraceTable
        (pLsppFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppReplyPadTlv
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
             :  INT4 i4SetValFsLsppReplyPadTlv
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppReplyPadTlv (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                         INT4 i4SetValFsLsppReplyPadTlv)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppReplyPadTlv =
        i4SetValFsLsppReplyPadTlv;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReplyPadTlv = OSIX_TRUE;

    if (LsppSetAllFsLsppPingTraceTable
        (pLsppFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppForceExplicitNull
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
             :  INT4 i4SetValFsLsppForceExplicitNull
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppForceExplicitNull (UINT4 u4FsLsppContextId,
                               UINT4 u4FsLsppSenderHandle,
                               INT4 i4SetValFsLsppForceExplicitNull)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppForceExplicitNull =
        i4SetValFsLsppForceExplicitNull;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppForceExplicitNull = OSIX_TRUE;

    if (LsppSetAllFsLsppPingTraceTable
        (pLsppFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppInterfaceLabelTlv
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
             :  INT4 i4SetValFsLsppInterfaceLabelTlv
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppInterfaceLabelTlv (UINT4 u4FsLsppContextId,
                               UINT4 u4FsLsppSenderHandle,
                               INT4 i4SetValFsLsppInterfaceLabelTlv)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppInterfaceLabelTlv =
        i4SetValFsLsppInterfaceLabelTlv;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppInterfaceLabelTlv = OSIX_TRUE;

    if (LsppSetAllFsLsppPingTraceTable
        (pLsppFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppSameSeqNumOption
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
             :  INT4 i4SetValFsLsppSameSeqNumOption
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppSameSeqNumOption (UINT4 u4FsLsppContextId,
                              UINT4 u4FsLsppSenderHandle,
                              INT4 i4SetValFsLsppSameSeqNumOption)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppSameSeqNumOption =
        i4SetValFsLsppSameSeqNumOption;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSameSeqNumOption = OSIX_TRUE;

    if (LsppSetAllFsLsppPingTraceTable
        (pLsppFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppVerbose
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
             :  INT4 i4SetValFsLsppVerbose
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppVerbose (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                     INT4 i4SetValFsLsppVerbose)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppVerbose =
        i4SetValFsLsppVerbose;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppVerbose = OSIX_TRUE;

    if (LsppSetAllFsLsppPingTraceTable
        (pLsppFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppReversePathVerify
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
             :  INT4 i4SetValFsLsppReversePathVerify
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppReversePathVerify (UINT4 u4FsLsppContextId,
                               UINT4 u4FsLsppSenderHandle,
                               INT4 i4SetValFsLsppReversePathVerify)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppReversePathVerify =
        i4SetValFsLsppReversePathVerify;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReversePathVerify = OSIX_TRUE;

    if (LsppSetAllFsLsppPingTraceTable
        (pLsppFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppEncapType
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
             :  INT4 i4SetValFsLsppEncapType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppEncapType (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                       INT4 i4SetValFsLsppEncapType)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppEncapType =
        i4SetValFsLsppEncapType;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppEncapType = OSIX_TRUE;

    if (LsppSetAllFsLsppPingTraceTable
        (pLsppFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsLsppRowStatus
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
             :  INT4 i4SetValFsLsppRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsLsppRowStatus (UINT4 u4FsLsppContextId, UINT4 u4FsLsppSenderHandle,
                       INT4 i4SetValFsLsppRowStatus)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }
    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRowStatus =
        i4SetValFsLsppRowStatus;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRowStatus = OSIX_TRUE;

    if (LsppSetAllFsLsppPingTraceTable
        (pLsppFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         OSIX_FALSE, OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppSystemControl
 Input       :  The Indices
                FsLsppContextId

                The Object 
                testValFsLsppSystemControl
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppSystemControl (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                              INT4 i4TestValFsLsppSystemControl)
{
    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigTableEntry = NULL;
    tLsppIsSetFsLsppGlobalConfigTableEntry
        * pLsppIsSetFsLsppGlobalConfigTableEntry = NULL;

    pLsppFsLsppGlobalConfigTableEntry =
        (tLsppFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);

    if (pLsppFsLsppGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppGlobalConfigTableEntry =
        (tLsppIsSetFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);

    if (pLsppIsSetFsLsppGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (pLsppIsSetFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppIsSetFsLsppGlobalConfigTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppContextId = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppSystemControl =
        i4TestValFsLsppSystemControl;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppSystemControl = OSIX_TRUE;

    if (LsppTestAllFsLsppGlobalConfigTable
        (pu4ErrorCode, pLsppFsLsppGlobalConfigTableEntry,
         pLsppIsSetFsLsppGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppTrapStatus
 Input       :  The Indices
                FsLsppContextId

                The Object 
                testValFsLsppTrapStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppTrapStatus (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                           INT4 i4TestValFsLsppTrapStatus)
{
    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigTableEntry = NULL;
    tLsppIsSetFsLsppGlobalConfigTableEntry
        * pLsppIsSetFsLsppGlobalConfigTableEntry = NULL;

    pLsppFsLsppGlobalConfigTableEntry =
        (tLsppFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);

    if (pLsppFsLsppGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppGlobalConfigTableEntry =
        (tLsppIsSetFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);

    if (pLsppIsSetFsLsppGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (pLsppIsSetFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppIsSetFsLsppGlobalConfigTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppContextId = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppTrapStatus =
        i4TestValFsLsppTrapStatus;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppTrapStatus = OSIX_TRUE;

    if (LsppTestAllFsLsppGlobalConfigTable
        (pu4ErrorCode, pLsppFsLsppGlobalConfigTableEntry,
         pLsppIsSetFsLsppGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppTraceLevel
 Input       :  The Indices
                FsLsppContextId

                The Object 
                testValFsLsppTraceLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppTraceLevel (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                           INT4 i4TestValFsLsppTraceLevel)
{
    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigTableEntry = NULL;
    tLsppIsSetFsLsppGlobalConfigTableEntry
        * pLsppIsSetFsLsppGlobalConfigTableEntry = NULL;

    pLsppFsLsppGlobalConfigTableEntry =
        (tLsppFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);

    if (pLsppFsLsppGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppGlobalConfigTableEntry =
        (tLsppIsSetFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);

    if (pLsppIsSetFsLsppGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (pLsppIsSetFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppIsSetFsLsppGlobalConfigTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppContextId = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppTraceLevel =
        i4TestValFsLsppTraceLevel;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppTraceLevel = OSIX_TRUE;

    if (LsppTestAllFsLsppGlobalConfigTable
        (pu4ErrorCode, pLsppFsLsppGlobalConfigTableEntry,
         pLsppIsSetFsLsppGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppAgeOutTime
 Input       :  The Indices
                FsLsppContextId

                The Object 
                testValFsLsppAgeOutTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppAgeOutTime (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                           UINT4 u4TestValFsLsppAgeOutTime)
{
    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigTableEntry = NULL;
    tLsppIsSetFsLsppGlobalConfigTableEntry
        * pLsppIsSetFsLsppGlobalConfigTableEntry = NULL;

    pLsppFsLsppGlobalConfigTableEntry =
        (tLsppFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);

    if (pLsppFsLsppGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppGlobalConfigTableEntry =
        (tLsppIsSetFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);

    if (pLsppIsSetFsLsppGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (pLsppIsSetFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppIsSetFsLsppGlobalConfigTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppContextId = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppAgeOutTime =
        u4TestValFsLsppAgeOutTime;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppAgeOutTime = OSIX_TRUE;

    if (LsppTestAllFsLsppGlobalConfigTable
        (pu4ErrorCode, pLsppFsLsppGlobalConfigTableEntry,
         pLsppIsSetFsLsppGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppAgeOutTmrUnit
 Input       :  The Indices
                FsLsppContextId

                The Object 
                testValFsLsppAgeOutTmrUnit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppAgeOutTmrUnit (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                              INT4 i4TestValFsLsppAgeOutTmrUnit)
{
    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigTableEntry = NULL;
    tLsppIsSetFsLsppGlobalConfigTableEntry
        * pLsppIsSetFsLsppGlobalConfigTableEntry = NULL;

    pLsppFsLsppGlobalConfigTableEntry =
        (tLsppFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);

    if (pLsppFsLsppGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppGlobalConfigTableEntry =
        (tLsppIsSetFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);

    if (pLsppIsSetFsLsppGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (pLsppIsSetFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppIsSetFsLsppGlobalConfigTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppContextId = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppAgeOutTmrUnit =
        i4TestValFsLsppAgeOutTmrUnit;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppAgeOutTmrUnit = OSIX_TRUE;

    if (LsppTestAllFsLsppGlobalConfigTable
        (pu4ErrorCode, pLsppFsLsppGlobalConfigTableEntry,
         pLsppIsSetFsLsppGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppClearEchoStats
 Input       :  The Indices
                FsLsppContextId

                The Object 
                testValFsLsppClearEchoStats
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppClearEchoStats (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                               INT4 i4TestValFsLsppClearEchoStats)
{
    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigTableEntry = NULL;
    tLsppIsSetFsLsppGlobalConfigTableEntry
        * pLsppIsSetFsLsppGlobalConfigTableEntry = NULL;

    pLsppFsLsppGlobalConfigTableEntry =
        (tLsppFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);

    if (pLsppFsLsppGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppGlobalConfigTableEntry =
        (tLsppIsSetFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);

    if (pLsppIsSetFsLsppGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (pLsppIsSetFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppIsSetFsLsppGlobalConfigTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppContextId = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppClearEchoStats =
        i4TestValFsLsppClearEchoStats;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppClearEchoStats = OSIX_TRUE;

    if (LsppTestAllFsLsppGlobalConfigTable
        (pu4ErrorCode, pLsppFsLsppGlobalConfigTableEntry,
         pLsppIsSetFsLsppGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppBfdBtStrapRespReq
 Input       :  The Indices
                FsLsppContextId

                The Object 
                testValFsLsppBfdBtStrapRespReq
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppBfdBtStrapRespReq (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                                  INT4 i4TestValFsLsppBfdBtStrapRespReq)
{
    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigTableEntry = NULL;
    tLsppIsSetFsLsppGlobalConfigTableEntry
        * pLsppIsSetFsLsppGlobalConfigTableEntry = NULL;

    pLsppFsLsppGlobalConfigTableEntry =
        (tLsppFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);

    if (pLsppFsLsppGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppGlobalConfigTableEntry =
        (tLsppIsSetFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);

    if (pLsppIsSetFsLsppGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (pLsppIsSetFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppIsSetFsLsppGlobalConfigTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppContextId = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppBfdBtStrapRespReq =
        i4TestValFsLsppBfdBtStrapRespReq;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppBfdBtStrapRespReq =
        OSIX_TRUE;

    if (LsppTestAllFsLsppGlobalConfigTable
        (pu4ErrorCode, pLsppFsLsppGlobalConfigTableEntry,
         pLsppIsSetFsLsppGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppBfdBtStrapAgeOutTime
 Input       :  The Indices
                FsLsppContextId

                The Object 
                testValFsLsppBfdBtStrapAgeOutTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppBfdBtStrapAgeOutTime (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsLsppContextId,
                                     UINT4 u4TestValFsLsppBfdBtStrapAgeOutTime)
{
    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigTableEntry = NULL;
    tLsppIsSetFsLsppGlobalConfigTableEntry
        * pLsppIsSetFsLsppGlobalConfigTableEntry = NULL;

    pLsppFsLsppGlobalConfigTableEntry =
        (tLsppFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);

    if (pLsppFsLsppGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppGlobalConfigTableEntry =
        (tLsppIsSetFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);

    if (pLsppIsSetFsLsppGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (pLsppIsSetFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppIsSetFsLsppGlobalConfigTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppContextId = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppBfdBtStrapAgeOutTime =
        u4TestValFsLsppBfdBtStrapAgeOutTime;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppBfdBtStrapAgeOutTime =
        OSIX_TRUE;

    if (LsppTestAllFsLsppGlobalConfigTable
        (pu4ErrorCode, pLsppFsLsppGlobalConfigTableEntry,
         pLsppIsSetFsLsppGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppBfdBtStrapAgeOutTmrUnit
 Input       :  The Indices
                FsLsppContextId

                The Object 
                testValFsLsppBfdBtStrapAgeOutTmrUnit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppBfdBtStrapAgeOutTmrUnit (UINT4 *pu4ErrorCode,
                                        UINT4 u4FsLsppContextId,
                                        INT4
                                        i4TestValFsLsppBfdBtStrapAgeOutTmrUnit)
{
    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigTableEntry = NULL;
    tLsppIsSetFsLsppGlobalConfigTableEntry
        * pLsppIsSetFsLsppGlobalConfigTableEntry = NULL;

    pLsppFsLsppGlobalConfigTableEntry =
        (tLsppFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);

    if (pLsppFsLsppGlobalConfigTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppGlobalConfigTableEntry =
        (tLsppIsSetFsLsppGlobalConfigTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID);

    if (pLsppIsSetFsLsppGlobalConfigTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));
    MEMSET (pLsppIsSetFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppIsSetFsLsppGlobalConfigTableEntry));

    /* Assign the index */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppContextId = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppGlobalConfigTableEntry->MibObject.
        i4FsLsppBfdBtStrapAgeOutTmrUnit =
        i4TestValFsLsppBfdBtStrapAgeOutTmrUnit;
    pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppBfdBtStrapAgeOutTmrUnit =
        OSIX_TRUE;

    if (LsppTestAllFsLsppGlobalConfigTable
        (pu4ErrorCode, pLsppFsLsppGlobalConfigTableEntry,
         pLsppIsSetFsLsppGlobalConfigTableEntry) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppFsLsppGlobalConfigTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppGlobalConfigTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppRequestType
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                testValFsLsppRequestType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppRequestType (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                            UINT4 u4FsLsppSenderHandle,
                            INT4 i4TestValFsLsppRequestType)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRequestType =
        i4TestValFsLsppRequestType;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRequestType = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable
        (pu4ErrorCode, pLsppFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppPathType
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                testValFsLsppPathType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppPathType (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                         UINT4 u4FsLsppSenderHandle,
                         INT4 i4TestValFsLsppPathType)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppPathType =
        i4TestValFsLsppPathType;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPathType = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable
        (pu4ErrorCode, pLsppFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppPathPointer
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                testValFsLsppPathPointer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppPathPointer (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                            UINT4 u4FsLsppSenderHandle,
                            tSNMP_OID_TYPE * pTestValFsLsppPathPointer)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pLsppFsLsppPingTraceTableEntry->MibObject.au4FsLsppPathPointer,
            pTestValFsLsppPathPointer->pu4_OidList,
            pTestValFsLsppPathPointer->u4_Length * sizeof (UINT4));

    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPathPointer = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppPathPointerLen =
        (INT4)pTestValFsLsppPathPointer->u4_Length;

    if (LsppTestAllFsLsppPingTraceTable
        (pu4ErrorCode, pLsppFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppTgtMipGlobalId
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                testValFsLsppTgtMipGlobalId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppTgtMipGlobalId (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                               UINT4 u4FsLsppSenderHandle,
                               UINT4 u4TestValFsLsppTgtMipGlobalId)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipGlobalId =
        u4TestValFsLsppTgtMipGlobalId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTgtMipGlobalId = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable
        (pu4ErrorCode, pLsppFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppTgtMipNodeId
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                testValFsLsppTgtMipNodeId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppTgtMipNodeId (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                             UINT4 u4FsLsppSenderHandle,
                             UINT4 u4TestValFsLsppTgtMipNodeId)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipNodeId =
        u4TestValFsLsppTgtMipNodeId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTgtMipNodeId = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable
        (pu4ErrorCode, pLsppFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppTgtMipIfNum
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                testValFsLsppTgtMipIfNum
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppTgtMipIfNum (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                            UINT4 u4FsLsppSenderHandle,
                            UINT4 u4TestValFsLsppTgtMipIfNum)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipIfNum =
        u4TestValFsLsppTgtMipIfNum;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTgtMipIfNum = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable
        (pu4ErrorCode, pLsppFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppReplyMode
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                testValFsLsppReplyMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppReplyMode (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                          UINT4 u4FsLsppSenderHandle,
                          INT4 i4TestValFsLsppReplyMode)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppReplyMode =
        i4TestValFsLsppReplyMode;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReplyMode = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable
        (pu4ErrorCode, pLsppFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppRepeatCount
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                testValFsLsppRepeatCount
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppRepeatCount (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                            UINT4 u4FsLsppSenderHandle,
                            UINT4 u4TestValFsLsppRepeatCount)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppRepeatCount =
        u4TestValFsLsppRepeatCount;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRepeatCount = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable
        (pu4ErrorCode, pLsppFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppPacketSize
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                testValFsLsppPacketSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppPacketSize (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                           UINT4 u4FsLsppSenderHandle,
                           UINT4 u4TestValFsLsppPacketSize)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppPacketSize =
        u4TestValFsLsppPacketSize;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPacketSize = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable
        (pu4ErrorCode, pLsppFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppPadPattern
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                testValFsLsppPadPattern
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppPadPattern (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                           UINT4 u4FsLsppSenderHandle,
                           tSNMP_OCTET_STRING_TYPE * pTestValFsLsppPadPattern)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    MEMCPY (pLsppFsLsppPingTraceTableEntry->MibObject.au1FsLsppPadPattern,
            pTestValFsLsppPadPattern->pu1_OctetList,
            pTestValFsLsppPadPattern->i4_Length);
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppPadPatternLen =
        pTestValFsLsppPadPattern->i4_Length;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPadPattern = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable
        (pu4ErrorCode, pLsppFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppTTLValue
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                testValFsLsppTTLValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppTTLValue (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                         UINT4 u4FsLsppSenderHandle,
                         UINT4 u4TestValFsLsppTTLValue)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTTLValue =
        u4TestValFsLsppTTLValue;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTTLValue = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable
        (pu4ErrorCode, pLsppFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppWFRInterval
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                testValFsLsppWFRInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppWFRInterval (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                            UINT4 u4FsLsppSenderHandle,
                            UINT4 u4TestValFsLsppWFRInterval)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppWFRInterval =
        u4TestValFsLsppWFRInterval;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWFRInterval = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable
        (pu4ErrorCode, pLsppFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppWFRTmrUnit
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                testValFsLsppWFRTmrUnit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppWFRTmrUnit (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                           UINT4 u4FsLsppSenderHandle,
                           INT4 i4TestValFsLsppWFRTmrUnit)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppWFRTmrUnit =
        i4TestValFsLsppWFRTmrUnit;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWFRTmrUnit = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable
        (pu4ErrorCode, pLsppFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppWTSInterval
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                testValFsLsppWTSInterval
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppWTSInterval (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                            UINT4 u4FsLsppSenderHandle,
                            UINT4 u4TestValFsLsppWTSInterval)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppWTSInterval =
        u4TestValFsLsppWTSInterval;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWTSInterval = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable
        (pu4ErrorCode, pLsppFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppWTSTmrUnit
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                testValFsLsppWTSTmrUnit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppWTSTmrUnit (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                           UINT4 u4FsLsppSenderHandle,
                           INT4 i4TestValFsLsppWTSTmrUnit)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppWTSTmrUnit =
        i4TestValFsLsppWTSTmrUnit;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWTSTmrUnit = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable
        (pu4ErrorCode, pLsppFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppReplyDscpValue
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                testValFsLsppReplyDscpValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppReplyDscpValue (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                               UINT4 u4FsLsppSenderHandle,
                               UINT4 u4TestValFsLsppReplyDscpValue)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppReplyDscpValue =
        u4TestValFsLsppReplyDscpValue;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReplyDscpValue = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable
        (pu4ErrorCode, pLsppFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppSweepOption
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                testValFsLsppSweepOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppSweepOption (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                            UINT4 u4FsLsppSenderHandle,
                            INT4 i4TestValFsLsppSweepOption)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppSweepOption =
        i4TestValFsLsppSweepOption;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepOption = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable
        (pu4ErrorCode, pLsppFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppSweepMinimum
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                testValFsLsppSweepMinimum
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppSweepMinimum (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                             UINT4 u4FsLsppSenderHandle,
                             UINT4 u4TestValFsLsppSweepMinimum)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepMinimum =
        u4TestValFsLsppSweepMinimum;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepMinimum = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable
        (pu4ErrorCode, pLsppFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppSweepMaximum
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                testValFsLsppSweepMaximum
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppSweepMaximum (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                             UINT4 u4FsLsppSenderHandle,
                             UINT4 u4TestValFsLsppSweepMaximum)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepMaximum =
        u4TestValFsLsppSweepMaximum;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepMaximum = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable
        (pu4ErrorCode, pLsppFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppSweepIncrement
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                testValFsLsppSweepIncrement
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppSweepIncrement (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                               UINT4 u4FsLsppSenderHandle,
                               UINT4 u4TestValFsLsppSweepIncrement)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepIncrement =
        u4TestValFsLsppSweepIncrement;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepIncrement = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable
        (pu4ErrorCode, pLsppFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppBurstOption
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                testValFsLsppBurstOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppBurstOption (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                            UINT4 u4FsLsppSenderHandle,
                            INT4 i4TestValFsLsppBurstOption)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppBurstOption =
        i4TestValFsLsppBurstOption;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppBurstOption = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable
        (pu4ErrorCode, pLsppFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppBurstSize
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                testValFsLsppBurstSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppBurstSize (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                          UINT4 u4FsLsppSenderHandle,
                          UINT4 u4TestValFsLsppBurstSize)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppBurstSize =
        u4TestValFsLsppBurstSize;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppBurstSize = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable
        (pu4ErrorCode, pLsppFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppEXPValue
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                testValFsLsppEXPValue
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppEXPValue (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                         UINT4 u4FsLsppSenderHandle,
                         UINT4 u4TestValFsLsppEXPValue)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppEXPValue =
        u4TestValFsLsppEXPValue;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppEXPValue = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable
        (pu4ErrorCode, pLsppFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppDsMap
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                testValFsLsppDsMap
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppDsMap (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                      UINT4 u4FsLsppSenderHandle, INT4 i4TestValFsLsppDsMap)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppDsMap =
        i4TestValFsLsppDsMap;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppDsMap = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable
        (pu4ErrorCode, pLsppFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppFecValidate
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                testValFsLsppFecValidate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppFecValidate (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                            UINT4 u4FsLsppSenderHandle,
                            INT4 i4TestValFsLsppFecValidate)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppFecValidate =
        i4TestValFsLsppFecValidate;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppFecValidate = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable
        (pu4ErrorCode, pLsppFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppReplyPadTlv
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                testValFsLsppReplyPadTlv
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppReplyPadTlv (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                            UINT4 u4FsLsppSenderHandle,
                            INT4 i4TestValFsLsppReplyPadTlv)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppReplyPadTlv =
        i4TestValFsLsppReplyPadTlv;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReplyPadTlv = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable
        (pu4ErrorCode, pLsppFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppForceExplicitNull
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                testValFsLsppForceExplicitNull
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppForceExplicitNull (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                                  UINT4 u4FsLsppSenderHandle,
                                  INT4 i4TestValFsLsppForceExplicitNull)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppForceExplicitNull =
        i4TestValFsLsppForceExplicitNull;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppForceExplicitNull = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable
        (pu4ErrorCode, pLsppFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppInterfaceLabelTlv
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                testValFsLsppInterfaceLabelTlv
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppInterfaceLabelTlv (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                                  UINT4 u4FsLsppSenderHandle,
                                  INT4 i4TestValFsLsppInterfaceLabelTlv)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppInterfaceLabelTlv =
        i4TestValFsLsppInterfaceLabelTlv;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppInterfaceLabelTlv = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable
        (pu4ErrorCode, pLsppFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppSameSeqNumOption
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                testValFsLsppSameSeqNumOption
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppSameSeqNumOption (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                                 UINT4 u4FsLsppSenderHandle,
                                 INT4 i4TestValFsLsppSameSeqNumOption)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppSameSeqNumOption =
        i4TestValFsLsppSameSeqNumOption;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSameSeqNumOption = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable
        (pu4ErrorCode, pLsppFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppVerbose
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                testValFsLsppVerbose
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppVerbose (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                        UINT4 u4FsLsppSenderHandle, INT4 i4TestValFsLsppVerbose)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppVerbose =
        i4TestValFsLsppVerbose;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppVerbose = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable
        (pu4ErrorCode, pLsppFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppReversePathVerify
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                testValFsLsppReversePathVerify
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppReversePathVerify (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                                  UINT4 u4FsLsppSenderHandle,
                                  INT4 i4TestValFsLsppReversePathVerify)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppReversePathVerify =
        i4TestValFsLsppReversePathVerify;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReversePathVerify = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable
        (pu4ErrorCode, pLsppFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppEncapType
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                testValFsLsppEncapType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppEncapType (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                          UINT4 u4FsLsppSenderHandle,
                          INT4 i4TestValFsLsppEncapType)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppEncapType =
        i4TestValFsLsppEncapType;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppEncapType = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable
        (pu4ErrorCode, pLsppFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsLsppRowStatus
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle

                The Object 
                testValFsLsppRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsLsppRowStatus (UINT4 *pu4ErrorCode, UINT4 u4FsLsppContextId,
                          UINT4 u4FsLsppSenderHandle,
                          INT4 i4TestValFsLsppRowStatus)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppIsSetFsLsppPingTraceTableEntry =
        NULL;

    pLsppFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pLsppIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);

    if (pLsppIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }
    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Assign the index */
    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
        u4FsLsppContextId;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;

    pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
        u4FsLsppSenderHandle;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;

    /* Assign the value */
    pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRowStatus =
        i4TestValFsLsppRowStatus;
    pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRowStatus = OSIX_TRUE;

    if (LsppTestAllFsLsppPingTraceTable
        (pu4ErrorCode, pLsppFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry, OSIX_FALSE,
         OSIX_FALSE) != OSIX_SUCCESS)
    {

        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
        return SNMP_FAILURE;
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppIsSetFsLsppPingTraceTableEntry);
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhDepv2FsLsppGlobalConfigTable
 Input       :  The Indices
                FsLsppContextId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLsppGlobalConfigTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsLsppPingTraceTable
 Input       :  The Indices
                FsLsppContextId
                FsLsppSenderHandle
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsLsppPingTraceTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
