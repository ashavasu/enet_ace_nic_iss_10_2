/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: lsppdval.c,v 1.9 2013/06/27 12:23:21 siva Exp $
*
* Description: This file contains the routines to initialize the
*              protocol structure for the module Lspp 
*********************************************************************/

#include "lsppinc.h"

/****************************************************************************
* Function    : LsppInitializeFsLsppPingTraceTable
* Input       : pLsppFsLsppPingTraceTableEntry
* Description : It Intialize the given structure
* Output      : None
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
LsppInitializeFsLsppPingTraceTable (tLsppFsLsppPingTraceTableEntry *
                                    pLsppFsLsppPingTraceTableEntry)
{
    INT4                i4RetVal = OSIX_FAILURE;

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        return OSIX_FAILURE;
    }

    MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    i4RetVal =
        LsppInitializeMibFsLsppPingTraceTable (pLsppFsLsppPingTraceTableEntry);

    return i4RetVal;
}

/****************************************************************************
* Function    : LsppInitFsLsppGlobalConfigTable
* Input       : pLsppFsLsppGlobalConfigTableEntry
* Output      : This intializes the given structure
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
LsppInitFsLsppGlobalConfigTable (tLsppFsLsppGlobalConfigTableEntry *
                                 pLsppFsLsppGlobalConfigTableEntry)
{
    /*Initialize all the parameters to its default values. */
    LsppInitMibGlobalConfigTable (pLsppFsLsppGlobalConfigTableEntry);
    pLsppFsLsppGlobalConfigTableEntry->IndexMgrId = 0;
    MEMSET (&(pLsppFsLsppGlobalConfigTableEntry->au1IndexBitMapList), 0,
            sizeof (pLsppFsLsppGlobalConfigTableEntry->au1IndexBitMapList));
    MEMSET (pLsppFsLsppGlobalConfigTableEntry->au1ContextName, 0,
            sizeof (pLsppFsLsppGlobalConfigTableEntry->au1ContextName));

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : LsppInitFsLsppGlobalStatsTable
* Input       : pLsppFsLsppGlobalStatsTableEntry
* Output      : This intializes the given structure
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
LsppInitFsLsppGlobalStatsTable (tLsppFsLsppGlobalStatsTableEntry *
                                pLsppFsLsppGlobalStatsTableEntry)
{
    /*Initialize all the parameters to its default values. */
    MEMSET (pLsppFsLsppGlobalStatsTableEntry, 0,
            sizeof (tLsppFsLsppGlobalStatsTableEntry));

    return OSIX_SUCCESS;
}

/****************************************************************************
* Function    : LsppInitMibGlobalConfigTable
* Input       : pLsppGlobalConfigTableEntry
* Output      : This intializes the given structure
* Returns     : OSIX_SUCCESS or OSIX_FAILURE
*****************************************************************************/
INT4
LsppInitMibGlobalConfigTable (tLsppFsLsppGlobalConfigTableEntry *
                              pLsppGlobalConfigTableEntry)
{
    /*Initialize the parameters to its default values as specified in MIB */
    pLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId = 0;
    pLsppGlobalConfigTableEntry->MibObject.i4FsLsppSystemControl = LSPP_START;
    pLsppGlobalConfigTableEntry->MibObject.u4FsLsppAgeOutTime =
        LSPP_DEFAULT_AGEOUT_TIMER_VAL;
    pLsppGlobalConfigTableEntry->MibObject.u4FsLsppBfdBtStrapAgeOutTime = 0;
    pLsppGlobalConfigTableEntry->MibObject.i4FsLsppTrapStatus = 0;
    pLsppGlobalConfigTableEntry->MibObject.i4FsLsppTraceLevel = 0;
    pLsppGlobalConfigTableEntry->MibObject.
        i4FsLsppAgeOutTmrUnit = LSPP_TMR_UNIT_MIN;
    pLsppGlobalConfigTableEntry->MibObject.
        i4FsLsppClearEchoStats = LSPP_SNMP_FALSE;
    pLsppGlobalConfigTableEntry->MibObject.
        i4FsLsppBfdBtStrapRespReq = LSPP_SNMP_TRUE;
    pLsppGlobalConfigTableEntry->MibObject.
        i4FsLsppBfdBtStrapAgeOutTmrUnit = LSPP_TMR_UNIT_MIN;

    return OSIX_SUCCESS;
}
