/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: lsppdbg.c,v 1.11 2016/06/03 10:27:23 siva Exp $
*
* Description: This file contains the routines for the protocol Database Access for the module Lspp 
*********************************************************************/

#include "lsppinc.h"

/****************************************************************************
 Function    :  LsppGetAllFsLsppGlobalConfigTable
 Input       :  pLsppGetFsLsppGlobalConfigTable
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
LsppGetAllFsLsppGlobalConfigTable (tLsppFsLsppGlobalConfigTableEntry *
                                   pLsppGetFsLsppGlobalConfigTableEntry)
{
    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigTableEntry = NULL;

    /* Check whether the node is already present */
    pLsppFsLsppGlobalConfigTableEntry =
        RBTreeGet (gLsppGlobals.LsppGlbMib.FsLsppGlobalConfigTable,
                   (tRBElem *) pLsppGetFsLsppGlobalConfigTableEntry);

    if (pLsppFsLsppGlobalConfigTableEntry == NULL)
    {
        LSPP_TRC ((LSPP_UTIL_TRC,
                   "LsppGetAllFsLsppGlobalConfigTable:"
                   "Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pLsppGetFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppSystemControl =
        pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppSystemControl;

    pLsppGetFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppTrapStatus =
        pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppTrapStatus;

    pLsppGetFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppTraceLevel =
        pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppTraceLevel;

    pLsppGetFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppAgeOutTime =
        pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppAgeOutTime;

    pLsppGetFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppAgeOutTmrUnit =
        pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppAgeOutTmrUnit;

    pLsppGetFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppClearEchoStats =
        pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppClearEchoStats;

    pLsppGetFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppBfdBtStrapRespReq =
        pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppBfdBtStrapRespReq;

    pLsppGetFsLsppGlobalConfigTableEntry->MibObject.
        u4FsLsppBfdBtStrapAgeOutTime =
        pLsppFsLsppGlobalConfigTableEntry->MibObject.
        u4FsLsppBfdBtStrapAgeOutTime;

    pLsppGetFsLsppGlobalConfigTableEntry->MibObject.
        i4FsLsppBfdBtStrapAgeOutTmrUnit =
        pLsppFsLsppGlobalConfigTableEntry->MibObject.
        i4FsLsppBfdBtStrapAgeOutTmrUnit;

    if (LsppGetAllUtlFsLsppGlobalConfigTable
        (pLsppGetFsLsppGlobalConfigTableEntry,
         pLsppFsLsppGlobalConfigTableEntry) == OSIX_FAILURE)

    {
        LSPP_TRC ((LSPP_UTIL_TRC, "LsppGetAllFsLsppGlobalConfigTable:"
                   "LsppGetAllUtlFsLsppGlobalConfigTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  LsppGetAllFsLsppGlobalStatsTable
 Input       :  pLsppGetFsLsppGlobalStatsTable
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
LsppGetAllFsLsppGlobalStatsTable (tLsppFsLsppGlobalStatsTableEntry *
                                  pLsppGetFsLsppGlobalStatsTableEntry)
{
    tLsppFsLsppGlobalStatsTableEntry *pLsppFsLsppGlobalStatsTableEntry = NULL;

    /* Check whether the node is already present */
    pLsppFsLsppGlobalStatsTableEntry =
        RBTreeGet (gLsppGlobals.LsppGlbMib.FsLsppGlobalStatsTable,
                   (tRBElem *) pLsppGetFsLsppGlobalStatsTableEntry);

    if (pLsppFsLsppGlobalStatsTableEntry == NULL)
    {
        LSPP_TRC ((LSPP_UTIL_TRC,
                   "LsppGetAllFsLsppGlobalStatsTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pLsppGetFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatReqTx =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatReqTx;

    pLsppGetFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatReqRx =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatReqRx;

    pLsppGetFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatReqTimedOut =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatReqTimedOut;

    pLsppGetFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatReqUnSent =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatReqUnSent;

    pLsppGetFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatReplyTx =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatReplyTx;

    pLsppGetFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatReplyRx =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatReplyRx;

    pLsppGetFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatReplyDropped =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatReplyDropped;

    pLsppGetFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatReplyUnSent =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatReplyUnSent;

    pLsppGetFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatReplyFromEgr =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatReplyFromEgr;

    pLsppGetFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatUnLbldOutIf =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatUnLbldOutIf;

    pLsppGetFsLsppGlobalStatsTableEntry->MibObject.
        u4FsLsppGlbStatDsMapMismatch =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.
        u4FsLsppGlbStatDsMapMismatch;

    pLsppGetFsLsppGlobalStatsTableEntry->MibObject.
        u4FsLsppGlbStatFecLblMismatch =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.
        u4FsLsppGlbStatFecLblMismatch;

    pLsppGetFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatNoFecMapping =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatNoFecMapping;

    pLsppGetFsLsppGlobalStatsTableEntry->MibObject.
        u4FsLsppGlbStatUnKUpstreamIf =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.
        u4FsLsppGlbStatUnKUpstreamIf;

    pLsppGetFsLsppGlobalStatsTableEntry->MibObject.
        u4FsLsppGlbStatReqLblSwitched =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.
        u4FsLsppGlbStatReqLblSwitched;

    pLsppGetFsLsppGlobalStatsTableEntry->MibObject.
        u4FsLsppGlbStatReqUnSupptdTlv =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.
        u4FsLsppGlbStatReqUnSupptdTlv;

    pLsppGetFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatMalformedReq =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatMalformedReq;

    pLsppGetFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatNoLblEntry =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatNoLblEntry;

    pLsppGetFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatPreTermReq =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatPreTermReq;

    pLsppGetFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatProtMismatch =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatProtMismatch;

    pLsppGetFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatRsvdRetCode =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatRsvdRetCode;

    pLsppGetFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatNoRetCode =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatNoRetCode;

    pLsppGetFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatUndefRetCode =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppGlbStatUndefRetCode;

    pLsppGetFsLsppGlobalStatsTableEntry->MibObject.
        u4FsLsppGlbStatInvalidPktDropped =
        pLsppFsLsppGlobalStatsTableEntry->MibObject.
        u4FsLsppGlbStatInvalidPktDropped;

    if (LsppGetAllUtlFsLsppGlobalStatsTable
        (pLsppGetFsLsppGlobalStatsTableEntry,
         pLsppFsLsppGlobalStatsTableEntry) == OSIX_FAILURE)

    {
        LSPP_TRC ((LSPP_UTIL_TRC, "LsppGetAllFsLsppGlobalStatsTable:"
                   "LsppGetAllUtlFsLsppGlobalStatsTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  LsppGetAllFsLsppPingTraceTable
 Input       :  pLsppGetFsLsppPingTraceTable
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
LsppGetAllFsLsppPingTraceTable (tLsppFsLsppPingTraceTableEntry *
                                pLsppGetFsLsppPingTraceTableEntry)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;

    /* Check whether the node is already present */
    pLsppFsLsppPingTraceTableEntry =
        RBTreeGet (gLsppGlobals.LsppGlbMib.FsLsppPingTraceTable,
                   (tRBElem *) pLsppGetFsLsppPingTraceTableEntry);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        LSPP_TRC ((LSPP_UTIL_TRC,
                   "LsppGetAllFsLsppPingTraceTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pLsppGetFsLsppPingTraceTableEntry->MibObject.i4FsLsppRequestType =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRequestType;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.i4FsLsppRequestOwner =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRequestOwner;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.i4FsLsppPathType =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppPathType;

    MEMCPY (pLsppGetFsLsppPingTraceTableEntry->MibObject.au4FsLsppPathPointer,
            pLsppFsLsppPingTraceTableEntry->MibObject.au4FsLsppPathPointer,
            (pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppPathPointerLen *
             sizeof (UINT4)));
    pLsppGetFsLsppPingTraceTableEntry->MibObject.i4FsLsppPathPointerLen =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppPathPointerLen;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipGlobalId =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipGlobalId;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipNodeId =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipNodeId;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipIfNum =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipIfNum;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.i4FsLsppReplyMode =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppReplyMode;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.u4FsLsppRepeatCount =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppRepeatCount;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.u4FsLsppPacketSize =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppPacketSize;

    MEMCPY (pLsppGetFsLsppPingTraceTableEntry->MibObject.au1FsLsppPadPattern,
            pLsppFsLsppPingTraceTableEntry->MibObject.au1FsLsppPadPattern,
            pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppPadPatternLen);

    pLsppGetFsLsppPingTraceTableEntry->MibObject.i4FsLsppPadPatternLen =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppPadPatternLen;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.u4FsLsppTTLValue =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTTLValue;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.u4FsLsppWFRInterval =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppWFRInterval;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.i4FsLsppWFRTmrUnit =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppWFRTmrUnit;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.u4FsLsppWTSInterval =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppWTSInterval;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.i4FsLsppWTSTmrUnit =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppWTSTmrUnit;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.u4FsLsppReplyDscpValue =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppReplyDscpValue;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.i4FsLsppSweepOption =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppSweepOption;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepMinimum =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepMinimum;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepMaximum =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepMaximum;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepIncrement =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepIncrement;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.i4FsLsppBurstOption =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppBurstOption;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.u4FsLsppBurstSize =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppBurstSize;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.u4FsLsppEXPValue =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppEXPValue;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.i4FsLsppDsMap =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppDsMap;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.i4FsLsppFecValidate =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppFecValidate;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.i4FsLsppReplyPadTlv =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppReplyPadTlv;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.i4FsLsppForceExplicitNull =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppForceExplicitNull;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.i4FsLsppInterfaceLabelTlv =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppInterfaceLabelTlv;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.i4FsLsppSameSeqNumOption =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppSameSeqNumOption;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.i4FsLsppVerbose =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppVerbose;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.i4FsLsppReversePathVerify =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppReversePathVerify;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.i4FsLsppEncapType =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppEncapType;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.i4FsLsppStatus =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppStatus;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.u4FsLsppActualHopCount =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppActualHopCount;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.i4FsLsppResponderAddrType =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppResponderAddrType;

    MEMCPY (pLsppGetFsLsppPingTraceTableEntry->MibObject.au1FsLsppResponderAddr,
            pLsppFsLsppPingTraceTableEntry->MibObject.au1FsLsppResponderAddr,
            pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppResponderAddrLen);

    pLsppGetFsLsppPingTraceTableEntry->MibObject.i4FsLsppResponderAddrLen =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppResponderAddrLen;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.u4FsLsppResponderGlobalId =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppResponderGlobalId;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.u4FsLsppResponderId =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppResponderId;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.u4FsLsppMaxRtt =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppMaxRtt;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.u4FsLsppMinRtt =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppMinRtt;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.u4FsLsppAverageRtt =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppAverageRtt;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.u4FsLsppPktsTx =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppPktsTx;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.u4FsLsppPktsRx =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppPktsRx;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.u4FsLsppPktsUnSent =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppPktsUnSent;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.i4FsLsppRowStatus =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRowStatus;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.i4FsLsppStatusPathDirection =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppStatusPathDirection;

    MEMCPY (pLsppGetFsLsppPingTraceTableEntry->MibObject.au1FsLsppResponderIcc,
            pLsppFsLsppPingTraceTableEntry->MibObject.au1FsLsppResponderIcc,
            pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppResponderIccLen);

    pLsppGetFsLsppPingTraceTableEntry->MibObject.i4FsLsppResponderIccLen =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppResponderIccLen;

    MEMCPY (pLsppGetFsLsppPingTraceTableEntry->MibObject.au1FsLsppResponderUMC,
            pLsppFsLsppPingTraceTableEntry->MibObject.au1FsLsppResponderUMC,
            pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppResponderUMCLen);

    pLsppGetFsLsppPingTraceTableEntry->MibObject.i4FsLsppResponderUMCLen =
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppResponderUMCLen;

    pLsppGetFsLsppPingTraceTableEntry->MibObject.u4FsLsppResponderMepIndex =
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppResponderMepIndex;

    if (LsppGetAllUtlFsLsppPingTraceTable
        (pLsppGetFsLsppPingTraceTableEntry,
         pLsppFsLsppPingTraceTableEntry) == OSIX_FAILURE)

    {
        LSPP_TRC ((LSPP_UTIL_TRC, "LsppGetAllFsLsppPingTraceTable:"
                   "LsppGetAllUtlFsLsppPingTraceTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  LsppGetAllFsLsppEchoSequenceTable
 Input       :  pLsppGetFsLsppEchoSequenceTable
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
LsppGetAllFsLsppEchoSequenceTable (tLsppFsLsppEchoSequenceTableEntry *
                                   pLsppGetFsLsppEchoSequenceTableEntry)
{
    tLsppFsLsppEchoSequenceTableEntry *pLsppFsLsppEchoSequenceTableEntry = NULL;

    /* Check whether the node is already present */
    pLsppFsLsppEchoSequenceTableEntry =
        RBTreeGet (gLsppGlobals.LsppGlbMib.FsLsppEchoSequenceTable,
                   (tRBElem *) pLsppGetFsLsppEchoSequenceTableEntry);

    if (pLsppFsLsppEchoSequenceTableEntry == NULL)
    {
        LSPP_TRC ((LSPP_UTIL_TRC,
                   "LsppGetAllFsLsppEchoSequenceTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pLsppGetFsLsppEchoSequenceTableEntry->MibObject.u4FsLsppReturnCode =
        pLsppFsLsppEchoSequenceTableEntry->MibObject.u4FsLsppReturnCode;

    pLsppGetFsLsppEchoSequenceTableEntry->MibObject.u4FsLsppReturnSubCode =
        pLsppFsLsppEchoSequenceTableEntry->MibObject.u4FsLsppReturnSubCode;

    MEMCPY (pLsppGetFsLsppEchoSequenceTableEntry->MibObject.
            au1FsLsppReturnCodeStr,
            pLsppFsLsppEchoSequenceTableEntry->MibObject.au1FsLsppReturnCodeStr,
            pLsppFsLsppEchoSequenceTableEntry->MibObject.
            i4FsLsppReturnCodeStrLen);

    pLsppGetFsLsppEchoSequenceTableEntry->MibObject.i4FsLsppReturnCodeStrLen =
        pLsppFsLsppEchoSequenceTableEntry->MibObject.i4FsLsppReturnCodeStrLen;

    if (LsppGetAllUtlFsLsppEchoSequenceTable
        (pLsppGetFsLsppEchoSequenceTableEntry,
         pLsppFsLsppEchoSequenceTableEntry) == OSIX_FAILURE)

    {
        LSPP_TRC ((LSPP_UTIL_TRC, "LsppGetAllFsLsppEchoSequenceTable:"
                   "LsppGetAllUtlFsLsppEchoSequenceTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  LsppGetAllFsLsppHopTable
 Input       :  pLsppGetFsLsppHopTable
 Description :  This Routine Take the Indices &
                Gets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
LsppGetAllFsLsppHopTable (tLsppFsLsppHopTableEntry *
                          pLsppGetFsLsppHopTableEntry)
{
    tLsppFsLsppHopTableEntry *pLsppFsLsppHopTableEntry = NULL;

    /* Check whether the node is already present */
    pLsppFsLsppHopTableEntry =
        RBTreeGet (gLsppGlobals.LsppGlbMib.FsLsppHopTable,
                   (tRBElem *) pLsppGetFsLsppHopTableEntry);

    if (pLsppFsLsppHopTableEntry == NULL)
    {
        LSPP_TRC ((LSPP_UTIL_TRC,
                   "LsppGetAllFsLsppHopTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /* Assign values from the existing node */
    pLsppGetFsLsppHopTableEntry->MibObject.i4FsLsppHopAddrType =
        pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopAddrType;

    MEMCPY (pLsppGetFsLsppHopTableEntry->MibObject.au1FsLsppHopAddr,
            pLsppFsLsppHopTableEntry->MibObject.au1FsLsppHopAddr,
            pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopAddrLen);

    pLsppGetFsLsppHopTableEntry->MibObject.i4FsLsppHopAddrLen =
        pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopAddrLen;

    pLsppGetFsLsppHopTableEntry->MibObject.u4FsLsppHopGlobalId =
        pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopGlobalId;

    pLsppGetFsLsppHopTableEntry->MibObject.u4FsLsppHopId =
        pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopId;

    pLsppGetFsLsppHopTableEntry->MibObject.u4FsLsppHopIfNum =
        pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopIfNum;

    pLsppGetFsLsppHopTableEntry->MibObject.u4FsLsppHopReturnCode =
        pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopReturnCode;

    pLsppGetFsLsppHopTableEntry->MibObject.u4FsLsppHopReturnSubCode =
        pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopReturnSubCode;

    MEMCPY (pLsppGetFsLsppHopTableEntry->MibObject.au1FsLsppHopReturnCodeStr,
            pLsppFsLsppHopTableEntry->MibObject.au1FsLsppHopReturnCodeStr,
            pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopReturnCodeStrLen);

    pLsppGetFsLsppHopTableEntry->MibObject.i4FsLsppHopReturnCodeStrLen =
        pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopReturnCodeStrLen;

    pLsppGetFsLsppHopTableEntry->MibObject.i4FsLsppHopRxAddrType =
        pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopRxAddrType;

    MEMCPY (pLsppGetFsLsppHopTableEntry->MibObject.au1FsLsppHopRxIPAddr,
            pLsppFsLsppHopTableEntry->MibObject.au1FsLsppHopRxIPAddr,
            pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopRxIPAddrLen);

    pLsppGetFsLsppHopTableEntry->MibObject.i4FsLsppHopRxIPAddrLen =
        pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopRxIPAddrLen;

    MEMCPY (pLsppGetFsLsppHopTableEntry->MibObject.au1FsLsppHopRxIfAddr,
            pLsppFsLsppHopTableEntry->MibObject.au1FsLsppHopRxIfAddr,
            pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopRxIfAddrLen);

    pLsppGetFsLsppHopTableEntry->MibObject.i4FsLsppHopRxIfAddrLen =
        pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopRxIfAddrLen;

    pLsppGetFsLsppHopTableEntry->MibObject.u4FsLsppHopRxIfNum =
        pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopRxIfNum;

    MEMCPY (pLsppGetFsLsppHopTableEntry->MibObject.au1FsLsppHopRxLabelStack,
            pLsppFsLsppHopTableEntry->MibObject.au1FsLsppHopRxLabelStack,
            pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopRxLabelStackLen);

    pLsppGetFsLsppHopTableEntry->MibObject.i4FsLsppHopRxLabelStackLen =
        pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopRxLabelStackLen;

    MEMCPY (pLsppGetFsLsppHopTableEntry->MibObject.au1FsLsppHopRxLabelExp,
            pLsppFsLsppHopTableEntry->MibObject.au1FsLsppHopRxLabelExp,
            pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopRxLabelExpLen);

    pLsppGetFsLsppHopTableEntry->MibObject.i4FsLsppHopRxLabelExpLen =
        pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopRxLabelExpLen;

    pLsppGetFsLsppHopTableEntry->MibObject.u4FsLsppHopRtt =
        pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopRtt;

    pLsppGetFsLsppHopTableEntry->MibObject.u4FsLsppHopDsMtu =
        pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopDsMtu;

    pLsppGetFsLsppHopTableEntry->MibObject.i4FsLsppHopDsAddrType =
        pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopDsAddrType;

    MEMCPY (pLsppGetFsLsppHopTableEntry->MibObject.au1FsLsppHopDsIPAddr,
            pLsppFsLsppHopTableEntry->MibObject.au1FsLsppHopDsIPAddr,
            pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopDsIPAddrLen);

    pLsppGetFsLsppHopTableEntry->MibObject.i4FsLsppHopDsIPAddrLen =
        pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopDsIPAddrLen;

    MEMCPY (pLsppGetFsLsppHopTableEntry->MibObject.au1FsLsppHopDsIfAddr,
            pLsppFsLsppHopTableEntry->MibObject.au1FsLsppHopDsIfAddr,
            pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopDsIfAddrLen);

    pLsppGetFsLsppHopTableEntry->MibObject.i4FsLsppHopDsIfAddrLen =
        pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopDsIfAddrLen;

    pLsppGetFsLsppHopTableEntry->MibObject.u4FsLsppHopDsIfNum =
        pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopDsIfNum;

    MEMCPY (pLsppGetFsLsppHopTableEntry->MibObject.au1FsLsppHopDsLabelStack,
            pLsppFsLsppHopTableEntry->MibObject.au1FsLsppHopDsLabelStack,
            pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopDsLabelStackLen);

    pLsppGetFsLsppHopTableEntry->MibObject.i4FsLsppHopDsLabelStackLen =
        pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopDsLabelStackLen;

    MEMCPY (pLsppGetFsLsppHopTableEntry->MibObject.au1FsLsppHopDsLabelExp,
            pLsppFsLsppHopTableEntry->MibObject.au1FsLsppHopDsLabelExp,
            pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopDsLabelExpLen);

    pLsppGetFsLsppHopTableEntry->MibObject.i4FsLsppHopDsLabelExpLen =
        pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopDsLabelExpLen;

    MEMCPY (pLsppGetFsLsppHopTableEntry->MibObject.au1FsLsppHopIcc,
            pLsppFsLsppHopTableEntry->MibObject.au1FsLsppHopIcc,
            pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopIccLen);

    pLsppGetFsLsppHopTableEntry->MibObject.i4FsLsppHopIccLen =
        pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopIccLen;

    MEMCPY (pLsppGetFsLsppHopTableEntry->MibObject.au1FsLsppHopUMC,
            pLsppFsLsppHopTableEntry->MibObject.au1FsLsppHopUMC,
            pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopUMCLen);

    pLsppGetFsLsppHopTableEntry->MibObject.i4FsLsppHopUMCLen =
        pLsppFsLsppHopTableEntry->MibObject.i4FsLsppHopUMCLen;

    pLsppGetFsLsppHopTableEntry->MibObject.u4FsLsppHopMepIndex =
        pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopMepIndex;

    if (LsppGetAllUtlFsLsppHopTable
        (pLsppGetFsLsppHopTableEntry, pLsppFsLsppHopTableEntry) == OSIX_FAILURE)

    {
        LSPP_TRC ((LSPP_UTIL_TRC, "LsppGetAllFsLsppHopTable:"
                   "LsppGetAllUtlFsLsppHopTable Returns Failure\r\n"));

        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  LsppSetAllFsLsppGlobalConfigTable
 Input       :  pLsppSetFsLsppGlobalConfigTable
                pLsppIsSetFsLsppGlobalConfigTable
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
LsppSetAllFsLsppGlobalConfigTable (tLsppFsLsppGlobalConfigTableEntry *
                                   pLsppSetFsLsppGlobalConfigTableEntry,
                                   tLsppIsSetFsLsppGlobalConfigTableEntry *
                                   pLsppIsSetFsLsppGlobalConfigTableEntry)
{
    tLsppFsLsppGlobalConfigTableEntry *pLsppFsLsppGlobalConfigTableEntry = NULL;
    tLsppFsLsppGlobalConfigTableEntry LsppOldFsLsppGlobalConfigTableEntry;

    MEMSET (&LsppOldFsLsppGlobalConfigTableEntry, 0,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));

    /* Check whether the node is already present */
    pLsppFsLsppGlobalConfigTableEntry =
        RBTreeGet (gLsppGlobals.LsppGlbMib.FsLsppGlobalConfigTable,
                   (tRBElem *) pLsppSetFsLsppGlobalConfigTableEntry);

    if (pLsppFsLsppGlobalConfigTableEntry == NULL)
    {
        LSPP_TRC ((LSPP_UTIL_TRC,
                   "LsppSetAllFsLsppGlobalConfigTable: Entry doesn't exist\r\n"));
        return OSIX_FAILURE;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsLsppGlobalConfigTableFilterInputs
        (pLsppFsLsppGlobalConfigTableEntry,
         pLsppSetFsLsppGlobalConfigTableEntry,
         pLsppIsSetFsLsppGlobalConfigTableEntry) != OSIX_TRUE)
    {
        return OSIX_SUCCESS;
    }

    /* Copy the previous values before setting the new values */
    MEMCPY (&LsppOldFsLsppGlobalConfigTableEntry,
            pLsppFsLsppGlobalConfigTableEntry,
            sizeof (tLsppFsLsppGlobalConfigTableEntry));

    /* Assign values for the existing node */
    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppSystemControl !=
        OSIX_FALSE)
    {
        pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppSystemControl =
            pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
            i4FsLsppSystemControl;
    }
    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppTrapStatus != OSIX_FALSE)
    {
        pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppTrapStatus =
            pLsppSetFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppTrapStatus;
    }
    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppTraceLevel != OSIX_FALSE)
    {
        pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppTraceLevel =
            pLsppSetFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppTraceLevel;
    }
    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppAgeOutTime != OSIX_FALSE)
    {
        pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppAgeOutTime =
            pLsppSetFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppAgeOutTime;
    }
    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppAgeOutTmrUnit !=
        OSIX_FALSE)
    {
        pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppAgeOutTmrUnit =
            pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
            i4FsLsppAgeOutTmrUnit;
    }
    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppClearEchoStats !=
        OSIX_FALSE)
    {
        pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppClearEchoStats =
            pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
            i4FsLsppClearEchoStats;
    }
    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppBfdBtStrapRespReq !=
        OSIX_FALSE)
    {
        pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppBfdBtStrapRespReq =
            pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
            i4FsLsppBfdBtStrapRespReq;
    }
    if (pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppBfdBtStrapAgeOutTime !=
        OSIX_FALSE)
    {
        pLsppFsLsppGlobalConfigTableEntry->MibObject.
            u4FsLsppBfdBtStrapAgeOutTime =
            pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
            u4FsLsppBfdBtStrapAgeOutTime;
    }
    if (pLsppIsSetFsLsppGlobalConfigTableEntry->
        bFsLsppBfdBtStrapAgeOutTmrUnit != OSIX_FALSE)
    {
        pLsppFsLsppGlobalConfigTableEntry->MibObject.
            i4FsLsppBfdBtStrapAgeOutTmrUnit =
            pLsppSetFsLsppGlobalConfigTableEntry->MibObject.
            i4FsLsppBfdBtStrapAgeOutTmrUnit;
    }

    if (LsppUtilUpdateFsLsppGlobalConfigTable
        (&LsppOldFsLsppGlobalConfigTableEntry,
         pLsppFsLsppGlobalConfigTableEntry,
         pLsppIsSetFsLsppGlobalConfigTableEntry) != OSIX_SUCCESS)
    {
        if (LsppSetAllFsLsppGlobalConfigTableTrigger
            (pLsppSetFsLsppGlobalConfigTableEntry,
             pLsppIsSetFsLsppGlobalConfigTableEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)

        {
            return OSIX_FAILURE;
        }
        /*Restore back with previous values */
        MEMCPY (pLsppFsLsppGlobalConfigTableEntry,
                &LsppOldFsLsppGlobalConfigTableEntry,
                sizeof (tLsppFsLsppGlobalConfigTableEntry));
        return OSIX_FAILURE;
    }

    if (LsppSetAllFsLsppGlobalConfigTableTrigger
        (pLsppSetFsLsppGlobalConfigTableEntry,
         pLsppIsSetFsLsppGlobalConfigTableEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
 Function    :  LsppSetAllFsLsppPingTraceTable
 Input       :  pLsppSetFsLsppPingTraceTable
                pLsppIsSetFsLsppPingTraceTable
                i4RowStatusLogic
                i4RowCreateOption
 Description :  This Routine Take the Indices &
                Sets the Value accordingly.
 Output      :  None
 Returns     :  OSIX_SUCCESS or OSIX_FAILURE
****************************************************************************/
INT4
LsppSetAllFsLsppPingTraceTable (tLsppFsLsppPingTraceTableEntry *
                                pLsppSetFsLsppPingTraceTableEntry,
                                tLsppIsSetFsLsppPingTraceTableEntry *
                                pLsppIsSetFsLsppPingTraceTableEntry,
                                INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tLsppFsLsppPingTraceTableEntry *pLsppFsLsppPingTraceTableEntry = NULL;
    tLsppFsLsppPingTraceTableEntry *pLsppOldFsLsppPingTraceTableEntry = NULL;
    tLsppFsLsppPingTraceTableEntry *pLsppTrgFsLsppPingTraceTableEntry = NULL;
    tLsppIsSetFsLsppPingTraceTableEntry *pLsppTrgIsSetFsLsppPingTraceTableEntry
        = NULL;
    INT4                i4RowMakeActive = FALSE;
    INT4 		i4ErrFlag = FALSE;
    pLsppOldFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppOldFsLsppPingTraceTableEntry == NULL)
    {
        return OSIX_FAILURE;
    }
    pLsppTrgFsLsppPingTraceTableEntry =
        (tLsppFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppTrgFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppOldFsLsppPingTraceTableEntry);
        return OSIX_FAILURE;
    }
    pLsppTrgIsSetFsLsppPingTraceTableEntry =
        (tLsppIsSetFsLsppPingTraceTableEntry *)
        MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
    if (pLsppTrgIsSetFsLsppPingTraceTableEntry == NULL)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppOldFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppTrgFsLsppPingTraceTableEntry);
        return OSIX_FAILURE;
    }
    MEMSET (pLsppOldFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppTrgFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppFsLsppPingTraceTableEntry));
    MEMSET (pLsppTrgIsSetFsLsppPingTraceTableEntry, 0,
            sizeof (tLsppIsSetFsLsppPingTraceTableEntry));

    /* Check whether the node is already present */
    pLsppFsLsppPingTraceTableEntry =
        RBTreeGet (gLsppGlobals.LsppGlbMib.FsLsppPingTraceTable,
                   (tRBElem *) pLsppSetFsLsppPingTraceTableEntry);

    if (pLsppFsLsppPingTraceTableEntry == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppRowStatus ==
             CREATE_AND_WAIT)
            || (pLsppSetFsLsppPingTraceTableEntry->MibObject.
                i4FsLsppRowStatus == CREATE_AND_GO)
            ||
            ((pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppRowStatus ==
              ACTIVE) && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pLsppFsLsppPingTraceTableEntry =
                (tLsppFsLsppPingTraceTableEntry *)
                MemAllocMemBlk (LSPP_FSLSPPPINGTRACETABLE_POOLID);
            if (pLsppFsLsppPingTraceTableEntry == NULL)
            {
                if (LsppSetAllFsLsppPingTraceTableTrigger
                    (pLsppSetFsLsppPingTraceTableEntry,
                     pLsppIsSetFsLsppPingTraceTableEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    LSPP_TRC ((LSPP_UTIL_TRC,
                               "LsppSetAllFsLsppPingTraceTable: "
                               "LsppSetAllFsLsppPingTraceTableTrigger "
                               "function fails\r\n"));

                }
                LSPP_TRC ((LSPP_UTIL_TRC,
                           "LsppSetAllFsLsppPingTraceTable: Fail to "
                           "Allocate Memory.\r\n"));
                MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                    (UINT1 *)
                                    pLsppOldFsLsppPingTraceTableEntry);
                MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                    (UINT1 *)
                                    pLsppTrgFsLsppPingTraceTableEntry);
                MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                    (UINT1 *)
                                    pLsppTrgIsSetFsLsppPingTraceTableEntry);
                return OSIX_FAILURE;
            }

            MEMSET (pLsppFsLsppPingTraceTableEntry, 0,
                    sizeof (tLsppFsLsppPingTraceTableEntry));
            if ((LsppInitializeFsLsppPingTraceTable
                 (pLsppFsLsppPingTraceTableEntry)) == OSIX_FAILURE)
            {
                if (LsppSetAllFsLsppPingTraceTableTrigger
                    (pLsppSetFsLsppPingTraceTableEntry,
                     pLsppIsSetFsLsppPingTraceTableEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    LSPP_TRC ((LSPP_UTIL_TRC,
                               "LsppSetAllFsLsppPingTraceTable: "
                               "LsppSetAllFsLsppPingTraceTableTrigger "
                               "function fails\r\n"));

                }
                LSPP_TRC ((LSPP_UTIL_TRC,
                           "LsppSetAllFsLsppPingTraceTable: Fail to "
                           "Initialize the Objects.\r\n"));

                MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                    (UINT1 *) pLsppFsLsppPingTraceTableEntry);
                MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                    (UINT1 *)
                                    pLsppOldFsLsppPingTraceTableEntry);
                MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                    (UINT1 *)
                                    pLsppTrgFsLsppPingTraceTableEntry);
                MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                    (UINT1 *)
                                    pLsppTrgIsSetFsLsppPingTraceTableEntry);
                return OSIX_FAILURE;
            }
            /* Assign values for the new node */
            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle !=
                OSIX_FALSE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    u4FsLsppSenderHandle;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRequestType !=
                OSIX_FALSE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRequestType =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    i4FsLsppRequestType;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPathType !=
                OSIX_FALSE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppPathType =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    i4FsLsppPathType;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPathPointer !=
                OSIX_FALSE)
            {
                MEMCPY (pLsppFsLsppPingTraceTableEntry->MibObject.
                        au4FsLsppPathPointer,
                        pLsppSetFsLsppPingTraceTableEntry->MibObject.
                        au4FsLsppPathPointer,
                        (pLsppSetFsLsppPingTraceTableEntry->MibObject.
                         i4FsLsppPathPointerLen * sizeof (UINT4)));

                pLsppFsLsppPingTraceTableEntry->MibObject.
                    i4FsLsppPathPointerLen =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    i4FsLsppPathPointerLen;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTgtMipGlobalId !=
                OSIX_FALSE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.
                    u4FsLsppTgtMipGlobalId =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    u4FsLsppTgtMipGlobalId;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTgtMipNodeId !=
                OSIX_FALSE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipNodeId =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    u4FsLsppTgtMipNodeId;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTgtMipIfNum !=
                OSIX_FALSE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipIfNum =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    u4FsLsppTgtMipIfNum;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReplyMode !=
                OSIX_FALSE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppReplyMode =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    i4FsLsppReplyMode;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRepeatCount !=
                OSIX_FALSE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppRepeatCount =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    u4FsLsppRepeatCount;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPacketSize !=
                OSIX_FALSE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppPacketSize =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    u4FsLsppPacketSize;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPadPattern !=
                OSIX_FALSE)
            {
                MEMSET (pLsppFsLsppPingTraceTableEntry->MibObject.
                        au1FsLsppPadPattern, 0,
                        sizeof (pLsppFsLsppPingTraceTableEntry->MibObject.
                                au1FsLsppPadPattern));
                MEMCPY (pLsppFsLsppPingTraceTableEntry->MibObject.
                        au1FsLsppPadPattern,
                        pLsppSetFsLsppPingTraceTableEntry->MibObject.
                        au1FsLsppPadPattern,
                        pLsppSetFsLsppPingTraceTableEntry->MibObject.
                        i4FsLsppPadPatternLen);

                pLsppFsLsppPingTraceTableEntry->MibObject.
                    i4FsLsppPadPatternLen =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    i4FsLsppPadPatternLen;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTTLValue !=
                OSIX_FALSE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTTLValue =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    u4FsLsppTTLValue;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWFRInterval !=
                OSIX_FALSE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppWFRInterval =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    u4FsLsppWFRInterval;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWFRTmrUnit !=
                OSIX_FALSE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppWFRTmrUnit =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    i4FsLsppWFRTmrUnit;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWTSInterval !=
                OSIX_FALSE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppWTSInterval =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    u4FsLsppWTSInterval;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWTSTmrUnit !=
                OSIX_FALSE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppWTSTmrUnit =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    i4FsLsppWTSTmrUnit;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReplyDscpValue !=
                OSIX_FALSE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.
                    u4FsLsppReplyDscpValue =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    u4FsLsppReplyDscpValue;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepOption !=
                OSIX_FALSE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppSweepOption =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    i4FsLsppSweepOption;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepMinimum !=
                OSIX_FALSE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepMinimum =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    u4FsLsppSweepMinimum;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepMaximum !=
                OSIX_FALSE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepMaximum =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    u4FsLsppSweepMaximum;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepIncrement !=
                OSIX_FALSE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.
                    u4FsLsppSweepIncrement =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    u4FsLsppSweepIncrement;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppBurstOption !=
                OSIX_FALSE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppBurstOption =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    i4FsLsppBurstOption;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppBurstSize !=
                OSIX_FALSE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppBurstSize =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    u4FsLsppBurstSize;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppEXPValue !=
                OSIX_FALSE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppEXPValue =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    u4FsLsppEXPValue;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppDsMap != OSIX_FALSE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppDsMap =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppDsMap;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppFecValidate !=
                OSIX_FALSE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppFecValidate =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    i4FsLsppFecValidate;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReplyPadTlv !=
                OSIX_FALSE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppReplyPadTlv =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    i4FsLsppReplyPadTlv;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppForceExplicitNull !=
                OSIX_FALSE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.
                    i4FsLsppForceExplicitNull =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    i4FsLsppForceExplicitNull;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppInterfaceLabelTlv !=
                OSIX_FALSE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.
                    i4FsLsppInterfaceLabelTlv =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    i4FsLsppInterfaceLabelTlv;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSameSeqNumOption !=
                OSIX_FALSE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.
                    i4FsLsppSameSeqNumOption =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    i4FsLsppSameSeqNumOption;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppVerbose !=
                OSIX_FALSE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppVerbose =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    i4FsLsppVerbose;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReversePathVerify !=
                OSIX_FALSE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.
                    i4FsLsppReversePathVerify =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    i4FsLsppReversePathVerify;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppEncapType !=
                OSIX_FALSE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppEncapType =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    i4FsLsppEncapType;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRowStatus !=
                OSIX_FALSE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRowStatus =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    i4FsLsppRowStatus;
            }

            if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId !=
                OSIX_FALSE)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId =
                    pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    u4FsLsppContextId;
            }

            if ((pLsppSetFsLsppPingTraceTableEntry->MibObject.
                 i4FsLsppRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pLsppSetFsLsppPingTraceTableEntry->MibObject.
                        i4FsLsppRowStatus == ACTIVE)))
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRowStatus =
                    ACTIVE;
            }
            else if (pLsppSetFsLsppPingTraceTableEntry->MibObject.
                     i4FsLsppRowStatus == CREATE_AND_WAIT)
            {
                pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRowStatus =
                    NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd
                (gLsppGlobals.LsppGlbMib.FsLsppPingTraceTable,
                 (tRBElem *) pLsppFsLsppPingTraceTableEntry) != RB_SUCCESS)
            {
                if (LsppSetAllFsLsppPingTraceTableTrigger
                    (pLsppSetFsLsppPingTraceTableEntry,
                     pLsppIsSetFsLsppPingTraceTableEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    LSPP_TRC ((LSPP_UTIL_TRC,
                               "LsppSetAllFsLsppPingTraceTable: "
                               "LsppSetAllFsLsppPingTraceTableTrigger "
                               "function returns failure.\r\n"));
                }
                LSPP_TRC ((LSPP_UTIL_TRC,
                           "LsppSetAllFsLsppPingTraceTable: RBTreeAdd "
                           "is failed.\r\n"));

                MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                    (UINT1 *) pLsppFsLsppPingTraceTableEntry);
                MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                    (UINT1 *)
                                    pLsppOldFsLsppPingTraceTableEntry);
                MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                    (UINT1 *)
                                    pLsppTrgFsLsppPingTraceTableEntry);
                MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                    (UINT1 *)
                                    pLsppTrgIsSetFsLsppPingTraceTableEntry);
                return OSIX_FAILURE;
            }
            i4ErrFlag = LsppUtilUpdateFsLsppPingTraceTable
                (NULL, pLsppFsLsppPingTraceTableEntry,
                 pLsppIsSetFsLsppPingTraceTableEntry);
	   		 if(i4ErrFlag != OSIX_SUCCESS)
            {
                LSPP_TRC ((LSPP_UTIL_TRC,
                           "LsppSetAllFsLsppPingTraceTable: "
                           "LsppUtilUpdateFsLsppPingTraceTable function "
                           "returns failure.\r\n"));

                if (LsppSetAllFsLsppPingTraceTableTrigger
                    (pLsppSetFsLsppPingTraceTableEntry,
                     pLsppIsSetFsLsppPingTraceTableEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)

                {
                    LSPP_TRC ((LSPP_UTIL_TRC,
                               "LsppSetAllFsLsppPingTraceTable: "
                               "LsppSetAllFsLsppPingTraceTableTrigger "
                               "function returns failure.\r\n"));

                }
                RBTreeRem (gLsppGlobals.LsppGlbMib.FsLsppPingTraceTable,
                           pLsppFsLsppPingTraceTableEntry);
                MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                    (UINT1 *) pLsppFsLsppPingTraceTableEntry);
                MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                    (UINT1 *)
                                    pLsppOldFsLsppPingTraceTableEntry);
                MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                    (UINT1 *)
                                    pLsppTrgFsLsppPingTraceTableEntry);
                MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                    (UINT1 *)
                                    pLsppTrgIsSetFsLsppPingTraceTableEntry);
                return i4ErrFlag;
            }

            if ((pLsppSetFsLsppPingTraceTableEntry->MibObject.
                 i4FsLsppRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1)
                    && (pLsppSetFsLsppPingTraceTableEntry->MibObject.
                        i4FsLsppRowStatus == ACTIVE)))
            {

                if (pLsppSetFsLsppPingTraceTableEntry->MibObject.
                    i4FsLsppRowStatus == CREATE_AND_GO)
                {
                    /* For MSR and RM Trigger */
                    pLsppTrgFsLsppPingTraceTableEntry->MibObject.
                        u4FsLsppSenderHandle =
                        pLsppSetFsLsppPingTraceTableEntry->MibObject.
                        u4FsLsppSenderHandle;
                    pLsppTrgFsLsppPingTraceTableEntry->MibObject.
                        u4FsLsppContextId =
                        pLsppSetFsLsppPingTraceTableEntry->MibObject.
                        u4FsLsppContextId;
                    pLsppTrgFsLsppPingTraceTableEntry->MibObject.
                        i4FsLsppRowStatus = CREATE_AND_GO;
                    pLsppTrgIsSetFsLsppPingTraceTableEntry->bFsLsppRowStatus =
                        OSIX_TRUE;

                }
                else
                {
                    /* For MSR and RM Trigger */
                    pLsppTrgFsLsppPingTraceTableEntry->MibObject.
                        u4FsLsppSenderHandle =
                        pLsppSetFsLsppPingTraceTableEntry->MibObject.
                        u4FsLsppSenderHandle;
                    pLsppTrgFsLsppPingTraceTableEntry->MibObject.
                        u4FsLsppContextId =
                        pLsppSetFsLsppPingTraceTableEntry->MibObject.
                        u4FsLsppContextId;
                    pLsppTrgFsLsppPingTraceTableEntry->MibObject.
                        i4FsLsppRowStatus = CREATE_AND_WAIT;
                    pLsppTrgIsSetFsLsppPingTraceTableEntry->bFsLsppRowStatus =
                        OSIX_TRUE;

                }
                if (LsppSetAllFsLsppPingTraceTableTrigger
                    (pLsppTrgFsLsppPingTraceTableEntry,
                     pLsppTrgIsSetFsLsppPingTraceTableEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    LSPP_TRC ((LSPP_UTIL_TRC,
                               "LsppSetAllFsLsppPingTraceTable: "
                               "LsppSetAllFsLsppPingTraceTableTrigger "
                               "function returns failure.\r\n"));

		    OsixSemDel (pLsppFsLsppPingTraceTableEntry->SemId);
                    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                        (UINT1 *)
                                        pLsppFsLsppPingTraceTableEntry);
                    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                        (UINT1 *)
                                        pLsppOldFsLsppPingTraceTableEntry);
                    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                        (UINT1 *)
                                        pLsppTrgFsLsppPingTraceTableEntry);
                    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                        (UINT1 *)
                                        pLsppTrgIsSetFsLsppPingTraceTableEntry);
                    return OSIX_FAILURE;
                }
            }
            else if (pLsppSetFsLsppPingTraceTableEntry->MibObject.
                     i4FsLsppRowStatus == CREATE_AND_WAIT)
            {

                /* For MSR and RM Trigger */
                pLsppTrgFsLsppPingTraceTableEntry->MibObject.i4FsLsppRowStatus =
                    CREATE_AND_WAIT;
                pLsppTrgIsSetFsLsppPingTraceTableEntry->bFsLsppRowStatus =
                    OSIX_TRUE;

                if (LsppSetAllFsLsppPingTraceTableTrigger
                    (pLsppTrgFsLsppPingTraceTableEntry,
                     pLsppTrgIsSetFsLsppPingTraceTableEntry,
                     SNMP_FAILURE) != OSIX_SUCCESS)
                {
                    LSPP_TRC ((LSPP_UTIL_TRC,
                               "LsppSetAllFsLsppPingTraceTable: "
                               "LsppSetAllFsLsppPingTraceTableTrigger "
                               "function returns failure.\r\n"));
		    OsixSemDel (pLsppFsLsppPingTraceTableEntry->SemId);
                    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                        (UINT1 *)
                                        pLsppFsLsppPingTraceTableEntry);
                    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                        (UINT1 *)
                                        pLsppOldFsLsppPingTraceTableEntry);
                    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                        (UINT1 *)
                                        pLsppTrgFsLsppPingTraceTableEntry);
                    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                        (UINT1 *)
                                        pLsppTrgIsSetFsLsppPingTraceTableEntry);
                    return OSIX_FAILURE;
                }
            }
            if (LsppSetAllFsLsppPingTraceTableTrigger
                (pLsppSetFsLsppPingTraceTableEntry,
                 pLsppIsSetFsLsppPingTraceTableEntry,
                 SNMP_SUCCESS) != OSIX_SUCCESS)
            {
                LSPP_TRC ((LSPP_UTIL_TRC,
                           "LsppSetAllFsLsppPingTraceTable:  "
                           "LsppSetAllFsLsppPingTraceTableTrigger function "
                           "returns failure.\r\n"));
            }
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppOldFsLsppPingTraceTableEntry);
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppTrgFsLsppPingTraceTableEntry);
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *)
                                pLsppTrgIsSetFsLsppPingTraceTableEntry);
	    KW_FALSEPOSITIVE_FIX (pLsppFsLsppPingTraceTableEntry->SemId);
            return OSIX_SUCCESS;

        }
        else
        {
            if (LsppSetAllFsLsppPingTraceTableTrigger
                (pLsppSetFsLsppPingTraceTableEntry,
                 pLsppIsSetFsLsppPingTraceTableEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                LSPP_TRC ((LSPP_UTIL_TRC,
                           "LsppSetAllFsLsppPingTraceTable: "
                           "LsppSetAllFsLsppPingTraceTableTrigger function "
                           "returns failure.\r\n"));
            }
            LSPP_TRC ((LSPP_UTIL_TRC,
                       "LsppSetAllFsLsppPingTraceTable: Failure.\r\n"));
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppOldFsLsppPingTraceTableEntry);
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppTrgFsLsppPingTraceTableEntry);
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *)
                                pLsppTrgIsSetFsLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }
    }
    else if ((pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppRowStatus ==
              CREATE_AND_WAIT)
             || (pLsppSetFsLsppPingTraceTableEntry->MibObject.
                 i4FsLsppRowStatus == CREATE_AND_GO))
    {
        if (LsppSetAllFsLsppPingTraceTableTrigger
            (pLsppSetFsLsppPingTraceTableEntry,
             pLsppIsSetFsLsppPingTraceTableEntry, SNMP_FAILURE) != OSIX_SUCCESS)

        {
            LSPP_TRC ((LSPP_UTIL_TRC,
                       "LsppSetAllFsLsppPingTraceTable: "
                       "LsppSetAllFsLsppPingTraceTableTrigger function "
                       "returns failure.\r\n"));
        }
        LSPP_TRC ((LSPP_UTIL_TRC,
                   "LsppSetAllFsLsppPingTraceTable: The row is "
                   "already present.\r\n"));
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppOldFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppTrgFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppTrgIsSetFsLsppPingTraceTableEntry);
        return OSIX_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (pLsppOldFsLsppPingTraceTableEntry, pLsppFsLsppPingTraceTableEntry,
            sizeof (tLsppFsLsppPingTraceTableEntry));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppRowStatus ==
        DESTROY)
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRowStatus = DESTROY;

        if (LsppUtilUpdateFsLsppPingTraceTable
            (pLsppOldFsLsppPingTraceTableEntry, pLsppFsLsppPingTraceTableEntry,
             pLsppIsSetFsLsppPingTraceTableEntry) != OSIX_SUCCESS)
        {

            if (LsppSetAllFsLsppPingTraceTableTrigger
                (pLsppSetFsLsppPingTraceTableEntry,
                 pLsppIsSetFsLsppPingTraceTableEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                LSPP_TRC ((LSPP_UTIL_TRC,
                           "LsppSetAllFsLsppPingTraceTable: LsppSetAllFsLsppPingTraceTableTrigger function returns failure.\r\n"));
            }
            LSPP_TRC ((LSPP_UTIL_TRC,
                       "LsppSetAllFsLsppPingTraceTable: LsppUtilUpdateFsLsppPingTraceTable function returns failure.\r\n"));
        }
        RBTreeRem (gLsppGlobals.LsppGlbMib.FsLsppPingTraceTable,
                   pLsppFsLsppPingTraceTableEntry);
        if (LsppSetAllFsLsppPingTraceTableTrigger
            (pLsppSetFsLsppPingTraceTableEntry,
             pLsppIsSetFsLsppPingTraceTableEntry, SNMP_SUCCESS) != OSIX_SUCCESS)
        {
            LSPP_TRC ((LSPP_UTIL_TRC,
                       "LsppSetAllFsLsppPingTraceTable: LsppSetAllFsLsppPingTraceTableTrigger function returns failure.\r\n"));
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppOldFsLsppPingTraceTableEntry);
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppTrgFsLsppPingTraceTableEntry);
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *)
                                pLsppTrgIsSetFsLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppOldFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppTrgFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppTrgIsSetFsLsppPingTraceTableEntry);
        return OSIX_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsLsppPingTraceTableFilterInputs
        (pLsppFsLsppPingTraceTableEntry, pLsppSetFsLsppPingTraceTableEntry,
         pLsppIsSetFsLsppPingTraceTableEntry) != OSIX_TRUE)
    {
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppOldFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppTrgFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppTrgIsSetFsLsppPingTraceTableEntry);
        return OSIX_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRowStatus == ACTIVE)
        && (pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppRowStatus !=
            NOT_IN_SERVICE))
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        pLsppTrgFsLsppPingTraceTableEntry->MibObject.i4FsLsppRowStatus =
            NOT_IN_SERVICE;
        pLsppTrgIsSetFsLsppPingTraceTableEntry->bFsLsppRowStatus = OSIX_TRUE;

        if (LsppUtilUpdateFsLsppPingTraceTable
            (pLsppOldFsLsppPingTraceTableEntry, pLsppFsLsppPingTraceTableEntry,
             pLsppIsSetFsLsppPingTraceTableEntry) != OSIX_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pLsppFsLsppPingTraceTableEntry,
                    pLsppOldFsLsppPingTraceTableEntry,
                    sizeof (tLsppFsLsppPingTraceTableEntry));
            LSPP_TRC ((LSPP_UTIL_TRC,
                       "LsppSetAllFsLsppPingTraceTable:                 LsppUtilUpdateFsLsppPingTraceTable Function returns failure.\r\n"));

            if (LsppSetAllFsLsppPingTraceTableTrigger
                (pLsppSetFsLsppPingTraceTableEntry,
                 pLsppIsSetFsLsppPingTraceTableEntry,
                 SNMP_FAILURE) != OSIX_SUCCESS)

            {
                LSPP_TRC ((LSPP_UTIL_TRC,
                           "LsppSetAllFsLsppPingTraceTable: LsppSetAllFsLsppPingTraceTableTrigger function returns failure.\r\n"));
            }
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppOldFsLsppPingTraceTableEntry);
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *) pLsppTrgFsLsppPingTraceTableEntry);
            MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                                (UINT1 *)
                                pLsppTrgIsSetFsLsppPingTraceTableEntry);
            return OSIX_FAILURE;
        }

        if (LsppSetAllFsLsppPingTraceTableTrigger
            (pLsppTrgFsLsppPingTraceTableEntry,
             pLsppTrgIsSetFsLsppPingTraceTableEntry,
             SNMP_FAILURE) != OSIX_SUCCESS)
        {
            LSPP_TRC ((LSPP_UTIL_TRC,
                       "LsppSetAllFsLsppPingTraceTable: LsppSetAllFsLsppPingTraceTableTrigger function returns failure.\r\n"));
        }
    }

    if (pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppRowStatus ==
        ACTIVE)
    {
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRequestType != OSIX_FALSE)
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRequestType =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppRequestType;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPathType != OSIX_FALSE)
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppPathType =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppPathType;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPathPointer != OSIX_FALSE)
    {
        MEMCPY (pLsppFsLsppPingTraceTableEntry->MibObject.au4FsLsppPathPointer,
                pLsppSetFsLsppPingTraceTableEntry->MibObject.
                au4FsLsppPathPointer,
                (pLsppSetFsLsppPingTraceTableEntry->MibObject.
                 i4FsLsppPathPointerLen * sizeof (UINT4)));

        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppPathPointerLen =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppPathPointerLen;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTgtMipGlobalId !=
        OSIX_FALSE)
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipGlobalId =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipGlobalId;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTgtMipNodeId != OSIX_FALSE)
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipNodeId =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipNodeId;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTgtMipIfNum != OSIX_FALSE)
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipIfNum =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipIfNum;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReplyMode != OSIX_FALSE)
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppReplyMode =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppReplyMode;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRepeatCount != OSIX_FALSE)
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppRepeatCount =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppRepeatCount;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPacketSize != OSIX_FALSE)
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppPacketSize =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppPacketSize;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPadPattern != OSIX_FALSE)
    {
        MEMCPY (pLsppFsLsppPingTraceTableEntry->MibObject.au1FsLsppPadPattern,
                pLsppSetFsLsppPingTraceTableEntry->MibObject.
                au1FsLsppPadPattern,
                pLsppSetFsLsppPingTraceTableEntry->MibObject.
                i4FsLsppPadPatternLen);

        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppPadPatternLen =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppPadPatternLen;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTTLValue != OSIX_FALSE)
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTTLValue =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppTTLValue;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWFRInterval != OSIX_FALSE)
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppWFRInterval =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppWFRInterval;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWFRTmrUnit != OSIX_FALSE)
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppWFRTmrUnit =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppWFRTmrUnit;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWTSInterval != OSIX_FALSE)
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppWTSInterval =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppWTSInterval;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWTSTmrUnit != OSIX_FALSE)
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppWTSTmrUnit =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppWTSTmrUnit;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReplyDscpValue !=
        OSIX_FALSE)
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppReplyDscpValue =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppReplyDscpValue;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepOption != OSIX_FALSE)
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppSweepOption =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppSweepOption;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepMinimum != OSIX_FALSE)
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepMinimum =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepMinimum;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepMaximum != OSIX_FALSE)
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepMaximum =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepMaximum;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepIncrement !=
        OSIX_FALSE)
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepIncrement =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepIncrement;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppBurstOption != OSIX_FALSE)
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppBurstOption =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppBurstOption;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppBurstSize != OSIX_FALSE)
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppBurstSize =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppBurstSize;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppEXPValue != OSIX_FALSE)
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppEXPValue =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.u4FsLsppEXPValue;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppDsMap != OSIX_FALSE)
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppDsMap =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppDsMap;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppFecValidate != OSIX_FALSE)
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppFecValidate =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppFecValidate;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReplyPadTlv != OSIX_FALSE)
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppReplyPadTlv =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppReplyPadTlv;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppForceExplicitNull !=
        OSIX_FALSE)
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppForceExplicitNull =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.
            i4FsLsppForceExplicitNull;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppInterfaceLabelTlv !=
        OSIX_FALSE)
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppInterfaceLabelTlv =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.
            i4FsLsppInterfaceLabelTlv;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSameSeqNumOption !=
        OSIX_FALSE)
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppSameSeqNumOption =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.
            i4FsLsppSameSeqNumOption;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppVerbose != OSIX_FALSE)
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppVerbose =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppVerbose;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReversePathVerify !=
        OSIX_FALSE)
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppReversePathVerify =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.
            i4FsLsppReversePathVerify;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppEncapType != OSIX_FALSE)
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppEncapType =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppEncapType;
    }
    if (pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRowStatus != OSIX_FALSE)
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRowStatus =
            pLsppSetFsLsppPingTraceTableEntry->MibObject.i4FsLsppRowStatus;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRowStatus = ACTIVE;
    }

    if (LsppUtilUpdateFsLsppPingTraceTable (pLsppOldFsLsppPingTraceTableEntry,
                                            pLsppFsLsppPingTraceTableEntry,
                                            pLsppIsSetFsLsppPingTraceTableEntry)
        != OSIX_SUCCESS)
    {

        if (LsppSetAllFsLsppPingTraceTableTrigger
            (pLsppSetFsLsppPingTraceTableEntry,
             pLsppIsSetFsLsppPingTraceTableEntry, SNMP_FAILURE) != OSIX_SUCCESS)

        {
            LSPP_TRC ((LSPP_UTIL_TRC,
                       "LsppSetAllFsLsppPingTraceTable: LsppSetAllFsLsppPingTraceTableTrigger function returns failure.\r\n"));

        }
        LSPP_TRC ((LSPP_UTIL_TRC,
                   "LsppSetAllFsLsppPingTraceTable: LsppUtilUpdateFsLsppPingTraceTable function returns failure.\r\n"));
        /*Restore back with previous values */
        MEMCPY (pLsppFsLsppPingTraceTableEntry,
                pLsppOldFsLsppPingTraceTableEntry,
                sizeof (tLsppFsLsppPingTraceTableEntry));
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppOldFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppTrgFsLsppPingTraceTableEntry);
        MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                            (UINT1 *) pLsppTrgIsSetFsLsppPingTraceTableEntry);
        return OSIX_FAILURE;

    }
    if (LsppSetAllFsLsppPingTraceTableTrigger
        (pLsppSetFsLsppPingTraceTableEntry, pLsppIsSetFsLsppPingTraceTableEntry,
         SNMP_SUCCESS) != OSIX_SUCCESS)
    {

        LSPP_TRC ((LSPP_UTIL_TRC,
                   "LsppSetAllFsLsppPingTraceTable: LsppSetAllFsLsppPingTraceTableTrigger function returns failure.\r\n"));
    }

    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppOldFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppTrgFsLsppPingTraceTableEntry);
    MemReleaseMemBlock (LSPP_FSLSPPPINGTRACETABLE_POOLID,
                        (UINT1 *) pLsppTrgIsSetFsLsppPingTraceTableEntry);
    return OSIX_SUCCESS;

}
