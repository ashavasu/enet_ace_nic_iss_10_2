/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: lsppsize.txt,v 1.3 2012/02/29 09:11:00 siva Exp $
*
*********************************************************************/

tLsppFsLsppGlobalConfigTableEntry     MAX_LSPP_FSLSPPGLOBALCONFIGTABLE

tLsppFsLsppGlobalStatsTableEntry      MAX_LSPP_FSLSPPGLOBALSTATSTABLE

tLsppFsLsppPingTraceTableEntry        MAX_LSPP_FSLSPPPINGTRACETABLE

tLsppFsLsppEchoSequenceTableEntry     MAX_LSPP_FSLSPPECHOSEQUENCETABLE

tLsppFsLsppHopTableEntry              MAX_LSPP_FSLSPPHOPTABLE

tLsppEchoMsg                          MAX_LSPP_ECHOMSG_BUFFER_BLK

tLsppQMsg                             MAX_LSPP_QUEUE_DEPTH

tLsppExtOutParams                     MAX_LSPP_OUTPUT_PARAMS

tLsppExtOutParams                     MAX_LSPP_OUTPUT_PARAMS

tLsppExtInParams                      MAX_LSPP_INPUT_PARAMS

UINT1[LSPP_MAX_PDU_LEN]               MAX_LSPP_PDU_BUF

tMplsApiInInfo                        MAX_LSPP_MPLS_API_INPUT_BUF

tMplsApiOutInfo                       MAX_LSPP_MPLS_API_OUTPUT_BUF

tBfdReqParams                         MAX_LSPP_BFD_REQ_PARAM_BUF
