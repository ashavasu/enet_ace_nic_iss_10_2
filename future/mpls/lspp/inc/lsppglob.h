/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lsppglob.h,v 1.2 2010/10/17 03:01:22 prabuc Exp $
 *
 * Description: This file contains declaration of global variables 
 *              of lspp module.
 *******************************************************************/

#ifndef __LSPPGLOBAL_H__
#define __LSPPGLOBAL_H__

tLsppGlobals gLsppGlobals;


CONST CHR1  *gapReturnCodeString [] =
{
    "No return code",
    "Malformed echo request received",
    "One or more of the TLVs was not understood",
    "Replying router is an egress for the FEC at stackdepth %d",
    "Replying router has no mapping for the FEC at stackdepth %d",
    "Downstream Mapping Mismatch",
    "Upstream Interface Index Unknown",
    "Reserved ",
    "Label switched at stack-depth %d ",
    "Label switched but no MPLS forwarding at stack-depth %d ",
    "Mapping for this FEC is not the given label at stackdepth %d",
    "No label entry at stack-depth %d ",
    "Protocol not associated with interface at FEC stackdepth %d ",
    "Premature termination of ping due to label \
        stack shrinking to a single label FEC at stackdepth %d"
};


#endif  /* __LSPPGLOBAL_H__ */


/*-----------------------------------------------------------------------*/
/*                       End of the file lsppglob.h                      */
/*-----------------------------------------------------------------------*/
