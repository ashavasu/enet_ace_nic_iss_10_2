/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: lspptmr.h,v 1.2 2010/10/22 13:11:14 prabuc Exp $
*
* Description: This file contains definitions for lspp Timer
 *******************************************************************/

#ifndef __LSPPTMR_H__
#define __LSPPTMR_H__



/* constants for timer types */
typedef enum {
    LSPP_WFR_TMR = 0,
    LSPP_WTS_TMR,
    LSPP_AGE_OUT_TMR,
    LSPP_MAX_TMRS
} enLsppTmrId;

#define     NO_OF_TICKS_PER_SEC             SYS_NUM_OF_TIME_UNITS_IN_A_SEC

typedef struct _LSPP_TMR_DESC {
    VOID                (*pTmrExpFunc)(VOID *);
    INT2                i2Offset;
                            /* If this field is -1 then the fn takes no
                             * parameter
                             */
    UINT2               u2Pad;
                            /* Included for 4-byte Alignment
                             */
} tLsppTmrDesc;


#endif  /* __LSPPTMR_H__  */


/*-----------------------------------------------------------------------*/
/*                       End of the file  lspptmr.h                      */
/*-----------------------------------------------------------------------*/
