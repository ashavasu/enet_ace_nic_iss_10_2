/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: lsppprotg.h,v 1.2 2010/11/23 04:53:21 siva Exp $
*
* This file contains prototypes for functions defined in Lspp.
*********************************************************************/




PUBLIC INT4 LsppUtlCreateRBTree PROTO ((VOID));

PUBLIC INT4 LsppFsLsppGlobalConfigTableCreate PROTO ((VOID));

PUBLIC INT4 FsLsppGlobalConfigTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 LsppFsLsppGlobalStatsTableCreate PROTO ((VOID));

PUBLIC INT4 FsLsppGlobalStatsTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 LsppFsLsppPingTraceTableCreate PROTO ((VOID));

PUBLIC INT4 FsLsppPingTraceTableRBCmp PROTO ((tRBElem *, tRBElem *));


INT4 LsppUtlConvertStringtoOid PROTO((tLsppFsLsppPingTraceTableEntry *, UINT4 *, INT4 *));

PUBLIC INT4 LsppFsLsppEchoSequenceTableCreate PROTO ((VOID));

PUBLIC INT4 FsLsppEchoSequenceTableRBCmp PROTO ((tRBElem *, tRBElem *));

PUBLIC INT4 LsppFsLsppHopTableCreate PROTO ((VOID));

PUBLIC INT4 FsLsppHopTableRBCmp PROTO ((tRBElem *, tRBElem *));
tLsppFsLsppGlobalConfigTableEntry * LsppFsLsppGlobalConfigTableCreateApi PROTO ((tLsppFsLsppGlobalConfigTableEntry *));
tLsppFsLsppGlobalStatsTableEntry * LsppFsLsppGlobalStatsTableCreateApi PROTO ((tLsppFsLsppGlobalStatsTableEntry *));
tLsppFsLsppPingTraceTableEntry * LsppFsLsppPingTraceTableCreateApi PROTO ((tLsppFsLsppPingTraceTableEntry *));
tLsppFsLsppEchoSequenceTableEntry * LsppFsLsppEchoSequenceTableCreateApi PROTO ((tLsppFsLsppEchoSequenceTableEntry *));
tLsppFsLsppHopTableEntry * LsppFsLsppHopTableCreateApi PROTO ((tLsppFsLsppHopTableEntry *));
tLsppFsLsppGlobalConfigTableEntry * LsppGetFirstFsLsppGlobalConfigTable PROTO ((VOID));
tLsppFsLsppGlobalConfigTableEntry * LsppGetNextFsLsppGlobalConfigTable PROTO ((tLsppFsLsppGlobalConfigTableEntry *));
tLsppFsLsppGlobalConfigTableEntry * LsppGetFsLsppGlobalConfigTable PROTO ((tLsppFsLsppGlobalConfigTableEntry *));
INT4 LsppGetAllFsLsppGlobalConfigTable PROTO ((tLsppFsLsppGlobalConfigTableEntry *));
INT4 LsppGetAllUtlFsLsppGlobalConfigTable PROTO((tLsppFsLsppGlobalConfigTableEntry *, tLsppFsLsppGlobalConfigTableEntry *));
INT4 LsppTestAllFsLsppGlobalConfigTable PROTO ((UINT4 *, tLsppFsLsppGlobalConfigTableEntry *, tLsppIsSetFsLsppGlobalConfigTableEntry *));
INT4 LsppSetAllFsLsppGlobalConfigTable PROTO ((tLsppFsLsppGlobalConfigTableEntry *, tLsppIsSetFsLsppGlobalConfigTableEntry *));
INT4 LsppSetAllFsLsppGlobalConfigTableTrigger PROTO ((tLsppFsLsppGlobalConfigTableEntry *, tLsppIsSetFsLsppGlobalConfigTableEntry *, INT4));
INT4 FsLsppGlobalConfigTableFilterInputs PROTO ((tLsppFsLsppGlobalConfigTableEntry *, tLsppFsLsppGlobalConfigTableEntry *, tLsppIsSetFsLsppGlobalConfigTableEntry *));
INT4 LsppUtilUpdateFsLsppGlobalConfigTable PROTO ((tLsppFsLsppGlobalConfigTableEntry *, tLsppFsLsppGlobalConfigTableEntry *, tLsppIsSetFsLsppGlobalConfigTableEntry *));
tLsppFsLsppGlobalStatsTableEntry * LsppGetFirstFsLsppGlobalStatsTable PROTO ((VOID));
tLsppFsLsppGlobalStatsTableEntry * LsppGetNextFsLsppGlobalStatsTable PROTO ((tLsppFsLsppGlobalStatsTableEntry *));
tLsppFsLsppGlobalStatsTableEntry * LsppGetFsLsppGlobalStatsTable PROTO ((tLsppFsLsppGlobalStatsTableEntry *));
INT4 LsppGetAllFsLsppGlobalStatsTable PROTO ((tLsppFsLsppGlobalStatsTableEntry *));
INT4 LsppGetAllUtlFsLsppGlobalStatsTable PROTO((tLsppFsLsppGlobalStatsTableEntry *, tLsppFsLsppGlobalStatsTableEntry *));
tLsppFsLsppPingTraceTableEntry * LsppGetFirstFsLsppPingTraceTable PROTO ((VOID));
tLsppFsLsppPingTraceTableEntry * LsppGetNextFsLsppPingTraceTable PROTO ((tLsppFsLsppPingTraceTableEntry *));
tLsppFsLsppPingTraceTableEntry * LsppGetFsLsppPingTraceTable PROTO ((tLsppFsLsppPingTraceTableEntry *));
INT4 LsppGetAllFsLsppPingTraceTable PROTO ((tLsppFsLsppPingTraceTableEntry *));
INT4 LsppGetAllUtlFsLsppPingTraceTable PROTO((tLsppFsLsppPingTraceTableEntry *, tLsppFsLsppPingTraceTableEntry *));
INT4 LsppTestAllFsLsppPingTraceTable PROTO ((UINT4 *, tLsppFsLsppPingTraceTableEntry *, tLsppIsSetFsLsppPingTraceTableEntry *, INT4 , INT4));
INT4 LsppSetAllFsLsppPingTraceTable PROTO ((tLsppFsLsppPingTraceTableEntry *, tLsppIsSetFsLsppPingTraceTableEntry *, INT4 , INT4 ));

INT4 LsppInitializeFsLsppPingTraceTable PROTO ((tLsppFsLsppPingTraceTableEntry *));

INT4 LsppInitializeMibFsLsppPingTraceTable PROTO ((tLsppFsLsppPingTraceTableEntry *));
INT4 LsppSetAllFsLsppPingTraceTableTrigger PROTO ((tLsppFsLsppPingTraceTableEntry *, tLsppIsSetFsLsppPingTraceTableEntry *, INT4));
INT4 FsLsppPingTraceTableFilterInputs PROTO ((tLsppFsLsppPingTraceTableEntry *, tLsppFsLsppPingTraceTableEntry *, tLsppIsSetFsLsppPingTraceTableEntry *));
INT4 LsppUtilUpdateFsLsppPingTraceTable PROTO ((tLsppFsLsppPingTraceTableEntry *, tLsppFsLsppPingTraceTableEntry *, tLsppIsSetFsLsppPingTraceTableEntry *));
tLsppFsLsppEchoSequenceTableEntry * LsppGetFirstFsLsppEchoSequenceTable PROTO ((VOID));
tLsppFsLsppEchoSequenceTableEntry * LsppGetNextFsLsppEchoSequenceTable PROTO ((tLsppFsLsppEchoSequenceTableEntry *));
tLsppFsLsppEchoSequenceTableEntry * LsppGetFsLsppEchoSequenceTable PROTO ((tLsppFsLsppEchoSequenceTableEntry *));
INT4 LsppGetAllFsLsppEchoSequenceTable PROTO ((tLsppFsLsppEchoSequenceTableEntry *));
INT4 LsppGetAllUtlFsLsppEchoSequenceTable PROTO((tLsppFsLsppEchoSequenceTableEntry *, tLsppFsLsppEchoSequenceTableEntry *));
tLsppFsLsppHopTableEntry * LsppGetFirstFsLsppHopTable PROTO ((VOID));
tLsppFsLsppHopTableEntry * LsppGetNextFsLsppHopTable PROTO ((tLsppFsLsppHopTableEntry *));
tLsppFsLsppHopTableEntry * LsppGetFsLsppHopTable PROTO ((tLsppFsLsppHopTableEntry *));
INT4 LsppGetAllFsLsppHopTable PROTO ((tLsppFsLsppHopTableEntry *));
INT4 LsppGetAllUtlFsLsppHopTable PROTO((tLsppFsLsppHopTableEntry *, tLsppFsLsppHopTableEntry *));
INT4 LsppGetFsLsppTrapContextName PROTO ((UINT1 *));

PUBLIC INT4 LsppLock (VOID);
PUBLIC INT4 LsppUnLock (VOID);

