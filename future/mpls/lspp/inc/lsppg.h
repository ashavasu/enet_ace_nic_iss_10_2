/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lsppg.h,v 1.1 2010/10/14 21:23:49 prabuc Exp $
 *
 * Description: This file contains definition of LSP Ping module lock and 
 *              unlock
 *******************************************************************/


#define LSPP_LOCK  LsppMainTaskLock ()

#define LSPP_UNLOCK  LsppMainTaskUnLock ()
