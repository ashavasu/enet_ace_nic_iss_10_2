/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lsppinc.h,v 1.8 2012/02/29 09:11:00 siva Exp $
 *
 * Description: This file contains include files of lspp module.
 *******************************************************************/


#ifndef __LSPPINC_H__
#define __LSPPINC_H__

#include "lr.h"
#include "cli.h"
#include "cfa.h"
#include "vcm.h"
#include "lspp.h"
#include "ip.h"
#include "fm.h"
#include "index.h" 
#include "fssocket.h"
#include "fssnmp.h"
#include "fssyslog.h"
#include "params.h"
#include "snmcdefn.h"
#include "snmctdfs.h"
#include "snmccons.h"
#include "lsppg.h"

#include "fssyslog.h"
#include "snmputil.h"

#include "lsppdefn.h"
#include "lspptdfsg.h"
#include "lspptdfs.h"
#include "lspptmr.h"
#include "lspptrc.h"
#include "lsppmacs.h"
#include "bfd.h"

#ifdef __LSPPMAIN_C__

#include "lsppglob.h"
#include "lspplwg.h"
#include "lsppdefg.h"
#include "lsppsz.h"
#include "lsppwrg.h"

#else

#include "lsppextn.h"
#include "lsppmibclig.h"
#include "lspplwg.h"
#include "lsppdefg.h"
#include "lsppsz.h"
#include "lsppwrg.h"

#endif /* __LSPPMAIN_C__*/

#include "lsppprot.h"
#include "lsppprotg.h"
#include "mplsutl.h"
#include "teextrn.h"
#include "mpls.h"
#endif
/*-----------------------------------------------------------------------*/
/*                       End of the file lsppinc.h                       */
/*-----------------------------------------------------------------------*/

