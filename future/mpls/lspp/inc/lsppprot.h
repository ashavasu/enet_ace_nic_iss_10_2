/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * 
 * $Id: lsppprot.h,v 1.30 2013/12/07 10:58:54 siva Exp $
 *
 * Description: This file contains declaration of function 
 *              prototypes of lspp module.
 *******************************************************************/

#ifndef __LSPPPROT_H__
#define __LSPPPROT_H__ 

#include "cli.h"

/* Add Prototypes here */

/**************************lsppmain.c*******************************/
PUBLIC VOID LsppMainTask             PROTO ((VOID));
PUBLIC UINT4 LsppMainTaskInit         PROTO ((VOID));
PUBLIC VOID LsppMainDeInit            PROTO ((VOID));
PUBLIC INT4 LsppMainTaskLock          PROTO ((VOID));
PUBLIC INT4 LsppMainTaskUnLock        PROTO ((VOID));



INT4 LsppMainVcmRegister (VOID);
VOID LsppMainVcmDeRegister (VOID);


INT4 LsppMainSysLogRegister (VOID);
VOID LsppMainSysLogDeRegister (VOID);

VOID LsppMainDestroyAllTables (VOID);
/**************************lsppque.c********************************/
PUBLIC VOID LsppQueProcessMsgs        PROTO ((VOID));
INT4 LsppQueEnqMsg PROTO ((tLsppQMsg *pQMsg, UINT1 *pu1ErrorCode));
INT4 LsppQueRxPdu  PROTO ((UINT4 u4ContextId, 
                                   tLsppRxPduInfo *pLsppRxPduInfo));

/**************************lspptmr.c********************************/
PUBLIC VOID  LsppTmrInitTmrDesc      PROTO(( VOID));
PUBLIC INT4  LsppTmrInit             PROTO(( VOID));
PUBLIC VOID  LsppTmrDeInit             PROTO(( VOID));
PUBLIC VOID  LsppTmrStartTmr         PROTO((tTmrBlk * pLsppTmr, 
                                  UINT1 u1TmrId, UINT4 u4Secs,
                                           UINT4 u4MilliSecs));
PUBLIC VOID  LsppTmrRestartTmr       PROTO((tTmrBlk * pLsppTmr, 
                                UINT1 u1TimerId, UINT4 u4Secs, 
                                            UINT4 u4MilliSecs));
PUBLIC VOID  LsppTmrStopTmr          PROTO((tTmrBlk * pLsppTmr));
PUBLIC VOID  LsppTmrHandleExpiry     PROTO((VOID));

/**************************lspptrc.c*******************************/


CHR1  *LsppTrc           PROTO(( UINT4 u4Trc,  const char *fmt, ...));
VOID   LsppTrcPrint      PROTO(( const char *fname, UINT4 u4Line, const char *s));
VOID   LsppTrcWrite      PROTO(( CHR1 *s));

/**************************lspptask.c*******************************/

INT4 LsppExtTriggerPing PROTO ((UINT4 u4ContextId, 
                          tLsppExtTrigInfo *pLsppExtTrigInfo));

INT4 LsppRxProcessPdu PROTO ((tLsppEchoMsg * pEchoMsg, UINT1 *pu1Pkt, 
                              UINT4 u4PktSize, BOOL1 bIsMPls));


PUBLIC VOID LsppTrcLsppEventLogNotify PROTO (( UINT4 u4ContextId, 
                                               UINT4 u4ArrayIndex, ...));

PUBLIC VOID LsppTrcIssEventLogNotify PROTO (( UINT4 u4ContextId, 
                                                   UINT4 u4ArrayIndex, ...));

VOID LsppTrcSysLogMsg PROTO ((UINT4, UINT4, CHR1 *));

VOID LsppTrapSendTrapNotifications PROTO ((va_list, UINT4, UINT1 *, INT4));

PUBLIC 
INT4 LsppTrapFormVarBindList PROTO ((UINT1 *, UINT4, tSNMP_VAR_BIND **, 
                                        tSNMP_COUNTER64_TYPE));





INT4 LsppInitFsLsppGlobalConfigTable PROTO ((tLsppFsLsppGlobalConfigTableEntry *));
INT4 LsppInitFsLsppGlobalStatsTable PROTO ((tLsppFsLsppGlobalStatsTableEntry *));
INT4
LsppInitMibGlobalConfigTable PROTO ((tLsppFsLsppGlobalConfigTableEntry *                                                       pLsppGlobalConfigTableEntry));

VOID LsppTmrWTSTmrExp PROTO ((VOID *));
VOID LsppTmrWFRTmrExp PROTO ((VOID *));
VOID LsppTmrAgeOutTmrExp PROTO ((VOID *));
INT4 LsppTmrRestartAgeOutTimer PROTO((tLsppFsLsppGlobalConfigTableEntry *,
                                      tLsppFsLsppGlobalConfigTableEntry *));


/*-----------------------  lsppdb.c ---------------------*/


INT4 LsppDbPingTraceCheckDependency  PROTO((tLsppFsLsppPingTraceTableEntry *));

INT4 LsppDbPingTraceDependencyTest  PROTO((tLsppFsLsppPingTraceTableEntry *));

INT4
LsppDbPingTraceInitialTest PROTO ((tLsppFsLsppPingTraceTableEntry *
                            pLsppSetFsLsppPingTraceTable,
                            tLsppIsSetFsLsppPingTraceTableEntry *
                            pLsppIsSetFsLsppPingTraceTable));



tLsppFsLsppEchoSequenceTableEntry *
LsppDbCreateEchoSeqNode PROTO ((tLsppFsLsppPingTraceTableEntry 
                                *pLsppPingTraceEntry));
INT4
LsppDbGlobalConfigInitialTest PROTO ((tLsppFsLsppGlobalConfigTableEntry
                               *pLsppSetFsLsppGlobalConfigTable,
                               tLsppIsSetFsLsppGlobalConfigTableEntry
                               *pLsppIsSetFsLsppGlobalConfigTable));

/*-----------------------  lspputil.c ---------------------*/

INT4 LsppUtilTestBaseOid  PROTO((tLsppFsLsppPingTraceTableEntry *, INT4));
INT4 LsppUtilDeleteAllTablesInCxt PROTO((tLsppFsLsppGlobalConfigTableEntry *));
VOID LsppUtilGetMaxReqsToSend PROTO ((tLsppFsLsppPingTraceTableEntry *, UINT4 *));

PUBLIC VOID LsppUtilCalcPingPduSize PROTO ((tLsppEchoMsg *));

PUBLIC VOID
LsppUtilCalculatePktSize PROTO ((tLsppEchoMsg * pLsppEchoMsg));

VOID
LsppUtilFillStaticLspFec PROTO ((tLsppTeTnlInfo * pTeTnlInfo,                                                  tLsppStaticLspFec *pLsppStaticLspFec));

VOID
LsppUtilFillRsvpFecInfo PROTO ((tLsppTeTnlInfo *pLsppTeTnlInfo, 
                         tLsppEchoMsg *pLsppEchoMsg, UINT1 u1FecStackDepth));

VOID
LsppUtilFillStaticPwFec PROTO ((tLsppPwDetail *pLsppPwDetail,                                                  tLsppStaticPwFec *pLsppStaticPwFec));

PUBLIC VOID
LsppUtilFillLdpFecInfo PROTO ((tLsppNonTeInfo *pLsppNonTeInfo, 
                       tLsppEchoMsg *pLsppEchoMsg, UINT1 u1FecStackDepth));

VOID
LsppUtilFillFec128PwDetails PROTO ((tLsppPwDetail * pLsppPwDetail,                                            tLsppPwFec128Tlv *pLsppPwFec128Tlv));

VOID
LsppUtilFillFec129PwDetails PROTO ((tLsppPwDetail * pLsppPwDetail, 
                             tLsppPwFec129Tlv *pLsppPwFec129Tlv));
PUBLIC VOID
LsppUtilFillLblInfo PROTO ((tLsppLblInfo *pLblInfo, UINT4 u4Label, UINT1 u1Exp, 
                     UINT1 u1Si, UINT1 u1Ttl, UINT1 u1Protocol));
VOID
LsppUtilFillLsppHeader PROTO ((tLsppFsLsppPingTraceTableEntry * pLsppPingTraceEntry,
                                              tLsppHeader * pLsppHeader));
VOID
LsppUtilUpdateIccMepTlv PROTO ((tLsppMegDetail * pLsppMegDetail,
                                tMplsIccMepTlv * pIccMepTlv));
VOID
LsppUtilUpdateIpLspMepTlv PROTO ((tLsppPathInfo *pLsppPathInfo,
                                  tMplsIpLspMepTlv * pIpLspMepTlv));
VOID
LsppUtilUpdateIpPwMepTlv PROTO ((tLsppPwDetail *pLsppPwDetail,
                                 tMplsIpPwMepTlv * pMplsIpPwMepTlv));
VOID LsppUtilTmToSec PROTO ((tUtlTm * tm, UINT4 *pu4Secs));
VOID LsppUtilGetSysTime PROTO ((UINT4 *pu4TimeInSec, UINT4 *pu4TimeInMSec));
PUBLIC VOID
LsppUtilCalcTimeToStartTmr PROTO ((INT4 i4TmrUnit, UINT4 u4TmrInterval, 
                                  UINT4 *pu4TimeInSec,
                                  UINT4 *pu4TimeMilliInSec));
tCRU_BUF_CHAIN_HEADER *
LsppUtilDupBuf (tCRU_BUF_CHAIN_HEADER * pSrc);

VOID
LsppUtilUpdateLblStk PROTO ((tLsppLblInfo * pLblInfo, UINT1 u1LblStackDepth,
                      UINT1 *pu1LabelStack, INT4 *pi4LblStkLen,
                      UINT1 *pu1ExpStack, INT4 *pi4LblExpStkLen));

VOID
LsppUtilClearEchoStatistics PROTO ((tLsppFsLsppGlobalStatsTableEntry *));

PUBLIC INT4
LsppUtilVerifyIpAddrRange PROTO ((UINT4 u4Addr));

/************************* lsppsock.c *************/
PUBLIC INT4 LsppSockInit (VOID);
VOID LsppSockDeInit (VOID);
INT4 LsppSockInitSockParams PROTO ((INT4));
INT4 LsppSockTransmitEchoRplyPkt PROTO 
(( UINT4, tCRU_BUF_CHAIN_HEADER *,UINT4, UINT1 *, UINT4, BOOL1));
PUBLIC INT4 LsppSockAddFd (VOID);
VOID LsppSockReceiveEchoPkt (VOID);
PUBLIC UINT4 LsppSockSendMessage (UINT1 *pu1Data, UINT4 u4BufLen,
                            UINT4 u4DestAddr,UINT4,
                            UINT4 u4IfAddr);

/************************* lsppapi.c *************/
PUBLIC VOID LsppApiPktRcvdOnSocket (INT4);




/************************* lsppport.c *************/
VOID LsppPortConvertProtocolVal (tLsppExtOutParams * pLsppExtOutParams, UINT1, 
                                 UINT4);
VOID LsppPortMapProtocol (tMplsLblInfo *pLblInfo, UINT1 u1LblStkCnt, UINT1, 
                          UINT4);

INT4 LsppPortHandleExtInteraction PROTO ((tLsppExtInParams *, 
                                          tLsppExtOutParams *));

VOID LsppPortCopyMegInfo PROTO ((tLsppExtOutParams *, tMplsApiOutInfo *));
VOID LsppPortCopyTnlInfo PROTO ((tLsppExtOutParams *, tMplsApiOutInfo *,UINT1,
                                 UINT1));
VOID LsppPortCopyLspInfo PROTO ((tLsppExtOutParams *, tMplsApiOutInfo *));
VOID LsppPortCopyPwInfo PROTO ((tLsppExtOutParams *, tMplsApiOutInfo *));

INT4 LsppPortGetMegMeName PROTO ((UINT4, tLsppMegIndices *, tLsppMegMeName *));

INT4 LsppMainRegisterWithVcm (VOID);

VOID LsppApiVcmCallback PROTO ((UINT4, UINT4, UINT1));

INT4
LsppCliFreeSendersHandle PROTO ((UINT4 u4ContextId, UINT4 u4SenderHandle));

INT4 LsppCliSendEchoRqstShowOutput PROTO ((tCliHandle, 
                                          tLsppFsLsppPingTraceTableEntry *));

VOID LsppCliPrintGlobalStats PROTO ((tCliHandle CliHandle,
                             tLsppFsLsppGlobalStatsTableEntry *, UINT1 *));

VOID LsppCliPrintGlobalConfig PROTO ((tCliHandle CliHandle,
                               tLsppFsLsppGlobalConfigTableEntry *));

VOID LsppCliPrintPingTraceTable PROTO ((tCliHandle CliHandle,
                                 tLsppFsLsppPingTraceTableEntry *, UINT1 *,
                                 UINT1 *, UINT1));
VOID LsppCliDisplayPingTable PROTO ((tCliHandle CliHandle,
                              tLsppFsLsppPingTraceTableEntry *));
VOID LsppCliDisplayTraceRouteTable PROTO ((tCliHandle CliHandle,
                                    tLsppFsLsppPingTraceTableEntry *));
VOID LsppCliPrintEchoSequenceTable PROTO ((tCliHandle CliHandle,
                                    tLsppFsLsppPingTraceTableEntry *));
VOID LsppCliDisplayEchoSequence PROTO ((tCliHandle CliHandle,
                                 tLsppFsLsppEchoSequenceTableEntry *));
VOID LsppCliPrintHopInfoTable PROTO( (tCliHandle CliHandle,
                               tLsppFsLsppPingTraceTableEntry *));
VOID LsppCliDisplayHopInfo PROTO ((tCliHandle CliHandle,tLsppFsLsppHopTableEntry *,
                                   tLsppFsLsppPingTraceTableEntry *));
VOID LsppCliPrintGlobalStatsDetail PROTO((tCliHandle CliHandle,
                                    tLsppFsLsppGlobalStatsTableEntry *));
INT4 LsppCliPrintOutputHeader PROTO((tCliHandle CliHandle,
                               tLsppFsLsppPingTraceTableEntry *));
VOID LsppCliPrintCodes PROTO((tCliHandle CliHandle));
VOID LsppCliShowPingOutput PROTO((tCliHandle CliHandle, 
                                  tLsppFsLsppPingTraceTableEntry *,
                            tLsppFsLsppEchoSequenceTableEntry *,
                            tLsppFsLsppHopTableEntry *));
VOID LsppCliShowTraceRouteOutput PROTO((tCliHandle CliHandle,
                                  tLsppFsLsppPingTraceTableEntry *,
                                  tLsppFsLsppEchoSequenceTableEntry *,
                                  tLsppFsLsppHopTableEntry *));

INT4 
LsppCliSetFsLsppGlobalConfigTable PROTO ((tCliHandle CliHandle, 
        tLsppFsLsppGlobalConfigTableEntry * pLsppSetFsLsppGlobalConfigTable,            tLsppIsSetFsLsppGlobalConfigTableEntry * 
        pLsppIsSetFsLsppGlobalConfigTable));

INT4 LsppCliSetDebugLevel (tCliHandle CliHandle, INT4 i4CliDebugLevel); 
INT4 
LsppCliSetFsLsppPingTraceTable PROTO ((tCliHandle CliHandle, 
        tLsppFsLsppPingTraceTableEntry * pLsppSetFsLsppPingTraceTable, 
        tLsppIsSetFsLsppPingTraceTableEntry * pLsppIsSetFsLsppPingTraceTable));


/**************************lsppcxt.c*******************************/
PUBLIC INT4 LsppCxtCreateGlobalConfig (UINT4 u4ContextId);
PUBLIC INT4 LsppCxtCreateGlobalStats (UINT4 u4ContextId);
PUBLIC INT4 LsppCxtCreateContext (UINT4 u4ContextId);
PUBLIC INT4 LsppCxtDeleteGlobalConfig (UINT4 u4ContextId);
PUBLIC INT4 LsppCxtDeleteGlobalStats (UINT4 u4ContextId);
PUBLIC VOID LsppCxtDeleteHopInfoNode (tLsppFsLsppHopTableEntry *pLsppHopInfoEntry);
PUBLIC VOID LsppCxtDeleteHopInfoEntries (UINT4 u4ContextId, 
                                       UINT4 u4SenderHandle);
PUBLIC VOID LsppCxtDeleteEchoSeqNode (tLsppFsLsppEchoSequenceTableEntry *);
PUBLIC VOID LsppCxtDeleteEchoSequences (UINT4 , UINT4 );
PUBLIC VOID LsppCxtDeletePingTraceEntries (UINT4 u4ContextId);
PUBLIC INT4 LsppCxtDeletePingTraceNode PROTO ((tLsppFsLsppPingTraceTableEntry 
                                               *pLsppFsLsppPingTraceEntry));

PUBLIC INT4 LsppCxtDeletePingTraceInfo PROTO ((tLsppFsLsppPingTraceTableEntry 
                                               *pLsppFsLsppPingTraceEntry));

PUBLIC INT4 LsppCxtDeleteContext (UINT4 u4ContextId, BOOL1);


/********************** lsppcore.c ********************/
PUBLIC INT4
LsppCoreCreateAndTriggerEcho (tLsppFsLsppPingTraceTableEntry * pPingTraceEntry,                                    UINT4 u4BfdDiscriminator);
PUBLIC INT4 LsppCoreFecLabelValidate (tLsppEchoMsg *pLsppEchoMsg);
PUBLIC INT4 LsppCoreVerifyDsmap (tLsppEchoMsg *pLsppEchoMsg);
PUBLIC INT4 LsppCoreEgressProcess (tLsppEchoMsg *pLsppEchoMsg);
INT4 LsppCoreFecValidate PROTO ((UINT4 u4ContextId, 
                                        tLsppFecTlv *pLsppFecTlv,
                                          UINT4 u4RxLabelL,
                                          UINT4,
                                          UINT1 *pu1FecStatus, 
                                          UINT1 *pu1ReturnCode));
PUBLIC UINT4
LsppCoreUpdateOutPathInfo PROTO ((tLsppEchoMsg * pLsppEchoMsg, 
                           UINT1 u1PathType));

INT4 LsppCoreCheckFecLabelMap (UINT4 u4ContextId, 
                                       tLsppFecTlv *pLsppFecTlv, 
                                       UINT4 u4RxLabelL,
                                       UINT4,
                                       UINT1 *pu1FecStatus, 
                                       UINT1 *pu1ReturnCode);
INT4 LsppCoreGetLabelInfo (UINT4 u4ContextId, UINT4 u4RxIfIndex,
                                   UINT4 u4RxLabel, UINT1 *pu1EntryExists,
                                   UINT1 *pu1LblAction);
INT4
LsppCoreSendEchoReply (tLsppEchoMsg *pLsppEchoMsg);
INT4 LsppCoreGetDsmapInfo (tLsppEchoMsg *pLsppEchoMsg);

VOID LsppCoreUpdateGlobalStats (UINT4 u4ContextId, UINT1 u1ParamType,
                                UINT1 *pu1RetCode);
INT4 LsppCoreGetL3If (UINT4 u4ContextId, UINT4 u4IfIndex, UINT4 *pu4L3IfIndex);
INT4 LsppCoreGetSrcIpFromDestIp (UINT4 u4ContextId,UINT4 u4DestIp, 
                                 UINT4 *pu4SrcIp);
INT4 LsppCoreGetRevPathFromDestIp (UINT4 u4ContextId,UINT4 u4DestIp,UINT4 *u4SrcIp, tLsppPathInfo *pLsppPathInfo);
INT4 LsppCoreGetIfMtu (UINT4 u4ContextId, UINT4 u4IfIndex, UINT2 *pu2Mtu);

VOID LsppCoreUpdateMepOrMipTlv (tLsppEchoMsg *pEchoMsg);

VOID LsppCoreUpdateMepTlv (UINT1 u1OperatorType, UINT1 u1ServiceType,
                           tLsppPathInfo *, tLsppAchInfo *);

PUBLIC 
UINT4 LsppCoreProcessRequest (tLsppEchoMsg * pLsppEchoMsg,
                        tLsppFsLsppPingTraceTableEntry * pLsppPingTraceEntry,
                        tLsppFsLsppEchoSequenceTableEntry * pLsppEchoSequenceEntry,
                        tLsppSysTime *pLsppEchoPktRxTime,
                        UINT1);
    
VOID LsppCoreUpdateEchoSeqTableInfo(tLsppEchoMsg * pLsppEchoMsg,
                                    tLsppFsLsppEchoSequenceTableEntry *);

VOID LsppCoreUpdatePingTraceInfo(tLsppEchoMsg * pLsppEchoMsg,
                                 tLsppFsLsppPingTraceTableEntry *pPingTraceEntry);

VOID LsppCoreUpdateEchoRttInfo(tLsppFsLsppPingTraceTableEntry * pLsppPingTraceEntry,
                                UINT4 u4Rtt);
UINT4
LsppCoreStartAgeOutTimer (UINT4 u4ContextId,
                          tLsppFsLsppPingTraceTableEntry * pLsppPingTraceEntry,
                          UINT4 u4AgeOutTmrValue, INT4 i4AgeOutTmrUnit);
PUBLIC UINT4
LsppCoreFillEncapHeader( tLsppFsLsppPingTraceTableEntry * pLsppPingTraceEntry,
                         tLsppEchoMsg * pLsppEchoMsg);

PUBLIC UINT4
LsppCoreFillDsMapTlvInfo (UINT4, tLsppOutSegInfo * pOutSegInfo,
                          tLsppDsMapTlv * pDsMapTlv);

PUBLIC UINT4
LsppCoreUpdateTgtFecInfo(tLsppEchoMsg *pLsppEchoMsg, UINT1 u1PathType);

PUBLIC INT4
LsppCoreUpdateOutLblInfo (tLsppEchoMsg *pLsppEchoMsg, tLsppLblInfo *pLblInfo,
                          UINT1 *pu1LblStkDepth);

PUBLIC INT4 LsppCoreUpdateDsMapInfo (tLsppEchoMsg *pLsppEchoMsg,
                                     tLsppFsLsppPingTraceTableEntry *);

PUBLIC INT4 LsppCoreUpdateNilFec (tLsppEchoMsg *pLsppEchoMsg);

VOID LsppCoreUpdatePadTlvInfo(UINT1 *pPadPattern, 
                              UINT1 u1PadPatternLen,
                              tLsppPadTlv *pLsppPadTlv);

PUBLIC UINT4
LsppCoreUpdateEchoMsgInfo(tLsppFsLsppPingTraceTableEntry *pLsppPingTraceEntry,
                          tLsppEchoMsg * pLsppEchoMsg,
                          tLsppDsMapTlv *pRxDsMapTlv,
                          UINT4 u4BfdDiscriminator);

PUBLIC INT4
LsppCoreGetPathInfo( UINT4 u4ContextId,
                     tLsppPathId * pLsppPathId,
                     tLsppPathInfo * pLsppPathInfo, UINT4);

PUBLIC UINT4
LsppCoreUpdatePingTraceResults(tLsppFsLsppPingTraceTableEntry *pPingTraceEntry,
                          tLsppEchoMsg *pLsppEchoMsg, UINT1 u1ReturnCode);


PUBLIC INT4        LsppCoreGetIfLblStkTlvInfo (tLsppEchoMsg * pLsppEchoMsg);

PUBLIC UINT4
LsppCoreSendTraceRouteReq (tLsppFsLsppPingTraceTableEntry * pPingTraceEntry,
                           tLsppEchoMsg * pLsppEchoMsg);

PUBLIC INT4
LsppCoreGetBfdBtStrapRespReq (UINT4 u4ContextId, UINT1 *pu1IsRespReq);

PUBLIC UINT4
LsppCoreHandleTraceUnsent(tLsppFsLsppPingTraceTableEntry * pLsppPingTraceEntry);


PUBLIC INT4
LsppCoreGetModuleStatus PROTO ((UINT4 u4ContextId, INT4 *pi4Status));


PUBLIC VOID
LsppCoreUpdateEchoReplyStats PROTO ((tLsppEchoMsg * pLsppEchoMsg,
                              tLsppFsLsppPingTraceTableEntry *pPingTraceEntry,
                              tLsppFsLsppEchoSequenceTableEntry *
                              pEchoSequenceEntry,
                              tLsppSysTime * pLsppEchoPktRxTime));


PUBLIC INT4
LsppCoreVerifyDsmapProtocol PROTO ((tLsppEchoMsg *pLsppEchoMsg));

INT4
LsppCoreGetRevPathFromFwd PROTO ((tLsppEchoMsg *pLsppEchoMsg));

PUBLIC INT4 LsppCoreHandleReqTimedOut PROTO((tLsppFsLsppEchoSequenceTableEntry *));

PUBLIC UINT4 LsppCoreSendEchoReq  PROTO((tLsppFsLsppPingTraceTableEntry *,
                                 tLsppEchoMsg *, tLsppDsMapTlv *, UINT4, UINT4 *));

PUBLIC UINT4 LsppCoreInitiateRequest  PROTO((tLsppFsLsppPingTraceTableEntry *,
                                 tLsppEchoMsg *, tLsppDsMapTlv *, UINT4));

VOID LsppCoreHandleAgeOut PROTO ((tLsppFsLsppPingTraceTableEntry *));

VOID
LsppCoreUpdateDefaultReplyMode PROTO ((tLsppEchoMsg * pLsppEchoMsg));

PUBLIC UINT4
LsppCoreReversePathValidate (tLsppEchoMsg * pLsppEchoMsg,
                             tLsppFsLsppPingTraceTableEntry  *pPingTraceEntry);

PUBLIC UINT4
LsppCoreGetNodeId PROTO ((UINT4, tLsppNodeId *pLsppNodeId));

PUBLIC UINT4
LsppCoreCalcRtt PROTO ((tLsppEchoMsg * pLsppEchoMsg,
                        tLsppSysTime * pLsppEchoPktRxTime, UINT4 *pu4Rtt));


/* Prototype declarations for Rx sub-module. */


PUBLIC INT4 LsppRxProcessMplsPdu PROTO ((UINT1 **ppPkt, 
                                         tLsppEchoMsg * pLsppEchoMsg,
                                         UINT4 *pu4Offset));

PUBLIC INT4 LsppRxParseValidatePdu PROTO ((UINT1 **ppPkt, 
                                        tLsppPduInfo * pLsppPduInfo,
                                        UINT4,
                                        UINT4 u4PktSize, UINT4 u4Offset, 
                                        UINT1 *pu1Result));
PUBLIC INT4
LsppRxDecodeValidateLsppHeader PROTO ((tLsppEchoMsg *pLsppEchoMsg, 
                                       UINT1 **ppPkt, 
                                       UINT1 *pu1ValidationResult));

PUBLIC INT4
LsppRxIsTlvSupported PROTO ((UINT4 u4ContextId, UINT2 u2TlvType,
                             UINT2 u2SubTlvType, UINT2 *pu2TlvLength));

PUBLIC VOID
LsppRxCheckTlvsSupported PROTO ((UINT1 **ppPkt, UINT4 u4ContextId,
                          UINT4 u4PktSize, UINT4 u4Offset,
                          UINT1 *pu1Result, tLsppPduInfo * pLsppPduInfo));


PUBLIC VOID LsppRxUpdateRcAndErrorTlv PROTO ((UINT1 **ppPkt, UINT2 u2TlvLength,
                                           tLsppErrorTlv *pErrorTlv,
                                           UINT2* pu2TlvsPresent));

PUBLIC INT4 LsppRxValidateIfLblStkTlv PROTO ((UINT4, tLsppIfLblStkTlv * 
                                              pLsppIfLblStkTlv));

PUBLIC INT4 LsppRxValidatePadTlv PROTO ((UINT4, UINT1 u1ReqType, 
                                         tLsppPadTlv * pLsppPadTlv ));

PUBLIC INT4 LsppRxValidateReplyTosTlv PROTO ((UINT4, tLsppReplyTosTlv * 
                                             pLsppReplyTosTlv));
PUBLIC INT4 LsppRxValidateDsmapTlv PROTO (( UINT4, 
                                            tLsppDsMapTlv * pLsppDsMapTlv, 
                                           UINT1 *pu1ReturnCode));

VOID LsppRxGetMplsLabel PROTO ((UINT1 **ppPkt,  tLsppLblInfo * pLsppLblInfo));

PUBLIC INT4 LsppRxValidateLsppHeader PROTO ((tLsppEchoMsg *pLsppEchoMsg, 
                                             UINT1 *pu1MsgTypeInvalid));
PUBLIC INT4 LsppRxDecodeValidatePdu PROTO ((UINT1 **ppPkt, UINT2 u2TlvType ,
                                            UINT2 u2SubTlvType,                                                             tLsppPduInfo * pLsppPduInfo,
                                            UINT4,
                                            UINT1 u1FecStackDepth,                                                          UINT1 u1MsgType, 
                                            UINT2 *pu2TlvLength,
                                            UINT1 *pu1Result, 
                                            UINT2 *pu2ReadBytes));


PUBLIC VOID LsppRxDecodeIpv4LdpFec PROTO (( UINT4, UINT1 **ppPkt, 
                                            tLsppLdpFecTlv * pLsppLdpFecTlv));

PUBLIC VOID LsppRxDecodeIpv6LdpFec PROTO ((UINT4, UINT1 **ppPkt, 
                                           tLsppLdpFecTlv * 
                                           pLsppLdpFecTlv));


PUBLIC VOID
LsppRxDecodeIpv4RsvpFec PROTO (( UINT4, UINT1 **ppPkt, 
                                 tLsppRsvpTeFecTlv * pLsppRsvpTeFecTlv));

 PUBLIC VOID
LsppRxDecodeIpv6RsvpFec PROTO (( UINT4, UINT1 **ppPkt, 
                                 tLsppRsvpTeFecTlv * pLsppRsvpTeFecTlv ));


 PUBLIC VOID
LsppRxDecodeFec128DeprecatePwFec PROTO ((UINT4, UINT1 **ppPkt,
                  tLsppPwFec128DeprecTlv * pLsppPwFec128DeprecTlv));


    PUBLIC VOID
LsppRxDecodeFec128Pw PROTO (( UINT4, UINT1 **ppPkt,
                      tLsppPwFec128Tlv * pLsppPwFec128Tlv));

PUBLIC VOID
LsppRxDecodeFec129Pw PROTO((UINT4, UINT1 **ppPkt, 
                            tLsppPwFec129Tlv * pLsppPwFec129Tlv,
                    UINT2  *pu2TlvLength));

PUBLIC VOID
LsppRxDecodeNilFec PROTO((UINT4, UINT1 **ppPkt, 
                          tLsppNilFecTlv * pLsppNilFecTlv));

PUBLIC VOID
LsppRxDecodeStaticLspFec PROTO((UINT4, UINT1 **ppPkt, tLsppStaticLspFec * 
                                pLsppStaticLspFec));

PUBLIC VOID
LsppRxDecodeStaticPwFec PROTO(( UINT4, UINT1 **ppPkt, tLsppStaticPwFec * 
                                pLsppStaticPwFec , UINT2 *pu2TlvLength));


PUBLIC VOID
LsppRxDecodePadTlv PROTO((UINT4, UINT1 **ppPkt, tLsppPadTlv * pLsppPadTlv,
                    UINT2 u2TlvLength));


PUBLIC VOID
LsppRxDecodeReplyTosTlv PROTO ((UINT4, UINT1 **ppPkt, tLsppReplyTosTlv * 
                                pLsppReplyTosTlv));


PUBLIC VOID
LsppRxDecodeErrorTlv PROTO ((UINT4, UINT1 **ppPkt,  
                             tLsppErrorTlv * pLsppErrorTlv, 
                             UINT2 u2TlvLength));


PUBLIC VOID
LsppRxDecodeBfdDiscTlv PROTO (( UINT4, UINT1 **ppPkt, tLsppBfdDiscTlv  * 
                                pLsppBfdDiscTlv));


PUBLIC VOID
LsppRxDecodeHeader PROTO ((UINT1 **ppPkt,  tLsppHeader * pLsppHeader));


PUBLIC UINT4
LsppRxDecodeIfLblStkTlv PROTO ((UINT4, UINT1 **ppPkt,tLsppIfLblStkTlv *
                                pLsppIfLblStkTlv));


PUBLIC UINT4
LsppRxDecodeDsmapTlv PROTO((UINT4, UINT1 **ppPkt,  
                            tLsppDsMapTlv * pLsppDsMapTlv, UINT2 *));


PUBLIC INT4
LsppRxMatchEchoSequenceEntry PROTO ((UINT4 u4ContextId, tLsppHeader * 
                            pLsppHeader,
                            tLsppFsLsppPingTraceTableEntry   **ppLsppPingTraceEntry,
                            tLsppFsLsppEchoSequenceTableEntry 
                            **ppLsppEchoSequenceEntry));

PUBLIC INT4
LsppRxValidateLspMepTlv PROTO ((tMplsIpLspMepTlv *pRxLspMepTlv,
                                tLsppTeTnlInfo     *pTeTnlDetail));

PUBLIC INT4
LsppRxValidatePwMepTlv PROTO ((tMplsIpPwMepTlv * pRxIpPwMepTlv,
                                                tLsppPwDetail * pPwDetail));

PUBLIC INT4
LsppRxValidateIccMepTlv PROTO ((tMplsIccMepTlv   *pRxIccMepTlv,
                      tLsppMegDetail   *pMegDetail, 
                      UINT1 u1PathDirection));


PUBLIC INT4
LsppRxValidateMipTlv PROTO ((tMplsMipTlv      *pRxMipTlv,
                             tLsppMegDetail   *pMegDetail));

INT4
LsppRxValidateAchTlvs PROTO ((UINT4 u4ContextId,
                              tLsppAchInfo * pLsppAchInfo, 
                              tLsppPathInfo * pLsppPathInfo));

PUBLIC INT4
LsppRxGetAndValidateIPHeader PROTO ((UINT4 u4ContextId, UINT1 **ppPkt, 
                                     t_IP * pRxLsppIpHeader));

PUBLIC INT4
LsppRxGetAndValidateUdpHeader PROTO ((UINT1 **ppPkt , UINT4 u4ContextId, 
                                      tLsppUdpHeader * pLsppUdpHeader));


VOID
LsppRxGetAchHdr PROTO ((UINT1 **ppPkt, tLsppAchHeader * pAchHeader));


PUBLIC INT4
LsppRxValidateIpUdpHeader PROTO ((UINT1 **ppPkt, tLsppEchoMsg * pLsppEchoMsg));


PUBLIC INT4
LsppRxVerifyCcCvCabability PROTO ((UINT4, UINT1 u1CcReceived,UINT1 u1CcSelected,
                         UINT1 u1CvSelected));

VOID  LsppRxGetAchTunnelMepTlv PROTO ((UINT1 **ppPkt,
                             tMplsIpLspMepTlv *pLsppIpLspMepTlv));


VOID  LsppRxGetAchPwMepTlv PROTO ((UINT1 **ppPkt,
                             tMplsIpPwMepTlv * pIpPwMepTlv, UINT2 *pu2TlvLen));


VOID LsppRxGetAchIccMepTlv PROTO ((UINT1 **ppPkt,
                            tMplsIccMepTlv * pIccMepTlv));


VOID LsppRxGetAchMipTlv PROTO ((UINT1 **ppPkt, tMplsMipTlv * pMplsMipTlv));


UINT4 LsppRxGetAchTlv PROTO ((UINT1 **ppPkt, tLsppAchInfo *pLsppAchInfo ,
                    UINT2 u2AchTlvLength, UINT4));


PUBLIC INT4
LsppRxGetPathIdFromLabel PROTO ((UINT4 u4ContextId, UINT4 u4IfNum, 
                                 UINT4 u4Label, tLsppPathId *pLsppPathId));

PUBLIC VOID
LsppRxGetMplsLabelStack PROTO (( UINT1 **ppPkt, tLsppEchoMsg *pLsppEchoMsg,
                       UINT1 *pu1LabelCount, UINT4 *pu4OuterLabel, 
                       UINT4 *pu4Offset));

VOID
LsppRxUpdateLabelStack PROTO ((tLsppEchoMsg * pLsppEchoMsg, UINT1 * 
pu1LabelStackDepth));

UINT4
LsppRxSendBfdBtInfo(tLsppFsLsppPingTraceTableEntry * pLsppPingTraceEntry,
                    tLsppBfdDiscTlv *pLsppBfdDiscTlv,
                    UINT1 u1ReturnCode);

PUBLIC UINT4
LsppRxIpAddrValidate PROTO ((tLsppEchoMsg *pLsppEchoMsg, UINT1 u1RcvdMsgType));

PUBLIC UINT4
LsppRxFwdPacket PROTO ((tLsppEchoMsg * pLsppEchoMsg));

/* Tx sub-module related prototypes. */

INT4 LsppTxBfdBtStrapReplyPkt PROTO ((UINT4 u4ContextId,
                               tLsppBfdInfo *pLsppBfdInfo));

VOID LsppTxFrameHeader PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, 
                               UINT4 *pu4BufOffset,tLsppHeader *pLsppHeader));

VOID LsppTxFrameLdpIpV4Fec PROTO ((tCRU_BUF_CHAIN_HEADER *pBuf, 
                                   UINT4 *pu4BufOffset, 
                                   tLsppLdpFecTlv *pLsppLdpFecTlv,
                                  UINT4 u4ContextId));

VOID LsppTxFrameLdpIpV6Fec PROTO((tCRU_BUF_CHAIN_HEADER * pBuf,
                                  UINT4 * pu4BufOffset,
                                  tLsppLdpFecTlv * pLsppLdpFecTlv,
                                  UINT4 u4ContextId));

VOID LsppTxFrameRsvpIpV4Fec PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, 
                                    UINT4 * pu4BufOffset,
                                    tLsppRsvpTeFecTlv *pLsppRsvpTeFecTlv,
                                    UINT4 u4ContextId));

VOID LsppTxFrameRsvpIpV6Fec PROTO((tCRU_BUF_CHAIN_HEADER * pBuf,
                                   UINT4 * pu4BufOffset,
                                   tLsppRsvpTeFecTlv *pLsppRsvpTeFecTlv,
                                   UINT4 u4ContextId));

VOID LsppTxFrameFec128DeprecatePwFec PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                                             UINT4 * pu4BufOffset,
                                             tLsppPwFec128DeprecTlv * 
                                             pLsppPwFec128DeprecTlv,
                                             UINT4 u4ContextId));

VOID LsppTxFrameFec128Pw PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                                 UINT4 * pu4BufOffset,
                                 tLsppPwFec128Tlv * pLsppPwFec128Tlv,
                                 UINT4 u4ContextId));


VOID LsppTxFrameFec129Pw PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, 
                                 UINT4 * pu4BufOffset, 
                                 tLsppPwFec129Tlv * pLsppPwFec129Tlv, 
                                 UINT1 u1SkipCount,
                                 UINT4 u4ContextId));

VOID LsppTxFrameNilFec PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, 
                               UINT4 * pu4BufOffset,
                               tLsppNilFecTlv* pLsppNilFecTlv,
                               UINT4 u4ContextId));

VOID LsppTxFrameStaticLspFec PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                        UINT4 * pu4BufOffset,
                        tLsppStaticLspFec * pLsppStaticLspFec,
                        UINT4 u4ContextId));

VOID LsppTxFrameStaticPwFec PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                        UINT4 * pu4BufOffset,
                        tLsppStaticPwFec * pLsppStaticPwFec,
                        UINT1 u1SkipCount,
                        UINT4 u4ContextId));

VOID LsppTxFramePadTlv PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, 
                               UINT4 * pu4BufOffset,
                               tLsppPadTlv * pLsppPadTlv, 
                               UINT2 u2TlvLength,
                               UINT4 u4ContextId));

VOID LsppTxFrameBfdDiscTlv PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, 
                                   UINT4 * pu4BufOffset,
                                   tLsppBfdDiscTlv * pLsppBfdDiscTlv,
                                   UINT4 u4ContextId));

VOID LsppTxFrameReplyTosTlv PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                        UINT4 * pu4BufOffset,
                        tLsppReplyTosTlv * pLsppReplyTosTlv,
                        UINT4 u4ContextId));

VOID LsppTxFrameMplsLabel PROTO ((UINT4 u4Label, UINT1 u1Exp, UINT1 u1SI, 
                                  UINT1 u1Ttl, UINT4 *pu4MplsHdr));

VOID LsppTxFrameIfLblStkTlv PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, 
                                    UINT4 * pu4BufOffset,
                                    tLsppIfLblStkTlv * pLsppIfLblStkTlv,
                                    UINT4 u4ContextId));

VOID LsppTxFrameDsmapTlv PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, 
                                 UINT4 * pu4BufOffset, 
                                 tLsppDsMapTlv * pLsppDsMapTlv,
                                 UINT4 u4ContextId));

INT4 LsppTxFillIpUdpHdr PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                    tLsppEchoMsg * pLsppEchoMsg));

VOID LsppTxFrameLspMepTlv PROTO ((tLsppMepTlv * pLsppMepTlv, 
                                  tLsppAchTlv * pMplsAchTlv));

VOID LsppTxFramePwMepTlv PROTO ((tLsppMepTlv * pLsppMepTlv, 
                                 tLsppAchTlv * pMplsAchTlv));

VOID LsppTxFrameIccMepTlv PROTO ((tLsppMepTlv * pLsppMepTlv,
                                  tLsppAchTlv * pMplsAchTlv));

VOID LsppTxFrameMipTlv PROTO ((tLsppMipTlv * pMplsMipTlv,
                               tLsppAchTlv * pMplsAchTlv));

UINT4 LsppTxEchoPkt PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4ContextId,
             tLsppOutSegInfo  * pOutSegInfo));

UINT4 LsppTxModifyEchoPkt PROTO ((tCRU_BUF_CHAIN_HEADER * pBuf,
                        tLsppEchoMsg * pLsppEchoMsg, UINT4 u4LastSentSeqNum));

UINT4 LsppTxUpdateOutGoingLblStk PROTO ((tLsppEchoMsg * pLsppEchoMsg));

INT4 LsppTxEncapsulatePdu PROTO ((tCRU_BUF_CHAIN_HEADER * pMsg, 
                                          tLsppEchoMsg * pLsppEchoMsg));

INT4 LsppTxFramePingPayload PROTO ((tCRU_BUF_CHAIN_HEADER *pMsg, 
                                    UINT4 *pu4BufOffset,
                                    tLsppPduInfo *pLsppPduInfo,
                                    UINT4));

PUBLIC UINT4 LsppTxSendPacketToBfd PROTO ((tCRU_BUF_CHAIN_HEADER * pMsg,
                       tLsppEchoMsg * pLsppEchoMsg,
                       UINT1 u1ReplyPath));



UINT1 LsppTxGetReplyPath PROTO ((tLsppEchoMsg * pLsppEchoMsg,
                                 UINT1 *pu1DscpValue,BOOL1 *pbRouterAlertOpt));

UINT4 LsppTxFrameAndSendPdu PROTO ((tLsppEchoMsg * pLsppEchoMsg,
                          tLsppFsLsppPingTraceTableEntry * pLsppPingTraceEntry,
                          tLsppFsLsppEchoSequenceTableEntry *,
                          tCRU_BUF_CHAIN_HEADER *pMsg));



/***************************** lspptest.c ********************************/

INT4
LsppUtPortHdlExtInteraction (tLsppExtInParams * pLsppExtInParams, 
                             tLsppExtOutParams * pLsppExtOutParams);

PUBLIC VOID LsppTrcCruBufDump PROTO ((UINT4 u4ContextId, 
                                      tCRU_BUF_CHAIN_HEADER * pMsg));

PUBLIC VOID
LsppTrcDump PROTO ((UINT1 *pu1Buf, UINT4 i4Len));

PUBLIC UINT4
LsppTrcGetTraceLevel PROTO ((UINT4 u4ContextId , INT4 *pi4TraceLevel));

INT4 LsppUtilGetGlobalNodeId PROTO ((tLsppFsLsppPingTraceTableEntry 
                                     *pLsppFsLsppPingTraceTableEntry,
                                   tLsppExtOutParams *pLsppExtOutParams));

#endif   /* __LSPPPROT_H__ */
/*-----------------------------------------------------------------------*/
/*                       End of the file  lsppprot.h                     */
/*-----------------------------------------------------------------------*/
