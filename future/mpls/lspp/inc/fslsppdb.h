/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: fslsppdb.h,v 1.4 2011/05/17 13:07:22 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSLSPPDB_H
#define _FSLSPPDB_H

UINT1 FsLsppGlobalConfigTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsLsppGlobalStatsTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsLsppPingTraceTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsLsppEchoSequenceTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsLsppHopTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fslspp [] ={1,3,6,1,4,1,2076,13,11};
tSNMP_OID_TYPE fslsppOID = {9, fslspp};


UINT4 FsLsppContextId [ ] ={1,3,6,1,4,1,2076,13,11,1,1,1,1};
UINT4 FsLsppSystemControl [ ] ={1,3,6,1,4,1,2076,13,11,1,1,1,2};
UINT4 FsLsppTrapStatus [ ] ={1,3,6,1,4,1,2076,13,11,1,1,1,3};
UINT4 FsLsppTraceLevel [ ] ={1,3,6,1,4,1,2076,13,11,1,1,1,4};
UINT4 FsLsppAgeOutTime [ ] ={1,3,6,1,4,1,2076,13,11,1,1,1,5};
UINT4 FsLsppAgeOutTmrUnit [ ] ={1,3,6,1,4,1,2076,13,11,1,1,1,6};
UINT4 FsLsppClearEchoStats [ ] ={1,3,6,1,4,1,2076,13,11,1,1,1,7};
UINT4 FsLsppBfdBtStrapRespReq [ ] ={1,3,6,1,4,1,2076,13,11,1,1,1,8};
UINT4 FsLsppBfdBtStrapAgeOutTime [ ] ={1,3,6,1,4,1,2076,13,11,1,1,1,9};
UINT4 FsLsppBfdBtStrapAgeOutTmrUnit [ ] ={1,3,6,1,4,1,2076,13,11,1,1,1,10};
UINT4 FsLsppGlbStatReqTx [ ] ={1,3,6,1,4,1,2076,13,11,1,2,1,1};
UINT4 FsLsppGlbStatReqRx [ ] ={1,3,6,1,4,1,2076,13,11,1,2,1,2};
UINT4 FsLsppGlbStatReqTimedOut [ ] ={1,3,6,1,4,1,2076,13,11,1,2,1,3};
UINT4 FsLsppGlbStatReqUnSent [ ] ={1,3,6,1,4,1,2076,13,11,1,2,1,4};
UINT4 FsLsppGlbStatReplyTx [ ] ={1,3,6,1,4,1,2076,13,11,1,2,1,5};
UINT4 FsLsppGlbStatReplyRx [ ] ={1,3,6,1,4,1,2076,13,11,1,2,1,6};
UINT4 FsLsppGlbStatReplyDropped [ ] ={1,3,6,1,4,1,2076,13,11,1,2,1,7};
UINT4 FsLsppGlbStatReplyUnSent [ ] ={1,3,6,1,4,1,2076,13,11,1,2,1,8};
UINT4 FsLsppGlbStatReplyFromEgr [ ] ={1,3,6,1,4,1,2076,13,11,1,2,1,9};
UINT4 FsLsppGlbStatUnLbldOutIf [ ] ={1,3,6,1,4,1,2076,13,11,1,2,1,10};
UINT4 FsLsppGlbStatDsMapMismatch [ ] ={1,3,6,1,4,1,2076,13,11,1,2,1,11};
UINT4 FsLsppGlbStatFecLblMismatch [ ] ={1,3,6,1,4,1,2076,13,11,1,2,1,12};
UINT4 FsLsppGlbStatNoFecMapping [ ] ={1,3,6,1,4,1,2076,13,11,1,2,1,13};
UINT4 FsLsppGlbStatUnKUpstreamIf [ ] ={1,3,6,1,4,1,2076,13,11,1,2,1,14};
UINT4 FsLsppGlbStatReqLblSwitched [ ] ={1,3,6,1,4,1,2076,13,11,1,2,1,15};
UINT4 FsLsppGlbStatReqUnSupptdTlv [ ] ={1,3,6,1,4,1,2076,13,11,1,2,1,16};
UINT4 FsLsppGlbStatMalformedReq [ ] ={1,3,6,1,4,1,2076,13,11,1,2,1,17};
UINT4 FsLsppGlbStatNoLblEntry [ ] ={1,3,6,1,4,1,2076,13,11,1,2,1,18};
UINT4 FsLsppGlbStatPreTermReq [ ] ={1,3,6,1,4,1,2076,13,11,1,2,1,19};
UINT4 FsLsppGlbStatProtMismatch [ ] ={1,3,6,1,4,1,2076,13,11,1,2,1,20};
UINT4 FsLsppGlbStatRsvdRetCode [ ] ={1,3,6,1,4,1,2076,13,11,1,2,1,21};
UINT4 FsLsppGlbStatNoRetCode [ ] ={1,3,6,1,4,1,2076,13,11,1,2,1,22};
UINT4 FsLsppGlbStatUndefRetCode [ ] ={1,3,6,1,4,1,2076,13,11,1,2,1,23};
UINT4 FsLsppGlbStatInvalidPktDropped [ ] ={1,3,6,1,4,1,2076,13,11,1,2,1,24};
UINT4 FsLsppSenderHandle [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,1};
UINT4 FsLsppRequestType [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,2};
UINT4 FsLsppRequestOwner [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,3};
UINT4 FsLsppPathType [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,4};
UINT4 FsLsppPathPointer [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,5};
UINT4 FsLsppTgtMipGlobalId [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,6};
UINT4 FsLsppTgtMipNodeId [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,7};
UINT4 FsLsppTgtMipIfNum [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,8};
UINT4 FsLsppReplyMode [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,9};
UINT4 FsLsppRepeatCount [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,10};
UINT4 FsLsppPacketSize [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,11};
UINT4 FsLsppPadPattern [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,12};
UINT4 FsLsppTTLValue [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,13};
UINT4 FsLsppWFRInterval [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,14};
UINT4 FsLsppWFRTmrUnit [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,15};
UINT4 FsLsppWTSInterval [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,16};
UINT4 FsLsppWTSTmrUnit [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,17};
UINT4 FsLsppReplyDscpValue [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,18};
UINT4 FsLsppSweepOption [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,19};
UINT4 FsLsppSweepMinimum [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,20};
UINT4 FsLsppSweepMaximum [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,21};
UINT4 FsLsppSweepIncrement [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,22};
UINT4 FsLsppBurstOption [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,23};
UINT4 FsLsppBurstSize [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,24};
UINT4 FsLsppEXPValue [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,25};
UINT4 FsLsppDsMap [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,26};
UINT4 FsLsppFecValidate [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,27};
UINT4 FsLsppReplyPadTlv [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,28};
UINT4 FsLsppForceExplicitNull [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,29};
UINT4 FsLsppInterfaceLabelTlv [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,30};
UINT4 FsLsppSameSeqNumOption [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,31};
UINT4 FsLsppVerbose [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,32};
UINT4 FsLsppReversePathVerify [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,33};
UINT4 FsLsppEncapType [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,34};
UINT4 FsLsppStatus [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,35};
UINT4 FsLsppActualHopCount [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,36};
UINT4 FsLsppResponderAddrType [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,37};
UINT4 FsLsppResponderAddr [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,38};
UINT4 FsLsppResponderGlobalId [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,39};
UINT4 FsLsppResponderId [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,40};
UINT4 FsLsppMaxRtt [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,41};
UINT4 FsLsppMinRtt [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,42};
UINT4 FsLsppAverageRtt [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,43};
UINT4 FsLsppPktsTx [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,44};
UINT4 FsLsppPktsRx [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,45};
UINT4 FsLsppPktsUnSent [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,46};
UINT4 FsLsppRowStatus [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,47};
UINT4 FsLsppStatusPathDirection [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,48};
UINT4 FsLsppResponderIcc [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,49};
UINT4 FsLsppResponderUMC [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,50};
UINT4 FsLsppResponderMepIndex [ ] ={1,3,6,1,4,1,2076,13,11,2,1,1,51};
UINT4 FsLsppSequenceNumber [ ] ={1,3,6,1,4,1,2076,13,11,2,2,1,1};
UINT4 FsLsppReturnCode [ ] ={1,3,6,1,4,1,2076,13,11,2,2,1,2};
UINT4 FsLsppReturnSubCode [ ] ={1,3,6,1,4,1,2076,13,11,2,2,1,3};
UINT4 FsLsppReturnCodeStr [ ] ={1,3,6,1,4,1,2076,13,11,2,2,1,4};
UINT4 FsLsppHopIndex [ ] ={1,3,6,1,4,1,2076,13,11,2,3,1,1};
UINT4 FsLsppHopAddrType [ ] ={1,3,6,1,4,1,2076,13,11,2,3,1,2};
UINT4 FsLsppHopAddr [ ] ={1,3,6,1,4,1,2076,13,11,2,3,1,3};
UINT4 FsLsppHopGlobalId [ ] ={1,3,6,1,4,1,2076,13,11,2,3,1,4};
UINT4 FsLsppHopId [ ] ={1,3,6,1,4,1,2076,13,11,2,3,1,5};
UINT4 FsLsppHopIfNum [ ] ={1,3,6,1,4,1,2076,13,11,2,3,1,6};
UINT4 FsLsppHopReturnCode [ ] ={1,3,6,1,4,1,2076,13,11,2,3,1,7};
UINT4 FsLsppHopReturnSubCode [ ] ={1,3,6,1,4,1,2076,13,11,2,3,1,8};
UINT4 FsLsppHopReturnCodeStr [ ] ={1,3,6,1,4,1,2076,13,11,2,3,1,9};
UINT4 FsLsppHopRxAddrType [ ] ={1,3,6,1,4,1,2076,13,11,2,3,1,10};
UINT4 FsLsppHopRxIPAddr [ ] ={1,3,6,1,4,1,2076,13,11,2,3,1,11};
UINT4 FsLsppHopRxIfAddr [ ] ={1,3,6,1,4,1,2076,13,11,2,3,1,12};
UINT4 FsLsppHopRxIfNum [ ] ={1,3,6,1,4,1,2076,13,11,2,3,1,13};
UINT4 FsLsppHopRxLabelStack [ ] ={1,3,6,1,4,1,2076,13,11,2,3,1,14};
UINT4 FsLsppHopRxLabelExp [ ] ={1,3,6,1,4,1,2076,13,11,2,3,1,15};
UINT4 FsLsppHopRtt [ ] ={1,3,6,1,4,1,2076,13,11,2,3,1,16};
UINT4 FsLsppHopDsMtu [ ] ={1,3,6,1,4,1,2076,13,11,2,3,1,17};
UINT4 FsLsppHopDsAddrType [ ] ={1,3,6,1,4,1,2076,13,11,2,3,1,18};
UINT4 FsLsppHopDsIPAddr [ ] ={1,3,6,1,4,1,2076,13,11,2,3,1,19};
UINT4 FsLsppHopDsIfAddr [ ] ={1,3,6,1,4,1,2076,13,11,2,3,1,20};
UINT4 FsLsppHopDsIfNum [ ] ={1,3,6,1,4,1,2076,13,11,2,3,1,21};
UINT4 FsLsppHopDsLabelStack [ ] ={1,3,6,1,4,1,2076,13,11,2,3,1,22};
UINT4 FsLsppHopDsLabelExp [ ] ={1,3,6,1,4,1,2076,13,11,2,3,1,23};
UINT4 FsLsppHopIcc [ ] ={1,3,6,1,4,1,2076,13,11,2,3,1,24};
UINT4 FsLsppHopUMC [ ] ={1,3,6,1,4,1,2076,13,11,2,3,1,25};
UINT4 FsLsppHopMepIndex [ ] ={1,3,6,1,4,1,2076,13,11,2,3,1,26};




tMbDbEntry fslsppMibEntry[]= {

{{13,FsLsppContextId}, GetNextIndexFsLsppGlobalConfigTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsLsppGlobalConfigTableINDEX, 1, 0, 0, NULL},

{{13,FsLsppSystemControl}, GetNextIndexFsLsppGlobalConfigTable, FsLsppSystemControlGet, FsLsppSystemControlSet, FsLsppSystemControlTest, FsLsppGlobalConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLsppGlobalConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsLsppTrapStatus}, GetNextIndexFsLsppGlobalConfigTable, FsLsppTrapStatusGet, FsLsppTrapStatusSet, FsLsppTrapStatusTest, FsLsppGlobalConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsLsppGlobalConfigTableINDEX, 1, 0, 0, "0"},

{{13,FsLsppTraceLevel}, GetNextIndexFsLsppGlobalConfigTable, FsLsppTraceLevelGet, FsLsppTraceLevelSet, FsLsppTraceLevelTest, FsLsppGlobalConfigTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsLsppGlobalConfigTableINDEX, 1, 0, 0, "0"},

{{13,FsLsppAgeOutTime}, GetNextIndexFsLsppGlobalConfigTable, FsLsppAgeOutTimeGet, FsLsppAgeOutTimeSet, FsLsppAgeOutTimeTest, FsLsppGlobalConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsLsppGlobalConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsLsppAgeOutTmrUnit}, GetNextIndexFsLsppGlobalConfigTable, FsLsppAgeOutTmrUnitGet, FsLsppAgeOutTmrUnitSet, FsLsppAgeOutTmrUnitTest, FsLsppGlobalConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLsppGlobalConfigTableINDEX, 1, 0, 0, "3"},

{{13,FsLsppClearEchoStats}, GetNextIndexFsLsppGlobalConfigTable, FsLsppClearEchoStatsGet, FsLsppClearEchoStatsSet, FsLsppClearEchoStatsTest, FsLsppGlobalConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLsppGlobalConfigTableINDEX, 1, 0, 0, "2"},

{{13,FsLsppBfdBtStrapRespReq}, GetNextIndexFsLsppGlobalConfigTable, FsLsppBfdBtStrapRespReqGet, FsLsppBfdBtStrapRespReqSet, FsLsppBfdBtStrapRespReqTest, FsLsppGlobalConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLsppGlobalConfigTableINDEX, 1, 0, 0, "1"},

{{13,FsLsppBfdBtStrapAgeOutTime}, GetNextIndexFsLsppGlobalConfigTable, FsLsppBfdBtStrapAgeOutTimeGet, FsLsppBfdBtStrapAgeOutTimeSet, FsLsppBfdBtStrapAgeOutTimeTest, FsLsppGlobalConfigTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsLsppGlobalConfigTableINDEX, 1, 0, 0, "0"},

{{13,FsLsppBfdBtStrapAgeOutTmrUnit}, GetNextIndexFsLsppGlobalConfigTable, FsLsppBfdBtStrapAgeOutTmrUnitGet, FsLsppBfdBtStrapAgeOutTmrUnitSet, FsLsppBfdBtStrapAgeOutTmrUnitTest, FsLsppGlobalConfigTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLsppGlobalConfigTableINDEX, 1, 0, 0, "3"},

{{13,FsLsppGlbStatReqTx}, GetNextIndexFsLsppGlobalStatsTable, FsLsppGlbStatReqTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLsppGlobalStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsLsppGlbStatReqRx}, GetNextIndexFsLsppGlobalStatsTable, FsLsppGlbStatReqRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLsppGlobalStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsLsppGlbStatReqTimedOut}, GetNextIndexFsLsppGlobalStatsTable, FsLsppGlbStatReqTimedOutGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLsppGlobalStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsLsppGlbStatReqUnSent}, GetNextIndexFsLsppGlobalStatsTable, FsLsppGlbStatReqUnSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLsppGlobalStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsLsppGlbStatReplyTx}, GetNextIndexFsLsppGlobalStatsTable, FsLsppGlbStatReplyTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLsppGlobalStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsLsppGlbStatReplyRx}, GetNextIndexFsLsppGlobalStatsTable, FsLsppGlbStatReplyRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLsppGlobalStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsLsppGlbStatReplyDropped}, GetNextIndexFsLsppGlobalStatsTable, FsLsppGlbStatReplyDroppedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLsppGlobalStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsLsppGlbStatReplyUnSent}, GetNextIndexFsLsppGlobalStatsTable, FsLsppGlbStatReplyUnSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLsppGlobalStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsLsppGlbStatReplyFromEgr}, GetNextIndexFsLsppGlobalStatsTable, FsLsppGlbStatReplyFromEgrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLsppGlobalStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsLsppGlbStatUnLbldOutIf}, GetNextIndexFsLsppGlobalStatsTable, FsLsppGlbStatUnLbldOutIfGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLsppGlobalStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsLsppGlbStatDsMapMismatch}, GetNextIndexFsLsppGlobalStatsTable, FsLsppGlbStatDsMapMismatchGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLsppGlobalStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsLsppGlbStatFecLblMismatch}, GetNextIndexFsLsppGlobalStatsTable, FsLsppGlbStatFecLblMismatchGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLsppGlobalStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsLsppGlbStatNoFecMapping}, GetNextIndexFsLsppGlobalStatsTable, FsLsppGlbStatNoFecMappingGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLsppGlobalStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsLsppGlbStatUnKUpstreamIf}, GetNextIndexFsLsppGlobalStatsTable, FsLsppGlbStatUnKUpstreamIfGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLsppGlobalStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsLsppGlbStatReqLblSwitched}, GetNextIndexFsLsppGlobalStatsTable, FsLsppGlbStatReqLblSwitchedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLsppGlobalStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsLsppGlbStatReqUnSupptdTlv}, GetNextIndexFsLsppGlobalStatsTable, FsLsppGlbStatReqUnSupptdTlvGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLsppGlobalStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsLsppGlbStatMalformedReq}, GetNextIndexFsLsppGlobalStatsTable, FsLsppGlbStatMalformedReqGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLsppGlobalStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsLsppGlbStatNoLblEntry}, GetNextIndexFsLsppGlobalStatsTable, FsLsppGlbStatNoLblEntryGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLsppGlobalStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsLsppGlbStatPreTermReq}, GetNextIndexFsLsppGlobalStatsTable, FsLsppGlbStatPreTermReqGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLsppGlobalStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsLsppGlbStatProtMismatch}, GetNextIndexFsLsppGlobalStatsTable, FsLsppGlbStatProtMismatchGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLsppGlobalStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsLsppGlbStatRsvdRetCode}, GetNextIndexFsLsppGlobalStatsTable, FsLsppGlbStatRsvdRetCodeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLsppGlobalStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsLsppGlbStatNoRetCode}, GetNextIndexFsLsppGlobalStatsTable, FsLsppGlbStatNoRetCodeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLsppGlobalStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsLsppGlbStatUndefRetCode}, GetNextIndexFsLsppGlobalStatsTable, FsLsppGlbStatUndefRetCodeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLsppGlobalStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsLsppGlbStatInvalidPktDropped}, GetNextIndexFsLsppGlobalStatsTable, FsLsppGlbStatInvalidPktDroppedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLsppGlobalStatsTableINDEX, 1, 0, 0, NULL},

{{13,FsLsppSenderHandle}, GetNextIndexFsLsppPingTraceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsLsppPingTraceTableINDEX, 2, 0, 0, NULL},

{{13,FsLsppRequestType}, GetNextIndexFsLsppPingTraceTable, FsLsppRequestTypeGet, FsLsppRequestTypeSet, FsLsppRequestTypeTest, FsLsppPingTraceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLsppPingTraceTableINDEX, 2, 0, 0, NULL},

{{13,FsLsppRequestOwner}, GetNextIndexFsLsppPingTraceTable, FsLsppRequestOwnerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsLsppPingTraceTableINDEX, 2, 0, 0, NULL},

{{13,FsLsppPathType}, GetNextIndexFsLsppPingTraceTable, FsLsppPathTypeGet, FsLsppPathTypeSet, FsLsppPathTypeTest, FsLsppPingTraceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLsppPingTraceTableINDEX, 2, 0, 0, NULL},

{{13,FsLsppPathPointer}, GetNextIndexFsLsppPingTraceTable, FsLsppPathPointerGet, FsLsppPathPointerSet, FsLsppPathPointerTest, FsLsppPingTraceTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, FsLsppPingTraceTableINDEX, 2, 0, 0, NULL},

{{13,FsLsppTgtMipGlobalId}, GetNextIndexFsLsppPingTraceTable, FsLsppTgtMipGlobalIdGet, FsLsppTgtMipGlobalIdSet, FsLsppTgtMipGlobalIdTest, FsLsppPingTraceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsLsppPingTraceTableINDEX, 2, 0, 0, NULL},

{{13,FsLsppTgtMipNodeId}, GetNextIndexFsLsppPingTraceTable, FsLsppTgtMipNodeIdGet, FsLsppTgtMipNodeIdSet, FsLsppTgtMipNodeIdTest, FsLsppPingTraceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsLsppPingTraceTableINDEX, 2, 0, 0, NULL},

{{13,FsLsppTgtMipIfNum}, GetNextIndexFsLsppPingTraceTable, FsLsppTgtMipIfNumGet, FsLsppTgtMipIfNumSet, FsLsppTgtMipIfNumTest, FsLsppPingTraceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsLsppPingTraceTableINDEX, 2, 0, 0, NULL},

{{13,FsLsppReplyMode}, GetNextIndexFsLsppPingTraceTable, FsLsppReplyModeGet, FsLsppReplyModeSet, FsLsppReplyModeTest, FsLsppPingTraceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLsppPingTraceTableINDEX, 2, 0, 0, NULL},

{{13,FsLsppRepeatCount}, GetNextIndexFsLsppPingTraceTable, FsLsppRepeatCountGet, FsLsppRepeatCountSet, FsLsppRepeatCountTest, FsLsppPingTraceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsLsppPingTraceTableINDEX, 2, 0, 0, "5"},

{{13,FsLsppPacketSize}, GetNextIndexFsLsppPingTraceTable, FsLsppPacketSizeGet, FsLsppPacketSizeSet, FsLsppPacketSizeTest, FsLsppPingTraceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsLsppPingTraceTableINDEX, 2, 0, 0, "200"},

{{13,FsLsppPadPattern}, GetNextIndexFsLsppPingTraceTable, FsLsppPadPatternGet, FsLsppPadPatternSet, FsLsppPadPatternTest, FsLsppPingTraceTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsLsppPingTraceTableINDEX, 2, 0, 0, "43981"},

{{13,FsLsppTTLValue}, GetNextIndexFsLsppPingTraceTable, FsLsppTTLValueGet, FsLsppTTLValueSet, FsLsppTTLValueTest, FsLsppPingTraceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsLsppPingTraceTableINDEX, 2, 0, 0, NULL},

{{13,FsLsppWFRInterval}, GetNextIndexFsLsppPingTraceTable, FsLsppWFRIntervalGet, FsLsppWFRIntervalSet, FsLsppWFRIntervalTest, FsLsppPingTraceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsLsppPingTraceTableINDEX, 2, 0, 0, "2"},

{{13,FsLsppWFRTmrUnit}, GetNextIndexFsLsppPingTraceTable, FsLsppWFRTmrUnitGet, FsLsppWFRTmrUnitSet, FsLsppWFRTmrUnitTest, FsLsppPingTraceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLsppPingTraceTableINDEX, 2, 0, 0, "2"},

{{13,FsLsppWTSInterval}, GetNextIndexFsLsppPingTraceTable, FsLsppWTSIntervalGet, FsLsppWTSIntervalSet, FsLsppWTSIntervalTest, FsLsppPingTraceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsLsppPingTraceTableINDEX, 2, 0, 0, "1"},

{{13,FsLsppWTSTmrUnit}, GetNextIndexFsLsppPingTraceTable, FsLsppWTSTmrUnitGet, FsLsppWTSTmrUnitSet, FsLsppWTSTmrUnitTest, FsLsppPingTraceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLsppPingTraceTableINDEX, 2, 0, 0, "2"},

{{13,FsLsppReplyDscpValue}, GetNextIndexFsLsppPingTraceTable, FsLsppReplyDscpValueGet, FsLsppReplyDscpValueSet, FsLsppReplyDscpValueTest, FsLsppPingTraceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsLsppPingTraceTableINDEX, 2, 0, 0, "0"},

{{13,FsLsppSweepOption}, GetNextIndexFsLsppPingTraceTable, FsLsppSweepOptionGet, FsLsppSweepOptionSet, FsLsppSweepOptionTest, FsLsppPingTraceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLsppPingTraceTableINDEX, 2, 0, 0, "2"},

{{13,FsLsppSweepMinimum}, GetNextIndexFsLsppPingTraceTable, FsLsppSweepMinimumGet, FsLsppSweepMinimumSet, FsLsppSweepMinimumTest, FsLsppPingTraceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsLsppPingTraceTableINDEX, 2, 0, 0, "100"},

{{13,FsLsppSweepMaximum}, GetNextIndexFsLsppPingTraceTable, FsLsppSweepMaximumGet, FsLsppSweepMaximumSet, FsLsppSweepMaximumTest, FsLsppPingTraceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsLsppPingTraceTableINDEX, 2, 0, 0, "17986"},

{{13,FsLsppSweepIncrement}, GetNextIndexFsLsppPingTraceTable, FsLsppSweepIncrementGet, FsLsppSweepIncrementSet, FsLsppSweepIncrementTest, FsLsppPingTraceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsLsppPingTraceTableINDEX, 2, 0, 0, "100"},

{{13,FsLsppBurstOption}, GetNextIndexFsLsppPingTraceTable, FsLsppBurstOptionGet, FsLsppBurstOptionSet, FsLsppBurstOptionTest, FsLsppPingTraceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLsppPingTraceTableINDEX, 2, 0, 0, "2"},

{{13,FsLsppBurstSize}, GetNextIndexFsLsppPingTraceTable, FsLsppBurstSizeGet, FsLsppBurstSizeSet, FsLsppBurstSizeTest, FsLsppPingTraceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsLsppPingTraceTableINDEX, 2, 0, 0, "3"},

{{13,FsLsppEXPValue}, GetNextIndexFsLsppPingTraceTable, FsLsppEXPValueGet, FsLsppEXPValueSet, FsLsppEXPValueTest, FsLsppPingTraceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsLsppPingTraceTableINDEX, 2, 0, 0, "0"},

{{13,FsLsppDsMap}, GetNextIndexFsLsppPingTraceTable, FsLsppDsMapGet, FsLsppDsMapSet, FsLsppDsMapTest, FsLsppPingTraceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLsppPingTraceTableINDEX, 2, 0, 0, "2"},

{{13,FsLsppFecValidate}, GetNextIndexFsLsppPingTraceTable, FsLsppFecValidateGet, FsLsppFecValidateSet, FsLsppFecValidateTest, FsLsppPingTraceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLsppPingTraceTableINDEX, 2, 0, 0, "1"},

{{13,FsLsppReplyPadTlv}, GetNextIndexFsLsppPingTraceTable, FsLsppReplyPadTlvGet, FsLsppReplyPadTlvSet, FsLsppReplyPadTlvTest, FsLsppPingTraceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLsppPingTraceTableINDEX, 2, 0, 0, "2"},

{{13,FsLsppForceExplicitNull}, GetNextIndexFsLsppPingTraceTable, FsLsppForceExplicitNullGet, FsLsppForceExplicitNullSet, FsLsppForceExplicitNullTest, FsLsppPingTraceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLsppPingTraceTableINDEX, 2, 0, 0, "2"},

{{13,FsLsppInterfaceLabelTlv}, GetNextIndexFsLsppPingTraceTable, FsLsppInterfaceLabelTlvGet, FsLsppInterfaceLabelTlvSet, FsLsppInterfaceLabelTlvTest, FsLsppPingTraceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLsppPingTraceTableINDEX, 2, 0, 0, "2"},

{{13,FsLsppSameSeqNumOption}, GetNextIndexFsLsppPingTraceTable, FsLsppSameSeqNumOptionGet, FsLsppSameSeqNumOptionSet, FsLsppSameSeqNumOptionTest, FsLsppPingTraceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLsppPingTraceTableINDEX, 2, 0, 0, "2"},

{{13,FsLsppVerbose}, GetNextIndexFsLsppPingTraceTable, FsLsppVerboseGet, FsLsppVerboseSet, FsLsppVerboseTest, FsLsppPingTraceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLsppPingTraceTableINDEX, 2, 0, 0, "2"},

{{13,FsLsppReversePathVerify}, GetNextIndexFsLsppPingTraceTable, FsLsppReversePathVerifyGet, FsLsppReversePathVerifySet, FsLsppReversePathVerifyTest, FsLsppPingTraceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLsppPingTraceTableINDEX, 2, 0, 0, "2"},

{{13,FsLsppEncapType}, GetNextIndexFsLsppPingTraceTable, FsLsppEncapTypeGet, FsLsppEncapTypeSet, FsLsppEncapTypeTest, FsLsppPingTraceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLsppPingTraceTableINDEX, 2, 0, 0, NULL},

{{13,FsLsppStatus}, GetNextIndexFsLsppPingTraceTable, FsLsppStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsLsppPingTraceTableINDEX, 2, 0, 0, NULL},

{{13,FsLsppActualHopCount}, GetNextIndexFsLsppPingTraceTable, FsLsppActualHopCountGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsLsppPingTraceTableINDEX, 2, 0, 0, NULL},

{{13,FsLsppResponderAddrType}, GetNextIndexFsLsppPingTraceTable, FsLsppResponderAddrTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsLsppPingTraceTableINDEX, 2, 0, 0, NULL},

{{13,FsLsppResponderAddr}, GetNextIndexFsLsppPingTraceTable, FsLsppResponderAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsLsppPingTraceTableINDEX, 2, 0, 0, NULL},

{{13,FsLsppResponderGlobalId}, GetNextIndexFsLsppPingTraceTable, FsLsppResponderGlobalIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsLsppPingTraceTableINDEX, 2, 0, 0, NULL},

{{13,FsLsppResponderId}, GetNextIndexFsLsppPingTraceTable, FsLsppResponderIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsLsppPingTraceTableINDEX, 2, 0, 0, NULL},

{{13,FsLsppMaxRtt}, GetNextIndexFsLsppPingTraceTable, FsLsppMaxRttGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsLsppPingTraceTableINDEX, 2, 0, 0, NULL},

{{13,FsLsppMinRtt}, GetNextIndexFsLsppPingTraceTable, FsLsppMinRttGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsLsppPingTraceTableINDEX, 2, 0, 0, NULL},

{{13,FsLsppAverageRtt}, GetNextIndexFsLsppPingTraceTable, FsLsppAverageRttGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsLsppPingTraceTableINDEX, 2, 0, 0, NULL},

{{13,FsLsppPktsTx}, GetNextIndexFsLsppPingTraceTable, FsLsppPktsTxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLsppPingTraceTableINDEX, 2, 0, 0, NULL},

{{13,FsLsppPktsRx}, GetNextIndexFsLsppPingTraceTable, FsLsppPktsRxGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLsppPingTraceTableINDEX, 2, 0, 0, NULL},

{{13,FsLsppPktsUnSent}, GetNextIndexFsLsppPingTraceTable, FsLsppPktsUnSentGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, FsLsppPingTraceTableINDEX, 2, 0, 0, NULL},

{{13,FsLsppRowStatus}, GetNextIndexFsLsppPingTraceTable, FsLsppRowStatusGet, FsLsppRowStatusSet, FsLsppRowStatusTest, FsLsppPingTraceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsLsppPingTraceTableINDEX, 2, 0, 1, NULL},

{{13,FsLsppStatusPathDirection}, GetNextIndexFsLsppPingTraceTable, FsLsppStatusPathDirectionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsLsppPingTraceTableINDEX, 2, 0, 0, "1"},

{{13,FsLsppResponderIcc}, GetNextIndexFsLsppPingTraceTable, FsLsppResponderIccGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsLsppPingTraceTableINDEX, 2, 0, 0, NULL},

{{13,FsLsppResponderUMC}, GetNextIndexFsLsppPingTraceTable, FsLsppResponderUMCGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsLsppPingTraceTableINDEX, 2, 0, 0, NULL},

{{13,FsLsppResponderMepIndex}, GetNextIndexFsLsppPingTraceTable, FsLsppResponderMepIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsLsppPingTraceTableINDEX, 2, 0, 0, NULL},

{{13,FsLsppSequenceNumber}, GetNextIndexFsLsppEchoSequenceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsLsppEchoSequenceTableINDEX, 3, 0, 0, NULL},

{{13,FsLsppReturnCode}, GetNextIndexFsLsppEchoSequenceTable, FsLsppReturnCodeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsLsppEchoSequenceTableINDEX, 3, 0, 0, NULL},

{{13,FsLsppReturnSubCode}, GetNextIndexFsLsppEchoSequenceTable, FsLsppReturnSubCodeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsLsppEchoSequenceTableINDEX, 3, 0, 0, NULL},

{{13,FsLsppReturnCodeStr}, GetNextIndexFsLsppEchoSequenceTable, FsLsppReturnCodeStrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsLsppEchoSequenceTableINDEX, 3, 0, 0, NULL},

{{13,FsLsppHopIndex}, GetNextIndexFsLsppHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsLsppHopTableINDEX, 3, 0, 0, NULL},

{{13,FsLsppHopAddrType}, GetNextIndexFsLsppHopTable, FsLsppHopAddrTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsLsppHopTableINDEX, 3, 0, 0, NULL},

{{13,FsLsppHopAddr}, GetNextIndexFsLsppHopTable, FsLsppHopAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsLsppHopTableINDEX, 3, 0, 0, NULL},

{{13,FsLsppHopGlobalId}, GetNextIndexFsLsppHopTable, FsLsppHopGlobalIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsLsppHopTableINDEX, 3, 0, 0, NULL},

{{13,FsLsppHopId}, GetNextIndexFsLsppHopTable, FsLsppHopIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsLsppHopTableINDEX, 3, 0, 0, NULL},

{{13,FsLsppHopIfNum}, GetNextIndexFsLsppHopTable, FsLsppHopIfNumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsLsppHopTableINDEX, 3, 0, 0, NULL},

{{13,FsLsppHopReturnCode}, GetNextIndexFsLsppHopTable, FsLsppHopReturnCodeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsLsppHopTableINDEX, 3, 0, 0, NULL},

{{13,FsLsppHopReturnSubCode}, GetNextIndexFsLsppHopTable, FsLsppHopReturnSubCodeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsLsppHopTableINDEX, 3, 0, 0, NULL},

{{13,FsLsppHopReturnCodeStr}, GetNextIndexFsLsppHopTable, FsLsppHopReturnCodeStrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsLsppHopTableINDEX, 3, 0, 0, NULL},

{{13,FsLsppHopRxAddrType}, GetNextIndexFsLsppHopTable, FsLsppHopRxAddrTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsLsppHopTableINDEX, 3, 0, 0, NULL},

{{13,FsLsppHopRxIPAddr}, GetNextIndexFsLsppHopTable, FsLsppHopRxIPAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsLsppHopTableINDEX, 3, 0, 0, NULL},

{{13,FsLsppHopRxIfAddr}, GetNextIndexFsLsppHopTable, FsLsppHopRxIfAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsLsppHopTableINDEX, 3, 0, 0, NULL},

{{13,FsLsppHopRxIfNum}, GetNextIndexFsLsppHopTable, FsLsppHopRxIfNumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsLsppHopTableINDEX, 3, 0, 0, NULL},

{{13,FsLsppHopRxLabelStack}, GetNextIndexFsLsppHopTable, FsLsppHopRxLabelStackGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsLsppHopTableINDEX, 3, 0, 0, NULL},

{{13,FsLsppHopRxLabelExp}, GetNextIndexFsLsppHopTable, FsLsppHopRxLabelExpGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsLsppHopTableINDEX, 3, 0, 0, NULL},

{{13,FsLsppHopRtt}, GetNextIndexFsLsppHopTable, FsLsppHopRttGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsLsppHopTableINDEX, 3, 0, 0, NULL},

{{13,FsLsppHopDsMtu}, GetNextIndexFsLsppHopTable, FsLsppHopDsMtuGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsLsppHopTableINDEX, 3, 0, 0, NULL},

{{13,FsLsppHopDsAddrType}, GetNextIndexFsLsppHopTable, FsLsppHopDsAddrTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, FsLsppHopTableINDEX, 3, 0, 0, NULL},

{{13,FsLsppHopDsIPAddr}, GetNextIndexFsLsppHopTable, FsLsppHopDsIPAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsLsppHopTableINDEX, 3, 0, 0, NULL},

{{13,FsLsppHopDsIfAddr}, GetNextIndexFsLsppHopTable, FsLsppHopDsIfAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsLsppHopTableINDEX, 3, 0, 0, NULL},

{{13,FsLsppHopDsIfNum}, GetNextIndexFsLsppHopTable, FsLsppHopDsIfNumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsLsppHopTableINDEX, 3, 0, 0, NULL},

{{13,FsLsppHopDsLabelStack}, GetNextIndexFsLsppHopTable, FsLsppHopDsLabelStackGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsLsppHopTableINDEX, 3, 0, 0, NULL},

{{13,FsLsppHopDsLabelExp}, GetNextIndexFsLsppHopTable, FsLsppHopDsLabelExpGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsLsppHopTableINDEX, 3, 0, 0, NULL},

{{13,FsLsppHopIcc}, GetNextIndexFsLsppHopTable, FsLsppHopIccGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsLsppHopTableINDEX, 3, 0, 0, NULL},

{{13,FsLsppHopUMC}, GetNextIndexFsLsppHopTable, FsLsppHopUMCGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, FsLsppHopTableINDEX, 3, 0, 0, NULL},

{{13,FsLsppHopMepIndex}, GetNextIndexFsLsppHopTable, FsLsppHopMepIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsLsppHopTableINDEX, 3, 0, 0, NULL},
};
tMibData fslsppEntry = { 115, fslsppMibEntry };

#endif /* _FSLSPPDB_H */

