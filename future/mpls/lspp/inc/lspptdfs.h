/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: lspptdfs.h,v 1.25 2015/12/30 13:50:01 siva Exp $
*
* Description: This file contains type definitions for Lspp module.
*********************************************************************/

#ifndef __LSPPTDFS_H__
#define __LSPPTDFS_H__

#include "vcm.h"
#include "fm.h"
#include "ip.h"
#include "lspp.h"
#include "lsppdefn.h"
#include "mplsapi.h"


/*structure for all scalars in the mib*/
typedef struct
{

 tRBTree   FsLsppGlobalConfigTable;
 tRBTree   FsLsppGlobalStatsTable;
 tRBTree   FsLsppPingTraceTable;
 tRBTree   FsLsppEchoSequenceTable;
 tRBTree   FsLsppHopTable;
 INT4 i4FsLsppTrapContextNameLen;
 UINT1 au1FsLsppTrapContextName[32];

} tLsppGlbMib;


typedef struct
{
 tLsppMibFsLsppGlobalConfigTableEntry    MibObject;
    tIndexMgrId    IndexMgrId;
    UINT1          au1ContextName[LSPP_MAX_CONTEXT_NAME_LEN];
    UINT1          au1IndexBitMapList[LSPP_SENDER_HANDLE_INDEX_MGR_BITMAP_LEN];
    UINT1          au1Pad[3];
} tLsppFsLsppGlobalConfigTableEntry;


typedef struct
{
 tLsppMibFsLsppGlobalStatsTableEntry       MibObject;
} tLsppFsLsppGlobalStatsTableEntry;


typedef tPwPathId          tLsppPwFecInfo;
typedef tMplsTnlLspId      tLsppTnlLspInfo;
typedef tMplsPktInfo       tLsppPktTxInfo;
typedef tMplsServiceOid    tLsppServiceOid;
typedef tMplsOutSegInfo    tLsppOutSegInfo;
typedef tMplsInSegInfo     tLsppInSegInfo;
typedef tMplsInLblInfo     tLsppInLblInfo;
typedef tMplsAchHdr        tLsppAchHeader;
typedef tMplsAchTlvHdr     tLsppAchTlvHeader;
typedef tMplsGlobalNodeId  tLsppGlobalNodeId;
typedef tMplsMepTlv        tLsppMepTlv;
typedef tMplsMipTlv        tLsppMipTlv;
typedef tMplsAchTlv        tLsppAchTlv;
typedef tMplsHdr           tLsppMplsHdr;
typedef tMplsIccMepTlv     tLsppIccMep;



typedef struct
{
    tLsppMibFsLsppPingTraceTableEntry       MibObject;
    tOsixSemId                         SemId;  /* Sem to wait in CLI task
                                                      to print output */
    tTmrBlk                            AgeOutTimer;
    tTmrBlk                            WTSTimer;
    tLsppPathId                        PathId;  /* path Info structure for
                                                     protocol access */
    UINT4                              u4BfdSessionIndex;
    UINT4                              u4LastGeneratedSeqNum;
    UINT4                              u4LastUpdatedSeqIndex;
    UINT4                              u4SuccessCount;
    UINT4                              u4ReqCompletedCount;
    UINT1                              u1LastUpdatedHopIndex;
    UINT1                              u1LastSentTtl;
    UINT1                              u1RequestOwnerSubType;
    UINT1                              u1MegOperatorType;
    UINT1                              u1SrcTnlDsIPAddr[16];
    UINT4                              u4SrcTnlDsLabel;
    UINT2                              u2SrcTnlDsMtu;
    UINT1                              u1SrcTnlDsLabelExp;
    UINT1                              au1Pad[1];
} tLsppFsLsppPingTraceTableEntry;


typedef struct
{
 tLsppMibFsLsppEchoSequenceTableEntry       MibObject;
    tTmrBlk                               WFRTimer;
    UINT1                                 u1Status;
    UINT1                                 au1Pad[3];    
} tLsppFsLsppEchoSequenceTableEntry;


typedef struct
{
 tLsppMibFsLsppHopTableEntry       MibObject;
} tLsppFsLsppHopTableEntry;



/* -------------------------------------------------------
 *
 *                LSPP   Data Structures
 *
 * ------------------------------------------------------*/


typedef struct LSPP_GLOBALS {
 tTimerListId        lsppTmrLst;
 UINT4               u4LsppTrc;
 UINT4               u4LsppTrcLvl; 
 tOsixTaskId         lsppTaskId;
 UINT1               au1TaskSemName[8];
 tOsixSemId          lsppTaskSemId;
 tOsixQId            lsppQueId;
 tLsppGlbMib         LsppGlbMib;
 tMemPoolId          LsppFsLsppGlobalConfigTablePoolId;
 tMemPoolId          LsppFsLsppGlobalStatsTablePoolId;
 tMemPoolId          LsppFsLsppPingTraceTablePoolId;
 tMemPoolId          LsppFsLsppEchoSequenceTablePoolId;
 tMemPoolId          LsppFsLsppHopTablePoolId;
    tMemPoolId          LsppFsLsppQMsgPoolId;   /*for Q msg */
    tMemPoolId          LsppFsLsppEchoMsgPoolId; /* Mem Pool for Echo message
                                                    framing */
    INT4                i4LsppSockId;       /* UDP socket ID */
    INT4                i4SysLogId;
    UINT1               au1DupBuf[LSPP_MAX_PDU_LEN];
} tLsppGlobals;



typedef tLsppReqParams tLsppQMsg;


typedef struct
{
    UINT2     u2Src_u_port;
    UINT2     u2Dest_u_port;
    UINT2     u2Len;
    UINT2     u2Cksum;
} tLsppUdpHeader;



/************************* MPLS Label Information ********************/
typedef struct 
{
    UINT4 u4Label;    /* MPLS Label */
    UINT1 u1Ttl;      /* Time to live value */
    UINT1 u1Exp;      /* Traffic Class value */
    UINT1 u1SI;       /* Stack Indicator */
    UINT1 u1Protocol; /* Protocol which has distributed this label */
}tLsppLblInfo;

 

/************************* ACH TLV Information ********************/
typedef struct 
{
    tLsppAchHeader       AchHeader;          /* ACH Header Structure */
    tLsppAchTlvHeader    AchTlvHeader;       /* Ach TLV header Info */
    tLsppMepTlv          LsppMepTlv;         /* MEP TLV Info */
    tLsppMipTlv          LsppMipTlv;         /* Mip Tlv Info */
    UINT1                u1AchTlvPresent;    /* To find which TLV's are present
                                             l bit = LSP Mep TLV present
                                             2 bit = Pw Mep TLV present
                                             3 bit = ICC Mep TLV present
                                             4 bit = Mip TLV present
                                             others  - future use */
    UINT1                au1Pad[3];           /* Pad */
}tLsppAchInfo;

/************************* Lspp Header Information ********************/
typedef struct 
{
    UINT4    u4SenderHandle;            /* Specifies the handle for a ping 
                                        instance*/
    UINT4    u4SequenceNum;             /* Specifies the sequence number for 
                                        an instance */
    UINT4    u4TimeStampSentInSec;      /* Specifies time at which request is 
                                        sent in seconds*/
    UINT4    u4TimeStampSentInMicroSec; /* Specifies the time at which request 
                                        is sent in ms */  
    UINT4    u4TimeStampRxInSec;        /* Specifies the time at which 
                                        request packet at egress is received 
            in seconds*/ 
    UINT4    u4TimeStampRxInMicroSec;   /* Specifies the time at which request 
                                        packet is received at egress in ms */
    UINT2    u2Version;              /* Version number*/
    UINT2    u2GlobalFlag;             /* Global flags*/
    UINT1    u1MessageType;             /* Indicates request/reply */
    UINT1    u1ReplyMode;               /* Indicates the reply mode */
    UINT1    u1ReturnCode;              /* Return code */
    UINT1    u1ReturnSubCode;           /* Indicates point in the label stack 
                                        where operation is terminated */
}tLsppHeader;


/************************* LDP FEC Stack Information ***********/
typedef struct 
{
    tIpAddr    Prefix;         /* IPv4/Ipv6 Address */
    UINT1      u1PrefixType;   /* IPV4/IPV6 */
    UINT1      u1PrefixLength; /* Mask */
    UINT1      au1Pad[2];      /* Padding */
}tLsppLdpFecTlv;

/************************* RSVP-TE FEC Stack Information ***********/
typedef struct 
{
    tIpAddr    TunnelSenderAddr; /* Ingress Id */
    tIpAddr    TunnelEndAddr;    /* Eggress Id */
    tIpAddr    ExtendedTunnelId; /* Ingress Id */
    UINT2      u2TunnelId;       /* Tunnel Interface number */
    UINT2      u2LspId;          /* Tunnel instance */
    UINT1      u1AddrType;       /* Address type */
    UINT1      au1Pad[3];        /* Padding */
}tLsppRsvpTeFecTlv;

/************************* PW FEC Stack Information ***********/
typedef struct 
{
    tIpAddr    RemotePEAddr; /* Ip Address of the Peer */
    UINT4      u4PWId;       /* Pseudowire Id */
    UINT2      u2PWType;     /* Pseudowire Type */
    UINT1      au1Pad[2];    /* Pad */
}tLsppPwTlvInfo;

typedef struct 
{
    tLsppPwTlvInfo LsppPwInfo; /* Pw Info */
}tLsppPwFec128DeprecTlv;

typedef struct 
{
    tIpAddr        SenderPEAddr; /* Source Ip Address */
    tLsppPwTlvInfo LsppPwInfo;   /* Pw Info */
}tLsppPwFec128Tlv;


typedef struct 
{
    tIpAddr    SenderPEAddr;                   /* Source PE  Address */
    tIpAddr    RemotePEAddr;                   /* Destination PE Address */ 
    UINT1      au1Agi[LSPP_L2VPN_PWVC_MAX_AI_LEN];  /* Agi Value */
    UINT1      au1Saii[LSPP_L2VPN_PWVC_MAX_AI_LEN]; /* SAII value */
    UINT1      au1Taii[LSPP_L2VPN_PWVC_MAX_AI_LEN]; /* TAII value */
    UINT2      u2PWType;                       /* Pw Type */
    UINT1      u1AgiType;                      /* Agi Type */
    UINT1      u1AgiLen;                    /* Agi Length */
    UINT1      u1SaiiType;                     /* Source Attachment Id Type */  
    UINT1      u1SaiiLen;                   /* Source Attachment Id length*/
    UINT1      u1TaiiType;                     /* Target Attachment Id Type */
    UINT1      u1TaiiLen;                   /* Target Attachment Id length*/
}tLsppPwFec129Tlv;


/************************* Nil FEC Stack Information ***********/
typedef struct 
{
    UINT4     u4Label; /* Nil Fec Label */
}tLsppNilFecTlv;

/************************* Static LSP FEC Stack Information ***********/
typedef struct 
{
    UINT4    u4SrcGlobalId;   /* Global Id of the Source node of tunnel*/
    UINT4    u4SrcNodeId;     /* Node Id of Source node of tunnel */
    UINT4    u4DstGlobalId;   /* Global Id of destination node */
    UINT4    u4DstNodeId;     /* Node of destination node of tunnel */
    UINT2    u2SrcTnlNum;     /* Tunnel number at Source node */
    UINT2    u2LspNum;        /* LSP number */
    UINT2    u2DstTnlNum;     /* Tunnel number of the reverse tunnel */
    UINT1    au1Pad[2];       /* Pad */
}tLsppStaticLspFec;

/************************* Static PW FEC Stack Information ***********/
typedef struct 
{   
    UINT4    u4SrcGlobalId;   /* Global Id of the Source node of tunnel*/
    UINT4    u4SrcNodeId;     /* Node Id of Source node of tunnel */
    UINT4    u4SrcAcId;       /* Attachment circuit to the ingress node */
    UINT4    u4DstGlobalId;  /* Global Id of destination node */
    UINT4    u4DstNodeId;    /* Node of destination node of tunnel */
    UINT4    u4DstAcId;      /* Attachment Circuit to the Egress node */
    UINT1    au1Agi[LSPP_L2VPN_PWVC_MAX_AI_LEN];  /* Agi Value */
    UINT1    u1AgiType;                      /* Agi Type */
    UINT1    u1AgiLen;                    /* Agi Length */
    UINT1    au1Pad[2];      /* Padding */
}tLsppStaticPwFec;

/************************* DSMAP TLV Information ***********/
typedef struct 
{
    tLsppLblInfo    DsLblInfo[LSPP_MAX_LABEL_STACK];  /* DS LABEL stack */
    UINT1           au1MultipathInfo[LSPP_MAX_MULTIPATH_LENGTH]; /* Multipath 
                                                                    Info length
                                                                    of TLV */
    /* If LSPP_MAX_MULTIPATH_LENGTH value is modified then structure needs to be
     * paded appropriately to maintain word-alignment */
    tIpAddr         DsIpAddr;                        /* Downstream IpAddress */
    tIpAddr         DsIfAddr;                        /* Downstream IfAddress */
    UINT2           u2Mtu;                           /* MTU of DS interface */
    UINT2           u2MultipathLength;               /* Multipath Length */
    UINT2           u2Length;                        /* length of TLV */
    
    UINT1           u1AddrType;                      /* Ds Address type */
    UINT1           u1DsFlags;                       /* 1 bit- Interface 
                                                          and Label Stack 
                                                          Object Request
                                                        2 bit - Treat as
                                                          Non-Ip Packet */
    UINT1           u1MultipathType;                 /* Multipath type */
    UINT1           u1DepthLimit;                    /* Depth Limit */
    UINT1           u1DsLblStackDepth;               /* Number of labels 
                                                        in the DS Label stack.*/
    UINT1           au1Pad[1];                    /* Padding */

}tLsppDsMapTlv;


/************************* PAD TLV Information ****************************/
typedef struct 
{
    UINT1  au1PadInfo[LSPP_MAX_PAD_LEN]; /* Pad info received or to be framed.*/
    UINT2  u2Length;                     /* PAD length */
    UINT1  u1FirstOctet;                /* Indicates to drop or copy pad tlv */
    UINT1  au1Pad[3];
}tLsppPadTlv;


/************************* Interface and Label Stack TLV Information */
typedef struct 
{
    tLsppLblInfo    RxLblInfo[LSPP_MAX_LABEL_STACK]; /* RX LABEL stack */
    tIpAddr         RxIpAddr;                        /* Downstream IpAddress */
    tIpAddr         RxIfAddr;                        /* Downstream IfAddress */
    UINT2           u2Length;                        /* length of TLV */
    UINT1           u1AddrType;                      /* RX If Address type */
    UINT1           u1LblStackDepth;                 /* Rcvd Label Stack Depth*/
}tLsppIfLblStkTlv;

/************************* Error TLV Information *******************/
typedef struct 
{
    UINT1           au1Value[LSPP_MAX_ERROR_TLV_LEN]; /*  Error TLV Value*/
    UINT2           u2ErrorTLVLength;
    UINT2           au1Pad[2];
}tLsppErrorTlv;

/************************* Reply TOS TLV Information *******************/
typedef struct 
{
    UINT1           u1TosValue;     /*Reply Tos value */
    UINT1           au1Pad[3];     /*Reply Tos value */
}tLsppReplyTosTlv;

/************************* BFD Disc TLV Information *******************/
typedef struct 
{
    UINT4           u4Discriminator;     /*Reply Tos value */
}tLsppBfdDiscTlv;

/************************* Target FEC Stack Information ********************/
typedef struct 
{   
    UINT2                     u2TlvType;             /*Specifies the FEC Type*/ 
    UINT2                     u2TlvLength;           /* Target FEC Length */
    
    /* Targer FEC STACL TLV *******/
    union
    {
        tLsppLdpFecTlv            LsppLdpFecTlv;         /*MPLS V4/V6 LDP 
                                                           FEC TLV */
        tLsppRsvpTeFecTlv         LsppRsvpTeFecTlv;      /*MPLS V4/V6 RSVP-TE 
                                                           FEC TLV*/
        tLsppPwFec128DeprecTlv    LsppFec128DeprecTlv;   /* MPLS Deprecated 
                                                            FEC128PW TLV*/
        tLsppPwFec128Tlv          LsppPwFec128Tlv;       /* MPLS FEC 128 Pw */ 
        tLsppPwFec129Tlv          LsppPwFec129Tlv;       /* MPLS FEC 129 PW */
        tLsppNilFecTlv            LsppNilFecTlv;         /* Nil Fec */
        tLsppStaticLspFec         LsppStaticLspFec;      /* MPLS-TP LSP FEC 
                                                            TLV*/
        tLsppStaticPwFec          LsppStaticPwFec;       /* MPLS-TP  PW FEC 
                                                            TLV */
    }unFecTlv;

#define LdpFecTlv         unFecTlv.LsppLdpFecTlv 
#define RsvpTeFecTlv      unFecTlv.LsppRsvpTeFecTlv
#define PwFec128DeprecTlv  unFecTlv.LsppFec128DeprecTlv
#define PwFec128Tlv       unFecTlv.LsppPwFec128Tlv
#define PwFec129Tlv       unFecTlv.LsppPwFec129Tlv
#define NilFecTlv         unFecTlv.LsppNilFecTlv
#define StaticLspFec      unFecTlv.LsppStaticLspFec
#define StaticPwFec       unFecTlv.LsppStaticPwFec 
}tLsppFecTlv;                                                    



/************************* Lspp Pdu Information ********************/
typedef struct 
{
    tLsppHeader      LsppHeader;                           /* Lsp Ping Header */
    tLsppFecTlv      LsppFecTlv[LSPP_MAX_FEC_STACK_DEPTH];  /* Target Fec 
                                                              Stack TLV */    
    /****************** Other TLVs ******************/
    tLsppDsMapTlv             LsppDsMapTlv;          /* DSMAP TLV */
    tLsppIfLblStkTlv          LsppIfLblStkTlv;       /* Interface label 
                                                        stack TLV */
    tLsppPadTlv               LsppPadTlv;            /* Pad TLV */
    tLsppReplyTosTlv          LsppReplyTosTlv;       /* Reply TOS TLV */
    tLsppErrorTlv             LsppErrorTlv;          /* Error TLV */
    tLsppBfdDiscTlv           LsppBfdDiscTlv;        /* BFD TLV */

    /* List of TLVs in the echo packet received or to be framed.
     *
     * 0000 0000 0000 0001 - Target FEC TLV
     * 0000 0000 0000 0010 - Downstream Mapping TLV
     * 0000 0000 0000 0100 - Interface & Label Stack TLV
     * 0000 0000 0000 1000 - Pad TLV
     * 0000 0000 0001 0000 - Reply TOS Byte TLV
     * 0000 0000 0010 0000 - Errored TLV
     * 0000 0000 0100 0000 - BFD Discriminator TLV
     */
    UINT2                     u2TlvsPresent;         
    UINT2                     u2FecStackTlvLength;   /* To store the 
                                                         Fec stack TLv
                                                         length */
    UINT1                     u1FecStackDepth;       /* To store the 
                                                         Fec stack
                                                            depth */
    UINT1                     u1IncludeIfLblStk;     /* This indicates whether 
                                                        interface and label 
                                                        stack TLV should be 
                                                        included in reply. */
    UINT1                     au1Pad[2];
}tLsppPduInfo;



typedef struct
{
    tLsppLdpInfo        LdpInfo;
    tLsppInSegInfo      InSegInfo; /* In Segment information */
    tLsppOutSegInfo     OutSegInfo; /* Out Segment information */
}tLsppNonTeInfo;


typedef struct
{
    tLsppTnlLspInfo     TnlInfo;
    tIpAddr             ExtendedTnlId;
    tLsppInSegInfo      InSegInfo; /* In Segment information */
    tLsppOutSegInfo     OutSegInfo; /* Out Segment information */
    tLsppMegIndices     MegIndices;
    UINT4               u4TnlType; /* Tunnel type - mpls, mpls-tp,gmpls,h-lsp */
    UINT4               u4TnlPrimaryInstance;
    UINT1               au1NextHopMac[MAC_ADDR_LEN];
    UINT1               u1OperStatus;
    UINT1               u1TnlOwner; /* snmp/ldp/crldp/rsvpTe */
    UINT1               u1TnlMode; /* unidirectional/co-routed bi-directional/
                                      associated bi-directional */
    UINT1               u1TnlRole; /* head/transit/tail */
    UINT1               u1TnlSgnlPrtcl; /* Static / RSVP-TE / CR-LDP */
    UINT1               au1Pad[1];
}tLsppTeTnlInfo;



typedef struct
{
    tLsppPwFecInfo        PwFecInfo;
    union {
        tLsppTnlLspInfo TnlIndices;  /* TE Tunnel identifier */
        UINT4 u4NonTeXcIndex; /* NON-TE Tunnel identifierr */
        UINT4 u4OutIfIndex; /* pwOnly */
    }unOutTnlInfo;
    tLsppMegIndices  MegIndices;
    UINT1 au1NextHopMac[MAC_ADDR_LEN]; /* NextHop MAC address for PwOnly case */
    UINT1 u1CcSelected; /* VCCV: ACH, TTL expiry and router alert label */
    UINT1 u1CvSelected; /* VCCV: ICMP ping, LSP ping and BFD */
    UINT4 u4OutVcLabel; /* Outgoing VC label */
    UINT4 u4InVcLabel; /* Incoming VC label */
 /*   UINT1 u1InVcLabelAction; */
    UINT1 u1MplsType; /* TE/NON-TE/PwOnly */
    UINT1 u1Ttl; /* TTL value for the outgoing VC label */
    UINT1 u1PwType; /* MPLS/MPLS-TP */
    UINT1 u1IpVersion; /* IP version of PW with LDP signalling */
    INT1  i1OperStatus; /* UP/DOWN/Lower Layer down */
    UINT1 au1Pad[3];

}tLsppPwDetail;
  
typedef struct
{
    UINT4 u4PwIndex;
}tLsppPwPathInfo;


typedef struct
{
    tLsppMegInfo      MegInfo;
    union {
        tLsppTnlLspInfo ServiceTnlId; /* Tunnel identifiers */
        tLsppPwPathInfo ServicePwId; /* Pseudowire Identifiers */
    }unServiceInfo;
    tLsppGlobalNodeId MplsNodeId; /* MIP Global_ID::Node_ID */
    UINT4 u4MpIfIndex; /* MEP/MIP interface index */
    UINT4 u4OnDemandTCValue; /* PHB value for LSP ping */
    UINT2 u2IccSrcMepIndex; /* ICC based Source MEP index */
    UINT2 u2IccSinkMepIndex; /* ICC based Sink MEP index */
    UINT1 au1Icc[LSPP_MPLS_ICC_LENGTH+1]; /* ICC based MEG-ID-1 */
    UINT1 u1MpType; /* MEP/MIP */
    UINT1 u1MepDirection; /* DOWN/UP */
    UINT1 au1Umc[LSPP_MPLS_UMC_LENGTH+1]; /* ICC based MEG-ID-2 */
    UINT1 u1MeState; /* UP/DOWN - UP [if ME is admin up and Service is up]
                        - DOWN [if ME is admin down or Service is down]*/
    UINT1 u1ServiceType; /* LSP/Pseudowire/Section */
    UINT1 u1ServiceLocation; /* Per Node/Per Interface */
    UINT1 u1OperatorType; /* IP/ICC operator */
    UINT1 u1Pad[3];

}tLsppMegDetail;




typedef struct PathInfo
{
    tLsppNonTeInfo   NonTeInfo;
    tLsppTeTnlInfo   TeTnlInfo;
    tLsppPwDetail    PwDetail;
    tLsppMegDetail   MegDetail;
    UINT1            u1PathType;
    UINT1            au1Pad[3];
}tLsppPathInfo;





/*********************** LSPP payload details ***********************/
typedef struct LsppMsg
{
    tLsppLblInfo       LblInfo[LSPP_MAX_LABEL_STACK]; /* LABEL stack */

    tLsppAchInfo       LsppAchInfo;        /* ACH header details */

    t_IP               LsppIpHeader;       /* Ip header details */

    tLsppUdpHeader    LsppUdpHeader;      /* Udp header details */

    tLsppPduInfo       LsppPduInfo;        /* LSP Ping PDU */

    tLsppPathId        LsppPathId;
    tLsppPathInfo      LsppPathInfo;       /* This Structure is to 
                                             store the path info */
    tLsppOutSegInfo    *pOutSegInfo;       /* Pointer to Outsegmen
                                              Structure*/ 
    UINT4              u4IfIndex;          /* To store the received If 
                                              Index*/    
    UINT4              u4IpHdrOffset;      /* To store the Ip Header offset*/

    UINT4              u4UdpHdrOffset;     /* To store the Udp Header Offset*/

    UINT4              u4BfdTlvOffset;     /* To store the BFD TLV Offset*/

    UINT4              u4EncapHdrLen;        /* To store the Encapsultiaon 
                                              Header Length*/
    UINT4              u4LsppPduLen;       /* To store the LSPP Pdu Length*/

    UINT4              u4ContextId;        /* Identifier to the specific 
                                              context.*/  
    UINT1              u1LabelStackDepth;  /* To store the label stack
                                              depth */
    UINT1              u1LblSwapAtDepth;   /* To store the index of label 
                                              stack at which the label 
                                              will be swapped.*/
    UINT1              u1EncapType;        /* To store the index*/ 

    UINT1              u1CcSelected;       /* To store the index*/

    UINT1              u1IsRouteExists;    /* Set when the route exits for                                                    the destination IP. */

    UINT1              au1Pad[3];           /*Padding */
}tLsppEchoMsg;




typedef struct SysTime
{
    UINT4  u4TimeInSec;
    UINT4  u4TimeInMicroSec;
}tLsppSysTime;


typedef struct 
{
    CHR1    ac1SyslogMsg [LSPP_MAX_SYSLOG_MSG_LEN];
    UINT4   u4SyslogLevel;
}tLsppSyslogMsg;

typedef struct
{
    UINT4 u4ContextId;
    UINT4 u4RequestType;
    union
    {
        tLsppBfdInfo     BfdInfo;
        tLsppLdpInfo     LdpInfo;
        tLsppTnlInfo     TnlInfo;
        tLsppPwFecInfo   PwFecInfo;
        tLsppMegInfo     MegInfo;
        tFmFaultMsg      FmFaultMsg;
        tLsppInLblInfo   LblInfo;
        tLsppPktTxInfo   LsppPktTxInfo;
        tLsppNodeId      LsppNodeId;
        tLsppSyslogMsg   SyslogMsg;
        tLsppIccMep      IccMep;
        UINT1            au1ContextName[LSPP_MAX_CONTEXT_NAME_LEN];
        UINT4            u4FtnIndex;
        UINT4            u4XcIndex;
        UINT4            u4IfIndex;
    }uInParams;
#define InBfdInfo     uInParams.BfdInfo 
#define InLdpInfo     uInParams.LdpInfo 
#define InTnlInfo     uInParams.TnlInfo 
#define InPwFecInfo   uInParams.PwFecInfo 
#define InMegInfo     uInParams.MegInfo 
#define InLblInfo     uInParams.LblInfo 
#define InPktTxInfo   uInParams.LsppPktTxInfo 
#define InLsppNodeId  uInParams.LsppNodeId 
#define LsppSyslogMsg uInParams.SyslogMsg
#define InIccMep  uInParams.IccMep
}tLsppExtInParams;




typedef struct
{
    union
    {
        tLsppNonTeInfo   LdpInfo;
        tLsppTeTnlInfo   TnlInfo;
        tLsppPwDetail    PwDetail;
        tLsppMegDetail   MegDetail;
        tLsppServiceOid  ServiceOid;
        tLsppNodeId      LsppNodeId;
        UINT1            au1ContextName[LSPP_MAX_CONTEXT_NAME_LEN];
        UINT4            u4ContextId;
        UINT4            u4Mtu;
        UINT4            u4IfIndex; 
    }unOutParams;

#define OutLdpInfo    unOutParams.LdpInfo
#define OutTnlInfo    unOutParams.TnlInfo
#define OutPwDetail   unOutParams.PwDetail
#define OutMegDetail  unOutParams.MegDetail
#define OutOid        unOutParams.ServiceOid
#define OutLsppNodeId unOutParams.LsppNodeId
    UINT1    u1PathType;
    UINT1      au1Pad[3];
}tLsppExtOutParams;


typedef struct EventLogNotify {
    UINT4   u4TraceLevel;
    UINT4   u4SyslogLevel;
    CHR1   ac1LogMsg [LSPP_MAX_SYSLOG_MSG_LEN];
    UINT4   u4TrapType;
}tLsppEventLogNotify;



typedef enum {
    LSPP_BFD_PROCESS_BTSTRAP_INFO = 1,
    LSPP_SYSLOG_REGISTER,
    LSPP_SYSLOG_DEREGISTER,
    LSPP_SYSLOG_MSG,
    LSPP_VCM_REGISTER_WITH_VCM,
    LSPP_VCM_DEREGISTER_WITH_VCM,
    LSPP_VCM_GET_CXT_NAME_FRM_ID,
    LSPP_VCM_GET_CXT_ID_FRM_IF_INDEX,
    LSPP_VCM_GET_CXT_ID_FRM_NAME,
    LSPP_CFA_GET_MTU,
    LSPP_CFA_GET_L3_IF,
    LSPP_CFA_GET_IF_ADDR,
    LSPP_NETIP_GET_IF_INDEX,
    LSPP_NETIP_GET_SRC_IP_FRM_DEST_IP,
    LSPP_MPLSRTR_TX_OAM_PKT,
    LSPP_MPLS_OAM_GET_MEG_ME_NAME,
    LSPP_MPLS_OAM_GET_MEG_OID,
    LSPP_MPLS_OAM_GET_MEG_INFO,
    LSPP_MPLS_OAM_GET_REV_PATH_INFO,
    LSPP_MPLS_OAM_GET_ROUTER_ID,
    LSPP_GET_PATH_FRM_INLBL_INFO,
    LSPP_GET_FTN_BASE_OID,
    LSPP_GET_PW_BASE_OID,
    LSPP_GET_TNL_BASE_OID,
    LSPP_GET_MEG_BASE_OID,
    LSPP_MPLSDB_GET_FTN_OID,
    LSPP_MPLSDB_GET_TUNNEL_OID,
    LSPP_MPLSDB_GET_LDP_INFO_FRM_FEC,
    LSPP_MPLSDB_GET_LDP_INFO_FRM_XC_INDEX,
    LSPP_MPLSDB_GET_LDP_INFO_FRM_FTN_INDEX,
    LSPP_MPLSDB_GET_TNL_INFO,
    LSPP_MPLSDB_GET_REV_TNL_INFO,
    LSPP_L2VPN_GET_PW_OID,
    LSPP_L2VPN_GET_VCID_FRM_PW_INDEX,
    LSPP_L2VPN_GET_PW_INFO_FRM_PW_INDEX,
    LSPP_L2VPN_GET_PW_INFO_FRM_VC_ID,
    LSPP_L2VPN_GET_PW_INFO_FRM_PWID_FEC,
    LSPP_L2VPN_GET_PW_INFO_FRM_GENFEC_TYPE1,
    LSPP_L2VPN_GET_PW_INFO_FRM_GENFEC_TYPE2,
    LSPP_L2VPN_GET_CCCV_CAPABILITY,
    LSPP_FM_SEND_TRAP,
    LSPP_EXT_GET_VCM_CONTEXT_NAME,
    LSPP_MPLS_OAM_GET_NODE_ID,
    LSPP_MPLS_OAM_GET_MEG_INFO_FROM_ICC_MEG
}eExitReqType;


typedef enum {
    LSPP_REQ_TX = 1,
    LSPP_REQ_RX,
    LSPP_REQ_DROPPED,
    LSPP_REQ_TIMEDOUT,
    LSPP_REQ_UNSENT,
    LSPP_REPLY_TX,
    LSPP_REPLY_RX,
    LSPP_REPLY_DROPPED,
    LSPP_REPLY_UNSENT,
    LSPP_INVALID_PKT_DROPPED
}eGlobalReqType;

typedef struct
{
    UINT1 u1Type;
    UINT1 u1Len;
    UINT2 u2Value;
}tLsppIpRouterAlertOpt;

typedef struct
{
    UINT4               u4Src;         /* Source address */
    UINT4               u4Dest;        /* Destination address */
    UINT1               u1Zero;        /* Fill ZERO for 8 bits*/
    UINT1               u1Proto;       /* Protocol */
    UINT2               u2UdpLen;
} tLsppUdpIpPseudoHdr;

#endif  /* __LSPPTDFS_H__ */
/*-----------------------------------------------------------------------*/


