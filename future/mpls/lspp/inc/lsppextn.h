/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lsppextn.h,v 1.2 2010/10/17 03:01:22 prabuc Exp $
 *
 * Description: This file contains extern declaration of global 
 *              variables of the lspp module.
 *******************************************************************/

#ifndef __LSPPEXTN_H__
#define __LSPPEXTN_H__

PUBLIC tLsppGlobals gLsppGlobals;
PUBLIC CONST CHR1 *gapReturnCodeString [];

#endif/*__LSPPEXTN_H__*/
   
/*-----------------------------------------------------------------------*/
/*                       End of the file lsppextn.h                      */
/*-----------------------------------------------------------------------*/

