/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lspptrc.h,v 1.25 2014/07/16 12:49:21 siva Exp $
 *
 * Description:This file contains procedures and definitions 
 *             used for debugging.
 *******************************************************************/


#ifndef __LSPPTRC_H__
#define __LSPPTRC_H__

#define  LSPP_TRC_FLAG  gLsppGlobals.u4LsppTrc
#define  LSPP_TRC_LVL  gLsppGlobals.u4LsppTrcLvl
#define  LSPP_NAME      "LSPP"               

#ifdef LSPP_TRACE_WANTED


#define  LSPP_TRC(x)       LsppTrcPrint( __FILE__, __LINE__, LsppTrc x)
#define  LSPP_TRC_FUNC(x)  LsppTrcPrint( __FILE__, __LINE__, LsppTrc x)
#define  LSPP_TRC_CRIT(x)  LsppTrcPrint( __FILE__, __LINE__, LsppTrc x)
#define  LSPP_TRC_PKT(x)   LsppTrcWrite( LsppTrc x)




#else /* LSPP_TRACE_WANTED */

#define  LSPP_TRC(x) 
#define  LSPP_TRC_FUNC(x)
#define  LSPP_TRC_CRIT(x)
#define  LSPP_TRC_PKT(x)

#endif /* LSPP_TRACE_WANTED */


#define  LSPP_FN_ENTRY  (0x1 << 9)
#define  LSPP_FN_EXIT   (0x1 << 10)
#define  LSPP_CLI_TRC   (0x1 << 11)
#define  LSPP_MAIN_TRC  (0x1 << 12)
#define  LSPP_PKT_TRC   (0x1 << 13)
#define  LSPP_QUE_TRC   (0x1 << 14)
#define  LSPP_TASK_TRC  (0x1 << 15)
#define  LSPP_TMR_TRC   (0x1 << 16)
#define  LSPP_UTIL_TRC  (0x1 << 17)
#define  LSPP_ALL_TRC   LSPP_FN_ENTRY |\
                        LSPP_FN_EXIT  |\
                        LSPP_CLI_TRC  |\
                        LSPP_MAIN_TRC |\
                        LSPP_PKT_TRC  |\
                        LSPP_QUE_TRC  |\
                        LSPP_TASK_TRC |\
                        LSPP_TMR_TRC  |\
                        LSPP_UTIL_TRC

#ifdef __LSPPMAIN_C__

tLsppEventLogNotify  gaLsppIssEventLogNotify[ ] = 
{

    /* LSPP_MEM_RELEASE_FAILED */
    /* Freeing Memory failure; Argument: Function name, Table or Queue Name */ 
    { 
        (LSPP_RESOURCE_TRC | LSPP_BUF_TRC | LSPP_ALL_FAILURE_TRC), 
        SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s Unable to free memory pool for %s !!!\n" , 
        LSPP_INVALID_TRAP 
    },

    /* LSPP_QUEUE_SEND_FAILED */
    /* Sending to OSIX q failure ; Argument : Function name */
    { 
        (LSPP_RESOURCE_TRC | LSPP_ALL_FAILURE_TRC), 
        SYSLOG_ALERT_LEVEL, 
        "\r\n %s Osix Q send failed !!!\n", 
        LSPP_INVALID_TRAP 
    },

    /* LSPP_EVENT_SEND_FAILED */
    /* Sending OSIX event failure ; Argument : Function name */
    { 
        (LSPP_RESOURCE_TRC | LSPP_ALL_FAILURE_TRC), 
        SYSLOG_ALERT_LEVEL, 
        "\r\n %s Osix Event send failed !!!\n",
        LSPP_INVALID_TRAP },

    /* LSPP_MEM_ALLOC_FAIL */
    /*  Trap logging failure, Argument : Function name */
    { 
        (LSPP_ERROR_TRC | LSPP_ALL_FAILURE_TRC), 
        SYSLOG_ALERT_LEVEL, 
        "\r\n %s: Memory Allocation Failed\n",
        LSPP_INVALID_TRAP
    },

    /* LSPP_TMR_START_FAILED */
    /* Timer Start Failure ; Argument:  Timer Type*/ 
    { 
        (LSPP_RESOURCE_TRC  | LSPP_ALL_FAILURE_TRC),
        SYSLOG_ALERT_LEVEL, 
        "\r\n Timer start for type %d failed!!!\n", 
        LSPP_INVALID_TRAP 
    },

    /* LSPP_GET_REMAINING_TIME_FAILED */

    { 
        (LSPP_RESOURCE_TRC  | LSPP_ALL_FAILURE_TRC),
        SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s Timer Get Remaining time failed!!!\n", 
        LSPP_INVALID_TRAP 
    },
    /* LSPP_ENQUEUE_FAILED */

    { 
        (LSPP_RESOURCE_TRC  | LSPP_ALL_FAILURE_TRC),
        SYSLOG_ALERT_LEVEL, 
        "\r\n %s Enqueue in queue failed!!!\n", 
        LSPP_INVALID_TRAP 
    },

    /* LSPP_SOCK_CREATION_FAILED */
    { 
        (LSPP_RESOURCE_TRC  | LSPP_ALL_FAILURE_TRC),
        SYSLOG_ALERT_LEVEL, 
        "\r\n %s Socket creation failed !!!\n", 
        LSPP_INVALID_TRAP 
    },

    /* LSPP_SOCK_BIND_FAILED */ 
    { 
        (LSPP_RESOURCE_TRC  | LSPP_ALL_FAILURE_TRC),
        SYSLOG_ALERT_LEVEL, 
        "\r\n %s Socket bind failed !!!\n", 
        LSPP_INVALID_TRAP 
    },

    /* LSPP_SOCK_FCNTL_SET_FAILED */

    { 
        (LSPP_RESOURCE_TRC  | LSPP_ALL_FAILURE_TRC),
        SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s Socket fcntl function failed !!!\n", 
        LSPP_INVALID_TRAP 
    },
    /* LSPP_SET_SOCK_OPT_FAILED */

    { 
        (LSPP_RESOURCE_TRC  | LSPP_ALL_FAILURE_TRC),
        SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s setsockopt failed while setting %s !!!\n", 
        LSPP_INVALID_TRAP 
    },

    /* LSPP_SOCK_SEND_TO_FAILED */

    { 
        (LSPP_RESOURCE_TRC  | LSPP_ALL_FAILURE_TRC),
        SYSLOG_ALERT_LEVEL, 
        "\r\n %s Socket sendto call failed !!!\n", 
        LSPP_INVALID_TRAP 
    },

    /* LSPP_ADD_FD_IN_SELECT_FAILED */
    { 
        (LSPP_RESOURCE_TRC  | LSPP_ALL_FAILURE_TRC),
        SYSLOG_ALERT_LEVEL, 
        "\r\n %s Add socket Fd in select utility failed !!!\n", 
        LSPP_INVALID_TRAP 
    },

    /* LSPP_SOCK_RECV_FROM_FAILED */
    { 
        (LSPP_RESOURCE_TRC  | LSPP_ALL_FAILURE_TRC),
        SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s Socket recvfrom call failed !!!\n", 
        LSPP_INVALID_TRAP 
    },

    /* LSPP_GET_SOCK_OPT_FAILED */
    { 
        (LSPP_RESOURCE_TRC  | LSPP_ALL_FAILURE_TRC),
        SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s getsockopt failed !!!\n", 
        LSPP_INVALID_TRAP 
    },
    
    /* LSPP_DE_REGISTER_WITH_VCM_FAILED */
    {
        (LSPP_RESOURCE_TRC  | LSPP_ALL_FAILURE_TRC),
        SYSLOG_CRITICAL_LEVEL,
        "\r\n %s De-Register with VCM module failed !!!\n",
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_SEM_CREATION_FAILED */
    {
        (LSPP_RESOURCE_TRC  | LSPP_ALL_FAILURE_TRC),
        SYSLOG_ALERT_LEVEL,
        "\r\n %s sem creation failed !!!\n",
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_CRU_BUF_ALLOC_FAILED */
    {
        (LSPP_RESOURCE_TRC  | LSPP_ALL_FAILURE_TRC),
        SYSLOG_ALERT_LEVEL,
        "\r\n %s CRU Buf alloc Failed !!!\n",
        LSPP_INVALID_TRAP
    },

    /* LSPP_CONTEXT_ID_INVALID */

    {
        (LSPP_RESOURCE_TRC  | LSPP_ALL_FAILURE_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s Context %u does not exist.\n",
        LSPP_INVALID_TRAP
    },
    /* LSPP_SOCK_SEND_MSG_FAILED */

    { 
        (LSPP_RESOURCE_TRC  | LSPP_ALL_FAILURE_TRC),
        SYSLOG_ALERT_LEVEL, 
        "\r\n %s Socket sendmsg call failed !!!\n", 
        LSPP_INVALID_TRAP 
    }
};



tLsppEventLogNotify  gaLsppEventLogNotify[ ] = 
{

    /* LSPP_CXT_CREATION_FAILED */
    { 
        LSPP_ALL_FAILURE_TRC,
        SYSLOG_CRITICAL_LEVEL,
        "\r\n %s : Unable to create context with context id %d !!!\n",
        LSPP_INVALID_TRAP },

    /* LSPP_CXT_DELETION_FAILED */
    {
        LSPP_ALL_FAILURE_TRC,
        SYSLOG_CRITICAL_LEVEL,
        "\r\n %s : Unable to delete context with context id %d !!!\n",
        LSPP_INVALID_TRAP },

    /* LSPP_PROCESS_RX_PKT_FAIL */
    {
        LSPP_ALL_FAILURE_TRC,
        SYSLOG_NOTICE_LEVEL,
        "\r\n %s Received echo packet processing failed\n" ,
        LSPP_INVALID_TRAP
    },

    /* LSPP_INVALID_MSG_TYPE_IN_QUE  */
    {
        LSPP_ALL_FAILURE_TRC,
        SYSLOG_NOTICE_LEVEL,
        "\r\n %s Invalid message type received from queue\n" ,
        LSPP_INVALID_TRAP
    },
    /* LSPP_UNABLE_TO_SEND_ECHO_REQ */
    {
        LSPP_ALL_FAILURE_TRC,
        SYSLOG_CRITICAL_LEVEL,
        "\r\n %s Unable to send echo request\n" ,
        LSPP_INVALID_TRAP
    },

    /* LSPP_REQ_TIMED_OUT_PROCESS_FAILED */
    {
        LSPP_ALL_FAILURE_TRC,
        SYSLOG_ERROR_LEVEL,
        "\r\n %s Processing after WFR timer expiry failed\n" ,
        LSPP_INVALID_TRAP
    },

    /* LSPP_UNABLE_TO_DELETE_PING_TRACE_INFO */
    {
        LSPP_ALL_FAILURE_TRC,
        SYSLOG_WARN_LEVEL,
        "\r\n %s Unable to delete sequence Table entries and the Hop table"
            "entries with senders handle %d\n" ,
        LSPP_INVALID_TRAP
    },

    /* LSPP_API_HANDLE_FAILED */
    {
        LSPP_ALL_FAILURE_TRC,
        SYSLOG_CRITICAL_LEVEL,
        "\r\n %s External API Handler function failed\n",
        LSPP_INVALID_TRAP
    },

    /* LSPP_SOCK_INIT_FAILED */

    { 
        (LSPP_RESOURCE_TRC  | LSPP_ALL_FAILURE_TRC),
        SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s Socket init failed !!!\n", 
        LSPP_INVALID_TRAP 
    },

    /* LSPP_TRAP_PING_COMPLETED */
    {
        (LSPP_EVENT_TRC),
        SYSLOG_INFO_LEVEL, 
        "\r\n Ping Completed !!!\n", 
        (LSPP_PING_COMPLETION_TRAP) 
    },
    
    /* LSPP_TRAP_TRACE_COMPLETED */
    {
        (LSPP_EVENT_TRC),
        SYSLOG_INFO_LEVEL, 
        "\r\n Traceroute Completed !!!\n", 
        (LSPP_TRACE_ROUTE_COMPLETION_TRAP) 
    },

    
    /* LSPP_TRAP_BFD_BOOTSTRAP_REQ */
    {
        (LSPP_EVENT_TRC),
        SYSLOG_INFO_LEVEL, 
        "\r\n Bfd Bootstrap Request Received\n", 
        LSPP_BFD_BTSTRAP_TRAP 
    },

    /* LSPP_PROCESS_MPLS_PKT_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s :Processing MPLS Packet Failed!!!\n", 
        LSPP_INVALID_TRAP
    },

    /* LSPP_ECHO_MSG_VALIDATION_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s :Echo Message Validation Failed!!!\n", 
        LSPP_INVALID_TRAP
    },

    /* LSPP_DSMAP_NOT_PRESENT_IN_REPLY_FRM_TRANSIT */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s :Dsmap Tlv is not present in the echo reply "
            "from transit node.\n", 
        LSPP_INVALID_TRAP
    },

    /* LSPP_DSMAP_PRESENT_IN_REPLY_FRM_EGRESS */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s :Dsmap Tlv present in the echo reply from egress.\n", 
        LSPP_INVALID_TRAP
    },

    /* LSPP_SOURCE_UDP_PORT_MISMATCH */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s :Source Port Mismatch.Packet received with Invalid "
            "source port %d!!!\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_ECHO_MATCH_ENTRY_NOT_FOUND */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_NOTICE_LEVEL, 
        "\r\n %s :Match Entry Not Found!!!\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_PROCESS_REQUEST_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s :1.Reqeust 2.Reply\n Processing %d Failed !!!\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_SEND_BFD_RESPONSE_FAILURE */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s :Sending BFD Discriminator in Ingress Failed!!!\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_PKT_RECEIVED_WITH_INVALID_LBL_COUNT  */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s :Packet received with Invalid Label count %d!!!\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_GET_PATH_ID_FROM_LABEL_FAILED  */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s :Get Path Id from Bottol label %d Failed!!!\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_GET_PATH_INFO_FROM_PATH_ID_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s :Get Path Info from Path Id Failed!!!\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_CHANNEL_TYPE */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s :Packet Received with Invalid channel type %d!!!\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_IP_UDP_HEADER_VALIDATION_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s :IP UDP Header Validation Failed!!!\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_GET_ACH_TLV_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s :Get Ach Tlv from Packet Failed!!!\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_VALIDATE_ACH_TLV_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s :Validation of Ach Tlv Failed!!!\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_PKT_RECEVIED_WITH_INVALID_ENCAP */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s :Packet received with unknown encapsulation!!!\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_PW_IP_VERSION_VERIFICATION_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s :VCCV Source IP Version Validation Failed!!!\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_PW_SRC_IP_VERIFICATION_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s :VCCV Source IP Validation Failed!!!\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_VCCV_VERIFICATION_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s :VCCV verificaiton failed!!!\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_HEADER_VALIDATION_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s :Lspp Ping Header Validation Failed.Validation Result %d!!!\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_DECODING_TLV_FAILED  */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s :Decode and Validate TLVs failed !!!\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_MALFORMED_REQ_RECEIVED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_WARN_LEVEL, 
        "\r\n %s :Malformed Echo request received. "
            "If Lbl Stck Tlv is present or Dsmap TLV is present Twice!!!\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_IF_LBL_STK_ADDR_TYPE */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s :Packet received with Invalid Address Type %d in "
            " If Label Stack TLV\n",
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_PAD_TLV_FIRST_OCTET_VAL */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s :Invalid Pad TLV. Invalid First Octet Val %d\n",
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_PAD_TLV_FIRST_OCTET_VAL_IN_RPLY */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s :Invalid Pad TLV in echo reply. Invalid First Octet Val %d\n",
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_REPLY_TOS_VAL */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s :Invalid Reply Tos TLV with Reply Tos Value %d\n",
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_DSMAP_TLV_ADDR_TYPE */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s :Invalid Dsmap TLV with Address Type %d\n",
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_DSMAP_TLV_DSFLAG */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s :Invalid Dsmap TLV with global flag Value %d\n",
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_DSMAP_MULTIPATH_TYPE */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s :Invalid Dsmap TLV with multipath type %d\n",
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_DSMAP_MULTIPATH_LEN */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s :Invalid Dsmap TLV with multipath length %d\n",
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_PROTOCOL_VERSION */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s :Invalid Protocol Header with Version %d\n",
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_GLOBAL_FLAG */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s :Invalid Protocol Header with global flag value %d\n",
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_MESSAGE_TYPE */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s :Invalid Protocol Header with message Type %d\n",
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_REPLY_MODE */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s :Invalid Protocol Header with reply mode %d\n",
        LSPP_INVALID_TRAP
    },

    /* LSPP_INVALID_REPLY_MODE_FOR_ENCAP */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s :Invalid Reply mode %d in header for Encapsulation %d\n",
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_REQ_WITH_RC_OR_RSC */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s :Echo request received with Invalid Return Code %d or "
            " Return SubCode %d!!!\n",
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_REPLY_WITH_RC_OR_RSC */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s :Echo reply received with Invalid Return Code or Return "
            "Sub code!!!\n",
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_UNKNOWN_SUBTLV_RECEIVED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_NOTICE_LEVEL, 
        "\r\n %s :Packet received with not understandable Target Fec Stack "
            "Tlv of type %d!!!\n",
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_SUBTYPE_RECEIVED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s :Packet received with invalid Target Fec Stack "
            "Tlv of type %d!!!\n",
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_DSMAP_VALIDATION_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s :Dsmap Tlv Validation Failed\n",     
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_IF_LBL_STK_VALIDATION_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s :Interface Label Stack Tlv Validation Failed\n",     
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_PAD_TLV_VALIDATION_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s :Pad Tlv Validation Failed\n",     
        LSPP_INVALID_TRAP
    },

    /* LSPP_INVALID_REPLY_TOS_IN_REPLY */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s :Reply Tos Tlv Received in reply packet\n",     
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_REPLY_TOS_TLV_VALDIATION_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s :Reply Tos Tlv validation failed\n",     
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_VENDOR_TLV_NOT_UNDERSTANDABLE */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_NOTICE_LEVEL,
        "\r\n %s :Packet received with not understadable Vendor Address Tlv\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_TLV_TYPE */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s :Packet received with Invalid Tlv Type %d\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_PINGTRACE_ENTRY_NOT_FOUND */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC),
        SYSLOG_WARN_LEVEL,
        "\r\n %s :Ping Trace Entry With Senders Handle %d not found\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_ECHO_SEQ_ENTRY_NOT_FOUND */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC ),
        SYSLOG_WARN_LEVEL,
        "\r\n %s :Ping Trace Entry With Senders Handle %d  Sequence Entry %d "
            "not found \n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_ECHO_STATUS_ALREADY_COMPLETED*/
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC ),
        SYSLOG_NOTICE_LEVEL,
        "\r\n %s :Ping Trace Entry With Senders Handle %d  Sequence Entry %d "
            "already got completed \n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_ACH_LSP_MEP_VALIDATION_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC ),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s :ACH LSP MEP TLV validation failed\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_ACH_PW_MEP_VALIDATION_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC ),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s :ACH PW MEP TLV validation failed\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_ACH_ICC_MEP_VALIDATION_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC ),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s :ACH ICC MEP TLV validation failed\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_ACH_MIP_VALIDATION_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC ),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s :ACH MIP TLV validation failed\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_IP_HDR_LEN*/
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC ),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s :Invalid IP header Length\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_FETCH_FROM_EXT_MOD_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC ),
        SYSLOG_CRITICAL_LEVEL,
        "\r\n %s :Fetch From External Module Failed\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_DESTINATION_ADDR */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC ),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s :Invalid Destination Address in Ip Hdr of received pkt\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_IP_HDR_VALIDATION_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC ),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s :Ip Header validation Failed!!!\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_UDP_VALIDATION_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC ),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s :Udp Validation Failed.Invlaid UDP dest port!!!\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_CV_VALIDATION_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC ),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s :Cv Validation Failed !!!\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_CC_VALIDATION_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC ),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s :Cc Validation Failed !!!\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_LSP_MEP_ALREADY_PRESENT */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC ),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s :Lsp Mep Tlv already present in the received packet!!!\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_PW_MEP_ALREADY_PRESENT */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC ),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s :Pw Mep Tlv already present in the received packet!!!\n", 
        LSPP_INVALID_TRAP
    },

    /* LSPP_ICC_MEP_ALREADY_PRESENT */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC ),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s :Icc Mep Tlv already present in the received packet!!!\n", 
        LSPP_INVALID_TRAP
    },

    /* LSPP_MIP_ALREADY_PRESENT */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC ),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s :Mip Tlv already present in the received packet!!!\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_ACH_TLV_PRESENT */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC ),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s :Invalid Ach Tlv present in the received packet!!!\n", 
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_PATH_ID_TYPE */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_PKT_ERROR_TRC ),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s : Path type fetched from Ext module for the outermost label "
            "is invalid!!!\n", 
        LSPP_INVALID_TRAP
    },

    /* LSPP_EXT_TRIG_CXT_NOT_EXIST*/
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s : External trigger failed !!! \nContext specified by the"
            "external module not present\n",
        LSPP_INVALID_TRAP
    },

    /* LSPP_EXT_TRIG_RCVD_FROM_BFD*/
    {
        (LSPP_EVENT_TRC),
        SYSLOG_INFO_LEVEL, 
        "\r\n %s : External trigger received from BFD with "
            "Context ID : %u \n Session : %u \n Discriminator : %u\n",
        LSPP_INVALID_TRAP,
    },

    /* LSPP_BFD_DISCRIMINATOR_FROM_REMOTE */
    {
        (LSPP_EVENT_TRC),
        SYSLOG_INFO_LEVEL, 
        "\r\n %s : BFD Discriminator received from remote "
            "Context ID : %u \n Remote Discriminator : %u\n",
        LSPP_INVALID_TRAP,
    },
    
    /* LSPP_BFD_LOCAL_DISCRIMINATOR_FOR_REPLY */
    {
        (LSPP_EVENT_TRC),
        SYSLOG_INFO_LEVEL, 
        "\r\n %s : BFD Local Discriminator to be sent in the reply"
            "Context ID : %u \n Local Discriminator : %u\n",
        LSPP_INVALID_TRAP,
    },

    /* LSPP_BFD_DISCRIMINATOR_FROM_REMOTE_FOR_SESSION */
    {
        (LSPP_EVENT_TRC),
        SYSLOG_INFO_LEVEL, 
        "\r\n %s : BFD Discriminator received from remote "
            "Context ID : %u \n Session : %u \n Remote Discriminator : %u\n",
        LSPP_INVALID_TRAP,
    },

    /* LSPP_EXT_TRIG_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s : External trigger failed !!! \n",
        LSPP_INVALID_TRAP
    },
    /*  LSPP_PINGTRACE_TABLE_INIT_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s : Ping trace table initialization failed\n",
        LSPP_INVALID_TRAP
    },

    /* LSPP_EXT_TRIG_GET_MEG_NAME_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s : Unable to get Meg and ME name from the Meg Indices"
            "provided by the external modules\n",
        LSPP_INVALID_TRAP
    },

    /* LSPP_EXT_TRIG_GET_OID_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s : Unable to get the Path OID for the Path Indices"
            "provided by the external modules\n",
        LSPP_INVALID_TRAP
    },

    /* LSPP_PINGTRACE_ENTRY_CREATION_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s : Creation of new Ping Trace entry failed\n",
        LSPP_INVALID_TRAP
    },

    /* LSPP_EXT_TRIG_ECHO_SEND_FAILED*/
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s : Unable to send Echo request for the extranal triggger\n",
        LSPP_INVALID_TRAP
    },

    /* LSPP_BFD_API_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s : API function provided by BFD for processing the Bootstrap"
            "Information failed\n",
        LSPP_INVALID_TRAP
    },


    /* LSPP_SYSLOG_REGISTER_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s : Register with Syslog failed\n",
        LSPP_INVALID_TRAP
    },

    /* LSPP_VCM_REGISTER_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s : Register with VCM failed\n",
        LSPP_INVALID_TRAP
    },

    /* LSPP_VCM_DEREGISTER_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s : De-Register with VCM failed\n",
        LSPP_INVALID_TRAP
    },


    /* LSPP_GET_CXT_NAME_FRM_ID_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s : Get context name from the given context Id failed\n",
        LSPP_INVALID_TRAP
    },

    /* LSPP_GET_CXT_ID_FROM_IF_INDEX_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL, 
        "\r\n %s : Get context Id from the given Interface index failed\n",
        LSPP_INVALID_TRAP
    },

    /* LSPP_CONTEXT_NOT_EXIST */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
         SYSLOG_ERROR_LEVEL, 
         "\r\n %s : Context does not exist for the Context name : %d\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_GET_INTF_MTU_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
         SYSLOG_ERROR_LEVEL, 
         "\r\n %s : Unable to get MTU for the given interface index\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_GET_INTF_TYPE_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
         SYSLOG_ERROR_LEVEL, 
         "\r\n %s : Unable to get the type of the given interface index\n",
         LSPP_INVALID_TRAP
    },

    /* LSPP_GET_MPLS_INTF_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
         SYSLOG_ERROR_LEVEL, 
         "\r\n %s : Unable to get the MPLS interface from the given "
         "MPLS tunnel interface\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_GET_L3_INTF_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
         SYSLOG_ERROR_LEVEL, 
         "\r\n %s : Unable to get the L3 interface from the given "
         "MPLS interface\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_GET_IP_ADDR_FRM_INTF_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
         SYSLOG_ERROR_LEVEL, 
        "\r\n %s : Unable to get the IP address from the given interface index\n",
         LSPP_INVALID_TRAP
    },

    /*  LSPP_GET_IF_INDEX_FROM_IP_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
         SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s : Unable to get the Interface index from the "
            "given IP address\n",
         LSPP_INVALID_TRAP
    },

    
    /*  LSPP_GET_SRC_IP_FRM_DEST_IP_FAILED  */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
         SYSLOG_ERROR_LEVEL, 
        "\r\n %s : Unable to get the source Ip address from the Destination IP "
        "address\n",
         LSPP_INVALID_TRAP
    },

    /*  LSPP_MPLS_API_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
         SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s : MPLS API failed !!! \nUnable to get %d\n",
         LSPP_INVALID_TRAP
    },

    /*  LSPP_FM_TRAP_SEND_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
         SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s : Trap send failed !!!\n",
         LSPP_INVALID_TRAP
    },

    /*  LSPP_UNKNOWN_REQ_TYPE_IN_PORT_FUNC */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
         SYSLOG_NOTICE_LEVEL, 
        "\r\n %s : Unknown request type in external port function !!!\n",
         LSPP_INVALID_TRAP
    },

    /*  LSPP_GLOBAL_STATS_ENTRY_NOT_EXIST */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
         SYSLOG_ERROR_LEVEL, 
        "\r\n %s : Global stats entry not exist for context %d !!!\n",
         LSPP_INVALID_TRAP
    },
    
    
    /*  LSPP_RESTART_AGE_OUT_TIMER_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
         SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s : Re-start age-out timer failed !!!\n",
         LSPP_INVALID_TRAP
    },
    
    /*  LSPP_MANDATORY_FIELDS_NOT_PRESENT */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
         SYSLOG_ERROR_LEVEL, 
        "\r\n %s : Mandatory fields are not configured in the Ping trace table\n",
         LSPP_INVALID_TRAP
    },
    
    
    /*  LSPP_GET_PATH_ID_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
         SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s : Unbale to get the %s Path Id to filled in "
            "ping trace entry\n",
         LSPP_INVALID_TRAP
    },
    
    /*  LSPP_INVALID_PATH_TYPE_IN_ENTRY */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
         SYSLOG_ERROR_LEVEL, 
        "\r\n %s : Invalid path type is present in the ping trace entry\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_DEL_PINGTRACE_NODE_NOT_ALLOWED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
         SYSLOG_WARN_LEVEL, 
        "\r\n %s : Deletion of ping trace entry through SNMP is not allowed"
         "if the OWNER is %d\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_CRU_BUF_UDP_HDR_PREPEND_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
         SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s :Prepending Cru Buf with UDP header Failed\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_CRU_UDP_CHCKSUM_UPDATION_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
         SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s :Checksum Updation in Udp Header Failed\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_CRU_BUF_IP_HDR_PREPEND_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
         SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s :Prepending Cru Buf with IP header Failed\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_CRU_IP_CHCKSUM_UPDATION_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
         SYSLOG_ERROR_LEVEL, 
        "\r\n %s :Checksum Updation in Ip Header Failed\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_LABEL_COUNT */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
         SYSLOG_ERROR_LEVEL, 
        "\r\n %s :Invalid Label count Will Cause buffer overflow\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_UPDATING_LBL_STK_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
         SYSLOG_ERROR_LEVEL, 
        "\r\n %s :Updating %d Label cause buffer overflow\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_UPDATING_IP_UDP_HDR_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
         SYSLOG_ERROR_LEVEL, 
        "\r\n %s :Updating Ip Udp Header Failed\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_UPDATING_OUTGOING_LBL_STK_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
         SYSLOG_ERROR_LEVEL, 
        "\r\n %s : Updating Outgoing Label Stack Failed\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_TLV_SUBTYPE_TO_FRAME */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
         SYSLOG_ERROR_LEVEL, 
        "\r\n %s : Invalid Tlv Sub type is requested to frame\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_FRAMING_PING_PAYLOAD_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
         SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s :Framing Ping Payload Failed\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_SENDING_PKT_TO_BFD_FAILED*/
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
         SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s :Transmitting packet to BFD failed\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_SENDING_PKT_ON_SOCK_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
         SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s :Transmitting on socket failed\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_ENCAP_PDU_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
         SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s :Encapsulating Header failed\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_TRANSMIT_PDU_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
         SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s :Transmitting PDU TO MPLS_RTR failed\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_ECHO_SEQ_NODE_CREATE_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
         SYSLOG_CRITICAL_LEVEL, 
        "\r\n %s :Echo Seq node Creation Failed\n",
         LSPP_INVALID_TRAP
    },

    /* LSPP_DECODE_TLV */
    {
        LSPP_TLV_TRC,
        SYSLOG_DEBUG_LEVEL,
        "\r\n %s: Type : %d \nLength : %d\n",
         LSPP_INVALID_TRAP
    },

    /* LSPP_DECODE_IPV4_LDP_FEC */
    {
        LSPP_TLV_TRC,
        SYSLOG_DEBUG_LEVEL,
        "\r\n %s: LDP IPV4 Prefix\n LDP Prefix :%d\n Prefix Length :%d\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_DECODE_IPV6_LDP_FEC */
    {
        LSPP_TLV_TRC,
        SYSLOG_DEBUG_LEVEL,
        "\r\n %s: LDP IPV6 Prefix\n LDP Prefix :%s\n Prefix Length :%d\n",
         LSPP_INVALID_TRAP
    },

    /* LSPP_DECODE_IPV4_RSVP_FEC */
    {
        LSPP_TLV_TRC,
        SYSLOG_DEBUG_LEVEL,
        "\r\n %s: RSVP IPV4 LSP\n"
            "Tunnel End-point address : %d\nTunnel Id : %d\n"
            "Extended Tunnel Id : %d\n"
            "Tunnel Sender address : %d\nLSP Number :%d\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_DECODE_IPV6_RSVP_FEC */
    {
        LSPP_TLV_TRC,
        SYSLOG_DEBUG_LEVEL,
        "\r\n %s: RSVP IPV6 LSP\n"
            "Tunnel End-point address : %s\nTunnel Id : %d\n"
            "Extended Tunnel Id : %s\n"
            "Tunnel Sender address : %s\nLSP Number :%d\n",
         LSPP_INVALID_TRAP
    },

    /* LSPP_DECODE_FEC_PW_DEPREC */
    {
        LSPP_TLV_TRC,
        SYSLOG_DEBUG_LEVEL,
        "\r\n %s: FEC 128 Deprecated Tlv\n"
            "Remote PE address :%d\nPW-Id :%d\nPW-type :%d\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_DECODE_FEC_128_PW */
    {
        LSPP_TLV_TRC,
        SYSLOG_DEBUG_LEVEL,
        "\r\n %s: FEC 128 Tlv\nsource PE address :%d\n"
            "Remote PE address :%d\nPW-Id :%d\nPW-type :%d\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_DECODE_FEC_129_PW */
    {
        LSPP_TLV_TRC,
        SYSLOG_DEBUG_LEVEL,
        "\r\n %s: FEC 129 Tlv\nsource PE address :%d\n"
            "Remote PE address :%d\nPW-Id :%d\nAgi Type :%d\n"
        "Agi Len :%d\nAgiValue :%s\nSaii Type %d\nSaii Len:%d\n"
        "Saii Value :%s\nTaii Type :%d\nTaii Len:%d\n,Taii Val:%s\n",
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_DECODE_NIL_FEC */
    {
        LSPP_TLV_TRC,
        SYSLOG_DEBUG_LEVEL,
        "\r\n %s: Nil Fec Tlv\n Label %d\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_DECODE_STATIC_LSP_FEC */
    {
        LSPP_TLV_TRC,
        SYSLOG_DEBUG_LEVEL,
        "\r\n %s: Static LSP Fec\n"
            "Source Global Id:%d\nSource Node Id:%d\nSource Tnl Num:%d\n"
            "Source Lsp Id :%d\nDest Global Id:%d\nDest Node Id:%d\n"
            "Dest Tnl Num:%d\n",
         LSPP_INVALID_TRAP
    },

    /* LSPP_DECODE_STATIC_PW_FEC */
    {
        LSPP_TLV_TRC,
        SYSLOG_DEBUG_LEVEL,
        "\r\n %s: Static Pw Fec\n"
            " Agi Type :%d\nAgi Len:%d\nAgi Value:%s\n"
            "Source Global Id:%d\nSource Node Id:%d\nSource Ac_ID %d\n"
            "Dest Global Id:%d\nDest Node Id:%d\n Dest Ac_ID:%d\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_DECODE_IPV4_DSMAP_TLV */ 
    {
        LSPP_TLV_TRC,
        SYSLOG_DEBUG_LEVEL,
        "\r\n %s: DSMAP TLV\nMTU: %d\nAddress-type : %d\nDS Flag : %d\n"
            "DS IP Address : %x\n DS Interface Address: %x\n"
            "Multipath Type: %d\nDepth Limit: %d\n Multipath Length: %d\n"
             "Multipath Info: %s\nDownstream Labels : %s\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_DECODE_IPV6_DSMAP_TLV */ 
    {
        LSPP_TLV_TRC,
        SYSLOG_DEBUG_LEVEL,
        "\r\n %s: DSMAP TLV\nMTU: %d\nAddress-type : %d\nDS IP Address : %s\n"
            "DS Interface Address: %s\nMultipath Type: %d\nDepth Limit: %d\n"
          "Multipath Length: %d\nMultipath Info: %s\nDownstream Labels : %s\n",
         LSPP_INVALID_TRAP
    },

    /* LSPP_DECODE_IPV4_IF_LBL_STK_TLV */ 
    {
        LSPP_TLV_TRC,
        SYSLOG_DEBUG_LEVEL,
        "\r\n %s: Address-type : %d\nIP Address : %x\nInterface Address: %x\n"
            " Label Stack: %s\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_DECODE_IPV6_IF_LBL_STK_TLV */ 
    {
        LSPP_TLV_TRC,
        SYSLOG_DEBUG_LEVEL,
        "\r\n %s: Address-type : %d\nIP Address : %s\nInterface Address: %s\n"
            " Label Stack: %s\n",
         LSPP_INVALID_TRAP
    },

    /* LSPP_DECODE_REPLY_TOS_TLV */
    {
        LSPP_TLV_TRC,
        SYSLOG_DEBUG_LEVEL,
        "\r\n %s: Reply Tos Tlv\nTos Value %d\n",
         LSPP_INVALID_TRAP
    },

    /* LSPP_DECODE_BFD_DISC_TLV */
    {
        LSPP_TLV_TRC,
        SYSLOG_DEBUG_LEVEL,
        "\r\n %s:BFD Dicriminator TLV \nDiscriminator Value %d\n",
         LSPP_INVALID_TRAP
    },
 
    /* LSPP_DECODE_PAD_TLV */
    {
        LSPP_TLV_TRC,
        SYSLOG_DEBUG_LEVEL,
        "\r\n %s:Pad TLV \nFirst Octet %d\n Pad Pattern: ",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_DECODE_ERROR_TLV */
    {
        LSPP_TLV_TRC,
        SYSLOG_DEBUG_LEVEL,
        "\r\n %s:Error TLV \n Error Value : ",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_GET_LABEL_INFO_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Getting Label Info Failed !!!\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_LABEL_ENTRY_NOT_FOUND */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: No mapping found for label %d at stack depth %d !!!\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_SEND_ECHO_REPLY_FAILED*/
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL,
        "\r\n %s: Send Echo Reply Failed!!!\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_LABEL_ENTRY_NOT_FOUND_FOR_ECHO_REPLY */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Echo reply Validation failed as No mapping "
            "found for label !!!\n",
         LSPP_INVALID_TRAP
    },

    /* LSPP_LABEL_ACTION_INVALID */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Invalid action for the label : %u at stack depth : %u \n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_EGRESS_PROCESSING_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Egress Processing Failed!!!\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_SEND_TRACEROUTE_REQ_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL,
        "\r\n %s: Send TraceRotue Req failed!!!\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_AGEOUT_TIMER_START_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL,
        "\r\n %s: Starting AgeOut Timer failed!!!\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_CONTEXT_ID */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Invalid Context Id %d!!!\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_PATH_ID_OR_PATH_INFO */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Path Id or Path Info given as Input is Null !!!\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_FETCH_MEG_INFO_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL,
        "\r\n %s: Fetch Meg Info Failed !!!\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_FETCH_PW_INFO_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL,
        "\r\n %s: Fetch Pw Info Failed !!!\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_FETCH_TNL_INFO_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL,
        "\r\n %s: Fetch Tunnel Info Failed !!!\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_FETCH_LSP_INFO_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL,
        "\r\n %s: Fetch LSP Info Failed !!!\n",
         LSPP_INVALID_TRAP
    },

    /* LSPP_NONTE_LSP_FOR_PW_INVALID */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL,
        "\r\n %s: PW under Non-Te LSP is currently not supported\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_PW_CV */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Lsp Ping is not the configured CV for this Pw!!!\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_UPDATE_TARGET_FEC_STACK_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Updation of Target FEC stack Failed!!!\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_UPDATE_OUTSEG_INFO_STACK_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Updation of Out Path Info Failed!!!\n",
         LSPP_INVALID_TRAP
    },

    /* LSPP_UPDATE_OUT_LABEL_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Updation of Outgoing label stack Failed!!!\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_UPDATE_DSMAP_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Updation of DSMAP TLV failed!!!\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_UPDATE_NIL_FEC_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Updation of Nil Fec failed!!!\n",
         LSPP_INVALID_TRAP
    },

    /* LSPP_UPDATE_ENCAP_HEADER_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Filling Encapsulation details failed!!!\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_PACKET_SIZE_CONFIGURED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Configured Packet Size is less when compared to "
            "total packet size including out label stack!!!\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_PATH_TYPE_FOR_TARGET_FEC_UPDATAION */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Invalid Path Type for targer Fec Stack Updation\n",
         LSPP_INVALID_TRAP
    },

    /* LSPP_INVALID_PATH_TYPE_FOR_OUTSEG_INFO_UPDATAION */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Invalid Path Type for Out Seg Info Updation\n",
         LSPP_INVALID_TRAP
    },

    /* LSPP_GET_L3_IF_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Fetching L3 Interface from Tunnel Interface Failed\n",
         LSPP_INVALID_TRAP
    },

    /* LSPP_GET_BFD_BOOTSTRAP_RESP_REQ_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL,
        "\r\n %s: Getting BFD Resposne Required Information failed\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_UPDATE_ECHO_MSG_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL,
        "\r\n %s: Updating Echo Message Failed\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_SEND_ECHO_REQ_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL,
        "\r\n %s: Sending next Echo request Failed\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_ALLOC_HOP_ENTRY_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL,
        "\r\n %s: Allocating Hop Entry with Hop Index %d Failed\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_PING_ENTRY_FOR_ECHO_REQUEST */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Invalid Ping Trace or Echo sequence entry in request\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_PROCESS_REQ_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Processing Echo request/reply failed\n",
         LSPP_INVALID_TRAP
    },

    /* LSPP_FEC_LBL_VALIDATE_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Fec Label Validation failed\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_ECHO_SEQ_ALREADY_COMPLETED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_NOTICE_LEVEL,
        "\r\n %s: Echo seq with sequence number %d already completed\n",
         LSPP_INVALID_TRAP
    },

    /* LSPP_ECHO_ALREADY_COMPLETED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_NOTICE_LEVEL,
        "\r\n %s: PingTrace Entry with sender handle %d already completed\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_PATH_TYPE */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Invalid path type\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_RETURN_CODE */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Invalid return code to update\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_UPDATE_PARAM */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Invalid Updation param\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_SWAP_DEPTH */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Invalid Label Swap Depth\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_PW_PATH_TYPE */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Invalid Pw Path Type at intermediate node\n",
         LSPP_INVALID_TRAP
    },
   
    /* LSPP_INVALID_MEG_SERVICE_TYPE */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL,
        "\r\n %s: Invalid Meg Service Type at intermediate node\n",
         LSPP_INVALID_TRAP
    },

    /* LSPP_INVALID_DSMAP_PATH_TYPE */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Invalid Path Type while Fetching Dsmap Info\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_GET_IF_LBL_STK_INFO_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Fetching If Lbl Stk Inforaamtion failed\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_GET_DSMAP_INFO_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Fetching DSMAP Tlv Information failed\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_NO_IP_ROUTE_EXIST */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL,
        "\r\n %s: No Ip route exist for this destiantion\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_NO_REVERSE_PATH */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL,
        "\r\n %s: No reverse path exist for the destiantion\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_GET_REVERSE_PATH_FROM_FWD_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL,
        "\r\n %s: Get reverse path from forward path failed\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_VALIDATING_FEC_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Fec Validaiton Failed!!!\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_FETCH_IF_INFO_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Fectch Interface Info Failed!!!\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_FETCH_ROUTER_ID_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Fectch Router Id Failed!!!\n",
         LSPP_INVALID_TRAP
    },
   
    /* LSPP_ENCAP_SRC_IP_OR_IP_VERSION_INVALID */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Fetched Source IP address or IP version invalid.\n",
         LSPP_INVALID_TRAP
    },

    /* LSPP_MISMATCH_IN_DSMAP_IP */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Dsmap IP %d doesnt match with received Interface IP!!!\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_MISMATCH_IN_DSMAP_LABEL */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Dsmap Label Stack doesnt match with received label stack!!!\n",
         LSPP_INVALID_TRAP
    },
   
    /* LSPP_MISMATCH_IN_DSMAP_LBL_STK_PROTOCOL */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Protocol mismatch in Dsmap Label stack\n",
        LSPP_INVALID_TRAP
    },

    /* LSPP_MISMATCH_IN_DSMAP_LBL_AND_RX_LBL_COUNT */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Dsmap Label count %d is less than Rx label Count %d !!!\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_FEC_OR_LBL_STK_DEPTH */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Invalid label count %d or Invalid Fec stack count %d !!!\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_FEC_LBL_MAPPING_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Fec to Label Mapping Failed !!!\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_FEC_TYPE_FOR_LABEL_MAPPING */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Invalid Fec type for Label mapping !!!\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_LABEL_FOR_FEC_MAPPING */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Label Fetched for the FEC of type %d is invalid !!!\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_RX_LABEL_FOR_FEC */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Mismatch in feched label %d and received label %d!!!\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_PROTOCOL_VALIDAITON_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Protcol valdiaiotn failed\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_MPLS_PATH_TYPE */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Path type %d fetched for label %d is invalid\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_FEC_TYPE_TO_CALC_PDU */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Invalid FEC Type %d to given as input to caclulate PDU Size\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_PATH_TYPE_TO_FETCH_OID */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Invalid Path ID Type %d to fetch OID\n",
         LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_OID_LEN */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: OID Fetched is of invalid length %d\n",
         LSPP_INVALID_TRAP
    },
    
    /*  LSPP_INVALID_MSG_TYPE */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_NOTICE_LEVEL,
        "\r\n %s: Invalid request type from external module\n",
        LSPP_INVALID_TRAP
    },

    /*  LSPP_CFG_INVALID_FOR_PW */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Invalid options has been configured for pseudowire\n",
        LSPP_INVALID_TRAP
    },
    /* LSPP_INTER_CV_CFG_INVALID */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Mandatory fields for intermediate connectivity "
            "verification are not configured\n",
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_PKT_RECEIVED */
    {
        LSPP_CONTROL_PLANE_TRC,
        SYSLOG_DEBUG_LEVEL,
        "\r\n %s: Packet received for Processing\n",
        LSPP_INVALID_TRAP
    },  
    
    /* LSPP_WFR_TIMER_STARTED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_MGMT_TRC),
        SYSLOG_DEBUG_LEVEL,
        "\r\n %s: WFR timer started\n",
        LSPP_INVALID_TRAP
    },
 
    /* LSPP_WTS_TIMER_STARTED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_MGMT_TRC),
        SYSLOG_DEBUG_LEVEL,
        "\r\n %s: WTS timer started\n",
        LSPP_INVALID_TRAP
    },

    /* LSPP_TX_PKT_FAIL */
    {
        (LSPP_ALL_FAILURE_TRC),
        SYSLOG_CRITICAL_LEVEL,
        "\r\n %s: Transmitting packet failed\n",
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_PING_TRACE_INFO_DELETE_FAIL */
    {
        (LSPP_ALL_FAILURE_TRC),
        SYSLOG_CRITICAL_LEVEL,
        "\r\n %s: Ping Trace Information deletion failed\n",
        LSPP_INVALID_TRAP
    },
 
    /* LSPP_PING_TRACE_NODE_DELETE_FAIL */
    {
        (LSPP_ALL_FAILURE_TRC),
        SYSLOG_CRITICAL_LEVEL,
        "\r\n %s: Ping Trace Node deletion failed\n",
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_GLOBAL_CONFIG_CREATION_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC),
        SYSLOG_CRITICAL_LEVEL,
        "\r\n %s: Global Config entry creation Failed\n",
        LSPP_INVALID_TRAP
    },
 
    /* LSPP_INDEX_MGR_INIT_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL,
        "\r\n %s: Index Manager Init Failed\n",
        LSPP_INVALID_TRAP
    },    
    
    /* LSPP_GLOBAL_STAT_CREATE_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL,
        "\r\n %s: Global Stats configuration failed\n",
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_CREATE_GLOBAL_CONFIG_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL,
        "\r\n %s: Creating Global Config Table Failed\n",
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_CREATE_GLOBAL_STATS_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL,
        "\r\n %s: Creating Global Config Table Failed\n",
        LSPP_INVALID_TRAP
    },
     
    /* LSPP_DELETE_GLOBAL_CONFIG_FAIL */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL,
        "\r\n %s: Deleting Global Config Table Failed\n",
        LSPP_INVALID_TRAP
    },

    /* LSPP_DELETE_GLOBAL_STAS_FAIL */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_CRITICAL_LEVEL,
        "\r\n %s: Deleting Global Config Table Failed\n",
        LSPP_INVALID_TRAP
    },

    /* LSPP_DEFAULT_CXT_DELETION_NOT_ALLOWED */

    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_WARN_LEVEL,
        "\r\n %s:Changing the module Status of Default context "
            "to shutdown is not allowed\n",
        LSPP_INVALID_TRAP
    },

    /* LSPP_MODULE_STATUS_DOWN_IN_CXT */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_WARN_LEVEL,
        "\r\n %s: LSPP Module is shutdown in the context : %u\n",
        LSPP_INVALID_TRAP
    },


    /* LSPP_ECHO_SEQ_ENTRY_FOUND */
    {
        (LSPP_EVENT_TRC),
        SYSLOG_DEBUG_LEVEL,
        "\r\n %s:Ping trace entry found with Context id : %u,"
        "Sender handle : %u, Sequence Number: %u\n",
        LSPP_INVALID_TRAP
     },

    /* LSPP_AGEOUT_TIMER_STARTED */
    {
        LSPP_EVENT_TRC,
        SYSLOG_DEBUG_LEVEL,
        "\r\n %s:Age out timer started for PingTrace entry with "
            "Context id : %u, Sender Handle : %u\n",
        LSPP_INVALID_TRAP
    },

    /* LSPP_REQ_TIMED_OUT_WFR_TIMER */
    {
        (LSPP_EVENT_TRC),
         SYSLOG_INFO_LEVEL,
        "\r\n %s: WFR Timer expired for Sequence entry with Context Id: %u,"
            " Sender Handle : %u and Sequence Num: %u\n",
        LSPP_INVALID_TRAP
    },

    /* LSPP_WFR_TIMER_STOPPED */
    {
        (LSPP_EVENT_TRC),
         SYSLOG_DEBUG_LEVEL,
         "\r\n %s: WFR Timer stopped\n",
         LSPP_INVALID_TRAP
    },

    /* LSPP_WTS_TIMER_EXPIRED */
    {
        (LSPP_EVENT_TRC),
         SYSLOG_INFO_LEVEL,
        "\r\n %s: WTS Timer expired for Ping request with Context Id : %u,"
        " Sender Handle : %u\n",
        LSPP_INVALID_TRAP
    },

    /* LSPP_AGE_OUT_TIMER_EXPIRED */
    {
        (LSPP_EVENT_TRC),
         SYSLOG_INFO_LEVEL,
         "\r\n %s: Age out timer expired for PingTrace entry with "
             "Context id : %u, Sender Handle : %u\n",
         LSPP_INVALID_TRAP
    },

    /* LSPP_REPLY_MODE_INVALID_FOR_DSCP */
    {
        LSPP_ALL_FAILURE_TRC,
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Reply mode should be IP/UDP or IP/UDP with router alert when"
            "DSCP is configured\n",
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_SWEEP_MIN_GREATER_THAN_MAX */
    {
        LSPP_ALL_FAILURE_TRC,
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Sweep Minimum value should not be greater than sweep "
            "maximum value\n",
        LSPP_INVALID_TRAP
    },

    /* LSPP_FEC_VALIDATION_SUCCEEDED */
    {
        (LSPP_EVENT_TRC),
        SYSLOG_INFO_LEVEL,
        "\r\n %s: Validation of Fec is successful \n",
        LSPP_INVALID_TRAP
    },
  
    /* LSPP_FETCH_PW_SRC_IP_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Fetching Pw Source Ip Address Failed\n",
        LSPP_INVALID_TRAP
    },

    /* LSPP_IP_CHECKSUM_VALIDATION_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Ip Checksum Validation failed\n",
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_UDP_CHECKSUM_VALIDATION_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Udp Checksum Validation failed\n",
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_IP_HDR */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Invalid Ip header received\n",
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_SRC_IP_IN_INVALID_RANGE */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Source Ip in Invalid range\n",
        LSPP_INVALID_TRAP
    },

    /* LSPP_DEST_IP_IN_INVALID_RANGE */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Destination Ip in Invalid range\n",
        LSPP_INVALID_TRAP
    },

    /* LSPP_INVALID_IP_HEADER_IN_REQ */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Invalid Ip header or Ip TTl \n",
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_SRC_IP_IN_REPLY */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Invalid source Ip in echo reply \n",
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_DEST_IP_IN_REPLY */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Dest Ip in echo reply is not my Ip \n",
        LSPP_INVALID_TRAP
    },
    
    /* LSPP_INVALID_SERVICE_OID_LEN */ 
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Invalid Service Oid Length\n",
        LSPP_INVALID_TRAP
    },

    /* LSPP_REVERSE_PATH_VALIDATE_FAILED */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Packet received in invalid reverse path\n",
        LSPP_INVALID_TRAP
    },

    /*LSPP_INTERFACE_VALIDAITON_FAILED */    
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Interface not associated with this protocol\n",
        LSPP_INVALID_TRAP
    },    
    /* LSPP_SKIPPING_DSMAP_IF_VALIDATION */
    {
        LSPP_EVENT_TRC,
        SYSLOG_INFO_LEVEL,
        "\r\n %s: Dsmap Interface Validation Skipped\n",
        LSPP_INVALID_TRAP
    },

    /* LSPP_SKIPPING_DSMAP_LBL_VALIDATION */
    {
        LSPP_EVENT_TRC,
        SYSLOG_INFO_LEVEL,
        "\r\n %s: Dsmap Label Validation Skipped\n",
        LSPP_INVALID_TRAP
    },
    /*LSPP_UNDERLYING_PATH_DOWN */
    {
        (LSPP_ALL_FAILURE_TRC | LSPP_ERROR_TRC),
        SYSLOG_ERROR_LEVEL,
        "\r\n %s: Path fetched is down\n",
        LSPP_INVALID_TRAP
    },
    /* LSPP_SKIPPING_DSMAP_LBL_IF_VALIDATION */
    {
        LSPP_EVENT_TRC,
        SYSLOG_INFO_LEVEL,
        "\r\n %s: Dsmap Label Validation Skipped and Dsmap Interface Validation Skipped\n",
        LSPP_INVALID_TRAP
    } 
};

#else
extern tLsppEventLogNotify     gaLsppIssEventLogNotify[ ];
extern tLsppEventLogNotify     gaLsppEventLogNotify[ ];
#endif

#ifdef TRACE_WANTED
#define LSPP_CRUBUF_DUMP_PKT(pMsg, u4PktLen)  DumpPkt(pMsg, u4PktLen)
#else
#define LSPP_CRUBUF_DUMP_PKT(pMsg, u4PktLen)
#endif

#endif /* _LSPPTRC_H */


/*-----------------------------------------------------------------------*/
/*                       End of the file  lspptrc.h                      */
/*-----------------------------------------------------------------------*/
