/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: lsppmibclig.h,v 1.4 2011/10/25 09:29:33 siva Exp $
*
* Description: Extern Declaration of the Mib objects
*********************************************************************/


/* extern declaration corresponding to OID variable present in protocol db.h */


extern UINT4 FsLsppSystemControl[13];

extern UINT4 FsLsppTrapStatus[13];

extern UINT4 FsLsppTraceLevel[13];

extern UINT4 FsLsppAgeOutTime[13];

extern UINT4 FsLsppAgeOutTmrUnit[13];

extern UINT4 FsLsppClearEchoStats[13];

extern UINT4 FsLsppBfdBtStrapRespReq[13];

extern UINT4 FsLsppBfdBtStrapAgeOutTime[13];

extern UINT4 FsLsppBfdBtStrapAgeOutTmrUnit[13];

extern UINT4 FsLsppContextId[13];


extern UINT4 FsLsppRequestType[13];

extern UINT4 FsLsppPathType[13];

extern UINT4 FsLsppPathPointer[13];

extern UINT4 FsLsppTgtMipGlobalId[13];

extern UINT4 FsLsppTgtMipNodeId[13];

extern UINT4 FsLsppTgtMipIfNum[13];

extern UINT4 FsLsppReplyMode[13];

extern UINT4 FsLsppRepeatCount[13];

extern UINT4 FsLsppPacketSize[13];

extern UINT4 FsLsppPadPattern[13];

extern UINT4 FsLsppTTLValue[13];

extern UINT4 FsLsppWFRInterval[13];

extern UINT4 FsLsppWFRTmrUnit[13];

extern UINT4 FsLsppWTSInterval[13];

extern UINT4 FsLsppWTSTmrUnit[13];

extern UINT4 FsLsppReplyDscpValue[13];

extern UINT4 FsLsppSweepOption[13];

extern UINT4 FsLsppSweepMinimum[13];

extern UINT4 FsLsppSweepMaximum[13];

extern UINT4 FsLsppSweepIncrement[13];

extern UINT4 FsLsppBurstOption[13];

extern UINT4 FsLsppBurstSize[13];

extern UINT4 FsLsppEXPValue[13];

extern UINT4 FsLsppDsMap[13];

extern UINT4 FsLsppFecValidate[13];

extern UINT4 FsLsppReplyPadTlv[13];

extern UINT4 FsLsppForceExplicitNull[13];

extern UINT4 FsLsppInterfaceLabelTlv[13];

extern UINT4 FsLsppSameSeqNumOption[13];

extern UINT4 FsLsppVerbose[13];

extern UINT4 FsLsppReversePathVerify[13];

extern UINT4 FsLsppEncapType[13];

extern UINT4 FsLsppRowStatus[13];


extern UINT4 FsLsppSequenceNumber[13];



extern UINT4 FsLsppHopIndex[13];


extern UINT4 FsLsppSenderHandle[13];

extern UINT4 FsLsppTrapContextName[11];


