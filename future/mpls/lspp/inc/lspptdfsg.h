/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: lspptdfsg.h,v 1.4 2011/05/17 13:07:22 siva Exp $
*
* Description: This file contains data structures defined for Lspp module.
*********************************************************************/
/* Structure used by CLI to indicate which 
 all objects to be set in FsLsppGlobalConfigTableEntry */

typedef struct
{
 BOOL1  bFsLsppContextId;
 BOOL1  bFsLsppSystemControl;
 BOOL1  bFsLsppTrapStatus;
 BOOL1  bFsLsppTraceLevel;
 BOOL1  bFsLsppAgeOutTime;
 BOOL1  bFsLsppAgeOutTmrUnit;
 BOOL1  bFsLsppClearEchoStats;
 BOOL1  bFsLsppBfdBtStrapRespReq;
 BOOL1  bFsLsppBfdBtStrapAgeOutTime;
 BOOL1  bFsLsppBfdBtStrapAgeOutTmrUnit;
} tLsppIsSetFsLsppGlobalConfigTableEntry;


/* Structure used by Lspp protocol for FsLsppGlobalConfigTableEntry */

typedef struct
{
 tRBNodeEmbd  FsLsppGlobalConfigTableNode;
 UINT4 u4FsLsppContextId;
 UINT4 u4FsLsppAgeOutTime;
 UINT4 u4FsLsppBfdBtStrapAgeOutTime;
 INT4 i4FsLsppSystemControl;
 INT4 i4FsLsppTrapStatus;
 INT4 i4FsLsppTraceLevel;
 INT4 i4FsLsppAgeOutTmrUnit;
 INT4 i4FsLsppClearEchoStats;
 INT4 i4FsLsppBfdBtStrapRespReq;
 INT4 i4FsLsppBfdBtStrapAgeOutTmrUnit;
} tLsppMibFsLsppGlobalConfigTableEntry;


/* Structure used by Lspp protocol for FsLsppGlobalStatsTableEntry */

typedef struct
{
 tRBNodeEmbd  FsLsppGlobalStatsTableNode;
 UINT4 u4FsLsppGlbStatReqTx;
 UINT4 u4FsLsppGlbStatReqRx;
 UINT4 u4FsLsppGlbStatReqTimedOut;
 UINT4 u4FsLsppGlbStatReqUnSent;
 UINT4 u4FsLsppGlbStatReplyTx;
 UINT4 u4FsLsppGlbStatReplyRx;
 UINT4 u4FsLsppGlbStatReplyDropped;
 UINT4 u4FsLsppGlbStatReplyUnSent;
 UINT4 u4FsLsppGlbStatReplyFromEgr;
 UINT4 u4FsLsppGlbStatUnLbldOutIf;
 UINT4 u4FsLsppGlbStatDsMapMismatch;
 UINT4 u4FsLsppGlbStatFecLblMismatch;
 UINT4 u4FsLsppGlbStatNoFecMapping;
 UINT4 u4FsLsppGlbStatUnKUpstreamIf;
 UINT4 u4FsLsppGlbStatReqLblSwitched;
 UINT4 u4FsLsppGlbStatReqUnSupptdTlv;
 UINT4 u4FsLsppGlbStatMalformedReq;
 UINT4 u4FsLsppGlbStatNoLblEntry;
 UINT4 u4FsLsppGlbStatPreTermReq;
 UINT4 u4FsLsppGlbStatProtMismatch;
 UINT4 u4FsLsppGlbStatRsvdRetCode;
 UINT4 u4FsLsppGlbStatNoRetCode;
 UINT4 u4FsLsppGlbStatUndefRetCode;
 UINT4 u4FsLsppGlbStatInvalidPktDropped;
 UINT4 u4FsLsppContextId;
} tLsppMibFsLsppGlobalStatsTableEntry;
/* Structure used by CLI to indicate which 
 all objects to be set in FsLsppPingTraceTableEntry */

typedef struct
{
 BOOL1  bFsLsppSenderHandle;
 BOOL1  bFsLsppRequestType;
 BOOL1  bFsLsppPathType;
 BOOL1  bFsLsppPathPointer;
 BOOL1  bFsLsppTgtMipGlobalId;
 BOOL1  bFsLsppTgtMipNodeId;
 BOOL1  bFsLsppTgtMipIfNum;
 BOOL1  bFsLsppReplyMode;
 BOOL1  bFsLsppRepeatCount;
 BOOL1  bFsLsppPacketSize;
 BOOL1  bFsLsppPadPattern;
 BOOL1  bFsLsppTTLValue;
 BOOL1  bFsLsppWFRInterval;
 BOOL1  bFsLsppWFRTmrUnit;
 BOOL1  bFsLsppWTSInterval;
 BOOL1  bFsLsppWTSTmrUnit;
 BOOL1  bFsLsppReplyDscpValue;
 BOOL1  bFsLsppSweepOption;
 BOOL1  bFsLsppSweepMinimum;
 BOOL1  bFsLsppSweepMaximum;
 BOOL1  bFsLsppSweepIncrement;
 BOOL1  bFsLsppBurstOption;
 BOOL1  bFsLsppBurstSize;
 BOOL1  bFsLsppEXPValue;
 BOOL1  bFsLsppDsMap;
 BOOL1  bFsLsppFecValidate;
 BOOL1  bFsLsppReplyPadTlv;
 BOOL1  bFsLsppForceExplicitNull;
 BOOL1  bFsLsppInterfaceLabelTlv;
 BOOL1  bFsLsppSameSeqNumOption;
 BOOL1  bFsLsppVerbose;
 BOOL1  bFsLsppReversePathVerify;
 BOOL1  bFsLsppEncapType;
 BOOL1  bFsLsppRowStatus;
 BOOL1  bFsLsppContextId;
} tLsppIsSetFsLsppPingTraceTableEntry;


/* Structure used by Lspp protocol for FsLsppPingTraceTableEntry */

typedef struct
{
 tRBNodeEmbd  FsLsppPingTraceTableNode;
 UINT4 u4FsLsppSenderHandle;
 UINT4 u4FsLsppTgtMipGlobalId;
 UINT4 u4FsLsppTgtMipNodeId;
 UINT4 u4FsLsppTgtMipIfNum;
 UINT4 u4FsLsppRepeatCount;
 UINT4 u4FsLsppPacketSize;
 UINT4 u4FsLsppTTLValue;
 UINT4 u4FsLsppWFRInterval;
 UINT4 u4FsLsppWTSInterval;
 UINT4 u4FsLsppReplyDscpValue;
 UINT4 u4FsLsppSweepMinimum;
 UINT4 u4FsLsppSweepMaximum;
 UINT4 u4FsLsppSweepIncrement;
 UINT4 u4FsLsppBurstSize;
 UINT4 u4FsLsppEXPValue;
 UINT4 u4FsLsppActualHopCount;
 UINT4 u4FsLsppResponderGlobalId;
 UINT4 u4FsLsppResponderId;
 UINT4 u4FsLsppMaxRtt;
 UINT4 u4FsLsppMinRtt;
 UINT4 u4FsLsppAverageRtt;
 UINT4 u4FsLsppPktsTx;
 UINT4 u4FsLsppPktsRx;
 UINT4 u4FsLsppPktsUnSent;
 UINT4 u4FsLsppResponderMepIndex;
 UINT4 u4FsLsppContextId;
 UINT4 au4FsLsppPathPointer[256];
 INT4 i4FsLsppRequestType;
 INT4 i4FsLsppRequestOwner;
 INT4 i4FsLsppPathType;
 INT4 i4FsLsppReplyMode;
 INT4 i4FsLsppWFRTmrUnit;
 INT4 i4FsLsppWTSTmrUnit;
 INT4 i4FsLsppSweepOption;
 INT4 i4FsLsppBurstOption;
 INT4 i4FsLsppDsMap;
 INT4 i4FsLsppFecValidate;
 INT4 i4FsLsppReplyPadTlv;
 INT4 i4FsLsppForceExplicitNull;
 INT4 i4FsLsppInterfaceLabelTlv;
 INT4 i4FsLsppSameSeqNumOption;
 INT4 i4FsLsppVerbose;
 INT4 i4FsLsppReversePathVerify;
 INT4 i4FsLsppEncapType;
 INT4 i4FsLsppStatus;
 INT4 i4FsLsppResponderAddrType;
 INT4 i4FsLsppRowStatus;
 INT4 i4FsLsppStatusPathDirection;
 INT4 i4FsLsppPathPointerLen;
 INT4 i4FsLsppPadPatternLen;
 INT4 i4FsLsppResponderAddrLen;
 INT4 i4FsLsppResponderIccLen;
 INT4 i4FsLsppResponderUMCLen;
 UINT1 au1FsLsppPadPattern[16];
 UINT1 au1FsLsppResponderAddr[16];
 UINT1 au1FsLsppResponderIcc[6];
 UINT1 au1FsLsppResponderUMC[7];
 UINT1 au1Align[3];
} tLsppMibFsLsppPingTraceTableEntry;


/* Structure used by Lspp protocol for FsLsppEchoSequenceTableEntry */

typedef struct
{
 tRBNodeEmbd  FsLsppEchoSequenceTableNode;
 UINT4 u4FsLsppSequenceNumber;
 UINT4 u4FsLsppReturnCode;
 UINT4 u4FsLsppReturnSubCode;
 UINT4 u4FsLsppContextId;
 UINT4 u4FsLsppSenderHandle;
 INT4 i4FsLsppReturnCodeStrLen;
 UINT1 au1FsLsppReturnCodeStr[128];
} tLsppMibFsLsppEchoSequenceTableEntry;


/* Structure used by Lspp protocol for FsLsppHopTableEntry */

typedef struct
{
 tRBNodeEmbd  FsLsppHopTableNode;
 UINT4 u4FsLsppHopIndex;
 UINT4 u4FsLsppHopGlobalId;
 UINT4 u4FsLsppHopId;
 UINT4 u4FsLsppHopIfNum;
 UINT4 u4FsLsppHopReturnCode;
 UINT4 u4FsLsppHopReturnSubCode;
 UINT4 u4FsLsppHopRxIfNum;
 UINT4 u4FsLsppHopRtt;
 UINT4 u4FsLsppHopDsMtu;
 UINT4 u4FsLsppHopDsIfNum;
 UINT4 u4FsLsppHopMepIndex;
 UINT4 u4FsLsppContextId;
 UINT4 u4FsLsppSenderHandle;
 INT4 i4FsLsppHopAddrType;
 INT4 i4FsLsppHopRxAddrType;
 INT4 i4FsLsppHopDsAddrType;
 INT4 i4FsLsppHopAddrLen;
 INT4 i4FsLsppHopReturnCodeStrLen;
 INT4 i4FsLsppHopRxIPAddrLen;
 INT4 i4FsLsppHopRxIfAddrLen;
 INT4 i4FsLsppHopRxLabelStackLen;
 INT4 i4FsLsppHopRxLabelExpLen;
 INT4 i4FsLsppHopDsIPAddrLen;
 INT4 i4FsLsppHopDsIfAddrLen;
 INT4 i4FsLsppHopDsLabelStackLen;
 INT4 i4FsLsppHopDsLabelExpLen;
 INT4 i4FsLsppHopIccLen;
 INT4 i4FsLsppHopUMCLen;
 UINT1 au1FsLsppHopAddr[16];
 UINT1 au1FsLsppHopReturnCodeStr[128];
 UINT1 au1FsLsppHopRxIPAddr[16];
 UINT1 au1FsLsppHopRxIfAddr[16];
 UINT1 au1FsLsppHopRxLabelStack[256];
 UINT1 au1FsLsppHopRxLabelExp[256];
 UINT1 au1FsLsppHopDsIPAddr[16];
 UINT1 au1FsLsppHopDsIfAddr[16];
 UINT1 au1FsLsppHopDsLabelStack[256];
 UINT1 au1FsLsppHopDsLabelExp[256];
 UINT1 au1FsLsppHopIcc[6];
 UINT1 au1FsLsppHopUMC[7];
 UINT1 au1Align[3];
} tLsppMibFsLsppHopTableEntry;
