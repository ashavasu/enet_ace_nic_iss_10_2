/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: lspplwg.h,v 1.4 2011/05/17 13:07:22 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsLsppGlobalConfigTable. */
INT1
nmhValidateIndexInstanceFsLsppGlobalConfigTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsLsppGlobalConfigTable  */

INT1
nmhGetFirstIndexFsLsppGlobalConfigTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsLsppGlobalConfigTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsLsppSystemControl ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsLsppTrapStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsLsppTraceLevel ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsLsppAgeOutTime ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsLsppAgeOutTmrUnit ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsLsppClearEchoStats ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsLsppBfdBtStrapRespReq ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsLsppBfdBtStrapAgeOutTime ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsLsppBfdBtStrapAgeOutTmrUnit ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsLsppSystemControl ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsLsppTrapStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsLsppTraceLevel ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsLsppAgeOutTime ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsLsppAgeOutTmrUnit ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsLsppClearEchoStats ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsLsppBfdBtStrapRespReq ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsLsppBfdBtStrapAgeOutTime ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsLsppBfdBtStrapAgeOutTmrUnit ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsLsppSystemControl ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2FsLsppTrapStatus ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2FsLsppTraceLevel ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2FsLsppAgeOutTime ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

INT1
nmhTestv2FsLsppAgeOutTmrUnit ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2FsLsppClearEchoStats ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2FsLsppBfdBtStrapRespReq ARG_LIST((UINT4 * , UINT4  ,INT4 ));

INT1
nmhTestv2FsLsppBfdBtStrapAgeOutTime ARG_LIST((UINT4 * , UINT4  ,UINT4 ));

INT1
nmhTestv2FsLsppBfdBtStrapAgeOutTmrUnit ARG_LIST((UINT4 * , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsLsppGlobalConfigTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsLsppGlobalStatsTable. */
INT1
nmhValidateIndexInstanceFsLsppGlobalStatsTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsLsppGlobalStatsTable  */

INT1
nmhGetFirstIndexFsLsppGlobalStatsTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsLsppGlobalStatsTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsLsppGlbStatReqTx ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsLsppGlbStatReqRx ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsLsppGlbStatReqTimedOut ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsLsppGlbStatReqUnSent ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsLsppGlbStatReplyTx ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsLsppGlbStatReplyRx ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsLsppGlbStatReplyDropped ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsLsppGlbStatReplyUnSent ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsLsppGlbStatReplyFromEgr ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsLsppGlbStatUnLbldOutIf ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsLsppGlbStatDsMapMismatch ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsLsppGlbStatFecLblMismatch ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsLsppGlbStatNoFecMapping ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsLsppGlbStatUnKUpstreamIf ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsLsppGlbStatReqLblSwitched ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsLsppGlbStatReqUnSupptdTlv ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsLsppGlbStatMalformedReq ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsLsppGlbStatNoLblEntry ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsLsppGlbStatPreTermReq ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsLsppGlbStatProtMismatch ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsLsppGlbStatRsvdRetCode ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsLsppGlbStatNoRetCode ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsLsppGlbStatUndefRetCode ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsLsppGlbStatInvalidPktDropped ARG_LIST((UINT4 ,UINT4 *));

/* Proto Validate Index Instance for FsLsppPingTraceTable. */
INT1
nmhValidateIndexInstanceFsLsppPingTraceTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsLsppPingTraceTable  */

INT1
nmhGetFirstIndexFsLsppPingTraceTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsLsppPingTraceTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsLsppRequestType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsLsppRequestOwner ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsLsppPathType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsLsppPathPointer ARG_LIST((UINT4  , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetFsLsppTgtMipGlobalId ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppTgtMipNodeId ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppTgtMipIfNum ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppReplyMode ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsLsppRepeatCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppPacketSize ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppPadPattern ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLsppTTLValue ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppWFRInterval ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppWFRTmrUnit ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsLsppWTSInterval ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppWTSTmrUnit ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsLsppReplyDscpValue ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppSweepOption ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsLsppSweepMinimum ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppSweepMaximum ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppSweepIncrement ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppBurstOption ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsLsppBurstSize ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppEXPValue ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppDsMap ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsLsppFecValidate ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsLsppReplyPadTlv ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsLsppForceExplicitNull ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsLsppInterfaceLabelTlv ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsLsppSameSeqNumOption ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsLsppVerbose ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsLsppReversePathVerify ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsLsppEncapType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsLsppStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsLsppActualHopCount ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppResponderAddrType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsLsppResponderAddr ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLsppResponderGlobalId ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppResponderId ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppMaxRtt ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppMinRtt ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppAverageRtt ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppPktsTx ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppPktsRx ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppPktsUnSent ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsLsppStatusPathDirection ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsLsppResponderIcc ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLsppResponderUMC ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLsppResponderMepIndex ARG_LIST((UINT4  , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsLsppRequestType ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsLsppPathType ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsLsppPathPointer ARG_LIST((UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetFsLsppTgtMipGlobalId ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsLsppTgtMipNodeId ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsLsppTgtMipIfNum ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsLsppReplyMode ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsLsppRepeatCount ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsLsppPacketSize ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsLsppPadPattern ARG_LIST((UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsLsppTTLValue ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsLsppWFRInterval ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsLsppWFRTmrUnit ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsLsppWTSInterval ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsLsppWTSTmrUnit ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsLsppReplyDscpValue ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsLsppSweepOption ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsLsppSweepMinimum ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsLsppSweepMaximum ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsLsppSweepIncrement ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsLsppBurstOption ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsLsppBurstSize ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsLsppEXPValue ARG_LIST((UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsLsppDsMap ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsLsppFecValidate ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsLsppReplyPadTlv ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsLsppForceExplicitNull ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsLsppInterfaceLabelTlv ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsLsppSameSeqNumOption ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsLsppVerbose ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsLsppReversePathVerify ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsLsppEncapType ARG_LIST((UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsLsppRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsLsppRequestType ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsLsppPathType ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsLsppPathPointer ARG_LIST((UINT4 * , UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2FsLsppTgtMipGlobalId ARG_LIST((UINT4 * , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsLsppTgtMipNodeId ARG_LIST((UINT4 * , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsLsppTgtMipIfNum ARG_LIST((UINT4 * , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsLsppReplyMode ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsLsppRepeatCount ARG_LIST((UINT4 * , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsLsppPacketSize ARG_LIST((UINT4 * , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsLsppPadPattern ARG_LIST((UINT4 * , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsLsppTTLValue ARG_LIST((UINT4 * , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsLsppWFRInterval ARG_LIST((UINT4 * , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsLsppWFRTmrUnit ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsLsppWTSInterval ARG_LIST((UINT4 * , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsLsppWTSTmrUnit ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsLsppReplyDscpValue ARG_LIST((UINT4 * , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsLsppSweepOption ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsLsppSweepMinimum ARG_LIST((UINT4 * , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsLsppSweepMaximum ARG_LIST((UINT4 * , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsLsppSweepIncrement ARG_LIST((UINT4 * , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsLsppBurstOption ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsLsppBurstSize ARG_LIST((UINT4 * , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsLsppEXPValue ARG_LIST((UINT4 * , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsLsppDsMap ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsLsppFecValidate ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsLsppReplyPadTlv ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsLsppForceExplicitNull ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsLsppInterfaceLabelTlv ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsLsppSameSeqNumOption ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsLsppVerbose ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsLsppReversePathVerify ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsLsppEncapType ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsLsppRowStatus ARG_LIST((UINT4 * , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsLsppPingTraceTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsLsppEchoSequenceTable. */
INT1
nmhValidateIndexInstanceFsLsppEchoSequenceTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsLsppEchoSequenceTable  */

INT1
nmhGetFirstIndexFsLsppEchoSequenceTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsLsppEchoSequenceTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsLsppReturnCode ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppReturnSubCode ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppReturnCodeStr ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for FsLsppHopTable. */
INT1
nmhValidateIndexInstanceFsLsppHopTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsLsppHopTable  */

INT1
nmhGetFirstIndexFsLsppHopTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsLsppHopTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsLsppHopAddrType ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsLsppHopAddr ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLsppHopGlobalId ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppHopId ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppHopIfNum ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppHopReturnCode ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppHopReturnSubCode ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppHopReturnCodeStr ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLsppHopRxAddrType ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsLsppHopRxIPAddr ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLsppHopRxIfAddr ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLsppHopRxIfNum ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppHopRxLabelStack ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLsppHopRxLabelExp ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLsppHopRtt ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppHopDsMtu ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppHopDsAddrType ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsLsppHopDsIPAddr ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLsppHopDsIfAddr ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLsppHopDsIfNum ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsLsppHopDsLabelStack ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLsppHopDsLabelExp ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLsppHopIcc ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLsppHopUMC ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsLsppHopMepIndex ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */
