/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: lsppmacs.h,v 1.7 2012/08/17 09:13:25 siva Exp $
*
* Description: This file contains macro definitions used in the LSP 
*              ping module.
*******************************************************************/
#ifndef __LSPPMACS_H__
#define __LSPPMACS_H__


#define LSPP_ECHO_REPLY_RECEIVED_UDP_PORT \
    pLsppEchoMsg->LsppUdpHeader.u2Dest_u_port

/************* DSMAP TLV Inforamtion ****/

#define LSPP_DSMAP_LABEL_STACK_DEPTH \
    pLsppPduInfo->LsppDsMapTlv.u1DsLblStackDepth

#define LSPP_DSMAP_TLV_ADDR_TYPE \
    pLsppDsMapTlv->u1AddrType

#define LSPP_DSMAP_TLV_DSFLAG \
    pLsppDsMapTlv->u1DsFlags

#define LSPP_DSMAP_TLV_MULTIPATH_LENGTH \
    pLsppDsMapTlv->u2MultipathLength

#define LSPP_DSMAP_TLV_ADDR_TYPE \
    pLsppDsMapTlv->u1AddrType

#define LSPP_DSMAP_TLV_MULTIPATH_LENGTH \
    pLsppDsMapTlv->u2MultipathLength

#define LSPP_DSMAP_TLV_MULTIPATH_TYPE\
    pLsppDsMapTlv->u1MultipathType



/************** Target FEC stack TLV Address *******/
#define LSPP_TGT_FEC_STACK \
        pLsppEchoMsg->LsppPduInfo.LsppFecTlv

#define LSPP_TGT_FEC_STACK_DEPTH \
        pLsppEchoMsg->LsppPduInfo.u1FecStackDepth

#define LSPP_RX_LBL_STACK \
        pLsppEchoMsg->LblInfo

#define LSPP_RX_LBL_STACK_DEPTH \
        pLsppEchoMsg->u1LabelStackDepth

#define LSPP_LDP_FEC_TLV_BASEADDR(u1FecStackDepth)\
    &(pLsppPduInfo->LsppFecTlv[u1FecStackDepth].LdpFecTlv)

#define LSPP_RSVP_FEC_TLV_BASEADDR(u1FecStackDepth) \
    &(pLsppPduInfo->LsppFecTlv[u1FecStackDepth].RsvpTeFecTlv)

#define LSPP_DEPREC_PW_TLV_BASEADDR(u1FecStackDepth) \
    &(pLsppPduInfo->LsppFecTlv[u1FecStackDepth].PwFec128DeprecTlv)

#define LSPP_FEC128_PW_TLV_BASEADDR(u1FecStackDepth) \
    &(pLsppPduInfo->LsppFecTlv[u1FecStackDepth].PwFec128Tlv)

#define LSPP_FEC129_PW_TLV_BASEADDR(u1FecStackDepth) \
    &(pLsppPduInfo->LsppFecTlv[u1FecStackDepth].PwFec129Tlv)

#define LSPP_NIL_FEC_TLV_BASEADDR(u1FecStackDepth) \
    &(pLsppPduInfo->LsppFecTlv[u1FecStackDepth].NilFecTlv)

#define LSPP_STATIC_LSP_FEC_TLV_BASEADDR(u1FecStackDepth) \
    &(pLsppPduInfo->LsppFecTlv[u1FecStackDepth].StaticLspFec)

#define LSPP_STATIC_PW_FEC_TLV_BASEADDR(u1FecStackDepth) \
    &(pLsppPduInfo->LsppFecTlv[u1FecStackDepth].StaticPwFec)

#define LSPP_BFD_FEC_TLV_BASEADDR &(pLsppPduInfo->LsppBfdDiscTlv)

#define LSPP_HEADER_BASEADDR &(pLsppPduInfo->LsppHeader)


#define LSPP_STATIC_PW_AGI_LEN(u1FecStackDepth)\
        pLsppPduInfo->LsppFecTlv[u1FecStackDepth].StaticPwFec.u1AgiLen


/*************** Fec 129 Tlv Info *********/
#define LSPP_FEC129_AGI_LEN(u1FecStackDepth) \
    pLsppPduInfo->LsppFecTlv[u1FecStackDepth].PwFec129Tlv.u1AgiLen

#define LSPP_FEC129_SAII_LEN(u1FecStackDepth) \
    pLsppPduInfo->LsppFecTlv[u1FecStackDepth].PwFec129Tlv.u1SaiiLen

#define LSPP_FEC129_TAII_LEN(u1FecStackDepth) \
    pLsppPduInfo->LsppFecTlv[u1FecStackDepth].PwFec129Tlv.u1TaiiLen

/************* IF lbl stk TLV Info *********/
#define LSPP_IF_LBL_STK_TLV_BASEADDR \
    &(pLsppPduInfo->LsppIfLblStkTlv)

#define LSPP_IF_LABEL_STACK_DEPTH \
    pLsppPduInfo->LsppIfLblStkTlv.u1LblStackDepth

#define LSPP_IF_LBL_STK_ADDR_TYPE \
    pLsppIfLblStkTlv->u1AddrType

#define LSPP_DSMAP_TLV_BASEADDR \
    &(pLsppPduInfo->LsppDsMapTlv)

/*********** Pad TLV Info *****************/
#define LSPP_PAD_TLV_BASEADDR \
    &(pLsppPduInfo->LsppPadTlv)

#define LSPP_PAD_TLV_FIRST_OCTET pLsppPadTlv->u1FirstOctet

/*********** Error TLV Info **************/
#define LSPP_ERROR_TLV_BASEADDR \
    &(pLsppPduInfo->LsppErrorTlv) 

#define LSPP_ERROR_TLV_VALUE  &(pLsppErrorTlv->au1Value)

#define LSPP_ERROR_TLV_LENGTH pLsppErrorTlv.u2ErrorTLVLength


/*********** Reply Tos TLV Info **************/
#define LSPP_REPLY_TOS_TLV_BASEADDR \
    &(pLsppPduInfo->LsppReplyTosTlv) 
#define LSPP_REPLY_TOS_VALUE  pLsppReplyTosTlv->u1TosValue

/* Macros used to extract values from buffer */
#define LSPP_GET_1BYTE(u1Val, pu1Buf)\
    do{\
            u1Val = *pu1Buf;\
            pu1Buf += 1;\
    }while(0)

#define LSPP_GET_2BYTE(u2Val, pu1Buf)\
      do{\
            MEMCPY (&u2Val, pu1Buf, 2);\
            pu1Buf += 2;\
            u2Val = (UINT2) (OSIX_NTOHS(u2Val));\
      }while(0)

#define LSPP_GET_4BYTE(u4Val, pu1Buf)\
        do{\
                MEMCPY (&u4Val, pu1Buf, 4);\
                pu1Buf += 4;\
                u4Val = (UINT4) (OSIX_NTOHL(u4Val));\
        }while(0)

#define LSPP_GET_NBYTE(pu1Dest, pu1Buf, u4Len)\
        do{\
                MEMCPY ((UINT1 *) pu1Dest, (UINT1 *) pu1Buf, u4Len);\
                pu1Buf += u4Len;\
        }while(0)

/*Macros used to assign values to buffer */
#define LSPP_PUT_NBYTE(pu1PktBuf, pu1Src, u4Len)\
{\
    MEMCPY ((UINT1 *) pu1PktBuf, (UINT1 *) pu1Src, u4Len);\
    pu1PktBuf += u4Len;\
}


/*Macros used to assign values to CRU buffer */
#define LSPP_CRU_PUT_1BYTE(pMsg, pu4Offset, u1Value) \
{\
       UINT1  u1LinearBuf = (UINT1) u1Value;\
       CRU_BUF_Copy_OverBufChain(pMsg, ((UINT1 *) &u1LinearBuf),*pu4Offset, 1);\
       *pu4Offset = *pu4Offset + 1;\
}

#define LSPP_CRU_PUT_2BYTE(pMsg, pu4Offset, u2Value) \
{\
       UINT2  u2LinearBuf = OSIX_HTONS ((UINT2) u2Value);\
       CRU_BUF_Copy_OverBufChain(pMsg, ((UINT1 *) &u2LinearBuf),*pu4Offset, 2);\
       *pu4Offset = *pu4Offset + 2;\
}

#define LSPP_CRU_PUT_4BYTE(pMsg, pu4Offset, u4Value) \
{\
       UINT4  u4LinearBuf = OSIX_HTONL ((UINT4) u4Value);\
       CRU_BUF_Copy_OverBufChain(pMsg, ((UINT1 *) &u4LinearBuf),*pu4Offset, 4);\
       *pu4Offset = *pu4Offset + 4;\
}

#define LSPP_CRU_PUT_NBYTE(pMsg, pu1_Src, pu4Offset, u4_SrcLength) \
{\
       CRU_BUF_Copy_OverBufChain(pMsg, pu1_Src, *pu4Offset,\
                                 u4_SrcLength);\
       *pu4Offset = *pu4Offset + u4_SrcLength;\
}

#define LSPP_DUMP_PKT(u4ContextId, pu1Buf, u4Len)\
{\
    INT4 i4TraceLevel = 0;\
    LsppTrcGetTraceLevel (u4ContextId , &i4TraceLevel);\
    if (i4TraceLevel & LSPP_PKT_DUMP_TRC)\
    {\
        LsppTrcDump(pu1Buf, u4Len);\
    }\
}

#define LSPP_DUMP_TLV(u4ContextId, pu1Buf, u4Len)\
{\
    INT4 i4TraceLevel = 0;\
    LsppTrcGetTraceLevel (u4ContextId , &i4TraceLevel);\
    if (i4TraceLevel & LSPP_TLV_TRC)\
    {\
        LsppTrcDump(pu1Buf, u4Len);\
    }\
}


/* Timestamp related macros */
#define LSPP_SECS_IN_YEAR(yr) (IS_LEAP((yr)) ? 31622400 : 31536000)
#define IS_LEAP(yr)  ((yr) % 4 == 0 && ((yr) % 100 != 0 || (yr) % 400 == 0))


/*************** TX related MACROS ***************/
#define LSPP_PAD_TLV_LEN pLsppPduInfo->LsppPadTlv.u2Length
#define LSPP_ERRORED_TLV_LEN pLsppPduInfo->LsppErrorTlv.u2ErrorTLVLength


#define LSPP_GET_CRU_VALID_BYTE_COUNT CRU_BUF_Get_ChainValidByteCount
#define LSPP_CRU_MOVE_VALID_OFFSET CRU_BUF_Move_ValidOffset

#define LSPP_RELEASE_CRU_BUF CRU_BUF_Release_MsgBufChain
#define LSPP_ALLOC_CRU_BUF CRU_BUF_Allocate_MsgBufChain

#define LSPP_COPY_FROM_BUF_CHAIN CRU_BUF_Copy_FromBufChain
#define LSPP_COPY_OVER_BUF_CHAIN CRU_BUF_Copy_OverBufChain


#define LSPP_LOG LsppTrcLsppEventLogNotify
#define LSPP_CMN_TRC LsppTrcIssEventLogNotify

#define LSPP_FSLSPP_ECHOMSG_ENTRY_POOLID \
        LSPPMemPoolIds[MAX_LSPP_ECHOMSG_BUFFER_BLK_SIZING_ID]

#define LSPP_FSLSPP_QUEUE_MSG_ENTRY_POOLID \
        LSPPMemPoolIds[MAX_LSPP_QUEUE_DEPTH_SIZING_ID]


#endif /* _LSPP_MACS_H */


/*-----------------------------------------------------------------------*/
/*                       End of the file  lsppmacs.h                     */
/*-----------------------------------------------------------------------*/
