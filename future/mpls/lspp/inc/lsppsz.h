/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: lsppsz.h,v 1.7 2012/03/21 13:05:04 siva Exp $
 *
 **********************************************************************/

enum {
    MAX_LSPP_BFD_REQ_PARAM_BUF_SIZING_ID,
    MAX_LSPP_ECHOMSG_BUFFER_BLK_SIZING_ID,
    MAX_LSPP_FSLSPPECHOSEQUENCETABLE_SIZING_ID,
    MAX_LSPP_FSLSPPGLOBALCONFIGTABLE_SIZING_ID,
    MAX_LSPP_FSLSPPGLOBALSTATSTABLE_SIZING_ID,
    MAX_LSPP_FSLSPPHOPTABLE_SIZING_ID,
    MAX_LSPP_FSLSPPPINGTRACETABLE_SIZING_ID,
    MAX_LSPP_INPUT_PARAMS_SIZING_ID,
    MAX_LSPP_MPLS_API_INPUT_BUF_SIZING_ID,
    MAX_LSPP_MPLS_API_OUTPUT_BUF_SIZING_ID,
    MAX_LSPP_OUTPUT_PARAMS_SIZING_ID,
    MAX_LSPP_PDU_BUF_SIZING_ID,
    MAX_LSPP_QUEUE_DEPTH_SIZING_ID,
    LSPP_MAX_SIZING_ID
};

#ifdef  _LSPPSZ_C
tMemPoolId LSPPMemPoolIds[ LSPP_MAX_SIZING_ID];
INT4  LsppSizingMemCreateMemPools(VOID);
VOID  LsppSizingMemDeleteMemPools(VOID);
INT4  LsppSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _LSPPSZ_C  */
extern tMemPoolId LSPPMemPoolIds[ ];
extern INT4  LsppSizingMemCreateMemPools(VOID);
extern VOID  LsppSizingMemDeleteMemPools(VOID);
extern INT4  LsppSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _LSPPSZ_C  */


#ifdef  _LSPPSZ_C
tFsModSizingParams FsLSPPSizingParams [] = {
{ "tBfdReqParams", "MAX_LSPP_BFD_REQ_PARAM_BUF", sizeof(tBfdReqParams),MAX_LSPP_BFD_REQ_PARAM_BUF, MAX_LSPP_BFD_REQ_PARAM_BUF,0 },
{ "tLsppEchoMsg", "MAX_LSPP_ECHOMSG_BUFFER_BLK", sizeof(tLsppEchoMsg),MAX_LSPP_ECHOMSG_BUFFER_BLK, MAX_LSPP_ECHOMSG_BUFFER_BLK,0 },
{ "tLsppFsLsppEchoSequenceTableEntry", "MAX_LSPP_FSLSPPECHOSEQUENCETABLE", sizeof(tLsppFsLsppEchoSequenceTableEntry),MAX_LSPP_FSLSPPECHOSEQUENCETABLE, MAX_LSPP_FSLSPPECHOSEQUENCETABLE,0 },
{ "tLsppFsLsppGlobalConfigTableEntry", "MAX_LSPP_FSLSPPGLOBALCONFIGTABLE", sizeof(tLsppFsLsppGlobalConfigTableEntry),MAX_LSPP_FSLSPPGLOBALCONFIGTABLE, MAX_LSPP_FSLSPPGLOBALCONFIGTABLE,0 },
{ "tLsppFsLsppGlobalStatsTableEntry", "MAX_LSPP_FSLSPPGLOBALSTATSTABLE", sizeof(tLsppFsLsppGlobalStatsTableEntry),MAX_LSPP_FSLSPPGLOBALSTATSTABLE, MAX_LSPP_FSLSPPGLOBALSTATSTABLE,0 },
{ "tLsppFsLsppHopTableEntry", "MAX_LSPP_FSLSPPHOPTABLE", sizeof(tLsppFsLsppHopTableEntry),MAX_LSPP_FSLSPPHOPTABLE, MAX_LSPP_FSLSPPHOPTABLE,0 },
{ "tLsppFsLsppPingTraceTableEntry", "MAX_LSPP_FSLSPPPINGTRACETABLE", sizeof(tLsppFsLsppPingTraceTableEntry),MAX_LSPP_FSLSPPPINGTRACETABLE, MAX_LSPP_FSLSPPPINGTRACETABLE,0 },
{ "tLsppExtInParams", "MAX_LSPP_INPUT_PARAMS", sizeof(tLsppExtInParams),MAX_LSPP_INPUT_PARAMS, MAX_LSPP_INPUT_PARAMS,0 },
{ "tMplsApiInInfo", "MAX_LSPP_MPLS_API_INPUT_BUF", sizeof(tMplsApiInInfo),MAX_LSPP_MPLS_API_INPUT_BUF, MAX_LSPP_MPLS_API_INPUT_BUF,0 },
{ "tMplsApiOutInfo", "MAX_LSPP_MPLS_API_OUTPUT_BUF", sizeof(tMplsApiOutInfo),MAX_LSPP_MPLS_API_OUTPUT_BUF, MAX_LSPP_MPLS_API_OUTPUT_BUF,0 },
{ "tLsppExtOutParams", "MAX_LSPP_OUTPUT_PARAMS", sizeof(tLsppExtOutParams),MAX_LSPP_OUTPUT_PARAMS, MAX_LSPP_OUTPUT_PARAMS,0 },
{ "UINT1[LSPP_MAX_PDU_LEN]", "MAX_LSPP_PDU_BUF", sizeof(UINT1[LSPP_MAX_PDU_LEN]),MAX_LSPP_PDU_BUF, MAX_LSPP_PDU_BUF,0 },
{ "tLsppQMsg", "MAX_LSPP_QUEUE_DEPTH", sizeof(tLsppQMsg),MAX_LSPP_QUEUE_DEPTH, MAX_LSPP_QUEUE_DEPTH,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _LSPPSZ_C  */
extern tFsModSizingParams FsLSPPSizingParams [];
#endif /*  _LSPPSZ_C  */


