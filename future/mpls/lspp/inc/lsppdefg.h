/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: lsppdefg.h,v 1.6 2012/02/29 09:11:00 siva Exp $
*
* Description: Proto types & DataStructure definition 
*********************************************************************/


/* Macro used to fill the CLI structure for FsLsppGlobalConfigTable 
 using the input given in def file */

#define LSPP_FSLSPPGLOBALCONFIGTABLE_POOLID \
        LSPPMemPoolIds[MAX_LSPP_FSLSPPGLOBALCONFIGTABLE_SIZING_ID]

#define LSPP_FSLSPPGLOBALSTATSTABLE_POOLID \
        LSPPMemPoolIds[MAX_LSPP_FSLSPPGLOBALSTATSTABLE_SIZING_ID]

#define LSPP_FSLSPPPINGTRACETABLE_POOLID \
        LSPPMemPoolIds[MAX_LSPP_FSLSPPPINGTRACETABLE_SIZING_ID]

#define LSPP_FSLSPPECHOSEQUENCETABLE_POOLID \
        LSPPMemPoolIds[MAX_LSPP_FSLSPPECHOSEQUENCETABLE_SIZING_ID]

#define LSPP_FSLSPPOUTPUTPARAMS_POOLID \
        LSPPMemPoolIds[MAX_LSPP_OUTPUT_PARAMS_SIZING_ID]

#define LSPP_FSLSPPINPUTPARAMS_POOLID \
        LSPPMemPoolIds[MAX_LSPP_INPUT_PARAMS_SIZING_ID]

#define LSPP_FSLSPP_PDU_BUFFER_POOLID \
        LSPPMemPoolIds[MAX_LSPP_PDU_BUF_SIZING_ID]

#define LSPP_FSLSPPHOPTABLE_POOLID \
        LSPPMemPoolIds[MAX_LSPP_FSLSPPHOPTABLE_SIZING_ID]

#define LSPP_FSLSPPMPLSINPUTPARAMS_POOLID \
        LSPPMemPoolIds[MAX_LSPP_MPLS_API_INPUT_BUF_SIZING_ID]

#define LSPP_FSLSPPMPLSOUTPUTPARAMS_POOLID \
        LSPPMemPoolIds[MAX_LSPP_MPLS_API_OUTPUT_BUF_SIZING_ID]

#define LSPP_FSLSPPBFDREQPARAMS_POOLID \
        LSPPMemPoolIds[MAX_LSPP_BFD_REQ_PARAM_BUF_SIZING_ID]

#define  LSPP_FILL_FSLSPPGLOBALCONFIGTABLE_ARGS(pLsppFsLsppGlobalConfigTableEntry,\
   pLsppIsSetFsLsppGlobalConfigTableEntry,\
   pau1FsLsppContextId,\
   pau1FsLsppSystemControl,\
   pau1FsLsppTrapStatus,\
   pau1FsLsppTraceLevel,\
   pau1FsLsppAgeOutTime,\
   pau1FsLsppAgeOutTmrUnit,\
   pau1FsLsppClearEchoStats,\
   pau1FsLsppBfdBtStrapRespReq,\
   pau1FsLsppBfdBtStrapAgeOutTime,\
   pau1FsLsppBfdBtStrapAgeOutTmrUnit)\
  {\
  if (pau1FsLsppContextId != NULL)\
  {\
   pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId = *(UINT4 *) (pau1FsLsppContextId);\
   pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppContextId = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppContextId = OSIX_FALSE;\
  }\
  if (pau1FsLsppSystemControl != NULL)\
  {\
   pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppSystemControl = *(INT4 *) (pau1FsLsppSystemControl);\
   pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppSystemControl = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppSystemControl = OSIX_FALSE;\
  }\
  if (pau1FsLsppTrapStatus != NULL)\
  {\
   pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppTrapStatus = *(INT4 *) (pau1FsLsppTrapStatus);\
   pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppTrapStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppTrapStatus = OSIX_FALSE;\
  }\
  if (pau1FsLsppTraceLevel != NULL)\
  {\
   pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppTraceLevel = *(INT4 *) (pau1FsLsppTraceLevel);\
   pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppTraceLevel = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppTraceLevel = OSIX_FALSE;\
  }\
  if (pau1FsLsppAgeOutTime != NULL)\
  {\
   pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppAgeOutTime = *(UINT4 *) (pau1FsLsppAgeOutTime);\
   pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppAgeOutTime = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppAgeOutTime = OSIX_FALSE;\
  }\
  if (pau1FsLsppAgeOutTmrUnit != NULL)\
  {\
   pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppAgeOutTmrUnit = *(INT4 *) (pau1FsLsppAgeOutTmrUnit);\
   pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppAgeOutTmrUnit = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppAgeOutTmrUnit = OSIX_FALSE;\
  }\
  if (pau1FsLsppClearEchoStats != NULL)\
  {\
   pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppClearEchoStats = *(INT4 *) (pau1FsLsppClearEchoStats);\
   pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppClearEchoStats = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppClearEchoStats = OSIX_FALSE;\
  }\
  if (pau1FsLsppBfdBtStrapRespReq != NULL)\
  {\
   pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppBfdBtStrapRespReq = *(INT4 *) (pau1FsLsppBfdBtStrapRespReq);\
   pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppBfdBtStrapRespReq = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppBfdBtStrapRespReq = OSIX_FALSE;\
  }\
  if (pau1FsLsppBfdBtStrapAgeOutTime != NULL)\
  {\
   pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppBfdBtStrapAgeOutTime = *(UINT4 *) (pau1FsLsppBfdBtStrapAgeOutTime);\
   pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppBfdBtStrapAgeOutTime = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppBfdBtStrapAgeOutTime = OSIX_FALSE;\
  }\
  if (pau1FsLsppBfdBtStrapAgeOutTmrUnit != NULL)\
  {\
   pLsppFsLsppGlobalConfigTableEntry->MibObject.i4FsLsppBfdBtStrapAgeOutTmrUnit = *(INT4 *) (pau1FsLsppBfdBtStrapAgeOutTmrUnit);\
   pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppBfdBtStrapAgeOutTmrUnit = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppGlobalConfigTableEntry->bFsLsppBfdBtStrapAgeOutTmrUnit = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the index values in  structure for FsLsppGlobalConfigTableEntry 
 using the input given in def file */

#define  LSPP_FILL_FSLSPPGLOBALCONFIGTABLE_INDEX(pLsppFsLsppGlobalConfigTableEntry,\
   pau1FsLsppContextId)\
  {\
  if (pau1FsLsppContextId != NULL)\
  {\
   pLsppFsLsppGlobalConfigTableEntry->MibObject.u4FsLsppContextId = *(UINT4 *) (pau1FsLsppContextId);\
  }\
  }




/* Macro used to fill the index values in  structure for FsLsppGlobalStatsTableEntry 
 using the input given in def file */

#define  LSPP_FILL_FSLSPPGLOBALSTATSTABLE_INDEX(pLsppFsLsppGlobalStatsTableEntry,\
   pau1FsLsppContextId)\
  {\
  if (pau1FsLsppContextId != NULL)\
  {\
   pLsppFsLsppGlobalStatsTableEntry->MibObject.u4FsLsppContextId = *(UINT4 *) (pau1FsLsppContextId);\
  }\
  }




/* Macro used to fill the CLI structure for FsLsppPingTraceTableEntry 
 using the input given in def file */

#define  LSPP_FILL_FSLSPPPINGTRACETABLE_ARGS(pLsppFsLsppPingTraceTableEntry,\
   pLsppIsSetFsLsppPingTraceTableEntry,\
   pau1FsLsppSenderHandle,\
   pau1FsLsppRequestType,\
   pau1FsLsppPathType,\
   pau1FsLsppPathPointer,\
   pau1FsLsppPathPointerLen,\
   pau1FsLsppTgtMipGlobalId,\
   pau1FsLsppTgtMipNodeId,\
   pau1FsLsppTgtMipIfNum,\
   pau1FsLsppReplyMode,\
   pau1FsLsppRepeatCount,\
   pau1FsLsppPacketSize,\
   pau1FsLsppPadPattern,\
   pau1FsLsppPadPatternLen,\
   pau1FsLsppTTLValue,\
   pau1FsLsppWFRInterval,\
   pau1FsLsppWFRTmrUnit,\
   pau1FsLsppWTSInterval,\
   pau1FsLsppWTSTmrUnit,\
   pau1FsLsppReplyDscpValue,\
   pau1FsLsppSweepOption,\
   pau1FsLsppSweepMinimum,\
   pau1FsLsppSweepMaximum,\
   pau1FsLsppSweepIncrement,\
   pau1FsLsppBurstOption,\
   pau1FsLsppBurstSize,\
   pau1FsLsppEXPValue,\
   pau1FsLsppDsMap,\
   pau1FsLsppFecValidate,\
   pau1FsLsppReplyPadTlv,\
   pau1FsLsppForceExplicitNull,\
   pau1FsLsppInterfaceLabelTlv,\
   pau1FsLsppSameSeqNumOption,\
   pau1FsLsppVerbose,\
   pau1FsLsppReversePathVerify,\
   pau1FsLsppEncapType,\
   pau1FsLsppRowStatus,\
   pau1FsLsppContextId)\
  {\
  if (pau1FsLsppSenderHandle != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle = *(UINT4 *) (pau1FsLsppSenderHandle);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSenderHandle = OSIX_FALSE;\
  }\
  if (pau1FsLsppRequestType != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRequestType = *(INT4 *) (pau1FsLsppRequestType);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRequestType = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRequestType = OSIX_FALSE;\
  }\
  if (pau1FsLsppPathType != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppPathType = *(INT4 *) (pau1FsLsppPathType);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPathType = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPathType = OSIX_FALSE;\
  }\
  if (pau1FsLsppPathPointer != NULL)\
  {\
   LsppUtlConvertStringtoOid (pLsppFsLsppPingTraceTableEntry,\
       (UINT4 *)pau1FsLsppPathPointer,(INT4 *)pau1FsLsppPathPointerLen);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPathPointer = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPathPointer = OSIX_FALSE;\
  }\
  if (pau1FsLsppTgtMipGlobalId != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipGlobalId = *(UINT4 *) (pau1FsLsppTgtMipGlobalId);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTgtMipGlobalId = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTgtMipGlobalId = OSIX_FALSE;\
  }\
  if (pau1FsLsppTgtMipNodeId != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipNodeId = *(UINT4 *) (pau1FsLsppTgtMipNodeId);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTgtMipNodeId = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTgtMipNodeId = OSIX_FALSE;\
  }\
  if (pau1FsLsppTgtMipIfNum != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTgtMipIfNum = *(UINT4 *) (pau1FsLsppTgtMipIfNum);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTgtMipIfNum = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTgtMipIfNum = OSIX_FALSE;\
  }\
  if (pau1FsLsppReplyMode != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppReplyMode = *(INT4 *) (pau1FsLsppReplyMode);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReplyMode = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReplyMode = OSIX_FALSE;\
  }\
  if (pau1FsLsppRepeatCount != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppRepeatCount = *(UINT4 *) (pau1FsLsppRepeatCount);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRepeatCount = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRepeatCount = OSIX_FALSE;\
  }\
  if (pau1FsLsppPacketSize != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppPacketSize = *(UINT4 *) (pau1FsLsppPacketSize);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPacketSize = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPacketSize = OSIX_FALSE;\
  }\
  if (pau1FsLsppPadPattern != NULL)\
  {\
   MEMCPY (pLsppFsLsppPingTraceTableEntry->MibObject.au1FsLsppPadPattern, pau1FsLsppPadPattern, *(INT4 *)pau1FsLsppPadPatternLen);\
   pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppPadPatternLen = *(INT4 *)pau1FsLsppPadPatternLen;\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPadPattern = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppPadPattern = OSIX_FALSE;\
  }\
  if (pau1FsLsppTTLValue != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppTTLValue = *(UINT4 *) (pau1FsLsppTTLValue);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTTLValue = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppTTLValue = OSIX_FALSE;\
  }\
  if (pau1FsLsppWFRInterval != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppWFRInterval = *(UINT4 *) (pau1FsLsppWFRInterval);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWFRInterval = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWFRInterval = OSIX_FALSE;\
  }\
  if (pau1FsLsppWFRTmrUnit != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppWFRTmrUnit = *(INT4 *) (pau1FsLsppWFRTmrUnit);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWFRTmrUnit = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWFRTmrUnit = OSIX_FALSE;\
  }\
  if (pau1FsLsppWTSInterval != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppWTSInterval = *(UINT4 *) (pau1FsLsppWTSInterval);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWTSInterval = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWTSInterval = OSIX_FALSE;\
  }\
  if (pau1FsLsppWTSTmrUnit != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppWTSTmrUnit = *(INT4 *) (pau1FsLsppWTSTmrUnit);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWTSTmrUnit = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppWTSTmrUnit = OSIX_FALSE;\
  }\
  if (pau1FsLsppReplyDscpValue != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppReplyDscpValue = *(UINT4 *) (pau1FsLsppReplyDscpValue);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReplyDscpValue = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReplyDscpValue = OSIX_FALSE;\
  }\
  if (pau1FsLsppSweepOption != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppSweepOption = *(INT4 *) (pau1FsLsppSweepOption);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepOption = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepOption = OSIX_FALSE;\
  }\
  if (pau1FsLsppSweepMinimum != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepMinimum = *(UINT4 *) (pau1FsLsppSweepMinimum);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepMinimum = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepMinimum = OSIX_FALSE;\
  }\
  if (pau1FsLsppSweepMaximum != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepMaximum = *(UINT4 *) (pau1FsLsppSweepMaximum);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepMaximum = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepMaximum = OSIX_FALSE;\
  }\
  if (pau1FsLsppSweepIncrement != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSweepIncrement = *(UINT4 *) (pau1FsLsppSweepIncrement);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepIncrement = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSweepIncrement = OSIX_FALSE;\
  }\
  if (pau1FsLsppBurstOption != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppBurstOption = *(INT4 *) (pau1FsLsppBurstOption);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppBurstOption = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppBurstOption = OSIX_FALSE;\
  }\
  if (pau1FsLsppBurstSize != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppBurstSize = *(UINT4 *) (pau1FsLsppBurstSize);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppBurstSize = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppBurstSize = OSIX_FALSE;\
  }\
  if (pau1FsLsppEXPValue != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppEXPValue = *(UINT4 *) (pau1FsLsppEXPValue);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppEXPValue = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppEXPValue = OSIX_FALSE;\
  }\
  if (pau1FsLsppDsMap != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppDsMap = *(INT4 *) (pau1FsLsppDsMap);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppDsMap = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppDsMap = OSIX_FALSE;\
  }\
  if (pau1FsLsppFecValidate != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppFecValidate = *(INT4 *) (pau1FsLsppFecValidate);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppFecValidate = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppFecValidate = OSIX_FALSE;\
  }\
  if (pau1FsLsppReplyPadTlv != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppReplyPadTlv = *(INT4 *) (pau1FsLsppReplyPadTlv);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReplyPadTlv = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReplyPadTlv = OSIX_FALSE;\
  }\
  if (pau1FsLsppForceExplicitNull != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppForceExplicitNull = *(INT4 *) (pau1FsLsppForceExplicitNull);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppForceExplicitNull = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppForceExplicitNull = OSIX_FALSE;\
  }\
  if (pau1FsLsppInterfaceLabelTlv != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppInterfaceLabelTlv = *(INT4 *) (pau1FsLsppInterfaceLabelTlv);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppInterfaceLabelTlv = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppInterfaceLabelTlv = OSIX_FALSE;\
  }\
  if (pau1FsLsppSameSeqNumOption != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppSameSeqNumOption = *(INT4 *) (pau1FsLsppSameSeqNumOption);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSameSeqNumOption = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppSameSeqNumOption = OSIX_FALSE;\
  }\
  if (pau1FsLsppVerbose != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppVerbose = *(INT4 *) (pau1FsLsppVerbose);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppVerbose = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppVerbose = OSIX_FALSE;\
  }\
  if (pau1FsLsppReversePathVerify != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppReversePathVerify = *(INT4 *) (pau1FsLsppReversePathVerify);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReversePathVerify = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppReversePathVerify = OSIX_FALSE;\
  }\
  if (pau1FsLsppEncapType != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppEncapType = *(INT4 *) (pau1FsLsppEncapType);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppEncapType = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppEncapType = OSIX_FALSE;\
  }\
  if (pau1FsLsppRowStatus != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.i4FsLsppRowStatus = *(INT4 *) (pau1FsLsppRowStatus);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRowStatus = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppRowStatus = OSIX_FALSE;\
  }\
  if (pau1FsLsppContextId != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId = *(UINT4 *) (pau1FsLsppContextId);\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_TRUE;\
  }\
  else\
  {\
   pLsppIsSetFsLsppPingTraceTableEntry->bFsLsppContextId = OSIX_FALSE;\
  }\
  }
/* Macro used to fill the index values in  structure for FsLsppPingTraceTableEntry 
 using the input given in def file */

#define  LSPP_FILL_FSLSPPPINGTRACETABLE_INDEX(pLsppFsLsppPingTraceTableEntry,\
   pau1FsLsppContextId,\
   pau1FsLsppSenderHandle)\
  {\
  if (pau1FsLsppContextId != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppContextId = *(UINT4 *) (pau1FsLsppContextId);\
  }\
  if (pau1FsLsppSenderHandle != NULL)\
  {\
   pLsppFsLsppPingTraceTableEntry->MibObject.u4FsLsppSenderHandle = *(UINT4 *) (pau1FsLsppSenderHandle);\
  }\
  }




/* Macro used to fill the index values in  structure for FsLsppEchoSequenceTableEntry 
 using the input given in def file */

#define  LSPP_FILL_FSLSPPECHOSEQUENCETABLE_INDEX(pLsppFsLsppEchoSequenceTableEntry,\
   pau1FsLsppContextId,\
   pau1FsLsppSenderHandle,\
   pau1FsLsppSequenceNumber)\
  {\
  if (pau1FsLsppContextId != NULL)\
  {\
   pLsppFsLsppEchoSequenceTableEntry->MibObject.u4FsLsppContextId = *(UINT4 *) (pau1FsLsppContextId);\
  }\
  if (pau1FsLsppSenderHandle != NULL)\
  {\
   pLsppFsLsppEchoSequenceTableEntry->MibObject.u4FsLsppSenderHandle = *(UINT4 *) (pau1FsLsppSenderHandle);\
  }\
  if (pau1FsLsppSequenceNumber != NULL)\
  {\
   pLsppFsLsppEchoSequenceTableEntry->MibObject.u4FsLsppSequenceNumber = *(UINT4 *) (pau1FsLsppSequenceNumber);\
  }\
  }




/* Macro used to fill the index values in  structure for FsLsppHopTableEntry 
 using the input given in def file */

#define  LSPP_FILL_FSLSPPHOPTABLE_INDEX(pLsppFsLsppHopTableEntry,\
   pau1FsLsppContextId,\
   pau1FsLsppSenderHandle,\
   pau1FsLsppHopIndex)\
  {\
  if (pau1FsLsppContextId != NULL)\
  {\
   pLsppFsLsppHopTableEntry->MibObject.u4FsLsppContextId = *(UINT4 *) (pau1FsLsppContextId);\
  }\
  if (pau1FsLsppSenderHandle != NULL)\
  {\
   pLsppFsLsppHopTableEntry->MibObject.u4FsLsppSenderHandle = *(UINT4 *) (pau1FsLsppSenderHandle);\
  }\
  if (pau1FsLsppHopIndex != NULL)\
  {\
   pLsppFsLsppHopTableEntry->MibObject.u4FsLsppHopIndex = *(UINT4 *) (pau1FsLsppHopIndex);\
  }\
  }




/* Macro used to convert the input 
  to required datatype for FsLsppTrapContextName*/

#define  LSPP_FILL_FSLSPPTRAPCONTEXTNAME(pFsLsppTrapContextName,\
   pau1FsLsppTrapContextName)\
  {\
  if (pau1FsLsppTrapContextName != NULL)\
  {\
   STRCPY (pFsLsppTrapContextName, pau1FsLsppTrapContextName);\
  }\
  }




