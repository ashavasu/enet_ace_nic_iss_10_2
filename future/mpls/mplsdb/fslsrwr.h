/* $Id: fslsrwr.h,v 1.3 2013/07/26 13:31:11 siva Exp $ */
#ifndef _FSLSRWR_H
#define _FSLSRWR_H
INT4 GetNextIndexFsMplsInSegmentTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSLSR (VOID);

VOID UnRegisterFSLSR (VOID);
INT4 FsMplsInSegmentDirectionGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsInSegmentDirectionSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsInSegmentDirectionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsInSegmentTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMplsOutSegmentTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMplsOutSegmentDirectionGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsOutSegmentDirectionSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsOutSegmentDirectionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsOutSegmentTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 FsMplsLsrRfc6428CompatibleCodePointGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLsrRfc6428CompatibleCodePointSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLsrRfc6428CompatibleCodePointTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsLsrRfc6428CompatibleCodePointDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
#endif /* _FSLSRWR_H */
