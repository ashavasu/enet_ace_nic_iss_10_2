
/* $Id: mplsdbinc.h,v 1.5 2016/07/18 11:19:37 siva Exp $*/
/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * ******************************************************************************
 *   ******************************************************************************
 *  FILE  NAME             : mplsdbinc.h
 *      PRINCIPAL AUTHOR       : Aricent Inc.
 *      SUBSYSTEM NAME         : MPLS
 *      MODULE NAME            : MPLSDB
 *      LANGUAGE               : ANSI-C
 *      TARGET ENVIRONMENT     : Linux (Portable)
 *      DATE OF FIRST RELEASE  :
 *      DESCRIPTION            : This file contains all the associated include
 *                               files for the MPLSDB support.
 * *---------------------------------------------------------------------------*/

#ifndef _MPLSDBINCS_H
#define _MPLSDBINCS_H


/* Common Include Files */
#include "lr.h"
#include "cust.h"
#include "cfa.h"
#include "ip.h"
#include "include.h"

/* RBTree Include */
#include "redblack.h"

/* label manager  includes */
#include "lblmgrex.h"


/* mplsFM includes */
#include "mplssize.h"

/* Te includes */
#include "teextrn.h"
#include "temacs.h"

/* Index manager include */
#include "inmgrex.h"
#include "indexmgr.h"

#include "mplcmndb.h"

#include "mplsftn.h"
#include "mplslsr.h"
#include "mplsdbsz.h"
#include "cli.h"
#include "csr.h"

#ifdef NPAPI_WANTED
#include "mplsnpwr.h"
#endif /* NPAPI_WANTED */

#ifdef RFC6374_WANTED
#include "r6374cli.h"
#endif
#endif
