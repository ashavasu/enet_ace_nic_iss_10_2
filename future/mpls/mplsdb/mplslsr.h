/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mplslsr.h,v 1.35 2014/11/08 11:41:03 siva Exp $
 *
 * Description: This file contains LSR-MIB database typedefs 
 *******************************************************************/
#ifndef   _MPLSLSR_H_
#define   _MPLSLSR_H_
#include "teextrn.h"

#define MAX_LSP_ID         6            /* MplsLSPID RFC 3811 */
#define MIN_LSP_ID         2
#define TE_TNL_RES_DEF_OFFSET 14 

#define MPLS_RB_EQUAL     0
#define MPLS_RB_GREATER   1
#define MPLS_RB_LESSER    -1


typedef struct IFGMPLSENTRY {
   UINT1 u1SignalingCaps;          /* Signalling Capability */
   UINT1 u1SigCapsLen;
   UINT1 au1Pad[2];
}tGmplsIfEntry;

typedef struct IFMPLSENTRY {
   UINT4 u4Index;             /* Interface Index */
   UINT4 u4LabelMinIn;        /* Min Label In */
   UINT4 u4LabelMinOut;       /* Min Label Out */
   UINT4 u4LabelMaxIn;        /* Max Label In */
   UINT4 u4LabelMaxOut;       /* Max Label Out */
   UINT4 u4TotalBW;           /* Total Band width */
   UINT4 u4AvailableBW;       /* Band width available for Mpls */
   tGmplsIfEntry GmplsIfEntry; /*Entry for Gmpls Interface augmented from mpls
                                Interface table*/
   UINT1 u1PartType;          /* Label per Interface/platform */ 
   UINT1 u1Status;            /* Interface Status */
   UINT1 u1Pad[2];            /* Pad Object */
}tMplsIfEntry;

/******************************************************************************


                          LSR Data Structure Link
     
  
                  pXcIndex                   pOutIndex
  tInSegment ------------------> tXcEntry ------------------>tOutSegment
             <-----------------     |     <------------------
                  pInIndex          |             pXcIndex  
                                  __|
                                 |
                                 V
                            tLblStkEntry 
                               |                    (SLL)
                               |------>tLblEntry ------>tLblEntry--->tLblEntry

******************************************************************************/          
    
typedef struct INSEGPERF {
   UINT4 u4InSegDisConTime;    /* In Segment Discontinuity Time */
}tInSegPerf;

typedef struct LBLSTK {
 UINT4          u4Index;    /* Table Index */
        tTMO_SLL       LblList;    /* Lbl Entries in SLL */ 
}tLblStkEntry;

/*gmplsInSegmentTable which enhances mplsInSegmenTable*/
typedef struct GMPLSINSEG {
   eDirection  InSegmentDirection;
}tGmplsInSegment;

/*gmplsOutSegmentTable which enhances mplsOutSegmenTable*/
typedef struct GMPLSOUTSEG {
   eDirection OutSegmentDirection;
}tGmplsOutSegment;

typedef struct INSEGMENT {
 UINT4              u4Index;                /* Table Index */
 UINT4              u4IfIndex;              /* Interface Index */
 UINT4              u4Label;                /* Incomming Label */
 INT4               i4NPop;                 /* Number of Label to be popped */
 UINT2              u2AddrFmly;             /* IANA Address Family Numner */
 UINT1              u1Owner;                /* Entry Owner */
 UINT1              u1RowStatus;            /* Entry SNMP Row Status */
 VOID              *pXcIndex;               /* XC Table Entry Index */
 VOID              *pTrfIndex;              /* Traffic Param Pointer */
 tLblStkEntry      *mplsLabelIndex;         /* Label Stack Index - 
                                                   For more than one label handling, 
                                                   in this case u4Label should be zero */
 tInSegPerf         InSegPerf;              /* In Segment Performance values */ 
 UINT4              u4InMPXcIndex1;         /* Backup tnl XC index1 */
 UINT4              u4InMPXcIndex2;         /* Backup tnl XC index2 */
 tGmplsInSegment    GmplsInSegment;         /* GMPLS Insegment Entry */
#ifdef LDP_GR_WANTED
 tMplsIpAddress     ILMHwListFec;
 UINT1              u1ILMHwListPrefLen;
 UINT1              u1IsNPBlock;
#endif
 UINT1              u1Storage;              /* Storage Type */
 UINT1              u1HwStatus;             /* H/W status for this S/W entry */ 
 UINT1              u1LblAction; 
#ifdef LDP_GR_WANTED
 UINT1              au1Pad[3];
#else
 UINT1              u1Pad; 
#endif
}tInSegment;

typedef struct OUTSEGPERF {
    /* MPLS_P2MP_LSP_CHANGES - S */
    FS_UINT8 u8OutSegmentPerfHCOctets;  /* Out-segment octet count */
    FS_UINT8 u8OutSegmentPerfHCPackets; /* Out-segment packet count */
    UINT4    u4OutSegmentPerfErrors;    /* Out-segment error count */
    UINT4    u4OutSegmentPerfDiscards;  /* Out-segment discard count */
    /* MPLS_P2MP_LSP_CHANGES - E */
   UINT4 u4OutSegDisConTime;   /* Out  Segment Discontinuity Time */
}tOutSegPerf;


typedef struct OUTSEGMENT {
 UINT4              u4Index;                         /* Table Index */
 UINT4              u4IfIndex;                       /* Interface Index */
 UINT4              u4Label;                         /* Outgoing Label */
 BOOL1              bPushLabel;                      /* Top label to be pushed or not */
 UINT1              u1NHAddrType;                    /* Next Hop Address Type */
 UINT1              u1RowStatus;                     /* Entry SNMP Row Status */
 UINT1              u1Owner;                         /* Entry Owner */
 VOID              *pXcIndex;                        /* XC Table Entry Index */
 VOID              *pTrfIndex;                       /* Traffic Param Pointer */
 tIpAddr            NHAddr;                          /* Next Hop Address */
 UINT1              au1NextHopMac[MAC_ADDR_LEN];     /* NextHop Mac address */
 UINT1              u1Storage;                       /* Storage Type */
 UINT1              u1HwStatus;                      /* H/W status for this S/W entry */

 tGmplsOutSegment   GmplsOutSegment;                 /* GMPLS Outsegment Entry */
 tOutSegPerf   OutSegPerf;                           /* Out Segment Performance values */ 
 tRBNodeEmbd        OutSegmentRBNode;                /* Node for the Label + NHAddr
                                                        based RBTree */
}tOutSegment;


typedef struct LBL {
 tTMO_SLL_NODE Next;        /* Next Lbl Idx in the same Lbl Stack */
  UINT4         u4LblIndex;  /* Label Index in a Index Group */
 UINT4         u4Label;     /* Label */
 UINT1         u1RowStatus; /* SNMP Row Status for this Entry */
 UINT1         u1Storage;   /* Storage Type */
 UINT1         u1Pad[2];    /* Padding Object */
}tLblEntry;

typedef struct XCTABLE {
 UINT4         u4Index;                         /* Table Index */
 tInSegment    *pInIndex;                       /* InSegment Table Index */
 tOutSegment   *pOutIndex;                      /* OutSegment Table Index */
 tLblStkEntry  *mplsLabelIndex;                 /* Label Stack Index */
    tTeTnlInfo    *pTeTnlInfo;                     /* Tunnel back pointer */
 UINT1         au1LspId[MAX_LSP_ID];            /* LSP ID Value */
 UINT1         u1Owner;                         /* Entry Owner */
 UINT1         u1RowStatus;                     /* Entry SNMP Row Status */
 UINT1         u1Storage;                       /* Storage Type */
 UINT1         u1AdminStatus;                   /* Admin Status */
 UINT1         u1OperStatus;                    /* Operational Status */
    UINT1         u1ArpResolveStatus;              /* Arp Resolve Status */
    UINT1         u1SegStatus;                     /* Reports the Segment status */
    UINT1         u1LspIdLen;
    UINT1         au1Pad[2];                       /* Padding */
}tXcEntry; 

/* INSEGMENT MACROS */
#define INSEGMENT_INDEX(pInSegment)          pInSegment->u4Index       /* Table Index */
#define INSEGMENT_IF_INDEX(pInSegment)       pInSegment->u4IfIndex      /* Interface Index */
#define INSEGMENT_LABEL(pInSegment)          pInSegment->u4Label        /* Incomming Label */
#define INSEGMENT_NPOP(pInSegment)           pInSegment->i4NPop         /* Number of Label to be popped */
#define INSEGMENT_ADDR_FAMILY(pInSegment)    pInSegment->u2AddrFmly     /* IANA Address Family Numner */
#define INSEGMENT_OWNER(pInSegment)          pInSegment->u1Owner        /* Entry Owner */
#define INSEGMENT_ROW_STATUS(pInSegment)     pInSegment->u1RowStatus    /* Entry SNMP Row Status */
#define INSEGMENT_HW_STATUS(pInSegment)      pInSegment->u1HwStatus    /* Entry H/W Status */
#define INSEGMENT_STRG_TYPE(pInSegment)      pInSegment->u1Storage     /* Storage Type */
#define INSEGMENT_XC_INDEX(pInSegment)       pInSegment->pXcIndex      /* XC Table Entry Index */
#define INSEGMENT_TRFC_PTR(pInSegment)       pInSegment->pTrfIndex     /* Traffic Param Pointer */
#define INSEGMENT_PERF_PTR(pInSegment)       pInSegment->InSegPerf      /* In Segment Performance values */
#define INSEGMENT_DIRECTION(pInSegment)      pInSegment->GmplsInSegment.InSegmentDirection      /* In-segment Direction */

#define INSEGMENT_PERF_DISC_TIME(pInSegment) pInSegment->InSegPerf.u4InSegDisConTime  

#define OUTSEGMENT_INDEX(pOutSegment)        pOutSegment->u4Index                       /* Table Index */
#define OUTSEGMENT_IF_INDEX(pOutSegment)     pOutSegment->u4IfIndex                     /* Interface Index */
#define OUTSEGMENT_LABEL(pOutSegment)        pOutSegment->u4Label                       /* Outgoing Label */
#define OUTSEGMENT_PUSH_FLAG(pOutSegment)    pOutSegment->bPushLabel                    /* Top label to be pushed or not */
#define OUTSEGMENT_NH_ADDRTYPE(pOutSegment)  pOutSegment->u1NHAddrType                  /* Next Hop Address Type */
#define OUTSEGMENT_ROW_STATUS(pOutSegment)   pOutSegment->u1RowStatus                   /* Entry SNMP Row Status */
#define OUTSEGMENT_OWNER(pOutSegment)        pOutSegment->u1Owner                       /* Entry Owner */
#define OUTSEGMENT_XC_INDEX(pOutSegment)     pOutSegment->pXcIndex                     /* XC Table Entry Index */
#define OUTSEGMENT_TRFC_PTR(pOutSegment)     pOutSegment->pTrfIndex                    /* Traffic Param Pointer */
#define OUTSEGMENT_NH_ADDR(pOutSegment)      pOutSegment->NHAddr.u4_addr[0]            /* Next Hop Address */
#define OUTSEGMENT_NH_IPV6_ADDR(pOutSegment) pOutSegment->NHAddr.u1_addr               /* Next IPv6Hop Address */
#define OUTSEGMENT_STRG_TYPE(pOutSegment)    pOutSegment->u1Storage                     /* Storage Type */
#define OUTSEGMENT_PERF_PTR(pOutSegment)     pOutSegment->OutSegPerf   
#define OUTSEGMENT_DIRECTION(pOutSegment)    pOutSegment->GmplsOutSegment.OutSegmentDirection                     /* Out-segment Direction */
/* MPLS_P2MP_LSP_CHANGES - S */
#define OUTSEGMENT_HW_STATUS(pOutSegment)      pOutSegment->u1HwStatus                  /* Entry H/W Status */
#define OUTSEGMENT_PERF_HC_OCTETS(pOutSegment) pOutSegment->OutSegPerf.u8OutSegmentPerfHCOctets
#define OUTSEGMENT_PERF_HC_PKTS(pOutSegment)   pOutSegment->OutSegPerf.u8OutSegmentPerfHCPackets
#define OUTSEGMENT_PERF_ERRORS(pOutSegment)    pOutSegment->OutSegPerf.u4OutSegmentPerfErrors
#define OUTSEGMENT_PERF_DISCARDS(pOutSegment)  pOutSegment->OutSegPerf.u4OutSegmentPerfDiscards
/* MPLS_P2MP_LSP_CHANGES - E */
#define OUTSEGMENT_PERF_DISC_TIME(pOutSegment) pOutSegment->OutSegPerf.u4OutSegDisConTime  


#define XC_INDEX(pXCEntry)                     pXCEntry->u4Index            
#define XC_ININDEX(pXCEntry)                   pXCEntry->pInIndex          
#define XC_OUTINDEX(pXCEntry)                  pXCEntry->pOutIndex         
#define XC_LBL_INDEX(pXCEntry)                 pXCEntry->mplsLabelIndex    
#define XC_LSP_ID(pXCEntry)                    pXCEntry->au1LspId
#define XC_OWNER(pXCEntry)                     pXCEntry->u1Owner         
#define XC_ROW_STATUS(pXCEntry)                pXCEntry->u1RowStatus        
#define XC_STRG_TYPE(pXCEntry)                 pXCEntry->u1Storage       
#define XC_ADMIN_STATUS(pXCEntry)              pXCEntry->u1AdminStatus      
#define XC_OPER_STATUS(pXCEntry)               pXCEntry->u1OperStatus       
#define XC_ARP_STATUS(pXCEntry)                pXCEntry->u1ArpResolveStatus
#define XC_TNL_TBL_PTR(pXCEntry)               pXCEntry->pTeTnlInfo


  

#define XC_ADMIN_UP            1
#define XC_ADMIN_DOWN          2
#define XC_ADMIN_TEST          3

#define XC_OPER_UP             MPLS_OPER_UP
#define XC_OPER_DOWN           MPLS_OPER_DOWN
#define XC_OPER TEST           MPLS_OPER_TESTING
#define XC_OPER_UNKNOWN        MPLS_OPER_UNKNOWN
#define XC_DORMANT             MPLS_OPER_DORMANT
#define XC_NOT_PRESENT         MPLS_OPER_NOT_PRESENT
#define XC_LOWLAYER_DOWN       MPLS_OPER_LOWER_LAYER_DOWN
#define XC_DEF_ADMN_STATUS     XC_ADMIN_UP

#define LSR_DEFAULT_POPLABEL   1
#define LSR_PER_PLATFORM       1  

#define LSR_ACTIVE             1
#define LSR_NOTINSERVICE       2
#define LSR_NOTREADY           3
#define LSR_CREATEANDGO        4
#define LSR_CREATEANDWAIT      5
#define LSR_DESTROY            6

#define LABEL_STACK_INDEX_MIN_LEN  1
#define LABEL_STACK_INDEX_MAX_LEN  24

#define MIN_OUTSEG_HOP_ADDR_LEN     0
#define MAX_OUTSEG_HOP_ADDR_LEN     255 
     
 
#define OUTSEGMENT_INIT_DEF_VALUES(pOutSegment) \
   OUTSEGMENT_PUSH_FLAG(pOutSegment) = TRUE  ; \
         OUTSEGMENT_TRFC_PTR(pOutSegment)  = NULL; \
         OUTSEGMENT_STRG_TYPE(pOutSegment) = MPLS_STORAGE_VOLATILE; \
   OUTSEGMENT_ROW_STATUS(pOutSegment)= LSR_NOTREADY; \
            OUTSEGMENT_OWNER(pOutSegment)   = MPLS_OWNER_SNMP; \
            pOutSegment->GmplsOutSegment.OutSegmentDirection = MPLS_DIRECTION_FORWARD; \
            OUTSEGMENT_LABEL(pOutSegment) = MPLS_ZERO; \
            OUTSEGMENT_HW_STATUS(pOutSegment) = MPLS_FALSE;  /* MPLS_P2MP_LSP_CHANGES */

#define INSEGMENT_INIT_DEF_VALUES(pInSegment) \
    INSEGMENT_NPOP(pInSegment)        = LSR_DEFAULT_POPLABEL; \
    INSEGMENT_ADDR_FAMILY(pInSegment) = LSR_ADDR_UNKNOWN; \
    INSEGMENT_TRFC_PTR(pInSegment)    = NULL; \
    INSEGMENT_ROW_STATUS(pInSegment)  = LSR_NOTREADY; \
    INSEGMENT_HW_STATUS(pInSegment)  = MPLS_FALSE; \
    INSEGMENT_STRG_TYPE(pInSegment)   = MPLS_STORAGE_VOLATILE; \
             INSEGMENT_OWNER(pInSegment)   = MPLS_OWNER_SNMP; \
             pInSegment->GmplsInSegment.InSegmentDirection = MPLS_DIRECTION_FORWARD;
    
#define XC_INIT_DEF_VALUES(pXCEntry) \
    XC_STRG_TYPE(pXCEntry)    = MPLS_STORAGE_VOLATILE; \
    XC_ADMIN_STATUS(pXCEntry) = XC_DEF_ADMN_STATUS; \
    XC_ROW_STATUS(pXCEntry)   = LSR_NOTREADY; \
             XC_OWNER(pXCEntry)        = MPLS_OWNER_SNMP; \
             XC_OPER_STATUS(pXCEntry)  = XC_OPER_DOWN; \
             XC_ARP_STATUS(pXCEntry)   = MPLS_ARP_RESOLVE_UNKNOWN; \
             MEMSET (XC_LSP_ID(pXCEntry),0,MAX_LSP_ID); \
             XC_TNL_TBL_PTR(pXCEntry)  = NULL; \
             pXCEntry->u1SegStatus = XC_NO_FAULT
 

#define XC_NO_FAULT                0x00
#define XC_INSEGMENT_FAULT         0x01
#define XC_OUTSEGMENT_FAULT        0x02
#define XC_BOTH_SEGMENT_FAULT      0x03

#define XC_IN_SEGMENT              0x01
#define XC_OUT_SEGMENT             0x02
#define XC_BOTH_SEGMENT            0x03
      
/* INSEGMENT MACROS */

/*Macros for Set,Get and Test functions for stdgmlsr.mib*/
#define GMPLS_LSR_INTF_SIGNALLING_CAPS                    1
#define GMPLS_LSR_INSEG_DIRECTION                         2
#define GMPLS_LSR_OUTSEG_DIRECTION                        3

/* Init  and DeInit Routines */
INT4 MplsLSRInit(VOID);
VOID MplsLSRDeInit(VOID);

/* If Table */
tMplsIfEntry *MplsCreateIfTableEntry(UINT4 u4IfIndex);
tMplsIfEntry *MplsGetIfTableEntry(UINT4 u4IfIndex);
INT4 MplsDeleteIfTableEntry(tMplsIfEntry *pMplsIfEntry);
tMplsIfEntry *MplsIfTableNextEntry(UINT4 u4IfIndex);

/* InSegment Table */
tInSegment *MplsCreateInSegmentTableEntry(UINT4 u4Index);
tInSegment *MplsGetInSegmentTableEntry(UINT4 u4Index);
tInSegment *MplsSignalGetInSegmentTableEntry(UINT4 u4IfIndex,UINT4 u4InLabel);
INT4 MplsDeleteInSegmentTableEntry(tInSegment *pInSegment);
tInSegment *MplsInSegmentTableNextEntry(UINT4 u4Index);
INT4 MplsInSegmentSignalRbTreeAdd(tInSegment *pInSegment);
INT4 MplsInSegmentSignalRbTreeDelete(tInSegment *pInSegment);
tInSegment * MplsFwdGetInSegmentTableEntry (UINT4 u4InLabel);

/* OutSegment Table */
tOutSegment *MplsCreateOutSegmentTableEntry(UINT4 u4Index);
tOutSegment *MplsGetOutSegmentTableEntry(UINT4 u4Index);

/* MPLS_P2MP_LSP_CHANGES - S */
INT1 MplsGetOutSegmentPerfStats (UINT4 u4OutIndex);
UINT1 MplsCheckXCForP2mp (tXcEntry* pXcEntry);
INT1 MplsCheckP2mpTnlRoleNotEgress (tTeTnlInfo * pTeTnlInfo);
INT4 MplsDbUpdateP2mpTunnelInHw (tTeTnlInfo * pTeTnlInfo, 
  tXcEntry * pXcEntry, UINT1 u1TnlOperStatus);
INT1 MplsP2mpUpdateBudNode (tTeTnlInfo * pTeTnlInfo, UINT1 u1Status); 
/* MPLS_P2MP_LSP_CHANGES - E */

INT4 MplsDeleteOutSegmentTableEntry(tOutSegment *pOutSegment);
tOutSegment *MplsOutSegmentTableNextEntry(UINT4 u4Index);
tOutSegment *MplsGetOutSegmentEntry (UINT4 u4IfIndex,UINT4 u4OutLabel);
INT4
MplsGetOutSegIfIndexFrmXcIndex (UINT4 u4XcIndex, UINT4 *pu4OutIfIndex);

/* XC Table */
tXcEntry *MplsCreateXCTableEntry(UINT4 u4Index,UINT4 u4InIndex,UINT4 u4OutIndex);
tXcEntry *MplsGetXCTableEntry(UINT4 u4XcIndex, UINT4 u4InIndex, UINT4 u4OutIndex);
INT4 MplsDeleteXCTableEntry(tXcEntry *pXcEntry);
tXcEntry *
MplsGetXCEntryByDirection (UINT4 u4XcIndex, eDirection Direction);
tXcEntry *MplsXCTableNextEntry(UINT4 u4XcIndex, UINT4 u4InIndex, UINT4 u4OutIndex);
UINT1
MplsGetXcIndexFromInIfIndexAndInLabel (UINT4 u4IfIndex, UINT4 u4InLabel,
                                      UINT4 *pu4XcIndex);
tInSegment *MplsGetInSegFromBottomLbl (UINT4 u4BottomLabel);

tXcEntry *
MplsGetXCEntryByLabelStack (UINT4 u4XcIndex);
/* Label Stack Table */
tLblStkEntry *MplsCreateLblStkTableEntry(UINT4 u4Index);
tLblStkEntry *MplsGetLblStkTableEntry(UINT4 u4Index);
INT4 MplsDeleteLblStkTableEntry(tLblStkEntry *pLblStkEntry);
tLblStkEntry *MplsLblStkTableNextEntry(UINT4 u4Index);
INT4
MplsDeleteLblStkAndLabelEntries (tLblStkEntry * pLblStkEntry);
tInSegment * MplsSignalGetTwoInSegmentLabelEntry (UINT4 u4IfIndex, 
                                                  UINT4 u4TopLabel,
                                                  UINT4 u4BottomLabel);
tInSegment * MplsFwdGetTwoInSegmentLabelEntry (UINT4 u4TopLabel,
                                               UINT4 u4BottomLabel);

tLblEntry *MplsAddLblStkLabelEntry(UINT4 u4Index,UINT4 u4LabelIndex);
INT4 MplsDeleteLblStkLabelEntry(UINT4 u4Index,UINT4 u4LabelIndex);
INT4
MplsDeleteStkLblFrmStkLabelEntry (UINT4 u4Index, UINT4 u4StkLabel);

tInSegment *MplsSignalGetNextInSegmentTableEntry(UINT4 u4IfIndex,
                                                 UINT4 u4InLabel);
VOID
MplsDbUpdateXcOperStatus (tXcEntry *pXcEntry, BOOL1 bCheckLlStatus,
                          UINT1 u1OperStatus, UINT1 u1Value);
/* For XC Trap Notification */
INT1 MplsLSRXcOperNotif (tXcEntry *pXCEntry,UINT1 u1State);

/* NP Calls */
INT4 MplsILMAdd(tInSegment *pInSegment, tTeTnlInfo *pTeTnlInfo);
INT4 MplsILMDel(tInSegment *pInSegment);

INT4         MplsILMHwAdd (tInSegment * pInSegment,
                           tTeTnlInfo * pTeTnlInfo, VOID *pSInfo);
INT4         MplsILMHwDel (tInSegment * pInSegment, VOID *pSInfo);
/* MPLS_P2MP_LSP_CHANGES - S */
INT4 MplsP2mpILMHwAdd (tOutSegment * pOutSegment, VOID *pSlotInfo);
INT4 MplsP2mpILMHwDel (tOutSegment * pOutSegment, VOID *pSlotInfo);
VOID MplsDbUpdateP2mpDestOperStatus (tTeTnlInfo * pTeTnlInfo,
                                     tXcEntry   * pXcEntry,
                                     UINT1        u1OperStatus);
/* MPLS_P2MP_LSP_CHANGES - E */

/*MPLS_STDGMPLSR_MIB_FUNC -S*/
INT1
MplsDbGetAllGmplsInterfaceTable ARG_LIST ((INT4 i4MplsInterfaceIndex,
                                       tMplsIfEntry *pGetMplsIfEntry));

INT1 
MplsDbSetAllGmplsInterfaceTable ARG_LIST ((INT4 i4MplsInterfaceIndex,
                                       tMplsIfEntry *pSetMplsIfEntry,
                                       UINT4 u4ObjectId));

INT1
MplsDbTestAllGmplsInterfaceTable ARG_LIST ((UINT4 *pu4ErrorCode,
                             INT4 i4MplsInterfaceIndex,
                             tMplsIfEntry *pSetMplsIfEntry,
                             UINT4 u4ObjectId));
INT1
MplsDbGetAllGmplsInSegmentTable ARG_LIST ((UINT4 u4Index,tInSegment *pGetInSegEntry));

INT1
MplsDbSetAllGmplsInSegmentTable ARG_LIST ((UINT4 u4Index,tInSegment *pSetInSegEntry,UINT4 u4ObjectId));

INT1
MplsDbTestAllGmplsInSegmentTable ARG_LIST ((UINT4 *pu4ErrorCode,
                             UINT4 u4Index,tInSegment *pTestInSegEntry,
                             UINT4 u4ObjectId));

INT1
MplsDbGetAllGmplsOutSegmentTable ARG_LIST ((UINT4 u4Index,tOutSegment *pGetOutSegEntry));

INT1
MplsDbSetAllGmplsOutSegmentTable ARG_LIST ((UINT4 u4Index,tOutSegment *pSetOutSegEntry,UINT4 u4ObjectId));

INT1
MplsDbTestAllGmplsOutSegmentTable ARG_LIST ((UINT4 *pu4ErrorCode,
                             UINT4 u4Index,tOutSegment *pTestOutSegEntry,
                             UINT4 u4ObjectId));
INT1
MplsGetFtnXcValues ARG_LIST ((INT4 i4MplsActionType,
                            tSNMP_OID_TYPE * pRetValMplsFTNActionPointer,
                            UINT4 *pu4XcIndex, UINT4 *pu4InIndex, UINT4 *pu4OutIndex));
/*MPLS_STDGMPLSR_MIB_FUNC -E*/

/* Label + NHAddr based OutSegment RBTree Implementation.
 * Only RSVPTE can use because RBTree Add and Del Logic is 
 * implemented only to support RSVPTE GR Not for Static
 */
INT4 MplsSetOutLabel (tOutSegment *pOutSegment, UINT4 u4Label);
tOutSegment *MplsSignalGetOutSegmentTableEntry(UINT4 u4NextHop,UINT4 u4OutLabel);
#endif /* _LSRTDFS_H_ */
