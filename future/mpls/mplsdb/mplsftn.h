/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mplsftn.h,v 1.14 2014/11/08 11:41:04 siva Exp $
 *
 * Description: This file contains FTN-MIB database typedefs 
 *******************************************************************/
#include "teextrn.h"
#include "ip.h"
#include "ip6util.h"

#ifndef   _MPLSFTN_H_
#define   _MPLSFTN_H_


#define MPLS_FTN_SRC_BIT_MASK       0x01
#define MPLS_FTN_DST_BIT_MASK       0x02
#define MPLS_FTN_SRC_PORT_BIT_MASK  0x04
#define MPLS_FTN_DST_PORT_BIT_MASK  0x08
#define MPLS_FTN_IP_PROTO_BIT_MASK  0x10
#define MPLS_FTN_DSCP_BIT_MASK      0x20


#define MPLS_FTN_ON_NON_TE_LSP 1
#define MPLS_FTN_ON_TE_LSP     2

#define MPLS_APPL_ID           14
#define MPLS_FTN_TYPE          7 

#ifdef MPLS_IPV6_WANTED
#define MPLS_FTN_IPV6_TYPE 8
#endif

#define FTN_MEM_TYPE           MEM_DEFAULT_MEMORY_TYPE


#define  REDIRECTLSP              1 
#define  REDIRECTTUNNEL           2

#define  FTN_ROUTE_EXIST          0 
#define  FTN_ROUTE_NOT_EXIST      1 

#define  FTN_DEST_BITS_SET 0x2

#define  FTN_ACTION_PTR(pFTnEntry) pFtnEntry->pActionPtr

#define CMD_STORAGE_OTHER        MPLS_STORAGE_OTHER 
#define CMD_STORAGE_VOLATILE     MPLS_STORAGE_VOLATILE
#define CMD_STORAGE_NONVOLATILE  MPLS_STORAGE_NONVOLATILE
#define CMD_STORAGE_PERMANENT    MPLS_STORAGE_PERMANENT
#define CMD_STORAGE_READONLY     MPLS_STORAGE_READONLY


enum
{
    CMD_ADDR_TYPE_UNKNOWN = 0,
    CMD_ADDR_TYPE_IPV4 = 1,
    CMD_ADDR_TYPE_IPV6 = 2
};

typedef struct FTNPERFENTRY {
  UINT4 u4MatchedPkts[2];   /* No of Packets matched for this FTN */
  UINT4 u4MatchedOctets[2]; /* No of Octet matched for this FTN */ 
  UINT4 u4FTNDisConTime; /* FTN Discontinuity Time */
}tFtnPerfEntry;

typedef struct FTNENTRY {
        VOID           *pNext; 
        VOID           *pNextAlternate; 
        tTMO_DLL_NODE  FtnInfoNode;
 UINT4          u4FtnIndex;                       /* FTN Entry Index */
 UINT1          au1Descr[MAX_SNMP_ADMIN_STRING];  /* Description for this FTN Entry */
 tIpAddr        SrcAddrMin;                       /* Source Address Min Value */
 tIpAddr        SrcAddrMax;                       /* Source Address Max Value */
 tIpAddr        DestAddrMin;                      /* Destination Address Min Value */
 tIpAddr        DestAddrMax;                      /* Destination Address Max Value */
 UINT4          u4SrcPortMin;                     /* Source Port Min Value */
 UINT4          u4SrcPortMax;                     /* Source Port Max Value */
 UINT4          u4DestPortMin;                    /* Destination Port Min Value */
 UINT4          u4DestPortMax;                    /* Destination Port Max Value */
 INT4           i4Protocol;                       /* Protocol for FTN Selection */
 INT4           i4Dscp;                           /* Diff Server Code Point */
 VOID           *pActionPtr;                      /* Pointer to Tunnel/LSP Entry */
 INT4           i4ActionType;                     /* Action Type is Tunnel/LSP */
 UINT1          u1AddrType;                       /* Address Type */
 UINT1          u1FtnMask;                        /* Mask for Decision */
 UINT1          u1Storage;                        /* Storage Type */
 UINT1          u1RowStatus;                      /* Entry SNMP Row Status */
 UINT1          u1HwStatus;                        /* H/W status for this S/W Entry */
        BOOL1          bIsHostAddrFecForPSN;             /* Host address FEC for the PSN, 
                                                            this is useful in the 
                                                            L2VPN (NON_TE PW type) 
                                                            over LDP over RSVP case */
        UINT1          u1ArpResolveStatus;               /* Arp Resolve Status */
        UINT1          u1RouteStatus;                   /* Route exists in the IP or not*/
        UINT1          u1IsLspUsedByL2VPN;
        UINT1          u1IsLspUsedByL3VPN;
        UINT1          au1Pad[2];
}tFtnEntry;

typedef struct FTNMAPENTRY
{
 UINT4 u4IfIndex;       /* If Index */
 UINT4 u4PrevIndex;     /* Previous FTN Index */
 UINT4 u4CurIndex;      /* Current FTN Index */
 UINT1 u1RowStatus;     /* SNMP Row Status */
 UINT1 u1Storage;       /* Storage Type */
 UINT1 u1Pad[2];        /* Padding Object */
 tFtnPerfEntry FtnPerf; /* FTN Performance Info */
}tFtnMapEntry;

/* Init  and DeInit Routines */
INT4 MplsFTNInit(VOID);

/* IPV4 TRIE */
UINT4 MplsFtnInitTrie (VOID);

#ifdef MPLS_IPV6_WANTED
/* IPV6 TRIE */
UINT4 MplsFtnIpv6InitTrie (VOID);
INT4 MplsRegisterEntrywithIpv6Trie (tFtnEntry * pFtnEntry);
INT4 MplsDeleteFtnIpv6TableEntry (tFtnEntry * pFtnEntry);
tFtnEntry* MplsFwdGetFtnIpv6TableEntry (tIp6Addr *pLookUpAddr);
tFtnEntry* MplsSignalGetFtnIpv6TableEntry(tIp6Addr *pLookUpAddr);
#endif

/* FTN Table */
tFtnEntry *MplsCreateFtnTableEntry(UINT4 u4FtnIndex);
tFtnEntry *MplsGetFtnTableEntry(UINT4 u4FtnIndex);
tFtnEntry *MplsSignalGetFtnTableEntry(UINT4 u4Address);
tFtnEntry *MplsFwdGetFtnTableEntry(UINT4 u4Address);
INT4 MplsDeleteFtnTableEntry(tFtnEntry *pFtnEntry);
tFtnEntry *MplsFtnTableNextEntry(UINT4 u4FtnIndex);
INT4 MplsRegisterEntrywithTrie(tFtnEntry *pFtnEntry);
VOID MplsFtnUpdateSysTime(VOID);
UINT4 MplsFtnGetSysTime(VOID);
/* FTN Map Table */
tFtnMapEntry *MplsCreateFtnMapTableEntry(UINT4 u4IfIndex,UINT4 u4FtnIndex);
tFtnMapEntry *MplsGetFtnMapTableEntry(UINT4 u4IfIndex,UINT4 u4FtnIndex);
INT4 MplsDeleteFtnMapTableEntry(tFtnMapEntry *pFtnMapEntry);
tFtnMapEntry *MplsFtnMapTableNextEntry(UINT4 u4IfIndex, UINT4 u4FtnIndex);
UINT4 MplsFtnLastIndex(UINT4 u4IfIndex);
VOID MplsFtnMapUpdateSysTime(VOID);
UINT4 MplsFtnMapGetSysTime(VOID);
INT4 MplsFtnMaskToPrefixLen (UINT4 u4Mask);

/* Macros Used For Ftn NP  */
INT4 MplsFTNAdd(tFtnEntry *pFtnEntry);
INT4 MplsFTNDel(tFtnEntry *pFtnEntry);

INT4         MplsFTNHwAdd (tFtnEntry * pFtnEntry, VOID *pSInfo);
INT4         MplsFTNHwDel (tFtnEntry * pFtnEntry, VOID *pSInfo);

#endif /* _MPLSFTN_H_ */
