/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdgmldb.h,v 1.1 2011/11/30 09:58:13 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDGMLDB_H
#define _STDGMLDB_H

UINT1 GmplsInterfaceTableINDEX [] = {SNMP_DATA_TYPE_INTEGER32};
UINT1 GmplsInSegmentTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 GmplsOutSegmentTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};

UINT4 stdgml [] ={1,3,6,1,2,1,10,166,15};
tSNMP_OID_TYPE stdgmlOID = {9, stdgml};


UINT4 GmplsInterfaceSignalingCaps [ ] ={1,3,6,1,2,1,10,166,15,1,1,1,1};
UINT4 GmplsInterfaceRsvpHelloPeriod [ ] ={1,3,6,1,2,1,10,166,15,1,1,1,2};
UINT4 GmplsInSegmentDirection [ ] ={1,3,6,1,2,1,10,166,15,1,2,1,1};
UINT4 GmplsInSegmentExtraParamsPtr [ ] ={1,3,6,1,2,1,10,166,15,1,2,1,2};
UINT4 GmplsOutSegmentDirection [ ] ={1,3,6,1,2,1,10,166,15,1,3,1,1};
UINT4 GmplsOutSegmentTTLDecrement [ ] ={1,3,6,1,2,1,10,166,15,1,3,1,2};
UINT4 GmplsOutSegmentExtraParamsPtr [ ] ={1,3,6,1,2,1,10,166,15,1,3,1,3};




tMbDbEntry stdgmlMibEntry[]= {

{{13,GmplsInterfaceSignalingCaps}, GetNextIndexGmplsInterfaceTable, GmplsInterfaceSignalingCapsGet, GmplsInterfaceSignalingCapsSet, GmplsInterfaceSignalingCapsTest, GmplsInterfaceTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, GmplsInterfaceTableINDEX, 1, 0, 0, "64"},

{{13,GmplsInterfaceRsvpHelloPeriod}, GetNextIndexGmplsInterfaceTable, GmplsInterfaceRsvpHelloPeriodGet, GmplsInterfaceRsvpHelloPeriodSet, GmplsInterfaceRsvpHelloPeriodTest, GmplsInterfaceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, GmplsInterfaceTableINDEX, 1, 0, 0, "3000"},

{{13,GmplsInSegmentDirection}, GetNextIndexGmplsInSegmentTable, GmplsInSegmentDirectionGet, GmplsInSegmentDirectionSet, GmplsInSegmentDirectionTest, GmplsInSegmentTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, GmplsInSegmentTableINDEX, 1, 0, 0, NULL},

{{13,GmplsInSegmentExtraParamsPtr}, GetNextIndexGmplsInSegmentTable, GmplsInSegmentExtraParamsPtrGet, GmplsInSegmentExtraParamsPtrSet, GmplsInSegmentExtraParamsPtrTest, GmplsInSegmentTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, GmplsInSegmentTableINDEX, 1, 0, 0, NULL},

{{13,GmplsOutSegmentDirection}, GetNextIndexGmplsOutSegmentTable, GmplsOutSegmentDirectionGet, GmplsOutSegmentDirectionSet, GmplsOutSegmentDirectionTest, GmplsOutSegmentTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, GmplsOutSegmentTableINDEX, 1, 0, 0, NULL},

{{13,GmplsOutSegmentTTLDecrement}, GetNextIndexGmplsOutSegmentTable, GmplsOutSegmentTTLDecrementGet, GmplsOutSegmentTTLDecrementSet, GmplsOutSegmentTTLDecrementTest, GmplsOutSegmentTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, GmplsOutSegmentTableINDEX, 1, 0, 0, "0"},

{{13,GmplsOutSegmentExtraParamsPtr}, GetNextIndexGmplsOutSegmentTable, GmplsOutSegmentExtraParamsPtrGet, GmplsOutSegmentExtraParamsPtrSet, GmplsOutSegmentExtraParamsPtrTest, GmplsOutSegmentTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, GmplsOutSegmentTableINDEX, 1, 0, 0, NULL},
};
tMibData stdgmlEntry = { 7, stdgmlMibEntry };

#endif /* _STDGMLDB_H */

