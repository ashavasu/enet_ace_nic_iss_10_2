#ifndef _FTNWR_H
#define _FTNWR_H

VOID RegisterFTN(VOID);
INT4 MplsFTNIndexNextGet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNTableLastChangedGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexMplsFTNTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsFTNIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNDescrGet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNMaskGet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNAddrTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNSourceAddrMinGet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNSourceAddrMaxGet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNDestAddrMinGet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNDestAddrMaxGet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNSourcePortMinGet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNSourcePortMaxGet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNDestPortMinGet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNDestPortMaxGet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNProtocolGet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNDscpGet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNActionTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNActionPointerGet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNDescrSet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNMaskSet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNAddrTypeSet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNSourceAddrMinSet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNSourceAddrMaxSet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNDestAddrMinSet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNDestAddrMaxSet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNSourcePortMinSet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNSourcePortMaxSet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNDestPortMinSet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNDestPortMaxSet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNProtocolSet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNDscpSet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNActionTypeSet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNActionPointerSet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsFTNDescrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsFTNMaskTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsFTNAddrTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsFTNSourceAddrMinTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsFTNSourceAddrMaxTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsFTNDestAddrMinTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsFTNDestAddrMaxTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsFTNSourcePortMinTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsFTNSourcePortMaxTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsFTNDestPortMinTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsFTNDestPortMaxTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsFTNProtocolTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsFTNDscpTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsFTNActionTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsFTNActionPointerTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsFTNStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsFTNTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);

















INT4 MplsFTNMapTableLastChangedGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexMplsFTNMapTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsFTNMapIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNMapPrevIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNMapCurrIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNMapRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNMapStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNMapRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNMapStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNMapRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsFTNMapStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsFTNMapTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);


INT4 GetNextIndexMplsFTNPerfTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsFTNPerfIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNPerfCurrIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNPerfMatchedPacketsGet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNPerfMatchedOctetsGet(tSnmpIndex *, tRetVal *);
INT4 MplsFTNPerfDiscontinuityTimeGet(tSnmpIndex *, tRetVal *);
#endif /* _FTNWR_H */
