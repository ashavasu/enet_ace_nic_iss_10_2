#include "mplsdbinc.h"

/* $Id: mplsdbst.c,v 1.2 2014/12/12 11:56:43 siva Exp $ */

/****************************************************************************
 Function    :  MplsDbSetAllGmplsInterfaceTable
 Description :  This function sets GMPLS Interface Table MIB objects                
 Input       :  i4MplsInterfaceIndex - MPLS Interface Index
                pSetMplsIfEntry      - Pointer to MPLS IF Entry
                u4ObjectId           - Object ID
 Output      :  None
 Returns     :  SNMP_SUCCESS / SNMP_FAILURE
****************************************************************************/
INT1
MplsDbSetAllGmplsInterfaceTable (INT4 i4MplsInterfaceIndex,
                                 tMplsIfEntry *pSetMplsIfEntry,
                                 UINT4 u4ObjectId)

{
    tMplsIfEntry *pMplsIfEntry= NULL;
    
    pMplsIfEntry = MplsGetIfTableEntry ((UINT4)i4MplsInterfaceIndex);
    
    if (pMplsIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }
 
    switch (u4ObjectId)
    {
        case GMPLS_LSR_INTF_SIGNALLING_CAPS:
        {
            pMplsIfEntry->GmplsIfEntry.u1SignalingCaps =
                pSetMplsIfEntry->GmplsIfEntry.u1SignalingCaps;
            break;
        }

        default:
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  MplsDbSetAllGmplsInSegmentTable
 Description :  This function sets GMPLS InSegment Table MIB objects                
 Input       :  u4Index              - InSegment Index
                pSetInSegEntry       - Pointer to Insegment Entry
                u4ObjectId           - Object ID
 Output      :  None
 Returns     :  SNMP_SUCCESS / SNMP_FAILURE
****************************************************************************/
INT1
MplsDbSetAllGmplsInSegmentTable (UINT4 u4Index, tInSegment *pSetInSegEntry,
                                 UINT4 u4ObjectId)
{
    tInSegment  *pInSegment = NULL;
    
    pInSegment = MplsGetInSegmentTableEntry (u4Index);
    
    if (pInSegment == NULL)
    {
        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case GMPLS_LSR_INSEG_DIRECTION:
        {
            pInSegment->GmplsInSegment.InSegmentDirection = 
                pSetInSegEntry->GmplsInSegment.InSegmentDirection;
            break;
        }

        default:
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  MplsDbSetAllGmplsOutSegmentTable
 Description :  This function sets GMPLS InSegment Table MIB objects                
 Input       :  u4Index              - OutSegment Index
                pSetOutSegEntry      - Pointer to Outsegment Entry
                u4ObjectId           - Object ID
 Output      :  None
 Returns     :  SNMP_SUCCESS / SNMP_FAILURE
****************************************************************************/
INT1
MplsDbSetAllGmplsOutSegmentTable (UINT4 u4Index, tOutSegment *pSetOutSegEntry,
                                  UINT4 u4ObjectId)
{
    tOutSegment  *pOutSegment = NULL;
    
    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
    
    if (pOutSegment == NULL)
    {
        return SNMP_FAILURE;
    }
    
    switch(u4ObjectId)
    {
        case GMPLS_LSR_OUTSEG_DIRECTION:
        {
            pOutSegment->GmplsOutSegment.OutSegmentDirection
                = pSetOutSegEntry->GmplsOutSegment.OutSegmentDirection;
            break;
        }

        default:
        {
            return SNMP_FAILURE;
        }
    }
    
    return SNMP_SUCCESS;
}
