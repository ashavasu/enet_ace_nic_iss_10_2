/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdlsrlw.c,v 1.49 2016/10/25 06:32:23 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include "mplsdbinc.h"
#include "fssnmp.h"
#include "stdlsrlw.h"
#include "mplcmndb.h"
#include "mplsnp.h"

UINT4               au4TnlResourceTableOid[TE_TNL_RES_DEF_OFFSET] =
    { 1, 3, 6, 1, 2, 1, 10, 166, 3, 2, 6, 1, 2, 0 };
UINT1               gu1NotifEnable = MPLS_SNMP_FALSE;

/* LOW LEVEL Routines for Table : MplsInterfaceTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsInterfaceTable
 Input       :  The Indices
    return SNMP_SUCCESS;
                MplsInterfaceIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsInterfaceTable (INT4 i4MplsInterfaceIndex)
{
    /* Interface table can have index zero meaning per platform
     * entry so, accept zero also valid index */
    if (i4MplsInterfaceIndex < (INT4) gu4MplsDbMaxIfEntries)
    {
        return SNMP_SUCCESS;
    }
    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
               "ValidateIndexInstanceMplsInterfaceTable Failed \t\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsInterfaceTable
 Input       :  The Indices
                MplsInterfaceIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsInterfaceTable (INT4 *pi4MplsInterfaceIndex)
{
    tMplsIfEntry       *pMplsInIfEntry = NULL;
    MPLS_CMN_LOCK ();
    if ((pMplsInIfEntry = MplsIfTableNextEntry (gu4MplsDbMaxIfEntries)) == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "GetFirstIndexMplsInterfaceTable Failed \t\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pi4MplsInterfaceIndex = (INT4)pMplsInIfEntry->u4Index;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsInterfaceTable
 Input       :  The Indices
                MplsInterfaceIndex
                nextMplsInterfaceIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMplsInterfaceTable (INT4 i4MplsInterfaceIndex,
                                   INT4 *pi4NextMplsInterfaceIndex)
{
    tMplsIfEntry       *pMplsIfEntry = NULL;
    UINT4               u4IfIndex = 0;
    MPLS_CMN_LOCK ();
    pMplsIfEntry = MplsIfTableNextEntry ((UINT4)i4MplsInterfaceIndex);
    if (pMplsIfEntry != NULL)
    {
        *pi4NextMplsInterfaceIndex = (INT4)pMplsIfEntry->u4Index;
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    else
    {
        while ((pMplsIfEntry = MplsIfTableNextEntry (u4IfIndex)) != NULL)
        {
            if (pMplsIfEntry->u4Index > (UINT4) i4MplsInterfaceIndex)
            {
                *pi4NextMplsInterfaceIndex = (INT4)pMplsIfEntry->u4Index;
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
            u4IfIndex = pMplsIfEntry->u4Index;
        }
    }
    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
               "GetNextIndexMplsInterfaceTable  Failed \t\n");
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsInterfaceLabelMinIn
 Input       :  The Indices
                MplsInterfaceIndex

                The Object 
                retValMplsInterfaceLabelMinIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsInterfaceLabelMinIn (INT4 i4MplsInterfaceIndex,
                               UINT4 *pu4RetValMplsInterfaceLabelMinIn)
{

    tMplsIfEntry       *pMplsIfEntry = NULL;
    MPLS_CMN_LOCK ();
    if ((pMplsIfEntry = MplsGetIfTableEntry ((UINT4)i4MplsInterfaceIndex)) == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "GetMplsInterfaceLabelMinIn Failed \t\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pu4RetValMplsInterfaceLabelMinIn = pMplsIfEntry->u4LabelMinIn;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsInterfaceLabelMaxIn
 Input       :  The Indices
                MplsInterfaceIndex

                The Object 
                retValMplsInterfaceLabelMaxIn
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsInterfaceLabelMaxIn (INT4 i4MplsInterfaceIndex,
                               UINT4 *pu4RetValMplsInterfaceLabelMaxIn)
{

    tMplsIfEntry       *pMplsIfEntry = NULL;
    MPLS_CMN_LOCK ();
    if ((pMplsIfEntry = MplsGetIfTableEntry ((UINT4)i4MplsInterfaceIndex)) == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "GetMplsInterfaceLabelMaxIn Failed \t\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pu4RetValMplsInterfaceLabelMaxIn = pMplsIfEntry->u4LabelMaxIn;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsInterfaceLabelMinOut
 Input       :  The Indices
                MplsInterfaceIndex

                The Object 
                retValMplsInterfaceLabelMinOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsInterfaceLabelMinOut (INT4 i4MplsInterfaceIndex,
                                UINT4 *pu4RetValMplsInterfaceLabelMinOut)
{

    tMplsIfEntry       *pMplsIfEntry = NULL;
    MPLS_CMN_LOCK ();
    if ((pMplsIfEntry = MplsGetIfTableEntry ((UINT4)i4MplsInterfaceIndex)) == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "GetMplsInterfaceLabelMinOut Failed \t\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pu4RetValMplsInterfaceLabelMinOut = pMplsIfEntry->u4LabelMinOut;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsInterfaceLabelMaxOut
 Input       :  The Indices
                MplsInterfaceIndex

                The Object 
                retValMplsInterfaceLabelMaxOut
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsInterfaceLabelMaxOut (INT4 i4MplsInterfaceIndex,
                                UINT4 *pu4RetValMplsInterfaceLabelMaxOut)
{

    tMplsIfEntry       *pMplsIfEntry = NULL;
    MPLS_CMN_LOCK ();
    if ((pMplsIfEntry = MplsGetIfTableEntry ((UINT4)i4MplsInterfaceIndex)) == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "GetMplsInterfaceLabelMaxOut Failed \t\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pu4RetValMplsInterfaceLabelMaxOut = pMplsIfEntry->u4LabelMaxOut;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsInterfaceTotalBandwidth
 Input       :  The Indices
                MplsInterfaceIndex

                The Object 
                retValMplsInterfaceTotalBandwidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsInterfaceTotalBandwidth (INT4 i4MplsInterfaceIndex,
                                   UINT4 *pu4RetValMplsInterfaceTotalBandwidth)
{

    tMplsIfEntry       *pMplsIfEntry = NULL;
    MPLS_CMN_LOCK ();
    if ((pMplsIfEntry = MplsGetIfTableEntry ((UINT4)i4MplsInterfaceIndex)) == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "GetMplsInterfaceTotalBandwidth Failed \t\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pu4RetValMplsInterfaceTotalBandwidth = pMplsIfEntry->u4TotalBW;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsInterfaceAvailableBandwidth
 Input       :  The Indices
                MplsInterfaceIndex

                The Object 
                retValMplsInterfaceAvailableBandwidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsInterfaceAvailableBandwidth (INT4 i4MplsInterfaceIndex,
                                       UINT4
                                       *pu4RetValMplsInterfaceAvailableBandwidth)
{

    tMplsIfEntry       *pMplsIfEntry = NULL;
    MPLS_CMN_LOCK ();
    if ((pMplsIfEntry = MplsGetIfTableEntry ((UINT4)i4MplsInterfaceIndex)) == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "GetMplsInterfaceAvailableBandwidth Failed \t\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pu4RetValMplsInterfaceAvailableBandwidth = pMplsIfEntry->u4AvailableBW;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsInterfaceLabelParticipationType
 Input       :  The Indices
                MplsInterfaceIndex

                The Object 
                retValMplsInterfaceLabelParticipationType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsInterfaceLabelParticipationType (INT4 i4MplsInterfaceIndex,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pRetValMplsInterfaceLabelParticipationType)
{

    tMplsIfEntry       *pMplsIfEntry = NULL;
    MPLS_CMN_LOCK ();
    if ((pMplsIfEntry = MplsGetIfTableEntry ((UINT4)i4MplsInterfaceIndex)) == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "GetMplsInterfaceLabelParticipationType Failed \t\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MEMCPY (pRetValMplsInterfaceLabelParticipationType->pu1_OctetList,
            (UINT1 *) &pMplsIfEntry->u1PartType, sizeof (UINT1));
    pRetValMplsInterfaceLabelParticipationType->i4_Length = sizeof (UINT1);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MplsInterfacePerfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsInterfacePerfTable
 Input       :  The Indices
                MplsInterfaceIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceMplsInterfacePerfTable (INT4 i4MplsInterfaceIndex)
{

    if (i4MplsInterfaceIndex < (INT4) gu4MplsDbMaxIfEntries)
    {
        return SNMP_SUCCESS;
    }
    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
               "ValidateIndexInstanceMplsInterfacePerfTable Failed \t\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsInterfacePerfTable
 Input       :  The Indices
                MplsInterfaceIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsInterfacePerfTable (INT4 *pi4MplsInterfaceIndex)
{
    tMplsIfEntry       *pMplsInIfEntry = NULL;
    MPLS_CMN_LOCK ();
    /* Zero is also a valid entry so, use MPLS_MAX_LSR_IFENTRY 
     * to specify first entry */
    if ((pMplsInIfEntry = MplsIfTableNextEntry (gu4MplsDbMaxIfEntries)) == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "GetFirstIndexMplsInterfacePerfTable Failed \t\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pi4MplsInterfaceIndex = (INT4)pMplsInIfEntry->u4Index;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsInterfacePerfTable
 Input       :  The Indices
                MplsInterfaceIndex
                nextMplsInterfaceIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMplsInterfacePerfTable (INT4 i4MplsInterfaceIndex,
                                       INT4 *pi4NextMplsInterfaceIndex)
{
    tMplsIfEntry       *pMplsIfEntry = NULL;
    UINT4               u4IfIndex = 0;
    MPLS_CMN_LOCK ();
    pMplsIfEntry = MplsIfTableNextEntry ((UINT4)i4MplsInterfaceIndex);
    if (pMplsIfEntry != NULL)
    {
        *pi4NextMplsInterfaceIndex = (INT4)pMplsIfEntry->u4Index;
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    else
    {
        while ((pMplsIfEntry = MplsIfTableNextEntry (u4IfIndex)) != NULL)
        {
            if (pMplsIfEntry->u4Index > (UINT4) i4MplsInterfaceIndex)
            {
                *pi4NextMplsInterfaceIndex = (INT4)pMplsIfEntry->u4Index;
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
            u4IfIndex = pMplsIfEntry->u4Index;
        }
    }
    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
               "GetNextIndexMplsInterfaceTable  Failed \t\n");
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsInterfacePerfInLabelsInUse
 Input       :  The Indices
                MplsInterfaceIndex

                The Object 
                retValMplsInterfacePerfInLabelsInUse
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsInterfacePerfInLabelsInUse (INT4 i4MplsInterfaceIndex,
                                      UINT4
                                      *pu4RetValMplsInterfacePerfInLabelsInUse)
{
    tMplsIfEntry       *pMplsIfEntry = NULL;

    MPLS_CMN_LOCK ();
    if ((pMplsIfEntry = MplsGetIfTableEntry ((UINT4)i4MplsInterfaceIndex)) == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "GetMplsInterfacePerfInLabelsInUse Failed \t\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
#ifdef NPAPI_WANTED
    {
        tMplsInputParams    MplsStatsInput;
        tStatsInfo          StatsInfo;

        MEMSET (&MplsStatsInput, 0, sizeof (tMplsInputParams));
        StatsInfo.u8Value.u4Hi = 0;
        StatsInfo.u8Value.u4Lo = 0;

        MplsStatsInput.i1NpReset = FALSE;
        MplsStatsInput.InputType = MPLS_GET_IF_STATS;
        MplsStatsInput.u.MplsIfParam.u4IntfNum = pMplsIfEntry->u4Index;
        MplsFsMplsHwGetMplsStats (&MplsStatsInput,
                                  MPLS_HW_STAT_IF_IN_LABELS_INUSE, &StatsInfo);
        *pu4RetValMplsInterfacePerfInLabelsInUse = StatsInfo.u8Value.u4Lo;
    }
#else
    CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                "GetMplsInterfacePerfInLabelsInUse succeeded for index %d\n",
                pMplsIfEntry->u4Index);
    *pu4RetValMplsInterfacePerfInLabelsInUse = 0;
#endif
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsInterfacePerfInLabelLookupFailures
 Input       :  The Indices
                MplsInterfaceIndex

                The Object 
                retValMplsInterfacePerfInLabelLookupFailures
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsInterfacePerfInLabelLookupFailures (INT4 i4MplsInterfaceIndex,
                                              UINT4
                                              *pu4RetValMplsInterfacePerfInLabelLookupFailures)
{
    tMplsIfEntry       *pMplsIfEntry = NULL;

    MPLS_CMN_LOCK ();
    if ((pMplsIfEntry = MplsGetIfTableEntry ((UINT4)i4MplsInterfaceIndex)) == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "GetMplsInterfacePerfInLabelLookupFailures Failed \t\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
#ifdef NPAPI_WANTED
    {
        tMplsInputParams    MplsStatsInput;
        tStatsInfo          StatsInfo;

        MEMSET (&MplsStatsInput, 0, sizeof (tMplsInputParams));
        StatsInfo.u8Value.u4Hi = 0;
        StatsInfo.u8Value.u4Lo = 0;

        MplsStatsInput.i1NpReset = FALSE;
        MplsStatsInput.InputType = MPLS_GET_IF_STATS;
        MplsStatsInput.u.MplsIfParam.u4IntfNum = pMplsIfEntry->u4Index;
        MplsFsMplsHwGetMplsStats (&MplsStatsInput,
                                  MPLS_HW_STAT_IF_IN_LBL_LOOKUP_FAILURES,
                                  &StatsInfo);
        *pu4RetValMplsInterfacePerfInLabelLookupFailures =
            StatsInfo.u8Value.u4Lo;
    }
#else
    CMNDB_DBG1
        (DEBUG_DEBUG_LEVEL,
         "GetMplsInterfacePerfInLabelLookupFailures succeeded for index %d\n",
         pMplsIfEntry->u4Index);

    *pu4RetValMplsInterfacePerfInLabelLookupFailures = 0;
#endif
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsInterfacePerfOutLabelsInUse
 Input       :  The Indices
                MplsInterfaceIndex

                The Object 
                retValMplsInterfacePerfOutLabelsInUse
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsInterfacePerfOutLabelsInUse (INT4 i4MplsInterfaceIndex,
                                       UINT4
                                       *pu4RetValMplsInterfacePerfOutLabelsInUse)
{

    tMplsIfEntry       *pMplsIfEntry = NULL;
    MPLS_CMN_LOCK ();
    if ((pMplsIfEntry = MplsGetIfTableEntry ((UINT4)i4MplsInterfaceIndex)) == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "GetMplsInterfacePerfOutLabelsInUse Failed \t\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
#ifdef NPAPI_WANTED
    {
        tMplsInputParams    MplsStatsInput;
        tStatsInfo          StatsInfo;

        MEMSET (&MplsStatsInput, 0, sizeof (tMplsInputParams));
        StatsInfo.u8Value.u4Hi = 0;
        StatsInfo.u8Value.u4Lo = 0;

        MplsStatsInput.i1NpReset = FALSE;
        MplsStatsInput.InputType = MPLS_GET_IF_STATS;
        MplsStatsInput.u.MplsIfParam.u4IntfNum = pMplsIfEntry->u4Index;
        MplsFsMplsHwGetMplsStats (&MplsStatsInput,
                                  MPLS_HW_STAT_IF_OUT_LABELS_INUSE, &StatsInfo);
        *pu4RetValMplsInterfacePerfOutLabelsInUse = StatsInfo.u8Value.u4Lo;
    }
#else
    CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                "GetMplsInterfacePerfOutLabelsInUse succeeded for index %d\n",
                pMplsIfEntry->u4Index);

    *pu4RetValMplsInterfacePerfOutLabelsInUse = 0;
#endif
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsInterfacePerfOutFragmentedPkts
 Input       :  The Indices
                MplsInterfaceIndex

                The Object 
                retValMplsInterfacePerfOutFragmentedPkts
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsInterfacePerfOutFragmentedPkts (INT4 i4MplsInterfaceIndex,
                                          UINT4
                                          *pu4RetValMplsInterfacePerfOutFragmentedPkts)
{
    tMplsIfEntry       *pMplsIfEntry = NULL;

    MPLS_CMN_LOCK ();
    if ((pMplsIfEntry = MplsGetIfTableEntry ((UINT4)i4MplsInterfaceIndex)) == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "GetMplsInterfacePerfOutLabelsInUse Failed \t\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
#ifdef NPAPI_WANTED
    {
        tMplsInputParams    MplsStatsInput;
        tStatsInfo          StatsInfo;

        MEMSET (&MplsStatsInput, 0, sizeof (tMplsInputParams));
        StatsInfo.u8Value.u4Hi = 0;
        StatsInfo.u8Value.u4Lo = 0;

        MplsStatsInput.i1NpReset = FALSE;
        MplsStatsInput.InputType = MPLS_GET_IF_STATS;
        MplsStatsInput.u.MplsIfParam.u4IntfNum = pMplsIfEntry->u4Index;
        MplsFsMplsHwGetMplsStats (&MplsStatsInput,
                                  MPLS_HW_STAT_IF_OUT_FRAGMENTED_PKTS,
                                  &StatsInfo);
        *pu4RetValMplsInterfacePerfOutFragmentedPkts = StatsInfo.u8Value.u4Lo;
    }
#else
    *pu4RetValMplsInterfacePerfOutFragmentedPkts = 0;
    CMNDB_DBG1
        (DEBUG_DEBUG_LEVEL,
         "nmhGetMplsInterfacePerfOutFragmentedPkts : Interface index is %d",
         pMplsIfEntry->u4Index);
#endif
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsInSegmentIndexNext
 Input       :  The Indices

                The Object 
                retValMplsInSegmentIndexNext
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsInSegmentIndexNext (tSNMP_OCTET_STRING_TYPE *
                              pRetValMplsInSegmentIndexNext)
{
    UINT4               u4NextIndex;

    u4NextIndex = MplsInSegmentGetIndex ();
    MPLS_INTEGER_TO_OCTETSTRING (u4NextIndex, pRetValMplsInSegmentIndexNext);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MplsInSegmentTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceMplsInSegmentTable
Input       :  The Indices
MplsInSegmentIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsInSegmentTable (tSNMP_OCTET_STRING_TYPE *
                                            pMplsInSegmentIndex)
{
    UINT4               u4Index;
    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);
    if ((u4Index == 0) || (u4Index > gu4MplsDbMaxInSegEntries))
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhValidateIndexInstanceMplsInSegmentTable :"
                   "Invalid Indices \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetFirstIndexMplsInSegmentTable
Input       :  The Indices
MplsInSegmentIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsInSegmentTable (tSNMP_OCTET_STRING_TYPE *
                                    pMplsInSegmentIndex)
{
    tInSegment         *pInSegment = NULL;

    /* Locks are provided to ensure that some other thread may not 
     * modify the same Data Structure */
    MPLS_CMN_LOCK ();

    pInSegment = MplsInSegmentTableNextEntry (0);

    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhGetFirstIndexMplsInSegmentTable :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;

    }
    MPLS_INTEGER_TO_OCTETSTRING (INSEGMENT_INDEX (pInSegment),
                                 pMplsInSegmentIndex);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetNextIndexMplsInSegmentTable
Input       :  The Indices
MplsInSegmentIndex
nextMplsInSegmentIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMplsInSegmentTable (tSNMP_OCTET_STRING_TYPE *
                                   pMplsInSegmentIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pNextMplsInSegmentIndex)
{
    UINT4               u4Index;
    UINT4               u4InIndex = 0;
    tInSegment         *pInSegment = NULL;

    if (pMplsInSegmentIndex->i4_Length < 0)
    {
        return SNMP_FAILURE;
    }

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pInSegment = MplsInSegmentTableNextEntry (u4Index);
    if (pInSegment != NULL)
    {
        MPLS_INTEGER_TO_OCTETSTRING (INSEGMENT_INDEX (pInSegment),
                                     pNextMplsInSegmentIndex);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    else
    {
        /* For Partial Index Support */
        while ((pInSegment = MplsInSegmentTableNextEntry (u4InIndex)) != NULL)
        {
            if (INSEGMENT_INDEX (pInSegment) > u4Index)
            {
                MPLS_INTEGER_TO_OCTETSTRING (INSEGMENT_INDEX (pInSegment),
                                             pNextMplsInSegmentIndex);
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
            u4InIndex = INSEGMENT_INDEX (pInSegment);
        }
    }
    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetNextIndexMplsInSegmentTable :"
               "Indexed Entry Unavailable \r\n");
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetMplsInSegmentInterface
Input       :  The Indices
MplsInSegmentIndex

The Object 
retValMplsInSegmentInterface
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsInSegmentInterface (tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
                              INT4 *pi4RetValMplsInSegmentInterface)
{
    UINT4               u4Index;
    tInSegment         *pInSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pInSegment = MplsGetInSegmentTableEntry (u4Index);
    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetMplsInSegmentInterface :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pi4RetValMplsInSegmentInterface = (INT4) INSEGMENT_IF_INDEX (pInSegment);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetMplsInSegmentLabel
Input       :  The Indices MplsInSegmentIndex The Object 
               retValMplsInSegmentLabel
Output      :  The Get Low Lev Routine Take the Indices &
               store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsInSegmentLabel (tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
                          UINT4 *pu4RetValMplsInSegmentLabel)
{
    UINT4               u4Index;
    tInSegment         *pInSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();

    pInSegment = MplsGetInSegmentTableEntry (u4Index);
    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetMplsInSegmentLabel :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pu4RetValMplsInSegmentLabel = INSEGMENT_LABEL (pInSegment);

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetMplsInSegmentLabelPtr
Input       :  The Indices
MplsInSegmentIndex

The Object 
retValMplsInSegmentLabelPtr
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsInSegmentLabelPtr (tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
                             tSNMP_OID_TYPE * pRetValMplsInSegmentLabelPtr)
{
    UINT4               u4Index;
    tInSegment         *pInSegment = NULL;
    UINT4               au4LblStkTableOid[LBL_STACK_TABLE_MAX_OFFSET + 1] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 2, 1, 13, 1, 1, 0 };

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();

    pInSegment = MplsGetInSegmentTableEntry (u4Index);
    if (pInSegment == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (pInSegment->mplsLabelIndex == NULL)
    {
        pRetValMplsInSegmentLabelPtr->pu4_OidList[0] = 0;
        pRetValMplsInSegmentLabelPtr->pu4_OidList[1] = 0;
        pRetValMplsInSegmentLabelPtr->u4_Length = 2;

        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }

    pRetValMplsInSegmentLabelPtr->u4_Length = LBL_STACK_TABLE_MAX_OFFSET + 1;
    MEMCPY (pRetValMplsInSegmentLabelPtr->pu4_OidList, au4LblStkTableOid,
            LBL_STACK_TABLE_MAX_OFFSET * sizeof (UINT4));

    /* Fill the value of Label stack index at the last position */
    pRetValMplsInSegmentLabelPtr->pu4_OidList[LBL_STACK_TABLE_MAX_OFFSET]
        = pInSegment->mplsLabelIndex->u4Index;

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetMplsInSegmentNPop
Input       :  The Indices
MplsInSegmentIndex

The Object 
retValMplsInSegmentNPop
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsInSegmentNPop (tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
                         INT4 *pi4RetValMplsInSegmentNPop)
{
    UINT4               u4Index;
    tInSegment         *pInSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();

    pInSegment = MplsGetInSegmentTableEntry (u4Index);
    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetMplsInSegmentNPop :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pi4RetValMplsInSegmentNPop = INSEGMENT_NPOP (pInSegment);

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetMplsInSegmentAddrFamily
Input       :  The Indices
MplsInSegmentIndex

The Object 
retValMplsInSegmentAddrFamily
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsInSegmentAddrFamily (tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
                               INT4 *pi4RetValMplsInSegmentAddrFamily)
{
    UINT4               u4Index;
    tInSegment         *pInSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();

    pInSegment = MplsGetInSegmentTableEntry (u4Index);
    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetMplsInSegmentAddrFamily :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pi4RetValMplsInSegmentAddrFamily =
        (INT4) INSEGMENT_ADDR_FAMILY (pInSegment);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetMplsInSegmentXCIndex
Input       :  The Indices
MplsInSegmentIndex

The Object 
retValMplsInSegmentXCIndex
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsInSegmentXCIndex (tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValMplsInSegmentXCIndex)
{
    UINT4               u4Index;
    tInSegment         *pInSegment = NULL;
    tXcEntry           *pXcEntry = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();

    pInSegment = MplsGetInSegmentTableEntry (u4Index);
    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetMplsInSegmentXCIndex :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (INSEGMENT_XC_INDEX (pInSegment) == NULL)
    {
        MEMSET (pRetValMplsInSegmentXCIndex->pu1_OctetList, 0, sizeof (UINT1));
        pRetValMplsInSegmentXCIndex->i4_Length = sizeof (UINT1);
    }
    else
    {
        pXcEntry = (tXcEntry *) INSEGMENT_XC_INDEX (pInSegment);
        MPLS_INTEGER_TO_OCTETSTRING (XC_INDEX (pXcEntry),
                                     pRetValMplsInSegmentXCIndex);
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetMplsInSegmentOwner
Input       :  The Indices
MplsInSegmentIndex

The Object 
retValMplsInSegmentOwner
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsInSegmentOwner (tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
                          INT4 *pi4RetValMplsInSegmentOwner)
{
    UINT4               u4Index;
    tInSegment         *pInSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();

    pInSegment = MplsGetInSegmentTableEntry (u4Index);
    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetMplsInSegmentOwner :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pi4RetValMplsInSegmentOwner = (INT4) INSEGMENT_OWNER (pInSegment);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetMplsInSegmentTrafficParamPtr
Input       :  The Indices
               MplsInSegmentIndex

The Object 
retValMplsInSegmentTrafficParamPtr
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1                nmhGetMplsInSegmentTrafficParamPtr
    (tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
     tSNMP_OID_TYPE * pRetValMplsInSegmentTrafficParamPtr)
{
    UINT4               u4Index;
    tInSegment         *pInSegment = NULL;
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pInSegment = MplsGetInSegmentTableEntry (u4Index);
    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhGetMplsInSegmentTrafficParamPtr :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (INSEGMENT_TRFC_PTR (pInSegment) == NULL)
    {
        MEMSET (pRetValMplsInSegmentTrafficParamPtr->pu4_OidList,
                0, sizeof (UINT4) * 2);
        pRetValMplsInSegmentTrafficParamPtr->u4_Length = 2;
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    else
    {
        pTeTrfcParams = INSEGMENT_TRFC_PTR (pInSegment);
        au4TnlResourceTableOid[TE_TNL_RES_DEF_OFFSET - 1] =
            pTeTrfcParams->u4TrfcParamIndex;

        MEMCPY (pRetValMplsInSegmentTrafficParamPtr->pu4_OidList,
                au4TnlResourceTableOid,
                (TE_TNL_RES_DEF_OFFSET * (sizeof (UINT4))));
        pRetValMplsInSegmentTrafficParamPtr->u4_Length = TE_TNL_RES_DEF_OFFSET;

        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }

}

/****************************************************************************
Function    :  nmhGetMplsInSegmentRowStatus
Input       :  The Indices
MplsInSegmentIndex

The Object 
retValMplsInSegmentRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsInSegmentRowStatus (tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
                              INT4 *pi4RetValMplsInSegmentRowStatus)
{
    UINT4               u4Index;
    tInSegment         *pInSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();

    pInSegment = MplsGetInSegmentTableEntry (u4Index);
    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetMplsInSegmentRowStatus :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pi4RetValMplsInSegmentRowStatus = (INT4) INSEGMENT_ROW_STATUS (pInSegment);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhGetMplsInSegmentStorageType
Input       :  The Indices
MplsInSegmentIndex

The Object 
retValMplsInSegmentStorageType
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsInSegmentStorageType (tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
                                INT4 *pi4RetValMplsInSegmentStorageType)
{
    UINT4               u4Index;
    tInSegment         *pInSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();

    pInSegment = MplsGetInSegmentTableEntry (u4Index);
    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetMplsInSegmentStorageType :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pi4RetValMplsInSegmentStorageType =
        (INT4) INSEGMENT_STRG_TYPE (pInSegment);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetMplsInSegmentInterface
Input       :  The Indices
MplsInSegmentIndex

The Object 
setValMplsInSegmentInterface
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsInSegmentInterface (tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
                              INT4 i4SetValMplsInSegmentInterface)
{
    UINT4               u4Index;
    tInSegment         *pInSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();

    pInSegment = MplsGetInSegmentTableEntry (u4Index);
    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhSetMplsInSegmentInterface :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    INSEGMENT_IF_INDEX (pInSegment) = (UINT4)i4SetValMplsInSegmentInterface;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetMplsInSegmentLabel
Input       :  The Indices
MplsInSegmentIndex

The Object 
setValMplsInSegmentLabel
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsInSegmentLabel (tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
                          UINT4 u4SetValMplsInSegmentLabel)
{
    UINT4               u4Index;
    tInSegment         *pInSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();

    pInSegment = MplsGetInSegmentTableEntry (u4Index);
    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhSetMplsInSegmentLabel :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    /* If the Owner of this entry is MPLS_OWNER_SNMP (CLI),
     * then the STATIC Label has to be updated to the Label Manager */
    if (INSEGMENT_OWNER (pInSegment) == MPLS_OWNER_SNMP)
    {
        if ((INSEGMENT_LABEL (pInSegment) != 0) &&
            (INSEGMENT_LABEL (pInSegment) != u4SetValMplsInSegmentLabel))
        {
            if (MplsReleaseLblToLblGroup
                (INSEGMENT_LABEL (pInSegment)) != MPLS_SUCCESS)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n  nmhSetMplsInSegmentLabel :"
                           "Static Label Release Failed \r\n");
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
        }
        if (MplsAssignLblToLblGroup (u4SetValMplsInSegmentLabel)
            == MPLS_FAILURE)
        {
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    INSEGMENT_LABEL (pInSegment) = u4SetValMplsInSegmentLabel;

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhSetMplsInSegmentLabelPtr
Input       :  The Indices
MplsInSegmentIndex

The Object 
setValMplsInSegmentLabelPtr
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsInSegmentLabelPtr (tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
                             tSNMP_OID_TYPE * pSetValMplsInSegmentLabelPtr)
{
    /* Currently we are not supporting external label pointer.
     * we need only 20 bits of label so, existing label object
     * in this table is enough for us */
    UNUSED_PARAM (*pMplsInSegmentIndex);
    UNUSED_PARAM (*pSetValMplsInSegmentLabelPtr);
    return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhSetMplsInSegmentNPop
Input       :  The Indices
                MplsInSegmentIndex

                The Object 
                setValMplsInSegmentNPop
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsInSegmentNPop (tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
                         INT4 i4SetValMplsInSegmentNPop)
{
    UINT4               u4Index;
    tInSegment         *pInSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pInSegment = MplsGetInSegmentTableEntry (u4Index);
    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhSetMplsInSegmentNPop :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    /* If an LSR supports popping of more than one label, this object MUST
     * be set to that number*/
    INSEGMENT_NPOP (pInSegment) = i4SetValMplsInSegmentNPop;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetMplsInSegmentAddrFamily
 Input       :  The Indices
                MplsInSegmentIndex

                The Object 
                setValMplsInSegmentAddrFamily
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsInSegmentAddrFamily (tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
                               INT4 i4SetValMplsInSegmentAddrFamily)
{
    UINT4               u4Index;
    tInSegment         *pInSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pInSegment = MplsGetInSegmentTableEntry (u4Index);
    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhSetMplsInSegmentAddrFamily :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    INSEGMENT_ADDR_FAMILY (pInSegment) =
        (UINT2) i4SetValMplsInSegmentAddrFamily;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetMplsInSegmentTrafficParamPtr
 Input       :  The Indices
                MplsInSegmentIndex

                The Object 
                setValMplsInSegmentTrafficParamPtr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsInSegmentTrafficParamPtr (tSNMP_OCTET_STRING_TYPE *
                                    pMplsInSegmentIndex,
                                    tSNMP_OID_TYPE *
                                    pSetValMplsInSegmentTrafficParamPtr)
{
    UINT4               u4Index;
    UINT4               u4ResIndex;
    tTeTrfcParams      *pTeTrfcParams = NULL;
    tInSegment         *pInSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pInSegment = MplsGetInSegmentTableEntry (u4Index);
    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhSetMplsInSegmentTrafficParamPtr :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    u4ResIndex = pSetValMplsInSegmentTrafficParamPtr->pu4_OidList
        [pSetValMplsInSegmentTrafficParamPtr->u4_Length - 1];

    if (TeCheckTrfcParamInTrfcParamTable (u4ResIndex, &pTeTrfcParams) ==
        TE_SUCCESS)
    {
        INSEGMENT_TRFC_PTR (pInSegment) = (tTeTrfcParams *) pTeTrfcParams;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetMplsInSegmentRowStatus
 Input       :  The Indices
                MplsInSegmentIndex

                The Object 
                setValMplsInSegmentRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsInSegmentRowStatus (tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
                              INT4 i4SetValMplsInSegmentRowStatus)
{
    UINT4               u4Index;
    tInSegment         *pInSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pInSegment = MplsGetInSegmentTableEntry (u4Index);

    switch (i4SetValMplsInSegmentRowStatus)
    {
        case LSR_CREATEANDWAIT:
            if (pInSegment != NULL)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "SetMplsInSegmentRowStatus Failed \t\n");
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            if (MplsInSegmentSetIndex (u4Index) != u4Index)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "SetMplsInSegmentRowStatus Failed \t\n");
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            pInSegment = MplsCreateInSegmentTableEntry (u4Index);
            if (pInSegment == NULL)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "\r\n nmhSetMplsInSegmentRowStatus :"
                           "Indexed Entry Unavailable \r\n");
                MplsInSegmentRelIndex (u4Index);
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;

        case LSR_CREATEANDGO:
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;

        case LSR_NOTINSERVICE:
            if ((pInSegment == NULL) ||
                (INSEGMENT_OWNER (pInSegment) != MPLS_OWNER_SNMP))
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "SetMplsInSegmentRowStatus Failed \t\n");
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            INSEGMENT_ROW_STATUS (pInSegment) = LSR_NOTINSERVICE;
            if (MplsInSegmentSignalRbTreeDelete (pInSegment) == MPLS_FAILURE)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "\r nmhSetMplsInSegmentRowStatus :"
                           " RBTree Signal Delete Failed \n");
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;

        case LSR_DESTROY:
            if ((pInSegment == NULL) ||
                (INSEGMENT_OWNER (pInSegment) != MPLS_OWNER_SNMP))
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "SetMplsInSegmentRowStatus Failed \t\n");
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            if ((INSEGMENT_OWNER (pInSegment) == MPLS_OWNER_SNMP) &&
                (INSEGMENT_LABEL (pInSegment) != 0) &&
                (INSEGMENT_LABEL (pInSegment) != MPLS_INVALID_LABEL))
            {
                if (MplsReleaseLblToLblGroup
                    (INSEGMENT_LABEL (pInSegment)) != MPLS_SUCCESS)
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               "\r\n  nmhSetMplsInSegmentRowStatus :"
                               "Static Label Release Failed \r\n");
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
            }
            if (MplsDeleteInSegmentTableEntry (pInSegment) != MPLS_SUCCESS)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "\r\n  nmhSetMplsInSegmentRowStatus :"
                           "Insegment Entry Delete Failed \r\n");
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            else
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
        case LSR_ACTIVE:
            if ((pInSegment == NULL) ||
                (INSEGMENT_OWNER (pInSegment) != MPLS_OWNER_SNMP))
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "SetMplsInSegmentRowStatus Failed \t\n");
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            INSEGMENT_ROW_STATUS (pInSegment) = LSR_ACTIVE;

            if (MplsInSegmentSignalRbTreeAdd (pInSegment) == MPLS_FAILURE)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "\r\n nmhSetMplsInSegmentRowStatus :"
                           "RB Signal Failed \r\n");
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            if (pInSegment->pXcIndex != NULL)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL, "nmhSetMplsInSegmentRowStatus: "
                           "Updating XC Oper Status\r\n");
                MplsDbUpdateXcOperStatus (pInSegment->pXcIndex, TRUE,
                                          CFA_IF_UP, XC_IN_SEGMENT);
            }
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        default:
            CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhSetMplsInSegmentRowStatus :"
                       "Invalid Input Status\r\n");
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetMplsInSegmentStorageType
 Input       :  The Indices
                MplsInSegmentIndex

                The Object 
                setValMplsInSegmentStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsInSegmentStorageType (tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
                                INT4 i4SetValMplsInSegmentStorageType)
{
    UINT4               u4Index;
    tInSegment         *pInSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pInSegment = MplsGetInSegmentTableEntry (u4Index);
    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhSetMplsInSegmentStorageType :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    INSEGMENT_STRG_TYPE (pInSegment) = (UINT1) i4SetValMplsInSegmentStorageType;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsInSegmentInterface
 Input       :  The Indices
                MplsInSegmentIndex

                The Object 
                testValMplsInSegmentInterface
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsInSegmentInterface (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
                                 INT4 i4TestValMplsInSegmentInterface)
{
    UINT4               u4Index;
    tInSegment         *pInSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);
    if (u4Index == 0 || u4Index > gu4MplsDbMaxInSegEntries)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* If the Participation Type is PER_INTERFACE */
    if ((i4TestValMplsInSegmentInterface < MPLS_MIN_TNL_IF_VALUE) &&
        (i4TestValMplsInSegmentInterface > MPLS_MAX_TNL_IF_VALUE))
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhTestv2MplsInSegmentInterface :" "Invalid If \r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;

    }

    MPLS_CMN_LOCK ();

    pInSegment = MplsGetInSegmentTableEntry (u4Index);
    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n  nmhTestv2MplsInSegmentInterface :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (INSEGMENT_ROW_STATUS (pInSegment) == LSR_ACTIVE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhTestv2MplsInSegmentInterface : "
                   "Cant Modify Objects \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsInSegmentLabel
 Input       :  The Indices
                MplsInSegmentIndex

                The Object 
                testValMplsInSegmentLabel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsInSegmentLabel (UINT4 *pu4ErrorCode,
                             tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
                             UINT4 u4TestValMplsInSegmentLabel)
{
    UINT4               u4Index;
    tInSegment         *pInSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);
    if ((u4Index == 0) || (u4Index > gu4MplsDbMaxInSegEntries))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    MPLS_CMN_LOCK ();

    pInSegment = MplsGetInSegmentTableEntry (u4Index);
    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhTestv2MplsInSegmentLabel :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    /* If the Owner of this entry is MPLS_OWNER_OTHER (CLI in our case),
     * then the STATIC Label Range check is to be done */
    if (INSEGMENT_OWNER (pInSegment) == MPLS_OWNER_SNMP)
    {
        if ((u4TestValMplsInSegmentLabel >=
             gSystemSize.MplsSystemSize.u4MinStaticLblRange) &&
            (u4TestValMplsInSegmentLabel <=
             gSystemSize.MplsSystemSize.u4MaxStaticLblRange))
        {
            if (MplsCheckLabelInGroup (u4TestValMplsInSegmentLabel) ==
                MPLS_FAILURE)
            {
                MPLS_CMN_UNLOCK ();
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        }
        else
        {
            MPLS_CMN_UNLOCK ();
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }
    if (INSEGMENT_ROW_STATUS (pInSegment) == LSR_ACTIVE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\nnmhTestv2MplsInSegmentLabel : "
                   "Cant Modify Objects\r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsInSegmentLabelPtr
 Input       :  The Indices
                MplsInSegmentIndex

                The Object 
                testValMplsInSegmentLabelPtr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsInSegmentLabelPtr (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
                                tSNMP_OID_TYPE * pTestValMplsInSegmentLabelPtr)
{
    /* If the mplsInSegmentLabelPtr is zeroDotZero 
     * then MplsInSegmentLabel MUST have the label
     * associated with this in-segment. If not MplsInSegmentLabel SHOULD
     * be zero and MUST be ignored.Currently we don't 
     * support this because we do not require more label size.
     * label object in this table itself enough for us. we need only
     * 20 bits label */
    UNUSED_PARAM (*pMplsInSegmentIndex);
    UNUSED_PARAM (pTestValMplsInSegmentLabelPtr);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2MplsInSegmentNPop
 Input       :  The Indices
                MplsInSegmentIndex

                The Object 
                testValMplsInSegmentNPop
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsInSegmentNPop (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
                            INT4 i4TestValMplsInSegmentNPop)
{
    UINT4               u4Index;
    tInSegment         *pInSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);
    if ((u4Index == 0) || (u4Index > gu4MplsDbMaxInSegEntries))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* If an LSR supports popping of more than one label, this object MUST
     * be set to that number.*/

    if (i4TestValMplsInSegmentNPop <= 0)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhTestv2MplsInSegmentNPop :" "Wrong POP Value \r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    MPLS_CMN_LOCK ();
    pInSegment = MplsGetInSegmentTableEntry (u4Index);
    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhTestv2MplsInSegmentNPop :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (INSEGMENT_ROW_STATUS (pInSegment) == LSR_ACTIVE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhTestv2MplsInSegmentNPop :"
                   "Cant Modify Objects \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsInSegmentAddrFamily
 Input       :  The Indices
                MplsInSegmentIndex

                The Object 
                testValMplsInSegmentAddrFamily
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsInSegmentAddrFamily (UINT4 *pu4ErrorCode,
                                  tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
                                  INT4 i4TestValMplsInSegmentAddrFamily)
{
    UINT4               u4Index;
    tInSegment         *pInSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);
    if ((u4Index == 0) || (u4Index > gu4MplsDbMaxInSegEntries))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    /* A value of other(0) indicates that the family type is
     * either unknown or undefined; this SHOULD NOT be used
     * at an egress LSR.*/

    switch (i4TestValMplsInSegmentAddrFamily)
    {
        case LSR_ADDR_IPV4:
            break;
        case LSR_ADDR_IPV6:
            break;
        default:
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "\r\n nmhTestv2MplsInSegmentAddrFamily :"
                       "Invalid Address Family\r\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    MPLS_CMN_LOCK ();
    pInSegment = MplsGetInSegmentTableEntry (u4Index);
    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhTestv2MplsInSegmentAddrFamily :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (INSEGMENT_ROW_STATUS (pInSegment) == LSR_ACTIVE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhTestv2MplsInSegmentAddrFamily :"
                   "Cant Modify Objects \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsInSegmentTrafficParamPtr
 Input       :  The Indices
                MplsInSegmentIndex

                The Object 
                testValMplsInSegmentTrafficParamPtr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsInSegmentTrafficParamPtr (UINT4 *pu4ErrorCode,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMplsInSegmentIndex,
                                       tSNMP_OID_TYPE *
                                       pTestValMplsInSegmentTrafficParamPtr)
{
    UINT4               u4Index;
    UINT4               u4ResIndex;
    UINT1               u1Length;
    tInSegment         *pInSegment = NULL;
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);
    if ((u4Index == 0) || (u4Index > gu4MplsDbMaxInSegEntries))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    u1Length = (UINT1) pTestValMplsInSegmentTrafficParamPtr->u4_Length;
    /** Here we expect the OID length to be equal to a value of
    * TE_TNL_RES_DEF_OFFSET= 14. The Row pointer is expected 
    * to have a value of 1.3.6.1.2.1.10.166.3.2.6.1.2.'x'. where the value x 
    * represents the Resource index in the Resource Table.*/
    if (u1Length != TE_TNL_RES_DEF_OFFSET)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhTestv2MplsInSegmentTrafficParamPtr :"
                   "Wrong Traffic Param Length\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    /* Comparing whether the OIDs are same */
    if (MEMCMP (au4TnlResourceTableOid, pTestValMplsInSegmentTrafficParamPtr->
                pu4_OidList, ((u1Length - 1) * sizeof (UINT4))) != 0)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhTestv2MplsInSegmentTrafficParamPtr :"
                   "Wrong Traffic Param \r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    u4ResIndex = pTestValMplsInSegmentTrafficParamPtr->pu4_OidList
        [pTestValMplsInSegmentTrafficParamPtr->u4_Length - 1];
    MPLS_CMN_LOCK ();
    /* Checking whether the Traffic Param is valid */
    if (TeCheckTrfcParamInTrfcParamTable (u4ResIndex, &pTeTrfcParams) !=
        TE_SUCCESS)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhTestv2MplsInSegmentTrafficParamPtr :"
                   "Wrong Traffic Param \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;

    }

    pInSegment = MplsGetInSegmentTableEntry (u4Index);
    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhTestv2MplsInSegmentTrafficParamPtr :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (INSEGMENT_ROW_STATUS (pInSegment) == LSR_ACTIVE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhTestv2MplsInSegmentTrafficParamPtr :"
                   "Cant Modify Objects \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsInSegmentRowStatus
 Input       :  The Indices
                MplsInSegmentIndex

                The Object 
                testValMplsInSegmentRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsInSegmentRowStatus (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
                                 INT4 i4TestValMplsInSegmentRowStatus)
{
    UINT4               u4Index;
    tInSegment         *pInSegment = NULL;
    tXcEntry           *pXCEntry = NULL;
    tOutSegment         *pOutSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);
    if ((u4Index == 0) || (u4Index > gu4MplsDbMaxInSegEntries))
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhTestv2MplsInSegmentRowStatus :"
                   "Invalid Indices \r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    MPLS_CMN_LOCK ();

    pInSegment = MplsGetInSegmentTableEntry (u4Index);

    switch (i4TestValMplsInSegmentRowStatus)
    {
        case LSR_DESTROY:
            if (pInSegment == NULL)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "\r\n nmhTestv2MplsInSegmentRowStatus :"
                           "Indexed Entry Unavailable \r\n");
                MPLS_CMN_UNLOCK ();
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            pXCEntry = INSEGMENT_XC_INDEX (pInSegment);
            if (pXCEntry != NULL)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "\r\n nmhTestv2MplsInSegmentRowStatus :"
                           "XC is Associated.Cant delete!!! \r\n");
                MPLS_CMN_UNLOCK ();
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;

        case LSR_ACTIVE:
            if (pInSegment == NULL)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "\r\n nmhTestv2MplsInSegmentRowStatus :"
                           "Indexed Entry Unavailable \r\n");
                MPLS_CMN_UNLOCK ();
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            if ((INSEGMENT_ROW_STATUS (pInSegment) == LSR_NOTINSERVICE) ||
                (INSEGMENT_ROW_STATUS (pInSegment) == LSR_NOTREADY))
            {
                if ((INSEGMENT_ADDR_FAMILY (pInSegment) == LSR_ADDR_IPV4)
#ifdef MPLS_IPV6_WANTED
					|| (INSEGMENT_ADDR_FAMILY (pInSegment) == LSR_ADDR_IPV6)
#endif
					)
                {


                    pXCEntry = INSEGMENT_XC_INDEX (pInSegment);
                    if (pXCEntry != NULL)
                    {
                        pOutSegment = XC_OUTINDEX(pXCEntry);
                    }

                    if ((pOutSegment != NULL) && (INSEGMENT_LABEL (pInSegment) == 0))
                    {
                        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                                   "\r\n nmhTestv2MplsInSegmentRowStatus :"
                                   "Wrong Label Defined \r\n");
                        MPLS_CMN_UNLOCK ();
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        return SNMP_FAILURE;
                    }
                    else
                    {
                        if (INSEGMENT_XC_INDEX (pInSegment) != NULL)
                        {
                            MPLS_CMN_UNLOCK ();
                            return SNMP_SUCCESS;
                        }
                        else
                        {
                            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                                       "\r\n nmhTestv2MplsInSegmentRowStatus :"
                                       "No Xc Index Defined \r\n");
                            MPLS_CMN_UNLOCK ();
                            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                            return SNMP_FAILURE;
                        }
                    }
                }
                else
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               "\r\n nmhTestv2MplsInSegmentRowStatus :"
                               "No Address Family Defined \r\n");
                    MPLS_CMN_UNLOCK ();
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
            else
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "\r\n nmhTestv2MplsInSegmentRowStatus :"
                           "Wrong RowStatus Input \r\n");
                MPLS_CMN_UNLOCK ();
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        case LSR_CREATEANDWAIT:

            if (pInSegment == NULL)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "\r\n nmhTestv2MplsInSegmentRowStatus :"
                           "Indexed Entry Can be Created \r\n");
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "\r\n nmhTestv2MplsInSegmentRowStatus :"
                       "Indexed Entry Exists \r\n");
            MPLS_CMN_UNLOCK ();
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        case LSR_CREATEANDGO:

            MPLS_CMN_UNLOCK ();
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        case LSR_NOTINSERVICE:

            if (pInSegment == NULL)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "\r\n nmhTestv2MplsInSegmentRowStatus :"
                           "Indexed Entry Doesnt Exists \r\n");
                MPLS_CMN_UNLOCK ();
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            if ((INSEGMENT_ROW_STATUS (pInSegment) == LSR_ACTIVE) ||
                (INSEGMENT_ROW_STATUS (pInSegment) == LSR_NOTINSERVICE))
            {
                /* If XC Exists,then NIS is not allowed to reduce complexity 
                 * because XC is interelated to Out and In Segments.*/
                pXCEntry = INSEGMENT_XC_INDEX (pInSegment);
                if (pXCEntry != NULL &&
                    pXCEntry->u1RowStatus != LSR_NOTINSERVICE)
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               "\r\n nmhTestv2MplsInSegmentRowStatus :"
                               "XC Exists.Cant set to NIS \r\n");
                    MPLS_CMN_UNLOCK ();
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
                else
                {
                    MPLS_CMN_UNLOCK ();
                    return SNMP_SUCCESS;
                }
            }
            else
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "\r\n nmhTestv2MplsInSegmentRowStatus :"
                           "Indexed Entry Cannot be set to this state\r\n");
                MPLS_CMN_UNLOCK ();
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        default:
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "\r\n nmhTestv2MplsInSegmentRowStatus :"
                       "Wrong Input Status \r\n");
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2MplsInSegmentStorageType
 Input       :  The Indices
                MplsInSegmentIndex

                The Object 
                testValMplsInSegmentStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsInSegmentStorageType (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pMplsInSegmentIndex,
                                   INT4 i4TestValMplsInSegmentStorageType)
{
    UINT4               u4Index;
    tInSegment         *pInSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);

    if ((u4Index == 0) || (u4Index > gu4MplsDbMaxInSegEntries))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    switch (i4TestValMplsInSegmentStorageType)
    {
        case MPLS_STORAGE_VOLATILE:
        case MPLS_STORAGE_NONVOLATILE:
            break;
        default:
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "\r\n nmhTestv2MplsInSegmentStorageType :"
                       "Wrong Storage Type \r\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    MPLS_CMN_LOCK ();

    pInSegment = MplsGetInSegmentTableEntry (u4Index);

    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhTestv2MplsInSegmentStorageType :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2MplsInSegmentTable
 Input       :  The Indices
                MplsInSegmentIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MplsInSegmentTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MplsInSegmentPerfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsInSegmentPerfTable
 Input       :  The Indices
                MplsInSegmentIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsInSegmentPerfTable (tSNMP_OCTET_STRING_TYPE *
                                                pMplsInSegmentIndex)
{
    UINT4               u4Index;
    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);
    if ((u4Index == 0) || (u4Index > gu4MplsDbMaxInSegEntries))
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhValidateIndexInstanceMplsInSegmentPerfTable :"
                   "Invalid Indices \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsInSegmentPerfTable
 Input       :  The Indices
                MplsInSegmentIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsInSegmentPerfTable (tSNMP_OCTET_STRING_TYPE *
                                        pMplsInSegmentIndex)
{
    tInSegment         *pInSegment = NULL;

    MPLS_CMN_LOCK ();
    pInSegment = MplsInSegmentTableNextEntry (0);
    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhGetFirstIndexMplsInSegmentPerfTable :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;

    }
    MPLS_INTEGER_TO_OCTETSTRING (INSEGMENT_INDEX (pInSegment),
                                 pMplsInSegmentIndex);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsInSegmentPerfTable
 Input       :  The Indices
                MplsInSegmentIndex
                nextMplsInSegmentIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMplsInSegmentPerfTable (tSNMP_OCTET_STRING_TYPE *
                                       pMplsInSegmentIndex,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pNextMplsInSegmentIndex)
{
    UINT4               u4Index;
    UINT4               u4InIndex = 0;
    tInSegment         *pInSegment = NULL;

    if (pMplsInSegmentIndex->i4_Length < 0)
    {
        return SNMP_FAILURE;
    }

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pInSegment = MplsInSegmentTableNextEntry (u4Index);
    if (pInSegment != NULL)
    {
        MPLS_INTEGER_TO_OCTETSTRING (INSEGMENT_INDEX (pInSegment),
                                     pNextMplsInSegmentIndex);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    else
    {
        /* For Partial Index Support */
        while ((pInSegment = MplsInSegmentTableNextEntry (u4InIndex)) != NULL)
        {
            if (INSEGMENT_INDEX (pInSegment) > u4Index)
            {
                MPLS_INTEGER_TO_OCTETSTRING (INSEGMENT_INDEX (pInSegment),
                                             pNextMplsInSegmentIndex);
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
            u4InIndex = INSEGMENT_INDEX (pInSegment);
        }
    }
    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetNextIndexMplsInSegmentPerfTable :"
               "Indexed Entry Unavailable \r\n");
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsInSegmentPerfOctets
 Input       :  The Indices
                MplsInSegmentIndex

                The Object 
                retValMplsInSegmentPerfOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsInSegmentPerfOctets (tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
                               UINT4 *pu4RetValMplsInSegmentPerfOctets)
{
    UINT4               u4Index;
    tInSegment         *pInSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pInSegment = MplsGetInSegmentTableEntry (u4Index);
    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetMplsInSegmentPerfOctets :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
#ifdef NPAPI_WANTED
    {
        tMplsInputParams    MplsStatsInput;
        tStatsInfo          StatsInfo;

        MEMSET (&MplsStatsInput, 0, sizeof (tMplsInputParams));
        StatsInfo.u8Value.u4Hi = 0;
        StatsInfo.u8Value.u4Lo = 0;

        MplsStatsInput.i1NpReset = FALSE;
        MplsStatsInput.InputType = MPLS_GET_TNL_LBL_STATS;
        MPLS_HW_STATS_INPUT_LABEL (MplsStatsInput) =
            INSEGMENT_LABEL (pInSegment);
        MplsFsMplsHwGetMplsStats (&MplsStatsInput, MPLS_HW_STAT_TUNNEL_INBYTES,
                                  &StatsInfo);
        *pu4RetValMplsInSegmentPerfOctets = StatsInfo.u8Value.u4Lo;
    }
#else
    *pu4RetValMplsInSegmentPerfOctets = 0;
#endif
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsInSegmentPerfPackets
 Input       :  The Indices
                MplsInSegmentIndex

                The Object 
                retValMplsInSegmentPerfPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsInSegmentPerfPackets (tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
                                UINT4 *pu4RetValMplsInSegmentPerfPackets)
{
    UINT4               u4Index;
    tInSegment         *pInSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pInSegment = MplsGetInSegmentTableEntry (u4Index);
    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetMplsInSegmentPerfPackets :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
#ifdef NPAPI_WANTED
    {
        tMplsInputParams    MplsStatsInput;
        tStatsInfo          StatsInfo;

        MEMSET (&MplsStatsInput, 0, sizeof (tMplsInputParams));
        StatsInfo.u8Value.u4Hi = 0;
        StatsInfo.u8Value.u4Lo = 0;

        MplsStatsInput.i1NpReset = FALSE;
        MplsStatsInput.InputType = MPLS_GET_TNL_LBL_STATS;
        MPLS_HW_STATS_INPUT_LABEL (MplsStatsInput) =
            INSEGMENT_LABEL (pInSegment);
        MplsFsMplsHwGetMplsStats (&MplsStatsInput, MPLS_HW_STAT_TUNNEL_INPKTS,
                                  &StatsInfo);
        *pu4RetValMplsInSegmentPerfPackets = StatsInfo.u8Value.u4Lo;
    }
#else
    *pu4RetValMplsInSegmentPerfPackets = 0;
#endif
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsInSegmentPerfErrors
 Input       :  The Indices
                MplsInSegmentIndex

                The Object 
                retValMplsInSegmentPerfErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsInSegmentPerfErrors (tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
                               UINT4 *pu4RetValMplsInSegmentPerfErrors)
{
    UINT4               u4Index;
    tInSegment         *pInSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pInSegment = MplsGetInSegmentTableEntry (u4Index);
    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetMplsInSegmentPerfErrors :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
#ifdef NPAPI_WANTED
    {
        tMplsInputParams    MplsStatsInput;
        tStatsInfo          StatsInfo;

        MEMSET (&MplsStatsInput, 0, sizeof (tMplsInputParams));
        StatsInfo.u8Value.u4Hi = 0;
        StatsInfo.u8Value.u4Lo = 0;

        MplsStatsInput.i1NpReset = FALSE;
        MplsStatsInput.InputType = MPLS_GET_TNL_LBL_STATS;
        MPLS_HW_STATS_INPUT_LABEL (MplsStatsInput) =
            INSEGMENT_LABEL (pInSegment);
        MplsFsMplsHwGetMplsStats (&MplsStatsInput, MPLS_HW_STAT_TUNNEL_INERR,
                                  &StatsInfo);
        *pu4RetValMplsInSegmentPerfErrors = StatsInfo.u8Value.u4Lo;
    }
#else
    *pu4RetValMplsInSegmentPerfErrors = 0;
#endif
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsInSegmentPerfDiscards
 Input       :  The Indices
                MplsInSegmentIndex

                The Object 
                retValMplsInSegmentPerfDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsInSegmentPerfDiscards (tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
                                 UINT4 *pu4RetValMplsInSegmentPerfDiscards)
{
    UINT4               u4Index;
    tInSegment         *pInSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pInSegment = MplsGetInSegmentTableEntry (u4Index);
    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetMplsInSegmentPerfDiscards :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
#ifdef NPAPI_WANTED
    {
        tMplsInputParams    MplsStatsInput;
        tStatsInfo          StatsInfo;

        MEMSET (&MplsStatsInput, 0, sizeof (tMplsInputParams));
        StatsInfo.u8Value.u4Hi = 0;
        StatsInfo.u8Value.u4Lo = 0;

        MplsStatsInput.i1NpReset = FALSE;
        MplsStatsInput.InputType = MPLS_GET_TNL_LBL_STATS;
        MPLS_HW_STATS_INPUT_LABEL (MplsStatsInput) =
            INSEGMENT_LABEL (pInSegment);
        MplsFsMplsHwGetMplsStats (&MplsStatsInput, MPLS_HW_STAT_TNL_IN_DISCARDS,
                                  &StatsInfo);
        *pu4RetValMplsInSegmentPerfDiscards = StatsInfo.u8Value.u4Lo;
    }
#else
    *pu4RetValMplsInSegmentPerfDiscards = 0;
#endif
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsInSegmentPerfHCOctets
 Input       :  The Indices
                MplsInSegmentIndex

                The Object 
                retValMplsInSegmentPerfHCOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsInSegmentPerfHCOctets (tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
                                 tSNMP_COUNTER64_TYPE *
                                 pu8RetValMplsInSegmentPerfHCOctets)
{
    UINT4               u4Index;
    tInSegment         *pInSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pInSegment = MplsGetInSegmentTableEntry (u4Index);
    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetMplsInSegmentPerfHCOctets :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
#ifdef NPAPI_WANTED
    {
        tMplsInputParams    MplsStatsInput;
        tStatsInfo          StatsInfo;

        MEMSET (&MplsStatsInput, 0, sizeof (tMplsInputParams));
        StatsInfo.u8Value.u4Hi = 0;
        StatsInfo.u8Value.u4Lo = 0;

        MplsStatsInput.i1NpReset = FALSE;
        MplsStatsInput.InputType = MPLS_GET_TNL_LBL_STATS;
        MPLS_HW_STATS_INPUT_LABEL (MplsStatsInput) =
            INSEGMENT_LABEL (pInSegment);
        MplsFsMplsHwGetMplsStats (&MplsStatsInput, MPLS_HW_STAT_TUNNEL_INBYTES,
                                  &StatsInfo);
        pu8RetValMplsInSegmentPerfHCOctets->msn = StatsInfo.u8Value.u4Hi;
        pu8RetValMplsInSegmentPerfHCOctets->lsn = StatsInfo.u8Value.u4Lo;
    }
#else
    pu8RetValMplsInSegmentPerfHCOctets->msn = 0;
    pu8RetValMplsInSegmentPerfHCOctets->lsn = 0;
#endif
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsInSegmentPerfDiscontinuityTime
 Input       :  The Indices
                MplsInSegmentIndex

                The Object 
                retValMplsInSegmentPerfDiscontinuityTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsInSegmentPerfDiscontinuityTime (tSNMP_OCTET_STRING_TYPE *
                                          pMplsInSegmentIndex,
                                          UINT4
                                          *pu4RetValMplsInSegmentPerfDiscontinuityTime)
{
    UINT4               u4Index;
    tInSegment         *pInSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pInSegment = MplsGetInSegmentTableEntry (u4Index);
    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhGetMplsInSegmentPerfDiscontinuityTime :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pu4RetValMplsInSegmentPerfDiscontinuityTime =
        INSEGMENT_PERF_DISC_TIME (pInSegment);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsOutSegmentIndexNext
 Input       :  The Indices

                The Object 
                retValMplsOutSegmentIndexNext
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsOutSegmentIndexNext (tSNMP_OCTET_STRING_TYPE *
                               pRetValMplsOutSegmentIndexNext)
{
    UINT4               u4NextIndex;

    u4NextIndex = MplsOutSegmentGetIndex ();
    MPLS_INTEGER_TO_OCTETSTRING (u4NextIndex, pRetValMplsOutSegmentIndexNext);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MplsOutSegmentTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsOutSegmentTable
 Input       :  The Indices
                MplsOutSegmentIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsOutSegmentTable (tSNMP_OCTET_STRING_TYPE *
                                             pMplsOutSegmentIndex)
{
    UINT4               u4Index;
    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);
    if ((u4Index == 0) || (u4Index > gu4MplsDbMaxOutSegEntries))
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhValidateIndexInstanceMplsOutSegmentTable :"
                   "Invalid Indices \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsOutSegmentTable
 Input       :  The Indices
                MplsOutSegmentIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsOutSegmentTable (tSNMP_OCTET_STRING_TYPE *
                                     pMplsOutSegmentIndex)
{
    tOutSegment        *pOutSegment = NULL;

    MPLS_CMN_LOCK ();
    pOutSegment = MplsOutSegmentTableNextEntry (0);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhGetFirstIndexMplsOutSegmentTable :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;

    }
    MPLS_INTEGER_TO_OCTETSTRING (OUTSEGMENT_INDEX (pOutSegment),
                                 pMplsOutSegmentIndex);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsOutSegmentTable
 Input       :  The Indices
                MplsOutSegmentIndex
                nextMplsOutSegmentIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMplsOutSegmentTable (tSNMP_OCTET_STRING_TYPE *
                                    pMplsOutSegmentIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pNextMplsOutSegmentIndex)
{
    UINT4               u4Index;
    UINT4               u4OutIndex = 0;
    tOutSegment        *pOutSegment = NULL;

    if (pMplsOutSegmentIndex->i4_Length < 0)
    {
        return SNMP_FAILURE;
    }

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pOutSegment = MplsOutSegmentTableNextEntry (u4Index);
    if (pOutSegment != NULL)
    {
        MPLS_INTEGER_TO_OCTETSTRING (OUTSEGMENT_INDEX (pOutSegment),
                                     pNextMplsOutSegmentIndex);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    else
    {
        /* For Partial Index Support */
        while ((pOutSegment = MplsOutSegmentTableNextEntry (u4OutIndex))
               != NULL)
        {
            if (OUTSEGMENT_INDEX (pOutSegment) > u4Index)
            {
                MPLS_INTEGER_TO_OCTETSTRING (OUTSEGMENT_INDEX (pOutSegment),
                                             pNextMplsOutSegmentIndex);
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
            u4OutIndex = OUTSEGMENT_INDEX (pOutSegment);
        }
    }
    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetNextIndexMplsOutSegmentTable :"
               "Indexed Entry Unavailable \r\n");
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsOutSegmentInterface
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                retValMplsOutSegmentInterface
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsOutSegmentInterface (tSNMP_OCTET_STRING_TYPE * pMplsOutSegmentIndex,
                               INT4 *pi4RetValMplsOutSegmentInterface)
{
    UINT4               u4Index;
    tOutSegment        *pOutSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetMplsOutSegmentInterface :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pi4RetValMplsOutSegmentInterface = (INT4)OUTSEGMENT_IF_INDEX (pOutSegment);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsOutSegmentPushTopLabel
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                retValMplsOutSegmentPushTopLabel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsOutSegmentPushTopLabel (tSNMP_OCTET_STRING_TYPE *
                                  pMplsOutSegmentIndex,
                                  INT4 *pi4RetValMplsOutSegmentPushTopLabel)
{
    UINT4               u4Index;
    tOutSegment        *pOutSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetMplsOutSegmentPushTopLabel :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;

    }
    *pi4RetValMplsOutSegmentPushTopLabel =
        (INT4) OUTSEGMENT_PUSH_FLAG (pOutSegment);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsOutSegmentTopLabel
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                retValMplsOutSegmentTopLabel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsOutSegmentTopLabel (tSNMP_OCTET_STRING_TYPE * pMplsOutSegmentIndex,
                              UINT4 *pu4RetValMplsOutSegmentTopLabel)
{
    UINT4               u4Index;
    tOutSegment        *pOutSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetMplsOutSegmentTopLabel :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pu4RetValMplsOutSegmentTopLabel = OUTSEGMENT_LABEL (pOutSegment);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsOutSegmentTopLabelPtr
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                retValMplsOutSegmentTopLabelPtr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsOutSegmentTopLabelPtr (tSNMP_OCTET_STRING_TYPE * pMplsOutSegmentIndex,
                                 tSNMP_OID_TYPE *
                                 pRetValMplsOutSegmentTopLabelPtr)
{
    UNUSED_PARAM (*pMplsOutSegmentIndex);
    pRetValMplsOutSegmentTopLabelPtr->pu4_OidList[0] = 0;
    pRetValMplsOutSegmentTopLabelPtr->pu4_OidList[1] = 0;
    pRetValMplsOutSegmentTopLabelPtr->u4_Length = 2;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsOutSegmentNextHopAddrType
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                retValMplsOutSegmentNextHopAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsOutSegmentNextHopAddrType (tSNMP_OCTET_STRING_TYPE *
                                     pMplsOutSegmentIndex,
                                     INT4
                                     *pi4RetValMplsOutSegmentNextHopAddrType)
{
    UINT4               u4Index;
    tOutSegment        *pOutSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhGetMplsOutSegmentNextHopAddrType :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pi4RetValMplsOutSegmentNextHopAddrType =
        (UINT1) OUTSEGMENT_NH_ADDRTYPE (pOutSegment);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsOutSegmentNextHopAddr
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                retValMplsOutSegmentNextHopAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsOutSegmentNextHopAddr (tSNMP_OCTET_STRING_TYPE * pMplsOutSegmentIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValMplsOutSegmentNextHopAddr)
{
    UINT4               u4Index;
    UINT4               u4IPAddr;
    tOutSegment        *pOutSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetMplsOutSegmentNextHopAddr :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;

    }
#ifdef MPLS_IPV6_WANTED
	if (OUTSEGMENT_NH_ADDRTYPE (pOutSegment) == LSR_ADDR_IPV6)
	{
		MEMCPY (pRetValMplsOutSegmentNextHopAddr->pu1_OctetList, 
                OUTSEGMENT_NH_IPV6_ADDR (pOutSegment),
				IPV6_ADDR_LENGTH);

		pRetValMplsOutSegmentNextHopAddr->i4_Length = IPV6_ADDR_LENGTH;
	}
	else
	{
#endif
    	u4IPAddr = OSIX_HTONL (OUTSEGMENT_NH_ADDR (pOutSegment));
	    MEMCPY (pRetValMplsOutSegmentNextHopAddr->pu1_OctetList,
    	        (UINT1 *) &u4IPAddr, IPV4_ADDR_LENGTH);
	    pRetValMplsOutSegmentNextHopAddr->i4_Length = IPV4_ADDR_LENGTH;

#ifdef MPLS_IPV6_WANTED
	}
#endif
    
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsOutSegmentXCIndex
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                retValMplsOutSegmentXCIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsOutSegmentXCIndex (tSNMP_OCTET_STRING_TYPE * pMplsOutSegmentIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValMplsOutSegmentXCIndex)
{
    UINT4               u4Index;
    tOutSegment        *pOutSegment = NULL;
    tXcEntry           *pXcEntry = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);

    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetMplsOutSegmentXCIndex :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;

    }
    if (OUTSEGMENT_XC_INDEX (pOutSegment) == NULL)
    {
        MEMSET (pRetValMplsOutSegmentXCIndex->pu1_OctetList, 0, sizeof (UINT1));
        pRetValMplsOutSegmentXCIndex->i4_Length = sizeof (UINT1);
    }
    else
    {
        pXcEntry = (tXcEntry *) OUTSEGMENT_XC_INDEX (pOutSegment);
        MPLS_INTEGER_TO_OCTETSTRING (XC_INDEX (pXcEntry),
                                     pRetValMplsOutSegmentXCIndex);
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsOutSegmentOwner
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                retValMplsOutSegmentOwner
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsOutSegmentOwner (tSNMP_OCTET_STRING_TYPE * pMplsOutSegmentIndex,
                           INT4 *pi4RetValMplsOutSegmentOwner)
{
    UINT4               u4Index;
    tOutSegment        *pOutSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetMplsOutSegmentOwner :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pi4RetValMplsOutSegmentOwner = (INT4) OUTSEGMENT_OWNER (pOutSegment);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsOutSegmentTrafficParamPtr
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                retValMplsOutSegmentTrafficParamPtr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsOutSegmentTrafficParamPtr (tSNMP_OCTET_STRING_TYPE *
                                     pMplsOutSegmentIndex,
                                     tSNMP_OID_TYPE *
                                     pRetValMplsOutSegmentTrafficParamPtr)
{

    UINT4               u4Index;
    tOutSegment        *pOutSegment = NULL;
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);

    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhGetMplsOutSegmentTrafficParamPtr :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;

    }
    if (OUTSEGMENT_TRFC_PTR (pOutSegment) == NULL)
    {
        MPLS_CMN_UNLOCK ();
        MEMSET (pRetValMplsOutSegmentTrafficParamPtr->pu4_OidList,
                0, sizeof (UINT4) * 2);
        pRetValMplsOutSegmentTrafficParamPtr->u4_Length = 2;
        return SNMP_SUCCESS;
    }
    else
    {
        pTeTrfcParams = OUTSEGMENT_TRFC_PTR (pOutSegment);
        au4TnlResourceTableOid[TE_TNL_RES_DEF_OFFSET - 1] =
            pTeTrfcParams->u4TrfcParamIndex;

        MEMCPY (pRetValMplsOutSegmentTrafficParamPtr->pu4_OidList,
                au4TnlResourceTableOid,
                (TE_TNL_RES_DEF_OFFSET * (sizeof (UINT4))));
        pRetValMplsOutSegmentTrafficParamPtr->u4_Length = TE_TNL_RES_DEF_OFFSET;

        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhGetMplsOutSegmentRowStatus
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                retValMplsOutSegmentRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsOutSegmentRowStatus (tSNMP_OCTET_STRING_TYPE * pMplsOutSegmentIndex,
                               INT4 *pi4RetValMplsOutSegmentRowStatus)
{
    UINT4               u4Index;
    tOutSegment        *pOutSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetMplsOutSegmentRowStatus :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;

    }
    *pi4RetValMplsOutSegmentRowStatus =
        (INT4) OUTSEGMENT_ROW_STATUS (pOutSegment);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsOutSegmentStorageType
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                retValMplsOutSegmentStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsOutSegmentStorageType (tSNMP_OCTET_STRING_TYPE * pMplsOutSegmentIndex,
                                 INT4 *pi4RetValMplsOutSegmentStorageType)
{
    UINT4               u4Index;
    tOutSegment        *pOutSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetMplsOutSegmentStorageType :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;

    }
    *pi4RetValMplsOutSegmentStorageType =
        (INT4) OUTSEGMENT_STRG_TYPE (pOutSegment);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsOutSegmentInterface
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                setValMplsOutSegmentInterface
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsOutSegmentInterface (tSNMP_OCTET_STRING_TYPE * pMplsOutSegmentIndex,
                               INT4 i4SetValMplsOutSegmentInterface)
{
    UINT4               u4Index;
    tOutSegment        *pOutSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhSetMplsOutSegmentInterface :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    OUTSEGMENT_IF_INDEX (pOutSegment) = (UINT4) i4SetValMplsOutSegmentInterface;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetMplsOutSegmentPushTopLabel
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                setValMplsOutSegmentPushTopLabel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsOutSegmentPushTopLabel (tSNMP_OCTET_STRING_TYPE *
                                  pMplsOutSegmentIndex,
                                  INT4 i4SetValMplsOutSegmentPushTopLabel)
{
    UINT4               u4Index;
    tOutSegment        *pOutSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhSetMplsOutSegmentPushTopLabel :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    OUTSEGMENT_PUSH_FLAG (pOutSegment) =
        (BOOL1) i4SetValMplsOutSegmentPushTopLabel;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetMplsOutSegmentTopLabel
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                setValMplsOutSegmentTopLabel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsOutSegmentTopLabel (tSNMP_OCTET_STRING_TYPE * pMplsOutSegmentIndex,
                              UINT4 u4SetValMplsOutSegmentTopLabel)
{
    UINT4               u4Index;
    tOutSegment        *pOutSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhSetMplsOutSegmentTopLabel :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (OUTSEGMENT_PUSH_FLAG (pOutSegment) == MPLS_SNMP_TRUE)
    {
        OUTSEGMENT_LABEL (pOutSegment) = u4SetValMplsOutSegmentTopLabel;
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    else
    {
        /*If Push Label Flag is FALSE then this value should be "0" */
        OUTSEGMENT_LABEL (pOutSegment) = 0;
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;    /* CHeck whether is Failure */
    }
}

/****************************************************************************
 Function    :  nmhSetMplsOutSegmentTopLabelPtr
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                setValMplsOutSegmentTopLabelPtr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsOutSegmentTopLabelPtr (tSNMP_OCTET_STRING_TYPE *
                                 pMplsOutSegmentIndex,
                                 tSNMP_OID_TYPE *
                                 pSetValMplsOutSegmentTopLabelPtr)
{
    /* Currently we are not supporting external label pointer.
     * we need only 20 bits of label so, existing label object
     * in this table is enough for us */
    UNUSED_PARAM (*pMplsOutSegmentIndex);
    UNUSED_PARAM (*pSetValMplsOutSegmentTopLabelPtr);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsOutSegmentNextHopAddrType
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                setValMplsOutSegmentNextHopAddrType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsOutSegmentNextHopAddrType (tSNMP_OCTET_STRING_TYPE *
                                     pMplsOutSegmentIndex,
                                     INT4 i4SetValMplsOutSegmentNextHopAddrType)
{
    UINT4               u4Index;
    tOutSegment        *pOutSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhSetMplsOutSegmentNextHopAddrType :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    OUTSEGMENT_NH_ADDRTYPE (pOutSegment) =
        (UINT1) i4SetValMplsOutSegmentNextHopAddrType;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetMplsOutSegmentNextHopAddr
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                setValMplsOutSegmentNextHopAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsOutSegmentNextHopAddr (tSNMP_OCTET_STRING_TYPE *
                                 pMplsOutSegmentIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValMplsOutSegmentNextHopAddr)
{
    UINT4               u4Index;
    UINT4               u4IPAddr;
    tOutSegment        *pOutSegment = NULL;

#ifdef MPLS_IPV6_WANTED
    tIpAddr             Ipv6Addr;
    MEMSET (&Ipv6Addr, 0 , sizeof (tIpAddr));
#endif

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);
    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhSetMplsOutSegmentNextHopAddr :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
#ifdef MPLS_IPV6_WANTED
	if (pSetValMplsOutSegmentNextHopAddr->i4_Length == IPV6_ADDR_LENGTH)
	{
		MEMCPY (Ipv6Addr.u1_addr, pSetValMplsOutSegmentNextHopAddr->pu1_OctetList,
				IPV6_ADDR_LENGTH);
		MEMCPY (OUTSEGMENT_NH_IPV6_ADDR(pOutSegment), Ipv6Addr.u1_addr,
				IPV6_ADDR_LENGTH);
	}
	else
	{
#endif
    	MEMCPY ((UINT1 *) &u4IPAddr,
        	    pSetValMplsOutSegmentNextHopAddr->pu1_OctetList, IPV4_ADDR_LENGTH);
	    OUTSEGMENT_NH_ADDR (pOutSegment) = OSIX_NTOHL (u4IPAddr);
#ifdef MPLS_IPV6_WANTED
	}
#endif
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetMplsOutSegmentTrafficParamPtr
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                setValMplsOutSegmentTrafficParamPtr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsOutSegmentTrafficParamPtr (tSNMP_OCTET_STRING_TYPE *
                                     pMplsOutSegmentIndex,
                                     tSNMP_OID_TYPE *
                                     pSetValMplsOutSegmentTrafficParamPtr)
{
    UINT4               u4Index;
    UINT4               u4ResIndex;
    tTeTrfcParams      *pTeTrfcParams = NULL;
    tOutSegment        *pOutSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhSetMplsOutSegmentTrafficParamPtr :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    u4ResIndex = pSetValMplsOutSegmentTrafficParamPtr->pu4_OidList
        [pSetValMplsOutSegmentTrafficParamPtr->u4_Length - 1];

    if (TeCheckTrfcParamInTrfcParamTable (u4ResIndex, &pTeTrfcParams) ==
        TE_SUCCESS)
    {
        OUTSEGMENT_TRFC_PTR (pOutSegment) = pTeTrfcParams;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetMplsOutSegmentRowStatus
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                setValMplsOutSegmentRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsOutSegmentRowStatus (tSNMP_OCTET_STRING_TYPE * pMplsOutSegmentIndex,
                               INT4 i4SetValMplsOutSegmentRowStatus)
{

    UINT4               u4Index;
    tOutSegment        *pOutSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);

    switch (i4SetValMplsOutSegmentRowStatus)
    {
        case LSR_CREATEANDWAIT:
            if (pOutSegment != NULL)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "SetMplsOutSegmentRowStatus Failed \t\n");
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            if (MplsOutSegmentSetIndex (u4Index) != u4Index)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "SetMplsOutSegmentRowStatus Failed \t\n");
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            pOutSegment = MplsCreateOutSegmentTableEntry (u4Index);
            if (pOutSegment == NULL)
            {
                MplsOutSegmentRelIndex (u4Index);
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;

        case LSR_CREATEANDGO:
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;

        case LSR_NOTINSERVICE:
            if ((pOutSegment == NULL) ||
                (OUTSEGMENT_OWNER (pOutSegment) != MPLS_OWNER_SNMP))
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "SetMplsOutSegmentRowStatus Failed \t\n");
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            OUTSEGMENT_ROW_STATUS (pOutSegment) = LSR_NOTINSERVICE;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;

        case LSR_DESTROY:
            if ((pOutSegment == NULL) ||
                (OUTSEGMENT_OWNER (pOutSegment) != MPLS_OWNER_SNMP))
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "SetMplsOutSegmentRowStatus Failed \t\n");
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            if (MplsDeleteOutSegmentTableEntry (pOutSegment) != MPLS_SUCCESS)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            else
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }

        case LSR_ACTIVE:
            if ((pOutSegment == NULL) ||
                (OUTSEGMENT_OWNER (pOutSegment) != MPLS_OWNER_SNMP))
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "SetMplsOutSegmentRowStatus Failed \t\n");
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            OUTSEGMENT_ROW_STATUS (pOutSegment) = LSR_ACTIVE;
            if (pOutSegment->pXcIndex != NULL)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL, "nmhSetMplsOutSegmentRowStatus: "
                           "Updating XC Oper status\r\n");
                MplsDbUpdateXcOperStatus (pOutSegment->pXcIndex,
                                          TRUE, CFA_IF_UP, XC_OUT_SEGMENT);
            }
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;

        default:
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetMplsOutSegmentStorageType
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                setValMplsOutSegmentStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsOutSegmentStorageType (tSNMP_OCTET_STRING_TYPE *
                                 pMplsOutSegmentIndex,
                                 INT4 i4SetValMplsOutSegmentStorageType)
{
    UINT4               u4Index;
    tOutSegment        *pOutSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhSetMplsOutSegmentStorageType :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    OUTSEGMENT_STRG_TYPE (pOutSegment) =
        (UINT1) i4SetValMplsOutSegmentStorageType;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsOutSegmentInterface
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                testValMplsOutSegmentInterface
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsOutSegmentInterface (UINT4 *pu4ErrorCode,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pMplsOutSegmentIndex,
                                  INT4 i4TestValMplsOutSegmentInterface)
{
    UINT4               u4Index;
    tOutSegment        *pOutSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);

    if ((u4Index == 0) || (u4Index > gu4MplsDbMaxOutSegEntries))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    if ((i4TestValMplsOutSegmentInterface < MPLS_MIN_TNL_IF_VALUE) &&
        (i4TestValMplsOutSegmentInterface > MPLS_MAX_TNL_IF_VALUE))
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhTestv2MplsOutSegmentInterface :"
                   "Wrong Interface Value\r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhTestv2MplsOutSegmentInterface :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (OUTSEGMENT_ROW_STATUS (pOutSegment) == LSR_ACTIVE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhTestv2MplsOutSegmentInterface :"
                   "Cant Modify Objects \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsOutSegmentPushTopLabel
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                testValMplsOutSegmentPushTopLabel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsOutSegmentPushTopLabel (UINT4 *pu4ErrorCode,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMplsOutSegmentIndex,
                                     INT4 i4TestValMplsOutSegmentPushTopLabel)
{
    UINT4               u4Index;
    tOutSegment        *pOutSegment = NULL;
    tXcEntry           *pXcEntry = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);
    if ((u4Index == 0) || (u4Index > gu4MplsDbMaxOutSegEntries))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhTestv2MplsOutSegmentPushTopLabel :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (OUTSEGMENT_ROW_STATUS (pOutSegment) == LSR_ACTIVE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhTestv2MplsOutSegmentPushTopLabel :"
                   "Cant Modify Objects \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch (i4TestValMplsOutSegmentPushTopLabel)
    {
        case MPLS_SNMP_TRUE:
            break;
        case MPLS_SNMP_FALSE:
            pXcEntry = OUTSEGMENT_XC_INDEX (pOutSegment);
            if (pXcEntry != NULL)
            {
                if ((XC_LBL_INDEX (pXcEntry) != NULL) &&
                    (TMO_SLL_Count (&((XC_LBL_INDEX (pXcEntry))->LblList)) !=
                     0))
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               "\r\n nmhTestv2MplsOutSegmentPushTopLabel :"
                               "Entry Exists in LblEntry \r\n");
                    MPLS_CMN_UNLOCK ();
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }
            break;
        default:
            MPLS_CMN_UNLOCK ();
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2MplsOutSegmentTopLabel
Input       :  The Indices
               MplsOutSegmentIndex
               The Object 
               testValMplsOutSegmentTopLabel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsOutSegmentTopLabel (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE * pMplsOutSegmentIndex,
                                 UINT4 u4TestValMplsOutSegmentTopLabel)
{
    UINT4               u4Index;
    tOutSegment        *pOutSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);
    if ((u4Index == 0) || (u4Index > gu4MplsDbMaxOutSegEntries))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    MPLS_CMN_LOCK ();

    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhTestv2MplsOutSegmentTopLabel :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    /* If the Owner of this entry is MPLS_OWNER_OTHER (CLI in our case),
     * then the STATIC Label Range check is to be done */
    if (OUTSEGMENT_OWNER (pOutSegment) == MPLS_OWNER_SNMP)
    {
        if (((u4TestValMplsOutSegmentTopLabel <
              GEN_MIN_LBL) &&
             (u4TestValMplsOutSegmentTopLabel != MPLS_IPV4_EXPLICIT_NULL_LABEL)
             && (u4TestValMplsOutSegmentTopLabel != MPLS_IMPLICIT_NULL_LABEL))
            || (u4TestValMplsOutSegmentTopLabel >
                GEN_MAX_LBL))
        {
            MPLS_CMN_UNLOCK ();
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    if (OUTSEGMENT_ROW_STATUS (pOutSegment) == LSR_ACTIVE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhTestv2MplsOutSegmentTopLabel :"
                   "Cant Modify Objects \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsOutSegmentTopLabelPtr
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                testValMplsOutSegmentTopLabelPtr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsOutSegmentTopLabelPtr (UINT4 *pu4ErrorCode,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pMplsOutSegmentIndex,
                                    tSNMP_OID_TYPE *
                                    pTestValMplsOutSegmentTopLabelPtr)
{
    /* If the mplsInSegmentLabelPtr is zeroDotZero then 
     * MplsInSegmentLabel MUST have the label associated with 
     * this in-segment. If not MplsInSegmentLabel SHOULD be zero 
     * and MUST be ignored.Currently we don't support this */
    UNUSED_PARAM (*pMplsOutSegmentIndex);
    UNUSED_PARAM (pTestValMplsOutSegmentTopLabelPtr);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2MplsOutSegmentNextHopAddrType
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                testValMplsOutSegmentNextHopAddrType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsOutSegmentNextHopAddrType (UINT4 *pu4ErrorCode,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pMplsOutSegmentIndex,
                                        INT4
                                        i4TestValMplsOutSegmentNextHopAddrType)
{
    UINT4               u4Index;
    tOutSegment        *pOutSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);

    if ((u4Index == 0) || (u4Index > gu4MplsDbMaxOutSegEntries))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    switch (i4TestValMplsOutSegmentNextHopAddrType)
    {
        case LSR_ADDR_IPV4:
            break;
        case LSR_ADDR_UNKNOWN:
            break;
        case LSR_ADDR_IPV6:
            break;
        
		default:
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "\r\n nmhTestv2MplsOutSegmentNextHopAddrType :"
                       "Wrong Next Hop Address Type \r\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhTestv2MplsOutSegmentNextHopAddrType :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (OUTSEGMENT_ROW_STATUS (pOutSegment) == LSR_ACTIVE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhTestv2MplsOutSegmentNextHopAddrType :"
                   "Cant Modify Objects \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsOutSegmentNextHopAddr
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                testValMplsOutSegmentNextHopAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsOutSegmentNextHopAddr (UINT4 *pu4ErrorCode,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pMplsOutSegmentIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValMplsOutSegmentNextHopAddr)
{
    UINT4               u4Index;
    UINT4               u4IPAddr;
    tOutSegment        *pOutSegment = NULL;    
    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);
    if ((u4Index == 0) || (u4Index > gu4MplsDbMaxOutSegEntries))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
#ifdef MPLS_IPV6_WANTED
	/* For IPv6 Prefix */
	if (pTestValMplsOutSegmentNextHopAddr->i4_Length == IPV6_ADDR_LENGTH)
	{
		if (MplsValidateIPv6Addr (pu4ErrorCode,
            pTestValMplsOutSegmentNextHopAddr) == SNMP_FAILURE)
		{
    		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
        		 "nmhTestv2MplsOutSegmentNextHopAddr: ValidateIpv6Addr Failed \t\n");
    		return SNMP_FAILURE;
		}
	}
	else
	{
#endif

    	if ((pTestValMplsOutSegmentNextHopAddr->i4_Length < MIN_OUTSEG_HOP_ADDR_LEN)
        	|| (pTestValMplsOutSegmentNextHopAddr->i4_Length >
            	MAX_OUTSEG_HOP_ADDR_LEN))
	    {
    	    *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        	return SNMP_FAILURE;
	    }

    	MEMCPY ((UINT1 *) &u4IPAddr,
        	    pTestValMplsOutSegmentNextHopAddr->pu1_OctetList, IPV4_ADDR_LENGTH);
	    u4IPAddr = OSIX_NTOHL (u4IPAddr);
        if(u4IPAddr==0)
        {
          CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                    "\r\n nmhTestv2MplsOutSegmentNextHopAddr :"
                    "Invalid Next Hop IP \r\n");
          *pu4ErrorCode=SNMP_ERR_INCONSISTENT_VALUE;
           return SNMP_FAILURE;
        }
    	if (!(IP_IS_ADDR_CLASS_A (u4IPAddr) || IP_IS_ADDR_CLASS_B (u4IPAddr) ||
        	  IP_IS_ADDR_CLASS_C (u4IPAddr)))
	    {
    	    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
        	           "\r\n nmhTestv2MplsOutSegmentNextHopAddr :"
            	       "Wrong Next Hop IP \r\n");
	        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    	    return SNMP_FAILURE;
	    }
#ifdef MPLS_IPV6_WANTED
	}
#endif
    MPLS_CMN_LOCK ();

    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhTestv2MplsOutSegmentNextHopAddr :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (OUTSEGMENT_ROW_STATUS (pOutSegment) == LSR_ACTIVE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhTestv2MplsOutSegmentNextHopAddr :"
                   "Cant Modify Objects \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* Check for Address Type to be used for this Object.
     * Currently we support Both IPv4 & Ipv6*/
    if ((OUTSEGMENT_NH_ADDRTYPE (pOutSegment) != LSR_ADDR_IPV4)
#ifdef MPLS_IPV6_WANTED
			&& (OUTSEGMENT_NH_ADDRTYPE (pOutSegment) != LSR_ADDR_IPV6 )
#endif
		)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhTestv2MplsOutSegmentNextHopAddr :"
                   "Invalid Address Type\r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsOutSegmentTrafficParamPtr
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                testValMplsOutSegmentTrafficParamPtr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsOutSegmentTrafficParamPtr (UINT4 *pu4ErrorCode,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pMplsOutSegmentIndex,
                                        tSNMP_OID_TYPE *
                                        pTestValMplsOutSegmentTrafficParamPtr)
{
    UINT4               u4Index;
    UINT4               u4ResIndex;
    UINT1               u1Length;
    tOutSegment        *pOutSegment;
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);

    if ((u4Index == 0) || (u4Index > gu4MplsDbMaxOutSegEntries))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    u1Length = (UINT1) pTestValMplsOutSegmentTrafficParamPtr->u4_Length;

    /* Here we expect the OID length to be equal to a value of
     *  TE_TNL_RES_DEF_OFFSET= 14. The Row pointer is expected
     *  to have a value of 1.3.6.1.2.1.10.166.3.2.6.1.2.'x'. where the value x
     *  represents the Resource index in the Resource Table.*/
    if (u1Length != TE_TNL_RES_DEF_OFFSET)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhTestv2MplsOutSegmentTrafficParamPtr :"
                   "Wrong Traffic Param \r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    /* Comparing whether the OIDs are same */
    if (MEMCMP (au4TnlResourceTableOid, pTestValMplsOutSegmentTrafficParamPtr->
                pu4_OidList, ((u1Length - 1) * sizeof (UINT4))) != 0)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhTestv2MplsOutSegmentTrafficParamPtr :"
                   "Wrong Traffic Param \r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    u4ResIndex = pTestValMplsOutSegmentTrafficParamPtr->pu4_OidList
        [pTestValMplsOutSegmentTrafficParamPtr->u4_Length - 1];
    MPLS_CMN_LOCK ();
    /* Checking whether the Traffic Param is valid */
    if (TeCheckTrfcParamInTrfcParamTable (u4ResIndex, &pTeTrfcParams) !=
        TE_SUCCESS)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhTestv2MplsOutSegmentTrafficParamPtr :"
                   "Wrong Traffic Param \r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhTestv2MplsOutSegmentTrafficParamPtr :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (OUTSEGMENT_ROW_STATUS (pOutSegment) == LSR_ACTIVE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhTestv2MplsOutSegmentTrafficParamPtr :"
                   "Cant Modify Objects \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsOutSegmentRowStatus
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                testValMplsOutSegmentRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsOutSegmentRowStatus (UINT4 *pu4ErrorCode,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pMplsOutSegmentIndex,
                                  INT4 i4TestValMplsOutSegmentRowStatus)
{
    UINT4               u4Index;
    UINT4               u4IPAddr;
    tOutSegment        *pOutSegment = NULL;
    tCfaIfInfo          CfaIfInfo;
    tXcEntry           *pXcEntry = NULL;

#ifdef MPLS_IPV6_WANTED
	tSNMP_OCTET_STRING_TYPE NextHopIpv6Addr;
	static UINT1        au1NextHopIpv6Addr[IPV6_ADDR_LENGTH];
	NextHopIpv6Addr.pu1_OctetList = au1NextHopIpv6Addr;
#endif
    
    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);
    if ((u4Index == 0) || (u4Index > gu4MplsDbMaxOutSegEntries))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);

    switch (i4TestValMplsOutSegmentRowStatus)
    {
        case LSR_DESTROY:
            if (pOutSegment == NULL)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "\r\n nmhTestv2MplsOutSegmentRowStatus :"
                           "Indexed Entry Unavailable \r\n");
                MPLS_CMN_UNLOCK ();
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            pXcEntry = OUTSEGMENT_XC_INDEX (pOutSegment);
            if (pXcEntry != NULL)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "\r\n nmhTestv2MplsOutSegmentRowStatus :"
                           "XC is Associated.Cant delete!!! \r\n");
                MPLS_CMN_UNLOCK ();
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        case LSR_ACTIVE:
            if (pOutSegment == NULL)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "\r\n nmhTestv2MplsOutSegmentRowStatus :"
                           "Indexed Entry Unavailable \r\n");
                MPLS_CMN_UNLOCK ();
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            if (OUTSEGMENT_XC_INDEX (pOutSegment) == NULL)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "\r\n nmhTestv2MplsOutSegmentRowStatus :"
                           "No Xc Index Defined \r\n");
                MPLS_CMN_UNLOCK ();
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if ((OUTSEGMENT_ROW_STATUS (pOutSegment) == LSR_NOTINSERVICE) ||
                (OUTSEGMENT_ROW_STATUS (pOutSegment) == LSR_NOTREADY))
            {
#ifdef MPLS_IPV6_WANTED
                if (OUTSEGMENT_NH_ADDRTYPE (pOutSegment) == LSR_ADDR_IPV6)
				{
					MEMCPY (NextHopIpv6Addr.pu1_OctetList , OUTSEGMENT_NH_IPV6_ADDR(pOutSegment),
								IPV6_ADDR_LENGTH);
					NextHopIpv6Addr.i4_Length = IPV6_ADDR_LENGTH;

					if (MplsValidateIPv6Addr (pu4ErrorCode,
    								&NextHopIpv6Addr) == SNMP_FAILURE)
					{
					    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
					         "nmhTestv2MplsOutSegmentRowStatus : ValidateIpv6Addr Failed \t\n");
                	    MPLS_CMN_UNLOCK ();
					    return SNMP_FAILURE;
					}
    	            if (CfaGetIfInfo ((UINT2) OUTSEGMENT_IF_INDEX
        	                              (pOutSegment), &CfaIfInfo) != CFA_FAILURE)
            	    {
                	    MPLS_CMN_UNLOCK ();
                    	return SNMP_SUCCESS;
	                }
    	            else
        	        {
            	        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                	               "nmhTestv2MplsOutSegmentRowStatus :"
                                   "No If Entry is present (IPv6) \r\n");
	                    MPLS_CMN_UNLOCK ();
    	                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        	            return SNMP_FAILURE;
            	    }
				}
				else
				{
#endif
                	if ((OUTSEGMENT_NH_ADDRTYPE (pOutSegment) == LSR_ADDR_IPV4) ||
                    	(OUTSEGMENT_NH_ADDRTYPE (pOutSegment) == LSR_ADDR_UNKNOWN))
	                {
    	                u4IPAddr = OUTSEGMENT_NH_ADDR (pOutSegment);
        	            if ((OUTSEGMENT_NH_ADDRTYPE (pOutSegment) == LSR_ADDR_IPV4)
            	            && (!(IP_IS_ADDR_CLASS_A (u4IPAddr))
                	            && !(IP_IS_ADDR_CLASS_B (u4IPAddr))
                    	        && !(IP_IS_ADDR_CLASS_C (u4IPAddr))))
	                    {
    	                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
        	                           "\r\n nmhTestv2MplsOutSegmentRowStatus :"
            	                       "Invalid Next Hop Addr\r\n");
                	        MPLS_CMN_UNLOCK ();
                    	    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        	return SNMP_FAILURE;
	                    }

    	                if (CfaGetIfInfo ((UINT2) OUTSEGMENT_IF_INDEX
        	                              (pOutSegment), &CfaIfInfo) != CFA_FAILURE)
            	        {
                	        MPLS_CMN_UNLOCK ();
                    	    return SNMP_SUCCESS;
	                    }
    	                else
        	            {
            	            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                	                   "nmhTestv2MplsOutSegmentRowStatus :"
                    	               "No If Entry is present \r\n");
	                        MPLS_CMN_UNLOCK ();
    	                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        	                return SNMP_FAILURE;
            	        }

                	}
                	else
                	{
                    	CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                        	       "\r\n nmhTestv2MplsOutSegmentRowStatus :"
                            	   "Invalid Address Type\r\n");
	                    MPLS_CMN_UNLOCK ();
    	                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        	            return SNMP_FAILURE;
            	    }
#ifdef MPLS_IPV6_WANTED
            	}
#endif
			}
            else
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "\r\n nmhTestv2MplsOutSegmentRowStatus :"
                           "Wrong RowStatus Input \r\n");
                MPLS_CMN_UNLOCK ();
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

        case LSR_CREATEANDWAIT:

            if (pOutSegment == NULL)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "\r\n nmhTestv2MplsOutSegmentRowStatus :"
                           "Indexed Entry Can be Created \r\n");
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "\r\n nmhTestv2MplsOutSegmentRowStatus :"
                       "Indexed Entry Already Exits\r\n");
            MPLS_CMN_UNLOCK ();
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        case LSR_CREATEANDGO:

            MPLS_CMN_UNLOCK ();
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        case LSR_NOTINSERVICE:
            if (pOutSegment == NULL)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "\r\n nmhTestv2MplsOutSegmentRowStatus :"
                           "Indexed Entry Doesnt Exists \r\n");
                MPLS_CMN_UNLOCK ();
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            if (OUTSEGMENT_ROW_STATUS (pOutSegment) == LSR_ACTIVE ||
                OUTSEGMENT_ROW_STATUS (pOutSegment) == LSR_NOTINSERVICE)
            {
                /* If XC Exists,then NIS is not allowed to reduce complexity 
                 * because XC is interelated to Out and In Segments.*/
                pXcEntry = OUTSEGMENT_XC_INDEX (pOutSegment);
                if (pXcEntry != NULL &&
                    pXcEntry->u1RowStatus != LSR_NOTINSERVICE)
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               "\r\n nmhTestv2MplsOutSegmentRowStatus :"
                               "XC Exists.Cant set to NIS \r\n");
                    MPLS_CMN_UNLOCK ();
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
                else
                {
                    MPLS_CMN_UNLOCK ();
                    return SNMP_SUCCESS;
                }
            }
            else
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "\r\n nmhTestv2MplsOutSegmentRowStatus :"
                           "Indexed Entry Cannot be set to this state \r\n");
                MPLS_CMN_UNLOCK ();
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
        default:
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "\r\n nmhTestv2MplsOutSegmentRowStatus :"
                       "Wrong Input Status \r\n");
            MPLS_CMN_UNLOCK ();
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2MplsOutSegmentStorageType
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                testValMplsOutSegmentStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsOutSegmentStorageType (UINT4 *pu4ErrorCode,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pMplsOutSegmentIndex,
                                    INT4 i4TestValMplsOutSegmentStorageType)
{
    UINT4               u4Index;
    tOutSegment        *pOutSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);

    if ((u4Index == 0) || (u4Index > gu4MplsDbMaxOutSegEntries))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    switch (i4TestValMplsOutSegmentStorageType)
    {
        case MPLS_STORAGE_VOLATILE:
        case MPLS_STORAGE_NONVOLATILE:
            break;
        default:
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "\r\n nmhTestv2MplsOutSegmentStorageType :"
                       "Wrong Storage Type \r\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    MPLS_CMN_LOCK ();

    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhTestv2MplsOutSegmentStorageType :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2MplsOutSegmentTable
 Input       :  The Indices
                MplsOutSegmentIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MplsOutSegmentTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MplsOutSegmentPerfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsOutSegmentPerfTable
 Input       :  The Indices
                MplsOutSegmentIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsOutSegmentPerfTable (tSNMP_OCTET_STRING_TYPE *
                                                 pMplsOutSegmentIndex)
{
    UINT4               u4Index;
    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);
    if ((u4Index == 0) || (u4Index > gu4MplsDbMaxOutSegEntries))
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhValidateIndexInstanceMplsOutSegmentPerfTable :"
                   "Invalid Indices \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsOutSegmentPerfTable
 Input       :  The Indices
                MplsOutSegmentIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsOutSegmentPerfTable (tSNMP_OCTET_STRING_TYPE *
                                         pMplsOutSegmentIndex)
{
    tOutSegment        *pOutSegment = NULL;

    MPLS_CMN_LOCK ();

    pOutSegment = MplsOutSegmentTableNextEntry (0);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhGetFirstIndexMplsOutSegmentPerfTable :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;

    }
    MPLS_INTEGER_TO_OCTETSTRING (OUTSEGMENT_INDEX (pOutSegment),
                                 pMplsOutSegmentIndex);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsOutSegmentPerfTable
 Input       :  The Indices
                MplsOutSegmentIndex
                nextMplsOutSegmentIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMplsOutSegmentPerfTable (tSNMP_OCTET_STRING_TYPE *
                                        pMplsOutSegmentIndex,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pNextMplsOutSegmentIndex)
{
    UINT4               u4Index;
    UINT4               u4OutIndex = 0;
    tOutSegment        *pOutSegment = NULL;

    if (pMplsOutSegmentIndex->i4_Length < 0)
    {
        return SNMP_FAILURE;
    }

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pOutSegment = MplsOutSegmentTableNextEntry (u4Index);
    if (pOutSegment != NULL)
    {
        MPLS_INTEGER_TO_OCTETSTRING (OUTSEGMENT_INDEX (pOutSegment),
                                     pNextMplsOutSegmentIndex);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    else
    {
        /* For Partial Index Support */
        while ((pOutSegment = MplsOutSegmentTableNextEntry (u4OutIndex))
               != NULL)
        {
            if (OUTSEGMENT_INDEX (pOutSegment) > u4Index)
            {
                MPLS_INTEGER_TO_OCTETSTRING (OUTSEGMENT_INDEX (pOutSegment),
                                             pNextMplsOutSegmentIndex);
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
            u4OutIndex = OUTSEGMENT_INDEX (pOutSegment);
        }
    }
    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
               "\r\n nmhGetNextIndexMplsOutSegmentPerfTable :"
               "Indexed Entry Unavailable \r\n");
    MPLS_CMN_UNLOCK ();

    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsOutSegmentPerfOctets
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                retValMplsOutSegmentPerfOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsOutSegmentPerfOctets (tSNMP_OCTET_STRING_TYPE * pMplsOutSegmentIndex,
                                UINT4 *pu4RetValMplsOutSegmentPerfOctets)
{
    UINT4               u4Index;
    tOutSegment        *pOutSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetMplsOutSegmentPerfOctets :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
#ifdef NPAPI_WANTED
    {
        tMplsInputParams    MplsStatsInput;
        tStatsInfo          StatsInfo;

        MEMSET (&MplsStatsInput, 0, sizeof (tMplsInputParams));
        StatsInfo.u8Value.u4Hi = 0;
        StatsInfo.u8Value.u4Lo = 0;

        MplsStatsInput.i1NpReset = FALSE;
        MplsStatsInput.InputType = MPLS_GET_TNL_LBL_STATS;
        MPLS_HW_STATS_INPUT_LABEL (MplsStatsInput) =
            OUTSEGMENT_LABEL (pOutSegment);
        MplsFsMplsHwGetMplsStats (&MplsStatsInput, MPLS_HW_STAT_TUNNEL_OUTBYTES,
                                  &StatsInfo);
        *pu4RetValMplsOutSegmentPerfOctets = StatsInfo.u8Value.u4Lo;
    }
#else
    *pu4RetValMplsOutSegmentPerfOctets = 0;
#endif
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsOutSegmentPerfPackets
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                retValMplsOutSegmentPerfPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsOutSegmentPerfPackets (tSNMP_OCTET_STRING_TYPE * pMplsOutSegmentIndex,
                                 UINT4 *pu4RetValMplsOutSegmentPerfPackets)
{
    UINT4               u4Index;
    tOutSegment        *pOutSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetMplsOutSegmentPerfPackets :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
#ifdef NPAPI_WANTED
    {
        tMplsInputParams    MplsStatsInput;
        tStatsInfo          StatsInfo;

        MEMSET (&MplsStatsInput, 0, sizeof (tMplsInputParams));
        StatsInfo.u8Value.u4Hi = 0;
        StatsInfo.u8Value.u4Lo = 0;

        MplsStatsInput.i1NpReset = FALSE;
        MplsStatsInput.InputType = MPLS_GET_TNL_LBL_STATS;
        MPLS_HW_STATS_INPUT_LABEL (MplsStatsInput) =
            OUTSEGMENT_LABEL (pOutSegment);
        MplsFsMplsHwGetMplsStats (&MplsStatsInput, MPLS_HW_STAT_TUNNEL_OUTPKTS,
                                  &StatsInfo);
        *pu4RetValMplsOutSegmentPerfPackets = StatsInfo.u8Value.u4Lo;
    }
#else
    *pu4RetValMplsOutSegmentPerfPackets = 0;
#endif
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsOutSegmentPerfErrors
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                retValMplsOutSegmentPerfErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsOutSegmentPerfErrors (tSNMP_OCTET_STRING_TYPE * pMplsOutSegmentIndex,
                                UINT4 *pu4RetValMplsOutSegmentPerfErrors)
{
    UINT4               u4Index;
    tOutSegment        *pOutSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n  nmhGetMplsOutSegmentPerfErrors :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
#ifdef NPAPI_WANTED
    {
        tMplsInputParams    MplsStatsInput;
        tStatsInfo          StatsInfo;

        MEMSET (&MplsStatsInput, 0, sizeof (tMplsInputParams));
        StatsInfo.u8Value.u4Hi = 0;
        StatsInfo.u8Value.u4Lo = 0;

        MplsStatsInput.i1NpReset = FALSE;
        MplsStatsInput.InputType = MPLS_GET_TNL_LBL_STATS;
        MPLS_HW_STATS_INPUT_LABEL (MplsStatsInput) =
            OUTSEGMENT_LABEL (pOutSegment);
        MplsFsMplsHwGetMplsStats (&MplsStatsInput, MPLS_HW_STAT_TUNNEL_OUTERR,
                                  &StatsInfo);
        *pu4RetValMplsOutSegmentPerfErrors = StatsInfo.u8Value.u4Lo;
    }
#else
    *pu4RetValMplsOutSegmentPerfErrors = 0;
#endif
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsOutSegmentPerfDiscards
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                retValMplsOutSegmentPerfDiscards
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsOutSegmentPerfDiscards (tSNMP_OCTET_STRING_TYPE *
                                  pMplsOutSegmentIndex,
                                  UINT4 *pu4RetValMplsOutSegmentPerfDiscards)
{
    UINT4               u4Index;
    tOutSegment        *pOutSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetMplsOutSegmentPerfDiscards :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
#ifdef NPAPI_WANTED
    {
        tMplsInputParams    MplsStatsInput;
        tStatsInfo          StatsInfo;

        MEMSET (&MplsStatsInput, 0, sizeof (tMplsInputParams));
        StatsInfo.u8Value.u4Hi = 0;
        StatsInfo.u8Value.u4Lo = 0;

        MplsStatsInput.i1NpReset = FALSE;
        MplsStatsInput.InputType = MPLS_GET_TNL_LBL_STATS;
        MPLS_HW_STATS_INPUT_LABEL (MplsStatsInput) =
            OUTSEGMENT_LABEL (pOutSegment);
        MplsFsMplsHwGetMplsStats (&MplsStatsInput,
                                  MPLS_HW_STAT_TNL_OUT_DISCARDS, &StatsInfo);
        *pu4RetValMplsOutSegmentPerfDiscards = StatsInfo.u8Value.u4Lo;
    }
#else
    *pu4RetValMplsOutSegmentPerfDiscards = 0;
#endif
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsOutSegmentPerfHCOctets
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                retValMplsOutSegmentPerfHCOctets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsOutSegmentPerfHCOctets (tSNMP_OCTET_STRING_TYPE *
                                  pMplsOutSegmentIndex,
                                  tSNMP_COUNTER64_TYPE *
                                  pu8RetValMplsOutSegmentPerfHCOctets)
{
    UINT4               u4Index;
    tOutSegment        *pOutSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\nnmhGetMplsOutSegmentPerfHCOctets :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
#ifdef NPAPI_WANTED
    {
        tMplsInputParams    MplsStatsInput;
        tStatsInfo          StatsInfo;

        MEMSET (&MplsStatsInput, 0, sizeof (tMplsInputParams));
        StatsInfo.u8Value.u4Hi = 0;
        StatsInfo.u8Value.u4Lo = 0;

        MplsStatsInput.i1NpReset = FALSE;
        MplsStatsInput.InputType = MPLS_GET_TNL_LBL_STATS;
        MPLS_HW_STATS_INPUT_LABEL (MplsStatsInput) =
            OUTSEGMENT_LABEL (pOutSegment);
        MplsFsMplsHwGetMplsStats (&MplsStatsInput, MPLS_HW_STAT_TUNNEL_OUTBYTES,
                                  &StatsInfo);
        pu8RetValMplsOutSegmentPerfHCOctets->msn = StatsInfo.u8Value.u4Hi;
        pu8RetValMplsOutSegmentPerfHCOctets->lsn = StatsInfo.u8Value.u4Lo;
    }
#else
    pu8RetValMplsOutSegmentPerfHCOctets->msn = 0;
    pu8RetValMplsOutSegmentPerfHCOctets->lsn = 0;
#endif
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsOutSegmentPerfDiscontinuityTime
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                retValMplsOutSegmentPerfDiscontinuityTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsOutSegmentPerfDiscontinuityTime (tSNMP_OCTET_STRING_TYPE *
                                           pMplsOutSegmentIndex,
                                           UINT4
                                           *pu4RetValMplsOutSegmentPerfDiscontinuityTime)
{
    UINT4               u4Index;
    tOutSegment        *pOutSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhGetMplsOutSegmentPerfDiscontinuityTime :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pu4RetValMplsOutSegmentPerfDiscontinuityTime =
        OUTSEGMENT_PERF_DISC_TIME (pOutSegment);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsXCIndexNext
 Input       :  The Indices

                The Object 
                retValMplsXCIndexNext
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsXCIndexNext (tSNMP_OCTET_STRING_TYPE * pRetValMplsXCIndexNext)
{
    UINT4               u4NextIndex;

    u4NextIndex = MplsXcTableGetIndex ();
    MPLS_INTEGER_TO_OCTETSTRING (u4NextIndex, pRetValMplsXCIndexNext);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MplsXCTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsXCTable
 Input       :  The Indices
                MplsXCIndex
                MplsXCInSegmentIndex
                MplsXCOutSegmentIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsXCTable (tSNMP_OCTET_STRING_TYPE * pMplsXCIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMplsXCInSegmentIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMplsXCOutSegmentIndex)
{
    UINT4               u4InIndex = 0;
    UINT4               u4OutIndex = 0;
    UINT4               u4XCIndex = 0;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCIndex, u4XCIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCInSegmentIndex, u4InIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCOutSegmentIndex, u4OutIndex);

    if ((u4XCIndex == 0) || (u4XCIndex > gu4MplsDbMaxXCEntries) ||
        (u4OutIndex > gu4MplsDbMaxOutSegEntries) ||
        (u4InIndex > gu4MplsDbMaxInSegEntries))
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhValidateIndexInstanceMplsXCTable :"
                   "Invalid Indices \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsXCTable
 Input       :  The Indices
                MplsXCIndex
                MplsXCInSegmentIndex
                MplsXCOutSegmentIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsXCTable (tSNMP_OCTET_STRING_TYPE * pMplsXCIndex,
                             tSNMP_OCTET_STRING_TYPE * pMplsXCInSegmentIndex,
                             tSNMP_OCTET_STRING_TYPE * pMplsXCOutSegmentIndex)
{
    tXcEntry           *pXcEntry = NULL;

    MPLS_CMN_LOCK ();
    pXcEntry = MplsXCTableNextEntry (0, 0, 0);
    if (pXcEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetFirstIndexMplsXCTable :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    MPLS_INTEGER_TO_OCTETSTRING ((XC_INDEX (pXcEntry)), pMplsXCIndex);
    if (XC_ININDEX (pXcEntry) == NULL)
    {
        MEMSET (pMplsXCInSegmentIndex->pu1_OctetList, 0, sizeof (UINT1));
        pMplsXCInSegmentIndex->i4_Length = sizeof (UINT1);

    }
    else
    {
        MPLS_INTEGER_TO_OCTETSTRING ((XC_ININDEX (pXcEntry))->u4Index,
                                     pMplsXCInSegmentIndex);
    }
    if (XC_OUTINDEX (pXcEntry) == NULL)
    {
        MEMSET (pMplsXCOutSegmentIndex->pu1_OctetList, 0, sizeof (UINT1));
        pMplsXCOutSegmentIndex->i4_Length = sizeof (UINT1);

    }
    else
    {
        MPLS_INTEGER_TO_OCTETSTRING ((XC_OUTINDEX (pXcEntry))->u4Index,
                                     pMplsXCOutSegmentIndex);
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsXCTable
 Input       :  The Indices
                MplsXCIndex
                nextMplsXCIndex
                MplsXCInSegmentIndex
                nextMplsXCInSegmentIndex
                MplsXCOutSegmentIndex
                nextMplsXCOutSegmentIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMplsXCTable (tSNMP_OCTET_STRING_TYPE * pMplsXCIndex,
                            tSNMP_OCTET_STRING_TYPE * pNextMplsXCIndex,
                            tSNMP_OCTET_STRING_TYPE * pMplsXCInSegmentIndex,
                            tSNMP_OCTET_STRING_TYPE * pNextMplsXCInSegmentIndex,
                            tSNMP_OCTET_STRING_TYPE * pMplsXCOutSegmentIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pNextMplsXCOutSegmentIndex)
{
    UINT4               u4Index;
    UINT4               u4InIndex = 0;
    UINT4               u4OutIndex = 0;
    tXcEntry           *pXcEntry = NULL;

    if (pMplsXCIndex->i4_Length < 0)
    {
        return SNMP_FAILURE;
    }

    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCIndex, u4Index);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCInSegmentIndex, u4InIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCOutSegmentIndex, u4OutIndex);

    MPLS_CMN_LOCK ();
    pXcEntry = MplsXCTableNextEntry (u4Index, u4InIndex, u4OutIndex);

    if (pXcEntry == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_INTEGER_TO_OCTETSTRING (XC_INDEX (pXcEntry), pNextMplsXCIndex);
    if (XC_ININDEX (pXcEntry) == NULL)
    {
        MEMSET (pNextMplsXCInSegmentIndex->pu1_OctetList, 0, sizeof (UINT1));
        pNextMplsXCInSegmentIndex->i4_Length = sizeof (UINT1);

    }
    else
    {
        MPLS_INTEGER_TO_OCTETSTRING ((XC_ININDEX (pXcEntry))->u4Index,
                                     pNextMplsXCInSegmentIndex);
    }
    if (XC_OUTINDEX (pXcEntry) == NULL)
    {
        MEMSET (pNextMplsXCOutSegmentIndex->pu1_OctetList, 0, sizeof (UINT1));
        pNextMplsXCOutSegmentIndex->i4_Length = sizeof (UINT1);

    }
    else
    {
        MPLS_INTEGER_TO_OCTETSTRING ((XC_OUTINDEX (pXcEntry))->u4Index,
                                     pNextMplsXCOutSegmentIndex);
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsXCLspId
 Input       :  The Indices
                MplsXCIndex
                MplsXCInSegmentIndex
                MplsXCOutSegmentIndex

                The Object 
                retValMplsXCLspId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsXCLspId (tSNMP_OCTET_STRING_TYPE * pMplsXCIndex,
                   tSNMP_OCTET_STRING_TYPE * pMplsXCInSegmentIndex,
                   tSNMP_OCTET_STRING_TYPE * pMplsXCOutSegmentIndex,
                   tSNMP_OCTET_STRING_TYPE * pRetValMplsXCLspId)
{
    UINT4               u4XCIndex;
    tXcEntry           *pXCEntry = NULL;
    UINT4               u4XCInIndex = 0;
    UINT4               u4XCOutIndex = 0;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCIndex, u4XCIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCInSegmentIndex, u4XCInIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCOutSegmentIndex, u4XCOutIndex);

    MPLS_CMN_LOCK ();
    pXCEntry = MplsGetXCTableEntry (u4XCIndex, u4XCInIndex, u4XCOutIndex);
    if (pXCEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhGetMplsXCLspId :" "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MEMCPY (pRetValMplsXCLspId->pu1_OctetList, XC_LSP_ID (pXCEntry),
            pXCEntry->u1LspIdLen);
    pRetValMplsXCLspId->i4_Length = pXCEntry->u1LspIdLen;

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsXCLabelStackIndex
 Input       :  The Indices
                MplsXCIndex
                MplsXCInSegmentIndex
                MplsXCOutSegmentIndex

                The Object 
                retValMplsXCLabelStackIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsXCLabelStackIndex (tSNMP_OCTET_STRING_TYPE * pMplsXCIndex,
                             tSNMP_OCTET_STRING_TYPE * pMplsXCInSegmentIndex,
                             tSNMP_OCTET_STRING_TYPE * pMplsXCOutSegmentIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValMplsXCLabelStackIndex)
{
    UINT4               u4XCIndex;
    tXcEntry           *pXCEntry = NULL;
    UINT4               u4XCInIndex = 0;
    UINT4               u4XCOutIndex = 0;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCIndex, u4XCIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCInSegmentIndex, u4XCInIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCOutSegmentIndex, u4XCOutIndex);

    MPLS_CMN_LOCK ();
    pXCEntry = MplsGetXCTableEntry (u4XCIndex, u4XCInIndex, u4XCOutIndex);
    if (pXCEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetMplsXCLabelStackIndex :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (XC_LBL_INDEX (pXCEntry) == NULL)
    {
        MEMSET (pRetValMplsXCLabelStackIndex->pu1_OctetList, 0, sizeof (UINT1));
        pRetValMplsXCLabelStackIndex->i4_Length = sizeof (UINT1);
    }
    else
    {
        MPLS_INTEGER_TO_OCTETSTRING ((XC_LBL_INDEX (pXCEntry))->u4Index,
                                     pRetValMplsXCLabelStackIndex);
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsXCOwner
 Input       :  The Indices
                MplsXCIndex
                MplsXCInSegmentIndex
                MplsXCOutSegmentIndex

                The Object 
                retValMplsXCOwner
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsXCOwner (tSNMP_OCTET_STRING_TYPE * pMplsXCIndex,
                   tSNMP_OCTET_STRING_TYPE * pMplsXCInSegmentIndex,
                   tSNMP_OCTET_STRING_TYPE * pMplsXCOutSegmentIndex,
                   INT4 *pi4RetValMplsXCOwner)
{
    UINT4               u4XCIndex;
    tXcEntry           *pXCEntry = NULL;
    UINT4               u4XCInIndex = 0;
    UINT4               u4XCOutIndex = 0;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCIndex, u4XCIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCInSegmentIndex, u4XCInIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCOutSegmentIndex, u4XCOutIndex);

    MPLS_CMN_LOCK ();
    pXCEntry = MplsGetXCTableEntry (u4XCIndex, u4XCInIndex, u4XCOutIndex);
    if (pXCEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhGetMplsXCOwner :" "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pi4RetValMplsXCOwner = (INT4) XC_OWNER (pXCEntry);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsXCRowStatus
 Input       :  The Indices
                MplsXCIndex
                MplsXCInSegmentIndex
                MplsXCOutSegmentIndex

                The Object 
                retValMplsXCRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsXCRowStatus (tSNMP_OCTET_STRING_TYPE * pMplsXCIndex,
                       tSNMP_OCTET_STRING_TYPE * pMplsXCInSegmentIndex,
                       tSNMP_OCTET_STRING_TYPE * pMplsXCOutSegmentIndex,
                       INT4 *pi4RetValMplsXCRowStatus)
{
    UINT4               u4XCIndex;
    tXcEntry           *pXCEntry = NULL;
    UINT4               u4XCInIndex = 0;
    UINT4               u4XCOutIndex = 0;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCIndex, u4XCIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCInSegmentIndex, u4XCInIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCOutSegmentIndex, u4XCOutIndex);

    MPLS_CMN_LOCK ();
    pXCEntry = MplsGetXCTableEntry (u4XCIndex, u4XCInIndex, u4XCOutIndex);
    if (pXCEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetMplsXCRowStatus :"
                   "Indexed Entry Unavailable\r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pi4RetValMplsXCRowStatus = (INT4) XC_ROW_STATUS (pXCEntry);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsXCStorageType
 Input       :  The Indices
                MplsXCIndex
                MplsXCInSegmentIndex
                MplsXCOutSegmentIndex

                The Object 
                retValMplsXCStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsXCStorageType (tSNMP_OCTET_STRING_TYPE * pMplsXCIndex,
                         tSNMP_OCTET_STRING_TYPE * pMplsXCInSegmentIndex,
                         tSNMP_OCTET_STRING_TYPE * pMplsXCOutSegmentIndex,
                         INT4 *pi4RetValMplsXCStorageType)
{
    UINT4               u4XCIndex;
    tXcEntry           *pXCEntry = NULL;
    UINT4               u4XCInIndex = 0;
    UINT4               u4XCOutIndex = 0;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCIndex, u4XCIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCInSegmentIndex, u4XCInIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCOutSegmentIndex, u4XCOutIndex);

    MPLS_CMN_LOCK ();
    pXCEntry = MplsGetXCTableEntry (u4XCIndex, u4XCInIndex, u4XCOutIndex);
    if (pXCEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetMplsXCStorageType :"
                   "Indexed Entry Unavailable\r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pi4RetValMplsXCStorageType = (INT4) XC_STRG_TYPE (pXCEntry);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsXCAdminStatus
 Input       :  The Indices
                MplsXCIndex
                MplsXCInSegmentIndex
                MplsXCOutSegmentIndex

                The Object 
                retValMplsXCAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsXCAdminStatus (tSNMP_OCTET_STRING_TYPE * pMplsXCIndex,
                         tSNMP_OCTET_STRING_TYPE * pMplsXCInSegmentIndex,
                         tSNMP_OCTET_STRING_TYPE * pMplsXCOutSegmentIndex,
                         INT4 *pi4RetValMplsXCAdminStatus)
{
    UINT4               u4XCIndex;
    tXcEntry           *pXCEntry = NULL;
    UINT4               u4XCInIndex = 0;
    UINT4               u4XCOutIndex = 0;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCIndex, u4XCIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCInSegmentIndex, u4XCInIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCOutSegmentIndex, u4XCOutIndex);

    MPLS_CMN_LOCK ();
    pXCEntry = MplsGetXCTableEntry (u4XCIndex, u4XCInIndex, u4XCOutIndex);
    if (pXCEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, " \r\n nmhGetMplsXCAdminStatus :"
                   "Indexed Entry Unavailable\r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pi4RetValMplsXCAdminStatus = (INT4) XC_ADMIN_STATUS (pXCEntry);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsXCOperStatus
 Input       :  The Indices
                MplsXCIndex
                MplsXCInSegmentIndex
                MplsXCOutSegmentIndex

                The Object 
                retValMplsXCOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsXCOperStatus (tSNMP_OCTET_STRING_TYPE * pMplsXCIndex,
                        tSNMP_OCTET_STRING_TYPE * pMplsXCInSegmentIndex,
                        tSNMP_OCTET_STRING_TYPE * pMplsXCOutSegmentIndex,
                        INT4 *pi4RetValMplsXCOperStatus)
{
    UINT4               u4XCIndex;
    tXcEntry           *pXCEntry = NULL;
    UINT4               u4XCInIndex = 0;
    UINT4               u4XCOutIndex = 0;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCIndex, u4XCIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCInSegmentIndex, u4XCInIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCOutSegmentIndex, u4XCOutIndex);

    MPLS_CMN_LOCK ();
    pXCEntry = MplsGetXCTableEntry (u4XCIndex, u4XCInIndex, u4XCOutIndex);
    if (pXCEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetMplsXCOperStatus :"
                   "Indexed Entry Unavailable\r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pi4RetValMplsXCOperStatus = (INT4) XC_OPER_STATUS (pXCEntry);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsXCLspId
 Input       :  The Indices
                MplsXCIndex
                MplsXCInSegmentIndex
                MplsXCOutSegmentIndex

                The Object 
                setValMplsXCLspId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsXCLspId (tSNMP_OCTET_STRING_TYPE * pMplsXCIndex,
                   tSNMP_OCTET_STRING_TYPE * pMplsXCInSegmentIndex,
                   tSNMP_OCTET_STRING_TYPE * pMplsXCOutSegmentIndex,
                   tSNMP_OCTET_STRING_TYPE * pSetValMplsXCLspId)
{
    UINT4               u4XCIndex;
    tXcEntry           *pXCEntry = NULL;
    UINT4               u4XCInIndex = 0;
    UINT4               u4XCOutIndex = 0;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCIndex, u4XCIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCInSegmentIndex, u4XCInIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCOutSegmentIndex, u4XCOutIndex);

    MPLS_CMN_LOCK ();
    pXCEntry = MplsGetXCTableEntry (u4XCIndex, u4XCInIndex, u4XCOutIndex);
    if (pXCEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhSetMplsXCLspId : Indexed Entry Unavailable\r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    MEMCPY (XC_LSP_ID (pXCEntry), pSetValMplsXCLspId->pu1_OctetList,
            pSetValMplsXCLspId->i4_Length);
    pXCEntry->u1LspIdLen = (UINT1) pSetValMplsXCLspId->i4_Length;

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetMplsXCLabelStackIndex
 Input       :  The Indices
                MplsXCIndex
                MplsXCInSegmentIndex
                MplsXCOutSegmentIndex

                The Object 
                setValMplsXCLabelStackIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsXCLabelStackIndex (tSNMP_OCTET_STRING_TYPE * pMplsXCIndex,
                             tSNMP_OCTET_STRING_TYPE * pMplsXCInSegmentIndex,
                             tSNMP_OCTET_STRING_TYPE * pMplsXCOutSegmentIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValMplsXCLabelStackIndex)
{
    UINT4               u4XCIndex;
    UINT4               u4LblIndex;
    tLblStkEntry       *pLblStkEntry = NULL;
    tXcEntry           *pXCEntry = NULL;
    UINT4               u4XCInIndex = 0;
    UINT4               u4XCOutIndex = 0;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCIndex, u4XCIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCInSegmentIndex, u4XCInIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCOutSegmentIndex, u4XCOutIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pSetValMplsXCLabelStackIndex, u4LblIndex);

    MPLS_CMN_LOCK ();
    pXCEntry = MplsGetXCTableEntry (u4XCIndex, u4XCInIndex, u4XCOutIndex);
    if (pXCEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "nmhSetMplsXCLabelStackIndex :"
                   "Indexed Entry Unavailable\r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pLblStkEntry = MplsGetLblStkTableEntry (u4LblIndex);

    if (pLblStkEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "nmhSetMplsXCLabelStackIndex :"
                   "LblStack Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    else
    {
        XC_LBL_INDEX (pXCEntry) = pLblStkEntry;
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhSetMplsXCRowStatus
 Input       :  The Indices
                MplsXCIndex
                MplsXCInSegmentIndex
                MplsXCOutSegmentIndex

                The Object 
                setValMplsXCRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsXCRowStatus (tSNMP_OCTET_STRING_TYPE * pMplsXCIndex,
                       tSNMP_OCTET_STRING_TYPE * pMplsXCInSegmentIndex,
                       tSNMP_OCTET_STRING_TYPE * pMplsXCOutSegmentIndex,
                       INT4 i4SetValMplsXCRowStatus)
{
    UINT4               u4InIndex;
    UINT4               u4OutIndex;
    UINT4               u4XCIndex;
    tInSegment         *pInSegment = NULL;
    tOutSegment        *pOutSegment = NULL;
    tXcEntry           *pXCEntry = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT1               u1SetIndexFlag = FALSE;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCIndex, u4XCIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCInSegmentIndex, u4InIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCOutSegmentIndex, u4OutIndex);

    MPLS_CMN_LOCK ();

    pXCEntry = MplsGetXCTableEntry (u4XCIndex, u4InIndex, u4OutIndex);

    switch (i4SetValMplsXCRowStatus)
    {
        case LSR_CREATEANDWAIT:
            if (pXCEntry != NULL)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\nnmhSetMplsXCRowStatus :"
                           "Already the Outsegment is Present!!\r\n");
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            if (MplsGetXCEntryByDirection (u4XCIndex,
                                           (eDirection) MPLS_DIRECTION_ANY) ==
                NULL)
            {
                if (MplsXCTableSetIndex (u4XCIndex) != u4XCIndex)
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               "SetMplsXCRowStatus Failed \t\n");
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
                u1SetIndexFlag = TRUE;
            }

            pXCEntry = MplsCreateXCTableEntry (u4XCIndex, u4InIndex,
                                               u4OutIndex);
            if (pXCEntry == NULL)
            {
                if (u1SetIndexFlag == TRUE)
                {
                    MplsXcTableRelIndex (u4XCIndex);
                }
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            XC_INIT_DEF_VALUES (pXCEntry);
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        case LSR_CREATEANDGO:
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        case LSR_NOTINSERVICE:
            /* NP DELETE */
            if ((pXCEntry == NULL) || (XC_OWNER (pXCEntry) != MPLS_OWNER_SNMP))
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL, "SetMplsXCRowStatus Failed \t\n");
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            if (XC_ROW_STATUS (pXCEntry) == LSR_ACTIVE)
            {
                if (XC_ININDEX (pXCEntry) != NULL &&
                    pXCEntry->pInIndex->u1RowStatus == ACTIVE)
                {
                    if ((pXCEntry->pTeTnlInfo != NULL) &&
                        (TeTlmUpdateTrafficControl (pXCEntry->pTeTnlInfo,
                                                    MPLS_OPER_DOWN,
                                                    pXCEntry->pInIndex->
                                                    GmplsInSegment.
                                                    InSegmentDirection)
                         == TE_FAILURE))
                    {
                        MPLS_CMN_UNLOCK ();
                        return SNMP_FAILURE;
                    }

                    if (MplsILMDel (XC_ININDEX (pXCEntry)) == MPLS_FAILURE)
                    {
                        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                                   "\r\n MplsILMHwDel Failed \t\n");
                        MPLS_CMN_UNLOCK ();
                        return SNMP_FAILURE;
                    }
                }
            }
            XC_OPER_STATUS (pXCEntry) = XC_OPER_DOWN;
            if (gu1NotifEnable == MPLS_SNMP_TRUE)
            {
                if (MplsLSRXcOperNotif (pXCEntry, XC_OPER_DOWN) == MPLS_FAILURE)
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhSetMplsXCRowStatus :"
                               "XC DOWN Notfn Send failed \n");
                }
            }
            XC_ROW_STATUS (pXCEntry) = LSR_NOTINSERVICE;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        case LSR_DESTROY:
            /* NP Delete */
            if ((pXCEntry == NULL) || (XC_OWNER (pXCEntry) != MPLS_OWNER_SNMP))
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL, "SetMplsXCRowStatus Failed \t\n");
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            if (XC_ROW_STATUS (pXCEntry) == LSR_ACTIVE)
            {
                if (XC_ININDEX (pXCEntry) != NULL &&
                    pXCEntry->pInIndex->u1RowStatus == ACTIVE)
                {
                    /* MPLS_P2MP_LSP_CHANGES - S */
                    if ((MplsCheckXCForP2mp (pXCEntry)) != MPLS_SUCCESS)
                    {
                        if ((pXCEntry->pTeTnlInfo != NULL) &&
                            (TeTlmUpdateTrafficControl (pXCEntry->pTeTnlInfo,
                                                        MPLS_OPER_DOWN,
                                                        pXCEntry->pInIndex->
                                                        GmplsInSegment.
                                                        InSegmentDirection)
                             == TE_FAILURE))
                        {
                            MPLS_CMN_UNLOCK ();
                            return SNMP_FAILURE;
                        }

                        if (MplsILMDel (XC_ININDEX (pXCEntry)) == MPLS_FAILURE)
                        {
                            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                                       "\r\n MplsILMHwDel Failed \t\n");
                            MPLS_CMN_UNLOCK ();
                            return SNMP_FAILURE;
                        }
                    }
                    /* MPLS_P2MP_LSP_CHANGES - E */
                }
            }
            pOutSegment = XC_OUTINDEX (pXCEntry);
            if (pOutSegment != NULL)
            {
                OUTSEGMENT_ROW_STATUS (pOutSegment) = LSR_NOTREADY;
            }
            pInSegment = XC_ININDEX (pXCEntry);
            if (pInSegment != NULL)
            {
                INSEGMENT_ROW_STATUS (pInSegment) = LSR_NOTREADY;
            }
            XC_OPER_STATUS (pXCEntry) = XC_OPER_DOWN;
            if (gu1NotifEnable == MPLS_SNMP_TRUE)
            {
                if (MplsLSRXcOperNotif (pXCEntry, XC_OPER_DOWN) == MPLS_FAILURE)
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhSetMplsXCRowStatus :"
                               "XC DOWN Notfn Send failed \n");
                }
            }
            if (pXCEntry->mplsLabelIndex != NULL)
            {
                MplsDeleteLblStkAndLabelEntries (pXCEntry->mplsLabelIndex);
                pXCEntry->mplsLabelIndex = NULL;
            }
            if (MplsDeleteXCTableEntry (pXCEntry) != MPLS_SUCCESS)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            else
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }

        case LSR_ACTIVE:
            if ((pXCEntry == NULL) || (XC_OWNER (pXCEntry) != MPLS_OWNER_SNMP))
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL, "SetMplsXCRowStatus Failed \t\n");
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            if (((XC_OWNER (pXCEntry) == MPLS_OWNER_CRLDP) &&
                 (pXCEntry->u1LspIdLen != MAX_LSP_ID)) ||
                ((XC_OWNER (pXCEntry) == MPLS_OWNER_RSVPTE) &&
                 (pXCEntry->u1LspIdLen != MIN_LSP_ID)))
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL, "SetMplsXCRowStatus Failed \t\n");
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            if ((XC_ROW_STATUS (pXCEntry) == LSR_NOTREADY) ||
                (XC_ROW_STATUS (pXCEntry) == LSR_NOTINSERVICE))
            {
                /* NP Add */
                if (XC_ININDEX (pXCEntry) != NULL &&
                    pXCEntry->pInIndex->u1RowStatus == ACTIVE)
                {
                    /* MPLS_P2MP_LSP_CHANGES - S */
                    if ((MplsCheckXCForP2mp (pXCEntry)) != MPLS_SUCCESS)
                    {
                        /* Program the entry only if XC is not associated with 
                         * TE. This check will be avoided when traffic params 
                         * are filled in In and Out segment entries */
                        if (TeCheckXcIndex (u4XCIndex, &pTeTnlInfo)
                            != TE_SUCCESS)
                        {
                            if ((MplsILMAdd (XC_ININDEX (pXCEntry), NULL)
                                 == MPLS_FAILURE) &&
                                (XC_ARP_STATUS (pXCEntry)
                                 != MPLS_ARP_RESOLVE_WAITING))
                            {
                                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                                           "\r\n MplsILMHwAdd Failed \t\n");
                                MPLS_CMN_UNLOCK ();
                                return SNMP_FAILURE;
                            }
                        }
                    }
                    /* MPLS_P2MP_LSP_CHANGES - E */
                }
                CMNDB_DBG (DEBUG_DEBUG_LEVEL, "nmhSetMplsXCRowStatus :"
                           "Updating XC oper status\r\n");
                MplsDbUpdateXcOperStatus (pXCEntry, TRUE, CFA_IF_UP,
                                          XC_BOTH_SEGMENT);
                if (gu1NotifEnable == MPLS_SNMP_TRUE)
                {
                    if (MplsLSRXcOperNotif (pXCEntry, XC_OPER_UP)
                        == MPLS_FAILURE)
                    {
                        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                                   "\r\nnmhSetMplsXCRowStatus :"
                                   "XC UP Notfn Send  failed \n");
                    }
                }
                XC_ROW_STATUS (pXCEntry) = LSR_ACTIVE;
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
            else if (XC_ROW_STATUS (pXCEntry) == LSR_ACTIVE)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhSetMplsXCRowStatus :"
                           "Already the XC is Active!!\r\n");
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
            else
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

        default:
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhSetMplsXCStorageType
 Input       :  The Indices
                MplsXCIndex
                MplsXCInSegmentIndex
                MplsXCOutSegmentIndex

                The Object 
                setValMplsXCStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsXCStorageType (tSNMP_OCTET_STRING_TYPE * pMplsXCIndex,
                         tSNMP_OCTET_STRING_TYPE * pMplsXCInSegmentIndex,
                         tSNMP_OCTET_STRING_TYPE * pMplsXCOutSegmentIndex,
                         INT4 i4SetValMplsXCStorageType)
{
    UINT4               u4XCIndex;
    tXcEntry           *pXCEntry = NULL;
    UINT4               u4XCInIndex = 0;
    UINT4               u4XCOutIndex = 0;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCIndex, u4XCIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCInSegmentIndex, u4XCInIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCOutSegmentIndex, u4XCOutIndex);

    MPLS_CMN_LOCK ();
    pXCEntry = MplsGetXCTableEntry (u4XCIndex, u4XCInIndex, u4XCOutIndex);
    if (pXCEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhSetMplsXCStorageType :"
                   "Indexed Entry Unavailable\r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    XC_STRG_TYPE (pXCEntry) = (UINT1) i4SetValMplsXCStorageType;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetMplsXCAdminStatus
 Input       :  The Indices
                MplsXCIndex
                MplsXCInSegmentIndex
                MplsXCOutSegmentIndex

                The Object 
                setValMplsXCAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsXCAdminStatus (tSNMP_OCTET_STRING_TYPE * pMplsXCIndex,
                         tSNMP_OCTET_STRING_TYPE * pMplsXCInSegmentIndex,
                         tSNMP_OCTET_STRING_TYPE * pMplsXCOutSegmentIndex,
                         INT4 i4SetValMplsXCAdminStatus)
{
    UINT4               u4XCIndex;
    tXcEntry           *pXCEntry = NULL;
    UINT4               u4XCInIndex = 0;
    UINT4               u4XCOutIndex = 0;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCIndex, u4XCIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCInSegmentIndex, u4XCInIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCOutSegmentIndex, u4XCOutIndex);

    MPLS_CMN_LOCK ();
    pXCEntry = MplsGetXCTableEntry (u4XCIndex, u4XCInIndex, u4XCOutIndex);
    if (pXCEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhSetMplsXCStorageType :"
                   "Indexed Entry Unavailable\r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    switch (i4SetValMplsXCAdminStatus)
    {
        case XC_ADMIN_UP:

            if (XC_ADMIN_STATUS (pXCEntry) == XC_ADMIN_DOWN)
            {
                /* NP CALL */
                XC_ADMIN_STATUS (pXCEntry) = XC_ADMIN_UP;
                XC_OPER_STATUS (pXCEntry) = XC_OPER_UP;
                if (gu1NotifEnable == MPLS_SNMP_TRUE)
                {
                    if (MplsLSRXcOperNotif (pXCEntry, XC_OPER_UP)
                        == MPLS_FAILURE)
                    {
                        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                                   "\r\n nmhSetMplsXCRowStatus :"
                                   "XC UP Notfn Send  failed \n");
                    }
                }
                if (pXCEntry->pTeTnlInfo->u1TnlAdminStatus != TE_ADMIN_DOWN)
                {
                    if ((TeUpdateTunnelOperStatusAndProgHw
                         (pXCEntry->pTeTnlInfo, TE_OPER_UP)) != SNMP_SUCCESS)
                    {
                        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                                   "\r\n Unable to set Tunnel"
                                   "Oper status :\r\n");
                        return SNMP_FAILURE;
                    }
                }
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
            else
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhSetMplsXCAdminStatus :"
                           "Already Admin UP\r\n");
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
        case XC_ADMIN_DOWN:

            if (XC_ADMIN_STATUS (pXCEntry) == XC_ADMIN_UP)
            {
                /* NP CALL */
                XC_OPER_STATUS (pXCEntry) = XC_OPER_DOWN;
                XC_ADMIN_STATUS (pXCEntry) = XC_ADMIN_DOWN;
                if (gu1NotifEnable == MPLS_SNMP_TRUE)
                {
                    if (MplsLSRXcOperNotif (pXCEntry, XC_OPER_DOWN)
                        == MPLS_FAILURE)
                    {
                        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                                   "\r\n nmhSetMplsXCRowStatus :"
                                   "XC DOWN Notfn Send failed \n");
                    }
                }

                if (pXCEntry->pTeTnlInfo->u1TnlAdminStatus != TE_ADMIN_DOWN)
                {
                    if ((TeUpdateTunnelOperStatusAndProgHw
                         (pXCEntry->pTeTnlInfo, TE_OPER_DOWN)) != SNMP_SUCCESS)
                    {
                        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                                   "\r\n Unable to set Tunnel"
                                   "Oper status :\r\n");
                        return SNMP_FAILURE;
                    }
                }

                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
            else
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhSetMplsXCAdminStatus :"
                           "Already Admin Down\r\n");
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
        default:
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
    }
}

                    /* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsXCLspId
 Input       :  The Indices
                MplsXCIndex
                MplsXCInSegmentIndex
                MplsXCOutSegmentIndex

                The Object 
                testValMplsXCLspId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsXCLspId (UINT4 *pu4ErrorCode,
                      tSNMP_OCTET_STRING_TYPE * pMplsXCIndex,
                      tSNMP_OCTET_STRING_TYPE * pMplsXCInSegmentIndex,
                      tSNMP_OCTET_STRING_TYPE * pMplsXCOutSegmentIndex,
                      tSNMP_OCTET_STRING_TYPE * pTestValMplsXCLspId)
{
    UINT4               u4XCIndex;
    UINT4               u4InIndex;
    UINT4               u4OutIndex;
    tXcEntry           *pXCEntry = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCIndex, u4XCIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCInSegmentIndex, u4InIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCOutSegmentIndex, u4OutIndex);

    if ((u4XCIndex == 0) || (u4XCIndex > gu4MplsDbMaxXCEntries) ||
        (u4InIndex > gu4MplsDbMaxInSegEntries) ||
        (u4OutIndex > gu4MplsDbMaxOutSegEntries))
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhTestv2MplsXCLspId : Invalid Indices \r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    /* XCLspId should be of length 2 or 6 */
    if ((pTestValMplsXCLspId->i4_Length != RSVP_TE_LSP_ID_LEN) &&
        (pTestValMplsXCLspId->i4_Length != CRLDP_LSP_ID_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }
    MPLS_CMN_LOCK ();

    pXCEntry = MplsGetXCTableEntry (u4XCIndex, u4InIndex, u4OutIndex);
    if (pXCEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhTestv2MplsXCLspId : Indexed Entry Unavailable\r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (XC_ROW_STATUS (pXCEntry) == LSR_ACTIVE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhTestv2MplsXCLspId : Cant Modify Objects \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsXCLabelStackIndex
 Input       :  The Indices
                MplsXCIndex
                MplsXCInSegmentIndex
                MplsXCOutSegmentIndex

                The Object 
                testValMplsXCLabelStackIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsXCLabelStackIndex (UINT4 *pu4ErrorCode,
                                tSNMP_OCTET_STRING_TYPE * pMplsXCIndex,
                                tSNMP_OCTET_STRING_TYPE * pMplsXCInSegmentIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pMplsXCOutSegmentIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValMplsXCLabelStackIndex)
{
    UINT4               u4XCIndex;
    UINT4               u4InIndex;
    UINT4               u4OutIndex;
    UINT4               u4LblIndex;
    tLblStkEntry       *pLblStkEntry = NULL;
    tXcEntry           *pXCEntry = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCIndex, u4XCIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCInSegmentIndex, u4InIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCOutSegmentIndex, u4OutIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pTestValMplsXCLabelStackIndex, u4LblIndex);

    if ((u4XCIndex == 0) || (u4XCIndex > gu4MplsDbMaxXCEntries) ||
        (u4InIndex > gu4MplsDbMaxInSegEntries) ||
        (u4OutIndex > gu4MplsDbMaxOutSegEntries))
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhTestv2MplsXCLabelStackIndex :"
                   "Invalid Indices \r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((pTestValMplsXCLabelStackIndex->i4_Length < LABEL_STACK_INDEX_MIN_LEN)
        || (pTestValMplsXCLabelStackIndex->i4_Length >
            LABEL_STACK_INDEX_MAX_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    MPLS_CMN_LOCK ();
    pXCEntry = MplsGetXCTableEntry (u4XCIndex, u4InIndex, u4OutIndex);
    if (pXCEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhTestv2MplsXCLabelStackIndex :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (XC_ROW_STATUS (pXCEntry) == LSR_ACTIVE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhTestv2MplsXCLabelStackIndex :"
                   "Cant Modify Objects \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    pLblStkEntry = MplsGetLblStkTableEntry (u4LblIndex);
    if (pLblStkEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhTestv2MplsXCLabelStackIndex : LblStack NULL \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsXCRowStatus
 Input       :  The Indices
                MplsXCIndex
                MplsXCInSegmentIndex
                MplsXCOutSegmentIndex

                The Object 
                testValMplsXCRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsXCRowStatus (UINT4 *pu4ErrorCode,
                          tSNMP_OCTET_STRING_TYPE * pMplsXCIndex,
                          tSNMP_OCTET_STRING_TYPE * pMplsXCInSegmentIndex,
                          tSNMP_OCTET_STRING_TYPE * pMplsXCOutSegmentIndex,
                          INT4 i4TestValMplsXCRowStatus)
{
    UINT4               u4XCIndex;
    UINT4               u4InIndex;
    UINT4               u4OutIndex;
    tXcEntry           *pXCEntry = NULL;
    tOutSegment        *pOutSegment = NULL;
    /* MPLS_P2MP_LSP_CHANGES - S */
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tP2mpBranchEntry   *pTeP2mpBranchEntry = NULL;
    /* MPLS_P2MP_LSP_CHANGES - E */

    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCIndex, u4XCIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCInSegmentIndex, u4InIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCOutSegmentIndex, u4OutIndex);

    if ((u4XCIndex == 0) || (u4XCIndex > gu4MplsDbMaxXCEntries) ||
        (u4InIndex > gu4MplsDbMaxInSegEntries) ||
        (u4OutIndex > gu4MplsDbMaxOutSegEntries))
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhTestv2MplsXCRowStatus : Invalid Indices \r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4OutIndex);
    pXCEntry = MplsGetXCTableEntry (u4XCIndex, u4InIndex, u4OutIndex);

    if ((u4InIndex == 0) && (u4OutIndex == 0))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhTestv2MplsXCRowStatus:No In and Out Segments\r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (pMplsXCIndex->i4_Length == 0)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhTestv2MplsXCRowStatus : Invalid XC Index \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    switch (i4TestValMplsXCRowStatus)
    {
        case LSR_DESTROY:
            if (pXCEntry == NULL)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhTestv2MplsXCRowStatus :"
                           "Indexed Entry Unavailable \r\n");
                MPLS_CMN_UNLOCK ();
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }

            /* MPLS_P2MP_LSP_CHANGES - S */
            if ((NULL != pOutSegment) && (NULL != XC_TNL_TBL_PTR (pXCEntry)))
            {
                pTeTnlInfo = XC_TNL_TBL_PTR (pXCEntry);
                if ((MplsCheckXCForP2mp (pXCEntry)) == MPLS_SUCCESS)
                {
                    TMO_SLL_Scan (&(TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo)),
                                  pTeP2mpBranchEntry, tP2mpBranchEntry *)
                    {
                        if ((OUTSEGMENT_INDEX (pOutSegment)
                             ==
                             TE_P2MP_BRANCH_OUTSEG_INDEX (pTeP2mpBranchEntry))
                            && (TE_P2MP_BRANCH_DEST_COUNT (pTeP2mpBranchEntry)
                                != MPLS_ZERO))
                        {
                            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                                       "\r\n nmhTestv2MplsXCRowStatus : XC is "
                                       "associated to P2MP destination. Cannot "
                                       "delete!!! \r\n");
                            MPLS_CMN_UNLOCK ();
                            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                            return SNMP_FAILURE;
                        }
                    }
                }
            }
            /* MPLS_P2MP_LSP_CHANGES - E */
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        case LSR_ACTIVE:
            if (pXCEntry == NULL)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhTestv2MplsXCRowStatus :"
                           "Indexed Entry Unavailable \r\n");
                MPLS_CMN_UNLOCK ();
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            if ((XC_ROW_STATUS (pXCEntry) == LSR_NOTREADY) ||
                (XC_ROW_STATUS (pXCEntry) == LSR_NOTINSERVICE))
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
            CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhTestv2MplsXCRowStatus :"
                       "Wrong RowStatus Input \r\n");
            MPLS_CMN_UNLOCK ();
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        case LSR_CREATEANDWAIT:
            if (pXCEntry == NULL)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhTestv2MplsXCRowStatus :"
                           "Indexed Entry can be created \r\n");
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
            CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhTestv2MplsXCRowStatus :"
                       "Indexed Entry Already There \r\n");
            MPLS_CMN_UNLOCK ();
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        case LSR_CREATEANDGO:

            MPLS_CMN_UNLOCK ();
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;

        case LSR_NOTINSERVICE:
            if (pXCEntry == NULL)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhTestv2MplsXCRowStatus :"
                           "Indexed Entry Doesnt Exists \r\n");
                MPLS_CMN_UNLOCK ();
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                return SNMP_FAILURE;
            }
            if (XC_ROW_STATUS (pXCEntry) != LSR_ACTIVE)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhTestv2MplsXCRowStatus :"
                           "Indexed Entry Can't be set to this state \r\n");
                MPLS_CMN_UNLOCK ();
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            else
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
        default:
            MPLS_CMN_UNLOCK ();
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
}

/****************************************************************************
 Function    :  nmhTestv2MplsXCStorageType
 Input       :  The Indices
                MplsXCIndex
                MplsXCInSegmentIndex
                MplsXCOutSegmentIndex

                The Object 
                testValMplsXCStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsXCStorageType (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pMplsXCIndex,
                            tSNMP_OCTET_STRING_TYPE * pMplsXCInSegmentIndex,
                            tSNMP_OCTET_STRING_TYPE * pMplsXCOutSegmentIndex,
                            INT4 i4TestValMplsXCStorageType)
{
    UINT4               u4XCIndex;
    UINT4               u4InIndex;
    UINT4               u4OutIndex;
    tXcEntry           *pXCEntry = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCIndex, u4XCIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCInSegmentIndex, u4InIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCOutSegmentIndex, u4OutIndex);

    if ((u4XCIndex == 0) || (u4XCIndex > gu4MplsDbMaxXCEntries) ||
        (u4InIndex > gu4MplsDbMaxInSegEntries) ||
        (u4OutIndex > gu4MplsDbMaxOutSegEntries))
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhTestv2MplsXCStorageType : Invalid Indices \r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    switch (i4TestValMplsXCStorageType)
    {
        case MPLS_STORAGE_OTHER:
            /* Fall through */
        case MPLS_STORAGE_VOLATILE:
        case MPLS_STORAGE_NONVOLATILE:
        case MPLS_STORAGE_PERMANENT:
        case MPLS_STORAGE_READONLY:
            break;
        default:
            CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhTestv2MplsXCStorageType :"
                       "Wrong Storage Type \r\n");
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
    MPLS_CMN_LOCK ();

    pXCEntry = MplsGetXCTableEntry (u4XCIndex, u4InIndex, u4OutIndex);
    if (pXCEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhTestv2MplsXCStorageType :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsXCAdminStatus
 Input       :  The Indices
                MplsXCIndex
                MplsXCInSegmentIndex
                MplsXCOutSegmentIndex

                The Object 
                testValMplsXCAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsXCAdminStatus (UINT4 *pu4ErrorCode,
                            tSNMP_OCTET_STRING_TYPE * pMplsXCIndex,
                            tSNMP_OCTET_STRING_TYPE * pMplsXCInSegmentIndex,
                            tSNMP_OCTET_STRING_TYPE * pMplsXCOutSegmentIndex,
                            INT4 i4TestValMplsXCAdminStatus)
{
    UINT4               u4XCIndex;
    UINT4               u4InIndex;
    UINT4               u4OutIndex;
    tXcEntry           *pXCEntry = NULL;

    UNUSED_PARAM (i4TestValMplsXCAdminStatus);

    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCIndex, u4XCIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCInSegmentIndex, u4InIndex);
    MPLS_OCTETSTRING_TO_INTEGER (pMplsXCOutSegmentIndex, u4OutIndex);

    if ((u4XCIndex == 0) || (u4XCIndex > gu4MplsDbMaxXCEntries) ||
        (u4InIndex > gu4MplsDbMaxInSegEntries) ||
        (u4OutIndex > gu4MplsDbMaxOutSegEntries))
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhTestv2MplsXCAdminStatus : Invalid Indices \r\n");
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    MPLS_CMN_LOCK ();
    pXCEntry = MplsGetXCTableEntry (u4XCIndex, u4InIndex, u4OutIndex);
    if (pXCEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhTestv2MplsXCAdminStatus :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (!((XC_ROW_STATUS (pXCEntry) == LSR_NOTREADY) ||
          (XC_ROW_STATUS (pXCEntry) == LSR_NOTINSERVICE) ||
          (XC_ROW_STATUS (pXCEntry) == LSR_ACTIVE)))
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhTestv2MplsXCAdminStatus : Invalid Row Status \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();

    switch (i4TestValMplsXCAdminStatus)
    {
        case XC_ADMIN_UP:
            /* Fall  Through */
        case XC_ADMIN_DOWN:
            return SNMP_SUCCESS;
        case XC_ADMIN_TEST:
            CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhTestv2MplsXCAdminStatus :"
                       "Test State currently not supported \r\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
}

/* LOW LEVEL Routines for Table : MplsInSegmentMapTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsInSegmentMapTable
 Input       :  The Indices
                MplsInSegmentMapInterface
                MplsInSegmentMapLabel
                MplsInSegmentMapLabelPtrIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsInSegmentMapTable (INT4 i4MplsInSegmentMapInterface,
                                               UINT4 u4MplsInSegmentMapLabel,
                                               tSNMP_OID_TYPE *
                                               pMplsInSegmentMapLabelPtrIndex)
{
    UNUSED_PARAM (pMplsInSegmentMapLabelPtrIndex);

    if ((i4MplsInSegmentMapInterface < MPLS_MIN_TNL_IF_VALUE) &&
        (i4MplsInSegmentMapInterface > MPLS_MAX_TNL_IF_VALUE))
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhValidateIndexInstanceMplsInSegmentMapTable :"
                   "Invalid Indices \r\n");
        return SNMP_FAILURE;
    }
    if (u4MplsInSegmentMapLabel == 0)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhValidateIndexInstanceMplsInSegmentMapTable :"
                   "Wrong Label values \r\n");
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsInSegmentMapTable
 Input       :  The Indices
                MplsInSegmentMapInterface
                MplsInSegmentMapLabel
                MplsInSegmentMapLabelPtrIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsInSegmentMapTable (INT4 *pi4MplsInSegmentMapInterface,
                                       UINT4 *pu4MplsInSegmentMapLabel,
                                       tSNMP_OID_TYPE *
                                       pMplsInSegmentMapLabelPtrIndex)
{
    tInSegment         *pInSegment = NULL;
    UINT4               u4MplsIndex = 0;
    UINT4               u4TempMplsInSegmentMapLabel = 0;
    UINT4               u4TempMplsInSegmentMapInterface = 0;

    MPLS_CMN_LOCK ();

    /* Looping gpInSegmentRbTree to get least IfIndex */
    while ((pInSegment = MplsInSegmentTableNextEntry (u4MplsIndex)) != NULL)
    {
        /* Populating Temporary Ifindex and Label with the first entry */
        if ((u4TempMplsInSegmentMapInterface == 0) &&
            (u4TempMplsInSegmentMapLabel == 0))
        {
            u4TempMplsInSegmentMapInterface = INSEGMENT_IF_INDEX (pInSegment);
            u4TempMplsInSegmentMapLabel = INSEGMENT_LABEL (pInSegment);
            u4MplsIndex = INSEGMENT_INDEX (pInSegment);
            continue;
        }

        if (INSEGMENT_IF_INDEX (pInSegment) < u4TempMplsInSegmentMapInterface)
        {
            u4TempMplsInSegmentMapInterface = INSEGMENT_IF_INDEX (pInSegment);
            u4TempMplsInSegmentMapLabel = INSEGMENT_LABEL (pInSegment);
        }

        /* When IfIndex is similar then fetching least label value */
        if ((INSEGMENT_IF_INDEX (pInSegment) ==
             u4TempMplsInSegmentMapInterface) &&
            (INSEGMENT_LABEL (pInSegment) < u4TempMplsInSegmentMapLabel))
        {
            u4TempMplsInSegmentMapLabel = INSEGMENT_LABEL (pInSegment);
        }
        u4MplsIndex = INSEGMENT_INDEX (pInSegment);
    }                            /* End of while */

    /* Temporary IfIndex and label contains least value from RB Tree */
    if ((u4TempMplsInSegmentMapInterface != 0) &&
        (u4TempMplsInSegmentMapLabel != 0))
    {
        *pi4MplsInSegmentMapInterface = (INT4) u4TempMplsInSegmentMapInterface;
        *pu4MplsInSegmentMapLabel = u4TempMplsInSegmentMapLabel;
        MEMSET (pMplsInSegmentMapLabelPtrIndex->pu4_OidList, 0,
                sizeof (UINT4) * 2);
        pMplsInSegmentMapLabelPtrIndex->u4_Length = 2;
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsInSegmentMapTable
 Input       :  The Indices
                MplsInSegmentMapInterface
                nextMplsInSegmentMapInterface
                MplsInSegmentMapLabel
                nextMplsInSegmentMapLabel
                MplsInSegmentMapLabelPtrIndex
                nextMplsInSegmentMapLabelPtrIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMplsInSegmentMapTable (INT4 i4MplsInSegmentMapInterface,
                                      INT4 *pi4NextMplsInSegmentMapInterface,
                                      UINT4 u4MplsInSegmentMapLabel,
                                      UINT4 *pu4NextMplsInSegmentMapLabel,
                                      tSNMP_OID_TYPE *
                                      pMplsInSegmentMapLabelPtrIndex,
                                      tSNMP_OID_TYPE *
                                      pNextMplsInSegmentMapLabelPtrIndex)
{
    UINT4               u4MplsIndex = 0;
    UINT4               u4TempMplsInSegmentMapLabel = 0;
    UINT4               u4TempMplsInSegmentMapInterface = 0;
    tInSegment         *pInSegment = NULL;

    UNUSED_PARAM (pMplsInSegmentMapLabelPtrIndex);

    MPLS_CMN_LOCK ();

    /* Looping gpInSegmentRbTree to get next least Ifindex */
    while ((pInSegment = MplsInSegmentTableNextEntry (u4MplsIndex)) != NULL)
    {
        /* Assigning first entry from RB tree to temporary values
         * such that index value should be greater than or equal to 
         * input IfIndex. */
        if ((u4TempMplsInSegmentMapInterface == 0) &&
            (u4TempMplsInSegmentMapLabel == 0))
        {
            if (INSEGMENT_IF_INDEX (pInSegment) >
                (UINT4) i4MplsInSegmentMapInterface)
            {
                u4TempMplsInSegmentMapInterface =
                    INSEGMENT_IF_INDEX (pInSegment);
                u4TempMplsInSegmentMapLabel = INSEGMENT_LABEL (pInSegment);
                u4MplsIndex = INSEGMENT_INDEX (pInSegment);
                continue;
            }
            /* If IfIndex is similar then comparison is done for label */
            else if ((INSEGMENT_IF_INDEX (pInSegment) ==
                      (UINT4) i4MplsInSegmentMapInterface)
                     && (INSEGMENT_LABEL (pInSegment) >
                         u4MplsInSegmentMapLabel))
            {
                u4TempMplsInSegmentMapInterface =
                    INSEGMENT_IF_INDEX (pInSegment);
                u4TempMplsInSegmentMapLabel = INSEGMENT_LABEL (pInSegment);
                u4MplsIndex = INSEGMENT_INDEX (pInSegment);
                continue;
            }
        }

        /* Always assigning temporary variable with best least values */
        if ((INSEGMENT_IF_INDEX (pInSegment) < u4TempMplsInSegmentMapInterface)
            && (INSEGMENT_IF_INDEX (pInSegment) >
                (UINT4) i4MplsInSegmentMapInterface))
        {
            u4TempMplsInSegmentMapInterface = INSEGMENT_IF_INDEX (pInSegment);
            u4TempMplsInSegmentMapLabel = INSEGMENT_LABEL (pInSegment);
        }
	else if ((INSEGMENT_IF_INDEX (pInSegment) < u4TempMplsInSegmentMapInterface)
          && (INSEGMENT_IF_INDEX (pInSegment) ==
             (UINT4) i4MplsInSegmentMapInterface))
        {
             if(INSEGMENT_LABEL (pInSegment) > u4MplsInSegmentMapLabel)
             {
                u4TempMplsInSegmentMapInterface = INSEGMENT_IF_INDEX (pInSegment);
                u4TempMplsInSegmentMapLabel = INSEGMENT_LABEL (pInSegment);
             }
        }
        else if ((INSEGMENT_IF_INDEX (pInSegment) ==
                  u4TempMplsInSegmentMapInterface)
                 && (INSEGMENT_IF_INDEX (pInSegment) >
                     (UINT4) i4MplsInSegmentMapInterface))
        {
            if (INSEGMENT_LABEL (pInSegment) < u4TempMplsInSegmentMapLabel)
            {
                u4TempMplsInSegmentMapLabel = INSEGMENT_LABEL (pInSegment);
            }
        }
        else if ((INSEGMENT_IF_INDEX (pInSegment) ==
                  u4TempMplsInSegmentMapInterface)
                 && (INSEGMENT_IF_INDEX (pInSegment) ==
                     (UINT4) i4MplsInSegmentMapInterface))
        {
            if ((INSEGMENT_LABEL (pInSegment) < u4TempMplsInSegmentMapLabel) &&
                (INSEGMENT_LABEL (pInSegment) > u4MplsInSegmentMapLabel))
            {
                u4TempMplsInSegmentMapLabel = INSEGMENT_LABEL (pInSegment);
            }
        }
        u4MplsIndex = INSEGMENT_INDEX (pInSegment);
    }                            /* End of while */

    if ((u4TempMplsInSegmentMapInterface != 0) &&
        (u4TempMplsInSegmentMapInterface >=
         (UINT4) i4MplsInSegmentMapInterface))
    {
        *pi4NextMplsInSegmentMapInterface =
            (INT4) u4TempMplsInSegmentMapInterface;
        *pu4NextMplsInSegmentMapLabel = u4TempMplsInSegmentMapLabel;
        MEMSET (pNextMplsInSegmentMapLabelPtrIndex->pu4_OidList,
                0, sizeof (UINT4) * 2);
        pNextMplsInSegmentMapLabelPtrIndex->u4_Length = 2;
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }

    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
               "\r\n nmhGetNextIndexMplsInSegmentMapTable :"
               "Indexed Entry Unavailable \r\n");
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsInSegmentMapIndex
 Input       :  The Indices
                MplsInSegmentMapInterface
                MplsInSegmentMapLabel
                MplsInSegmentMapLabelPtrIndex

                The Object 
                retValMplsInSegmentMapIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsInSegmentMapIndex (INT4 i4MplsInSegmentMapInterface,
                             UINT4 u4MplsInSegmentMapLabel,
                             tSNMP_OID_TYPE * pMplsInSegmentMapLabelPtrIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValMplsInSegmentMapIndex)
{
    tInSegment         *pInSegment = NULL;

    UNUSED_PARAM (pMplsInSegmentMapLabelPtrIndex);

    MPLS_CMN_LOCK ();
    pInSegment = MplsSignalGetInSegmentTableEntry
        ((UINT4) i4MplsInSegmentMapInterface, u4MplsInSegmentMapLabel);
    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetMplsInSegmentMapIndex :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    MPLS_INTEGER_TO_OCTETSTRING (INSEGMENT_INDEX (pInSegment),
                                 pRetValMplsInSegmentMapIndex);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsXCNotificationsEnable
 Input       :  The Indices

                The Object 
                retValMplsXCNotificationsEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsXCNotificationsEnable (INT4 *pi4RetValMplsXCNotificationsEnable)
{
    *pi4RetValMplsXCNotificationsEnable = gu1NotifEnable;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsXCNotificationsEnable
 Input       :  The Indices

                The Object 
                setValMplsXCNotificationsEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsXCNotificationsEnable (INT4 i4SetValMplsXCNotificationsEnable)
{
    gu1NotifEnable = (UINT1) i4SetValMplsXCNotificationsEnable;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsXCNotificationsEnable
 Input       :  The Indices

                The Object 
                testValMplsXCNotificationsEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsXCNotificationsEnable (UINT4 *pu4ErrorCode,
                                    INT4 i4TestValMplsXCNotificationsEnable)
{
    switch (i4TestValMplsXCNotificationsEnable)
    {
        case MPLS_SNMP_TRUE:
            /* Fall Through */
        case MPLS_SNMP_FALSE:
            return SNMP_SUCCESS;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2MplsXCTable
 Input       :  The Indices
                MplsXCIndex
                MplsXCInSegmentIndex
                MplsXCOutSegmentIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MplsXCTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsMaxLabelStackDepth
 Input       :  The Indices

                The Object 
                retValMplsMaxLabelStackDepth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsMaxLabelStackDepth (UINT4 *pu4RetValMplsMaxLabelStackDepth)
{

    *pu4RetValMplsMaxLabelStackDepth = MAX_MPLSDB_LABELS_PER_ENTRY;
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsLabelStackIndexNext
 Input       :  The Indices

                The Object 
                retValMplsLabelStackIndexNext
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLabelStackIndexNext (tSNMP_OCTET_STRING_TYPE *
                               pRetValMplsLabelStackIndexNext)
{

    UINT4               u4Index;

    u4Index = MplsLblStackGetIndex ();
    MPLS_INTEGER_TO_OCTETSTRING (u4Index, pRetValMplsLabelStackIndexNext);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MplsLabelStackTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsLabelStackTable
 Input       :  The Indices
                MplsLabelStackIndex
                MplsLabelStackLabelIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsLabelStackTable (tSNMP_OCTET_STRING_TYPE *
                                             pMplsLabelStackIndex,
                                             UINT4 u4MplsLabelStackLabelIndex)
{

    UINT4               u4LabelIndex;
    MPLS_OCTETSTRING_TO_INTEGER (pMplsLabelStackIndex, u4LabelIndex);
    if ((u4MplsLabelStackLabelIndex > 0) && (u4LabelIndex > 0))
    {
        return SNMP_SUCCESS;
    }
    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
               "ValidateIndexInstanceMplsLabelStackTable Failed \t\n");
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsLabelStackTable
 Input       :  The Indices
                MplsLabelStackIndex
                MplsLabelStackLabelIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsLabelStackTable (tSNMP_OCTET_STRING_TYPE *
                                     pMplsLabelStackIndex,
                                     UINT4 *pu4MplsLabelStackLabelIndex)
{

    tLblStkEntry       *pLblStkEntry = NULL;
    tLblEntry          *pLblEntry = NULL;
    MPLS_CMN_LOCK ();
    if ((pLblStkEntry = MplsLblStkTableNextEntry (0)) == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "GetFirstIndexMplsLabelStackTable Failed \t\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    MPLS_INTEGER_TO_OCTETSTRING (pLblStkEntry->u4Index, pMplsLabelStackIndex);
    pLblEntry = (tLblEntry *) TMO_SLL_First (&(pLblStkEntry->LblList));
    if (pLblEntry == NULL)
    {
        CMNDB_DBG
            (DEBUG_DEBUG_LEVEL,
             "GetFirstIndexMplsLabelStackTable Label Entry Get Failed\t\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pu4MplsLabelStackLabelIndex = pLblEntry->u4LblIndex;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsLabelStackTable
 Input       :  The Indices
                MplsLabelStackIndex
                nextMplsLabelStackIndex
                MplsLabelStackLabelIndex
                nextMplsLabelStackLabelIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMplsLabelStackTable (tSNMP_OCTET_STRING_TYPE *
                                    pMplsLabelStackIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pNextMplsLabelStackIndex,
                                    UINT4 u4MplsLabelStackLabelIndex,
                                    UINT4 *pu4NextMplsLabelStackLabelIndex)
{
    tLblStkEntry       *pLblStkEntry = NULL;
    tLblEntry          *pLblEntry = NULL;
    UINT4               u4LblStkIndex = 0, u4LblStkLocalIndex = 0;

    if (pMplsLabelStackIndex->i4_Length < 0)
    {
        return SNMP_FAILURE;
    }

    MPLS_CMN_LOCK ();
    MPLS_OCTETSTRING_TO_INTEGER (pMplsLabelStackIndex, u4LblStkIndex);
    while ((pLblStkEntry =
            MplsLblStkTableNextEntry (u4LblStkLocalIndex)) != NULL)
    {
        if (pLblStkEntry->u4Index == u4LblStkIndex)
        {
            TMO_SLL_Scan (&(pLblStkEntry->LblList), pLblEntry, tLblEntry *)
            {
                if (pLblEntry->u4LblIndex > u4MplsLabelStackLabelIndex)
                {
                    MPLS_INTEGER_TO_OCTETSTRING (pLblStkEntry->u4Index,
                                                 pNextMplsLabelStackIndex);
                    *pu4NextMplsLabelStackLabelIndex = pLblEntry->u4LblIndex;
                    MPLS_CMN_UNLOCK ();
                    return SNMP_SUCCESS;
                }
            }
        }
        else if (pLblStkEntry->u4Index > u4LblStkIndex)
        {
            MPLS_INTEGER_TO_OCTETSTRING (pLblStkEntry->u4Index,
                                         pNextMplsLabelStackIndex);
            pLblEntry = (tLblEntry *) TMO_SLL_First (&(pLblStkEntry->LblList));
            if (pLblEntry != NULL)
            {
                *pu4NextMplsLabelStackLabelIndex = pLblEntry->u4LblIndex;
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
        }
        u4LblStkLocalIndex = pLblStkEntry->u4Index;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsLabelStackLabel
 Input       :  The Indices
                MplsLabelStackIndex
                MplsLabelStackLabelIndex

                The Object 
                retValMplsLabelStackLabel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLabelStackLabel (tSNMP_OCTET_STRING_TYPE * pMplsLabelStackIndex,
                           UINT4 u4MplsLabelStackLabelIndex,
                           UINT4 *pu4RetValMplsLabelStackLabel)
{

    tLblStkEntry       *pLblStkEntry = NULL;
    tLblEntry          *pLblEntry = NULL;
    UINT4               u4LblStkIndex = 0;
    MPLS_CMN_LOCK ();
    MPLS_OCTETSTRING_TO_INTEGER (pMplsLabelStackIndex, u4LblStkIndex);
    pLblStkEntry = MplsGetLblStkTableEntry (u4LblStkIndex);
    if (pLblStkEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "GetMplsLabelStackLabel Failed \t\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pLblStkEntry->LblList), pLblEntry, tLblEntry *)
    {
        if (pLblEntry->u4LblIndex == u4MplsLabelStackLabelIndex)
        {
            *pu4RetValMplsLabelStackLabel = pLblEntry->u4Label;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetMplsLabelStackLabelPtr
 Input       :  The Indices
                MplsLabelStackIndex
                MplsLabelStackLabelIndex

                The Object 
                retValMplsLabelStackLabelPtr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLabelStackLabelPtr (tSNMP_OCTET_STRING_TYPE * pMplsLabelStackIndex,
                              UINT4 u4MplsLabelStackLabelIndex,
                              tSNMP_OID_TYPE * pRetValMplsLabelStackLabelPtr)
{

    tLblStkEntry       *pLblStkEntry = NULL;
    tLblEntry          *pLblEntry = NULL;
    UINT4               u4LblStkIndex = 0;
    MPLS_CMN_LOCK ();
    MPLS_OCTETSTRING_TO_INTEGER (pMplsLabelStackIndex, u4LblStkIndex);
    pLblStkEntry = MplsGetLblStkTableEntry (u4LblStkIndex);
    if (pLblStkEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "GetMplsLabelStackLabel Failed \t\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pLblStkEntry->LblList), pLblEntry, tLblEntry *)
    {
        if (pLblEntry->u4LblIndex == u4MplsLabelStackLabelIndex)
        {
            pRetValMplsLabelStackLabelPtr->pu4_OidList[0] = 0;
            pRetValMplsLabelStackLabelPtr->pu4_OidList[1] = 0;
            pRetValMplsLabelStackLabelPtr->u4_Length = 2;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetMplsLabelStackRowStatus
 Input       :  The Indices
                MplsLabelStackIndex
                MplsLabelStackLabelIndex

                The Object 
                retValMplsLabelStackRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLabelStackRowStatus (tSNMP_OCTET_STRING_TYPE * pMplsLabelStackIndex,
                               UINT4 u4MplsLabelStackLabelIndex,
                               INT4 *pi4RetValMplsLabelStackRowStatus)
{

    tLblStkEntry       *pLblStkEntry = NULL;
    tLblEntry          *pLblEntry = NULL;
    UINT4               u4LblStkIndex = 0;
    MPLS_CMN_LOCK ();
    MPLS_OCTETSTRING_TO_INTEGER (pMplsLabelStackIndex, u4LblStkIndex);
    pLblStkEntry = MplsGetLblStkTableEntry (u4LblStkIndex);
    if (pLblStkEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "GetMplsLabelStackRowStatus Failed \t\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pLblStkEntry->LblList), pLblEntry, tLblEntry *)
    {
        if (pLblEntry->u4LblIndex == u4MplsLabelStackLabelIndex)
        {
            *pi4RetValMplsLabelStackRowStatus = pLblEntry->u1RowStatus;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetMplsLabelStackStorageType
 Input       :  The Indices
                MplsLabelStackIndex
                MplsLabelStackLabelIndex

                The Object 
                retValMplsLabelStackStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsLabelStackStorageType (tSNMP_OCTET_STRING_TYPE * pMplsLabelStackIndex,
                                 UINT4 u4MplsLabelStackLabelIndex,
                                 INT4 *pi4RetValMplsLabelStackStorageType)
{

    tLblStkEntry       *pLblStkEntry = NULL;
    tLblEntry          *pLblEntry = NULL;
    UINT4               u4LblStkIndex = 0;
    MPLS_CMN_LOCK ();
    MPLS_OCTETSTRING_TO_INTEGER (pMplsLabelStackIndex, u4LblStkIndex);
    pLblStkEntry = MplsGetLblStkTableEntry (u4LblStkIndex);
    if (pLblStkEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "GetMplsLabelStackStorageType Failed \t\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(pLblStkEntry->LblList), pLblEntry, tLblEntry *)
    {
        if (pLblEntry->u4LblIndex == u4MplsLabelStackLabelIndex)
        {
            *pi4RetValMplsLabelStackStorageType = pLblEntry->u1Storage;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsLabelStackLabel
 Input       :  The Indices
                MplsLabelStackIndex
                MplsLabelStackLabelIndex

                The Object 
                setValMplsLabelStackLabel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLabelStackLabel (tSNMP_OCTET_STRING_TYPE * pMplsLabelStackIndex,
                           UINT4 u4MplsLabelStackLabelIndex,
                           UINT4 u4SetValMplsLabelStackLabel)
{

    tLblStkEntry       *pLblStkEntry = NULL;
    tLblEntry          *pLblEntry = NULL;
    UINT4               u4LblStkIndex = 0;
    MPLS_CMN_LOCK ();
    MPLS_OCTETSTRING_TO_INTEGER (pMplsLabelStackIndex, u4LblStkIndex);
    pLblStkEntry = MplsGetLblStkTableEntry (u4LblStkIndex);
    if (pLblStkEntry == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    TMO_SLL_Scan (&(pLblStkEntry->LblList), pLblEntry, tLblEntry *)
    {
        if (pLblEntry->u4LblIndex == u4MplsLabelStackLabelIndex)
        {
            pLblEntry->u4Label = u4SetValMplsLabelStackLabel;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsLabelStackLabelPtr
 Input       :  The Indices
                MplsLabelStackIndex
                MplsLabelStackLabelIndex

                The Object 
                setValMplsLabelStackLabelPtr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLabelStackLabelPtr (tSNMP_OCTET_STRING_TYPE * pMplsLabelStackIndex,
                              UINT4 u4MplsLabelStackLabelIndex,
                              tSNMP_OID_TYPE * pSetValMplsLabelStackLabelPtr)
{
    UNUSED_PARAM (pMplsLabelStackIndex);
    UNUSED_PARAM (u4MplsLabelStackLabelIndex);
    UNUSED_PARAM (pSetValMplsLabelStackLabelPtr);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsLabelStackRowStatus
 Input       :  The Indices
                MplsLabelStackIndex
                MplsLabelStackLabelIndex

                The Object 
                setValMplsLabelStackRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLabelStackRowStatus (tSNMP_OCTET_STRING_TYPE * pMplsLabelStackIndex,
                               UINT4 u4MplsLabelStackLabelIndex,
                               INT4 i4SetValMplsLabelStackRowStatus)
{

    tLblStkEntry       *pLblStkEntry = NULL;
    tLblEntry          *pLblEntry = NULL;
    UINT4               u4LblStkIndex = 0;
    MPLS_CMN_LOCK ();
    MPLS_OCTETSTRING_TO_INTEGER (pMplsLabelStackIndex, u4LblStkIndex);
    pLblStkEntry = MplsGetLblStkTableEntry (u4LblStkIndex);
    if (pLblStkEntry != NULL)
    {
        TMO_SLL_Scan (&(pLblStkEntry->LblList), pLblEntry, tLblEntry *)
        {
            if (pLblEntry->u4LblIndex == u4MplsLabelStackLabelIndex)
            {
                break;
            }
        }
    }
    switch (i4SetValMplsLabelStackRowStatus)
    {
        case CREATE_AND_WAIT:

            if ((pLblStkEntry == NULL) && (pLblEntry == NULL))
            {
                if (MplsLblStackSetIndex (u4LblStkIndex) != u4LblStkIndex)
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               "SetMplsLabelStackRowStatus Failed \t\n");
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
                pLblStkEntry = MplsCreateLblStkTableEntry (u4LblStkIndex);
                if (pLblStkEntry == NULL)
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               "SetMplsLabelStackRowStatus Failed \t\n");
                    MplsLblStackRelIndex (u4LblStkIndex);
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
            }
            MplsAddLblStkLabelEntry (u4LblStkIndex, u4MplsLabelStackLabelIndex);
            TMO_SLL_Scan (&(pLblStkEntry->LblList), pLblEntry, tLblEntry *)
            {
                if (pLblEntry->u4LblIndex == u4MplsLabelStackLabelIndex)
                {
                    break;
                }
            }
            if (pLblEntry != NULL)
            {
                pLblEntry->u1Storage = CMD_STORAGE_VOLATILE;
                pLblEntry->u1RowStatus = NOT_READY;
            }
            break;
        case ACTIVE:
            if (pLblEntry != NULL)
            {
                pLblEntry->u1RowStatus = ACTIVE;
            }
            break;
        case NOT_IN_SERVICE:
            if (pLblEntry != NULL)
            {
                pLblEntry->u1RowStatus = NOT_IN_SERVICE;
            }
            break;
        case DESTROY:
            /* TODO Scan through Xc Table and 
             * if(mplsLabelIndex == pLblStkEntry) then
             * set mplsLabelIndex=NULL */
            if ((pLblStkEntry != NULL) && (pLblEntry != NULL))
            {
                MplsDeleteLblStkLabelEntry (pLblStkEntry->u4Index,
                                            pLblEntry->u4LblIndex);
                if ((pLblStkEntry->LblList.u4_Count) == 0)
                {
                    MplsDeleteLblStkTableEntry (pLblStkEntry);
                }
            }
            break;
        default:
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "SetMplsLabelStackRowStatus Failed \t\n");
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetMplsLabelStackStorageType
 Input       :  The Indices
                MplsLabelStackIndex
                MplsLabelStackLabelIndex

                The Object 
                setValMplsLabelStackStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsLabelStackStorageType (tSNMP_OCTET_STRING_TYPE *
                                 pMplsLabelStackIndex,
                                 UINT4 u4MplsLabelStackLabelIndex,
                                 INT4 i4SetValMplsLabelStackStorageType)
{

    tLblStkEntry       *pLblStkEntry = NULL;
    tLblEntry          *pLblEntry = NULL;
    UINT4               u4LblStkIndex = 0;
    MPLS_CMN_LOCK ();
    MPLS_OCTETSTRING_TO_INTEGER (pMplsLabelStackIndex, u4LblStkIndex);
    pLblStkEntry = MplsGetLblStkTableEntry (u4LblStkIndex);
    if (pLblStkEntry == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    TMO_SLL_Scan (&(pLblStkEntry->LblList), pLblEntry, tLblEntry *)
    {
        if (pLblEntry->u4LblIndex == u4MplsLabelStackLabelIndex)
        {
            pLblEntry->u1Storage = (UINT1) i4SetValMplsLabelStackStorageType;
            break;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsLabelStackLabel
 Input       :  The Indices
                MplsLabelStackIndex
                MplsLabelStackLabelIndex

                The Object 
                testValMplsLabelStackLabel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLabelStackLabel (UINT4 *pu4ErrorCode,
                              tSNMP_OCTET_STRING_TYPE * pMplsLabelStackIndex,
                              UINT4 u4MplsLabelStackLabelIndex,
                              UINT4 u4TestValMplsLabelStackLabel)
{

    tLblStkEntry       *pLblStkEntry = NULL;
    tLblEntry          *pLblEntry = NULL;
    UINT4               u4LblStkIndex = 0;
    MPLS_CMN_LOCK ();
    MPLS_OCTETSTRING_TO_INTEGER (pMplsLabelStackIndex, u4LblStkIndex);
    if ((pLblStkEntry = MplsGetLblStkTableEntry (u4LblStkIndex)) == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Testv2MplsLabelStackLabel Failed :No Entry Present\t\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    TMO_SLL_Scan (&(pLblStkEntry->LblList), pLblEntry, tLblEntry *)
    {
        if (pLblEntry->u4LblIndex == u4MplsLabelStackLabelIndex)
        {
            break;
        }
    }

    if (pLblEntry == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    if ((pLblEntry->u1RowStatus == NOT_READY)
        || (pLblEntry->u1RowStatus == NOT_IN_SERVICE))
    {
        if (u4TestValMplsLabelStackLabel > 0)
        {
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;

        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsLabelStackLabel Failed \t\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsLabelStackLabel Failed \t\n");
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2MplsLabelStackLabelPtr
 Input       :  The Indices
                MplsLabelStackIndex
                MplsLabelStackLabelIndex

                The Object 
                testValMplsLabelStackLabelPtr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLabelStackLabelPtr (UINT4 *pu4ErrorCode,
                                 tSNMP_OCTET_STRING_TYPE * pMplsLabelStackIndex,
                                 UINT4 u4MplsLabelStackLabelIndex,
                                 tSNMP_OID_TYPE *
                                 pTestValMplsLabelStackLabelPtr)
{
    UNUSED_PARAM (pMplsLabelStackIndex);
    UNUSED_PARAM (u4MplsLabelStackLabelIndex);
    UNUSED_PARAM (pTestValMplsLabelStackLabelPtr);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2MplsLabelStackRowStatus
 Input       :  The Indices
                MplsLabelStackIndex
                MplsLabelStackLabelIndex

                The Object 
                testValMplsLabelStackRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLabelStackRowStatus (UINT4 *pu4ErrorCode,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pMplsLabelStackIndex,
                                  UINT4 u4MplsLabelStackLabelIndex,
                                  INT4 i4TestValMplsLabelStackRowStatus)
{

    tLblStkEntry       *pLblStkEntry = NULL;
    tLblEntry          *pLblEntry = NULL;
    UINT4               u4LblStkIndex = 0;
    MPLS_CMN_LOCK ();
    MPLS_OCTETSTRING_TO_INTEGER (pMplsLabelStackIndex, u4LblStkIndex);
    pLblStkEntry = MplsGetLblStkTableEntry (u4LblStkIndex);
    if (pLblStkEntry != NULL)
    {
        TMO_SLL_Scan (&(pLblStkEntry->LblList), pLblEntry, tLblEntry *)
        {
            if (pLblEntry->u4LblIndex == u4MplsLabelStackLabelIndex)
            {
                break;
            }
        }
    }
    switch (i4TestValMplsLabelStackRowStatus)
    {
        case CREATE_AND_WAIT:
            if (pLblEntry == NULL)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "Testv2MplsLabelStackRowStatus Failed "
                       "Entry Present :Can't create\t\n");
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        case CREATE_AND_GO:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsLabelStackRowStatus"
                       "Inconsistent Value\t\n");
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        case ACTIVE:
            if (pLblEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "Testv2MplsLabelStackRowStatus Failed"
                           "No Entry Present\t\n");
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            if ((pLblEntry->u1RowStatus == NOT_READY) ||
                (pLblEntry->u1RowStatus == NOT_IN_SERVICE))
            {
                if (pLblEntry->u4Label != 0)
                {
                    MPLS_CMN_UNLOCK ();
                    return SNMP_SUCCESS;
                }
                else
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               "Testv2MplsLabelStackRowStatus Failed \t\n");
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
            }
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "Testv2MplsLabelStackRowStatus Failed \t\n");
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        case NOT_IN_SERVICE:
            if (pLblEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "Testv2MplsLabelStackRowStatus Failed :"
                           "No Entry Present\t\n");
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            if (pLblEntry->u1RowStatus == NOT_READY)
            {
                if (pLblEntry->u4Label > 0)
                {
                    break;
                }
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "Testv2MplsLabelStackRowStatus Failed \t\n");
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            if (pLblEntry->u1RowStatus != ACTIVE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "Testv2MplsLabelStackRowStatus Failed \t\n");
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            break;
        case NOT_READY:
            if (pLblEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "Testv2MplsLabelStackRowStatus Failed :"
                           "No Entry Present\t\n");
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            if ((pLblEntry->u1RowStatus == ACTIVE) ||
                (pLblEntry->u1RowStatus == NOT_IN_SERVICE))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "Testv2MplsLabelStackRowStatus Failed \t\n");
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            break;
        case DESTROY:
            if (pLblEntry == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "Testv2MplsLabelStackRowStatus Failed :\
                           No Entry Present\t\n");
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "Testv2MplsLabelStackRowStatus Failed \t\n");
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2MplsLabelStackStorageType
 Input       :  The Indices
                MplsLabelStackIndex
                MplsLabelStackLabelIndex

                The Object 
                testValMplsLabelStackStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsLabelStackStorageType (UINT4 *pu4ErrorCode,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pMplsLabelStackIndex,
                                    UINT4 u4MplsLabelStackLabelIndex,
                                    INT4 i4TestValMplsLabelStackStorageType)
{

    tLblStkEntry       *pLblStkEntry = NULL;
    tLblEntry          *pLblEntry = NULL;
    UINT4               u4LblStkIndex;
    MPLS_CMN_LOCK ();
    MPLS_OCTETSTRING_TO_INTEGER (pMplsLabelStackIndex, u4LblStkIndex);
    if ((pLblStkEntry = MplsGetLblStkTableEntry (u4LblStkIndex)) == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Testv2MplsLabelStackLabel Failed :No Entry Present\t\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    TMO_SLL_Scan (&(pLblStkEntry->LblList), pLblEntry, tLblEntry *)
    {
        if (pLblEntry->u4LblIndex == u4MplsLabelStackLabelIndex)
        {
            break;
        }
    }
    if (pLblEntry == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    if ((pLblEntry->u1RowStatus == NOT_READY) ||
        (pLblEntry->u1RowStatus == NOT_IN_SERVICE))
    {
        switch (i4TestValMplsLabelStackStorageType)
        {
            case CMD_STORAGE_VOLATILE:
            case CMD_STORAGE_NONVOLATILE:
                break;
            case CMD_STORAGE_OTHER:
            case CMD_STORAGE_PERMANENT:
            case CMD_STORAGE_READONLY:
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "Testv2MplsLabelStackStorageType Failed \t\n");
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "Testv2MplsLabelStackStorageType Failed \t\n");
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Testv2MplsLabelStackStorageType Failed \t\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2MplsLabelStackTable
 Input       :  The Indices
                MplsLabelStackIndex
                MplsLabelStackLabelIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MplsLabelStackTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2MplsXCNotificationsEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MplsXCNotificationsEnable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
