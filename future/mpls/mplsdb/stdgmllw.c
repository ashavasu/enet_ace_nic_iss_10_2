/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdgmllw.c,v 1.3 2014/11/08 11:41:04 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "mplsdbinc.h"
# include  "stdlsrlw.h"
# include  "stdgmllw.h"

/****************************************************************************
 Function    :  nmhValidateIndexInstanceGmplsInterfaceTable
 Input       :  The Indices
                MplsInterfaceIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceGmplsInterfaceTable (INT4 i4MplsInterfaceIndex)
{
    return nmhValidateIndexInstanceMplsInterfaceTable (i4MplsInterfaceIndex);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexGmplsInterfaceTable
 Input       :  The Indices
                MplsInterfaceIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexGmplsInterfaceTable (INT4 *pi4MplsInterfaceIndex)
{
    return nmhGetFirstIndexMplsInterfaceTable (pi4MplsInterfaceIndex);
}

/****************************************************************************
 Function    :  nmhGetNextIndexGmplsInterfaceTable
 Input       :  The Indices
                MplsInterfaceIndex
                nextMplsInterfaceIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexGmplsInterfaceTable (INT4 i4MplsInterfaceIndex,
                                    INT4 *pi4NextMplsInterfaceIndex)
{
    return nmhGetNextIndexMplsInterfaceTable (i4MplsInterfaceIndex,
                                              pi4NextMplsInterfaceIndex);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetGmplsInterfaceSignalingCaps
 Input       :  The Indices
                MplsInterfaceIndex

                The Object 
                retValGmplsInterfaceSignalingCaps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsInterfaceSignalingCaps (INT4 i4MplsInterfaceIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValGmplsInterfaceSignalingCaps)
{
    tMplsIfEntry        MplsIfEntry;
    INT1                i1RetVal = MPLS_ZERO;

    MEMSET (&MplsIfEntry, MPLS_ZERO, sizeof (tMplsIfEntry));

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDbGetAllGmplsInterfaceTable (i4MplsInterfaceIndex,
                                                (&MplsIfEntry));

    MPLS_CMN_UNLOCK ();

    pRetValGmplsInterfaceSignalingCaps->pu1_OctetList[0] =
        MplsIfEntry.GmplsIfEntry.u1SignalingCaps;
    pRetValGmplsInterfaceSignalingCaps->i4_Length = sizeof (UINT1);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetGmplsInterfaceRsvpHelloPeriod
 Input       :  The Indices
                MplsInterfaceIndex

                The Object 
                retValGmplsInterfaceRsvpHelloPeriod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsInterfaceRsvpHelloPeriod (INT4 i4MplsInterfaceIndex,
                                     UINT4
                                     *pu4RetValGmplsInterfaceRsvpHelloPeriod)
{
    UNUSED_PARAM (i4MplsInterfaceIndex);
    UNUSED_PARAM (pu4RetValGmplsInterfaceRsvpHelloPeriod);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetGmplsInterfaceSignalingCaps
 Input       :  The Indices
                MplsInterfaceIndex

                The Object 
                setValGmplsInterfaceSignalingCaps
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetGmplsInterfaceSignalingCaps (INT4 i4MplsInterfaceIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pSetValGmplsInterfaceSignalingCaps)
{
    tMplsIfEntry        MplsIfEntry;
    INT1                i1RetVal = MPLS_ZERO;

    MEMSET (&MplsIfEntry, MPLS_ZERO, sizeof (tMplsIfEntry));

    MplsIfEntry.GmplsIfEntry.u1SignalingCaps =
        pSetValGmplsInterfaceSignalingCaps->pu1_OctetList[MPLS_ZERO];

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDbSetAllGmplsInterfaceTable (i4MplsInterfaceIndex,
                                                (&MplsIfEntry),
                                                GMPLS_LSR_INTF_SIGNALLING_CAPS);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetGmplsInterfaceRsvpHelloPeriod
 Input       :  The Indices
                MplsInterfaceIndex

                The Object 
                setValGmplsInterfaceRsvpHelloPeriod
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetGmplsInterfaceRsvpHelloPeriod (INT4 i4MplsInterfaceIndex,
                                     UINT4
                                     u4SetValGmplsInterfaceRsvpHelloPeriod)
{
    UNUSED_PARAM (i4MplsInterfaceIndex);
    UNUSED_PARAM (u4SetValGmplsInterfaceRsvpHelloPeriod);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2GmplsInterfaceSignalingCaps
 Input       :  The Indices
                MplsInterfaceIndex

                The Object 
                testValGmplsInterfaceSignalingCaps
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2GmplsInterfaceSignalingCaps (UINT4 *pu4ErrorCode,
                                      INT4 i4MplsInterfaceIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pTestValGmplsInterfaceSignalingCaps)
{
    tMplsIfEntry        MplsIfEntry;
    INT1                i1RetVal = MPLS_ZERO;

    MEMSET (&MplsIfEntry, MPLS_ZERO, sizeof (tMplsIfEntry));

    MPLS_CMN_LOCK ();

    MplsIfEntry.GmplsIfEntry.u1SignalingCaps =
        pTestValGmplsInterfaceSignalingCaps->pu1_OctetList[MPLS_ZERO];

    MplsIfEntry.GmplsIfEntry.u1SigCapsLen =
        (UINT1)pTestValGmplsInterfaceSignalingCaps->i4_Length;

    i1RetVal = MplsDbTestAllGmplsInterfaceTable (pu4ErrorCode,
                                                 i4MplsInterfaceIndex,
                                                 (&MplsIfEntry),
                                                 GMPLS_LSR_INTF_SIGNALLING_CAPS);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2GmplsInterfaceRsvpHelloPeriod
 Input       :  The Indices
                MplsInterfaceIndex

                The Object 
                testValGmplsInterfaceRsvpHelloPeriod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2GmplsInterfaceRsvpHelloPeriod (UINT4 *pu4ErrorCode,
                                        INT4 i4MplsInterfaceIndex,
                                        UINT4
                                        u4TestValGmplsInterfaceRsvpHelloPeriod)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (i4MplsInterfaceIndex);
    UNUSED_PARAM (u4TestValGmplsInterfaceRsvpHelloPeriod);
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2GmplsInterfaceTable
 Input       :  The Indices
                MplsInterfaceIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2GmplsInterfaceTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : GmplsInSegmentTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceGmplsInSegmentTable
 Input       :  The Indices
                MplsInSegmentIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceGmplsInSegmentTable (tSNMP_OCTET_STRING_TYPE *
                                             pMplsInSegmentIndex)
{
    return nmhValidateIndexInstanceMplsInSegmentTable (pMplsInSegmentIndex);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexGmplsInSegmentTable
 Input       :  The Indices
                MplsInSegmentIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexGmplsInSegmentTable (tSNMP_OCTET_STRING_TYPE *
                                     pMplsInSegmentIndex)
{
    return nmhGetFirstIndexMplsInSegmentTable (pMplsInSegmentIndex);
}

/****************************************************************************
 Function    :  nmhGetNextIndexGmplsInSegmentTable
 Input       :  The Indices
                MplsInSegmentIndex
                nextMplsInSegmentIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexGmplsInSegmentTable (tSNMP_OCTET_STRING_TYPE *
                                    pMplsInSegmentIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pNextMplsInSegmentIndex)
{
    return nmhGetNextIndexMplsInSegmentTable (pMplsInSegmentIndex,
                                              pNextMplsInSegmentIndex);

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetGmplsInSegmentDirection
 Input       :  The Indices
                MplsInSegmentIndex

                The Object 
                retValGmplsInSegmentDirection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsInSegmentDirection (tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
                               INT4 *pi4RetValGmplsInSegmentDirection)
{
    tInSegment          InSegment;
    UINT4               u4Index = MPLS_ZERO;
    INT1                i1RetVal = MPLS_ZERO;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);
    MEMSET (&InSegment, MPLS_ZERO, sizeof (tInSegment));

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDbGetAllGmplsInSegmentTable (u4Index, (&InSegment));

    MPLS_CMN_UNLOCK ();

    *pi4RetValGmplsInSegmentDirection =
        InSegment.GmplsInSegment.InSegmentDirection;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetGmplsInSegmentExtraParamsPtr
 Input       :  The Indices
                MplsInSegmentIndex

                The Object 
                retValGmplsInSegmentExtraParamsPtr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsInSegmentExtraParamsPtr (tSNMP_OCTET_STRING_TYPE *
                                    pMplsInSegmentIndex,
                                    tSNMP_OID_TYPE *
                                    pRetValGmplsInSegmentExtraParamsPtr)
{
    UNUSED_PARAM (pMplsInSegmentIndex);
    UNUSED_PARAM (pRetValGmplsInSegmentExtraParamsPtr);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetGmplsInSegmentDirection
 Input       :  The Indices
                MplsInSegmentIndex

                The Object 
                setValGmplsInSegmentDirection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetGmplsInSegmentDirection (tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
                               INT4 i4SetValGmplsInSegmentDirection)
{
    tInSegment          InSegment;
    UINT4               u4Index = MPLS_ZERO;
    INT1                i1RetVal = MPLS_ZERO;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);

    InSegment.GmplsInSegment.InSegmentDirection
        = (eDirection) i4SetValGmplsInSegmentDirection;

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDbSetAllGmplsInSegmentTable (u4Index, (&InSegment),
                                                GMPLS_LSR_INSEG_DIRECTION);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetGmplsInSegmentExtraParamsPtr
 Input       :  The Indices
                MplsInSegmentIndex

                The Object 
                setValGmplsInSegmentExtraParamsPtr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetGmplsInSegmentExtraParamsPtr (tSNMP_OCTET_STRING_TYPE *
                                    pMplsInSegmentIndex,
                                    tSNMP_OID_TYPE *
                                    pSetValGmplsInSegmentExtraParamsPtr)
{
    UNUSED_PARAM (pMplsInSegmentIndex);
    UNUSED_PARAM (pSetValGmplsInSegmentExtraParamsPtr);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2GmplsInSegmentDirection
 Input       :  The Indices
                MplsInSegmentIndex

                The Object 
                testValGmplsInSegmentDirection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2GmplsInSegmentDirection (UINT4 *pu4ErrorCode,
                                  tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
                                  INT4 i4TestValGmplsInSegmentDirection)
{
    tInSegment          InSegment;
    UINT4               u4Index = MPLS_ZERO;
    INT1                i1RetVal = MPLS_ZERO;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);

    MEMSET (&InSegment, MPLS_ZERO, sizeof (tInSegment));

    InSegment.GmplsInSegment.InSegmentDirection =
        (eDirection) i4TestValGmplsInSegmentDirection;

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDbTestAllGmplsInSegmentTable (pu4ErrorCode,
                                                 u4Index, (&InSegment),
                                                 GMPLS_LSR_INSEG_DIRECTION);

    MPLS_CMN_UNLOCK ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2GmplsInSegmentExtraParamsPtr
 Input       :  The Indices
                MplsInSegmentIndex

                The Object 
                testValGmplsInSegmentExtraParamsPtr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2GmplsInSegmentExtraParamsPtr (UINT4 *pu4ErrorCode,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMplsInSegmentIndex,
                                       tSNMP_OID_TYPE *
                                       pTestValGmplsInSegmentExtraParamsPtr)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pMplsInSegmentIndex);
    UNUSED_PARAM (pTestValGmplsInSegmentExtraParamsPtr);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2GmplsInSegmentTable
 Input       :  The Indices
                MplsInSegmentIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2GmplsInSegmentTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : GmplsOutSegmentTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceGmplsOutSegmentTable
 Input       :  The Indices
                MplsOutSegmentIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceGmplsOutSegmentTable (tSNMP_OCTET_STRING_TYPE *
                                              pMplsOutSegmentIndex)
{
    return nmhValidateIndexInstanceMplsOutSegmentTable (pMplsOutSegmentIndex);
}

/****************************************************************************
 Function    :  nmhGetFirstIndexGmplsOutSegmentTable
 Input       :  The Indices
                MplsOutSegmentIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexGmplsOutSegmentTable (tSNMP_OCTET_STRING_TYPE *
                                      pMplsOutSegmentIndex)
{
    return nmhGetFirstIndexMplsOutSegmentTable (pMplsOutSegmentIndex);
}

/****************************************************************************
 Function    :  nmhGetNextIndexGmplsOutSegmentTable
 Input       :  The Indices
                MplsOutSegmentIndex
                nextMplsOutSegmentIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexGmplsOutSegmentTable (tSNMP_OCTET_STRING_TYPE *
                                     pMplsOutSegmentIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextMplsOutSegmentIndex)
{
    return nmhGetNextIndexMplsOutSegmentTable (pMplsOutSegmentIndex,
                                               pNextMplsOutSegmentIndex);
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetGmplsOutSegmentDirection
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                retValGmplsOutSegmentDirection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsOutSegmentDirection (tSNMP_OCTET_STRING_TYPE * pMplsOutSegmentIndex,
                                INT4 *pi4RetValGmplsOutSegmentDirection)
{
    tOutSegment         OutSegment;
    UINT4               u4Index = MPLS_ZERO;
    INT1                i1RetVal = MPLS_ZERO;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);
    MEMSET (&OutSegment, MPLS_ZERO, sizeof (tOutSegment));

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDbGetAllGmplsOutSegmentTable (u4Index, (&OutSegment));

    MPLS_CMN_UNLOCK ();

    *pi4RetValGmplsOutSegmentDirection =
        OutSegment.GmplsOutSegment.OutSegmentDirection;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetGmplsOutSegmentTTLDecrement
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                retValGmplsOutSegmentTTLDecrement
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsOutSegmentTTLDecrement (tSNMP_OCTET_STRING_TYPE *
                                   pMplsOutSegmentIndex,
                                   UINT4 *pu4RetValGmplsOutSegmentTTLDecrement)
{
    UNUSED_PARAM (pMplsOutSegmentIndex);
    UNUSED_PARAM (pu4RetValGmplsOutSegmentTTLDecrement);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetGmplsOutSegmentExtraParamsPtr
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                retValGmplsOutSegmentExtraParamsPtr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsOutSegmentExtraParamsPtr (tSNMP_OCTET_STRING_TYPE *
                                     pMplsOutSegmentIndex,
                                     tSNMP_OID_TYPE *
                                     pRetValGmplsOutSegmentExtraParamsPtr)
{
    UNUSED_PARAM (pMplsOutSegmentIndex);
    UNUSED_PARAM (pRetValGmplsOutSegmentExtraParamsPtr);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetGmplsOutSegmentDirection
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                setValGmplsOutSegmentDirection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetGmplsOutSegmentDirection (tSNMP_OCTET_STRING_TYPE * pMplsOutSegmentIndex,
                                INT4 i4SetValGmplsOutSegmentDirection)
{
    tOutSegment         OutSegment;
    UINT4               u4Index = MPLS_ZERO;
    INT1                i1RetVal = MPLS_ZERO;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);

    OutSegment.GmplsOutSegment.OutSegmentDirection
        = (eDirection) i4SetValGmplsOutSegmentDirection;

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDbSetAllGmplsOutSegmentTable (u4Index, (&OutSegment),
                                                 GMPLS_LSR_OUTSEG_DIRECTION);

    MPLS_CMN_UNLOCK ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetGmplsOutSegmentTTLDecrement
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                setValGmplsOutSegmentTTLDecrement
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetGmplsOutSegmentTTLDecrement (tSNMP_OCTET_STRING_TYPE *
                                   pMplsOutSegmentIndex,
                                   UINT4 u4SetValGmplsOutSegmentTTLDecrement)
{
    UNUSED_PARAM (pMplsOutSegmentIndex);
    UNUSED_PARAM (u4SetValGmplsOutSegmentTTLDecrement);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetGmplsOutSegmentExtraParamsPtr
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                setValGmplsOutSegmentExtraParamsPtr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetGmplsOutSegmentExtraParamsPtr (tSNMP_OCTET_STRING_TYPE *
                                     pMplsOutSegmentIndex,
                                     tSNMP_OID_TYPE *
                                     pSetValGmplsOutSegmentExtraParamsPtr)
{
    UNUSED_PARAM (pMplsOutSegmentIndex);
    UNUSED_PARAM (pSetValGmplsOutSegmentExtraParamsPtr);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2GmplsOutSegmentDirection
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                testValGmplsOutSegmentDirection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2GmplsOutSegmentDirection (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pMplsOutSegmentIndex,
                                   INT4 i4TestValGmplsOutSegmentDirection)
{
    tOutSegment         OutSegment;
    UINT4               u4Index = MPLS_ZERO;
    INT1                i1RetVal = MPLS_ZERO;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);
    MEMSET (&OutSegment, MPLS_ZERO, sizeof (tOutSegment));

    OutSegment.GmplsOutSegment.OutSegmentDirection =
        (eDirection) i4TestValGmplsOutSegmentDirection;

    MPLS_CMN_LOCK ();

    i1RetVal = MplsDbTestAllGmplsOutSegmentTable (pu4ErrorCode,
                                                  u4Index, (&OutSegment),
                                                  GMPLS_LSR_OUTSEG_DIRECTION);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2GmplsOutSegmentTTLDecrement
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                testValGmplsOutSegmentTTLDecrement
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2GmplsOutSegmentTTLDecrement (UINT4 *pu4ErrorCode,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsOutSegmentIndex,
                                      UINT4
                                      u4TestValGmplsOutSegmentTTLDecrement)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pMplsOutSegmentIndex);
    UNUSED_PARAM (u4TestValGmplsOutSegmentTTLDecrement);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2GmplsOutSegmentExtraParamsPtr
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                testValGmplsOutSegmentExtraParamsPtr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2GmplsOutSegmentExtraParamsPtr (UINT4 *pu4ErrorCode,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pMplsOutSegmentIndex,
                                        tSNMP_OID_TYPE *
                                        pTestValGmplsOutSegmentExtraParamsPtr)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pMplsOutSegmentIndex);
    UNUSED_PARAM (pTestValGmplsOutSegmentExtraParamsPtr);
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2GmplsOutSegmentTable
 Input       :  The Indices
                MplsOutSegmentIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2GmplsOutSegmentTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
