/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdftndb.h,v 1.4 2008/08/20 15:15:07 iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FTNDB_H
#define _FTNDB_H

UINT1 MplsFTNTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 MplsFTNMapTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 MplsFTNPerfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 ftn [] ={1,3,6,1,2,1,10,166,8};
tSNMP_OID_TYPE ftnOID = {9, ftn};


UINT4 MplsFTNIndexNext [ ] ={1,3,6,1,2,1,10,166,8,1,1};
UINT4 MplsFTNTableLastChanged [ ] ={1,3,6,1,2,1,10,166,8,1,2};
UINT4 MplsFTNIndex [ ] ={1,3,6,1,2,1,10,166,8,1,3,1,1};
UINT4 MplsFTNRowStatus [ ] ={1,3,6,1,2,1,10,166,8,1,3,1,2};
UINT4 MplsFTNDescr [ ] ={1,3,6,1,2,1,10,166,8,1,3,1,3};
UINT4 MplsFTNMask [ ] ={1,3,6,1,2,1,10,166,8,1,3,1,4};
UINT4 MplsFTNAddrType [ ] ={1,3,6,1,2,1,10,166,8,1,3,1,5};
UINT4 MplsFTNSourceAddrMin [ ] ={1,3,6,1,2,1,10,166,8,1,3,1,6};
UINT4 MplsFTNSourceAddrMax [ ] ={1,3,6,1,2,1,10,166,8,1,3,1,7};
UINT4 MplsFTNDestAddrMin [ ] ={1,3,6,1,2,1,10,166,8,1,3,1,8};
UINT4 MplsFTNDestAddrMax [ ] ={1,3,6,1,2,1,10,166,8,1,3,1,9};
UINT4 MplsFTNSourcePortMin [ ] ={1,3,6,1,2,1,10,166,8,1,3,1,10};
UINT4 MplsFTNSourcePortMax [ ] ={1,3,6,1,2,1,10,166,8,1,3,1,11};
UINT4 MplsFTNDestPortMin [ ] ={1,3,6,1,2,1,10,166,8,1,3,1,12};
UINT4 MplsFTNDestPortMax [ ] ={1,3,6,1,2,1,10,166,8,1,3,1,13};
UINT4 MplsFTNProtocol [ ] ={1,3,6,1,2,1,10,166,8,1,3,1,14};
UINT4 MplsFTNDscp [ ] ={1,3,6,1,2,1,10,166,8,1,3,1,15};
UINT4 MplsFTNActionType [ ] ={1,3,6,1,2,1,10,166,8,1,3,1,16};
UINT4 MplsFTNActionPointer [ ] ={1,3,6,1,2,1,10,166,8,1,3,1,17};
UINT4 MplsFTNStorageType [ ] ={1,3,6,1,2,1,10,166,8,1,3,1,18};
UINT4 MplsFTNMapTableLastChanged [ ] ={1,3,6,1,2,1,10,166,8,1,4};
UINT4 MplsFTNMapIndex [ ] ={1,3,6,1,2,1,10,166,8,1,5,1,1};
UINT4 MplsFTNMapPrevIndex [ ] ={1,3,6,1,2,1,10,166,8,1,5,1,2};
UINT4 MplsFTNMapCurrIndex [ ] ={1,3,6,1,2,1,10,166,8,1,5,1,3};
UINT4 MplsFTNMapRowStatus [ ] ={1,3,6,1,2,1,10,166,8,1,5,1,4};
UINT4 MplsFTNMapStorageType [ ] ={1,3,6,1,2,1,10,166,8,1,5,1,5};
UINT4 MplsFTNPerfIndex [ ] ={1,3,6,1,2,1,10,166,8,1,6,1,1};
UINT4 MplsFTNPerfCurrIndex [ ] ={1,3,6,1,2,1,10,166,8,1,6,1,2};
UINT4 MplsFTNPerfMatchedPackets [ ] ={1,3,6,1,2,1,10,166,8,1,6,1,3};
UINT4 MplsFTNPerfMatchedOctets [ ] ={1,3,6,1,2,1,10,166,8,1,6,1,4};
UINT4 MplsFTNPerfDiscontinuityTime [ ] ={1,3,6,1,2,1,10,166,8,1,6,1,5};


tMbDbEntry ftnMibEntry[]= {

{{11,MplsFTNIndexNext}, NULL, MplsFTNIndexNextGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,MplsFTNTableLastChanged}, NULL, MplsFTNTableLastChangedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,MplsFTNIndex}, GetNextIndexMplsFTNTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, MplsFTNTableINDEX, 1, 0, 0, NULL},

{{13,MplsFTNRowStatus}, GetNextIndexMplsFTNTable, MplsFTNRowStatusGet, MplsFTNRowStatusSet, MplsFTNRowStatusTest, MplsFTNTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsFTNTableINDEX, 1, 0, 1, NULL},

{{13,MplsFTNDescr}, GetNextIndexMplsFTNTable, MplsFTNDescrGet, MplsFTNDescrSet, MplsFTNDescrTest, MplsFTNTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, MplsFTNTableINDEX, 1, 0, 0, NULL},

{{13,MplsFTNMask}, GetNextIndexMplsFTNTable, MplsFTNMaskGet, MplsFTNMaskSet, MplsFTNMaskTest, MplsFTNTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, MplsFTNTableINDEX, 1, 0, 0, NULL},

{{13,MplsFTNAddrType}, GetNextIndexMplsFTNTable, MplsFTNAddrTypeGet, MplsFTNAddrTypeSet, MplsFTNAddrTypeTest, MplsFTNTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsFTNTableINDEX, 1, 0, 0, NULL},

{{13,MplsFTNSourceAddrMin}, GetNextIndexMplsFTNTable, MplsFTNSourceAddrMinGet, MplsFTNSourceAddrMinSet, MplsFTNSourceAddrMinTest, MplsFTNTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, MplsFTNTableINDEX, 1, 0, 0, NULL},

{{13,MplsFTNSourceAddrMax}, GetNextIndexMplsFTNTable, MplsFTNSourceAddrMaxGet, MplsFTNSourceAddrMaxSet, MplsFTNSourceAddrMaxTest, MplsFTNTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, MplsFTNTableINDEX, 1, 0, 0, NULL},

{{13,MplsFTNDestAddrMin}, GetNextIndexMplsFTNTable, MplsFTNDestAddrMinGet, MplsFTNDestAddrMinSet, MplsFTNDestAddrMinTest, MplsFTNTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, MplsFTNTableINDEX, 1, 0, 0, NULL},

{{13,MplsFTNDestAddrMax}, GetNextIndexMplsFTNTable, MplsFTNDestAddrMaxGet, MplsFTNDestAddrMaxSet, MplsFTNDestAddrMaxTest, MplsFTNTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, MplsFTNTableINDEX, 1, 0, 0, NULL},

{{13,MplsFTNSourcePortMin}, GetNextIndexMplsFTNTable, MplsFTNSourcePortMinGet, MplsFTNSourcePortMinSet, MplsFTNSourcePortMinTest, MplsFTNTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsFTNTableINDEX, 1, 0, 0, "0"},

{{13,MplsFTNSourcePortMax}, GetNextIndexMplsFTNTable, MplsFTNSourcePortMaxGet, MplsFTNSourcePortMaxSet, MplsFTNSourcePortMaxTest, MplsFTNTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsFTNTableINDEX, 1, 0, 0, "65535"},

{{13,MplsFTNDestPortMin}, GetNextIndexMplsFTNTable, MplsFTNDestPortMinGet, MplsFTNDestPortMinSet, MplsFTNDestPortMinTest, MplsFTNTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsFTNTableINDEX, 1, 0, 0, "0"},

{{13,MplsFTNDestPortMax}, GetNextIndexMplsFTNTable, MplsFTNDestPortMaxGet, MplsFTNDestPortMaxSet, MplsFTNDestPortMaxTest, MplsFTNTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsFTNTableINDEX, 1, 0, 0, "65535"},

{{13,MplsFTNProtocol}, GetNextIndexMplsFTNTable, MplsFTNProtocolGet, MplsFTNProtocolSet, MplsFTNProtocolTest, MplsFTNTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, MplsFTNTableINDEX, 1, 0, 0, "255"},

{{13,MplsFTNDscp}, GetNextIndexMplsFTNTable, MplsFTNDscpGet, MplsFTNDscpSet, MplsFTNDscpTest, MplsFTNTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, MplsFTNTableINDEX, 1, 0, 0, NULL},

{{13,MplsFTNActionType}, GetNextIndexMplsFTNTable, MplsFTNActionTypeGet, MplsFTNActionTypeSet, MplsFTNActionTypeTest, MplsFTNTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsFTNTableINDEX, 1, 0, 0, NULL},

{{13,MplsFTNActionPointer}, GetNextIndexMplsFTNTable, MplsFTNActionPointerGet, MplsFTNActionPointerSet, MplsFTNActionPointerTest, MplsFTNTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, MplsFTNTableINDEX, 1, 0, 0, NULL},

{{13,MplsFTNStorageType}, GetNextIndexMplsFTNTable, MplsFTNStorageTypeGet, MplsFTNStorageTypeSet, MplsFTNStorageTypeTest, MplsFTNTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsFTNTableINDEX, 1, 0, 0, "3"},

{{11,MplsFTNMapTableLastChanged}, NULL, MplsFTNMapTableLastChangedGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,MplsFTNMapIndex}, GetNextIndexMplsFTNMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, MplsFTNMapTableINDEX, 3, 0, 0, NULL},

{{13,MplsFTNMapPrevIndex}, GetNextIndexMplsFTNMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, MplsFTNMapTableINDEX, 3, 0, 0, NULL},

{{13,MplsFTNMapCurrIndex}, GetNextIndexMplsFTNMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, MplsFTNMapTableINDEX, 3, 0, 0, NULL},

{{13,MplsFTNMapRowStatus}, GetNextIndexMplsFTNMapTable, MplsFTNMapRowStatusGet, MplsFTNMapRowStatusSet, MplsFTNMapRowStatusTest, MplsFTNMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsFTNMapTableINDEX, 3, 0, 1, NULL},

{{13,MplsFTNMapStorageType}, GetNextIndexMplsFTNMapTable, MplsFTNMapStorageTypeGet, MplsFTNMapStorageTypeSet, MplsFTNMapStorageTypeTest, MplsFTNMapTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsFTNMapTableINDEX, 3, 0, 0, "3"},

{{13,MplsFTNPerfIndex}, GetNextIndexMplsFTNPerfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, MplsFTNPerfTableINDEX, 2, 0, 0, NULL},

{{13,MplsFTNPerfCurrIndex}, GetNextIndexMplsFTNPerfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, MplsFTNPerfTableINDEX, 2, 0, 0, NULL},

{{13,MplsFTNPerfMatchedPackets}, GetNextIndexMplsFTNPerfTable, MplsFTNPerfMatchedPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, MplsFTNPerfTableINDEX, 2, 0, 0, NULL},

{{13,MplsFTNPerfMatchedOctets}, GetNextIndexMplsFTNPerfTable, MplsFTNPerfMatchedOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, MplsFTNPerfTableINDEX, 2, 0, 0, NULL},

{{13,MplsFTNPerfDiscontinuityTime}, GetNextIndexMplsFTNPerfTable, MplsFTNPerfDiscontinuityTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MplsFTNPerfTableINDEX, 2, 0, 0, NULL},
};
tMibData ftnEntry = { 31, ftnMibEntry };
#endif /* _FTNDB_H */

