/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdftnlw.h,v 1.4 2008/08/20 15:15:07 iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsFTNIndexNext ARG_LIST((UINT4 *));

INT1
nmhGetMplsFTNTableLastChanged ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for MplsFTNTable. */
INT1
nmhValidateIndexInstanceMplsFTNTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for MplsFTNTable  */

INT1
nmhGetFirstIndexMplsFTNTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsFTNTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsFTNRowStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetMplsFTNDescr ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMplsFTNMask ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMplsFTNAddrType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetMplsFTNSourceAddrMin ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMplsFTNSourceAddrMax ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMplsFTNDestAddrMin ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMplsFTNDestAddrMax ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMplsFTNSourcePortMin ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetMplsFTNSourcePortMax ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetMplsFTNDestPortMin ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetMplsFTNDestPortMax ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetMplsFTNProtocol ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetMplsFTNDscp ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetMplsFTNActionType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetMplsFTNActionPointer ARG_LIST((UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetMplsFTNStorageType ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsFTNRowStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetMplsFTNDescr ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetMplsFTNMask ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetMplsFTNAddrType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetMplsFTNSourceAddrMin ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetMplsFTNSourceAddrMax ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetMplsFTNDestAddrMin ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetMplsFTNDestAddrMax ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetMplsFTNSourcePortMin ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetMplsFTNSourcePortMax ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetMplsFTNDestPortMin ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetMplsFTNDestPortMax ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetMplsFTNProtocol ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetMplsFTNDscp ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetMplsFTNActionType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetMplsFTNActionPointer ARG_LIST((UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetMplsFTNStorageType ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsFTNRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2MplsFTNDescr ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2MplsFTNMask ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2MplsFTNAddrType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2MplsFTNSourceAddrMin ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2MplsFTNSourceAddrMax ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2MplsFTNDestAddrMin ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2MplsFTNDestAddrMax ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2MplsFTNSourcePortMin ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2MplsFTNSourcePortMax ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2MplsFTNDestPortMin ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2MplsFTNDestPortMax ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2MplsFTNProtocol ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2MplsFTNDscp ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2MplsFTNActionType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2MplsFTNActionPointer ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2MplsFTNStorageType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MplsFTNTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsFTNMapTableLastChanged ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for MplsFTNMapTable. */
INT1
nmhValidateIndexInstanceMplsFTNMapTable ARG_LIST((INT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for MplsFTNMapTable  */

INT1
nmhGetFirstIndexMplsFTNMapTable ARG_LIST((INT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsFTNMapTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsFTNMapRowStatus ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetMplsFTNMapStorageType ARG_LIST((INT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsFTNMapRowStatus ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetMplsFTNMapStorageType ARG_LIST((INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsFTNMapRowStatus ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2MplsFTNMapStorageType ARG_LIST((UINT4 *  ,INT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MplsFTNMapTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for MplsFTNPerfTable. */
INT1
nmhValidateIndexInstanceMplsFTNPerfTable ARG_LIST((INT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for MplsFTNPerfTable  */

INT1
nmhGetFirstIndexMplsFTNPerfTable ARG_LIST((INT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsFTNPerfTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsFTNPerfMatchedPackets ARG_LIST((INT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetMplsFTNPerfMatchedOctets ARG_LIST((INT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetMplsFTNPerfDiscontinuityTime ARG_LIST((INT4  , UINT4 ,UINT4 *));
