/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fslsrlw.c,v 1.8 2013/12/07 10:58:54 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
# include  "lr.h"
# include  "fssnmp.h"
# include  "stdlsrlw.h"
# include  "fslsrlw.h"
# include  "mplsdbinc.h"
#include "bfd.h"

/* LOW LEVEL Routines for Table : FsMplsInSegmentTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsInSegmentTable
 Input       :  The Indices
                MplsInSegmentIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMplsInSegmentTable (tSNMP_OCTET_STRING_TYPE *
                                              pMplsInSegmentIndex)
{
    return (nmhValidateIndexInstanceMplsInSegmentTable (pMplsInSegmentIndex));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsInSegmentTable
 Input       :  The Indices
                MplsInSegmentIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMplsInSegmentTable (tSNMP_OCTET_STRING_TYPE *
                                      pMplsInSegmentIndex)
{
    return (nmhGetFirstIndexMplsInSegmentTable (pMplsInSegmentIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMplsInSegmentTable
 Input       :  The Indices
                MplsInSegmentIndex
                nextMplsInSegmentIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMplsInSegmentTable (tSNMP_OCTET_STRING_TYPE *
                                     pMplsInSegmentIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pNextMplsInSegmentIndex)
{
    return (nmhGetNextIndexMplsInSegmentTable (pMplsInSegmentIndex,
                                               pNextMplsInSegmentIndex));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsInSegmentDirection
 Input       :  The Indices
                MplsInSegmentIndex

                The Object 
                retValFsMplsInSegmentDirection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsInSegmentDirection (tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
                                INT4 *pi4RetValFsMplsInSegmentDirection)
{
    UINT4               u4Index;
    tInSegment         *pInSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pInSegment = MplsGetInSegmentTableEntry (u4Index);
    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetFsMplsInSegmentDirection :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pi4RetValFsMplsInSegmentDirection =
        (INT4) pInSegment->GmplsInSegment.InSegmentDirection;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMplsInSegmentDirection
 Input       :  The Indices
                MplsInSegmentIndex

                The Object 
                setValFsMplsInSegmentDirection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsInSegmentDirection (tSNMP_OCTET_STRING_TYPE * pMplsInSegmentIndex,
                                INT4 i4SetValFsMplsInSegmentDirection)
{
    UINT4               u4Index;
    tInSegment         *pInSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();

    pInSegment = MplsGetInSegmentTableEntry (u4Index);
    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhSetFsMplsInSegmentDirection :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    pInSegment->GmplsInSegment.InSegmentDirection =
        (eDirection) i4SetValFsMplsInSegmentDirection;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMplsInSegmentDirection
 Input       :  The Indices
                MplsInSegmentIndex

                The Object 
                testValFsMplsInSegmentDirection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhTestv2FsMplsInSegmentDirection (UINT4 *pu4ErrorCode,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pMplsInSegmentIndex,
                                   INT4 i4TestValFsMplsInSegmentDirection)
{
    UINT4               u4Index;
    tInSegment         *pInSegment = NULL;
    tXcEntry           *pXcEntry = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsInSegmentIndex, u4Index);
    if (u4Index == 0 || u4Index > gu4MplsDbMaxInSegEntries)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMplsInSegmentDirection != MPLS_DIRECTION_FORWARD) &&
        (i4TestValFsMplsInSegmentDirection != MPLS_DIRECTION_REVERSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    MPLS_CMN_LOCK ();

    pInSegment = MplsGetInSegmentTableEntry (u4Index);
    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n  nmhTestv2FsMplsInSegmentDirection :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (INSEGMENT_ROW_STATUS (pInSegment) == LSR_ACTIVE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhTestv2FsMplsInSegmentDirection : "
                   "Cant Modify Objects \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* If in-segment is associated to P2MP tunnel, block label 
     * configuration in reverse direction */
    pXcEntry = (tXcEntry *) INSEGMENT_XC_INDEX (pInSegment);
    if ((pXcEntry != NULL) && (XC_TNL_TBL_PTR (pXcEntry) != NULL))
    {
        pTeTnlInfo = XC_TNL_TBL_PTR (pXcEntry);
        if ((TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_P2MP)
            && (i4TestValFsMplsInSegmentDirection == MPLS_DIRECTION_REVERSE))
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "\r\n nmhTestv2FsMplsInSegmentDirection : "
                       "Configuring the In-segment direction of P2MP tunnel "
                       "as reverse is not applicable. \r\n");
            MPLS_CMN_UNLOCK ();
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsInSegmentTable
 Input       :  The Indices
                MplsInSegmentIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsInSegmentTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMplsOutSegmentTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsOutSegmentTable
 Input       :  The Indices
                MplsOutSegmentIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMplsOutSegmentTable (tSNMP_OCTET_STRING_TYPE *
                                               pMplsOutSegmentIndex)
{
    return (nmhValidateIndexInstanceMplsOutSegmentTable (pMplsOutSegmentIndex));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsOutSegmentTable
 Input       :  The Indices
                MplsOutSegmentIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMplsOutSegmentTable (tSNMP_OCTET_STRING_TYPE *
                                       pMplsOutSegmentIndex)
{
    return (nmhGetFirstIndexMplsOutSegmentTable (pMplsOutSegmentIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMplsOutSegmentTable
 Input       :  The Indices
                MplsOutSegmentIndex
                nextMplsOutSegmentIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMplsOutSegmentTable (tSNMP_OCTET_STRING_TYPE *
                                      pMplsOutSegmentIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pNextMplsOutSegmentIndex)
{
    return (nmhGetNextIndexMplsOutSegmentTable (pMplsOutSegmentIndex,
                                                pNextMplsOutSegmentIndex));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsOutSegmentDirection
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                retValFsMplsOutSegmentDirection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsOutSegmentDirection (tSNMP_OCTET_STRING_TYPE * pMplsOutSegmentIndex,
                                 INT4 *pi4RetValFsMplsOutSegmentDirection)
{
    UINT4               u4Index;
    tOutSegment        *pOutSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhGetFsMplsOutSegmentDirection :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    *pi4RetValFsMplsOutSegmentDirection =
        (INT4) pOutSegment->GmplsOutSegment.OutSegmentDirection;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMplsOutSegmentDirection
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                setValFsMplsOutSegmentDirection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsOutSegmentDirection (tSNMP_OCTET_STRING_TYPE * pMplsOutSegmentIndex,
                                 INT4 i4SetValFsMplsOutSegmentDirection)
{
    UINT4               u4Index;
    tOutSegment        *pOutSegment = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);

    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n nmhSetFsMplsOutSegmentDirection :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    pOutSegment->GmplsOutSegment.OutSegmentDirection =
        (eDirection) i4SetValFsMplsOutSegmentDirection;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMplsOutSegmentDirection
 Input       :  The Indices
                MplsOutSegmentIndex

                The Object 
                testValFsMplsOutSegmentDirection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsOutSegmentDirection (UINT4 *pu4ErrorCode,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pMplsOutSegmentIndex,
                                    INT4 i4TestValFsMplsOutSegmentDirection)
{
    UINT4               u4Index;
    tOutSegment        *pOutSegment = NULL;
    tXcEntry           *pXcEntry = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_OCTETSTRING_TO_INTEGER (pMplsOutSegmentIndex, u4Index);

    if ((u4Index == 0) || (u4Index > gu4MplsDbMaxOutSegEntries))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMplsOutSegmentDirection != MPLS_DIRECTION_FORWARD) &&
        (i4TestValFsMplsOutSegmentDirection != MPLS_DIRECTION_REVERSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhTestv2FsMplsOutSegmentDirection :"
                   "Indexed Entry Unavailable \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (OUTSEGMENT_ROW_STATUS (pOutSegment) == LSR_ACTIVE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "\r\n nmhTestv2FsMplsOutSegmentDirection :"
                   "Cant Modify Objects \r\n");
        MPLS_CMN_UNLOCK ();
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    /* If out-segment is associated to P2MP tunnel, block label 
     * configuration in reverse direction */
    pXcEntry = (tXcEntry *) OUTSEGMENT_XC_INDEX (pOutSegment);
    if ((pXcEntry != NULL) && (XC_TNL_TBL_PTR (pXcEntry) != NULL))
    {
        pTeTnlInfo = XC_TNL_TBL_PTR (pXcEntry);
        if ((TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_P2MP)
            && (i4TestValFsMplsOutSegmentDirection == MPLS_DIRECTION_REVERSE))
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "\r\n nmhTestv2FsMplsOutSegmentDirection : "
                       "Configuring the Out-segment direction of P2MP tunnel "
                       "as reverse is not applicable. \r\n");
            MPLS_CMN_UNLOCK ();
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsOutSegmentTable
 Input       :  The Indices
                MplsOutSegmentIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsOutSegmentTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsLsrRfc6428CompatibleCodePoint
 Input       :  The Indices

                The Object 
                retValFsMplsLsrRfc6428CompatibleCodePoint
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLsrRfc6428CompatibleCodePoint (INT4
                                           *pi4RetValFsMplsLsrRfc6428CompatibleCodePoint)
{
    *pi4RetValFsMplsLsrRfc6428CompatibleCodePoint
        = (INT4) gbRfc6428CompatibleCodePoint;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMplsLsrRfc6428CompatibleCodePoint
 Input       :  The Indices

                The Object 
                setValFsMplsLsrRfc6428CompatibleCodePoint
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsLsrRfc6428CompatibleCodePoint (INT4
                                           i4SetValFsMplsLsrRfc6428CompatibleCodePoint)
{
    tMplsAchChannelType MplsAchChannelType;

    MEMSET (&MplsAchChannelType, 0, sizeof (tMplsAchChannelType));

    MPLS_CMN_LOCK ();

    if (gbRfc6428CompatibleCodePoint ==
        (BOOL1) i4SetValFsMplsLsrRfc6428CompatibleCodePoint)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }

    MplsAchChannelType.u2AchChnlTypeCcIpv4 = MPLS_ACH_CHANNEL_DEF_CC_IPV4;
    MplsAchChannelType.u2AchChnlTypeCcIpv6 = MPLS_ACH_CHANNEL_DEF_CC_IPV6;

    gbRfc6428CompatibleCodePoint
        = (BOOL1) i4SetValFsMplsLsrRfc6428CompatibleCodePoint;

    if (gbRfc6428CompatibleCodePoint == MPLS_SNMP_TRUE)
    {
        MplsAchChannelType.u2AchChnlTypeCcBfd = MPLS_ACH_CHANNEL_DEF_CC_BFD_NEW;
        MplsAchChannelType.u2AchChnlTypeCvBfd = MPLS_ACH_CHANNEL_DEF_CV_BFD_NEW;
        MplsAchChannelType.u2AchChnlTypeCvIpv4 =
            MPLS_ACH_CHANNEL_DEF_CV_IPV4_NEW;
        MplsAchChannelType.u2AchChnlTypeCvIpv6 =
            MPLS_ACH_CHANNEL_DEF_CV_IPV6_NEW;
    }
    else
    {
        MplsAchChannelType.u2AchChnlTypeCcBfd = MPLS_ACH_CHANNEL_DEF_CC_BFD;
        MplsAchChannelType.u2AchChnlTypeCvBfd = MPLS_ACH_CHANNEL_DEF_CV_BFD;
        MplsAchChannelType.u2AchChnlTypeCvIpv4 = MPLS_ACH_CHANNEL_DEF_CV_IPV4;
        MplsAchChannelType.u2AchChnlTypeCvIpv6 = MPLS_ACH_CHANNEL_DEF_CV_IPV6;
    }

    MPLS_CMN_UNLOCK ();

    MplsApiUpdateChannelTypes (&MplsAchChannelType, TRUE);

#ifdef BFD_WANTED
    BfdApiUpdateChannelTypes (&MplsAchChannelType, TRUE);
#endif

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMplsLsrRfc6428CompatibleCodePoint
 Input       :  The Indices

                The Object 
                testValFsMplsLsrRfc6428CompatibleCodePoint
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsLsrRfc6428CompatibleCodePoint (UINT4 *pu4ErrorCode,
                                              INT4
                                              i4TestValFsMplsLsrRfc6428CompatibleCodePoint)
{
    if ((i4TestValFsMplsLsrRfc6428CompatibleCodePoint != MPLS_SNMP_FALSE) &&
        (i4TestValFsMplsLsrRfc6428CompatibleCodePoint != MPLS_SNMP_TRUE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsLsrRfc6428CompatibleCodePoint
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsLsrRfc6428CompatibleCodePoint (UINT4 *pu4ErrorCode,
                                             tSnmpIndexList * pSnmpIndexList,
                                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);

    return SNMP_SUCCESS;
}
