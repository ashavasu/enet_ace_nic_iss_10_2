#include "mplsdbinc.h"

/* $Id: mplsdbgt.c,v 1.2 2014/12/12 11:56:43 siva Exp $ */

/****************************************************************************
 Function    :  MplsDbGetAllGmplsInterfaceTable
 Description :  This function gets GMPLS Interface Table MIB objects                
 Input       :  i4MplsInterfaceIndex - MPLS Interface Index
 Output      :  pGetMplsIfEntry      - Pointer to MPLS IF Entry
 Returns     :  SNMP_SUCCESS / SNMP_FAILURE
****************************************************************************/
INT1
MplsDbGetAllGmplsInterfaceTable (INT4 i4MplsInterfaceIndex,
                                 tMplsIfEntry *pGetMplsIfEntry)
{
    tMplsIfEntry *pMplsIfEntry = NULL;

    pMplsIfEntry = MplsGetIfTableEntry ((UINT4)i4MplsInterfaceIndex);

    if (pMplsIfEntry == NULL)
    {
        return SNMP_FAILURE;
    }

    pGetMplsIfEntry->GmplsIfEntry.u1SignalingCaps
        = pMplsIfEntry->GmplsIfEntry.u1SignalingCaps;
    
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  MplsDbGetAllGmplsInSegmentTable
 Description :  This function gets GMPLS InSegment Table MIB objects                
 Input       :  u4Index              - InSegment Index
 Output      :  pGetInSegEntry       - Pointer to Insegment Entry
 Returns     :  SNMP_SUCCESS / SNMP_FAILURE
****************************************************************************/
INT1
MplsDbGetAllGmplsInSegmentTable (UINT4 u4Index, tInSegment *pGetInSegEntry)
{
    tInSegment  *pInSegment = NULL;
    
    pInSegment = MplsGetInSegmentTableEntry (u4Index);

    if (pInSegment == NULL)
    {
        return SNMP_FAILURE;
    }
    
    pGetInSegEntry->GmplsInSegment.InSegmentDirection
        = pInSegment->GmplsInSegment.InSegmentDirection;
    
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  MplsDbGetAllGmplsOutSegmentTable
 Description :  This function gets GMPLS OutSegment Table MIB objects                
 Input       :  u4Index              - OutSegment Index
 Output      :  pGetOutSegEntry      - Pointer to Outsegment Entry
 Returns     :  SNMP_SUCCESS / SNMP_FAILURE
****************************************************************************/

INT1
MplsDbGetAllGmplsOutSegmentTable (UINT4 u4Index, tOutSegment *pGetOutSegEntry)
{
    tOutSegment  *pOutSegment = NULL;
    
    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);
    
    if (pOutSegment == NULL)
    {
        return SNMP_FAILURE;
    }
    
    pGetOutSegEntry->GmplsOutSegment.OutSegmentDirection
        = pOutSegment->GmplsOutSegment.OutSegmentDirection;

    return SNMP_SUCCESS;
}
