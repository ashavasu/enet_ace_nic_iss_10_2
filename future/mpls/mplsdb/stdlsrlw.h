/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdlsrlw.h,v 1.5 2008/08/20 15:15:07 iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for MplsInterfaceTable. */
INT1
nmhValidateIndexInstanceMplsInterfaceTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for MplsInterfaceTable  */

INT1
nmhGetFirstIndexMplsInterfaceTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsInterfaceTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsInterfaceLabelMinIn ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetMplsInterfaceLabelMaxIn ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetMplsInterfaceLabelMinOut ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetMplsInterfaceLabelMaxOut ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetMplsInterfaceTotalBandwidth ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetMplsInterfaceAvailableBandwidth ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetMplsInterfaceLabelParticipationType ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for MplsInterfacePerfTable. */
INT1
nmhValidateIndexInstanceMplsInterfacePerfTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for MplsInterfacePerfTable  */

INT1
nmhGetFirstIndexMplsInterfacePerfTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsInterfacePerfTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsInterfacePerfInLabelsInUse ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetMplsInterfacePerfInLabelLookupFailures ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetMplsInterfacePerfOutLabelsInUse ARG_LIST((INT4 ,UINT4 *));

INT1
nmhGetMplsInterfacePerfOutFragmentedPkts ARG_LIST((INT4 ,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsInSegmentIndexNext ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for MplsInSegmentTable. */
INT1
nmhValidateIndexInstanceMplsInSegmentTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for MplsInSegmentTable  */

INT1
nmhGetFirstIndexMplsInSegmentTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsInSegmentTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsInSegmentInterface ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetMplsInSegmentLabel ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetMplsInSegmentLabelPtr ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OID_TYPE * ));

INT1
nmhGetMplsInSegmentNPop ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetMplsInSegmentAddrFamily ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetMplsInSegmentXCIndex ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMplsInSegmentOwner ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetMplsInSegmentTrafficParamPtr ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OID_TYPE * ));

INT1
nmhGetMplsInSegmentRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetMplsInSegmentStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsInSegmentInterface ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetMplsInSegmentLabel ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhSetMplsInSegmentLabelPtr ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OID_TYPE *));

INT1
nmhSetMplsInSegmentNPop ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetMplsInSegmentAddrFamily ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetMplsInSegmentTrafficParamPtr ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OID_TYPE *));

INT1
nmhSetMplsInSegmentRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetMplsInSegmentStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsInSegmentInterface ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2MplsInSegmentLabel ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhTestv2MplsInSegmentLabelPtr ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OID_TYPE *));

INT1
nmhTestv2MplsInSegmentNPop ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2MplsInSegmentAddrFamily ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2MplsInSegmentTrafficParamPtr ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OID_TYPE *));

INT1
nmhTestv2MplsInSegmentRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2MplsInSegmentStorageType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MplsInSegmentTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for MplsInSegmentPerfTable. */
INT1
nmhValidateIndexInstanceMplsInSegmentPerfTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for MplsInSegmentPerfTable  */

INT1
nmhGetFirstIndexMplsInSegmentPerfTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsInSegmentPerfTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsInSegmentPerfOctets ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetMplsInSegmentPerfPackets ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetMplsInSegmentPerfErrors ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetMplsInSegmentPerfDiscards ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetMplsInSegmentPerfHCOctets ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetMplsInSegmentPerfDiscontinuityTime ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsOutSegmentIndexNext ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for MplsOutSegmentTable. */
INT1
nmhValidateIndexInstanceMplsOutSegmentTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for MplsOutSegmentTable  */

INT1
nmhGetFirstIndexMplsOutSegmentTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsOutSegmentTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsOutSegmentInterface ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetMplsOutSegmentPushTopLabel ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetMplsOutSegmentTopLabel ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetMplsOutSegmentTopLabelPtr ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OID_TYPE * ));

INT1
nmhGetMplsOutSegmentNextHopAddrType ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetMplsOutSegmentNextHopAddr ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMplsOutSegmentXCIndex ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMplsOutSegmentOwner ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetMplsOutSegmentTrafficParamPtr ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OID_TYPE * ));

INT1
nmhGetMplsOutSegmentRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetMplsOutSegmentStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsOutSegmentInterface ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetMplsOutSegmentPushTopLabel ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetMplsOutSegmentTopLabel ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhSetMplsOutSegmentTopLabelPtr ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OID_TYPE *));

INT1
nmhSetMplsOutSegmentNextHopAddrType ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetMplsOutSegmentNextHopAddr ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetMplsOutSegmentTrafficParamPtr ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OID_TYPE *));

INT1
nmhSetMplsOutSegmentRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetMplsOutSegmentStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsOutSegmentInterface ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2MplsOutSegmentPushTopLabel ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2MplsOutSegmentTopLabel ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhTestv2MplsOutSegmentTopLabelPtr ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OID_TYPE *));

INT1
nmhTestv2MplsOutSegmentNextHopAddrType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2MplsOutSegmentNextHopAddr ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2MplsOutSegmentTrafficParamPtr ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OID_TYPE *));

INT1
nmhTestv2MplsOutSegmentRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2MplsOutSegmentStorageType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MplsOutSegmentTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for MplsOutSegmentPerfTable. */
INT1
nmhValidateIndexInstanceMplsOutSegmentPerfTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for MplsOutSegmentPerfTable  */

INT1
nmhGetFirstIndexMplsOutSegmentPerfTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsOutSegmentPerfTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsOutSegmentPerfOctets ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetMplsOutSegmentPerfPackets ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetMplsOutSegmentPerfErrors ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetMplsOutSegmentPerfDiscards ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetMplsOutSegmentPerfHCOctets ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetMplsOutSegmentPerfDiscontinuityTime ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsXCIndexNext ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for MplsXCTable. */
INT1
nmhValidateIndexInstanceMplsXCTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for MplsXCTable  */

INT1
nmhGetFirstIndexMplsXCTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsXCTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsXCLspId ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhGetMplsXCLabelStackIndex ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMplsXCOwner ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetMplsXCRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetMplsXCStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetMplsXCAdminStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetMplsXCOperStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsXCLspId ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetMplsXCLabelStackIndex ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetMplsXCRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetMplsXCStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetMplsXCAdminStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsXCLspId ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE*));

INT1
nmhTestv2MplsXCLabelStackIndex ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2MplsXCRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2MplsXCStorageType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2MplsXCAdminStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MplsXCTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsMaxLabelStackDepth ARG_LIST((UINT4 *));

INT1
nmhGetMplsLabelStackIndexNext ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for MplsLabelStackTable. */
INT1
nmhValidateIndexInstanceMplsLabelStackTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for MplsLabelStackTable  */

INT1
nmhGetFirstIndexMplsLabelStackTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *  , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsLabelStackTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsLabelStackLabel ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,UINT4 *));

INT1
nmhGetMplsLabelStackLabelPtr ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetMplsLabelStackRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

INT1
nmhGetMplsLabelStackStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsLabelStackLabel ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhSetMplsLabelStackLabelPtr ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetMplsLabelStackRowStatus ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhSetMplsLabelStackStorageType ARG_LIST((tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsLabelStackLabel ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,UINT4 ));

INT1
nmhTestv2MplsLabelStackLabelPtr ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2MplsLabelStackRowStatus ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

INT1
nmhTestv2MplsLabelStackStorageType ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MplsLabelStackTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for MplsInSegmentMapTable. */
INT1
nmhValidateIndexInstanceMplsInSegmentMapTable ARG_LIST((INT4  , UINT4  , tSNMP_OID_TYPE *));

/* Proto Type for Low Level GET FIRST fn for MplsInSegmentMapTable  */

INT1
nmhGetFirstIndexMplsInSegmentMapTable ARG_LIST((INT4 * , UINT4 * , tSNMP_OID_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsInSegmentMapTable ARG_LIST((INT4 , INT4 * , UINT4 , UINT4 * , tSNMP_OID_TYPE *, tSNMP_OID_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsInSegmentMapIndex ARG_LIST((INT4  , UINT4  , tSNMP_OID_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsXCNotificationsEnable ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsXCNotificationsEnable ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsXCNotificationsEnable ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MplsXCNotificationsEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
