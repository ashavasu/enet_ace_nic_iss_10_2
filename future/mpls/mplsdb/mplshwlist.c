/********************************************************************
 *** Copyright (C) 2014 Aricent Inc . All Rights Reserved
 ***
 *** $Id: mplshwlist.c,v 1.8 2015/11/30 06:25:58 siva Exp $
 ***
 *** Description: Files contails hw list API 
 *** NOTE: This file should included in release packaging.
 *** ***********************************************************************/


#include "mplsdbinc.h"
#include "mplcmndb.h"
#include "mplsnp.h"
#include "ipnpwr.h"
#include "ipv6npwr.h"
#include "ipnp.h"
#include "ldpext.h"
#include "l2vpextn.h"
#include "mplsutil.h"
#include "mplsprot.h"
#include "rtm.h"
#include "mplshwlist.h"
#include "mplsred.h"

#include "ip6np.h"
#include "ipv6npwr.h"
#include "ip6util.h"
#include "ipv6.h"

struct rbtree  *gpFTNHwListRBTree;
struct rbtree  *gpILMHwListRBTree;

/*** Global Structures**/
 tMplsHwL3FTNInfo       MplsHwL3FTNInfo;
 tMplsHwL3FTNInfo       MplsHwGetL3FTNInfo;
 tMplsHwIlmInfo         MplsGetHwIlmInfo;

#if defined(VPLS_GR_WANTED) || defined(LDP_GR_WANTED)
tRBTree             gMplsL2VpnPwHwListTree;
tRBTree             gMplsL2VpnVplsHwListTree;
static INT4
MplsL2vpnRbTreeCompFunc (tRBElem * pRBElem1,tRBElem * pRBElem2);
static INT4
MplsL2vpnVplsRbTreeCompFunc (tRBElem * pRBElem1,tRBElem * pRBElem2);
#endif
/************************************************************************
 *  Function Name   : MplsHwListInit
 *  Description     : Function to Init the FTN and ILM Hw List
 *  Input           : None
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE
 ************************************************************************/
INT4
MplsHwListInit (VOID)
{
    gpFTNHwListRBTree =
                 RBTreeCreateEmbedded (0, MplsHwListRbTreeFTNHwListCmpFunc);
    if(gpFTNHwListRBTree == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "FTN Hw List RB Tree Create Failed \t\n");
        return MPLS_FAILURE;
    }

    gpILMHwListRBTree =
                RBTreeCreateEmbedded (0, MplsHwListRbTreeILMHwListCmpFunc);
    if(gpILMHwListRBTree == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "ILM Hw List RB Tree Create Failed \t\n");
        return MPLS_FAILURE;
    }

    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsHwListRbTreeFTNHwListCmpFunc
 *  Description     : RBTree Compare function for Mpls Ftn HW List entries
                      Key for the RB-tree: Address Type, Prefix Len, Prefix
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0
 ************************************************************************/
INT4
MplsHwListRbTreeFTNHwListCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tMplsIpAddress       Addr1 = ((tFTNHwListEntry *) pRBElem1)->Fec;
    tMplsIpAddress       Addr2 = ((tFTNHwListEntry *) pRBElem2)->Fec;

    INT4                 i4AddressType1 = ((tFTNHwListEntry *) pRBElem1)->Fec.i4IpAddrType;
    INT4                 i4AddressType2 = ((tFTNHwListEntry *) pRBElem2)->Fec.i4IpAddrType;

    UINT1                u1PrefixLen1 = ((tFTNHwListEntry *) pRBElem1)->u1FecPrefixLen;
    UINT1                u1PrefixLen2 = ((tFTNHwListEntry *) pRBElem2)->u1FecPrefixLen;

    if(i4AddressType1 > i4AddressType2)
    {
        return 1;
    }
    else if (i4AddressType1 < i4AddressType2)
    {
        return -1;
    }

    if (u1PrefixLen1 > u1PrefixLen2)
    {
        return 1;
    }
    else if (u1PrefixLen1 < u1PrefixLen2)
    {
        return -1;
    }

    /* At this point, the Address types are same, hence compare the prefix values */
    if(i4AddressType1 == MPLS_IPV6_ADDR_TYPE)
    {
        /* Compare 16 bytes of the prefix */
        if (MEMCMP (&Addr1.IpAddress, &Addr2.IpAddress, 
                   IPV6_ADDR_LENGTH) > 0)
        {
            return 1;
        }
        else if (MEMCMP (&Addr1.IpAddress, &Addr2.IpAddress,
                 IPV6_ADDR_LENGTH) < 0)
        {
            return -1;
        }
    }

    else if(i4AddressType1 == MPLS_IPV4_ADDR_TYPE)
    {
        /* Compare 4 bytes of the prefix */
        if(MEMCMP(Addr1.IpAddress.au1Ipv4Addr,
              Addr2.IpAddress.au1Ipv4Addr, IPV4_ADDR_LENGTH) > 0)
        {
            return 1;
        }
        else if (MEMCMP(Addr1.IpAddress.au1Ipv4Addr,
              Addr2.IpAddress.au1Ipv4Addr, IPV4_ADDR_LENGTH) < 0)
        {
            return -1;    
        }
    }
    return 0;
}

/************************************************************************
 *  Function Name   : MplsHwListRbTreeILMHwListCmpFunc
 *  Description     : RBTree Compare function for Mpls ILM HW List entries
                      Key For the RB-Tree: Address Tye, Prefix Len, Prefix, Interface
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0
 ************************************************************************/

INT4
MplsHwListRbTreeILMHwListCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    UINT4                u4L3Intf1 = ((tILMHwListEntry *)(pRBElem1))->u4InL3Intf;
    UINT4                u4L3Intf2 = ((tILMHwListEntry *)(pRBElem2))->u4InL3Intf;

    tMplsIpAddress       Addr1 = ((tILMHwListEntry *) pRBElem1)->Fec;
    tMplsIpAddress       Addr2 = ((tILMHwListEntry *) pRBElem2)->Fec;

    INT4                 i4AddressType1 = ((tILMHwListEntry *) pRBElem1)->Fec.i4IpAddrType;
    INT4                 i4AddressType2 = ((tILMHwListEntry *) pRBElem2)->Fec.i4IpAddrType;

    UINT1                u1PrefixLen1 = ((tILMHwListEntry *) pRBElem1)->u1FecPrefixLen;
    UINT1                u1PrefixLen2 = ((tILMHwListEntry *) pRBElem2)->u1FecPrefixLen;

    uLabel               InLabel1 = ((tILMHwListEntry *) pRBElem1)->InLabel;
    uLabel               InLabel2 = ((tILMHwListEntry *) pRBElem2)->InLabel; 

    if(i4AddressType1 > i4AddressType2)
    {
        return 1;
    }
    else if (i4AddressType1 < i4AddressType2)
    {
        return -1;
    }

    if (u1PrefixLen1 > u1PrefixLen2)
    {
        return 1;
    }
    else if (u1PrefixLen1 < u1PrefixLen2)
    {
        return -1;
    }

    /* At this point, the Address types are same, hence compare the prefix values */
    if(i4AddressType1 == MPLS_IPV6_ADDR_TYPE)
    {
        /* Compare 16 bytes of the prefix */
        if (MEMCMP (&Addr1.IpAddress, &Addr2.IpAddress, 
                    IPV6_ADDR_LENGTH) > 0)
        {
            return 1;
        }
        else if (MEMCMP (&Addr1.IpAddress, &Addr2.IpAddress,
                 IPV6_ADDR_LENGTH) < 0)
        {
            return -1;
        }
    }

    else if(i4AddressType1 == MPLS_IPV4_ADDR_TYPE)
    {
        /* Compare 4 bytes of the prefix */
        if(MEMCMP(Addr1.IpAddress.au1Ipv4Addr,
              Addr2.IpAddress.au1Ipv4Addr, IPV4_ADDR_LENGTH) > 0)
        {
            return 1;
        }
        else if (MEMCMP(Addr1.IpAddress.au1Ipv4Addr,
                    Addr2.IpAddress.au1Ipv4Addr, IPV4_ADDR_LENGTH) < 0)
        {
            return -1;
        }
    }

    if (u4L3Intf1 > u4L3Intf2)
    {
        return 1;
    }
    else if (u4L3Intf1 < u4L3Intf2)
    {
        return -1;
    }

    if (MEMCMP (&InLabel1, &InLabel2, sizeof (uLabel)) > 0)
    {
        return 1;
    }
    else if (MEMCMP (&InLabel1, &InLabel2, sizeof (uLabel)) < 0)
    {
        return -1;
    }
    return 0;
}

/************************************************************************
 *  Function Name   : MplsHwListCreateFTNHwListEntry
 *  Description     : Function to allocate memory for the FTN Hw List entry
 *  Input           : NONE
 *  Output          : NONE
 *  Returns         : pFTNHwListEntry
 ************************************************************************/
tFTNHwListEntry *
MplsHwListCreateFTNHwListEntry ()
{
    tFTNHwListEntry     *pFTNHwListEntry = NULL;

    pFTNHwListEntry = (tFTNHwListEntry *)
                      MemAllocMemBlk (MPLS_HW_LIST_FTN_POOL_ID);
  
    if (pFTNHwListEntry != NULL)
    {
        MEMSET (pFTNHwListEntry, 0, sizeof (tFTNHwListEntry));
    }

    return pFTNHwListEntry;
}

/************************************************************************
 *  Function Name   : MplsHwListCreateILMHwListEntry
 *  Description     : Function to allocate memory for the ILM Hw List entry
 *  Input           : NONE
 *  Output          : NONE
 *  Returns         : pILMHwListEntry
 ************************************************************************/
tILMHwListEntry *
MplsHwListCreateILMHwListEntry ()
{
    tILMHwListEntry     *pILMHwListEntry = NULL;

    pILMHwListEntry = (tILMHwListEntry *)
                       MemAllocMemBlk (MPLS_HW_LIST_ILM_POOL_ID);

    if (pILMHwListEntry != NULL)
    {
        MEMSET (pILMHwListEntry, 0, sizeof (tILMHwListEntry));
    }

    return pILMHwListEntry;
}



/************************************************************************
 *  Function Name   : MplsHwListInitFTNHwListEntry
 *  Description     : Function to Init the FTN Hw list entry from the FTN entry
                      Xc Entry and L3 interface
 *  Input           : pFTNHwListEntry: FTN Hw List Entry
                      pFtnEntry: FTN Entry
                      pXcEntry: Xc Entry
                      u4L3Intf: L3 interface
 *  Output          : NONE
 *  Returns         : MPLS_SUCCESS/ MPLS_FAILURE
 ************************************************************************/
INT4
MplsHwListInitFTNHwListEntry (tFTNHwListEntry *pFTNHwListEntry,
        tFtnEntry *pFtnEntry,
        tXcEntry *pXcEntry, UINT4 u4L3Intf)
{

    UINT4        u4NetAddr = 0;
    UINT4        u4NetMask = 0;
#ifdef MPLS_IPV6_WANTED
    tIp6Addr     Ipv6NetAddr;
    tIp6Addr     Ipv6NetMask;
#endif

#ifdef MPLS_IPV6_WANTED
    MEMSET(&Ipv6NetAddr,0,sizeof(tIp6Addr));
    MEMSET(&Ipv6NetMask,0,sizeof(tIp6Addr));
#endif

    if (pXcEntry->pOutIndex == NULL)
    {
        return MPLS_FAILURE;
    }
    MPLS_FTN_HW_LIST_FEC_ADDR_TYPE (pFTNHwListEntry) =
        pFtnEntry->u1AddrType;

    MPLS_FTN_HW_LIST_NEXT_HOP_ADDR_TYPE (pFTNHwListEntry) =
    pXcEntry->pOutIndex->u1NHAddrType;

#ifdef MPLS_IPV6_WANTED
    if( MPLS_FTN_HW_LIST_FEC_ADDR_TYPE (pFTNHwListEntry) == MPLS_IPV6_ADDR_TYPE)
    {
        MEMCPY (&Ipv6NetAddr, pFtnEntry->DestAddrMin.u1_addr, IPV6_ADDR_LENGTH);
        MEMCPY (&Ipv6NetMask, pFtnEntry->DestAddrMax.u1_addr, IPV6_ADDR_LENGTH);

        MEMCPY (&MPLS_IPV6_FTN_HW_LIST_FEC (pFTNHwListEntry),
                &Ipv6NetAddr, IPV6_ADDR_LENGTH);

        MPLS_FTN_HW_LIST_FEC_PREFIX_LEN (pFTNHwListEntry) = 
                    MplsGetIpv6Subnetmasklen(Ipv6NetMask.u1_addr);

        MEMCPY (&MPLS_IPV6_FTN_HW_LIST_NEXT_HOP (pFTNHwListEntry),
                &(pXcEntry->pOutIndex->NHAddr.u1_addr),IPV6_ADDR_LENGTH);
    }
    else
#endif
    {
        MEMCPY (&u4NetAddr, pFtnEntry->DestAddrMin.u4_addr, IPV4_ADDR_LENGTH);
        MEMCPY (&u4NetMask, pFtnEntry->DestAddrMax.u4_addr, IPV4_ADDR_LENGTH);

        MEMCPY (MPLS_FTN_HW_LIST_FEC (pFTNHwListEntry),
                &u4NetAddr, IPV4_ADDR_LENGTH);

        MPLS_GET_PRFX_LEN_FROM_MASK (u4NetMask,
                MPLS_FTN_HW_LIST_FEC_PREFIX_LEN (pFTNHwListEntry));

        MEMCPY (MPLS_FTN_HW_LIST_NEXT_HOP (pFTNHwListEntry),
                &(pXcEntry->pOutIndex->NHAddr.u4_addr[0]), IPV4_ADDR_LENGTH);
    }

    MEMCPY (&MPLS_FTN_HW_LIST_LABEL (pFTNHwListEntry),
            &(pXcEntry->pOutIndex->u4Label), sizeof(uLabel));

    MPLS_FTN_HW_LIST_OUT_TNL_INTF (pFTNHwListEntry) =
        pXcEntry->pOutIndex->u4IfIndex;

    MPLS_FTN_HW_LIST_OUT_L3_INTF (pFTNHwListEntry) = u4L3Intf;

    MPLS_FTN_SET_HW_LIST_NP_BIT (MPLS_FTN_HW_LIST_NP_STATUS (pFTNHwListEntry),
            NPAPI_FTN_CALLED);

    MPLS_FTN_HW_LIST_STALE_STATUS (pFTNHwListEntry) = MPLS_FALSE;

    /* Currently Hw List entries are added only for NON-TE LSP's */
    /* If the owner is LDP => the entry is dynamic else the entry
     * is static */
    if (pXcEntry->pOutIndex->u1Owner == MPLS_OWNER_LDP)
    {
        MPLS_FTN_HW_LIST_IS_STATIC (pFTNHwListEntry) = MPLS_FALSE;
    }
    else
    {
        MPLS_FTN_HW_LIST_IS_STATIC (pFTNHwListEntry) = MPLS_TRUE;
    }
    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsHwListInitILMHwListEntry
 *  Description     : Function to Init the ILM Hw list entry from the In
                      Segment Entry, Xc Entry.
 *  Input           : pILMHwListEntry: ILM Hw List Entry
                      pInSegment: InSegment Entry
                      pXcEntry: Xc Entry
 *  Output          : NONE
 *  Returns         : MPLS_SUCCESS/ MPLS_FAILURE
 ************************************************************************/

INT4
MplsHwListInitILMHwListEntry (tILMHwListEntry *pILMHwListEntry,
                        tInSegment *pInSegment,
                        tXcEntry *pXcEntry)
{

    UINT4            u4L3Intf = 0;



    MPLS_ILM_HW_LIST_IN_L3_INTF (pILMHwListEntry) =  pInSegment->u4IfIndex;

    MEMCPY (&pILMHwListEntry->Fec,
            &pInSegment->ILMHwListFec, sizeof (tMplsIpAddress));

    MPLS_ILM_HW_LIST_FEC_PREFIX_LEN (pILMHwListEntry) =
                                                     pInSegment->u1ILMHwListPrefLen;

    MPLS_ILM_HW_LIST_IN_LABEL (pILMHwListEntry) = pInSegment->u4Label;
    MPLS_ILM_HW_LIST_STALE_STATUS (pILMHwListEntry) = MPLS_FALSE;
    MPLS_ILM_HW_LIST_NP_STATUS (pILMHwListEntry) = NPAPI_ILM_CALLED;


    /* If Out Segment is not null, Fill the OutSegment params */
    if (pXcEntry->pOutIndex != NULL)
    {
        MPLS_ILM_HW_LIST_NEXT_HOP_ADDR_TYPE (pILMHwListEntry) =
                                               pXcEntry->pOutIndex->u1NHAddrType;

#ifdef MPLS_IPV6_WANTED
	    if(MPLS_ILM_HW_LIST_NEXT_HOP_ADDR_TYPE(pILMHwListEntry) == MPLS_IPV6_ADDR_TYPE)
	    {
		    MEMCPY (&MPLS_IPV6_ILM_HW_LIST_NEXT_HOP (pILMHwListEntry),
				    pXcEntry->pOutIndex->NHAddr.u1_addr, IPV6_ADDR_LENGTH);
	    }
	    else
#endif
	    {
		    MEMCPY (MPLS_ILM_HW_LIST_NEXT_HOP (pILMHwListEntry),
				    &pXcEntry->pOutIndex->NHAddr.u4_addr[0], IPV4_ADDR_LENGTH);
	    }

        /* When the Out label is IMPLICIT-NULL, do not save the MPLS Tunnel interface
         * in the ILM Hw List Entry, since the corresponding FTN entry is not programmed
         * in the data plane and also the Tunnel interface NPAPI deletion is not
         * blocked for IMPLICIT NULL case. For Implicit NULL case a new Tunnel interface
         * will always be allocated for the corresponsing FTN entry. This interface should
         * be indifferent for ILM as well, since we do not program this in the Data plane
         * while programming FTN entry. However in the current implementation the ILM
         * data plance entry still use TUNNEL index in the out ifindex. We are not saving
         * this in the HW list for implicit NULL case */

        if (pXcEntry->pOutIndex->u4Label != MPLS_IMPLICIT_NULL_LABEL)
        {
            MPLS_ILM_HW_LIST_OUT_TNL_INTF (pILMHwListEntry) =
                                                     pXcEntry->pOutIndex->u4IfIndex;
        }
        else
        {
            MPLS_ILM_HW_LIST_OUT_TNL_INTF (pILMHwListEntry) = 0;
        }
        /* Fetch the L3 interface from the MPLS tunnel interface. */
        if (MplsGetL3Intf (pXcEntry->pOutIndex->u4IfIndex, &u4L3Intf) != MPLS_SUCCESS)
        {
            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL, "MplsInitILMHwListEntry : Fetching L3 interface failed "
                                           "for tunnel interface: %d\t\n",
                                            pXcEntry->pOutIndex->u4IfIndex);
            return MPLS_FAILURE;
        }

        MPLS_ILM_HW_LIST_OUT_L3_INTF (pILMHwListEntry) = u4L3Intf;

        MPLS_ILM_HW_LIST_SWAP_LABEL (pILMHwListEntry) =
                                     pXcEntry->pOutIndex->u4Label;
    }
  
    if (pInSegment->u1Owner == MPLS_OWNER_LDP)
    {
        MPLS_ILM_HW_LIST_IS_STATIC (pILMHwListEntry) = MPLS_FALSE;
    }
    else
    {
        MPLS_ILM_HW_LIST_IS_STATIC (pILMHwListEntry) = MPLS_TRUE;
    }

    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsHwListAddOrUpdateFTNHwListEntry
 *  Description     : Function to Add entry in the FTN Hw list,
                      if the FTN Hw List entry does not already exists.
                      If the entry exists, it updates the NP bit mask.
 *  Input           : pFTNHwListEntry
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE
 ************************************************************************/
INT4
MplsHwListAddOrUpdateFTNHwListEntry (tFTNHwListEntry *pFTNHwListEntry, 
                                     UINT4 u4NpBitMask)
{

    tFTNHwListEntry          *pAddUpdateFTNHwListEntry = NULL;

    pAddUpdateFTNHwListEntry = (tFTNHwListEntry *)
                                RBTreeGet (gpFTNHwListRBTree, pFTNHwListEntry);

    /* If FTN hw List Entry is not found, then add it to the RB-Tree */
    if (pAddUpdateFTNHwListEntry == NULL)
    {
        pAddUpdateFTNHwListEntry = MplsHwListCreateFTNHwListEntry ();
        if (pAddUpdateFTNHwListEntry != NULL)
        {
            MEMCPY (pAddUpdateFTNHwListEntry, pFTNHwListEntry, sizeof (tFTNHwListEntry));

            if (RBTreeAdd (gpFTNHwListRBTree, pAddUpdateFTNHwListEntry) == RB_FAILURE)
            {
                MPLS_DEALLOC_FTN_HW_LIST_ENTRY (pAddUpdateFTNHwListEntry);
                return MPLS_FAILURE;
            }
#ifdef LDP_HA_WANTED
            LdpRmSendFTNHwListEntry (pFTNHwListEntry, ADD_FTN);
#endif
            CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsHwListAddOrUpdateFTNHwListEntry: New FTN HW List Entry "
                                          "ADDED Successfully\n");
        }
        else
        {
            CMNDB_DBG (DEBUG_ERROR_LEVEL, "MplsHwListAddOrUpdateFTNHwListEntry: ERROR in Creating "
                                          "FTN Hw List Entry\n");
            return MPLS_FAILURE;
        }
    }
    else
    {

        /* Copying the FTN Hw list Entry at an offset so that we do not
         * corrupt the RB Tree node information */

        MEMCPY (((UINT1 *) pAddUpdateFTNHwListEntry) + sizeof (tRBNodeEmbd), 
                ((UINT1 *) pFTNHwListEntry ) + sizeof (tRBNodeEmbd), 
                sizeof (tFTNHwListEntry) - sizeof (tRBNodeEmbd));

        /* FTN Hw List Entry found, Thus update the NP Bit mask */
        MPLS_FTN_HW_LIST_NP_STATUS (pAddUpdateFTNHwListEntry) = u4NpBitMask;
        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL, "MplsHwListAddOrUpdateFTNHwListEntry: Existing  "
                                      "FTN Hw List Entry UPDATED with NP Bit = %d\n", u4NpBitMask);

#ifdef LDP_HA_WANTED
       LdpRmSendFTNHwListEntry (pAddUpdateFTNHwListEntry, UPDATE_FTN); 
#endif
    }
    return MPLS_SUCCESS;
}


/************************************************************************
 *  Function Name   : MplsHwListAddOrUpdateILMHwListEntry
 *  Description     : Function to Add entry in the ILM Hw List,
                      if the ILM Hw List entry does not already exists.
                      If the entry exists, it updates the NP bit mask.
 *  Input           : pILMHwListEntry
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE
 ************************************************************************/
INT4
MplsHwListAddOrUpdateILMHwListEntry (tILMHwListEntry *pILMHwListEntry,
                                     UINT4 u4NpBitMask)
{
    tILMHwListEntry           *pAddUpdateILMHwListEntry = NULL;
    
    pAddUpdateILMHwListEntry = (tILMHwListEntry *)
                               RBTreeGet (gpILMHwListRBTree, pILMHwListEntry);

    if (pAddUpdateILMHwListEntry == NULL)
    {
        pAddUpdateILMHwListEntry = MplsHwListCreateILMHwListEntry ();

        if (pAddUpdateILMHwListEntry != NULL)
        {
            MEMCPY (pAddUpdateILMHwListEntry, pILMHwListEntry, sizeof (tILMHwListEntry));

            if(RBTreeAdd (gpILMHwListRBTree, pAddUpdateILMHwListEntry) == RB_FAILURE)
            {
                MPLS_DEALLOC_ILM_HW_LIST_ENTRY (pAddUpdateILMHwListEntry);
                return MPLS_FAILURE;
            }

#ifdef LDP_HA_WANTED
            LdpRmSendILMHwListEntry (pILMHwListEntry, ADD_ILM);
#endif
            CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsHwListAddOrUpdateILMHwListEntry: New ILM HW List Entry "
                                          "ADDED Successfully\n");
        }
        else
        {
            CMNDB_DBG (DEBUG_ERROR_LEVEL, "MplsHwListAddOrUpdateILMHwListEntry: ERROR in creating ILM "
                                          "Hw List Entry\n");
            return MPLS_FAILURE;
        }
    }
    else
    {
        /* Copying the ILM Hw list Entry at an offset so that we do not
         * corrupt the RB Tree node information */

        MEMCPY (((UINT1 *) pAddUpdateILMHwListEntry) + sizeof (tRBNodeEmbd),
                ((UINT1 *) pILMHwListEntry ) + sizeof (tRBNodeEmbd),
                sizeof (tILMHwListEntry) - sizeof (tRBNodeEmbd));

        /* Update the NP Bit mask for the ILM hw List Entry */
        MPLS_ILM_HW_LIST_NP_STATUS (pAddUpdateILMHwListEntry) = u4NpBitMask;
        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL, "MplsHwListAddOrUpdateILMHwListEntry: Existing  "
                                      "ILM Hw List Entry UPDATED with NP bit = %d\n", u4NpBitMask);
#ifdef LDP_HA_WANTED
        LdpRmSendILMHwListEntry (pAddUpdateILMHwListEntry, UPDATE_ILM); 
#endif
    }
    return MPLS_SUCCESS;
}


/************************************************************************
 *  Function Name   : MplsHwListGetFTNHwListEntry
 *  Description     : Function to Get entry from the FTN Hw List based on the Fec
 *  Input           : Fec
 *  Output          : NONE
 *  Returns         : FTNHwListEntry or NULL
 ************************************************************************/
tFTNHwListEntry *
MplsHwListGetFTNHwListEntry (tMplsIpAddress Fec, UINT1 u1PrefixLen)
{
    tFTNHwListEntry         FTNHwListEntry;
    tFTNHwListEntry         *pFTNHwListEntry = NULL;

    MEMSET(&FTNHwListEntry, 0, sizeof(tFTNHwListEntry));
    MEMCPY (&(FTNHwListEntry.Fec), &Fec, sizeof(tMplsIpAddress));
    FTNHwListEntry.u1FecPrefixLen = u1PrefixLen;

    pFTNHwListEntry = (tFTNHwListEntry *)
                      RBTreeGet (gpFTNHwListRBTree, &FTNHwListEntry);
    return pFTNHwListEntry;

}

/************************************************************************
 *  Function Name   : MplsHwListGetILMHwListEntry
 *  Description     : Function to Get entry from the ILM Hw List based on the Fec,
                      L3 interface and In label.
 *  Input           : Fec, u4L3Intf
 *  Output          : NONE
 *  Returns         : pILMHwListEntry or NULL
 ************************************************************************/
tILMHwListEntry *
MplsHwListGetILMHwListEntry (tMplsIpAddress Fec, UINT1 u1PrefixLen, UINT4 u4L3Intf, uLabel InLabel)
{
     tILMHwListEntry          ILMHwListEntry;
     tILMHwListEntry          *pILMHwListEntry = NULL;

     MEMSET (&ILMHwListEntry, 0, sizeof(tILMHwListEntry));
     MEMCPY (&ILMHwListEntry.Fec, &Fec, sizeof (tMplsIpAddress));
     ILMHwListEntry.u1FecPrefixLen = u1PrefixLen;
     ILMHwListEntry.u4InL3Intf = u4L3Intf;
     MEMCPY (&ILMHwListEntry.InLabel, &InLabel, sizeof(uLabel)); 

     pILMHwListEntry = (tILMHwListEntry *)
                         RBTreeGet (gpILMHwListRBTree, &(ILMHwListEntry));
     return pILMHwListEntry;
}


/************************************************************************
 *  Function Name   : MplsGetILMHwListEntryFromFecAndInIntf
 *  Description     : Function to Get entry from the ILM Hw List Entry based on
                      FEC and L3 interface. This API should be used only to fetch
                      the dynamic ILM H/w list entries
 *  Input           : Fec, u4L3Intf
 *  Output          : NONE
 *  Returns         : pILMHwListEntry or NULL
 ************************************************************************/
tILMHwListEntry *
MplsGetILMHwListEntryFromFecAndInIntf (tMplsIpAddress Fec, UINT1 u1PrefixLen, UINT4 u4L3Intf)
{
     tILMHwListEntry          ILMHwListEntry;
     tILMHwListEntry          *pILMHwListEntry = NULL;

     MEMSET (&ILMHwListEntry, 0, sizeof(tILMHwListEntry));
     MEMCPY (&ILMHwListEntry.Fec, &Fec, sizeof (tMplsIpAddress));
     ILMHwListEntry.u1FecPrefixLen = u1PrefixLen;
     ILMHwListEntry.u4InL3Intf = u4L3Intf;
     MEMSET(&ILMHwListEntry.InLabel, 0, sizeof (uLabel));
     
     /* The In-Label is also the Key for the Rb-Tree 
      * However, for dynamic entries, FEC and In Interface will always 
      * be unique. For static ILM Hw List Entries FEC will be 0
      * and interface and In Label will be unique for such entries */

     pILMHwListEntry = (tILMHwListEntry *)
                         RBTreeGetNext (gpILMHwListRBTree, &(ILMHwListEntry), NULL);

     if (pILMHwListEntry != NULL)
     {
         if ((MEMCMP (&pILMHwListEntry->Fec, &Fec, sizeof (tMplsIpAddress)) == 0) &&
             (MPLS_ILM_HW_LIST_FEC_PREFIX_LEN (pILMHwListEntry) == u1PrefixLen) &&
             (MPLS_ILM_HW_LIST_IN_L3_INTF (pILMHwListEntry) == u4L3Intf))
         {
             return pILMHwListEntry;
         }
     }
     return NULL;
}

/************************************************************************
 *  Function Name   : MplsHwListDelFTNHwListEntry
 *  Description     : Function to Delete entry from the FTN Hw List.
 *  Input           : pFTNHwListEntry
 *  Output          : NONE
 *  Returns         : MPLS_SUCCESS/MPLS_FAILURE
 ************************************************************************/
INT4
MplsHwListDelFTNHwListEntry (tFTNHwListEntry *pFTNHwListEntry)
{
    if(RBTreeRemove (gpFTNHwListRBTree, pFTNHwListEntry) == RB_FAILURE)
    {
        return MPLS_FAILURE;
    }

    MPLS_DEALLOC_FTN_HW_LIST_ENTRY (pFTNHwListEntry);

#ifdef LDP_HA_WANTED
    LdpRmSendFTNHwListEntry (pFTNHwListEntry, DEL_FTN);
#endif
    return MPLS_SUCCESS;
}


/************************************************************************
 *  Function Name   : MplsHwListDelILMHwListEntry
 *  Description     : Function to Delete entry from the ILM Hw List.
 *  Input           : pILMHwListEntry
 *  Output          : NONE
 *  Returns         : MPLS_SUCCESS/MPLS_FAILURE
 ************************************************************************/
INT4
MplsHwListDelILMHwListEntry (tILMHwListEntry *pILMHwListEntry)
{
    if(RBTreeRemove (gpILMHwListRBTree, pILMHwListEntry) == RB_FAILURE)
    {
        return MPLS_FAILURE;
    }

    MPLS_DEALLOC_ILM_HW_LIST_ENTRY (pILMHwListEntry);

#ifdef LDP_HA_WANTED
     LdpRmSendILMHwListEntry (pILMHwListEntry, DEL_ILM);
#endif
    return MPLS_SUCCESS;
}



/************************************************************************
 *  Function Name   : MplsHwListGetFirstFTNHwListEntry
 *  Description     : Function to Get the first entry from the FTN hw list.
 *  Input           : NONE
 *  Output          : NONE
 *  Returns         : pFTNHwListEntry
 ************************************************************************/
tFTNHwListEntry *
MplsHwListGetFirstFTNHwListEntry ()
{
    tFTNHwListEntry     *pFTNHwListEntry = NULL;

    pFTNHwListEntry = (tFTNHwListEntry *) RBTreeGetFirst (gpFTNHwListRBTree);
    return pFTNHwListEntry;
}

/************************************************************************
 *  Function Name   : MplsHwListGetFirstILMHwListEntry
 *  Description     : Function to Get the first entry from the ILM hw list.
 *  Input           : NONE
 *  Output          : NONE
 *  Returns         : pFTNHwListEntry
 ************************************************************************/
tILMHwListEntry *
MplsHwListGetFirstILMHwListEntry ()
{
    tILMHwListEntry     *pILMHwListEntry = NULL;

    pILMHwListEntry = (tILMHwListEntry *) RBTreeGetFirst (gpILMHwListRBTree);
    return pILMHwListEntry;
}




/************************************************************************
 *  Function Name   : MplsHwListGetNextFTNHwListEntry
 *  Description     : Function to Get the next FTN Hw List entry based on the entry passed.
 *  Input           : pFTNHwListEntry
 *  Output          : NONE
 *  Returns         : pFTNHwListNextEntry
 ************************************************************************/
tFTNHwListEntry *
MplsHwListGetNextFTNHwListEntry (tFTNHwListEntry *pFTNHwListEntry)
{
    tFTNHwListEntry     *pFTNHwListNextEntry = NULL;

    pFTNHwListNextEntry = (tFTNHwListEntry *)
                            RBTreeGetNext (gpFTNHwListRBTree, pFTNHwListEntry, NULL);
    return pFTNHwListNextEntry;
}


/************************************************************************
 *  Function Name   : MplsHwListGetNextILMHwListEntry
 *  Description     : Function to Get the next ILM Hw List entry based on the entry passed.
 *  Input           : pILMHwListEntry
 *  Output          : NONE
 *  Returns         : pILMHwListNextEntry
 ************************************************************************/
tILMHwListEntry *
MplsHwListGetNextILMHwListEntry (tILMHwListEntry *pILMHwListEntry)
{
    tILMHwListEntry     *pILMHwListNextEntry = NULL;

    pILMHwListNextEntry = (tILMHwListEntry *)
                              RBTreeGetNext (gpILMHwListRBTree, pILMHwListEntry, NULL);
    return pILMHwListNextEntry;
}


/************************************************************************
 *  Function Name   : MplsHwListValidateFTNHwListEntry
 *  Description     : Function to Validate the FTN Hw List Entry.
 *  Input           : pFTNHwListEntry
                      Label
                      NextHop
 *  Output          : NONE
 *  Returns         : MPLS_TRUE/MPLS_FALSE
 ************************************************************************/
INT4
MplsHwListValidateFTNHwListEntry (tFTNHwListEntry *pFTNHwListEntry,
                            uLabel Label, tMplsIpAddress NextHop)
{

    if ((MEMCMP (&(pFTNHwListEntry->Label), &Label, sizeof (uLabel)) == 0) &&
        (MEMCMP (&NextHop, &(pFTNHwListEntry->NextHopIp), sizeof (tMplsIpAddress)) == 0))
    {
        return MPLS_TRUE;
    }

    return MPLS_FALSE;
}



/************************************************************************
 *  Function Name   : MplsHwListValidateILMHwListEntry
 *  Description     : Function to Validate the ILM Hw List Entry.
 *  Input           : pILMHwListEntry
                      OutLabel
                      NextHop
                      u4OutTnlIntf
                      u4InLabel
 *  Output          : NONE
 *  Returns         : MPLS_TRUE/MPLS_FALSE
 ************************************************************************/
INT4
MplsHwListValidateILMHwListEntry (tILMHwListEntry *pILMHwListEntry,
                            uLabel OutLabel, tMplsIpAddress NextHop,
                            UINT4 u4OutTnlIntf)
{

    if ((MEMCMP (&pILMHwListEntry->SwapLabel, &OutLabel, sizeof (uLabel))== 0) &&
        (MEMCMP (&NextHop, &pILMHwListEntry->NextHopIp, sizeof (tMplsIpAddress)) == 0) &&
        (MPLS_ILM_HW_LIST_OUT_TNL_INTF (pILMHwListEntry) == u4OutTnlIntf)) 
    {
        return MPLS_TRUE;
    }
    return MPLS_FALSE;
}


/************************************************************************
 *  Function Name   : MplsHwListGetHwILMParams
 *  Description     : Function to get the ILM Hw Params from the ILM Hw List
                      Entry
 *  Input           : pILMHwListEntry
 *  Output          : pMplsHwDelIlmParams
 *  Returns         : MPLS_SUCCESS/MPLS_FAILURE
 ************************************************************************/
INT4
MplsHwListGetHwILMParams (tILMHwListEntry *pILMHwListEntry,
                    tMplsHwIlmInfo  *pMplsHwDelIlmParams)
{
    UINT4               u4InL3If = 0;
    UINT2               u2InVlanId = 0;
    tCfaIfInfo          IfInfo;


    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsHwListGetHwILMParams  ENTRY\n");
    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

    pMplsHwDelIlmParams->InLabelList[0].u.MplsShimLabel =
                                    MPLS_ILM_HW_LIST_IN_LABEL (pILMHwListEntry);
    pMplsHwDelIlmParams->InLabelList[0].MplsLabelType =
                                              MPLS_HW_LABEL_TYPE_GENERIC;
    pMplsHwDelIlmParams->u4InL3Intf =
                                   MPLS_ILM_HW_LIST_IN_L3_INTF (pILMHwListEntry);

    if (CfaGetIfInfo (pMplsHwDelIlmParams->u4InL3Intf, &IfInfo) != CFA_SUCCESS)
    {
        CMNDB_DBG (DEBUG_ERROR_LEVEL, "MplsGetHwILMParams: "
                                      "CfaGetIfInfo Failed \r\n");
        return MPLS_FAILURE;
    }
    else
    {
        if (IfInfo.u1IfType == CFA_MPLS_TUNNEL)
        {
            /* Source Interface */
            if (CfaUtilGetIfIndexFromMplsTnlIf ((UINT2) pMplsHwDelIlmParams->u4InL3Intf,
                                               &u4InL3If, TRUE) == CFA_FAILURE)
            {
                CMNDB_DBG (DEBUG_ERROR_LEVEL, "MplsGetHwILMParams: Failed to get L3 Interface from "
                                              "MPLS Tunnel interface\n");
                return MPLS_FAILURE;
            }
            else
            {
                if (CfaGetVlanId (u4InL3If, &u2InVlanId) != CFA_SUCCESS)
                {
                    CMNDB_DBG (DEBUG_ERROR_LEVEL, "MplsGetHwILMParams: "
                                             "CfaGetVlanId Failed\r\n");
                    return MPLS_FAILURE;
                }

                pMplsHwDelIlmParams->u4InVlanId = (UINT4) u2InVlanId;
                 /* Get the IVR L3 Interface for the Tnl IF */
                pMplsHwDelIlmParams->u4InL3Intf = u4InL3If;
            }
        }
        else
        {
            if (CfaGetVlanId ((UINT2) pMplsHwDelIlmParams->u4InL3Intf,
                             &u2InVlanId) != CFA_SUCCESS)
            {
                CMNDB_DBG (DEBUG_ERROR_LEVEL, "MplsGetHwILMParams: "
                                    "CfaGetVlanId Failed\r\n");
                return MPLS_FAILURE;
            }
            pMplsHwDelIlmParams->u4InVlanId = (UINT4) u2InVlanId;
        }
    }
    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsHwListGetHwILMParams  EXIT\n");
    return MPLS_SUCCESS;
}


/************************************************************************
 *  Function Name   : MplsHwListGetHwL3FTNParams
 *  Description     : Function to get the FTN Hw Params from the FTN Hw List
 *                    Entry. This API also returns the Fec Prefix, Next Hop
                      and Net Mask from the FTN Hw List entry.
 *  Input           : pFTNHwListEntry
 *  Output          : pMplsHwL3FTNInfo
 *                    pu4FecPrefix
 *                    pu4NetMask
 *                    pNextHop
 *  Returns         : MPLS_SUCCESS/MPLS_FAILURE
 ************************************************************************/
INT4
MplsHwListGetHwL3FTNParams (tFTNHwListEntry *pFTNHwListEntry,
                            tMplsHwL3FTNInfo *pMplsHwL3FTNInfo, 
                            uGenU4Addr *pFecPrefix,
                            uGenU4Addr *pNetMask, 
                            uGenU4Addr *pNextHopGt, 
                            UINT4 *pNextHopIfIndex, 
                            UINT1 *pNextHopRtCount)
{
    UINT4              u4L3Intf = 0;


    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsHwListGetHwL3FTNParams ENTRY\n");

#ifdef MPLS_IPV6_WANTED

    if(MPLS_FTN_HW_LIST_FEC_ADDR_TYPE(pFTNHwListEntry) == MPLS_IPV6_ADDR_TYPE)
    {
	    MEMCPY (MPLS_P_IPV6_U4_ADDR(pFecPrefix), &MPLS_IPV6_FTN_HW_LIST_FEC (pFTNHwListEntry),
			    IPV6_ADDR_LENGTH);

	    MplsGetIPV6Subnetmask(MPLS_FTN_HW_LIST_FEC_PREFIX_LEN (pFTNHwListEntry),
                              MPLS_P_IPV6_U4_ADDR(pNetMask));

	    MEMCPY (MPLS_P_IPV6_U4_ADDR(pNextHopGt), &MPLS_IPV6_FTN_HW_LIST_NEXT_HOP (pFTNHwListEntry),
			    IPV6_ADDR_LENGTH);
    }
    else
#endif
    {
	    MEMCPY (&MPLS_P_IPV4_U4_ADDR(pFecPrefix), MPLS_FTN_HW_LIST_FEC (pFTNHwListEntry),
			    IPV4_ADDR_LENGTH);

	    MPLS_GET_MASK_FROM_PRFX_LEN (MPLS_FTN_HW_LIST_FEC_PREFIX_LEN (pFTNHwListEntry),
			    MPLS_P_IPV4_U4_ADDR(pNetMask));

	    MEMCPY (&MPLS_P_IPV4_U4_ADDR(pNextHopGt), MPLS_FTN_HW_LIST_NEXT_HOP (pFTNHwListEntry),
			    IPV4_ADDR_LENGTH);
    }

    *pNextHopIfIndex = MPLS_FTN_HW_LIST_OUT_L3_INTF (pFTNHwListEntry);
    *pNextHopRtCount = 1;

    pMplsHwL3FTNInfo->u4EgrL3Intf =
                    MPLS_FTN_HW_LIST_OUT_TNL_INTF (pFTNHwListEntry);

    /* Fetch the L3 interface from the MPLS tunnel interface. */
    if (MplsGetL3Intf (pMplsHwL3FTNInfo->u4EgrL3Intf, &u4L3Intf) != MPLS_SUCCESS)
    {
         CMNDB_DBG1 (DEBUG_ERROR_LEVEL, "MplsGetHwL3FTNParams(): Fetching L3 interface failed "
                                         "for tunnel interface %d\t\n", pMplsHwL3FTNInfo->u4EgrL3Intf);
         return MPLS_FAILURE;
    }

    /* The VLAN Id will get updated with valid value only if the L3 interface
     * is IVR interface. */
    if (CfaGetVlanId (u4L3Intf, &(pMplsHwL3FTNInfo->u2OutVlanId)) != CFA_SUCCESS)
    {
         CMNDB_DBG1 (DEBUG_ERROR_LEVEL, "MplsGetHwL3FTNParams() : CfaGetVlanId Failed for L3 "
                                  "interface %d\r\n", u4L3Intf);
        return MPLS_FAILURE;
    }
    
    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsHwListGetHwL3FTNParams EXIT\n");
    return MPLS_SUCCESS;
}


/************************************************************************
 *  Function Name   : MplsHwListDeleteILMEntryFromHw
 *  Description     : Function to Delete  ILM entry from the H/w using the 
                      ILM Hw List Entry
 *                    Free the reserved label.
 *                    Deletes the ILM entry from the H/w list RBTree.
 *  Input           : pILMHwListEntry
 *  Output          : NONE
 *  Returns         : MPLS_SUCCESS/MPLS_FAILURE
 ************************************************************************/

INT4
MplsHwListDeleteILMEntryFromHw (tILMHwListEntry *pILMHwListEntry, VOID *pSlotInfo)
{

    tMplsHwIlmInfo          MplsHwDelIlmParams;
    tMplsHwIlmInfo          MplsHwIlmInfo;
    INT4                    i4RetVal = MPLS_SUCCESS;
#if defined(NPAPI_WANTED)|| defined(MBSM_WANTED)
    UINT4                   u4Action = 0;
#endif
    uLabel                  Label;
#ifndef MBSM_WANTED
    UNUSED_PARAM (pSlotInfo);
#endif


    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsHwListDeleteILMEntryFromHw: ENTRY\n");
    MEMSET (&MplsHwDelIlmParams, 0, sizeof (tMplsHwIlmInfo));
    MEMSET (&MplsHwIlmInfo, 0, sizeof (tMplsHwIlmInfo));
    MEMSET (&MplsGetHwIlmInfo, 0, sizeof (tMplsHwIlmInfo));
    MEMSET (&Label, 0, sizeof (uLabel));
#ifdef MBSM_WANTED
    if (pSlotInfo != NULL)
    {
        if (FsMplsMbsmHwDeleteILM (&MplsHwDelIlmParams, u4Action, pSlotInfo) ==
            FNP_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsHwListDeleteILMEntryFromHw : FsMplsMbsmHwDeleteILM failed\t\n");
            return MPLS_FAILURE;
        } 
    }
    else
#endif
    {
#ifdef L2RED_WANTED
        if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
        {
#ifdef NPAPI_WANTED
             i4RetVal = MplsHwListGetHwILMParams (pILMHwListEntry, &MplsHwDelIlmParams);

             if (i4RetVal == MPLS_FAILURE)
             {
                  CMNDB_DBG (DEBUG_ERROR_LEVEL, "MplsHwListDeleteILMEntryFromHw: Error in Fetching ILM Hw "
                                               "Params from the ILM Hw List Entry\n");
             }
             else
             { 
                 MplsHwIlmInfo.u4InL3Intf = MplsHwDelIlmParams.u4InL3Intf;
                 MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel = MplsHwDelIlmParams.InLabelList[0].u.MplsShimLabel;
                 MplsHwIlmInfo.InLabelList[1].u.MplsShimLabel = MplsHwDelIlmParams.InLabelList[1].u.MplsShimLabel;

                 if (MplsFsMplsHwGetILM (&MplsHwIlmInfo, &MplsGetHwIlmInfo) == FNP_SUCCESS)
                 {
                     CMNDB_DBG2 (DEBUG_DEBUG_LEVEL, "MplsHwListDeleteILMEntryFromHw: ILM Found in the DATA PLANE "
                             "for the Interface: %d, Label: %d\n", MplsHwIlmInfo.u4InL3Intf,
                             MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel);

                     if (MplsFsMplsHwDeleteILM (&MplsHwDelIlmParams, u4Action) == FNP_FAILURE)
                     {
                         CMNDB_DBG (DEBUG_ERROR_LEVEL, "MplsHwListDeleteILMEntryFromHw: Failed to Delete the "
                                 "ILM Entry from H/w\n");
                         return MPLS_FAILURE;
                     }
                     CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsHwListDeleteILMEntryFromHw: ILM Deleted successfully from DATA PLANE\n");
                 }
                 else
                 {
                     CMNDB_DBG2 (DEBUG_DEBUG_LEVEL, "MplsHwListDeleteILMEntryFromHw: ILM Hw Entry Not found in "
                             "the DATA PLANE for interface: %d and Label: %d\n", MplsHwIlmInfo.u4InL3Intf,
                             MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel);
                 }
                 /* Delete the Entry from the ILM hw List */
                 i4RetVal = MplsHwListDelILMHwListEntry (pILMHwListEntry);

                 if (i4RetVal == MPLS_FAILURE)
                 {
                     CMNDB_DBG (DEBUG_ERROR_LEVEL, "MplsHwListDeleteILMEntryFromHw: Failed to Delete "
                             "the ILM Hw List Entry\n");
                 }
             }
#endif
       }
    }
    if (MPLS_ILM_HW_LIST_IS_STATIC (pILMHwListEntry) == MPLS_FALSE)
    {
        /* Post an Event to Release LDP Label */
        MEMCPY (&Label.u4GenLbl, &MPLS_ILM_HW_LIST_IN_LABEL (pILMHwListEntry), sizeof (UINT4));
        LdpGrLblRelEventHandler (Label);
    }
    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsHwListDeleteILMEntryFromHw: EXIT\n");
    return i4RetVal;
}

/************************************************************************
 *  Function Name   : MplsHwListDeleteFTNEntryFromHw
 *  Description     : Function to Delete FTN entry from the H/w using the FTN 
 *                    Hw List Entry
 *                    Delete the CFA tunnel interface and stacking.
 *                    Deletes the FTN entry from the H/w list RBTree.
 *  Input           : pFTNHwListEntry
 *  Output          : NONE
 *  Returns         : MPLS_SUCCESS/MPLS_FAILURE
 ************************************************************************/

INT4
MplsHwListDeleteFTNEntryFromHw (tFTNHwListEntry *pFTNHwListEntry, VOID *pSlotInfo)
{
    tRtInfoQueryMsg        RtQuery;
    tFsNpNextHopInfo       NextHop;
    uGenU4Addr             NextHopGt;

#ifdef NPAPI_WANTED
    UINT4                  u4NextHopIfIndex=0;
    UINT1                  u1NextHopRtCount=0;
    uGenU4Addr             NetMask;
    INT4                   i4TblFull = 0;
    UINT4                  u4VpnId = MPLS_DEF_VRF;
#endif
    tNpRtmInput            RtmNpInParam;
    tNpRtmOutput           RtmNpOutParam;
    tGenU4Addr             FecPrefix;

#ifdef MPLS_IPV6_WANTED
    tNetIpv6RtInfoQueryMsg NetIpv6RtInfoQueryMsg;
    tNetIpv6RtInfo         NetIpv6RtInfo;
    tFsNpIntInfo           IntInfo;
    tFsNpRouteInfo         RouteInfo;
    tIp6Addr               NextHopV6;
#ifdef NPAPI_WANTED
    UINT4                  u4NHType=0;
    UINT1                  u1PrefixLen=0;
#endif
#endif

#ifndef MBSM_WANTED
    UNUSED_PARAM (pSlotInfo);
#endif

#ifdef MPLS_IPV6_WANTED
    MEMSET (&NetIpv6RtInfoQueryMsg, 0, sizeof(tNetIpv6RtInfoQueryMsg));
    MEMSET (&NetIpv6RtInfo, 0, sizeof(tNetIpv6RtInfo));
    MEMSET (&IntInfo, 0, sizeof (tFsNpIntInfo));
    MEMSET (&NextHopV6, 0, sizeof (tIp6Addr));
    MEMSET (&RouteInfo, 0, sizeof(tFsNpRouteInfo));
#endif



    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsHwListDeleteStaleFTNHwListEntry: ENTRY\n");
    MEMSET (&MplsHwL3FTNInfo, 0, sizeof (tMplsHwL3FTNInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));
    MEMSET (&NextHop, 0, sizeof (tFsNpNextHopInfo));
    MEMSET (&MplsHwGetL3FTNInfo, 0, sizeof (tMplsHwL3FTNInfo));
    MEMSET (&RtmNpInParam, 0, sizeof (tNpRtmInput));
    MEMSET (&RtmNpOutParam, 0, sizeof (tNpRtmOutput));
    MEMSET (&FecPrefix, 0, sizeof(tGenU4Addr));
#ifdef NPAPI_WANTED
    MEMSET (&NetMask, 0, sizeof(uGenU4Addr));
#endif
    MEMSET (&NextHopGt, 0, sizeof(uGenU4Addr));


#ifdef MBSM_WANTED
    /* The Slot info can itself be derived from the Hw List Entry */
    if (pSlotInfo != NULL)
    {
        if (FsMplsMbsmHwDeleteL3FTN (MPLS_FTN_HW_LIST_OUT_TNL_INTF (pFTNHwListEntry), 
                    pSlotInfo) == FNP_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                    "MplsHwListDeleteFTNEntryFromHw: FsMplsMbsmHwDeleteL3FTN failed\n");
            return MPLS_FAILURE;
        }
    }
    else
#endif
    {
#ifdef L2RED_WANTED
        if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
        {
#ifdef NPAPI_WANTED
            if (MplsHwListGetHwL3FTNParams (pFTNHwListEntry, &MplsHwL3FTNInfo,
                        &FecPrefix.Addr, &NetMask, &NextHopGt, &u4NextHopIfIndex, &u1NextHopRtCount) == MPLS_FAILURE)
            {
                CMNDB_DBG (DEBUG_ERROR_LEVEL, "MplsHwListDeleteFTNEntryFromHw: "
                        "Fetching the FTN Params failed from the FTN Hw List Entry\n");
                return MPLS_FAILURE;
            }
#ifdef MPLS_IPV6_WANTED
            if(MPLS_FTN_HW_LIST_FEC_ADDR_TYPE(pFTNHwListEntry) == MPLS_IPV6_ADDR_TYPE)
            {
                u1PrefixLen = MplsGetIpv6Subnetmasklen (MPLS_IPV6_U4_ADDR(NetMask));

                /* Delete the mpls route from fast path table */
                RouteInfo.u1RouteCount = 1;
                if (MplsFsMplsHwDeleteMplsIpv6Route(VCM_DEFAULT_CONTEXT,
                            (UINT1 *) &FecPrefix.Addr.Ip6Addr,
                            u1PrefixLen,
                            (UINT1 *) &NextHopGt.Ip6Addr,
                            u4NextHopIfIndex,
                            &RouteInfo) == FNP_SUCCESS)
                {
                    /* After deleting mpls based route, restore the best route as by
                     * maintained RTM in the fast path table.
                     */
                    NetIpv6RtInfoQueryMsg.u4ContextId = VCM_DEFAULT_CONTEXT;
                    NetIpv6RtInfoQueryMsg.u1QueryFlag = RTM6_QUERIED_FOR_NEXT_HOP;
                    Ip6AddrCopy (&NetIpv6RtInfo.Ip6Dst, &(FecPrefix.Addr.Ip6Addr));
                    NetIpv6RtInfo.u1Prefixlen = u1PrefixLen;

                    if(NetIpv6GetRoute (&NetIpv6RtInfoQueryMsg,
                                &NetIpv6RtInfo)!=NETIPV6_SUCCESS)
                    {
                        CMNDB_DBG (DEBUG_DEBUG_LEVEL, " INT Exit - Route doesn't exist \n");
                        /* To discuss if failure to be returned from here.
                         * Also route delete should happen only in case it exists*/
                    }
                    else
                    {
                        /* Reference - Rtm6FillIntInfo*/
                        IntInfo.u1IfType = Ip6GetIfType (NetIpv6RtInfo.u4Index);
                        if (IntInfo.u1IfType == CFA_L3IPVLAN)
                        {
                            IntInfo.u4PhyIfIndex = NetIpv6RtInfo.u4Index;
                        }
                        if (IntInfo.u1IfType == CFA_PSEUDO_WIRE)
                        {
                            IntInfo.u4PhyIfIndex = NetIpv6RtInfo.u4Index;
                        }
                        if (IntInfo.u1IfType != IP6_ENET_INTERFACE_TYPE)
                        {
                            if (CfaGetVlanId (IntInfo.u4PhyIfIndex, &(IntInfo.u2VlanId)) ==
                                    CFA_FAILURE)
                            {
                                return MPLS_FAILURE;
                            }
                        }
                        else
                        {
                            IntInfo.u4PhyIfIndex = NetIpv6RtInfo.u4Index;
                            IntInfo.u2VlanId = 0;
                        }

                        /* check for the type before programming */
                        if (NetIpv6RtInfo.i1Type == IP6_ROUTE_TYPE_DIRECT)
                        {
                            u4NHType = NH_DIRECT;
                        }
                        else if (NetIpv6RtInfo.i1Type == IP6_ROUTE_TYPE_DISCARD)
                        {
                            /* Route type if set as discard type previously is
                             *                              * now reverted to DIRECT Type */
                            u4NHType = NH_DIRECT;
                        }
                        else if (NetIpv6RtInfo.i1Type == IP6_ROUTE_TYPE_INDIRECT)
                        {
                            u4NHType = NH_REMOTE;
                        }
                        else
                        {
                            u4NHType = NH_SENDTO_CP;
                        }

                        if (Ipv6FsNpIpv6UcRouteAdd (VCM_DEFAULT_CONTEXT,
                                    (UINT1 *) &NetIpv6RtInfo.Ip6Dst,
                                    u1PrefixLen,
                                    (UINT1 *) &NetIpv6RtInfo.NextHop,
                                    u4NHType, &IntInfo) == FNP_FAILURE)
                        {
                            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                                    " INT Exit - Fast path IPv6 best route add failed\n");
                            return MPLS_FAILURE;
                        }
                    }
                }
                else
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                            "FTN Deletion failed: MPLS Route does not exist \n");
                    /* Fast path route delete fail */
                    return MPLS_FAILURE;
                }

            }
            else
#endif
            {
                RtmNpInParam.u4CxtId = u4VpnId;
                RtmNpInParam.u4DestNet = MPLS_IPV4_U4_ADDR(FecPrefix.Addr);
                NextHop.u4NextHopGt=MPLS_IPV4_U4_ADDR(NextHopGt);
                NextHop.u4IfIndex=u4NextHopIfIndex;
                NextHop.u1RtCount=u1NextHopRtCount;

                if (IpFsNpIpv4UcGetRoute (RtmNpInParam, &RtmNpOutParam) == FNP_SUCCESS)
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsHwListDeleteFTNEntryFromHw: Route Found in DATA PLANE\n");
                    /* Delete the MPLS Route */
                    if(MplsFsNpIpv4UcDelRoute (0 /* IP_VRID */,
                                MPLS_IPV4_U4_ADDR(FecPrefix.Addr), MPLS_IPV4_U4_ADDR(NetMask),
                                NextHop, &i4TblFull) == FNP_SUCCESS)
                    {
                        MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

                        RtQuery.u4DestinationIpAddress = MPLS_IPV4_U4_ADDR(FecPrefix.Addr);
                        RtQuery.u4DestinationSubnetMask = MPLS_IPV4_U4_ADDR(NetMask);
                        RtQuery.u1QueryFlag = RTM_QUERIED_FOR_EXACT_DEST;

                        if (RtmGetAndProgrammeBestRoute (&RtQuery) == IP_FAILURE)
                        {
                            CMNDB_DBG (DEBUG_ERROR_LEVEL, "MplsHwListDeleteFTNEntryFromHw: "
                                    "FTN Deletion failed: IP best Route addition failed\n");
                        }
                        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL, "MplsHwListDeleteFTNEntryFromHw: Route Deleted Successfully "
                                "from the DATA PLANE for FEC: %x\n", MPLS_IPV4_U4_ADDR(FecPrefix.Addr));
                    }
                    else
                    {
                        CMNDB_DBG (DEBUG_ERROR_LEVEL, "MplsHwListDeleteFTNEntryFromHw: MPLS Route Deletion Failed\n");
                    }
                }
                else
                {
                    CMNDB_DBG1 (DEBUG_ERROR_LEVEL, "MplsHwListDeleteFTNEntryFromHw: Route Not found for "
                            "Prefix: %d\n",MPLS_IPV4_U4_ADDR(FecPrefix.Addr));
                }
            } 
            if (MplsFsMplsHwGetL3FTN (u4VpnId,
                        MPLS_FTN_HW_LIST_OUT_TNL_INTF (pFTNHwListEntry),
                        &MplsHwGetL3FTNInfo) == FNP_SUCCESS)
            {
                CMNDB_DBG1 (DEBUG_DEBUG_LEVEL, "MplsHwListDeleteFTNEntryFromHw: L3 FTN found in DATA PLANE for TnlItf=%d\n",MPLS_FTN_HW_LIST_OUT_TNL_INTF (pFTNHwListEntry));
                if (MplsFsMplsWpHwDeleteL3FTN (u4VpnId, &MplsHwL3FTNInfo) == FNP_FAILURE)
                {
                    CMNDB_DBG (DEBUG_ERROR_LEVEL, "MplsHwListDeleteFTNEntryFromHw: FsMplsWpHwDeleteL3FTN failed\n");
                    return MPLS_FAILURE;
                }
                CMNDB_DBG1 (DEBUG_DEBUG_LEVEL, "MplsHwListDeleteFTNEntryFromHw: L3 FTN deleted successfully "
                        "from the DATA PLANE for "
                        "TNL_IF_INDEX: %d\n",  MPLS_FTN_HW_LIST_OUT_TNL_INTF (pFTNHwListEntry)); 
            }
            else
            {
                CMNDB_DBG1 (DEBUG_DEBUG_LEVEL, "MplsHwListDeleteFTNEntryFromHw: L3 FTN Not found in "
                        "DATA PLANE for TNL_IF_INDEX: %d\n", 
                        MPLS_FTN_HW_LIST_OUT_TNL_INTF (pFTNHwListEntry));
            }
            /* Delete the FTN HW List Entry */
            if (MplsHwListDelFTNHwListEntry (pFTNHwListEntry) == MPLS_FAILURE)
            {
                CMNDB_DBG (DEBUG_ERROR_LEVEL, "MplsHwListDeleteFTNEntryFromHw: Failed to delete "
                        "the FTN Hw List Entry\n");
                return MPLS_FAILURE;
            }
#endif
        }
    }

    /*  Send Event to L2VPN */
    FecPrefix.u2AddrType = MPLS_FTN_HW_LIST_FEC_ADDR_TYPE (pFTNHwListEntry);

    MplsNonTePostEventForL2Vpn (MPLS_ZERO, &FecPrefix,
            L2VPN_MPLS_PWVC_LSP_DOWN);

    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsHwListDeleteFTNEntryFromHw: EXIT\n");
    return MPLS_SUCCESS;
}




#if defined(VPLS_GR_WANTED) || defined(LDP_GR_WANTED)
/*****************************************************************************/
/* Function     : MplsCreateRbTreeForPwHwList                                */
/* Description  : This function create a RB Tree to store PW  HW List info   */
/* Input        : None                                                       */
/* Output       : NONE                                                       */
/* Returns      : MPLS_SUCCESS/MPLS_FAILURE                                  */
/*****************************************************************************/
INT4
MplsAllocateMemoryForHwList(tMplsL2VpnPwHwList **ppMplsL2VpnPwHwList)
{

    *ppMplsL2VpnPwHwList = (tMplsL2VpnPwHwList *) MemAllocMemBlk
                            (MPLS_PW_HW_LIST_POOL_ID);

    if(NULL == *ppMplsL2VpnPwHwList)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,"Memory Allocation failed"
                "from Pool Id - MPLS_PW_HW_LIST_POOL_ID\n");
        return MPLS_FAILURE;
    }
    return MPLS_SUCCESS;
}

/*****************************************************************************/
/* Function     : MplsCreateRbTreeForPwHwList                                */
/* Description  : This function create a RB Tree to store PW  HW List info   */
/* Input        : None                                                       */
/* Output       : NONE                                                       */
/* Returns      : MPLS_SUCCESS/MPLS_FAILURE                                  */
/*****************************************************************************/
INT4
MplsCreateRbTreeForPwHwList()
{

    gMplsL2VpnPwHwListTree =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tMplsL2VpnPwHwList,
                    NextEntry),MplsL2vpnRbTreeCompFunc);

        if(gMplsL2VpnPwHwListTree == NULL)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                    "MplsCreateRbTreeForPwHwList : Failed to create RB Tree "
                    "for PW HW List.\n");
            return MPLS_FAILURE;
        }
    return MPLS_SUCCESS;
}
/*****************************************************************************/
/* Function     : MplsL2vpnRbTreeCompFunc                                    */
/* Description  : This is the RB Tree compare function for the L2VPN HW List */
/*                Indices of the PW Hw List is u4VplsIndex and u4PwIndex     */
/* Input        : pRBElem1 - Pointer to the Node 1 for comparison            */
/*                pRBElem2 - Pointer to the Node 2 for comparison            */
/* Output       : NONE                                                       */
/* Returns      : MPLS_RB_EQUAL   - if all the keys matched for both         */
/*                                  the nodes                                */
/*                MPLS_RB_LESSER - if node pRBElem1's key is less than         */
/*                                  node pRBElem2's key.                     */
/*                MPLS_RB_GREATER - if node pRBElem1's key is greater        */
/*                                  than node pRBElem2's key.                */
/*****************************************************************************/
PRIVATE INT4
MplsL2vpnRbTreeCompFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tMplsL2VpnPwHwList * pMplsL2VpnPwHwListA = NULL;
    tMplsL2VpnPwHwList * pMplsL2VpnPwHwListB = NULL;

    pMplsL2VpnPwHwListA = (tMplsL2VpnPwHwList *)pRBElem1;
    pMplsL2VpnPwHwListB = (tMplsL2VpnPwHwList *)pRBElem2;

    if(pMplsL2VpnPwHwListA->u4VplsIndex > pMplsL2VpnPwHwListB->u4VplsIndex)
    {
        return MPLS_RB_GREATER;
    }
    else if(pMplsL2VpnPwHwListA->u4VplsIndex < pMplsL2VpnPwHwListB->u4VplsIndex)
    {
        return MPLS_RB_LESSER;
    }
    if(pMplsL2VpnPwHwListA->u4PwIndex > pMplsL2VpnPwHwListB->u4PwIndex)
    {
        return MPLS_RB_GREATER;
    }
    else if(pMplsL2VpnPwHwListA->u4PwIndex < pMplsL2VpnPwHwListB->u4PwIndex)
    {
         return MPLS_RB_LESSER;
    }
    return MPLS_RB_EQUAL;
}
/*****************************************************************************/
/* Function     : MplsL2vpnHwListAdd                                         */
/* Description  : This function is used to add a entry in HW List            */
/* Input        : pMplsL2VpnPwHwList - Node which is added in HWList         */
/* Output       : NONE                                                       */
/* Returns      : MPLS_SUCCESS/MPLS_FAILURE                                  */
/*****************************************************************************/
INT4 MplsL2vpnHwListAdd(tMplsL2VpnPwHwList *pMplsL2VpnPwHwListUpdate)
{
	tMplsL2VpnPwHwList *pMplsL2VpnPwHwListEntry;

	if(MplsAllocateMemoryForHwList(&pMplsL2VpnPwHwListEntry) == MPLS_FAILURE)
	{
		return MPLS_FAILURE;
	}

	MEMCPY(pMplsL2VpnPwHwListEntry, pMplsL2VpnPwHwListUpdate, sizeof(tMplsL2VpnPwHwList));

    if(RBTreeAdd(gMplsL2VpnPwHwListTree,(tRBElem *)pMplsL2VpnPwHwListEntry)  == RB_FAILURE)
    {
        MemReleaseMemBlock (MPLS_PW_HW_LIST_POOL_ID,(UINT1 *) pMplsL2VpnPwHwListEntry);
		return MPLS_FAILURE;
    }
    return MPLS_SUCCESS;
}
/*****************************************************************************/
/* Function     : MplsL2vpnHwListUpdate                                      */
/* Description  : To Update an existing entry in HW List            	     */
/* Input        : pMplsL2VpnPwHwList - Node which is to be updated in HWList */
/* Output       : NONE                                                       */
/* Returns      : MPLS_SUCCESS/MPLS_FAILURE                                  */
/*****************************************************************************/
INT4 MplsL2vpnHwListUpdate(tMplsL2VpnPwHwList *pMplsL2VpnPwHwListUpdate)
{
	tMplsL2VpnPwHwList *pMplsL2VpnPwHwEntry = NULL;

	/*Get the Matching Entry from RBTree*/

	pMplsL2VpnPwHwEntry =(tMplsL2VpnPwHwList *)RBTreeGet(gMplsL2VpnPwHwListTree,
			(tRBElem *)pMplsL2VpnPwHwListUpdate);

	if(pMplsL2VpnPwHwEntry == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,"Fetch info from RBTree"
				"is failed\n");
		return MPLS_FAILURE;
	}
	/* In case HWList is to be ported to file -
	 * A write in file for this update will be required.
	 * Here since we are updaing RBTree, update directly through the pointer of RBTree Entry*/

    /*Since NextEntry Maintains the RBTree Node info, preserve it*/
    MEMCPY(&pMplsL2VpnPwHwListUpdate->NextEntry, &pMplsL2VpnPwHwEntry->NextEntry, sizeof(tRBNodeEmbd));
	
    /*Copy/Update Rest of the Data*/
    MEMCPY(pMplsL2VpnPwHwEntry, pMplsL2VpnPwHwListUpdate, sizeof(tMplsL2VpnPwHwList));

    return MPLS_SUCCESS;
}

/*****************************************************************************/
/* Function     : MplsL2vpnHwListDelete                                      */
/* Description  : This function is used to delete a entry from HW List       */
/*                  and release the memory                                   */
/* Input        : pMplsL2VpnPwHwList - Node which has to be deleted          */
/* Output       : NONE                                                       */
/* Returns      : MPLS_SUCCESS/MPLS_FAILURE                                  */
/*****************************************************************************/

INT4 MplsL2vpnHwListDelete(tMplsL2VpnPwHwList *pMplsL2VpnPwHwList)
{

    tMplsL2VpnPwHwList *pMplsL2VpnPwHwEntry = NULL;

    /*Search for entry in RBTree*/
	pMplsL2VpnPwHwEntry =(tMplsL2VpnPwHwList *)RBTreeGet(gMplsL2VpnPwHwListTree,
			(tRBElem *)pMplsL2VpnPwHwList);

	if(pMplsL2VpnPwHwEntry == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,"Fetch info from RBTree"
				"is failed\n");
		return MPLS_FAILURE;
	}
    /*Delete the Entry*/
	RBTreeRem (gMplsL2VpnPwHwListTree, pMplsL2VpnPwHwEntry);
    MemReleaseMemBlock (MPLS_PW_HW_LIST_POOL_ID,(UINT1 *) pMplsL2VpnPwHwEntry);
    return MPLS_SUCCESS;
}
/*****************************************************************************/
/* Function     : MplsL2vpnHwListGetFirst                                    */
/* Description  : This function is used to get the first entry from HW List  */
/* Input        : ppMplsL2VpnPwHwList - store the first entry of HW List     */
/* Output       : NONE                                                       */
/* Returns      : MPLS_SUCCESS/MPLS_FAILURE                                  */
/*****************************************************************************/

INT4 MplsL2vpnHwListGetFirst(tMplsL2VpnPwHwList *pMplsL2VpnPwHwList)
{

    tMplsL2VpnPwHwList *pMplsL2VpnPwHwEntry;

    pMplsL2VpnPwHwEntry = (tMplsL2VpnPwHwList *)RBTreeGetFirst (gMplsL2VpnPwHwListTree);

    if(pMplsL2VpnPwHwEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,"Fetch info from RBTree"
                "is failed\n");
        return MPLS_FAILURE;
    }

    MEMCPY(pMplsL2VpnPwHwList, pMplsL2VpnPwHwEntry, sizeof(tMplsL2VpnPwHwList));

    return MPLS_SUCCESS;
}
/*****************************************************************************/
/* Function     : MplsL2vpnHwListGet                                         */
/* Description  : This function is used to get a particular entry from HWList*/
/* Input        : pMplsL2VpnPwHwList - contains key information              */
/*                ppMplsL2VpnPwHwList - store the pointer of entry based on  */
/*                                      key                                  */
/* Output       : NONE                                                       */
/* Returns      : MPLS_SUCCESS/MPLS_FAILURE                                  */
/*****************************************************************************/

INT4 MplsL2vpnHwListGet(tMplsL2VpnPwHwList *pMplsL2VpnPwHwListKey,
                        tMplsL2VpnPwHwList *pMplsL2VpnPwHwList)
{

    tMplsL2VpnPwHwList *pMplsL2VpnPwHwEntry = NULL;

    pMplsL2VpnPwHwEntry =(tMplsL2VpnPwHwList *)RBTreeGet(gMplsL2VpnPwHwListTree,
            (tRBElem *)pMplsL2VpnPwHwListKey);

    if(pMplsL2VpnPwHwEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,"Fetch info from RBTree"
                    "is failed\n");
        return MPLS_FAILURE;
    }
    MEMCPY(pMplsL2VpnPwHwList, pMplsL2VpnPwHwEntry, sizeof(tMplsL2VpnPwHwList));
    return MPLS_SUCCESS;
}
/*****************************************************************************/
/* Function     : MplsL2vpnHwListGetNext                                     */
/* Description  : This function is used to get next entry from HW List       */
/* Input        : pMplsL2VpnPwHwList - contains key information              */
/*                ppMplsL2VpnPwHwList - store the pointer of entry based on  */
/*                                      key                                  */
/* Output       : NONE                                                       */
/* Returns      : MPLS_SUCCESS/MPLS_FAILURE                                  */
/*****************************************************************************/
INT4 MplsL2vpnHwListGetNext(tMplsL2VpnPwHwList *pMplsL2VpnPwHwList,
                            tMplsL2VpnPwHwList *pMplsL2VpnPwHwNext)
{
    tMplsL2VpnPwHwList *pMplsL2VpnPwHwEntry = NULL;

    pMplsL2VpnPwHwEntry =(tMplsL2VpnPwHwList *)RBTreeGetNext(gMplsL2VpnPwHwListTree,
                            (tRBElem *)pMplsL2VpnPwHwList,NULL);

    if(pMplsL2VpnPwHwEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,"Fetch info from RBTree"
                    "is failed\n");
        return MPLS_FAILURE;
    }

    MEMCPY(pMplsL2VpnPwHwNext, pMplsL2VpnPwHwEntry, sizeof(tMplsL2VpnPwHwList));
    return MPLS_SUCCESS;
}
/*****************************************************************************/
/* Function     : MplsL2vpnHwListDestroy                                     */
/* Description  : This function is used to destroy the HW List               */
/* Input        : None                                                       */
/* Output       : NONE                                                       */
/* Returns      : MPLS_SUCCESS/MPLS_FAILURE                                  */
/*****************************************************************************/

VOID MplsL2vpnHwListDestroy(VOID)
{

    if(gMplsL2VpnPwHwListTree != NULL)
    {
        RBTreeDestroy (gMplsL2VpnPwHwListTree, NULL, 0);
        gMplsL2VpnPwHwListTree = NULL;
    }
    else
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,"Unable to destroy"
            "RB Tree\n");
    }
}
/*****************************************************************************/
/* Function     : MplsCreateRbTreeForVplsHwList                                */
/* Description  : This function create a RB Tree to store VPLS HW List info   */
/* Input        : None                                                       */
/* Output       : NONE                                                       */
/* Returns      : MPLS_SUCCESS/MPLS_FAILURE                                  */
/*****************************************************************************/
INT4
MplsAllocateMemoryForVplsHwList(tMplsL2VpnVplsHwList **ppMplsL2VpnVplsHwList)
{

    *ppMplsL2VpnVplsHwList = (tMplsL2VpnVplsHwList *) MemAllocMemBlk
                            (MPLS_L2VPN_VPLS_HW_LIST_POOL_ID);

    if(NULL == *ppMplsL2VpnVplsHwList)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,"Memory Allocation failed"
                "from Pool Id - MPLS_VPLS_HW_LIST_POOL_ID\n");
        return MPLS_FAILURE;
    }
    return MPLS_SUCCESS;
}

/*****************************************************************************/
/* Function     : MplsCreateRbTreeForVplsHwList                                */
/* Description  : This function create a RB Tree to store VPLS HW List info   */
/* Input        : None                                                       */
/* Output       : NONE                                                       */
/* Returns      : MPLS_SUCCESS/MPLS_FAILURE                                  */
/*****************************************************************************/
INT4
MplsCreateRbTreeForVplsHwList()
{

    gMplsL2VpnVplsHwListTree =
        RBTreeCreateEmbedded (FSAP_OFFSETOF (tMplsL2VpnVplsHwList,
                    NextEntry),MplsL2vpnVplsRbTreeCompFunc);

        if(gMplsL2VpnVplsHwListTree == NULL)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                    "MplsCreateRbTreeForVplsHwList : Failed to create RB Tree "
                    "for VPLS HW List.\n");
            return MPLS_FAILURE;
        }
    return MPLS_SUCCESS;
}
/*****************************************************************************/
/* Function     : MplsL2vpnVplsRbTreeCompFunc                                    */
/* Description  : This is the RB Tree compare function for the L2VPN VPLS List */
/*                Index of the VPLS Hw List is u4VplsIndex     */
/* Input        : pRBElem1 - Pointer to the Node 1 for comparison            */
/*                pRBElem2 - Pointer to the Node 2 for comparison            */
/* Output       : NONE                                                       */
/* Returns      : MPLS_RB_EQUAL   - if all the keys matched for both         */
/*                                  the nodes                                */
/*                MPLS_RB_LESSER - if node pRBElem1's key is less than         */
/*                                  node pRBElem2's key.                     */
/*                MPLS_RB_GREATER - if node pRBElem1's key is greater        */
/*                                  than node pRBElem2's key.                */
/*****************************************************************************/
PRIVATE INT4
MplsL2vpnVplsRbTreeCompFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tMplsL2VpnVplsHwList * pMplsL2VpnVplsHwListA = NULL;
    tMplsL2VpnVplsHwList * pMplsL2VpnVplsHwListB = NULL;

    pMplsL2VpnVplsHwListA = (tMplsL2VpnVplsHwList *)pRBElem1;
    pMplsL2VpnVplsHwListB = (tMplsL2VpnVplsHwList *)pRBElem2;

    if(pMplsL2VpnVplsHwListA->u4VplsIndex > pMplsL2VpnVplsHwListB->u4VplsIndex)
    {
        return MPLS_RB_GREATER;
    }
    else if(pMplsL2VpnVplsHwListA->u4VplsIndex < pMplsL2VpnVplsHwListB->u4VplsIndex)
    {
        return MPLS_RB_LESSER;
    }
    return MPLS_RB_EQUAL;
}
/*****************************************************************************/
/* Function     : MplsL2vpnVplsHwListAdd                                         */
/* Description  : This function is used to add a entry in HW List            */
/* Input        : pMplsL2VpnVplsHwList - Node which is added in HWList         */
/* Output       : NONE                                                       */
/* Returns      : MPLS_SUCCESS/MPLS_FAILURE                                  */
/*****************************************************************************/
INT4 MplsL2vpnVplsHwListAdd(tMplsL2VpnVplsHwList *pMplsL2VpnVplsHwListUpdate)
{
	tMplsL2VpnVplsHwList *pMplsL2VpnVplsHwListEntry;

	if(MplsAllocateMemoryForVplsHwList(&pMplsL2VpnVplsHwListEntry) == MPLS_FAILURE)
	{
		return MPLS_FAILURE;
	}

	MEMCPY(pMplsL2VpnVplsHwListEntry, pMplsL2VpnVplsHwListUpdate, sizeof(tMplsL2VpnVplsHwList));

    if(RBTreeAdd(gMplsL2VpnVplsHwListTree,(tRBElem *)pMplsL2VpnVplsHwListEntry)  == RB_FAILURE)
    {
        MemReleaseMemBlock (MPLS_L2VPN_VPLS_HW_LIST_POOL_ID,(UINT1 *) pMplsL2VpnVplsHwListEntry);
		return MPLS_FAILURE;
    }
    return MPLS_SUCCESS;
}
/*****************************************************************************/
/* Function     : MplsL2vpnVplsHwListUpdate                                      */
/* Description  : To Update an existing entry in HW List            	     */
/* Input        : pMplsL2VpnVplsHwList - Node which is to be updated in HWList */
/* Output       : NONE                                                       */
/* Returns      : MPLS_SUCCESS/MPLS_FAILURE                                  */
/*****************************************************************************/
INT4 MplsL2vpnVplsHwListUpdate(tMplsL2VpnVplsHwList *pMplsL2VpnVplsHwListUpdate)
{
	tMplsL2VpnVplsHwList *pMplsL2VpnVplsHwEntry = NULL;

	/*Get the Matching Entry from RBTree*/

	pMplsL2VpnVplsHwEntry =(tMplsL2VpnVplsHwList *)RBTreeGet(gMplsL2VpnVplsHwListTree,
			(tRBElem *)pMplsL2VpnVplsHwListUpdate);

	if(pMplsL2VpnVplsHwEntry == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,"Fetch info from RBTree"
				"is failed\n");
		return MPLS_FAILURE;
	}
	/* In case HWList is to be ported to file -
	 * A write in file for this update will be required.
	 * Here since we are updaing RBTree, update directly through the pointer of RBTree Entry*/

    /*Since NextEntry Maintains the RBTree Node info, preserve it*/
    MEMCPY(&pMplsL2VpnVplsHwListUpdate->NextEntry, &pMplsL2VpnVplsHwEntry->NextEntry, sizeof(tRBNodeEmbd));
	
    /*Copy/Update Rest of the Data*/
    MEMCPY(pMplsL2VpnVplsHwEntry, pMplsL2VpnVplsHwListUpdate, sizeof(tMplsL2VpnVplsHwList));

    return MPLS_SUCCESS;
}

/*****************************************************************************/
/* Function     : MplsL2vpnVplsHwListDelete                                      */
/* Description  : This function is used to delete a entry from HW List       */
/*                  and release the memory                                   */
/* Input        : pMplsL2VpnVplsHwList - Node which has to be deleted          */
/* Output       : NONE                                                       */
/* Returns      : MPLS_SUCCESS/MPLS_FAILURE                                  */
/*****************************************************************************/

INT4 MplsL2vpnVplsHwListDelete(tMplsL2VpnVplsHwList *pMplsL2VpnVplsHwList)
{

    tMplsL2VpnVplsHwList *pMplsL2VpnVplsHwEntry = NULL;

    /*Search for entry in RBTree*/
	pMplsL2VpnVplsHwEntry =(tMplsL2VpnVplsHwList *)RBTreeGet(gMplsL2VpnVplsHwListTree,
			(tRBElem *)pMplsL2VpnVplsHwList);

	if(pMplsL2VpnVplsHwEntry == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,"Fetch info from RBTree"
				"is failed\n");
		return MPLS_FAILURE;
	}
    /*Delete the Entry*/
	RBTreeRem (gMplsL2VpnVplsHwListTree, pMplsL2VpnVplsHwEntry);
    MemReleaseMemBlock (MPLS_L2VPN_VPLS_HW_LIST_POOL_ID,(UINT1 *) pMplsL2VpnVplsHwEntry);
    return MPLS_SUCCESS;
}
/*****************************************************************************/
/* Function     : MplsL2vpnVplsHwListGetFirst                                    */
/* Description  : This function is used to get the first entry from HW List  */
/* Input        : ppMplsL2VpnVplsHwList - store the first entry of HW List     */
/* Output       : NONE                                                       */
/* Returns      : MPLS_SUCCESS/MPLS_FAILURE                                  */
/*****************************************************************************/

INT4 MplsL2vpnVplsHwListGetFirst(tMplsL2VpnVplsHwList *pMplsL2VpnVplsHwList)
{

    tMplsL2VpnVplsHwList *pMplsL2VpnVplsHwEntry;

    pMplsL2VpnVplsHwEntry = (tMplsL2VpnVplsHwList *)RBTreeGetFirst (gMplsL2VpnVplsHwListTree);

    if(pMplsL2VpnVplsHwEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,"Fetch info from RBTree"
                "is failed\n");
        return MPLS_FAILURE;
    }

    MEMCPY(pMplsL2VpnVplsHwList, pMplsL2VpnVplsHwEntry, sizeof(tMplsL2VpnVplsHwList));

    return MPLS_SUCCESS;
}
/*****************************************************************************/
/* Function     : MplsL2vpnVplsHwListGet                                         */
/* Description  : This function is used to get a particular entry from HWList*/
/* Input        : pMplsL2VpnVplsHwList - contains key information              */
/*                ppMplsL2VpnVplsHwList - store the pointer of entry based on  */
/*                                      key                                  */
/* Output       : NONE                                                       */
/* Returns      : MPLS_SUCCESS/MPLS_FAILURE                                  */
/*****************************************************************************/

INT4 MplsL2vpnVplsHwListGet(tMplsL2VpnVplsHwList *pMplsL2VpnVplsHwListKey,
                        tMplsL2VpnVplsHwList *pMplsL2VpnVplsHwList)
{

    tMplsL2VpnVplsHwList *pMplsL2VpnVplsHwEntry = NULL;

    pMplsL2VpnVplsHwEntry =(tMplsL2VpnVplsHwList *)RBTreeGet(gMplsL2VpnVplsHwListTree,
            (tRBElem *)pMplsL2VpnVplsHwListKey);

    if(pMplsL2VpnVplsHwEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,"Fetch info from RBTree"
                    "is failed\n");
        return MPLS_FAILURE;
    }
    MEMCPY(pMplsL2VpnVplsHwList, pMplsL2VpnVplsHwEntry, sizeof(tMplsL2VpnVplsHwList));
    return MPLS_SUCCESS;
}
/*****************************************************************************/
/* Function     : MplsL2vpnVplsHwListGetNext                                     */
/* Description  : This function is used to get next entry from HW List       */
/* Input        : pMplsL2VpnVplsHwList - contains key information              */
/*                ppMplsL2VpnVplsHwList - store the pointer of entry based on  */
/*                                      key                                  */
/* Output       : NONE                                                       */
/* Returns      : MPLS_SUCCESS/MPLS_FAILURE                                  */
/*****************************************************************************/
INT4 MplsL2vpnVplsHwListGetNext(tMplsL2VpnVplsHwList *pMplsL2VpnVplsHwList,
                            tMplsL2VpnVplsHwList *pMplsL2VpnVplsHwNext)
{
    tMplsL2VpnVplsHwList *pMplsL2VpnVplsHwEntry = NULL;

    pMplsL2VpnVplsHwEntry =(tMplsL2VpnVplsHwList *)RBTreeGetNext(gMplsL2VpnVplsHwListTree,
                            (tRBElem *)pMplsL2VpnVplsHwList,NULL);

    if(pMplsL2VpnVplsHwEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,"Fetch info from RBTree"
                    "is failed\n");
        return MPLS_FAILURE;
    }

    MEMCPY(pMplsL2VpnVplsHwNext, pMplsL2VpnVplsHwEntry, sizeof(tMplsL2VpnVplsHwList));
    return MPLS_SUCCESS;
}
/*****************************************************************************/
/* Function     : MplsL2vpnVplsHwListDestroy                                     */
/* Description  : This function is used to destroy the HW List               */
/* Input        : None                                                       */
/* Output       : NONE                                                       */
/* Returns      : MPLS_SUCCESS/MPLS_FAILURE                                  */
/*****************************************************************************/

VOID MplsL2vpnVplsHwListDestroy(VOID)
{

    if(gMplsL2VpnVplsHwListTree != NULL)
    {
        RBTreeDestroy (gMplsL2VpnVplsHwListTree, NULL, 0);
        gMplsL2VpnVplsHwListTree = NULL;
    }
    else
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,"Unable to destroy"
            "RB Tree\n");
    }
}

#endif
