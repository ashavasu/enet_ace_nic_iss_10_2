######################################################################
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
#
# $Id: make.h,v 1.5 2014/08/25 11:58:53 siva Exp $
#
# Description: Specifies the options and modules to be included for
#              for building the MPLS-DB Module
#######################################################################

# Set the PROJ_BASE_DIR as the directory where you untar the project files
PROJECT_NAME            = MPLSDB
PROJECT_BASE_DIR        = ${BASE_DIR}/mpls/mplsdb
MPLS_INCL_DIR           = $(BASE_DIR)/mpls/mplsinc
MPLS_INDEX_DIR          = $(BASE_DIR)/mpls/mplsrtr/inc
PROJECT_SOURCE_DIR      = ${PROJECT_BASE_DIR}
PROJECT_INCLUDE_DIR     = ${PROJECT_BASE_DIR}
PROJECT_OBJECT_DIR      = ${PROJECT_BASE_DIR}

# Specify the project include directories and dependencies
PROJECT_INCLUDE_FILES  = $(MPLS_INCL_DIR)/mplcmndb.h
 

PROJECT_FINAL_INCLUDES_DIRS= -I$(PROJECT_INCLUDE_DIR) \
                             -I$(MPLS_INCL_DIR) \
                             -I$(MPLS_INDEX_DIR) \
                               $(COMMON_INCLUDE_DIRS)

PROJECT_FINAL_INCLUDE_FILES= $(PROJECT_INCLUDE_FILES)

PROJECT_DEPENDENCIES= $(COMMON_DEPENDENCIES) \
                      $(PROJECT_FINAL_INCLUDE_FILES) \
                      $(MPLS_INCL_DIR)/mplcmndb.h \
                      $(MPLS_INCL_DIR)/mplshwlist.h \
                      $(MPLS_INCL_DIR)/mplssize.h \
                      $(MPLS_BASE_DIR)/make.h \
                      $(PROJECT_BASE_DIR)/Makefile \
                      $(PROJECT_BASE_DIR)/mplslsr.h \
                      $(PROJECT_BASE_DIR)/mplsftn.h \
                      $(PROJECT_BASE_DIR)/stdlsrlw.h \
                      $(PROJECT_BASE_DIR)/stdgmllw.h \
                      $(PROJECT_BASE_DIR)/fslsrlw.h \
                      $(PROJECT_BASE_DIR)/stdgmlwr.h \
                      $(PROJECT_BASE_DIR)/stdlsrwr.h \
                      $(PROJECT_BASE_DIR)/fslsrwr.h \
                      $(PROJECT_BASE_DIR)/stdlsrdb.h \
                      $(PROJECT_BASE_DIR)/stdftnwr.h \
                      $(PROJECT_BASE_DIR)/stdftnlw.h \
                      $(PROJECT_BASE_DIR)/stdftndb.h \
                      $(PROJECT_BASE_DIR)/make.h
