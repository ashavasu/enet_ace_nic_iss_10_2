/********************************************************************
 * * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * *
 * * $Id: mplscli.c,v 1.105 2017/07/11 13:16:05 siva Exp $
 * *
 * * Description: Protocol Low Level Routines
 * *********************************************************************/
#ifndef __MPLSCLI_C__
#define __MPLSCLI_C__

#include "mplsdbinc.h"
#include "cli.h"
#include "rtm.h"
#include "mplcmndb.h"
#include "mplscli.h"
#include "mpl3vpncmn.h"
#include "fssnmp.h"
#include "stdftnwr.h"
#include "stdftnlw.h"
#include "stdlsrwr.h"
#include "stdlsrlw.h"
#include "stdmpfcli.h"

#include "stdmplcli.h"
#include "stdgmllw.h"
#include "stdgmlcli.h"
#include "stdgmlwr.h"
#include "fscfacli.h"
#include "ifmibcli.h"
#include "mplsprot.h"
#include "fsmplslw.h"
#include "fsmplswr.h"
#include "fsmplscli.h"
#include "fslsrlw.h"
#include "fslsrwr.h"
#include "fsmlsrcli.h"
#include "oamext.h"
#include "lsppext.h"
#include "ldpext.h"
PRIVATE INT4        MplsCliInOutXcFtnActive (tSNMP_OCTET_STRING_TYPE
                                             InSegmentIndex,
                                             tSNMP_OCTET_STRING_TYPE
                                             OutSegmentIndex,
                                             tSNMP_OCTET_STRING_TYPE XCIndex,
                                             UINT4 u4FtnIndex, UINT4 u4Flag);

PRIVATE VOID        MplsCliDisplayOutLabels (tCliHandle CliHandle,
                                             UINT4 u4NextHopAddr,
                                             UINT4 u4OutSegLabel,
                                             UINT4 u4OutStackLabel);

#ifdef MPLS_IPV6_WANTED
PRIVATE VOID        MplsCliDisplayIpv6OutLabels (tCliHandle CliHandle,
                                                 tIp6Addr NextHopIpv6Addr,
                                                 UINT4 u4OutSegLabel,
                                                 UINT4 u4OutStackLabel);
#endif

PRIVATE BOOL1       MplsCliDisplayXcLabel (tCliHandle CliHandle, UINT4 u4Label);

PRIVATE INT4        MplsCliRouterId (tCliHandle CliHandle, UINT4 u4IfType,
                                     UINT4 u4Value, INT4 i4Status);
extern INT4         IfMainAdminStatusSet (tSnmpIndex *, tRetVal *);
extern INT4         IfMainRowStatusSet (tSnmpIndex *, tRetVal *);
extern INT4         IfMainTypeSet (tSnmpIndex *, tRetVal *);
extern INT4         IfStackStatusSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData);
extern INT4         L2VpnShowRunningConfig (tCliHandle CliHandle);

INT4
cli_process_mpls_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *pu4Args[MPLS_CLI_MAX_ARGS];
    INT4                i4RetStatus = 0;
    INT1                i1Argno = 0;
    INT4                i4Inst;
    UINT4               u4IfIndex = 0;
    UINT4               u4ErrCode;
    UINT4               u4Prefix = 0;
    UINT4               u4NextHop = 0;
    UINT4               u4LowLabel = 0;
    UINT4               u4HighLabel = 0;
    UINT4               u4TnlLspNum = 0;
    UINT4               u4TnlIngressLSRId = 0;
    UINT4               u4TnlEgressLSRId = 0;
    INT4                i4FtnActType = 0;
    INT4                i4Args = 0;
    tGenU4Addr          CLIAddrPrfx;
    tGenU4Addr          CLIAddrNxthp;
    tGenU4Addr          CLIAddr;
    UINT2               u2AddrType = 0;
    INT4                i4gMplsFlag = CLI_FM_ZERO;
    INT4                i4MplsDbg = CLI_FM_ZERO;
    MEMSET (&CLIAddrPrfx, 0, sizeof (tGenU4Addr));
    MEMSET (&CLIAddrNxthp, 0, sizeof (tGenU4Addr));
    MEMSET (&CLIAddr, 0, sizeof (tGenU4Addr));

    if (MplsIsMplsInitialised () != TRUE)
    {
        CliPrintf (CliHandle, "\r %%MPLS module is shutdown\n");
        return CLI_FAILURE;
    }
    va_start (ap, u4Command);

    /* Third arguement is always InterfaceName/Index */

    i4Inst = CLI_PTR_TO_I4 (va_arg (ap, UINT1 *));

    if (i4Inst != 0)
    {
        u4IfIndex = (UINT4) i4Inst;
    }

    while (1)
    {
        pu4Args[i1Argno++] = va_arg (ap, UINT4 *);
        if (i1Argno == MPLS_CLI_MAX_ARGS)
        {
            break;
        }
    }

    va_end (ap);

    switch (u4Command)
    {
        case CLI_FM_DEBUG_LEVEL_ENABLE:
            if (SNMP_SUCCESS != (nmhGetFsMplsFmDebugLevel (&(i4gMplsFlag))))
            {
                return CLI_FAILURE;
            }
            i4MplsDbg = ((i4gMplsFlag) | (CLI_PTR_TO_I4 (pu4Args[0])));
            if (SNMP_SUCCESS != (nmhSetFsMplsFmDebugLevel (i4MplsDbg)))
            {
                return CLI_FAILURE;
            }
            break;
        case CLI_FM_DEBUG_LEVEL_DISABLE:
            if (SNMP_SUCCESS != (nmhGetFsMplsFmDebugLevel (&(i4gMplsFlag))))
            {
                return CLI_FAILURE;
            }
            i4MplsDbg = ((i4gMplsFlag) & (~(CLI_PTR_TO_I4 (pu4Args[0]))));
            if (SNMP_SUCCESS != (nmhSetFsMplsFmDebugLevel (i4MplsDbg)))
            {
                return CLI_FAILURE;
            }
            break;
        case CLI_MPLS_ENABLE:
            u4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus = MplsCliSetFsMplsEnable (CliHandle, u4IfIndex);
            break;
        case CLI_MPLS_DISABLE:
            u4IfIndex = CLI_GET_IFINDEX ();
            i4RetStatus = MplsCliSetFsMplsDisable (CliHandle, u4IfIndex);
            break;
        case MPLS_CLI_FTN_EGRS_LBL_BINDING:

            i4FtnActType = REDIRECTLSP;

            CLIAddrPrfx.u2AddrType = MPLS_IPV4_ADDR_TYPE;

            i4RetStatus = MplsCliFtnLabelBinding (CliHandle,
                                                  CLIAddrPrfx,
                                                  0,
                                                  CLIAddrNxthp,
                                                  CLI_PTR_TO_U4 (pu4Args[2]),
                                                  (*((pu4Args[3]))),
                                                  MPLS_INVALID_LABEL,
                                                  i4FtnActType, 0);
            break;

        case MPLS_CLI_FTN_INGRS_LBL_BINDING:

            i4FtnActType = REDIRECTLSP;

            CLIAddrPrfx.u2AddrType = MPLS_IPV4_ADDR_TYPE;
            CLIAddrNxthp.u2AddrType = MPLS_IPV4_ADDR_TYPE;
            MEMCPY (&(CLIAddrPrfx.MPLS_IPV4_U4_ADDR (Addr)), (pu4Args[0]),
                    IPV4_ADDR_LENGTH);
            MEMCPY (&(CLIAddrNxthp.MPLS_IPV4_U4_ADDR (Addr)), (pu4Args[2]),
                    IPV4_ADDR_LENGTH);

            i4RetStatus = MplsCliFtnLabelBinding (CliHandle,
                                                  (CLIAddrPrfx),
                                                  (*((pu4Args[1]))),
                                                  (CLIAddrNxthp),
                                                  0, MPLS_INVALID_LABEL,
                                                  (*((pu4Args[3]))),
                                                  i4FtnActType, 0);

            break;

        case MPLS_CLI_FTN_NO_EGRS_LBL_BINDING:

            i4FtnActType = REDIRECTLSP;

            CLIAddrPrfx.u2AddrType = MPLS_IPV4_ADDR_TYPE;

            i4RetStatus = MplsCliFtnNoEgrsLB (CliHandle,
                                              (CLIAddrPrfx),
                                              (*((pu4Args[0]))), i4FtnActType);
            break;

        case MPLS_CLI_FTN_NO_INGRS_LBL_BINDING:

            i4FtnActType = REDIRECTLSP;

            CLIAddrPrfx.u2AddrType = MPLS_IPV4_ADDR_TYPE;
            CLIAddrNxthp.u2AddrType = MPLS_IPV4_ADDR_TYPE;
            MEMCPY (&(MPLS_IPV4_U4_ADDR (CLIAddrPrfx.Addr)), (pu4Args[0]),
                    IPV4_ADDR_LENGTH);
            MEMCPY (&(MPLS_IPV4_U4_ADDR (CLIAddrNxthp.Addr)), (pu4Args[2]),
                    IPV4_ADDR_LENGTH);

            i4RetStatus = MplsCliFtnNoIngrsLB (CliHandle,
                                               (CLIAddrPrfx),
                                               (*((pu4Args[1]))),
                                               (CLIAddrNxthp),
                                               (*((pu4Args[3]))), i4FtnActType);
            break;

        case MPLS_CLI_FTN_NO_LBL_BINDING:
            u2AddrType = MPLS_IPV4_ADDR_TYPE;
            i4RetStatus = MplsCliFtnNoLabelBinding (CliHandle, u2AddrType);
            break;

        case MPLS_CLI_FTN_TE_BINDING:
            i4FtnActType = REDIRECTTUNNEL;
            if (pu4Args[3] != NULL)
            {
                u4TnlEgressLSRId = *(pu4Args[3]);
            }
            if (pu4Args[4] != NULL)
            {
                u4TnlEgressLSRId = *(pu4Args[4]);
            }
            if (pu4Args[5] != NULL)
            {
                u4TnlIngressLSRId = *(pu4Args[5]);
            }
            if (pu4Args[6] != NULL)
            {
                u4TnlIngressLSRId = *(pu4Args[6]);
            }
            if (pu4Args[7] != NULL)
            {
                u4TnlLspNum = *(pu4Args[7]);
            }

            i4RetStatus = MplsCliFtnTeBinding (CliHandle,
                                               (*(UINT4 *) pu4Args[0]),
                                               (*(UINT4 *) pu4Args[1]),
                                               (*(UINT4 *) pu4Args[2]),
                                               i4FtnActType, u4TnlLspNum,
                                               u4TnlIngressLSRId,
                                               u4TnlEgressLSRId);
            break;
        case MPLS_CLI_NO_FTN_TE_BINDING:
            i4FtnActType = REDIRECTTUNNEL;
            i4RetStatus = MplsCliNoFtnTeBinding (CliHandle,
                                                 (*(UINT4 *) pu4Args[0]),
                                                 (*(UINT4 *) pu4Args[1]),
                                                 i4FtnActType);
            break;
        case MPLS_CLI_FTN_STATIC_CROSSCONNECT:

            CLIAddr.u2AddrType = MPLS_IPV4_ADDR_TYPE;

            MEMCPY (&(MPLS_IPV4_U4_ADDR (CLIAddr.Addr)), (pu4Args[2]),
                    IPV4_ADDR_LENGTH);

            i4RetStatus = MplsCliStaticXC (CliHandle,
                                           CLI_PTR_TO_U4
                                           (pu4Args[0]),
                                           (*((pu4Args[1]))),
                                           (CLIAddr),
                                           (CLI_PTR_TO_U4 (pu4Args[3])), 0);
            break;

        case MPLS_CLI_FTN_NO_STATIC_CROSSCONNECT:

            CLIAddr.u2AddrType = MPLS_IPV4_ADDR_TYPE;

            MEMCPY (&(MPLS_IPV4_U4_ADDR (CLIAddr.Addr)), (pu4Args[1]),
                    IPV4_ADDR_LENGTH);

            i4RetStatus = MplsCliNoStaticXC (CliHandle,
                                             (*((pu4Args[0]))),
                                             CLIAddr,
                                             (CLI_PTR_TO_U4 (pu4Args[2])));
            break;

#ifdef MPLS_IPV6_WANTED
        case MPLS_CLI_IPV6_FTN_STATIC_CROSSCONNECT:

            /*Check & get IPV4 or IPV6 Prefix Address */
            MplsGetIpv4Ipv6AddrfromCliStr (CliHandle,
                                           (CONST CHR1 *) pu4Args[2], &CLIAddr);

            i4RetStatus = MplsCliStaticXC (CliHandle,
                                           CLI_PTR_TO_U4
                                           (pu4Args[0]),
                                           (*((pu4Args[1]))),
                                           (CLIAddr),
                                           (CLI_PTR_TO_U4
                                            (pu4Args[3])),
                                           CLI_PTR_TO_U4 (pu4Args[4]));
            break;

        case MPLS_CLI_IPV6_FTN_NO_STATIC_CROSSCONNECT:

            /*Check & get IPV4 or IPV6 Prefix Address */
            MplsGetIpv4Ipv6AddrfromCliStr (CliHandle,
                                           (CONST CHR1 *) pu4Args[1], &CLIAddr);

            i4RetStatus = MplsCliNoStaticXC (CliHandle,
                                             (*((pu4Args[0]))),
                                             CLIAddr,
                                             (CLI_PTR_TO_U4 (pu4Args[2])));
            break;
#endif

        case MPLS_CLI_COMMON_DB_TRACE:

            if (CLI_PTR_TO_U4 (pu4Args[0]) == CLI_ENABLE)
            {
                if (pu4Args[1] != NULL)
                {
                    i4Args = CLI_PTR_TO_I4 (pu4Args[1]);
                    MplsCommonCliSetDebugLevel (CliHandle, i4Args);
                }
                gu4MplsDbTraceFlag = 0xffffffff;
                gu4MplsDbTraceLvl = 0xffffffff;
            }
            else
            {
                gu4MplsDbTraceFlag = 0x0;
                gu4MplsDbTraceLvl = 0;
            }
            i4RetStatus = CLI_SUCCESS;
            break;

        case MPLS_CLI_SHOWSTATICBINDING:
            if (pu4Args[1] != NULL)
            {
                u4Prefix = *((pu4Args[1]));
            }
            if (pu4Args[3] != NULL)
            {
                u4NextHop = *((pu4Args[3]));
            }
            i4RetStatus = MplsCliShowStaticBinding (CliHandle,
                                                    CLI_PTR_TO_U4
                                                    (pu4Args[0]),
                                                    u4Prefix,
                                                    CLI_PTR_TO_U4
                                                    (pu4Args[2]),
                                                    u4NextHop,
                                                    CLI_PTR_TO_U4 (pu4Args[4]));
            break;

        case MPLS_CLI_SHOWSTATICCROSSCONNECT:
            if (pu4Args[1] != NULL)
            {
                u4LowLabel = *((pu4Args[1]));
            }
            if (pu4Args[2] != NULL)
            {
                u4HighLabel = *((pu4Args[2]));
            }
            i4RetStatus = MplsCliShowStaticCrossConnect (CliHandle,
                                                         CLI_PTR_TO_U4
                                                         (pu4Args[0]),
                                                         u4LowLabel,
                                                         u4HighLabel,
                                                         CLI_PTR_TO_U4
                                                         (pu4Args[3]));
            break;

        case MPLS_CLI_ROUTER_ID:
            MPLS_CMN_LOCK ();
            i4RetStatus = MplsCliRouterId (CliHandle,
                                           (CLI_PTR_TO_U4 (pu4Args[0])),
                                           (CLI_PTR_TO_U4 (pu4Args[1])),
                                           (CLI_PTR_TO_U4 (pu4Args[2])));
            MPLS_CMN_UNLOCK ();
            break;

        case MPLS_CLI_SIG_CAPABILITY:
            i4RetStatus = MplsCliSignallingCaps (CliHandle,
                                                 CLI_PTR_TO_U4 (pu4Args[0]));
            break;

        case MPLS_CLI_GR_NO_MAX_WAIT_TIME:
        case MPLS_CLI_GR_MAX_WAIT_TIME:
            i4RetStatus = MplsCliSetMaxWaitTimers (CliHandle,
                                                   CLI_PTR_TO_U4 (pu4Args[0]),
                                                   CLI_PTR_TO_I4 (pu4Args[1]));
            break;

        case MPLS_CLI_TUNNEL_MODE:
            i4RetStatus = MplsCliTunnelModel (CliHandle,
                                              CLI_PTR_TO_U4 (pu4Args[0]));
            break;

        case MPLS_CLI_TTL_VALUE:
            i4RetStatus = MplsCliTTLValue (CliHandle, *((INT4 *) pu4Args[0]));
            break;

        case MPLS_CLI_SHOW_TUNNEL_MODE:
            i4RetStatus = MplsCliShowTunnelMode (CliHandle);
            break;

        case MPLS_CLI_SHOW_TTL_VALUE:
            i4RetStatus = MplsCliShowTTL (CliHandle);
            break;

        case MPLS_CLI_ACH_CODE_POINT:
            i4RetStatus = MplsCliSetAchCodePoint (CliHandle,
                                                  CLI_PTR_TO_I4 (pu4Args[0]));
            break;
        case MPLS_CLI_SHOW_MPLS_INTERFACES:
            i4RetStatus = MplsCliShowMplsInterfaces (CliHandle);
            break;
#ifdef MPLS_TEST_WANTED
#ifdef LDP_GR_WANTED
        case LDP_TEST_SH_FTN_HW_LIST:
            i4RetStatus = MplsDisplayFTNHwList (CliHandle);
            break;

        case LDP_TEST_SH_ILM_HW_LIST:
            i4RetStatus = MplsDisplayILMHwList (CliHandle);
            break;
#endif
#endif
#ifdef MPLS_IPV6_WANTED
        case MPLS_CLI_IPV6_SHOWSTATICBINDING:
            if (pu4Args[1] != NULL)
            {
                /*Check & get IPV4 or IPV6 Prefix Address */
                MplsGetIpv4Ipv6AddrfromCliStr (CliHandle,
                                               (CONST CHR1 *)
                                               pu4Args[1], &CLIAddrPrfx);
            }

            if (pu4Args[3] != NULL)
            {
                /*Check & get IPV4 or IPV6 Next Hop Address */
                MplsGetIpv4Ipv6AddrfromCliStr (CliHandle,
                                               (CONST CHR1 *)
                                               pu4Args[3], &CLIAddrNxthp);
            }

            i4RetStatus = MplsCliShowIpv6StaticBinding (CliHandle,
                                                        CLI_PTR_TO_U4
                                                        (pu4Args[0]),
                                                        CLIAddrPrfx,
                                                        CLI_PTR_TO_U4
                                                        (pu4Args[2]),
                                                        CLIAddrNxthp,
                                                        CLI_PTR_TO_U4
                                                        (pu4Args[4]));
            break;

        case MPLS_CLI_IPV6_FTN_EGRS_LBL_BINDING:

            i4FtnActType = REDIRECTLSP;

            /*Check & get IPV4 or IPV6 Address */
            MplsGetIpv4Ipv6AddrfromCliStr (CliHandle,
                                           (CONST CHR1 *)
                                           pu4Args[0], &CLIAddrPrfx);

            MEMSET (&CLIAddrNxthp, 0, sizeof (tGenU4Addr));

            i4RetStatus = MplsCliFtnLabelBinding (CliHandle,
                                                  (CLIAddrPrfx),
                                                  (*(pu4Args[1])),
                                                  CLIAddrNxthp,
                                                  CLI_PTR_TO_U4
                                                  (pu4Args[2]),
                                                  (*((pu4Args[3]))),
                                                  MPLS_INVALID_LABEL,
                                                  i4FtnActType, 0);

            break;

        case MPLS_CLI_IPV6_FTN_INGRS_LBL_BINDING:

            i4FtnActType = REDIRECTLSP;

            /*Check & get IPV4 or IPV6 Prefix Address */
            MplsGetIpv4Ipv6AddrfromCliStr (CliHandle,
                                           (CONST CHR1 *)
                                           pu4Args[0], &CLIAddrPrfx);

            /*Check & get IPV4 or IPV6 Next Hop Address */
            MplsGetIpv4Ipv6AddrfromCliStr (CliHandle,
                                           (CONST CHR1 *)
                                           pu4Args[2], &CLIAddrNxthp);

            i4RetStatus = MplsCliFtnLabelBinding (CliHandle,
                                                  (CLIAddrPrfx),
                                                  (*((pu4Args[1]))),
                                                  (CLIAddrNxthp),
                                                  0, MPLS_INVALID_LABEL,
                                                  (*((pu4Args[3]))),
                                                  i4FtnActType,
                                                  CLI_PTR_TO_U4 (pu4Args[4]));
            break;

        case MPLS_CLI_IPV6_FTN_NO_EGRS_LBL_BINDING:

            i4FtnActType = REDIRECTLSP;

            /*Check & get IPV4 or IPV6 Next Hop Address */
            MplsGetIpv4Ipv6AddrfromCliStr (CliHandle,
                                           (CONST CHR1 *)
                                           pu4Args[0], &CLIAddrPrfx);

            i4RetStatus = MplsCliFtnNoEgrsLB (CliHandle,
                                              (CLIAddrPrfx),
                                              (*((pu4Args[1]))), i4FtnActType);
            break;

        case MPLS_CLI_IPV6_FTN_NO_INGRS_LBL_BINDING:
            i4FtnActType = REDIRECTLSP;

            /*Check & get IPV4 or IPV6 Prefix Address */
            MplsGetIpv4Ipv6AddrfromCliStr (CliHandle,
                                           (CONST CHR1 *)
                                           pu4Args[0], &CLIAddrPrfx);

            /*Check & get IPV4 or IPV6 Next Hop Address */
            MplsGetIpv4Ipv6AddrfromCliStr (CliHandle,
                                           (CONST CHR1 *)
                                           pu4Args[2], &CLIAddrNxthp);

            i4RetStatus = MplsCliFtnNoIngrsLB (CliHandle,
                                               (CLIAddrPrfx),
                                               (*((pu4Args[1]))),
                                               (CLIAddrNxthp),
                                               (*((pu4Args[3]))), i4FtnActType);

            break;

        case MPLS_CLI_IPV6_FTN_NO_LBL_BINDING:
            u2AddrType = MPLS_IPV6_ADDR_TYPE;
            i4RetStatus = MplsCliFtnNoLabelBinding (CliHandle, u2AddrType);
            break;
#endif
        default:
            CliPrintf (CliHandle, "\r%% Unknown command \r\n");
            break;
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode >= CLI_ERR_START_ID_MPLS) &&
            (u4ErrCode < MPLS_CLI_MAX_ERROR_MSGS))
        {
            CliPrintf (CliHandle, "%s",
                       MPLS_CLI_ERROR_MSGS[CLI_ERR_OFFSET_MPLS (u4ErrCode)]);
        }
        CLI_SET_ERR (0);
    }
    return i4RetStatus;
}

/****************************************************************************
 *    Function    :  MplsCliShowMplsInterfaces
 *    Description :  This routine Displays the MPLS Enabled  interfaces
 *    Input       :  CliHandle
 *    Output      :  None
 *    Returns     :  CLI_SUCCESS/CLI_FAILURE
 *****************************************************************************/

INT4
MplsCliShowMplsInterfaces (tCliHandle CliHandle)
{
    UINT4               u4IfIndex = 0;
    tCfaIfInfo          CfaIfInfo;
    UINT4               u4NextIndex = 0;
    UINT4               u4PrevIndex = 0;
    UINT1               u1OperStatus = CFA_IF_DOWN;
    UINT4               u4L3IfIndex = 0;
    BOOL1               bLockReq = TRUE;
    INT1               *piIfName;
    UINT4               u4MplsInterIf = 0;
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH];

    piIfName = (INT1 *) &au1IfName[0];

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    CliPrintf (CliHandle,
               " Interface           IP              Tunnel             Operational   \r\n");
    CliPrintf (CliHandle,
               "-----------        -------         -----------        ----------------  \r\n");
    if (CfaApiGetFirstMplsIfInfo (&u4IfIndex, &CfaIfInfo) == CLI_SUCCESS)
    {
        if (CfaUtilGetL3IfFromMplsIf (u4IfIndex, &u4L3IfIndex, bLockReq) !=
            CFA_SUCCESS)
        {
            return CLI_FAILURE;
        }
        if (CfaCliGetIfName (u4L3IfIndex, piIfName) == CLI_FAILURE)
        {
            return CLI_FAILURE;
        }
        CliPrintf (CliHandle, "%-13s", piIfName);
        MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
        if (CfaGetIfOperStatus (u4IfIndex, &u1OperStatus) == CFA_FAILURE)
        {
            return CFA_FAILURE;
        }

        if (LdpIsIntfMapped (u4L3IfIndex) == MPLS_TRUE)
        {
            CliPrintf (CliHandle, "\t  %s", "Yes(ldp)");
        }
        else
        {
            CliPrintf (CliHandle, "\t     %s", "Yes ");
        }
        if (CfaUtilGetMplsTnlIfFromMplsIfIndex
            (u4IfIndex, &u4MplsInterIf, bLockReq) == CLI_SUCCESS)
        {
            CliPrintf (CliHandle, "\t     %s", "Yes");
        }
        else
        {
            CliPrintf (CliHandle, "\t      %s", "No");
        }
        if (u1OperStatus == CFA_IF_UP)
        {
            CliPrintf (CliHandle, "                   %s\n", "Yes");
        }
        else
        {
            CliPrintf (CliHandle, "                   %s\n", "No");
        }

        u4PrevIndex = u4IfIndex;
        while (CfaApiGetNextMplsIfInfo (u4PrevIndex, &u4NextIndex, &CfaIfInfo)
               == CLI_SUCCESS)
        {
            if (CfaUtilGetL3IfFromMplsIf (u4NextIndex, &u4L3IfIndex, bLockReq)
                != CFA_SUCCESS)
            {
                return CLI_FAILURE;
            }
            if (CfaCliGetIfName (u4L3IfIndex, piIfName) == CLI_FAILURE)
            {
                return CLI_FAILURE;
            }
            CliPrintf (CliHandle, "%-13s", piIfName);
            MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
            if (CfaGetIfOperStatus (u4NextIndex, &u1OperStatus) == CFA_FAILURE)
            {
                return CFA_FAILURE;
            }
            if (LdpIsIntfMapped (u4L3IfIndex) == MPLS_TRUE)
            {
                CliPrintf (CliHandle, "\t  %s", "Yes(ldp)");
            }
            else
            {
                CliPrintf (CliHandle, "\t     %s", "Yes ");
            }
            if (CfaUtilGetMplsTnlIfFromMplsIfIndex
                (u4NextIndex, &u4MplsInterIf, bLockReq) == CLI_SUCCESS)
            {
                CliPrintf (CliHandle, "\t     %s", "Yes");
            }
            else
            {
                CliPrintf (CliHandle, "\t      %s", "No");
            }
            if (u1OperStatus == CFA_IF_UP)
            {

                CliPrintf (CliHandle, "                   %s\n", "Yes");
            }
            else
            {
                CliPrintf (CliHandle, "                   %s\n", "No");
            }

            u4PrevIndex = u4NextIndex;
        }
    }
    else
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/****************************************************************************
 * Function    :  MplsCommonCliSetDebugLevel
 * Description :
 * Input       :  CliHandle, i4CliDebugLevel
 * Output      :  None
 * Returns     :  CLI_SUCCESS/CLI_FAILURE
 ****************************************************************************/
INT4
MplsCommonCliSetDebugLevel (tCliHandle CliHandle, INT4 i4CliDebugLevel)
{
    UNUSED_PARAM (CliHandle);

    if ((i4CliDebugLevel == DEBUG_DEBUG_LEVEL)
        || (i4CliDebugLevel == DEBUG_ALERT_LEVEL)
        || (i4CliDebugLevel == DEBUG_NOTICE_LEVEL)
        || (i4CliDebugLevel == DEBUG_WARN_LEVEL))
    {
        gu4MplsDbTraceLvl = DEBUG_DEBUG_LEVEL;
    }
    else if ((i4CliDebugLevel == DEBUG_CRITICAL_LEVEL)
             || (i4CliDebugLevel == DEBUG_ERROR_LEVEL)
             || (i4CliDebugLevel == DEBUG_EMERG_LEVEL))
    {
        gu4MplsDbTraceLvl = DEBUG_ERROR_LEVEL;
    }
    else if (i4CliDebugLevel == DEBUG_DEF_LVL_FLAG)
    {
        return CLI_SUCCESS;
    }
    else
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : MplsCliFtnLabelBinding 
 * Description   : This routine is used for static binding of labels to IPv4 &
 *                  IPv6  prefixes
 * Input(s)      : CliHandle   - Cli Context Handle
 *                 u4Prefix    - ip address prefix
 *                 u4Mask      - mask
 *                 u4NextHop   - NextHop
 *                 u4InIfIndex  - Incoming Interface Index
 *                 u4InLabel   - Incoming Label
 *                 u4OutLabel  - Outgoing Label
 *                 i4FtnActType  - FTNAction Type
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 ******************************************************************************/
INT1
MplsCliFtnLabelBinding (tCliHandle CliHandle,
                        tGenU4Addr Prefix,
                        UINT4 u4Mask, tGenU4Addr NextHop, UINT4 u4InIfIndex,
                        UINT4 u4InLabel, UINT4 u4OutLabel, INT4 i4FtnActType,
                        UINT4 u4OutIfIndex)
{
    tSNMP_OCTET_STRING_TYPE NextHopAddr;
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OID_TYPE      ActionPointer;
    UINT4               u4FtnIndex = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4XCIndex = 0;
    UINT4               u4InIndex = 0;
    UINT4               u4OutIndex = 0;
    INT4                i4InStatus = 0;
    INT4                i4OutStatus = 0;
    UINT4               u4InSegLabel = 0;
    UINT4               u4OutSegLabel = 0;
    INT4                i4MplsTnlIfIndex = 0;
    BOOL1               bFtnEntryExists = FALSE;
    UINT1               u1IsNhExist = TRUE;

    INT1                i1RetVal = SNMP_FAILURE;
    INT4                i4ActionType = 0;
    static UINT1        au1NextHopAddr[MPLS_INDEX_LENGTH];
    static UINT4        au4ActionPointer[MPLS_TE_XC_TABLE_OFFSET];
    static UINT4        au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 2, 1, 10, 1, 4, 4, 0, 0, 0, 0, 4, 0, 0, 0,
        0, 4, 0, 0, 0, 0
    };
    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];
    tRtInfoQueryMsg     RtQuery;
    tNetIpv4RtInfo      NetIpRtInfo;
    UINT4               u4RouteMask = 0xffffffff;
    UINT4               u4ErrorCode = 0;
    UINT4               u4NextHopAddr = MPLS_ZERO;
    UINT4               u4L3VlanIf = MPLS_ZERO;
    tInSegment         *pInSegmentEntry;

#ifdef MPLS_IPV6_WANTED
    UINT4               u4IfIndex = 0;
    INT4                i4CmpRet;
    UINT1               u1Scope;
    tIp6Addr            Ip6Addr;
    tSNMP_OCTET_STRING_TYPE NextHopIpv6Addr;
    static UINT1        au1NextHopIpv6Addr[IPV6_ADDR_LENGTH];
    NextHopIpv6Addr.pu1_OctetList = au1NextHopIpv6Addr;
#endif

    NextHopAddr.pu1_OctetList = au1NextHopAddr;
    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    XCIndex.pu1_OctetList = au1XCIndex;
    XCIndex.i4_Length = MPLS_INDEX_LENGTH;
    ActionPointer.pu4_OidList = au4ActionPointer;

    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));
    MEMSET (au1InSegIndex, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1XCIndex, 0, MPLS_INDEX_LENGTH);

    RtQuery.u4DestinationSubnetMask = u4RouteMask;
    RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;

    /* generate min and max address from input prefix and mask */
    /* TODO 
       u4MinAddr = (u4Prefix & u4Mask) + 1;
       u4MaxAddr = (u4Prefix & u4Mask) | (~(u4Mask));
     */

#ifdef MPLS_IPV6_WANTED
    /* Static binding Output Case for IPv4 */
    if (NextHop.u2AddrType == MPLS_IPV6_ADDR_TYPE)
    {
        /* Return failure if Next hop is LLA/SLA and OutInterface is Not given */
        u1Scope = Ip6GetAddrScope (&(NextHop.Addr.Ip6Addr));

        if ((u1Scope == ADDR6_SCOPE_LLOCAL)
            || (u1Scope == ADDR6_SCOPE_SITELOCAL))
        {
            if (u4OutIfIndex == 0)
            {
                CliPrintf (CliHandle,
                           "\r%% Out Interface is not given for Ipv6 Next Hop Address\n");
                return CLI_FAILURE;
            }
            CliPrintf (CliHandle, "%%Unable to set the Outsegment Entry\r\n");
        }

        MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
        i4CmpRet = MEMCMP (Ip6Addr.u1_addr, MPLS_IPV6_U4_ADDR (NextHop.Addr),
                           IPV6_ADDR_LENGTH);
        if (i4CmpRet != 0)
        {
            /*to use LdpIpv6IsAddrLocal ()  */
            if (NetIpv6IsOurAddress (&(NextHop.Addr.Ip6Addr), &u4IfIndex) ==
                NETIPV6_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r%% Next hop is same as self-ipv6 address\n");
                return CLI_FAILURE;
            }
        }

    }
    /* Static binding Output Case for IPv6 */
    else
    {
#endif
        if (MPLS_IPV4_U4_ADDR (NextHop.Addr) != 0)
        {
            if (NetIpv4IfIsOurAddress (NextHop.MPLS_IPV4_U4_ADDR (Addr)) ==
                NETIPV4_SUCCESS)
            {
                CliPrintf (CliHandle,
                           "\r%% Next hop is same as self-ip address\n");
                return CLI_FAILURE;
            }
        }
#ifdef MPLS_IPV6_WANTED
    }
#endif

    if (u4InLabel == MPLS_INVALID_LABEL && u4OutLabel != MPLS_INVALID_LABEL)
    {
        /* a matching entry have been found with ftn index as u4FtnIndex */
        if ((MplsCliGetOrCreateFTNEntry (Prefix, u4Mask, &u4FtnIndex,
                                         &bFtnEntryExists, i4FtnActType,
                                         &u4ErrorCode)) == CLI_FAILURE)
        {
            if (u4ErrorCode == SNMP_ERR_WRONG_VALUE)
            {
                CliPrintf (CliHandle, "%% Invalid Ip Address\r\n");
            }
            else
            {
                CliPrintf (CliHandle,
                           "%%Unable to fetch/create a FTN entry\r\n");
            }
            if (nmhSetMplsFTNRowStatus (u4FtnIndex, MPLS_STATUS_DESTROY)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            return CLI_FAILURE;
        }
    }
    else
    {
        pInSegmentEntry =
            MplsSignalGetInSegmentTableEntry (u4InIfIndex, u4InLabel);
        if (pInSegmentEntry != NULL)
        {
            CliPrintf (CliHandle, "%%Entry already exists.\r\n");
            return FAILURE;
        }
    }
    if (bFtnEntryExists)
    {
        /* set the InSeg Entry corresponding to the ftn entry as not in 
         * service. Index of the InSeg entry is obtained from the action 
         * pointer .Also corresponding XC entry's index is also obtained.*/

        /* Because an FTN entry already exists, it would be in 
         * 'not in service' state before entering here */

        if ((nmhGetMplsFTNActionType (u4FtnIndex, &i4ActionType))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "%%Unable to get FTN Action Type\r\n");
            return CLI_FAILURE;
        }
        if (i4ActionType != REDIRECTLSP)
        {
            CliPrintf (CliHandle,
                       "%%Cannot create FTN. Action Type is "
                       "not redirect LSP\r\n");
            return CLI_FAILURE;
        }
        if ((nmhGetMplsFTNActionPointer (u4FtnIndex, &ActionPointer))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "%%Unable to get the FTN Action Pointer\r\n");
            return CLI_FAILURE;
        }
        MPLS_OID_TO_INTEGER ((&ActionPointer), u4XCIndex,
                             MPLS_XC_INDEX_START_OFFSET);
        MPLS_INTEGER_TO_OCTETSTRING (u4XCIndex, (&XCIndex));
        MPLS_OID_TO_INTEGER ((&ActionPointer), u4InIndex,
                             MPLS_IN_INDEX_START_OFFSET);
        MPLS_INTEGER_TO_OCTETSTRING (u4InIndex, (&InSegmentIndex));
        MPLS_OID_TO_INTEGER ((&ActionPointer), u4OutIndex,
                             MPLS_OUT_INDEX_START_OFFSET);
        MPLS_INTEGER_TO_OCTETSTRING (u4OutIndex, (&OutSegmentIndex));

        /* deleting XC entry with the InSeg index and OutSeg index obtained
         * from the action pointer */
        if ((nmhTestv2MplsXCRowStatus (&u4ErrCode, &XCIndex, &InSegmentIndex,
                                       &OutSegmentIndex,
                                       MPLS_STATUS_NOT_INSERVICE)) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "%%Unable to set XC Row Status "
                       "as Not In Service\r\n");
            return CLI_FAILURE;
        }
        if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex,
                                    &OutSegmentIndex,
                                    MPLS_STATUS_NOT_INSERVICE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "%%Unable to set XC Row Status "
                       "as Not In Service\r\n");
            return CLI_FAILURE;
        }
        if ((nmhGetMplsInSegmentRowStatus (&InSegmentIndex,
                                           &i4InStatus)) == SNMP_SUCCESS)
        {
            if ((u4InLabel != MPLS_INVALID_LABEL)
                && (i4InStatus == MPLS_STATUS_ACTIVE))
            {
                if (nmhGetMplsInSegmentLabel (&InSegmentIndex, &u4InSegLabel) ==
                    SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "%%Unable to get the InSegment "
                               "Label\r\n");
                    return CLI_FAILURE;
                }

                if (nmhGetMplsInSegmentInterface
                    (&InSegmentIndex, &i4MplsTnlIfIndex) == SNMP_SUCCESS)
                {
#ifdef CFA_WANTED
                    if (CfaUtilGetIfIndexFromMplsTnlIf
                        ((UINT4) i4MplsTnlIfIndex, &u4L3VlanIf,
                         TRUE) == CFA_FAILURE)
                    {
                        CLI_SET_ERR (MPLS_CLI_ERR_OUTSTACK_ENTRY_CREATION);
                        return CLI_FAILURE;
                    }
#endif
                }
                else
                {
                    CliPrintf (CliHandle, "%%Unable to get the InSegment "
                               "Interface\r\n");
                    return CLI_FAILURE;
                }
                if ((u4InLabel == u4InSegLabel) && (u4InIfIndex == u4L3VlanIf))
                {

                    /* Already an FTN Entry exists with the same inLabel,
                     * So, Making the entry that was made 'not in service' 
                     * ===> active*/
                    MplsCliInOutXcFtnActive (InSegmentIndex, OutSegmentIndex,
                                             XCIndex, u4FtnIndex,
                                             MPLS_CLI_FTN_EGRS_LBL_BINDING);
                    CliPrintf (CliHandle, "\r%%Entry already present\r\n");
                    return CLI_FAILURE;
                }
                if ((nmhTestv2MplsInSegmentRowStatus
                     (&u4ErrCode, &InSegmentIndex,
                      MPLS_STATUS_NOT_INSERVICE)) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "%%Unable to set InSegment Row Status "
                               "as Not in service\r\n");
                    return CLI_FAILURE;
                }
                if ((nmhSetMplsInSegmentRowStatus (&InSegmentIndex,
                                                   MPLS_STATUS_NOT_INSERVICE))
                    == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "%%Unable to set InSegment Row Status "
                               "as Not in service\r\n");
                    return CLI_FAILURE;
                }
            }
        }
        if ((nmhGetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                            &i4OutStatus)) == SNMP_SUCCESS)
        {
            if ((u4OutLabel != MPLS_INVALID_LABEL)
                && (i4OutStatus == MPLS_STATUS_ACTIVE))
            {
                if ((nmhGetMplsOutSegmentTopLabel
                     (&OutSegmentIndex, &u4OutSegLabel)) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n%%Get MPLS Out Segment failed\r\n");
                    return CLI_FAILURE;
                }
#ifdef MPLS_IPV6_WANTED
                if (Prefix.u2AddrType == MPLS_IPV6_ADDR_TYPE)
                {
                    if (nmhGetMplsOutSegmentNextHopAddr (&OutSegmentIndex,
                                                         &NextHopIpv6Addr) ==
                        SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "%%Unable to get the Next Hop address\r\n");
                        return CLI_FAILURE;
                    }

                    i4CmpRet =
                        MEMCMP (NextHopIpv6Addr.pu1_OctetList,
                                MPLS_IPV6_U4_ADDR (NextHop.Addr),
                                IPV6_ADDR_LENGTH);
                    if ((u4OutLabel == u4OutSegLabel) && (i4CmpRet == 0))
                    {
                        /* Already an FTN Entry exists with the same outlabel,
                         * So, Making the entry that was made 'not in service'
                         * ===> active*/
                        MplsCliInOutXcFtnActive (InSegmentIndex,
                                                 OutSegmentIndex, XCIndex,
                                                 u4FtnIndex,
                                                 MPLS_CLI_FTN_INGRS_LBL_BINDING);
                        {

                            CliPrintf (CliHandle,
                                       "\r%%Entry already present\r\n");
                            return CLI_FAILURE;
                        }
                    }
                }
                else
                {
#endif
                    if (nmhGetMplsOutSegmentNextHopAddr (&OutSegmentIndex,
                                                         &NextHopAddr) ==
                        SNMP_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "%%Unable to get the Next Hop address\r\n");
                        return CLI_FAILURE;
                    }

                    MPLS_OCTETSTRING_TO_INTEGER ((&NextHopAddr), u4NextHopAddr);
                    if ((u4OutLabel == u4OutSegLabel)
                        && (u4NextHopAddr == NextHop.MPLS_IPV4_U4_ADDR (Addr)))
                    {
                        /* Already an FTN Entry exists with the same outlabel,
                         * So, Making the entry that was made 'not in service' 
                         * ===> active*/
                        MplsCliInOutXcFtnActive (InSegmentIndex,
                                                 OutSegmentIndex, XCIndex,
                                                 u4FtnIndex,
                                                 MPLS_CLI_FTN_INGRS_LBL_BINDING);
                        {
                            CliPrintf (CliHandle,
                                       "\r%%Entry already present\r\n");
                            return CLI_FAILURE;
                        }
                    }
#ifdef MPLS_IPV6_WANTED
                }
#endif

                if ((nmhTestv2MplsOutSegmentRowStatus
                     (&u4ErrCode, &OutSegmentIndex,
                      MPLS_STATUS_NOT_INSERVICE)) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "%%Unable to set Outsegment Row Status "
                               "as Not in service\r\n");
                    return CLI_FAILURE;
                }
                if ((nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                                    MPLS_STATUS_NOT_INSERVICE))
                    == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "%%Unable to set Outsegment Row Status "
                               "as Not in service\r\n");
                    return CLI_FAILURE;
                }
            }
        }
    }
    if (MplsCliInFTNEntry (&InSegmentIndex,
                           u4InLabel, u4InIfIndex, Prefix.u2AddrType,
                           bFtnEntryExists) == CLI_FAILURE)
    {
        if (nmhGetMplsInSegmentInterface (&InSegmentIndex, &i4MplsTnlIfIndex)
            == SNMP_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "nmhSetMplsOutSegmentRowStatus failed \t\n");
        }
        MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex,
                                     (UINT4) i4MplsTnlIfIndex, CFA_MPLS_TUNNEL,
                                     MPLS_TRUE);
        if (nmhSetMplsInSegmentRowStatus (&InSegmentIndex, MPLS_STATUS_DESTROY)
            == SNMP_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "nmhSetMplsInSegmentRowStatus failed \t\n");
        }
        if (!bFtnEntryExists)
        {
            if (nmhSetMplsFTNRowStatus (u4FtnIndex, MPLS_STATUS_DESTROY)
                == SNMP_FAILURE)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "nmhSetMplsFTNRowStatus Failed \t\n");
            }
        }
        CliPrintf (CliHandle, "%%Unable to set the Insegment Entry\r\n");
        return CLI_FAILURE;
    }

    if (MplsCliOutFTNEntry (&OutSegmentIndex,
                            u4OutLabel, NextHop,
                            bFtnEntryExists, &u1IsNhExist,
                            u4OutIfIndex) == CLI_FAILURE)
    {
        if (!bFtnEntryExists)
        {
            if (u1IsNhExist == TRUE)
            {
                if (nmhGetMplsOutSegmentInterface
                    (&OutSegmentIndex, &i4MplsTnlIfIndex) == SNMP_FAILURE)
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               "nmhGetMplsOutSegmentInterface failed \r\n");
                }
                if (CfaUtilGetIfIndexFromMplsTnlIf ((UINT4) i4MplsTnlIfIndex,
                                                    &u4OutIfIndex,
                                                    TRUE) == CFA_FAILURE)
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               "CfaUtilGetIfIndexFromMplsTnlIf failed \r\n");
                }
                MplsDeleteMplsIfOrMplsTnlIf (u4OutIfIndex,
                                             (UINT4) i4MplsTnlIfIndex,
                                             CFA_MPLS_TUNNEL, MPLS_TRUE);
                if (nmhSetMplsInSegmentRowStatus
                    (&InSegmentIndex, MPLS_STATUS_DESTROY) == SNMP_FAILURE)
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               "nmhSetMplsInSegmentRowStatus failed \t\r\n");
                }
                if (nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                                   MPLS_STATUS_DESTROY) ==
                    SNMP_FAILURE)
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               "MPLSOutSegment set failed\r\n");
                }
            }
            if (nmhSetMplsFTNRowStatus (u4FtnIndex, MPLS_STATUS_DESTROY)
                == SNMP_FAILURE)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "MPLS FTN Row Status Set failed\r\n");
            }
        }
        else
        {
            if (u1IsNhExist == TRUE)
            {
                if ((nmhGetMplsOutSegmentInterface
                     (&OutSegmentIndex, &i4MplsTnlIfIndex)) == SNMP_FAILURE)
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               "nmhGetMplsOutSegmentInterface failed \r\n");
                }
                if (CfaUtilGetIfIndexFromMplsTnlIf (i4MplsTnlIfIndex,
                                                    &u4OutIfIndex,
                                                    TRUE) == MPLS_FAILURE)
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               "CfaUtilGetIfIndexFromMplsTnlIf failed \r\n");
                }
                MplsDeleteMplsIfOrMplsTnlIf (u4OutIfIndex,
                                             (UINT4) i4MplsTnlIfIndex,
                                             CFA_MPLS_TUNNEL, MPLS_TRUE);
                if ((nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                                    MPLS_STATUS_DESTROY)) ==
                    SNMP_FAILURE)
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               "nmhSetMplsOutSegmentInterface failed \r\n");
                }
            }
            else
            {
                if ((nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                                    MPLS_STATUS_ACTIVE))
                    == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "%%Unable to set Outsegment Row Status "
                               "as Active\r\n");
                    return CLI_FAILURE;
                }
            }
        }

        CliPrintf (CliHandle, "%%Unable to set the Outsegment Entry\r\n");
        return CLI_FAILURE;
    }
    do
    {
        if (!bFtnEntryExists)
        {
            /* create XC entry */
            if ((nmhGetMplsXCIndexNext (&XCIndex)) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_NO_FREE_INDEX);
                i1RetVal = SNMP_FAILURE;
                break;
            }
            /* In segment index */
            if ((nmhTestv2MplsXCRowStatus (&u4ErrCode, &XCIndex,
                                           &InSegmentIndex, &OutSegmentIndex,
                                           MPLS_STATUS_CREATE_AND_WAIT)) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_XC_ENTRY_CREATION);
                i1RetVal = SNMP_FAILURE;
                break;
            }
            if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex,
                                        &OutSegmentIndex,
                                        MPLS_STATUS_CREATE_AND_WAIT)) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_XC_ENTRY_CREATION);
                i1RetVal = SNMP_FAILURE;
                break;
            }
        }
        /* set the action pointer */
        MPLS_OCTETSTRING_TO_INTEGER ((&XCIndex), u4XCIndex);
        MPLS_OCTETSTRING_TO_INTEGER ((&OutSegmentIndex), u4OutIndex);
        MPLS_OCTETSTRING_TO_INTEGER ((&InSegmentIndex), u4InIndex);

        MPLS_OCTETSTRING_TO_OID (au4XCTableOid,
                                 (&XCIndex), MPLS_XC_INDEX_START_OFFSET);
        MPLS_OCTETSTRING_TO_OID (au4XCTableOid,
                                 (&InSegmentIndex), MPLS_IN_INDEX_START_OFFSET);
        MPLS_OCTETSTRING_TO_OID (au4XCTableOid,
                                 (&OutSegmentIndex),
                                 MPLS_OUT_INDEX_START_OFFSET);

        if (u4OutLabel != MPLS_INVALID_LABEL)
        {
            ActionPointer.pu4_OidList = au4XCTableOid;
            ActionPointer.u4_Length = MPLS_TE_XC_TABLE_OFFSET;
            if ((nmhTestv2MplsFTNActionPointer (&u4ErrCode, u4FtnIndex,
                                                &ActionPointer)) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_ERR_ACTION_POINTER);
                i1RetVal = SNMP_FAILURE;
                break;
            }
            if ((nmhSetMplsFTNActionPointer (u4FtnIndex, &ActionPointer))
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_ERR_ACTION_POINTER);
                i1RetVal = SNMP_FAILURE;
                break;
            }
        }
        i1RetVal = SNMP_SUCCESS;
    }
    while (0);
    if (i1RetVal != SNMP_SUCCESS)
    {
        MplsCliDelTables (&InSegmentIndex, &OutSegmentIndex,
                          &XCIndex, u4InIfIndex, u4FtnIndex);
        CliPrintf (CliHandle, "\r%% FTN Entry creation / updation failed\n");
        return CLI_FAILURE;
    }

    do
    {
        if (u4OutLabel != MPLS_INVALID_LABEL)
        {

            if ((nmhTestv2MplsOutSegmentRowStatus (&u4ErrCode, &OutSegmentIndex,
                                                   MPLS_STATUS_ACTIVE))
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n%% Unable to set the Out segment "
                           "row status as active");
                break;
            }
            if ((nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                                MPLS_STATUS_ACTIVE)) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n%% Unable to set the Out segment "
                           "row status as active");
                break;
            }
        }
        else                    /*(u4Inlabel != MPLS_INVALID_LABEL) */
        {
            if ((nmhTestv2MplsInSegmentRowStatus (&u4ErrCode, &InSegmentIndex,
                                                  MPLS_STATUS_ACTIVE)) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n%% Unable to set the In segment "
                           "row status as active");
                break;
            }
            if ((nmhSetMplsInSegmentRowStatus (&InSegmentIndex,
                                               MPLS_STATUS_ACTIVE)) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n%% Unable to set the In segment "
                           "row status as active");
                break;
            }
        }

        if ((nmhTestv2MplsXCRowStatus (&u4ErrCode, &XCIndex, &InSegmentIndex,
                                       &OutSegmentIndex,
                                       MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "%% Unable to activate XC Entry\r\n");
            break;
        }
        if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex,
                                    &OutSegmentIndex,
                                    MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "%% Unable to activate XC Entry\r\n");
            break;
        }
        /* Making all tables (FTN, XC) Active */
        /* set row status to active */
        if (u4OutLabel != MPLS_INVALID_LABEL)
        {
            if ((nmhTestv2MplsFTNRowStatus (&u4ErrCode, u4FtnIndex,
                                            MPLS_STATUS_ACTIVE)) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "%% Unable to activate FTN Entry\r\n");
                break;
            }
            if ((nmhSetMplsFTNRowStatus (u4FtnIndex, MPLS_STATUS_ACTIVE)) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "%% Unable to activate FTN Entry\r\n");
                break;
            }
            /* every set operation have been sucessful */
        }
        return CLI_SUCCESS;
    }
    while (0);

    MplsCliDelTables (&InSegmentIndex, &OutSegmentIndex, &XCIndex,
                      u4InIfIndex, u4FtnIndex);
    /* delete the created ftn and OutSegment and XC entry */
    return CLI_FAILURE;
}

/******************************************************************************
 * Function Name : MplsCliGetOrCreateFTNEntry 
 * Description   : This routine is used for static binding of labels to IPv4 & IPv6 
 *                 prefixes
 * Input(s)      : Prefix   - ip address prefix
 *                 u4Mask     - mask
 *                 pu4FtnIndex  - FTN Index
 *                 bFtnEntryExists    - FTN Entry check
 *                 i4FtnActType  - FTNAction Type
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 ******************************************************************************/

INT1
MplsCliGetOrCreateFTNEntry (tGenU4Addr Prefix, UINT4 u4Mask,
                            UINT4 *pu4FtnIndex, BOOL1 * bFtnEntryExists,
                            INT4 i4FtnActType, UINT4 *pu4ErrorCode)
{
    tSNMP_OCTET_STRING_TYPE DestAddrMin;
    tSNMP_OCTET_STRING_TYPE DestAddrMax;

    tSNMP_OCTET_STRING_TYPE MaskBit;
    static UINT1        au1DestAddrMin[MPLS_INDEX_LENGTH];
    static UINT1        au1DestAddrMax[MPLS_INDEX_LENGTH];
    static UINT1        au1Mask[MPLS_INDEX_LENGTH];
    tFtnEntry          *pFtnEntry = NULL;

    UINT4               u4MinAddr = Prefix.Addr.u4Addr;
    UINT4               u4MaxAddr = u4Mask;
    UINT4               u4ErrCode = 0;
    UINT4               u4FtnIndex = 0;
    UINT4               u4Prefix = 0;
#ifdef MPLS_IPV6_WANTED

    tIp6Addr            Ipv6Prefix;
    tIp6Addr            TempDestIpv6AddrMax;

    tSNMP_OCTET_STRING_TYPE DestIpv6AddrMin;
    tSNMP_OCTET_STRING_TYPE DestIpv6AddrMax;
    static UINT1        au1DestIpv6AddrMin[IPV6_ADDR_LENGTH];
    static UINT1        au1DestIpv6AddrMax[IPV6_ADDR_LENGTH];
    DestIpv6AddrMin.pu1_OctetList = au1DestIpv6AddrMin;
    DestIpv6AddrMax.pu1_OctetList = au1DestIpv6AddrMax;

    MEMSET (&Ipv6Prefix, 0, sizeof (tIp6Addr));
    MEMSET (&TempDestIpv6AddrMax, 0, sizeof (tIp6Addr));
#endif
    MaskBit.pu1_OctetList = au1Mask;
    MaskBit.i4_Length = MPLS_INDEX_LENGTH;
    DestAddrMin.pu1_OctetList = au1DestAddrMin;
    DestAddrMax.pu1_OctetList = au1DestAddrMax;

    MPLS_CMN_LOCK ();

#ifdef MPLS_IPV6_WANTED
    if (Prefix.u2AddrType == MPLS_IPV6_ADDR_TYPE)
    {
        /* Get FTN Entry from IPv6 TRIE */
        MEMCPY (&(Ipv6Prefix.u1_addr), MPLS_IPV6_U4_ADDR (Prefix.Addr),
                IPV6_ADDR_LENGTH);

        pFtnEntry = MplsSignalGetFtnIpv6TableEntry (&Ipv6Prefix);
        if (pFtnEntry != NULL)
        {
            u4FtnIndex = pFtnEntry->u4FtnIndex;
        }
    }
    else
    {
#endif
        /* Get FTN Entry from IPv4 TRIE */
        u4Prefix = Prefix.MPLS_IPV4_U4_ADDR (Addr);
        pFtnEntry = MplsSignalGetFtnTableEntry (u4Prefix);
        if (pFtnEntry != NULL)
        {
            u4FtnIndex = pFtnEntry->u4FtnIndex;
        }
#ifdef MPLS_IPV6_WANTED
    }
#endif

    MPLS_CMN_UNLOCK ();

    if (u4FtnIndex != 0)
    {
        *pu4FtnIndex = u4FtnIndex;
        *bFtnEntryExists = TRUE;
        if ((nmhTestv2MplsFTNRowStatus (&u4ErrCode, u4FtnIndex,
                                        MPLS_STATUS_NOT_INSERVICE)) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if ((nmhSetMplsFTNRowStatus
             (u4FtnIndex, MPLS_STATUS_NOT_INSERVICE)) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        return CLI_SUCCESS;
    }

    /*  create an ftn entry */
    if ((nmhGetMplsFTNIndexNext (&u4FtnIndex)) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if ((nmhTestv2MplsFTNRowStatus (&u4ErrCode, u4FtnIndex,
                                    MPLS_STATUS_CREATE_AND_WAIT)) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if ((nmhSetMplsFTNRowStatus (u4FtnIndex, MPLS_STATUS_CREATE_AND_WAIT))
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    /* setting the action type */
    if (nmhTestv2MplsFTNActionType (&u4ErrCode, u4FtnIndex, i4FtnActType)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetMplsFTNActionType (u4FtnIndex, i4FtnActType) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* set the address type */

#ifdef MPLS_IPV6_WANTED
    /* IPv6 Case */
    if (Prefix.u2AddrType == MPLS_IPV6_ADDR_TYPE)
    {
        if (nmhTestv2MplsFTNAddrType
            (&u4ErrCode, u4FtnIndex, MPLS_IPV6_ADDR_TYPE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetMplsFTNAddrType (u4FtnIndex, MPLS_IPV6_ADDR_TYPE) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        /* set the min and max address generated from input Ipv6 prefix and mask 
         * to ftn dest min and max address*/
        MEMCPY ((DestIpv6AddrMin.pu1_OctetList),
                &(MPLS_IPV6_U4_ADDR (Prefix.Addr)), IPV6_ADDR_LENGTH);
        DestIpv6AddrMin.i4_Length = IPV6_ADDR_LENGTH;

        /* Get Mask in Hexa :: form */
        MplsGetIPV6Subnetmask (u4Mask, TempDestIpv6AddrMax.u1_addr);

        MEMCPY (DestIpv6AddrMax.pu1_OctetList, TempDestIpv6AddrMax.u1_addr,
                IPV6_ADDR_LENGTH);
        DestIpv6AddrMax.i4_Length = IPV6_ADDR_LENGTH;

        if ((nmhTestv2MplsFTNDestAddrMin (pu4ErrorCode, u4FtnIndex,
                                          &DestIpv6AddrMin)) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if ((nmhSetMplsFTNDestAddrMin (u4FtnIndex, &DestIpv6AddrMin)) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if ((nmhTestv2MplsFTNDestAddrMax (&u4ErrCode, u4FtnIndex,
                                          &DestIpv6AddrMax)) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if ((nmhSetMplsFTNDestAddrMax (u4FtnIndex,
                                       &DestIpv6AddrMax)) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    else
    {
#endif
        if (nmhTestv2MplsFTNAddrType
            (&u4ErrCode, u4FtnIndex, MPLS_IPV4_ADDR_TYPE) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetMplsFTNAddrType (u4FtnIndex, MPLS_IPV4_ADDR_TYPE) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        /* set the min and max address generated from input prefix and mask 
         * to ftn dest min and max address*/

        MPLS_INTEGER_TO_OCTETSTRING (u4MinAddr, (&DestAddrMin));
        MPLS_INTEGER_TO_OCTETSTRING (u4MaxAddr, (&DestAddrMax));

        if ((nmhTestv2MplsFTNDestAddrMin (pu4ErrorCode, u4FtnIndex,
                                          &DestAddrMin)) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if ((nmhSetMplsFTNDestAddrMin (u4FtnIndex, &DestAddrMin)) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if ((nmhTestv2MplsFTNDestAddrMax (&u4ErrCode, u4FtnIndex,
                                          &DestAddrMax)) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if ((nmhSetMplsFTNDestAddrMax (u4FtnIndex, &DestAddrMax)) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
#ifdef MPLS_IPV6_WANTED
    }
#endif

    /* setting the mask bit */
    *(MaskBit.pu1_OctetList) = FTN_DEST_BITS_SET;
    if (nmhTestv2MplsFTNMask (&u4ErrCode, u4FtnIndex, &MaskBit) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetMplsFTNMask (u4FtnIndex, &MaskBit) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    *pu4FtnIndex = u4FtnIndex;
    *bFtnEntryExists = FALSE;
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : MplsCliInFTNEntry  
 * Description   : This routine is used for static binding of labels to IPv4
 *                 prefixes
 * Input(s)      : InSegmentIndex  - Insegment Index
 *                 u4InLabel       - the In label
 *                 bFtnEntryExists - FTN Entry check
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 ******************************************************************************/
INT1
MplsCliInFTNEntry (tSNMP_OCTET_STRING_TYPE * InSegmentIndex, UINT4 u4InLabel,
                   UINT4 u4InIfIndex, UINT2 u2AddrType, BOOL1 bFtnEntryExists)
{
    UINT4               u4ErrCode = MPLS_ZERO;
    UINT4               u4MplsTnlIfIndex = MPLS_ZERO;
    UINT4               u4InSegmentLabel = MPLS_ZERO;
    UINT4               u4L3Intf = MPLS_ZERO;

    if (u4InLabel == MPLS_INVALID_LABEL)
    {
        return CLI_SUCCESS;
    }

    if (!bFtnEntryExists)
    {
        /* the other case that is no entry is found with matching address */
        /* create out segmnt entry */
        if ((nmhGetMplsInSegmentIndexNext (InSegmentIndex)) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if ((nmhTestv2MplsInSegmentRowStatus (&u4ErrCode, InSegmentIndex,
                                              MPLS_STATUS_CREATE_AND_WAIT))
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if ((nmhSetMplsInSegmentRowStatus
             (InSegmentIndex, MPLS_STATUS_CREATE_AND_WAIT)) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        /* setting the InSegment ip addr family to ipv4 */
        if (nmhTestv2MplsInSegmentAddrFamily (&u4ErrCode, InSegmentIndex,
                                              u2AddrType) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhSetMplsInSegmentAddrFamily (InSegmentIndex,
                                           u2AddrType) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
    }
    if (u4InLabel != MPLS_INVALID_LABEL)
    {
        if (nmhGetMplsInSegmentLabel (InSegmentIndex, &u4InSegmentLabel)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (u4InSegmentLabel != u4InLabel)
        {
            /* Set label value in InSegment table */
            if ((nmhTestv2MplsInSegmentLabel (&u4ErrCode, InSegmentIndex,
                                              u4InLabel)) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
            if ((nmhSetMplsInSegmentLabel (InSegmentIndex, u4InLabel))
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
        }
        if ((nmhGetMplsInSegmentInterface (InSegmentIndex,
                                           (INT4 *) &u4MplsTnlIfIndex)) ==
            SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (u4MplsTnlIfIndex != MPLS_ZERO)
        {
            if (CfaUtilGetIfIndexFromMplsTnlIf
                (u4MplsTnlIfIndex, &u4L3Intf, TRUE) == CFA_FAILURE)
            {
                return CLI_FAILURE;
            }
            if (u4L3Intf != u4InIfIndex)
            {
                if (MplsDeleteMplsIfOrMplsTnlIf (u4L3Intf,
                                                 u4MplsTnlIfIndex,
                                                 CFA_MPLS_TUNNEL,
                                                 MPLS_TRUE) == MPLS_FAILURE)
                {
                    return CLI_FAILURE;
                }
            }
        }
        /* Set InSegment NPop to one */
        if ((nmhTestv2MplsInSegmentNPop (&u4ErrCode, InSegmentIndex,
                                         1)) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if ((nmhSetMplsInSegmentNPop (InSegmentIndex, 1)) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (u4L3Intf != u4InIfIndex)
        {

            if (CfaGetFreeInterfaceIndex (&u4MplsTnlIfIndex, CFA_MPLS_TUNNEL) ==
                OSIX_FAILURE)
            {
                return CLI_FAILURE;
            }

            if ((nmhTestv2MplsInSegmentInterface (&u4ErrCode,
                                                  InSegmentIndex,
                                                  (INT4) u4MplsTnlIfIndex))
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if ((nmhSetMplsInSegmentInterface (InSegmentIndex,
                                               (INT4) u4MplsTnlIfIndex))
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (MplsCreateMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlIfIndex,
                                             CFA_MPLS_TUNNEL) == MPLS_FAILURE)
            {

                CLI_SET_ERR (MPLS_CLI_ERR_INSTACK_ENTRY_CREATION);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : MplsCliOutFTNEntry 
 * Description   : This routine is used for static binding of labels to IPv4 
 *                 prefixes
 * Input(s)      : CliHandle  - Cli Context Handle
 *                 u4Prefix   - ip address prefix
 *                 u4Mask     - mask
 *                 u4NextHop  - NextHop
 *                 u4Label    - Label
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 ******************************************************************************/

INT1
MplsCliOutFTNEntry (tSNMP_OCTET_STRING_TYPE * OutSegmentIndex,
                    UINT4 u4OutLabel, tGenU4Addr NextHop, BOOL1 bFtnEntryExists,
                    UINT1 *pu1IsNhExist, UINT4 u4OutIfIndex)
{
    tSNMP_OCTET_STRING_TYPE NextHopAddr;
    static UINT1        au1NextHopAddr[MPLS_INDEX_LENGTH];
    tRtInfoQueryMsg     RtQuery;
    tNetIpv4RtInfo      NetIpRtInfo;
    UINT4               u4RouteMask = 0xffffffff;
    UINT4               u4IfaceIndex = 0;
#ifdef CFA_WANTED
    UINT4               u4MplsIfIndex = 0;
#endif
    UINT4               u4ErrCode = 0;
    INT4                i4MplsTnlIfIndex = 0;
    UINT4               u4L3Intf = 0;
#ifdef MPLS_IPV6_WANTED
    tSNMP_OCTET_STRING_TYPE Ipv6NextHopAddr;
    static UINT1        au1Ipv6NextHopAddr[IPV6_ADDR_LENGTH];
    INT4                i4CmpRet;
    UINT1               u1Scope = 0;
    UINT1               u1DestPrefixLen = 128;
    tIp6Addr            Ip6Addr;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tNetIpv6RtInfoQueryMsg Ipv6RtQuery;

    Ipv6NextHopAddr.pu1_OctetList = au1Ipv6NextHopAddr;
    MEMSET (&Ip6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&Ipv6RtQuery, 0, sizeof (tNetIpv6RtInfoQueryMsg));

#endif

#ifndef MPLS_IPV6_WANTED
    UNUSED_PARAM (u4OutIfIndex);
#endif
    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

    NextHopAddr.pu1_OctetList = au1NextHopAddr;

    if (u4OutLabel == MPLS_INVALID_LABEL)
    {
        return CLI_SUCCESS;
    }

    if ((NextHop.u2AddrType == MPLS_IPV4_ADDR_TYPE)
        && (MPLS_IPV4_U4_ADDR (NextHop.Addr) != 0))
    {
        RtQuery.u4DestinationSubnetMask = u4RouteMask;
        RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
        RtQuery.u4DestinationIpAddress = NextHop.MPLS_IPV4_U4_ADDR (Addr);

        if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_SUCCESS)
        {

            if (NetIpv4GetCfaIfIndexFromPort (NetIpRtInfo.u4RtIfIndx,
                                              &u4IfaceIndex) == NETIPV4_FAILURE)
            {
                *pu1IsNhExist = FALSE;
                return CLI_FAILURE;
            }
        }
#ifdef CFA_WANTED
        else
        {
            if (CfaIpIfGetIfIndexFromHostIpAddressInCxt (MPLS_DEF_CONTEXT_ID,
                                                         NextHop.
                                                         MPLS_IPV4_U4_ADDR
                                                         (Addr),
                                                         &u4IfaceIndex) ==
                CFA_FAILURE)
            {
                *pu1IsNhExist = FALSE;
                CLI_SET_ERR (MPLS_CLI_OUTSEG_ENTRY_CREATION);
                return CLI_FAILURE;
            }
        }
        if (CfaUtilGetMplsIfFromIfIndex (u4IfaceIndex, &u4MplsIfIndex, TRUE) ==
            CFA_FAILURE)
        {
            *pu1IsNhExist = FALSE;
            CLI_SET_ERR (MPLS_CLI_ERR_OUTSTACK_ENTRY_CREATION);
            return CLI_FAILURE;
        }
#endif
    }
#ifdef MPLS_IPV6_WANTED
    else
    {
        /* Return failure if Next hop is LLA/SLA and OutInterface is Not given */
        u1Scope = Ip6GetAddrScope (&(NextHop.Addr.Ip6Addr));

        if ((u1Scope == ADDR6_SCOPE_LLOCAL)
            || (u1Scope == ADDR6_SCOPE_SITELOCAL))
        {
            if (u4OutIfIndex == 0)
            {
                *pu1IsNhExist = FALSE;
                return CLI_FAILURE;
            }
            else
            {
                u4IfaceIndex = u4OutIfIndex;
            }
        }
        else
        {
            i4CmpRet =
                MEMCMP (Ip6Addr.u1_addr, MPLS_IPV6_U4_ADDR (NextHop.Addr),
                        IPV6_ADDR_LENGTH);
            if (i4CmpRet != 0)
            {
                Ipv6RtQuery.u4ContextId = VCM_DEFAULT_CONTEXT;
                Ipv6RtQuery.u1QueryFlag = RTM6_QUERIED_FOR_NEXT_HOP;

                MEMCPY (NetIpv6RtInfo.Ip6Dst.u1_addr,
                        MPLS_IPV6_U4_ADDR (NextHop.Addr), IPV6_ADDR_LENGTH);

                NetIpv6RtInfo.u1Prefixlen = u1DestPrefixLen;

                if (NetIpv6GetRoute (&Ipv6RtQuery, &NetIpv6RtInfo) ==
                    NETIPV6_SUCCESS)
                {
                    if (NetIpv6GetCfaIfIndexFromPort (NetIpv6RtInfo.u4Index,
                                                      &u4IfaceIndex) ==
                        NETIPV6_FAILURE)
                    {
                        *pu1IsNhExist = FALSE;
                        return CLI_FAILURE;
                    }
                }
            }
        }
    }
#endif
    if (!bFtnEntryExists)
    {
        /* create out segmnt entry */
        if ((nmhGetMplsOutSegmentIndexNext (OutSegmentIndex)) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_NO_FREE_INDEX);
            /* delete the created ftn entry */
            return CLI_FAILURE;
        }
        if ((nmhTestv2MplsOutSegmentRowStatus (&u4ErrCode, OutSegmentIndex,
                                               MPLS_STATUS_CREATE_AND_WAIT))
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_OUTSEG_ENTRY_CREATION);
            /* delete the created ftn entry */
            return CLI_FAILURE;
        }
        if ((nmhSetMplsOutSegmentRowStatus
             (OutSegmentIndex, MPLS_STATUS_CREATE_AND_WAIT)) == SNMP_FAILURE)
        {
            /* delete the created ftn entry */
            return CLI_FAILURE;
        }
    }
    if (u4OutLabel != MPLS_INVALID_LABEL)
    {
        if ((nmhGetMplsOutSegmentInterface (OutSegmentIndex,
                                            &i4MplsTnlIfIndex)) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        /*  Delete the MPLS interface stacked over the L3 Interface
         *  if the Out Interface is changed. */
        if (i4MplsTnlIfIndex != MPLS_ZERO)
        {
            if (CfaUtilGetIfIndexFromMplsTnlIf
                ((UINT4) i4MplsTnlIfIndex, &u4L3Intf, TRUE) == CFA_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_ERR_INSEG_LABEL);
                return CLI_FAILURE;
            }
            if (u4L3Intf != u4IfaceIndex)
            {
                if (MplsDeleteMplsIfOrMplsTnlIf (u4L3Intf,
                                                 (UINT4) i4MplsTnlIfIndex,
                                                 CFA_MPLS_TUNNEL,
                                                 MPLS_TRUE) == MPLS_FAILURE)
                {
                    return CLI_FAILURE;
                }
            }
        }

        if ((nmhTestv2MplsOutSegmentTopLabel (&u4ErrCode, OutSegmentIndex,
                                              u4OutLabel)) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if ((nmhSetMplsOutSegmentTopLabel (OutSegmentIndex, u4OutLabel))
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        /* set the PushTopLabel in OutSegment table to true */
        if ((nmhTestv2MplsOutSegmentPushTopLabel
             (&u4ErrCode, OutSegmentIndex, MPLS_TRUE)) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_ERR_OUTSEG_PUSH_TOP_LABEL);
            return CLI_FAILURE;
        }
        if ((nmhSetMplsOutSegmentPushTopLabel (OutSegmentIndex,
                                               MPLS_TRUE)) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_ERR_OUTSEG_LABEL);
            return CLI_FAILURE;
        }

#ifdef MPLS_IPV6_WANTED
        if (NextHop.u2AddrType == MPLS_IPV6_ADDR_TYPE)
        {
            /* set the next hop addr type to ipv6 */
            if (nmhTestv2MplsOutSegmentNextHopAddrType (&u4ErrCode,
                                                        OutSegmentIndex,
                                                        LSR_ADDR_IPV6) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_ERR_ADDR_TYPE);
                return CLI_FAILURE;
            }
            if (nmhSetMplsOutSegmentNextHopAddrType (OutSegmentIndex,
                                                     LSR_ADDR_IPV6) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            /* set the Ipv6 NextHop address */
            MEMCPY ((Ipv6NextHopAddr.pu1_OctetList),
                    &(MPLS_IPV6_U4_ADDR (NextHop.Addr)), IPV6_ADDR_LENGTH);
            Ipv6NextHopAddr.i4_Length = IPV6_ADDR_LENGTH;

            if ((nmhTestv2MplsOutSegmentNextHopAddr (&u4ErrCode,
                                                     OutSegmentIndex,
                                                     &Ipv6NextHopAddr)) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_ERR_NEXT_HOP_ADDR);
                return CLI_FAILURE;
            }
            if ((nmhSetMplsOutSegmentNextHopAddr (OutSegmentIndex,
                                                  &Ipv6NextHopAddr)) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
        }
        else
        {
#endif
            /* set the next hop addr type to ipv4 */
            if (nmhTestv2MplsOutSegmentNextHopAddrType (&u4ErrCode,
                                                        OutSegmentIndex,
                                                        LSR_ADDR_IPV4) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_ERR_ADDR_TYPE);
                return CLI_FAILURE;
            }
            if (nmhSetMplsOutSegmentNextHopAddrType (OutSegmentIndex,
                                                     LSR_ADDR_IPV4) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            /* set the NextHop address */
            MPLS_INTEGER_TO_OCTETSTRING (NextHop.MPLS_IPV4_U4_ADDR (Addr),
                                         (&NextHopAddr));
            if ((nmhTestv2MplsOutSegmentNextHopAddr
                 (&u4ErrCode, OutSegmentIndex, &NextHopAddr)) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_ERR_NEXT_HOP_ADDR);
                return CLI_FAILURE;
            }
            if ((nmhSetMplsOutSegmentNextHopAddr (OutSegmentIndex,
                                                  &NextHopAddr)) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
#ifdef MPLS_IPV6_WANTED
        }
#endif

        /* Create a MPLS interface and set this interface index in the
         * OutSegment table only if it is already not created*/
        if (u4L3Intf != u4IfaceIndex)
        {
            if (CfaGetFreeInterfaceIndex ((UINT4 *) &i4MplsTnlIfIndex,
                                          CFA_MPLS_TUNNEL) == OSIX_FAILURE)
            {
                return CLI_FAILURE;
            }

            if ((nmhTestv2MplsOutSegmentInterface (&u4ErrCode,
                                                   OutSegmentIndex,
                                                   i4MplsTnlIfIndex))
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_ERR_OUTSEG_IF);
                return CLI_FAILURE;
            }

            if ((nmhSetMplsOutSegmentInterface (OutSegmentIndex,
                                                i4MplsTnlIfIndex))
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (MplsCreateMplsIfOrMplsTnlIf (u4IfaceIndex,
                                             (UINT4) i4MplsTnlIfIndex,
                                             CFA_MPLS_TUNNEL) == MPLS_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_ERR_OUTSTACK_ENTRY_CREATION);
                return CLI_FAILURE;
            }
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : MplsCliDelTables 
 * Description   : This routine is used for deleting the MPLS tables
 * Input(s)      : InSegmentIndex  - InSegment Index
 *                 OutSegmentIndex - OutSegment Index
 *                 XCIndex         - Cross connect Index
 *                 u4InIfIndex     - Incoming Interface Index
 *                 u4FtnIndex      - FEC to NHLFE Index
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 ******************************************************************************/

VOID
MplsCliDelTables (tSNMP_OCTET_STRING_TYPE * InSegmentIndex,
                  tSNMP_OCTET_STRING_TYPE * OutSegmentIndex,
                  tSNMP_OCTET_STRING_TYPE * XCIndex, UINT4 u4InIfIndex,
                  UINT4 u4FtnIndex)
{
    INT4                i4MplsTnlIfIndex = 0;
    UINT4               u4OutIfIndex = 0;

    if (nmhSetMplsXCRowStatus (XCIndex, InSegmentIndex, OutSegmentIndex,
                               MPLS_STATUS_DESTROY) == SNMP_FAILURE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "nmhSetMplsXCRowStatus failed\t\n");
    }
    if (nmhGetMplsInSegmentInterface (InSegmentIndex, &i4MplsTnlIfIndex)
        == SNMP_FAILURE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "nmhGetMplsInSegmentInterface Failed :No Entry Present\t\n");
    }

    if (u4InIfIndex == 0)
    {
        if (CfaUtilGetIfIndexFromMplsTnlIf ((UINT4) i4MplsTnlIfIndex,
                                            (UINT4 *) &u4InIfIndex,
                                            TRUE) == MPLS_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       " CfaUtilGetIfIndexFromMplsTnlIf fialed \t\n");
        }
    }
    MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, (UINT4) i4MplsTnlIfIndex,
                                 CFA_MPLS_TUNNEL, MPLS_TRUE);
    if (nmhGetMplsOutSegmentInterface (OutSegmentIndex, &i4MplsTnlIfIndex)
        == SNMP_FAILURE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "nmhGetMplsOutSegmentInterface Failed \t\n");
    }
    if (CfaUtilGetIfIndexFromMplsTnlIf ((UINT4) i4MplsTnlIfIndex, &u4OutIfIndex,
                                        TRUE) == MPLS_FAILURE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "CfaUtilGetIfIndexFromMplsTnlIf failed \r\n");
    }
    MplsDeleteMplsIfOrMplsTnlIf (u4OutIfIndex, (UINT4) i4MplsTnlIfIndex,
                                 CFA_MPLS_TUNNEL, MPLS_TRUE);
    if (nmhSetMplsOutSegmentRowStatus (OutSegmentIndex, MPLS_STATUS_DESTROY)
        == SNMP_FAILURE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "nmhSetMplsOutSegmentRowStatus failed \t\n");
    }
    if (nmhSetMplsInSegmentRowStatus (InSegmentIndex, MPLS_STATUS_DESTROY)
        == SNMP_FAILURE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "nmhSetMplsInSegmentRowStatus failed \t\n");
    }

    if (nmhSetMplsFTNRowStatus (u4FtnIndex, MPLS_STATUS_DESTROY)
        == SNMP_FAILURE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "nmhSetMplsFTNRowStatus failed \t\n");
    }

}

/******************************************************************************
 * Function Name : MplsCliFtnNoEgrsLB  
 * Description   : This routine is used for removing the static binding 
 *                 in Egress Side
 * Input(s)      : CliHandle  - Cli Context Handle
 *                 Prefix   - Prefix
 *                 u4Mask     - Mask
 *                 u4Label    - the In label
 *                 i4FtnActType  - FTNAction Type
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 ******************************************************************************/
INT1
MplsCliFtnNoEgrsLB (tCliHandle CliHandle, tGenU4Addr Prefix,
                    UINT4 u4Label, INT4 i4FtnActType)
{
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    INT4                i4XCOwner = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4XCIndex = 0;
    UINT4               u4InIndex = 0;
    UINT4               u4OutIndex = 0;
    UINT4               u4L3Intf = 0;
    INT4                i4IfIndex = 0;
    INT4                i4RetValMplsInSegmentAddrFamily = 0;
    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];
    tXcEntry           *pXcEntry = NULL;
    tInSegment         *pInEntry = NULL;
#ifdef MPLS_IPV6_WANTED
    tIp6Addr            FtnMinAddr;
    tIp6Addr            FtnMaxAddr;
    tIp6Addr            TempIpv6Addr;
    MEMSET (&FtnMinAddr, 0, sizeof (tIp6Addr));
    MEMSET (&FtnMaxAddr, 0, sizeof (tIp6Addr));
    MEMSET (&TempIpv6Addr, 0, sizeof (tIp6Addr));

#endif
    UNUSED_PARAM (Prefix);
    UNUSED_PARAM (i4FtnActType);
    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    XCIndex.pu1_OctetList = au1XCIndex;
    XCIndex.i4_Length = MPLS_INDEX_LENGTH;

    /*Get the XC Index using In Label. In segment is not known so passing NULL */

    if (MplsGetXcIndexFromInIfIndexAndInLabel (MPLS_ZERO, u4Label,
                                               (UINT4 *) &u4XCIndex) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_NO_ENTRY);
        return CLI_FAILURE;
    }
    pInEntry = MplsSignalGetInSegmentTableEntry (MPLS_ZERO, u4Label);
    if (pInEntry == NULL)
    {
        CliPrintf (CliHandle, "%%Entry do not exists.\r\n");
        return FAILURE;
    }

    u4InIndex = INSEGMENT_INDEX (pInEntry);
    pXcEntry =
        (tXcEntry *) MplsGetXCTableEntry (u4XCIndex, u4InIndex, u4OutIndex);

    if (pXcEntry == NULL)
    {
        CLI_SET_ERR (MPLS_CLI_NO_ENTRY);
        return CLI_FAILURE;
    }
    MPLS_INTEGER_TO_OCTETSTRING (u4InIndex, (&InSegmentIndex));
    MPLS_INTEGER_TO_OCTETSTRING (u4XCIndex, (&XCIndex));
    if (nmhGetMplsInSegmentAddrFamily (&InSegmentIndex,
                                       &i4RetValMplsInSegmentAddrFamily) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /*1. check for Address family */
    if (i4RetValMplsInSegmentAddrFamily != (INT4) Prefix.u2AddrType)
    {
        return CLI_FAILURE;
    }
    /* 2.check if XCOwner is admin */
    if ((nmhGetMplsXCOwner (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                            &i4XCOwner)) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if ((u4InIndex == 0) || (i4XCOwner != MPLS_OWNER_SNMP))
    {
        return CLI_FAILURE;
    }

    if ((nmhTestv2MplsXCRowStatus (&u4ErrCode, &XCIndex,
                                   &InSegmentIndex, &OutSegmentIndex,
                                   MPLS_STATUS_DESTROY)) == SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_XC_ENTRY_DELETION);
        return CLI_FAILURE;
    }
    if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex,
                                &OutSegmentIndex, MPLS_STATUS_DESTROY))
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_XC_ENTRY_DELETION);
        return CLI_FAILURE;
    }
    /* get the interface index b4 deleting the out seg entry */
    if ((nmhGetMplsInSegmentInterface (&InSegmentIndex,
                                       &i4IfIndex)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "%%Wrong Insegment interface index.\r\n");
        return CLI_FAILURE;
    }

    if ((CfaUtilGetIfIndexFromMplsTnlIf
         ((UINT4) i4IfIndex, &u4L3Intf, TRUE)) == CFA_FAILURE)
    {
        CliPrintf (CliHandle, "%%Invalid interface index %d \r\n", i4IfIndex);
        return CLI_FAILURE;
    }

    if (MplsDeleteMplsIfOrMplsTnlIf (u4L3Intf,
                                     (UINT4) i4IfIndex,
                                     CFA_MPLS_TUNNEL,
                                     MPLS_TRUE) == MPLS_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to delete the MPLS "
                   "tunnel interface\r\n");
        return CLI_FAILURE;
    }

    if ((nmhTestv2MplsInSegmentRowStatus (&u4ErrCode,
                                          &InSegmentIndex,
                                          MPLS_STATUS_DESTROY)) == SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_INSEG_ENTRY_DELETION);
        return CLI_FAILURE;
    }
    if ((nmhSetMplsInSegmentRowStatus
         (&InSegmentIndex, MPLS_STATUS_DESTROY)) == SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_INSEG_ENTRY_DELETION);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : MplsCliFtnNoIngrsLB  
 * Description   : This routine is used for removing the static binding of 
 *                 labels to IPv4 
 * Input(s)      : CliHandle  - Cli Context Handle
 *                 Prefix   - Prefix
 *                 u4Mask     - Mask
 *                 NextHop  - nexthop to reach the prefix
 *                 u4Label    - the Out label
 *                 i4FtnActType  - FTNAction Type
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 ******************************************************************************/
INT1
MplsCliFtnNoIngrsLB (tCliHandle CliHandle, tGenU4Addr Prefix, UINT4 u4Mask,
                     tGenU4Addr NextHop, UINT4 u4Label, INT4 i4FtnActType)
{
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OCTET_STRING_TYPE NextHopAddr;
    tSNMP_OID_TYPE      ActionPointer;
    tSNMP_OCTET_STRING_TYPE DestAddrMin;
    tSNMP_OCTET_STRING_TYPE DestAddrMax;
    INT4                i4XCOwner = 0;
    INT4                i4IfIndex = 0;
    INT4                i4FtnActionType = 0;
    UINT4               u4FtnIndex = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4XCIndex = 0;
    UINT4               u4PrevFtnIndex = 0;
    UINT4               u4OutSegLabel = 0;
    UINT4               u4NextHopAddr = 0;
    UINT4               u4OutIndex = 0;
    UINT4               u4InIndex = 0;
    UINT4               u4FtnMaxAddr = 0;
    UINT4               u4FtnMinAddr = 0;
    INT4                i4InStatus = 0;
    UINT4               u4L3Intf = 0;
    INT1                i1RetVal = SNMP_SUCCESS;
    BOOL1               bEntryPresent = FALSE;
    static UINT4        au4ActionPointer[MPLS_TE_XC_TABLE_OFFSET];
    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1NextHopAddr[IPV6_ADDR_LENGTH];
    static UINT1        au1DestAddrMin[IPV6_ADDR_LENGTH];
    static UINT1        au1DestAddrMax[IPV6_ADDR_LENGTH];
#ifdef MPLS_IPV6_WANTED
    UINT1               u1DestPrefixLen = 0;
    INT4                i4CmpRet = 0;
    INT4                i4NextHopCmpRet = 0;
    tIp6Addr            FtnMinAddr;
    tIp6Addr            FtnMaxAddr;
    tIp6Addr            Ipv6NextHopAddr;
    tIp6Addr            TempIpv6Addr;
    tSNMP_OCTET_STRING_TYPE DestIpv6AddrMin;
    tSNMP_OCTET_STRING_TYPE DestIpv6AddrMax;
    tSNMP_OCTET_STRING_TYPE NextHopIpv6Addr;
    static UINT1        au1DestIpv6AddrMin[IPV6_ADDR_LENGTH];
    static UINT1        au1DestIpv6AddrMax[IPV6_ADDR_LENGTH];
    static UINT1        au1NextHopIpv6Addr[IPV6_ADDR_LENGTH];

    DestIpv6AddrMin.pu1_OctetList = au1DestIpv6AddrMin;
    DestIpv6AddrMax.pu1_OctetList = au1DestIpv6AddrMax;
    NextHopIpv6Addr.pu1_OctetList = au1NextHopIpv6Addr;
    MEMSET (&FtnMinAddr, 0, sizeof (tIp6Addr));
    MEMSET (&FtnMaxAddr, 0, sizeof (tIp6Addr));
    MEMSET (&TempIpv6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&Ipv6NextHopAddr, 0, sizeof (tIp6Addr));

#endif

    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    XCIndex.pu1_OctetList = au1XCIndex;
    XCIndex.i4_Length = MPLS_INDEX_LENGTH;
    NextHopAddr.pu1_OctetList = au1NextHopAddr;
    ActionPointer.pu4_OidList = au4ActionPointer;
    DestAddrMin.pu1_OctetList = au1DestAddrMin;
    DestAddrMax.pu1_OctetList = au1DestAddrMax;

    /* 1. scan the ftn table and get the XC index for each entry */

    while (1)
    {
        if ((u4FtnIndex != 0) &&
            ((nmhGetNextIndexMplsFTNTable (u4PrevFtnIndex, &u4FtnIndex)) ==
             SNMP_FAILURE))
        {
            break;
        }

        /* Only executed first time */
        if ((u4FtnIndex == 0) &&
            ((nmhGetFirstIndexMplsFTNTable (&u4FtnIndex)) == SNMP_FAILURE))
        {
            break;
        }

        if ((nmhGetMplsFTNActionPointer (u4FtnIndex, &ActionPointer))
            == SNMP_FAILURE)
        {
            u4PrevFtnIndex = u4FtnIndex;
            continue;
        }
        if ((nmhGetMplsFTNActionType (u4FtnIndex,
                                      &i4FtnActionType)) == SNMP_FAILURE)
        {
            u4PrevFtnIndex = u4FtnIndex;
            continue;
        }

#ifdef MPLS_IPV6_WANTED
        if (Prefix.u2AddrType == MPLS_IPV6_ADDR_TYPE)
        {
            if ((nmhGetMplsFTNDestAddrMin (u4FtnIndex,
                                           &DestIpv6AddrMin)) == SNMP_FAILURE)
            {
                u4PrevFtnIndex = u4FtnIndex;
                continue;
            }
            if ((nmhGetMplsFTNDestAddrMax (u4FtnIndex,
                                           &DestIpv6AddrMax)) == SNMP_FAILURE)
            {
                u4PrevFtnIndex = u4FtnIndex;
                continue;
            }

            MEMCPY (FtnMinAddr.u1_addr, DestIpv6AddrMin.pu1_OctetList,
                    DestIpv6AddrMin.i4_Length);
            MEMCPY (FtnMaxAddr.u1_addr, DestIpv6AddrMax.pu1_OctetList,
                    DestIpv6AddrMax.i4_Length);

            u1DestPrefixLen = MplsGetIpv6Subnetmasklen (FtnMaxAddr.u1_addr);

            i4CmpRet =
                MEMCMP (FtnMinAddr.u1_addr, MPLS_IPV6_U4_ADDR (Prefix.Addr),
                        IPV6_ADDR_LENGTH);

            if ((i4FtnActionType != i4FtnActType) || (i4CmpRet != 0) ||
                (u1DestPrefixLen != u4Mask))
            {
                u4PrevFtnIndex = u4FtnIndex;
                continue;
            }
        }
        else
        {
#endif
            if ((nmhGetMplsFTNDestAddrMin (u4FtnIndex,
                                           &DestAddrMin)) == SNMP_FAILURE)
            {
                u4PrevFtnIndex = u4FtnIndex;
                continue;
            }
            if ((nmhGetMplsFTNDestAddrMax (u4FtnIndex,
                                           &DestAddrMax)) == SNMP_FAILURE)
            {
                u4PrevFtnIndex = u4FtnIndex;
                continue;
            }

            MPLS_OCTETSTRING_TO_INTEGER ((&DestAddrMin), u4FtnMinAddr);
            MPLS_OCTETSTRING_TO_INTEGER ((&DestAddrMax), u4FtnMaxAddr);

            if ((i4FtnActionType != i4FtnActType)
                || (u4FtnMinAddr != (Prefix.MPLS_IPV4_U4_ADDR (Addr)))
                || (u4FtnMaxAddr != u4Mask))
            {
                u4PrevFtnIndex = u4FtnIndex;
                continue;
            }
#ifdef MPLS_IPV6_WANTED
        }
#endif

        MPLS_OID_TO_INTEGER ((&ActionPointer), u4XCIndex,
                             MPLS_XC_INDEX_START_OFFSET);
        MPLS_INTEGER_TO_OCTETSTRING (u4XCIndex, (&XCIndex));
        MPLS_OID_TO_INTEGER ((&ActionPointer), u4InIndex,
                             MPLS_IN_INDEX_START_OFFSET);
        MPLS_INTEGER_TO_OCTETSTRING (u4InIndex, (&InSegmentIndex));
        MPLS_OID_TO_INTEGER ((&ActionPointer), u4OutIndex,
                             MPLS_OUT_INDEX_START_OFFSET);
        MPLS_INTEGER_TO_OCTETSTRING (u4OutIndex, (&OutSegmentIndex));

        /* 2.get the XCOwner is MPLS_OWNER_SNMP */
        if ((nmhGetMplsXCOwner (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                                &i4XCOwner)) == SNMP_FAILURE)
        {
            u4PrevFtnIndex = u4FtnIndex;
            continue;
        }
        if (u4OutIndex != 0)
        {
            /* get OutSegmentLabel and OutSegmentNextHopAddr */
            if ((nmhGetMplsOutSegmentTopLabel (&OutSegmentIndex,
                                               &u4OutSegLabel)) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%%Unable to get OutSegment Top Label");
                i1RetVal = SNMP_FAILURE;
                break;
            }

#ifdef MPLS_IPV6_WANTED
            if (Prefix.u2AddrType == MPLS_IPV6_ADDR_TYPE)
            {
                if ((nmhGetMplsOutSegmentNextHopAddr (&OutSegmentIndex,
                                                      &NextHopIpv6Addr)) ==
                    SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n%%Unable to get OutSegment Ipv6 Next Hop Addr");
                    i1RetVal = SNMP_FAILURE;
                    break;
                }

                MEMCPY (Ipv6NextHopAddr.u1_addr, NextHopIpv6Addr.pu1_OctetList,
                        DestIpv6AddrMin.i4_Length);

                i4NextHopCmpRet =
                    MEMCMP (Ipv6NextHopAddr.u1_addr,
                            MPLS_IPV6_U4_ADDR (NextHop.Addr), IPV6_ADDR_LENGTH);

            }
            else
            {
#endif
                if ((nmhGetMplsOutSegmentNextHopAddr (&OutSegmentIndex,
                                                      &NextHopAddr)) ==
                    SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r\n%%Unable to get OutSegment Ipv4 Next Hop Addr");
                    i1RetVal = SNMP_FAILURE;
                    break;
                }
                MPLS_OCTETSTRING_TO_INTEGER ((&NextHopAddr), u4NextHopAddr);
#ifdef MPLS_IPV6_WANTED
            }
#endif
        }
        else
        {
            u4PrevFtnIndex = u4FtnIndex;
            continue;
        }

        /*   3.compare XCOwner with MPLS_OWNER_SNMP and label with 
         *   OutSegmntLabel and next hop addr OutSegmntNextHopAddr*/
        if ((i4XCOwner == MPLS_OWNER_SNMP) && (u4Label == u4OutSegLabel) &&
#ifdef MPLS_IPV6_WANTED
            (
#endif
                (MPLS_IPV4_U4_ADDR (NextHop.Addr) == u4NextHopAddr)
#ifdef MPLS_IPV6_WANTED
                || (i4NextHopCmpRet == 0))
#endif
            )
        {

            bEntryPresent = TRUE;
            /* delete ftn entry and xc entry */
            if ((nmhTestv2MplsFTNRowStatus (&u4ErrCode, u4FtnIndex,
                                            MPLS_STATUS_DESTROY)) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_FTN_ENTRY_DELETION);
                i1RetVal = SNMP_FAILURE;
                break;
            }
            if ((nmhSetMplsFTNRowStatus (u4FtnIndex, MPLS_STATUS_DESTROY))
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_FTN_ENTRY_DELETION);
                i1RetVal = SNMP_FAILURE;
                break;
            }
            if ((nmhTestv2MplsXCRowStatus (&u4ErrCode, &XCIndex,
                                           &InSegmentIndex,
                                           &OutSegmentIndex,
                                           MPLS_STATUS_DESTROY)) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_XC_ENTRY_DELETION);
                i1RetVal = SNMP_FAILURE;
                break;
            }
            if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex,
                                        &OutSegmentIndex,
                                        MPLS_STATUS_DESTROY)) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_XC_ENTRY_DELETION);
                i1RetVal = SNMP_FAILURE;
                break;
            }

            /* get the interface index b4 deleting the out seg entry */
            if ((nmhGetMplsOutSegmentInterface (&OutSegmentIndex,
                                                &i4IfIndex)) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "%%Unable to fetch the outsegment interface\r\n");
                i1RetVal = SNMP_FAILURE;
                break;
            }
            if ((nmhTestv2MplsOutSegmentRowStatus (&u4ErrCode,
                                                   &OutSegmentIndex,
                                                   MPLS_STATUS_DESTROY)) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_OUTSEG_ENTRY_DELETION);
                i1RetVal = SNMP_FAILURE;
                break;
            }
            if ((nmhSetMplsOutSegmentRowStatus
                 (&OutSegmentIndex, MPLS_STATUS_DESTROY)) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_OUTSEG_ENTRY_DELETION);
                i1RetVal = SNMP_FAILURE;
                break;
            }

            if ((CfaUtilGetIfIndexFromMplsTnlIf
                 ((UINT4) i4IfIndex, &u4L3Intf, TRUE)) == CFA_FAILURE)
            {
                CliPrintf (CliHandle,
                           "%%Invalid interface index %d \r\n", i4IfIndex);
                i1RetVal = SNMP_FAILURE;
                break;
            }
            if (MplsDeleteMplsIfOrMplsTnlIf (u4L3Intf, (UINT4) i4IfIndex,
                                             CFA_MPLS_TUNNEL, MPLS_TRUE)
                == MPLS_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n%%Unable to delete the MPLS "
                           "tunnel interface\r\n");
                return CLI_FAILURE;
            }
            if (u4InIndex != 0)
            {

                /* get the interface index b4 deleting the out seg entry */
                if ((nmhGetMplsInSegmentRowStatus (&InSegmentIndex,
                                                   &i4InStatus))
                    == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "\r\n%%Unable to fetch the "
                               "Insegment row status\r\n");
                    i1RetVal = SNMP_FAILURE;
                    break;
                }
                if ((nmhGetMplsInSegmentInterface (&InSegmentIndex,
                                                   &i4IfIndex)) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "%%Wrong Insegment interface index.\r\n");
                    i1RetVal = SNMP_FAILURE;
                    break;
                }
                if ((i4InStatus == MPLS_STATUS_NOT_READY) && (i4IfIndex != 0))
                {

                    if ((CfaUtilGetIfIndexFromMplsTnlIf ((UINT4) i4IfIndex,
                                                         &u4L3Intf, TRUE))
                        == CFA_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "%%Invalid interface index %d \r\n",
                                   i4IfIndex);
                        i1RetVal = SNMP_FAILURE;
                        break;
                    }
                    if (MplsDeleteMplsIfOrMplsTnlIf (u4L3Intf,
                                                     (UINT4) i4IfIndex,
                                                     CFA_MPLS_TUNNEL,
                                                     MPLS_TRUE) == MPLS_FAILURE)
                    {
                        CliPrintf (CliHandle, "\r\n%%Unable to delete the MPLS "
                                   "tunnel interface\r\n");
                        i1RetVal = SNMP_FAILURE;
                        break;
                    }
                }
                if ((nmhTestv2MplsInSegmentRowStatus (&u4ErrCode,
                                                      &InSegmentIndex,
                                                      MPLS_STATUS_DESTROY)) ==
                    SNMP_FAILURE)
                {
                    CLI_SET_ERR (MPLS_CLI_INSEG_ENTRY_DELETION);
                    i1RetVal = SNMP_FAILURE;
                    break;
                }
                if ((nmhSetMplsInSegmentRowStatus
                     (&InSegmentIndex, MPLS_STATUS_DESTROY)) == SNMP_FAILURE)
                {
                    CLI_SET_ERR (MPLS_CLI_INSEG_ENTRY_DELETION);
                    i1RetVal = SNMP_FAILURE;
                    break;
                }
            }
        }

        break;
    }

    if (bEntryPresent == FALSE)
    {
        CLI_SET_ERR (MPLS_CLI_NO_ENTRY);
        return CLI_FAILURE;
    }

    if (i1RetVal == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;

}

/******************************************************************************
 * Function Name : MplsCliFtnNoLabelBinding 
 * Description   : This routine is used for removing the static binding of 
 *                 labels to IPv4 
 * Input(s)      : CliHandle  - Cli Context Handle
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 ******************************************************************************/
INT1
MplsCliFtnNoLabelBinding (tCliHandle CliHandle, UINT2 u2AddrType)
{
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OID_TYPE      ActionPointer;
    INT4                i4XCOwner = 0;
    INT4                i4IfIndex = 0;
    UINT4               u4FtnIndex = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4XCIndex = 0;
    UINT4               u4InIndex = 0;
    UINT4               u4PrevFtnIndex = 0;
    UINT4               u4OutIndex = 0;
    UINT4               u4L3Intf = 0;
    INT4                i4ActionType = 0;
    INT4                i4InStatus = 0;
    INT4                i4OutStatus = 0;
    INT4                i4FTNAddrType = 0;

    static UINT4        au4ActionPointer[MPLS_TE_XC_TABLE_OFFSET];
    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];
    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    XCIndex.pu1_OctetList = au1XCIndex;
    XCIndex.i4_Length = MPLS_INDEX_LENGTH;
    ActionPointer.pu4_OidList = au4ActionPointer;

    /* get the first index of ftn table */
    if ((nmhGetFirstIndexMplsFTNTable (&u4FtnIndex)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%No entry present\r\n");
        return CLI_FAILURE;
    }

    do
    {
        /* scan the ftn table */
        if ((nmhGetMplsFTNActionPointer (u4FtnIndex, &ActionPointer))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to fetch the FTN "
                       "action pointer\r\n");
            return CLI_FAILURE;
        }
        if ((nmhGetMplsFTNActionType (u4FtnIndex, &i4ActionType))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to fetch the FTN "
                       "action type\r\n");
            return CLI_FAILURE;
        }
        if (i4ActionType != REDIRECTLSP)
        {
            continue;
        }

        /*Get the FTN Type, Ipv4 or Ipv6 */
        if ((nmhGetMplsFTNAddrType (u4FtnIndex, &i4FTNAddrType))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to fetch the FTN "
                       "FTNAddrType\r\n");
            return CLI_FAILURE;
        }

        if (u2AddrType != (UINT2) i4FTNAddrType)
        {
            u4PrevFtnIndex = u4FtnIndex;
            continue;
        }

        /* from the action pointer get the InSegment , OutSegment and XC table 
         * index */
        MPLS_OID_TO_INTEGER ((&ActionPointer), u4XCIndex,
                             MPLS_XC_INDEX_START_OFFSET);
        MPLS_INTEGER_TO_OCTETSTRING (u4XCIndex, (&XCIndex));
        MPLS_OID_TO_INTEGER ((&ActionPointer), u4InIndex,
                             MPLS_IN_INDEX_START_OFFSET);
        MPLS_INTEGER_TO_OCTETSTRING (u4InIndex, (&InSegmentIndex));
        MPLS_OID_TO_INTEGER ((&ActionPointer), u4OutIndex,
                             MPLS_OUT_INDEX_START_OFFSET);
        MPLS_INTEGER_TO_OCTETSTRING (u4OutIndex, (&OutSegmentIndex));

        /* get the XCOwner */
        if ((nmhGetMplsXCOwner (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                                &i4XCOwner)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Unable to get XC Owner\n");
            return CLI_FAILURE;
        }
        if (i4XCOwner == MPLS_OWNER_SNMP)
        {
            /* if owner is MPLS_OWNER_SNMP delete ftn entry and xc entry */
            if ((nmhTestv2MplsFTNRowStatus
                 (&u4ErrCode, u4FtnIndex, MPLS_STATUS_DESTROY)) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_FTN_ENTRY_DELETION);
                return CLI_FAILURE;
            }
            if ((nmhSetMplsFTNRowStatus (u4FtnIndex, MPLS_STATUS_DESTROY)) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_FTN_ENTRY_DELETION);
                return CLI_FAILURE;
            }
            if ((nmhTestv2MplsXCRowStatus (&u4ErrCode, &XCIndex,
                                           &InSegmentIndex, &OutSegmentIndex,
                                           MPLS_STATUS_DESTROY)) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_XC_ENTRY_DELETION);
                return CLI_FAILURE;
            }
            if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex,
                                        &OutSegmentIndex,
                                        MPLS_STATUS_DESTROY)) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_XC_ENTRY_DELETION);
                return CLI_FAILURE;
            }

            /* 4. delete in and out segment entries if link is present */
            if (u4InIndex != 0)
            {
                /* get the interface index b4 deleting the In seg entry */
                if ((nmhGetMplsInSegmentRowStatus (&InSegmentIndex,
                                                   &i4InStatus))
                    == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "\r\n%%Unable to fetch the "
                               "Insegment row status\r\n");
                    return CLI_FAILURE;
                }
                /* delete the out interface also */
                if ((nmhGetMplsInSegmentInterface (&InSegmentIndex,
                                                   &i4IfIndex)) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "%%Wrong Insegment interface index.\r\n");
                    return CLI_FAILURE;
                }
                if ((i4InStatus == MPLS_STATUS_NOT_READY) && (i4IfIndex != 0))
                {
                    if ((CfaUtilGetIfIndexFromMplsTnlIf
                         ((UINT4) i4IfIndex, &u4L3Intf, TRUE)) == CFA_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "%%Invalid interface index %d \r\n",
                                   i4IfIndex);
                        return CLI_FAILURE;
                    }
                    if (MplsDeleteMplsIfOrMplsTnlIf (u4L3Intf,
                                                     (UINT4) i4IfIndex,
                                                     CFA_MPLS_TUNNEL,
                                                     MPLS_TRUE) == MPLS_FAILURE)
                    {
                        CliPrintf (CliHandle, "\r\n%%Unable to delete the MPLS "
                                   "tunnel interface\r\n");
                        return CLI_FAILURE;
                    }
                }
                if ((nmhTestv2MplsInSegmentRowStatus (&u4ErrCode,
                                                      &InSegmentIndex,
                                                      MPLS_STATUS_DESTROY)) ==
                    SNMP_FAILURE)
                {
                    CLI_SET_ERR (MPLS_CLI_INSEG_ENTRY_DELETION);
                    return CLI_FAILURE;
                }
                if ((nmhSetMplsInSegmentRowStatus
                     (&InSegmentIndex, MPLS_STATUS_DESTROY)) == SNMP_FAILURE)
                {
                    CLI_SET_ERR (MPLS_CLI_INSEG_ENTRY_DELETION);
                    return CLI_FAILURE;
                }
            }
            if (u4OutIndex != 0)
            {
                /* get the interface index b4 deleting the out seg entry */
                if ((nmhGetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                                    &i4OutStatus))
                    == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "%%Unable to get the Outsegment row status");
                    return CLI_FAILURE;
                }
                /* delete the out interface also */
                if ((nmhGetMplsOutSegmentInterface (&OutSegmentIndex,
                                                    &i4IfIndex)) ==
                    SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "%%Unable to fetch the outsegment interface\r\n");
                    return CLI_FAILURE;
                }
                if ((i4OutStatus == MPLS_STATUS_NOT_READY) && (i4IfIndex != 0))
                {
                    if ((CfaUtilGetIfIndexFromMplsTnlIf
                         ((UINT4) i4IfIndex, &u4L3Intf, TRUE)) == CFA_FAILURE)
                    {
                        CliPrintf (CliHandle,
                                   "%%Invalid interface index %d \r\n",
                                   i4IfIndex);
                        return CLI_FAILURE;
                    }
                    if (MplsDeleteMplsIfOrMplsTnlIf (u4L3Intf,
                                                     (UINT4) i4IfIndex,
                                                     CFA_MPLS_TUNNEL,
                                                     MPLS_TRUE) == MPLS_FAILURE)
                    {
                        CliPrintf (CliHandle, "\r\n%%Unable to delete the MPLS "
                                   "tunnel interface\r\n");
                        return CLI_FAILURE;
                    }
                }
                if ((nmhTestv2MplsOutSegmentRowStatus (&u4ErrCode,
                                                       &OutSegmentIndex,
                                                       MPLS_STATUS_DESTROY)) ==
                    SNMP_FAILURE)
                {
                    CLI_SET_ERR (MPLS_CLI_OUTSEG_ENTRY_DELETION);
                    return CLI_FAILURE;
                }
                if ((nmhSetMplsOutSegmentRowStatus
                     (&OutSegmentIndex, MPLS_STATUS_DESTROY)) == SNMP_FAILURE)
                {
                    CLI_SET_ERR (MPLS_CLI_OUTSEG_ENTRY_DELETION);
                    return CLI_FAILURE;
                }
            }
        }
        u4PrevFtnIndex = u4FtnIndex;
    }
    while ((nmhGetNextIndexMplsFTNTable (u4PrevFtnIndex, &u4FtnIndex)) !=
           SNMP_FAILURE);
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : MplsCliShowStaticBinding  
 * Description   : This routine displays the static prefix-label binding 
 * Input(s)      : CliHandle  - Cli Context Handle
 *                 u4Flag     - Flag to indicate which bit(s) 
 *                              set for CLI display   
 *                 u4SBPrefix - This value is taken only when prefix 
 *                              bit is set in the Flag.   
 *                 u4SBMask   - This value is taken only when prefix
 *                              bit is set in the Flag and based on the 
 *                              prefix and mask, entry is searched.
 *                 u4NextHop  - To find the entry matches with this NextHop 
 *                              and for this operation NextHop bit must be set
 *                 u4StaticFlag - Static Flag - If Set displays only static entries
 *                                            - If not set displayes all entries.  
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 ******************************************************************************/
INT1
MplsCliShowStaticBinding (tCliHandle CliHandle, UINT4 u4Flag,
                          UINT4 u4SBPrefix, UINT4 u4SBMask, UINT4 u4NextHop,
                          UINT4 u4StaticFlag)
{
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE InSegmentIndexPrev;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndexPrev;
    tSNMP_OCTET_STRING_TYPE LblStkIndex;
    tSNMP_OCTET_STRING_TYPE DestAddrMin;
    tSNMP_OCTET_STRING_TYPE DestAddrMax;
    tSNMP_OCTET_STRING_TYPE NextHopAddr;
    tSNMP_OCTET_STRING_TYPE Alias;
    tSNMP_OCTET_STRING_TYPE XCIndexPrev;
    tSNMP_OID_TYPE      ActionPointer;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tFtnEntry          *pFtnEntry = NULL;
    INT4                i4IfIndex = 0;
    UINT4               u4L3VlanIf = 0;
    CHR1               *pc1Prefix = NULL;
    INT4                i4XCOwner;
    UINT4               u4FtnIndex = 0;
    UINT4               u4XCIndex = 0;
    UINT4               u4InIndex = 0;
    UINT4               u4OutIndex = 0;
    UINT4               u4NextHopAddr = 0;
    UINT4               u4MinAddr = 0;
    UINT4               u4MaxAddr = 0;
    UINT4               u4Prefix = 0;
    UINT4               u4Mask = 0;
    UINT4               u4InSegLabel = 0;
    UINT4               u4OutSegLabel = MPLS_INVALID_LABEL;
    UINT4               u4OutStackLabel = MPLS_INVALID_LABEL;
    UINT4               u4LblStkIndex = 0;
    INT4                i4InStatus = 0;
    INT4                i4OutStatus = 0;
    INT4                i4MplsActionType = 0;
    INT4                i4FTNAddrType = 0;
    static UINT4        au4ActionPointer[MPLS_TE_XC_TABLE_OFFSET];
    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1InSegIndexPrev[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndexPrev[MPLS_INDEX_LENGTH];
    static UINT1        au1LblStkIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndexPrev[MPLS_INDEX_LENGTH];
    static UINT1        au1NextHopAddr[IPV6_ADDR_LENGTH];
    static UINT1        au1DestAddrMin[IPV6_ADDR_LENGTH];
    static UINT1        au1DestAddrMax[IPV6_ADDR_LENGTH];
    static UINT1        au1Alias[CFA_MAX_PORT_NAME_LENGTH];
    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndexPrev.pu1_OctetList = au1InSegIndexPrev;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndexPrev.pu1_OctetList = au1OutSegIndexPrev;
    Alias.pu1_OctetList = au1Alias;
    LblStkIndex.pu1_OctetList = au1LblStkIndex;
    XCIndex.pu1_OctetList = au1XCIndex;
    XCIndexPrev.pu1_OctetList = au1XCIndexPrev;
    NextHopAddr.pu1_OctetList = au1NextHopAddr;
    DestAddrMax.pu1_OctetList = au1DestAddrMax;
    DestAddrMin.pu1_OctetList = au1DestAddrMin;
    ActionPointer.pu4_OidList = au4ActionPointer;
    /* 1. scan the ftn table and get the XC index for each entry */
    /* get the first index of ftn table */

    while (1)
    {
        u4OutStackLabel = MPLS_INVALID_LABEL;
        u4OutSegLabel = MPLS_INVALID_LABEL;

        if ((u4FtnIndex != 0) &&
            ((nmhGetNextIndexMplsFTNTable (u4FtnIndex, &u4FtnIndex)) ==
             SNMP_FAILURE))
        {
            break;
        }

        /* Only executed first time */
        if ((u4FtnIndex == 0) &&
            ((nmhGetFirstIndexMplsFTNTable (&u4FtnIndex)) == SNMP_FAILURE))
        {
            return CLI_SUCCESS;
        }
        if ((nmhGetMplsFTNAddrType (u4FtnIndex, &i4FTNAddrType))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to fetch the FTN "
                       "FTNAddrType\r\n");
            return CLI_FAILURE;
        }

        if (i4FTNAddrType != MPLS_IPV4_ADDR_TYPE)
        {
            continue;
        }

        /* get min and max dest address....get prefix and mask from that */
        if ((nmhGetMplsFTNDestAddrMin (u4FtnIndex, &DestAddrMin))
            == SNMP_FAILURE)
        {
            continue;
        }
        MPLS_OCTETSTRING_TO_INTEGER ((&DestAddrMin), u4MinAddr);
        if ((nmhGetMplsFTNDestAddrMax (u4FtnIndex, &DestAddrMax))
            == SNMP_FAILURE)
        {
            continue;
        }
        MPLS_OCTETSTRING_TO_INTEGER ((&DestAddrMax), u4MaxAddr);
        /* TODO */
        /*
           u4Prefix = (u4MaxAddr & u4MinAddr);
           u4Mask = (u4MaxAddr & u4MinAddr) | (~(u4MinAddr));
         */
        u4Prefix = u4MinAddr;
        u4Mask = u4MaxAddr;

        if ((nmhGetMplsFTNActionPointer (u4FtnIndex, &ActionPointer))
            == SNMP_FAILURE)
        {
            continue;
        }
        if (ActionPointer.u4_Length == 2)
        {
            CLI_CONVERT_IPADDR_TO_STR (pc1Prefix, u4Prefix);
            CliPrintf (CliHandle, "%s/%d: Incoming Label: none;\n"
                       " Outgoing Labels: none\n", pc1Prefix,
                       CliGetMaskBits (u4Mask));
            continue;
        }
        MPLS_CMN_LOCK ();
        if ((pFtnEntry = MplsGetFtnTableEntry (u4FtnIndex)) == NULL)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "GetMplsFTNActionType Failed :No Entry Present\t\n");
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }
        MPLS_CMN_UNLOCK ();
        i4MplsActionType = pFtnEntry->i4ActionType;

        if (MPLS_FAILURE == MplsGetFtnXcValues (i4MplsActionType,
                                                &ActionPointer, &u4XCIndex,
                                                &u4InIndex, &u4OutIndex))
        {
            continue;
        }

        MPLS_INTEGER_TO_OCTETSTRING (u4XCIndex, (&XCIndex));
        MPLS_INTEGER_TO_OCTETSTRING (u4InIndex, (&InSegmentIndex));
        MPLS_INTEGER_TO_OCTETSTRING (u4OutIndex, (&OutSegmentIndex));

        /* 2.check if XCOwner is admin */
        if ((nmhGetMplsXCOwner (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                                &i4XCOwner)) == SNMP_FAILURE)
        {
            continue;
        }

        if (nmhGetMplsXCLabelStackIndex (&XCIndex, &InSegmentIndex,
                                         &OutSegmentIndex,
                                         &LblStkIndex) == SNMP_FAILURE)
        {
            continue;
        }
        MPLS_OCTETSTRING_TO_INTEGER ((&LblStkIndex), u4LblStkIndex);

        if ((u4StaticFlag == TRUE) && (i4XCOwner != MPLS_OWNER_SNMP))
        {
            continue;
        }

        if (((u4Flag & MPLS_SB_PREFIX) == MPLS_SB_PREFIX) &&
            ((u4Prefix != u4SBPrefix) || (u4Mask != u4SBMask)))
        {
            continue;
        }
        CLI_CONVERT_IPADDR_TO_STR (pc1Prefix, u4Prefix);

        if ((u4OutIndex != 0) && ((u4Flag & MPLS_SB_REMOTE) == MPLS_SB_REMOTE))
        {
            if ((nmhGetMplsOutSegmentNextHopAddr (&OutSegmentIndex,
                                                  &NextHopAddr)) ==
                SNMP_FAILURE)
            {
                continue;
            }
            MPLS_OCTETSTRING_TO_INTEGER ((&NextHopAddr), u4NextHopAddr);
        }
        if (((u4Flag & MPLS_SB_NEXTHOP) == MPLS_SB_NEXTHOP) &&
            (u4NextHopAddr != u4NextHop))
        {
            continue;
        }

        if (u4LblStkIndex != MPLS_ZERO)
        {
            if (nmhGetMplsLabelStackLabel (&LblStkIndex, MPLS_ONE,
                                           &u4OutStackLabel) == SNMP_FAILURE)
            {
                continue;
            }
        }

        if ((u4OutIndex != 0) && ((u4Flag & MPLS_SB_REMOTE) == MPLS_SB_REMOTE))
        {
            if ((nmhGetMplsOutSegmentRowStatus (&OutSegmentIndex, &i4OutStatus))
                == SNMP_FAILURE)
            {
                continue;
            }
            if (i4OutStatus == MPLS_STATUS_ACTIVE)
            {
                if ((nmhGetMplsOutSegmentTopLabel (&OutSegmentIndex,
                                                   &u4OutSegLabel))
                    == SNMP_FAILURE)
                {
                    continue;
                }

                CliPrintf (CliHandle, "%s/%d: Outgoing Labels:",
                           pc1Prefix, CliGetMaskBits (u4Mask));

                MplsCliDisplayOutLabels (CliHandle, u4NextHopAddr,
                                         u4OutSegLabel, u4OutStackLabel);
            }
            else
            {
                CliPrintf (CliHandle, " Outgoing Labels: None\r\n");
            }
        }
        else if ((u4Flag & MPLS_SB_REMOTE) == MPLS_SB_REMOTE)
        {
            CliPrintf (CliHandle, " Outgoing Labels: None\r\n");
        }
    }
    MEMSET (au1InSegIndex, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1XCIndex, 0, MPLS_INDEX_LENGTH);
    MEMSET (&u4XCIndex, 0, sizeof (UINT4));
    if ((u4Flag & MPLS_SB_LOCAL) == MPLS_SB_LOCAL)
    {
        while (1)
        {
            u4InSegLabel = MPLS_INVALID_LABEL;

            if ((u4XCIndex != 0) &&
                ((nmhGetNextIndexMplsXCTable
                  (&XCIndexPrev, &XCIndex, &InSegmentIndexPrev, &InSegmentIndex,
                   &OutSegmentIndexPrev, &OutSegmentIndex)) == SNMP_FAILURE))
            {
                break;
            }

            /* Only executed first time */
            if ((u4XCIndex == 0) &&
                ((nmhGetFirstIndexMplsXCTable (&XCIndex, &InSegmentIndex,
                                               &OutSegmentIndex)) ==
                 SNMP_FAILURE))
            {
                return CLI_SUCCESS;
            }

            /* Get first FTN entry */
            MEMCPY (XCIndexPrev.pu1_OctetList, XCIndex.pu1_OctetList,
                    XCIndex.i4_Length);
            XCIndexPrev.i4_Length = XCIndex.i4_Length;

            MEMCPY (InSegmentIndexPrev.pu1_OctetList,
                    InSegmentIndex.pu1_OctetList, InSegmentIndex.i4_Length);
            InSegmentIndexPrev.i4_Length = InSegmentIndex.i4_Length;

            MEMCPY (OutSegmentIndexPrev.pu1_OctetList,
                    OutSegmentIndex.pu1_OctetList, OutSegmentIndex.i4_Length);
            OutSegmentIndexPrev.i4_Length = OutSegmentIndex.i4_Length;

            MPLS_OCTETSTRING_TO_INTEGER ((&InSegmentIndex), u4InIndex);
            MPLS_OCTETSTRING_TO_INTEGER ((&OutSegmentIndex), u4OutIndex);
            MPLS_OCTETSTRING_TO_INTEGER ((&XCIndex), u4XCIndex);

            if (TeCheckXcIndex (u4XCIndex, &pTeTnlInfo) == TE_SUCCESS)
            {
                continue;
            }
            if (nmhGetMplsXCOwner (&XCIndex, &InSegmentIndex,
                                   &OutSegmentIndex,
                                   &i4XCOwner) == SNMP_FAILURE)
            {
                continue;
            }

            if ((u4OutIndex != 0) || (u4InIndex == 0))
            {
                continue;
            }

            if ((u4StaticFlag == TRUE) && (i4XCOwner != MPLS_OWNER_SNMP))
            {
                continue;
            }

            if (nmhGetMplsInSegmentLabel (&InSegmentIndex, &u4InSegLabel)
                == SNMP_FAILURE)
            {
                continue;
            }
            if (nmhGetMplsInSegmentInterface (&InSegmentIndex, &i4IfIndex)
                == SNMP_FAILURE)
            {
                continue;
            }
            if (CfaUtilGetIfIndexFromMplsTnlIf
                ((UINT4) i4IfIndex, &u4L3VlanIf, TRUE) == CFA_FAILURE)
            {
                continue;
            }
            if (nmhGetIfAlias (u4L3VlanIf, &Alias) == SNMP_FAILURE)
            {
                continue;
            }
            /* 4. print inseglabel if in seg entry is present */
            if ((u4InIndex != 0) && ((u4Flag & MPLS_SB_LOCAL) == MPLS_SB_LOCAL))
            {
                if ((nmhGetMplsInSegmentRowStatus
                     (&InSegmentIndex, &i4InStatus)) == SNMP_FAILURE)
                {
                    continue;
                }
                if (i4InStatus == MPLS_STATUS_ACTIVE)
                {
                    if ((nmhGetMplsInSegmentLabel
                         (&InSegmentIndex, &u4InSegLabel)) == SNMP_FAILURE)
                    {
                        continue;
                    }
                    CliPrintf (CliHandle, " Incoming Labels:\r\n");
                    switch (u4InSegLabel)
                    {
                        case MPLS_IPV4_EXPLICIT_NULL_LABEL:
                            CliPrintf (CliHandle, "  Label:\
                                    explicit-null\r\n");
                            break;
                        case MPLS_IMPLICIT_NULL_LABEL:
                            CliPrintf (CliHandle, "  Label:\
                                    implicit-null\r\n");
                            break;
                        default:
                            CliPrintf (CliHandle, "  Label:"
                                       " %d (in LIB)\r\n", u4InSegLabel);
                    }
                }
                else
                {
                    CliPrintf (CliHandle, " Incoming Labels:\r\n");
                    CliPrintf (CliHandle, "  Label: none;\r\n");

                }
            }
            else if ((u4Flag & MPLS_SB_LOCAL) == MPLS_SB_LOCAL)
            {
                CliPrintf (CliHandle, " Incoming Label: none;\r\n");
            }
            Alias.pu1_OctetList[Alias.i4_Length] = '\0';
            CliPrintf (CliHandle, "  %-8s\n", Alias.pu1_OctetList);
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : MplsCliStaticXC 
 * Description   : This routine is used for static binding of labels to IPv4 & 
 *                 IPv6 prefixes
 * Input(s)      : CliHandle  - Cli Context Handle
 *                 u4InIfIndex - L3 Incoming Interface Index
 *                 u4InLabel  - Insegment Label
 *                 NextHop  - NextHop ipv4/ipv6 address
 *                 u4OutLabel - OutSegment Label
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 ******************************************************************************/
INT1
MplsCliStaticXC (tCliHandle CliHandle, UINT4 u4InIfIndex, UINT4 u4InLabel,
                 tGenU4Addr NextHop, UINT4 u4OutLabel, UINT4 u4OutIfIndex)
{
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OCTET_STRING_TYPE NextHopAddr;
    INT4                i4InSegmntNPop;
    UINT4               u4ErrCode = 0;
    UINT4               u4MplsTnlInIfIndex = 0;
    UINT4               u4MplsTnlOutIfIndex = 0;
    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1NextHopAddr[MPLS_INDEX_LENGTH];

    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;
    UINT4               u4Mask = 0xffffffff;
#ifdef CFA_WANTED
    UINT4               u4MplsIfIndex = 0;
#endif

#ifdef MPLS_IPV6_WANTED
    UINT4               u4IfIndex = 0;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tNetIpv6RtInfoQueryMsg Ipv6RtQuery;
    UINT1               u1DestPrefixLen = 128;
    UINT1               u1Scope = 0;

    tSNMP_OCTET_STRING_TYPE NextHopIpv6Addr;
    static UINT1        au1NextHopIpv6Addr[IPV6_ADDR_LENGTH];
    NextHopIpv6Addr.pu1_OctetList = au1NextHopIpv6Addr;
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&Ipv6RtQuery, 0, sizeof (tNetIpv6RtInfoQueryMsg));
#endif

    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    XCIndex.pu1_OctetList = au1XCIndex;
    XCIndex.i4_Length = MPLS_INDEX_LENGTH;
    NextHopAddr.pu1_OctetList = au1NextHopAddr;

    MEMSET (au1InSegIndex, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1XCIndex, 0, MPLS_INDEX_LENGTH);
    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

#ifdef MPLS_IPV6_WANTED
    if (NextHop.u2AddrType == MPLS_IPV6_ADDR_TYPE)
    {
        /* Return failure if Next hop is LLA/SLA and OutInterface is Not given */
        u1Scope = Ip6GetAddrScope (&(NextHop.Addr.Ip6Addr));

        if ((u1Scope == ADDR6_SCOPE_LLOCAL)
            || (u1Scope == ADDR6_SCOPE_SITELOCAL))
        {
            if (u4OutIfIndex == 0)
            {
                CliPrintf (CliHandle,
                           "\r%% Out Interface is not given for Ipv6 Next Hop Address\n");
                return CLI_FAILURE;
            }
        }
        else
        {
#if 0
            if (LdpIpv6IsAddrLocal (MPLS_IPV6_U4_ADDR (NextHop.Addr)) ==
                LDP_TRUE)
#endif
                if (NetIpv6IsOurAddress (&(NextHop.Addr.Ip6Addr), &u4IfIndex) ==
                    NETIPV6_SUCCESS)
                {
                    CliPrintf (CliHandle,
                               "\r%% Next hop is same as self-ip IPv6 address\n");
                    return CLI_FAILURE;
                }

            Ipv6RtQuery.u4ContextId = VCM_DEFAULT_CONTEXT;
            Ipv6RtQuery.u1QueryFlag = RTM6_QUERIED_FOR_NEXT_HOP;

            MEMCPY (NetIpv6RtInfo.Ip6Dst.u1_addr,
                    MPLS_IPV6_U4_ADDR (NextHop.Addr), IPV6_ADDR_LENGTH);

            NetIpv6RtInfo.u1Prefixlen = u1DestPrefixLen;

            if (NetIpv6GetRoute (&Ipv6RtQuery, &NetIpv6RtInfo) ==
                NETIPV6_SUCCESS)
            {
                if (NetIpv6GetCfaIfIndexFromPort (NetIpv6RtInfo.u4Index,
                                                  &u4OutIfIndex) ==
                    NETIPV6_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%% Unable to fetch Next hop information\n");
                    return CLI_FAILURE;
                }
            }
        }
    }
    else
    {
#endif
        if (NetIpv4IfIsOurAddress (MPLS_IPV4_U4_ADDR (NextHop.Addr)) ==
            NETIPV4_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%% Next hop is same as self-ip address\n");
            return CLI_FAILURE;
        }

        RtQuery.u4DestinationIpAddress = MPLS_IPV4_U4_ADDR (NextHop.Addr);
        RtQuery.u4DestinationSubnetMask = u4Mask;
        RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;

        if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_SUCCESS)
        {
            if (NetIpv4GetCfaIfIndexFromPort (NetIpRtInfo.u4RtIfIndx,
                                              &u4OutIfIndex) == NETIPV4_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Unable to fetch Next hop information\n");
                return CLI_FAILURE;
            }
        }
#ifdef CFA_WANTED
        else
        {
            if (CfaIpIfGetIfIndexFromHostIpAddressInCxt (MPLS_DEF_CONTEXT_ID,
                                                         MPLS_IPV4_U4_ADDR
                                                         (NextHop.Addr),
                                                         &u4OutIfIndex) ==
                CFA_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to reach next hop\n");
                return CLI_FAILURE;
            }
        }
        if (CfaUtilGetMplsIfFromIfIndex (u4OutIfIndex, &u4MplsIfIndex, TRUE) ==
            CFA_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_ERR_OUTSTACK_ENTRY_CREATION);
            return CLI_FAILURE;
        }
#endif

#ifdef MPLS_IPV6_WANTED
    }
#endif

    /* create a new in seg , out seg and XC table entry */
    if ((nmhGetMplsInSegmentIndexNext (&InSegmentIndex)) == SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_NO_FREE_INDEX);
        return CLI_FAILURE;
    }
    if ((nmhTestv2MplsInSegmentRowStatus (&u4ErrCode, &InSegmentIndex,
                                          MPLS_STATUS_CREATE_AND_WAIT)) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_INSEG_ENTRY_CREATION);
        return CLI_FAILURE;
    }
    if ((nmhSetMplsInSegmentRowStatus
         (&InSegmentIndex, MPLS_STATUS_CREATE_AND_WAIT)) == SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_INSEG_ENTRY_CREATION);
        return CLI_FAILURE;
    }

    /* create an OutSeg entry */
    if ((nmhGetMplsOutSegmentIndexNext (&OutSegmentIndex)) == SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_NO_FREE_INDEX);
        /* delete the created InSeg entry */
        nmhSetMplsInSegmentRowStatus (&InSegmentIndex, MPLS_STATUS_DESTROY);
        return CLI_FAILURE;
    }
    if ((nmhTestv2MplsOutSegmentRowStatus (&u4ErrCode, &OutSegmentIndex,
                                           MPLS_STATUS_CREATE_AND_WAIT)) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_OUTSEG_ENTRY_CREATION);
        /* delete the created InSeg entry */
        nmhSetMplsInSegmentRowStatus (&InSegmentIndex, MPLS_STATUS_DESTROY);
        return CLI_FAILURE;
    }
    if ((nmhSetMplsOutSegmentRowStatus
         (&OutSegmentIndex, MPLS_STATUS_CREATE_AND_WAIT)) == SNMP_FAILURE)
    {
        /* delete the created InSeg entry */
        nmhSetMplsInSegmentRowStatus (&InSegmentIndex, MPLS_STATUS_DESTROY);
        CLI_SET_ERR (MPLS_CLI_OUTSEG_ENTRY_CREATION);
        return CLI_FAILURE;
    }

    /* create an XC entry */
    if ((nmhGetMplsXCIndexNext (&XCIndex)) == SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_NO_FREE_INDEX);
        /* delete the created InSeg and OutSeg entry */
        if (nmhSetMplsInSegmentRowStatus (&InSegmentIndex, MPLS_STATUS_DESTROY)
            == SNMP_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "nmhSetMplsInSegmentRowStatus failed \t\n");
        }

        if (nmhSetMplsOutSegmentRowStatus
            (&OutSegmentIndex, MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "nmhSetMplsOutSegmentRowStatus  failed \t\n");
        }
        return CLI_FAILURE;
    }
    if ((nmhTestv2MplsXCRowStatus (&u4ErrCode, &XCIndex, &InSegmentIndex,
                                   &OutSegmentIndex,
                                   MPLS_STATUS_CREATE_AND_WAIT))
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_XC_ENTRY_CREATION);
        /* delete the created InSeg and OutSeg entry */
        if (nmhSetMplsInSegmentRowStatus (&InSegmentIndex, MPLS_STATUS_DESTROY)
            == SNMP_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "nmhSetMplsInSegmentRowStatus Failed \t\n");
        }
        if (nmhSetMplsOutSegmentRowStatus
            (&OutSegmentIndex, MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "nmhSetMplsOutSegmentRowStatus  Failed \t\n");
        }
        return CLI_FAILURE;
    }
    if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex,
                                &OutSegmentIndex,
                                MPLS_STATUS_CREATE_AND_WAIT)) == SNMP_FAILURE)
    {
        /* delete the created InSeg and OutSeg entry */
        if (nmhSetMplsInSegmentRowStatus (&InSegmentIndex, MPLS_STATUS_DESTROY)
            == SNMP_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "nmhSetMplsInSegmentRowStatus Failed \t\n");
        }

        if (nmhSetMplsOutSegmentRowStatus
            (&OutSegmentIndex, MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "nmhSetMplsOutSegmentRowStatus Failed \t\n");
        }
        return CLI_FAILURE;
    }

    do
    {
        /* OUTSEGMENT TABLE CONFIGURATION */
        /* 2. set inlabel value in seg table */
        if ((nmhTestv2MplsInSegmentLabel (&u4ErrCode, &InSegmentIndex,
                                          u4InLabel)) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_ERR_INSEG_LABEL);
            break;
        }
        if ((nmhSetMplsInSegmentLabel (&InSegmentIndex, u4InLabel))
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_ERR_INSEG_LABEL);
            break;
        }
        /* 3.set InSegment NPop */
        i4InSegmntNPop = 1;
        if ((nmhTestv2MplsInSegmentNPop (&u4ErrCode, &InSegmentIndex,
                                         i4InSegmntNPop)) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_ERR_INSEG_NPOP);
            break;
        }
        if ((nmhSetMplsInSegmentNPop (&InSegmentIndex, i4InSegmntNPop))
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_ERR_INSEG_NPOP);
            break;
        }

#ifdef MPLS_IPV6_WANTED
        if (NextHop.u2AddrType == MPLS_IPV6_ADDR_TYPE)
        {
            /* setting the InSegment ip addr family to ipv6 */
            if (nmhTestv2MplsInSegmentAddrFamily (&u4ErrCode, &InSegmentIndex,
                                                  LSR_ADDR_IPV6) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_ERR_INSEG_ADDR_FAMILY);
                break;
            }
            if (nmhSetMplsInSegmentAddrFamily (&InSegmentIndex,
                                               LSR_ADDR_IPV6) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_ERR_INSEG_ADDR_FAMILY);
                break;
            }
        }
        else
        {
#endif
            /* setting the InSegment ip addr family to ipv4 */
            if (nmhTestv2MplsInSegmentAddrFamily (&u4ErrCode, &InSegmentIndex,
                                                  LSR_ADDR_IPV4) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_ERR_INSEG_ADDR_FAMILY);
                break;
            }
            if (nmhSetMplsInSegmentAddrFamily (&InSegmentIndex, LSR_ADDR_IPV4)
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_ERR_INSEG_ADDR_FAMILY);
                break;
            }
#ifdef MPLS_IPV6_WANTED
        }
#endif
        /* OUTSEGMENT TABLE CONFIGURATION */
        /* 5. set out segmnt top label */
        if (nmhTestv2MplsOutSegmentTopLabel (&u4ErrCode, &OutSegmentIndex,
                                             u4OutLabel) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_ERR_OUTSEG_LABEL);
            break;
        }
        if (nmhSetMplsOutSegmentTopLabel (&OutSegmentIndex, u4OutLabel)
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_ERR_OUTSEG_LABEL);
            break;
        }
        /* 6. set out segmnt push top label to true */
        if (nmhTestv2MplsOutSegmentPushTopLabel (&u4ErrCode, &OutSegmentIndex,
                                                 MPLS_TRUE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_ERR_OUTSEG_PUSH_TOP_LABEL);
            break;
        }
        if (nmhSetMplsOutSegmentPushTopLabel (&OutSegmentIndex,
                                              MPLS_TRUE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_ERR_OUTSEG_PUSH_TOP_LABEL);
            break;
        }

#ifdef MPLS_IPV6_WANTED
        if (NextHop.u2AddrType == MPLS_IPV6_ADDR_TYPE)
        {
            /* set the next hop addr type to ipv6 */
            if (nmhTestv2MplsOutSegmentNextHopAddrType (&u4ErrCode,
                                                        &OutSegmentIndex,
                                                        LSR_ADDR_IPV6)
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_ERR_NEXT_HOP_ADDR_TYPE);
                break;
            }
            if (nmhSetMplsOutSegmentNextHopAddrType (&OutSegmentIndex,
                                                     LSR_ADDR_IPV6) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_ERR_NEXT_HOP_ADDR_TYPE);
                break;
            }

            /* 7. set nexthop IPv6 address */
            MEMCPY (NextHopIpv6Addr.pu1_OctetList,
                    MPLS_IPV6_U4_ADDR (NextHop.Addr), IPV6_ADDR_LENGTH);
            NextHopIpv6Addr.i4_Length = IPV6_ADDR_LENGTH;

            if (nmhTestv2MplsOutSegmentNextHopAddr
                (&u4ErrCode, &OutSegmentIndex,
                 &NextHopIpv6Addr) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_ERR_NEXT_HOP_ADDR);
                break;
            }
            if (nmhSetMplsOutSegmentNextHopAddr (&OutSegmentIndex,
                                                 &NextHopIpv6Addr) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_ERR_NEXT_HOP_ADDR);
                break;
            }
        }
        else
        {
#endif
            /* set the next hop addr type to ipv4 */
            if (nmhTestv2MplsOutSegmentNextHopAddrType (&u4ErrCode,
                                                        &OutSegmentIndex,
                                                        LSR_ADDR_IPV4)
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_ERR_NEXT_HOP_ADDR_TYPE);
                break;
            }
            if (nmhSetMplsOutSegmentNextHopAddrType (&OutSegmentIndex,
                                                     LSR_ADDR_IPV4) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_ERR_NEXT_HOP_ADDR_TYPE);
                break;
            }

            /* 7. set nexthop IPv4 address */
            if (NextHop.u2AddrType == IPV4)
            {
                MPLS_INTEGER_TO_OCTETSTRING (MPLS_IPV4_U4_ADDR (NextHop.Addr),
                                             (&NextHopAddr));

                if (nmhTestv2MplsOutSegmentNextHopAddr
                    (&u4ErrCode, &OutSegmentIndex,
                     &NextHopAddr) == SNMP_FAILURE)
                {
                    CLI_SET_ERR (MPLS_CLI_ERR_NEXT_HOP_ADDR);
                    break;
                }
                if (nmhSetMplsOutSegmentNextHopAddr (&OutSegmentIndex,
                                                     &NextHopAddr) ==
                    SNMP_FAILURE)
                {
                    CLI_SET_ERR (MPLS_CLI_ERR_NEXT_HOP_ADDR);
                    break;
                }
            }
#ifdef MPLS_IPV6_WANTED
        }
#endif

        if (CfaGetFreeInterfaceIndex (&u4MplsTnlInIfIndex, CFA_MPLS_TUNNEL) ==
            OSIX_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_NO_FREE_INDEX);
            break;
        }
        if ((nmhTestv2MplsInSegmentInterface (&u4ErrCode,
                                              &InSegmentIndex,
                                              (INT4) u4MplsTnlInIfIndex))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set insegment interface");
            break;
        }
        if ((nmhSetMplsInSegmentInterface (&InSegmentIndex,
                                           (INT4) u4MplsTnlInIfIndex))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set insegment interface");
            break;
        }
        if (MplsCreateMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL) == MPLS_FAILURE)
        {
            CliPrintf (CliHandle, "\r %%Please enable MPLS on the "
                       "Incoming interface !!!\r\n");
            break;
        }
        /* 9.set the row status of the three tables as active */
        if ((nmhTestv2MplsInSegmentRowStatus (&u4ErrCode, &InSegmentIndex,
                                              MPLS_STATUS_ACTIVE)) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r %%Unable to set In segment row status active");
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            break;
        }
        if ((nmhSetMplsInSegmentRowStatus (&InSegmentIndex, MPLS_STATUS_ACTIVE))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r %%Unable to set In segment row status active");
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            break;
        }
        if (CfaGetFreeInterfaceIndex (&u4MplsTnlOutIfIndex, CFA_MPLS_TUNNEL) ==
            OSIX_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_NO_FREE_INDEX);
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            break;
        }
        if ((nmhTestv2MplsOutSegmentInterface (&u4ErrCode,
                                               &OutSegmentIndex,
                                               (INT4) u4MplsTnlOutIfIndex))
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_ERR_OUTSEG_IF);
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            break;
        }
        if ((nmhSetMplsOutSegmentInterface (&OutSegmentIndex,
                                            (INT4) u4MplsTnlOutIfIndex))
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_ERR_OUTSEG_IF);
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            break;
        }
        /*4. Create a new MPLS Tunnel Interface and stack over MPLS If */
        if (MplsCreateMplsIfOrMplsTnlIf (u4OutIfIndex, u4MplsTnlOutIfIndex,
                                         CFA_MPLS_TUNNEL) == MPLS_FAILURE)
        {
            CliPrintf (CliHandle, "\r %%Please enable MPLS on the "
                       "Outgoing interface !!!\r\n");
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            break;
        }

        if ((nmhTestv2MplsOutSegmentRowStatus (&u4ErrCode, &OutSegmentIndex,
                                               MPLS_STATUS_ACTIVE)) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r %%Unable to set Out segment row status active");
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            MplsDeleteMplsIfOrMplsTnlIf (u4OutIfIndex, u4MplsTnlOutIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            break;
        }
        if ((nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                            MPLS_STATUS_ACTIVE)) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r %%Unable to set Out segment row status active");
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            MplsDeleteMplsIfOrMplsTnlIf (u4OutIfIndex, u4MplsTnlOutIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            break;
        }
        if ((nmhTestv2MplsXCRowStatus (&u4ErrCode, &XCIndex, &InSegmentIndex,
                                       &OutSegmentIndex,
                                       MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r %%Unable to set XC row status active");
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            MplsDeleteMplsIfOrMplsTnlIf (u4OutIfIndex, u4MplsTnlOutIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            break;
        }
        if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex,
                                    &OutSegmentIndex,
                                    MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r %%Unable to set XC row status active");
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            MplsDeleteMplsIfOrMplsTnlIf (u4OutIfIndex, u4MplsTnlOutIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            break;
        }
        /* all set operations have been successful */
        return CLI_SUCCESS;
    }
    while (0);
    /* delete the created InSeg and OutSeg and XC entry */
    if (nmhSetMplsInSegmentRowStatus (&InSegmentIndex, MPLS_STATUS_DESTROY)
        == SNMP_FAILURE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "nmhSetMplsInSegmentRowStatus Failed \t\n");
    }
    if (nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex, MPLS_STATUS_DESTROY)
        == SNMP_FAILURE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "nmhSetMplsOutSegmentRowStatus Failed \t\n");
    }
    if (nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               MPLS_STATUS_DESTROY) == SNMP_FAILURE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "nmhSetMPLSXCRowStatus Failed \t\n");
    }
    return CLI_FAILURE;
}

/******************************************************************************
* Function Name : MplsCliNoStaticXC  
* Description   : This routine removes the LFIB entry  
* Input(s)      : CliHandle       - Cli Context Handle
*                 u4InLabel       - the in label
*                 u4NextHop       - Nexthop address
*                 u4OutLabel      - the out label
* Output(s)     : None
* Return(s)     : CLI_SUCCESS/CLI_FAILURE
******************************************************************************/
INT1
MplsCliNoStaticXC (tCliHandle CliHandle, UINT4 u4InLabel, tGenU4Addr NextHop,
                   UINT4 u4OutLabel)
{
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OCTET_STRING_TYPE InSegmentIndexPrev;
    tSNMP_OCTET_STRING_TYPE XCIndexPrev;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndexPrev;
    tSNMP_OCTET_STRING_TYPE NextHopAddr;
    INT4                i4IfIndex = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4InSegLabel = 0;
    UINT4               u4NextHopAddr = 0;
    UINT4               u4OutSegLabel = 0;
    UINT4               u4XCIndex = 0;
    UINT4               u4InIndex = 0;
    UINT4               u4OutIndex = 0;
    INT1                i1RetVal = SNMP_SUCCESS;
    UINT4               u4L3Intf = 0;
    BOOL1               bEntryPresent = FALSE;
    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1NextHopAddr[MPLS_INDEX_LENGTH];
    static UINT1        au1InSegIndexPrev[IPV6_ADDR_LENGTH];
    static UINT1        au1OutSegIndexPrev[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndexPrev[MPLS_INDEX_LENGTH];
#ifdef MPLS_IPV6_WANTED

    INT4                i4NextHopCmpRet = 0;
    tIp6Addr            Ipv6NextHopAddr;

    tSNMP_OCTET_STRING_TYPE NextHopIpv6Addr;
    static UINT1        au1NextHopIpv6Addr[IPV6_ADDR_LENGTH];
    NextHopIpv6Addr.pu1_OctetList = au1NextHopIpv6Addr;
    MEMSET (&Ipv6NextHopAddr, 0, sizeof (tIp6Addr));

#endif

    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    XCIndex.pu1_OctetList = au1XCIndex;
    XCIndex.i4_Length = MPLS_INDEX_LENGTH;
    NextHopAddr.pu1_OctetList = au1NextHopAddr;
    InSegmentIndexPrev.pu1_OctetList = au1InSegIndexPrev;
    OutSegmentIndexPrev.pu1_OctetList = au1OutSegIndexPrev;
    XCIndexPrev.pu1_OctetList = au1XCIndexPrev;

    /* 1. scan the XC entry */
    /* get the first index of XC table , in seg and out seg table */
    if (nmhGetFirstIndexMplsXCTable (&XCIndex, &InSegmentIndex,
                                     &OutSegmentIndex) == SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_NO_ENTRY);
        return CLI_FAILURE;
    }
    do
    {

        MEMCPY (XCIndexPrev.pu1_OctetList, XCIndex.pu1_OctetList,
                XCIndex.i4_Length);
        XCIndexPrev.i4_Length = XCIndex.i4_Length;

        MEMCPY (InSegmentIndexPrev.pu1_OctetList, InSegmentIndex.pu1_OctetList,
                InSegmentIndex.i4_Length);
        InSegmentIndexPrev.i4_Length = InSegmentIndex.i4_Length;

        MEMCPY (OutSegmentIndexPrev.pu1_OctetList,
                OutSegmentIndex.pu1_OctetList, OutSegmentIndex.i4_Length);
        OutSegmentIndexPrev.i4_Length = OutSegmentIndex.i4_Length;

        MPLS_OCTETSTRING_TO_INTEGER ((&XCIndex), u4XCIndex);
        MPLS_OCTETSTRING_TO_INTEGER ((&InSegmentIndex), u4InIndex);
        MPLS_OCTETSTRING_TO_INTEGER ((&OutSegmentIndex), u4OutIndex);

        if (u4InIndex == MPLS_ZERO || u4OutIndex == MPLS_ZERO)
        {
            continue;
        }
        if (nmhGetMplsInSegmentLabel (&InSegmentIndex, &u4InSegLabel)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to fetch the InSegment Label entry\r\n");
            i1RetVal = SNMP_FAILURE;
            break;
        }
        if (nmhGetMplsOutSegmentTopLabel (&OutSegmentIndex,
                                          &u4OutSegLabel) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to fetch the OutSegment Top Label\r\n");
            i1RetVal = SNMP_FAILURE;
            break;
        }
#ifdef MPLS_IPV6_WANTED
        if (NextHop.u2AddrType == MPLS_IPV6_ADDR_TYPE)
        {
            if (nmhGetMplsOutSegmentNextHopAddr
                (&OutSegmentIndex, &NextHopIpv6Addr) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n%%Unable to fetch "
                           "the OutSegment next hop address\r\n");
                i1RetVal = SNMP_FAILURE;
                break;
            }

            MEMCPY (Ipv6NextHopAddr.u1_addr, NextHopIpv6Addr.pu1_OctetList,
                    NextHopIpv6Addr.i4_Length);

            i4NextHopCmpRet =
                MEMCMP (Ipv6NextHopAddr.u1_addr,
                        MPLS_IPV6_U4_ADDR (NextHop.Addr), IPV6_ADDR_LENGTH);
        }
        else
        {
#endif
            if (nmhGetMplsOutSegmentNextHopAddr (&OutSegmentIndex, &NextHopAddr)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n%%Unable to fetch "
                           "the OutSegment next hop address\r\n");
                i1RetVal = SNMP_FAILURE;
                break;
            }

            MPLS_OCTETSTRING_TO_INTEGER ((&NextHopAddr), u4NextHopAddr);

#ifdef MPLS_IPV6_WANTED
        }
#endif

        if ((u4InLabel == u4InSegLabel) && (u4OutLabel == u4OutSegLabel) &&
#ifdef MPLS_IPV6_WANTED
            (
#endif
                ((NextHop.u2AddrType == MPLS_IPV4_ADDR_TYPE) &&
                 (MPLS_IPV4_U4_ADDR (NextHop.Addr) == u4NextHopAddr))
#ifdef MPLS_IPV6_WANTED
                || ((NextHop.u2AddrType == MPLS_IPV6_ADDR_TYPE) &&
                    (i4NextHopCmpRet == 0)))
#endif
            )
        {
            bEntryPresent = TRUE;
            if ((nmhTestv2MplsXCRowStatus (&u4ErrCode, &XCIndex,
                                           &InSegmentIndex, &OutSegmentIndex,
                                           MPLS_STATUS_DESTROY)) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_XC_ENTRY_DELETION);
                i1RetVal = SNMP_FAILURE;
                break;
            }

            if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex,
                                        &OutSegmentIndex,
                                        MPLS_STATUS_DESTROY)) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_XC_ENTRY_DELETION);
                i1RetVal = SNMP_FAILURE;
                break;
            }
            if (nmhGetMplsInSegmentInterface (&InSegmentIndex, &i4IfIndex)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "%%Wrong Insegment interface index.\r\n");
                i1RetVal = SNMP_FAILURE;
                break;
            }

            if ((CfaUtilGetIfIndexFromMplsTnlIf
                 ((UINT4) i4IfIndex, &u4L3Intf, TRUE)) == CFA_FAILURE)
            {
                CliPrintf (CliHandle,
                           "%%Invalid interface index %d \r\n", i4IfIndex);
                i1RetVal = SNMP_FAILURE;
                break;
            }
            if (MplsDeleteMplsIfOrMplsTnlIf (u4L3Intf, (UINT4) i4IfIndex,
                                             CFA_MPLS_TUNNEL, MPLS_TRUE)
                == MPLS_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n%%Unable to delete the MPLS "
                           "tunnel interface\r\n");
                return CLI_FAILURE;
            }

            if ((nmhTestv2MplsInSegmentRowStatus (&u4ErrCode,
                                                  &InSegmentIndex,
                                                  MPLS_STATUS_DESTROY)) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_INSEG_ENTRY_DELETION);
                i1RetVal = SNMP_FAILURE;
                break;
            }
            if ((nmhSetMplsInSegmentRowStatus
                 (&InSegmentIndex, MPLS_STATUS_DESTROY)) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_INSEG_ENTRY_DELETION);
                i1RetVal = SNMP_FAILURE;
                break;
            }
            if (nmhGetMplsOutSegmentInterface (&OutSegmentIndex, &i4IfIndex)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "%%Unable to fetch the outsegment interface\r\n");
                i1RetVal = SNMP_FAILURE;
                break;
            }

            if ((CfaUtilGetIfIndexFromMplsTnlIf
                 ((UINT4) i4IfIndex, &u4L3Intf, TRUE)) == CFA_FAILURE)
            {
                CliPrintf (CliHandle,
                           "%%Invalid interface index %d \r\n", i4IfIndex);
                i1RetVal = SNMP_FAILURE;
                break;
            }
            if (MplsDeleteMplsIfOrMplsTnlIf (u4L3Intf, (UINT4) i4IfIndex,
                                             CFA_MPLS_TUNNEL, MPLS_TRUE)
                == MPLS_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%%Unable to delete the MPLS tunnel interface\r\n");
                return CLI_FAILURE;
            }

            if ((nmhTestv2MplsOutSegmentRowStatus (&u4ErrCode,
                                                   &OutSegmentIndex,
                                                   MPLS_STATUS_DESTROY)) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_OUTSEG_ENTRY_DELETION);
                i1RetVal = SNMP_FAILURE;
                break;
            }
            if ((nmhSetMplsOutSegmentRowStatus
                 (&OutSegmentIndex, MPLS_STATUS_DESTROY)) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_OUTSEG_ENTRY_DELETION);
                i1RetVal = SNMP_FAILURE;
                break;
            }
        }
    }
    while (nmhGetNextIndexMplsXCTable
           (&XCIndexPrev, &XCIndex, &InSegmentIndexPrev, &InSegmentIndex,
            &OutSegmentIndexPrev, &OutSegmentIndex) != SNMP_FAILURE);

    if (i1RetVal == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (bEntryPresent == FALSE)
    {
        CliPrintf (CliHandle, "\r%%No Entry Present\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : MplsCliShowStaticCrossConnect  
* Description   : This routine displays the information about the crossconnects
*                 that has been configured  
* Input(s)      : CliHandle   - Cli Context Handle
*                 u4Flag      - To indicate which bits(low label | high label) 
*                               have set in the flag
*                 u4LowLabel  - Lower Label
*                 u4HighLabel - Higher Label
*                 u4StaticFlag - Static Flag - If set displays only 
*                                              static entries.
*                                              If not displays all entries.
* Output(s)     : None
* Return(s)     : CLI_SUCCESS/CLI_FAILURE
******************************************************************************/
INT1
MplsCliShowStaticCrossConnect (tCliHandle CliHandle, UINT4 u4Flag,
                               UINT4 u4LowLabel, UINT4 u4HighLabel,
                               UINT4 u4StaticFlag)
{
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OCTET_STRING_TYPE InSegmentIndexPrev;
    tSNMP_OCTET_STRING_TYPE XCIndexPrev;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndexPrev;
    tSNMP_OCTET_STRING_TYPE NextHopAddr;
    tSNMP_OCTET_STRING_TYPE Alias;
    tSNMP_OCTET_STRING_TYPE OutLblStkIndex;
    tSNMP_OCTET_STRING_TYPE TmpLblStkIndex;
    tSNMP_OID_TYPE      ActionPointer;
    tSNMP_OID_TYPE      InLblStkIndex;
    CHR1               *pc1NextHop = NULL;
    INT4                i4XCOwner;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4OutSegTopLabel = MPLS_INVALID_LABEL;
    UINT4               u4OutStackLabel = MPLS_INVALID_LABEL;
    UINT4               u4FtnIndex = 0;
    UINT4               u4FtnXCIndex = 0;
    UINT4               u4FtnInIndex = 0;
    UINT4               u4FtnOutIndex = 0;
    UINT4               u4InLblStkIndex = 0;
    UINT4               u4OutLblStkIndex = 0;
    INT4                i4IfIndex = 0;
    INT4                i4NextHopAddrType = LSR_ADDR_UNKNOWN;
    UINT4               u4OutIndex = 0;
    UINT4               u4InIndex = 0;
    UINT4               u4XCIndex = 0;
    UINT4               u4InSegLabel = MPLS_INVALID_LABEL;
    UINT4               u4InStackLabel = MPLS_INVALID_LABEL;
    INT4                i4NextHop = 0;
    UINT4               u4L3VlanIf = 0;
    BOOL1               bTitleFlag = TRUE;
    BOOL1               bXcIndexMatch = FALSE;
    BOOL1               bFlag = MPLS_FALSE;
    BOOL1               bStkFlag = MPLS_FALSE;
    static UINT4        au4ActionPointer[MPLS_TE_XC_TABLE_OFFSET];
    static UINT4        au4InLblStkIndex[LBL_STACK_TABLE_MAX_OFFSET];
    static UINT1        au1OutLblStkIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1TmpLblStkIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1NextHopAddr[MPLS_INDEX_LENGTH];
    static UINT1        au1InSegIndexPrev[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndexPrev[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndexPrev[MPLS_INDEX_LENGTH];
    static UINT1        au1Alias[CFA_MAX_PORT_NAME_LENGTH];
#ifdef MPLS_IPV6_WANTED

    tIp6Addr            PrintIp6Addr;

    tSNMP_OCTET_STRING_TYPE NextHopIpv6Addr;
    static UINT1        au1NextHopIpv6Addr[IPV6_ADDR_LENGTH];
    NextHopIpv6Addr.pu1_OctetList = au1NextHopIpv6Addr;
    MEMSET (&PrintIp6Addr, 0, sizeof (tIp6Addr));

#endif

    ActionPointer.pu4_OidList = au4ActionPointer;
    InLblStkIndex.pu4_OidList = au4InLblStkIndex;
    OutLblStkIndex.pu1_OctetList = au1OutLblStkIndex;
    TmpLblStkIndex.pu1_OctetList = au1TmpLblStkIndex;
    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    XCIndex.pu1_OctetList = au1XCIndex;
    NextHopAddr.pu1_OctetList = au1NextHopAddr;
    InSegmentIndexPrev.pu1_OctetList = au1InSegIndexPrev;
    OutSegmentIndexPrev.pu1_OctetList = au1OutSegIndexPrev;
    XCIndexPrev.pu1_OctetList = au1XCIndexPrev;
    Alias.pu1_OctetList = au1Alias;

    /* 1. scan the XC entry */
    /* get the first index of XC table , in seg and out seg table */

    while (1)
    {
        u4InLblStkIndex = MPLS_ZERO;
        u4OutLblStkIndex = MPLS_ZERO;
        u4InSegLabel = MPLS_INVALID_LABEL;
        u4InStackLabel = MPLS_INVALID_LABEL;
        u4OutStackLabel = MPLS_INVALID_LABEL;
        u4OutSegTopLabel = MPLS_INVALID_LABEL;

        if ((u4XCIndex != 0) &&
            ((nmhGetNextIndexMplsXCTable
              (&XCIndexPrev, &XCIndex, &InSegmentIndexPrev, &InSegmentIndex,
               &OutSegmentIndexPrev, &OutSegmentIndex)) == SNMP_FAILURE))
        {
            break;
        }

        /* Only executed first time */
        if ((u4XCIndex == 0) &&
            ((nmhGetFirstIndexMplsXCTable (&XCIndex, &InSegmentIndex,
                                           &OutSegmentIndex)) == SNMP_FAILURE))
        {
            return CLI_SUCCESS;
        }

        /* Get first FTN entry */
        MEMCPY (XCIndexPrev.pu1_OctetList, XCIndex.pu1_OctetList,
                XCIndex.i4_Length);
        XCIndexPrev.i4_Length = XCIndex.i4_Length;

        MEMCPY (InSegmentIndexPrev.pu1_OctetList, InSegmentIndex.pu1_OctetList,
                InSegmentIndex.i4_Length);
        InSegmentIndexPrev.i4_Length = InSegmentIndex.i4_Length;

        MEMCPY (OutSegmentIndexPrev.pu1_OctetList,
                OutSegmentIndex.pu1_OctetList, OutSegmentIndex.i4_Length);
        OutSegmentIndexPrev.i4_Length = OutSegmentIndex.i4_Length;

        MPLS_OCTETSTRING_TO_INTEGER ((&InSegmentIndex), u4InIndex);
        MPLS_OCTETSTRING_TO_INTEGER ((&OutSegmentIndex), u4OutIndex);
        MPLS_OCTETSTRING_TO_INTEGER ((&XCIndex), u4XCIndex);

        if (TeCheckXcIndex (u4XCIndex, &pTeTnlInfo) == TE_SUCCESS)
        {
            continue;
        }

        if ((nmhGetFirstIndexMplsFTNTable (&u4FtnIndex) == SNMP_SUCCESS) &&
            (u4FtnIndex != 0))
        {
            do
            {
                /* get FTN ActionPointer */
                if ((SNMP_FAILURE ==
                     nmhGetMplsFTNActionPointer (u4FtnIndex, &ActionPointer)))
                {
                    continue;
                }
                if (2 == ActionPointer.u4_Length)
                {
                    continue;
                }

                MPLS_OID_TO_INTEGER ((&ActionPointer), u4FtnXCIndex,
                                     MPLS_IN_INDEX_START_OFFSET);
                MPLS_OID_TO_INTEGER ((&ActionPointer), u4FtnInIndex,
                                     MPLS_IN_INDEX_START_OFFSET);
                MPLS_OID_TO_INTEGER ((&ActionPointer), u4FtnOutIndex,
                                     MPLS_OUT_INDEX_START_OFFSET);

                if ((u4FtnXCIndex == u4XCIndex) &&
                    (u4FtnInIndex == u4InIndex)
                    && (u4FtnOutIndex == u4OutIndex))
                {
                    bXcIndexMatch = TRUE;
                    break;
                }
            }
            while ((nmhGetNextIndexMplsFTNTable (u4FtnIndex, &u4FtnIndex)) ==
                   SNMP_SUCCESS);
            if (bXcIndexMatch)
            {
                bXcIndexMatch = FALSE;
                continue;
            }
        }

        if (nmhGetMplsXCOwner (&XCIndex, &InSegmentIndex,
                               &OutSegmentIndex, &i4XCOwner) == SNMP_FAILURE)
        {
            continue;
        }

        if (nmhGetMplsXCLabelStackIndex (&XCIndex, &InSegmentIndex,
                                         &OutSegmentIndex,
                                         &OutLblStkIndex) == SNMP_FAILURE)
        {
            continue;
        }
        MPLS_OCTETSTRING_TO_INTEGER ((&OutLblStkIndex), u4OutLblStkIndex);

        if ((u4InIndex == 0) || (u4OutIndex == 0))
        {
            continue;
        }

        if ((u4StaticFlag == TRUE) && (i4XCOwner != MPLS_OWNER_SNMP))
        {
            continue;
        }

        if (nmhGetMplsInSegmentLabel (&InSegmentIndex, &u4InSegLabel)
            == SNMP_FAILURE)
        {
            continue;
        }

        if (nmhGetMplsInSegmentLabelPtr (&InSegmentIndex, &InLblStkIndex)
            == SNMP_SUCCESS)
        {
            u4InLblStkIndex =
                InLblStkIndex.pu4_OidList[InLblStkIndex.u4_Length - 1];
        }

        if (((u4Flag & MPLS_SB_LLABEL) == MPLS_SB_LLABEL)
            && (u4InSegLabel != u4LowLabel))
        {
            continue;
        }
        if (u4InLblStkIndex != MPLS_ZERO)
        {
            MPLS_INTEGER_TO_OCTETSTRING (u4InLblStkIndex, (&TmpLblStkIndex));
            if (nmhGetMplsLabelStackLabel (&TmpLblStkIndex, MPLS_ONE,
                                           &u4InSegLabel) == SNMP_FAILURE)
            {
                continue;
            }

            if (nmhGetMplsLabelStackLabel (&TmpLblStkIndex, MPLS_TWO,
                                           &u4InStackLabel) == SNMP_FAILURE)
            {
                continue;
            }
        }

        if (u4OutLblStkIndex != MPLS_ZERO)
        {
            if (nmhGetMplsLabelStackLabel (&OutLblStkIndex, MPLS_ONE,
                                           &u4OutStackLabel) == SNMP_FAILURE)
            {
                continue;
            }
        }

        if (nmhGetMplsOutSegmentTopLabel (&OutSegmentIndex,
                                          &u4OutSegTopLabel) == SNMP_FAILURE)
        {
            continue;
        }
        if (((u4Flag & MPLS_SB_HLABEL) == MPLS_SB_HLABEL)
            && (u4OutSegTopLabel != u4HighLabel))
        {
            continue;
        }

        if (nmhGetMplsOutSegmentNextHopAddrType
            (&OutSegmentIndex, &i4NextHopAddrType) == SNMP_FAILURE)
        {
            continue;
        }

#ifdef MPLS_IPV6_WANTED
        if (i4NextHopAddrType == LSR_ADDR_IPV6)
        {
            if (nmhGetMplsOutSegmentNextHopAddr
                (&OutSegmentIndex, &NextHopIpv6Addr) == SNMP_FAILURE)
            {
                continue;
            }

            MEMCPY (PrintIp6Addr.u1_addr, NextHopIpv6Addr.pu1_OctetList,
                    IPV6_ADDR_LENGTH);
        }
        else
        {
#endif
            if (nmhGetMplsOutSegmentNextHopAddr (&OutSegmentIndex, &NextHopAddr)
                == SNMP_FAILURE)
            {
                continue;
            }

            MPLS_OCTETSTRING_TO_INTEGER ((&NextHopAddr), i4NextHop);
#ifdef MPLS_IPV6_WANTED
        }
#endif
        if (nmhGetMplsOutSegmentInterface (&OutSegmentIndex, &i4IfIndex)
            == SNMP_FAILURE)
        {
            continue;
        }
        if (CfaUtilGetIfIndexFromMplsTnlIf
            ((UINT4) i4IfIndex, &u4L3VlanIf, TRUE) == CFA_FAILURE)
        {
            continue;
        }
        if (nmhGetIfAlias (u4L3VlanIf, &Alias) == SNMP_FAILURE)
        {
            continue;
        }
        Alias.pu1_OctetList[Alias.i4_Length] = '\0';
        CLI_CONVERT_IPADDR_TO_STR (pc1NextHop, i4NextHop);
        if (bTitleFlag)
        {
            CliPrintf (CliHandle,
                       "\rLocal label                "
                       "Outgoing label               "
                       "Outgoing Next Hop       \n");
            CliPrintf (CliHandle,
                       "\r                           "
                       "                             " "intf\n");
            CliPrintf (CliHandle,
                       "\r-----------                "
                       "--------------               " "-------- --------\n");
            bTitleFlag = FALSE;
        }

        CliPrintf (CliHandle, "\r");

        bFlag = MplsCliDisplayXcLabel (CliHandle, u4InSegLabel);

        if (u4InStackLabel != MPLS_INVALID_LABEL)
        {
            CliPrintf (CliHandle, ", ");

            bStkFlag = MplsCliDisplayXcLabel (CliHandle, u4InStackLabel);

            if (bFlag && bStkFlag)
            {
                CliPrintf (CliHandle, "             ");
            }
            else if (bFlag)
            {
                CliPrintf (CliHandle, "      ");
            }
            else
            {
                CliPrintf (CliHandle, " ");
            }
        }
        else
        {
            if (bFlag)
            {
                CliPrintf (CliHandle, "                     ");
            }
            else
            {
                CliPrintf (CliHandle, "              ");
            }
        }

        bFlag = MplsCliDisplayXcLabel (CliHandle, u4OutSegTopLabel);

        if (u4OutStackLabel != MPLS_INVALID_LABEL)
        {
            CliPrintf (CliHandle, ", ");

            bStkFlag = MplsCliDisplayXcLabel (CliHandle, u4OutStackLabel);

            if (bFlag && bStkFlag)
            {
                CliPrintf (CliHandle, "               ");
            }
            else if (bFlag)
            {
                CliPrintf (CliHandle, "        ");
            }
            else
            {
                CliPrintf (CliHandle, " ");
            }
        }
        else
        {
            if (bFlag)
            {
                CliPrintf (CliHandle, "                       ");
            }
            else
            {
                CliPrintf (CliHandle, "                ");
            }
        }

#ifdef MPLS_IPV6_WANTED
        if (i4NextHopAddrType == LSR_ADDR_IPV6)
        {
            CliPrintf (CliHandle, "%-8s %-15s\n", Alias.pu1_OctetList,
                       Ip6PrintAddr (&(PrintIp6Addr)));
        }
        else
        {
#endif
            CliPrintf (CliHandle, "%-8s %-15s\n", Alias.pu1_OctetList,
                       pc1NextHop);
#ifdef MPLS_IPV6_WANTED
        }
#endif
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : MplsCliSetFsMplsEnable  
* Description   : This routine is used for enabling the MPLS on interface 
* Input(s)      : CliHandle  - Cli Context Handle
*                 u4IfaceIndex  - L3 Interface Index
* Output(s)     : None
* Return(s)     : CLI_SUCCESS/CLI_FAILURE
******************************************************************************/
INT1
MplsCliSetFsMplsEnable (tCliHandle CliHandle, UINT4 u4IfaceIndex)
{
    UINT4               u4MplsIfIndex = 0;
    tCfaIfInfo          CfaIfInfo;

    if (CfaValidateCfaIfIndex ((UINT2) u4IfaceIndex) == CFA_FAILURE)
    {
        CliPrintf (CliHandle, "%%Invalid interface index %u\r\n", u4IfaceIndex);
        return CLI_FAILURE;
    }
#ifdef CFA_WANTED
    if (CfaUtilGetMplsIfFromIfIndex (u4IfaceIndex, &u4MplsIfIndex, TRUE) ==
        CFA_SUCCESS)
    {
        return CLI_SUCCESS;
    }
#endif

    if (CfaGetIfInfo (u4IfaceIndex, &CfaIfInfo) == CFA_FAILURE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "%%unable to fetch CfaIfInfo \r\n");
    }
    if (!((CfaIfInfo.u1IfType == CFA_L3IPVLAN) ||
          ((CfaIfInfo.u1IfType == CFA_ENET) &&
           (CfaIfInfo.u1BridgedIface == CFA_DISABLED))
          || (CfaIfInfo.u1IfType == CFA_LAGG)))
    {
        CliPrintf (CliHandle, "\r %%MPLS should be enabled only on "
                   "layer 3 interface !!!\r\n");
        return (CLI_FAILURE);
    }

    if (CfaGetFreeInterfaceIndex (&u4MplsIfIndex, CFA_MPLS) == OSIX_FAILURE)
    {
        return MPLS_FAILURE;
    }

    if (MplsCreateMplsIfOrMplsTnlIf (u4IfaceIndex, u4MplsIfIndex, CFA_MPLS)
        == MPLS_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%%Enabling MPLS over Layer 3 interface failed\r\n");
        return (CLI_FAILURE);
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : MplsCliSetFsMplsDisable  
 * Description   : This routine is used for disabling the MPLS on interface 
 * Input(s)      : CliHandle  - Cli Context Handle
 *                 u4IfaceIndex   - L3 Interface Index
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 ******************************************************************************/
INT1
MplsCliSetFsMplsDisable (tCliHandle CliHandle, UINT4 u4IfaceIndex)
{
    UINT4               u4MplsIfIndex = 0;
#ifdef TLM_WANTED
    UINT4               u4HighStackIndex = MPLS_ZERO;
#endif
#ifdef CFA_WANTED
    if (CfaUtilGetMplsIfFromIfIndex (u4IfaceIndex, &u4MplsIfIndex, TRUE) ==
        CFA_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r MPLS is not enabled on this interface !!!\r\n");
        return CLI_FAILURE;
    }
#endif
#ifdef TLM_WANTED
    if (CfaApiGetTeLinkIfFromL3If ((UINT4) u4IfaceIndex,
                                   (UINT4 *) &u4HighStackIndex, MPLS_ONE,
                                   TRUE) == CLI_SUCCESS)
    {

        CliPrintf (CliHandle, "\r MPLS Interface is associated with TE.\r\n");
        return CLI_FAILURE;
    }
#endif
    /* To check whether the Interface is mapped with LDP Entity
     * and to block the deletion of Mpls Interface associated to LDP
     */

    if (LdpIsIntfMapped (u4IfaceIndex) == MPLS_TRUE)

    {
        CliPrintf (CliHandle, "\r%% MPLS Interface is associated with"
                   " LDP. Remove the association and then delete"
                   " the interface.\n");
        return CLI_FAILURE;

    }

    if (MplsDeleteMplsIfOrMplsTnlIf (u4IfaceIndex, u4MplsIfIndex, CFA_MPLS,
                                     MPLS_TRUE) == MPLS_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%%Remove MPLS Tnl Interfaces(stacked over MPLS If) "
                   "before making MPLS If deletion !!!\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : MplsCliInOutXcFtnActive 
 * Description   : This routine is used for setting the XC, InSegment,
 *                 OutSegment and FTN active.
 * Input(s)      : XCIndex  - Cross Connect Index
 *                 InSegmentIndex - In Segment Entry Index
 *                 OutSegmentIndex - Out Segment Entry Index
 *                 u4FtnIndex - FTN Entry Index
 *                 u4Flag - MPLS_CLI_FTN_EGRS_LBL_BINDING or 
 *                 MPLS_CLI_FTN_INGRS_LBL_BINDING
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 ******************************************************************************/
PRIVATE INT4
MplsCliInOutXcFtnActive (tSNMP_OCTET_STRING_TYPE InSegmentIndex,
                         tSNMP_OCTET_STRING_TYPE OutSegmentIndex,
                         tSNMP_OCTET_STRING_TYPE XCIndex,
                         UINT4 u4FtnIndex, UINT4 u4Flag)
{
    INT1                i1RetVal = CLI_SUCCESS;
    UINT4               u4ErrCode = 0;
    INT4                i4MplsTnlIfIndex = 0;
    UINT4               u4IfIndex = 0;
    do
    {
        if (nmhTestv2MplsXCRowStatus (&u4ErrCode, &XCIndex, &InSegmentIndex,
                                      &OutSegmentIndex, MPLS_STATUS_ACTIVE)
            == SNMP_FAILURE)
        {
            i1RetVal = CLI_FAILURE;
            break;
        }
        if (nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                                   MPLS_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            i1RetVal = CLI_FAILURE;
            break;
        }

        if (nmhTestv2MplsFTNRowStatus (&u4ErrCode, u4FtnIndex,
                                       MPLS_STATUS_ACTIVE) == SNMP_FAILURE)
        {
            i1RetVal = CLI_FAILURE;
            break;
        }

        if (nmhSetMplsFTNRowStatus (u4FtnIndex, MPLS_STATUS_ACTIVE)
            == SNMP_FAILURE)
        {
            i1RetVal = CLI_FAILURE;
            break;
        }
        return i1RetVal;
    }
    while (0);

    if (u4Flag == MPLS_CLI_FTN_EGRS_LBL_BINDING)
    {
        if (nmhGetMplsInSegmentInterface (&InSegmentIndex, &i4MplsTnlIfIndex)
            == SNMP_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "nmhGetMplsInSegmentInterface failed \r\n");
        }
        if (CfaUtilGetIfIndexFromMplsTnlIf
            ((UINT4) i4MplsTnlIfIndex, &u4IfIndex, TRUE) == CFA_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "CfaUtilGetIfIndexFromMplsTnlIffailed \r\n");
        }
        MplsDeleteMplsIfOrMplsTnlIf (u4IfIndex, (UINT4) i4MplsTnlIfIndex,
                                     CFA_MPLS_TUNNEL, MPLS_TRUE);
        if (nmhSetMplsInSegmentRowStatus (&InSegmentIndex, MPLS_STATUS_DESTROY)
            == SNMP_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "nmhSetMplsInSegmentRowStatus Failed \t\n");
        }
    }
    else
    {
        if (nmhGetMplsOutSegmentInterface (&OutSegmentIndex, &i4MplsTnlIfIndex)
            == SNMP_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "nmhGetMplsoutSegmentInterface failed \r\n");
        }
        if (CfaUtilGetIfIndexFromMplsTnlIf
            ((UINT4) i4MplsTnlIfIndex, &u4IfIndex, TRUE) == CFA_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "CfaUtilGetIfIndexFromMplsTnlIffailed \r\n");
        }
        MplsDeleteMplsIfOrMplsTnlIf (u4IfIndex, (UINT4) i4MplsTnlIfIndex,
                                     CFA_MPLS_TUNNEL, MPLS_TRUE);
        if (nmhSetMplsOutSegmentRowStatus
            (&OutSegmentIndex, MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "nmhSetMplsOutSegmentRowStatus Failed \t\n");
        }
    }
    if (nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               MPLS_STATUS_DESTROY) == SNMP_FAILURE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "nmhSetMplsXCRowStatus Failed \t\n");
    }
    if (nmhSetMplsFTNRowStatus (u4FtnIndex, MPLS_STATUS_DESTROY) ==
        SNMP_FAILURE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "nmhSetMplsFTNRowStatus Failed \t\n");
    }
    return CLI_FAILURE;
}

/******************************************************************************
 * Function Name : MplsCliFtnTeBinding 
 * Description   : This routine is used for binding TE to a FTN
 * Input(s)      : CliHandle   - Cli Context Handle
 *                 u4Prefix    - ip address prefix
 *                 u4Mask      - mask
 *                 u4TunnelId  - u4TunnelId
 *                 i4FtnActType  - FTNAction Type
 *                 u4TnlLspNum - Tunnel LSP number
 *                 u4TnlIngressLSRId - Tunnel ingress ID
 *                 u4TnlEgressLSRId - Tunnel Egress ID
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 ******************************************************************************/
INT1
MplsCliFtnTeBinding (tCliHandle CliHandle,
                     UINT4 u4Prefix, UINT4 u4Mask, UINT4 u4TunnelId,
                     INT4 i4FtnActType, UINT4 u4TnlLspNum,
                     UINT4 u4TnlIngressLSRId, UINT4 u4TnlEgressLSRId)
{
    tSNMP_OID_TYPE      ActionPointer;
    UINT4               u4FtnIndex = 0;
    UINT4               u4ErrCode = 0;
    UINT4               u4TunnelIndex = 0;
    UINT4               u4TunnelInstance = 0;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;
    BOOL1               bFtnEntryExists = FALSE;
    INT1                i1RetVal = SNMP_SUCCESS;
    INT4                i4ActionType = 0;
    tGenU4Addr          Prefix;
    static UINT4        au4ActionPointer[MPLS_FTN_TE_TABLE_DEF_OFFSET];
    static UINT4        au4TunnelTableOid[MPLS_FTN_TE_TABLE_DEF_OFFSET] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 8, 1, 3, 1, 17, 0, 0, 0, 0 };
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4NetworkAddr = MPLS_ZERO;
    UINT4               u4ErrorCode = MPLS_ZERO;
    ActionPointer.pu4_OidList = au4ActionPointer;

    u4NetworkAddr = u4Prefix | (~(u4Mask));
    if (u4NetworkAddr == u4Prefix)
    {
        CliPrintf (CliHandle, "\r%% Invalid prefix and SubnetMask\n");
        return CLI_FAILURE;
    }

    if (MplsGetFTNActionType (u4Prefix, &i4ActionType) == MPLS_SUCCESS)
    {
        if (i4ActionType == REDIRECTLSP)
        {
            /*  In future the redirection of FEC packets into Tunnel or LSP is 
             *  allowed based on LSP local preference, now the only one 
             *  i.e redirection to tunnel or LSP is allowed in the FTN table"*/
            CliPrintf (CliHandle,
                       "\r%%Ftn Entry Redirects Packets into LSP\r\n");
            return CLI_SUCCESS;
        }
    }

    Prefix.Addr.u4Addr = u4Prefix;
    if ((MplsCliGetOrCreateFTNEntry (Prefix, u4Mask, &u4FtnIndex,
                                     &bFtnEntryExists, i4FtnActType,
                                     &u4ErrorCode)) == CLI_FAILURE)
    {
        if (u4ErrorCode == SNMP_ERR_WRONG_VALUE)
        {
            CliPrintf (CliHandle, "%% Invalid Ip Address\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "%%Unable to fetch/create a FTN entry\r\n");
        }
        if (nmhSetMplsFTNRowStatus (u4FtnIndex, MPLS_STATUS_DESTROY)
            == SNMP_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL, "nmhSetMplsFTNRowStatus Failed \t\n");
        }
        return CLI_FAILURE;
    }

    do
    {
        if ((u4TunnelId != 0) && (u4TnlLspNum != 0) &&
            (u4TnlIngressLSRId != 0) && (u4TnlEgressLSRId != 0))
        {
            pTeTnlInfo = TeGetTunnelInfo (u4TunnelId, u4TnlLspNum,
                                          u4TnlIngressLSRId, u4TnlEgressLSRId);
            if (pTeTnlInfo == NULL)
            {
                CliPrintf (CliHandle, "\r%%Tunnel Does Not Exist\r\n");
                i1RetVal = SNMP_FAILURE;
                break;
            }
        }
        else if ((u4TunnelId != 0) && (u4TnlLspNum == 0) &&
                 (u4TnlIngressLSRId != 0) && (u4TnlEgressLSRId != 0))

        {
            pTeTnlInfo =
                TeGetTnlInfoByOwnerAndRole (u4TunnelId, u4TnlLspNum,
                                            u4TnlIngressLSRId,
                                            u4TnlEgressLSRId,
                                            TE_TNL_OWNER_SNMP, TE_INGRESS);

            if (pTeTnlInfo == NULL)
            {
                pTeTnlInfo =
                    TeGetTnlInfoByOwnerAndRole (u4TunnelId, u4TnlLspNum,
                                                u4TnlIngressLSRId,
                                                u4TnlEgressLSRId, 0, TE_EGRESS);

                if ((pTeTnlInfo == NULL) ||
                    (pTeTnlInfo->u4TnlMode !=
                     TE_TNL_MODE_COROUTED_BIDIRECTIONAL))
                {
                    CliPrintf (CliHandle, "\r%%Tunnel Does Not Exist\r\n");
                    i1RetVal = SNMP_FAILURE;
                    break;
                }
            }
        }

        else
        {
            pTeTnlInfo =
                TeGetTnlInfoByOwnerAndRole (u4TunnelId, u4TnlLspNum, 0, 0,
                                            TE_TNL_OWNER_SNMP, TE_INGRESS);

            if (pTeTnlInfo == NULL)
            {
                pTeTnlInfo =
                    TeGetTnlInfoByOwnerAndRole (u4TunnelId, u4TnlLspNum, 0, 0,
                                                TE_TNL_OWNER_SNMP, TE_EGRESS);

                if ((pTeTnlInfo == NULL) ||
                    (pTeTnlInfo->u4TnlMode !=
                     TE_TNL_MODE_COROUTED_BIDIRECTIONAL))
                {
                    CliPrintf (CliHandle, "\r%%Tunnel Does Not Exist\r\n");
                    i1RetVal = SNMP_FAILURE;
                    break;
                }
            }
        }
        u4TunnelIndex = TE_TNL_TNL_INDEX (pTeTnlInfo);
        u4TunnelInstance = TE_TNL_TNL_INSTANCE (pTeTnlInfo);
        MEMCPY ((UINT1 *) (&u4IngressId),
                pTeTnlInfo->TnlIngressLsrId, ROUTER_ID_LENGTH);
        MEMCPY ((UINT1 *) (&u4EgressId),
                TE_TNL_EGRESS_LSRID (pTeTnlInfo), ROUTER_ID_LENGTH);
        u4IngressId = OSIX_HTONL (u4IngressId);
        u4EgressId = OSIX_HTONL (u4EgressId);

        au4TunnelTableOid[MPLS_FTN_TE_TABLE_DEF_OFFSET - 4] = u4TunnelIndex;
        au4TunnelTableOid[MPLS_FTN_TE_TABLE_DEF_OFFSET - 3] = u4TunnelInstance;
        au4TunnelTableOid[MPLS_FTN_TE_TABLE_DEF_OFFSET - 2] = u4IngressId;
        au4TunnelTableOid[MPLS_FTN_TE_TABLE_DEF_OFFSET - 1] = u4EgressId;
        ActionPointer.pu4_OidList = au4TunnelTableOid;
        ActionPointer.u4_Length = MPLS_FTN_TE_TABLE_DEF_OFFSET;
        if ((nmhTestv2MplsFTNActionPointer (&u4ErrCode, u4FtnIndex,
                                            &ActionPointer)) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_ERR_ACTION_POINTER);
            i1RetVal = SNMP_FAILURE;
            break;
        }
        if ((nmhSetMplsFTNActionPointer (u4FtnIndex, &ActionPointer))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%FTN Binding Failed\r\n");
            i1RetVal = SNMP_FAILURE;
            break;
        }
        break;
    }
    while (1);
    if (i1RetVal != SNMP_SUCCESS)
    {
        if (nmhSetMplsFTNRowStatus (u4FtnIndex, MPLS_STATUS_DESTROY)
            == SNMP_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL, "nmhSetMplsFTNRowStatus Failed \t\n");
        }
        return CLI_FAILURE;
    }
    if ((nmhSetMplsFTNRowStatus (u4FtnIndex, MPLS_STATUS_ACTIVE)) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set the FTN row status active");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : MplsCliNoFtnTeBinding 
 * Description   : This routine is used for unbinding TE from a FTN
 * Input(s)      : CliHandle   - Cli Context Handle
 *                 u4Prefix    - ip address prefix
 *                 u4Mask      - mask
 *                 i4FtnActType  - FTNAction Type
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 ******************************************************************************/
INT1
MplsCliNoFtnTeBinding (tCliHandle CliHandle,
                       UINT4 u4Prefix, UINT4 u4Mask, INT4 i4FtnActType)
{
    tFtnEntry          *pFtnEntry = NULL;
    UNUSED_PARAM (u4Mask);

    pFtnEntry = MplsSignalGetFtnTableEntry (u4Prefix);

    if (pFtnEntry == NULL)
    {
        CliPrintf (CliHandle, "\r\n%% No Entry Found for the prefix given\n");
        return CLI_FAILURE;
    }

    if (pFtnEntry->i4ActionType == i4FtnActType)
    {
        if (nmhSetMplsFTNRowStatus (pFtnEntry->u4FtnIndex,
                                    MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Cannot delete the FTN entry\n");
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/* MPLS SHOW RUNNING COFIG COMMAND */
/*****************************************************************************/
/*     FUNCTION NAME    : MplsShowRunningConfig                              */
/*                                                                           */
/*     DESCRIPTION      : Displays MPLS Configuration                        */
/*                                                                           */
/*     INPUT            : tCliHandle - Handle to the Cli Context             */
/*                                                                           */
/*     RETURNS          : NONE                                               */
/*****************************************************************************/
INT4
MplsShowRunningConfig (tCliHandle CliHandle)
{
    /* Show - mpls static cross connect */
    if (CLI_SUCCESS != MplsStaticCrossConnectShowRunningConfig (CliHandle))
    {
        return CLI_FAILURE;
    }
    /* Show - mpls Tunnel model */
    if (CLI_SUCCESS != MplsTnlmodelShowRunningConfig (CliHandle))
    {
        return CLI_FAILURE;
    }
    /* Show - mpls TTL Value */
    if (CLI_SUCCESS != MplsTTLShowRunningConfig (CliHandle))
    {
        return CLI_FAILURE;
    }

    if (IssGetModuleSystemControl (RSVPTE_MODULE_ID) != MODULE_SHUTDOWN)
    {
        if (CLI_SUCCESS != RpteCliRsvpTeShowRunnningConfig (CliHandle))
        {
            return CLI_FAILURE;
        }
        /* Show - mpls frr configuraton */
        if (CLI_SUCCESS != RpteFrrFacShowRunningConfig (CliHandle))
        {
            return CLI_FAILURE;
        }
    }

    OamShowRunningConfig (CliHandle);

    /* Show - mpls tunnel */

    if (CLI_SUCCESS != MplsLspReoptimizationRunningConfig (CliHandle))
    {
        return CLI_FAILURE;
    }

    /* Show - mpls tunnel */

    if (CLI_SUCCESS != MplsTunnelShowRunningConfig (CliHandle))
    {
        return CLI_FAILURE;
    }

    /* Show - mpls static binding ipv4 */
    if (CLI_SUCCESS != MplsStaticBindingShowRunningConfig (CliHandle))
    {
        return CLI_FAILURE;
    }
    if (IssGetModuleSystemControl (LDP_MODULE_ID) != MODULE_SHUTDOWN)
    {
        /* Show - mpls ldp configuration */
        if (CLI_SUCCESS != LdpCliShowRunningConfig (CliHandle))
        {
            return CLI_FAILURE;
        }
    }

#ifdef  LSPP_WANTED
    IssMplsOamEchoSrcGlobalConfig (CliHandle);
#endif

    if (CLI_SUCCESS != L2VpnShowRunningConfig (CliHandle))
    {
        return CLI_FAILURE;
    }

#ifdef MPLS_L3VPN_WANTED
    /* Show - mpls l3vpn configuration */
    if (CLI_SUCCESS != L3vpnCliShowRunningConfig (CliHandle))
    {
        return CLI_FAILURE;
    }
#endif

    /* Show - PW global configurations */
    MplsPwGlobalShowRunningConfig (CliHandle);

/* Show - RFC6374 configurations */
#ifdef RFC6374_WANTED
    R6374CliShowRunningConfig (CliHandle);
#endif
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : MplsStaticBindingShowRunningConfig  
* Description   : This routine displays the static prefix-label binding 
* Input(s)      : CliHandle - Cli Context Handle
* Return(s)     : CLI_SUCCESS/CLI_FAILURE
******************************************************************************/
INT4
MplsStaticBindingShowRunningConfig (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE DestAddrMin;
    tSNMP_OCTET_STRING_TYPE DestAddrMax;
    tSNMP_OCTET_STRING_TYPE InSegmentIndexPrev;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndexPrev;
    tSNMP_OCTET_STRING_TYPE NextHopAddr;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE GmplsIfSignalingCaps;
    tSNMP_OCTET_STRING_TYPE XCIndexPrev;
    tSNMP_OID_TYPE      ActionPointer;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4NextHopAddr = 0;
    UINT4               u4OutSegLabel = 0;
    UINT4               u4InSegLabel = 0;
    UINT4               u4OutIndex = 0;
    UINT4               u4FtnIndex = 0;
    UINT4               u4XCIndex = 0;
    UINT4               u4InIndex = 0;
    UINT4               u4MinAddr = 0;
    UINT4               u4MaxAddr = 0;
    UINT4               u4Prefix = 0;
    UINT4               u4Mask = 0;
    UINT4               u4InSegAddrFamily = 0;
#ifdef CFA_WANTED
    UINT4               u4L3VlanIf = 0;
#ifdef MPLS_IPV6_WANTED
    UINT4               u4L3VlanOutIf = 0;
#endif
#endif
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { 0 };
    INT4                i4IfIndex = 0;
#ifdef MPLS_IPV6_WANTED
    INT4                i4OutIfIndex = 0;
    INT1               *pi1OutIfName = NULL;
    UINT1               au1OutIfName[CFA_MAX_PORT_NAME_LENGTH] = { 0 };
#endif
    INT4                i4XCOwner = 0;
    INT4                i4InStatus = 0;
    INT4                i4OutStatus = 0;
    INT4                i4AchCodePoint = 0;
    CHR1               *pc1NextHopAddr = NULL;
    CHR1               *pc1MaxAddr = NULL;
    INT1               *pi1IfName = NULL;
    CHR1               *pc1Prefix = NULL;
    BOOL1               bLoop = FALSE;
    static UINT1        au1InSegIndexPrev[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndexPrev[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndexPrev[MPLS_INDEX_LENGTH];
    static UINT4        au4ActionPointer[MPLS_TE_XC_TABLE_OFFSET];
    static UINT1        au1NextHopAddr[IPV6_ADDR_LENGTH];
    static UINT1        au1DestAddrMin[IPV6_ADDR_LENGTH];
    static UINT1        au1DestAddrMax[IPV6_ADDR_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1GmplsIfSignallingCaps[MPLS_INDEX_LENGTH];

#ifdef MPLS_IPV6_WANTED
    tIp6Addr            Ipv6MinAddr;
    tIp6Addr            Ipv6MaxAddr;
    tIp6Addr            NextHopIpv6Addr;

    UINT1               u1DestPrefixLen = 0;
    pi1OutIfName = (INT1 *) &au1OutIfName[0];
    MEMSET (&Ipv6MinAddr, 0, sizeof (tIp6Addr));
    MEMSET (&Ipv6MaxAddr, 0, sizeof (tIp6Addr));
    MEMSET (&NextHopIpv6Addr, 0, sizeof (tIp6Addr));
    MEMSET (au1OutIfName, 0, CFA_MAX_PORT_NAME_LENGTH);
#endif

    InSegmentIndexPrev.pu1_OctetList = au1InSegIndexPrev;
    OutSegmentIndexPrev.pu1_OctetList = au1OutSegIndexPrev;
    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = MPLS_ZERO;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_ZERO;
    ActionPointer.pu4_OidList = au4ActionPointer;
    ActionPointer.u4_Length = MPLS_ZERO;
    NextHopAddr.pu1_OctetList = au1NextHopAddr;
    NextHopAddr.i4_Length = MPLS_ZERO;
    DestAddrMax.pu1_OctetList = au1DestAddrMax;
    DestAddrMax.i4_Length = MPLS_ZERO;
    DestAddrMin.pu1_OctetList = au1DestAddrMin;
    DestAddrMin.i4_Length = MPLS_ZERO;
    XCIndex.pu1_OctetList = au1XCIndex;
    XCIndex.i4_Length = MPLS_ZERO;
    XCIndexPrev.pu1_OctetList = au1XCIndexPrev;

    GmplsIfSignalingCaps.pu1_OctetList = au1GmplsIfSignallingCaps;
    GmplsIfSignalingCaps.i4_Length = MPLS_ZERO;

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    pi1IfName = (INT1 *) &au1IfName[0];

    if ((nmhGetFsMplsLsrRfc6428CompatibleCodePoint (&i4AchCodePoint)
         == SNMP_SUCCESS) && (i4AchCodePoint == MPLS_SNMP_TRUE))
    {
        CliPrintf (CliHandle, "\r\nmpls ach code-point rfc6428-compatible\r\n");
        FilePrintf (CliHandle,
                    "\r\nmpls ach code-point rfc6428-compatible\r\n");
    }

    while (1)
    {
        /* get next entry */
        if ((TRUE == bLoop) &&
            ((SNMP_FAILURE ==
              nmhGetNextIndexGmplsInterfaceTable (i4IfIndex, &i4IfIndex))))
        {
            break;
        }

        /* get first entry */
        if ((FALSE == bLoop) &&
            ((SNMP_FAILURE ==
              nmhGetFirstIndexGmplsInterfaceTable (&i4IfIndex))))
        {
            break;
        }

        bLoop = TRUE;

        if (SNMP_FAILURE ==
            nmhGetGmplsInterfaceSignalingCaps (i4IfIndex,
                                               &GmplsIfSignalingCaps))
        {
            continue;
        }

        if (GmplsIfSignalingCaps.pu1_OctetList[0] != GMPLS_SIG_CAP_RSVP_BIT)
        {

            switch (GmplsIfSignalingCaps.pu1_OctetList[0])
            {
                case GMPLS_SIG_CAP_UNKNOWN_BIT:
                {
                    CliPrintf (CliHandle,
                               "mpls signalling capabilities unknown");
                    FilePrintf (CliHandle,
                                "mpls signalling capabilities unknown");
                    break;
                }
                case GMPLS_SIG_CAP_OTHER_BIT:
                {
                    CliPrintf (CliHandle, "mpls signalling capabilities other");
                    FilePrintf (CliHandle,
                                "mpls signalling capabilities other");
                    break;
                }
                case GMPLS_SIG_CAP_RSVP_BIT:
                {
                    CliPrintf (CliHandle, "mpls signalling capabilities rsvp");
                    FilePrintf (CliHandle, "mpls signalling capabilities rsvp");
                    break;
                }
                case (GMPLS_SIG_CAP_RSVP_BIT + GMPLS_SIG_CAP_OTHER_BIT):
                {
                    CliPrintf (CliHandle, "mpls signalling capabilities rsvp");
                    FilePrintf (CliHandle, "mpls signalling capabilities rsvp");
                    CliPrintf (CliHandle,
                               "\nmpls signalling capabilities other");
                    FilePrintf (CliHandle,
                                "\nmpls signalling capabilities other");
                    break;
                }
                default:
                {
                    break;
                }
            }
        }
    }

    /* scan the ftn table and get the XC index for each entry */
    /* get the first index of ftn table */
    while (1)
    {
        /* get next entry */
        if ((TRUE == bLoop) &&
            ((SNMP_FAILURE ==
              nmhGetNextIndexMplsFTNTable (u4FtnIndex, &u4FtnIndex))))
        {
            break;
        }

        /* get first entry */
        if ((FALSE == bLoop) &&
            ((SNMP_FAILURE == nmhGetFirstIndexMplsFTNTable (&u4FtnIndex))))
        {
            return CLI_SUCCESS;
        }

        bLoop = TRUE;

        /* get FTN ActionPointer */
        if ((SNMP_FAILURE ==
             nmhGetMplsFTNActionPointer (u4FtnIndex, &ActionPointer)))
        {
            continue;
        }
        if (2 == ActionPointer.u4_Length)
        {
            continue;
        }

        MPLS_OID_TO_INTEGER ((&ActionPointer), u4InIndex,
                             MPLS_IN_INDEX_START_OFFSET);
        MPLS_OID_TO_INTEGER ((&ActionPointer), u4OutIndex,
                             MPLS_OUT_INDEX_START_OFFSET);

        /* get min and max dest address */
        if ((SNMP_FAILURE ==
             nmhGetMplsFTNDestAddrMin (u4FtnIndex, &DestAddrMin)))
        {
            continue;
        }
#ifdef MPLS_IPV6_WANTED
        if (DestAddrMin.i4_Length == IPV6_ADDR_LENGTH)
        {
            MEMCPY (Ipv6MinAddr.u1_addr, DestAddrMin.pu1_OctetList,
                    DestAddrMin.i4_Length);
        }
        else
        {
#endif
            MPLS_OCTETSTRING_TO_INTEGER ((&DestAddrMin), u4MinAddr);
#ifdef MPLS_IPV6_WANTED
        }
#endif

        if ((SNMP_FAILURE ==
             nmhGetMplsFTNDestAddrMax (u4FtnIndex, &DestAddrMax)))
        {
            continue;
        }
#ifdef MPLS_IPV6_WANTED
        if (DestAddrMax.i4_Length == IPV6_ADDR_LENGTH)
        {
            MEMCPY (Ipv6MaxAddr.u1_addr, DestAddrMax.pu1_OctetList,
                    DestAddrMax.i4_Length);
            u1DestPrefixLen = MplsGetIpv6Subnetmasklen (Ipv6MaxAddr.u1_addr);
        }
        else
        {
#endif
            MPLS_OCTETSTRING_TO_INTEGER ((&DestAddrMax), u4MaxAddr);
#ifdef MPLS_IPV6_WANTED
        }
#endif
        /* get prefix and mask from that */
        u4Prefix = u4MinAddr;
        u4Mask = u4MaxAddr;

        if ((ActionPointer.u4_Length == MPLS_FTN_TE_TABLE_DEF_OFFSET)
            || ((0 == u4InIndex) && (0 == u4OutIndex)))
        {
            CliPrintf (CliHandle, "\r\n!");
            FilePrintf (CliHandle, "\r\n!");

            CliPrintf (CliHandle, "\r\nmpls binding ipv4 ");
            FilePrintf (CliHandle, "\r\nmpls binding ipv4 ");

            CLI_CONVERT_IPADDR_TO_STR (pc1Prefix, u4Prefix);
            CliPrintf (CliHandle, "%s ", pc1Prefix);
            FilePrintf (CliHandle, "%s ", pc1Prefix);

            CLI_CONVERT_IPADDR_TO_STR (pc1MaxAddr, u4Mask);
            CliPrintf (CliHandle, "%s ", pc1MaxAddr);
            FilePrintf (CliHandle, "%s ", pc1MaxAddr);

            CliPrintf (CliHandle, "te ");
            FilePrintf (CliHandle, "te ");

            CliPrintf (CliHandle, "%d\r\n ",
                       ActionPointer.pu4_OidList[MPLS_FTN_TE_TABLE_DEF_OFFSET -
                                                 4]);
            FilePrintf (CliHandle, "%d\r\n ",
                        ActionPointer.pu4_OidList[MPLS_FTN_TE_TABLE_DEF_OFFSET -
                                                  4]);

            continue;

        }

        MPLS_INTEGER_TO_OCTETSTRING (u4InIndex, (&InSegmentIndex));
        if (0 != u4InIndex)
        {
            /* get Outsegment RowStatus */
            if ((SNMP_FAILURE ==
                 nmhGetMplsInSegmentRowStatus (&InSegmentIndex, &i4InStatus)))
            {
                continue;
            }
        }
        else
        {
            i4InStatus = 0;
        }
        MPLS_INTEGER_TO_OCTETSTRING (u4OutIndex, (&OutSegmentIndex));
        if (0 != u4OutIndex)
        {
            /* get Outsegment RowStatus */
            if ((SNMP_FAILURE ==
                 nmhGetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                                &i4OutStatus)))
            {
                continue;
            }
        }
        else
        {
            i4OutStatus = 0;
        }
        /* RowStatus is ACTIVE */
        if (MPLS_STATUS_ACTIVE == i4InStatus)
        {
            /* get InSegmaent Label */
            if ((SNMP_FAILURE ==
                 nmhGetMplsInSegmentLabel (&InSegmentIndex, &u4InSegLabel)))
            {
                continue;
            }
            /* get InSegment interface */
            if (SNMP_FAILURE ==
                nmhGetMplsInSegmentInterface (&InSegmentIndex, &i4IfIndex))
            {
                continue;
            }
#ifdef CFA_WANTED
            /* get tunnel interface */
            if (CFA_FAILURE ==
                CfaUtilGetIfIndexFromMplsTnlIf (i4IfIndex, &u4L3VlanIf, TRUE))
            {
                continue;
            }
            /* get interface name */
            if (CFA_SUCCESS != CfaCliConfGetIfName (u4L3VlanIf, pi1IfName))
            {
                continue;
            }
#endif

#ifdef MPLS_IPV6_WANTED
            if (DestAddrMin.i4_Length == IPV6_ADDR_LENGTH)
            {
                CliPrintf (CliHandle, "mpls static binding ipv6 ");
                FilePrintf (CliHandle, "mpls static binding ipv6 ");

                CliPrintf (CliHandle, "%s ", Ip6PrintAddr (&Ipv6MinAddr));
                FilePrintf (CliHandle, "%s ", Ip6PrintAddr (&Ipv6MinAddr));

                CliPrintf (CliHandle, "%d ", u1DestPrefixLen);
                FilePrintf (CliHandle, "%d ", u1DestPrefixLen);
            }
            else
            {
#endif
                CliPrintf (CliHandle, "mpls static binding ipv4 ");
                FilePrintf (CliHandle, "mpls static binding ipv4 ");

                CLI_CONVERT_IPADDR_TO_STR (pc1Prefix, u4Prefix);
                CliPrintf (CliHandle, "%s ", pc1Prefix);
                FilePrintf (CliHandle, "%s ", pc1Prefix);

                CLI_CONVERT_IPADDR_TO_STR (pc1MaxAddr, u4Mask);
                CliPrintf (CliHandle, "%s ", pc1MaxAddr);
                FilePrintf (CliHandle, "%s ", pc1MaxAddr);
#ifdef MPLS_IPV6_WANTED
            }
#endif

            CliPrintf (CliHandle, "input ");
            FilePrintf (CliHandle, "input ");

            CliPrintf (CliHandle, "%s ", pi1IfName);
            FilePrintf (CliHandle, "%s ", pi1IfName);

            CliPrintf (CliHandle, "%u\r\n", u4InSegLabel);
            FilePrintf (CliHandle, "%u\r\n", u4InSegLabel);

        }
        if (MPLS_STATUS_ACTIVE == i4OutStatus)
        {
            MPLS_INTEGER_TO_OCTETSTRING (u4OutIndex, (&OutSegmentIndex));
            if ((SNMP_FAILURE == nmhGetMplsOutSegmentTopLabel (&OutSegmentIndex,
                                                               &u4OutSegLabel)))
            {
                continue;
            }

            /* get NextHop */
            if ((SNMP_FAILURE ==
                 nmhGetMplsOutSegmentNextHopAddr (&OutSegmentIndex,
                                                  &NextHopAddr)))
            {
                continue;
            }
#ifdef MPLS_IPV6_WANTED
            if (NextHopAddr.i4_Length == IPV6_ADDR_LENGTH)
            {
                MEMCPY (NextHopIpv6Addr.u1_addr, NextHopAddr.pu1_OctetList,
                        NextHopAddr.i4_Length);
            }
            else
            {
#endif
                MPLS_OCTETSTRING_TO_INTEGER ((&NextHopAddr), u4NextHopAddr);
#ifdef MPLS_IPV6_WANTED
            }
#endif

            MPLS_OID_TO_INTEGER ((&ActionPointer), u4XCIndex,
                                 MPLS_XC_INDEX_START_OFFSET);
            MPLS_INTEGER_TO_OCTETSTRING (u4XCIndex, (&XCIndex));
            /* check if XCOwner is admin */
            if ((SNMP_FAILURE ==
                 nmhGetMplsXCOwner (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                                    &i4XCOwner)))
            {
                continue;
            }
            if (MPLS_OWNER_SNMP != i4XCOwner)
            {
                continue;
            }

            /* fetch top label */
#ifdef MPLS_IPV6_WANTED

            /* get OutSegment Interface */
            if (SNMP_FAILURE ==
                nmhGetMplsOutSegmentInterface (&OutSegmentIndex, &i4OutIfIndex))
            {
                continue;
            }
#ifdef CFA_WANTED
            /* get tunnel interface */
            if (CFA_FAILURE ==
                CfaUtilGetIfIndexFromMplsTnlIf (i4OutIfIndex, &u4L3VlanOutIf,
                                                TRUE))
            {
                continue;
            }
            /* get interface name */
            if (CFA_SUCCESS !=
                CfaCliConfGetIfName (u4L3VlanOutIf, pi1OutIfName))
            {
                continue;
            }
#endif
            if (DestAddrMin.i4_Length == IPV6_ADDR_LENGTH)
            {
                CliPrintf (CliHandle, "mpls static binding ipv6 ");
                FilePrintf (CliHandle, "mpls static binding ipv6 ");

                CliPrintf (CliHandle, "%s ", Ip6PrintAddr (&Ipv6MinAddr));
                FilePrintf (CliHandle, "%s ", Ip6PrintAddr (&Ipv6MinAddr));

                CliPrintf (CliHandle, "%d ", u1DestPrefixLen);
                FilePrintf (CliHandle, "%d ", u1DestPrefixLen);

                CliPrintf (CliHandle, "output %s ",
                           Ip6PrintAddr (&NextHopIpv6Addr));
                FilePrintf (CliHandle, "output %s ",
                            Ip6PrintAddr (&NextHopIpv6Addr));

                CliPrintf (CliHandle, "%s ", pi1OutIfName);
                FilePrintf (CliHandle, "%s ", pi1OutIfName);
            }
            else
            {
#endif
                CliPrintf (CliHandle, "mpls static binding ipv4 ");
                FilePrintf (CliHandle, "mpls static binding ipv4 ");

                CLI_CONVERT_IPADDR_TO_STR (pc1Prefix, u4Prefix);
                CliPrintf (CliHandle, "%s ", pc1Prefix);
                FilePrintf (CliHandle, "%s ", pc1Prefix);

                CLI_CONVERT_IPADDR_TO_STR (pc1MaxAddr, u4Mask);
                CliPrintf (CliHandle, "%s ", pc1MaxAddr);
                FilePrintf (CliHandle, "%s ", pc1MaxAddr);

                CLI_CONVERT_IPADDR_TO_STR (pc1NextHopAddr, u4NextHopAddr);
                CliPrintf (CliHandle, "output %s ", pc1NextHopAddr);
                FilePrintf (CliHandle, "output %s ", pc1NextHopAddr);
#ifdef MPLS_IPV6_WANTED
            }
#endif
            CliPrintf (CliHandle, "%u\r\n", u4OutSegLabel);
            FilePrintf (CliHandle, "%u\r\n", u4OutSegLabel);
        }
    }
    u4XCIndex = 0;
    while (1)
    {
        if ((u4XCIndex != 0) &&
            ((nmhGetNextIndexMplsXCTable
              (&XCIndexPrev, &XCIndex, &InSegmentIndexPrev, &InSegmentIndex,
               &OutSegmentIndexPrev, &OutSegmentIndex)) == SNMP_FAILURE))
        {
            break;
        }
        if ((u4XCIndex == 0)
            &&
            (nmhGetFirstIndexMplsXCTable
             (&XCIndex, &InSegmentIndex, &OutSegmentIndex)) == SNMP_FAILURE)
        {
            return CLI_SUCCESS;
        }
        MEMCPY (XCIndexPrev.pu1_OctetList, XCIndex.pu1_OctetList,
                XCIndex.i4_Length);
        XCIndexPrev.i4_Length = XCIndex.i4_Length;

        MEMCPY (InSegmentIndexPrev.pu1_OctetList, InSegmentIndex.pu1_OctetList,
                InSegmentIndex.i4_Length);
        InSegmentIndexPrev.i4_Length = InSegmentIndex.i4_Length;

        MEMCPY (OutSegmentIndexPrev.pu1_OctetList,
                OutSegmentIndex.pu1_OctetList, OutSegmentIndex.i4_Length);
        OutSegmentIndexPrev.i4_Length = OutSegmentIndex.i4_Length;

        MPLS_OCTETSTRING_TO_INTEGER ((&InSegmentIndex), u4InIndex);
        MPLS_OCTETSTRING_TO_INTEGER ((&OutSegmentIndex), u4OutIndex);
        MPLS_OCTETSTRING_TO_INTEGER ((&XCIndex), u4XCIndex);

        if (TeCheckXcIndex (u4XCIndex, &pTeTnlInfo) == TE_SUCCESS)
        {
            continue;
        }
        if (nmhGetMplsXCOwner (&XCIndex, &InSegmentIndex,
                               &OutSegmentIndex, &i4XCOwner) == SNMP_FAILURE)
        {
            continue;
        }

        if ((u4OutIndex != 0) || (u4InIndex == 0))
        {
            continue;
        }

        if (nmhGetMplsInSegmentLabel (&InSegmentIndex, &u4InSegLabel)
            == SNMP_FAILURE)
        {
            continue;
        }
        if (nmhGetMplsInSegmentInterface (&InSegmentIndex, &i4IfIndex)
            == SNMP_FAILURE)
        {
            continue;
        }
        if (CfaUtilGetIfIndexFromMplsTnlIf
            ((UINT4) i4IfIndex, &u4L3VlanIf, TRUE) == CFA_FAILURE)
        {
            continue;
        }
        if (CFA_SUCCESS != CfaCliConfGetIfName (u4L3VlanIf, pi1IfName))
        {
            continue;
        }

        if (u4InIndex != 0)
        {
            if ((nmhGetMplsInSegmentRowStatus (&InSegmentIndex, &i4InStatus))
                == SNMP_FAILURE)
            {
                continue;
            }
            if (i4InStatus == MPLS_STATUS_ACTIVE)
            {
                if ((nmhGetMplsInSegmentLabel (&InSegmentIndex, &u4InSegLabel))
                    == SNMP_FAILURE)
                {
                    continue;
                }
                if ((nmhGetMplsInSegmentAddrFamily
                     (&InSegmentIndex,
                      (INT4 *) &u4InSegAddrFamily)) == SNMP_FAILURE)
                {
                    continue;
                }
                if (MPLS_IPV4_ADDR_TYPE == u4InSegAddrFamily)
                {
                    CliPrintf (CliHandle, "mpls static binding ipv4 input ");
                    FilePrintf (CliHandle, "mpls static binding ipv4 input");
                }
                else
                {
                    CliPrintf (CliHandle, "mpls static binding ipv6 input");
                    FilePrintf (CliHandle, "mpls static binding ipv6 input");
                }
                CliPrintf (CliHandle, " %s", pi1IfName);
                FilePrintf (CliHandle, " %s", pi1IfName);
                switch (u4InSegLabel)
                {
                    case MPLS_IPV4_EXPLICIT_NULL_LABEL:
                        CliPrintf (CliHandle, " explicit-null\r\n");
                        FilePrintf (CliHandle, " explicit-null\r\n");
                        break;
                    case MPLS_IMPLICIT_NULL_LABEL:
                        CliPrintf (CliHandle, " implicit-null\r\n");
                        FilePrintf (CliHandle, " implicit-null\r\n");
                        break;
                    default:
                        CliPrintf (CliHandle, " %d \r\n", u4InSegLabel);
                        FilePrintf (CliHandle, " %d \r\n", u4InSegLabel);
                }
            }
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : MplsStaticCrossConnectShowRunningConfig  
* Description   : This routine displays the information about the crossconnects
*                 that has been configured  
* Input(s)      : CliHandle - Cli Context Handle
* Output(s)     : None
* Return(s)     : CLI_SUCCESS/CLI_FAILURE
******************************************************************************/
INT1
MplsStaticCrossConnectShowRunningConfig (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE OutSegmentIndexPrev;
    tSNMP_OCTET_STRING_TYPE InSegmentIndexPrev;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCIndexPrev;
    tSNMP_OCTET_STRING_TYPE NextHopAddr;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OID_TYPE      ActionPointer;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4OutSegTopLabel = 0;
    UINT4               u4InSegLabel = 0;
    UINT4               u4OutIndex = 0;
    UINT4               u4InIndex = 0;
    UINT4               u4XCIndex = 0;
    UINT4               u4FtnIndex = 0;
    UINT4               u4FtnXCIndex = 0;
    UINT4               u4FtnInIndex = 0;
    UINT4               u4FtnOutIndex = 0;
#ifdef CFA_WANTED
    UINT4               u4L3VlanIf = 0;
#ifdef MPLS_IPV6_WANTED
    UINT4               u4L3VlanOutIf = 0;
#endif
#endif
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { 0 };
    INT4                i4XCOwner = 0;
    INT4                i4InIfIndex = 0;
    INT4                i4NextHop = 0;
    INT1               *pi1IfName = NULL;
    CHR1               *pc1NextHop = NULL;
    BOOL1               bLoop = FALSE;
    BOOL1               bXcIndexMatch = FALSE;
    static UINT4        au4ActionPointer[MPLS_TE_XC_TABLE_OFFSET];
    static UINT1        au1OutSegIndexPrev[MPLS_INDEX_LENGTH];
    static UINT1        au1InSegIndexPrev[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1NextHopAddr[IPV6_ADDR_LENGTH];
    static UINT1        au1XCIndexPrev[MPLS_INDEX_LENGTH];
    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];
#ifdef MPLS_IPV6_WANTED
    INT4                i4OutIfIndex = 0;
    INT1               *pi1OutIfName = NULL;
    UINT1               au1OutIfName[CFA_MAX_PORT_NAME_LENGTH] = { 0 };
    tIp6Addr            NextHopIpv6Addr;
    MEMSET (au1OutIfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    pi1OutIfName = (INT1 *) &au1OutIfName[0];
    MEMSET (&NextHopIpv6Addr, 0, sizeof (tIp6Addr));
#endif

    MEMSET (au1IfName, 0, CFA_MAX_PORT_NAME_LENGTH);
    pi1IfName = (INT1 *) &au1IfName[0];

    OutSegmentIndexPrev.pu1_OctetList = au1OutSegIndexPrev;
    InSegmentIndexPrev.pu1_OctetList = au1InSegIndexPrev;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    NextHopAddr.pu1_OctetList = au1NextHopAddr;
    XCIndexPrev.pu1_OctetList = au1XCIndexPrev;
    XCIndex.pu1_OctetList = au1XCIndex;
    ActionPointer.pu4_OidList = au4ActionPointer;
    /* scan the XC entry */
    /* get the first index of XC table , in seg and out seg table */
    while (1)
    {
        /* get next XC entry */
        if ((TRUE == bLoop) &&
            ((SNMP_FAILURE ==
              nmhGetNextIndexMplsXCTable (&XCIndexPrev, &XCIndex,
                                          &InSegmentIndexPrev, &InSegmentIndex,
                                          &OutSegmentIndexPrev,
                                          &OutSegmentIndex))))
        {
            return CLI_SUCCESS;
        }

        /* get first XC entry */
        if ((FALSE == bLoop) &&
            ((SNMP_FAILURE ==
              nmhGetFirstIndexMplsXCTable (&XCIndex, &InSegmentIndex,
                                           &OutSegmentIndex))))
        {
            return CLI_SUCCESS;
        }

        bLoop = TRUE;
        MEMCPY (XCIndexPrev.pu1_OctetList, XCIndex.pu1_OctetList,
                XCIndex.i4_Length);
        XCIndexPrev.i4_Length = XCIndex.i4_Length;

        MEMCPY (InSegmentIndexPrev.pu1_OctetList, InSegmentIndex.pu1_OctetList,
                InSegmentIndex.i4_Length);
        InSegmentIndexPrev.i4_Length = InSegmentIndex.i4_Length;

        MEMCPY (OutSegmentIndexPrev.pu1_OctetList,
                OutSegmentIndex.pu1_OctetList, OutSegmentIndex.i4_Length);
        OutSegmentIndexPrev.i4_Length = OutSegmentIndex.i4_Length;

        /* check whether Owner is admin */
        if (SNMP_FAILURE == nmhGetMplsXCOwner (&XCIndex, &InSegmentIndex,
                                               &OutSegmentIndex, &i4XCOwner))
        {
            continue;
        }
        if (MPLS_OWNER_SNMP != i4XCOwner)
        {
            continue;
        }

        MPLS_OCTETSTRING_TO_INTEGER ((&XCIndex), u4XCIndex);
        MPLS_OCTETSTRING_TO_INTEGER ((&InSegmentIndex), u4InIndex);
        MPLS_OCTETSTRING_TO_INTEGER ((&OutSegmentIndex), u4OutIndex);

        if ((0 == u4InIndex) || (0 == u4OutIndex))
        {
            continue;
        }
        if (TeCheckXcIndex (u4XCIndex, &pTeTnlInfo) == TE_SUCCESS)
        {
            continue;
        }

        if ((nmhGetFirstIndexMplsFTNTable (&u4FtnIndex) == SNMP_SUCCESS) &&
            (0 != u4FtnIndex))
        {
            do
            {
                /* get FTN ActionPointer */
                if ((SNMP_FAILURE ==
                     nmhGetMplsFTNActionPointer (u4FtnIndex, &ActionPointer)))
                {
                    continue;
                }
                if (2 == ActionPointer.u4_Length)
                {
                    continue;
                }

                MPLS_OID_TO_INTEGER ((&ActionPointer), u4FtnXCIndex,
                                     MPLS_IN_INDEX_START_OFFSET);
                MPLS_OID_TO_INTEGER ((&ActionPointer), u4FtnInIndex,
                                     MPLS_IN_INDEX_START_OFFSET);
                MPLS_OID_TO_INTEGER ((&ActionPointer), u4FtnOutIndex,
                                     MPLS_OUT_INDEX_START_OFFSET);

                if ((u4FtnXCIndex == u4XCIndex) &&
                    (u4FtnInIndex == u4InIndex)
                    && (u4FtnOutIndex == u4OutIndex))
                {
                    bXcIndexMatch = TRUE;
                    break;
                }
            }
            while ((nmhGetNextIndexMplsFTNTable (u4FtnIndex, &u4FtnIndex)) ==
                   SNMP_SUCCESS);
            if (bXcIndexMatch)
            {
                bXcIndexMatch = FALSE;
                continue;
            }
        }

#ifdef MPLS_IPV6_WANTED
        nmhGetMplsOutSegmentNextHopAddr (&OutSegmentIndex, &NextHopAddr);
        if (NextHopAddr.i4_Length == IPV6_ADDR_LENGTH)
        {
            if (SNMP_FAILURE ==
                nmhGetMplsInSegmentLabel (&InSegmentIndex, &u4InSegLabel))
            {
                continue;
            }

            CliPrintf (CliHandle, "mpls static crossconnect-ipv6 ");
            FilePrintf (CliHandle, "mpls static crossconnect-ipv6 ");
        }
        else
        {
#endif
            if (SNMP_FAILURE ==
                nmhGetMplsInSegmentLabel (&InSegmentIndex, &u4InSegLabel))
            {
                continue;
            }

            CliPrintf (CliHandle, "mpls static crossconnect ");
            FilePrintf (CliHandle, "mpls static crossconnect ");
#ifdef MPLS_IPV6_WANTED
        }
#endif
        if (0 != u4InIndex)
        {
            /* get InSegment Label */
            if (SNMP_FAILURE ==
                nmhGetMplsInSegmentLabel (&InSegmentIndex, &u4InSegLabel))
            {
                continue;
            }
            /* get InSegment interface */
            if (SNMP_FAILURE ==
                nmhGetMplsInSegmentInterface (&InSegmentIndex, &i4InIfIndex))
            {
                continue;
            }
#ifdef CFA_WANTED
            /* get tunnel interface */
            if (CFA_FAILURE ==
                CfaUtilGetIfIndexFromMplsTnlIf ((UINT4) i4InIfIndex,
                                                &u4L3VlanIf, TRUE))
            {
                continue;
            }
            /* get interface name */
            if (CFA_SUCCESS != CfaCliConfGetIfName (u4L3VlanIf, pi1IfName))
            {
                continue;
            }
            CliPrintf (CliHandle, "%s ", pi1IfName);
            FilePrintf (CliHandle, "%s ", pi1IfName);
            CliPrintf (CliHandle, "%u ", u4InSegLabel);
            FilePrintf (CliHandle, "%u ", u4InSegLabel);
#endif
        }

        if (0 != u4OutIndex)
        {
            /* get OutSegment Label */
            if (SNMP_FAILURE == nmhGetMplsOutSegmentTopLabel (&OutSegmentIndex,
                                                              &u4OutSegTopLabel))
            {
                continue;
            }
            /* get NextHop */
            if (SNMP_FAILURE ==
                nmhGetMplsOutSegmentNextHopAddr (&OutSegmentIndex,
                                                 &NextHopAddr))
            {
                continue;
            }
#ifdef MPLS_IPV6_WANTED
            if (NextHopAddr.i4_Length == IPV6_ADDR_LENGTH)
            {
                /* get InSegment interface */
                if (SNMP_FAILURE ==
                    nmhGetMplsOutSegmentInterface (&OutSegmentIndex,
                                                   &i4OutIfIndex))
                {
                    continue;
                }
#ifdef CFA_WANTED
                /* get tunnel interface */
                if (CFA_FAILURE ==
                    CfaUtilGetIfIndexFromMplsTnlIf ((UINT4) i4OutIfIndex,
                                                    &u4L3VlanOutIf, TRUE))
                {
                    continue;
                }
                /* get interface name */
                if (CFA_SUCCESS !=
                    CfaCliConfGetIfName (u4L3VlanOutIf, pi1OutIfName))
                {
                    continue;
                }
#endif

                MEMCPY (NextHopIpv6Addr.u1_addr, NextHopAddr.pu1_OctetList,
                        NextHopAddr.i4_Length);

                CliPrintf (CliHandle, "%s ", Ip6PrintAddr (&NextHopIpv6Addr));
                FilePrintf (CliHandle, "%s ", Ip6PrintAddr (&NextHopIpv6Addr));

                CliPrintf (CliHandle, "%s ", pi1OutIfName);
                FilePrintf (CliHandle, "%s ", pi1OutIfName);
            }
            else
            {
#endif
                MPLS_OCTETSTRING_TO_INTEGER ((&NextHopAddr), i4NextHop);

                CLI_CONVERT_IPADDR_TO_STR (pc1NextHop, i4NextHop);
                CliPrintf (CliHandle, "%s ", pc1NextHop);
                FilePrintf (CliHandle, "%s ", pc1NextHop);
#ifdef MPLS_IPV6_WANTED
            }
#endif

            if (0 == u4OutSegTopLabel)
            {
                CliPrintf (CliHandle, "explicit-null\r\n");
                FilePrintf (CliHandle, "explicit-null\r\n");
            }
            else if (3 == u4OutSegTopLabel)
            {
                CliPrintf (CliHandle, "implicit-null\r\n");
                FilePrintf (CliHandle, "implicit-null\r\n");
            }
            else
            {
                CliPrintf (CliHandle, "%u\r\n", u4OutSegTopLabel);
                FilePrintf (CliHandle, "%u\r\n", u4OutSegTopLabel);
            }
        }
    }
}

/******************************************************************************
* Function Name : MplsCliDisplayOutLabels
* Description   : This routine displays the labels for FTN binding information
* Input(s)      : CliHandle             - Cli Context Handle
*                 u4NextHopAddr         - Next Hop Address
*                 u4OutSegLabel         - Top Label (Tunnel Label if LDPoRSVP,
*                                                    LDP Label if Normal)
*                 u4OutStackLabel       - Bottom Label (LDP Label if LDPoRSVP,
*                                                       Invalid if Normal)
* Output(s)     : None
* Return(s)     : None
******************************************************************************/
PRIVATE VOID
MplsCliDisplayOutLabels (tCliHandle CliHandle, UINT4 u4NextHopAddr,
                         UINT4 u4OutSegLabel, UINT4 u4OutStackLabel)
{
    CHR1               *pc1NextHopAddr = NULL;

    CLI_CONVERT_IPADDR_TO_STR (pc1NextHopAddr, u4NextHopAddr);

    switch (u4OutSegLabel)
    {
        case MPLS_IPV4_EXPLICIT_NULL_LABEL:
            CliPrintf (CliHandle, "\n  %-15s explicit-null", pc1NextHopAddr);
            FilePrintf (CliHandle, "\n  %-15s explicit-null", pc1NextHopAddr);
            break;
        case MPLS_IMPLICIT_NULL_LABEL:
            CliPrintf (CliHandle, "\n  %-15s implicit-null", pc1NextHopAddr);
            FilePrintf (CliHandle, "\n  %-15s implicit-null", pc1NextHopAddr);
            break;
        default:
            CliPrintf (CliHandle,
                       "\n  %-15s %d", pc1NextHopAddr, u4OutSegLabel);
            FilePrintf (CliHandle,
                        "\n  %-15s %d", pc1NextHopAddr, u4OutSegLabel);
            break;
    }

    if (u4OutStackLabel != MPLS_INVALID_LABEL)
    {
        switch (u4OutStackLabel)
        {
            case MPLS_IPV4_EXPLICIT_NULL_LABEL:
                CliPrintf (CliHandle, ", explicit-null\r\n");
                FilePrintf (CliHandle, ", explicit-null\r\n");
                break;
            case MPLS_IMPLICIT_NULL_LABEL:
                CliPrintf (CliHandle, ", implicit-null\r\n");
                FilePrintf (CliHandle, ", implicit-null\r\n");
                break;
            default:
                CliPrintf (CliHandle, ", %d\r\n", u4OutStackLabel);
                FilePrintf (CliHandle, ", %d\r\n", u4OutStackLabel);
                break;
        }
    }
    else
    {
        CliPrintf (CliHandle, "\r\n");
        FilePrintf (CliHandle, "\r\n");
    }
}

/******************************************************************************
* Function Name : MplsCliDisplayXcLabel
* Description   : This routine displays the label for XC information
* Input(s)      : CliHandle             - Cli Context Handle
*                 u4Label               - Label
* Output(s)     : None
* Return(s)     : MPLS_TRUE  - if Valid label
*                 MPLS_FALSE - if Label 0 or Label 3
******************************************************************************/
PRIVATE             BOOL1
MplsCliDisplayXcLabel (tCliHandle CliHandle, UINT4 u4Label)
{
    BOOL1               bFlag = MPLS_FALSE;

    if (u4Label == MPLS_IPV4_EXPLICIT_NULL_LABEL)
    {
        CliPrintf (CliHandle, "explicit-null");
        FilePrintf (CliHandle, "explicit-null");
    }
    else if (u4Label == MPLS_IMPLICIT_NULL_LABEL)
    {
        CliPrintf (CliHandle, "implicit-null");
        FilePrintf (CliHandle, "implicit-null");
    }
    else
    {
        CliPrintf (CliHandle, "%6d", u4Label);
        FilePrintf (CliHandle, "%6d", u4Label);
        bFlag = MPLS_TRUE;
    }

    return bFlag;
}

/******************************************************************************
* Function Name : MplsCliRouterId
* Description   : This routine used to configure the Router-ID for MPLS
* Input(s)      : CliHandle             - Cli Context Handle
*                 u4IfType              - Interface type
*                 u4Value               - Router Id value 
*                 i4Status              - Router configuration/deconfiguration 
* Output(s)     : None
* Return(s)     : CLI_SUCCESS/CLI_FAILURE
******************************************************************************/

PRIVATE INT4
MplsCliRouterId (tCliHandle CliHandle, UINT4 u4IfType, UINT4 u4Value,
                 INT4 i4Status)
{
    tSNMP_OCTET_STRING_TYPE RouterId;
    tNetIpv4IfInfo      NetIpIfInfo;
    UINT4               u4ErrorCode = MPLS_ZERO;
    UINT4               u4Port = MPLS_ZERO;

    UINT4               u4RouterId = 0;
    RouterId.pu1_OctetList = (UINT1 *) &u4RouterId;

    if (i4Status == CLI_ENABLE)
    {
        if (u4IfType != CFA_NONE)
        {
            if (NetIpv4GetPortFromIfIndex (u4Value, &u4Port) == NETIPV4_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to get the Port from If"
                           " Index\n");
                FilePrintf (CliHandle, "\r%% Unable to get the Port from If"
                            " Index\n");
                return CLI_FAILURE;
            }

            if (NetIpv4GetIfInfo (u4Port, &NetIpIfInfo) == NETIPV4_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to get IP Address from If"
                           " Index\n");
                FilePrintf (CliHandle, "\r%% Unable to get IP Address from If"
                            " Index\n");
                return CLI_FAILURE;
            }
            u4Value = NetIpIfInfo.u4Addr;
        }
        RouterId.i4_Length = ROUTER_ID_LENGTH;

        MPLS_INTEGER_TO_OCTETSTRING (u4Value, (&RouterId));
    }
    else
    {
        RouterId.i4_Length = 0;
    }

    if (i4Status == CLI_ENABLE)
    {
        if (nmhTestv2FsMplsRouterID (&u4ErrorCode, &RouterId) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_ERR_INVALID_ROUTER_ID);
            return CLI_FAILURE;
        }
    }

    if (nmhSetFsMplsRouterID (&RouterId) == SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_ERR_INVALID_ROUTER_ID);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

 /***************************************************************************/
 /*                                                                         */
 /*     Function Name : IssMplsCommonShowDebugging                          */
 /*                                                                         */
 /*     Description   : This function is used to display debug level for    */
 /*                     MPLS COMMON module                                  */
 /*                                                                         */
 /*     INPUT         : CliHandle                                           */
 /*                                                                         */
 /*     OUTPUT        : None                                                */
 /*                                                                         */
 /*     RETURNS       : None                                                */
 /*                                                                         */
 /***************************************************************************/
VOID
IssMplsCommonShowDebugging (tCliHandle CliHandle)
{
    UINT4               u4MplsDbDebugLevel = 0x0;

    MPLS_CMN_LOCK ();
    u4MplsDbDebugLevel = gu4MplsDbTraceFlag;

    if (u4MplsDbDebugLevel == CMNDB_LVL_ALL)
    {
        CliPrintf (CliHandle, "\rMPLS COMMON :");
        CliPrintf (CliHandle, "\r\n  MPLS common database debugging is on");
        CliPrintf (CliHandle, "\r\n");
    }

    MPLS_CMN_UNLOCK ();
    return;
}

/* STATIC_HLSP */
/****************************************************************************
 *
 *    Function Name      : MplsCreateTnlIntfOverHLSP 
 *
 *    Description        : This function creates the  MPLS
 *                         Tunnel Interface over HLSP Tunnel in ifTable or ifMainTable.
 *
 *                         This function should only be called from management
 *                         task like SNMP, CLI and WEB since it calls nmhSet
 *                         routines to delete the interface.
 *                         This function should not be called from any
 *                         protocol task.
 *
 *                         Created interface index is returned to the caller.
 *
 *    Input(s)           : u4HlspIntf - HLSP Tunnel interface on which service tunnel
 *                       is to be created.
 *                         
 *                         u4IfIndex  - MPLS Tunnel IfIndex created.
 *
 *    Output(s)          : None.
 *
 *    Returns            : MPLS_SUCCESS or MPLS_FAILURE.
 *
 *****************************************************************************/

INT4
MplsCreateTnlIntfOverHLSP (UINT4 u4HlspIntf, UINT4 u4IfIndex)
{
    UINT1               u1IfType = 0;

    if (CFA_FAILURE == CfaGetIfType (u4HlspIntf, &u1IfType))
    {
        return MPLS_FAILURE;
    }
    if (u1IfType != CFA_MPLS_TUNNEL)
    {
        return MPLS_FAILURE;
    }
    if (MplsStackMplsIfOrMplsTnlIf (u4HlspIntf, u4IfIndex,
                                    CFA_MPLS_TUNNEL) == MPLS_FAILURE)
    {
        return MPLS_FAILURE;
    }
    return MPLS_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name      : MplsDeleteTnlIntfOverHLSP 
 *
 *    Description        : This function deletes the  MPLS
 *                         Tunnel Interface over HLSP Tunnel in ifTable or ifMainTable.
 *
 *                         This function should only be called from management
 *                         task like SNMP, CLI and WEB since it calls nmhSet
 *                         routines to delete the interface.
 *                         This function should not be called from any
 *                         protocol task.
 *
 *                         Created interface index is returned to the caller.
 *
 *    Input(s)           : u4HlspIntf - HLSP Tunnel interface on which service tunnel
 *                       is stacked.
 *                         
 *                         u4IfIndex  - MPLS Tunnel IfIndex.
 *
 *    Output(s)          : None.
 *
 *    Returns            : MPLS_SUCCESS or MPLS_FAILURE.
 *
 *****************************************************************************/

INT4
MplsDeleteTnlIntfOverHLSP (UINT4 u4HlspIntf, UINT4 u4IfIndex, UINT1 u1Flag)
{

    UINT1               u1IfType = 0;

    if (CFA_FAILURE == CfaGetIfType (u4HlspIntf, &u1IfType))
    {
        return MPLS_FAILURE;
    }
    if (u1IfType != CFA_MPLS_TUNNEL)
    {
        return MPLS_FAILURE;
    }

    if (MplsDeleteMplsIfOrMplsTnlIf (u4HlspIntf, u4IfIndex,
                                     CFA_MPLS_TUNNEL, u1Flag) == CFA_FAILURE)
    {
        return MPLS_FAILURE;
    }
    return MPLS_SUCCESS;
}

/******************************************************************************
 * Function Name : MplsCliSignallingCaps
 * Description   : This routine is called to set the Signalling capabilities. 
 * Input(s)      : CliHandle             - Cli Context Handle
 *                 UINT1                 - u1SigCaps
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS - If success
 *                 CLI_FAILURE - if failure
 * ******************************************************************************/
INT1
MplsCliSignallingCaps (tCliHandle CliHandle, UINT1 u1SigCaps)
{
    UINT4               u4ErrorCode = MPLS_ZERO;
    UINT4               u4MplsInterfaceIndex = MPLS_ZERO;
    static UINT1        au1Length[MPLS_ONE] = { 0 };
    tSNMP_OCTET_STRING_TYPE MplsSignallingCaps;

    MplsSignallingCaps.pu1_OctetList = au1Length;
    MplsSignallingCaps.i4_Length = MPLS_ONE;

    if ((nmhGetGmplsInterfaceSignalingCaps (u4MplsInterfaceIndex,
                                            &MplsSignallingCaps)) ==
        SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if ((GMPLS_SIG_CAP_UNKNOWN_BIT != u1SigCaps)
        && (GMPLS_SIG_CAP_UNKNOWN_BIT !=
            MplsSignallingCaps.pu1_OctetList[MPLS_ZERO]))
    {
        MplsSignallingCaps.pu1_OctetList[MPLS_ZERO] =
            MplsSignallingCaps.pu1_OctetList[MPLS_ZERO] | u1SigCaps;
    }
    else
    {
        MplsSignallingCaps.pu1_OctetList[MPLS_ZERO] = u1SigCaps;
    }

    if ((nmhTestv2GmplsInterfaceSignalingCaps (&u4ErrorCode,
                                               u4MplsInterfaceIndex,
                                               &MplsSignallingCaps))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Cannot set GMPLS Signalling Capabilities\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetGmplsInterfaceSignalingCaps (u4MplsInterfaceIndex,
                                           &MplsSignallingCaps) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Cannot set GMPLS Signalling Capabilities\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : MplsCliSetMaxWaitTimers
 * Description   : This routine is called to set the Max Wait Timers          
 * Input(s)      : CliHandle             - Cli Context Handle
 *                 UINT1                 - u1Flag. TRUE/FALSE. If Set to False,
 *                                         sets Rsvpte Max Wait Timer. Else
 *                                         sets LDP Max Wait Timer.
 *                 i4GrMaxWaitTime       - GR Max Wait Timer. If Passed -1,
 *                                         sets the Default Values.
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS - If success
 *                 CLI_FAILURE - if failure
 * ******************************************************************************/

INT1
MplsCliSetMaxWaitTimers (tCliHandle CliHandle, UINT1 u1Flag,
                         INT4 i4GrMaxWaitTime)
{
    UINT4               u4ErrCode = MPLS_ZERO;

    if (i4GrMaxWaitTime == -1)
    {
        i4GrMaxWaitTime = MPLS_CLI_DEF_GR_MAX_WAIT_TMR;
    }
    /* If it is Passed True, Set the fsMplsRsvpTeGrMaxWaitTime
     * Object.
     * Else set fsMplsLdpGrMaxWaitTime Object
     */
    if (u1Flag == TRUE)
    {
        if (nmhTestv2FsMplsRsvpTeGrMaxWaitTime (&u4ErrCode,
                                                i4GrMaxWaitTime)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Cannot set RsvpTe GR Max Wait Timer\r\n");

            return CLI_FAILURE;
        }
        if (nmhSetFsMplsRsvpTeGrMaxWaitTime (i4GrMaxWaitTime) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Cannot set RsvpTe GR Max Wait Timer\r\n");
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsMplsLdpGrMaxWaitTime (&u4ErrCode,
                                             i4GrMaxWaitTime) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Cannot set LDP GR Max Wait Timer\r\n");

            return CLI_FAILURE;
        }
        if (nmhSetFsMplsLdpGrMaxWaitTime (i4GrMaxWaitTime) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Cannot set LDP GR Max Wait Timer\r\n");
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : MplsCliTunnelModel
 * Description   : This routine is called to set the Tunnel Model.
 * Input(s)      : CliHandle             - Cli Context Handle
 *                 UINT1                 - Tunnel Model
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS - If success
 *                 CLI_FAILURE - if failure
 * ****************************************************************************/
INT1
MplsCliTunnelModel (tCliHandle CliHandle, UINT1 u1TnlMdl)
{
    UINT4               u4ErrorCode = MPLS_ZERO;
    UNUSED_PARAM (CliHandle);
    if ((nmhTestv2FsMplsTnlModel (&u4ErrorCode, u1TnlMdl)) == SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_ERR_INVALID_TUNNEL_MODEL);
        return CLI_FAILURE;
    }

    nmhSetFsMplsTnlModel (u1TnlMdl);

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : MplsCliShowTunnelModel
 * Description   : This routine is called to set the Tunnel Model.
 * Input(s)      : CliHandle             - Cli Context Handle
 *                : Output(s)     : None
 * Return(s)     : CLI_SUCCESS - If success
 *                 CLI_FAILURE - if failure
 * ****************************************************************************/
INT4
MplsCliShowTunnelMode (tCliHandle CliHandle)
{

    INT4                i4TnlModel = 0;

    nmhGetFsMplsTnlModel (&i4TnlModel);
    if (i4TnlModel == MPLS_TNL_UNIFORM_MODEL)
    {
        CliPrintf (CliHandle, "\r\nThe tunnel mode is: Uniform\r\n");
    }
    else if (i4TnlModel == MPLS_TNL_PIPE_MODEL)
    {
        CliPrintf (CliHandle, "\r\nThe tunnel mode is: Pipe\r\n");
    }
    else if (i4TnlModel == MPLS_TNL_SHORTPIPE_MODEL)
    {
        CliPrintf (CliHandle, "\r\nThe tunnel mode is: Short-Pipe\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r\nUnknown tunnel mode\r\n");
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * * Function Name : MplsCliShowTTL
 * * Description   : This routine is called to show the TTL Value.
 * * Input(s)      : CliHandle             - Cli Context Handle
 * *               : Output(s)     : TTL Value
 * * Return(s)     : CLI_SUCCESS - CLI_success
 * *                 CLI_FAILURE - CLI_failure
 ******************************************************************************/
INT4
MplsCliShowTTL (tCliHandle CliHandle)
{

    INT4                i4TTLValue = 0;

    if (nmhGetFsMplsTTLVal (&i4TTLValue) == SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\nThe TTL Value is: %d\r\n", i4TTLValue);
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 *  * Function Name : MplsCliTTLValue
 *  * Description   : This routine is called to set the TTL Value.
 *  * Input(s)      : CliHandle            - Cli Context Handle
 *  *                 INT4                 - TTL Value
 *  * Output(s)     : None
 *  * Return(s)     : CLI_SUCCESS - If success
 *  *                 CLI_FAILURE - if failure
 *  * *************************************************************************/
INT1
MplsCliTTLValue (tCliHandle CliHandle, INT4 u4TTLval)
{
    UINT4               u4ErrorCode = MPLS_ZERO;
    UNUSED_PARAM (CliHandle);

    if ((nmhTestv2FsMplsTTLVal (&u4ErrorCode, u4TTLval)) == SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_ERR_INVALID_TTL_VALUE);
        return CLI_FAILURE;
    }

    nmhSetFsMplsTTLVal (u4TTLval);
    return CLI_SUCCESS;
}

/******************************************************************************
 * * Function Name : MplsTTLShowRunningConfig
 t* * Description   : This routine displays the information about the MPLS TTL
 * *                 that has been configured
 * * Input(s)      : CliHandle - Cli Context Handle
 * * Output(s)     : None
 * * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 * ****************************************************************************/
INT1
MplsTTLShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4TTLvalue;

    /* To retrieve the TTL Value of the MPLS */
    if ((nmhGetFsMplsTTLVal (&i4TTLvalue) == SNMP_SUCCESS) &&
        (i4TTLvalue != MPLS_DEF_TTL_VAL))
    {
        CliPrintf (CliHandle, "\r\nmpls ttl %d\r\n", i4TTLvalue);
        return CLI_SUCCESS;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 *  * * Function Name : MplsTnlmodelShowRunningConfig
 * * * Description    : This routine displays the information about the Tunnel
 *  * *                 Model that has been configured
 *  * * Input(s)      : CliHandle - Cli Context Handle
 *  * * Output(s)     : None
 *  * * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 * * **************************************************************************/
INT1
MplsTnlmodelShowRunningConfig (tCliHandle CliHandle)
{
    INT4                i4Tnlmodelvalue;

    /* Retrieve the Tunnel model of the MPLS */
    if ((nmhGetFsMplsTnlModel (&i4Tnlmodelvalue) == SNMP_SUCCESS) &&
        (i4Tnlmodelvalue != MPLS_TNL_PIPE_MODEL))
    {
        if (i4Tnlmodelvalue == MPLS_TNL_UNIFORM_MODEL)
        {
            CliPrintf (CliHandle, "\r\nmpls tunnel-mode uniform\r\n");
        }
        else if (i4Tnlmodelvalue == MPLS_TNL_SHORTPIPE_MODEL)
        {
            CliPrintf (CliHandle, "\r\nmpls tunnel-mode short-pipe\r\n");
        }
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 *  Function Name : MplsCliSetAchCodePoint
 *  Description   : This routine is called to set the Ach Code Point.
 *  Input(s)      : CliHandle            - Cli Context Handle
 *                  INT4                 - Ach Code Point Value
 *  Output(s)     : None
 *  Return(s)     : CLI_SUCCESS - If success
 *                  CLI_FAILURE - if failure
 ****************************************************************************/
INT1
MplsCliSetAchCodePoint (tCliHandle CliHandle, INT4 i4AchCodePoint)
{
    UINT4               u4ErrorCode = MPLS_ZERO;

    if (nmhTestv2FsMplsLsrRfc6428CompatibleCodePoint (&u4ErrorCode,
                                                      i4AchCodePoint)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%% Unable to set Ach Code Point. Check the values\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsLsrRfc6428CompatibleCodePoint (i4AchCodePoint) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%% Unable to set Ach Code Point. Check the values\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

#ifdef MPLS_TEST_WANTED
#ifdef LDP_GR_WANTED
INT1
MplsDisplayFTNHwList (tCliHandle CliHandle)
{
    tFTNHwListEntry    *pFTNHwListEntry = NULL;
    UINT4               u4TotalEntries = 0;
    UINT4               u4StaleEntries = 0;

    pFTNHwListEntry = MplsHwListGetFirstFTNHwListEntry ();
    while (pFTNHwListEntry != NULL)
    {

#ifdef MPLS_IPV6_WANTED
        if (MPLS_FTN_HW_LIST_FEC_ADDR_TYPE (pFTNHwListEntry) ==
            MPLS_IPV6_ADDR_TYPE)
        {
            /* Print all the contents of FTN hw List Entry */
            CliPrintf (CliHandle, "FEC: %s\n",
                       Ip6PrintAddr (&MPLS_IPV6_FTN_HW_LIST_FEC
                                     (pFTNHwListEntry)));
            CliPrintf (CliHandle, "Next Hop: %s\n",
                       Ip6PrintAddr (&MPLS_IPV6_FTN_HW_LIST_NEXT_HOP
                                     (pFTNHwListEntry)));
        }
        else
#endif
        {
            CliPrintf (CliHandle, "FEC: %d.%d.%d.%d\n",
                       MPLS_FTN_HW_LIST_FEC (pFTNHwListEntry)[0],
                       MPLS_FTN_HW_LIST_FEC (pFTNHwListEntry)[1],
                       MPLS_FTN_HW_LIST_FEC (pFTNHwListEntry)[2],
                       MPLS_FTN_HW_LIST_FEC (pFTNHwListEntry)[3]);
            CliPrintf (CliHandle, "Next Hop: %d.%d.%d.%d\n",
                       MPLS_FTN_HW_LIST_NEXT_HOP (pFTNHwListEntry)[0],
                       MPLS_FTN_HW_LIST_NEXT_HOP (pFTNHwListEntry)[1],
                       MPLS_FTN_HW_LIST_NEXT_HOP (pFTNHwListEntry)[2],
                       MPLS_FTN_HW_LIST_NEXT_HOP (pFTNHwListEntry)[3]);
        }

        CliPrintf (CliHandle, "Address Type: %d\n",
                   MPLS_FTN_HW_LIST_FEC_ADDR_TYPE (pFTNHwListEntry));

        CliPrintf (CliHandle, "Prefix Len: %d\n",
                   MPLS_FTN_HW_LIST_FEC_PREFIX_LEN (pFTNHwListEntry));

        CliPrintf (CliHandle, "Next Hop Address Type: %d\n",
                   MPLS_FTN_HW_LIST_NEXT_HOP_ADDR_TYPE (pFTNHwListEntry));

        CliPrintf (CliHandle, "OutLabel: %d\n",
                   MPLS_FTN_HW_LIST_LABEL (pFTNHwListEntry));

        CliPrintf (CliHandle, "Out Tunnel Interface: %d\n",
                   MPLS_FTN_HW_LIST_OUT_TNL_INTF (pFTNHwListEntry));

        CliPrintf (CliHandle, "Out L3 Interface: %d\n",
                   MPLS_FTN_HW_LIST_OUT_L3_INTF (pFTNHwListEntry));

        CliPrintf (CliHandle, "NP Bit Status MASK: %d\n",
                   MPLS_FTN_HW_LIST_NP_STATUS (pFTNHwListEntry));

        CliPrintf (CliHandle, "Stale Status: %d\n",
                   MPLS_FTN_HW_LIST_STALE_STATUS (pFTNHwListEntry));

        CliPrintf (CliHandle, "IS STATIC: %d\n",
                   MPLS_FTN_HW_LIST_IS_STATIC (pFTNHwListEntry));

        if (MPLS_FTN_HW_LIST_STALE_STATUS (pFTNHwListEntry) == MPLS_TRUE)
        {
            u4StaleEntries++;
        }
        u4TotalEntries++;
        CliPrintf (CliHandle, "\r\n\n");

        pFTNHwListEntry = MplsHwListGetNextFTNHwListEntry (pFTNHwListEntry);
    }

    CliPrintf (CliHandle, "Total FTN HW List Entries: %d\n", u4TotalEntries);
    CliPrintf (CliHandle, "Total FTN HW List Stale Entries: %d\n",
               u4StaleEntries);
    return CLI_SUCCESS;
}

INT1
MplsDisplayILMHwList (tCliHandle CliHandle)
{
    tILMHwListEntry    *pILMHwListEntry = NULL;
    UINT4               u4TotalEntries = 0;
    UINT4               u4StaleEntries = 0;

    pILMHwListEntry = MplsHwListGetFirstILMHwListEntry ();
    while (pILMHwListEntry != NULL)
    {
#ifdef MPLS_IPV6_WANTED
        if (MPLS_ILM_HW_LIST_FEC_ADDR_TYPE (pILMHwListEntry) ==
            MPLS_IPV6_ADDR_TYPE)
        {
            CliPrintf (CliHandle, "FEC: %s\n",
                       Ip6PrintAddr (&MPLS_IPV6_ILM_HW_LIST_FEC
                                     (pILMHwListEntry)));
        }
        else
#endif
        {
            CliPrintf (CliHandle, "FEC: %d.%d.%d.%d\n",
                       MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[0],
                       MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[1],
                       MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[2],
                       MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[3]);
        }

        CliPrintf (CliHandle, "FEC Address Type: %d\n",
                   MPLS_ILM_HW_LIST_FEC_ADDR_TYPE (pILMHwListEntry));

        CliPrintf (CliHandle, "FEC prefix Len: %d\n",
                   MPLS_ILM_HW_LIST_FEC_PREFIX_LEN (pILMHwListEntry));

#ifdef MPLS_IPV6_WANTED
        if (MPLS_ILM_HW_LIST_NEXT_HOP_ADDR_TYPE (pILMHwListEntry) ==
            MPLS_IPV6_ADDR_TYPE)
        {
            CliPrintf (CliHandle, "Next Hop: %s\n",
                       Ip6PrintAddr (&MPLS_IPV6_ILM_HW_LIST_NEXT_HOP
                                     (pILMHwListEntry)));
        }
        else
#endif
        {
            CliPrintf (CliHandle, "Next Hop: %d.%d.%d.%d\n",
                       MPLS_ILM_HW_LIST_NEXT_HOP (pILMHwListEntry)[0],
                       MPLS_ILM_HW_LIST_NEXT_HOP (pILMHwListEntry)[1],
                       MPLS_ILM_HW_LIST_NEXT_HOP (pILMHwListEntry)[2],
                       MPLS_ILM_HW_LIST_NEXT_HOP (pILMHwListEntry)[3]);
        }

        CliPrintf (CliHandle, "Next Hop Address Type: %d\n",
                   MPLS_ILM_HW_LIST_NEXT_HOP_ADDR_TYPE (pILMHwListEntry));

        CliPrintf (CliHandle, "In L3 Interface: %d\n",
                   MPLS_ILM_HW_LIST_IN_L3_INTF (pILMHwListEntry));

        CliPrintf (CliHandle, "Out Tunnel Interface: %d\n",
                   MPLS_ILM_HW_LIST_OUT_TNL_INTF (pILMHwListEntry));

        CliPrintf (CliHandle, "In Label: %d\n",
                   MPLS_ILM_HW_LIST_IN_LABEL (pILMHwListEntry));

        CliPrintf (CliHandle, "Swap Label: %d\n",
                   MPLS_ILM_HW_LIST_SWAP_LABEL (pILMHwListEntry));

        CliPrintf (CliHandle, "Out L3 Interface: %d\n",
                   MPLS_ILM_HW_LIST_OUT_L3_INTF (pILMHwListEntry));

        CliPrintf (CliHandle, "NP Bit Status Mask: %d\n",
                   MPLS_ILM_HW_LIST_NP_STATUS (pILMHwListEntry));

        CliPrintf (CliHandle, "Stale Status: %d\n",
                   MPLS_ILM_HW_LIST_STALE_STATUS (pILMHwListEntry));

        CliPrintf (CliHandle, "IS STATIC: %d\n",
                   MPLS_FTN_HW_LIST_IS_STATIC (pILMHwListEntry));

        if (MPLS_ILM_HW_LIST_STALE_STATUS (pILMHwListEntry) == MPLS_TRUE)
        {
            u4StaleEntries++;
        }
        u4TotalEntries++;

        CliPrintf (CliHandle, "\r\n\n");

        pILMHwListEntry = MplsHwListGetNextILMHwListEntry (pILMHwListEntry);
    }
    CliPrintf (CliHandle, "Total ILM HW List Entries: %d\n", u4TotalEntries);
    CliPrintf (CliHandle, "Total ILM HW List Stale Entries: %d\n",
               u4StaleEntries);

    return CLI_SUCCESS;
}
#endif
#endif

/******************************************************************************
 *  Function Name : MplsGetIpv4Ipv6AddrfromCliStr
 *  Description   : This routine is called to set the type of IP Address and 
 converting ascii string to integer.
 *  Input(s)      : IP Address string from the CLI's.
 *  Output(s)     : Address type and Integer IP Address.
 *  Return(s)     : CLIAddr structure.
 ****************************************************************************/
INT1
MplsGetIpv4Ipv6AddrfromCliStr (tCliHandle CliHandle, CONST CHR1 * pInArgs,
                               tGenU4Addr * pCLIAddress)
{
    INT1                i1DotFnd = 0;
    INT1                i1ColFnd = 0;
    tUtlIn6Addr         Ipv6Addr;
    tUtlInAddr          Ipv4Addr;

    /*finding incoming address string If it is ipv4 or ipv6 based
     * on ':' & '.' */
    i1DotFnd = CliIsDelimit ('.', (CONST CHR1 *) pInArgs);
    i1ColFnd = CliIsDelimit (':', (CONST CHR1 *) pInArgs);

    if (i1DotFnd != 0)
    {
        /*converting incoming ascii string to integer for ipv4 address */
        if (0 == INET_ATON4 (pInArgs, &Ipv4Addr))
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "failed in converting ascii string to integer \n");
        }
        Ipv4Addr.u4Addr = OSIX_NTOHL (Ipv4Addr.u4Addr);
        MEMCPY (&(MPLS_IPV4_U4_ADDR (pCLIAddress->Addr)), &(Ipv4Addr.u4Addr),
                IPV4_ADDR_LENGTH);
        pCLIAddress->u2AddrType = MPLS_IPV4_ADDR_TYPE;

        return CLI_SUCCESS;
    }
    else if (i1ColFnd != 0)
    {
        INET_ATON6 (pInArgs, &Ipv6Addr);    /*converting incoming ascii string to integer for ipv6 address */
        MEMCPY (&(pCLIAddress->MPLS_IPV6_U4_ADDR (Addr)), &(Ipv6Addr.u1addr),
                IPV6_ADDR_LENGTH);
        pCLIAddress->u2AddrType = MPLS_IPV6_ADDR_TYPE;

        return CLI_SUCCESS;
    }
    else
    {
        CliPrintf (CliHandle, "\r\n%% Invalid IP Address\r\n");
        return CLI_FAILURE;
    }
}

#ifdef MPLS_IPV6_WANTED

/******************************************************************************
 * Function Name : MplsCliShowIpv6StaticBinding  
 * Description   : This routine displays the static prefix-label binding 
 * Input(s)      : CliHandle  - Cli Context Handle
 *                 u4Flag     - Flag to indicate which bit(s) 
 *                              set for CLI display   
 *                 SBPrefix - This value is taken only when prefix 
 *                              bit is set in the Flag.   
 *                 u4SBMask   - This value is taken only when prefix
 *                              bit is set in the Flag and based on the 
 *                              prefix and mask, entry is searched.
 *                 NextHop  - To find the entry matches with this NextHop 
 *                              and for this operation NextHop bit must be set
 *                 u4StaticFlag - Static Flag - If Set displays only static entries
 *                                            - If not set displayes all entries.  
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS/CLI_FAILURE
 ******************************************************************************/
INT1
MplsCliShowIpv6StaticBinding (tCliHandle CliHandle, UINT4 u4Flag,
                              tGenU4Addr SBPrefix, UINT4 u4SBMask,
                              tGenU4Addr NextHop, UINT4 u4StaticFlag)
{
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OCTET_STRING_TYPE LblStkIndex;
    tSNMP_OCTET_STRING_TYPE DestIpv6AddrMin;
    tSNMP_OCTET_STRING_TYPE DestIpv6AddrMax;
    tSNMP_OCTET_STRING_TYPE NextHopIpv6Addr;
    tSNMP_OID_TYPE      ActionPointer;
    tFtnEntry          *pFtnEntry = NULL;
    INT4                i4XCOwner;
    UINT4               u4FtnIndex = 0;
    UINT4               u4XCIndex = 0;
    UINT4               u4InIndex = 0;
    UINT4               u4OutIndex = 0;
    tIp6Addr            Ipv6MinAddr;
    tIp6Addr            Ipv6MaxAddr;
    tIp6Addr            NextHopAddr;
    INT4                i4PrefixCmpRet;
    INT4                i4NextHopCmpRet = 0;
    UINT1               u1DestPrefixLen = 0;
    UINT4               u4InSegLabel = 0;
    UINT4               u4OutSegLabel = MPLS_INVALID_LABEL;
    UINT4               u4OutStackLabel = MPLS_INVALID_LABEL;
    UINT4               u4LblStkIndex = 0;
    INT4                i4InStatus = 0;
    INT4                i4OutStatus = 0;
    INT4                i4MplsActionType = 0;
    INT4                i4FTNAddrType = 0;
    static UINT4        au4ActionPointer[MPLS_TE_XC_TABLE_OFFSET];
    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1LblStkIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1NextHopIpv6Addr[IPV6_ADDR_LENGTH];
    static UINT1        au1DestIpv6AddrMin[IPV6_ADDR_LENGTH];
    static UINT1        au1DestIpv6AddrMax[IPV6_ADDR_LENGTH];

    MEMSET (&Ipv6MinAddr, 0, sizeof (tIp6Addr));
    MEMSET (&Ipv6MaxAddr, 0, sizeof (tIp6Addr));
    MEMSET (&NextHopAddr, 0, sizeof (tIp6Addr));

    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    LblStkIndex.pu1_OctetList = au1LblStkIndex;
    XCIndex.pu1_OctetList = au1XCIndex;
    NextHopIpv6Addr.pu1_OctetList = au1NextHopIpv6Addr;
    DestIpv6AddrMax.pu1_OctetList = au1DestIpv6AddrMax;
    DestIpv6AddrMin.pu1_OctetList = au1DestIpv6AddrMin;
    ActionPointer.pu4_OidList = au4ActionPointer;
    /* 1. scan the ftn table and get the XC index for each entry */
    /* get the first index of ftn table */

    while (1)
    {
        u4OutStackLabel = MPLS_INVALID_LABEL;
        u4OutSegLabel = MPLS_INVALID_LABEL;

        if ((u4FtnIndex != 0) &&
            ((nmhGetNextIndexMplsFTNTable (u4FtnIndex, &u4FtnIndex)) ==
             SNMP_FAILURE))
        {
            break;
        }

        /* Only executed first time */
        if ((u4FtnIndex == 0) &&
            ((nmhGetFirstIndexMplsFTNTable (&u4FtnIndex)) == SNMP_FAILURE))
        {
            return CLI_SUCCESS;
        }

        if ((nmhGetMplsFTNAddrType (u4FtnIndex, &i4FTNAddrType))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to fetch the FTN "
                       "FTNAddrType\r\n");
            return CLI_FAILURE;
        }

        if (i4FTNAddrType != MPLS_IPV6_ADDR_TYPE)
        {
            continue;
        }

        /* get min and max Ipv6 dest address; get prefix and mask from that */
        if ((nmhGetMplsFTNDestAddrMin (u4FtnIndex, &DestIpv6AddrMin))
            == SNMP_FAILURE)
        {
            continue;
        }

        MEMCPY (Ipv6MinAddr.u1_addr, DestIpv6AddrMin.pu1_OctetList,
                DestIpv6AddrMin.i4_Length);

        i4PrefixCmpRet =
            MEMCMP (Ipv6MinAddr.u1_addr, MPLS_IPV6_U4_ADDR (SBPrefix.Addr),
                    IPV6_ADDR_LENGTH);

        if ((nmhGetMplsFTNDestAddrMax (u4FtnIndex, &DestIpv6AddrMax))
            == SNMP_FAILURE)
        {
            continue;
        }

        MEMCPY (Ipv6MaxAddr.u1_addr, DestIpv6AddrMax.pu1_OctetList,
                DestIpv6AddrMax.i4_Length);

        u1DestPrefixLen = MplsGetIpv6Subnetmasklen (Ipv6MaxAddr.u1_addr);

        if ((nmhGetMplsFTNActionPointer (u4FtnIndex, &ActionPointer))
            == SNMP_FAILURE)
        {
            continue;
        }
        if (ActionPointer.u4_Length == 2)
        {
            CliPrintf (CliHandle, "%s/%d: Incoming Label: none;\n"
                       " Outgoing Labels: none\n",
                       Ip6PrintAddr (&(Ipv6MinAddr)), u1DestPrefixLen);
            continue;
        }

        MPLS_CMN_LOCK ();

        if ((pFtnEntry = MplsGetFtnTableEntry (u4FtnIndex)) == NULL)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "GetMplsFTNActionType Failed :No Entry Present\t\n");
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }

        MPLS_CMN_UNLOCK ();

        i4MplsActionType = pFtnEntry->i4ActionType;

        if (MPLS_FAILURE == MplsGetFtnXcValues (i4MplsActionType,
                                                &ActionPointer, &u4XCIndex,
                                                &u4InIndex, &u4OutIndex))
        {
            continue;
        }

        MPLS_INTEGER_TO_OCTETSTRING (u4XCIndex, (&XCIndex));
        MPLS_INTEGER_TO_OCTETSTRING (u4InIndex, (&InSegmentIndex));
        MPLS_INTEGER_TO_OCTETSTRING (u4OutIndex, (&OutSegmentIndex));

        /* 2.check if XCOwner is admin */
        if ((nmhGetMplsXCOwner (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                                &i4XCOwner)) == SNMP_FAILURE)
        {
            continue;
        }

        if (nmhGetMplsXCLabelStackIndex (&XCIndex, &InSegmentIndex,
                                         &OutSegmentIndex,
                                         &LblStkIndex) == SNMP_FAILURE)
        {
            continue;
        }
        MPLS_OCTETSTRING_TO_INTEGER ((&LblStkIndex), u4LblStkIndex);

        if ((u4StaticFlag == TRUE) && (i4XCOwner != MPLS_OWNER_SNMP))
        {
            continue;
        }

        if (((u4Flag & MPLS_SB_PREFIX) == MPLS_SB_PREFIX) &&
            ((i4PrefixCmpRet != 0) || (u1DestPrefixLen != u4SBMask)))
        {
            continue;
        }

        if ((u4OutIndex != 0) && ((u4Flag & MPLS_SB_REMOTE) == MPLS_SB_REMOTE))
        {
            if ((nmhGetMplsOutSegmentNextHopAddr (&OutSegmentIndex,
                                                  &NextHopIpv6Addr)) ==
                SNMP_FAILURE)
            {
                continue;
            }

            MEMCPY (NextHopAddr.u1_addr, NextHopIpv6Addr.pu1_OctetList,
                    NextHopIpv6Addr.i4_Length);

            i4NextHopCmpRet = MEMCMP (NextHopAddr.u1_addr,
                                      MPLS_IPV6_U4_ADDR (NextHop.Addr),
                                      IPV6_ADDR_LENGTH);
        }

        if (((u4Flag & MPLS_SB_NEXTHOP) == MPLS_SB_NEXTHOP) &&
            (i4NextHopCmpRet != 0))
        {
            continue;
        }

        if (u4LblStkIndex != MPLS_ZERO)
        {
            if (nmhGetMplsLabelStackLabel (&LblStkIndex, MPLS_ONE,
                                           &u4OutStackLabel) == SNMP_FAILURE)
            {
                continue;
            }
        }

        /* 4. print inseglabel if in seg entry is present */
        if ((u4InIndex != 0) && ((u4Flag & MPLS_SB_LOCAL) == MPLS_SB_LOCAL))
        {
            if ((nmhGetMplsInSegmentRowStatus (&InSegmentIndex, &i4InStatus))
                == SNMP_FAILURE)
            {
                continue;
            }
            if (i4InStatus == MPLS_STATUS_ACTIVE)
            {
                if ((nmhGetMplsInSegmentLabel (&InSegmentIndex, &u4InSegLabel))
                    == SNMP_FAILURE)
                {
                    continue;
                }
                switch (u4InSegLabel)
                {
                    case MPLS_IPV4_EXPLICIT_NULL_LABEL:
                        CliPrintf (CliHandle, "%s/%d: Incoming Label:\
                                explicit-null\r\n", Ip6PrintAddr (&(Ipv6MinAddr)), u1DestPrefixLen);
                        break;

                    case MPLS_IMPLICIT_NULL_LABEL:
                        CliPrintf (CliHandle, "%s/%d: Incoming Label:\
                                implicit-null\r\n", Ip6PrintAddr (&(Ipv6MinAddr)), u1DestPrefixLen);
                        break;

                    default:
                        CliPrintf (CliHandle, "%s/%d: Incoming Label:"
                                   " %d (in LIB)\r\n",
                                   Ip6PrintAddr (&(Ipv6MinAddr)),
                                   u1DestPrefixLen, u4InSegLabel);
                }
            }
            else
            {
                CliPrintf (CliHandle, "%s/%d: Incoming Label: none;\r\n",
                           Ip6PrintAddr (&(Ipv6MinAddr)), u1DestPrefixLen);

            }
        }
        else if ((u4Flag & MPLS_SB_LOCAL) == MPLS_SB_LOCAL)
        {
            CliPrintf (CliHandle, "%s/%d: Incoming Label: none;\r\n",
                       Ip6PrintAddr (&(Ipv6MinAddr)), u1DestPrefixLen);
        }

        if ((u4OutIndex != 0) && ((u4Flag & MPLS_SB_REMOTE) == MPLS_SB_REMOTE))
        {
            if ((nmhGetMplsOutSegmentRowStatus (&OutSegmentIndex, &i4OutStatus))
                == SNMP_FAILURE)
            {
                continue;
            }
            if (i4OutStatus == MPLS_STATUS_ACTIVE)
            {
                if ((nmhGetMplsOutSegmentTopLabel (&OutSegmentIndex,
                                                   &u4OutSegLabel))
                    == SNMP_FAILURE)
                {
                    continue;
                }

                if ((u4Flag & MPLS_SB_LOCAL) == MPLS_SB_LOCAL)
                {
                    CliPrintf (CliHandle, " Outgoing Labels:");
                }
                else
                {
                    CliPrintf (CliHandle, "%s/%d: Outgoing Labels:",
                               Ip6PrintAddr (&(Ipv6MinAddr)), u1DestPrefixLen);
                }

                MplsCliDisplayIpv6OutLabels (CliHandle, NextHopAddr,
                                             u4OutSegLabel, u4OutStackLabel);
            }
            else
            {
                CliPrintf (CliHandle, " Outgoing Labels: None\r\n");
            }
        }
        else if ((u4Flag & MPLS_SB_REMOTE) == MPLS_SB_REMOTE)
        {
            CliPrintf (CliHandle, " Outgoing Labels: None\r\n");
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : MplsCliDisplayIpv6OutLabels
 * Description   : This routine displays the labels for FTN binding information
 * Input(s)      : CliHandle             - Cli Context Handle
 *                 u4NextHopAddr         - Next Hop Address
 *                 u4OutSegLabel         - Top Label (Tunnel Label if LDPoRSVP,
 *                                                    LDP Label if Normal)
 *                 u4OutStackLabel       - Bottom Label (LDP Label if LDPoRSVP,
 *                                                       Invalid if Normal)
 * Output(s)     : None
 * Return(s)     : None
 ******************************************************************************/
PRIVATE VOID
MplsCliDisplayIpv6OutLabels (tCliHandle CliHandle, tIp6Addr NextHopIpv6Addr,
                             UINT4 u4OutSegLabel, UINT4 u4OutStackLabel)
{

    switch (u4OutSegLabel)
    {
        case MPLS_IPV4_EXPLICIT_NULL_LABEL:
            CliPrintf (CliHandle, "\n  %-15s explicit-null",
                       Ip6PrintAddr (&(NextHopIpv6Addr)));
            FilePrintf (CliHandle, "\n  %-15s explicit-null",
                        Ip6PrintAddr (&(NextHopIpv6Addr)));
            break;
        case MPLS_IMPLICIT_NULL_LABEL:
            CliPrintf (CliHandle, "\n  %-15s implicit-null",
                       Ip6PrintAddr (&(NextHopIpv6Addr)));
            FilePrintf (CliHandle, "\n  %-15s implicit-null",
                        Ip6PrintAddr (&(NextHopIpv6Addr)));
            break;
        default:
            CliPrintf (CliHandle,
                       "\n  %-15s %d", Ip6PrintAddr (&(NextHopIpv6Addr)),
                       u4OutSegLabel);
            FilePrintf (CliHandle, "\n  %-15s %d",
                        Ip6PrintAddr (&(NextHopIpv6Addr)), u4OutSegLabel);
            break;
    }

    if (u4OutStackLabel != MPLS_INVALID_LABEL)
    {
        switch (u4OutStackLabel)
        {
            case MPLS_IPV4_EXPLICIT_NULL_LABEL:
                CliPrintf (CliHandle, ", explicit-null\r\n");
                FilePrintf (CliHandle, ", explicit-null\r\n");
                break;
            case MPLS_IMPLICIT_NULL_LABEL:
                CliPrintf (CliHandle, ", implicit-null\r\n");
                FilePrintf (CliHandle, ", implicit-null\r\n");
                break;
            default:
                CliPrintf (CliHandle, ", %d\r\n", u4OutStackLabel);
                FilePrintf (CliHandle, ", %d\r\n", u4OutStackLabel);
                break;
        }
    }
    else
    {
        CliPrintf (CliHandle, "\r\n");
        FilePrintf (CliHandle, "\r\n");
    }
}

#endif
/*****************************************************************************/
/*                                                                           */
/*     FUNCTION NAME    : IssMplsFmShowDebugging                             */
/*                                                                           */
/*     DESCRIPTION      : This function prints the MPLS FM debug level       */
/*                                                                           */
/*     INPUT            : CliHandle                                          */
/*                                                                           */
/*     OUTPUT           : None                                               */
/*                                                                           */
/*     RETURNS          : CLI_SUCCESS/CLI_FAILURE                            */
/*                                                                           */
/*****************************************************************************/
#ifdef FM_WANTED
VOID
IssMplsFmShowDebugging (tCliHandle CliHandle)
{
    INT4                i4gMplsFlag = CLI_FM_ZERO;

    if (SNMP_SUCCESS != (nmhGetFsMplsFmDebugLevel (&(i4gMplsFlag))))
    {
        return;
    }

    if (CLI_FM_ZERO == i4gMplsFlag)
    {
        return;
    }

    CliPrintf (CliHandle, "\rMPLSFM :");

    if ((i4gMplsFlag & MPLSFM_DBG_MEM) != CLI_FM_ZERO)
    {
        CliPrintf (CliHandle, "\r\n  Memory related debugging is on");
    }

    if ((i4gMplsFlag & MPLSFM_DBG_TIMER) != CLI_FM_ZERO)
    {

        CliPrintf (CliHandle, "\r\n  Timer related debugging is on");
    }
    if ((i4gMplsFlag & MPLSFM_DBG_SEM) != CLI_FM_ZERO)
    {
        CliPrintf (CliHandle, "\r\n  Memory related debugging is on");
    }

    if ((i4gMplsFlag & MPLSFM_DBG_TIMER) != CLI_FM_ZERO)
    {

        CliPrintf (CliHandle, "\r\n  Timer related debugging is on");
    }
    if ((i4gMplsFlag & MPLSFM_DBG_SEM) != CLI_FM_ZERO)
    {
        CliPrintf (CliHandle, "\r\n  Semaphore related debugging is on");
    }

    if ((i4gMplsFlag & MPLSFM_DBG_PRCS) != CLI_FM_ZERO)
    {
        CliPrintf (CliHandle, "\r\n  Process related debugging is on");
    }

    if ((i4gMplsFlag & MPLSFM_DBG_SNMP) != CLI_FM_ZERO)
    {
        CliPrintf (CliHandle, "\r\n  Snmp related debugging is on");
    }

    if ((i4gMplsFlag & MPLSFM_DBG_MISC) != CLI_FM_ZERO)
    {
        CliPrintf (CliHandle, "\r\n  Miscellaneous related debugging is on");
    }

    if ((i4gMplsFlag & MPLSFM_DBG_ETEXT) != CLI_FM_ZERO)
    {
        CliPrintf (CliHandle, "\r\n  Entry-Exit related debugging is on");
    }

    if ((i4gMplsFlag & MPLSFM_DBG_IF) != CLI_FM_ZERO)
    {
        CliPrintf (CliHandle, "\r\n  Interfaces related debugging is on");
    }

    if ((i4gMplsFlag & MPLSFM_MAIN_DEBUG) != CLI_FM_ZERO)
    {
        CliPrintf (CliHandle,
                   "\r\n  Displaying of debugging messages from the main module is on");
    }

    if ((i4gMplsFlag & MPLSFM_IF_DEBUG) != CLI_FM_ZERO)
    {
        CliPrintf (CliHandle,
                   "\r\n  Displaying of debugging messages from the interface module is on");
    }

    if ((i4gMplsFlag & MPLSFM_PRCS_DEBUG) != CLI_FM_ZERO)
    {
        CliPrintf (CliHandle,
                   "\r\n  Displaying of debugging messages from the process module is on");
    }

    CliPrintf (CliHandle, "\r\n");
}

#endif
#endif
