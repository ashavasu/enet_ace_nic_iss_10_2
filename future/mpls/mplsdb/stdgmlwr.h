/* $Id: stdgmlwr.h,v 1.1 2011/11/30 09:58:13 siva Exp $ */
#ifndef _STDGMLWR_H
#define _STDGMLWR_H
INT4 GetNextIndexGmplsInterfaceTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterSTDGML(VOID);

VOID UnRegisterSTDGML(VOID);
INT4 GmplsInterfaceSignalingCapsGet(tSnmpIndex *, tRetVal *);
INT4 GmplsInterfaceRsvpHelloPeriodGet(tSnmpIndex *, tRetVal *);
INT4 GmplsInterfaceSignalingCapsSet(tSnmpIndex *, tRetVal *);
INT4 GmplsInterfaceRsvpHelloPeriodSet(tSnmpIndex *, tRetVal *);
INT4 GmplsInterfaceSignalingCapsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 GmplsInterfaceRsvpHelloPeriodTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 GmplsInterfaceTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexGmplsInSegmentTable(tSnmpIndex *, tSnmpIndex *);
INT4 GmplsInSegmentDirectionGet(tSnmpIndex *, tRetVal *);
INT4 GmplsInSegmentExtraParamsPtrGet(tSnmpIndex *, tRetVal *);
INT4 GmplsInSegmentDirectionSet(tSnmpIndex *, tRetVal *);
INT4 GmplsInSegmentExtraParamsPtrSet(tSnmpIndex *, tRetVal *);
INT4 GmplsInSegmentDirectionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 GmplsInSegmentExtraParamsPtrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 GmplsInSegmentTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexGmplsOutSegmentTable(tSnmpIndex *, tSnmpIndex *);
INT4 GmplsOutSegmentDirectionGet(tSnmpIndex *, tRetVal *);
INT4 GmplsOutSegmentTTLDecrementGet(tSnmpIndex *, tRetVal *);
INT4 GmplsOutSegmentExtraParamsPtrGet(tSnmpIndex *, tRetVal *);
INT4 GmplsOutSegmentDirectionSet(tSnmpIndex *, tRetVal *);
INT4 GmplsOutSegmentTTLDecrementSet(tSnmpIndex *, tRetVal *);
INT4 GmplsOutSegmentExtraParamsPtrSet(tSnmpIndex *, tRetVal *);
INT4 GmplsOutSegmentDirectionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 GmplsOutSegmentTTLDecrementTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 GmplsOutSegmentExtraParamsPtrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 GmplsOutSegmentTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
#endif /* _STDGMLWR_H */
