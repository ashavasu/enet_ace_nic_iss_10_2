/* $Id: stdgmlwr.c,v 1.1 2011/11/30 09:58:13 siva Exp $ */
# include  "lr.h" 
# include  "fssnmp.h" 
# include  "stdgmllw.h"
# include  "stdgmlwr.h"
# include  "stdgmldb.h"

INT4 GetNextIndexGmplsInterfaceTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexGmplsInterfaceTable(
			&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexGmplsInterfaceTable(
			pFirstMultiIndex->pIndex[0].i4_SLongValue,
			&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}


VOID RegisterSTDGML ()
{
	SNMPRegisterMib (&stdgmlOID, &stdgmlEntry, SNMP_MSR_TGR_TRUE);
	SNMPAddSysorEntry (&stdgmlOID, (const UINT1 *) "stdgmlsr");
}



VOID UnRegisterSTDGML ()
{
	SNMPUnRegisterMib (&stdgmlOID, &stdgmlEntry);
	SNMPDelSysorEntry (&stdgmlOID, (const UINT1 *) "stdgmlsr");
}

INT4 GmplsInterfaceSignalingCapsGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceGmplsInterfaceTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetGmplsInterfaceSignalingCaps(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->pOctetStrValue));

}
INT4 GmplsInterfaceRsvpHelloPeriodGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceGmplsInterfaceTable(
		pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetGmplsInterfaceRsvpHelloPeriod(
		pMultiIndex->pIndex[0].i4_SLongValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 GmplsInterfaceSignalingCapsSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetGmplsInterfaceSignalingCaps(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->pOctetStrValue));

}

INT4 GmplsInterfaceRsvpHelloPeriodSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetGmplsInterfaceRsvpHelloPeriod(
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 GmplsInterfaceSignalingCapsTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2GmplsInterfaceSignalingCaps(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->pOctetStrValue));

}

INT4 GmplsInterfaceRsvpHelloPeriodTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2GmplsInterfaceRsvpHelloPeriod(pu4Error,
		pMultiIndex->pIndex[0].i4_SLongValue,
		pMultiData->u4_ULongValue));

}

INT4 GmplsInterfaceTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2GmplsInterfaceTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexGmplsInSegmentTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexGmplsInSegmentTable(
			pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexGmplsInSegmentTable(
			pFirstMultiIndex->pIndex[0].pOctetStrValue,
			pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 GmplsInSegmentDirectionGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceGmplsInSegmentTable(
		pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetGmplsInSegmentDirection(
		pMultiIndex->pIndex[0].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 GmplsInSegmentExtraParamsPtrGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceGmplsInSegmentTable(
		pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetGmplsInSegmentExtraParamsPtr(
		pMultiIndex->pIndex[0].pOctetStrValue,
		pMultiData->pOidValue));

}
INT4 GmplsInSegmentDirectionSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetGmplsInSegmentDirection(
		pMultiIndex->pIndex[0].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 GmplsInSegmentExtraParamsPtrSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetGmplsInSegmentExtraParamsPtr(
		pMultiIndex->pIndex[0].pOctetStrValue,
		pMultiData->pOidValue));

}

INT4 GmplsInSegmentDirectionTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2GmplsInSegmentDirection(pu4Error,
		pMultiIndex->pIndex[0].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 GmplsInSegmentExtraParamsPtrTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2GmplsInSegmentExtraParamsPtr(pu4Error,
		pMultiIndex->pIndex[0].pOctetStrValue,
		pMultiData->pOidValue));

}

INT4 GmplsInSegmentTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2GmplsInSegmentTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}


INT4 GetNextIndexGmplsOutSegmentTable(tSnmpIndex *pFirstMultiIndex,tSnmpIndex * pNextMultiIndex)
{
	if (pFirstMultiIndex == NULL) 
	{
		if (nmhGetFirstIndexGmplsOutSegmentTable(
			pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
		{
 			return SNMP_FAILURE;
		}
	}
	else
	{
		if (nmhGetNextIndexGmplsOutSegmentTable(
			pFirstMultiIndex->pIndex[0].pOctetStrValue,
			pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
		{
			return SNMP_FAILURE;
		}
	}
	
	return SNMP_SUCCESS;
}
INT4 GmplsOutSegmentDirectionGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceGmplsOutSegmentTable(
		pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetGmplsOutSegmentDirection(
		pMultiIndex->pIndex[0].pOctetStrValue,
		&(pMultiData->i4_SLongValue)));

}
INT4 GmplsOutSegmentTTLDecrementGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceGmplsOutSegmentTable(
		pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetGmplsOutSegmentTTLDecrement(
		pMultiIndex->pIndex[0].pOctetStrValue,
		&(pMultiData->u4_ULongValue)));

}
INT4 GmplsOutSegmentExtraParamsPtrGet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	if (nmhValidateIndexInstanceGmplsOutSegmentTable(
		pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
	{
		return SNMP_FAILURE;
	}
	return(nmhGetGmplsOutSegmentExtraParamsPtr(
		pMultiIndex->pIndex[0].pOctetStrValue,
		pMultiData->pOidValue));

}
INT4 GmplsOutSegmentDirectionSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetGmplsOutSegmentDirection(
		pMultiIndex->pIndex[0].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 GmplsOutSegmentTTLDecrementSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetGmplsOutSegmentTTLDecrement(
		pMultiIndex->pIndex[0].pOctetStrValue,
		pMultiData->u4_ULongValue));

}

INT4 GmplsOutSegmentExtraParamsPtrSet(tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhSetGmplsOutSegmentExtraParamsPtr(
		pMultiIndex->pIndex[0].pOctetStrValue,
		pMultiData->pOidValue));

}

INT4 GmplsOutSegmentDirectionTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2GmplsOutSegmentDirection(pu4Error,
		pMultiIndex->pIndex[0].pOctetStrValue,
		pMultiData->i4_SLongValue));

}

INT4 GmplsOutSegmentTTLDecrementTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2GmplsOutSegmentTTLDecrement(pu4Error,
		pMultiIndex->pIndex[0].pOctetStrValue,
		pMultiData->u4_ULongValue));

}

INT4 GmplsOutSegmentExtraParamsPtrTest(UINT4 *pu4Error ,tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
	return (nmhTestv2GmplsOutSegmentExtraParamsPtr(pu4Error,
		pMultiIndex->pIndex[0].pOctetStrValue,
		pMultiData->pOidValue));

}

INT4 GmplsOutSegmentTableDep(UINT4 *pu4Error, tSnmpIndexList *pSnmpIndexList, tSNMP_VAR_BIND *pSnmpvarbinds)
{
	return(nmhDepv2GmplsOutSegmentTable(pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

