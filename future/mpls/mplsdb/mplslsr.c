/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: mplslsr.c,v 1.39 2014/08/25 11:58:53 siva Exp $
 *
 * Description: Routines for Mpls LSR Module
 ********************************************************************/
#include "mplsdbinc.h"
#include "mplcmndb.h"
#include "snmputil.h"
/* MPLS_P2MP_LSP_CHANGES - S */
#include "mplsnp.h"
/* MPLS_P2MP_LSP_CHANGES - E */

/* Pool ID's */
#define  IF_TABLE_POOL_ID      MPLSDBMemPoolIds[MAX_MPLSDB_LSR_IFENTRY_SIZING_ID]
#define  INSEGMENT_POOL_ID     MPLSDBMemPoolIds[MAX_MPLSDB_LSR_INSEGMENT_SIZING_ID]
#define  XC_POOL_ID            MPLSDBMemPoolIds[MAX_MPLSDB_LSR_XCENTRY_SIZING_ID]
#define  OUTSEGMENT_POOL_ID    MPLSDBMemPoolIds[MAX_MPLSDB_LSR_OUTSEGMENT_SIZING_ID]
#define  LABEL_STACK_POOL_ID   MPLSDBMemPoolIds[MAX_MPLSDB_LSR_LBLSTKENTRY_SIZING_ID]
#define  LABEL_POOL_ID         MPLSDBMemPoolIds[MAX_MPLSDB_LABEL_ENTRY_SIZING_ID]

/* RedBlack Tree ID's */
static struct rbtree *gpIfTableRbTree = NULL;
static struct rbtree *gpInSegmentRbTree = NULL;
static struct rbtree *gpSignalInSegmentRbTree = NULL;
static struct rbtree *gpXcTableRbTree = NULL;
static struct rbtree *gpOutSegmentRbTree = NULL;
static struct rbtree *gpLblStackRbTree = NULL;
/* gOutSegmentLabelBasedRBTree is implemented ONLY FOR RSVP-TE Graceful Restart
 * Purpose. Static configurations will not get reflected in this
 * RBTree
 */
tRBTree             gOutSegmentLabelBasedRBTree;

/* Red Black Tree Compare Functions Prototypes */
INT4                MplsRbTreeIfTableCmpFunc (tRBElem * pRBElem1,
                                              tRBElem * pRBElem2);
INT4                MplsRbTreeInSegmentCmpFunc (tRBElem * pRBElem1,
                                                tRBElem * pRBElem2);
INT4                MplsRbTreeSignalInSegmentCmpFunc (tRBElem * pRBElem1,
                                                      tRBElem * pRBElem2);
INT4                MplsRbTreeOutSegmentCmpFunc (tRBElem * pRBElem1,
                                                 tRBElem * pRBElem2);
INT4                MplsRbTreeXcTableCmpFunc (tRBElem * pRBElem1,
                                              tRBElem * pRBElem2);
INT4                MplsRbTreeLblStackCmpFunc (tRBElem * pRBElem1,
                                               tRBElem * pRBElem2);

INT4                MplsOutSegmentLabelCmpFunc (tRBElem * pRBElem1,
                                                tRBElem * pRBElem2);

/************************************************************************
 *  Function Name   : MplsLSRInit 
 *  Description     : Function to Init LSR Module 
 *  Input           : None
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE
 ************************************************************************/
INT4
MplsLSRInit (VOID)
{
    /* Red Black Tree Create */
    gpIfTableRbTree =
        RBTreeCreate (gu4MplsDbMaxIfEntries, MplsRbTreeIfTableCmpFunc);
    if (gpIfTableRbTree == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "If Table RB Tree Create Failed \t\n");
        MplsLSRDeInit ();
        return MPLS_FAILURE;
    }

    gpInSegmentRbTree =
        RBTreeCreate (gu4MplsDbMaxInSegEntries, MplsRbTreeInSegmentCmpFunc);
    if (gpInSegmentRbTree == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "InSegment Table RB Tree Create Failed \t\n");
        MplsLSRDeInit ();
        return MPLS_FAILURE;
    }

    gpSignalInSegmentRbTree =
        RBTreeCreate (gu4MplsDbMaxInSegEntries,
                      MplsRbTreeSignalInSegmentCmpFunc);
    if (gpSignalInSegmentRbTree == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "InSegment Table RB Tree Create2 Failed \t\n");
        MplsLSRDeInit ();
        return MPLS_FAILURE;
    }

    gpOutSegmentRbTree =
        RBTreeCreate (gu4MplsDbMaxOutSegEntries, MplsRbTreeOutSegmentCmpFunc);
    if (gpOutSegmentRbTree == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "OutSegment Table RB Tree Create Failed \t\n");
        MplsLSRDeInit ();
        return MPLS_FAILURE;
    }

    gpXcTableRbTree =
        RBTreeCreate (gu4MplsDbMaxXCEntries, MplsRbTreeXcTableCmpFunc);
    if (gpXcTableRbTree == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "XC Table RB Tree Create Failed \t\n");
        MplsLSRDeInit ();
        return MPLS_FAILURE;
    }

    gpLblStackRbTree =
        RBTreeCreate (gu4MplsDbMaxLblStackEntries, MplsRbTreeLblStackCmpFunc);
    if (gpLblStackRbTree == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Label Stack Table RB Tree Create Failed \t\n");
        MplsLSRDeInit ();
        return MPLS_FAILURE;
    }
    gOutSegmentLabelBasedRBTree = RBTreeCreateEmbedded
        (FSAP_OFFSETOF (tOutSegment, OutSegmentRBNode),
         MplsOutSegmentLabelCmpFunc);

    if (gOutSegmentLabelBasedRBTree == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Label based OutSement RB Tree Create Failed \t\n");
        MplsLSRDeInit ();
        return MPLS_FAILURE;
    }

    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "LSR Init Success \t\n");

    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsLSRDeInit
 *  Description     : Function to DeInit LSR Module 
 *  Input           : None
 *  Output          : None
 *  Returns         : None 
 ************************************************************************/
VOID
MplsLSRDeInit (VOID)
{

    /* RedBlack Tree DeInit */
    if (gpIfTableRbTree != NULL)
    {
        RBTreeDelete (gpIfTableRbTree);
        gpIfTableRbTree = NULL;
    }

    if (gpInSegmentRbTree != NULL)
    {
        RBTreeDelete (gpInSegmentRbTree);
        gpInSegmentRbTree = NULL;
    }

    if (gpSignalInSegmentRbTree != NULL)
    {
        RBTreeDelete (gpSignalInSegmentRbTree);
        gpSignalInSegmentRbTree = NULL;
    }

    if (gpOutSegmentRbTree != NULL)
    {
        RBTreeDelete (gpOutSegmentRbTree);
        gpOutSegmentRbTree = NULL;
    }

    if (gpXcTableRbTree != NULL)
    {
        RBTreeDelete (gpXcTableRbTree);
        gpXcTableRbTree = NULL;
    }

    if (gpLblStackRbTree != NULL)
    {
        RBTreeDelete (gpLblStackRbTree);
        gpLblStackRbTree = NULL;
    }

    if (gOutSegmentLabelBasedRBTree != NULL)
    {
        RBTreeDelete (gOutSegmentLabelBasedRBTree);
        gOutSegmentLabelBasedRBTree = NULL;
    }

    return;
}

/************************************************************************
 *  Function Name   : MplsRbTreeIfTableCmpFunc 
 *  Description     : RBTree Compare function for Mpls If Table
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0 
 ************************************************************************/
INT4
MplsRbTreeIfTableCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    UINT4               u4Index1 = ((tMplsIfEntry *) pRBElem1)->u4Index;
    UINT4               u4Index2 = ((tMplsIfEntry *) pRBElem2)->u4Index;

    if (u4Index1 > u4Index2)
    {
        return 1;
    }
    else if (u4Index1 < u4Index2)
    {
        return -1;
    }
    return 0;
}

/************************************************************************
 *  Function Name   : MplsRbTreeInSegmentCmpFunc 
 *  Description     : RBTree Compare function for InSegment Table
 *  Input           : Two RBTree Nodes to be compared 
 *  Output          : None
 *  Returns         : 1/(-1)/0  
 ************************************************************************/
INT4
MplsRbTreeInSegmentCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    UINT4               u4Index1 = ((tInSegment *) pRBElem1)->u4Index;
    UINT4               u4Index2 = ((tInSegment *) pRBElem2)->u4Index;

    if (u4Index1 > u4Index2)
    {
        return 1;
    }
    else if (u4Index1 < u4Index2)
    {
        return -1;
    }
    return 0;

}

/************************************************************************
 *  Function Name   : MplsRbTreeSignalInSegmentCmpFunc 
 *  Description     : RBTree Compare function for InSegment Table.
 *                    This will be called from signal module with
 *                    IfIndex and InLabel as arguments                       
 *  Input           : Two RBTree Nodes to be compared 
 *  Output          : None
 *  Returns         : 1/(-1)/0  
 ************************************************************************/
INT4
MplsRbTreeSignalInSegmentCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    UINT4               au4Index1[2], au4Index2[2];
    au4Index1[0] = ((tInSegment *) pRBElem1)->u4IfIndex;
    au4Index1[1] = ((tInSegment *) pRBElem1)->u4Label;
    au4Index2[0] = ((tInSegment *) pRBElem2)->u4IfIndex;
    au4Index2[1] = ((tInSegment *) pRBElem2)->u4Label;

    /*TODO Per interface based label search to be done after 
     * solving the interface type conflicts for MPLS incoming 
     * interface (L3, MPLS/MPLS Tnl interface)
     if (au4Index1[0] > au4Index2[0])
     {
     return 1;
     }
     else if (au4Index1[0] < au4Index2[0])
     {
     return -1;
     }
     */
    if (au4Index1[1] > au4Index2[1])
    {
        return 1;
    }
    else if (au4Index1[1] < au4Index2[1])
    {
        return -1;
    }
    return 0;
}

/************************************************************************
 *  Function Name   : MplsRbTreeOutSegmentCmpFunc
 *  Description     : RBTree Compare function for OutSegment Table 
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0 
 ************************************************************************/
INT4
MplsRbTreeOutSegmentCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    UINT4               u4Index1 = ((tOutSegment *) pRBElem1)->u4Index;
    UINT4               u4Index2 = ((tOutSegment *) pRBElem2)->u4Index;

    if (u4Index1 > u4Index2)
    {
        return 1;
    }
    else if (u4Index1 < u4Index2)
    {
        return -1;
    }
    return 0;
}

/************************************************************************
 *  Function Name   : MplsRbTreeXcTableCmpFunc 
 *  Description     : RBTree Compare function for Cross Connect Table
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None 
 *  Returns         : 1/(-1)/0 
 ************************************************************************/
INT4
MplsRbTreeXcTableCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tXcEntry           *pXcEntry1 = NULL;
    tXcEntry           *pXcEntry2 = NULL;
    tInSegment         *pInSegment1 = NULL;
    tInSegment         *pInSegment2 = NULL;
    tOutSegment        *pOutSegment1 = NULL;
    tOutSegment        *pOutSegment2 = NULL;
    UINT4               u4Index1 = 0;
    UINT4               u4Index2 = 0;

    pXcEntry1 = (tXcEntry *) pRBElem1;
    pXcEntry2 = (tXcEntry *) pRBElem2;

    u4Index1 = pXcEntry1->u4Index;
    u4Index2 = pXcEntry2->u4Index;

    if (u4Index1 > u4Index2)
    {
        return 1;
    }
    else if (u4Index1 < u4Index2)
    {
        return -1;
    }

    u4Index1 = u4Index2 = 0;
    pInSegment1 = pXcEntry1->pInIndex;
    pInSegment2 = pXcEntry2->pInIndex;

    if (pInSegment1 != NULL)
    {
        u4Index1 = pInSegment1->u4Index;
    }

    if (pInSegment2 != NULL)
    {
        u4Index2 = pInSegment2->u4Index;
    }

    if (u4Index1 > u4Index2)
    {
        return 1;
    }
    else if (u4Index1 < u4Index2)
    {
        return -1;
    }

    u4Index1 = u4Index2 = 0;
    pOutSegment1 = pXcEntry1->pOutIndex;
    pOutSegment2 = pXcEntry2->pOutIndex;

    if (pOutSegment1 != NULL)
    {
        u4Index1 = pOutSegment1->u4Index;
    }

    if (pOutSegment2 != NULL)
    {
        u4Index2 = pOutSegment2->u4Index;
    }

    if (u4Index1 > u4Index2)
    {
        return 1;
    }
    else if (u4Index1 < u4Index2)
    {
        return -1;
    }

    return 0;
}

/************************************************************************
 *  Function Name   : MplsRbTreeLblStackCmpFunc
 *  Description     : RBTree Compare function for Label Stack Table
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None 
 *  Returns         : 1/(-1)/0 
 ************************************************************************/
INT4
MplsRbTreeLblStackCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    UINT4               u4Index1 = ((tLblStkEntry *) pRBElem1)->u4Index;
    UINT4               u4Index2 = ((tLblStkEntry *) pRBElem2)->u4Index;

    if (u4Index1 > u4Index2)
    {
        return 1;
    }
    else if (u4Index1 < u4Index2)
    {
        return -1;
    }
    return 0;
}

/************************************************************************
 *  Function Name   : MplsCreateIfTableEntry 
 *  Description     : Function to create an entry in Mpls If Table
 *  Input           : Mpls If Index
 *  Output          : None
 *  Returns         : Pointer of the Entry or NULL
 ************************************************************************/
tMplsIfEntry       *
MplsCreateIfTableEntry (UINT4 u4IfIndex)
{
    tMplsIfEntry       *pMplsIfEntry = NULL;

    if (MplsGetIfTableEntry (u4IfIndex) != NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Create If Table : Entry Present \t\n");
        return NULL;
    }

    pMplsIfEntry = (tMplsIfEntry *) MemAllocMemBlk (IF_TABLE_POOL_ID);

    if (pMplsIfEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Create If Table : Pool is Full \t\n");
        return NULL;
    }

    pMplsIfEntry->u4Index = u4IfIndex;

    if (RBTreeAdd (gpIfTableRbTree, (VOID *) pMplsIfEntry) == RB_FAILURE)
    {
        MemReleaseMemBlock (IF_TABLE_POOL_ID, (UINT1 *) pMplsIfEntry);
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Create If Table : RBTree Add Failed \t\n");
        return NULL;

    }
    return pMplsIfEntry;
}

/************************************************************************
 *  Function Name   : MplsGetIfTableEntry
 *  Description     : Function to get Mpls If Entry from Table
 *  Input           : Mpls If Index
 *  Output          : None
 *  Returns         : Pointer of the Entry or NULL 
 ************************************************************************/
tMplsIfEntry       *
MplsGetIfTableEntry (UINT4 u4IfIndex)
{
    tMplsIfEntry        MplsInIfEntry, *pMplsIfEntry = NULL;

    MplsInIfEntry.u4Index = u4IfIndex;
    pMplsIfEntry = RBTreeGet (gpIfTableRbTree, &MplsInIfEntry);
    if (pMplsIfEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Get Mpls If Entry : RB Tree Get Failed \t\n");
    }
    return pMplsIfEntry;
}

/************************************************************************
 *  Function Name   : MplsDeleteIfTableEntry 
 *  Description     : Function to delete a if entry from if table
 *  Input           : Mpls If Entry Pointer 
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE
 ************************************************************************/
INT4
MplsDeleteIfTableEntry (tMplsIfEntry * pMplsIfEntry)
{
    RBTreeRem (gpIfTableRbTree, pMplsIfEntry);
    MemReleaseMemBlock (IF_TABLE_POOL_ID, (UINT1 *) pMplsIfEntry);
    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsIfTableNextEntry 
 *  Description     : Function to get the Next Mpls If Entry from the 
 *                    If Table. If u4IfIndex ==0 then get the first 
 *                    entry                 
 *  Input           : Mpls If Index for which next entry is required
 *  Output          : None
 *  Returns         : Next Mpls If Entry Pointer or NULL
 ************************************************************************/
tMplsIfEntry       *
MplsIfTableNextEntry (UINT4 u4IfIndex)
{
    tMplsIfEntry        MplsInIfEntry;
    if (gpIfTableRbTree == NULL)
    {
        return NULL;
    }
    if (u4IfIndex == gu4MplsDbMaxIfEntries)
    {
        return (RBTreeGetFirst (gpIfTableRbTree));
    }
    MplsInIfEntry.u4Index = u4IfIndex;
    return (RBTreeGetNext (gpIfTableRbTree, &MplsInIfEntry, NULL));
}

/************************************************************************
 *  Function Name   : MplsCreateInSegmentTableEntry 
 *  Description     : Function to create InSegment Table Entry
 *  Input           : In Segment Entry Index
 *  Output          : None
 *  Returns         : Pointer to In Segment Entry or NULL
 ************************************************************************/
tInSegment         *
MplsCreateInSegmentTableEntry (UINT4 u4Index)
{
    tInSegment         *pInSegment = NULL;
    if (MplsGetInSegmentTableEntry (u4Index) != NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Create In Segment Entry : Entry present already \t\n");
        return NULL;
    }

    pInSegment = (tInSegment *) MemAllocMemBlk (INSEGMENT_POOL_ID);

    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Create In Segment Entry : Memory Allocate Failed \t\n");
        return NULL;
    }
    MEMSET (pInSegment, 0, sizeof (tInSegment));
    INSEGMENT_INIT_DEF_VALUES (pInSegment);
    pInSegment->u4Index = u4Index;
    if (RBTreeAdd (gpInSegmentRbTree, (VOID *) pInSegment) == RB_FAILURE)
    {
        MemReleaseMemBlock (INSEGMENT_POOL_ID, (UINT1 *) pInSegment);
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Create In Segment Entry : RBTree Add Failed \t\n");
        return NULL;

    }
    return pInSegment;

}

/************************************************************************
 *  Function Name   : MplsGetInSegmentTableEntry 
 *  Description     : Function to get In Segment Entry from Index
 *  Input           : In Segment Entry Index
 *  Output          : None
 *  Returns         : Pointer to In Segment Entry or NULL
 ************************************************************************/
tInSegment         *
MplsGetInSegmentTableEntry (UINT4 u4Index)
{
    tInSegment          InSegment, *pInSegment = NULL;

    if (gpInSegmentRbTree == NULL)
    {
        return NULL;
    }
    InSegment.u4Index = u4Index;
    pInSegment = RBTreeGet (gpInSegmentRbTree, &InSegment);
    if (pInSegment == NULL)
    {
        return NULL;
    }
    return pInSegment;
}

/************************************************************************
 *  Function Name   : MplsSignalGetInSegmentTableEntry 
 *  Description     : Function to get InSegment Entry from IfIndex and
 *                    In Label  
 *  Input           : Interface Index and In Label
 *  Output          : None
 *  Returns         : Pointer to InSegment Entry or NULL
 ************************************************************************/
tInSegment         *
MplsSignalGetInSegmentTableEntry (UINT4 u4IfIndex, UINT4 u4InLabel)
{
    tInSegment          InSegment, *pInSegment = NULL;

    if (u4InLabel != 0)
    {
        InSegment.u4IfIndex = u4IfIndex;
        InSegment.u4Label = u4InLabel;
        pInSegment = RBTreeGet (gpSignalInSegmentRbTree, &InSegment);
        if (pInSegment != NULL)
        {
            return pInSegment;
        }
    }
    else
    {
        pInSegment = MplsInSegmentTableNextEntry (0);
        while (pInSegment != NULL)
        {
            if ((pInSegment->u4Label == 0) &&
                (pInSegment->u4IfIndex == u4IfIndex) &&
                (pInSegment->mplsLabelIndex == NULL) &&
                (pInSegment->i4NPop == 1))
            {
                return pInSegment;
            }
            pInSegment = MplsInSegmentTableNextEntry (pInSegment->u4Index);
        }
    }
    CMNDB_DBG
        (DEBUG_DEBUG_LEVEL,
         "Get In Segment Entry from Signal : NO Insegment entry exists \t\n");
    return NULL;
}

/************************************************************************
 *  Function Name   : MplsSignalGetTwoInSegmentLabelEntry 
 *  Description     : Function to get InSegment Entry from IfIndex,
 *                    Top Label and Bottom label
 *  Input           : Interface Index, Top Label and Bottom label
 *  Output          : None
 *  Returns         : Pointer to InSegment Entry or NULL
 ************************************************************************/
tInSegment         *
MplsSignalGetTwoInSegmentLabelEntry (UINT4 u4IfIndex, UINT4 u4TopLabel,
                                     UINT4 u4BottomLabel)
{
    UNUSED_PARAM (u4IfIndex);

    return (MplsFwdGetTwoInSegmentLabelEntry (u4TopLabel, u4BottomLabel));
}

/************************************************************************
 *  Function Name   :MplsFwdGetInSegmentTableEntry 
 *  Description     :Function to get InSegment Entry from Label 
 *  Input           : u4InLabel
 *  Output          : None
 *  Returns         : Pointer to InSegment Entry or NULL
 ************************************************************************/
tInSegment         *
MplsFwdGetInSegmentTableEntry (UINT4 u4InLabel)
{
    tInSegment          InSegment;

    if (gpSignalInSegmentRbTree == NULL)
    {
        return NULL;
    }

    InSegment.u4Label = u4InLabel;

    return (RBTreeGet (gpSignalInSegmentRbTree, &InSegment));
}

/************************************************************************
 *  Function Name   : MplsFwdGetTwoInSegmentLabelEntry 
 *  Description     : Function to get InSegment Entry from
 *                    Top Label and Bottom label
 *  Input           : Interface Index, Top Label and Bottom label
 *  Output          : None
 *  Returns         : Pointer to InSegment Entry or NULL
 ************************************************************************/
tInSegment         *
MplsFwdGetTwoInSegmentLabelEntry (UINT4 u4TopLabel, UINT4 u4BottomLabel)
{
    tInSegment         *pInSegment = NULL;
    tLblEntry          *pLblEntry1 = NULL;
    tLblEntry          *pLblEntry2 = NULL;

    pInSegment = MplsInSegmentTableNextEntry (0);

    if (pInSegment == NULL)
    {
        return NULL;
    }

    do
    {
        if ((pInSegment->u4Label == 0) &&
            (pInSegment->mplsLabelIndex != NULL) && (pInSegment->i4NPop == 2))
        {
            if (TMO_SLL_Count (&(pInSegment->mplsLabelIndex->LblList)) != 2)
            {
                continue;
            }
            pLblEntry1 = (tLblEntry *)
                TMO_SLL_First (&(pInSegment->mplsLabelIndex->LblList));
            if (pLblEntry1 == NULL)
            {
                continue;
            }
            pLblEntry2 = (tLblEntry *)
                TMO_SLL_Last (&(pInSegment->mplsLabelIndex->LblList));
            if (pLblEntry2 == NULL)
            {
                continue;
            }
            if ((pLblEntry1->u4Label == u4TopLabel) &&
                (pLblEntry2->u4Label == u4BottomLabel))
            {
                return pInSegment;
            }
        }
    }
    while ((pInSegment = MplsInSegmentTableNextEntry (pInSegment->u4Index))
           != NULL);
    return NULL;
}

/************************************************************************
 *  Function Name   : MplsGetInSegFromBottomLbl 
 *  Description     : Function to get InSegment Entry from
 *                    Bottom label
 *  Input           : u4BottomLabel - Bottom label (LDP Label)
 *  Output          : None
 *  Returns         : Pointer to InSegment Entry or NULL
 ************************************************************************/
tInSegment         *
MplsGetInSegFromBottomLbl (UINT4 u4BottomLabel)
{
    tInSegment         *pInSegment = NULL;
    tLblEntry          *pLblEntry1 = NULL;
    tLblEntry          *pLblEntry2 = NULL;
    UINT4               u4InIndex = MPLS_ZERO;

    while ((pInSegment = MplsInSegmentTableNextEntry (u4InIndex)) != NULL)
    {
        u4InIndex = pInSegment->u4Index;

        if (pInSegment->u4Label != 0)
        {
            continue;
        }

        if (pInSegment->mplsLabelIndex == NULL)
        {
            continue;
        }

        if (pInSegment->i4NPop != MPLS_TWO)
        {
            continue;
        }

        if (TMO_SLL_Count (&(pInSegment->mplsLabelIndex->LblList)) != MPLS_TWO)
        {
            continue;
        }

        pLblEntry1 = (tLblEntry *)
            TMO_SLL_First (&(pInSegment->mplsLabelIndex->LblList));
        if (pLblEntry1 == NULL)
        {
            continue;
        }

        pLblEntry2 = (tLblEntry *)
            TMO_SLL_Last (&(pInSegment->mplsLabelIndex->LblList));
        if (pLblEntry2 == NULL)
        {
            continue;
        }

        if (pLblEntry2->u4Label == u4BottomLabel)
        {
            return pInSegment;
        }
    }

    return NULL;
}

/************************************************************************
 *  Function Name   : MplsGetXcIndexFromInIfIndexAndInLabel 
 *  Description     : Function to get XcIndex from In IfIndex and
 *                    In Label  
 *  Input           : Interface Index and In Label
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE
 ************************************************************************/
UINT1
MplsGetXcIndexFromInIfIndexAndInLabel (UINT4 u4IfIndex, UINT4 u4InLabel,
                                       UINT4 *pu4XcIndex)
{
    tInSegment         *pInSegment = NULL;

    pInSegment = MplsSignalGetInSegmentTableEntry (u4IfIndex, u4InLabel);
    if (pInSegment == NULL || pInSegment->pXcIndex == NULL)
    {
        CMNDB_DBG
            (DEBUG_DEBUG_LEVEL,
             "Get In Segment Entry from Signal : RB Tree Get Failed \t\n");
        return MPLS_FAILURE;
    }
    *pu4XcIndex = ((tXcEntry *) (pInSegment->pXcIndex))->u4Index;
    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsGetOutSegIfIndexFrmXcIndex
 *  Description     : Function to get Out Segment Entry from Index
 *  Input           : Out Segment Entry IfIndex,OutLabel
 *  Output          : None
 *  Returns         : Pointer to Out Segment Entry or NULL

 ************************************************************************/
INT4
MplsGetOutSegIfIndexFrmXcIndex (UINT4 u4XcIndex, UINT4 *pu4OutIfIndex)
{
    tXcEntry            XcEntry, *pXcEntry = NULL;

    XcEntry.u4Index = u4XcIndex;
    MPLS_CMN_LOCK ();
    if (gpXcTableRbTree == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Cross Connect Entry : RB Tree is not created\t\n");
        MPLS_CMN_UNLOCK ();
        return MPLS_FAILURE;
    }
    pXcEntry = RBTreeGet (gpXcTableRbTree, &XcEntry);
    if (pXcEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Cross Connect Entry : RB Tree is not created\t\n");
        MPLS_CMN_UNLOCK ();
        return MPLS_FAILURE;
    }
    *pu4OutIfIndex = pXcEntry->pOutIndex->u4IfIndex;
    MPLS_CMN_UNLOCK ();
    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsGetOutNextHopFrmXcIndex
 *  Description     : Function to get Nexthop address from XC Index
 *  Input           : Xc index
 *  Output          : pu4OutNextHop - Nexthop address
 *  Returns         : MPLS_SUCCESS/MPLS_FAILURE

 ************************************************************************/
INT4
MplsGetOutNextHopFrmXcIndex (UINT4 u4XcIndex, UINT4 *pu4OutNextHop)
{
    tXcEntry           *pXcEntry = NULL;

    MPLS_CMN_LOCK ();
    /* Called with DEF direction coz this func is called from INGRESS
     * and reverse entry will not be having OutSegment */
    pXcEntry = MplsGetXCEntryByDirection (u4XcIndex, MPLS_DEF_DIRECTION);
    if (pXcEntry == NULL || pXcEntry->pOutIndex == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Cross Connect Entry doesn't exist\t\n");
        MPLS_CMN_UNLOCK ();
        return MPLS_FAILURE;
    }
    MEMCPY (pu4OutNextHop, pXcEntry->pOutIndex->NHAddr.u4_addr,
            IPV4_ADDR_LENGTH);
    MPLS_CMN_UNLOCK ();
    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsSignalGetNextInSegmentTableEntry 
 *  Description     : Function to get InSegment Entry from IfIndex and
 *                    In Label  
 *  Input           : Interface Index and In Label
 *  Output          : None
 *  Returns         : Pointer to InSegment Entry or NULL
 ************************************************************************/
tInSegment         *
MplsSignalGetNextInSegmentTableEntry (UINT4 u4IfIndex, UINT4 u4InLabel)
{
    tInSegment          InSegment;

    if (gpSignalInSegmentRbTree == NULL)
    {
        return NULL;
    }
    if ((u4IfIndex == 0) && (u4InLabel == 0))
    {
        return (RBTreeGetFirst (gpSignalInSegmentRbTree));
    }
    InSegment.u4IfIndex = u4IfIndex;
    InSegment.u4Label = u4InLabel;
    return (RBTreeGetNext (gpSignalInSegmentRbTree, &InSegment, NULL));
}

/************************************************************************
 *  Function Name   : MplsDeleteInSegmentTableEntry 
 *  Description     : Delete a InSegment Entry
 *  Input           : Pointer to InSegment Entry
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE
 ************************************************************************/
INT4
MplsDeleteInSegmentTableEntry (tInSegment * pInSegment)
{
    if ((gpInSegmentRbTree == NULL) || (gpSignalInSegmentRbTree == NULL))
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Delete In Segment Entry: RBTree is not created\t\n");
        return MPLS_SUCCESS;
    }
    RBTreeRem (gpInSegmentRbTree, pInSegment);
    RBTreeRem (gpSignalInSegmentRbTree, pInSegment);
    MplsInSegmentRelIndex (pInSegment->u4Index);
    MemReleaseMemBlock (INSEGMENT_POOL_ID, (UINT1 *) pInSegment);
    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsInSegmentTableNextEntry 
 *  Description     : Function to get Next InSegment Entry for the given
 *                    InSegment Index. If Index is NULL then returns 
 *                    First Index                      
 *  Input           : InSegment Index
 *  Output          : None
 *  Returns         : Pointer to Next InSegment Entry or NULL
 ************************************************************************/
tInSegment         *
MplsInSegmentTableNextEntry (UINT4 u4Index)
{
    tInSegment          InSegment;

    if (gpInSegmentRbTree == NULL)
    {
        return NULL;
    }
    if (u4Index == 0)
    {
        return (RBTreeGetFirst (gpInSegmentRbTree));
    }
    InSegment.u4Index = u4Index;
    return (RBTreeGetNext (gpInSegmentRbTree, &InSegment, NULL));
}

/************************************************************************
 *  Function Name   : MplsInSegmentSignalRbTreeAdd 
 *  Description     : Function to add InSegment Entry to Signal RBTree
 *  Input           : InSegment Entry Pointer
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE
 ************************************************************************/
INT4
MplsInSegmentSignalRbTreeAdd (tInSegment * pInSegment)
{
    if (gpSignalInSegmentRbTree == NULL)
    {
        CMNDB_DBG
            (DEBUG_DEBUG_LEVEL,
             "In Segment Add  Signal RB Tree : RBTree is not created \t\n");
        return MPLS_FAILURE;
    }
    if (RBTreeAdd (gpSignalInSegmentRbTree, (VOID *) pInSegment) == RB_FAILURE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "In Segment Add  Signal RB Tree : RBTree Add Failed \t\n");
        return MPLS_FAILURE;
    }
    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsInSegmentSignalRbTreeDelete 
 *  Description     : Function to delete InSegment Entry to Signal RBTree
 *  Input           : InSegment Entry Pointer
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE
 ************************************************************************/
INT4
MplsInSegmentSignalRbTreeDelete (tInSegment * pInSegment)
{
    if (gpSignalInSegmentRbTree == NULL)
    {
        CMNDB_DBG
            (DEBUG_DEBUG_LEVEL,
             "In Segment Delete Signal RB Tree : RBTree is not created \t\n");
        return MPLS_FAILURE;
    }
    RBTreeRem (gpSignalInSegmentRbTree, (VOID *) pInSegment);
    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsCreateOutSegmentTableEntry 
 *  Description     : Function to create OutSegment Table Entry
 *  Input           : Out Segment Entry Index
 *  Output          : None
 *  Returns         : Pointer to Out Segment Entry or NULL

 ************************************************************************/
tOutSegment        *
MplsCreateOutSegmentTableEntry (UINT4 u4Index)
{
    tOutSegment        *pOutSegment = NULL;
    if (MplsGetOutSegmentTableEntry (u4Index) != NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Create Out Segment Entry : Entry present already \t\n");
        return NULL;
    }

    pOutSegment = (tOutSegment *) MemAllocMemBlk (OUTSEGMENT_POOL_ID);

    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Create Out Segment Entry : Memory Allocate Failed \t\n");
        return NULL;
    }
    MEMSET (pOutSegment, 0, sizeof (tOutSegment));
    OUTSEGMENT_INIT_DEF_VALUES (pOutSegment);
    pOutSegment->u4Index = u4Index;
    if (RBTreeAdd (gpOutSegmentRbTree, (VOID *) pOutSegment) == RB_FAILURE)
    {
        MemReleaseMemBlock (OUTSEGMENT_POOL_ID, (UINT1 *) pOutSegment);
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Create Out Segment Entry : RBTree Add Failed \t\n");
        return NULL;
    }
    return pOutSegment;
}

/************************************************************************
 *  Function Name   : MplsGetOutSegmentTableEntry
 *  Description     : Function to get Out Segment Entry from Index
 *  Input           : Out Segment Entry Index
 *  Output          : None
 *  Returns         : Pointer to Out Segment Entry or NULL

 ************************************************************************/
tOutSegment        *
MplsGetOutSegmentTableEntry (UINT4 u4Index)
{
    tOutSegment         OutSegment, *pOutSegment = NULL;
    if (gpOutSegmentRbTree == NULL)
    {
        return NULL;
    }
    OutSegment.u4Index = u4Index;
    pOutSegment = RBTreeGet (gpOutSegmentRbTree, &OutSegment);
    if (pOutSegment == NULL)
    {
        return NULL;
    }
    return pOutSegment;

}

/* MPLS_P2MP_LSP_CHANGES - S */
/************************************************************************
 *  Function Name   : MplsGetOutSegmentPerfStats
 *  Description     : Function to get Out Segment performance statistics
 *  Input           : Out Segment Entry Index
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE

 ************************************************************************/
INT1
MplsGetOutSegmentPerfStats (UINT4 u4OutIndex)
{
    tOutSegment        *pOutSegment = NULL;

    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4OutIndex);
    if (NULL == pOutSegment)
    {
        MPLS_CMN_UNLOCK ();
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n MplsGetOutSegmentPerfStats :"
                   "Indexed Entry Unavailable \r\n");
        return MPLS_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
#ifdef NPAPI_WANTED
    {
        tMplsInputParams    MplsStatsInput;
        tStatsInfo          HCOctetsStatsInfo;
        tStatsInfo          HCPacketsStatsInfo;
        tStatsInfo          ErrorStatsInfo;
        tStatsInfo          DiscardStatsInfo;

        MEMSET (&MplsStatsInput, 0, sizeof (tMplsInputParams));
        HCOctetsStatsInfo.u8Value.u4Hi = 0;
        HCOctetsStatsInfo.u8Value.u4Lo = 0;
        HCPacketsStatsInfo.u8Value.u4Hi = 0;
        HCPacketsStatsInfo.u8Value.u4Lo = 0;
        ErrorStatsInfo.u8Value.u4Hi = 0;
        ErrorStatsInfo.u8Value.u4Lo = 0;
        DiscardStatsInfo.u8Value.u4Hi = 0;
        DiscardStatsInfo.u8Value.u4Lo = 0;

        MplsStatsInput.i1NpReset = FALSE;
        MplsStatsInput.InputType = MPLS_GET_TNL_LBL_STATS;
        MPLS_HW_STATS_INPUT_LABEL (MplsStatsInput) =
            OUTSEGMENT_LABEL (pOutSegment);
        /* Fetch HC Octets */
        MplsFsMplsHwGetMplsStats (&MplsStatsInput,
                                  MPLS_HW_STAT_TUNNEL_OUTBYTES,
                                  &HCOctetsStatsInfo);
        /* Fetch HC Packets */
        MplsFsMplsHwGetMplsStats (&MplsStatsInput,
                                  MPLS_HW_STAT_TUNNEL_OUTPKTS,
                                  &HCPacketsStatsInfo);
        /* Fetch Errors */
        MplsFsMplsHwGetMplsStats (&MplsStatsInput,
                                  MPLS_HW_STAT_TUNNEL_OUTERR, &ErrorStatsInfo);
        /* Fetch Discards */
        MplsFsMplsHwGetMplsStats (&MplsStatsInput,
                                  MPLS_HW_STAT_TNL_OUT_DISCARDS,
                                  &DiscardStatsInfo);

        /* Update outsegment perf table with the retrieved statistics */
        MPLS_CMN_LOCK ();
        FSAP_U8_ASSIGN_HI (&(OUTSEGMENT_PERF_HC_OCTETS (pOutSegment)),
                           HCOctetsStatsInfo.u8Value.u4Hi);
        FSAP_U8_ASSIGN_LO (&(OUTSEGMENT_PERF_HC_OCTETS (pOutSegment)),
                           HCOctetsStatsInfo.u8Value.u4Lo);

        FSAP_U8_ASSIGN_HI (&(OUTSEGMENT_PERF_HC_PKTS (pOutSegment)),
                           HCPacketsStatsInfo.u8Value.u4Hi);
        FSAP_U8_ASSIGN_LO (&(OUTSEGMENT_PERF_HC_PKTS (pOutSegment)),
                           HCPacketsStatsInfo.u8Value.u4Lo);

        OUTSEGMENT_PERF_ERRORS (pOutSegment) = ErrorStatsInfo.u8Value.u4Lo;
        OUTSEGMENT_PERF_DISCARDS (pOutSegment) = DiscardStatsInfo.u8Value.u4Lo;
        MPLS_CMN_UNLOCK ();
    }
#endif
    return MPLS_SUCCESS;
}

/******************************************************************************
 Function Name  :  MplsCheckXCForP2mp 
 Input          :  pXcEntry - Cross Connect Pointer
 Output         :  NONE
 Returns        :  MPLS_SUCCESS / MPLS_FAILURE
****************************************************************************/
UINT1
MplsCheckXCForP2mp (tXcEntry * pXcEntry)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    if (NULL != XC_TNL_TBL_PTR (pXcEntry))
    {
        pTeTnlInfo = XC_TNL_TBL_PTR (pXcEntry);
        if ((MplsCheckP2mpTnlRoleNotEgress (pTeTnlInfo)) == MPLS_SUCCESS)
        {
            return MPLS_SUCCESS;
        }
    }
    return MPLS_FAILURE;
}

/* MPLS_P2MP_LSP_CHANGES - E */

/************************************************************************
 *  Function Name   : MplsGetOutSegmentEntry
 *  Description     : Function to get Out Segment Entry from Index
 *  Input           : Out Segment Entry IfIndex,OutLabel
 *  Output          : None
 *  Returns         : Pointer to Out Segment Entry or NULL

 ************************************************************************/
tOutSegment        *
MplsGetOutSegmentEntry (UINT4 u4IfIndex, UINT4 u4Label)
{
    tOutSegment        *pOutSegment = NULL;

    pOutSegment = MplsOutSegmentTableNextEntry (0);
    while (pOutSegment != NULL)
    {
        if ((u4IfIndex == pOutSegment->u4IfIndex) &&
            (u4Label == pOutSegment->u4Label))
        {
            return pOutSegment;
        }
        pOutSegment = MplsOutSegmentTableNextEntry (pOutSegment->u4Index);
    }
    return NULL;
}

/************************************************************************
 *  Function Name   : MplsCheckIsAdminCreatedMplsTnlIf
 *  Description     : Function checks whether the incoming MPLS Tnl interface
 *                    is created by Admin or not.
 *  Input           : u4IfIndex - MPLS Tnl Interface
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE

 ************************************************************************/
UINT4
MplsCheckIsAdminCreatedMplsTnlIf (UINT4 u4IfIndex)
{
    UINT1               u1IfType = 0;
    INT4                i4StorageType = 0;

    if ((u4IfIndex < MPLS_MIN_TNL_IF_VALUE) ||
        (u4IfIndex > MPLS_MAX_TNL_IF_VALUE))
    {
        return MPLS_SUCCESS;
    }

    if (CfaGetIfaceType (u4IfIndex, &u1IfType) == CFA_FAILURE)
    {
        return MPLS_FAILURE;
    }

    /* When an interface is created, ifType is Invalid.
     * MPLS Tunnel interfaces that are already saved in MPLS-DB
     * should be saved and that are not already saved in MPLS-DB
     * should be skipped. */
    if ((u1IfType != CFA_MPLS_TUNNEL) && (u1IfType != CFA_INVALID_TYPE))
    {
        return MPLS_SUCCESS;
    }

    CfaGetIfMainStorageType (u4IfIndex, &i4StorageType);

    if (i4StorageType == 2)
    {
        return MPLS_FAILURE;
    }

    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsDeleteOutSegmentTableEntry 
 *  Description     : Delete a OutSegment Entry
 *  Input           : Pointer to OutSegment Entry
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE

 ************************************************************************/
INT4
MplsDeleteOutSegmentTableEntry (tOutSegment * pOutSegment)
{
    if (gpOutSegmentRbTree == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Delete Out Segment Entry: RB Tree is not created\t\n");
        return MPLS_FAILURE;
    }
    RBTreeRem (gpOutSegmentRbTree, pOutSegment);
    MplsSetOutLabel (pOutSegment, MPLS_INVALID_LABEL);
    MplsOutSegmentRelIndex (pOutSegment->u4Index);
    MemReleaseMemBlock (OUTSEGMENT_POOL_ID, (UINT1 *) pOutSegment);
    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsOutSegmentTableNextEntry 
 *  Description     : Function to get Next OutSegment Entry for the given
 *                    OutSegment Index. If Index is NULL then returns
 *                    First Index
 *  Input           : OutSegment Index
 *  Output          : None
 *  Returns         : Pointer to Next OutSegment Entry or NULL

 ************************************************************************/
tOutSegment        *
MplsOutSegmentTableNextEntry (UINT4 u4Index)
{
    tOutSegment         OutSegment;
    if (gpOutSegmentRbTree == NULL)
    {
        return NULL;
    }
    if (u4Index == 0)
    {
        return (RBTreeGetFirst (gpOutSegmentRbTree));
    }
    OutSegment.u4Index = u4Index;
    return (RBTreeGetNext (gpOutSegmentRbTree, &OutSegment, NULL));
}

/************************************************************************
 *  Function Name   : MplsCreateXCTableEntry 
 *  Description     : Function to create XC Table Entry
 *  Input           : XC Entry Index,Insegment Index,OutSegmentIndex
 *  Output          : None
 *  Returns         : Pointer to XC Entry or NULL

 ************************************************************************/
tXcEntry           *
MplsCreateXCTableEntry (UINT4 u4Index, UINT4 u4InIndex, UINT4 u4OutIndex)
{
    tXcEntry           *pXcEntry = NULL;
    tInSegment         *pInSegment = NULL;
    tOutSegment        *pOutSegment = NULL;

    /* Create Cross Connect Entry */
    if (MplsGetXCTableEntry (u4Index, u4InIndex, u4OutIndex) != NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Create Cross Connect Table : Entry already Present\t\n");
        return NULL;
    }

    if (gpXcTableRbTree == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Create Cross Connect Table : RB tree is not created\t\n");
        return NULL;
    }
    pXcEntry = (tXcEntry *) MemAllocMemBlk (XC_POOL_ID);

    if (pXcEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Create Cross Connect Table : Memory allocation Failed\t\n");
        return NULL;
    }
    MEMSET (pXcEntry, 0, sizeof (tXcEntry));
    /* Create or Get InSegment Entry and Attach to XC */
    if (u4InIndex != 0)
    {
        if ((pInSegment = MplsGetInSegmentTableEntry (u4InIndex)) == NULL)
        {
            if (MplsInSegmentSetIndex (u4InIndex) != u4InIndex)
            {
                MemReleaseMemBlock (XC_POOL_ID, (UINT1 *) pXcEntry);
                return NULL;
            }
            pInSegment = MplsCreateInSegmentTableEntry (u4InIndex);
            if (pInSegment == NULL)
            {
                MemReleaseMemBlock (XC_POOL_ID, (UINT1 *) pXcEntry);
                return NULL;
            }
        }
    }

    /* Create or Get OutSegment Entry and Attach to XC */
    if (u4OutIndex != 0)
    {
        if ((pOutSegment = MplsGetOutSegmentTableEntry (u4OutIndex)) == NULL)
        {
            if (MplsOutSegmentSetIndex (u4OutIndex) != u4OutIndex)
            {
                MemReleaseMemBlock (XC_POOL_ID, (UINT1 *) pXcEntry);
                return NULL;
            }
            pOutSegment = MplsCreateOutSegmentTableEntry (u4OutIndex);
            if (pOutSegment == NULL)
            {
                MemReleaseMemBlock (XC_POOL_ID, (UINT1 *) pXcEntry);
                return NULL;
            }
        }
    }
    pXcEntry->u4Index = u4Index;

    if (u4InIndex != 0)
    {
        pInSegment->pXcIndex = pXcEntry;
        pXcEntry->pInIndex = pInSegment;    /* Back Pointer */
    }

    if (u4OutIndex != 0)
    {
        pOutSegment->pXcIndex = pXcEntry;
        pXcEntry->pOutIndex = pOutSegment;    /* Back Pointer */
    }

    if (RBTreeAdd (gpXcTableRbTree, (VOID *) pXcEntry) == RB_FAILURE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Create Cross Connect Entry : RBTree Add Failed \t\n");
        if (u4InIndex != 0)
        {
            MplsDeleteInSegmentTableEntry (pInSegment);
            pXcEntry->pInIndex = NULL;
        }

        if (u4OutIndex != 0)
        {
            MplsDeleteOutSegmentTableEntry (pOutSegment);
            pXcEntry->pOutIndex = NULL;
        }

        MemReleaseMemBlock (XC_POOL_ID, (UINT1 *) pXcEntry);
        return NULL;
    }

    /* Fill the LSP Id Length value for both static and
     * dynamic cases
     * */

    if (XC_OWNER (pXcEntry) == MPLS_OWNER_CRLDP)
    {
        pXcEntry->u1LspIdLen = MAX_LSP_ID;
    }
    else
    {
        pXcEntry->u1LspIdLen = MIN_LSP_ID;
    }

    return pXcEntry;
}

/************************************************************************
 *  Function Name   : MplsGetXCTableEntry 
 *  Description     : Function to get XC Entry from Index
 *  Input           : u4XcIndex   - XC Index
 *                    u4InIndex   - InSegment Index
 *                    u4OutIndex  - OutSegment Index
 *  Output          : None
 *  Returns         : Pointer to XC Entry or NULL

 ************************************************************************/
tXcEntry           *
MplsGetXCTableEntry (UINT4 u4XcIndex, UINT4 u4InIndex, UINT4 u4OutIndex)
{
    tXcEntry            XcEntry, *pXcEntry = NULL;
    tInSegment          InSegment;
    tOutSegment         OutSegment;

    XcEntry.pInIndex = &InSegment;
    XcEntry.pOutIndex = &OutSegment;

    if (gpXcTableRbTree == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Get Cross Connect Entry : RB Tree is not created \t\n");
        return NULL;
    }
    XcEntry.u4Index = u4XcIndex;
    XcEntry.pInIndex->u4Index = u4InIndex;
    XcEntry.pOutIndex->u4Index = u4OutIndex;

    pXcEntry = RBTreeGet (gpXcTableRbTree, &XcEntry);
    if (pXcEntry == NULL)
    {
        return NULL;
    }
    return pXcEntry;
}

/************************************************************************
 *  Function Name   : MplsDeleteXCTableEntry 
 *  Description     : Delete a XC Entry
 *  Input           : Pointer to XC  Entry
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE

 ************************************************************************/
INT4
MplsDeleteXCTableEntry (tXcEntry * pXcEntry)
{
    if (gpXcTableRbTree == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Delete Cross Connect Entry: RBTree is not created\t\n");
        return MPLS_FAILURE;
    }

    if (pXcEntry->mplsLabelIndex != NULL)
    {
        MplsDeleteLblStkAndLabelEntries (pXcEntry->mplsLabelIndex);
        pXcEntry->mplsLabelIndex = NULL;
    }

    RBTreeRem (gpXcTableRbTree, pXcEntry);
    /* Remove InSegment and OutSegment Pointers */
    if (pXcEntry->pOutIndex != NULL)
    {
        pXcEntry->pOutIndex->pXcIndex = NULL;
    }
    if (pXcEntry->pInIndex != NULL)
    {
        pXcEntry->pInIndex->pXcIndex = NULL;
    }

    /* Remove the back pointer reference of tunnel from XC */
    pXcEntry->pTeTnlInfo = NULL;

    if (MplsGetXCEntryByDirection (pXcEntry->u4Index,
                                   (eDirection) MPLS_DIRECTION_ANY) == NULL)
    {
        MplsXcTableRelIndex (pXcEntry->u4Index);
    }

    MemReleaseMemBlock (XC_POOL_ID, (UINT1 *) pXcEntry);
    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsXCTableNextEntry 
 *  Description     : Function to get Next XC  Entry for the given
 *                    XC Index. If Index is NULL then returns
 *                    First Index
 *  Input           : u4XcIndex     - XC Index
 *                    u4InIndex     - Insegment Index
 *                    u4OutIndex    - Outsegment Index
 *  Output          : None
 *  Returns         : Pointer to Next XC Entry or NULL

 ************************************************************************/
tXcEntry           *
MplsXCTableNextEntry (UINT4 u4XcIndex, UINT4 u4InIndex, UINT4 u4OutIndex)
{
    tXcEntry            XcEntry;
    tInSegment          InSegment;
    tOutSegment         OutSegment;

    XcEntry.pInIndex = &InSegment;
    XcEntry.pOutIndex = &OutSegment;

    if (gpXcTableRbTree == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Get next Cross Connect Entry: RBTree is not created\n");
        return NULL;
    }
    if (u4XcIndex == 0)
    {
        return (RBTreeGetFirst (gpXcTableRbTree));
    }
    XcEntry.u4Index = u4XcIndex;
    XcEntry.pInIndex->u4Index = u4InIndex;
    XcEntry.pOutIndex->u4Index = u4OutIndex;

    return (RBTreeGetNext (gpXcTableRbTree, &XcEntry, NULL));
}

/************************************************************************
 *  Function Name   : MplsCreateLblStkTableEntry
 *  Description     : Function to create Label Stack Table Entry
 *  Input           : Label Stack Entry Index
 *  Output          : None
 *  Returns         : Pointer to Label Stack Entry or NULL

 ************************************************************************/
tLblStkEntry       *
MplsCreateLblStkTableEntry (UINT4 u4Index)
{

    tLblStkEntry       *pLblStkEntry = NULL;

    if (MplsGetLblStkTableEntry (u4Index) != NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Create Label Stack Entry : Already Present \t\n");
        return NULL;
    }
    if (gpLblStackRbTree == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Create Label Stack Entry : RB Tree is not created \t\n");
        return NULL;
    }
    pLblStkEntry = (tLblStkEntry *) MemAllocMemBlk (LABEL_STACK_POOL_ID);

    if (pLblStkEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Create Label Stack Entry : Memory Allocate Failed \t\n");
        return NULL;
    }
    pLblStkEntry->u4Index = u4Index;
    TMO_SLL_Init (&(pLblStkEntry->LblList));

    if (RBTreeAdd (gpLblStackRbTree, (VOID *) pLblStkEntry) == RB_FAILURE)
    {
        MemReleaseMemBlock (LABEL_STACK_POOL_ID, (UINT1 *) pLblStkEntry);
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Create Label Stack Entry : RBTree Add Failed \t\n");
        return NULL;
    }

    return pLblStkEntry;
}

/************************************************************************
 *  Function Name   : MplsGetLblStkTableEntry 
 *  Description     : Function to Get Label Stack Table Entry
 *  Input           : Label Stack Entry Index
 *  Output          : None
 *  Returns         : Pointer to Label Stack Entry or NULL

 ************************************************************************/
tLblStkEntry       *
MplsGetLblStkTableEntry (UINT4 u4Index)
{
    tLblStkEntry        LblStackEntry, *pLblStkEntry = NULL;

    if (gpLblStackRbTree == NULL)
    {
        return NULL;
    }
    LblStackEntry.u4Index = u4Index;

    pLblStkEntry = RBTreeGet (gpLblStackRbTree, &LblStackEntry);
    if (pLblStkEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Get Label Stack Entry : RBTree Get Faile \t\n");
    }
    return pLblStkEntry;
}

/************************************************************************
 *  Function Name   : MplsDeleteLblStkTableEntry 
 *  Description     : Function to Delete  Label Stack Entry
 *  Input           : Pointer to Label Stack  Entry
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE

 ************************************************************************/
INT4
MplsDeleteLblStkTableEntry (tLblStkEntry * pLblStkEntry)
{
    if (gpLblStackRbTree == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Delete Label Stack Entry: RBTree is not created \t\n");
        return MPLS_FAILURE;
    }

    RBTreeRem (gpLblStackRbTree, pLblStkEntry);
    MplsLblStackRelIndex (pLblStkEntry->u4Index);
    MemReleaseMemBlock (LABEL_STACK_POOL_ID, (UINT1 *) pLblStkEntry);
    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsDeleteLblStkAndLabelEntries 
 *  Description     : Function to Delete  Label Stack Entry and label entires
 *  Input           : Pointer to Label Stack  Entry
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE

 ************************************************************************/
INT4
MplsDeleteLblStkAndLabelEntries (tLblStkEntry * pLblStkEntry)
{
    tLblEntry          *pLblEntry = NULL;
    if (gpLblStackRbTree == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Delete Label Stack Entry: RBTree is not created \t\n");
        return MPLS_FAILURE;
    }

    pLblEntry = (tLblEntry *) TMO_SLL_First (&(pLblStkEntry->LblList));

    while (pLblEntry != NULL)
    {
        TMO_SLL_Delete (&(pLblStkEntry->LblList), &(pLblEntry->Next));
        MemReleaseMemBlock (LABEL_POOL_ID, (UINT1 *) pLblEntry);
        pLblEntry = (tLblEntry *) TMO_SLL_First (&(pLblStkEntry->LblList));
    }

    RBTreeRem (gpLblStackRbTree, pLblStkEntry);
    MplsLblStackRelIndex (pLblStkEntry->u4Index);
    MemReleaseMemBlock (LABEL_STACK_POOL_ID, (UINT1 *) pLblStkEntry);
    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   :MplsLblStkTableNextEntry 
 *  Description     : Function to get Next Label Stack  Entry for the given
 *                    Label Stack Index. If Index is NULL then returns
 *                    First Index
 *  Input           : Label Stack Index
 *  Output          : None
 *  Returns         : Pointer to Next Label Stack Entry or NULL

 ************************************************************************/
tLblStkEntry       *
MplsLblStkTableNextEntry (UINT4 u4Index)
{
    tLblStkEntry        LblStackEntry;
    if (gpLblStackRbTree == NULL)
    {
        return NULL;
    }

    if (u4Index == 0)
    {
        return (RBTreeGetFirst (gpLblStackRbTree));
    }
    LblStackEntry.u4Index = u4Index;
    return (RBTreeGetNext (gpLblStackRbTree, &LblStackEntry, NULL));
}

/************************************************************************
 *  Function Name   :MplsAddLblStkLabelEntry 
 *  Description     :Function to add Label Entry 
 *  Input           :Label Stack Index,Label Index 
 *  Output          :None
 *  Returns         :Pointer to Label Entry 
 ************************************************************************/
tLblEntry          *
MplsAddLblStkLabelEntry (UINT4 u4Index, UINT4 u4LabelIndex)
{
    tLblStkEntry       *pLblStkEntry = NULL;
    tLblEntry          *pLblEntry = NULL;
    pLblStkEntry = MplsGetLblStkTableEntry (u4Index);
    if (pLblStkEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Add Label to Label Stack : No Stack Available \t\n");
        return NULL;
    }
    TMO_SLL_Scan (&(pLblStkEntry->LblList), pLblEntry, tLblEntry *)
    {
        if (pLblEntry->u4LblIndex == u4LabelIndex)
        {
            CMNDB_DBG
                (DEBUG_DEBUG_LEVEL,
                 "Add Label to Label Stack : Label Index already Available \t\n");
            return pLblEntry;
        }
    }

    pLblEntry = (tLblEntry *) MemAllocMemBlk (LABEL_POOL_ID);

    if (pLblEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Add Label to Label Stack : Malloc Failed \t\n");
        return NULL;
    }

    pLblEntry->u4LblIndex = u4LabelIndex;
    TMO_SLL_Add (&(pLblStkEntry->LblList), &(pLblEntry->Next));
    return pLblEntry;
}

/************************************************************************
 *  Function Name   :MplsDeleteLblStkLabelEntry 
 *  Description     :Function to Delete  Label Entry 
 *  Input           :Label Stack Index,Label Index 
 *  Output          :None 
 *  Returns         :MPLS_SUCCESS or MPLS_FAILURE 
 ************************************************************************/
INT4
MplsDeleteLblStkLabelEntry (UINT4 u4Index, UINT4 u4LabelIndex)
{
    tLblStkEntry       *pLblStkEntry = NULL;
    tLblEntry          *pLblEntry = NULL;
    pLblStkEntry = MplsGetLblStkTableEntry (u4Index);
    if (pLblStkEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Delete Label to Label Stack : No Stack Available \t\n");
        return MPLS_FAILURE;
    }
    TMO_SLL_Scan (&(pLblStkEntry->LblList), pLblEntry, tLblEntry *)
    {
        if (pLblEntry->u4LblIndex == u4LabelIndex)
        {
            TMO_SLL_Delete (&(pLblStkEntry->LblList), &(pLblEntry->Next));
            MemReleaseMemBlock (LABEL_POOL_ID, (UINT1 *) pLblEntry);
            return MPLS_SUCCESS;
        }
    }
    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
               "Delete Label to Label Stack : Label Index not Available \t\n");
    return MPLS_FAILURE;
}

/************************************************************************
 *  Function Name   :MplsDeleteStkLblFrmStkLabelEntry 
 *  Description     :Function to Delete  Label Entry 
 *  Input           :Label Stack Index,Label 
 *  Output          :None 
 *  Returns         :MPLS_SUCCESS or MPLS_FAILURE 
 ************************************************************************/
INT4
MplsDeleteStkLblFrmStkLabelEntry (UINT4 u4Index, UINT4 u4StkLabel)
{
    tLblStkEntry       *pLblStkEntry = NULL;
    tLblEntry          *pLblEntry = NULL;
    pLblStkEntry = MplsGetLblStkTableEntry (u4Index);
    if (pLblStkEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Delete Label to Label Stack : No Stack Available \t\n");
        return MPLS_FAILURE;
    }
    TMO_SLL_Scan (&(pLblStkEntry->LblList), pLblEntry, tLblEntry *)
    {
        if (pLblEntry->u4Label == u4StkLabel)
        {
            TMO_SLL_Delete (&(pLblStkEntry->LblList), &(pLblEntry->Next));
            MemReleaseMemBlock (LABEL_POOL_ID, (UINT1 *) pLblEntry);
            return MPLS_SUCCESS;
        }
    }
    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
               "Delete Label to Label Stack : Label Index not Available \t\n");
    return MPLS_FAILURE;
}

/************************************************************************
 *  Function Name   : MplsLSRXcOperNotif 
 *  Description     : This function sends Notification for the XC UP and 
 *                    DOWN to the Manager
 *  Input           : pXcEntry : Pointer to XC Entry
 *                    state    : XC state - Up/DOWN
 *  Output          : NONE
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE
 ************************************************************************/
INT1
MplsLSRXcOperNotif (tXcEntry * pXCEntry, UINT1 u1State)
{
#ifdef SNMP_2_WANTED
    UINT4               u4ArrLen;
    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL, *pOidValue = NULL, *pOidLast = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;

    UINT4               au4snmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    UINT4               au4MplsLSRNotify[] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 2, 0, 0 };
    UINT4               au1MplsXCOperStatus[] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 2, 1, 10, 1, 10, 0, 0, 0, 0, 0, 0 };
    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;

    /* Trap OID construction. Telling Manager that you have received
     * trap for MplsLSRNotifications.x */
    pOid = alloc_oid (sizeof (au4snmpTrapOid) / sizeof (UINT4));
    if (pOid == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsLSRXcOperNotif :Malloc Failed \n");
        return MPLS_FAILURE;
    }
    MEMCPY (pOid->pu4_OidList, au4snmpTrapOid, sizeof (au4snmpTrapOid));
    pOid->u4_Length = sizeof (au4snmpTrapOid) / sizeof (UINT4);

    pOidValue = alloc_oid (sizeof (au4MplsLSRNotify) / sizeof (UINT4));
    if (pOidValue == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsLSRXcOperNotif :Malloc Failed \n");
        free_oid (pOid);
        return MPLS_FAILURE;
    }
    au4MplsLSRNotify[(sizeof (au4MplsLSRNotify) / sizeof (UINT4)) - 1] =
        u1State;
    MEMCPY (pOidValue->pu4_OidList, au4MplsLSRNotify,
            sizeof (au4MplsLSRNotify));
    pOidValue->u4_Length = sizeof (au4MplsLSRNotify) / sizeof (UINT4);

    pVbList = ((tSNMP_VAR_BIND *)
               SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OBJECT_ID,
                                     0, 0, NULL, pOidValue, SnmpCnt64Type));
    if (pVbList == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsLSRXcOperNotif :VarBind Alloc Failed \n");
        return MPLS_FAILURE;
    }

    pStartVb = pVbList;

    u4ArrLen = sizeof (au1MplsXCOperStatus) / sizeof (UINT4);
    /* Value of the Trap  -> MplsXCOperStatus.XCIndex.InsegeIndex.OutSegIndex */
    pOid = alloc_oid (u4ArrLen);
    if (pOid == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsLSRXcOperNotif :Malloc Failed \n");
        return MPLS_FAILURE;
    }
    au1MplsXCOperStatus[u4ArrLen - 6] = sizeof (UINT4) / 4;
    au1MplsXCOperStatus[u4ArrLen - 5] = XC_INDEX (pXCEntry);
    au1MplsXCOperStatus[u4ArrLen - 4] = sizeof (UINT4) / 4;
    if (XC_ININDEX (pXCEntry) == NULL)
    {
        au1MplsXCOperStatus[u4ArrLen - 3] = 0;
    }
    else
    {
        au1MplsXCOperStatus[u4ArrLen - 3] =
            INSEGMENT_INDEX (XC_ININDEX (pXCEntry));
    }
    au1MplsXCOperStatus[u4ArrLen - 2] = sizeof (UINT4) / 4;
    if (XC_OUTINDEX (pXCEntry) == NULL)
    {
        au1MplsXCOperStatus[u4ArrLen - 1] = 0;
    }
    else
    {
        au1MplsXCOperStatus[u4ArrLen - 1] =
            OUTSEGMENT_INDEX (XC_OUTINDEX (pXCEntry));
    }
    MEMCPY (pOid->pu4_OidList, au1MplsXCOperStatus,
            sizeof (au1MplsXCOperStatus));
    pOid->u4_Length = u4ArrLen;

    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_INTEGER,
                                                  0, XC_OPER_STATUS (pXCEntry),
                                                  NULL, NULL, SnmpCnt64Type));
    if (pVbList->pNextVarBind == NULL)
    {
        free_oid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsLSRXcOperNotif :Malloc Failed \n");
        return MPLS_FAILURE;
    }

    pVbList = pVbList->pNextVarBind;

    pOidLast = alloc_oid (u4ArrLen);
    if (pOidLast == NULL)
    {
        SNMP_free_snmp_vb_list (pStartVb);
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsLSRXcOperNotif :Malloc Failed \n");
        return MPLS_FAILURE;
    }
    MEMCPY (pOidLast->pu4_OidList, au1MplsXCOperStatus,
            sizeof (au1MplsXCOperStatus));
    pOidLast->u4_Length = u4ArrLen;

    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOidLast,
                                                  SNMP_DATA_TYPE_INTEGER,
                                                  0, XC_OPER_STATUS (pXCEntry),
                                                  NULL, NULL, SnmpCnt64Type));
    if (pVbList->pNextVarBind == NULL)
    {
        free_oid (pOidLast);
        SNMP_free_snmp_vb_list (pStartVb);
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsLSRXcOperNotif :Malloc Failed \n");
        return MPLS_FAILURE;
    }

    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;

    /* Sending Trap to SNMP Module */
    SNMP_AGT_RIF_Notify_v2Trap (pStartVb);

    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsLSRXcOperNotif : EXIT \n");
#else
    UNUSED_PARAM (pXCEntry);
    UNUSED_PARAM (u1State);
#endif
    return (MPLS_SUCCESS);
}

/************************************************************************
 *  Function Name   : MplsGetXCEntryByDirection
 *  Description     : Function to get XC  Entry for the given
 *                    XC Index and Direction. If Direction is any then
 *                    it will return first entry with that index.
 *  Input           : u4XcIndex    - XC Index
                      u4Direction  - Direction of the segment
 *  Output          : None
 *  Returns         : Pointer to XC Entry or NULL
 ************************************************************************/
tXcEntry           *
MplsGetXCEntryByDirection (UINT4 u4XcIndex, eDirection Direction)
{
    tXcEntry           *pXcEntry = NULL;
    UINT4               u4InIndex = 0;
    UINT4               u4OutIndex = 0;

    if (u4XcIndex == 0)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsGetXCEntryByDirection: XCIndex is invalid\n");
        return NULL;
    }
    while ((pXcEntry = MplsXCTableNextEntry (u4XcIndex, u4InIndex, u4OutIndex))
           != NULL)
    {
        if (pXcEntry->u4Index != u4XcIndex)
        {
            break;
        }

        /*Check the direction only if required */
        if (Direction == MPLS_DIRECTION_ANY)
        {
            return pXcEntry;
        }

        if (pXcEntry->pInIndex != NULL)
        {
            if (pXcEntry->pInIndex->GmplsInSegment.InSegmentDirection ==
                Direction)
            {
                return pXcEntry;
            }
            else
            {
                u4InIndex = pXcEntry->pInIndex->u4Index;
            }
        }

        if (pXcEntry->pOutIndex != NULL)
        {
            if (pXcEntry->pOutIndex->GmplsOutSegment.OutSegmentDirection ==
                Direction)
            {
                return pXcEntry;
            }
            else
            {
                u4OutIndex = pXcEntry->pOutIndex->u4Index;
            }
        }
    }

    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
               "MplsGetXCEntryByDirection: XC entry not found\n");

    return NULL;
}

/************************************************************************
 *  Function Name   : MplsGetXCEntryByLabelStack
 *  Description     : Function to get XC  Entry for the given
 *                    XC Index. If It has Label stack Entry that XC Entry
 *                    will be returned.
 *  Input           : u4XcIndex    - XC Index
 *  Output          : None
 *  Returns         : Pointer to XC Entry or NULL
 ************************************************************************/
tXcEntry           *
MplsGetXCEntryByLabelStack (UINT4 u4XcIndex)
{
    tXcEntry           *pXcEntry = NULL;
    UINT4               u4InIndex = 0;
    UINT4               u4OutIndex = 0;

    if (u4XcIndex == 0)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsGetXCEntryByLabelStack: XCIndex is invalid\n");
        return NULL;
    }
    while ((pXcEntry = MplsXCTableNextEntry (u4XcIndex, u4InIndex, u4OutIndex))
           != NULL)
    {
        if (pXcEntry->u4Index != u4XcIndex)
        {
            break;
        }

        if (pXcEntry->mplsLabelIndex != NULL)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsGetXCEntryByLabelStack: XC entry found\n");
            return pXcEntry;
        }

        if (pXcEntry->pInIndex != NULL)
        {
            u4InIndex = pXcEntry->pInIndex->u4Index;
        }

        if (pXcEntry->pOutIndex != NULL)
        {
            u4OutIndex = pXcEntry->pOutIndex->u4Index;
        }

    }

    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
               "MplsGetXCEntryByLabelStack: XC entry not found\n");

    return NULL;
}

/************************************************************************
 *  Function Name   : MplsGetFtnXcValues
 *  Description     : Function to get the XC related values (i.e pu4XCIndex,pu4InIndex
 *                    and pu4OutIndex)
 *  Input           : i4MplsActionType,pRetValMplsFTNActionPointer
 *  Output          : pu4XCIndex,pu4InIndex and pu4OutIndex
 *  Returns         : Success or Failure
 ************************************************************************/
INT1
MplsGetFtnXcValues (INT4 i4MplsActionType,
                    tSNMP_OID_TYPE * pMplsFTNActionPointer,
                    UINT4 *pu4XCIndex, UINT4 *pu4InIndex, UINT4 *pu4OutIndex)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tXcEntry           *pXcEntry = NULL;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;
    UINT4               u4TnlInstance = 0;
    UINT4               u4TnlIndex = 0;
    UINT4               au4TnlTableOid[MPLS_FTN_TE_TABLE_DEF_OFFSET];
    UINT4               au4XCTblOid[MPLS_TE_XC_TABLE_OFFSET];

    MPLS_CMN_LOCK ();

    MEMSET (au4TnlTableOid, 0, MPLS_FTN_TE_TABLE_DEF_OFFSET * (sizeof (UINT4)));
    MEMSET (au4XCTblOid, 0, MPLS_TE_XC_TABLE_OFFSET);
    if (i4MplsActionType == REDIRECTTUNNEL)
    {
        CPY_FROM_OID (au4TnlTableOid, pMplsFTNActionPointer,
                      pMplsFTNActionPointer->u4_Length * (sizeof (UINT4)));

        u4EgressId = au4TnlTableOid[MPLS_FTN_TE_TABLE_DEF_OFFSET - 1];
        u4IngressId = au4TnlTableOid[MPLS_FTN_TE_TABLE_DEF_OFFSET - 2];
        u4TnlInstance = au4TnlTableOid[MPLS_FTN_TE_TABLE_DEF_OFFSET - 3];
        u4TnlIndex = au4TnlTableOid[MPLS_FTN_TE_TABLE_DEF_OFFSET - 4];

        pTeTnlInfo = TeGetTunnelInfo (u4TnlIndex,
                                      u4TnlInstance, u4IngressId, u4EgressId);
        if (pTeTnlInfo == NULL)
        {
            MPLS_CMN_UNLOCK ();
            return MPLS_FAILURE;
        }
        if (u4TnlInstance == 0)
        {
            pTeTnlInfo =
                TeGetTunnelInfo (u4TnlIndex, pTeTnlInfo->u4TnlPrimaryInstance,
                                 u4IngressId, u4EgressId);
            if (pTeTnlInfo == NULL)
            {
                MPLS_CMN_UNLOCK ();
                return MPLS_FAILURE;
            }
        }

        pXcEntry = MplsGetXCEntryByDirection (pTeTnlInfo->u4TnlXcIndex,
                                              (eDirection) MPLS_DIRECTION_ANY);
        if (pXcEntry == NULL)
        {
            MPLS_CMN_UNLOCK ();
            return MPLS_FAILURE;
        }
        *pu4XCIndex = pXcEntry->u4Index;
        if (pXcEntry->pInIndex != NULL)
        {
            *pu4InIndex = pXcEntry->pInIndex->u4Index;
        }
        if (pXcEntry->pOutIndex != NULL)
        {
            *pu4OutIndex = pXcEntry->pOutIndex->u4Index;
        }
    }
    else if (i4MplsActionType == REDIRECTLSP)
    {
        MPLS_OID_TO_INTEGER (pMplsFTNActionPointer, *pu4XCIndex,
                             MPLS_XC_INDEX_START_OFFSET);
        MPLS_OID_TO_INTEGER (pMplsFTNActionPointer, *pu4InIndex,
                             MPLS_IN_INDEX_START_OFFSET);
        MPLS_OID_TO_INTEGER (pMplsFTNActionPointer, *pu4OutIndex,
                             MPLS_OUT_INDEX_START_OFFSET);
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsOutSegmentLabelCmpFunc 
 *  Description     : RBTree Compare function for Label + Next Hop Addr
 *                    based RBTree
 *  Input           : Two RBTree Nodes to be compared 
 *  Output          : None
 *  Returns         : 1/(-1)/0  
 ************************************************************************/
INT4
MplsOutSegmentLabelCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    tOutSegment        *pOutSegment1 = (tOutSegment *) pRBElem1;
    tOutSegment        *pOutSegment2 = (tOutSegment *) pRBElem2;

    if (pOutSegment1->u4Label < pOutSegment2->u4Label)
    {
        return -1;
    }
    else if (pOutSegment1->u4Label > pOutSegment2->u4Label)
    {
        return 1;
    }

    if (pOutSegment1->NHAddr.ip6_addr_u.u4WordAddr[0] <
        pOutSegment2->NHAddr.ip6_addr_u.u4WordAddr[0])
    {
        return -1;
    }
    else if (pOutSegment1->NHAddr.ip6_addr_u.u4WordAddr[0] >
             pOutSegment2->NHAddr.ip6_addr_u.u4WordAddr[0])
    {
        return 1;
    }

    return 0;
}

/************************************************************************
 *  Function Name   : MplsSetOutLabel
 *  Description     : Add/Delete the Node from Label + Next Hop Addr
 *                    based OutSegment RBTree based on the u4Label
 *  Input           : pOutSegment -> Pointer to tOutSegment
 *                    u4Label -> OutSegment Label
 *  Output          : None
 *  Returns         : MPLS_SUCCESS / MPLS_FAILURE
 ************************************************************************/
INT4
MplsSetOutLabel (tOutSegment * pOutSegment, UINT4 u4Label)
{
    /* This RBTree is implemented ONLY FOR RSVP-TE Graceful Restart
     * Purpose. Static configurations will not get reflected in this
     * RBTree
     */
    if ((u4Label == MPLS_IMPLICIT_NULL_LABEL) ||
        (u4Label == MPLS_IPV4_EXPLICIT_NULL_LABEL))
    {
        pOutSegment->u4Label = u4Label;
        return MPLS_SUCCESS;
    }

    if (pOutSegment->NHAddr.ip6_addr_u.u4WordAddr[0] == MPLS_ZERO)
    {
        pOutSegment->u4Label = u4Label;
        return MPLS_SUCCESS;
    }

    if (u4Label == MPLS_INVALID_LABEL)
    {
        RBTreeRem (gOutSegmentLabelBasedRBTree, pOutSegment);

        CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                    "Deleted Node with %d Label and %x NextHopAddr from the "
                    "Label + NHop based OutSegment RBTree \t\n",
                    pOutSegment->u4Label, pOutSegment->NHAddr.u4_addr);

        pOutSegment->u4Label = MPLS_INVALID_LABEL;
        return MPLS_SUCCESS;
    }

    if (pOutSegment->u4Label != MPLS_INVALID_LABEL)
    {
        RBTreeRem (gOutSegmentLabelBasedRBTree, pOutSegment);

        CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                    "Deleted Node with %d Label and %x NextHopAddr from the "
                    "Label + NHop based OutSegment RBTree \t\n",
                    pOutSegment->u4Label, pOutSegment->NHAddr.u4_addr);
    }

    pOutSegment->u4Label = u4Label;

    if (RBTreeAdd (gOutSegmentLabelBasedRBTree, pOutSegment) != RB_SUCCESS)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Adding Nodes to Label + NHop based OutSegment RBTree "
                   "Failed\n");
        return MPLS_FAILURE;
    }

    CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                "Added Node with %d Label and %x NextHopAddr to the "
                "Label + NHop based OutSegment RBTree \t\n",
                pOutSegment->u4Label, pOutSegment->NHAddr.u4_addr);

    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsSignalGetOutSegmentTableEntry
 *  Description     : Function to get OutSegment Entry from IfIndex and
 *                    In Label
 *  Input           : Interface Index and In Label
 *  Output          : None
 *  Returns         : Pointer to InSegment Entry or NULL
 ************************************************************************/
tOutSegment        *
MplsSignalGetOutSegmentTableEntry (UINT4 u4NextHop, UINT4 u4OutLabel)
{
    tOutSegment         OutSegment, *pOutSegment = NULL;

    OutSegment.NHAddr.ip6_addr_u.u4WordAddr[0] = u4NextHop;
    OutSegment.u4Label = u4OutLabel;

    pOutSegment = RBTreeGet (gOutSegmentLabelBasedRBTree, &OutSegment);
    if (pOutSegment != NULL)
    {
        return pOutSegment;
    }
    CMNDB_DBG
        (DEBUG_DEBUG_LEVEL,
         "Get In Segment Entry from Signal : NO Insegment entry exists \t\n");
    return NULL;
}

/*---------------------------------------------------------------------------*/
/*                       End of file mplslsr.c                               */
/*---------------------------------------------------------------------------*/
