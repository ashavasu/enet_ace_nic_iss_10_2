
/*****************************************************************************
 * Copyright (C) 2013 Aricent Inc . All Rights Reserved
 *
 * $Id: mplsnpapi.c,v 1.1 2013/03/28 12:10:53 siva Exp $
 *
 * Description: This file contains function for invoking NP calls of MPLS module.
 ******************************************************************************/

#ifndef __MPLS_NPAPI_C__
#define __MPLS_NPAPI_C__

#include "nputil.h"

/***************************************************************************
 *                                                                          
 *    Function Name       : MplsFsNpIpv4UcDelRoute                                         
 *                                                                          
 *    Description         : This function using its arguments populates the 
 *                          generic NP structure and invokes NpUtilHwProgram
 *                          The Generic NP wrapper after validating the H/W
 *                          parameters invokes FsNpIpv4UcDelRoute
 *                                                                          
 *    Input(s)            : Arguments of FsNpIpv4UcDelRoute
 *                                                                          
 *    Output(s)           : None                                            
 *                                                                          
 *    Global Var Referred : None                                      
 *                                                                          
 *    Global Var Modified : None.                                     
 *                                                                          
 *    Use of Recursion    : None.                                       
 *                                                                          
 *    Exceptions or Operating                                               
 *    System Error Handling    : None.                                      
 *                                                                          
 *    Returns            : FNP_SUCCESS OR FNP_FAILURE                       
 *
 *****************************************************************************/

UINT1
MplsFsNpIpv4UcDelRoute (UINT4 u4VrId, UINT4 u4IpDestAddr, UINT4 u4IpSubNetMask,
                      tFsNpNextHopInfo routeEntry, INT4 *pi4FreeDefIpB4Del)
{
    tFsHwNp             FsHwNp;
    tIpNpModInfo       *pIpNpModInfo = NULL;
    tIpNpWrFsNpIpv4UcDelRoute *pEntry = NULL;
    NP_UTIL_FILL_PARAMS (FsHwNp,    /*Generic NP structure */
                         NP_IP_MOD,    /* Module ID */
                         FS_NP_IPV4_UC_DEL_ROUTE,    /* Function/OpCode */
                         0,        /* IfIndex value if applicable */
                         0,        /* No. of Port Params */
                         0);    /* No. of PortList Parms */
    pIpNpModInfo = &(FsHwNp.IpNpModInfo);
    pEntry = &pIpNpModInfo->IpNpFsNpIpv4UcDelRoute;

    pEntry->u4VrId = u4VrId;
    pEntry->u4IpDestAddr = u4IpDestAddr;
    pEntry->u4IpSubNetMask = u4IpSubNetMask;
    pEntry->routeEntry = routeEntry;
    pEntry->pi4FreeDefIpB4Del = pi4FreeDefIpB4Del;

    if (FNP_FAILURE == NpUtilHwProgram (&FsHwNp))
    {
        return (FNP_FAILURE);
    }
    return (FNP_SUCCESS);
}

#endif /* __MPLS_NPAPI_C__ */
