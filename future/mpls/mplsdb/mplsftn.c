/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: mplsftn.c,v 1.27 2015/02/05 11:29:59 siva Exp $
 *
 * Description: Routines for Mpls FTN Module
 ********************************************************************/
#include "mplsdbinc.h"
#include "mplcmndb.h"
#include "trie.h"

static INT4
 
 
 
 
FtnTrieAddEntry (VOID *pInputParams, VOID *pOutputParams,
                 VOID **ppAppSpecInfo, VOID *pNewAppSpecInfo);

static INT4
 
 
 
 
FtnTrieDeleteEntry (VOID *pInputParams, VOID **ppAppSpecInfo,
                    VOID *pOutputParams, VOID *pNextHop, tKey Key);

static INT4
 
 
 
 
FtnTrieSearchEntry (tInputParams * pInputParams,
                    tOutputParams * pOutputParams, VOID *pAppSpecInfo);

static UINT1       *FtnTrieDelAllEntry (tInputParams * pInputParams,
                                        VOID *dummy,
                                        tOutputParams * pOutputParams);

static VOID         FtnTrieDeInit (VOID *pDelParams);
static INT4         FtnTrieDelEntry (VOID *pInRtInfo, VOID **ppAppPtr,
                                     tKey key);
static INT4         FtnTrieLookUpEntry (tInputParams * pInputParams,
                                        tOutputParams * pOutputParams,
                                        VOID *pAppSpecInfo, UINT2 u2KeySize,
                                        tKey key);

static INT4
 
 
 
 
FtnTrieBestMatch (UINT2 u2KeySize, tInputParams * pInputParams,
                  VOID *pOutputParams, VOID *pAppSpecInfo, tKey Key);

VOID                MplsFtnTrieCbDelete (VOID *pInput);

static struct TrieAppFns FtnTrieLibFuncs = {
    (INT4 (*)(tInputParams * pInputParams, VOID *pOutputParams,
              VOID **ppAppPtr, VOID *pAppSpecInfo)) FtnTrieAddEntry,

    (INT4 (*)(tInputParams * pInputParams, VOID **ppAppPtr,
              VOID *pOutputParams, VOID *pNxtHop, tKey Key)) FtnTrieDeleteEntry,

    (INT4 (*)(tInputParams * pInputParams, VOID *pOutputParams,
              VOID *pAppPtr)) FtnTrieSearchEntry,

    (INT4 (*)(tInputParams * pInputParams, VOID *pOutputParams,
              VOID *pAppSpecInfo, UINT2 u2KeySize,
              tKey key)) FtnTrieLookUpEntry,

    (VOID *(*)(tInputParams *, void (*)(VOID *), VOID *)) NULL,

    (VOID (*)(VOID *)) NULL,

    (INT4 (*)(VOID *, VOID **, tKey Key)) NULL,

    (INT4 (*)(VOID *, VOID *, tKey Key)) NULL,

    (INT4 (*)(tInputParams *, VOID *, VOID **, void *, UINT4)) NULL,

    (VOID *(*)(tInputParams * pInputParams, VOID (*)(VOID *),
               VOID *pOutputParams)) FtnTrieDelAllEntry,

    (VOID (*)(VOID *pDummy)) FtnTrieDeInit,

    (INT4 (*)(VOID *, VOID **ppAppPtr, tKey key)) FtnTrieDelEntry,

    (INT4 (*)(tInputParams *, VOID *)) NULL,

    (INT4 (*)(UINT2 u2KeySize, tInputParams * pInputParams,
              VOID *pOutputParams, VOID *pAppSpecInfo,
              tKey Key)) FtnTrieBestMatch,

    (INT4 (*)(tInputParams *, VOID *, VOID *, tKey)) NULL,

    (INT4 (*)(tInputParams *, VOID *, VOID *, UINT2, tKey)) NULL
};



#ifdef MPLS_IPV6_WANTED

static INT4
FtnIpv6TrieAddEntry (VOID *pInputParams, VOID *pOutputParams,
                 VOID **ppAppSpecInfo, VOID *pNewAppSpecInfo);

static INT4
FtnIpv6TrieDeleteEntry (VOID *pInputParams, VOID **ppAppSpecInfo,
                    VOID *pOutputParams, VOID *pNextHop, tKey Key);

static INT4
FtnIpv6TrieSearchEntry (tInputParams * pInputParams,
                    tOutputParams * pOutputParams, VOID *pAppSpecInfo);

static UINT1       *FtnIpv6TrieDelAllEntry (tInputParams * pInputParams,
                                        VOID *dummy,
                                        tOutputParams * pOutputParams);

static VOID         FtnIpv6TrieDeInit (VOID *pDelParams);
static INT4         FtnIpv6TrieDelEntry (VOID *pInRtInfo, VOID **ppAppPtr,
                                     tKey key);
static INT4         FtnIpv6TrieLookUpEntry (tInputParams * pInputParams,
                                        tOutputParams * pOutputParams,
                                        VOID *pAppSpecInfo, UINT2 u2KeySize,
                                        tKey key);

static INT4
FtnIpv6TrieBestMatch (UINT2 u2KeySize, tInputParams * pInputParams,
                  VOID *pOutputParams, VOID *pAppSpecInfo, tKey Key);

VOID                MplsFtnIpv6TrieCbDelete (VOID *pInput);

static struct TrieAppFns FtnIpv6TrieLibFuncs = {
    (INT4 (*)(tInputParams * pInputParams, VOID *pOutputParams,
              VOID **ppAppPtr, VOID *pAppSpecInfo)) FtnIpv6TrieAddEntry,

    (INT4 (*)(tInputParams * pInputParams, VOID **ppAppPtr,
              VOID *pOutputParams, VOID *pNxtHop, tKey Key)) FtnIpv6TrieDeleteEntry,

    (INT4 (*)(tInputParams * pInputParams, VOID *pOutputParams,
              VOID *pAppPtr)) FtnIpv6TrieSearchEntry,

    (INT4 (*)(tInputParams * pInputParams, VOID *pOutputParams,
              VOID *pAppSpecInfo, UINT2 u2KeySize,
              tKey key)) FtnIpv6TrieLookUpEntry,

    (VOID *(*)(tInputParams *, void (*)(VOID *), VOID *)) NULL,

    (VOID (*)(VOID *)) NULL,

    (INT4 (*)(VOID *, VOID **, tKey Key)) NULL,

    (INT4 (*)(VOID *, VOID *, tKey Key)) NULL,

    (INT4 (*)(tInputParams *, VOID *, VOID **, void *, UINT4)) NULL,

    (VOID *(*)(tInputParams * pInputParams, VOID (*)(VOID *),
               VOID *pOutputParams)) FtnIpv6TrieDelAllEntry,

    (VOID (*)(VOID *pDummy)) FtnIpv6TrieDeInit,

    (INT4 (*)(VOID *, VOID **ppAppPtr, tKey key)) FtnIpv6TrieDelEntry,

    (INT4 (*)(tInputParams *, VOID *)) NULL,

    (INT4 (*)(UINT2 u2KeySize, tInputParams * pInputParams,
              VOID *pOutputParams, VOID *pAppSpecInfo,
              tKey Key)) FtnIpv6TrieBestMatch,

    (INT4 (*)(tInputParams *, VOID *, VOID *, tKey)) NULL,

    (INT4 (*)(tInputParams *, VOID *, VOID *, UINT2, tKey)) NULL
};

#endif

/* Pool ID's */
#define FTN_TABLE_POOL_ID     MPLSDBMemPoolIds[MAX_MPLSDB_FTN_ENTRY_SIZING_ID]
#define FTN_MAP_TABLE_POOL_ID MPLSDBMemPoolIds[MAX_MPLSDB_FTN_MAP_ENTRY_SIZING_ID]

/* RedBlack Tree ID's */
static struct rbtree *gpFtnTableRbTree = NULL;
static struct rbtree *gpFtnMapTableRbTree = NULL;

/* Ipv4 Trie Id */
static VOID        *gpFtnTrieRoot = NULL;

#ifdef MPLS_IPV6_WANTED
/* Ipv6 Trie Id */
static VOID  	   *gpFtnIpv6TrieRoot = NULL;
#endif

/* Red Black Tree Compare Functions Prototypes */
INT4                MplsRbTreeFtnTableCmpFunc (tRBElem * pRBElem1,
                                               tRBElem * pRBElem2);
INT4                MplsRbTreeFtnMapTableCmpFunc (tRBElem * pRBElem1,
                                                  tRBElem * pRBElem2);

UINT4               gFtnSetTime = 0;
UINT4               gFtnMapSetTime = 0;

/************************************************************************
 *  Function Name   : MplsFtnUpdateSysTime 
 *  Description     : Function to Update FTN Last Write Access Time
 *  Input           : None
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
MplsFtnUpdateSysTime ()
{
    OsixGetSysTime ((tOsixSysTime *) & gFtnSetTime);
}

/************************************************************************
 *  Function Name   : MplsFtnMapUpdateSysTime 
 *  Description     : Function to update FTN Map  Last Write Access Time
 *  Input           : None
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
MplsFtnMapUpdateSysTime ()
{
    OsixGetSysTime ((tOsixSysTime *) & gFtnMapSetTime);
}

/************************************************************************
 *  Function Name   : MplsFtnGetSysTime 
 *  Description     : Function to return FTN Last Write Access Time
 *  Input           : None
 *  Output          : None
 *  Returns         : Last Write Access Time
 ************************************************************************/
UINT4
MplsFtnGetSysTime ()
{
    return (gFtnSetTime);
}

/************************************************************************
 *  Function Name   : MplsFtnMapGetysTime 
 *  Description     : Function to return FTN Map  Last Write Access Time
 *  Input           : None
 *  Output          : None
 *  Returns         : Last write Access Time
 ************************************************************************/
UINT4
MplsFtnMapGetSysTime ()
{
    return (gFtnMapSetTime);
}

/************************************************************************
 *  Function Name   :  MplsFTNInit
 *  Description     : Function to Init FTN Module 
 *  Input           : None
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE
 ************************************************************************/

INT4
MplsFTNInit (VOID)
{
    /* Red Black Tree Create */

    gpFtnTableRbTree =
        RBTreeCreate (gu4MplsDbMaxFtnEntries, MplsRbTreeFtnTableCmpFunc);
    if (gpFtnTableRbTree == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "FTN Table RB Tree Create Failed \t\n");
        MplsFTNDeInit ();
        return MPLS_FAILURE;
    }
    gpFtnMapTableRbTree = RBTreeCreate (gu4MplsDbMaxFtnEntries,
                                        MplsRbTreeFtnMapTableCmpFunc);
    if (gpFtnMapTableRbTree == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "FTN Table RB Tree Create Failed \t\n");
        MplsFTNDeInit ();
        return MPLS_FAILURE;

    }
    if(MplsFtnInitTrie()==MPLS_FAILURE)
    {
	    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "FTN Table Trie Create Failed \t\n");
	    MplsFTNDeInit ();
	    return MPLS_FAILURE;

    }
#ifdef MPLS_IPV6_WANTED
    if(MplsFtnIpv6InitTrie()==MPLS_FAILURE)
    {
	    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "FTN Ipv6 Table Trie Create Failed \t\n");
	    MplsFTNDeInit ();
	    return MPLS_FAILURE;

    }
#endif
    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "FTN Init Success \t\n");
    return MPLS_SUCCESS;
}

UINT4
MplsFtnInitTrie (VOID)
{
	tTrieCrtParams      ftnTrieCrtParams;

	/*  Trie Create */
	ftnTrieCrtParams.u2KeySize = 2 * sizeof (UINT4);    /* key is Min Address */
	ftnTrieCrtParams.u4Type = MPLS_FTN_TYPE;
	ftnTrieCrtParams.AppFns = &(FtnTrieLibFuncs);
	ftnTrieCrtParams.u4NumRadixNodes = gu4MplsDbMaxFtnEntries + 1;
	ftnTrieCrtParams.u4NumLeafNodes = gu4MplsDbMaxFtnEntries;
	ftnTrieCrtParams.u4NoofRoutes = gu4MplsDbMaxFtnEntries;
	ftnTrieCrtParams.u1AppId = MPLS_APPL_ID;
	gpFtnTrieRoot = TrieCrt (&ftnTrieCrtParams);
	if (gpFtnTrieRoot == NULL)
	{
		return MPLS_FAILURE;
	}
	return MPLS_SUCCESS;
}

#ifdef MPLS_IPV6_WANTED
UINT4
MplsFtnIpv6InitTrie (VOID)
{
	tTrieCrtParams      ftnTrieCrtParams;

	/*  Trie Create */
	ftnTrieCrtParams.u2KeySize =  2 * IPV6_ADDR_LENGTH;/* key is Min Address */
	ftnTrieCrtParams.u4Type = MPLS_FTN_IPV6_TYPE;
	ftnTrieCrtParams.AppFns = &(FtnIpv6TrieLibFuncs);
	ftnTrieCrtParams.u4NumRadixNodes = gu4MplsDbMaxFtnEntries + 1;
	ftnTrieCrtParams.u4NumLeafNodes = gu4MplsDbMaxFtnEntries;
	ftnTrieCrtParams.u4NoofRoutes = gu4MplsDbMaxFtnEntries;
	ftnTrieCrtParams.u1AppId = MPLS_APPL_ID;
	gpFtnIpv6TrieRoot = TrieCrt (&ftnTrieCrtParams);
	if (gpFtnIpv6TrieRoot == NULL)
	{
		return MPLS_FAILURE;
	}

	return MPLS_SUCCESS;
}
#endif


/************************************************************************
 *  Function Name   : MplsFTNDeInit
 *  Description     : Function to DeInit FTN Module 
 *  Input           : None
 *  Output          : None
 *  Returns         : MPLS_SUCCESS/MPLS_FAILURE 
 ************************************************************************/

INT4
MplsFTNDeInit (VOID)
{
    tInputParams        FtnInputParams;
    tOutputParams       FtnOutputParams;
    INT4                i4Status = TRIE_FAILURE;

    FtnInputParams.pLeafNode = NULL;
    FtnInputParams.pRoot = gpFtnTrieRoot;
    FtnInputParams.i1AppId = MPLS_APPL_ID;
    FtnInputParams.pLeafNode = NULL;
    FtnInputParams.Key.pKey = NULL;

    FtnOutputParams.pAppSpecInfo = NULL;
    FtnOutputParams.Key.pKey = NULL;

    /*  Trie Delete */
    i4Status = TrieDel (&FtnInputParams, MplsFtnTrieCbDelete,
                        (VOID *) &FtnOutputParams);
#ifdef MPLS_IPV6_WANTED
    /* IPV6 Trie Delete */
    FtnInputParams.pLeafNode = NULL;
    FtnInputParams.pRoot = gpFtnIpv6TrieRoot;
    FtnInputParams.i1AppId = MPLS_APPL_ID;
    FtnInputParams.pLeafNode = NULL;
    FtnInputParams.Key.pKey = NULL;

    FtnOutputParams.pAppSpecInfo = NULL;
    FtnOutputParams.Key.pKey = NULL;

    i4Status = TrieDel (&FtnInputParams,MplsFtnIpv6TrieCbDelete,
		    (VOID *) &FtnOutputParams);
#endif

    /* RedBlack Tree DeInit */
    if (gpFtnTableRbTree != NULL)
    {
        RBTreeDelete (gpFtnTableRbTree);
        gpFtnTableRbTree = NULL;
    }

    if (gpFtnMapTableRbTree != NULL)
    {
        RBTreeDelete (gpFtnMapTableRbTree);
        gpFtnMapTableRbTree = NULL;
    }

    return ((i4Status >= TRIE_SUCCESS) ? (MPLS_SUCCESS) : (MPLS_FAILURE));
}

/*****************************************************************************/
/* Function Name : MplsFtnTrieCbDelete                                       */
/* Description   : This is a call back function for TRIE Delete              */
/* Input(s)      : pInput - Input Parameter.                                 */
/* Output(s)     : None.                                                     */
/* Return(s)     : None.                                                     */
/*****************************************************************************/
VOID
MplsFtnTrieCbDelete (VOID *pInput)
{
    UNUSED_PARAM (pInput);
    return;
}

/************************************************************************
 *  Function Name   : MplsRbTreeFtnTableCmpFunc 
 *  Description     : RBTree Compare function for Mpls Ftn Table
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0 
 ************************************************************************/
INT4
MplsRbTreeFtnTableCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    UINT4               u4Index1 = ((tFtnEntry *) pRBElem1)->u4FtnIndex;
    UINT4               u4Index2 = ((tFtnEntry *) pRBElem2)->u4FtnIndex;

    if (u4Index1 > u4Index2)
    {
        return 1;
    }
    else if (u4Index1 < u4Index2)
    {
        return -1;
    }
    return 0;
}

/************************************************************************
 *  Function Name   : MplsRbTreeFtnMapTableCmpFunc
 *  Description     : RBTree Compare function for Mpls Ftn Map Table
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0
 ************************************************************************/
INT4
MplsRbTreeFtnMapTableCmpFunc (tRBElem * pRBElem1, tRBElem * pRBElem2)
{
    UINT4               au4Index1[2], au4Index2[2];
    au4Index1[0] = ((tFtnMapEntry *) pRBElem1)->u4IfIndex;
    au4Index1[1] = ((tFtnMapEntry *) pRBElem1)->u4CurIndex;
    au4Index2[0] = ((tFtnMapEntry *) pRBElem2)->u4IfIndex;
    au4Index2[1] = ((tFtnMapEntry *) pRBElem2)->u4CurIndex;

    if (au4Index1[0] > au4Index2[0])
    {
        return 1;
    }
    else if (au4Index1[0] < au4Index2[0])
    {
        return -1;
    }
    if (au4Index1[1] > au4Index2[1])
    {
        return 1;
    }
    else if (au4Index1[1] < au4Index2[1])
    {
        return -1;
    }
    return 0;
}

/************************************************************************
 *  Function Name   : MplsCreateFtnTableEntry   
 *  Description     : Function to create an entry in Mpls FTN Table 
 *  Input           : Mpls FtnIndex 
 *  Output          : None
 *  Returns         : Pointer of the Entry or NULL 
 ************************************************************************/
tFtnEntry          *
MplsCreateFtnTableEntry (UINT4 u4FtnIndex)
{
    tFtnEntry          *pFtnEntry = NULL;
    if (MplsGetFtnTableEntry (u4FtnIndex) != NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Create FTN Table : Entry Present \t\n");
        return NULL;
    }

    pFtnEntry = (tFtnEntry *) MemAllocMemBlk (FTN_TABLE_POOL_ID);

    if (pFtnEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Create FTN Table : Pool is Full \t\n");
        return NULL;
    }
    MEMSET (pFtnEntry, 0, sizeof (tFtnEntry));
    pFtnEntry->u4FtnIndex = u4FtnIndex;
    pFtnEntry->u1HwStatus = MPLS_FALSE;
    pFtnEntry->bIsHostAddrFecForPSN = FALSE;
    if (RBTreeAdd (gpFtnTableRbTree, (tRBElem *) pFtnEntry) == RB_FAILURE)
    {
        MemReleaseMemBlock (FTN_TABLE_POOL_ID, (UINT1 *) pFtnEntry);
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Create FTN Table : RBTree Add Failed \t\n");
        return NULL;
    }
    return pFtnEntry;
}

/************************************************************************
 *  Function Name   : MplsSignalGetFtnTableEntry
 *  Description     : Function to get entry from the Trie
 *  Input           : Address 
 *  Output          : None
 *  Returns         : Pointer of the Entry or NULL
 ************************************************************************/
tFtnEntry          *
MplsSignalGetFtnTableEntry (UINT4 u4Address)
{

    tInputParams        FtnInputParams;
    tOutputParams       FtnOutputParams;
    tFtnEntry          *pFtnEntry = NULL;
    UINT4               au4InTrieKey[2];
    VOID               *pTemp = NULL;
    au4InTrieKey[0] = OSIX_HTONL (u4Address);
    au4InTrieKey[1] = OSIX_HTONL (0xFFFFFFFF);
    FtnInputParams.pRoot = gpFtnTrieRoot;
    FtnInputParams.pLeafNode = NULL;
    FtnInputParams.i1AppId = MPLS_APPL_ID;
    FtnInputParams.u1PrefixLen = 0;
    FtnInputParams.Key.pKey = (UINT1 *) au4InTrieKey;

    FtnOutputParams.Key.pKey = NULL;
    FtnOutputParams.pAppSpecInfo = NULL;

    if ((TrieSearch (&FtnInputParams, &FtnOutputParams,
                     (VOID **) &pTemp)) == TRIE_FAILURE)
    {
        return NULL;
    }
    pFtnEntry = (tFtnEntry *) FtnOutputParams.pAppSpecInfo;
    return pFtnEntry;

}

/************************************************************************
 *  Function Name   : MplsFwdGetFtnTableEntry 
 *  Description     : Function to get exact or best entry from the Trie
 *  Input           : Address 
 *  Output          : None
 *  Returns         : Pointer of the Entry or NULL
 ************************************************************************/
tFtnEntry          *
MplsFwdGetFtnTableEntry (UINT4 u4Address)
{

    tInputParams        FtnInputParams;
    tOutputParams       FtnOutputParams;
    tFtnEntry          *pFtnEntry = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               au4InTrieKey[2];
    VOID               *pTemp = NULL;

    FtnInputParams.pRoot = gpFtnTrieRoot;
    FtnInputParams.pLeafNode = NULL;
    FtnInputParams.i1AppId = MPLS_APPL_ID;
    FtnInputParams.u1PrefixLen = 0;
    au4InTrieKey[0] = OSIX_HTONL (u4Address);
    au4InTrieKey[1] = OSIX_HTONL (0xFFFFFFFF);
    FtnInputParams.Key.pKey = (UINT1 *) au4InTrieKey;

    FtnOutputParams.Key.pKey = NULL;
    FtnOutputParams.pAppSpecInfo = NULL;

    if ((TrieLookup (&FtnInputParams, &FtnOutputParams,
                     (VOID **) &pTemp)) == TRIE_FAILURE)
    {
        return NULL;
    }
    pFtnEntry = (tFtnEntry *) FtnOutputParams.pAppSpecInfo;

    if ((pFtnEntry != NULL) && (pFtnEntry->i4ActionType == MPLS_FTN_ON_TE_LSP))
    {
        pTeTnlInfo = pFtnEntry->pActionPtr;

        /* MPLS Switching should not happen on admin down tunnels.
         * This function will be removed once software forwarding portion
         * is removed. */
        if ((pTeTnlInfo == NULL) ||
            (pTeTnlInfo->u1TnlOperStatus == TE_OPER_DOWN))
        {
            return NULL;
        }
    }
    return pFtnEntry;
}

/************************************************************************
 *  Function Name   : MplsGetFtnTableEntry
 *  Description     : Function to get Mpls Ftn Entry from Table
 *  Input           : Mpls Ftn Index
 *  Output          : None
 *  Returns         : Pointer of the Entry or NULL 
 ************************************************************************/

tFtnEntry          *
MplsGetFtnTableEntry (UINT4 u4FtnIndex)
{
    tFtnEntry           MplsFtnEntry, *pFtnEntry = NULL;

    if (gpFtnTableRbTree == NULL)
    {
        return NULL;
    }
    MplsFtnEntry.u4FtnIndex = u4FtnIndex;
    pFtnEntry = RBTreeGet (gpFtnTableRbTree, &MplsFtnEntry);
    if (pFtnEntry == NULL)
    {
        return NULL;
    }
    return pFtnEntry;
}

/************************************************************************
 *  Function Name   : MplsDeleteFtnTableEntry 
 *  Description     : Function to delete a Ftn entry from Ftn table
 *  Input           : Mpls Ftn Entry Pointer 
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE
 ************************************************************************/

INT4
MplsDeleteFtnTableEntry (tFtnEntry * pFtnEntry)
{
    tInputParams        FtnInputParams;
    tOutputParams       FtnOutputParams;
    UINT4               au4InTrieKey[2];
    VOID               *pTemp = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;

    au4InTrieKey[0] = OSIX_HTONL (pFtnEntry->DestAddrMin.u4_addr[0]);
    au4InTrieKey[1] = OSIX_HTONL (0xFFFFFFFF);
    FtnInputParams.pLeafNode = NULL;
    FtnInputParams.u1PrefixLen = 0;
    FtnInputParams.pRoot = gpFtnTrieRoot;
    FtnInputParams.i1AppId = MPLS_APPL_ID;
    FtnInputParams.Key.pKey = (UINT1 *) au4InTrieKey;

    FtnOutputParams.Key.pKey = NULL;
    FtnOutputParams.pAppSpecInfo = NULL;

    RBTreeRem (gpFtnTableRbTree, pFtnEntry);

    CMNDB_DBG3 (DEBUG_DEBUG_LEVEL,"%s : %d Ipv4 Trie Deletion FEC = %x\n",
            __FUNCTION__,__LINE__,OSIX_HTONL(pFtnEntry->DestAddrMin.u4_addr[0]));

    if (TrieRemove (&FtnInputParams, &FtnOutputParams, pTemp) == TRIE_FAILURE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Delete FTN Table: Trie Delete Failed\t\n");
    }
    if (pFtnEntry->i4ActionType == REDIRECTTUNNEL)
    {
        pTeTnlInfo = pFtnEntry->pActionPtr;
        if (pTeTnlInfo != NULL)
        {
            TMO_DLL_Delete (&(pTeTnlInfo->FtnList), &(pFtnEntry->FtnInfoNode));
            CMNDB_DBG
                (DEBUG_DEBUG_LEVEL,
                 "Delete FTN Table: FTN is deleted from Tunnel entry\t\n");
        }
    }
    MplsFtnRelIndex (pFtnEntry->u4FtnIndex);
    MemReleaseMemBlock (FTN_TABLE_POOL_ID, (UINT1 *) pFtnEntry);
    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsFtnTableNextEntry 
 *  Description     : Function to get the Next Mpls Ftn Entry from the 
 *                    Ftn Table. If u4FtnIndex ==0 then get the first 
 *                    entry                 
 *  Input           : Mpls Ftn Index for which next entry is required
 *  Output          : None
 *  Returns         : Next Mpls Ftn Entry Pointer or NULL
 ************************************************************************/

tFtnEntry          *
MplsFtnTableNextEntry (UINT4 u4FtnIndex)
{
    tFtnEntry           MplsFtnEntry;
    if (gpFtnTableRbTree == NULL)
    {
        return NULL;
    }
    if (u4FtnIndex == 0)
    {
        return (RBTreeGetFirst (gpFtnTableRbTree));
    }
    MplsFtnEntry.u4FtnIndex = u4FtnIndex;
    return (RBTreeGetNext (gpFtnTableRbTree, &MplsFtnEntry, NULL));
}

/************************************************************************
 *  Function Name   : MplsRegisterEntrywithTrie
 *  Description     : Function to creates entry in Trie
 *  Input           : Mpls Ftn Entry Pointer
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE
 ************************************************************************/
INT4
MplsRegisterEntrywithTrie (tFtnEntry * pFtnEntry)
{

    tInputParams        FtnInputParams;
    tOutputParams       FtnOutputParams;
    UINT4               au4Indx[2];
    au4Indx[0] = OSIX_HTONL (pFtnEntry->DestAddrMin.u4_addr[0]);
    au4Indx[1] = OSIX_HTONL (0xFFFFFFFF);
    FtnInputParams.pLeafNode = NULL;
    FtnInputParams.u1PrefixLen = (UINT1)
        MplsFtnMaskToPrefixLen (pFtnEntry->DestAddrMax.u4_addr[0]);
    FtnInputParams.pRoot = gpFtnTrieRoot;
    FtnInputParams.i1AppId = MPLS_APPL_ID;
    FtnInputParams.Key.pKey = (UINT1 *) au4Indx;

    FtnOutputParams.pAppSpecInfo = NULL;
    FtnOutputParams.Key.pKey = NULL;
   
    CMNDB_DBG3 (DEBUG_DEBUG_LEVEL,"%s : %d Ipv4 Trie Addition FEC = %x\n",
            __FUNCTION__,__LINE__,OSIX_HTONL(pFtnEntry->DestAddrMin.u4_addr[0]));

    if ((TrieAdd (&FtnInputParams, pFtnEntry,
                  &FtnOutputParams)) == TRIE_FAILURE)

    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Create FTN Table : Trie Add Failed \t\n");
        return MPLS_FAILURE;
    }
    return MPLS_SUCCESS;
}

/* End FTN Table */

/*FTN MAP Table*/

/************************************************************************
 *  Function Name   : MplsCreateFtnMapTableEntry 
 *  Description     : Function to create an entry in Mpls Ftn Map Table
 *  Input           : Mpls If Index,Mpls Ftn Index
 *  Output          : None
 *  Returns         : Pointer of the Entry or NULL
 ************************************************************************/

tFtnMapEntry       *
MplsCreateFtnMapTableEntry (UINT4 u4IfIndex, UINT4 u4FtnIndex)
{

    tFtnMapEntry       *pFtnMapEntry = NULL;
    if (MplsGetFtnMapTableEntry (u4IfIndex, u4FtnIndex) != NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Create FTN Map Table : Entry Present \t\n");
        return NULL;
    }

    pFtnMapEntry = (tFtnMapEntry *) MemAllocMemBlk (FTN_MAP_TABLE_POOL_ID);

    if (pFtnMapEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Create FTN Map Table : Pool is Full \t\n");
        return NULL;
    }
    pFtnMapEntry->u4IfIndex = u4IfIndex;
    pFtnMapEntry->u4CurIndex = u4FtnIndex;

    pFtnMapEntry->u4PrevIndex = MplsFtnLastIndex (u4IfIndex);
    if (RBTreeAdd (gpFtnMapTableRbTree, (tRBElem *) pFtnMapEntry) == RB_FAILURE)
    {
        MemReleaseMemBlock (FTN_MAP_TABLE_POOL_ID, (UINT1 *) pFtnMapEntry);
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Create FTN Map Table : RBTree Add Failed \t\n");
        return NULL;
    }
    return pFtnMapEntry;
}

/************************************************************************
 *  Function Name   : MplsGetFtnMapTableEntry
 *  Description     : Function to get Mpls Ftn Map Entry from Table
 *  Input           : Mpls FtnIndex,Mpls IfIndex
 *  Output          : None
 *  Returns         : Pointer of the Entry or NULL
 ************************************************************************/

tFtnMapEntry       *
MplsGetFtnMapTableEntry (UINT4 u4IfIndex, UINT4 u4FtnIndex)
{
    tFtnMapEntry        MplsFtnMapEntry, *pFtnMapEntry = NULL;

    if (gpFtnMapTableRbTree == NULL)
    {
        return NULL;
    }
    MplsFtnMapEntry.u4IfIndex = u4IfIndex;
    MplsFtnMapEntry.u4CurIndex = u4FtnIndex;
    pFtnMapEntry = RBTreeGet (gpFtnMapTableRbTree, &MplsFtnMapEntry);
    if (pFtnMapEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Get Mpls FTN MAP Entry : RB Tree Get Failed \t\n");
    }
    return pFtnMapEntry;
}

/************************************************************************
 *  Function Name   : MplsDeleteFtnMapTableEntry
 *  Description     : Function to delete a Ftn Map entry from Ftn Map table
 *  Input           : Mpls Ftn Map Entry Pointer
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE
 ************************************************************************/
INT4
MplsDeleteFtnMapTableEntry (tFtnMapEntry * pFtnMapEntry)
{

    tFtnMapEntry       *pNextFtnMapEntry;
    if (gpFtnMapTableRbTree == NULL)
    {
        return MPLS_FAILURE;
    }
    pNextFtnMapEntry = RBTreeGetNext (gpFtnMapTableRbTree, pFtnMapEntry, NULL);
    RBTreeRem (gpFtnMapTableRbTree, pFtnMapEntry);
    if (pNextFtnMapEntry != NULL)
        pNextFtnMapEntry->u4PrevIndex = pFtnMapEntry->u4PrevIndex;
    MemReleaseMemBlock (FTN_MAP_TABLE_POOL_ID, (UINT1 *) pFtnMapEntry);
    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsFtnMapTableNextEntry
 *  Description     : Function to get the Next Mpls Ftn Map Entry from the
 *                    Ftn Map Table. If u4FtnIndex ==0 & u4IfIndex == 0
 *                    then get the first entry
 *  Input           : Mpls Ftn Index,Mpls IfIndex  for which next entry is
 *                    required
 *  Output          : None
 *  Returns         : Next Mpls Ftn Entry Pointer or NULL
 ************************************************************************/

tFtnMapEntry       *
MplsFtnMapTableNextEntry (UINT4 u4IfIndex, UINT4 u4FtnIndex)
{
    tFtnMapEntry        MplsFtnMapEntry;

    if (gpFtnMapTableRbTree == NULL)
    {
        return NULL;
    }
    if ((u4IfIndex == 0) && (u4FtnIndex == 0))
    {
        return (RBTreeGetFirst (gpFtnMapTableRbTree));
    }
    MplsFtnMapEntry.u4IfIndex = u4IfIndex;
    MplsFtnMapEntry.u4CurIndex = u4FtnIndex;
    return (RBTreeGetNext (gpFtnMapTableRbTree, &MplsFtnMapEntry, NULL));
}

/************************************************************************
 *  Function Name   : MplsFtnLastIndex 
 *  Description     : Function to get last index from the FtnMap Table 
 *  Input           : Mpls IfIndex
 *  Output          : None
 *  Returns         : Last Index
 ************************************************************************/

UINT4
MplsFtnLastIndex (UINT4 u4IfIndex)
{
    tFtnMapEntry       *pFtnMapEntry = NULL;
    tFtnMapEntry       *pNextFtnMapEntry = NULL;
    UINT4               u4Max = 0;
    if (gpFtnMapTableRbTree == NULL)
    {
        return 0;
    }
    if ((pFtnMapEntry = (RBTreeGetFirst (gpFtnMapTableRbTree))) == NULL)
    {
        return 0;
    }
    do
    {
        if (u4IfIndex == pFtnMapEntry->u4IfIndex)
        {
            if (pFtnMapEntry->u4CurIndex > u4Max)
                u4Max = pFtnMapEntry->u4CurIndex;
        }
        pNextFtnMapEntry =
            RBTreeGetNext (gpFtnMapTableRbTree, pFtnMapEntry, NULL);
        pFtnMapEntry = pNextFtnMapEntry;
    }
    while (pFtnMapEntry != NULL);
    return u4Max;
}

/* TRIE callback functions */

/*****************************************************************************/
/*                                                                           */
/* Function     : FtnTrieAddEntry                                         */
/*                                                                           */
/* Description  : This is the callback function to add the route entry to    */
/*                Ftn table                                         */
/*                                                                           */
/* Input        : pInputParams    - Pointer used by trie                     */
/*                pOutputParams   - Pointer not used by FTN                 */
/*                ppAppSpecInfo   - Pointer to the address of the            */
/*                                  FTN specific information present in     */
/*                                  trie leaf node                           */
/*                pNewAppSpecInfo - Pointer to the new application           */
/*                                  FTN specific information that has to    */
/*                                  added                                    */
/*                                                                           */
/* Output       : ppAppSpecInfo   - Pointer to the address of the updated    */
/*                                   FTN specific information                */
/*                                                                           */
/* Returns      : SUCCESS                                                    */
/*                                                                           */
/*****************************************************************************/
INT4
FtnTrieAddEntry (VOID *pInputParams, VOID *pOutputParams,
                 VOID **ppAppSpecInfo, VOID *pNewAppSpecInfo)
{

    UNUSED_PARAM (pInputParams);
    UNUSED_PARAM (pOutputParams);
    (*ppAppSpecInfo) = pNewAppSpecInfo;
    return SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FtnTrieDeleteRtEntry                                      */
/*                                                                           */
/* Description  : This is the callback function to delete the ftn entry    */
/*                from FTN routing                                     */
/*                                                                           */
/* Input        : pInputParams    - Pointer used by trie                     */
/*                pOutputParams   - Pointer not used by FTN                 */
/*                ppAppSpecInfo   - Pointer to the address of the            */
/*                                  FTN specific information present in     */
/*                                  trie leaf node                           */
/*                pNewAppSpecInfo - Pointer to the new application           */
/*                                  FTN specific information that has to    */
/*                                  added                                    */
/*                                                                           */
/* Output       : ppAppSpecInfo   - Pointer to the address of the updated    */
/*                                  FTN specific information                */
/*                                                                           */
/* Returns      : SUCCESS                                                    */
/*                                                                           */
/*****************************************************************************/
INT4
FtnTrieDeleteEntry (VOID *pInputParams, VOID **ppAppSpecInfo,
                    VOID *pOutputParams, VOID *pNextHop, tKey Key)
{

    UNUSED_PARAM (pInputParams);
    UNUSED_PARAM (pOutputParams);
    UNUSED_PARAM (pNextHop);
    UNUSED_PARAM (Key);
    (*ppAppSpecInfo) = NULL;
    return SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FtnTrieDelAllEntry                                         */
/*                                                                           */
/* Description  : This is the callback function to delete the all the route  */
/*                entries from FTN table. This is used when the              */
/*                entire routing table needs to be cleared.                  */
/*                                                                           */
/* Input        : pInputParams    - Pointer used pointing to input parameters*/
/*                pOutputParams   - Pointer not used by FTN                  */
/*                VOID (*)(VOID *)- Dummy pointer to the function for future */
/*                                  use.                                     */
/*                                                                           */
/* Output       : NONE                                                       */
/*                                                                           */
/* Returns      : Pointer to the value which helps in deciding whether to    */
/*                delete the route entry at IP level                         */
/*                                                                           */
/*****************************************************************************/

UINT1              *
FtnTrieDelAllEntry (tInputParams * pInputParams, VOID *dummy,
                    tOutputParams * pOutputParams)
{
    UINT1              *pInpFtnInfo;
    UNUSED_PARAM (pOutputParams);
    UNUSED_PARAM (dummy);
    pInpFtnInfo = (UINT1 *) pInputParams;
    return (pInpFtnInfo);
}

/*****************************************************************************/
/* Function Name : FtnTrieDeInit                                             */
/* Description   : This function callback function for de-initializing Trie  */
/*                 Delete.                                                   */
/* Input(s)      : pDelParams - Input Params                                 */
/* Output(s)     : None.                                                     */
/* Return(s)     : None.                                                     */
/*****************************************************************************/
VOID
FtnTrieDeInit (VOID *pDelParams)
{
    UNUSED_PARAM (pDelParams);
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FtnTrieSearchEntry                                      */
/*                                                                           */
/* Description  : This is the callback function to search the route entry    */
/*                from FTN table                                    */
/*                                                                           */
/* Input        : pInputParams    - Pointer used by trie                     */
/*                pOutputParams   - Pointer used by FTN to store the FTN   */
/*                                   entry                              */
/*                pAppSpecInfo    - Pointer to the FTN specific information */
/*                                  present in trie leaf node                */
/*                                                                           */
/* Output       : pOutputParams   - Pointer to the output parameters         */
/*                                  which has the route entry information    */
/*                                                                           */
/* Returns      : SUCCESS                                                    */
/*                                                                           */
/*****************************************************************************/

INT4
FtnTrieSearchEntry (tInputParams * pInputParams,
                    tOutputParams * pOutputParams, VOID *pAppSpecInfo)
{

    UNUSED_PARAM (pInputParams);
    pOutputParams->pAppSpecInfo = pAppSpecInfo;
    return SUCCESS;
}

/*****************************************************************************
 * Function     : FtnTrieLookUpEntry                                         
 * Description  : This is the callback function to lookup the route entry    
 *                from FTN table                                             
 * Input        : pInputParams    - Pointer used by trie                     
 *                pOutputParams   - Pointer used by FTN to store the FTN entry
 *                pAppSpecInfo    - Pointer to the FTN specific information  
 *                                  present in trie leaf node                
 *                u2KeySize       - Key Size
 *                key             - Key
 * Output       : pOutputParams   - Pointer to the output parameters         
 *                                  which has the route entry information    
 * Returns      : SUCCESS                                                    
*****************************************************************************/
INT4
FtnTrieLookUpEntry (tInputParams * pInputParams,
                    tOutputParams * pOutputParams,
                    VOID *pAppSpecInfo, UINT2 u2KeySize, tKey key)
{
    UNUSED_PARAM (pInputParams);
    UNUSED_PARAM (u2KeySize);
    UNUSED_PARAM (key);

    pOutputParams->pAppSpecInfo = pAppSpecInfo;
    return SUCCESS;
}

/*****************************************************************************
 * Function     : FtnTrieBestMatch 
 * Description  : This is the callback function to get the best route   
 *                from FTN table                                             
 * Input        : pInputParams    - Pointer used by trie                     
 *                pOutputParams   - Pointer used by FTN to store the FTN entry
 *                pAppSpecInfo    - Pointer to the FTN specific information  
 *                                  present in trie leaf node                
 *                u2KeySize       - Key Size
 *                key             - Key
 * Output       : pOutputParams   - Pointer to the output parameters         
 *                                  which has the route entry information    
 * Returns      : SUCCESS                                                    
*****************************************************************************/
INT4
FtnTrieBestMatch (UINT2 u2KeySize, tInputParams * pInputParams,
                  VOID *pOutputParams, VOID *pAppSpecInfo, tKey Key)
{
    UNUSED_PARAM (pInputParams);
    UNUSED_PARAM (u2KeySize);
    UNUSED_PARAM (Key);

    ((tOutputParams *) pOutputParams)->pAppSpecInfo = pAppSpecInfo;
    return SUCCESS;
}

/*****************************************************************************/
/* Function     : FtnTrieDelEntry                                            */
/*                                                                           */
/* Description  : This is the callback function to delete a particular       */
/*                FTN entry.                                                 */
/* Input        : pDelIpRt        - Pointer to the value which decides       */
/*                                  whether FTN entry need to be deleted     */
/*                ppAppPtr        - Pointer to the FTN Entry to be deleted   */
/*                                  given by trie.                           */
/*                key             - Key given to trie                        */
/*                                                                           */
/* Output       : ppAppPtr with NULL value                                   */
/*                                                                           */
/* Returns      : SUCCESS                                                    */
/*****************************************************************************/
INT4
FtnTrieDelEntry (VOID *pInRtInfo, VOID **ppAppPtr, tKey key)
{
    UNUSED_PARAM (pInRtInfo);
    UNUSED_PARAM (ppAppPtr);
    UNUSED_PARAM (key);
    return TRIE_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsFtnMaskToPrefixLen 
 *  Description     : Function to return the prefix length corresponding 
 *                    to the mask given 
 *  Input           : u4Mask  - Mask for which the length has to be found
 *  Output          : None
 *  Returns         : Prefix length 
 ************************************************************************/
INT4
MplsFtnMaskToPrefixLen (UINT4 u4Mask)
{
    UINT1               u1Tmp;
    UINT1              *pu1Byte;
    UINT1               u1TestByte = 0;
    INT4                i4Len = 0;
    INT4                i4Byte, i4Bit;

    u4Mask = OSIX_NTOHL (u4Mask);
    pu1Byte = (UINT1 *) &u4Mask;
    for (i4Byte = 0; i4Byte < 4; i4Byte++)
    {
        u1TestByte = *pu1Byte;
        u1Tmp = 0x80;
        for (i4Bit = 0; i4Bit < 8; i4Bit++)
        {
            if (!(u1TestByte & u1Tmp))
            {
                break;
            }
            i4Len++;
            u1TestByte = (UINT1) (u1TestByte << 1);
        }
        if (i4Bit != 8)
            break;
        pu1Byte++;
    }
    return (i4Len);
}

/*****************************************************************************/
/* Function Name      : MplsCheckTnlFrrEntryInFTN                            */
/*                                                                           */
/* Description        : This function checks whether the particular tunnel   */
/*                      used in the FTN, if used delete it and               */
/*                      associate with the optimised tunnel.                 */
/*                                                                           */
/* Input(s)           : pTeInTnlInfo - FRR tunnel indices                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
MplsCheckTnlFrrEntryInFTN (tFrrTeTnlInfo * pTeInTnlInfo)
{
    tFtnEntry          *pFtnEntry = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4Index = 0;
    UINT4               u4TnlIngressLSRId = 0;
    UINT4               u4TnlEgressLSRId = 0;

    MPLS_CMN_LOCK ();
    for (u4Index = MPLS_ONE; u4Index <= gu4MplsDbMaxFtnEntries; u4Index++)
    {
        pFtnEntry = MplsGetFtnTableEntry (u4Index);
        if ((pFtnEntry == NULL)
            || (pFtnEntry->i4ActionType != MPLS_FTN_ON_TE_LSP)
            || ((pFtnEntry->pActionPtr) == NULL))
        {
            continue;
        }
        pTeTnlInfo = (tTeTnlInfo *) pFtnEntry->pActionPtr;
        if (pTeInTnlInfo->bIsLocalRevert == MPLS_FALSE)
        {
            if ((pTeInTnlInfo->u4TnlIndex == pTeTnlInfo->u4TnlIndex) &&
                (pTeInTnlInfo->u4TnlInstance == pTeTnlInfo->u4TnlInstance) &&
                (MEMCMP (&TE_TNL_INGRESS_LSRID (pTeInTnlInfo),
                         &TE_TNL_INGRESS_LSRID (pTeTnlInfo),
                         IPV4_ADDR_LENGTH) == MPLS_ZERO) &&
                (MEMCMP (&TE_TNL_EGRESS_LSRID (pTeInTnlInfo),
                         &TE_TNL_EGRESS_LSRID (pTeTnlInfo),
                         IPV4_ADDR_LENGTH) == MPLS_ZERO))
            {
                CONVERT_TO_INTEGER (TE_TNL_INGRESS_LSRID (pTeInTnlInfo),
                                    u4TnlIngressLSRId);
                u4TnlIngressLSRId = OSIX_NTOHL (u4TnlIngressLSRId);
                CONVERT_TO_INTEGER (TE_TNL_EGRESS_LSRID (pTeInTnlInfo),
                                    u4TnlEgressLSRId);
                u4TnlEgressLSRId = OSIX_NTOHL (u4TnlEgressLSRId);
                pTeTnlInfo = TeGetTunnelInfo (pTeInTnlInfo->u4TnlIndex,
                                              pTeInTnlInfo->u4BkpTnlInstance,
                                              u4TnlIngressLSRId,
                                              u4TnlEgressLSRId);
                if (pTeTnlInfo != NULL)
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               " MplsCheckTnlFrrEntryInFTN to be programmed "
                               "the FTN on back-up tunnel \n");
                    pFtnEntry->pActionPtr = pTeTnlInfo;
                    if (MplsFTNAdd (pFtnEntry) == MPLS_FAILURE)    /*NP Add Call */
                    {
                        continue;
                    }
                }
            }
        }
        else
        {
            if ((pTeInTnlInfo->u4TnlIndex == pTeTnlInfo->u4TnlIndex) &&
                (pTeInTnlInfo->u4BkpTnlInstance == pTeTnlInfo->u4TnlInstance) &&
                (MEMCMP (&TE_TNL_INGRESS_LSRID (pTeInTnlInfo),
                         &TE_TNL_INGRESS_LSRID (pTeTnlInfo),
                         IPV4_ADDR_LENGTH) == MPLS_ZERO) &&
                (MEMCMP (&TE_TNL_EGRESS_LSRID (pTeInTnlInfo),
                         &TE_TNL_EGRESS_LSRID (pTeTnlInfo),
                         IPV4_ADDR_LENGTH) == MPLS_ZERO))
            {
                CONVERT_TO_INTEGER (TE_TNL_INGRESS_LSRID (pTeInTnlInfo),
                                    u4TnlIngressLSRId);
                u4TnlIngressLSRId = OSIX_NTOHL (u4TnlIngressLSRId);
                CONVERT_TO_INTEGER (TE_TNL_EGRESS_LSRID (pTeInTnlInfo),
                                    u4TnlEgressLSRId);
                u4TnlEgressLSRId = OSIX_NTOHL (u4TnlEgressLSRId);
                pTeTnlInfo = TeGetTunnelInfo (pTeInTnlInfo->u4TnlIndex,
                                              pTeInTnlInfo->u4TnlInstance,
                                              u4TnlIngressLSRId,
                                              u4TnlEgressLSRId);
                if (pTeTnlInfo != NULL)
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               " MplsCheckTnlFrrEntryInFTN to be programmed "
                               "the FTN on protected tunnel \n");
                    pFtnEntry->pActionPtr = pTeTnlInfo;
                    if (MplsFTNAdd (pFtnEntry) == MPLS_FAILURE)    /*NP Add Call */
                    {
                        continue;
                    }
                }
            }
        }
    }
    MPLS_CMN_UNLOCK ();
    return;
}


#ifdef MPLS_IPV6_WANTED
/* TRIE callback functions */

/*****************************************************************************/
/*                                                                           */
/* Function     : FtnIpv6TrieAddEntry                                            */
/*                                                                           */
/* Description  : This is the callback function to add the route entry to    */
/*                Ftn table                                                  */
/*                                                                           */
/* Input        : pInputParams    - Pointer used by trie                     */
/*                pOutputParams   - Pointer not used by FTN                  */
/*                ppAppSpecInfo   - Pointer to the address of the            */
/*                                  FTN specific information present in      */
/*                                  trie leaf node                           */
/*                pNewAppSpecInfo - Pointer to the new application           */
/*                                  FTN specific information that has to     */
/*                                  added                                    */
/*                                                                           */
/* Output       : ppAppSpecInfo   - Pointer to the address of the updated    */
/*                                   FTN specific information                */
/*                                                                           */
/* Returns      : SUCCESS                                                    */
/*                                                                           */
/*****************************************************************************/
INT4
FtnIpv6TrieAddEntry (VOID *pInputParams, VOID *pOutputParams,
                 VOID **ppAppSpecInfo, VOID *pNewAppSpecInfo)
{

    UNUSED_PARAM (pInputParams);
    UNUSED_PARAM (pOutputParams);
    (*ppAppSpecInfo) = pNewAppSpecInfo;
    return SUCCESS;
}


/*****************************************************************************/
/*                                                                           */
/* Function     : FtnIpv6TrieDeleteRtEntry                                      */
/*                                                                           */
/* Description  : This is the callback function to delete the ftn entry    */
/*                from FTN routing                                     */
/*                                                                           */
/* Input        : pInputParams    - Pointer used by trie                     */
/*                pOutputParams   - Pointer not used by FTN                 */
/*                ppAppSpecInfo   - Pointer to the address of the            */
/*                                  FTN specific information present in     */
/*                                  trie leaf node                           */
/*                pNewAppSpecInfo - Pointer to the new application           */
/*                                  FTN specific information that has to    */
/*                                  added                                    */
/*                                                                           */
/* Output       : ppAppSpecInfo   - Pointer to the address of the updated    */
/*                                  FTN specific information                */
/*                                                                           */
/* Returns      : SUCCESS                                                    */
/*                                                                           */
/*****************************************************************************/
INT4
FtnIpv6TrieDeleteEntry (VOID *pInputParams, VOID **ppAppSpecInfo,
                    VOID *pOutputParams, VOID *pNextHop, tKey Key)
{

    UNUSED_PARAM (pInputParams);
    UNUSED_PARAM (pOutputParams);
    UNUSED_PARAM (pNextHop);
    UNUSED_PARAM (Key);
    (*ppAppSpecInfo) = NULL;
    return SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FtnIpv6TrieDelAllEntry                                     */
/*                                                                           */
/* Description  : This is the callback function to delete the all the route  */
/*                entries from FTN table. This is used when the              */
/*                entire routing table needs to be cleared.                  */
/*                                                                           */
/* Input        : pInputParams    - Pointer used pointing to input parameters*/
/*                pOutputParams   - Pointer not used by FTN                  */
/*                VOID (*)(VOID *)- Dummy pointer to the function for future */
/*                                  use.                                     */
/*                                                                           */
/* Output       : NONE                                                       */
/*                                                                           */
/* Returns      : Pointer to the value which helps in deciding whether to    */
/*                delete the route entry at IP level                         */
/*                                                                           */
/*****************************************************************************/
UINT1              *
FtnIpv6TrieDelAllEntry (tInputParams * pInputParams, VOID *dummy,
                    tOutputParams * pOutputParams)
{
    UINT1              *pInpFtnInfo;
    UNUSED_PARAM (pOutputParams);
    UNUSED_PARAM (dummy);
    pInpFtnInfo = (UINT1 *) pInputParams;
    return (pInpFtnInfo);
}

/*****************************************************************************/
/* Function Name : FtnIpv6TrieDeInit                                         */
/* Description   : This function callback function for de-initializing Trie  */
/*                 Delete.                                                   */
/* Input(s)      : pDelParams - Input Params                                 */
/* Output(s)     : None.                                                     */
/* Return(s)     : None.                                                     */
/*****************************************************************************/
VOID
FtnIpv6TrieDeInit (VOID *pDelParams)
{
    UNUSED_PARAM (pDelParams);
    return;
}

/*****************************************************************************/
/*                                                                           */
/* Function     : FtnIpv6TrieSearchEntry                                     */
/*                                                                           */
/* Description  : This is the callback function to search the route entry    */
/*                from FTN table                                             */
/*                                                                           */
/* Input        : pInputParams    - Pointer used by trie                     */
/*                pOutputParams   - Pointer used by FTN to store the FTN     */
/*                                   entry                                   */
/*                pAppSpecInfo    - Pointer to the FTN specific information  */
/*                                  present in trie leaf node                */
/*                                                                           */
/* Output       : pOutputParams   - Pointer to the output parameters         */
/*                                  which has the route entry information    */
/*                                                                           */
/* Returns      : SUCCESS                                                    */
/*                                                                           */
/*****************************************************************************/

INT4
FtnIpv6TrieSearchEntry (tInputParams * pInputParams,
                    tOutputParams * pOutputParams, VOID *pAppSpecInfo)
{

    UNUSED_PARAM (pInputParams);
    pOutputParams->pAppSpecInfo = pAppSpecInfo;
    return SUCCESS;
}

/*****************************************************************************
 * Function     : FtnIpv6TrieLookUpEntry
 * Description  : This is the callback function to lookup the route entry
 *                from FTN table
 * Input        : pInputParams    - Pointer used by trie
 *                pOutputParams   - Pointer used by FTN to store the FTN entry
 *                pAppSpecInfo    - Pointer to the FTN specific information
 *                                  present in trie leaf node
 *                u2KeySize       - Key Size
 *                key             - Key
 * Output       : pOutputParams   - Pointer to the output parameters
 *                                  which has the route entry information
 * Returns      : SUCCESS
 *****************************************************************************/
INT4
FtnIpv6TrieLookUpEntry (tInputParams * pInputParams,
                    tOutputParams * pOutputParams,
                    VOID *pAppSpecInfo, UINT2 u2KeySize, tKey key)
{
    UNUSED_PARAM (pInputParams);
    UNUSED_PARAM (u2KeySize);
    UNUSED_PARAM (key);

    pOutputParams->pAppSpecInfo = pAppSpecInfo;
    return SUCCESS;
}

/*****************************************************************************
 * Function     : FtnIpv6TrieBestMatch
 * Description  : This is the callback function to get the best route
 *                from FTN table
 * Input        : pInputParams    - Pointer used by trie
 *                pOutputParams   - Pointer used by FTN to store the FTN entry
 *                pAppSpecInfo    - Pointer to the FTN specific information
 *                                  present in trie leaf node
 *                u2KeySize       - Key Size
 *                key             - Key
 * Output       : pOutputParams   - Pointer to the output parameters
 *                                  which has the route entry information
 * Returns      : SUCCESS
 *****************************************************************************/
	INT4
FtnIpv6TrieBestMatch (UINT2 u2KeySize, tInputParams * pInputParams,
                  VOID *pOutputParams, VOID *pAppSpecInfo, tKey Key)
{
    UNUSED_PARAM (pInputParams);
    UNUSED_PARAM (u2KeySize);
    UNUSED_PARAM (Key);

    ((tOutputParams *) pOutputParams)->pAppSpecInfo = pAppSpecInfo;
    return SUCCESS;
}

/*****************************************************************************/
/* Function     : FtnIpv6TrieDelEntry                                        */
/*                                                                           */
/* Description  : This is the callback function to delete a particular       */
/*                FTN entry.                                                 */
/* Input        : pDelIpRt        - Pointer to the value which decides       */
/*                                  whether FTN entry need to be deleted     */
/*                ppAppPtr        - Pointer to the FTN Entry to be deleted   */
/*                                  given by trie.                           */
/*                key             - Key given to trie                        */
/*                                                                           */
/* Output       : ppAppPtr with NULL value                                   */
/*                                                                           */
/* Returns      : SUCCESS                                                    */
/*****************************************************************************/
INT4
FtnIpv6TrieDelEntry (VOID *pInRtInfo, VOID **ppAppPtr, tKey key)
{
    UNUSED_PARAM (pInRtInfo);
    UNUSED_PARAM (ppAppPtr);
    UNUSED_PARAM (key);
    return TRIE_SUCCESS;
}

tFtnEntry *
MplsSignalGetFtnIpv6TableEntry(tIp6Addr *pLookUpAddr)
{
	tInputParams        FtnInputParams;
	tOutputParams       FtnOutputParams;
	tFtnEntry          *pFtnEntry = NULL;
	VOID               *pTemp = NULL;
	UINT1               u1Key[IP6_ADDR_SIZE+IP6_ADDR_SIZE];
	tIp6Addr            AddrMask;

	MEMSET ((UINT1 *) &AddrMask, 0xFF, sizeof (tIp6Addr));

	FtnInputParams.pRoot = gpFtnIpv6TrieRoot;
	FtnInputParams.pLeafNode = NULL;
	FtnInputParams.i1AppId = MPLS_APPL_ID;
	FtnInputParams.u1PrefixLen = 0;

	MEMCPY((tIp6Addr *) (VOID *) u1Key, pLookUpAddr, IP6_ADDR_SIZE);
        MEMCPY((tIp6Addr *) (VOID *) (u1Key + IP6_ADDR_SIZE), &AddrMask,
			IP6_ADDR_SIZE);

	FtnInputParams.Key.pKey = (UINT1 *) u1Key;

	FtnOutputParams.Key.pKey = NULL;
	FtnOutputParams.pAppSpecInfo = NULL;

	if ((TrieSearch (&FtnInputParams, &FtnOutputParams,
					(VOID **) &pTemp)) == TRIE_FAILURE)
	{
		return NULL;
	}
	pFtnEntry = (tFtnEntry *) FtnOutputParams.pAppSpecInfo;
	return pFtnEntry;
}

/************************************************************************
 *  Function Name   : MplsFwdGetFtnIpv6TableEntry
 *  Description     : Function to get exact or best entry from the Trie
 *  Input           : Address
 *  Output          : None
 *  Returns         : Pointer of the Entry or NULL
 ************************************************************************/
tFtnEntry          *
MplsFwdGetFtnIpv6TableEntry (tIp6Addr *pLookUpAddr)
{
    tInputParams        FtnInputParams;
    tOutputParams       FtnOutputParams;
    tFtnEntry          *pFtnEntry = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    VOID               *pTemp = NULL;
    UINT1               u1Key[IP6_ADDR_SIZE+IP6_ADDR_SIZE];
    tIp6Addr            AddrMask;

    MEMSET ((UINT1 *) &AddrMask, 0xFF, sizeof (tIp6Addr));

    FtnInputParams.pRoot = gpFtnIpv6TrieRoot;
    FtnInputParams.pLeafNode = NULL;
    FtnInputParams.i1AppId = MPLS_APPL_ID;
    FtnInputParams.u1PrefixLen = 0;

    MEMCPY((tIp6Addr *) (VOID *) u1Key, pLookUpAddr, IP6_ADDR_SIZE);
    MEMCPY((tIp6Addr *) (VOID *) (u1Key + IP6_ADDR_SIZE), &AddrMask,
		    IP6_ADDR_SIZE);

    FtnInputParams.Key.pKey = (UINT1 *) u1Key;

    FtnOutputParams.Key.pKey = NULL;
    FtnOutputParams.pAppSpecInfo = NULL;

    if ((TrieLookup (&FtnInputParams, &FtnOutputParams,
                     (VOID **) &pTemp)) == TRIE_FAILURE)
    {
        return NULL;
    }
    pFtnEntry = (tFtnEntry *) FtnOutputParams.pAppSpecInfo;

    if (pFtnEntry != NULL)
    {
	    if (pFtnEntry->i4ActionType == MPLS_FTN_ON_TE_LSP)
	    {
		    pTeTnlInfo = pFtnEntry->pActionPtr;

		    /* MPLS Switching should not happen on admin down tunnels.
		     * This function will be removed once software forwarding portion
		     * is removed. */
		    if ((pTeTnlInfo == NULL) ||
				    (pTeTnlInfo->u1TnlOperStatus == TE_OPER_DOWN))
		    {
			    return NULL;
		    }
	    }
    }
    return pFtnEntry;
}


/************************************************************************
 *  Function Name   : MplsDeleteFtnIpv6TableEntry
 *  Description     : Function to delete a Ftn entry from Ftn table
 *  Input           : Mpls Ftn Entry Pointer
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE
 ************************************************************************/
INT4
MplsDeleteFtnIpv6TableEntry (tFtnEntry * pFtnEntry)
{
    tInputParams        FtnInputParams;
    tOutputParams       FtnOutputParams;

    UINT1               u1Key[IP6_ADDR_SIZE+IP6_ADDR_SIZE];
    tIp6Addr            AddrMask;
    VOID               *pTemp = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tIp6Addr            Fec;

    MEMSET(&Fec,0,sizeof(tIp6Addr));
    MEMSET ((UINT1 *) &AddrMask, 0xFF, sizeof (tIp6Addr));


    FtnInputParams.pLeafNode = NULL;
    FtnInputParams.u1PrefixLen = 0;
    FtnInputParams.pRoot = gpFtnIpv6TrieRoot;
    FtnInputParams.i1AppId = MPLS_APPL_ID;

    MEMCPY( u1Key, pFtnEntry->DestAddrMin.u1_addr, IP6_ADDR_SIZE);
    MEMCPY((tIp6Addr *) (VOID *) (u1Key + IP6_ADDR_SIZE), &AddrMask,
		    IP6_ADDR_SIZE);

    FtnInputParams.Key.pKey = (UINT1 *) u1Key;


    FtnOutputParams.Key.pKey = NULL;
    FtnOutputParams.pAppSpecInfo = NULL;

    MEMCPY(Fec.u1_addr, pFtnEntry->DestAddrMin.u1_addr, IP6_ADDR_SIZE);

    CMNDB_DBG3(DEBUG_DEBUG_LEVEL,"%s : %d Ipv6 Trie Deletion FEC = %s\n",__FUNCTION__,__LINE__,Ip6PrintAddr(&Fec));


    RBTreeRem (gpFtnTableRbTree, pFtnEntry);

    if (TrieRemove (&FtnInputParams, &FtnOutputParams, pTemp) == TRIE_FAILURE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
			"Delete FTN Table: Trie Delete Failed\t\n");
        return MPLS_FAILURE;
    }

    if (pFtnEntry->i4ActionType == REDIRECTTUNNEL)
    {
	    pTeTnlInfo = pFtnEntry->pActionPtr;
	    if (pTeTnlInfo != NULL)
	    {
		    TMO_DLL_Delete (&(pTeTnlInfo->FtnList), &(pFtnEntry->FtnInfoNode));
		    CMNDB_DBG
			    (DEBUG_DEBUG_LEVEL,
			     "Delete FTN Table: FTN is deleted from Tunnel entry\t\n");
	    }
    }
    MplsFtnRelIndex (pFtnEntry->u4FtnIndex);
    MemReleaseMemBlock (FTN_TABLE_POOL_ID, (UINT1 *) pFtnEntry);
    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsRegisterEntrywithIpv6Trie
 *  Description     : Function to creates entry in Ipv6 Trie
 *  Input           : Mpls Ftn Entry Pointer
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE
 ************************************************************************/
INT4
MplsRegisterEntrywithIpv6Trie (tFtnEntry * pFtnEntry)
{

    tInputParams        FtnInputParams;
    tOutputParams       FtnOutputParams;

    UINT1               u1Key[IP6_ADDR_SIZE+IP6_ADDR_SIZE];
    tIp6Addr            AddrMask;
    tIp6Addr            Fec;

    MEMSET(&Fec,0,sizeof(tIp6Addr));
    MEMSET ((UINT1 *) &AddrMask, 0xFF, sizeof (tIp6Addr));


    FtnInputParams.pLeafNode = NULL;
    FtnInputParams.u1PrefixLen = MplsGetIpv6Subnetmasklen(pFtnEntry->DestAddrMax.u1_addr);
    FtnInputParams.pRoot = gpFtnIpv6TrieRoot;
    FtnInputParams.i1AppId = MPLS_APPL_ID;
    MEMCPY ( u1Key, pFtnEntry->DestAddrMin.u1_addr, IP6_ADDR_SIZE);
    MEMCPY ((tIp6Addr *) (VOID *) (u1Key + IP6_ADDR_SIZE), &AddrMask,
		    IP6_ADDR_SIZE);

    FtnInputParams.Key.pKey = (UINT1 *)u1Key;

    FtnOutputParams.pAppSpecInfo = NULL;
    FtnOutputParams.Key.pKey = NULL;

    MEMCPY(Fec.u1_addr, pFtnEntry->DestAddrMin.u1_addr, IP6_ADDR_SIZE);

    CMNDB_DBG3(DEBUG_DEBUG_LEVEL,
            "%s : %d Ipv6 Trie Addition FEC = %s\n",__FUNCTION__,__LINE__,Ip6PrintAddr(&Fec));


    if ((TrieAdd (&FtnInputParams, pFtnEntry,
                  &FtnOutputParams)) == TRIE_FAILURE)

    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "Create FTN Table : Trie Add Failed \t\n");
        return MPLS_FAILURE;
    }
    return MPLS_SUCCESS;
}
/*****************************************************************************/
/* Function Name : MplsFtnIpv6TrieCbDelete                                   */
/* Description   : This is a call back function for TRIE Delete              */
/* Input(s)      : pInput - Input Parameter.                                 */
/* Output(s)     : None.                                                     */
/* Return(s)     : None.                                                     */
/*****************************************************************************/
VOID
MplsFtnIpv6TrieCbDelete (VOID *pInput)
{
    UNUSED_PARAM (pInput);
    return;
}

#endif
