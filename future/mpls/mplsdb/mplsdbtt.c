#include "mplsdbinc.h"

/* $Id: mplsdbtt.c,v 1.5 2014/12/12 11:56:43 siva Exp $ */

/****************************************************************************
 Function    :  MplsDbTestAllGmplsInterfaceTable
 Description :  This function tests GMPLS Interface Table MIB objects                
 Input       :  i4MplsInterfaceIndex - MPLS Interface Index
                pTestMplsIfEntry     - Pointer to MPLS IF Entry
                u4ObjectId           - Object ID
 Output      :  pu4ErrorCode         - Error Code
 Returns     :  SNMP_SUCCESS / SNMP_FAILURE
****************************************************************************/
INT1
MplsDbTestAllGmplsInterfaceTable (UINT4 *pu4ErrorCode,
                                  INT4 i4MplsInterfaceIndex,
                                  tMplsIfEntry * pTestMplsIfEntry,
                                  UINT4 u4ObjectId)
{
    tMplsIfEntry       *pMplsIfEntry = NULL;
    UINT1               u1SigCaps = MPLS_ZERO;

    pMplsIfEntry = MplsGetIfTableEntry ((UINT4)i4MplsInterfaceIndex);

    if (pMplsIfEntry == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case GMPLS_LSR_INTF_SIGNALLING_CAPS:
        {
            u1SigCaps = pTestMplsIfEntry->GmplsIfEntry.u1SignalingCaps;

            if (pTestMplsIfEntry->GmplsIfEntry.u1SigCapsLen != sizeof (UINT1))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return SNMP_FAILURE;
            }

            switch (u1SigCaps)
            {
                case GMPLS_SIG_CAP_RSVP_BIT:
                case GMPLS_SIG_CAP_UNKNOWN_BIT:
                case GMPLS_SIG_CAP_OTHER_BIT:
                case (GMPLS_SIG_CAP_RSVP_BIT + GMPLS_SIG_CAP_OTHER_BIT):
                    break;
                default:
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }
            }

            break;
        }

        default:
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  MplsDbTestAllGmplsInSegmentTable
 Description :  This function tests GMPLS InSegment Table MIB objects                
 Input       :  u4Index              - InSegment Index
                pTestInSegEntry      - Pointer to Insegment Entry
                u4ObjectId           - Object ID
 Output      :  pu4ErrorCode         - Error Code
 Returns     :  SNMP_SUCCESS / SNMP_FAILURE
****************************************************************************/
INT1
MplsDbTestAllGmplsInSegmentTable (UINT4 *pu4ErrorCode,
                                  UINT4 u4Index, tInSegment * pTestInSegEntry,
                                  UINT4 u4ObjectId)
{
    tInSegment         *pInSegment = NULL;

    pInSegment = MplsGetInSegmentTableEntry (u4Index);

    if (pInSegment == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pInSegment->u1RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case GMPLS_LSR_INSEG_DIRECTION:
        {
            if ((pTestInSegEntry->GmplsInSegment.InSegmentDirection
                 != MPLS_DIRECTION_FORWARD) &&
                (pTestInSegEntry->GmplsInSegment.InSegmentDirection
                 != MPLS_DIRECTION_REVERSE))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }

        default:
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  MplsDbTestAllGmplsOutSegmentTable
 Description :  This function tests GMPLS OutSegment Table MIB objects                
 Input       :  u4Index              - OutSegment Index
                pTestOutSegEntry     - Pointer to Outsegment Entry
                u4ObjectId           - Object ID
 Output      :  pu4ErrorCode         - Error Code
 Returns     :  SNMP_SUCCESS / SNMP_FAILURE
****************************************************************************/
INT1
MplsDbTestAllGmplsOutSegmentTable (UINT4 *pu4ErrorCode,
                                   UINT4 u4Index,
                                   tOutSegment * pTestOutSegEntry,
                                   UINT4 u4ObjectId)
{
    tOutSegment        *pOutSegment = NULL;

    pOutSegment = MplsGetOutSegmentTableEntry (u4Index);

    if (pOutSegment == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pOutSegment->u1RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case GMPLS_LSR_OUTSEG_DIRECTION:
        {
            if ((pTestOutSegEntry->GmplsOutSegment.OutSegmentDirection
                 != MPLS_DIRECTION_FORWARD) &&
                (pTestOutSegEntry->GmplsOutSegment.OutSegmentDirection
                 != MPLS_DIRECTION_REVERSE))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }

        default:
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}
