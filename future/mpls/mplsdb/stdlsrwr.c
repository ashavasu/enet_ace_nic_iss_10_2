# include  "lr.h"
# include  "fssnmp.h"
# include  "stdlsrlw.h"
# include  "stdlsrwr.h"
# include  "stdlsrdb.h"

INT4
GetNextIndexMplsInterfaceTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMplsInterfaceTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMplsInterfaceTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

VOID
RegisterLSR ()
{
    SNMPRegisterMib (&lsrOID, &lsrEntry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&lsrOID, (const UINT1 *) "lsr");
}

INT4
MplsInterfaceIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[0].i4_SLongValue;

    return SNMP_SUCCESS;

}

INT4
MplsInterfaceLabelMinInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsInterfaceLabelMinIn (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
MplsInterfaceLabelMaxInGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsInterfaceLabelMaxIn (pMultiIndex->pIndex[0].i4_SLongValue,
                                           &(pMultiData->u4_ULongValue)));

}

INT4
MplsInterfaceLabelMinOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsInterfaceLabelMinOut
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsInterfaceLabelMaxOutGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsInterfaceLabelMaxOut
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsInterfaceTotalBandwidthGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsInterfaceTotalBandwidth
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsInterfaceAvailableBandwidthGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsInterfaceAvailableBandwidth
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsInterfaceLabelParticipationTypeGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInterfaceTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsInterfaceLabelParticipationType
            (pMultiIndex->pIndex[0].i4_SLongValue, pMultiData->pOctetStrValue));

}

INT4
GetNextIndexMplsInterfacePerfTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMplsInterfacePerfTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMplsInterfacePerfTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
MplsInterfacePerfInLabelsInUseGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInterfacePerfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsInterfacePerfInLabelsInUse
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsInterfacePerfInLabelLookupFailuresGet (tSnmpIndex * pMultiIndex,
                                           tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInterfacePerfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsInterfacePerfInLabelLookupFailures
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsInterfacePerfOutLabelsInUseGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInterfacePerfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsInterfacePerfOutLabelsInUse
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsInterfacePerfOutFragmentedPktsGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInterfacePerfTable
        (pMultiIndex->pIndex[0].i4_SLongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsInterfacePerfOutFragmentedPkts
            (pMultiIndex->pIndex[0].i4_SLongValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsInSegmentIndexNextGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetMplsInSegmentIndexNext (pMultiData->pOctetStrValue));
}

INT4
GetNextIndexMplsInSegmentTable (tSnmpIndex * pFirstMultiIndex,
                                tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMplsInSegmentTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMplsInSegmentTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
MplsInSegmentIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInSegmentTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[0].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[0].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[0].pOctetStrValue->i4_Length;

    return SNMP_SUCCESS;

}

INT4
MplsInSegmentInterfaceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInSegmentTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsInSegmentInterface (pMultiIndex->pIndex[0].pOctetStrValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
MplsInSegmentLabelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInSegmentTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsInSegmentLabel (pMultiIndex->pIndex[0].pOctetStrValue,
                                      &(pMultiData->u4_ULongValue)));

}

INT4
MplsInSegmentLabelPtrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInSegmentTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsInSegmentLabelPtr (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiData->pOidValue));

}

INT4
MplsInSegmentNPopGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInSegmentTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsInSegmentNPop (pMultiIndex->pIndex[0].pOctetStrValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
MplsInSegmentAddrFamilyGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInSegmentTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsInSegmentAddrFamily
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsInSegmentXCIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInSegmentTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsInSegmentXCIndex (pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->pOctetStrValue));

}

INT4
MplsInSegmentOwnerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInSegmentTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsInSegmentOwner (pMultiIndex->pIndex[0].pOctetStrValue,
                                      &(pMultiData->i4_SLongValue)));

}

INT4
MplsInSegmentTrafficParamPtrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInSegmentTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsInSegmentTrafficParamPtr
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->pOidValue));

}

INT4
MplsInSegmentRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInSegmentTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsInSegmentRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                          &(pMultiData->i4_SLongValue)));

}

INT4
MplsInSegmentStorageTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInSegmentTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsInSegmentStorageType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsInSegmentInterfaceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsInSegmentInterface (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
MplsInSegmentLabelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsInSegmentLabel (pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiData->u4_ULongValue));

}

INT4
MplsInSegmentLabelPtrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsInSegmentLabelPtr (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiData->pOidValue));

}

INT4
MplsInSegmentNPopSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsInSegmentNPop (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiData->i4_SLongValue));

}

INT4
MplsInSegmentAddrFamilySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsInSegmentAddrFamily
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
MplsInSegmentTrafficParamPtrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsInSegmentTrafficParamPtr
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->pOidValue));

}

INT4
MplsInSegmentRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsInSegmentRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiData->i4_SLongValue));

}

INT4
MplsInSegmentStorageTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsInSegmentStorageType
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
MplsInSegmentInterfaceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2MplsInSegmentInterface (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             pOctetStrValue,
                                             pMultiData->i4_SLongValue));

}

INT4
MplsInSegmentLabelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                        tRetVal * pMultiData)
{
    return (nmhTestv2MplsInSegmentLabel (pu4Error,
                                         pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiData->u4_ULongValue));

}

INT4
MplsInSegmentLabelPtrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2MplsInSegmentLabelPtr (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            pOctetStrValue,
                                            pMultiData->pOidValue));

}

INT4
MplsInSegmentNPopTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2MplsInSegmentNPop (pu4Error,
                                        pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
MplsInSegmentAddrFamilyTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2MplsInSegmentAddrFamily (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              pOctetStrValue,
                                              pMultiData->i4_SLongValue));

}

INT4
MplsInSegmentTrafficParamPtrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhTestv2MplsInSegmentTrafficParamPtr (pu4Error,
                                                   pMultiIndex->pIndex[0].
                                                   pOctetStrValue,
                                                   pMultiData->pOidValue));

}

INT4
MplsInSegmentRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2MplsInSegmentRowStatus (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             pOctetStrValue,
                                             pMultiData->i4_SLongValue));

}

INT4
MplsInSegmentStorageTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2MplsInSegmentStorageType (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiData->i4_SLongValue));

}

INT4
MplsInSegmentTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MplsInSegmentTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexMplsInSegmentPerfTable (tSnmpIndex * pFirstMultiIndex,
                                    tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMplsInSegmentPerfTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMplsInSegmentPerfTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
MplsInSegmentPerfOctetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInSegmentPerfTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsInSegmentPerfOctets
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsInSegmentPerfPacketsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInSegmentPerfTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsInSegmentPerfPackets
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsInSegmentPerfErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInSegmentPerfTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsInSegmentPerfErrors
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsInSegmentPerfDiscardsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInSegmentPerfTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsInSegmentPerfDiscards
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsInSegmentPerfHCOctetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInSegmentPerfTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsInSegmentPerfHCOctets
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
MplsInSegmentPerfDiscontinuityTimeGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInSegmentPerfTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsInSegmentPerfDiscontinuityTime
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsOutSegmentIndexNextGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetMplsOutSegmentIndexNext (pMultiData->pOctetStrValue));
}

INT4
GetNextIndexMplsOutSegmentTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMplsOutSegmentTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMplsOutSegmentTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
MplsOutSegmentIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsOutSegmentTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[0].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[0].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[0].pOctetStrValue->i4_Length;

    return SNMP_SUCCESS;

}

INT4
MplsOutSegmentInterfaceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsOutSegmentTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsOutSegmentInterface
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsOutSegmentPushTopLabelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsOutSegmentTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsOutSegmentPushTopLabel
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsOutSegmentTopLabelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsOutSegmentTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsOutSegmentTopLabel (pMultiIndex->pIndex[0].pOctetStrValue,
                                          &(pMultiData->u4_ULongValue)));

}

INT4
MplsOutSegmentTopLabelPtrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsOutSegmentTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsOutSegmentTopLabelPtr
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->pOidValue));

}

INT4
MplsOutSegmentNextHopAddrTypeGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsOutSegmentTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsOutSegmentNextHopAddrType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsOutSegmentNextHopAddrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsOutSegmentTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsOutSegmentNextHopAddr
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
MplsOutSegmentXCIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsOutSegmentTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsOutSegmentXCIndex (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiData->pOctetStrValue));

}

INT4
MplsOutSegmentOwnerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsOutSegmentTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsOutSegmentOwner (pMultiIndex->pIndex[0].pOctetStrValue,
                                       &(pMultiData->i4_SLongValue)));

}

INT4
MplsOutSegmentTrafficParamPtrGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsOutSegmentTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsOutSegmentTrafficParamPtr
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->pOidValue));

}

INT4
MplsOutSegmentRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsOutSegmentTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsOutSegmentRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsOutSegmentStorageTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsOutSegmentTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsOutSegmentStorageType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsOutSegmentInterfaceSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsOutSegmentInterface
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
MplsOutSegmentPushTopLabelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsOutSegmentPushTopLabel
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
MplsOutSegmentTopLabelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsOutSegmentTopLabel (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiData->u4_ULongValue));

}

INT4
MplsOutSegmentTopLabelPtrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsOutSegmentTopLabelPtr
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->pOidValue));

}

INT4
MplsOutSegmentNextHopAddrTypeSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetMplsOutSegmentNextHopAddrType
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
MplsOutSegmentNextHopAddrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsOutSegmentNextHopAddr
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
MplsOutSegmentTrafficParamPtrSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetMplsOutSegmentTrafficParamPtr
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->pOidValue));

}

INT4
MplsOutSegmentRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsOutSegmentRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
MplsOutSegmentStorageTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsOutSegmentStorageType
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
MplsOutSegmentInterfaceTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2MplsOutSegmentInterface (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              pOctetStrValue,
                                              pMultiData->i4_SLongValue));

}

INT4
MplsOutSegmentPushTopLabelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2MplsOutSegmentPushTopLabel (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 pOctetStrValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
MplsOutSegmentTopLabelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2MplsOutSegmentTopLabel (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             pOctetStrValue,
                                             pMultiData->u4_ULongValue));

}

INT4
MplsOutSegmentTopLabelPtrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2MplsOutSegmentTopLabelPtr (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                pOctetStrValue,
                                                pMultiData->pOidValue));

}

INT4
MplsOutSegmentNextHopAddrTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2MplsOutSegmentNextHopAddrType (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    pOctetStrValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
MplsOutSegmentNextHopAddrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2MplsOutSegmentNextHopAddr (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                pOctetStrValue,
                                                pMultiData->pOctetStrValue));

}

INT4
MplsOutSegmentTrafficParamPtrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2MplsOutSegmentTrafficParamPtr (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    pOctetStrValue,
                                                    pMultiData->pOidValue));

}

INT4
MplsOutSegmentRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2MplsOutSegmentRowStatus (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              pOctetStrValue,
                                              pMultiData->i4_SLongValue));

}

INT4
MplsOutSegmentStorageTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2MplsOutSegmentStorageType (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                pOctetStrValue,
                                                pMultiData->i4_SLongValue));

}

INT4
MplsOutSegmentTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MplsOutSegmentTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexMplsOutSegmentPerfTable (tSnmpIndex * pFirstMultiIndex,
                                     tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMplsOutSegmentPerfTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMplsOutSegmentPerfTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
MplsOutSegmentPerfOctetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsOutSegmentPerfTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsOutSegmentPerfOctets
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsOutSegmentPerfPacketsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsOutSegmentPerfTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsOutSegmentPerfPackets
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsOutSegmentPerfErrorsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsOutSegmentPerfTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsOutSegmentPerfErrors
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsOutSegmentPerfDiscardsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsOutSegmentPerfTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsOutSegmentPerfDiscards
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsOutSegmentPerfHCOctetsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsOutSegmentPerfTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsOutSegmentPerfHCOctets
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
MplsOutSegmentPerfDiscontinuityTimeGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsOutSegmentPerfTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsOutSegmentPerfDiscontinuityTime
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsXCIndexNextGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetMplsXCIndexNext (pMultiData->pOctetStrValue));
}

INT4
GetNextIndexMplsXCTable (tSnmpIndex * pFirstMultiIndex,
                         tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMplsXCTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMplsXCTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].pOctetStrValue,
             pNextMultiIndex->pIndex[1].pOctetStrValue,
             pFirstMultiIndex->pIndex[2].pOctetStrValue,
             pNextMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
MplsXCIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsXCTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[0].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[0].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[0].pOctetStrValue->i4_Length;

    return SNMP_SUCCESS;

}

INT4
MplsXCInSegmentIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsXCTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[1].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[1].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[1].pOctetStrValue->i4_Length;

    return SNMP_SUCCESS;

}

INT4
MplsXCOutSegmentIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsXCTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[2].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[2].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[2].pOctetStrValue->i4_Length;

    return SNMP_SUCCESS;

}

INT4
MplsXCLspIdGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsXCTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsXCLspId (pMultiIndex->pIndex[0].pOctetStrValue,
                               pMultiIndex->pIndex[1].pOctetStrValue,
                               pMultiIndex->pIndex[2].pOctetStrValue,
                               pMultiData->pOctetStrValue));

}

INT4
MplsXCLabelStackIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsXCTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsXCLabelStackIndex (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         pMultiIndex->pIndex[2].pOctetStrValue,
                                         pMultiData->pOctetStrValue));

}

INT4
MplsXCOwnerGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsXCTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsXCOwner (pMultiIndex->pIndex[0].pOctetStrValue,
                               pMultiIndex->pIndex[1].pOctetStrValue,
                               pMultiIndex->pIndex[2].pOctetStrValue,
                               &(pMultiData->i4_SLongValue)));

}

INT4
MplsXCRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsXCTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsXCRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiIndex->pIndex[1].pOctetStrValue,
                                   pMultiIndex->pIndex[2].pOctetStrValue,
                                   &(pMultiData->i4_SLongValue)));

}

INT4
MplsXCStorageTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsXCTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsXCStorageType (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiIndex->pIndex[2].pOctetStrValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
MplsXCAdminStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsXCTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsXCAdminStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiIndex->pIndex[2].pOctetStrValue,
                                     &(pMultiData->i4_SLongValue)));

}

INT4
MplsXCOperStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsXCTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].pOctetStrValue,
         pMultiIndex->pIndex[2].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsXCOperStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                    pMultiIndex->pIndex[1].pOctetStrValue,
                                    pMultiIndex->pIndex[2].pOctetStrValue,
                                    &(pMultiData->i4_SLongValue)));

}

INT4
MplsXCLspIdSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsXCLspId (pMultiIndex->pIndex[0].pOctetStrValue,
                               pMultiIndex->pIndex[1].pOctetStrValue,
                               pMultiIndex->pIndex[2].pOctetStrValue,
                               pMultiData->pOctetStrValue));

}

INT4
MplsXCLabelStackIndexSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsXCLabelStackIndex (pMultiIndex->pIndex[0].pOctetStrValue,
                                         pMultiIndex->pIndex[1].pOctetStrValue,
                                         pMultiIndex->pIndex[2].pOctetStrValue,
                                         pMultiData->pOctetStrValue));

}

INT4
MplsXCRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsXCRowStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                   pMultiIndex->pIndex[1].pOctetStrValue,
                                   pMultiIndex->pIndex[2].pOctetStrValue,
                                   pMultiData->i4_SLongValue));

}

INT4
MplsXCStorageTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsXCStorageType (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiIndex->pIndex[2].pOctetStrValue,
                                     pMultiData->i4_SLongValue));

}

INT4
MplsXCAdminStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsXCAdminStatus (pMultiIndex->pIndex[0].pOctetStrValue,
                                     pMultiIndex->pIndex[1].pOctetStrValue,
                                     pMultiIndex->pIndex[2].pOctetStrValue,
                                     pMultiData->i4_SLongValue));

}

INT4
MplsXCLspIdTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                 tRetVal * pMultiData)
{
    return (nmhTestv2MplsXCLspId (pu4Error,
                                  pMultiIndex->pIndex[0].pOctetStrValue,
                                  pMultiIndex->pIndex[1].pOctetStrValue,
                                  pMultiIndex->pIndex[2].pOctetStrValue,
                                  pMultiData->pOctetStrValue));

}

INT4
MplsXCLabelStackIndexTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                           tRetVal * pMultiData)
{
    return (nmhTestv2MplsXCLabelStackIndex (pu4Error,
                                            pMultiIndex->pIndex[0].
                                            pOctetStrValue,
                                            pMultiIndex->pIndex[1].
                                            pOctetStrValue,
                                            pMultiIndex->pIndex[2].
                                            pOctetStrValue,
                                            pMultiData->pOctetStrValue));

}

INT4
MplsXCRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                     tRetVal * pMultiData)
{
    return (nmhTestv2MplsXCRowStatus (pu4Error,
                                      pMultiIndex->pIndex[0].pOctetStrValue,
                                      pMultiIndex->pIndex[1].pOctetStrValue,
                                      pMultiIndex->pIndex[2].pOctetStrValue,
                                      pMultiData->i4_SLongValue));

}

INT4
MplsXCStorageTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2MplsXCStorageType (pu4Error,
                                        pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiIndex->pIndex[2].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
MplsXCAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                       tRetVal * pMultiData)
{
    return (nmhTestv2MplsXCAdminStatus (pu4Error,
                                        pMultiIndex->pIndex[0].pOctetStrValue,
                                        pMultiIndex->pIndex[1].pOctetStrValue,
                                        pMultiIndex->pIndex[2].pOctetStrValue,
                                        pMultiData->i4_SLongValue));

}

INT4
MplsXCTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MplsXCTable (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
MplsMaxLabelStackDepthGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetMplsMaxLabelStackDepth (&(pMultiData->u4_ULongValue)));
}

INT4
MplsLabelStackIndexNextGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetMplsLabelStackIndexNext (pMultiData->pOctetStrValue));
}

INT4
GetNextIndexMplsLabelStackTable (tSnmpIndex * pFirstMultiIndex,
                                 tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMplsLabelStackTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMplsLabelStackTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue,
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
MplsLabelStackIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLabelStackTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[0].pOctetStrValue->pu1_OctetList,
            pMultiIndex->pIndex[0].pOctetStrValue->i4_Length);
    pMultiData->pOctetStrValue->i4_Length =
        pMultiIndex->pIndex[0].pOctetStrValue->i4_Length;

    return SNMP_SUCCESS;

}

INT4
MplsLabelStackLabelIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLabelStackTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[1].u4_ULongValue;

    return SNMP_SUCCESS;

}

INT4
MplsLabelStackLabelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLabelStackTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLabelStackLabel (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       &(pMultiData->u4_ULongValue)));

}

INT4
MplsLabelStackLabelPtrGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLabelStackTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLabelStackLabelPtr (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->pOidValue));

}

INT4
MplsLabelStackRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLabelStackTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLabelStackRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLabelStackStorageTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsLabelStackTable
        (pMultiIndex->pIndex[0].pOctetStrValue,
         pMultiIndex->pIndex[1].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsLabelStackStorageType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsLabelStackLabelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsLabelStackLabel (pMultiIndex->pIndex[0].pOctetStrValue,
                                       pMultiIndex->pIndex[1].u4_ULongValue,
                                       pMultiData->u4_ULongValue));

}

INT4
MplsLabelStackLabelPtrSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsLabelStackLabelPtr (pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->pOidValue));

}

INT4
MplsLabelStackRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsLabelStackRowStatus
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
MplsLabelStackStorageTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsLabelStackStorageType
            (pMultiIndex->pIndex[0].pOctetStrValue,
             pMultiIndex->pIndex[1].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
MplsLabelStackLabelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                         tRetVal * pMultiData)
{
    return (nmhTestv2MplsLabelStackLabel (pu4Error,
                                          pMultiIndex->pIndex[0].pOctetStrValue,
                                          pMultiIndex->pIndex[1].u4_ULongValue,
                                          pMultiData->u4_ULongValue));

}

INT4
MplsLabelStackLabelPtrTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    return (nmhTestv2MplsLabelStackLabelPtr (pu4Error,
                                             pMultiIndex->pIndex[0].
                                             pOctetStrValue,
                                             pMultiIndex->pIndex[1].
                                             u4_ULongValue,
                                             pMultiData->pOidValue));

}

INT4
MplsLabelStackRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    return (nmhTestv2MplsLabelStackRowStatus (pu4Error,
                                              pMultiIndex->pIndex[0].
                                              pOctetStrValue,
                                              pMultiIndex->pIndex[1].
                                              u4_ULongValue,
                                              pMultiData->i4_SLongValue));

}

INT4
MplsLabelStackStorageTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2MplsLabelStackStorageType (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                pOctetStrValue,
                                                pMultiIndex->pIndex[1].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
MplsLabelStackTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MplsLabelStackTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexMplsInSegmentMapTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMplsInSegmentMapTable
            (&(pNextMultiIndex->pIndex[0].i4_SLongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pNextMultiIndex->pIndex[2].pOidValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMplsInSegmentMapTable
            (pFirstMultiIndex->pIndex[0].i4_SLongValue,
             &(pNextMultiIndex->pIndex[0].i4_SLongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].pOidValue,
             pNextMultiIndex->pIndex[2].pOidValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
MplsInSegmentMapInterfaceGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInSegmentMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOidValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->i4_SLongValue = pMultiIndex->pIndex[0].i4_SLongValue;

    return SNMP_SUCCESS;

}

INT4
MplsInSegmentMapLabelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInSegmentMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOidValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    pMultiData->u4_ULongValue = pMultiIndex->pIndex[1].u4_ULongValue;

    return SNMP_SUCCESS;

}

INT4
MplsInSegmentMapLabelPtrIndexGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInSegmentMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOidValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    MEMCPY (pMultiData->pOidValue->pu4_OidList,
            pMultiIndex->pIndex[2].pOidValue->pu4_OidList,
            (pMultiIndex->pIndex[2].pOidValue->u4_Length + sizeof (UINT4)));
    pMultiData->pOidValue->u4_Length =
        pMultiIndex->pIndex[2].pOidValue->u4_Length;

    return SNMP_SUCCESS;

}

INT4
MplsInSegmentMapIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsInSegmentMapTable
        (pMultiIndex->pIndex[0].i4_SLongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].pOidValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsInSegmentMapIndex (pMultiIndex->pIndex[0].i4_SLongValue,
                                         pMultiIndex->pIndex[1].u4_ULongValue,
                                         pMultiIndex->pIndex[2].pOidValue,
                                         pMultiData->pOctetStrValue));

}

INT4
MplsXCNotificationsEnableGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetMplsXCNotificationsEnable (&(pMultiData->i4_SLongValue)));
}

INT4
MplsXCNotificationsEnableSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetMplsXCNotificationsEnable (pMultiData->i4_SLongValue));
}

INT4
MplsXCNotificationsEnableTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2MplsXCNotificationsEnable
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
MplsXCNotificationsEnableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MplsXCNotificationsEnable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
