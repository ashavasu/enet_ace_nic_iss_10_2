/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: stdftnlw.c,v 1.44 2016/04/08 09:53:07 siva Exp $
 *
 * Description: Protocol Low Level Routines
 *********************************************************************/
# include  "mplsdbinc.h"
# include  "fssnmp.h"
# include  "stdftnlw.h"
# include  "mplcmndb.h"
# include  "mplsutil.h"
# include  "l2vpextn.h"
# include  "rtm.h"
# include  "mpls.h"
#include   "mpl3vpncmn.h"
#include "mplscli.h"


/* Low Level GET Routine for All Objects  */

UINT4               au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET] =
{ 1, 3, 6, 1, 2, 1, 10, 166, 2, 1, 10, 1, 4, 4, 0, 0, 0, 0, 4, 0, 0, 0, 0,
	4, 0, 0, 0, 0
};
UINT4               au4TnlTableOid[MPLS_FTN_TE_TABLE_DEF_OFFSET] =
{ 1, 3, 6, 1, 2, 1, 10, 166, 3, 2, 2, 1, 5, 0, 0, 0, 0 };
/****************************************************************************
Function    :  nmhetMplsFTNIndexNext
Input       :  The Indices

The Object 
retValMplsFTNIndexNext
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhGetMplsFTNIndexNext (UINT4 *pu4RetValMplsFTNIndexNext)
{

	*pu4RetValMplsFTNIndexNext = MplsFtnGetIndex ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetMplsFTNTableLastChanged
Input       :  The Indices

The Object 
retValMplsFTNTableLastChanged
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhGetMplsFTNTableLastChanged (UINT4 *pu4RetValMplsFTNTableLastChanged)
{
	*pu4RetValMplsFTNTableLastChanged = MplsFtnGetSysTime ();
	return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MplsFTNTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceMplsFTNTable
Input       :  The Indices
MplsFTNIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

	INT1
nmhValidateIndexInstanceMplsFTNTable (UINT4 u4MplsFTNIndex)
{
	if ((u4MplsFTNIndex > 0) && (u4MplsFTNIndex <= gu4MplsDbMaxFtnEntries))
	{
		return SNMP_SUCCESS;
	}
	return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhGetFirstIndexMplsFTNTable
Input       :  The Indices
MplsFTNIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

	INT1
nmhGetFirstIndexMplsFTNTable (UINT4 *pu4MplsFTNIndex)
{

	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	if ((pFtnEntry = MplsFtnTableNextEntry (0)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL, "GetFirstIndexMplsFTNTable Failed \t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	*pu4MplsFTNIndex = pFtnEntry->u4FtnIndex;
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetNextIndexMplsFTNTable
Input       :  The Indices
MplsFTNIndex
nextMplsFTNIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
	INT1
nmhGetNextIndexMplsFTNTable (UINT4 u4MplsFTNIndex, UINT4 *pu4NextMplsFTNIndex)
{

	tFtnEntry          *pFtnEntry = NULL;
	UINT4               u4FtnIndex = 0;
	MPLS_CMN_LOCK ();
	pFtnEntry = MplsFtnTableNextEntry (u4MplsFTNIndex);
	if (pFtnEntry != NULL)
	{
		*pu4NextMplsFTNIndex = pFtnEntry->u4FtnIndex;
		MPLS_CMN_UNLOCK ();
		return SNMP_SUCCESS;
	}
	else
	{
		while ((pFtnEntry = MplsFtnTableNextEntry (u4FtnIndex)) != NULL)
		{
			if (pFtnEntry->u4FtnIndex > u4MplsFTNIndex)
			{
				*pu4NextMplsFTNIndex = pFtnEntry->u4FtnIndex;
				MPLS_CMN_UNLOCK ();
				return SNMP_SUCCESS;
			}
			u4FtnIndex = pFtnEntry->u4FtnIndex;
		}
	}
	CMNDB_DBG (DEBUG_DEBUG_LEVEL, "GetNextIndexMplsFTNTable Failed \t\n");
	MPLS_CMN_UNLOCK ();
	return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetMplsFTNRowStatus
Input       :  The Indices
MplsFTNIndex

The Object 
retValMplsFTNRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhGetMplsFTNRowStatus (UINT4 u4MplsFTNIndex, INT4 *pi4RetValMplsFTNRowStatus)
{

	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	if ((pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"GetMplsFTNRowStatus Failed :No Entry Present\t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	*pi4RetValMplsFTNRowStatus = (INT4) pFtnEntry->u1RowStatus;
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetMplsFTNDescr
Input       :  The Indices
MplsFTNIndex

The Object 
retValMplsFTNDescr
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhGetMplsFTNDescr (UINT4 u4MplsFTNIndex,
		tSNMP_OCTET_STRING_TYPE * pRetValMplsFTNDescr)
{

	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	if ((pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"GetMplsFTNDescr Failed :No Entry Present\t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;

	}
	pRetValMplsFTNDescr->i4_Length = STRLEN (pFtnEntry->au1Descr);
	MEMCPY (pRetValMplsFTNDescr->pu1_OctetList, pFtnEntry->au1Descr,
			pRetValMplsFTNDescr->i4_Length);
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetMplsFTNMask
Input       :  The Indices
MplsFTNIndex

The Object 
retValMplsFTNMask
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhGetMplsFTNMask (UINT4 u4MplsFTNIndex,
		tSNMP_OCTET_STRING_TYPE * pRetValMplsFTNMask)
{

	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	if ((pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"GetMplsFTNMask Failed :No Entry Present\t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	MEMCPY (pRetValMplsFTNMask->pu1_OctetList, &pFtnEntry->u1FtnMask, 1);
	pRetValMplsFTNMask->i4_Length = sizeof (UINT1);
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetMplsFTNAddrType
Input       :  The Indices
MplsFTNIndex

The Object 
retValMplsFTNAddrType
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhGetMplsFTNAddrType (UINT4 u4MplsFTNIndex, INT4 *pi4RetValMplsFTNAddrType)
{

	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	if ((pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"GetMplsFTNAddrType Failed :No Entry Present\t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	*pi4RetValMplsFTNAddrType = (INT4) pFtnEntry->u1AddrType;
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetMplsFTNSourceAddrMin
Input       :  The Indices
MplsFTNIndex

The Object 
retValMplsFTNSourceAddrMin
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetMplsFTNSourceAddrMin (UINT4 u4MplsFTNIndex,
		tSNMP_OCTET_STRING_TYPE *
		pRetValMplsFTNSourceAddrMin)
{

	tFtnEntry          *pFtnEntry = NULL;
	UINT4               Addr = 0;
	MPLS_CMN_LOCK ();
	if ((pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"GetMplsFTNSourceAddrMin Failed :No Entry Present\t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	Addr = pFtnEntry->SrcAddrMin.u4_addr[0];
	Addr = OSIX_HTONL (Addr);
	MEMCPY (pRetValMplsFTNSourceAddrMin->pu1_OctetList, &Addr,
			IPV4_ADDR_LENGTH);
	pRetValMplsFTNSourceAddrMin->i4_Length = IPV4_ADDR_LENGTH;
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetMplsFTNSourceAddrMax
Input       :  The Indices
MplsFTNIndex

The Object 
retValMplsFTNSourceAddrMax
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetMplsFTNSourceAddrMax (UINT4 u4MplsFTNIndex,
		tSNMP_OCTET_STRING_TYPE *
		pRetValMplsFTNSourceAddrMax)
{

	tFtnEntry          *pFtnEntry = NULL;
	UINT4               Addr = 0;
	MPLS_CMN_LOCK ();
	if ((pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"GetMplsFTNSourceAddrMax Failed :No Entry Present\t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;

	}
	Addr = pFtnEntry->SrcAddrMax.u4_addr[0];
	Addr = OSIX_HTONL (Addr);
	MEMCPY (pRetValMplsFTNSourceAddrMax->pu1_OctetList, &Addr,
			IPV4_ADDR_LENGTH);
	pRetValMplsFTNSourceAddrMax->i4_Length = IPV4_ADDR_LENGTH;
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetMplsFTNDestAddrMin
Input       :  The Indices
MplsFTNIndex

The Object 
retValMplsFTNDestAddrMin
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhGetMplsFTNDestAddrMin (UINT4 u4MplsFTNIndex,
		tSNMP_OCTET_STRING_TYPE * pRetValMplsFTNDestAddrMin)
{

	tFtnEntry          *pFtnEntry = NULL;
	UINT4               Addr = 0;
	MPLS_CMN_LOCK ();
	if ((pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"GetMplsFTNDestAddrMin Failed :No Entry Present\t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}

#ifdef MPLS_IPV6_WANTED
	if (pFtnEntry->u1AddrType == MPLS_IPV6_ADDR_TYPE)
	{
		MEMCPY (pRetValMplsFTNDestAddrMin->pu1_OctetList, pFtnEntry->DestAddrMin.u1_addr,
				IPV6_ADDR_LENGTH);
		pRetValMplsFTNDestAddrMin->i4_Length = IPV6_ADDR_LENGTH;
	}
	else
	{
#endif
		Addr = pFtnEntry->DestAddrMin.u4_addr[0];
		Addr = OSIX_HTONL (Addr);
		MEMCPY (pRetValMplsFTNDestAddrMin->pu1_OctetList, &Addr, IPV4_ADDR_LENGTH);
		pRetValMplsFTNDestAddrMin->i4_Length = IPV4_ADDR_LENGTH;
#ifdef MPLS_IPV6_WANTED	
	}
#endif
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetMplsFTNDestAddrMax
Input       :  The Indices
MplsFTNIndex

The Object 
retValMplsFTNDestAddrMax
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhGetMplsFTNDestAddrMax (UINT4 u4MplsFTNIndex,
		tSNMP_OCTET_STRING_TYPE * pRetValMplsFTNDestAddrMax)
{

	tFtnEntry          *pFtnEntry = NULL;
	UINT4               Addr = 0;

	MPLS_CMN_LOCK ();

	if ((pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"GetMplsFTNDestAddrMax Failed :No Entry Present\t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}

#ifdef MPLS_IPV6_WANTED
	if (pFtnEntry->u1AddrType == MPLS_IPV6_ADDR_TYPE)
	{
		MEMCPY (pRetValMplsFTNDestAddrMax->pu1_OctetList, pFtnEntry->DestAddrMax.u1_addr,
				IPV6_ADDR_LENGTH);
		pRetValMplsFTNDestAddrMax->i4_Length = IPV6_ADDR_LENGTH;
	}
	else
	{
#endif
		Addr = pFtnEntry->DestAddrMax.u4_addr[0];
		Addr = OSIX_HTONL (Addr);
		MEMCPY (pRetValMplsFTNDestAddrMax->pu1_OctetList, &Addr, IPV4_ADDR_LENGTH);
		pRetValMplsFTNDestAddrMax->i4_Length = IPV4_ADDR_LENGTH;

#ifdef MPLS_IPV6_WANTED	
	}
#endif

	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetMplsFTNSourcePortMin
Input       :  The Indices
MplsFTNIndex

The Object 
retValMplsFTNSourcePortMin
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhGetMplsFTNSourcePortMin (UINT4 u4MplsFTNIndex,
		UINT4 *pu4RetValMplsFTNSourcePortMin)
{

	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	if ((pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"GetMplsFTNSourcePortMin Failed :No Entry Present\t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	*pu4RetValMplsFTNSourcePortMin = pFtnEntry->u4SrcPortMin;
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetMplsFTNSourcePortMax
Input       :  The Indices
MplsFTNIndex

The Object 
retValMplsFTNSourcePortMax
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhGetMplsFTNSourcePortMax (UINT4 u4MplsFTNIndex,
		UINT4 *pu4RetValMplsFTNSourcePortMax)
{

	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	if ((pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"GetMplsFTNSourcePortMax Failed :No Entry Present\t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	*pu4RetValMplsFTNSourcePortMax = pFtnEntry->u4SrcPortMax;
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetMplsFTNDestPortMin
Input       :  The Indices
MplsFTNIndex

The Object 
retValMplsFTNDestPortMin
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhGetMplsFTNDestPortMin (UINT4 u4MplsFTNIndex,
		UINT4 *pu4RetValMplsFTNDestPortMin)
{

	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	if ((pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"GetMplsFTNDestPortMin Failed :No Entry Present\t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	*pu4RetValMplsFTNDestPortMin = pFtnEntry->u4DestPortMin;
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetMplsFTNDestPortMax
Input       :  The Indices
MplsFTNIndex

The Object 
retValMplsFTNDestPortMax
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhGetMplsFTNDestPortMax (UINT4 u4MplsFTNIndex,
		UINT4 *pu4RetValMplsFTNDestPortMax)
{

	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	if ((pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"GetMplsFTNDestPortMax Failed :No Entry Present\t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	*pu4RetValMplsFTNDestPortMax = pFtnEntry->u4DestPortMax;
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetMplsFTNProtocol
Input       :  The Indices
MplsFTNIndex

The Object 
retValMplsFTNProtocol
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhGetMplsFTNProtocol (UINT4 u4MplsFTNIndex, INT4 *pi4RetValMplsFTNProtocol)
{

	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	if ((pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"GetMplsFTNProtocol Failed :No Entry Present\t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	*pi4RetValMplsFTNProtocol = pFtnEntry->i4Protocol;
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetMplsFTNDscp
Input       :  The Indices
MplsFTNIndex

The Object 
retValMplsFTNDscp
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhGetMplsFTNDscp (UINT4 u4MplsFTNIndex, INT4 *pi4RetValMplsFTNDscp)
{

	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	if ((pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"GetMplsFTNDscp Failed :No Entry Present\t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	*pi4RetValMplsFTNDscp = pFtnEntry->i4Dscp;
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetMplsFTNActionType
Input       :  The Indices
MplsFTNIndex

The Object 
retValMplsFTNActionType
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhGetMplsFTNActionType (UINT4 u4MplsFTNIndex, INT4 *pi4RetValMplsFTNActionType)
{

	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	if ((pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"GetMplsFTNActionType Failed :No Entry Present\t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	*pi4RetValMplsFTNActionType = pFtnEntry->i4ActionType;
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetMplsFTNActionPointer
Input       :  The Indices
MplsFTNIndex

The Object 
retValMplsFTNActionPointer
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhGetMplsFTNActionPointer (UINT4 u4MplsFTNIndex,
		tSNMP_OID_TYPE * pRetValMplsFTNActionPointer)
{

	tTeTnlInfo         *pTeTnlInfo = NULL;
	tFtnEntry          *pFtnEntry = NULL;
	tXcEntry           *pXcEntry = NULL;
	UINT4               u4XCIndex = 0;
	UINT4               u4InIndex = 0;
	UINT4               u4OutIndex = 0;
	UINT4               u4IngressId = 0;
	UINT4               u4EgressId = 0;
	MPLS_CMN_LOCK ();
	if ((pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"GetMplsFTNActionType Failed :No Entry Present\t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	if (pFtnEntry->pActionPtr == NULL)
	{
		MEMSET (pRetValMplsFTNActionPointer->pu4_OidList, 0,
				sizeof (UINT4) * 2);
		pRetValMplsFTNActionPointer->u4_Length = 2;
		MPLS_CMN_UNLOCK ();
		return SNMP_SUCCESS;
	}
	if (pFtnEntry->i4ActionType == REDIRECTLSP)
	{
		pXcEntry = pFtnEntry->pActionPtr;
		u4XCIndex = pXcEntry->u4Index;
		MPLS_INTEGER_TO_OID (au4XCTableOid, u4XCIndex,
				MPLS_XC_INDEX_START_OFFSET);

		if (pXcEntry->pInIndex != NULL)
		{
			u4InIndex = pXcEntry->pInIndex->u4Index;
		}
		MPLS_INTEGER_TO_OID (au4XCTableOid, u4InIndex,
				MPLS_IN_INDEX_START_OFFSET);
		if (pXcEntry->pOutIndex != NULL)
		{
			u4OutIndex = pXcEntry->pOutIndex->u4Index;
		}
		MPLS_INTEGER_TO_OID (au4XCTableOid, u4OutIndex,
				MPLS_OUT_INDEX_START_OFFSET);
		CPY_TO_OID (pRetValMplsFTNActionPointer, au4XCTableOid,
				MPLS_TE_XC_TABLE_OFFSET);
		MPLS_CMN_UNLOCK ();
		return SNMP_SUCCESS;
	}
	else if (pFtnEntry->i4ActionType == REDIRECTTUNNEL)
	{
		pTeTnlInfo = pFtnEntry->pActionPtr;
		CONVERT_TO_INTEGER ((pTeTnlInfo->TnlEgressLsrId), u4IngressId);
		CONVERT_TO_INTEGER ((pTeTnlInfo->TnlIngressLsrId), u4EgressId);
		u4IngressId = OSIX_NTOHL (u4IngressId);
		u4EgressId = OSIX_NTOHL (u4EgressId);
		au4TnlTableOid[MPLS_FTN_TE_TABLE_DEF_OFFSET - 1] = u4IngressId;
		au4TnlTableOid[MPLS_FTN_TE_TABLE_DEF_OFFSET - 2] = u4EgressId;
		au4TnlTableOid[MPLS_FTN_TE_TABLE_DEF_OFFSET - 3] =
			pTeTnlInfo->u4TnlInstance;
		au4TnlTableOid[MPLS_FTN_TE_TABLE_DEF_OFFSET - 4] =
			pTeTnlInfo->u4TnlIndex;

		CPY_TO_OID (pRetValMplsFTNActionPointer, au4TnlTableOid,
				MPLS_FTN_TE_TABLE_DEF_OFFSET);
		MPLS_CMN_UNLOCK ();
		return SNMP_SUCCESS;
	}
	CMNDB_DBG (DEBUG_DEBUG_LEVEL, "nmhGetMplsFTNActionPoin Failed\t\n");
	MPLS_CMN_UNLOCK ();
	return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetMplsFTNStorageType
Input       :  The Indices
MplsFTNIndex

The Object 
retValMplsFTNStorageType
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhGetMplsFTNStorageType (UINT4 u4MplsFTNIndex,
		INT4 *pi4RetValMplsFTNStorageType)
{

	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	if ((pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"GetMplsFTNStorageType Failed :No Entry Present\t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	*pi4RetValMplsFTNStorageType = (INT4) pFtnEntry->u1Storage;
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetMplsFTNRowStatus
Input       :  The Indices
MplsFTNIndex

The Object 
setValMplsFTNRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhSetMplsFTNRowStatus (UINT4 u4MplsFTNIndex, INT4 i4SetValMplsFTNRowStatus)
{

	tFtnEntry          *pFtnEntry = NULL;
	tXcEntry           *pXcEntry = NULL;
	tTeTnlInfo         *pTeTnlInfo = NULL;
	UINT4               u4Flag = 0;
	UINT4               u4IngressId = 0;
	UINT4               u4EgressId = 0;
        UINT4               u4DestAddrSubnet=0;
    	UINT4               u4NextHopSubnet=0;
    	UINT4               u4Prefix=0;
	tFtnMapEntry       *pFtnMapEntry = NULL;
	tFtnMapEntry       *pNextFtnMapEntry = NULL;
	tRtInfoQueryMsg     RtQuery;
	tNetIpv4RtInfo      NetIpRtInfo;
	eDirection          Direction = MPLS_DIRECTION_ANY;
	tGenU4Addr          Prefix;
        INT4                i4RetVal = 0;

#ifdef MPLS_IPV6_WANTED
	tIp6Addr			Ipv6Addr;
	tNetIpv6RtInfoQueryMsg NetIpv6RtInfoQueryMsg;
	tNetIpv6RtInfo         NetIpv6RtInfo;
	UINT1				   u1DestPrefixLen;

	MEMSET (&Ipv6Addr, 0 , sizeof (tIp6Addr));
#endif
	MEMSET(&Prefix,0,sizeof(tGenU4Addr));

	MPLS_CMN_LOCK ();
	pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex);
	switch (i4SetValMplsFTNRowStatus)
	{
		case CREATE_AND_WAIT:
			if (pFtnEntry != NULL)
			{
				CMNDB_DBG (DEBUG_DEBUG_LEVEL,
						"SetMplsFTNRowStatus Failed \t\n");
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;
			}
			if (MplsFtnSetIndex (u4MplsFTNIndex) != u4MplsFTNIndex)
			{
				CMNDB_DBG (DEBUG_DEBUG_LEVEL,
						"SetMplsFTNRowStatus Failed \t\n");
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;
			}
			pFtnEntry = MplsCreateFtnTableEntry (u4MplsFTNIndex);
			if (pFtnEntry == NULL)
			{
				CMNDB_DBG (DEBUG_DEBUG_LEVEL,
						"SetMplsFTNRowStatus Failed \t\n");
				MplsFtnRelIndex (u4MplsFTNIndex);
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;
			}
			pFtnEntry->u1RowStatus = NOT_READY;
			pFtnEntry->u4SrcPortMin = 0;
			pFtnEntry->u4SrcPortMax = 65535;
			pFtnEntry->u4DestPortMin = 0;
			pFtnEntry->u4DestPortMax = 65535;
			pFtnEntry->i4Protocol = 255;
			pFtnEntry->u1ArpResolveStatus = MPLS_ARP_RESOLVE_UNKNOWN;
			pFtnEntry->u1Storage = MPLS_STORAGE_NONVOLATILE;
			MplsFtnUpdateSysTime ();
			break;
		case ACTIVE:
			if (pFtnEntry == NULL)
			{
				CMNDB_DBG (DEBUG_DEBUG_LEVEL,
						"SetMplsFTNRowStatus Failed \t\n");
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;
			}

#ifdef MPLS_IPV6_WANTED
			if (pFtnEntry->u1AddrType == MPLS_IPV6_ADDR_TYPE)
			{
				MEMCPY (Ipv6Addr.u1_addr, pFtnEntry->DestAddrMin.u1_addr,
						IPV6_ADDR_LENGTH);
				if (MplsSignalGetFtnIpv6TableEntry (&Ipv6Addr)
						== NULL)
				{
					MplsRegisterEntrywithIpv6Trie (pFtnEntry);
				}
			}
			else
			{
#endif
				if (MplsSignalGetFtnTableEntry (pFtnEntry->DestAddrMin.
							u4_addr[0]) == NULL)
				{
					MplsRegisterEntrywithTrie (pFtnEntry);
				}
#ifdef MPLS_IPV6_WANTED
			}
#endif
			if (pFtnEntry->i4ActionType == MPLS_FTN_ON_TE_LSP)
			{
				pTeTnlInfo = pFtnEntry->pActionPtr;

				if (pTeTnlInfo != NULL)
				{
					if (pTeTnlInfo->u4TnlInstance == MPLS_ZERO)
					{
						CONVERT_TO_INTEGER (pTeTnlInfo->TnlIngressLsrId,
								u4IngressId);
						CONVERT_TO_INTEGER (pTeTnlInfo->TnlEgressLsrId,
								u4EgressId);

						u4IngressId = OSIX_NTOHL (u4IngressId);
						u4EgressId = OSIX_NTOHL (u4EgressId);

						pTeTnlInfo = TeGetTunnelInfo (pTeTnlInfo->u4TnlIndex,
								pTeTnlInfo->
								u4TnlPrimaryInstance,
								u4IngressId, u4EgressId);
						if (pTeTnlInfo == NULL)
						{
							pTeTnlInfo = pFtnEntry->pActionPtr;
						}
					}

					TeCmnExtGetDirectionFromTnl (pTeTnlInfo, &Direction);

					pXcEntry =
						MplsGetXCEntryByDirection (pTeTnlInfo->u4TnlXcIndex,
								Direction);
				}
			}
			else
			{
				pXcEntry = pFtnEntry->pActionPtr;
			}

#ifdef MPLS_IPV6_WANTED
			if (pFtnEntry->u1AddrType == MPLS_IPV6_ADDR_TYPE)
			{
				MEMSET(&NetIpv6RtInfoQueryMsg, 0, sizeof(tNetIpv6RtInfoQueryMsg));
				MEMSET(&NetIpv6RtInfo, 0, sizeof(tNetIpv6RtInfo));

				NetIpv6RtInfoQueryMsg.u4ContextId = VCM_DEFAULT_CONTEXT;
				NetIpv6RtInfoQueryMsg.u1QueryFlag = RTM6_QUERIED_FOR_NEXT_HOP;

				MEMCPY (&(NetIpv6RtInfo.Ip6Dst.u1_addr), &(pFtnEntry->DestAddrMin.u1_addr),
						IPV6_ADDR_LENGTH);

				u1DestPrefixLen = MplsGetIpv6Subnetmasklen (pFtnEntry->DestAddrMax.u1_addr);

				NetIpv6RtInfo.u1Prefixlen = u1DestPrefixLen;

				if(NetIpv6GetRoute (&NetIpv6RtInfoQueryMsg,
							&NetIpv6RtInfo)==NETIPV6_FAILURE)
				{
					pFtnEntry->u1RouteStatus = FTN_ROUTE_NOT_EXIST;
				}

			}
			else
#endif
			{	
				MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));
				MEMSET (&NetIpRtInfo, 0, sizeof (NetIpRtInfo));

				RtQuery.u4DestinationIpAddress = pFtnEntry->DestAddrMin.u4_addr[0];
				RtQuery.u4DestinationSubnetMask =
					pFtnEntry->DestAddrMax.u4_addr[0];
				RtQuery.u1QueryFlag = RTM_QUERIED_FOR_EXACT_DEST;
    
				if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_FAILURE)
				{
					pFtnEntry->u1RouteStatus = FTN_ROUTE_NOT_EXIST;
				}
                          if((pXcEntry != NULL) && (pXcEntry->pOutIndex != NULL))
                          {
                                u4Prefix=pFtnEntry->DestAddrMin.u4_addr[0];
                                if (NetIpv4IfIsOurAddress (u4Prefix) == NETIPV4_SUCCESS)
                                {
                                      CLI_SET_ERR(MPLS_CLI_SAME_DEST);
                                      MPLS_CMN_UNLOCK();
                                      return SNMP_FAILURE;
                                }
                           u4DestAddrSubnet=(pFtnEntry->DestAddrMin.u4_addr[0] & pFtnEntry->DestAddrMax.u4_addr[0] );
                           u4NextHopSubnet=(pXcEntry->pOutIndex->NHAddr.u4_addr[0] &   pFtnEntry->DestAddrMax.u4_addr[0] );
                           if ( MEMCMP(&(pFtnEntry->DestAddrMin),&(pXcEntry->pOutIndex->NHAddr),sizeof(tIpAddr))==0)
                           {
                                CLI_SET_ERR(MPLS_CLI_SAME_IP);
                                MPLS_CMN_UNLOCK();
                                return SNMP_FAILURE;
                           } 
                           else if(pFtnEntry->i4ActionType != MPLS_FTN_ON_TE_LSP )
                           {
                             if(u4DestAddrSubnet==u4NextHopSubnet)
                              {
                                CLI_SET_ERR(MPLS_CLI_SAME_SUBNET);
                                MPLS_CMN_UNLOCK();
                                return SNMP_FAILURE;
                              }
                           }
                          }        

			}

			if ((pXcEntry != NULL) &&
					(pXcEntry->pOutIndex != NULL) &&
					(pXcEntry->pOutIndex->u1RowStatus == ACTIVE) &&
					(pFtnEntry->u1RouteStatus == FTN_ROUTE_EXIST))
			{
				
                                 /*NP Add Call */
                                 i4RetVal= MplsFTNAdd (pFtnEntry);
                                 
                                 if (i4RetVal == MPLS_SUCCESS)
                                 {
                                     if (pFtnEntry->i4ActionType == MPLS_FTN_ON_NON_TE_LSP)
                                     {
                                         				
#ifdef MPLS_L2VPN_WANTED
					/* Send Event to L2VPN task. L2VPN will check this
					 * XC index in its data base and if present then it
					 * will make the Pseudowire Oper Status to lower layer Down */

					Prefix.u2AddrType=pFtnEntry->u1AddrType;
#ifdef MPLS_IPV6_WANTED
					if(Prefix.u2AddrType==MPLS_IPV6_ADDR_TYPE)
					{
						MPLS_IPV4_U4_ADDR(Prefix.Addr)=pFtnEntry->DestAddrMin.u4_addr[0];
					}
					else
#endif
					{
						MEMCPY(MPLS_IPV6_U4_ADDR(Prefix.Addr),pFtnEntry->DestAddrMin.u1_addr,
								IPV6_ADDR_LENGTH);
					}
					MplsNonTePostEventForL2Vpn (pXcEntry->u4Index,
							&Prefix,
							L2VPN_MPLS_PWVC_LSP_UP);
#endif
				
                                       }
                                }
                                else if ((i4RetVal == MPLS_FAILURE) &&
						(pFtnEntry->u1ArpResolveStatus != MPLS_ARP_RESOLVE_WAITING))
				{
					CMNDB_DBG (DEBUG_DEBUG_LEVEL,
							"SetMplsFTNRowStatus Failed \t\n");
					MPLS_CMN_UNLOCK ();
					return SNMP_FAILURE;
				}
			}
			pFtnEntry->u1RowStatus = ACTIVE;
			break;
		case NOT_IN_SERVICE:
			if (pFtnEntry == NULL)
			{
				CMNDB_DBG (DEBUG_DEBUG_LEVEL,
						"SetMplsFTNRowStatus Failed \t\n");
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;
			}
			if (pFtnEntry->u1RowStatus == ACTIVE)    /* NP Delete Call */
			{
				if (pFtnEntry->i4ActionType == MPLS_FTN_ON_TE_LSP)
				{
					pTeTnlInfo = pFtnEntry->pActionPtr;
					if (pTeTnlInfo == NULL)
					{
						CMNDB_DBG (DEBUG_DEBUG_LEVEL,
								"SetMplsFTNRowStatus Failed - "
								"Service pointer should be asscociated\t\n");
						MPLS_CMN_UNLOCK ();
						return SNMP_FAILURE;
					}

					if (pTeTnlInfo->u4TnlInstance == MPLS_ZERO)
					{
						CONVERT_TO_INTEGER (pTeTnlInfo->TnlIngressLsrId,
								u4IngressId);
						CONVERT_TO_INTEGER (pTeTnlInfo->TnlEgressLsrId,
								u4EgressId);

						u4IngressId = OSIX_NTOHL (u4IngressId);
						u4EgressId = OSIX_NTOHL (u4EgressId);

						pTeTnlInfo = TeGetTunnelInfo (pTeTnlInfo->u4TnlIndex,
								pTeTnlInfo->
								u4TnlPrimaryInstance,
								u4IngressId, u4EgressId);
						if (pTeTnlInfo == NULL)
						{
							pTeTnlInfo = pFtnEntry->pActionPtr;
						}
					}

					TeCmnExtGetDirectionFromTnl (pTeTnlInfo, &Direction);

					pXcEntry =
						MplsGetXCEntryByDirection (pTeTnlInfo->u4TnlXcIndex,
								Direction);
				}
				else
				{
					pXcEntry = pFtnEntry->pActionPtr;
				}
				if ((pXcEntry != NULL) &&
						(pXcEntry->pOutIndex != NULL) &&
						(pXcEntry->pOutIndex->u1RowStatus == ACTIVE) &&
						(pFtnEntry->u1RouteStatus == FTN_ROUTE_EXIST))
				{
					if (pFtnEntry->i4ActionType == MPLS_FTN_ON_NON_TE_LSP)
					{
#ifdef MPLS_L2VPN_WANTED
						/* Send Event to L2VPN task. L2VPN will check this
						 * XC index in its data base and if present then it
						 * will make the Pseudowire Oper Status to lower layer Down */
						Prefix.u2AddrType=pFtnEntry->u1AddrType;
#ifdef MPLS_IPV6_WANTED
						if(Prefix.u2AddrType==MPLS_IPV6_ADDR_TYPE)
						{
							MPLS_IPV4_U4_ADDR(Prefix.Addr)=pFtnEntry->DestAddrMin.u4_addr[0];
						}
						else
#endif
						{
							MEMCPY(MPLS_IPV6_U4_ADDR(Prefix.Addr),pFtnEntry->DestAddrMin.u1_addr,
									IPV6_ADDR_LENGTH);
						}
						MplsNonTePostEventForL2Vpn (pXcEntry->u4Index,
								&Prefix,
								L2VPN_MPLS_PWVC_LSP_DOWN);
#endif
#ifdef MPLS_L3VPN_WANTED
						if (pFtnEntry->u1IsLspUsedByL3VPN)
						{
							/* Give Notification only for Non-stacked LSP's */
							if (pXcEntry->mplsLabelIndex == NULL)
							{
								if (pXcEntry->pOutIndex != NULL)
								{
									L3VpnNonTeLspStatusChangeEventHandler
										(pXcEntry->pOutIndex->u4Label,
										 pFtnEntry->DestAddrMin.u4_addr[0],
										 L3VPN_MPLS_LSP_DOWN);
								}
								else
								{
									CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
											"Cannot post Event to L3VPN since outsegment "
											"is NULL for the Prefix: %d\n",
											pFtnEntry->DestAddrMin.
											u4_addr[0]);
								}
							}
						}
#endif
					}
					if (MplsFTNDel (pFtnEntry) == MPLS_FAILURE)
					{
						CMNDB_DBG (DEBUG_DEBUG_LEVEL,
								"SetMplsFTNRowStatus Failed \t\n");
						MPLS_CMN_UNLOCK ();
						return SNMP_FAILURE;
					}
				}
			}
			pFtnEntry->u1RowStatus = NOT_IN_SERVICE;
			break;
		case NOT_READY:
			if (pFtnEntry == NULL)
			{
				CMNDB_DBG (DEBUG_DEBUG_LEVEL,
						"SetMplsFTNRowStatus Failed \t\n");
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;
			}
			if (pFtnEntry->u1RowStatus == ACTIVE)    /* NP Delete Call */
			{
				if (pFtnEntry->i4ActionType == MPLS_FTN_ON_NON_TE_LSP)
				{
					pXcEntry = pFtnEntry->pActionPtr;
#ifdef MPLS_L2VPN_WANTED
					if (pXcEntry == NULL)
					{
						MPLS_CMN_UNLOCK ();
						return SNMP_FAILURE;
					}
					/* Send Event to L2VPN task. L2VPN will check this
					 * XC index in its data base and if present then it
					 * will make the Pseudowire Oper Status to lower layer Down */

					Prefix.u2AddrType=pFtnEntry->u1AddrType;
#ifdef MPLS_IPV6_WANTED
					if(Prefix.u2AddrType==MPLS_IPV6_ADDR_TYPE)
					{
						MPLS_IPV4_U4_ADDR(Prefix.Addr)=pFtnEntry->DestAddrMin.u4_addr[0];
					}
					else
#endif
					{
						MEMCPY(MPLS_IPV6_U4_ADDR(Prefix.Addr),pFtnEntry->DestAddrMin.u1_addr,
								IPV6_ADDR_LENGTH);
					}
					MplsNonTePostEventForL2Vpn (pXcEntry->u4Index,
							&Prefix,
							L2VPN_MPLS_PWVC_LSP_DOWN);
#endif
#ifdef MPLS_L3VPN_WANTED
					if (pFtnEntry->u1IsLspUsedByL3VPN)
					{
						/* Give Notification only for Non-stacked LSP's */
						if (pXcEntry->mplsLabelIndex == NULL)
						{
							if (pXcEntry->pOutIndex != NULL)
							{
								L3VpnNonTeLspStatusChangeEventHandler
									(pXcEntry->pOutIndex->u4Label,
									 pFtnEntry->DestAddrMin.u4_addr[0],
									 L3VPN_MPLS_LSP_DOWN);
							}
							else
							{
								CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
										"Cannot post Event to L3VPN since outsegment "
										"is NULL for the Prefix: %d\n",
										pFtnEntry->DestAddrMin.u4_addr[0]);
							}
						}
					}
#endif
				}
				if (MplsFTNDel (pFtnEntry) == MPLS_FAILURE)
				{
					CMNDB_DBG (DEBUG_DEBUG_LEVEL,
							"SetMplsFTNRowStatus Failed \t\n");
					MPLS_CMN_UNLOCK ();
					return SNMP_FAILURE;
				}
			}
			pFtnEntry->u1RowStatus = NOT_READY;
			break;

		case DESTROY:
			if (pFtnEntry == NULL)
			{
				CMNDB_DBG (DEBUG_DEBUG_LEVEL,
						"SetMplsFTNRowStatus Failed \t\n");
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;
			}
			if (pFtnEntry->u1RowStatus == ACTIVE)    /* NP Delete Call */
			{
				if (pFtnEntry->i4ActionType == MPLS_FTN_ON_TE_LSP)
				{
					pTeTnlInfo = pFtnEntry->pActionPtr;
					if (pTeTnlInfo == NULL)
					{
						CMNDB_DBG (DEBUG_DEBUG_LEVEL,
								"SetMplsFTNRowStatus Failed - "
								"Service pointer should be asscociated\t\n");
						MPLS_CMN_UNLOCK ();
						return SNMP_FAILURE;
					}

					if (pTeTnlInfo->u4TnlInstance == MPLS_ZERO)
					{
						CONVERT_TO_INTEGER (pTeTnlInfo->TnlIngressLsrId,
								u4IngressId);
						CONVERT_TO_INTEGER (pTeTnlInfo->TnlEgressLsrId,
								u4EgressId);

						u4IngressId = OSIX_NTOHL (u4IngressId);
						u4EgressId = OSIX_NTOHL (u4EgressId);

						pTeTnlInfo = TeGetTunnelInfo (pTeTnlInfo->u4TnlIndex,
								pTeTnlInfo->
								u4TnlPrimaryInstance,
								u4IngressId, u4EgressId);
						if (pTeTnlInfo == NULL)
						{
							pTeTnlInfo = pFtnEntry->pActionPtr;
						}
					}

					TeCmnExtGetDirectionFromTnl (pTeTnlInfo, &Direction);

					pXcEntry =
						MplsGetXCEntryByDirection (pTeTnlInfo->u4TnlXcIndex,
								Direction);
				}
				else
				{
					pXcEntry = pFtnEntry->pActionPtr;
				}
				if ((pXcEntry != NULL) &&
						(pXcEntry->pOutIndex != NULL) &&
						(pXcEntry->pOutIndex->u1RowStatus == ACTIVE) &&
						(pFtnEntry->u1RouteStatus == FTN_ROUTE_EXIST))
				{
					if (pFtnEntry->i4ActionType == MPLS_FTN_ON_NON_TE_LSP)
					{
#ifdef MPLS_L2VPN_WANTED
						/* Send Event to L2VPN task. L2VPN will check this
						 * XC index in its data base and if present then it
						 * will make the Pseudowire Oper Status to lower layer Down */
                        Prefix.u2AddrType=pFtnEntry->u1AddrType;
#ifdef MPLS_IPV6_WANTED
                        if(Prefix.u2AddrType==MPLS_IPV6_ADDR_TYPE)
                        {
                            MPLS_IPV4_U4_ADDR(Prefix.Addr)=pFtnEntry->DestAddrMin.u4_addr[0];
                        }
                        else
#endif
                        {
                            MEMCPY(MPLS_IPV6_U4_ADDR(Prefix.Addr),pFtnEntry->DestAddrMin.u1_addr,
                                    IPV6_ADDR_LENGTH);
                        }
                       

						MplsNonTePostEventForL2Vpn (pXcEntry->u4Index,
								&Prefix,
								L2VPN_MPLS_PWVC_LSP_DESTROY);

#endif

#ifdef MPLS_L3VPN_WANTED
						if (pFtnEntry->u1IsLspUsedByL3VPN)
						{
							/* Give Notification only for Non-stacked LSP's */
							if (pXcEntry->mplsLabelIndex == NULL)
							{
								if (pXcEntry->pOutIndex != NULL)
								{
									L3VpnNonTeLspStatusChangeEventHandler
										(pXcEntry->pOutIndex->u4Label,
										 pFtnEntry->DestAddrMin.u4_addr[0],
										 L3VPN_MPLS_LSP_DOWN);
								}
								else
								{

									CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
											"Cannot post Event to L3VPN since outsegment "
											"is NULL for the Prefix: %d\n",
											pFtnEntry->DestAddrMin.
											u4_addr[0]);
								}
							}
						}
#endif

					}
					if (MplsFTNDel (pFtnEntry) == MPLS_FAILURE)
					{
						CMNDB_DBG (DEBUG_DEBUG_LEVEL,
								"SetMplsFTNRowStatus Failed \t\n");
						MPLS_CMN_UNLOCK ();
						return SNMP_FAILURE;
					}
				}
			}

#ifdef MPLS_IPV6_WANTED
			if (pFtnEntry->u1AddrType == MPLS_IPV6_ADDR_TYPE)
			{
				if ((MplsDeleteFtnIpv6TableEntry (pFtnEntry)) == MPLS_FAILURE)
				{
					CMNDB_DBG (DEBUG_DEBUG_LEVEL,
							"MplsDeleteFtnIpv6TableEntry Failed \t\n");
					MPLS_CMN_UNLOCK ();
					return SNMP_FAILURE;
				}

			}
			else
			{
#endif
				if ((MplsDeleteFtnTableEntry (pFtnEntry)) == MPLS_FAILURE)
				{
					CMNDB_DBG (DEBUG_DEBUG_LEVEL,
							"MplsDeleteFtnTableEntry Failed \t\n");
					MPLS_CMN_UNLOCK ();
					return SNMP_FAILURE;
				}
#ifdef MPLS_IPV6_WANTED
			}
#endif
			pFtnMapEntry = MplsFtnMapTableNextEntry (0, 0);
			if (pFtnMapEntry != NULL)
			{
				do
				{
					if (pFtnMapEntry->u4CurIndex == u4MplsFTNIndex)
					{
						u4Flag = 1;
						break;
					}
					pNextFtnMapEntry =
						MplsFtnMapTableNextEntry (pFtnMapEntry->u4IfIndex,
								pFtnMapEntry->u4CurIndex);
					pFtnMapEntry = pNextFtnMapEntry;
				}
				while (pFtnMapEntry != NULL);
				if (u4Flag == 1)
				{
					if ((MplsDeleteFtnMapTableEntry (pFtnMapEntry)) ==
							MPLS_FAILURE)
					{
						CMNDB_DBG (DEBUG_DEBUG_LEVEL,
								"SetMplsFTNRowStatus Failed \t\n");
						MPLS_CMN_UNLOCK ();
						return SNMP_FAILURE;
					}
				}
			}
			else
			{
				CMNDB_DBG (DEBUG_DEBUG_LEVEL,
						"SetMplsFTNRowStatus :No entry present \
						in FTNMAP Table\t\n");
			}
			break;
		default:
			CMNDB_DBG (DEBUG_DEBUG_LEVEL, "SetMplsFTNRowStatus Failed \t\n");
			MPLS_CMN_UNLOCK ();
			return SNMP_FAILURE;
	}
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetMplsFTNDescr
Input       :  The Indices
MplsFTNIndex

The Object 
setValMplsFTNDescr
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhSetMplsFTNDescr (UINT4 u4MplsFTNIndex,
		tSNMP_OCTET_STRING_TYPE * pSetValMplsFTNDescr)
{

	tFtnEntry          *pFtnEntry = NULL;
	pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex);
	if (pFtnEntry == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsGetFtnTableEntry Failed \t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	MPLS_CMN_LOCK ();
	MEMCPY (pFtnEntry->au1Descr, pSetValMplsFTNDescr->pu1_OctetList,
			pSetValMplsFTNDescr->i4_Length);
	pSetValMplsFTNDescr->pu1_OctetList[pSetValMplsFTNDescr->i4_Length] = 0;
	MplsFtnUpdateSysTime ();
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetMplsFTNMask
Input       :  The Indices
MplsFTNIndex

The Object 
setValMplsFTNMask
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhSetMplsFTNMask (UINT4 u4MplsFTNIndex,
		tSNMP_OCTET_STRING_TYPE * pSetValMplsFTNMask)
{

	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex);
	if (pFtnEntry == NULL)
	{
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	pFtnEntry->u1FtnMask = (UINT1) *pSetValMplsFTNMask->pu1_OctetList;
	MplsFtnUpdateSysTime ();
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetMplsFTNAddrType
Input       :  The Indices
MplsFTNIndex

The Object 
setValMplsFTNAddrType
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhSetMplsFTNAddrType (UINT4 u4MplsFTNIndex, INT4 i4SetValMplsFTNAddrType)
{

	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex);
	if (pFtnEntry == NULL)
	{
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	pFtnEntry->u1AddrType = (UINT1) i4SetValMplsFTNAddrType;
	MplsFtnUpdateSysTime ();
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetMplsFTNSourceAddrMin
Input       :  The Indices
MplsFTNIndex

The Object 
setValMplsFTNSourceAddrMin
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetMplsFTNSourceAddrMin (UINT4 u4MplsFTNIndex,
		tSNMP_OCTET_STRING_TYPE *
		pSetValMplsFTNSourceAddrMin)
{

	tFtnEntry          *pFtnEntry = NULL;
	UINT4               Addr = 0;
	MPLS_CMN_LOCK ();
	pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex);
	if (pFtnEntry == NULL)
	{
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	MEMCPY (&Addr, pSetValMplsFTNSourceAddrMin->pu1_OctetList,
			IPV4_ADDR_LENGTH);
	pFtnEntry->SrcAddrMin.u4_addr[0] = OSIX_HTONL (Addr);
	MplsFtnUpdateSysTime ();
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetMplsFTNSourceAddrMax
Input       :  The Indices
MplsFTNIndex

The Object 
setValMplsFTNSourceAddrMax
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhSetMplsFTNSourceAddrMax (UINT4 u4MplsFTNIndex,
		tSNMP_OCTET_STRING_TYPE *
		pSetValMplsFTNSourceAddrMax)
{
	UINT4               Addr = 0;
	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex);
	if (pFtnEntry == NULL)
	{
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	MEMCPY (&Addr, pSetValMplsFTNSourceAddrMax->pu1_OctetList,
			IPV4_ADDR_LENGTH);
	pFtnEntry->SrcAddrMax.u4_addr[0] = OSIX_HTONL (Addr);
	MplsFtnUpdateSysTime ();
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetMplsFTNDestAddrMin
Input       :  The Indices
MplsFTNIndex

The Object 
setValMplsFTNDestAddrMin
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhSetMplsFTNDestAddrMin (UINT4 u4MplsFTNIndex,
		tSNMP_OCTET_STRING_TYPE * pSetValMplsFTNDestAddrMin)
{
	UINT4               Addr = 0;
	tFtnEntry          *pFtnEntry = NULL;

	MPLS_CMN_LOCK ();

	pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex);
	if (pFtnEntry == NULL)
	{
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}

#ifdef MPLS_IPV6_WANTED
	if (pSetValMplsFTNDestAddrMin->i4_Length == IPV6_ADDR_LENGTH)
	{
		MEMCPY (&(pFtnEntry->DestAddrMin.u1_addr), pSetValMplsFTNDestAddrMin->pu1_OctetList , IPV6_ADDR_LENGTH);
	}
	else
#endif
	{
		MEMCPY (&Addr, pSetValMplsFTNDestAddrMin->pu1_OctetList, IPV4_ADDR_LENGTH);
		pFtnEntry->DestAddrMin.u4_addr[0] = OSIX_HTONL (Addr);
	}

	MplsFtnUpdateSysTime ();

	MPLS_CMN_UNLOCK ();

	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetMplsFTNDestAddrMax
Input       :  The Indices
MplsFTNIndex

The Object 
setValMplsFTNDestAddrMax
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhSetMplsFTNDestAddrMax (UINT4 u4MplsFTNIndex,
		tSNMP_OCTET_STRING_TYPE * pSetValMplsFTNDestAddrMax)
{
	UINT4               Addr = 0;
	tFtnEntry          *pFtnEntry = NULL;

	MPLS_CMN_LOCK ();

	pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex);
	if (pFtnEntry == NULL)
	{
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	if (pSetValMplsFTNDestAddrMax->i4_Length == IPV4_ADDR_LENGTH)
	{
		MEMCPY (&Addr, pSetValMplsFTNDestAddrMax->pu1_OctetList, IPV4_ADDR_LENGTH);
		pFtnEntry->DestAddrMax.u4_addr[0] = OSIX_HTONL (Addr);
	}
#ifdef MPLS_IPV6_WANTED
	else
	{
		MEMCPY (pFtnEntry->DestAddrMax.u1_addr, pSetValMplsFTNDestAddrMax->pu1_OctetList,
				IPV6_ADDR_LENGTH);
	}
#endif
	MplsFtnUpdateSysTime ();
	MPLS_CMN_UNLOCK ();

	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetMplsFTNSourcePortMin
Input       :  The Indices
MplsFTNIndex

The Object 
setValMplsFTNSourcePortMin
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhSetMplsFTNSourcePortMin (UINT4 u4MplsFTNIndex,
		UINT4 u4SetValMplsFTNSourcePortMin)
{

	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex);
	if (pFtnEntry == NULL)
	{
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	pFtnEntry->u4SrcPortMin = u4SetValMplsFTNSourcePortMin;
	MplsFtnUpdateSysTime ();
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetMplsFTNSourcePortMax
Input       :  The Indices
MplsFTNIndex

The Object 
setValMplsFTNSourcePortMax
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhSetMplsFTNSourcePortMax (UINT4 u4MplsFTNIndex,
		UINT4 u4SetValMplsFTNSourcePortMax)
{

	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex);
	if (pFtnEntry == NULL)
	{
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	pFtnEntry->u4SrcPortMax = u4SetValMplsFTNSourcePortMax;
	MplsFtnUpdateSysTime ();
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetMplsFTNDestPortMin
Input       :  The Indices
MplsFTNIndex

The Object 
setValMplsFTNDestPortMin
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhSetMplsFTNDestPortMin (UINT4 u4MplsFTNIndex,
		UINT4 u4SetValMplsFTNDestPortMin)
{

	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex);
	if (pFtnEntry == NULL)
	{
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	pFtnEntry->u4DestPortMin = u4SetValMplsFTNDestPortMin;
	MplsFtnUpdateSysTime ();
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetMplsFTNDestPortMax
Input       :  The Indices
MplsFTNIndex

The Object 
setValMplsFTNDestPortMax
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhSetMplsFTNDestPortMax (UINT4 u4MplsFTNIndex,
		UINT4 u4SetValMplsFTNDestPortMax)
{

	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex);
	if (pFtnEntry == NULL)
	{
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	pFtnEntry->u4DestPortMax = u4SetValMplsFTNDestPortMax;
	MplsFtnUpdateSysTime ();
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetMplsFTNProtocol
Input       :  The Indices
MplsFTNIndex

The Object 
setValMplsFTNProtocol
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhSetMplsFTNProtocol (UINT4 u4MplsFTNIndex, INT4 i4SetValMplsFTNProtocol)
{

	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex);
	if (pFtnEntry == NULL)
	{
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	pFtnEntry->i4Protocol = i4SetValMplsFTNProtocol;
	MplsFtnUpdateSysTime ();
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetMplsFTNDscp
Input       :  The Indices
MplsFTNIndex

The Object 
setValMplsFTNDscp
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhSetMplsFTNDscp (UINT4 u4MplsFTNIndex, INT4 i4SetValMplsFTNDscp)
{

	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex);
	if (pFtnEntry == NULL)
	{
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	pFtnEntry->i4Dscp = i4SetValMplsFTNDscp;
	MplsFtnUpdateSysTime ();
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetMplsFTNActionType
Input       :  The Indices
MplsFTNIndex

The Object 
setValMplsFTNActionType
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhSetMplsFTNActionType (UINT4 u4MplsFTNIndex, INT4 i4SetValMplsFTNActionType)
{

	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex);
	if (pFtnEntry == NULL)
	{
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	pFtnEntry->i4ActionType = i4SetValMplsFTNActionType;
	MplsFtnUpdateSysTime ();
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetMplsFTNActionPointer
Input       :  The Indices
MplsFTNIndex

The Object 
setValMplsFTNActionPointer
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhSetMplsFTNActionPointer (UINT4 u4MplsFTNIndex,
		tSNMP_OID_TYPE * pSetValMplsFTNActionPointer)
{
	tFtnEntry          *pFtnEntry = NULL;
	UINT4               i4MplsTunnelIndex;
	UINT4               u4MplsTunnelInstance;
	UINT4               u4MplsTunnelIngressLSRId;
	UINT4               u4MplsTunnelEgressLSRId;
	UINT4               u4Length;
	tTeTnlInfo         *pTeTnlInfo = NULL;
	tTeTnlInfo         *pTeTmpTnlInfo = NULL;
	tXcEntry           *pXcEntry = NULL;
	UINT4               u4XcIndex = 0;

	MPLS_CMN_LOCK ();
	pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex);
	if (pFtnEntry == NULL)
	{
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	if (pFtnEntry->i4ActionType == REDIRECTLSP)
	{
		MPLS_OID_TO_INTEGER (pSetValMplsFTNActionPointer, u4XcIndex,
				MPLS_XC_INDEX_START_OFFSET);
		pXcEntry = MplsGetXCEntryByDirection (u4XcIndex, MPLS_DEF_DIRECTION);
		pFtnEntry->pActionPtr = pXcEntry;
		MPLS_CMN_UNLOCK ();
		return SNMP_SUCCESS;
	}
	else
	{
		u4Length = pSetValMplsFTNActionPointer->u4_Length;
		u4MplsTunnelEgressLSRId =
			pSetValMplsFTNActionPointer->pu4_OidList[u4Length - 1];
		u4MplsTunnelIngressLSRId =
			pSetValMplsFTNActionPointer->pu4_OidList[u4Length - 2];
		u4MplsTunnelInstance =
			pSetValMplsFTNActionPointer->pu4_OidList[u4Length - 3];
		i4MplsTunnelIndex =
			pSetValMplsFTNActionPointer->pu4_OidList[u4Length - 4];

		pTeTnlInfo = TeGetTunnelInfo (i4MplsTunnelIndex,
				u4MplsTunnelInstance,
				u4MplsTunnelIngressLSRId,
				u4MplsTunnelEgressLSRId);
		if (pTeTnlInfo != NULL)
		{
			if (pFtnEntry->pActionPtr != NULL)
			{
				/* Already this FTN might have associated with the tunnel */
				pTeTmpTnlInfo = pFtnEntry->pActionPtr;
				TMO_DLL_Delete (&(pTeTmpTnlInfo->FtnList),
						&(pFtnEntry->FtnInfoNode));
				CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\n nmhSetMplsFTNActionPointer: "
						"FTN is deleted from old Tunnel entry\n");
			}
			/* Allow only ingress tunnel or co-routed bidirectional 
			 * tunnel for FTN association */
			if ((TE_TNL_ROLE (pTeTnlInfo) == TE_INGRESS) ||
					((pTeTnlInfo->u4TnlMode
					  == TE_TNL_MODE_COROUTED_BIDIRECTIONAL) &&
					 (TE_TNL_ROLE (pTeTnlInfo) == TE_EGRESS)))
			{
				pFtnEntry->pActionPtr = pTeTnlInfo;
				TMO_DLL_Init_Node (&pFtnEntry->FtnInfoNode);
				TMO_DLL_Add (&(pTeTnlInfo->FtnList), &(pFtnEntry->FtnInfoNode));
				CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\n nmhSetMplsFTNActionPointer: "
						"FTN is added in the tunnel entry\n");
				MplsFtnUpdateSysTime ();
				MPLS_CMN_UNLOCK ();
				return SNMP_SUCCESS;
			}
		}
	}
	MPLS_CMN_UNLOCK ();
	return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhSetMplsFTNStorageType
Input       :  The Indices
MplsFTNIndex

The Object 
setValMplsFTNStorageType
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhSetMplsFTNStorageType (UINT4 u4MplsFTNIndex, INT4 i4SetValMplsFTNStorageType)
{

	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex);
	if (pFtnEntry == NULL)
	{
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}

	pFtnEntry->u1Storage = (UINT1) i4SetValMplsFTNStorageType;
	MplsFtnUpdateSysTime ();
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2MplsFTNRowStatus
Input       :  The Indices
MplsFTNIndex

The Object 
testValMplsFTNRowStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhTestv2MplsFTNRowStatus (UINT4 *pu4ErrorCode, UINT4 u4MplsFTNIndex,
		INT4 i4TestValMplsFTNRowStatus)
{

	tFtnEntry          *pFtnEntry = NULL;

#ifdef MPLS_IPV6_WANTED
	tIpAddr				Ipv6Addr;
	INT4				i4IsIpv6Zero;
#endif

#ifdef MPLS_IPV6_WANTED
	MEMSET (&Ipv6Addr, 0, sizeof (tIpAddr));
#endif
	if ((nmhValidateIndexInstanceMplsFTNTable (u4MplsFTNIndex)) == SNMP_FAILURE)
	{
		*pu4ErrorCode = SNMP_ERR_NO_CREATION;
		 return SNMP_FAILURE;
	}

	MPLS_CMN_LOCK ();
	pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex);

	switch (i4TestValMplsFTNRowStatus)
	{
		case CREATE_AND_WAIT:
			if (pFtnEntry != NULL)
			{
				*pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
				CMNDB_DBG (DEBUG_DEBUG_LEVEL,
						"Testv2MplsFTNRowStatus Failed :Entry Present :\
						Can't create\t\n");
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;
			}
			break;
		case CREATE_AND_GO:
			*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
			CMNDB_DBG (DEBUG_DEBUG_LEVEL,
					"Testv2MplsFTNRowStatus Failed :Inconsistent Value\t\n");
			MPLS_CMN_UNLOCK ();
			return SNMP_FAILURE;
		case ACTIVE:
			if (pFtnEntry == NULL)
			{
				*pu4ErrorCode = SNMP_ERR_NO_CREATION;
				CMNDB_DBG
					(DEBUG_DEBUG_LEVEL,
					 "Testv2MplsFTNRowStatus Failed :No Entry Present\t\n");
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;
			}

			if ((pFtnEntry->u1RowStatus == NOT_READY) ||
					(pFtnEntry->u1RowStatus == NOT_IN_SERVICE))
			{
				if (pFtnEntry->u1AddrType == MPLS_IPV4_ADDR_TYPE)
				{
					if ((pFtnEntry->u1FtnMask != 0)
							&& (pFtnEntry->i4ActionType != 0) &&
							(pFtnEntry->DestAddrMin.u4_addr[0] != 0))
					{
						MPLS_CMN_UNLOCK ();
						return SNMP_SUCCESS;
					}
				}
#ifdef MPLS_IPV6_WANTED
				else
				{
					i4IsIpv6Zero = MEMCMP(&(pFtnEntry->DestAddrMin), &(Ipv6Addr),
							sizeof(tIpAddr));

					if ((pFtnEntry->u1FtnMask != 0) && (pFtnEntry->u1AddrType != 0)
							&& (pFtnEntry->i4ActionType != 0) &&
							(i4IsIpv6Zero != 0))
					{
						MPLS_CMN_UNLOCK ();
						return SNMP_SUCCESS;

					}
				}
#endif
				*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
				CMNDB_DBG (DEBUG_DEBUG_LEVEL,
						"Testv2MplsFTNRowStatus Failed \t\n");
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;
			}
			*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
			CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNRowStatus Failed \t\n");
			MPLS_CMN_UNLOCK ();
			return SNMP_FAILURE;
		case NOT_IN_SERVICE:
			if (pFtnEntry == NULL)
			{
				*pu4ErrorCode = SNMP_ERR_NO_CREATION;
				CMNDB_DBG
					(DEBUG_DEBUG_LEVEL,
					 "Testv2MplsFTNRowStatus Failed :No Entry Present\t\n");
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;
			}

			if (pFtnEntry->u1RowStatus == NOT_READY)
			{
				if (pFtnEntry->u1AddrType == MPLS_IPV4_ADDR_TYPE)
				{
					if ((pFtnEntry->u1FtnMask != 0)
							&& (pFtnEntry->i4ActionType != 0) &&
							(pFtnEntry->DestAddrMin.u4_addr[0] != 0))
					{
						break;
					}
				}
#ifdef MPLS_IPV6_WANTED
				else
				{
					i4IsIpv6Zero = MEMCMP(&(pFtnEntry->DestAddrMin), &(Ipv6Addr),
							sizeof(tIpAddr));

					if ((pFtnEntry->u1FtnMask != 0) && (pFtnEntry->u1AddrType != 0)
							&& (pFtnEntry->i4ActionType != 0) &&
							(i4IsIpv6Zero != 0))
					{
						break;
					}
				}
#endif

				*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
				CMNDB_DBG (DEBUG_DEBUG_LEVEL,
						"Testv2MplsFTNRowStatus Failed \t\n");
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;
			}
			if (pFtnEntry->u1RowStatus != ACTIVE)
			{
				*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
				CMNDB_DBG (DEBUG_DEBUG_LEVEL,
						"Testv2MplsFTNRowStatus Failed \t\n");
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;
			}
			break;
		case NOT_READY:
			if (pFtnEntry == NULL)
			{
				*pu4ErrorCode = SNMP_ERR_NO_CREATION;
				CMNDB_DBG
					(DEBUG_DEBUG_LEVEL,
					 "Testv2MplsFTNRowStatus Failed :No Entry Present\t\n");
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;
			}
			if ((pFtnEntry->u1RowStatus == ACTIVE) ||
					(pFtnEntry->u1RowStatus == NOT_IN_SERVICE))
			{
				*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
				CMNDB_DBG (DEBUG_DEBUG_LEVEL,
						"Testv2MplsFTNRowStatus Failed \t\n");
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;
			}
			break;

		case DESTROY:
			if (pFtnEntry == NULL)
			{
				*pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
				CMNDB_DBG
					(DEBUG_DEBUG_LEVEL,
					 "Testv2MplsFTNRowStatus Failed :No Entry Present\t\n");
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;
			}
			break;
		default:
			*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
			CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNRowStatus Failed \t\n");
			MPLS_CMN_UNLOCK ();
			return SNMP_FAILURE;
	}
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2MplsFTNDescr
Input       :  The Indices
MplsFTNIndex

The Object 
testValMplsFTNDescr
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhTestv2MplsFTNDescr (UINT4 *pu4ErrorCode, UINT4 u4MplsFTNIndex,
		tSNMP_OCTET_STRING_TYPE * pTestValMplsFTNDescr)
{

	tFtnEntry          *pFtnEntry;
	MPLS_CMN_LOCK ();
	if ((pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"Testv2MplsFTNDescr Failed :No Entry Present\t\n");
		*pu4ErrorCode = SNMP_ERR_NO_CREATION;
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	if ((pFtnEntry->u1RowStatus == NOT_READY) ||
			(pFtnEntry->u1RowStatus == NOT_IN_SERVICE))
	{
		if ((pTestValMplsFTNDescr->i4_Length > 0) &&
				(pTestValMplsFTNDescr->i4_Length < MAX_SNMP_STRING_LENGTH))
		{
			MPLS_CMN_UNLOCK ();
			return SNMP_SUCCESS;
		}
	}
	else
	{
		*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
		CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNDescr Failed \t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	*pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
	CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNDescr Failed \t\n");
	MPLS_CMN_UNLOCK ();
	return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhTestv2MplsFTNMask
Input       :  The Indices
MplsFTNIndex

The Object 
testValMplsFTNMask
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhTestv2MplsFTNMask (UINT4 *pu4ErrorCode, UINT4 u4MplsFTNIndex,
		tSNMP_OCTET_STRING_TYPE * pTestValMplsFTNMask)
{

	tFtnEntry          *pFtnEntry;
	MPLS_CMN_LOCK ();
	if ((pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"Testv2MplsFTNMask Failed :No Entry Present\t\n");
		*pu4ErrorCode = SNMP_ERR_NO_CREATION;
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	if ((pFtnEntry->u1RowStatus == NOT_READY)
			|| (pFtnEntry->u1RowStatus == NOT_IN_SERVICE))
	{
		/*In this Code Destination Prefix mask only supported */
		if (*pTestValMplsFTNMask->pu1_OctetList == FTN_DEST_BITS_SET)
		{
			MPLS_CMN_UNLOCK ();
			return SNMP_SUCCESS;
		}
	}
	else
	{
		*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
		CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNMask Failed \t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
	CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNMask Failed \t\n");
	MPLS_CMN_UNLOCK ();
	return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhTestv2MplsFTNAddrType
Input       :  The Indices
MplsFTNIndex

The Object 
testValMplsFTNAddrType
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhTestv2MplsFTNAddrType (UINT4 *pu4ErrorCode, UINT4 u4MplsFTNIndex,
		INT4 i4TestValMplsFTNAddrType)
{
	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	if ((pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"Testv2MplsFTNAddrType Failed :No Entry Present\t\n");
		*pu4ErrorCode = SNMP_ERR_NO_CREATION;
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	if ((pFtnEntry->u1RowStatus == NOT_READY)
			|| (pFtnEntry->u1RowStatus == NOT_IN_SERVICE))
	{
		if ((i4TestValMplsFTNAddrType == CMD_ADDR_TYPE_IPV4)
#ifdef MPLS_IPV6_WANTED
				|| (i4TestValMplsFTNAddrType == CMD_ADDR_TYPE_IPV6)
#endif
		   )
		{
			MPLS_CMN_UNLOCK ();
			return SNMP_SUCCESS;
		}
	}
	else
	{
		*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
		CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNAddrType Failed \t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
	CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNAddrType Failed \t\n");
	MPLS_CMN_UNLOCK ();
	return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhTestv2MplsFTNSourceAddrMin
Input       :  The Indices
MplsFTNIndex

The Object 
testValMplsFTNSourceAddrMin
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2MplsFTNSourceAddrMin (UINT4 *pu4ErrorCode, UINT4 u4MplsFTNIndex,
		tSNMP_OCTET_STRING_TYPE *
		pTestValMplsFTNSourceAddrMin)
{

	UINT4               SourceAddrMin = 0;
	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	if ((pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"Testv2MplsFTNSourceAddrMin Failed :No Entry Present\t\n");
		*pu4ErrorCode = SNMP_ERR_NO_CREATION;
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	MEMCPY (&SourceAddrMin,
			pTestValMplsFTNSourceAddrMin->pu1_OctetList, IPV4_ADDR_LENGTH);
	SourceAddrMin = OSIX_NTOHL (SourceAddrMin);
	if (pTestValMplsFTNSourceAddrMin->i4_Length != IPV4_ADDR_LENGTH)
	{
		*pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
		CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNSourceAddrMin Failed \t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	if ((pFtnEntry->u1RowStatus == NOT_READY)
			|| (pFtnEntry->u1RowStatus == NOT_IN_SERVICE))
	{
		if (!((MPLS_IS_ADDR_CLASS_A (SourceAddrMin)) ||
					(MPLS_IS_ADDR_CLASS_B (SourceAddrMin)) ||
					(MPLS_IS_ADDR_CLASS_C (SourceAddrMin))))
		{

			*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
			CMNDB_DBG (DEBUG_DEBUG_LEVEL,
					"Testv2MplsFTNSourceAddrMin Failed \t\n");
			MPLS_CMN_UNLOCK ();
			return SNMP_FAILURE;

		}
		if ((MPLS_IS_ADDR_LOOPBACK (SourceAddrMin)) ||
				(MPLS_IS_BROADCAST_ADDR (SourceAddrMin)))
		{
			*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
			CMNDB_DBG (DEBUG_DEBUG_LEVEL,
					"Testv2MplsFTNSourceAddrMin Failed \t\n");
			MPLS_CMN_UNLOCK ();
			return SNMP_FAILURE;
		}

	}
	else
	{
		*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
		CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNSourceAddrMin Failed \t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	if (pFtnEntry->u1AddrType == CMD_ADDR_TYPE_IPV4)
	{
		MPLS_CMN_UNLOCK ();
		return SNMP_SUCCESS;
	}
	*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
	CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNSourceAddrMin Failed \t\n");
	MPLS_CMN_UNLOCK ();
	return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhTestv2MplsFTNSourceAddrMax
Input       :  The Indices
MplsFTNIndex

The Object 
testValMplsFTNSourceAddrMax
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2MplsFTNSourceAddrMax (UINT4 *pu4ErrorCode, UINT4 u4MplsFTNIndex,
		tSNMP_OCTET_STRING_TYPE *
		pTestValMplsFTNSourceAddrMax)
{

	UINT4               SourceAddrMax = 0;
	tFtnEntry          *pFtnEntry = NULL;

	MPLS_CMN_LOCK ();
	if ((pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"Testv2MplsFTNSourceAddrMax Failed :No Entry Present\t\n");
		*pu4ErrorCode = SNMP_ERR_NO_CREATION;
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}

	MEMCPY (&SourceAddrMax,
			pTestValMplsFTNSourceAddrMax->pu1_OctetList, IPV4_ADDR_LENGTH);
	SourceAddrMax = OSIX_NTOHL (SourceAddrMax);
	if (pTestValMplsFTNSourceAddrMax->i4_Length != IPV4_ADDR_LENGTH)
	{
		*pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
		CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNSourceAddrMax Failed \t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	if ((pFtnEntry->u1RowStatus == NOT_READY)
			|| (pFtnEntry->u1RowStatus == NOT_IN_SERVICE))
	{
		if (!((MPLS_IS_ADDR_CLASS_A (SourceAddrMax)) ||
					(MPLS_IS_ADDR_CLASS_B (SourceAddrMax)) ||
					(MPLS_IS_ADDR_CLASS_C (SourceAddrMax))))
		{
			*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
			CMNDB_DBG (DEBUG_DEBUG_LEVEL,
					"Testv2MplsFTNSourceAddrMax Failed \t\n");
			MPLS_CMN_UNLOCK ();
			return SNMP_FAILURE;
		}
	}
	else
	{
		*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
		CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNSourceAddrMax Failed \t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	if (pFtnEntry->u1AddrType == CMD_ADDR_TYPE_IPV4)
	{

		MPLS_CMN_UNLOCK ();
		return SNMP_SUCCESS;
	}
	*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
	CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNSourceAddrMax Failed \t\n");
	MPLS_CMN_UNLOCK ();
	return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhTestv2MplsFTNDestAddrMin
Input       :  The Indices
MplsFTNIndex

The Object 
testValMplsFTNDestAddrMin
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2MplsFTNDestAddrMin (UINT4 *pu4ErrorCode, UINT4 u4MplsFTNIndex,
		tSNMP_OCTET_STRING_TYPE *
		pTestValMplsFTNDestAddrMin)
{

	UINT4               DestAddrMin = 0;

#ifdef MPLS_IPV6_WANTED
	UINT1               u1Scope = 0;
#endif
	tFtnEntry          *pFtnEntry = NULL;

#ifdef MPLS_IPV6_WANTED
	tIp6Addr    ValIpv6Addr;
	MEMSET(&ValIpv6Addr, 0 , sizeof (tIp6Addr));
	MEMCPY (&ValIpv6Addr.u1_addr,
			pTestValMplsFTNDestAddrMin->pu1_OctetList,
			IPV6_ADDR_LENGTH);
#endif

	MPLS_CMN_LOCK ();

	if ((pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"Testv2MplsFTNDestAddrMin Failed :No Entry Present\t\n");
		*pu4ErrorCode = SNMP_ERR_NO_CREATION;
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}

#ifdef MPLS_IPV6_WANTED
	/* For IPv6 Prefix */
	if (pTestValMplsFTNDestAddrMin->i4_Length == IPV6_ADDR_LENGTH)
	{  
		if ((pFtnEntry->u1RowStatus == NOT_READY)
				|| (pFtnEntry->u1RowStatus == NOT_IN_SERVICE))	
		{
			if (MplsValidateIPv6Addr (pu4ErrorCode, 
						pTestValMplsFTNDestAddrMin) == SNMP_FAILURE)
			{
				CMNDB_DBG (DEBUG_DEBUG_LEVEL,
						"nmhTestv2MplsFTNDestAddrMin: ValidateIpv6Addr Failed \t\n");
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;		
			}
		}

		/* Get Scope of IPv6Addr  */
		u1Scope = Ip6GetAddrScope (&ValIpv6Addr);

		/* FEC cannot be Link Local Address  */
		if (u1Scope == ADDR6_SCOPE_LLOCAL)
		{
			*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
			CMNDB_DBG (DEBUG_DEBUG_LEVEL,
					"Testv2MplsFTNDestAddrMin Failed \t\n");
			MPLS_CMN_UNLOCK ();
			return SNMP_FAILURE;
		}

		if (pFtnEntry->u1AddrType == CMD_ADDR_TYPE_IPV6)
		{
			MPLS_CMN_UNLOCK ();
			return SNMP_SUCCESS;
		}
	}
	else
	{
#endif
		/* For Ipv4 Prefix */ 
		MEMCPY (&DestAddrMin, pTestValMplsFTNDestAddrMin->pu1_OctetList,
				IPV4_ADDR_LENGTH);

		DestAddrMin = OSIX_NTOHL (DestAddrMin);

		if (pTestValMplsFTNDestAddrMin->i4_Length != IPV4_ADDR_LENGTH)
		{
			*pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
			CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNDestAddrMin Failed \t\n");
			MPLS_CMN_UNLOCK ();
			return SNMP_FAILURE;
		}

		if ((pFtnEntry->u1RowStatus == NOT_READY)
				|| (pFtnEntry->u1RowStatus == NOT_IN_SERVICE))
		{
			if (DestAddrMin == 0)
			{
				*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
				CMNDB_DBG (DEBUG_DEBUG_LEVEL,
						"Testv2MplsFTNDestAddrMin Failed \t\n");
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;
			}
			else if (!((MPLS_IS_ADDR_CLASS_A (DestAddrMin)) ||
						(MPLS_IS_ADDR_CLASS_B (DestAddrMin)) ||
						(MPLS_IS_ADDR_CLASS_C (DestAddrMin))))
			{
				*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
				CMNDB_DBG (DEBUG_DEBUG_LEVEL,
						"Testv2MplsFTNDestAddrMin Failed \t\n");
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;
			}

			if ((MPLS_IS_ADDR_LOOPBACK (DestAddrMin)) ||
					(MPLS_IS_BROADCAST_ADDR (DestAddrMin)))
			{
				*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
				CMNDB_DBG (DEBUG_DEBUG_LEVEL,
						"Testv2MplsFTNDestAddrMin Failed \t\n");
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;
			}
		}
		else
		{
			*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
			CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNDestAddrMin Failed \t\n");
			MPLS_CMN_UNLOCK ();
			return SNMP_FAILURE;
		}

		if (pFtnEntry->u1AddrType == CMD_ADDR_TYPE_IPV4)
		{
			MPLS_CMN_UNLOCK ();
			return SNMP_SUCCESS;
		}
#ifdef MPLS_IPV6_WANTED
	}
#endif

	*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
	CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNDestAddrMin Failed \t\n");
	MPLS_CMN_UNLOCK ();

	return SNMP_FAILURE;
}

/****************************************************************************
Function    :  nmhTestv2MplsFTNDestAddrMax
Input       :  The Indices
MplsFTNIndex

The Object 
testValMplsFTNDestAddrMax
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhTestv2MplsFTNDestAddrMax (UINT4 *pu4ErrorCode, UINT4 u4MplsFTNIndex,
		tSNMP_OCTET_STRING_TYPE *
		pTestValMplsFTNDestAddrMax)
{

	UINT4               DestAddrMax = 0;
	tFtnEntry          *pFtnEntry = NULL;

	MPLS_CMN_LOCK ();

	if ((pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"Testv2MplsFTNDestAddrMax Failed :No Entry Present\t\n");
		*pu4ErrorCode = SNMP_ERR_NO_CREATION;
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}

	MEMCPY (&DestAddrMax, 
			pTestValMplsFTNDestAddrMax->pu1_OctetList, IPV4_ADDR_LENGTH);

	DestAddrMax = OSIX_NTOHL (DestAddrMax);
        if(DestAddrMax==0)
        {
         CLI_SET_ERR(MPLS_CLI_INVALID_SUBNET);
          CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Invalid Destination \t\n");
          *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
          MPLS_CMN_UNLOCK ();
          return SNMP_FAILURE;
        }
	if ((pTestValMplsFTNDestAddrMax->i4_Length != IPV4_ADDR_LENGTH)
#ifdef MPLS_IPV6_WANTED
			&& (pTestValMplsFTNDestAddrMax->i4_Length != IPV6_ADDR_LENGTH )
#endif
	   ) 
	{
		*pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
		CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNDestAddrMax Failed \t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}

	if ((pFtnEntry->u1RowStatus == NOT_READY)
			|| (pFtnEntry->u1RowStatus == NOT_IN_SERVICE))
	{
		/* TODO Currently passing IP mask as the max addr
		   if (!((IS_CLASS_A_ADDR (DestAddrMax)) ||
		   (IS_CLASS_B_ADDR (DestAddrMax)) ||
		   (IS_CLASS_C_ADDR (DestAddrMax))))
		   {
		 *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
		 CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNDestAddrMax Failed \t\n");
		 MPLS_CMN_UNLOCK ();
		 return SNMP_FAILURE;
		 }
		 */
	}
	else
	{
		*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
		CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNDestAddrMax Failed \t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}

	if (pFtnEntry->u1AddrType == CMD_ADDR_TYPE_IPV4)
	{
		MPLS_CMN_UNLOCK ();
		return SNMP_SUCCESS;
	}

#ifdef MPLS_IPV6_WANTED    
	if (pFtnEntry->u1AddrType == CMD_ADDR_TYPE_IPV6)
	{
		MPLS_CMN_UNLOCK ();
		return SNMP_SUCCESS;
	}
#endif

	*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
	CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNDestAddrMax Failed \t\n");
	MPLS_CMN_UNLOCK ();
	return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhTestv2MplsFTNSourcePortMin
Input       :  The Indices
MplsFTNIndex

The Object 
testValMplsFTNSourcePortMin
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhTestv2MplsFTNSourcePortMin (UINT4 *pu4ErrorCode, UINT4 u4MplsFTNIndex,
		UINT4 u4TestValMplsFTNSourcePortMin)
{

	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	if ((pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"Testv2MplsFTNSourcePortMin Failed :No Entry Present\t\n");
		*pu4ErrorCode = SNMP_ERR_NO_CREATION;
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	if ((pFtnEntry->u1RowStatus == NOT_READY)
			|| (pFtnEntry->u1RowStatus == NOT_IN_SERVICE))
	{
		if ((u4TestValMplsFTNSourcePortMin > 0)
				&& (u4TestValMplsFTNSourcePortMin <= 65535))
		{
			MPLS_CMN_UNLOCK ();
			return SNMP_SUCCESS;
		}
	}
	else
	{
		*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"nmhTestv2MplsFTNSourcePortMin Failed \t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;

	}
	*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
	CMNDB_DBG (DEBUG_DEBUG_LEVEL, "nmhTestv2MplsFTNSourcePortMin Failed \t\n");
	MPLS_CMN_UNLOCK ();
	return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhTestv2MplsFTNSourcePortMax
Input       :  The Indices
MplsFTNIndex

The Object 
testValMplsFTNSourcePortMax
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhTestv2MplsFTNSourcePortMax (UINT4 *pu4ErrorCode, UINT4 u4MplsFTNIndex,
		UINT4 u4TestValMplsFTNSourcePortMax)
{

	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	if ((pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"Testv2MplsFTNSourcePortMax Failed:No Entry Present\t\n");
		*pu4ErrorCode = SNMP_ERR_NO_CREATION;
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	if ((pFtnEntry->u1RowStatus == NOT_READY)
			|| (pFtnEntry->u1RowStatus == NOT_IN_SERVICE))
	{
		if ((u4TestValMplsFTNSourcePortMax > 0)
				&& (u4TestValMplsFTNSourcePortMax <= 65535))
		{
			MPLS_CMN_UNLOCK ();
			return SNMP_SUCCESS;
		}
	}
	else
	{
		*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
		CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNSourcePortMax Failed \t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;

	}
	*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
	CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNSourcePortMax Failed \t\n");
	MPLS_CMN_UNLOCK ();
	return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhTestv2MplsFTNDestPortMin
Input       :  The Indices
MplsFTNIndex

The Object 
testValMplsFTNDestPortMin
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhTestv2MplsFTNDestPortMin (UINT4 *pu4ErrorCode, UINT4 u4MplsFTNIndex,
		UINT4 u4TestValMplsFTNDestPortMin)
{

	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	if ((pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"Testv2MplsFTNDestPortMin Failed :No Entry Present\t\n");
		*pu4ErrorCode = SNMP_ERR_NO_CREATION;
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	if ((pFtnEntry->u1RowStatus == NOT_READY)
			|| (pFtnEntry->u1RowStatus == NOT_IN_SERVICE))
	{
		if ((u4TestValMplsFTNDestPortMin > 0)
				&& (u4TestValMplsFTNDestPortMin <= 65535))
		{
			MPLS_CMN_UNLOCK ();
			return SNMP_SUCCESS;
		}
	}
	else
	{
		*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
		CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNDestPortMin Failed \t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;

	}
	*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
	CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNDestPortMin Failed \t\n");
	MPLS_CMN_UNLOCK ();
	return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhTestv2MplsFTNDestPortMax
Input       :  The Indices
MplsFTNIndex

The Object 
testValMplsFTNDestPortMax
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhTestv2MplsFTNDestPortMax (UINT4 *pu4ErrorCode, UINT4 u4MplsFTNIndex,
		UINT4 u4TestValMplsFTNDestPortMax)
{

	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	if ((pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"Testv2MplsFTNDestPortMax Failed :No Entry Present \t\n");
		*pu4ErrorCode = SNMP_ERR_NO_CREATION;
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	if ((pFtnEntry->u1RowStatus == NOT_READY)
			|| (pFtnEntry->u1RowStatus == NOT_IN_SERVICE))
	{
		if ((u4TestValMplsFTNDestPortMax > 0)
				&& (u4TestValMplsFTNDestPortMax <= 65535))
		{
			MPLS_CMN_UNLOCK ();
			return SNMP_SUCCESS;
		}
	}
	else
	{
		*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
		CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNDestPortMax Failed \t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
	CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNDestPortMax Failed \t\n");
	MPLS_CMN_UNLOCK ();
	return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhTestv2MplsFTNProtocol
Input       :  The Indices
MplsFTNIndex

The Object 
testValMplsFTNProtocol
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhTestv2MplsFTNProtocol (UINT4 *pu4ErrorCode, UINT4 u4MplsFTNIndex,
		INT4 i4TestValMplsFTNProtocol)
{

	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	if ((pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"Testv2MplsFTNProtocol Failed :No Entry Present\t\n");
		*pu4ErrorCode = SNMP_ERR_NO_CREATION;
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	if ((pFtnEntry->u1RowStatus == NOT_READY)
			|| (pFtnEntry->u1RowStatus == NOT_IN_SERVICE))
	{
		if ((i4TestValMplsFTNProtocol >= 0)
				&& (i4TestValMplsFTNProtocol <= 255))
		{
			MPLS_CMN_UNLOCK ();
			return SNMP_SUCCESS;
		}
	}
	else
	{
		*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
		CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNProtocol Failed \t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
	CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNProtocol Failed \t\n");
	MPLS_CMN_UNLOCK ();
	return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhTestv2MplsFTNDscp
Input       :  The Indices
MplsFTNIndex

The Object 
testValMplsFTNDscp
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhTestv2MplsFTNDscp (UINT4 *pu4ErrorCode, UINT4 u4MplsFTNIndex,
		INT4 i4TestValMplsFTNDscp)
{

	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	if ((pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"Testv2MplsFTNDscp Failed :No Entry Present\t\n");
		*pu4ErrorCode = SNMP_ERR_NO_CREATION;
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	if ((pFtnEntry->u1RowStatus == NOT_READY)
			|| (pFtnEntry->u1RowStatus == NOT_IN_SERVICE))
	{
		if ((i4TestValMplsFTNDscp > 0) && (i4TestValMplsFTNDscp <= 7))
		{
			MPLS_CMN_UNLOCK ();
			return SNMP_SUCCESS;
		}
	}
	else
	{
		*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
		CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNDscp Failed \t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
	CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNDscp Failed \t\n");
	MPLS_CMN_UNLOCK ();
	return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhTestv2MplsFTNActionType
Input       :  The Indices
MplsFTNIndex

The Object 
testValMplsFTNActionType
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhTestv2MplsFTNActionType (UINT4 *pu4ErrorCode, UINT4 u4MplsFTNIndex,
		INT4 i4TestValMplsFTNActionType)
{

	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	if ((pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"Testv2MplsFTNActionType Failed :No Entry Present\t\n");
		*pu4ErrorCode = SNMP_ERR_NO_CREATION;
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	if ((pFtnEntry->u1RowStatus == NOT_READY)
			|| (pFtnEntry->u1RowStatus == NOT_IN_SERVICE))
	{
		if ((i4TestValMplsFTNActionType == REDIRECTLSP) ||
				(i4TestValMplsFTNActionType == REDIRECTTUNNEL))
		{
			MPLS_CMN_UNLOCK ();
			return SNMP_SUCCESS;
		}
	}
	else
	{
		*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
		CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNActionType Failed \t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
	CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNActionType Failed \t\n");
	MPLS_CMN_UNLOCK ();
	return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhTestv2MplsFTNActionPointer
Input       :  The Indices
MplsFTNIndex

The Object 
testValMplsFTNActionPointer
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhTestv2MplsFTNActionPointer (UINT4 *pu4ErrorCode, UINT4 u4MplsFTNIndex,
		tSNMP_OID_TYPE * pTestValMplsFTNActionPointer)
{

	tFtnEntry          *pFtnEntry = NULL;
	tXcEntry           *pXcEntry = NULL;
	UINT4               u4XcIndex = 0;
	UINT4               i4MplsTunnelIndex = 0;
	UINT4               u4MplsTunnelInstance = 0;
	UINT4               u4MplsTunnelIngressLSRId = 0;
	UINT4               u4MplsTunnelEgressLSRId = 0;
	tTeTnlInfo         *pTeTnlInfo = NULL;
	UINT4               u4Length;

	MPLS_CMN_LOCK ();
	if ((pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"Testv2MplsFTNActionPointer Failed :No Entry Present\t\n");
		*pu4ErrorCode = SNMP_ERR_NO_CREATION;
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	if ((pFtnEntry->u1RowStatus == NOT_READY)
			|| (pFtnEntry->u1RowStatus == NOT_IN_SERVICE))
	{
		if (pFtnEntry->i4ActionType == REDIRECTLSP)
		{
			if (pTestValMplsFTNActionPointer->u4_Length !=
					MPLS_TE_XC_TABLE_OFFSET)
			{
				*pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;
			}
			MPLS_OID_TO_INTEGER (pTestValMplsFTNActionPointer, u4XcIndex,
					MPLS_XC_INDEX_START_OFFSET);
			pXcEntry =
				MplsGetXCEntryByDirection (u4XcIndex, MPLS_DEF_DIRECTION);
			if (pXcEntry != NULL)
			{
				*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
				MPLS_CMN_UNLOCK ();
				return SNMP_SUCCESS;
			}
		}
		else if (pFtnEntry->i4ActionType == REDIRECTTUNNEL)
		{
			if (pTestValMplsFTNActionPointer->u4_Length !=
					MPLS_FTN_TE_TABLE_DEF_OFFSET)
			{
				*pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;
			}
			u4Length = pTestValMplsFTNActionPointer->u4_Length;
			u4MplsTunnelEgressLSRId = pTestValMplsFTNActionPointer->
				pu4_OidList[u4Length - 1];
			u4MplsTunnelIngressLSRId = pTestValMplsFTNActionPointer->
				pu4_OidList[u4Length - 2];
			u4MplsTunnelInstance = pTestValMplsFTNActionPointer->
				pu4_OidList[u4Length - 3];
			i4MplsTunnelIndex = pTestValMplsFTNActionPointer->
				pu4_OidList[u4Length - 4];
			pTeTnlInfo = TeGetTunnelInfo (i4MplsTunnelIndex,
					u4MplsTunnelInstance,
					u4MplsTunnelIngressLSRId,
					u4MplsTunnelEgressLSRId);
			if (pTeTnlInfo != NULL)
			{
				/* FTN cannot be associated with the S-LSP tunnel when it stitched
				 * to another tunnel.*/
				if (pTeTnlInfo->u4TnlType & TE_TNL_TYPE_SLSP)
				{
					if (pTeTnlInfo->pMapTnlInfo != NULL)
					{
						*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
						MPLS_CMN_UNLOCK ();
						return SNMP_FAILURE;
					}
				}
				MPLS_CMN_UNLOCK ();
				return SNMP_SUCCESS;
			}
		}
	}
	else
	{
		*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
		CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNActionPointer Failed \t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
	CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNActionPointer Failed \t\n");
	MPLS_CMN_UNLOCK ();
	return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhTestv2MplsFTNStorageType
Input       :  The Indices
MplsFTNIndex

The Object 
testValMplsFTNStorageType
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhTestv2MplsFTNStorageType (UINT4 *pu4ErrorCode, UINT4 u4MplsFTNIndex,
		INT4 i4TestValMplsFTNStorageType)
{

	tFtnEntry          *pFtnEntry = NULL;
	MPLS_CMN_LOCK ();
	if ((pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"Testv2MplsFTNStorageType Failed :No Entry Present\t\n");
		*pu4ErrorCode = SNMP_ERR_NO_CREATION;
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	if ((pFtnEntry->u1RowStatus == NOT_READY)
			|| (pFtnEntry->u1RowStatus == NOT_IN_SERVICE))
	{                            /*suppoting only volatile and non-volatile datatype */
		switch (i4TestValMplsFTNStorageType)
		{
			case CMD_STORAGE_VOLATILE:
			case CMD_STORAGE_NONVOLATILE:
				break;
			case CMD_STORAGE_OTHER:
			case CMD_STORAGE_PERMANENT:
			case CMD_STORAGE_READONLY:
				*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
				CMNDB_DBG (DEBUG_DEBUG_LEVEL,
						"Testv2MplsFTNStorageType Failed \t\n");
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;
			default:
				*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
				CMNDB_DBG (DEBUG_DEBUG_LEVEL,
						"Testv2MplsFTNStorageType Failed \t\n");
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;
		}
	}
	else
	{
		*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
		CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Testv2MplsFTNStorageType Failed \t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2MplsFTNTable
Input       :  The Indices
MplsFTNIndex
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhDepv2MplsFTNTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
		tSNMP_VAR_BIND * pSnmpVarBind)
{
	UNUSED_PARAM (pu4ErrorCode);
	UNUSED_PARAM (pSnmpIndexList);
	UNUSED_PARAM (pSnmpVarBind);
	return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetMplsFTNMapTableLastChanged
Input       :  The Indices

The Object 
retValMplsFTNMapTableLastChanged
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhGetMplsFTNMapTableLastChanged (UINT4 *pu4RetValMplsFTNMapTableLastChanged)
{
	*pu4RetValMplsFTNMapTableLastChanged = MplsFtnMapGetSysTime ();
	return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MplsFTNMapTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceMplsFTNMapTable
Input       :  The Indices
MplsFTNMapIndex
MplsFTNMapPrevIndex
MplsFTNMapCurrIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

	INT1
nmhValidateIndexInstanceMplsFTNMapTable (INT4 i4MplsFTNMapIndex,
		UINT4 u4MplsFTNMapPrevIndex,
		UINT4 u4MplsFTNMapCurrIndex)
{
	UNUSED_PARAM (u4MplsFTNMapPrevIndex);
	if ((i4MplsFTNMapIndex > 0) && (u4MplsFTNMapCurrIndex > 0))
	{
		return SNMP_SUCCESS;
	}
	CMNDB_DBG (DEBUG_DEBUG_LEVEL,
			"ValidateIndexInstanceMplsFTNMapTable Failed \t\n");
	return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetFirstIndexMplsFTNMapTable
Input       :  The Indices
MplsFTNMapIndex
MplsFTNMapPrevIndex
MplsFTNMapCurrIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

	INT1
nmhGetFirstIndexMplsFTNMapTable (INT4 *pi4MplsFTNMapIndex,
		UINT4 *pu4MplsFTNMapPrevIndex,
		UINT4 *pu4MplsFTNMapCurrIndex)
{

	tFtnMapEntry       *pFtnMapEntry = NULL;
	MPLS_CMN_LOCK ();
	if ((pFtnMapEntry = MplsFtnMapTableNextEntry (0, 0)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"GetFirstIndexMplsFTNMapTable Failed \t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	*pi4MplsFTNMapIndex = (INT4)pFtnMapEntry->u4IfIndex;
	*pu4MplsFTNMapPrevIndex = pFtnMapEntry->u4PrevIndex;
	*pu4MplsFTNMapCurrIndex = pFtnMapEntry->u4CurIndex;
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetNextIndexMplsFTNMapTable
Input       :  The Indices
MplsFTNMapIndex
nextMplsFTNMapIndex
MplsFTNMapPrevIndex
nextMplsFTNMapPrevIndex
MplsFTNMapCurrIndex
nextMplsFTNMapCurrIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
	INT1
nmhGetNextIndexMplsFTNMapTable (INT4 i4MplsFTNMapIndex,
		INT4 *pi4NextMplsFTNMapIndex,
		UINT4 u4MplsFTNMapPrevIndex,
		UINT4 *pu4NextMplsFTNMapPrevIndex,
		UINT4 u4MplsFTNMapCurrIndex,
		UINT4 *pu4NextMplsFTNMapCurrIndex)
{
	tFtnMapEntry       *pFtnMapEntry = NULL;
	UINT4               u4FtnMapIndex = 0, u4FtnCurIndex = 0;
	UNUSED_PARAM (u4MplsFTNMapPrevIndex);
	MPLS_CMN_LOCK ();

	pFtnMapEntry =
		MplsFtnMapTableNextEntry ((UINT4)i4MplsFTNMapIndex, u4MplsFTNMapCurrIndex);
	if (pFtnMapEntry != NULL)
	{
		*pi4NextMplsFTNMapIndex = (INT4)pFtnMapEntry->u4IfIndex;
		*pu4NextMplsFTNMapPrevIndex = pFtnMapEntry->u4PrevIndex;
		*pu4NextMplsFTNMapCurrIndex = pFtnMapEntry->u4CurIndex;
		MPLS_CMN_UNLOCK ();
		return SNMP_SUCCESS;
	}
	while ((pFtnMapEntry = MplsFtnMapTableNextEntry (u4FtnMapIndex,
					u4FtnCurIndex)) != NULL)
	{
		if (pFtnMapEntry->u4IfIndex == (UINT4) i4MplsFTNMapIndex)
		{
			if (pFtnMapEntry->u4CurIndex > u4MplsFTNMapCurrIndex)
			{
				*pi4NextMplsFTNMapIndex = (INT4)pFtnMapEntry->u4IfIndex;
				*pu4NextMplsFTNMapPrevIndex = pFtnMapEntry->u4PrevIndex;
				*pu4NextMplsFTNMapCurrIndex = pFtnMapEntry->u4CurIndex;
				MPLS_CMN_UNLOCK ();
				return SNMP_SUCCESS;
			}
		}
		else if (pFtnMapEntry->u4IfIndex > (UINT4) i4MplsFTNMapIndex)
		{
			*pi4NextMplsFTNMapIndex = (INT4)pFtnMapEntry->u4IfIndex;
			*pu4NextMplsFTNMapPrevIndex = pFtnMapEntry->u4PrevIndex;
			*pu4NextMplsFTNMapCurrIndex = pFtnMapEntry->u4CurIndex;
			MPLS_CMN_UNLOCK ();
			return SNMP_SUCCESS;
		}
		u4FtnMapIndex = pFtnMapEntry->u4IfIndex;
		u4FtnCurIndex = pFtnMapEntry->u4CurIndex;
	}
	MPLS_CMN_UNLOCK ();
	return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetMplsFTNMapRowStatus
Input       :  The Indices
MplsFTNMapIndex
MplsFTNMapPrevIndex
MplsFTNMapCurrIndex

The Object 
retValMplsFTNMapRowStatus
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhGetMplsFTNMapRowStatus (INT4 i4MplsFTNMapIndex, UINT4 u4MplsFTNMapPrevIndex,
		UINT4 u4MplsFTNMapCurrIndex,
		INT4 *pi4RetValMplsFTNMapRowStatus)
{

	tFtnMapEntry       *pFtnMapEntry = NULL;
	MPLS_CMN_LOCK ();
	UNUSED_PARAM (u4MplsFTNMapPrevIndex);
	if ((pFtnMapEntry =
				MplsGetFtnMapTableEntry ((UINT4)i4MplsFTNMapIndex,
					u4MplsFTNMapCurrIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL, "GetMplsFTNMapRowStatus Failed \t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	*pi4RetValMplsFTNMapRowStatus = (INT4) pFtnMapEntry->u1RowStatus;
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetMplsFTNMapStorageType
Input       :  The Indices
MplsFTNMapIndex
MplsFTNMapPrevIndex
MplsFTNMapCurrIndex

The Object 
retValMplsFTNMapStorageType
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhGetMplsFTNMapStorageType (INT4 i4MplsFTNMapIndex,
		UINT4 u4MplsFTNMapPrevIndex,
		UINT4 u4MplsFTNMapCurrIndex,
		INT4 *pi4RetValMplsFTNMapStorageType)
{

	tFtnMapEntry       *pFtnMapEntry = NULL;
	MPLS_CMN_LOCK ();
	UNUSED_PARAM (u4MplsFTNMapPrevIndex);
	if ((pFtnMapEntry =
				MplsGetFtnMapTableEntry ((UINT4)i4MplsFTNMapIndex,
					u4MplsFTNMapCurrIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL, "GetMplsFTNMapStorageType Failed \t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	*pi4RetValMplsFTNMapStorageType = (INT4) pFtnMapEntry->u1Storage;
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
Function    :  nmhSetMplsFTNMapRowStatus
Input       :  The Indices
MplsFTNMapIndex
MplsFTNMapPrevIndex
MplsFTNMapCurrIndex

The Object 
setValMplsFTNMapRowStatus
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhSetMplsFTNMapRowStatus (INT4 i4MplsFTNMapIndex,
		UINT4 u4MplsFTNMapPrevIndex,
		UINT4 u4MplsFTNMapCurrIndex,
		INT4 i4SetValMplsFTNMapRowStatus)
{

	tFtnMapEntry       *pFtnMapEntry = NULL;
	tFtnEntry          *pFtnEntry = NULL;
	UNUSED_PARAM (u4MplsFTNMapPrevIndex);
	MPLS_CMN_LOCK ();
	pFtnMapEntry =
		MplsGetFtnMapTableEntry ((UINT4)i4MplsFTNMapIndex, u4MplsFTNMapCurrIndex);
	pFtnEntry = MplsGetFtnTableEntry (u4MplsFTNMapCurrIndex);
	switch (i4SetValMplsFTNMapRowStatus)
	{
		case CREATE_AND_GO:
			if (pFtnMapEntry != NULL)
			{
				CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Entry Exists\t\n");
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;
			}
			if (pFtnEntry == NULL)
			{
				CMNDB_DBG (DEBUG_DEBUG_LEVEL, "SetMplsFTNMapRowStatus Failed :\
						No entry in FTN Table \t\n");
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;

			}
			pFtnMapEntry = MplsCreateFtnMapTableEntry ((UINT4)i4MplsFTNMapIndex,
					u4MplsFTNMapCurrIndex);
			if (pFtnMapEntry == NULL)
			{
				CMNDB_DBG (DEBUG_DEBUG_LEVEL,
						"SetMplsFTNMapRowStatus Failed \t\n");
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;
			}
			MplsFtnMapUpdateSysTime ();
			pFtnMapEntry->u1RowStatus = ACTIVE;
			break;
		case ACTIVE:
			if (pFtnMapEntry == NULL)
			{
				CMNDB_DBG (DEBUG_DEBUG_LEVEL,
						"SetMplsFTNMapRowStatus Failed \t\n");
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;
			}
			pFtnMapEntry->u1RowStatus = ACTIVE;
			break;
		case DESTROY:
			if (pFtnMapEntry == NULL)
			{
				CMNDB_DBG (DEBUG_DEBUG_LEVEL,
						"SetMplsFTNMapRowStatus Failed \t\n");
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;
			}
			if ((MplsDeleteFtnMapTableEntry (pFtnMapEntry)) == MPLS_FAILURE)
			{
				CMNDB_DBG (DEBUG_DEBUG_LEVEL,
						"SetMplsFTNMapRowStatus Failed \t\n");
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;
			}
			break;
		default:
			CMNDB_DBG (DEBUG_DEBUG_LEVEL, "SetMplsFTNMapRowStatus Failed \t\n");
			MPLS_CMN_UNLOCK ();
			return SNMP_FAILURE;
	}
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhSetMplsFTNMapStorageType
Input       :  The Indices
MplsFTNMapIndex
MplsFTNMapPrevIndex
MplsFTNMapCurrIndex

The Object 
setValMplsFTNMapStorageType
Output      :  The Set Low Lev Routine Take the Indices &
Sets the Value accordingly.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhSetMplsFTNMapStorageType (INT4 i4MplsFTNMapIndex,
		UINT4 u4MplsFTNMapPrevIndex,
		UINT4 u4MplsFTNMapCurrIndex,
		INT4 i4SetValMplsFTNMapStorageType)
{

	tFtnMapEntry       *pFtnMapEntry = NULL;
	MPLS_CMN_LOCK ();
	UNUSED_PARAM (u4MplsFTNMapPrevIndex);
	pFtnMapEntry =
		MplsGetFtnMapTableEntry ((UINT4)i4MplsFTNMapIndex, u4MplsFTNMapCurrIndex);
	if (pFtnMapEntry == NULL)
	{
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}

	pFtnMapEntry->u1Storage = (UINT1) i4SetValMplsFTNMapStorageType;
	MplsFtnMapUpdateSysTime ();
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
Function    :  nmhTestv2MplsFTNMapRowStatus
Input       :  The Indices
MplsFTNMapIndex
MplsFTNMapPrevIndex
MplsFTNMapCurrIndex

The Object 
testValMplsFTNMapRowStatus
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhTestv2MplsFTNMapRowStatus (UINT4 *pu4ErrorCode, INT4 i4MplsFTNMapIndex,
		UINT4 u4MplsFTNMapPrevIndex,
		UINT4 u4MplsFTNMapCurrIndex,
		INT4 i4TestValMplsFTNMapRowStatus)
{

	tFtnMapEntry       *pFtnMapEntry = NULL;

	if ((nmhValidateIndexInstanceMplsFTNMapTable
				(i4MplsFTNMapIndex, u4MplsFTNMapPrevIndex,
				 u4MplsFTNMapCurrIndex)) == SNMP_FAILURE)
	{
		*pu4ErrorCode = SNMP_ERR_NO_CREATION;
		return SNMP_FAILURE;
	}

	MPLS_CMN_LOCK ();
	UNUSED_PARAM (u4MplsFTNMapPrevIndex);
	pFtnMapEntry =
		MplsGetFtnMapTableEntry ((UINT4)i4MplsFTNMapIndex, u4MplsFTNMapCurrIndex);
	switch (i4TestValMplsFTNMapRowStatus)
	{
		case CREATE_AND_WAIT:
			*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
			CMNDB_DBG (DEBUG_DEBUG_LEVEL,
					"Testv2MplsFTNMapRowStatus Failed \t\n");
			MPLS_CMN_UNLOCK ();
			return SNMP_FAILURE;

		case CREATE_AND_GO:
			if (pFtnMapEntry != NULL)
			{
				*pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
				CMNDB_DBG (DEBUG_DEBUG_LEVEL,
						"Testv2MplsFTNMapRowStatus Failed \t\n");
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;
			}
			break;
		case ACTIVE:
			if (pFtnMapEntry == NULL)
			{
				*pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
				CMNDB_DBG (DEBUG_DEBUG_LEVEL,
						"Testv2MplsFTNMapRowStatus Failed \t\n");
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;
			}
			break;
		case NOT_READY:
			*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
			CMNDB_DBG (DEBUG_DEBUG_LEVEL,
					"Testv2MplsFTNMapRowStatus Failed \t\n");
			MPLS_CMN_UNLOCK ();
			return SNMP_FAILURE;

		case NOT_IN_SERVICE:
			*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
			CMNDB_DBG (DEBUG_DEBUG_LEVEL,
					"Testv2MplsFTNMapRowStatus Failed \t\n");
			MPLS_CMN_UNLOCK ();
			return SNMP_FAILURE;

		case DESTROY:
			if (pFtnMapEntry == NULL)
			{
				*pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
				CMNDB_DBG (DEBUG_DEBUG_LEVEL,
						"Testv2MplsFTNMapRowStatus Failed \t\n");
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;
			}
			break;
		default:
			*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
			CMNDB_DBG (DEBUG_DEBUG_LEVEL,
					"Testv2MplsFTNMapRowStatus Failed \t\n");
			MPLS_CMN_UNLOCK ();
			return SNMP_FAILURE;
	}
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;
}

/****************************************************************************
Function    :  nmhTestv2MplsFTNMapStorageType
Input       :  The Indices
MplsFTNMapIndex
MplsFTNMapPrevIndex
MplsFTNMapCurrIndex

The Object 
testValMplsFTNMapStorageType
Output      :  The Test Low Lev Routine Take the Indices &
Test whether that Value is Valid Input for Set.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhTestv2MplsFTNMapStorageType (UINT4 *pu4ErrorCode, INT4 i4MplsFTNMapIndex,
		UINT4 u4MplsFTNMapPrevIndex,
		UINT4 u4MplsFTNMapCurrIndex,
		INT4 i4TestValMplsFTNMapStorageType)
{

	tFtnMapEntry       *pFtnMapEntry = NULL;
	MPLS_CMN_LOCK ();
	UNUSED_PARAM (u4MplsFTNMapPrevIndex);
	if ((pFtnMapEntry =
				MplsGetFtnMapTableEntry ((UINT4)i4MplsFTNMapIndex,
					u4MplsFTNMapCurrIndex)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"Testv2MplsFTNMapStorageType Failed \t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	CMNDB_DBG1
		(DEBUG_DEBUG_LEVEL,
		 "nmhTestv2MplsFTNMapStorageType: FTN map table interface index is %d",
		 pFtnMapEntry->u4IfIndex);
	switch (i4TestValMplsFTNMapStorageType)
	{
		case CMD_STORAGE_VOLATILE:
		case CMD_STORAGE_NONVOLATILE:
			break;
		case CMD_STORAGE_OTHER:
		case CMD_STORAGE_PERMANENT:
		case CMD_STORAGE_READONLY:
			*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
			CMNDB_DBG (DEBUG_DEBUG_LEVEL,
					"Testv2MplsFTNMapStorageType Failed \t\n");
			MPLS_CMN_UNLOCK ();
			return SNMP_FAILURE;
		default:
			*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
			CMNDB_DBG (DEBUG_DEBUG_LEVEL,
					"Testv2MplsFTNMapStorageType Failed \t\n");
			MPLS_CMN_UNLOCK ();
			return SNMP_FAILURE;
	}
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
Function    :  nmhDepv2MplsFTNMapTable
Input       :  The Indices
MplsFTNMapIndex
MplsFTNMapPrevIndex
MplsFTNMapCurrIndex
Output      :  The Dependency Low Lev Routine Take the Indices &
check whether dependency is met or not.
Stores the value of error code in the Return val
Error Codes :  The following error codes are to be returned
SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
	INT1
nmhDepv2MplsFTNMapTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
		tSNMP_VAR_BIND * pSnmpVarBind)
{
	UNUSED_PARAM (pu4ErrorCode);
	UNUSED_PARAM (pSnmpIndexList);
	UNUSED_PARAM (pSnmpVarBind);
	return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MplsFTNPerfTable. */

/****************************************************************************
Function    :  nmhValidateIndexInstanceMplsFTNPerfTable
Input       :  The Indices
MplsFTNPerfIndex
MplsFTNPerfCurrIndex
Output      :  The Routines Validates the Given Indices.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

	INT1
nmhValidateIndexInstanceMplsFTNPerfTable (INT4 i4MplsFTNPerfIndex,
		UINT4 u4MplsFTNPerfCurrIndex)
{
	if ((i4MplsFTNPerfIndex > 0) && (u4MplsFTNPerfCurrIndex > 0))
	{
		return SNMP_SUCCESS;
	}
	CMNDB_DBG (DEBUG_DEBUG_LEVEL,
			"ValidateIndexInstanceMplsFTNPerfTable Failed \t\n");
	return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetFirstIndexMplsFTNPerfTable
Input       :  The Indices
MplsFTNPerfIndex
MplsFTNPerfCurrIndex
Output      :  The Get First Routines gets the Lexicographicaly
First Entry from the Table.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_FIRST Routine. */

	INT1
nmhGetFirstIndexMplsFTNPerfTable (INT4 *pi4MplsFTNPerfIndex,
		UINT4 *pu4MplsFTNPerfCurrIndex)
{

	tFtnMapEntry       *pFtnMapEntry = NULL;
	MPLS_CMN_LOCK ();
	if ((pFtnMapEntry = MplsFtnMapTableNextEntry (0, 0)) == NULL)
	{
		CMNDB_DBG (DEBUG_DEBUG_LEVEL,
				"GetFirstIndexMplsFTNPerfTable Failed \t\n");
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}
	*pi4MplsFTNPerfIndex = (INT4)pFtnMapEntry->u4IfIndex;
	*pu4MplsFTNPerfCurrIndex = pFtnMapEntry->u4CurIndex;
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhGetNextIndexMplsFTNPerfTable
Input       :  The Indices
MplsFTNPerfIndex
nextMplsFTNPerfIndex
MplsFTNPerfCurrIndex
nextMplsFTNPerfCurrIndex
Output      :  The Get Next function gets the Next Index for
the Index Value given in the Index Values. The
Indices are stored in the next_varname variables.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
/* GET_NEXT Routine.  */
	INT1
nmhGetNextIndexMplsFTNPerfTable (INT4 i4MplsFTNPerfIndex,
		INT4 *pi4NextMplsFTNPerfIndex,
		UINT4 u4MplsFTNPerfCurrIndex,
		UINT4 *pu4NextMplsFTNPerfCurrIndex)
{

	tFtnMapEntry       *pFtnMapEntry = NULL;
	UINT4               u4FtnMapIndex = 0, u4FtnMapCurrIndex = 0;
	MPLS_CMN_LOCK ();
	pFtnMapEntry = MplsFtnMapTableNextEntry ((UINT4)i4MplsFTNPerfIndex,
			u4MplsFTNPerfCurrIndex);
	if (pFtnMapEntry != NULL)
	{
		*pi4NextMplsFTNPerfIndex = (INT4)pFtnMapEntry->u4IfIndex;
		*pu4NextMplsFTNPerfCurrIndex = pFtnMapEntry->u4CurIndex;
		MPLS_CMN_UNLOCK ();
		return SNMP_SUCCESS;
	}
	else
	{
		for (u4FtnMapIndex = 0; u4FtnMapIndex <= gu4MplsDbMaxFtnMapEntries;
				u4FtnMapIndex++)
		{
			for (u4FtnMapCurrIndex = 0;
					u4FtnMapCurrIndex <= gu4MplsDbMaxFtnMapEntries;
					u4FtnMapCurrIndex++)
			{
				pFtnMapEntry =
					MplsFtnMapTableNextEntry (u4FtnMapIndex, u4FtnMapCurrIndex);
				if (pFtnMapEntry != NULL)
				{
					if (pFtnMapEntry->u4IfIndex == (UINT4) i4MplsFTNPerfIndex)
					{
						pFtnMapEntry =
							MplsFtnMapTableNextEntry (pFtnMapEntry->u4IfIndex,
									pFtnMapEntry->u4CurIndex);
						if (pFtnMapEntry != NULL)
						{
							*pi4NextMplsFTNPerfIndex = (INT4)pFtnMapEntry->u4IfIndex;
							*pu4NextMplsFTNPerfCurrIndex =
								pFtnMapEntry->u4CurIndex;
							MPLS_CMN_UNLOCK ();
							return SNMP_SUCCESS;
						}
						else
						{
							break;
						}
					}
					else if (pFtnMapEntry->u4IfIndex >
							(UINT4) i4MplsFTNPerfIndex)
					{
						*pi4NextMplsFTNPerfIndex = (INT4)pFtnMapEntry->u4IfIndex;
						*pu4NextMplsFTNPerfCurrIndex = pFtnMapEntry->u4CurIndex;
						MPLS_CMN_UNLOCK ();
						return SNMP_SUCCESS;
					}
				}
			}
		}
	}
	CMNDB_DBG (DEBUG_DEBUG_LEVEL, "GetNextIndexMplsFTNPerfTable Failed \t\n");
	MPLS_CMN_UNLOCK ();
	return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
Function    :  nmhGetMplsFTNPerfMatchedPackets
Input       :  The Indices
MplsFTNPerfIndex
MplsFTNPerfCurrIndex

The Object 
retValMplsFTNPerfMatchedPackets
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetMplsFTNPerfMatchedPackets (INT4 i4MplsFTNPerfIndex,
		UINT4 u4MplsFTNPerfCurrIndex,
		tSNMP_COUNTER64_TYPE *
		pu8RetValMplsFTNPerfMatchedPackets)
{
	tFtnMapEntry       *pFtnMapEntry = NULL;
	MPLS_CMN_LOCK ();
	if ((pFtnMapEntry =
				MplsGetFtnMapTableEntry ((UINT4)i4MplsFTNPerfIndex,
					u4MplsFTNPerfCurrIndex)) != NULL)
	{
		pu8RetValMplsFTNPerfMatchedPackets->lsn =
			pFtnMapEntry->FtnPerf.u4MatchedPkts[0];
		pu8RetValMplsFTNPerfMatchedPackets->msn =
			pFtnMapEntry->FtnPerf.u4MatchedPkts[1];
		MPLS_CMN_UNLOCK ();
		return SNMP_SUCCESS;
	}
	CMNDB_DBG (DEBUG_DEBUG_LEVEL, "GetMplsFTNPerfMatchedPackets Failed \t\n");
	MPLS_CMN_UNLOCK ();
	return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetMplsFTNPerfMatchedOctets
Input       :  The Indices
MplsFTNPerfIndex
MplsFTNPerfCurrIndex

The Object 
retValMplsFTNPerfMatchedOctets
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetMplsFTNPerfMatchedOctets (INT4 i4MplsFTNPerfIndex,
		UINT4 u4MplsFTNPerfCurrIndex,
		tSNMP_COUNTER64_TYPE *
		pu8RetValMplsFTNPerfMatchedOctets)
{
	tFtnMapEntry       *pFtnMapEntry = NULL;
	MPLS_CMN_LOCK ();
	if ((pFtnMapEntry =
				MplsGetFtnMapTableEntry ((UINT4)i4MplsFTNPerfIndex,
					u4MplsFTNPerfCurrIndex)) != NULL)
	{
		pu8RetValMplsFTNPerfMatchedOctets->lsn =
			pFtnMapEntry->FtnPerf.u4MatchedOctets[0];
		pu8RetValMplsFTNPerfMatchedOctets->msn =
			pFtnMapEntry->FtnPerf.u4MatchedOctets[1];
		MPLS_CMN_UNLOCK ();
		return SNMP_SUCCESS;
	}
	CMNDB_DBG (DEBUG_DEBUG_LEVEL, "GetMplsFTNPerfMatchedPackets Failed \t\n");
	MPLS_CMN_UNLOCK ();
	return SNMP_FAILURE;

}

/****************************************************************************
Function    :  nmhGetMplsFTNPerfDiscontinuityTime
Input       :  The Indices
MplsFTNPerfIndex
MplsFTNPerfCurrIndex

The Object 
retValMplsFTNPerfDiscontinuityTime
Output      :  The Get Low Lev Routine Take the Indices &
store the Value requested in the Return val.
Returns     :  SNMP_SUCCESS or SNMP_FAILURE
 ****************************************************************************/
INT1
nmhGetMplsFTNPerfDiscontinuityTime (INT4 i4MplsFTNPerfIndex,
		UINT4 u4MplsFTNPerfCurrIndex,
		UINT4
		*pu4RetValMplsFTNPerfDiscontinuityTime)
{
	tFtnMapEntry       *pFtnMapEntry = NULL;
	MPLS_CMN_LOCK ();
	if ((pFtnMapEntry =
				MplsGetFtnMapTableEntry ((UINT4)i4MplsFTNPerfIndex,
					u4MplsFTNPerfCurrIndex)) != NULL)
	{
		*pu4RetValMplsFTNPerfDiscontinuityTime =
			pFtnMapEntry->FtnPerf.u4FTNDisConTime;
		MPLS_CMN_UNLOCK ();
		return SNMP_SUCCESS;
	}
	CMNDB_DBG (DEBUG_DEBUG_LEVEL, "GetMplsFTNPerfMatchedPackets Failed \t\n");
	MPLS_CMN_UNLOCK ();
	return SNMP_FAILURE;
}
