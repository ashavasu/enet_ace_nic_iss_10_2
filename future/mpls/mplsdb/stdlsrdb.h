/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdlsrdb.h,v 1.6 2013/05/23 12:26:47 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _LSRDB_H
#define _LSRDB_H

UINT1 MplsInterfaceTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 MplsInterfacePerfTableINDEX [] = {SNMP_DATA_TYPE_INTEGER};
UINT1 MplsInSegmentTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 MplsInSegmentPerfTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 MplsOutSegmentTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 MplsOutSegmentPerfTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 MplsXCTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 MplsLabelStackTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 MplsInSegmentMapTableINDEX [] = {SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_OBJECT_ID};

UINT4 stdlsr [] ={1,3,6,1,2,1,10,166,2};
tSNMP_OID_TYPE lsrOID = {9, stdlsr};


UINT4 MplsInterfaceIndex [ ] ={1,3,6,1,2,1,10,166,2,1,1,1,1};
UINT4 MplsInterfaceLabelMinIn [ ] ={1,3,6,1,2,1,10,166,2,1,1,1,2};
UINT4 MplsInterfaceLabelMaxIn [ ] ={1,3,6,1,2,1,10,166,2,1,1,1,3};
UINT4 MplsInterfaceLabelMinOut [ ] ={1,3,6,1,2,1,10,166,2,1,1,1,4};
UINT4 MplsInterfaceLabelMaxOut [ ] ={1,3,6,1,2,1,10,166,2,1,1,1,5};
UINT4 MplsInterfaceTotalBandwidth [ ] ={1,3,6,1,2,1,10,166,2,1,1,1,6};
UINT4 MplsInterfaceAvailableBandwidth [ ] ={1,3,6,1,2,1,10,166,2,1,1,1,7};
UINT4 MplsInterfaceLabelParticipationType [ ] ={1,3,6,1,2,1,10,166,2,1,1,1,8};
UINT4 MplsInterfacePerfInLabelsInUse [ ] ={1,3,6,1,2,1,10,166,2,1,2,1,1};
UINT4 MplsInterfacePerfInLabelLookupFailures [ ] ={1,3,6,1,2,1,10,166,2,1,2,1,2};
UINT4 MplsInterfacePerfOutLabelsInUse [ ] ={1,3,6,1,2,1,10,166,2,1,2,1,3};
UINT4 MplsInterfacePerfOutFragmentedPkts [ ] ={1,3,6,1,2,1,10,166,2,1,2,1,4};
UINT4 MplsInSegmentIndexNext [ ] ={1,3,6,1,2,1,10,166,2,1,3};
UINT4 MplsInSegmentIndex [ ] ={1,3,6,1,2,1,10,166,2,1,4,1,1};
UINT4 MplsInSegmentInterface [ ] ={1,3,6,1,2,1,10,166,2,1,4,1,2};
UINT4 MplsInSegmentLabel [ ] ={1,3,6,1,2,1,10,166,2,1,4,1,3};
UINT4 MplsInSegmentLabelPtr [ ] ={1,3,6,1,2,1,10,166,2,1,4,1,4};
UINT4 MplsInSegmentNPop [ ] ={1,3,6,1,2,1,10,166,2,1,4,1,5};
UINT4 MplsInSegmentAddrFamily [ ] ={1,3,6,1,2,1,10,166,2,1,4,1,6};
UINT4 MplsInSegmentXCIndex [ ] ={1,3,6,1,2,1,10,166,2,1,4,1,7};
UINT4 MplsInSegmentOwner [ ] ={1,3,6,1,2,1,10,166,2,1,4,1,8};
UINT4 MplsInSegmentTrafficParamPtr [ ] ={1,3,6,1,2,1,10,166,2,1,4,1,9};
UINT4 MplsInSegmentRowStatus [ ] ={1,3,6,1,2,1,10,166,2,1,4,1,10};
UINT4 MplsInSegmentStorageType [ ] ={1,3,6,1,2,1,10,166,2,1,4,1,11};
UINT4 MplsInSegmentPerfOctets [ ] ={1,3,6,1,2,1,10,166,2,1,5,1,1};
UINT4 MplsInSegmentPerfPackets [ ] ={1,3,6,1,2,1,10,166,2,1,5,1,2};
UINT4 MplsInSegmentPerfErrors [ ] ={1,3,6,1,2,1,10,166,2,1,5,1,3};
UINT4 MplsInSegmentPerfDiscards [ ] ={1,3,6,1,2,1,10,166,2,1,5,1,4};
UINT4 MplsInSegmentPerfHCOctets [ ] ={1,3,6,1,2,1,10,166,2,1,5,1,5};
UINT4 MplsInSegmentPerfDiscontinuityTime [ ] ={1,3,6,1,2,1,10,166,2,1,5,1,6};
UINT4 MplsOutSegmentIndexNext [ ] ={1,3,6,1,2,1,10,166,2,1,6};
UINT4 MplsOutSegmentIndex [ ] ={1,3,6,1,2,1,10,166,2,1,7,1,1};
UINT4 MplsOutSegmentInterface [ ] ={1,3,6,1,2,1,10,166,2,1,7,1,2};
UINT4 MplsOutSegmentPushTopLabel [ ] ={1,3,6,1,2,1,10,166,2,1,7,1,3};
UINT4 MplsOutSegmentTopLabel [ ] ={1,3,6,1,2,1,10,166,2,1,7,1,4};
UINT4 MplsOutSegmentTopLabelPtr [ ] ={1,3,6,1,2,1,10,166,2,1,7,1,5};
UINT4 MplsOutSegmentNextHopAddrType [ ] ={1,3,6,1,2,1,10,166,2,1,7,1,6};
UINT4 MplsOutSegmentNextHopAddr [ ] ={1,3,6,1,2,1,10,166,2,1,7,1,7};
UINT4 MplsOutSegmentXCIndex [ ] ={1,3,6,1,2,1,10,166,2,1,7,1,8};
UINT4 MplsOutSegmentOwner [ ] ={1,3,6,1,2,1,10,166,2,1,7,1,9};
UINT4 MplsOutSegmentTrafficParamPtr [ ] ={1,3,6,1,2,1,10,166,2,1,7,1,10};
UINT4 MplsOutSegmentRowStatus [ ] ={1,3,6,1,2,1,10,166,2,1,7,1,11};
UINT4 MplsOutSegmentStorageType [ ] ={1,3,6,1,2,1,10,166,2,1,7,1,12};
UINT4 MplsOutSegmentPerfOctets [ ] ={1,3,6,1,2,1,10,166,2,1,8,1,1};
UINT4 MplsOutSegmentPerfPackets [ ] ={1,3,6,1,2,1,10,166,2,1,8,1,2};
UINT4 MplsOutSegmentPerfErrors [ ] ={1,3,6,1,2,1,10,166,2,1,8,1,3};
UINT4 MplsOutSegmentPerfDiscards [ ] ={1,3,6,1,2,1,10,166,2,1,8,1,4};
UINT4 MplsOutSegmentPerfHCOctets [ ] ={1,3,6,1,2,1,10,166,2,1,8,1,5};
UINT4 MplsOutSegmentPerfDiscontinuityTime [ ] ={1,3,6,1,2,1,10,166,2,1,8,1,6};
UINT4 MplsXCIndexNext [ ] ={1,3,6,1,2,1,10,166,2,1,9};
UINT4 MplsXCIndex [ ] ={1,3,6,1,2,1,10,166,2,1,10,1,1};
UINT4 MplsXCInSegmentIndex [ ] ={1,3,6,1,2,1,10,166,2,1,10,1,2};
UINT4 MplsXCOutSegmentIndex [ ] ={1,3,6,1,2,1,10,166,2,1,10,1,3};
UINT4 MplsXCLspId [ ] ={1,3,6,1,2,1,10,166,2,1,10,1,4};
UINT4 MplsXCLabelStackIndex [ ] ={1,3,6,1,2,1,10,166,2,1,10,1,5};
UINT4 MplsXCOwner [ ] ={1,3,6,1,2,1,10,166,2,1,10,1,6};
UINT4 MplsXCRowStatus [ ] ={1,3,6,1,2,1,10,166,2,1,10,1,7};
UINT4 MplsXCStorageType [ ] ={1,3,6,1,2,1,10,166,2,1,10,1,8};
UINT4 MplsXCAdminStatus [ ] ={1,3,6,1,2,1,10,166,2,1,10,1,9};
UINT4 MplsXCOperStatus [ ] ={1,3,6,1,2,1,10,166,2,1,10,1,10};
UINT4 MplsMaxLabelStackDepth [ ] ={1,3,6,1,2,1,10,166,2,1,11};
UINT4 MplsLabelStackIndexNext [ ] ={1,3,6,1,2,1,10,166,2,1,12};
UINT4 MplsLabelStackIndex [ ] ={1,3,6,1,2,1,10,166,2,1,13,1,1};
UINT4 MplsLabelStackLabelIndex [ ] ={1,3,6,1,2,1,10,166,2,1,13,1,2};
UINT4 MplsLabelStackLabel [ ] ={1,3,6,1,2,1,10,166,2,1,13,1,3};
UINT4 MplsLabelStackLabelPtr [ ] ={1,3,6,1,2,1,10,166,2,1,13,1,4};
UINT4 MplsLabelStackRowStatus [ ] ={1,3,6,1,2,1,10,166,2,1,13,1,5};
UINT4 MplsLabelStackStorageType [ ] ={1,3,6,1,2,1,10,166,2,1,13,1,6};
UINT4 MplsInSegmentMapInterface [ ] ={1,3,6,1,2,1,10,166,2,1,14,1,1};
UINT4 MplsInSegmentMapLabel [ ] ={1,3,6,1,2,1,10,166,2,1,14,1,2};
UINT4 MplsInSegmentMapLabelPtrIndex [ ] ={1,3,6,1,2,1,10,166,2,1,14,1,3};
UINT4 MplsInSegmentMapIndex [ ] ={1,3,6,1,2,1,10,166,2,1,14,1,4};
UINT4 MplsXCNotificationsEnable [ ] ={1,3,6,1,2,1,10,166,2,1,15};


tMbDbEntry lsrMibEntry[]= {

{{13,MplsInterfaceIndex}, GetNextIndexMplsInterfaceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, MplsInterfaceTableINDEX, 1, 0, 0, NULL},

{{13,MplsInterfaceLabelMinIn}, GetNextIndexMplsInterfaceTable, MplsInterfaceLabelMinInGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, MplsInterfaceTableINDEX, 1, 0, 0, NULL},

{{13,MplsInterfaceLabelMaxIn}, GetNextIndexMplsInterfaceTable, MplsInterfaceLabelMaxInGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, MplsInterfaceTableINDEX, 1, 0, 0, NULL},

{{13,MplsInterfaceLabelMinOut}, GetNextIndexMplsInterfaceTable, MplsInterfaceLabelMinOutGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, MplsInterfaceTableINDEX, 1, 0, 0, NULL},

{{13,MplsInterfaceLabelMaxOut}, GetNextIndexMplsInterfaceTable, MplsInterfaceLabelMaxOutGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, MplsInterfaceTableINDEX, 1, 0, 0, NULL},

{{13,MplsInterfaceTotalBandwidth}, GetNextIndexMplsInterfaceTable, MplsInterfaceTotalBandwidthGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, MplsInterfaceTableINDEX, 1, 0, 0, NULL},

{{13,MplsInterfaceAvailableBandwidth}, GetNextIndexMplsInterfaceTable, MplsInterfaceAvailableBandwidthGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, MplsInterfaceTableINDEX, 1, 0, 0, NULL},

{{13,MplsInterfaceLabelParticipationType}, GetNextIndexMplsInterfaceTable, MplsInterfaceLabelParticipationTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, MplsInterfaceTableINDEX, 1, 0, 0, NULL},

{{13,MplsInterfacePerfInLabelsInUse}, GetNextIndexMplsInterfacePerfTable, MplsInterfacePerfInLabelsInUseGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, MplsInterfacePerfTableINDEX, 1, 0, 0, NULL},

{{13,MplsInterfacePerfInLabelLookupFailures}, GetNextIndexMplsInterfacePerfTable, MplsInterfacePerfInLabelLookupFailuresGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsInterfacePerfTableINDEX, 1, 0, 0, NULL},

{{13,MplsInterfacePerfOutLabelsInUse}, GetNextIndexMplsInterfacePerfTable, MplsInterfacePerfOutLabelsInUseGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, MplsInterfacePerfTableINDEX, 1, 0, 0, NULL},

{{13,MplsInterfacePerfOutFragmentedPkts}, GetNextIndexMplsInterfacePerfTable, MplsInterfacePerfOutFragmentedPktsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsInterfacePerfTableINDEX, 1, 0, 0, NULL},

{{11,MplsInSegmentIndexNext}, NULL, MplsInSegmentIndexNextGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,MplsInSegmentIndex}, GetNextIndexMplsInSegmentTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, MplsInSegmentTableINDEX, 1, 0, 0, NULL},

{{13,MplsInSegmentInterface}, GetNextIndexMplsInSegmentTable, MplsInSegmentInterfaceGet, MplsInSegmentInterfaceSet, MplsInSegmentInterfaceTest, MplsInSegmentTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsInSegmentTableINDEX, 1, 0, 0, NULL},

{{13,MplsInSegmentLabel}, GetNextIndexMplsInSegmentTable, MplsInSegmentLabelGet, MplsInSegmentLabelSet, MplsInSegmentLabelTest, MplsInSegmentTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsInSegmentTableINDEX, 1, 0, 0, NULL},

{{13,MplsInSegmentLabelPtr}, GetNextIndexMplsInSegmentTable, MplsInSegmentLabelPtrGet, MplsInSegmentLabelPtrSet, MplsInSegmentLabelPtrTest, MplsInSegmentTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, MplsInSegmentTableINDEX, 1, 0, 0, NULL},

{{13,MplsInSegmentNPop}, GetNextIndexMplsInSegmentTable, MplsInSegmentNPopGet, MplsInSegmentNPopSet, MplsInSegmentNPopTest, MplsInSegmentTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, MplsInSegmentTableINDEX, 1, 0, 0, "1"},

{{13,MplsInSegmentAddrFamily}, GetNextIndexMplsInSegmentTable, MplsInSegmentAddrFamilyGet, MplsInSegmentAddrFamilySet, MplsInSegmentAddrFamilyTest, MplsInSegmentTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsInSegmentTableINDEX, 1, 0, 0, NULL},

{{13,MplsInSegmentXCIndex}, GetNextIndexMplsInSegmentTable, MplsInSegmentXCIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, MplsInSegmentTableINDEX, 1, 0, 0, NULL},

{{13,MplsInSegmentOwner}, GetNextIndexMplsInSegmentTable, MplsInSegmentOwnerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, MplsInSegmentTableINDEX, 1, 0, 0, NULL},

{{13,MplsInSegmentTrafficParamPtr}, GetNextIndexMplsInSegmentTable, MplsInSegmentTrafficParamPtrGet, MplsInSegmentTrafficParamPtrSet, MplsInSegmentTrafficParamPtrTest, MplsInSegmentTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, MplsInSegmentTableINDEX, 1, 0, 0, NULL},

{{13,MplsInSegmentRowStatus}, GetNextIndexMplsInSegmentTable, MplsInSegmentRowStatusGet, MplsInSegmentRowStatusSet, MplsInSegmentRowStatusTest, MplsInSegmentTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsInSegmentTableINDEX, 1, 0, 1, NULL},

{{13,MplsInSegmentStorageType}, GetNextIndexMplsInSegmentTable, MplsInSegmentStorageTypeGet, MplsInSegmentStorageTypeSet, MplsInSegmentStorageTypeTest, MplsInSegmentTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsInSegmentTableINDEX, 1, 0, 0, "2"},

{{13,MplsInSegmentPerfOctets}, GetNextIndexMplsInSegmentPerfTable, MplsInSegmentPerfOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsInSegmentPerfTableINDEX, 1, 0, 0, NULL},

{{13,MplsInSegmentPerfPackets}, GetNextIndexMplsInSegmentPerfTable, MplsInSegmentPerfPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsInSegmentPerfTableINDEX, 1, 0, 0, NULL},

{{13,MplsInSegmentPerfErrors}, GetNextIndexMplsInSegmentPerfTable, MplsInSegmentPerfErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsInSegmentPerfTableINDEX, 1, 0, 0, NULL},

{{13,MplsInSegmentPerfDiscards}, GetNextIndexMplsInSegmentPerfTable, MplsInSegmentPerfDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsInSegmentPerfTableINDEX, 1, 0, 0, NULL},

{{13,MplsInSegmentPerfHCOctets}, GetNextIndexMplsInSegmentPerfTable, MplsInSegmentPerfHCOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, MplsInSegmentPerfTableINDEX, 1, 0, 0, NULL},

{{13,MplsInSegmentPerfDiscontinuityTime}, GetNextIndexMplsInSegmentPerfTable, MplsInSegmentPerfDiscontinuityTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MplsInSegmentPerfTableINDEX, 1, 0, 0, NULL},

{{11,MplsOutSegmentIndexNext}, NULL, MplsOutSegmentIndexNextGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,MplsOutSegmentIndex}, GetNextIndexMplsOutSegmentTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, MplsOutSegmentTableINDEX, 1, 0, 0, NULL},

{{13,MplsOutSegmentInterface}, GetNextIndexMplsOutSegmentTable, MplsOutSegmentInterfaceGet, MplsOutSegmentInterfaceSet, MplsOutSegmentInterfaceTest, MplsOutSegmentTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsOutSegmentTableINDEX, 1, 0, 0, NULL},

{{13,MplsOutSegmentPushTopLabel}, GetNextIndexMplsOutSegmentTable, MplsOutSegmentPushTopLabelGet, MplsOutSegmentPushTopLabelSet, MplsOutSegmentPushTopLabelTest, MplsOutSegmentTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsOutSegmentTableINDEX, 1, 0, 0, "1"},

{{13,MplsOutSegmentTopLabel}, GetNextIndexMplsOutSegmentTable, MplsOutSegmentTopLabelGet, MplsOutSegmentTopLabelSet, MplsOutSegmentTopLabelTest, MplsOutSegmentTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsOutSegmentTableINDEX, 1, 0, 0, "0"},

{{13,MplsOutSegmentTopLabelPtr}, GetNextIndexMplsOutSegmentTable, MplsOutSegmentTopLabelPtrGet, MplsOutSegmentTopLabelPtrSet, MplsOutSegmentTopLabelPtrTest, MplsOutSegmentTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, MplsOutSegmentTableINDEX, 1, 0, 0, NULL},

{{13,MplsOutSegmentNextHopAddrType}, GetNextIndexMplsOutSegmentTable, MplsOutSegmentNextHopAddrTypeGet, MplsOutSegmentNextHopAddrTypeSet, MplsOutSegmentNextHopAddrTypeTest, MplsOutSegmentTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsOutSegmentTableINDEX, 1, 0, 0, NULL},

{{13,MplsOutSegmentNextHopAddr}, GetNextIndexMplsOutSegmentTable, MplsOutSegmentNextHopAddrGet, MplsOutSegmentNextHopAddrSet, MplsOutSegmentNextHopAddrTest, MplsOutSegmentTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, MplsOutSegmentTableINDEX, 1, 0, 0, NULL},

{{13,MplsOutSegmentXCIndex}, GetNextIndexMplsOutSegmentTable, MplsOutSegmentXCIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, MplsOutSegmentTableINDEX, 1, 0, 0, NULL},

{{13,MplsOutSegmentOwner}, GetNextIndexMplsOutSegmentTable, MplsOutSegmentOwnerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, MplsOutSegmentTableINDEX, 1, 0, 0, NULL},

{{13,MplsOutSegmentTrafficParamPtr}, GetNextIndexMplsOutSegmentTable, MplsOutSegmentTrafficParamPtrGet, MplsOutSegmentTrafficParamPtrSet, MplsOutSegmentTrafficParamPtrTest, MplsOutSegmentTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, MplsOutSegmentTableINDEX, 1, 0, 0, NULL},

{{13,MplsOutSegmentRowStatus}, GetNextIndexMplsOutSegmentTable, MplsOutSegmentRowStatusGet, MplsOutSegmentRowStatusSet, MplsOutSegmentRowStatusTest, MplsOutSegmentTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsOutSegmentTableINDEX, 1, 0, 1, NULL},

{{13,MplsOutSegmentStorageType}, GetNextIndexMplsOutSegmentTable, MplsOutSegmentStorageTypeGet, MplsOutSegmentStorageTypeSet, MplsOutSegmentStorageTypeTest, MplsOutSegmentTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsOutSegmentTableINDEX, 1, 0, 0, "2"},

{{13,MplsOutSegmentPerfOctets}, GetNextIndexMplsOutSegmentPerfTable, MplsOutSegmentPerfOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsOutSegmentPerfTableINDEX, 1, 0, 0, NULL},

{{13,MplsOutSegmentPerfPackets}, GetNextIndexMplsOutSegmentPerfTable, MplsOutSegmentPerfPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsOutSegmentPerfTableINDEX, 1, 0, 0, NULL},

{{13,MplsOutSegmentPerfErrors}, GetNextIndexMplsOutSegmentPerfTable, MplsOutSegmentPerfErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsOutSegmentPerfTableINDEX, 1, 0, 0, NULL},

{{13,MplsOutSegmentPerfDiscards}, GetNextIndexMplsOutSegmentPerfTable, MplsOutSegmentPerfDiscardsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsOutSegmentPerfTableINDEX, 1, 0, 0, NULL},

{{13,MplsOutSegmentPerfHCOctets}, GetNextIndexMplsOutSegmentPerfTable, MplsOutSegmentPerfHCOctetsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, MplsOutSegmentPerfTableINDEX, 1, 0, 0, NULL},

{{13,MplsOutSegmentPerfDiscontinuityTime}, GetNextIndexMplsOutSegmentPerfTable, MplsOutSegmentPerfDiscontinuityTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MplsOutSegmentPerfTableINDEX, 1, 0, 0, NULL},

{{11,MplsXCIndexNext}, NULL, MplsXCIndexNextGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,MplsXCIndex}, GetNextIndexMplsXCTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, MplsXCTableINDEX, 3, 0, 0, NULL},

{{13,MplsXCInSegmentIndex}, GetNextIndexMplsXCTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, MplsXCTableINDEX, 3, 0, 0, NULL},

{{13,MplsXCOutSegmentIndex}, GetNextIndexMplsXCTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, MplsXCTableINDEX, 3, 0, 0, NULL},

{{13,MplsXCLspId}, GetNextIndexMplsXCTable, MplsXCLspIdGet, MplsXCLspIdSet, MplsXCLspIdTest, MplsXCTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, MplsXCTableINDEX, 3, 0, 0, NULL},

{{13,MplsXCLabelStackIndex}, GetNextIndexMplsXCTable, MplsXCLabelStackIndexGet, MplsXCLabelStackIndexSet, MplsXCLabelStackIndexTest, MplsXCTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, MplsXCTableINDEX, 3, 0, 0, NULL},

{{13,MplsXCOwner}, GetNextIndexMplsXCTable, MplsXCOwnerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, MplsXCTableINDEX, 3, 0, 0, NULL},

{{13,MplsXCRowStatus}, GetNextIndexMplsXCTable, MplsXCRowStatusGet, MplsXCRowStatusSet, MplsXCRowStatusTest, MplsXCTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsXCTableINDEX, 3, 0, 1, NULL},

{{13,MplsXCStorageType}, GetNextIndexMplsXCTable, MplsXCStorageTypeGet, MplsXCStorageTypeSet, MplsXCStorageTypeTest, MplsXCTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsXCTableINDEX, 3, 0, 0, "2"},

{{13,MplsXCAdminStatus}, GetNextIndexMplsXCTable, MplsXCAdminStatusGet, MplsXCAdminStatusSet, MplsXCAdminStatusTest, MplsXCTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsXCTableINDEX, 3, 0, 0, "1"},

{{13,MplsXCOperStatus}, GetNextIndexMplsXCTable, MplsXCOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, MplsXCTableINDEX, 3, 0, 0, NULL},

{{11,MplsMaxLabelStackDepth}, NULL, MplsMaxLabelStackDepthGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,MplsLabelStackIndexNext}, NULL, MplsLabelStackIndexNextGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,MplsLabelStackIndex}, GetNextIndexMplsLabelStackTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, MplsLabelStackTableINDEX, 2, 0, 0, NULL},

{{13,MplsLabelStackLabelIndex}, GetNextIndexMplsLabelStackTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, MplsLabelStackTableINDEX, 2, 0, 0, NULL},

{{13,MplsLabelStackLabel}, GetNextIndexMplsLabelStackTable, MplsLabelStackLabelGet, MplsLabelStackLabelSet, MplsLabelStackLabelTest, MplsLabelStackTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsLabelStackTableINDEX, 2, 0, 0, NULL},

{{13,MplsLabelStackLabelPtr}, GetNextIndexMplsLabelStackTable, MplsLabelStackLabelPtrGet, MplsLabelStackLabelPtrSet, MplsLabelStackLabelPtrTest, MplsLabelStackTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, MplsLabelStackTableINDEX, 2, 0, 0, NULL},

{{13,MplsLabelStackRowStatus}, GetNextIndexMplsLabelStackTable, MplsLabelStackRowStatusGet, MplsLabelStackRowStatusSet, MplsLabelStackRowStatusTest, MplsLabelStackTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsLabelStackTableINDEX, 2, 0, 1, NULL},

{{13,MplsLabelStackStorageType}, GetNextIndexMplsLabelStackTable, MplsLabelStackStorageTypeGet, MplsLabelStackStorageTypeSet, MplsLabelStackStorageTypeTest, MplsLabelStackTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsLabelStackTableINDEX, 2, 0, 0, "2"},

{{13,MplsInSegmentMapInterface}, GetNextIndexMplsInSegmentMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, MplsInSegmentMapTableINDEX, 3, 0, 0, NULL},

{{13,MplsInSegmentMapLabel}, GetNextIndexMplsInSegmentMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, MplsInSegmentMapTableINDEX, 3, 0, 0, NULL},

{{13,MplsInSegmentMapLabelPtrIndex}, GetNextIndexMplsInSegmentMapTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_NOACCESS, MplsInSegmentMapTableINDEX, 3, 0, 0, NULL},

{{13,MplsInSegmentMapIndex}, GetNextIndexMplsInSegmentMapTable, MplsInSegmentMapIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, MplsInSegmentMapTableINDEX, 3, 0, 0, NULL},

{{11,MplsXCNotificationsEnable}, NULL, MplsXCNotificationsEnableGet, MplsXCNotificationsEnableSet, MplsXCNotificationsEnableTest, MplsXCNotificationsEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};
tMibData lsrEntry = { 73, lsrMibEntry };
#endif /* _LSRDB_H */

