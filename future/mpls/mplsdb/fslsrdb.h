/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: fslsrdb.h,v 1.3 2013/07/26 13:31:11 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSLSRDB_H
#define _FSLSRDB_H

UINT1 FsMplsInSegmentTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 FsMplsOutSegmentTableINDEX [] = {SNMP_DATA_TYPE_OCTET_PRIM};

UINT4 fslsr [] ={1,3,6,1,4,1,2076,13,13};
tSNMP_OID_TYPE fslsrOID = {9, fslsr};


UINT4 FsMplsInSegmentDirection [ ] ={1,3,6,1,4,1,2076,13,13,1,2,1,1};
UINT4 FsMplsOutSegmentDirection [ ] ={1,3,6,1,4,1,2076,13,13,1,3,1,1};
UINT4 FsMplsLsrRfc6428CompatibleCodePoint [ ] ={1,3,6,1,4,1,2076,13,13,1,1,1};




tMbDbEntry fslsrMibEntry[]= {

{{12,FsMplsLsrRfc6428CompatibleCodePoint}, NULL, FsMplsLsrRfc6428CompatibleCodePointGet, FsMplsLsrRfc6428CompatibleCodePointSet, FsMplsLsrRfc6428CompatibleCodePointTest, FsMplsLsrRfc6428CompatibleCodePointDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{13,FsMplsInSegmentDirection}, GetNextIndexFsMplsInSegmentTable, FsMplsInSegmentDirectionGet, FsMplsInSegmentDirectionSet, FsMplsInSegmentDirectionTest, FsMplsInSegmentTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsInSegmentTableINDEX, 1, 0, 0, "1"},

{{13,FsMplsOutSegmentDirection}, GetNextIndexFsMplsOutSegmentTable, FsMplsOutSegmentDirectionGet, FsMplsOutSegmentDirectionSet, FsMplsOutSegmentDirectionTest, FsMplsOutSegmentTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsOutSegmentTableINDEX, 1, 0, 0, "1"},
};
tMibData fslsrEntry = { 3, fslsrMibEntry };

#endif /* _FSLSRDB_H */

