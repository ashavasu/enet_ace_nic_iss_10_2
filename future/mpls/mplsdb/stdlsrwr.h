#ifndef _LSRWR_H
#define _LSRWR_H
INT4 GetNextIndexMplsInterfaceTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterLSR(VOID);
INT4 MplsInterfaceIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsInterfaceLabelMinInGet(tSnmpIndex *, tRetVal *);
INT4 MplsInterfaceLabelMaxInGet(tSnmpIndex *, tRetVal *);
INT4 MplsInterfaceLabelMinOutGet(tSnmpIndex *, tRetVal *);
INT4 MplsInterfaceLabelMaxOutGet(tSnmpIndex *, tRetVal *);
INT4 MplsInterfaceTotalBandwidthGet(tSnmpIndex *, tRetVal *);
INT4 MplsInterfaceAvailableBandwidthGet(tSnmpIndex *, tRetVal *);
INT4 MplsInterfaceLabelParticipationTypeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexMplsInterfacePerfTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsInterfacePerfInLabelsInUseGet(tSnmpIndex *, tRetVal *);
INT4 MplsInterfacePerfInLabelLookupFailuresGet(tSnmpIndex *, tRetVal *);
INT4 MplsInterfacePerfOutLabelsInUseGet(tSnmpIndex *, tRetVal *);
INT4 MplsInterfacePerfOutFragmentedPktsGet(tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentIndexNextGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexMplsInSegmentTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsInSegmentIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentInterfaceGet(tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentLabelGet(tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentLabelPtrGet(tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentNPopGet(tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentAddrFamilyGet(tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentXCIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentOwnerGet(tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentTrafficParamPtrGet(tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentInterfaceSet(tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentLabelSet(tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentLabelPtrSet(tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentNPopSet(tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentAddrFamilySet(tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentTrafficParamPtrSet(tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentInterfaceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentLabelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentLabelPtrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentNPopTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentAddrFamilyTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentTrafficParamPtrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);








INT4 GetNextIndexMplsInSegmentPerfTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsInSegmentPerfOctetsGet(tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentPerfPacketsGet(tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentPerfErrorsGet(tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentPerfDiscardsGet(tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentPerfHCOctetsGet(tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentPerfDiscontinuityTimeGet(tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentIndexNextGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexMplsOutSegmentTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsOutSegmentIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentInterfaceGet(tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentPushTopLabelGet(tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentTopLabelGet(tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentTopLabelPtrGet(tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentNextHopAddrTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentNextHopAddrGet(tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentXCIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentOwnerGet(tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentTrafficParamPtrGet(tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentInterfaceSet(tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentPushTopLabelSet(tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentTopLabelSet(tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentTopLabelPtrSet(tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentNextHopAddrTypeSet(tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentNextHopAddrSet(tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentTrafficParamPtrSet(tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentInterfaceTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentPushTopLabelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentTopLabelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentTopLabelPtrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentNextHopAddrTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentNextHopAddrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentTrafficParamPtrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);









INT4 GetNextIndexMplsOutSegmentPerfTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsOutSegmentPerfOctetsGet(tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentPerfPacketsGet(tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentPerfErrorsGet(tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentPerfDiscardsGet(tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentPerfHCOctetsGet(tSnmpIndex *, tRetVal *);
INT4 MplsOutSegmentPerfDiscontinuityTimeGet(tSnmpIndex *, tRetVal *);
INT4 MplsXCIndexNextGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexMplsXCTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsXCIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsXCInSegmentIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsXCOutSegmentIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsXCLspIdGet(tSnmpIndex *, tRetVal *);
INT4 MplsXCLabelStackIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsXCOwnerGet(tSnmpIndex *, tRetVal *);
INT4 MplsXCRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 MplsXCStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsXCAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 MplsXCOperStatusGet(tSnmpIndex *, tRetVal *);
INT4 MplsXCLspIdSet(tSnmpIndex *, tRetVal *);
INT4 MplsXCLabelStackIndexSet(tSnmpIndex *, tRetVal *);
INT4 MplsXCRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 MplsXCStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 MplsXCAdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 MplsXCLspIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsXCLabelStackIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsXCRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsXCStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsXCAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsXCTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);





INT4 MplsMaxLabelStackDepthGet(tSnmpIndex *, tRetVal *);
INT4 MplsLabelStackIndexNextGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexMplsLabelStackTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsLabelStackIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsLabelStackLabelIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsLabelStackLabelGet(tSnmpIndex *, tRetVal *);
INT4 MplsLabelStackLabelPtrGet(tSnmpIndex *, tRetVal *);
INT4 MplsLabelStackRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 MplsLabelStackStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsLabelStackLabelSet(tSnmpIndex *, tRetVal *);
INT4 MplsLabelStackLabelPtrSet(tSnmpIndex *, tRetVal *);
INT4 MplsLabelStackRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 MplsLabelStackStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 MplsLabelStackLabelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLabelStackLabelPtrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLabelStackRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLabelStackStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsLabelStackTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);




INT4 GetNextIndexMplsInSegmentMapTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsInSegmentMapInterfaceGet(tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentMapLabelGet(tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentMapLabelPtrIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsInSegmentMapIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsXCNotificationsEnableGet(tSnmpIndex *, tRetVal *);
INT4 MplsXCNotificationsEnableSet(tSnmpIndex *, tRetVal *);
INT4 MplsXCNotificationsEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsXCNotificationsEnableDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

#endif /* _LSRWR_H */
