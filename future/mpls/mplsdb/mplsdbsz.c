/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: mplsdbsz.c,v 1.4 2014/12/12 11:56:43 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _MPLSDBSZ_C
#include "mplsdbinc.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
MplsdbSizingMemCreateMemPools ()
{
    UINT4               u4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < MPLSDB_MAX_SIZING_ID; i4SizingId++)
    {
        u4RetVal =
            MemCreateMemPool (FsMPLSDBSizingParams[i4SizingId].u4StructSize,
                              FsMPLSDBSizingParams[i4SizingId].
                              u4PreAllocatedUnits, MEM_DEFAULT_MEMORY_TYPE,
                              &(MPLSDBMemPoolIds[i4SizingId]));
        if (u4RetVal == MEM_FAILURE)
        {
            MplsdbSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
MplsdbSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsMPLSDBSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, MPLSDBMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
MplsdbSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < MPLSDB_MAX_SIZING_ID; i4SizingId++)
    {
        if (MPLSDBMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (MPLSDBMemPoolIds[i4SizingId]);
            MPLSDBMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
