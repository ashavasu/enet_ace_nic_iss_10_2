/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved 
 *
 * $Id: mplsdbif.c,v 1.153 2018/01/03 11:31:21 siva Exp $
 *
 * Description: Routines for Mpls Common DB Init and Interface Module
 ********************************************************************/
#include "mplsdbinc.h"
#include "mplcmndb.h"
#include "npapi.h"
#include "arp.h"
#include "rpteext.h"
#include "tedsmacs.h"
#include "rtm.h"
#include "ipnp.h"
#include "mplsnp.h"
#include "tedsdefs.h"
#include "fsmplslw.h"
#include "mplstdfs.h"
#include "mplsdiff.h"
#include "ldpext.h"
#include "l2vpextn.h"
#include "mplsutil.h"
#include "mplsglob.h"
#include "mplsred.h"
#include "mplsprot.h"
#ifdef LDP_GR_WANTED
#include "mplshwlist.h"
#endif
#include "ip6np.h"
#include "ipv6npwr.h"
#include "ip6util.h"
#include "ipv6.h"
#include  "qosxtd.h"
#include  "cfanp.h"
#ifdef MPLS_L3VPN_WANTED
#include "mpl3vpncmn.h"
#endif

/* LSR Global Semaphore */
static tOsixSemId   gLsrSemId = 0;

#ifdef VPLSADS_WANTED
static UINT4        gu4BgpAdminState = L2VPN_BGP_ADMIN_DOWN;
#endif
#if defined(MPLS_L3VPN_WANTED) || defined(VPLSADS_WANTED)
static UINT4        gu4BgpAsnValue = 0;
static UINT1        gu1BgpAsnType = 3;
#endif

static INT4         MplsILMCreate (tLspInfo * pLspInfo);
static INT4         MplsILMModify (tLspInfo * pLspInfo);
static INT4         MplsILMDelete (tLspInfo * pLspInfo);
static INT4         MplsFTNCreate (tLspInfo * pLspInfo);
static INT4         MplsFTNModify (tLspInfo * pLspInfo);
static INT4         MplsFTNDelete (tLspInfo * pLspInfo);
static INT4         MplsTnlCreate (tLspInfo * pLspInfo);
static INT4         MplsTnlModify (tLspInfo * pLspInfo);
static INT4         MplsTnlDelete (tLspInfo * pLspInfo);
INT4                MplsTnlNPAdd (tTeTnlInfo *, tXcEntry *,
                                  tMbsmSlotInfo * pSlotInfo);
static INT4         MplsTnlNPDel (tTeTnlInfo *, UINT4 u4ModuleId);
static INT4         MplsDbPrgFrrTnlMP (tLspInfo * pLspInfo,
                                       UINT4 u4XcIndex, UINT4 *pu4InXcIndex);
static VOID         MplsDeleteInXcOutEntry (UINT4 u4XcIndex,
                                            eDirection Direction);
static VOID         MplsFillQosInfo (tTeTnlInfo * pTeTnlInfo,
                                     tQosInfo * pQosInfo);

static UINT2        gu2StaticId = 0;

UINT4               gu4MplsDbTraceFlag = 0x0;
UINT4               gu4MplsDbTraceLvl = 0;
UINT4               gu4MplsDbMaxIfEntries = 0;
UINT4               gu4MplsDbMaxFtnEntries = 0;
UINT4               gu4MplsDbMaxFtnMapEntries = 0;
UINT4               gu4MplsDbMaxXCEntries = 0;
UINT4               gu4MplsDbMaxInSegEntries = 0;
UINT4               gu4MplsDbMaxOutSegEntries = 0;
UINT4               gu4MplsDbMaxLblStackEntries = 0;

BOOL1               gbRfc6428CompatibleCodePoint = MPLS_SNMP_FALSE;

#define     IP_ADDR_LENGTH        4

/************************************************************************
 *  Function Name   : MplsCommondbInitWithSem 
 *  Description     : Function to Init Common Data base manager
 *  Input           : None
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE
 ************************************************************************/
INT4
MplsCommondbInitWithSem (VOID)
{
    /* Semaphore Create */
    if (OsixCreateSem (MPLS_LSR_SEM_NAME, 1, 0, &gLsrSemId) != OSIX_SUCCESS)
    {
        CMNDB_DBG (DEBUG_ERROR_LEVEL, "LSR Semaphore Create Failed \t\n");
        return MPLS_FAILURE;
    }

    if (MplsCmnDbInit () == MPLS_FAILURE)
    {
        CMNDB_DBG (DEBUG_ERROR_LEVEL,
                   "MPLS Common database initialization Failed \t\n");
        return MPLS_FAILURE;
    }

    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsCmnDbInit 
 *  Description     : Function to Init Common Data base manager
 *  Input           : None
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE
 ************************************************************************/
INT4
MplsCmnDbInit (VOID)
{
    tMplsIfEntry       *pMplsIfEntry = NULL;
    tKeyInfoStruct      LblRangeInfo;

    gu4MplsDbMaxIfEntries
        = FsMPLSDBSizingParams[MAX_MPLSDB_LSR_IFENTRY_SIZING_ID].
        u4PreAllocatedUnits;
    gu4MplsDbMaxFtnEntries
        = FsMPLSDBSizingParams[MAX_MPLSDB_FTN_ENTRY_SIZING_ID].
        u4PreAllocatedUnits;
    gu4MplsDbMaxFtnMapEntries
        = FsMPLSDBSizingParams[MAX_MPLSDB_FTN_MAP_ENTRY_SIZING_ID].
        u4PreAllocatedUnits;
    gu4MplsDbMaxXCEntries
        = FsMPLSDBSizingParams[MAX_MPLSDB_LSR_XCENTRY_SIZING_ID].
        u4PreAllocatedUnits;
    gu4MplsDbMaxInSegEntries
        = FsMPLSDBSizingParams[MAX_MPLSDB_LSR_INSEGMENT_SIZING_ID].
        u4PreAllocatedUnits;
    gu4MplsDbMaxOutSegEntries
        = FsMPLSDBSizingParams[MAX_MPLSDB_LSR_OUTSEGMENT_SIZING_ID].
        u4PreAllocatedUnits;
    gu4MplsDbMaxLblStackEntries
        = FsMPLSDBSizingParams[MAX_MPLSDB_LSR_LBLSTKENTRY_SIZING_ID].
        u4PreAllocatedUnits;

    /* Call autogenerated function to create mempools required for the module */
    if (MplsdbSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        return MPLS_FAILURE;
    }

    /* LSR Data base Init */
    if (MplsLSRInit () == MPLS_FAILURE)
    {
        MplsdbSizingMemDeleteMemPools ();
        return MPLS_FAILURE;
    }

    /* FTN Data base Init */
    if (MplsFTNInit () == MPLS_FAILURE)
    {
        MplsLSRDeInit ();
        MplsdbSizingMemDeleteMemPools ();
        return MPLS_FAILURE;
    }
    /* HwEnable will be called from MplsRmMakeNodeActive
     * (mplsred.c) incase of L2RED_WANTED enabled */
#ifndef L2RED_WANTED
#ifdef NPAPI_WANTED
    if (MplsFsMplsHwEnableMpls () == FNP_FAILURE)
    {
        MplsLSRDeInit ();
        MplsFTNDeInit ();
        MplsdbSizingMemDeleteMemPools ();
        return MPLS_FAILURE;
    }
#endif
#endif
    /* Create Default IfEntry for PerPlatform 
     * Label Space IfEntry support */
    if ((pMplsIfEntry = MplsCreateIfTableEntry (0)) == NULL)
    {
        CMNDB_DBG (DEBUG_ERROR_LEVEL,
                   "MplsCmnDbInit: Unable to create default IfEntry \t\n");
        MplsLSRDeInit ();
        MplsFTNDeInit ();
        MplsdbSizingMemDeleteMemPools ();
#ifndef L2RED_WANTED
#ifdef NPAPI_WANTED
        MplsFsMplsHwDisableMpls ();
#endif
#endif
        return MPLS_FAILURE;
    }
    /* Label Space for Generic PerPlatform
     * starts with label value 100 so,
     * min in label is 100 */
    pMplsIfEntry->u4LabelMinIn = 100;
    /* 2**20 Max Gen Label */
    pMplsIfEntry->u4LabelMaxIn = 1048576;
    pMplsIfEntry->u4LabelMinOut = 0;
    /* 2**20 Max Gen Label */
    pMplsIfEntry->u4LabelMaxOut = 1048576;
    pMplsIfEntry->u4TotalBW = 0;    /* Best Effort */
    pMplsIfEntry->u4AvailableBW = 0;    /* Best Effort */
    pMplsIfEntry->u1PartType = LSR_PER_PLATFORM;
    LblRangeInfo.u4Key1Min = 0;
    LblRangeInfo.u4Key1Max = 0;
    LblRangeInfo.u4Key2Min = gSystemSize.MplsSystemSize.u4MinStaticLblRange;
    LblRangeInfo.u4Key2Max = gSystemSize.MplsSystemSize.u4MaxStaticLblRange;

    /*Gmpls Parameters */
    pMplsIfEntry->GmplsIfEntry.u1SignalingCaps = GMPLS_SIG_CAP_RSVP_BIT;

    /* Create Label Space Group for the Static Labels */
    if (MplsCreateStaticLblSpaceGroup
        (LBL_ALLOC_BOTH_NUM, &LblRangeInfo, 1,
         STATIC_LBL_MODULE_ID, PER_PLATFORM_INTERFACE_INDEX,
         &gu2StaticId) == LBL_FAILURE)
    {
        MplsLSRDeInit ();
        MplsFTNDeInit ();
        MplsdbSizingMemDeleteMemPools ();
#ifndef L2RED_WANTED
#ifdef NPAPI_WANTED
        MplsFsMplsHwDisableMpls ();
#endif
#endif
        return MPLS_FAILURE;
    }
    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsCommondbDeInit 
 *  Description     : Function to DeInit Common Data base manager
 *  Input           : None
 *  Output          : None
 *  Returns         : None
 ************************************************************************/
VOID
MplsCommondbDeInit (VOID)
{
#ifndef L2RED_WANTED
#ifdef NPAPI_WANTED
    if (MplsFsMplsHwDisableMpls () == FNP_FAILURE)
    {
        CMNDB_DBG
            (DEBUG_ERROR_LEVEL,
             "MplsCommondbDeInit: Unable to disable MPLS on Hardware \t\n");
    }
#endif
#endif
    MplsLSRDeInit ();
    MplsFTNDeInit ();
    MplsdbSizingMemDeleteMemPools ();
    /* Deleting the Static Label Space allocated while 
     * initialization process */
    MplsDeleteStaticLblSpaceGroup (gu2StaticId);
    return;
}

/************************************************************************
 *  Function Name   :MplsMlibUpdate 
 *  Description     :Function to update the Label info into Mlib.      
 *  Input           :u2MlibOperation - Operation to be performed on the MLIB 
                     pLspInfo - Info to be updated in the MLIB   
 *  Output          :None 
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE  
 ************************************************************************/
UINT1
MplsMlibUpdate (UINT2 u2MlibOperation, tLspInfo * pLspInfo)
{
    MPLS_CMN_LOCK ();

    switch (u2MlibOperation)
    {
        case MPLS_MLIB_ILM_CREATE:
            MplsILMCreate (pLspInfo);
            break;
        case MPLS_MLIB_ILM_MODIFY:
            MplsILMModify (pLspInfo);
            break;
        case MPLS_MLIB_ILM_DELETE:
            MplsILMDelete (pLspInfo);
            break;
        case MPLS_MLIB_FTN_CREATE:
            if (MPLS_FAILURE == MplsFTNCreate (pLspInfo))
            {
                MPLS_CMN_UNLOCK ();
                return MPLS_FAILURE;
            }
            break;
        case MPLS_MLIB_FTN_DELETE:
            MplsFTNDelete (pLspInfo);
            break;
        case MPLS_MLIB_FTN_MODIFY:
            MplsFTNModify (pLspInfo);
            break;
        case MPLS_MLIB_TNL_CREATE:
            MplsTnlCreate (pLspInfo);
            break;
        case MPLS_MLIB_TNL_DELETE:
            MplsTnlDelete (pLspInfo);
            break;
        case MPLS_MLIB_TNL_MODIFY:
            MplsTnlModify (pLspInfo);
            break;
        default:
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsMlibUpdate : Unknow Mlib Operation \t\n");
            MPLS_CMN_UNLOCK ();
            return MPLS_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   :MplsSigGetLSRInSegmentInfo
 *  Description     :Function exported to Signalling to provide InSegment
 *                   Info for the given InSegment Index 
 *  Input           :u4InIndex - InSegment Index
 *  Output          :pu4IfIndex - IfIndex of the InSegment
 *                   pu4Label - In Label of the InSegment  
 *  Returns         : MPLS_SUCCESS OnSuccess and MPLS_FAILURE on Failure  
 ************************************************************************/
INT4
MplsSigGetLSRInSegmentInfo (UINT4 u4InIndex, UINT4 *pu4IfIndex, UINT4 *pu4Label)
{
    tInSegment         *pInSegment = NULL;

    MPLS_CMN_LOCK ();
    pInSegment = MplsGetInSegmentTableEntry (u4InIndex);
    if (pInSegment != NULL)
    {
        *pu4IfIndex = pInSegment->u4IfIndex;
        *pu4Label = pInSegment->u4Label;
        MPLS_CMN_UNLOCK ();
        return MPLS_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();

    return MPLS_FAILURE;
}

/************************************************************************
 *  Function Name   :MplsSigGetLSRInSegmentIndex
 *  Description     :Function exported to Signalling to provide InSegment
 *                   Index for the given InIf and InLabel 
 *  Input           :u4Label - In Label u4IfIndex - In If
 *  Output          :pu4Index - InSegment Index
 *  Returns         : MPLS_SUCCESS OnSuccess and MPLS_FAILURE on Failure  
 ************************************************************************/
INT4
MplsSigGetLSRInSegmentIndex (UINT4 u4IfIndex, UINT4 u4Label, UINT4 *pu4Index)
{
    tInSegment         *pInSegment = NULL;

    MPLS_CMN_LOCK ();
    pInSegment = MplsSignalGetInSegmentTableEntry (u4IfIndex, u4Label);
    if (pInSegment != NULL)
    {
        *pu4Index = pInSegment->u4Index;
        MPLS_CMN_UNLOCK ();
        return MPLS_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();

    return MPLS_FAILURE;
}

/************************************************************************
 *  Function Name   :MplsSigGetLSROutSegmentInfo
 *  Description     :Function exported to Signalling to provide OutSegment
 *                   Info for the given OutSegment Index 
 *  Input           :u4InIndex - OutSegment Index
 *  Output          :pu4IfIndex - IfIndex of the OutSegment
 *                   pu4Label - Out Label of the OutSegment  
 *  Returns         : MPLS_SUCCESS OnSuccess and MPLS_FAILURE on Failure  
 ************************************************************************/
INT4
MplsSigGetLSROutSegmentInfo (UINT4 u4OutIndex, UINT4 *pu4IfIndex,
                             UINT4 *pu4Label)
{
    tOutSegment        *pOutSegment = NULL;

    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4OutIndex);
    if (pOutSegment != NULL)
    {
        *pu4IfIndex = pOutSegment->u4IfIndex;
        *pu4Label = pOutSegment->u4Label;
        MPLS_CMN_UNLOCK ();
        return MPLS_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();

    return MPLS_FAILURE;
}

/************************************************************************
 *  Function Name   :MplsSigGetLSROutSegmentIndex
 *  Description     :Function exported to Signalling to provide OutSegment
 *                   Index for the given OutIf and OutLabel 
 *  Input           :u4Label - Out Label u4IfIndex - Out IF
 *  Output          :pu4Index - OutSegment Index
 *  Returns         : MPLS_SUCCESS OnSuccess and MPLS_FAILURE on Failure  
 ************************************************************************/
INT4
MplsSigGetLSROutSegmentIndex (UINT4 u4IfIndex, UINT4 u4Label, UINT4 *pu4Index)
{
    tOutSegment        *pOutSegment = NULL;

    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentEntry (u4IfIndex, u4Label);
    if (pOutSegment != NULL)
    {
        *pu4Index = pOutSegment->u4Index;
        MPLS_CMN_UNLOCK ();
        return MPLS_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();

    return MPLS_FAILURE;
}

/************************************************************************
 *  Function Name   :MplsGetXCIndexFromPrefix
 *  Description     :Function to get XC Index from Address Prefix 
 *  Input           :u4Prefix - Address Prefix
 *  Output          :None 
 *  Returns         :XC Index OnSuccess and 0 on Failure  
 ************************************************************************/
UINT4
MplsGetXCIndexFromPrefix (tGenU4Addr * pPrefix)
{
    tXcEntry           *pXcEntry = NULL;
    tFtnEntry          *pFtnEntry = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4XcIndex = 0;

    if (pPrefix == NULL)
    {
        return u4XcIndex;
    }

    MPLS_CMN_LOCK ();

    switch (pPrefix->u2AddrType)
    {
        case IPV4:
            pFtnEntry = MplsSignalGetFtnTableEntry (pPrefix->Addr.u4Addr);
            break;
#ifdef MPLS_IPV6_WANTED
        case IPV6:
            pFtnEntry = MplsSignalGetFtnIpv6TableEntry (&pPrefix->Addr.Ip6Addr);
            break;
#endif
        default:
            break;
    };

    if ((pFtnEntry != NULL) && (pFtnEntry->pActionPtr != NULL))
    {
        if (pFtnEntry->u1HwStatus != MPLS_TRUE)
        {
            MPLS_CMN_UNLOCK ();
            return u4XcIndex;
        }
        if (pFtnEntry->i4ActionType == MPLS_FTN_ON_TE_LSP)
        {
            pTeTnlInfo = pFtnEntry->pActionPtr;
            pXcEntry = MplsGetXCEntryByDirection (pTeTnlInfo->u4TnlXcIndex,
                                                  MPLS_DEF_DIRECTION);
        }
        else
        {
            pXcEntry = pFtnEntry->pActionPtr;
        }
        if (pXcEntry != NULL)
        {
            u4XcIndex = pXcEntry->u4Index;
        }
    }
    MPLS_CMN_UNLOCK ();
    return u4XcIndex;
}

/************************************************************************
 *  Function Name   :LdpGetNonTeLspOutSegmentFromPrefix
 *  Description     :Function to get OutSeg Index from Address Prefix 
 *  Input           :Prefix - Address Prefix
 *  Output          :*pu4OutIfIndex  - Out If Index
 *  Returns         :Success and  Failure  
 ************************************************************************/
INT4
LdpGetNonTeLspOutSegmentFromPrefix (uGenU4Addr * pPrefix, UINT4 *pu4OutIndex)
{

    tFtnEntry          *pFtnEntry = NULL;
    tXcEntry           *pNonTeXcEntry = NULL;

    if (pPrefix == NULL)
    {
        return MPLS_FAILURE;
    }

    MPLS_CMN_LOCK ();

    pFtnEntry = MplsSignalGetFtnTableEntry (pPrefix->u4Addr);

    if ((pFtnEntry == NULL) ||
        (pFtnEntry->pActionPtr == NULL) ||
        (pFtnEntry->i4ActionType == MPLS_FTN_ON_TE_LSP))
    {
        MPLS_CMN_UNLOCK ();
        return MPLS_FAILURE;
    }

    /* If the FTN entry is not yet programmed in H/w, return failure */
    /* This wil also take care of whther the ARP is resolved or not */
    if (pFtnEntry->u1HwStatus != MPLS_TRUE)
    {
        MPLS_CMN_UNLOCK ();
        return MPLS_FAILURE;
    }

    /* Only search for non-stacked NON-TE LSP's */
    pNonTeXcEntry = pFtnEntry->pActionPtr;
    if (pNonTeXcEntry->mplsLabelIndex != NULL)
    {
        MPLS_CMN_UNLOCK ();
        return MPLS_FAILURE;
    }

    if (pNonTeXcEntry->pOutIndex == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return MPLS_FAILURE;
    }

    *pu4OutIndex = pNonTeXcEntry->pOutIndex->u4Index;
    MPLS_CMN_UNLOCK ();

    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   :MplsGetNonTeLspInfoFromPrefix
 *  Description     :Function to get XC Index from Address Prefix 
 *  Input           :u4Prefix - Address Prefix
 *                   bHwStatusReqd  - Hw status Required flag.
 *                                    If This flag is true FTN entry with
 *                                    matched prefix and Hw Status as true 
 *                                    will be retrieved.
 *  Output          :pu4XcIndex     - XC Index
 *                   pu4OutIfIndex  - Out If Index
 *                   pu1XcOwner     - XC Owner
 *  Returns         :XC Index OnSuccess and 0 on Failure  
 ************************************************************************/
INT4
MplsGetNonTeLspInfoFromPrefix (tGenU4Addr * pPrefix, UINT4 *pu4XcIndex,
                               UINT4 *pu4OutIfIndex, UINT1 *pu1XcOwner,
                               BOOL1 bHwStatusReqd)
{
    tXcEntry           *pXcEntry = NULL;
    tFtnEntry          *pFtnEntry = NULL;

    if (pPrefix == NULL)
    {
        return MPLS_FAILURE;
    }
    MPLS_CMN_LOCK ();
    switch (pPrefix->u2AddrType)
    {
        case IPV4:
            pFtnEntry = MplsSignalGetFtnTableEntry (pPrefix->Addr.u4Addr);
            break;
#ifdef MPLS_IPV6_WANTED
        case IPV6:
            pFtnEntry = MplsSignalGetFtnIpv6TableEntry (&pPrefix->Addr.Ip6Addr);
            break;
#endif
        default:
            break;
    };
    if ((pFtnEntry == NULL) ||
        (pFtnEntry->pActionPtr == NULL) ||
        (pFtnEntry->i4ActionType == MPLS_FTN_ON_TE_LSP))
    {
        MPLS_CMN_UNLOCK ();
        return MPLS_FAILURE;
    }

    if ((bHwStatusReqd == MPLS_TRUE) && (pFtnEntry->u1HwStatus != MPLS_TRUE))
    {
        MPLS_CMN_UNLOCK ();
        return MPLS_FAILURE;
    }

    pXcEntry = pFtnEntry->pActionPtr;
    if (pXcEntry->pOutIndex == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return MPLS_FAILURE;
    }
    *pu4XcIndex = pXcEntry->u4Index;
    *pu4OutIfIndex = pXcEntry->pOutIndex->u4IfIndex;
    *pu1XcOwner = pXcEntry->u1Owner;
    MPLS_CMN_UNLOCK ();
    return MPLS_SUCCESS;
}

INT4
MplsGetNonTeLspOutSegmentFromPrefix (UINT4 u4Prefix, UINT4 *pu4OutIndex)
{
    tFtnEntry          *pFtnEntry = NULL;
    tXcEntry           *pNonTeXcEntry = NULL;

    MPLS_CMN_LOCK ();
    pFtnEntry = MplsSignalGetFtnTableEntry (u4Prefix);
    if ((pFtnEntry == NULL) ||
        (pFtnEntry->pActionPtr == NULL) ||
        (pFtnEntry->i4ActionType == MPLS_FTN_ON_TE_LSP))
    {
        MPLS_CMN_UNLOCK ();
        return MPLS_FAILURE;
    }

    /* If the FTN entry is not yet programmed in H/w, return failure */
    /* This wil also take care of whther the ARP is resolved or not */
    if (pFtnEntry->u1HwStatus != MPLS_TRUE)
    {
        MPLS_CMN_UNLOCK ();
        return MPLS_FAILURE;
    }
    /* Only search for non-stacked NON-TE LSP's */
    pNonTeXcEntry = pFtnEntry->pActionPtr;
    if (pNonTeXcEntry->mplsLabelIndex != NULL)
    {
        MPLS_CMN_UNLOCK ();
        return MPLS_FAILURE;
    }

    if (pNonTeXcEntry->pOutIndex == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return MPLS_FAILURE;
    }
    *pu4OutIndex = pNonTeXcEntry->pOutIndex->u4Index;
    MPLS_CMN_UNLOCK ();
    return MPLS_SUCCESS;

}

#ifdef MPLS_L3VPN_WANTED
INT4
MplsGetRsvpTeLspOutSegmentFromPrefix (UINT4 u4Prefix, UINT4 *pu4OutIndex)
{

    tL3VpnRsvpMapLabelEntry *pL3VpnRsvpMapLabelEntry = NULL;
    tXcEntry           *pXcEntry = NULL;
    UINT4               u4Mask = 0xFFFFFFFF;
    UINT1               u1PrefixType = IPV4;
    UINT1               u1MaskType = IPV4;

    MPLS_CMN_LOCK ();

    pL3VpnRsvpMapLabelEntry = L3vpnGetRsvpMapLabelEntry (u1PrefixType,
                                                         u4Prefix, u1MaskType,
                                                         u4Mask);

    if (pL3VpnRsvpMapLabelEntry == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return MPLS_FAILURE;
    }
    if (pL3VpnRsvpMapLabelEntry->u4TnlIndex == 0)
    {
        MPLS_CMN_UNLOCK ();
        return MPLS_FAILURE;
    }
    if (pL3VpnRsvpMapLabelEntry->u4TnlXcIndex != 0)
    {
        if ((pXcEntry =
             MplsGetXCEntryByDirection (pL3VpnRsvpMapLabelEntry->u4TnlXcIndex,
                                        (eDirection) MPLS_DIRECTION_ANY)) ==
            NULL)
        {
            MPLS_CMN_UNLOCK ();
            return MPLS_FAILURE;
        }
        if (pXcEntry->pOutIndex == NULL)
        {
            MPLS_CMN_UNLOCK ();
            return MPLS_FAILURE;

        }
        *pu4OutIndex = pXcEntry->pOutIndex->u4Index;
    }
    else
    {
        MPLS_CMN_UNLOCK ();
        return MPLS_FAILURE;

    }
    MPLS_CMN_UNLOCK ();
    return MPLS_SUCCESS;
}

INT4
MplsCreateXcEntryForL3Vpn (UINT4 u4LspOutIndex, UINT4 u4VpnLabel,
                           UINT4 *pu4L3VpnXcIndex, UINT4 *pu4L3VpnOutSegIndex)
{
    tLblStkEntry       *pLblStkEntry = NULL;
    tLblEntry          *pLblEntry = NULL;
    UINT4               u4LblStkIndex = 0;
    UINT4               u4MplsLabelStackLabelIndex = 1;
    tXcEntry           *pXcEntry = NULL;
    UINT4               u4OutIndex = 0, u4XcIndex = 0;
    tOutSegment        *pL3VpnOutSegment = NULL;
    tOutSegment        *pOutSegment = NULL;
    UINT4               u4TempOutIndex = 0;

    MPLS_CMN_LOCK ();

    pOutSegment = MplsGetOutSegmentTableEntry (u4LspOutIndex);
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "OutSegment is NULL for underlying LSP\n");
        MPLS_CMN_UNLOCK ();
        return MPLS_FAILURE;
    }
    /* Allocating the outsegemnt index */
    u4OutIndex = MplsOutSegmentGetIndex ();
    if (u4OutIndex == 0)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsFTNCreate : OutSegment Table is Full \t\n");
        MPLS_CMN_UNLOCK ();
        return MPLS_FAILURE;
    }

    /* Allocating the Xc Index */
    u4XcIndex = MplsXcTableGetIndex ();
    if (u4XcIndex == 0)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsFTNCreate : Cross Connect Table is Full \t\n");
        MPLS_CMN_UNLOCK ();
        return MPLS_FAILURE;
    }
    u4XcIndex = MplsXCTableSetIndex (u4XcIndex);

    /* Creating the XC with insegemnt = NULL */
    pXcEntry = MplsCreateXCTableEntry (u4XcIndex, 0, u4OutIndex);
    if (pXcEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsFTNCreate : XC Create Failed \t\n");
        MPLS_CMN_UNLOCK ();
        return MPLS_FAILURE;
    }

    u4LblStkIndex = MplsLblStackGetIndex ();
    if (MplsLblStackSetIndex (u4LblStkIndex) != u4LblStkIndex)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsFTNCreate : MplsLblStackSetIndex Failed\n");
        MPLS_CMN_UNLOCK ();
        return MPLS_FAILURE;
    }
    pLblStkEntry = MplsCreateLblStkTableEntry (u4LblStkIndex);
    if (pLblStkEntry == NULL)
    {
        CMNDB_DBG
            (DEBUG_DEBUG_LEVEL,
             "MplsFTNCreate : MplsCreateLblStkTableEntry Failed \t\n");
        MPLS_CMN_UNLOCK ();
        return MPLS_FAILURE;
    }
    pLblEntry = MplsAddLblStkLabelEntry (u4LblStkIndex,
                                         u4MplsLabelStackLabelIndex);
    if (pLblEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsFTNCreate : MplsAddLblStkLabelEntry Failed \t\n");
        MPLS_CMN_UNLOCK ();
        return MPLS_FAILURE;
    }
    pLblEntry->u4Label = u4VpnLabel;
    pLblEntry->u1Storage = MPLS_STORAGE_NONVOLATILE;
    CMNDB_DBG1 (DEBUG_DEBUG_LEVEL, "MplsFTNCreate : OUT Stack Label=%d\n",
                pLblEntry->u4Label);
    pLblEntry->u1RowStatus = ACTIVE;

    pL3VpnOutSegment = pXcEntry->pOutIndex;
    u4TempOutIndex = pL3VpnOutSegment->u4Index;
    /* Copying the outsegment from the underlying LSP to the L3VPN outsegment */
    MEMCPY (pL3VpnOutSegment, pOutSegment, sizeof (tOutSegment));

    /* Stacking the VPN label in the XC entry */
    pXcEntry->mplsLabelIndex = pLblStkEntry;
    pXcEntry->u1RowStatus = ACTIVE;
    pXcEntry->u1Storage = MPLS_STORAGE_NONVOLATILE;
    pXcEntry->u1Owner = MPLS_OWNER_SNMP;
    pXcEntry->u1AdminStatus = MPLS_OPER_UP;
    pXcEntry->u1OperStatus = MPLS_OPER_UP;

    /* assigning values releavnt to L3VPN outsegment */
    pL3VpnOutSegment->pXcIndex = pXcEntry;
    pL3VpnOutSegment->u4Index = u4TempOutIndex;
    pL3VpnOutSegment->u1Owner = MPLS_OWNER_SNMP;
    MEMSET (&(pL3VpnOutSegment->OutSegPerf), 0, sizeof (tOutSegPerf));
    MEMSET (&(pL3VpnOutSegment->OutSegmentRBNode), 0, sizeof (tRBNodeEmbd));

    *pu4L3VpnXcIndex = pXcEntry->u4Index;
    *pu4L3VpnOutSegIndex = pL3VpnOutSegment->u4Index;
    MPLS_CMN_UNLOCK ();
    return MPLS_SUCCESS;

}

#endif
/************************************************************************
 *  Function Name   : MplsNonTePostEventForL2Vpn 
 *  Description     : Posts an NON-TE LSP changes to L2VPN
 *  Input           : u4XcIndex - XC index
 *                    u4Prefix - Prefix
 *                    u4EvtType - Event to be posted
 *  Output          : None 
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE 
 ************************************************************************/
INT4
MplsNonTePostEventForL2Vpn (UINT4 u4XcIndex, tGenU4Addr * pPrefix,
                            UINT4 u4EvtType)
{
#ifdef MPLS_L2VPN_WANTED
    tXcEntry           *pXcEntry = NULL;
    tLblEntry          *pLblEntry = NULL;
    tInSegment         *pInSegment = NULL;
    UINT4               u4TnlBkpMPLabel1 = MPLS_INVALID_LABEL;
    UINT4               u4TnlBkpMPLabel2 = MPLS_INVALID_LABEL;
    UINT4               u4TnlBkpMPLabel3 = MPLS_INVALID_LABEL;

    if (u4EvtType == L2VPN_MPLS_PWVC_TE_FRR_MP_DEL)
    {
        pXcEntry = MplsGetXCEntryByDirection (u4XcIndex, MPLS_DEF_DIRECTION);
        if ((pXcEntry == NULL) || (pXcEntry->pInIndex == NULL))
        {
            CMNDB_DBG (DEBUG_ERROR_LEVEL, "MplsNonTePostEventForL2Vpn : "
                       "XC entry does not exist\t\n");
            return MPLS_FAILURE;
        }
        pInSegment = pXcEntry->pInIndex;
        if (pInSegment->i4NPop == MPLS_ONE)
        {
            u4TnlBkpMPLabel1 = pInSegment->u4Label;
        }
        else if (pInSegment->i4NPop == MPLS_TWO)
        {
            pLblEntry = (tLblEntry *)
                TMO_SLL_First (&(pInSegment->mplsLabelIndex->LblList));
            if (pLblEntry == NULL)
            {
                return MPLS_FAILURE;
            }
            u4TnlBkpMPLabel1 = pLblEntry->u4Label;

            pLblEntry = (tLblEntry *)
                TMO_SLL_Last (&(pInSegment->mplsLabelIndex->LblList));
            if (pLblEntry == NULL)
            {
                return MPLS_FAILURE;
            }
            u4TnlBkpMPLabel2 = pLblEntry->u4Label;
        }
        else if (pInSegment->i4NPop == MPLS_THREE)
        {
            pLblEntry = (tLblEntry *)
                TMO_SLL_First (&(pInSegment->mplsLabelIndex->LblList));
            if (pLblEntry == NULL)
            {
                return MPLS_FAILURE;
            }
            u4TnlBkpMPLabel1 = pLblEntry->u4Label;

            pLblEntry = (tLblEntry *)
                TMO_SLL_Next (&(pInSegment->mplsLabelIndex->LblList),
                              &(pLblEntry->Next));
            if (pLblEntry == NULL)
            {
                return MPLS_FAILURE;
            }

            u4TnlBkpMPLabel2 = pLblEntry->u4Label;

            pLblEntry = (tLblEntry *)
                TMO_SLL_Last (&(pInSegment->mplsLabelIndex->LblList));
            if (pLblEntry == NULL)
            {
                return MPLS_FAILURE;
            }
            u4TnlBkpMPLabel3 = pLblEntry->u4Label;
        }
    }
    /* Send Event to L2VPN task. L2VPN will check this
     * XC index in its data base and if present then it
     * will make the Pseudowire Oper States to Up */

    L2VpnNonTeEventHandler (u4XcIndex,
                            pPrefix, u4TnlBkpMPLabel1, u4TnlBkpMPLabel2,
                            u4TnlBkpMPLabel3, u4EvtType);
#else
    UNUSED_PARAM (u4XcIndex);
    UNUSED_PARAM (Prefix);
    UNUSED_PARAM (u4EvtType);
#endif
    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   :MplsILMCreate 
 *  Description     :Function to create ILM Entry 
 *  Input           :pLspInfo - Info for Creating ILM 
 *  Output          :None 
 *  Returns         :MPLS_SUCCESS or MPLS_FAILURE 
 ************************************************************************/
INT4
MplsILMCreate (tLspInfo * pLspInfo)
{
    tInSegment         *pInSegment = NULL;
    tXcEntry           *pXcEntry = NULL;
    tOutSegment        *pOutSegment = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlInfo         *pTeStkTnlInfo = NULL;
    tTeTnlInfo         *pTeOutStkTnlInfo = NULL;
    tTeTnlInfo         *pTeInStkTnlInfo = NULL;
    tLblStkEntry       *pOutLblStkEntry = NULL;
    tLblStkEntry       *pInLblStkEntry = NULL;
    tLblEntry          *pLblEntry = NULL;
    tXcEntry           *pTnlXcEntry = NULL;
    tOutSegment        *pTnlOutSegment = NULL;
    tInSegment         *pTnlInSegment = NULL;
    UINT4               u4LblStkIndex = MPLS_ZERO;
    tGenU4Addr          Prefix;
    UINT4               u4InIndex = MPLS_ZERO;
    UINT4               u4OutIndex = MPLS_ZERO;
    UINT4               u4XcIndex = MPLS_ZERO;
    UINT4               u4InXcIndex1 = MPLS_ZERO;
    UINT4               u4InXcIndex2 = MPLS_ZERO;

    MEMSET (&Prefix, 0, sizeof (tGenU4Addr));

    MplsGetPrefix (&pLspInfo->FecParams.DestAddrPrefix,
                   &pLspInfo->FecParams.DestMask,
                   pLspInfo->FecParams.u2AddrType, &Prefix.Addr);

    Prefix.u2AddrType = pLspInfo->FecParams.u2AddrType;

#ifdef MPLS_IPV6_WANTED
    if (Prefix.u2AddrType == IPV6)
    {
        CMNDB_DBG2 (DEBUG_DEBUG_LEVEL, " MplsILMCreate : Prefix=%s Mask=%s\n",
                    Ip6PrintAddr (&Prefix.Addr.Ip6Addr),
                    Ip6PrintAddr (&pLspInfo->FecParams.DestMask.Ip6Addr));
    }
    else
#endif
    {
        CMNDB_DBG2 (DEBUG_DEBUG_LEVEL, " MplsILMCreate : Prefix=%x Mask=%x\n",
                    Prefix.Addr.u4Addr, pLspInfo->FecParams.DestMask.u4Addr);
    }

    if (pLspInfo->StackTnlInfo.u1StackTnlBit == TRUE)
    {
        pTeStkTnlInfo = TeGetTunnelInfo (pLspInfo->StackTnlInfo.u4TnlId,
                                         pLspInfo->StackTnlInfo.u4TnlInstance,
                                         pLspInfo->StackTnlInfo.u4IngressId,
                                         pLspInfo->StackTnlInfo.u4EgressId);
        if (pTeStkTnlInfo == NULL)
        {
            CMNDB_DBG4
                (DEBUG_DEBUG_LEVEL,
                 "MplsILMCreate : Tunnel %d %d %x %x does not exist in TE "
                 "Tunnel Table\n", pLspInfo->StackTnlInfo.u4TnlId,
                 pLspInfo->StackTnlInfo.u4TnlInstance,
                 pLspInfo->StackTnlInfo.u4IngressId,
                 pLspInfo->StackTnlInfo.u4EgressId);
            return MPLS_FAILURE;
        }
        CMNDB_DBG4
            (DEBUG_DEBUG_LEVEL,
             "MplsILMCreate : Tunnel %d %d %x %x for incoming " "traffic\n",
             pLspInfo->StackTnlInfo.u4TnlId,
             pLspInfo->StackTnlInfo.u4TnlInstance,
             pLspInfo->StackTnlInfo.u4IngressId,
             pLspInfo->StackTnlInfo.u4EgressId);

        pTnlXcEntry = MplsGetXCEntryByDirection (pTeStkTnlInfo->u4TnlXcIndex,
                                                 MPLS_DEF_DIRECTION);
        if ((pTnlXcEntry == NULL) || (pTnlXcEntry->pInIndex == NULL))
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsILMCreate : Stack Tnl XC entry not exist\t\n");
            return MPLS_FAILURE;
        }
        pTnlInSegment = pTnlXcEntry->pInIndex;
        CMNDB_DBG3 (DEBUG_DEBUG_LEVEL,
                    "MplsILMCreate : In intf %d In Tnl label %d "
                    "In Top label %d\n", pLspInfo->u4IfIndex,
                    pTnlInSegment->u4Label, pLspInfo->u4InTopLabel);

        pInSegment =
            MplsSignalGetTwoInSegmentLabelEntry (pLspInfo->u4IfIndex,
                                                 pTnlInSegment->u4Label,
                                                 pLspInfo->u4InTopLabel);
        if ((pInSegment != NULL) && (pLspInfo->u4BkpXcIndex != MPLS_ZERO))
        {
            MplsDbPrgFrrTnlMP (pLspInfo, pLspInfo->u4BkpXcIndex, &u4InXcIndex1);
            if (pInSegment->u4InMPXcIndex1 != MPLS_ZERO)
            {
                pInSegment->u4InMPXcIndex1 = u4InXcIndex1;
            }
            else
            {
                pInSegment->u4InMPXcIndex2 = u4InXcIndex1;
            }
            return MPLS_SUCCESS;
        }
        else if ((pInSegment == NULL) &&
                 (pLspInfo->u4InTopLabel == MPLS_IMPLICIT_NULL_LABEL))
        {

#ifdef MPLS_IPV6_WANTED
            if (Prefix.u2AddrType == MPLS_IPV6_ADDR_TYPE)
            {
                CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                            "Backup Path LDPoRSVP Implicit-Null MP Add indication "
                            "given to L2VPN for prefix %s\n",
                            Ip6PrintAddr (&Prefix.Addr.Ip6Addr));
            }
            else
#endif
            {
                CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                            "Backup Path LDPoRSVP Implicit-Null MP Add indication "
                            "given to L2VPN for prefix %x\n",
                            MPLS_IPV4_U4_ADDR (Prefix.Addr));
            }

            MplsNonTePostEventForL2Vpn (pLspInfo->u4BkpXcIndex, &Prefix,
                                        L2VPN_MPLS_PWVC_TE_FRR_MP_ADD);
            return MPLS_SUCCESS;
        }
    }
    else
    {
        CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                    "MplsILMCreate : In intf %d In Top label %d\n",
                    pLspInfo->u4IfIndex, pLspInfo->u4InTopLabel);
        pInSegment =
            MplsSignalGetInSegmentTableEntry (pLspInfo->u4IfIndex,
                                              pLspInfo->u4InTopLabel);

    }
    if (pInSegment != NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsILMCreate : ILM entry already present\t\n");
        return MPLS_FAILURE;
    }

    if ((pLspInfo->FecParams.u1FecType == RSVPTE_TNL_TYPE) ||
        (pLspInfo->FecParams.u1FecType == CRLSP_TNL_TYPE))
    {
        pTeTnlInfo = TeGetTunnelInfo (pLspInfo->FecParams.u4TnlId,
                                      pLspInfo->FecParams.u4TnlInstance,
                                      pLspInfo->FecParams.u4IngressId,
                                      pLspInfo->FecParams.u4EgressId);
        if (pTeTnlInfo == NULL)
        {
            CMNDB_DBG
                (DEBUG_DEBUG_LEVEL, "MplsILMCreate: Tunnel does not exist in "
                 "TE Tunnel Table \t\n");
            return MPLS_FAILURE;
        }
    }
    /* Double Stacking */
    if (pLspInfo->pNhlfe->StackTnlInfo.u1StackTnlBit == TRUE)
    {
        pTeOutStkTnlInfo =
            TeGetTunnelInfo (pLspInfo->pNhlfe->StackTnlInfo.u4TnlId,
                             pLspInfo->pNhlfe->StackTnlInfo.u4TnlInstance,
                             pLspInfo->pNhlfe->StackTnlInfo.u4IngressId,
                             pLspInfo->pNhlfe->StackTnlInfo.u4EgressId);
        if (pTeOutStkTnlInfo == NULL)
        {
            CMNDB_DBG
                (DEBUG_DEBUG_LEVEL, "MplsILMCreate : Tunnel does not exist "
                 "in TE Tunnel Table \t\n");
            return MPLS_FAILURE;
        }
        u4LblStkIndex = MplsLblStackGetIndex ();
        if (MplsLblStackSetIndex (u4LblStkIndex) != u4LblStkIndex)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsILMCreate : MplsLblStackSetIndex Failed\n");
            return MPLS_FAILURE;
        }
        pOutLblStkEntry = MplsCreateLblStkTableEntry (u4LblStkIndex);

        if (pOutLblStkEntry == NULL)
        {
            CMNDB_DBG
                (DEBUG_DEBUG_LEVEL,
                 "MplsILMCreate : MplsCreateLblStkTableEntry Failed \t\n");
            return MPLS_FAILURE;
        }

        pLblEntry = MplsAddLblStkLabelEntry (u4LblStkIndex, MPLS_ONE);
        if (pLblEntry == NULL)
        {
            CMNDB_DBG
                (DEBUG_DEBUG_LEVEL,
                 "MplsILMCreate :  MplsCreateLblStkTableEntry Failed \t\n");
            return MPLS_FAILURE;
        }

        pLblEntry->u4Label = pLspInfo->pNhlfe->u4OutLabel;
        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL, "MplsILMCreate : OUT Stack Label=%d\n",
                    pLblEntry->u4Label);
        pLblEntry->u1Storage = MPLS_STORAGE_VOLATILE;
        pLblEntry->u1RowStatus = ACTIVE;
    }
    if (pLspInfo->StackTnlInfo.u1StackTnlBit == TRUE)
    {
        pTeInStkTnlInfo = TeGetTunnelInfo (pLspInfo->StackTnlInfo.u4TnlId,
                                           pLspInfo->StackTnlInfo.u4TnlInstance,
                                           pLspInfo->StackTnlInfo.u4IngressId,
                                           pLspInfo->StackTnlInfo.u4EgressId);
        if (pTeInStkTnlInfo == NULL)
        {
            CMNDB_DBG
                (DEBUG_DEBUG_LEVEL, "MplsILMCreate : Tunnel does not exist "
                 "in TE Tunnel Table \t\n");
            return MPLS_FAILURE;
        }
        pTnlXcEntry = MplsGetXCEntryByDirection (pTeInStkTnlInfo->u4TnlXcIndex,
                                                 MPLS_DEF_DIRECTION);
        if ((pTnlXcEntry == NULL) || (pTnlXcEntry->pInIndex == NULL))
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsILMCreate : Stack XC entry not exist\t\n");
            return MPLS_FAILURE;
        }
        if (pTeInStkTnlInfo->u4BkpTnlMPXcIndex1 != MPLS_ZERO)
        {
            MplsDbPrgFrrTnlMP (pLspInfo,
                               pTeInStkTnlInfo->u4BkpTnlMPXcIndex1,
                               &u4InXcIndex1);
        }
        if (pTeInStkTnlInfo->u4BkpTnlMPXcIndex2 != MPLS_ZERO)
        {
            MplsDbPrgFrrTnlMP (pLspInfo,
                               pTeInStkTnlInfo->u4BkpTnlMPXcIndex2,
                               &u4InXcIndex2);
        }

        pTnlInSegment = pTnlXcEntry->pInIndex;

        u4LblStkIndex = MplsLblStackGetIndex ();
        if (MplsLblStackSetIndex (u4LblStkIndex) != u4LblStkIndex)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsILMCreate : MplsLblStackSetIndex Failed\n");
            return MPLS_FAILURE;
        }
        pInLblStkEntry = MplsCreateLblStkTableEntry (u4LblStkIndex);

        if (pInLblStkEntry == NULL)
        {
            CMNDB_DBG
                (DEBUG_DEBUG_LEVEL,
                 "MplsILMCreate : MplsCreateLblStkTableEntry Failed \t\n");
            return MPLS_FAILURE;
        }

        pLblEntry = MplsAddLblStkLabelEntry (u4LblStkIndex, MPLS_ONE);
        if (pLblEntry == NULL)
        {
            CMNDB_DBG
                (DEBUG_DEBUG_LEVEL,
                 "MplsILMCreate :  MplsCreateLblStkTableEntry Failed \t\n");
            return MPLS_FAILURE;
        }

        pLblEntry->u4Label = pTnlInSegment->u4Label;
        pLblEntry->u1Storage = MPLS_STORAGE_VOLATILE;
        pLblEntry->u1RowStatus = ACTIVE;

        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                    "MplsILMCreate : IN-TNL Stack Label-1=%d\n",
                    pLblEntry->u4Label);
        pLblEntry = MplsAddLblStkLabelEntry (u4LblStkIndex, MPLS_TWO);
        if (pLblEntry == NULL)
        {
            return MPLS_FAILURE;
        }
        pLblEntry->u4Label = pLspInfo->u4InTopLabel;
        pLblEntry->u1Storage = MPLS_STORAGE_VOLATILE;
        pLblEntry->u1RowStatus = ACTIVE;
        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL, "MplsILMCreate : IN Stack Label-2=%d\n",
                    pLblEntry->u4Label);
    }

    /* Create XC, InSegment and OutSegment Entries */
    u4InIndex = MplsInSegmentGetIndex ();
    if (u4InIndex == 0)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsILMCreate : InSegment Table is Full \t\n");
        return MPLS_FAILURE;
    }

    /* Get the XC index from TeTnlInfo
     * 1. if this ILM entry is related to tunnel and
     * 2. Tunnel is Bidirectional and
     * 3. Tunnel already linked to XcEntry i.e XCPointer in
     * mplsTunnelTable is already filled */
    if ((pTeTnlInfo != NULL) &&
        (pTeTnlInfo->u4TnlMode != TE_TNL_MODE_UNIDIRECTIONAL) &&
        (pTeTnlInfo->u4TnlXcIndex != 0))
    {
        u4XcIndex = pTeTnlInfo->u4TnlXcIndex;
    }
    else
    {
        u4XcIndex = MplsXcTableGetIndex ();
        if (u4XcIndex == 0)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsILMCreate : Cross Connect Table is Full \t\n");
            return MPLS_FAILURE;
        }

        u4XcIndex = MplsXCTableSetIndex (u4XcIndex);
    }

    if (pLspInfo->pNhlfe->u4OutIfIndex != 0)
    {
        u4OutIndex = MplsOutSegmentGetIndex ();
        if (u4OutIndex == 0)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsILMCreate : OutSegment Table is Full \t\n");
            return MPLS_FAILURE;
        }
    }

    pXcEntry = MplsCreateXCTableEntry (u4XcIndex, u4InIndex, u4OutIndex);
    if (pXcEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsILMCreate : XC Create Failed \t\n");
        return MPLS_FAILURE;
    }

    /* Update Required Params in InSegment, 
       OutSegment and XC Table */

    if (pLspInfo->StackTnlInfo.u1StackTnlBit == TRUE)
    {
        pInSegment = pXcEntry->pInIndex;
        u4InIndex = pInSegment->u4Index;
        MEMCPY (pInSegment, pTnlInSegment, sizeof (tInSegment));
        pInSegment->u4Index = u4InIndex;
        pInSegment->u4Label = 0;
        INSEGMENT_HW_STATUS (pInSegment) = FALSE;
        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                    "MplsILMCreate : XC Create In interface in stacking %d\t\n",
                    pLspInfo->u4IfIndex);
        pInSegment->u4IfIndex = pLspInfo->u4IfIndex;
        pInSegment->pXcIndex = pXcEntry;
        pInSegment->mplsLabelIndex = pInLblStkEntry;
        MplsInSegmentSignalRbTreeAdd (pInSegment);
        if (u4InXcIndex1)
        {
            pInSegment->u4InMPXcIndex1 = u4InXcIndex1;
        }
        if (u4InXcIndex2)
        {
            pInSegment->u4InMPXcIndex2 = u4InXcIndex2;
        }
        pInSegment->i4NPop = 2;    /* Double Pop */
    }
    else
    {
        pInSegment = pXcEntry->pInIndex;
        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                    "MplsILMCreate : XC Create In interface %d\t\n",
                    pLspInfo->u4IfIndex);
        pInSegment->u4IfIndex = pLspInfo->u4IfIndex;
        pInSegment->u4Label = pLspInfo->u4InTopLabel;
        pInSegment->mplsLabelIndex = NULL;
        MplsInSegmentSignalRbTreeAdd (pInSegment);

        pInSegment->i4NPop = 1;    /* Single Pop */

        switch (Prefix.u2AddrType)
        {
#ifdef MPLS_IPV6_WANTED
            case IPV6:
                pInSegment->u2AddrFmly = MPLS_IPV6_ADDR_TYPE;
                break;
#endif
            default:
                pInSegment->u2AddrFmly = MPLS_IPV4_ADDR_TYPE;
                break;
        };

        pInSegment->u1Owner = pLspInfo->u1Owner;
        pInSegment->u1RowStatus = ACTIVE;
        pInSegment->u1Storage = MPLS_STORAGE_VOLATILE;
        pInSegment->GmplsInSegment.InSegmentDirection = pLspInfo->Direction;
        pInSegment->pTrfIndex = NULL;
    }
#ifdef LDP_GR_WANTED
    pInSegment->ILMHwListFec.i4IpAddrType = pLspInfo->FecParams.u2AddrType;

#ifdef MPLS_IPV6_WANTED
    if (pLspInfo->FecParams.u2AddrType == MPLS_IPV6_ADDR_TYPE)
    {
        MEMCPY (&(pInSegment->ILMHwListFec.IpAddress),
                &(pLspInfo->FecParams.DestAddrPrefix), IPV6_ADDR_LENGTH);

        pInSegment->u1ILMHwListPrefLen =
            MplsGetIpv6Subnetmasklen ((UINT1 *) (&pLspInfo->FecParams.
                                                 DestMask));
    }
#endif
    if (pLspInfo->FecParams.u2AddrType == MPLS_IPV4_ADDR_TYPE)
    {
        MEMCPY (pInSegment->ILMHwListFec.IpAddress.au1Ipv4Addr,
                &(pLspInfo->FecParams.DestAddrPrefix.u4Addr), IPV4_ADDR_LENGTH);
        MPLS_GET_PRFX_LEN_FROM_MASK (pLspInfo->FecParams.DestMask.u4Addr,
                                     pInSegment->u1ILMHwListPrefLen);
    }
#endif

    if (pLspInfo->pNhlfe->u4OutIfIndex != 0)
    {
        if (pLspInfo->pNhlfe->StackTnlInfo.u1StackTnlBit == TRUE)
        {
            if (pTeOutStkTnlInfo == NULL)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "MplsILMCreate : XC from Stack tunnel failed \t\n");
                return MPLS_FAILURE;
            }
            pTnlXcEntry =
                MplsGetXCEntryByDirection (pTeOutStkTnlInfo->u4TnlXcIndex,
                                           pLspInfo->Direction);
            if ((pTnlXcEntry == NULL) || (pTnlXcEntry->pOutIndex == NULL))
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "MplsILMCreate : XC from Stack tunnel failed \t\n");
                return MPLS_FAILURE;
            }
            pOutSegment = pXcEntry->pOutIndex;
            pTnlOutSegment = pTnlXcEntry->pOutIndex;
            u4OutIndex = pOutSegment->u4Index;
            MEMCPY (pOutSegment, pTnlOutSegment, sizeof (tOutSegment));
            pOutSegment->u4Index = u4OutIndex;

            /* Make sure interface index in pLspInfo->pNhlfe->u4OutIfIndex is 
             * given my LDP and not by RSVP. 
             * This is required because RSVP Tunnel is a generic medium,
             * applications like LDP uses it for label stacking. */
            pOutSegment->u4IfIndex = pLspInfo->pNhlfe->u4OutIfIndex;
            pOutSegment->pXcIndex = pXcEntry;
            pXcEntry->mplsLabelIndex = pOutLblStkEntry;
            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                        "MplsILMCreate : OUT-TNL Stack Label=%d\n",
                        pOutSegment->u4Label);
        }
        else
        {
            pOutSegment = pXcEntry->pOutIndex;
            pOutSegment->u4IfIndex = pLspInfo->pNhlfe->u4OutIfIndex;
            if (pLspInfo->StackTnlInfo.u1StackTnlBit == TRUE)
            {
                CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                            "MplsILMCreate : OUT SWAP Label=%d\n",
                            pOutSegment->u4Label);
            }
            pOutSegment->bPushLabel = MPLS_SNMP_TRUE;
            pOutSegment->u1NHAddrType = pLspInfo->pNhlfe->u1NHAddrType;
            pOutSegment->u1RowStatus = ACTIVE;
            pOutSegment->u1Owner = pLspInfo->u1Owner;
            pOutSegment->pTrfIndex = NULL;
            switch (Prefix.u2AddrType)
            {
#ifdef MPLS_IPV6_WANTED
                case MPLS_IPV6_ADDR_TYPE:
                    MEMCPY (pOutSegment->NHAddr.u1_addr,
                            &pLspInfo->pNhlfe->NextHopAddr.Ip6Addr,
                            IPV6_ADDR_LENGTH);
                    break;
#endif
                default:
                    pOutSegment->NHAddr.u4_addr[0] =
                        pLspInfo->pNhlfe->NextHopAddr.u4Addr;
                    break;
            };
            pOutSegment->u1Storage = MPLS_STORAGE_VOLATILE;
            pOutSegment->GmplsOutSegment.OutSegmentDirection =
                pLspInfo->Direction;
            pXcEntry->mplsLabelIndex = NULL;

            if (pLspInfo->u1Owner != MPLS_OWNER_LDP)
            {
                MplsSetOutLabel (pOutSegment, pLspInfo->pNhlfe->u4OutLabel);
            }
            else
            {
                pOutSegment->u4Label = pLspInfo->pNhlfe->u4OutLabel;
            }
        }

    }
    if (pTeTnlInfo != NULL)
    {
        if (!(pLspInfo->StackTnlInfo.u2IsFrrMPByPassTnl))
        {
            MplsDbUpdateTunnelXCIndex (pTeTnlInfo, pXcEntry, pXcEntry->u4Index);
        }
    }
    pXcEntry->u1Owner = pLspInfo->u1Owner;
    pXcEntry->u1RowStatus = ACTIVE;
    pXcEntry->u1Storage = MPLS_STORAGE_VOLATILE;

    pXcEntry->u1AdminStatus = ACTIVE;
    pXcEntry->u1OperStatus = XC_OPER_UP;

    /* TODO: add check and allow MplsILMAdd for Egress ILM */
    if (MplsILMAdd (pInSegment, pTeTnlInfo) == MPLS_FAILURE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsILMCreate : MplsILMAdd Failed \t\n");
        return MPLS_FAILURE;
    }

    TeCmnExtUpdateArpResolveStatus (pTeTnlInfo, MPLS_MLIB_ILM_CREATE,
                                    pLspInfo->Direction,
                                    XC_ARP_STATUS (pXcEntry));

    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsDbPrgFrrTnlMP 
 *  Description     : LDP over RSVP ILM add for FRR merge point
 *  Input           : pLspInfo - LSP info.
 *                    u4XcIndex - backup tnl XC index
 *  Output          : pu4InXcIndex - XC index of backup path
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE

 ************************************************************************/
INT4
MplsDbPrgFrrTnlMP (tLspInfo * pLspInfo, UINT4 u4XcIndex, UINT4 *pu4InXcIndex)
{
    tInSegment         *pInSegment = NULL;
    tOutSegment        *pOutSegment = NULL;
    tOutSegment        *pTnlOutSegment = NULL;
    tXcEntry           *pXcEntry = NULL;
    UINT4               u4InIndex = 0;
    UINT4               u4OutIndex = 0;
    tLblEntry          *pLblEntry = NULL;
    tLblStkEntry       *pInLblStkEntry = NULL;
    UINT4               u4LblStkIndex = 0;
    tXcEntry           *pTnlXcEntry = NULL;
    tInSegment         *pTnlInSegment = NULL;
    tTeTnlInfo         *pTeOutStkTnlInfo = NULL;
    tLblStkEntry       *pOutLblStkEntry = NULL;
    tGenU4Addr          Prefix;

    MEMSET (&Prefix, 0, sizeof (tGenU4Addr));

    Prefix.Addr.u4Addr = pLspInfo->FecParams.DestAddrPrefix.u4Addr &
        pLspInfo->FecParams.DestMask.u4Addr;

    Prefix.u2AddrType = pLspInfo->FecParams.u2AddrType;

#ifdef VISHAL_FECPARAM
    CMNDB_DBG3 (DEBUG_DEBUG_LEVEL,
                " MplsDbPrgFrrTnlMP : Prefix=%x Mask=%x XC index=%d\n",
                u4Prefix, pLspInfo->FecParams.u4DestMask, u4XcIndex);
#endif

    pTnlXcEntry = MplsGetXCEntryByDirection (u4XcIndex, MPLS_DEF_DIRECTION);

    if ((pTnlXcEntry == NULL) || (pTnlXcEntry->pInIndex == NULL))
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsDbPrgFrrTnlMP : Stack XC entry not exist\t\n");
        return MPLS_FAILURE;
    }

    pTnlInSegment = pTnlXcEntry->pInIndex;

    u4LblStkIndex = MplsLblStackGetIndex ();
    if (MplsLblStackSetIndex (u4LblStkIndex) != u4LblStkIndex)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsDbPrgFrrTnlMP : MplsLblStackSetIndex Failed\n");
        return MPLS_FAILURE;
    }
    pInLblStkEntry = MplsCreateLblStkTableEntry (u4LblStkIndex);

    if (pInLblStkEntry == NULL)
    {
        CMNDB_DBG
            (DEBUG_DEBUG_LEVEL,
             "MplsDbPrgFrrTnlMP : MplsCreateLblStkTableEntry Failed \t\n");
        return MPLS_FAILURE;
    }

    pLblEntry = MplsAddLblStkLabelEntry (u4LblStkIndex, MPLS_ONE);
    if (pLblEntry == NULL)
    {
        CMNDB_DBG
            (DEBUG_DEBUG_LEVEL,
             "MplsDbPrgFrrTnlMP :  MplsCreateLblStkTableEntry Failed \t\n");
        return MPLS_FAILURE;
    }

    pLblEntry->u4Label = pTnlInSegment->u4Label;
    pLblEntry->u1Storage = MPLS_STORAGE_VOLATILE;
    pLblEntry->u1RowStatus = ACTIVE;

    CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                "MplsDbPrgFrrTnlMP : IN-TNL Stack Label-1=%d\n",
                pLblEntry->u4Label);
    pLblEntry = MplsAddLblStkLabelEntry (u4LblStkIndex, MPLS_TWO);
    if (pLblEntry == NULL)
    {
        return MPLS_FAILURE;
    }
    pLblEntry->u4Label = pLspInfo->u4InTopLabel;
    pLblEntry->u1Storage = MPLS_STORAGE_VOLATILE;
    pLblEntry->u1RowStatus = ACTIVE;
    CMNDB_DBG1 (DEBUG_DEBUG_LEVEL, "MplsDbPrgFrrTnlMP : IN Stack Label-2=%d\n",
                pLblEntry->u4Label);

    /* Create XC, InSegment and OutSegment Entries */
    u4InIndex = MplsInSegmentGetIndex ();
    if (u4InIndex == 0)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsDbPrgFrrTnlMP : InSegment Table is Full \t\n");
        return MPLS_FAILURE;
    }
    u4XcIndex = MplsXcTableGetIndex ();
    if (u4XcIndex == 0)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsDbPrgFrrTnlMP : Cross Connect Table is Full \t\n");
        return MPLS_FAILURE;
    }
    u4XcIndex = MplsXCTableSetIndex (u4XcIndex);

    if (pLspInfo->pNhlfe->u4OutIfIndex != 0)
    {
        u4OutIndex = MplsOutSegmentGetIndex ();
        if (u4OutIndex == 0)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsDbPrgFrrTnlMP : OutSegment Table is Full \t\n");
            return MPLS_FAILURE;
        }
    }

    pXcEntry = MplsCreateXCTableEntry (u4XcIndex, u4InIndex, u4OutIndex);
    if (pXcEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsDbPrgFrrTnlMP : XC Create Failed \t\n");
        return MPLS_FAILURE;
    }
    *pu4InXcIndex = u4XcIndex;
    /* Update Required Params in InSegment, 
       OutSegment and XC Table */

    pInSegment = pXcEntry->pInIndex;
    u4InIndex = pInSegment->u4Index;
    MEMCPY (pInSegment, pTnlInSegment, sizeof (tInSegment));
    pInSegment->u4Index = u4InIndex;
    pInSegment->u4Label = 0;
    pInSegment->u4IfIndex = pLspInfo->u4IfIndex;
    pInSegment->pXcIndex = pXcEntry;
    pInSegment->mplsLabelIndex = pInLblStkEntry;
    MplsInSegmentSignalRbTreeAdd (pInSegment);

    pInSegment->i4NPop = 2;        /* Double Pop */

    if (pLspInfo->pNhlfe->u4OutIfIndex != 0)
    {
        /* Double Stacking */
        if (pLspInfo->pNhlfe->StackTnlInfo.u1StackTnlBit == TRUE)
        {
            pTeOutStkTnlInfo =
                TeGetTunnelInfo (pLspInfo->pNhlfe->StackTnlInfo.u4TnlId,
                                 pLspInfo->pNhlfe->StackTnlInfo.u4TnlInstance,
                                 pLspInfo->pNhlfe->StackTnlInfo.u4IngressId,
                                 pLspInfo->pNhlfe->StackTnlInfo.u4EgressId);
            if (pTeOutStkTnlInfo == NULL)
            {
                CMNDB_DBG
                    (DEBUG_DEBUG_LEVEL,
                     "MplsDbPrgFrrTnlMP : Tunnel does not exist in TE "
                     "Tunnel Table \t\n");
                return MPLS_FAILURE;
            }
            u4LblStkIndex = MplsLblStackGetIndex ();
            if (MplsLblStackSetIndex (u4LblStkIndex) != u4LblStkIndex)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "MplsDbPrgFrrTnlMP : MplsLblStackSetIndex Failed\n");
                return MPLS_FAILURE;
            }
            pOutLblStkEntry = MplsCreateLblStkTableEntry (u4LblStkIndex);

            if (pOutLblStkEntry == NULL)
            {
                CMNDB_DBG
                    (DEBUG_DEBUG_LEVEL,
                     "MplsDbPrgFrrTnlMP : MplsCreateLblStkTableEntry "
                     "Failed \t\n");
                return MPLS_FAILURE;
            }

            pLblEntry = MplsAddLblStkLabelEntry (u4LblStkIndex, MPLS_ONE);
            if (pLblEntry == NULL)
            {
                CMNDB_DBG
                    (DEBUG_DEBUG_LEVEL,
                     "MplsDbPrgFrrTnlMP :  MplsCreateLblStkTableEntry "
                     "Failed \t\n");
                return MPLS_FAILURE;
            }

            pLblEntry->u4Label = pLspInfo->pNhlfe->u4OutLabel;
            pLblEntry->u1Storage = MPLS_STORAGE_VOLATILE;
            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                        "MplsDbPrgFrrTnlMP : OUT Stack Label=%d\n",
                        pLblEntry->u4Label);
            pLblEntry->u1RowStatus = ACTIVE;

            pTnlXcEntry =
                MplsGetXCEntryByDirection (pTeOutStkTnlInfo->u4TnlXcIndex,
                                           MPLS_DEF_DIRECTION);

            if ((pTnlXcEntry == NULL) || (pTnlXcEntry->pOutIndex == NULL))
            {
                CMNDB_DBG
                    (DEBUG_DEBUG_LEVEL,
                     "MplsDbPrgFrrTnlMP : XC from Stack tunnel failed \t\n");
                return MPLS_FAILURE;
            }
            pOutSegment = pXcEntry->pOutIndex;
            pTnlOutSegment = pTnlXcEntry->pOutIndex;
            u4OutIndex = pOutSegment->u4Index;
            MEMCPY (pOutSegment, pTnlOutSegment, sizeof (tOutSegment));
            pOutSegment->u4Index = u4OutIndex;

            /* Make sure interface index in pLspInfo->pNhlfe->u4OutIfIndex is 
             * given my LDP and not by RSVP. 
             * This is required because RSVP Tunnel is a generic medium,
             * applications like LDP uses it for label stacking. */
            pOutSegment->u4IfIndex = pLspInfo->pNhlfe->u4OutIfIndex;
            pXcEntry->mplsLabelIndex = pOutLblStkEntry;
            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                        "MplsDbPrgFrrTnlMP : OUT-TNL Stack Label=%d\n",
                        pOutSegment->u4Label);
        }
        else
        {
            pOutSegment = pXcEntry->pOutIndex;
            pOutSegment->u4IfIndex = pLspInfo->pNhlfe->u4OutIfIndex;
            if (pLspInfo->StackTnlInfo.u1StackTnlBit == TRUE)
            {
                CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                            "MplsDbPrgFrrTnlMP : OUT SWAP Label=%d\n",
                            pOutSegment->u4Label);
            }
            pOutSegment->bPushLabel = MPLS_SNMP_TRUE;
            pOutSegment->u1NHAddrType = pLspInfo->pNhlfe->u1NHAddrType;
            pOutSegment->u1RowStatus = ACTIVE;
            pOutSegment->u1Owner = pLspInfo->u1Owner;
            pOutSegment->pTrfIndex = NULL;
            pOutSegment->NHAddr.u4_addr[0] =
                pLspInfo->pNhlfe->NextHopAddr.u4Addr;
            pOutSegment->u1Storage = MPLS_STORAGE_VOLATILE;
            pXcEntry->mplsLabelIndex = NULL;
            MplsSetOutLabel (pOutSegment, pLspInfo->pNhlfe->u4OutLabel);
        }
    }

    pXcEntry->u1Owner = pLspInfo->u1Owner;
    pXcEntry->u1RowStatus = ACTIVE;
    pXcEntry->u1Storage = MPLS_STORAGE_VOLATILE;

    pXcEntry->u1AdminStatus = ACTIVE;
    pXcEntry->u1OperStatus = ACTIVE;

    if (MplsILMAdd (pInSegment, NULL) == MPLS_FAILURE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsDbPrgFrrTnlMP : MplsILMAdd Failed \t\n");
        return MPLS_FAILURE;
    }
    MplsNonTePostEventForL2Vpn (u4XcIndex, &Prefix,
                                L2VPN_MPLS_PWVC_TE_FRR_MP_ADD);

    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   :MplsILMModify 
 *  Description     :Function to Modify ILM Entry
 *  Input           :pLspInfo - Info for Modify ILM Entry
 *  Output          :None
 *  Returns         :MPLS_SUCCESS or MPLS_FAILURE

 ************************************************************************/
INT4
MplsILMModify (tLspInfo * pLspInfo)
{
    tInSegment         *pInSegment = NULL;
    tXcEntry           *pXcEntry = NULL;
    tOutSegment        *pOutSegment = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;

    tTeTnlInfo         *pTeStkTnlInfo = NULL;
    tLblStkEntry       *pLblStkEntry = NULL;
    tLblEntry          *pLblEntry = NULL;
    UINT4               u4LblStkIndex = 0;
    tXcEntry           *pTnlXcEntry = NULL;
    tOutSegment        *pTnlOutSegment = NULL;
    tInSegment         *pTnlInSegment = NULL;
    UINT4               u4OutIndex = 0;
    UINT4               u4InXcIndex1 = 0;
    UINT4               u4InXcIndex2 = 0;
    /* Creating only one Stack Label Index */
    UINT4               u4MplsLabelStackLabelIndex = 1;
    tGenU4Addr          Prefix;

    MEMSET (&Prefix, 0, sizeof (tGenU4Addr));

    MplsGetPrefix (&pLspInfo->FecParams.DestAddrPrefix,
                   &pLspInfo->FecParams.DestMask,
                   pLspInfo->FecParams.u2AddrType, &Prefix.Addr);

    Prefix.u2AddrType = pLspInfo->FecParams.u2AddrType;

#ifdef MPLS_IPV6_WANTED
    if (Prefix.u2AddrType == IPV6)
    {
        CMNDB_DBG2 (DEBUG_DEBUG_LEVEL, " MplsILMCreate : Prefix=%s Mask=%s\n",
                    Ip6PrintAddr (&Prefix.Addr.Ip6Addr),
                    Ip6PrintAddr (&pLspInfo->FecParams.DestMask.Ip6Addr));
    }
    else
#endif
    {
        CMNDB_DBG2 (DEBUG_DEBUG_LEVEL, " MplsILMCreate : Prefix=%x Mask=%x\n",
                    Prefix.Addr.u4Addr, pLspInfo->FecParams.DestMask.u4Addr);
    }

    if (pLspInfo->StackTnlInfo.u1StackTnlBit == TRUE)
    {
        pTeStkTnlInfo = TeGetTunnelInfo (pLspInfo->StackTnlInfo.u4TnlId,
                                         pLspInfo->StackTnlInfo.u4TnlInstance,
                                         pLspInfo->StackTnlInfo.u4IngressId,
                                         pLspInfo->StackTnlInfo.u4EgressId);
        if (pTeStkTnlInfo == NULL)
        {
            CMNDB_DBG
                (DEBUG_DEBUG_LEVEL, "MplsILMModify : Tunnel does not exist "
                 "in TE Tunnel Table \t\n");
            return MPLS_FAILURE;
        }
        pTnlXcEntry = MplsGetXCEntryByDirection (pTeStkTnlInfo->u4TnlXcIndex,
                                                 MPLS_DEF_DIRECTION);
        if ((pTnlXcEntry == NULL) || (pTnlXcEntry->pInIndex == NULL))
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsILMModify : ILM Tunnel XC entry not  present\t\n");
            return MPLS_FAILURE;
        }
        pTnlInSegment = pTnlXcEntry->pInIndex;
        pInSegment =
            MplsSignalGetTwoInSegmentLabelEntry (pLspInfo->u4IfIndex,
                                                 pTnlInSegment->u4Label,
                                                 pLspInfo->u4InTopLabel);
        if (pInSegment != NULL)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsILMModify : ILM entry present\t\n");

            if (pInSegment->u4InMPXcIndex1 != MPLS_ZERO)
            {
                MplsDeleteInXcOutEntry (pInSegment->u4InMPXcIndex1,
                                        pInSegment->GmplsInSegment.
                                        InSegmentDirection);
                pInSegment->u4InMPXcIndex1 = 0;
            }
            if (pInSegment->u4InMPXcIndex2 != MPLS_ZERO)
            {
                MplsDeleteInXcOutEntry (pInSegment->u4InMPXcIndex2,
                                        pInSegment->GmplsInSegment.
                                        InSegmentDirection);
                pInSegment->u4InMPXcIndex2 = 0;
            }

            if (pTeStkTnlInfo->u4BkpTnlMPXcIndex1 != MPLS_ZERO)
            {
                MplsDbPrgFrrTnlMP (pLspInfo,
                                   pTeStkTnlInfo->u4BkpTnlMPXcIndex1,
                                   &u4InXcIndex1);
            }
            if (pTeStkTnlInfo->u4BkpTnlMPXcIndex2 != MPLS_ZERO)
            {
                MplsDbPrgFrrTnlMP (pLspInfo,
                                   pTeStkTnlInfo->u4BkpTnlMPXcIndex2,
                                   &u4InXcIndex2);
            }
            if (u4InXcIndex1)
            {
                pInSegment->u4InMPXcIndex1 = u4InXcIndex1;
            }
            if (u4InXcIndex2)
            {
                pInSegment->u4InMPXcIndex2 = u4InXcIndex2;
            }
        }
    }
    else
    {
        pInSegment = MplsSignalGetInSegmentTableEntry (pLspInfo->u4IfIndex,
                                                       pLspInfo->u4InTopLabel);

    }

    if ((pInSegment == NULL) || (pInSegment->pXcIndex == NULL))
    {
        CMNDB_DBG
            (DEBUG_DEBUG_LEVEL,
             "MplsILMModify : ILM entry not present, so creating here\t\n");
        MplsILMCreate (pLspInfo);
        return MPLS_SUCCESS;
    }

#ifdef LDP_GR_WANTED
    pInSegment->ILMHwListFec.i4IpAddrType = pLspInfo->FecParams.u2AddrType;

#ifdef MPLS_IPV6_WANTED
    if (pLspInfo->FecParams.u2AddrType == MPLS_IPV6_ADDR_TYPE)
    {
        MEMCPY ((UINT1 *) &(pInSegment->ILMHwListFec.IpAddress),
                (UINT1 *) &(pLspInfo->FecParams.DestAddrPrefix),
                IPV6_ADDR_LENGTH);

        pInSegment->u1ILMHwListPrefLen =
            MplsGetIpv6Subnetmasklen ((UINT1 *) (&pLspInfo->FecParams.
                                                 DestMask));
    }
    else
#endif
    {
        MEMCPY (pInSegment->ILMHwListFec.IpAddress.au1Ipv4Addr,
                &(pLspInfo->FecParams.DestAddrPrefix.u4Addr), IPV4_ADDR_LENGTH);

        MPLS_GET_PRFX_LEN_FROM_MASK (pLspInfo->FecParams.DestMask.u4Addr,
                                     pInSegment->u1ILMHwListPrefLen);
    }
#endif

    pXcEntry = pInSegment->pXcIndex;
    if (MplsILMDel (pInSegment) == MPLS_FAILURE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsILMModify : MplsILMDel Failed \t\n");
        return MPLS_FAILURE;
    }
    if ((pLspInfo->FecParams.u1FecType == RSVPTE_TNL_TYPE) ||
        (pLspInfo->FecParams.u1FecType == CRLSP_TNL_TYPE))
    {
        pTeTnlInfo = TeGetTunnelInfo (pLspInfo->FecParams.u4TnlId,
                                      pLspInfo->FecParams.u4TnlInstance,
                                      pLspInfo->FecParams.u4IngressId,
                                      pLspInfo->FecParams.u4EgressId);
        if (pTeTnlInfo == NULL)
        {
            CMNDB_DBG
                (DEBUG_DEBUG_LEVEL,
                 "MplsILMModify : Tunnel does not exist in TE Tunnel Table\n");
            return MPLS_FAILURE;
        }
    }

    /* Double Stacking */

    if (pLspInfo->pNhlfe->StackTnlInfo.u1StackTnlBit == TRUE)
    {
        pTeStkTnlInfo = TeGetTunnelInfo (pLspInfo->pNhlfe->StackTnlInfo.u4TnlId,
                                         pLspInfo->pNhlfe->StackTnlInfo.
                                         u4TnlInstance,
                                         pLspInfo->pNhlfe->StackTnlInfo.
                                         u4IngressId,
                                         pLspInfo->pNhlfe->StackTnlInfo.
                                         u4EgressId);
        if (pTeStkTnlInfo == NULL)
        {
            CMNDB_DBG
                (DEBUG_DEBUG_LEVEL, "MplsILMModify : Tunnel does not exist "
                 "in TE Tunnel Table \t\n");
            return MPLS_FAILURE;
        }

        u4LblStkIndex = MplsLblStackGetIndex ();
        if (MplsLblStackSetIndex (u4LblStkIndex) != u4LblStkIndex)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsILMModify : MplsLblStackSetIndex Failed\n");
            return MPLS_FAILURE;
        }
        pLblStkEntry = MplsCreateLblStkTableEntry (u4LblStkIndex);

        if (pLblStkEntry == NULL)
        {
            CMNDB_DBG
                (DEBUG_DEBUG_LEVEL,
                 "MplsILMModify : MplsCreateLblStkTableEntry Failed \t\n");
            return MPLS_FAILURE;
        }

        pLblEntry = MplsAddLblStkLabelEntry (u4LblStkIndex,
                                             u4MplsLabelStackLabelIndex);
        if (pLblEntry == NULL)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsILMModify : MplsAddLblStkLabelEntry failed \t\n");
            return MPLS_FAILURE;
        }
        pLblEntry->u4Label = pLspInfo->pNhlfe->u4OutLabel;
        pLblEntry->u1Storage = MPLS_STORAGE_VOLATILE;
        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL, "MplsILMModify : OUT Stack Label=%d\n",
                    pLblEntry->u4Label);
        pLblEntry->u1RowStatus = ACTIVE;
    }

    pXcEntry = pInSegment->pXcIndex;
    pOutSegment = pXcEntry->pOutIndex;
    /* Update XC, InSegment and OutSegment Entries */
    if (pLspInfo->pNhlfe->u4OutIfIndex != 0)
    {
        if (pOutSegment == NULL)
        {
            u4OutIndex = MplsOutSegmentGetIndex ();
            if (u4OutIndex == 0)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "MplsILMModify : OutSegment Table is Full \t\n");
                return MPLS_FAILURE;
            }
            if (MplsOutSegmentSetIndex (u4OutIndex) != u4OutIndex)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "MplsILMModify : Unable to set OutSeg Index\t\n");
                return MPLS_FAILURE;
            }
            pXcEntry->pOutIndex = MplsCreateOutSegmentTableEntry (u4OutIndex);
            if (pXcEntry->pOutIndex == NULL)
            {
                CMNDB_DBG
                    (DEBUG_DEBUG_LEVEL,
                     "MplsILMModify : OutSegment Table Create Failed \t\n");
                return MPLS_FAILURE;
            }
            pOutSegment = pXcEntry->pOutIndex;
        }
        if (pLspInfo->pNhlfe->StackTnlInfo.u1StackTnlBit == TRUE)
        {
            pTnlXcEntry =
                MplsGetXCEntryByDirection (pTeStkTnlInfo->u4TnlXcIndex,
                                           MPLS_DEF_DIRECTION);
            if (pTnlXcEntry == NULL)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "MplsILMModify : Stack XC not present \t\n");
                return MPLS_FAILURE;
            }
            pOutSegment = pXcEntry->pOutIndex;
            pTnlOutSegment = pTnlXcEntry->pOutIndex;
            u4OutIndex = pOutSegment->u4Index;
            MEMCPY (pOutSegment, pTnlOutSegment, sizeof (tOutSegment));
            pOutSegment->u4Index = u4OutIndex;

            /* Make sure interface index in pLspInfo->pNhlfe->u4OutIfIndex is 
             * given my LDP and not by RSVP. 
             * This is required because RSVP Tunnel is a generic medium,
             * applications like LDP uses it for label stacking. */
            pOutSegment->u4IfIndex = pLspInfo->pNhlfe->u4OutIfIndex;
            pOutSegment->pXcIndex = pXcEntry;
            pXcEntry->mplsLabelIndex = pLblStkEntry;
            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                        "MplsILMModify : OUT-TNL Stack Label=%d\n",
                        pOutSegment->u4Label);
        }
        else
        {
            pOutSegment->u4IfIndex = pLspInfo->pNhlfe->u4OutIfIndex;
            switch (Prefix.u2AddrType)
            {
#ifdef MPLS_IPV6_WANTED
                case MPLS_IPV6_ADDR_TYPE:
                    MEMCPY (pOutSegment->NHAddr.u1_addr,
                            &pLspInfo->pNhlfe->NextHopAddr.Ip6Addr,
                            IPV6_ADDR_LENGTH);
                    break;
#endif
                default:
                    pOutSegment->NHAddr.u4_addr[0] =
                        pLspInfo->pNhlfe->NextHopAddr.u4Addr;
                    break;
            };

            pOutSegment->u1NHAddrType = pLspInfo->pNhlfe->u1NHAddrType;
            if (pTeTnlInfo != NULL)
            {
                MplsDbUpdateTunnelXCIndex (pTeTnlInfo, pXcEntry,
                                           pXcEntry->u4Index);
                if ((pTeTnlInfo->u1TnlIsIf == TE_TRUE) && (pOutSegment != NULL))
                {
                    pOutSegment->u4IfIndex = pTeTnlInfo->u4TnlIfIndex;
                }
            }
            pXcEntry->mplsLabelIndex = NULL;
            if (pLspInfo->u1Owner != MPLS_OWNER_LDP)
            {
                MplsSetOutLabel (pOutSegment, pLspInfo->pNhlfe->u4OutLabel);
            }
            else
            {
                pOutSegment->u4Label = pLspInfo->pNhlfe->u4OutLabel;
            }
        }

        if (pTeTnlInfo != NULL)
        {
            MplsDbUpdateTunnelXCIndex (pTeTnlInfo, pXcEntry, pXcEntry->u4Index);
        }
        if (pLspInfo->pNhlfe->u1OperStatus == NOT_IN_SERVICE)
        {
            pXcEntry->u1OperStatus = NOT_IN_SERVICE;
        }
        else
        {
            pXcEntry->pOutIndex->u1RowStatus = ACTIVE;
            if (MplsILMAdd (pInSegment, pTeTnlInfo) == MPLS_FAILURE)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "MplsILMModify : MplsILMAdd Failed \t\n");
                return MPLS_FAILURE;
            }
            pXcEntry->u1OperStatus = ACTIVE;
        }
    }
    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   :MplsILMDelete
 *  Description     :Function to Delete ILM Entry
 *  Input           :pLspInfo - Info for delete ILM Entry 
 *  Output          :None
 *  Returns         :MPLS_SUCCESS or MPLS_FAILURE

 ************************************************************************/
INT4
MplsILMDelete (tLspInfo * pLspInfo)
{
    tInSegment         *pInSegment = NULL;
    tInSegment         *pTnlInSegment = NULL;
    tInSegment         *pBkpInSegment = NULL;
    tXcEntry           *pXcEntry = NULL;
    tXcEntry           *pTnlXcEntry = NULL;
    tXcEntry           *pBkpXcEntry = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlInfo         *pTeStkTnlInfo = NULL;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;
    tGenU4Addr          Prefix;

    MEMSET (&Prefix, 0, sizeof (tGenU4Addr));

    MplsGetPrefix (&pLspInfo->FecParams.DestAddrPrefix,
                   &pLspInfo->FecParams.DestMask,
                   pLspInfo->FecParams.u2AddrType, &Prefix.Addr);

    Prefix.u2AddrType = pLspInfo->FecParams.u2AddrType;

#ifdef MPLS_IPV6_WANTED
    if (Prefix.u2AddrType == IPV6)
    {
        CMNDB_DBG2 (DEBUG_DEBUG_LEVEL, " MplsILMCreate : Prefix=%s Mask=%s\n",
                    Ip6PrintAddr (&Prefix.Addr.Ip6Addr),
                    Ip6PrintAddr (&pLspInfo->FecParams.DestMask.Ip6Addr));
    }
    else
#endif
    {
        CMNDB_DBG2 (DEBUG_DEBUG_LEVEL, " MplsILMCreate : Prefix=%x Mask=%x\n",
                    Prefix.Addr.u4Addr, pLspInfo->FecParams.DestMask.u4Addr);
    }

    if (pLspInfo->StackTnlInfo.u1StackTnlBit == TRUE)
    {
        pTeStkTnlInfo = TeGetTunnelInfo (pLspInfo->StackTnlInfo.u4TnlId,
                                         pLspInfo->StackTnlInfo.u4TnlInstance,
                                         pLspInfo->StackTnlInfo.u4IngressId,
                                         pLspInfo->StackTnlInfo.u4EgressId);
        if (pTeStkTnlInfo == NULL)
        {
            CMNDB_DBG
                (DEBUG_DEBUG_LEVEL, "MplsILMDelete : Tunnel does not exist "
                 "in TE Tunnel Table \t\n");

            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL, "In tunnel label =%d\n",
                        pLspInfo->u4TnlLabel);
            pInSegment =
                MplsSignalGetTwoInSegmentLabelEntry (pLspInfo->u4IfIndex,
                                                     pLspInfo->u4TnlLabel,
                                                     pLspInfo->u4InTopLabel);
            if (pInSegment == NULL)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "MplsILMDelete : IN segment not present\t\n");
                return MPLS_FAILURE;
            }
        }
        if (pInSegment == NULL)
        {
            pTnlXcEntry =
                MplsGetXCEntryByDirection (pTeStkTnlInfo->u4TnlXcIndex,
                                           MPLS_DEF_DIRECTION);
            if (pTnlXcEntry == NULL)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "MplsILMDelete : Stack tunnel XC not present\t\n");
                pInSegment =
                    MplsSignalGetTwoInSegmentLabelEntry (pLspInfo->u4IfIndex,
                                                         pLspInfo->u4TnlLabel,
                                                         pLspInfo->
                                                         u4InTopLabel);
                if (pInSegment == NULL)
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               "MplsILMDelete : IN segment not present\t\n");
                    return MPLS_FAILURE;
                }
            }
            else
            {
                pTnlInSegment = pTnlXcEntry->pInIndex;

                pInSegment =
                    MplsSignalGetTwoInSegmentLabelEntry (pLspInfo->u4IfIndex,
                                                         pTnlInSegment->u4Label,
                                                         pLspInfo->
                                                         u4InTopLabel);
            }
        }
    }
    else
    {
        pInSegment = MplsSignalGetInSegmentTableEntry (pLspInfo->u4IfIndex,
                                                       pLspInfo->u4InTopLabel);

    }

    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsILMDelete : ILM entry not present\t\n");
        if (pLspInfo->u4BkpXcIndex == MPLS_ZERO)
        {
            return MPLS_FAILURE;
        }
        if (pLspInfo->u4InTopLabel != MPLS_IMPLICIT_NULL_LABEL)
        {
            return MPLS_FAILURE;
        }
        pXcEntry = MplsGetXCEntryByDirection (pLspInfo->u4BkpXcIndex,
                                              MPLS_DEF_DIRECTION);
        if ((pXcEntry == NULL) || (pXcEntry->pInIndex == NULL))
        {
#ifdef MPLS_L2VPN_WANTED
            L2VpnNonTeEventHandler (pLspInfo->u4BkpXcIndex,
                                    &Prefix, pLspInfo->u4TnlLabel,
                                    MPLS_INVALID_LABEL,
                                    MPLS_INVALID_LABEL,
                                    L2VPN_MPLS_PWVC_TE_FRR_MP_DEL);
#endif
        }
        else
        {
#ifdef MPLS_IPV6_WANTED
            if (Prefix.u2AddrType == MPLS_IPV6_ADDR_TYPE)
            {

                CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                            "Backup path LDPoRSVP Implicit-Null MP Del indication "
                            "given to L2VPN for prefix %s\n",
                            Ip6PrintAddr (&Prefix.Addr.Ip6Addr));
            }
            else
#endif
            {
                CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                            "Backup path LDPoRSVP Implicit-Null MP Del indication "
                            "given to L2VPN for prefix %x\n",
                            MPLS_IPV4_U4_ADDR (Prefix.Addr));
            }

            /* LDP Implicit-null case, Tnl XC Index needs to be posted to
             * L2VPN. */
            MplsNonTePostEventForL2Vpn (pLspInfo->u4BkpXcIndex,
                                        &Prefix, L2VPN_MPLS_PWVC_TE_FRR_MP_DEL);
        }
        return MPLS_SUCCESS;
    }

#ifdef LDP_GR_WANTED
    pInSegment->ILMHwListFec.i4IpAddrType = pLspInfo->FecParams.u2AddrType;

#ifdef MPLS_IPV6_WANTED
    if (pLspInfo->FecParams.u2AddrType == MPLS_IPV6_ADDR_TYPE)
    {
        MEMCPY ((UINT1 *) &(pInSegment->ILMHwListFec.IpAddress),
                (UINT1 *) &(pLspInfo->FecParams.DestAddrPrefix),
                IPV6_ADDR_LENGTH);

        pInSegment->u1ILMHwListPrefLen =
            MplsGetIpv6Subnetmasklen ((UINT1 *) (&pLspInfo->FecParams.
                                                 DestMask));
    }
    else
#endif
    {
        MEMCPY (pInSegment->ILMHwListFec.IpAddress.au1Ipv4Addr,
                &(pLspInfo->FecParams.DestAddrPrefix.u4Addr), IPV4_ADDR_LENGTH);

        MPLS_GET_PRFX_LEN_FROM_MASK (pLspInfo->FecParams.DestMask.u4Addr,
                                     pInSegment->u1ILMHwListPrefLen);
    }
#endif

    if ((pInSegment != NULL) && (pLspInfo->u4BkpXcIndex != MPLS_ZERO)
        && (pLspInfo->StackTnlInfo.u1StackTnlBit == TRUE))
    {
        /* u4BkpXcIndex is Tnl XC Index. Get XC Entry associated with it. */
        pTnlXcEntry = MplsGetXCEntryByDirection (pLspInfo->u4BkpXcIndex,
                                                 MPLS_DEF_DIRECTION);

        if ((pTnlXcEntry == NULL) || (pTnlXcEntry->pInIndex == NULL))
        {
#ifdef VISHAL_FECPARAM
            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL, "Tnl Xc Entry Null for prefix %x\n",
                        u4Prefix);
#endif
#ifdef MPLS_L2VPN_WANTED
            L2VpnNonTeEventHandler (pLspInfo->u4BkpXcIndex,
                                    &Prefix, pLspInfo->u4TnlLabel,
                                    pLspInfo->u4InTopLabel,
                                    MPLS_INVALID_LABEL,
                                    L2VPN_MPLS_PWVC_TE_FRR_MP_DEL);
#endif
            pBkpInSegment =
                MplsSignalGetTwoInSegmentLabelEntry (pLspInfo->u4IfIndex,
                                                     pLspInfo->u4TnlLabel,
                                                     pLspInfo->u4InTopLabel);
            if (pBkpInSegment == NULL)
            {
#ifdef VISHAL_FECPARAM
                CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                            "Backup In Segment Null for prefix %x\n", u4Prefix);
#endif
                return MPLS_FAILURE;
            }

            if (pInSegment->u4InMPXcIndex1 ==
                ((tXcEntry *) pBkpInSegment->pXcIndex)->u4Index)
            {
                pInSegment->u4InMPXcIndex1 = MPLS_ZERO;
            }
            else
            {
                pInSegment->u4InMPXcIndex2 = MPLS_ZERO;
            }
            MplsDeleteInXcOutEntry
                (((tXcEntry *) pBkpInSegment->pXcIndex)->u4Index,
                 pBkpInSegment->GmplsInSegment.InSegmentDirection);

            return MPLS_SUCCESS;
        }

        pTnlInSegment = pTnlXcEntry->pInIndex;

        pBkpInSegment =
            MplsSignalGetTwoInSegmentLabelEntry (pLspInfo->u4IfIndex,
                                                 pTnlInSegment->u4Label,
                                                 pLspInfo->u4InTopLabel);

        if (pBkpInSegment == NULL)
        {
#ifdef MPLS_IPV6_WANTED
            if (Prefix.u2AddrType == MPLS_IPV6_ADDR_TYPE)
            {
                CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                            "Backup In Segment Null for prefix %s\n",
                            Ip6PrintAddr (&Prefix.Addr.Ip6Addr));
            }
            else
#endif
            {
                CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                            "Backup In Segment Null for prefix %x\n",
                            MPLS_IPV4_U4_ADDR (Prefix.Addr));
            }
            return MPLS_FAILURE;
        }

        pBkpXcEntry = pBkpInSegment->pXcIndex;
        MplsNonTePostEventForL2Vpn (pBkpXcEntry->u4Index,
                                    &Prefix, L2VPN_MPLS_PWVC_TE_FRR_MP_DEL);
        MplsDeleteInXcOutEntry (pBkpXcEntry->u4Index,
                                pBkpInSegment->GmplsInSegment.
                                InSegmentDirection);

        if (pInSegment->u4InMPXcIndex1 == pBkpXcEntry->u4Index)
        {
            pInSegment->u4InMPXcIndex1 = MPLS_ZERO;
        }
        else
        {
            pInSegment->u4InMPXcIndex2 = MPLS_ZERO;
        }
        return MPLS_SUCCESS;
    }

    if ((pLspInfo->FecParams.u1FecType == RSVPTE_TNL_TYPE) ||
        (pLspInfo->FecParams.u1FecType == CRLSP_TNL_TYPE))
    {
        pTeTnlInfo = TeGetTunnelInfo (pLspInfo->FecParams.u4TnlId,
                                      pLspInfo->FecParams.u4TnlInstance,
                                      pLspInfo->FecParams.u4IngressId,
                                      pLspInfo->FecParams.u4EgressId);
        if (pTeTnlInfo == NULL)
        {
            CMNDB_DBG
                (DEBUG_DEBUG_LEVEL, "MplsILMDelete: Tunnel does not exist "
                 "in TE Tunnel Table \t\n");
            return MPLS_FAILURE;
        }
        if (pTeTnlInfo->u4TnlXcIndex == MPLS_ZERO)
        {
            CMNDB_DBG
                (DEBUG_DEBUG_LEVEL,
                 "MplsILMDelete:: Tunnel is not attached to a XC Entry \r\n");
            return MPLS_FAILURE;
        }
#ifdef MPLS_SIG_WANTED
        if (TE_LDP_OVER_RSVP_ENT_INDEX (pTeTnlInfo) != MPLS_ZERO)
        {
            CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTeTnlInfo)),
                                u4IngressId);
            CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTeTnlInfo)), u4EgressId);
            CMNDB_DBG5 (DEBUG_DEBUG_LEVEL,
                        "EXTN: Tunnel %d %d %x %x DOWN Indication given to LDP "
                        "Entity %d\n", TE_TNL_TNL_INDEX (pTeTnlInfo),
                        TE_TNL_TNL_INSTANCE (pTeTnlInfo),
                        OSIX_NTOHL (u4IngressId), OSIX_NTOHL (u4EgressId),
                        TE_LDP_OVER_RSVP_ENT_INDEX (pTeTnlInfo));

            LdpTnlOperUpDnHdlForLdpOverRsvp
                (TE_TNL_TNL_INDEX (pTeTnlInfo),
                 TE_TNL_TNL_INSTANCE (pTeTnlInfo),
                 OSIX_NTOHL (u4IngressId),
                 OSIX_NTOHL (u4EgressId),
                 TE_LDP_OVER_RSVP_ENT_INDEX (pTeTnlInfo),
                 pTeTnlInfo->u4TnlXcIndex, TE_OPER_DOWN, pTeTnlInfo->u1TnlRole);
        }
#endif
    }
#ifdef LDP_GR_WANTED
    pInSegment->u1IsNPBlock = pLspInfo->u1IsNPBlock;
#endif
    if (pInSegment->u4InMPXcIndex1)
    {
        MplsNonTePostEventForL2Vpn (pInSegment->u4InMPXcIndex1, &Prefix,
                                    L2VPN_MPLS_PWVC_TE_FRR_MP_DEL);
        MplsDeleteInXcOutEntry (pInSegment->u4InMPXcIndex1,
                                pInSegment->GmplsInSegment.InSegmentDirection);
    }
    if (pInSegment->u4InMPXcIndex2)
    {
        MplsNonTePostEventForL2Vpn (pInSegment->u4InMPXcIndex2, &Prefix,
                                    L2VPN_MPLS_PWVC_TE_FRR_MP_DEL);
        MplsDeleteInXcOutEntry (pInSegment->u4InMPXcIndex2,
                                pInSegment->GmplsInSegment.InSegmentDirection);
    }
    pXcEntry = pInSegment->pXcIndex;
    MplsDeleteInXcOutEntry (pXcEntry->u4Index,
                            pInSegment->GmplsInSegment.InSegmentDirection);

    TeCmnExtUpdateArpResolveStatus (pTeTnlInfo, MPLS_MLIB_ILM_DELETE,
                                    pLspInfo->Direction, MPLS_ZERO);

    /* Make the Tunnel XC Index as zero, if there are no more XC associations
     * forward or reverse with the tunnel. */
    if ((pLspInfo->FecParams.u1FecType == RSVPTE_TNL_TYPE) ||
        (pLspInfo->FecParams.u1FecType == CRLSP_TNL_TYPE))
    {
        if (MplsGetXCEntryByDirection (pTeTnlInfo->u4TnlXcIndex,
                                       (eDirection) MPLS_DIRECTION_ANY) == NULL)
        {
            MplsDbUpdateTunnelXCIndex (pTeTnlInfo, NULL, MPLS_ZERO);
        }
    }
    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   :MplsDeleteInXcOutEntry
 *  Description     :Function to delete the Insegment, Xc and Outsegment entries
 *  Input           :u4XcIndex -> XC index
 *                   Direction -> Direction of the segment.
 *  Output          :None
 *  Returns         :None

 ************************************************************************/
VOID
MplsDeleteInXcOutEntry (UINT4 u4XcIndex, eDirection Direction)
{
    tXcEntry           *pXcEntry = NULL;
    tInSegment         *pInSegment = NULL;
    tOutSegment        *pOutSegment = NULL;

    CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                "MplsDeleteInXcOutEntry: Xc Index =%d to be deleted \t\n",
                u4XcIndex);
    pXcEntry = MplsGetXCEntryByDirection (u4XcIndex, Direction);
    if (pXcEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsDeleteInXcOutEntry: XC Entry does not exist \t\n");
        return;
    }
    pInSegment = pXcEntry->pInIndex;
    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsDeleteInXcOutEntry: In Entry does not exist \t\n");
        return;
    }

    /* MPLS_HW_ACTION_POP_SEARCH label action case, 
     * L2VPN will take care of deleting this entry..so skip it here.*/
    if (pInSegment->u1LblAction != MPLS_HW_ACTION_POP_SEARCH)
    {
#ifdef LDP_GR_WANTED
        if (pInSegment->u1IsNPBlock == MPLS_FALSE)
        {
            if (MplsILMDel (pInSegment) == MPLS_FAILURE)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "MplsILMDelete :MplsILMDel Failed \t\n");
            }
        }
        else
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsDeleteInXcOutEntry: ILM NPAPI Deletion Blocked "
                       "Since the Module SHUTDOWN is in Progress and GR is Enabled\n");
        }
#else
        if (MplsILMDel (pInSegment) == MPLS_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsILMDelete :MplsILMDel Failed \t\n");
        }
#endif
    }
    pOutSegment = pXcEntry->pOutIndex;
    /* Delete XC, InSegment and OutSegment Entries */
    if (pXcEntry->mplsLabelIndex != NULL)
    {
        MplsDeleteLblStkAndLabelEntries (pXcEntry->mplsLabelIndex);
        pXcEntry->mplsLabelIndex = NULL;
    }
    if (pInSegment->mplsLabelIndex != NULL)
    {
        MplsDeleteLblStkAndLabelEntries (pInSegment->mplsLabelIndex);
        pInSegment->mplsLabelIndex = NULL;
    }

    MplsDeleteXCTableEntry (pXcEntry);
    MplsDeleteInSegmentTableEntry (pInSegment);
    if (pOutSegment != NULL)
    {
        MplsDeleteOutSegmentTableEntry (pOutSegment);
    }
    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
               "MplsDeleteInXcOutEntry: Deletion is successful \t\n");
}

/************************************************************************
 *  Function Name   :MplsRegOrDeRegNonTeLspWithL2Vpn
 *  Description     :Function to Reg or Dereg this FTN with L2VPN
 *  Input           :u4PeerAddr - L2VPN peer info, 
 *                   bIsRegFlag  
 *                       TRUE - Register
 *                       FALSE - Deregister
 *  Output          :None
 *  Returns         :MPLS_SUCCESS or MPLS_FAILURE

 ************************************************************************/
INT4
MplsRegOrDeRegNonTeLspWithL2Vpn (tGenU4Addr * pPeerAddr, BOOL1 bIsRegFlag)
{
    tFtnEntry          *pFtnEntry = NULL;

    if (pPeerAddr == NULL)
    {
        return MPLS_FAILURE;

    }

    MPLS_CMN_LOCK ();

    switch (pPeerAddr->u2AddrType)
    {
        case IPV4:
            pFtnEntry = MplsSignalGetFtnTableEntry (pPeerAddr->Addr.u4Addr);
            break;
#ifdef MPLS_IPV6_WANTED
        case IPV6:
            pFtnEntry =
                MplsSignalGetFtnIpv6TableEntry (&pPeerAddr->Addr.Ip6Addr);
            break;
#endif
        default:
            break;
    };

    if (pFtnEntry == NULL)
    {
        CMNDB_DBG
            (DEBUG_DEBUG_LEVEL,
             "MplsRegOrDeRegNonTeLspWithL2Vpn : NON-TE LSP is not present\t\n");
        MPLS_CMN_UNLOCK ();
        return MPLS_FAILURE;
    }
    if (bIsRegFlag)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsRegOrDeRegNonTeLspWithL2Vpn : "
                   "FTN registration done \t\n");
        pFtnEntry->u1IsLspUsedByL2VPN = TRUE;
    }
    else
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsRegOrDeRegNonTeLspWithL2Vpn : "
                   "FTN deregistration done \t\n");
        pFtnEntry->u1IsLspUsedByL2VPN = FALSE;
    }

    MPLS_CMN_UNLOCK ();
    return MPLS_SUCCESS;
}

#ifdef MPLS_L3VPN_WANTED
INT4
MplsRegOrDeRegNonTeLspWithL3Vpn (UINT4 u4PeerAddr, BOOL1 bIsRegFlag)
{
    tFtnEntry          *pFtnEntry = NULL;

    MPLS_CMN_LOCK ();
    pFtnEntry = MplsSignalGetFtnTableEntry (u4PeerAddr);
    if (pFtnEntry == NULL)
    {
        CMNDB_DBG
            (DEBUG_DEBUG_LEVEL,
             "MplsRegOrDeRegNonTeLspWithL2Vpn : NON-TE LSP is not present\t\n");
        MPLS_CMN_UNLOCK ();
        return MPLS_FAILURE;
    }
    if (bIsRegFlag)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsRegOrDeRegNonTeLspWithL2Vpn : "
                   "FTN registration done \t\n");
        pFtnEntry->u1IsLspUsedByL3VPN = TRUE;
    }
    else
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsRegOrDeRegNonTeLspWithL2Vpn : "
                   "FTN deregistration done \t\n");
        pFtnEntry->u1IsLspUsedByL3VPN = FALSE;
    }

    MPLS_CMN_UNLOCK ();
    return MPLS_SUCCESS;
}
#endif

/************************************************************************
 *  Function Name   :MplsFTNCreate
 *  Description     :Function to Create FTN Entry
 *  Input           :pLspInfo - Info for create FTN Entry
 *  Output          :None
 *  Returns         :MPLS_SUCCESS or MPLS_FAILURE

 ************************************************************************/
INT4
MplsFTNCreate (tLspInfo * pLspInfo)
{
    tFtnEntry          *pFtnEntry = NULL;
    tXcEntry           *pXcEntry = NULL;
    tOutSegment        *pOutSegment = NULL;
    /* Commenting pFtnMapEntry to fix coverity warning */
    /*tFtnMapEntry       *pFtnMapEntry = NULL; */
    tTeTnlInfo         *pTeStkTnlInfo = NULL;
    tLblStkEntry       *pLblStkEntry = NULL;
    tLblEntry          *pLblEntry = NULL;
    UINT4               u4LblStkIndex = 0;
    /* Creating only one Stack Label Index */
    UINT4               u4MplsLabelStackLabelIndex = 1;
    tXcEntry           *pTnlXcEntry = NULL;
    tOutSegment        *pTnlOutSegment = NULL;
    BOOL1               bIsHostAddrFecForPSN = FALSE;

    UINT4               u4FtnIndex = 0;
    UINT4               u4OutIndex = 0, u4XcIndex = 0;
    tGenU4Addr          Prefix;

    MEMSET (&Prefix, 0, sizeof (tGenAddr));

    MplsGetPrefix (&pLspInfo->FecParams.DestAddrPrefix,
                   &pLspInfo->FecParams.DestMask,
                   pLspInfo->FecParams.u2AddrType, &Prefix.Addr);
    Prefix.u2AddrType = pLspInfo->FecParams.u2AddrType;

#ifdef MPLS_IPV6_WANTED
    if (Prefix.u2AddrType == MPLS_IPV6_ADDR_TYPE)
    {
        CMNDB_DBG2 (DEBUG_DEBUG_LEVEL, "MplsFTNCreate : Prefix=%s Mask=%s\n",
                    Ip6PrintAddr (&Prefix.Addr.Ip6Addr),
                    Ip6PrintAddr (&pLspInfo->FecParams.DestMask.Ip6Addr));
    }
    else
#endif
    {
        CMNDB_DBG2 (DEBUG_DEBUG_LEVEL, "MplsFTNCreate : Prefix=%x Mask=%x\n",
                    MPLS_IPV4_U4_ADDR (Prefix.Addr),
                    MPLS_IPV4_U4_ADDR (pLspInfo->FecParams.DestMask));
    }
    switch (Prefix.u2AddrType)
    {
#ifdef MPLS_IPV6_WANTED
        case IPV6:
            pFtnEntry = MplsSignalGetFtnIpv6TableEntry (&Prefix.Addr.Ip6Addr);
            break;
#endif
        default:
            pFtnEntry = MplsSignalGetFtnTableEntry (Prefix.Addr.u4Addr);
            break;
    };
    if (pFtnEntry != NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsFTNCreate : FTN entry already present\t\n");
        return MPLS_FAILURE;
    }

    /* Double Stacking */
    if (pLspInfo->pNhlfe->StackTnlInfo.u1StackTnlBit == TRUE)
    {
        pTeStkTnlInfo = TeGetTunnelInfo (pLspInfo->pNhlfe->StackTnlInfo.u4TnlId,
                                         pLspInfo->pNhlfe->StackTnlInfo.
                                         u4TnlInstance,
                                         pLspInfo->pNhlfe->StackTnlInfo.
                                         u4IngressId,
                                         pLspInfo->pNhlfe->StackTnlInfo.
                                         u4EgressId);
        if (pTeStkTnlInfo == NULL)
        {
            CMNDB_DBG
                (DEBUG_DEBUG_LEVEL, "MplFTNCreate : Tunnel does not exist "
                 "in TE Tunnel Table \t\n");
            return MPLS_FAILURE;
        }
        /* Below check is to avoid the RSVP signalling messages via MPLS switching 
         * when the FTN is configured for the tunnel egress address. 
         * If this is allowed, RSVP packets will not be processed 
         * in the intermediate hops and this results in RSVP tunnel tear down */

        if (Prefix.Addr.u4Addr == (pLspInfo->pNhlfe->StackTnlInfo.u4EgressId
                                   & pLspInfo->FecParams.DestMask.u4Addr))
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsFTNCreate : Prefix and Tunnel egress address are "
                       "in same network, so not allowed \n");
            if (pLspInfo->FecParams.DestMask.u4Addr == 0xffffffff)
            {
                bIsHostAddrFecForPSN = TRUE;
            }
            else
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "MplsFTNCreate : FEC is NOT host address \n");
                return MPLS_FAILURE;
            }
        }
        u4LblStkIndex = MplsLblStackGetIndex ();
        if (MplsLblStackSetIndex (u4LblStkIndex) != u4LblStkIndex)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsFTNCreate : MplsLblStackSetIndex Failed\n");
            return MPLS_FAILURE;
        }
        pLblStkEntry = MplsCreateLblStkTableEntry (u4LblStkIndex);

        if (pLblStkEntry == NULL)
        {
            CMNDB_DBG
                (DEBUG_DEBUG_LEVEL,
                 "MplsFTNCreate : MplsCreateLblStkTableEntry Failed \t\n");
            return MPLS_FAILURE;
        }

        pLblEntry = MplsAddLblStkLabelEntry (u4LblStkIndex,
                                             u4MplsLabelStackLabelIndex);
        if (pLblEntry == NULL)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsFTNCreate : MplsAddLblStkLabelEntry Failed \t\n");
            return MPLS_FAILURE;
        }
        pLblEntry->u4Label = pLspInfo->pNhlfe->u4OutLabel;
        pLblEntry->u1Storage = MPLS_STORAGE_VOLATILE;
        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL, "MplsFTNCreate : OUT Stack Label=%d\n",
                    pLblEntry->u4Label);
        pLblEntry->u1RowStatus = ACTIVE;
    }

    /* Create XC, FTN and OutSegment Entries */
    u4FtnIndex = MplsFtnGetIndex ();
    if (u4FtnIndex == 0)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsFTNCreate : Ftn Table is Full \t\n");
        return MPLS_FAILURE;
    }
    u4FtnIndex = MplsFtnSetIndex (u4FtnIndex);
    u4OutIndex = MplsOutSegmentGetIndex ();
    if (u4OutIndex == 0)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsFTNCreate : OutSegment Table is Full \t\n");
        return MPLS_FAILURE;
    }
    u4XcIndex = MplsXcTableGetIndex ();
    if (u4XcIndex == 0)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsFTNCreate : Cross Connect Table is Full \t\n");
        return MPLS_FAILURE;
    }
    u4XcIndex = MplsXCTableSetIndex (u4XcIndex);
    pFtnEntry = MplsCreateFtnTableEntry (u4FtnIndex);
    if (pFtnEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsFTNCreate : FTN Create Failed \t\n");
        return MPLS_FAILURE;
    }
    pXcEntry = MplsCreateXCTableEntry (u4XcIndex, 0, u4OutIndex);
    if (pXcEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsFTNCreate : XC Create Failed \t\n");
        return MPLS_FAILURE;
    }

    /* TO-DO convert the Prefix and Dest Mask into
     * Min and Max Range values and store instead of 
     * this
     */
    switch (Prefix.u2AddrType)
    {
#ifdef MPLS_IPV6_WANTED
        case IPV6:
            MEMCPY (pFtnEntry->DestAddrMin.u1_addr, &Prefix.Addr.Ip6Addr,
                    IPV6_ADDR_LENGTH);
            MEMCPY (pFtnEntry->DestAddrMax.u1_addr,
                    &pLspInfo->FecParams.DestMask.Ip6Addr, IPV6_ADDR_LENGTH);
            pFtnEntry->u1AddrType = MPLS_IPV6_ADDR_TYPE;
            break;
#endif
        default:
            pFtnEntry->DestAddrMin.u4_addr[0] = Prefix.Addr.u4Addr;
            pFtnEntry->DestAddrMax.u4_addr[0] =
                pLspInfo->FecParams.DestMask.u4Addr;

            pFtnEntry->u1AddrType = MPLS_IPV4_ADDR_TYPE;

            break;
    };

    pFtnEntry->u1FtnMask = MPLS_FTN_DST_BIT_MASK;
    pFtnEntry->i4ActionType = MPLS_FTN_ON_NON_TE_LSP;
    pFtnEntry->u1Storage = MPLS_STORAGE_VOLATILE;
    pFtnEntry->pActionPtr = pXcEntry;
    pFtnEntry->u1RowStatus = ACTIVE;
    pFtnEntry->bIsHostAddrFecForPSN = bIsHostAddrFecForPSN;
    if (pLspInfo->pNhlfe->StackTnlInfo.u1StackTnlBit == TRUE)
    {
        pTnlXcEntry = MplsGetXCEntryByDirection (pTeStkTnlInfo->u4TnlXcIndex,
                                                 MPLS_DEF_DIRECTION);
        if (pTnlXcEntry == NULL)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsFTNCreate : Stack tunnel XC not present \t\n");
            return MPLS_FAILURE;
        }
        pOutSegment = pXcEntry->pOutIndex;
        pTnlOutSegment = pTnlXcEntry->pOutIndex;
        u4OutIndex = pOutSegment->u4Index;
        MEMCPY (pOutSegment, pTnlOutSegment, sizeof (tOutSegment));
        pOutSegment->u4Index = u4OutIndex;

        /* Make sure interface index in pLspInfo->pNhlfe->u4OutIfIndex is 
         * given my LDP and not by RSVP. 
         * This is required because RSVP Tunnel is a generic medium,
         * applications like LDP uses it for label stacking. */
        pOutSegment->u4IfIndex = pLspInfo->pNhlfe->u4OutIfIndex;
        pOutSegment->pXcIndex = pXcEntry;
        MEMCPY (pXcEntry->pOutIndex->au1NextHopMac,
                pTeStkTnlInfo->au1NextHopMac, MAC_ADDR_LEN);
        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                    "MplsFTNCreate : OUT-TNL Stack Label=%d\n",
                    pOutSegment->u4Label);
        pXcEntry->mplsLabelIndex = pLblStkEntry;
    }
    else
    {
        pOutSegment = pXcEntry->pOutIndex;
        pOutSegment->u4IfIndex = pLspInfo->pNhlfe->u4OutIfIndex;
        pOutSegment->bPushLabel = MPLS_SNMP_TRUE;
        pOutSegment->u1NHAddrType = pLspInfo->pNhlfe->u1NHAddrType;
        pOutSegment->u1RowStatus = ACTIVE;
        pOutSegment->u1Owner = pLspInfo->u1Owner;
        pOutSegment->pTrfIndex = NULL;

        switch (Prefix.u2AddrType)
        {
#ifdef MPLS_IPV6_WANTED
            case MPLS_IPV6_ADDR_TYPE:
                MEMCPY (pOutSegment->NHAddr.u1_addr,
                        &pLspInfo->pNhlfe->NextHopAddr.Ip6Addr,
                        IPV6_ADDR_LENGTH);
                break;
#endif
            default:
                pOutSegment->NHAddr.u4_addr[0] =
                    pLspInfo->pNhlfe->NextHopAddr.u4Addr;
                break;
        };

        pOutSegment->u1Storage = MPLS_STORAGE_VOLATILE;
        pXcEntry->mplsLabelIndex = NULL;
        MEMSET (pXcEntry->pOutIndex->au1NextHopMac, 0, MAC_ADDR_LEN);

        if (pLspInfo->u1Owner != MPLS_OWNER_LDP)
        {
            MplsSetOutLabel (pOutSegment, pLspInfo->pNhlfe->u4OutLabel);
        }
        else
        {
            pOutSegment->u4Label = pLspInfo->pNhlfe->u4OutLabel;
        }
    }
    pXcEntry->u1Owner = pLspInfo->u1Owner;
    pXcEntry->u1RowStatus = ACTIVE;
    pXcEntry->u1Storage = MPLS_STORAGE_VOLATILE;

    switch (Prefix.u2AddrType)
    {
#ifdef MPLS_IPV6_WANTED
        case MPLS_IPV6_ADDR_TYPE:
            MplsRegisterEntrywithIpv6Trie (pFtnEntry);
            break;
#endif
        default:
            MplsRegisterEntrywithTrie (pFtnEntry);
            break;
    };

    /* Commenting the below code to fix coveriy warning */
    /*pFtnMapEntry = MplsCreateFtnMapTableEntry (pLspInfo->u4IfIndex, u4FtnIndex);
       if (pFtnMapEntry == NULL)
       {
       CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsFTNCreate : MplsCreateFtnMapTableEntry  Failed \t\n");
       return MPLS_FAILURE;
       } */
    pXcEntry->u1AdminStatus = ACTIVE;
    pXcEntry->u1OperStatus = ACTIVE;

    if (MplsFTNAdd (pFtnEntry) == MPLS_FAILURE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsFTNCreate : MplsFTNAdd  Failed \t\n");
        return MPLS_FAILURE;
    }
    if (pLspInfo->bIsUsedByL2vpn)
    {
        pFtnEntry->u1IsLspUsedByL2VPN = TRUE;

#ifdef MPLS_IPV6_WANTED
        if (Prefix.u2AddrType == MPLS_IPV6_ADDR_TYPE)
        {
            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                        "PLR ADD event to posted to L2VPN for %x\n",
                        Ip6PrintAddr (&Prefix.Addr.Ip6Addr));
        }
        else
#endif
        {
            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                        "PLR ADD event to posted to L2VPN for %x\n",
                        MPLS_IPV4_U4_ADDR (Prefix.Addr));
        }

        MplsNonTePostEventForL2Vpn (pXcEntry->u4Index, &Prefix,
                                    L2VPN_MPLS_PWVC_TE_FRR_PLR_ADD);
    }
    else
    {

#ifdef MPLS_IPV6_WANTED
        if (Prefix.u2AddrType == MPLS_IPV6_ADDR_TYPE)
        {
            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                        "LSP UP event to posted to L2VPN for %x\n",
                        Ip6PrintAddr (&Prefix.Addr.Ip6Addr));
        }
        else
#endif
        {
            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                        "LSP UP event to posted to L2VPN for %x\n",
                        MPLS_IPV4_U4_ADDR (Prefix.Addr));
        }
        MplsNonTePostEventForL2Vpn (pXcEntry->u4Index, &Prefix,
                                    L2VPN_MPLS_PWVC_LSP_UP);
    }
    MplsFtnUpdateSysTime ();
    MplsFtnMapUpdateSysTime ();
    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   :MplsFTNModify
 *  Description     :Function to Modify FTN Entry
 *  Input           :pLspInfo - Info for Modify FTN Entry
 *  Output          :None
 *  Returns         :MPLS_SUCCESS or MPLS_FAILURE

 ************************************************************************/
INT4
MplsFTNModify (tLspInfo * pLspInfo)
{
    tFtnEntry          *pFtnEntry = NULL;
    tXcEntry           *pXcEntry = NULL;
    tTeTnlInfo         *pTeStkTnlInfo = NULL;
    tLblStkEntry       *pLblStkEntry = NULL;
    tLblEntry          *pLblEntry = NULL;
    tOutSegment        *pOutSegment = NULL;
    /* Commenting the below variable to fix coverity warning */
    /*tFtnMapEntry       *pFtnMapEntry = NULL; */
    tXcEntry           *pTnlXcEntry = NULL;
    tOutSegment        *pTnlOutSegment = NULL;
    /* Creating only one Stack Label Index */
    UINT4               u4MplsLabelStackLabelIndex = 1;
    UINT4               u4FtnIndex = 0;
    UINT4               u4XcIndex = 0;
    UINT4               u4OutIndex = 0;
    UINT4               u4LblStkIndex = 0;
    BOOL1               bIsHostAddrFecForPSN = FALSE;

    tGenU4Addr          Prefix;

    MEMSET (&Prefix, 0, sizeof (tGenU4Addr));

    MplsGetPrefix (&pLspInfo->FecParams.DestAddrPrefix,
                   &pLspInfo->FecParams.DestMask,
                   pLspInfo->FecParams.u2AddrType, &Prefix.Addr);

    Prefix.u2AddrType = pLspInfo->FecParams.u2AddrType;

#ifdef MPLS_IPV6_WANTED
    if (Prefix.u2AddrType == MPLS_IPV6_ADDR_TYPE)
    {
        CMNDB_DBG2 (DEBUG_DEBUG_LEVEL, "MplsFTNModify : Prefix=%x Mask=%x\n",
                    Ip6PrintAddr (&Prefix.Addr.Ip6Addr),
                    Ip6PrintAddr (&pLspInfo->FecParams.DestMask.Ip6Addr));
    }
    else
#endif
    {
        CMNDB_DBG2 (DEBUG_DEBUG_LEVEL, "MplsFTNModify : Prefix=%x Mask=%x\n",
                    MPLS_IPV4_U4_ADDR (Prefix.Addr),
                    MPLS_IPV4_U4_ADDR (pLspInfo->FecParams.DestMask));
    }

    MplsNonTePostEventForL2Vpn (MPLS_ZERO, &Prefix, L2VPN_MPLS_PWVC_LSP_DOWN);

    switch (Prefix.u2AddrType)
    {
#ifdef MPLS_IPV6_WANTED
        case IPV6:
            pFtnEntry = MplsSignalGetFtnIpv6TableEntry (&Prefix.Addr.Ip6Addr);
            break;
#endif
        default:
            pFtnEntry = MplsSignalGetFtnTableEntry (Prefix.Addr.u4Addr);
            break;

    };

    if (pFtnEntry != NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsFTNModify  : FTN entry exists \t\n");

        pXcEntry = pFtnEntry->pActionPtr;
        if (pXcEntry == NULL)
        {
            return MPLS_FAILURE;
        }
#ifdef MPLS_L3VPN_WANTED
        if (pFtnEntry->u1IsLspUsedByL3VPN)
        {
            /* Give Notification only for Non-stacked LSP's */
            if (pXcEntry->mplsLabelIndex == NULL)
            {
                if (pXcEntry->pOutIndex != NULL)
                {
                    L3VpnNonTeLspStatusChangeEventHandler (pXcEntry->pOutIndex->
                                                           u4Label,
                                                           Prefix.Addr.u4Addr,
                                                           L3VPN_MPLS_LSP_DOWN);
                }
                else
                {
#ifdef MPLS_IPV6_WANTED
                    if (Prefix.u2AddrType == MPLS_IPV6_ADDR_TYPE)
                    {

                        CMNDB_DBG1
                            (DEBUG_DEBUG_LEVEL,
                             "Cannot post Event to L3VPN since outsegment "
                             "is NULL for the Prefix: %s\n",
                             Ip6PrintAddr (&Prefix.Addr.Ip6Addr));
                    }
                    else
#endif
                    {
                        CMNDB_DBG1
                            (DEBUG_DEBUG_LEVEL,
                             "Cannot post Event to L3VPN since outsegment "
                             "is NULL for the Prefix: %d\n",
                             MPLS_IPV4_U4_ADDR (Prefix.Addr));
                    }
                }
            }
        }
#endif
        /* Double Stacking */
        if (pLspInfo->pNhlfe->StackTnlInfo.u1StackTnlBit == TRUE)
        {
            pTeStkTnlInfo =
                TeGetTunnelInfo (pLspInfo->pNhlfe->StackTnlInfo.u4TnlId,
                                 pLspInfo->pNhlfe->StackTnlInfo.u4TnlInstance,
                                 pLspInfo->pNhlfe->StackTnlInfo.u4IngressId,
                                 pLspInfo->pNhlfe->StackTnlInfo.u4EgressId);
            if (pTeStkTnlInfo == NULL)
            {
                CMNDB_DBG
                    (DEBUG_DEBUG_LEVEL, "MplsFTNModify : Tunnel does not exist "
                     "in TE Tunnel Table \t\n");
                return MPLS_FAILURE;
            }

            if (pXcEntry->mplsLabelIndex != NULL)
            {
                pLblEntry = (tLblEntry *)
                    TMO_SLL_First (&(pXcEntry->mplsLabelIndex->LblList));
                if (pLblEntry == NULL)
                {
                    CMNDB_DBG
                        (DEBUG_DEBUG_LEVEL,
                         "MplsFTNModify : Label entry does not exist "
                         "in Label stack table \t\n");
                    return MPLS_FAILURE;
                }
                pLblStkEntry = pXcEntry->mplsLabelIndex;
            }
            else
            {
                u4LblStkIndex = MplsLblStackGetIndex ();
                if (MplsLblStackSetIndex (u4LblStkIndex) != u4LblStkIndex)
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               "MplsFTNModify : MplsLblStackSetIndex Failed\n");
                    return MPLS_FAILURE;
                }
                pLblStkEntry = MplsCreateLblStkTableEntry (u4LblStkIndex);

                if (pLblStkEntry == NULL)
                {
                    CMNDB_DBG
                        (DEBUG_DEBUG_LEVEL,
                         "MplsFTNModify : MplsCreateLblStkTableEntry Failed \t\n");
                    return MPLS_FAILURE;
                }

                pLblEntry = MplsAddLblStkLabelEntry (u4LblStkIndex,
                                                     u4MplsLabelStackLabelIndex);
                if (pLblEntry == NULL)
                {
                    CMNDB_DBG
                        (DEBUG_DEBUG_LEVEL,
                         "MplsFTNModify : MplsAddLblStkLabelEntry Failed \t\n");
                    return MPLS_FAILURE;
                }
            }
            pLblEntry->u4Label = pLspInfo->pNhlfe->u4OutLabel;
            pLblEntry->u1Storage = MPLS_STORAGE_VOLATILE;
            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL, "MplsFTNModify : IN Label=%d\n",
                        pLblEntry->u4Label);
            pLblEntry->u1RowStatus = ACTIVE;
        }

        if (MplsFTNDel (pFtnEntry) == MPLS_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsFTNModify  :MplsFTNDel Failed \t\n");
            return MPLS_FAILURE;
        }
    }
    else
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsFTNModify  : FTN entry does not exist \t\n");
        /* Double Stacking */
        if (pLspInfo->pNhlfe->StackTnlInfo.u1StackTnlBit == TRUE)
        {
            pTeStkTnlInfo =
                TeGetTunnelInfo (pLspInfo->pNhlfe->StackTnlInfo.u4TnlId,
                                 pLspInfo->pNhlfe->StackTnlInfo.u4TnlInstance,
                                 pLspInfo->pNhlfe->StackTnlInfo.u4IngressId,
                                 pLspInfo->pNhlfe->StackTnlInfo.u4EgressId);
            if (pTeStkTnlInfo == NULL)
            {
                CMNDB_DBG
                    (DEBUG_DEBUG_LEVEL, "MplsFTNModify : Tunnel does not exist "
                     "in TE Tunnel Table \t\n");
                return MPLS_FAILURE;
            }
            /* Below check is to avoid the RSVP signalling messages via MPLS switching 
             * when the FTN is configured for the tunnel egress address. 
             * If this is allowed, RSVP packets will not be processed 
             * in the intermediate hops and this results in RSVP tunnel tear down */

            if (Prefix.Addr.u4Addr == (pLspInfo->pNhlfe->StackTnlInfo.u4EgressId
                                       & pLspInfo->FecParams.DestMask.u4Addr))
            {
                CMNDB_DBG
                    (DEBUG_DEBUG_LEVEL,
                     "MplsFTNModify : Prefix and Tunnel egress address are "
                     "in same network, so not allowed \n");
                if (pLspInfo->FecParams.DestMask.u4Addr == 0xffffffff)
                {
                    bIsHostAddrFecForPSN = TRUE;
                }
                else
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               "MplsFTNModify : FEC is NOT host address \n");
                    return MPLS_FAILURE;
                }
            }
            u4LblStkIndex = MplsLblStackGetIndex ();
            if (MplsLblStackSetIndex (u4LblStkIndex) != u4LblStkIndex)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "MplsFTNModify : MplsLblStackSetIndex Failed\n");
                return MPLS_FAILURE;
            }
            pLblStkEntry = MplsCreateLblStkTableEntry (u4LblStkIndex);

            if (pLblStkEntry == NULL)
            {
                CMNDB_DBG
                    (DEBUG_DEBUG_LEVEL,
                     "MplsFTNModify : MplsCreateLblStkTableEntry Failed \t\n");
                return MPLS_FAILURE;
            }

            pLblEntry = MplsAddLblStkLabelEntry (u4LblStkIndex,
                                                 u4MplsLabelStackLabelIndex);
            if (pLblEntry == NULL)
            {
                CMNDB_DBG
                    (DEBUG_DEBUG_LEVEL,
                     "MplsFTNModify : MplsAddLblStkLabelEntry Failed \t\n");
                return MPLS_FAILURE;
            }
            pLblEntry->u4Label = pLspInfo->pNhlfe->u4OutLabel;
            pLblEntry->u1Storage = MPLS_STORAGE_VOLATILE;
            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL, "MplsFTNModify : IN Label=%d\n",
                        pLblEntry->u4Label);
            pLblEntry->u1RowStatus = ACTIVE;
        }

        /* Create XC, FTN and OutSegment Entries */
        u4FtnIndex = MplsFtnGetIndex ();
        if (u4FtnIndex == 0)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsFTNModify : Ftn Table is Full \t\n");
            return MPLS_FAILURE;
        }
        u4FtnIndex = MplsFtnSetIndex (u4FtnIndex);
        u4OutIndex = MplsOutSegmentGetIndex ();
        if (u4OutIndex == 0)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsFTNModify : OutSegment Table is Full \t\n");
            return MPLS_FAILURE;
        }
        u4XcIndex = MplsXcTableGetIndex ();
        if (u4XcIndex == 0)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsFTNModify : Cross Connect Table is Full \t\n");
            return MPLS_FAILURE;
        }
        u4XcIndex = MplsXCTableSetIndex (u4XcIndex);
        pFtnEntry = MplsCreateFtnTableEntry (u4FtnIndex);
        if (pFtnEntry == NULL)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsFTNModify : FTN Create Failed \t\n");
            return MPLS_FAILURE;
        }
        pXcEntry = MplsCreateXCTableEntry (u4XcIndex, 0, u4OutIndex);
        if (pXcEntry == NULL)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsFTNModify : XC Create Failed \t\n");
            return MPLS_FAILURE;
        }

        /* TO-DO convert the Prefix and Dest Mask into
         * Min and Max Range values and store instead of 
         * this
         */

        switch (Prefix.u2AddrType)
        {
#ifdef MPLS_IPV6_WANTED
            case IPV6:
                MEMCPY (pFtnEntry->DestAddrMin.u1_addr, &Prefix.Addr.Ip6Addr,
                        IPV6_ADDR_LENGTH);
                MEMCPY (pFtnEntry->DestAddrMax.u1_addr,
                        &pLspInfo->FecParams.DestMask.Ip6Addr,
                        IPV6_ADDR_LENGTH);
                pFtnEntry->u1AddrType = MPLS_IPV6_ADDR_TYPE;
                break;
#endif
            default:
                pFtnEntry->DestAddrMin.u4_addr[0] = Prefix.Addr.u4Addr;
                pFtnEntry->DestAddrMax.u4_addr[0] =
                    pLspInfo->FecParams.DestMask.u4Addr;
                pFtnEntry->u1AddrType = MPLS_IPV4_ADDR_TYPE;

                break;
        };

        pFtnEntry->u1FtnMask = MPLS_FTN_DST_BIT_MASK;
        pFtnEntry->i4ActionType = MPLS_FTN_ON_NON_TE_LSP;
        pFtnEntry->u1Storage = MPLS_STORAGE_VOLATILE;
        pFtnEntry->pActionPtr = pXcEntry;
        pFtnEntry->u1RowStatus = ACTIVE;
        pFtnEntry->bIsHostAddrFecForPSN = bIsHostAddrFecForPSN;

        switch (Prefix.u2AddrType)
        {
#ifdef MPLS_IPV6_WANTED
            case IPV6:
                MplsRegisterEntrywithIpv6Trie (pFtnEntry);
                break;
#endif
            default:
                MplsRegisterEntrywithTrie (pFtnEntry);
                break;
        };

        /* Commenting the below code to fix coverity warning */
        /*pFtnMapEntry =
           MplsCreateFtnMapTableEntry (pLspInfo->u4IfIndex, u4FtnIndex);
           if (pFtnMapEntry == NULL)
           {
           CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsFTNModify : MplsCreateFtnMapTableEntry \t\n");
           return MPLS_FAILURE;
           } */
    }

    /* Update XC, FTN and OutSegment Entries of pLspInfo */
    if (pLspInfo->pNhlfe->StackTnlInfo.u1StackTnlBit == TRUE)
    {
        pTnlXcEntry = MplsGetXCEntryByDirection (pTeStkTnlInfo->u4TnlXcIndex,
                                                 MPLS_DEF_DIRECTION);
        if (pTnlXcEntry == NULL)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsFTNModify : Stack tunnel XC not present \t\n");
            return MPLS_FAILURE;
        }
        pOutSegment = pXcEntry->pOutIndex;
        if (pOutSegment == NULL)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsFTNModify : OutSegment is NULL \t\n");
            return MPLS_FAILURE;
        }
        pTnlOutSegment = pTnlXcEntry->pOutIndex;
        u4OutIndex = pOutSegment->u4Index;
        MEMCPY (pOutSegment, pTnlOutSegment, sizeof (tOutSegment));
        pOutSegment->u4Index = u4OutIndex;

        /* Make sure interface index in pLspInfo->pNhlfe->u4OutIfIndex is 
         * given my LDP and not by RSVP. 
         * This is required because RSVP Tunnel is a generic medium,
         * applications like LDP uses it for label stacking. */
        pOutSegment->u4IfIndex = pLspInfo->pNhlfe->u4OutIfIndex;
        pOutSegment->pXcIndex = pXcEntry;
        MEMCPY (pXcEntry->pOutIndex->au1NextHopMac,
                pTeStkTnlInfo->au1NextHopMac, MAC_ADDR_LEN);
        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                    "MplsFTNModify : OUT-TNL Stack Label=%d\n",
                    pOutSegment->u4Label);

        pXcEntry->mplsLabelIndex = pLblStkEntry;

    }
    else
    {
        pOutSegment = pXcEntry->pOutIndex;
        if (pOutSegment == NULL)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsFTNModify : OutSegment is NULL \t\n");
            return MPLS_FAILURE;
        }
        pOutSegment->u4IfIndex = pLspInfo->pNhlfe->u4OutIfIndex;
        pOutSegment->bPushLabel = MPLS_SNMP_TRUE;
        pOutSegment->u1NHAddrType = pLspInfo->pNhlfe->u1NHAddrType;
        pOutSegment->u1RowStatus = ACTIVE;
        pOutSegment->u1Owner = pLspInfo->u1Owner;
        pOutSegment->pTrfIndex = NULL;

        switch (Prefix.u2AddrType)
        {
#ifdef MPLS_IPV6_WANTED
            case MPLS_IPV6_ADDR_TYPE:
                MEMCPY (pOutSegment->NHAddr.u1_addr,
                        &pLspInfo->pNhlfe->NextHopAddr.Ip6Addr,
                        IPV6_ADDR_LENGTH);
                break;
#endif
            default:
                pOutSegment->NHAddr.u4_addr[0] =
                    pLspInfo->pNhlfe->NextHopAddr.u4Addr;
                break;
        };

        pOutSegment->u1Storage = MPLS_STORAGE_VOLATILE;
        pXcEntry->mplsLabelIndex = NULL;
        MEMSET (pXcEntry->pOutIndex->au1NextHopMac, 0, MAC_ADDR_LEN);
        if (pLspInfo->u1Owner != MPLS_OWNER_LDP)
        {
            MplsSetOutLabel (pOutSegment, pLspInfo->pNhlfe->u4OutLabel);
        }
        else
        {
            pOutSegment->u4Label = pLspInfo->pNhlfe->u4OutLabel;
        }
    }
    pXcEntry->u1Owner = pLspInfo->u1Owner;
    pXcEntry->u1RowStatus = ACTIVE;
    pXcEntry->u1Storage = MPLS_STORAGE_VOLATILE;

    pXcEntry->u1AdminStatus = ACTIVE;
    pXcEntry->u1OperStatus = ACTIVE;

    if (MplsFTNAdd (pFtnEntry) == MPLS_FAILURE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsFTNModify  :MplsFTNAdd Failed \t\n");
        return MPLS_FAILURE;
    }
#ifdef MPLS_IPV6_WANTED
    if (Prefix.u2AddrType == MPLS_IPV6_ADDR_TYPE)
    {
        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                    "LSP UP event to posted to L2VPN for %s\n",
                    Ip6PrintAddr (&Prefix.Addr.Ip6Addr));
    }
    else
#endif
    {
        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                    "LSP UP event to posted to L2VPN for %x\n",
                    MPLS_IPV4_U4_ADDR (Prefix.Addr));
    }

    MplsNonTePostEventForL2Vpn (pXcEntry->u4Index, &Prefix,
                                L2VPN_MPLS_PWVC_LSP_UP);

    MplsFtnUpdateSysTime ();
    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   :MplsFTNDelete
 *  Description     :Function to Delete FTN Entry
 *  Input           :pLspInfo - Info for Delete FTN Entry
 *  Output          :None
 *  Returns         :MPLS_SUCCESS or MPLS_FAILURE

 ************************************************************************/
INT4
MplsFTNDelete (tLspInfo * pLspInfo)
{
    tFtnEntry          *pFtnEntry = NULL;
    tXcEntry           *pXcEntry = NULL;
    tOutSegment        *pOutSegment = NULL;
    tFtnMapEntry       *pFtnMapEntry = NULL;
    tGenU4Addr          Prefix;

    MEMSET (&Prefix, 0, sizeof (tGenU4Addr));
    MplsGetPrefix (&pLspInfo->FecParams.DestAddrPrefix,
                   &pLspInfo->FecParams.DestMask,
                   pLspInfo->FecParams.u2AddrType, &Prefix.Addr);
    Prefix.u2AddrType = pLspInfo->FecParams.u2AddrType;

#ifdef MPLS_IPV6_WANTED
    if (Prefix.u2AddrType == MPLS_IPV6_ADDR_TYPE)
    {
        CMNDB_DBG2 (DEBUG_DEBUG_LEVEL, "MplsFTNDelete : Prefix=%s Mask=%s\n",
                    Ip6PrintAddr (&Prefix.Addr.Ip6Addr),
                    Ip6PrintAddr (&pLspInfo->FecParams.DestMask.Ip6Addr));
    }
    else
#endif
    {
        CMNDB_DBG2 (DEBUG_DEBUG_LEVEL, "MplsFTNDelete : Prefix=%x Mask=%x\n",
                    MPLS_IPV4_U4_ADDR (Prefix.Addr),
                    MPLS_IPV4_U4_ADDR (pLspInfo->FecParams.DestMask));
    }

    switch (Prefix.u2AddrType)
    {
#ifdef MPLS_IPV6_WANTED
        case IPV6:
            pFtnEntry = MplsSignalGetFtnIpv6TableEntry (&Prefix.Addr.Ip6Addr);
            break;
#endif
        default:
            pFtnEntry = MplsSignalGetFtnTableEntry (Prefix.Addr.u4Addr);
            break;
    };

    if ((pFtnEntry == NULL) || (pFtnEntry->pActionPtr == NULL))
    {
        MplsNonTePostEventForL2Vpn (MPLS_ZERO, &Prefix,
                                    L2VPN_MPLS_PWVC_LSP_DESTROY);
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsFTNDelete : FTN entry not present\t\n");
        return MPLS_FAILURE;
    }
    if (pFtnEntry->i4ActionType != MPLS_FTN_ON_NON_TE_LSP)
    {
        /* For other than NON TE LSPs, the below operation is taken 
         * in FTN delete through management interface */
        CMNDB_DBG
            (DEBUG_DEBUG_LEVEL, "MplsFTNDelete : Failed in FTN entry for "
             "NON-TE LSP deletion \t\n");
        return MPLS_FAILURE;
    }
    pXcEntry = pFtnEntry->pActionPtr;
    pFtnMapEntry = MplsGetFtnMapTableEntry (pLspInfo->u4IfIndex,
                                            pFtnEntry->u4FtnIndex);
    /* Delete XC, FTN and OutSegment Entries */
    pOutSegment = pXcEntry->pOutIndex;

#ifdef LDP_GR_WANTED
#ifdef MPLS_L2VPN_WANTED
    if (pLspInfo->u1IsNPBlock == MPLS_FALSE)
    {
        if (pFtnEntry->u1IsLspUsedByL2VPN)
        {
            if (pLspInfo->bIsUsedByL2vpn)
            {
                MplsNonTePostEventForL2Vpn (pXcEntry->u4Index, &(Prefix),
                                            L2VPN_MPLS_PWVC_TE_FRR_PLR_DEL);
            }
            else if (pLspInfo->bIsLspDestroy == MPLS_TRUE)
            {
                MplsNonTePostEventForL2Vpn (pXcEntry->u4Index,
                                            &(Prefix),
                                            L2VPN_MPLS_PWVC_LSP_DOWN);

            }
            else
            {
                MplsNonTePostEventForL2Vpn (pXcEntry->u4Index, &(Prefix),
                                            L2VPN_MPLS_PWVC_LSP_DESTROY);
            }
            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                        "MplsFTNDelete :NON-TE L2VPN runs on this LSP %x\n",
                        MPLS_IPV4_U4_ADDR (Prefix.Addr));
        }
    }
#endif
#ifdef MPLS_L3VPN_WANTED
    if (pLspInfo->u1IsNPBlock == MPLS_FALSE)
    {
        if (pFtnEntry->u1IsLspUsedByL3VPN)
        {
            /* Give Notification only for Non-stacked LSP's */
            if (pXcEntry->mplsLabelIndex == NULL)
            {
                if (pOutSegment != NULL)
                {
                    L3VpnNonTeLspStatusChangeEventHandler (pOutSegment->u4Label,
                                                           MPLS_IPV4_U4_ADDR
                                                           (Prefix.Addr),
                                                           L3VPN_MPLS_LSP_DOWN);
                }
                else
                {
                    CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                                "Cannot post Event to L3VPN since outsegment "
                                "is NULL for the Prefix: %d\n",
                                MPLS_IPV4_U4_ADDR (Prefix.Addr));
                }
            }
        }
    }
#endif

#else
#ifdef MPLS_L2VPN_WANTED
    if (pFtnEntry->u1IsLspUsedByL2VPN)
    {
        if (pLspInfo->bIsUsedByL2vpn)
        {
            MplsNonTePostEventForL2Vpn (pXcEntry->u4Index, &Prefix,
                                        L2VPN_MPLS_PWVC_TE_FRR_PLR_DEL);
        }
        else if (pLspInfo->bIsLspDestroy == MPLS_TRUE)
        {
            MplsNonTePostEventForL2Vpn (pXcEntry->u4Index,
                                        &Prefix, L2VPN_MPLS_PWVC_LSP_DOWN);

        }
        else
        {
            MplsNonTePostEventForL2Vpn (pXcEntry->u4Index, &Prefix,
                                        L2VPN_MPLS_PWVC_LSP_DESTROY);
        }
#ifdef MPLS_IPV6_WANTED
        if (Prefix.u2AddrType == MPLS_IPV6_ADDR_TYPE)
        {
            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                        "MplsFTNDelete :NON-TE L2VPN runs on this LSP %s\n",
                        Ip6PrintAddr (&Prefix.Addr.Ip6Addr));
        }
        else
#endif
        {
            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                        "MplsFTNDelete :NON-TE L2VPN runs on this LSP %x\n",
                        MPLS_IPV4_U4_ADDR (Prefix.Addr));
        }
    }
#endif
#ifdef MPLS_L3VPN_WANTED
    if (pFtnEntry->u1IsLspUsedByL3VPN)
    {
        /* Give Notification only for Non-stacked LSP's */
        if (pXcEntry->mplsLabelIndex == NULL)
        {
            if (pOutSegment != NULL)
            {
                L3VpnNonTeLspStatusChangeEventHandler (pOutSegment->u4Label,
                                                       Prefix.Addr.u4Addr,
                                                       L3VPN_MPLS_LSP_DOWN);
            }
            else
            {
#ifdef MPLS_IPV6_WANTED
                if (Prefix.u2AddrType == MPLS_IPV6_ADDR_TYPE)
                {
                    CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                                "Cannot post Event to L3VPN since outsegment "
                                "is NULL for the Prefix: %s\n",
                                Ip6PrintAddr (&Prefix.Addr.Ip6Addr));
                }
                else
#endif
                {
                    CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                                "Cannot post Event to L3VPN since outsegment "
                                "is NULL for the Prefix: %d\n",
                                MPLS_IPV4_U4_ADDR (Prefix.Addr));
                }
            }
        }
    }
#endif
#endif /* End of LDP_GR_WANTED */

#ifdef LDP_GR_WANTED
    if (pLspInfo->u1IsNPBlock == MPLS_FALSE)
    {
        if (MplsFTNDel (pFtnEntry) == MPLS_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsFTNDelete :MplsFTNDel Failed \t\n");
        }
    }
    else
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsFTNDelete: FTN Delete NPAPI Blocked since "
                   "the module SHUTDOWN is in progress\n");
    }
#else
    if (MplsFTNDel (pFtnEntry) == MPLS_FAILURE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsFTNDelete :MplsFTNDel Failed \t\n");
    }
#endif

#ifdef MPLS_IPV6_WANTED
    if (Prefix.u2AddrType == MPLS_IPV6_ADDR_TYPE)
    {
        MplsDeleteFtnIpv6TableEntry (pFtnEntry);
    }
    else
#endif
    {
        MplsDeleteFtnTableEntry (pFtnEntry);
    }

    MplsFtnUpdateSysTime ();
    if (pXcEntry->mplsLabelIndex != NULL)
    {
        MplsDeleteLblStkAndLabelEntries (pXcEntry->mplsLabelIndex);
        pXcEntry->mplsLabelIndex = NULL;
    }

    MplsDeleteXCTableEntry (pXcEntry);
    if (pOutSegment != NULL)
    {
        MplsDeleteOutSegmentTableEntry (pOutSegment);
    }
    else
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "pOutSegment is NULL. "
                   "MplsFTNDelete :MplsFTNDel Failed \t\n");
    }

    if (pFtnMapEntry != NULL)
    {
        MplsDeleteFtnMapTableEntry (pFtnMapEntry);
        MplsFtnMapUpdateSysTime ();
    }
    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   :MplsTnlCreate 
 *  Description     :Function to create Tnl Entry 
 *  Input           :pLspInfo - Info for Creating Tnl 
 *  Output          :None 
 *  Returns         :MPLS_SUCCESS or MPLS_FAILURE 
 ************************************************************************/
INT4
MplsTnlCreate (tLspInfo * pLspInfo)
{
    UINT4               u4OutIndex = 0, u4XcIndex = 0;
    tXcEntry           *pXcEntry = NULL;
    tXcEntry           *pTnlXcEntry = NULL;
    tOutSegment        *pOutSegment = NULL;
    tOutSegment        *pTnlOutSegment = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlInfo         *pTeStkTnlInfo = NULL;
    tLblStkEntry       *pLblStkEntry = NULL;
    tLblEntry          *pLblEntry = NULL;
    UINT4               u4LblStkIndex = 0;
    /* Creating only one Stack Label Index */
    UINT4               u4MplsLabelStackLabelIndex = 1;
    /* Get the Tunnel Info from TE Tunnel Table and 
     * check the XC Pointer. If it is already set then
     * we are suppose to return failure here  other wise
     * update the tunnel xc pointer with XC Index  */

    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsTnlCreate : ENTRY \t\n");
    pTeTnlInfo = TeGetTunnelInfo (pLspInfo->FecParams.u4TnlId,
                                  pLspInfo->FecParams.u4TnlInstance,
                                  pLspInfo->FecParams.u4IngressId,
                                  pLspInfo->FecParams.u4EgressId);
    if (pTeTnlInfo == NULL)
    {
        CMNDB_DBG
            (DEBUG_DEBUG_LEVEL,
             "MplsTnlCreate : Tunnel does not exist in TE Tunnel Table \t\n");
        return MPLS_FAILURE;
    }

    /* Double Stacking */
    if (pLspInfo->pNhlfe->StackTnlInfo.u1StackTnlBit == TRUE)
    {
        pTeStkTnlInfo = TeGetTunnelInfo (pLspInfo->pNhlfe->StackTnlInfo.u4TnlId,
                                         pLspInfo->pNhlfe->StackTnlInfo.
                                         u4TnlInstance,
                                         pLspInfo->pNhlfe->StackTnlInfo.
                                         u4IngressId,
                                         pLspInfo->pNhlfe->StackTnlInfo.
                                         u4EgressId);
        if (pTeStkTnlInfo == NULL)
        {
            CMNDB_DBG
                (DEBUG_DEBUG_LEVEL, "MplsTnlCreate : Tunnel does not exist "
                 "in TE Tunnel Table \t\n");
            return MPLS_FAILURE;
        }

        u4LblStkIndex = MplsLblStackGetIndex ();
        if (MplsLblStackSetIndex (u4LblStkIndex) != u4LblStkIndex)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsTnlCreate : MplsLblStackSetIndex Failed\n");
            return MPLS_FAILURE;
        }
        pLblStkEntry = MplsCreateLblStkTableEntry (u4LblStkIndex);

        if (pLblStkEntry == NULL)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsTnlCreate : MplsCreateLblStkTableEntry Failed\n");
            return MPLS_FAILURE;
        }

        pLblEntry = MplsAddLblStkLabelEntry (u4LblStkIndex,
                                             u4MplsLabelStackLabelIndex);
        if (pLblEntry == NULL)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsTnlCreate : MplsAddLblStkLabelEntry Failed\n");
            return MPLS_FAILURE;
        }
        pLblEntry->u4Label = pLspInfo->pNhlfe->u4OutLabel;
        pLblEntry->u1Storage = MPLS_STORAGE_VOLATILE;
        pLblEntry->u1RowStatus = ACTIVE;
    }

    if ((pTeTnlInfo->u1TnlHwStatus == TRUE) &&
        (pTeTnlInfo->bFrrDnStrLblChg != MPLS_TRUE) &&
        (pTeTnlInfo->u4TnlXcIndex != 0) &&
        (pTeTnlInfo->u4TnlMode == TE_TNL_MODE_UNIDIRECTIONAL))
    {
        CMNDB_DBG
            (DEBUG_DEBUG_LEVEL,
             "MplsTnlCreate : Tunnel is already attached to a XC Entry \t\n");
        return MPLS_FAILURE;
    }

    u4OutIndex = MplsOutSegmentGetIndex ();
    if (u4OutIndex == 0)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsTnlCreate : OutSegment Table is Full \t\n");
        return MPLS_FAILURE;
    }

    if ((pTeTnlInfo->u4TnlMode != TE_TNL_MODE_UNIDIRECTIONAL) &&
        (pTeTnlInfo->u4TnlXcIndex != 0))
    {
        /* Use OrgTnlXcIndex when its not 0, to fetch correct XC entry for the
         * tunnel.
         *
         * u4TnlXcIndex points the correct XC entry in normal scenarios. But
         * When Local Protection is in USE, u4OrgTnlXcIndex points the Correct XC
         * for the tunnel */
        if (pTeTnlInfo->u4OrgTnlXcIndex != 0)
        {
            u4XcIndex = pTeTnlInfo->u4OrgTnlXcIndex;
        }
        else
        {
            u4XcIndex = pTeTnlInfo->u4TnlXcIndex;
        }
    }
    else
    {
        u4XcIndex = MplsXcTableGetIndex ();

        if (u4XcIndex == 0)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsTnlCreate : Cross Connect Table is Full \t\n");
            return MPLS_FAILURE;
        }
        u4XcIndex = MplsXCTableSetIndex (u4XcIndex);
    }
    pXcEntry = MplsCreateXCTableEntry (u4XcIndex, 0, u4OutIndex);
    if (pXcEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsTnlCreate : XC Create Failed \t\n");
        return MPLS_FAILURE;
    }
    if (pLspInfo->pNhlfe->StackTnlInfo.u1StackTnlBit == TRUE)
    {
        pTnlXcEntry =
            MplsGetXCEntryByDirection (pTeStkTnlInfo->u4TnlXcIndex,
                                       pLspInfo->Direction);
        if (pTnlXcEntry == NULL)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsTnlCreate : Stack tunnel XC not present\n");
            return MPLS_FAILURE;
        }
        pOutSegment = pXcEntry->pOutIndex;
        pTnlOutSegment = pTnlXcEntry->pOutIndex;
        u4OutIndex = pOutSegment->u4Index;
        MEMCPY (pOutSegment, pTnlOutSegment, sizeof (tOutSegment));
        pOutSegment->u4Index = u4OutIndex;
        pXcEntry->mplsLabelIndex = pLblStkEntry;
        pOutSegment->u4IfIndex = pTeTnlInfo->u4TnlIfIndex;

    }
    else
    {
        pOutSegment = pXcEntry->pOutIndex;
        pOutSegment->bPushLabel = MPLS_SNMP_TRUE;
        pOutSegment->u1NHAddrType = pLspInfo->pNhlfe->u1NHAddrType;
        pOutSegment->u1RowStatus = ACTIVE;
        pOutSegment->u1Owner = pLspInfo->u1Owner;
        pOutSegment->pTrfIndex = NULL;

        pOutSegment->NHAddr.u4_addr[0] = pLspInfo->pNhlfe->NextHopAddr.u4Addr;

        pOutSegment->u1Storage = MPLS_STORAGE_VOLATILE;
        pOutSegment->GmplsOutSegment.OutSegmentDirection = pLspInfo->Direction;
        pXcEntry->mplsLabelIndex = NULL;

        if ((pLspInfo->FecParams.u1FecType == RSVPTE_TNL_TYPE) &&
            (pTeTnlInfo->u1TnlIsIf == TE_TRUE))
        {
            if ((TE_TNL_ROLE (pTeTnlInfo) == TE_INGRESS) &&
                (pTeTnlInfo->u1DetourActive == MPLS_TRUE))
            {
                pOutSegment->u4IfIndex = pTeTnlInfo->u4OrgTnlIfIndex;
            }
            else
            {
                if (pLspInfo->Direction == MPLS_DIRECTION_FORWARD)
                {
                    pOutSegment->u4IfIndex = pTeTnlInfo->u4TnlIfIndex;
                }
                else
                {
                    pOutSegment->u4IfIndex = pTeTnlInfo->u4RevTnlIfIndex;
                }
            }
        }
        else if ((pLspInfo->FecParams.u1FecType == CRLSP_TNL_TYPE) &&
                 (pTeTnlInfo->u1TnlIsIf == TE_TRUE))
        {
            /* CRLDP Case */
            pOutSegment->u4IfIndex = pLspInfo->pNhlfe->u4OutIfIndex;
        }
        MplsSetOutLabel (pOutSegment, pLspInfo->pNhlfe->u4OutLabel);
    }
    pXcEntry->u1Owner = pLspInfo->u1Owner;
    pXcEntry->u1RowStatus = ACTIVE;
    pXcEntry->u1Storage = MPLS_STORAGE_VOLATILE;

    pXcEntry->u1AdminStatus = ACTIVE;
    pXcEntry->u1OperStatus = ACTIVE;

    if ((pTeTnlInfo->u1FacFrrFlag != MPLS_TRUE) &&
        (TE_TNL_ROLE (pTeTnlInfo) == TE_INGRESS) &&
        (pTeTnlInfo->u1DetourActive == MPLS_TRUE))
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\n MplsTnlCreate : DETOUR ACTIVE \n");
        pTeTnlInfo->u4OrgTnlXcIndex = u4XcIndex;
    }
    else
    {
        MplsDbUpdateTunnelXCIndex (pTeTnlInfo, pXcEntry, u4XcIndex);
    }
    CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                "\n MplsTnlCreate : Tnl create XC index=%d\n", u4XcIndex);
    if (MplsTnlNPAdd (pTeTnlInfo, pXcEntry, NULL) == MPLS_FAILURE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsTnlCreate : NP Create Failed \t\n");
        return MPLS_FAILURE;
    }
    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsTnlCreate : EXIT \t\n");
    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   :MplsTnlModify 
 *  Description     :Function to modify Tnl Entry 
 *  Input           :pLspInfo - Info for modifing Tnl 
 *  Output          :None 
 *  Returns         :MPLS_SUCCESS or MPLS_FAILURE 
 ************************************************************************/
INT4
MplsTnlModify (tLspInfo * pLspInfo)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tXcEntry           *pXcEntry = NULL;

    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsTnlModify : ENTRY \t\n");
    pTeTnlInfo = TeGetTunnelInfo (pLspInfo->FecParams.u4TnlId,
                                  pLspInfo->FecParams.u4TnlInstance,
                                  pLspInfo->FecParams.u4IngressId,
                                  pLspInfo->FecParams.u4EgressId);
    if (pTeTnlInfo == NULL)
    {
        CMNDB_DBG
            (DEBUG_DEBUG_LEVEL,
             "MplsTnlModify : Tunnel does not exist in TE Tunnel Table \t\n");
        return MPLS_FAILURE;
    }

    if (pTeTnlInfo->u4TnlXcIndex == 0)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsTnlModify : Tunnel is not attached to a XC Entry \t\n");
        return MPLS_FAILURE;
    }

    pXcEntry = MplsGetXCEntryByDirection (pTeTnlInfo->u4TnlXcIndex,
                                          pLspInfo->Direction);
    if (pXcEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsTnlModify : XC Entry Does not Exists \t\n");
        return MPLS_FAILURE;
    }

    if (pLspInfo->pNhlfe->u1OperStatus == NOT_IN_SERVICE)
    {
        pXcEntry->u1OperStatus = NOT_IN_SERVICE;
        /* TO-DO If any FTN attached to this XC then 
         * you should remove the that FTN from Forwarding Plane */

        if (MplsTnlNPDel (pTeTnlInfo, MPLSDB_MODULE) == MPLS_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsTnlModify : NP Delete Failed \t\n");
            return MPLS_FAILURE;
        }
    }
    else
    {
        /* TO-DO If any FTN attached to this XC then 
         * you should add that FTN to Forwarding Plane */
        pXcEntry->u1OperStatus = ACTIVE;
        if (MplsTnlNPAdd (pTeTnlInfo, pXcEntry, NULL) == MPLS_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsTnlModify : NP Add Failed \t\n");
            return MPLS_FAILURE;
        }

    }
    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsTnlModify : EXIT \t\n");
    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   :MplsTnlDelete
 *  Description     :Function to delete Tnl Entry 
 *  Input           :pLspInfo - Info for deleting Tnl 
 *  Output          :None 
 *  Returns         :MPLS_SUCCESS or MPLS_FAILURE 
 ************************************************************************/
INT4
MplsTnlDelete (tLspInfo * pLspInfo)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlInfo         *pInstance0Tnl = NULL;
    tTeTnlInfo         *pTempTeTnlInfo = NULL;
    tXcEntry           *pXcEntry = NULL;
    tOutSegment        *pOutSegment = NULL;
    tFtnEntry          *pFtnEntry = NULL;
    tTMO_DLL_NODE      *pFtnNodeInfo = NULL;
    UINT4               u4TnlXcIndex = MPLS_ZERO;

    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsTnlDelete : ENTRY \t\n");
    pTeTnlInfo = TeGetTunnelInfo (pLspInfo->FecParams.u4TnlId,
                                  pLspInfo->FecParams.u4TnlInstance,
                                  pLspInfo->FecParams.u4IngressId,
                                  pLspInfo->FecParams.u4EgressId);
    pInstance0Tnl = TeGetTunnelInfo (pLspInfo->FecParams.u4TnlId,
                                     MPLS_ZERO,
                                     pLspInfo->FecParams.u4IngressId,
                                     pLspInfo->FecParams.u4EgressId);
    if (pTeTnlInfo == NULL)
    {
        CMNDB_DBG
            (DEBUG_DEBUG_LEVEL,
             "MplsTnlDelete : Tunnel does not exist in TE Tunnel Table \r\n");
        return MPLS_FAILURE;
    }

    pTempTeTnlInfo = pTeTnlInfo;
    CMNDB_DBG7 (DEBUG_DEBUG_LEVEL,
                "MplsTnlDelete : Tnl Index %d Inst %d Xc %d Org Xc %d If %d "
                "OrgIf %d DetourActive %d\n", pTeTnlInfo->u4TnlIndex,
                pTeTnlInfo->u4TnlInstance, pTeTnlInfo->u4TnlXcIndex,
                pTeTnlInfo->u4OrgTnlXcIndex, pTeTnlInfo->u4TnlIfIndex,
                pTeTnlInfo->u4OrgTnlIfIndex, pTeTnlInfo->u1DetourActive);

    CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                "MplsTnlDelete : Tnl Index %d Primary Tunnel Instance: %d\n",
                pTeTnlInfo->u4TnlIndex, pTeTnlInfo->u4TnlPrimaryInstance);
    if ((pTeTnlInfo->u4TnlXcIndex == 0) && (pTeTnlInfo->u4OrgTnlXcIndex == 0))
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsTnlDelete : Tunnel is not attached to a XC Entry \r\n");
        return MPLS_FAILURE;
    }

    /*RSVP-TE MPLS-L3VPN-TE 
     * For RSVP_TE tunnel  from MPLSDB we will notify RSVP-TE !!!*/
    if ((pTeTnlInfo->u1TnlRole == TE_INGRESS) &&
        (pTeTnlInfo->u4TnlInstance != MPLS_ZERO) &&
        (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_RSVP))
    {
        if (MplsRpteL3VPNEventHandler (TE_OPER_DOWN, pTeTnlInfo) ==
            MPLS_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "Posting to RSVP-TE failed UnProtected Notify Failed\n");
        }
    }

    /* Tunnel is going to be deleted now,
     * so delete all the FTN entries in h/w
     * and Ftn Mapped status is False */

    if ((pTeTnlInfo->bFrrDnStrLblChg != MPLS_TRUE) &&
        (pTeTnlInfo->u1IsFtnMapped == MPLS_TRUE))
    {
        if ((TMO_DLL_Count (&TE_TNL_FTN_LIST (pTeTnlInfo)) == MPLS_ZERO) &&
            (pInstance0Tnl != NULL))
        {
            pTempTeTnlInfo = pInstance0Tnl;
        }

        TMO_DLL_Scan (&TE_TNL_FTN_LIST (pTempTeTnlInfo), pFtnNodeInfo,
                      tTMO_DLL_NODE *)
        {
            pFtnEntry = (tFtnEntry *)
                (((FS_ULONG) pFtnNodeInfo -
                  (TE_OFFSET (tFtnEntry, FtnInfoNode))));
            if (MplsFTNDel (pFtnEntry) == MPLS_FAILURE)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "MplsTnlDelete : FTN delete failed\r\n");
            }
        }
    }
    if (MplsTnlNPDel (pTeTnlInfo, MPLSDB_MODULE) == MPLS_FAILURE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsTnlDelete : NP Tnl Delete Failed \r\n");
    }
    if ((TE_TNL_ROLE (pTeTnlInfo) == TE_INGRESS) &&
        (pTeTnlInfo->u1DetourActive == MPLS_TRUE))
    {
        u4TnlXcIndex = pTeTnlInfo->u4OrgTnlXcIndex;
        pTeTnlInfo->u4OrgTnlXcIndex = MPLS_ZERO;
    }
    else
    {
        if ((pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
             MPLS_TE_DEDICATED_ONE2ONE) &&
            (pTeTnlInfo->u4OrgTnlXcIndex != MPLS_ZERO))
        {
            u4TnlXcIndex = pTeTnlInfo->u4OrgTnlXcIndex;
        }
        else
        {
            u4TnlXcIndex = pTeTnlInfo->u4TnlXcIndex;
        }
        if (MplsGetXCEntryByDirection (u4TnlXcIndex,
                                       (eDirection) MPLS_DIRECTION_ANY) == NULL)
        {
            MplsDbUpdateTunnelXCIndex (pTeTnlInfo, NULL, MPLS_ZERO);
        }
    }

    pXcEntry = MplsGetXCEntryByDirection (u4TnlXcIndex, pLspInfo->Direction);
    if (pXcEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsTnlDelete : XC Entry Does not Exists \r\n");
        return MPLS_FAILURE;
    }

    /* TODO If any FTN is attached with this Tnl then we have to
     * remove the link between that entry and this XC */
    pXcEntry->pTeTnlInfo = NULL;
    pOutSegment = pXcEntry->pOutIndex;
    MplsDeleteXCTableEntry (pXcEntry);

    if (!((pTeTnlInfo->u1TnlRole == TE_INGRESS) &&
          (pTeTnlInfo->u1DetourActive == MPLS_TRUE)))
    {
        if (MplsGetXCEntryByDirection (u4TnlXcIndex,
                                       (eDirection) MPLS_DIRECTION_ANY) == NULL)
        {
            MplsDbUpdateTunnelXCIndex (pTeTnlInfo, NULL, MPLS_ZERO);
        }
    }
    if (pOutSegment != NULL)
    {
        MplsDeleteOutSegmentTableEntry (pOutSegment);
    }
    if (pTeTnlInfo->bFrrDnStrLblChg != MPLS_TRUE)
    {
        pTeTnlInfo->u4OrgTnlXcIndex = MPLS_ZERO;

        if (MplsGetXCEntryByDirection (u4TnlXcIndex,
                                       (eDirection) MPLS_DIRECTION_ANY) == NULL)
        {
            MplsDbUpdateTunnelXCIndex (pTeTnlInfo, NULL, MPLS_ZERO);
        }
    }

    TeCmnExtUpdateArpResolveStatus (pTeTnlInfo, MPLS_MLIB_TNL_DELETE,
                                    (eDirection) MPLS_ZERO, MPLS_ZERO);

    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsTnlDelete : EXIT \t\n");
    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsFTNAdd 
 *  Description     : Function to add FTN entry to NP
 *  Input           : FTN Info
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE
 ************************************************************************/
INT4
MplsFTNAdd (tFtnEntry * pFtnEntry)
{
    return (MplsFTNHwAdd (pFtnEntry, NULL));
}

/************************************************************************
 *  Function Name   : MplsFTNHwAdd
 *  Description     : Function to add FTN entry to NP
 *  Input           : FTN Info, Slot Info
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE
 ************************************************************************/
INT4
MplsFTNHwAdd (tFtnEntry * pFtnEntry, VOID *pSlotInfo)
{
    tFsNpNextHopInfo    NextHop;
    tXcEntry           *pXcEntry = NULL;
    tMplsHwL3FTNInfo    MplsHwL3FTNInfo;
    UINT1               u1EncapType;
    uGenU4Addr          NetAddr;
    uGenU4Addr          NetMask;
    tLblEntry          *pLblEntry = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tRtInfoQueryMsg     RtQuery;
    tNetIpv4RtInfo      NetIpRtInfo;
    UINT1               au1ZeroMac[MAC_ADDR_LEN];
    UINT4               u4Direction = 0;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;
    UINT4               u4L3Intf = 0;
    UINT4               u4NextHop = 0;
#ifdef NPAPI_WANTED
    UINT1               u1TblFull = 0;
    INT4                i4TblFull = 0;
    UINT4               u4OutPort = 0;
    UINT4               u4VpnId = MPLS_DEF_VRF;
#endif

#if defined(NPAPI_WANTED) || defined(MBSM_WANTED)
    tHwPortInfo         pHwPortInfo;
#ifdef QOSX_WANTED
    INT4                i4HwExpMapId = 0;
#endif
#endif
#ifdef LDP_GR_WANTED
    INT4                i4RetVal = MPLS_SUCCESS;
    tMplsIpAddress      Fec;
    tFTNHwListEntry    *pFTNHwListEntry = NULL;
    tFTNHwListEntry     FTNHwListEntry;
    tMplsIpAddress      HwListNextHop;
    uLabel              Label;
    UINT1               u1FtnHwListPrefLen = 0;
#endif

#ifdef MPLS_IPV6_WANTED
    tIp6Addr            NextHopIpv6Addr;
    UINT4               u4DstIntfIdx = 0;
    tNetIpv6RtInfoQueryMsg NetIpv6RtInfoQueryMsg;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tFsNpIntInfo        IntInfo;
    tFsNpRouteInfo      RouteInfo;
    UINT1               u1PrefixLen = 0;
#ifdef NPAPI_WANTED
    UINT4               u4NHType = NH_REMOTE;
#endif
#endif

#ifndef MBSM_WANTED
    UNUSED_PARAM (pSlotInfo);
#endif

    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsFTNHwAdd: Entry\n");

#ifdef LDP_GR_WANTED
    MEMSET (&FTNHwListEntry, 0, sizeof (tFTNHwListEntry));
    MEMSET (&Fec, 0, sizeof (tMplsIpAddress));
    MEMSET (&HwListNextHop, 0, sizeof (tMplsIpAddress));
    MEMSET (&Label, 0, sizeof (uLabel));
#endif
#ifdef MPLS_IPV6_WANTED
    MEMSET (&NextHopIpv6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&NetIpv6RtInfoQueryMsg, 0, sizeof (tNetIpv6RtInfoQueryMsg));
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&IntInfo, 0, sizeof (tFsNpIntInfo));
    MEMSET (&RouteInfo, 0, sizeof (tFsNpRouteInfo));

#endif
#if defined(NPAPI_WANTED) || defined(MBSM_WANTED)
    MEMSET (&pHwPortInfo, 0, sizeof (tHwPortInfo));
#endif
    MEMSET (&NetAddr, 0, sizeof (uGenU4Addr));
    MEMSET (&NetMask, 0, sizeof (uGenU4Addr));

    MEMSET (au1ZeroMac, MPLS_ZERO, MAC_ADDR_LEN);
    MEMSET (&MplsHwL3FTNInfo, 0, sizeof (tMplsHwL3FTNInfo));
    if (pFtnEntry->pActionPtr == NULL)
    {
        /* No Cross Connect Info */
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, " INT Exit - No FTN Action Pointer\n");
        return MPLS_FAILURE;
    }
    if (pFtnEntry->i4ActionType == MPLS_FTN_ON_TE_LSP)
    {
        pTeTnlInfo = pFtnEntry->pActionPtr;

        if (pFtnEntry->u1RouteStatus == FTN_ROUTE_NOT_EXIST)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       " INT Exit - Route does not exist \n");

            /* This FTN programming will be taken during tunnel
             * OPER UP indication */
            return MPLS_SUCCESS;
        }

        if (pTeTnlInfo == NULL)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL, " INT Exit - Tunnel is NULL\n");

            /* This FTN programming will be taken during tunnel
             * OPER UP indication */
            return MPLS_SUCCESS;
        }

        if (pTeTnlInfo->u4TnlInstance == MPLS_ZERO)
        {
            CONVERT_TO_INTEGER (pTeTnlInfo->TnlIngressLsrId, u4IngressId);
            CONVERT_TO_INTEGER (pTeTnlInfo->TnlEgressLsrId, u4EgressId);

            u4IngressId = OSIX_NTOHL (u4IngressId);
            u4EgressId = OSIX_NTOHL (u4EgressId);

            pTeTnlInfo = TeGetTunnelInfo (pTeTnlInfo->u4TnlIndex,
                                          pTeTnlInfo->u4TnlPrimaryInstance,
                                          u4IngressId, u4EgressId);

            if (pTeTnlInfo == NULL)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           " INT Exit - Primary Tunnel is NULL\n");

                /* This FTN programming will be taken during tunnel
                 * OPER UP indication */
                return MPLS_SUCCESS;
            }
        }

        if (pTeTnlInfo->u1TnlOperStatus != TE_OPER_UP)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       " INT Exit - Tunnel is not operationally UP \n");

            /* This FTN programming will be taken during tunnel 
             * OPER UP indication */
            return MPLS_SUCCESS;
        }

        if (pTeTnlInfo->u1TnlRole == TE_EGRESS)
        {
            u4Direction = MPLS_DIRECTION_REVERSE;
        }
        else
        {
            u4Direction = MPLS_DIRECTION_FORWARD;
        }

        pXcEntry = MplsGetXCEntryByDirection (pTeTnlInfo->u4TnlXcIndex,
                                              (eDirection) u4Direction);
        if (pXcEntry != NULL)
        {
            MEMCPY (&u4NextHop, pXcEntry->pOutIndex->NHAddr.u4_addr,
                    IP_ADDR_LENGTH);
            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL, "FTN ADD  u4NextHop = %x \n",
                        u4NextHop);
        }

    }
    else
    {
        pXcEntry = pFtnEntry->pActionPtr;
    }

    if ((pXcEntry == NULL) || (pXcEntry->pOutIndex == NULL))
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, " INT Exit - No XC/XC->OutIndex Info \n");
        return MPLS_FAILURE;
    }

    LdpCopyAddr (&NetAddr, (uGenU4Addr *) & pFtnEntry->DestAddrMin,
                 pFtnEntry->u1AddrType);
    LdpCopyAddr (&NetMask, (uGenU4Addr *) & pFtnEntry->DestAddrMax,
                 pFtnEntry->u1AddrType);

    if (MEMCMP (pXcEntry->pOutIndex->au1NextHopMac, au1ZeroMac, MAC_ADDR_LEN)
        == MPLS_ZERO)
    {
        if (pXcEntry->pOutIndex->u1NHAddrType != GMPLS_UNKNOWN)
        {
#ifdef MPLS_IPV6_WANTED
            if (pFtnEntry->u1AddrType == MPLS_IPV6_ADDR_TYPE)
            {
                if (CfaUtilGetIfIndexFromMplsTnlIf
                    (pXcEntry->pOutIndex->u4IfIndex, &u4DstIntfIdx,
                     TRUE) == CFA_FAILURE)
                {
                    return MPLS_FAILURE;
                }
                /* Get the destination MAC for a given Peer address */
                MEMCPY (&NextHopIpv6Addr, pXcEntry->pOutIndex->NHAddr.u1_addr,
                        IPV6_ADDR_LENGTH);

                if ((NetIpv6Nd6Resolve
                     (u4DstIntfIdx, &NextHopIpv6Addr,
                      MplsHwL3FTNInfo.au1DstMac)) == NETIPV6_FAILURE)
                {
                    if (MplsStartArpResolveTimer ((UINT1) MPLS_FTN_ARP_TIMER) ==
                        MPLS_SUCCESS)
                    {
                        pFtnEntry->u1ArpResolveStatus =
                            MPLS_ARP_RESOLVE_WAITING;
                        CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                                    "Arp Resolve Waiting for FTN %s Next Hop %s "
                                    "- Queued in ARP\n",
                                    Ip6PrintAddr (&NetAddr.Ip6Addr),
                                    Ip6PrintAddr (&NextHopIpv6Addr));
                        return MPLS_FAILURE;
                    }

                    pFtnEntry->u1ArpResolveStatus = MPLS_ARP_RESOLVE_FAILED;
                    CMNDB_DBG2
                        (DEBUG_DEBUG_LEVEL,
                         "INT Exit - ARP Resolve for FTN %s Next Hop %s failed\n",
                         Ip6PrintAddr (&NetAddr.Ip6Addr),
                         Ip6PrintAddr (&NextHopIpv6Addr));
                    return MPLS_FAILURE;
                }

            }
            else
#endif
            {

                if (ArpResolve (pXcEntry->pOutIndex->NHAddr.u4_addr[0],
                                (INT1 *) MplsHwL3FTNInfo.au1DstMac,
                                &u1EncapType) == ARP_FAILURE)
                {
                    if (MplsTriggerArpResolve
                        (pXcEntry->pOutIndex->NHAddr.u4_addr[0],
                         (UINT1) MPLS_FTN_ARP_TIMER) == MPLS_SUCCESS)
                    {
                        pFtnEntry->u1ArpResolveStatus =
                            MPLS_ARP_RESOLVE_WAITING;
                        CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                                    "Arp Resolve Waiting for FTN %x Next Hop %x "
                                    "- Queued in ARP\n",
                                    MPLS_IPV4_U4_ADDR (NetAddr),
                                    pXcEntry->pOutIndex->NHAddr.u4_addr[0]);
                        return MPLS_FAILURE;
                    }

                    pFtnEntry->u1ArpResolveStatus = MPLS_ARP_RESOLVE_FAILED;
                    CMNDB_DBG2
                        (DEBUG_DEBUG_LEVEL,
                         "INT Exit - ARP Resolve for FTN %x Next Hop %x failed\n",
                         MPLS_IPV4_U4_ADDR (NetAddr),
                         pXcEntry->pOutIndex->NHAddr.u4_addr[0]);
                    return MPLS_FAILURE;

                }
            }
        }
        else
        {
            MplsGetIfUnnumPeerMac (pXcEntry->pOutIndex->u4IfIndex,
                                   (UINT1 *) MplsHwL3FTNInfo.au1DstMac);
        }
        MEMCPY (pXcEntry->pOutIndex->au1NextHopMac,
                MplsHwL3FTNInfo.au1DstMac, MAC_ADDR_LEN);
    }
    else
    {
        MEMCPY (MplsHwL3FTNInfo.au1DstMac, pXcEntry->pOutIndex->au1NextHopMac,
                MAC_ADDR_LEN);
    }
    pFtnEntry->u1ArpResolveStatus = MPLS_ARP_RESOLVED;
#ifdef MPLS_IPV6_WANTED
    if (pFtnEntry->u1AddrType == MPLS_IPV6_ADDR_TYPE)
    {
        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                    "Arp Resolve succeeded for FTN %s Next Hop \n",
                    Ip6PrintAddr (&NetAddr.Ip6Addr));
    }
    else
#endif
    {
        CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                    "Arp Resolve succeeded for FTN %x Next Hop %x\n",
                    MPLS_IPV4_U4_ADDR (NetAddr),
                    pXcEntry->pOutIndex->NHAddr.u4_addr[0]);
    }

    if (pXcEntry->pOutIndex->u4Label == MPLS_IMPLICIT_NULL_LABEL)
    {
        pFtnEntry->u1HwStatus = MPLS_TRUE;
#ifdef MPLS_IPV6_WANTED
        if (pFtnEntry->u1AddrType == MPLS_IPV6_ADDR_TYPE)
        {
            CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                        "FTN Add Imp-Null Lbl programming skipped "
                        "for FEC %s %s - Normal\n",
                        Ip6PrintAddr (&NetAddr.Ip6Addr),
                        Ip6PrintAddr (&NetMask.Ip6Addr));
        }
        else
#endif
        {
            CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                        "FTN Add Imp-Null Lbl programming skipped "
                        "for FEC %x %x - Normal\n", MPLS_IPV4_U4_ADDR (NetAddr),
                        MPLS_IPV4_U4_ADDR (NetMask));

        }
        return MPLS_SUCCESS;
    }

    MplsHwL3FTNInfo.u4EgrL3Intf = pXcEntry->pOutIndex->u4IfIndex;

    /* Fetch the L3 interface from the MPLS tunnel interface. */
    if (MplsGetL3Intf (MplsHwL3FTNInfo.u4EgrL3Intf, &u4L3Intf) != MPLS_SUCCESS)
    {
        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                    "FTNHwAdd : Fetching L3 interface failed "
                    "for tunnel interface %d\t\n", MplsHwL3FTNInfo.u4EgrL3Intf);
    }

    /* The VLAN Id will get updated with valid value only if the L3 interface
     * is IVR interface. */
    if (CfaGetVlanId (u4L3Intf, &(MplsHwL3FTNInfo.u2OutVlanId)) != CFA_SUCCESS)
    {
        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL, "FTNHwAdd : CfaGetVlanId Failed for L3 "
                    "interface %d\r\n", u4L3Intf);
    }

    /* Get the physical port for the corresponding Logical L3 interface. */
    if (MplsGetPhyPortFromLogicalIfIndex (u4L3Intf,
                                          &(MplsHwL3FTNInfo.au1DstMac[0]),
                                          &(MplsHwL3FTNInfo.u4OutPort))
        != MPLS_SUCCESS)
    {
        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                    "Fetching Physical port for the L3 interface : %d"
                    "failed.\r\n", u4L3Intf);
    }

    if (pFtnEntry->i4ActionType == MPLS_FTN_ON_NON_TE_LSP)
    {
        /* Fill the FEC Information
         * check wheather it is host/net route. and based on that assign to
         * proper struct member
         MplsHwL3FTNInfo.FecInfo.u4DstIpv4Host = u4NetAddr; */

        MplsHwL3FTNInfo.MplsLabelList[0].MplsLabelType =
            MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwL3FTNInfo.MplsLabelList[0].u.MplsShimLabel =
            pXcEntry->pOutIndex->u4Label;

        if (pXcEntry->mplsLabelIndex != NULL)
        {
            pLblEntry = (tLblEntry *)
                TMO_SLL_First (&(pXcEntry->mplsLabelIndex->LblList));

            if ((pLblEntry != NULL) &&
                (pLblEntry->u4Label != MPLS_IMPLICIT_NULL_LABEL))
            {
                MplsHwL3FTNInfo.MplsLabelList[1].MplsLabelType =
                    MPLS_HW_LABEL_TYPE_GENERIC;
                MplsHwL3FTNInfo.MplsLabelList[1].u.MplsShimLabel =
                    pLblEntry->u4Label;
            }
        }

#ifdef MPLS_IPV6_WANTED
        if (pFtnEntry->u1AddrType == MPLS_IPV6_ADDR_TYPE)
        {
            CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                        "Egress Interface Index for FTN %s is %d\n",
                        Ip6PrintAddr (&NetAddr.Ip6Addr),
                        MplsHwL3FTNInfo.u4EgrL3Intf);
        }
        else
#endif
        {
            CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                        "Egress Interface Index for FTN %x is %d\n",
                        MPLS_IPV4_U4_ADDR (NetAddr),
                        MplsHwL3FTNInfo.u4EgrL3Intf);
        }

        switch (MPLS_DIFFSERV_TNL_MODEL (MPLS_DEF_INCARN))
        {
            case UNIFORM_MODEL:
                MplsHwL3FTNInfo.DsLspModel = MPLS_HW_DS_LSP_MODEL_UNIFORM;
                break;
            case PIPE_MODEL:
                MplsHwL3FTNInfo.DsLspModel = MPLS_HW_DS_LSP_MODEL_PIPE;
                break;
            case SHORT_PIPE_MODEL:
                MplsHwL3FTNInfo.DsLspModel = MPLS_HW_DS_LSP_MODEL_SHORTPIPE;
                break;
            default:
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "Invalid Tunnel DIFFSRV model \n");
                return MPLS_FAILURE;
        }

        /* As per the RFC the outermost TTL Value needs to be selected.  */
        MplsHwL3FTNInfo.u4L3Ttl[0] = MPLS_TTL_VALUE (MPLS_DEF_INCARN);

        CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                    "FTN ADD:: Tunnel Model : %d TTL Value : %d \n",
                    MplsHwL3FTNInfo.DsLspModel, MplsHwL3FTNInfo.u4L3Ttl[0]);

#ifdef LDP_GR_WANTED
        /* HW list will contain both static and dynamic entries 
         * for NON-TE LDP Lsp's, but validation only to done form dynamic entries */
        if (pXcEntry->pOutIndex->u1Owner == MPLS_OWNER_LDP)
        {
            Fec.i4IpAddrType = pFtnEntry->u1AddrType;

#ifdef MPLS_IPV6_WANTED
            if (Fec.i4IpAddrType == MPLS_IPV6_ADDR_TYPE)
            {
                MEMCPY (&Fec.IpAddress, &(NetAddr.Ip6Addr), IPV6_ADDR_LENGTH);
                u1FtnHwListPrefLen =
                    MplsGetIpv6Subnetmasklen (MPLS_IPV6_U4_ADDR (NetMask));
            }
            else
#endif
            {
                MEMCPY (&Fec.IpAddress, &(NetAddr.u4Addr), IP_ADDR_LENGTH);
                MPLS_GET_PRFX_LEN_FROM_MASK (NetMask.u4Addr,
                                             u1FtnHwListPrefLen);
            }
            /* Get the FTN Hw List Entry */
            pFTNHwListEntry =
                MplsHwListGetFTNHwListEntry (Fec, u1FtnHwListPrefLen);
            if (pFTNHwListEntry != NULL
                && (MPLS_FTN_HW_LIST_STALE_STATUS (pFTNHwListEntry) ==
                    MPLS_TRUE))
            {
                CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                            "MplsFTNHwAdd: FTN Hw List Entry found "
                            "for FEC: %x\n", NetAddr);

                HwListNextHop.i4IpAddrType = pXcEntry->pOutIndex->u1NHAddrType;
#ifdef MPLS_IPV6_WANTED
                if (HwListNextHop.i4IpAddrType == MPLS_IPV6_ADDR_TYPE)
                {
                    MEMCPY (&HwListNextHop.IpAddress.Ip6Addr,
                            (pXcEntry->pOutIndex->NHAddr.u1_addr),
                            IPV6_ADDR_LENGTH);
                }
                else
#endif
                {
                    /* For IPv6, this needs to be changed */
                    MEMCPY (HwListNextHop.IpAddress.au1Ipv4Addr,
                            &(pXcEntry->pOutIndex->NHAddr.u4_addr[0]),
                            IPV4_ADDR_LENGTH);
                }

                MEMCPY (&Label, &(pXcEntry->pOutIndex->u4Label),
                        sizeof (uLabel));

                /* If the Next-hop and Label from the peer is same  and Tunnel interface
                 * remains the same and the entry is stale, then mark it unstale and return */

                /* Validate the Hw List Entry */
                if ((MplsHwListValidateFTNHwListEntry
                     (pFTNHwListEntry, Label, HwListNextHop) == MPLS_TRUE)
                    && (MPLS_FTN_HW_LIST_OUT_TNL_INTF (pFTNHwListEntry) ==
                        pXcEntry->pOutIndex->u4IfIndex))
                {
                    CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                                "MplsFTNHwAdd: FTN Hw List Entry Validated properly,"
                                "Hence, marking the entry as Unstale for the FEC: %x\n",
                                NetAddr.u4Addr);
                    MPLS_FTN_HW_LIST_STALE_STATUS (pFTNHwListEntry) =
                        MPLS_FALSE;
                    /* Mark the Hw Status as TRUE */
                    pFtnEntry->u1HwStatus = MPLS_TRUE;
                    return MPLS_SUCCESS;
                }
                else
                {
                    CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                                "MplsFTNHwAdd: FTN Hw List Not Validated, Hence "
                                "Deleting it from the H/w for FEC: %x\n",
                                NetAddr.u4Addr);

                    i4RetVal =
                        MplsHwListDeleteFTNEntryFromHw (pFTNHwListEntry, NULL);

                    /* If the Tunnel interface has changed, delete it as well */
                    if ((MPLS_FTN_HW_LIST_OUT_TNL_INTF (pFTNHwListEntry)) !=
                        pXcEntry->pOutIndex->u4IfIndex)
                    {
                        if (MPLS_FTN_HW_LIST_IS_STATIC (pFTNHwListEntry) ==
                            MPLS_FALSE)
                        {
                            if (CfaIfmDeleteStackMplsTunnelInterface
                                (MPLS_FTN_HW_LIST_OUT_L3_INTF (pFTNHwListEntry),
                                 MPLS_FTN_HW_LIST_OUT_TNL_INTF
                                 (pFTNHwListEntry)) == CFA_FAILURE)
                            {
                                CMNDB_DBG1 (DEBUG_ERROR_LEVEL,
                                            "MplsHwListDeleteFTNEntryFromHw: Failed to Delete the "
                                            "TUNNEL interface: %d\n",
                                            MPLS_FTN_HW_LIST_OUT_TNL_INTF
                                            (pFTNHwListEntry));
                                return MPLS_FAILURE;
                            }
                        }
                    }
                    if (i4RetVal == MPLS_FAILURE)
                    {
                        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                                    "MplsFTNHwAdd: Error in Deleting Invalidated "
                                    "FTN hw List Entry for FEC: %x\n",
                                    NetAddr.u4Addr);
                        return MPLS_FAILURE;
                    }
                }
            }
        }
#endif

#ifdef MBSM_WANTED
        if (pSlotInfo != NULL)
        {
#ifdef LDP_GR_WANTED

            /* Init the FTN Hw List Entry based on the FTN Entry, XC Entry */
            if (MplsHwListInitFTNHwListEntry (&FTNHwListEntry, pFtnEntry,
                                              pXcEntry,
                                              u4L3Intf) == MPLS_FAILURE)
            {
                CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                            "MplsFTNHwAdd: FTN Hw List Init Failed for FEC: %x\n",
                            NetAddr.u4Addr);
                return MPLS_FAILURE;
            }

            /* Add the FTN Hw List Entry to Hw List */
            if (MplsHwListAddOrUpdateFTNHwListEntry (&FTNHwListEntry, 0) ==
                MPLS_FAILURE)
            {
                CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                            "MplsFTNHwAdd: FTN Hw List Addition Failed for FEC: %x\n",
                            NetAddr.u4Addr);
                return MPLS_FAILURE;
            }

            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                        "MplsFTNHwAdd: FTN Hw List Addition Successful for FEC: %x\n",
                        NetAddr.u4Addr);
#endif

            /*Fetch Qos Info for the Outgoing Interface */
            if (MplsHwL3FTNInfo.u4OutPort != 0)
            {
                u4OutPort = MplsHwL3FTNInfo.u4OutPort;
            }
            else
            {
                MEMCPY (pHwPortInfo.au1Mac,
                        MplsHwL3FTNInfo.au1DstMac, MAC_ADDR_LEN);
                pHwPortInfo.u2OutVlanId = MplsHwL3FTNInfo.u2OutVlanId;
                pHwPortInfo.u1MsgType = ISS_NP_GET_PORT_INFO;
                CfaNpGetHwPortInfo (&pHwPortInfo);
                u4OutPort = pHwPortInfo.u4OutPort;
            }
#ifdef QOSX_WANTED
            QosGetMplsExpProfileForPortAtEgress (u4OutPort, &i4HwExpMapId);
            MplsHwL3FTNInfo.QosInfo[0].u.MplsQosPolicy.i4HwExpMapId =
                i4HwExpMapId;
#endif
            if ((MplsFsMplsMbsmHwCreateL3FTN
                 (u4VpnId, &MplsHwL3FTNInfo, pSlotInfo)) == FNP_FAILURE)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           " INT Exit - MBSM H/W Create of L3 FTN failed\n");
                return MPLS_FAILURE;
            }
#ifdef LDP_GR_WANTED
            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                        "MplsFTNHwAdd: Updating the NP bit to NPAPI_FTN_PROGRAMMED "
                        "for FEC: %x\n", NetAddr.u4Addr);

            /* Update the NPAPI bit in the FTN hw list to NPAPI_PROGRAMMED */
            if (MplsHwListAddOrUpdateFTNHwListEntry (&FTNHwListEntry,
                                                     NPAPI_FTN_CALLED |
                                                     NPAPI_FTN_PROGRAMMED) ==
                MPLS_FAILURE)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "MplsFTNHwAdd: Failed to Update the FTN Hw List Entry\n");
                return MPLS_FAILURE;
            }
#endif
        }
        else
#endif
        {
#ifdef L2RED_WANTED
            if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
            {
#ifdef NPAPI_WANTED
#ifdef LDP_GR_WANTED

                /* Init the FTN Hw List Entry based on the FTN Entry, XC Entry */
                if (MplsHwListInitFTNHwListEntry (&FTNHwListEntry, pFtnEntry,
                                                  pXcEntry,
                                                  u4L3Intf) == MPLS_FAILURE)
                {
                    CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                                "MplsFTNHwAdd: FTN Hw List Init Failed for FEC: %x\n",
                                NetAddr.u4Addr);
                    return MPLS_FAILURE;
                }

                /* Add the FTN Hw List Entry to Hw List */
                if (MplsHwListAddOrUpdateFTNHwListEntry (&FTNHwListEntry, 0) ==
                    MPLS_FAILURE)
                {
                    CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                                "MplsFTNHwAdd: FTN Hw List Addition Failed for FEC: %x\n",
                                NetAddr.u4Addr);
                    return MPLS_FAILURE;
                }

                CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                            "MplsFTNHwAdd: FTN Hw List Addition Successful for FEC: %x\n",
                            NetAddr.u4Addr);
#endif
                /*Fetch Qos Info for the Outgoing Interface */
                if (MplsHwL3FTNInfo.u4OutPort != 0)
                {
                    u4OutPort = MplsHwL3FTNInfo.u4OutPort;
                }
                else
                {
                    MEMCPY (pHwPortInfo.au1Mac,
                            MplsHwL3FTNInfo.au1DstMac, MAC_ADDR_LEN);
                    pHwPortInfo.u2OutVlanId = MplsHwL3FTNInfo.u2OutVlanId;
                    pHwPortInfo.u1MsgType = ISS_NP_GET_PORT_INFO;
                    CfaNpGetHwPortInfo (&pHwPortInfo);
                    u4OutPort = pHwPortInfo.u4OutPort;
                }
#ifdef QOSX_WANTED
                QosGetMplsExpProfileForPortAtEgress (u4OutPort, &i4HwExpMapId);
                MplsHwL3FTNInfo.QosInfo[0].u.MplsQosPolicy.i4HwExpMapId =
                    i4HwExpMapId;
#endif
                if (MplsFsMplsHwCreateL3FTN (u4VpnId, &MplsHwL3FTNInfo) ==
                    FNP_FAILURE)
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               " INT Exit - H/W Create of L3 FTN failed\n");
                    return MPLS_FAILURE;
                }
#ifdef LDP_GR_WANTED
                CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                            "MplsFTNHwAdd: Updating the NP bit to NPAPI_FTN_PROGRAMMED "
                            "for FEC: %x\n", NetAddr.u4Addr);

                if (MplsHwListAddOrUpdateFTNHwListEntry (&FTNHwListEntry,
                                                         NPAPI_FTN_CALLED |
                                                         NPAPI_FTN_PROGRAMMED)
                    == MPLS_FAILURE)
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               "MplsFTNHwAdd: Failed to Update the FTN Hw List Entry\n");
                    return MPLS_FAILURE;
                }
#endif
#endif
            }
        }
#ifdef MPLS_IPV6_WANTED
        if (pFtnEntry->u1AddrType == MPLS_IPV6_ADDR_TYPE)
        {
            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                        "L3 FTN in HW for %s Created Successfully\n",
                        &NetAddr.Ip6Addr);
        }
        else
#endif
        {
            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                        "L3 FTN in HW for %x Created Successfully\n",
                        MPLS_IPV4_U4_ADDR (NetAddr));
        }
    }
    if (!(pFtnEntry->bIsHostAddrFecForPSN))
    {
        FSNP_INIT_NEXTHOP_INFO (NextHop);
#ifdef MPLS_IPV6_WANTED
        if (pFtnEntry->u1AddrType == MPLS_IPV6_ADDR_TYPE)
        {

            u1PrefixLen = MplsGetIpv6Subnetmasklen ((UINT1 *) &NetMask.Ip6Addr);
            /* For Static FTN Configuration */
            if (0 == u1PrefixLen)
            {
                u1PrefixLen = 64;    /*Default netmask for IPv6. Create a macro */
            }

            NetIpv6RtInfoQueryMsg.u4ContextId = VCM_DEFAULT_CONTEXT;
            NetIpv6RtInfoQueryMsg.u1QueryFlag = RTM6_QUERIED_FOR_NEXT_HOP;
            Ip6AddrCopy (&(NetIpv6RtInfo.Ip6Dst), &(NetAddr.Ip6Addr));
            NetIpv6RtInfo.u1Prefixlen = u1PrefixLen;

            if (NetIpv6GetRoute (&NetIpv6RtInfoQueryMsg,
                                 &NetIpv6RtInfo) != NETIPV6_SUCCESS)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           " INT Exit - Route doesn't exist \n");
                return MPLS_FAILURE;
                /* To discuss if failure to be returned from here. 
                   Also route delete should happen only in case it exists */
            }

#ifdef L2RED_WANTED
            if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
            {
#ifdef NPAPI_WANTED

                /* Delete the best route from fast path table */
                RouteInfo.u1RouteCount = NetIpv6RtInfo.u1EcmpCount;
                if (Ipv6FsNpIpv6UcRouteDelete (VCM_DEFAULT_CONTEXT,
                                               (UINT1 *) &NetIpv6RtInfo.Ip6Dst,
                                               u1PrefixLen,
                                               (UINT1 *) &NetIpv6RtInfo.NextHop,
                                               NetIpv6RtInfo.u4Index,
                                               &RouteInfo) == FNP_SUCCESS)
                {
                    /* After deleting, add mpls route as the best route to *                      
                       / * fast path table  */

                    CMNDB_DBG4 (DEBUG_DEBUG_LEVEL,
                                "MplsFTNAdd - IP route successfully deleted for Dest:%s, PrefixLen:%d, Next Hop:%s"
                                "Interface indx:%d\n",
                                Ip6PrintAddr (&NetIpv6RtInfo.Ip6Dst),
                                u1PrefixLen,
                                Ip6PrintAddr (&NetIpv6RtInfo.NextHop),
                                NetIpv6RtInfo.u4Index);

                    MEMCPY (&NetIpv6RtInfo.NextHop,
                            pXcEntry->pOutIndex->NHAddr.u1_addr,
                            IPV6_ADDR_LENGTH);

                    IntInfo.u1IfType = Ip6GetIfType (NetIpv6RtInfo.u4Index);
                    IntInfo.u4PhyIfIndex = MplsHwL3FTNInfo.u4EgrL3Intf;
                    IntInfo.u2VlanId = MplsHwL3FTNInfo.u2OutVlanId;
#ifdef L2RED_WANTED
                    if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
                    {

#ifdef LDP_GR_WANTED
                        if (pFtnEntry->i4ActionType == MPLS_FTN_ON_NON_TE_LSP)
                        {
                            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                                        "MplsFTNHwAdd: Updating the NP bit to NPAPI_ROUTE_CALLED "
                                        "for FEC: %x\n", NetAddr.u4Addr);
                            /* Update the MPLS Route Called bit in the NP bit mask */
                            if (MplsHwListAddOrUpdateFTNHwListEntry
                                (&FTNHwListEntry,
                                 NPAPI_FTN_CALLED | NPAPI_FTN_PROGRAMMED |
                                 NPAPI_MPLS_ROUTE_CALLED) == MPLS_FAILURE)
                            {
                                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                                           "MplsFTNHwAdd: Failed to Update the FTN Hw List Entry\n");
                                return MPLS_FAILURE;
                            }

                        }
#endif

                        if (MplsFsMplsHwAddMplsIpv6Route (VCM_DEFAULT_CONTEXT,
                                                          (UINT1 *)
                                                          &NetIpv6RtInfo.Ip6Dst,
                                                          u1PrefixLen,
                                                          (UINT1 *)
                                                          &NetIpv6RtInfo.
                                                          NextHop, u4NHType,
                                                          &IntInfo) ==
                            FNP_FAILURE)

                        {
                            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                                       " INT Exit - Fast path MPLS route add failed\n");
                            return MPLS_FAILURE;
                        }
                        CMNDB_DBG7 (DEBUG_DEBUG_LEVEL,
                                    "MplsFTNAdd - MPLS route successfully added for Dest:%s, PrefixLen:%d, Next Hop:%s"
                                    "NHType:%d, Interface type:%d indx:%d, VLAN id:%d\n",
                                    Ip6PrintAddr (&NetIpv6RtInfo.Ip6Dst),
                                    u1PrefixLen,
                                    Ip6PrintAddr (&NetIpv6RtInfo.NextHop),
                                    u4NHType, IntInfo.u1IfType,
                                    IntInfo.u4PhyIfIndex, IntInfo.u2VlanId);
#ifdef LDP_GR_WANTED
                        if (pFtnEntry->i4ActionType == MPLS_FTN_ON_NON_TE_LSP)
                        {
                            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                                        "MplsFTNHwAdd: Updating the NP bit to NPAPI_ROUTE_PROGRAMMED "
                                        "for FEC: %x\n", NetAddr.u4Addr);
                            /* Update the MPLS Route Programmed bit in the NP bit mask */
                            if (MplsHwListAddOrUpdateFTNHwListEntry
                                (&FTNHwListEntry,
                                 NPAPI_FTN_CALLED | NPAPI_FTN_PROGRAMMED |
                                 NPAPI_MPLS_ROUTE_CALLED |
                                 NPAPI_MPLS_ROUTE_PROGRAMMED) == MPLS_FAILURE)
                            {
                                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                                           "MplsFTNHwAdd: Failed to Update the FTN Hw List Entry\n");
                                return MPLS_FAILURE;
                            }
                        }
#endif

                    }
                }
                else
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               " Fast path Best route delete failed\n");
                    return MPLS_FAILURE;
                }
#endif
            }
        }

        else
#endif
        {
            /* For Static FTN Configuration */
            if (MPLS_IPV4_U4_ADDR (NetMask) == 0)
            {
                MPLS_IPV4_U4_ADDR (NetMask) =
                    MplsGetDefaultIpNetmask (MPLS_IPV4_U4_ADDR (NetAddr));
            }

            MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
            MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));
            RtQuery.u4DestinationIpAddress = MPLS_IPV4_U4_ADDR (NetAddr);
            RtQuery.u4DestinationSubnetMask = 0xFFFFFFFF;
            RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
            if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_FAILURE)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           " INT Exit - Route doesn't exist \n");
            }
            NextHop.u4NextHopGt = NetIpRtInfo.u4NextHop;
            NextHop.u1RtCount = 1;

#ifdef L2RED_WANTED
            if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
            {
#ifdef NPAPI_WANTED
                /* Delete the best route from fast path table */
                if (MplsFsNpIpv4UcDelRoute
                    (0 /* IP_VRID */ , MPLS_IPV4_U4_ADDR (NetAddr),
                     MPLS_IPV4_U4_ADDR (NetMask),
                     NextHop, &i4TblFull) == FNP_SUCCESS)
                {
                    /* After deleting, add mpls route as the best route to 
                     * fast path table 
                     */
                    MEMCPY (&NextHop.u4NextHopGt,
                            pXcEntry->pOutIndex->NHAddr.u4_addr,
                            IP_ADDR_LENGTH);

                    NextHop.u4IfIndex = MplsHwL3FTNInfo.u4EgrL3Intf;
                    NextHop.u2VlanId = MplsHwL3FTNInfo.u2OutVlanId;
#ifdef L2RED_WANTED
                    if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
                    {
#ifdef LDP_GR_WANTED
                        if (pFtnEntry->i4ActionType == MPLS_FTN_ON_NON_TE_LSP)
                        {
                            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                                        "MplsFTNHwAdd: Updating the NP bit to NPAPI_ROUTE_CALLED "
                                        "for FEC: %x\n", NetAddr.u4Addr);
                            /* Update the MPLS Route Called bit in the NP bit mask */
                            if (MplsHwListAddOrUpdateFTNHwListEntry
                                (&FTNHwListEntry,
                                 NPAPI_FTN_CALLED | NPAPI_FTN_PROGRAMMED |
                                 NPAPI_MPLS_ROUTE_CALLED) == MPLS_FAILURE)
                            {
                                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                                           "MplsFTNHwAdd: Failed to Update the FTN Hw List Entry\n");
                                return MPLS_FAILURE;
                            }

                        }
#endif
                        if (MplsFsMplsHwAddMplsRoute
                            (0 /* IP_VRID */ , MPLS_IPV4_U4_ADDR (NetAddr),
                             MPLS_IPV4_U4_ADDR (NetMask), NextHop,
                             &u1TblFull) == FNP_FAILURE)

                        {
                            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                                       " INT Exit - Fast path MPLS route add failed\n");
                            return MPLS_FAILURE;
                        }
                    }
#ifdef LDP_GR_WANTED
                    if (pFtnEntry->i4ActionType == MPLS_FTN_ON_NON_TE_LSP)
                    {
                        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                                    "MplsFTNHwAdd: Updating the NP bit to NPAPI_ROUTE_PROGRAMMED "
                                    "for FEC: %x\n", NetAddr.u4Addr);
                        /* Update the MPLS Route Programmed bit in the NP bit mask */
                        if (MplsHwListAddOrUpdateFTNHwListEntry
                            (&FTNHwListEntry,
                             NPAPI_FTN_CALLED | NPAPI_FTN_PROGRAMMED |
                             NPAPI_MPLS_ROUTE_CALLED |
                             NPAPI_MPLS_ROUTE_PROGRAMMED) == MPLS_FAILURE)
                        {
                            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                                       "MplsFTNHwAdd: Failed to Update the FTN Hw List Entry\n");
                            return MPLS_FAILURE;
                        }
                    }
#endif
                }
                else
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               " Fast path Best route delete failed\n");
                    return MPLS_FAILURE;
                }
#endif
            }
        }
        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                    " Cur Best Rt deleted and Mpls Rt added in Fast path for "
                    "FTN %x\n", MPLS_IPV4_U4_ADDR (NetAddr));
    }

    pFtnEntry->u1HwStatus = MPLS_TRUE;

    if (pFtnEntry->i4ActionType == MPLS_FTN_ON_TE_LSP)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, " MplsFTNHwAdd : Set u1IsFtnMapped \n");

        pTeTnlInfo->u1IsFtnMapped = MPLS_TRUE;
    }

    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsFTNHwAdd: Exit\n");
    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsFTNDelete
 *  Description     : Function to delete FTN entry from NP
 *  Input           : FTN Info
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE
 ************************************************************************/
INT4
MplsFTNDel (tFtnEntry * pFtnEntry)
{
    return (MplsFTNHwDel (pFtnEntry, NULL));
}

/************************************************************************
 *  Function Name   : MplsFTNHwDel
 *  Description     : Function to delete FTN entry from NP
 *  Input           : FTN Info, Slot Info
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE
 ************************************************************************/
INT4
MplsFTNHwDel (tFtnEntry * pFtnEntry, VOID *pSlotInfo)
{
    tXcEntry           *pXcEntry = NULL;
    tFsNpNextHopInfo    NextHop;
    tMplsHwL3FTNInfo    MplsHwL3FTNInfo;
#ifdef NPAPI_WANTED
    INT4                i4TblFull = 0;
    tRtInfoQueryMsg     RtQuery;
#endif
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4EgrL3Intf = 0;
    uGenU4Addr          NetAddr;
    uGenU4Addr          NetMask;
    UINT4               u4TnlXcIndex = 0;
    UINT4               u4NextHop = 0;
    UINT4               u4Direction = 0;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;
#ifdef NPAPI_WANTED
    UINT4               u4VpnId = MPLS_DEF_VRF;
#endif
    UINT4               u4L3Intf = 0;
#ifdef MPLS_IPV6_WANTED
    tNetIpv6RtInfoQueryMsg NetIpv6RtInfoQueryMsg;
    tNetIpv6RtInfo      NetIpv6RtInfo;
    tFsNpIntInfo        IntInfo;
    tFsNpRouteInfo      RouteInfo;
    UINT1               u1PrefixLen = 0;
    tIp6Addr            NextHopV6;
#ifdef NPAPI_WANTED
    UINT4               u4NHType = 0;
#endif
#endif

#ifdef LDP_GR_WANTED
    tMplsIpAddress      FtnHwListFec;
    tFTNHwListEntry    *pFTNHwListEntry = NULL;
    UINT1               u1FtnHwListPrefLen = 0;
#if defined(NPAPI_WANTED) || defined(MBSM_WANTED)
    UINT4               u4NpBitMask = 0;
#endif
#endif

#ifndef MBSM_WANTED
    UNUSED_PARAM (pSlotInfo);
#endif

    MEMSET (&MplsHwL3FTNInfo, 0, sizeof (tMplsHwL3FTNInfo));
#ifdef LDP_GR_WANTED
    MEMSET (&FtnHwListFec, 0, sizeof (tMplsIpAddress));
#endif
    MEMSET (&NetAddr, 0, sizeof (uGenU4Addr));
    MEMSET (&NetMask, 0, sizeof (uGenU4Addr));
#ifdef MPLS_IPV6_WANTED
    MEMSET (&NetIpv6RtInfoQueryMsg, 0, sizeof (tNetIpv6RtInfoQueryMsg));
    MEMSET (&NetIpv6RtInfo, 0, sizeof (tNetIpv6RtInfo));
    MEMSET (&IntInfo, 0, sizeof (tFsNpIntInfo));
    MEMSET (&NextHopV6, 0, sizeof (tIp6Addr));
    MEMSET (&RouteInfo, 0, sizeof (tFsNpRouteInfo));
#endif

    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsFTNHwDel: Entry\n");

    if (pFtnEntry->pActionPtr == NULL)
    {
        /* No Cross Connect Info */
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsFTNHwDel : Action pointer not present\n");
        return MPLS_FAILURE;
    }
    if (pFtnEntry->i4ActionType == MPLS_FTN_ON_TE_LSP)
    {
        pTeTnlInfo = pFtnEntry->pActionPtr;

        if (pTeTnlInfo == NULL)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL, " INT Exit - Tunnel is NULL\n");

            return MPLS_FAILURE;
        }

        if (pTeTnlInfo->u4TnlInstance == MPLS_ZERO)
        {
            CONVERT_TO_INTEGER (pTeTnlInfo->TnlIngressLsrId, u4IngressId);
            CONVERT_TO_INTEGER (pTeTnlInfo->TnlEgressLsrId, u4EgressId);

            u4IngressId = OSIX_NTOHL (u4IngressId);
            u4EgressId = OSIX_NTOHL (u4EgressId);

            pTeTnlInfo = TeGetTunnelInfo (pTeTnlInfo->u4TnlIndex,
                                          pTeTnlInfo->u4TnlPrimaryInstance,
                                          u4IngressId, u4EgressId);

            if (pTeTnlInfo == NULL)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           " INT Exit - Primary Tunnel is NULL\n");

                return MPLS_FAILURE;
            }
        }

        if (pTeTnlInfo->u1DetourActive == MPLS_TRUE)
        {
            u4TnlXcIndex = pTeTnlInfo->u4OrgTnlXcIndex;
        }
        else
        {
            u4TnlXcIndex = pTeTnlInfo->u4TnlXcIndex;
        }

        if (pTeTnlInfo->u1TnlRole == TE_EGRESS)
        {
            u4Direction = MPLS_DIRECTION_REVERSE;
        }
        else
        {
            u4Direction = MPLS_DIRECTION_FORWARD;
        }
        pXcEntry = MplsGetXCEntryByDirection (u4TnlXcIndex,
                                              (eDirection) u4Direction);
    }
    else
    {
        pXcEntry = pFtnEntry->pActionPtr;
    }
    if ((pXcEntry == NULL) || (pXcEntry->pOutIndex == NULL)
        || (pXcEntry->pOutIndex->u4IfIndex == 0))
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsFTNHwDel : XC/XC->OUT/XC->OUT->IF not present\n");
        return MPLS_FAILURE;
    }
#ifdef MPLS_IPV6_WANTED
    if (pFtnEntry->u1AddrType == MPLS_IPV6_ADDR_TYPE)
    {
        MEMCPY ((UINT1 *) &NetAddr, (UINT1 *) &pFtnEntry->DestAddrMin,
                IPV6_ADDR_LENGTH);
        MEMCPY ((UINT1 *) &NetMask, (UINT1 *) &pFtnEntry->DestAddrMax,
                IPV6_ADDR_LENGTH);
    }
    else
#endif
    {
        MEMCPY ((UINT1 *) &NetAddr, (UINT1 *) &pFtnEntry->DestAddrMin,
                IPV4_ADDR_LENGTH);
        MEMCPY ((UINT1 *) &NetMask, (UINT1 *) &pFtnEntry->DestAddrMax,
                IPV4_ADDR_LENGTH);
    }

    if (pXcEntry->pOutIndex->u4Label == MPLS_IMPLICIT_NULL_LABEL)
    {
        pFtnEntry->u1HwStatus = MPLS_FALSE;
#ifdef MPLS_IPV6_WANTED
        if (pFtnEntry->u1AddrType == MPLS_IPV6_ADDR_TYPE)
        {
            CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                        "FTN Del Imp-Null Lbl programming skipped for FEC %x %x\n",
                        Ip6PrintAddr (&NetAddr.Ip6Addr),
                        Ip6PrintAddr (&NetMask.Ip6Addr));
        }
        else
#endif
        {
            CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                        "FTN Del Imp-Null Lbl programming skipped for FEC %x %x\n",
                        MPLS_IPV4_U4_ADDR (NetAddr),
                        MPLS_IPV4_U4_ADDR (NetMask));
        }
        return MPLS_SUCCESS;
    }

    if ((pFtnEntry->i4ActionType == MPLS_FTN_ON_TE_LSP) &&
        (pTeTnlInfo->u1TnlIsIf == TE_TRUE))
    {
        if ((TE_TNL_ROLE (pTeTnlInfo) == TE_INGRESS) &&
            (pTeTnlInfo->u1DetourActive == MPLS_TRUE))
        {
            u4EgrL3Intf = pTeTnlInfo->u4OrgTnlIfIndex;
            u4NextHop = pTeTnlInfo->u4BkpNextHopAddr;
            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL, "FTN Del u4NextHop = %x \n",
                        u4NextHop);
        }
        else
        {
            u4EgrL3Intf = pTeTnlInfo->u4TnlIfIndex;
            MEMCPY (&u4NextHop, pXcEntry->pOutIndex->NHAddr.u4_addr,
                    IP_ADDR_LENGTH);
            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL, "FTN Del u4NextHop = %x \n",
                        u4NextHop);
        }
    }
    else
    {
        u4EgrL3Intf = pXcEntry->pOutIndex->u4IfIndex;
        MEMCPY (&u4NextHop, pXcEntry->pOutIndex->NHAddr.u4_addr,
                IP_ADDR_LENGTH);
        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL, "FTN Del u4NextHop = %x \n", u4NextHop);
    }

#ifdef LDP_GR_WANTED
    FtnHwListFec.i4IpAddrType = pFtnEntry->u1AddrType;

#ifdef MPLS_IPV6_WANTED
    if (pFtnEntry->u1AddrType == MPLS_IPV6_ADDR_TYPE)
    {
        MEMCPY ((UINT1 *) &FtnHwListFec.IpAddress, (UINT1 *) &NetAddr,
                IPV6_ADDR_LENGTH);
        u1FtnHwListPrefLen =
            MplsGetIpv6Subnetmasklen (MPLS_IPV6_U4_ADDR (NetMask));
    }
    else
#endif
    {
        MEMCPY ((UINT1 *) &FtnHwListFec.IpAddress, (UINT1 *) &NetAddr,
                IP_ADDR_LENGTH);
        MPLS_GET_PRFX_LEN_FROM_MASK (NetMask.u4Addr, u1FtnHwListPrefLen);
    }

    pFTNHwListEntry = MplsHwListGetFTNHwListEntry (FtnHwListFec,
                                                   u1FtnHwListPrefLen);
    if (pFTNHwListEntry != NULL)
    {
        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                    "MplsFTNHwDel: FTN Hw List Entry Found for " "FEC: %x\n",
                    NetAddr);
    }
#endif
    if (!(pFtnEntry->bIsHostAddrFecForPSN))
    {
        FSNP_INIT_NEXTHOP_INFO (NextHop);

#ifdef MPLS_IPV6_WANTED
        if (pFtnEntry->u1AddrType == MPLS_IPV6_ADDR_TYPE)
        {
            MEMCPY (&NextHopV6, pXcEntry->pOutIndex->NHAddr.u1_addr,
                    IPV6_ADDR_LENGTH);

            u1PrefixLen = MplsGetIpv6Subnetmasklen ((UINT1 *) &NetMask.Ip6Addr);

            /* For Static FTN Configuration */
            if (0 == u1PrefixLen)
            {
                u1PrefixLen = 64;    /*Default netmask for IPv6. Create a macro */
            }
#ifdef L2RED_WANTED
            if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
            {
#ifdef NPAPI_WANTED

#ifdef LDP_GR_WANTED
                if (pFTNHwListEntry != NULL)
                {
                    CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                                "MplsFTNHwDel: FTN Hw List Entry found for the FEC: %x\n",
                                NetAddr);

                    u4NpBitMask = MPLS_FTN_HW_LIST_NP_STATUS (pFTNHwListEntry);

                    /* Reset the MPLS Route NP bits */
                    MPLS_HW_LIST_RESET_NP_BIT (u4NpBitMask,
                                               (UINT4) (NPAPI_MPLS_ROUTE_CALLED
                                                        |
                                                        NPAPI_MPLS_ROUTE_PROGRAMMED));

                    /* Update the NP Bits in the existing the FTN Hw List Entry */
                    MplsHwListAddOrUpdateFTNHwListEntry (pFTNHwListEntry,
                                                         u4NpBitMask);
                }
#endif

                /* Delete the mpls route from fast path table */
                RouteInfo.u1RouteCount = 1;
                if (MplsFsMplsHwDeleteMplsIpv6Route (VCM_DEFAULT_CONTEXT,
                                                     (UINT1 *) &NetAddr.Ip6Addr,
                                                     u1PrefixLen,
                                                     (UINT1 *) &NextHopV6,
                                                     u4EgrL3Intf,
                                                     &RouteInfo) == FNP_SUCCESS)
                {
                    pFtnEntry->u1HwStatus = MPLS_FALSE;
                    /* After deleting mpls based route, restore the best route as by
                     * maintained RTM in the fast path table.
                     */
                    NetIpv6RtInfoQueryMsg.u4ContextId = VCM_DEFAULT_CONTEXT;
                    NetIpv6RtInfoQueryMsg.u1QueryFlag =
                        RTM6_QUERIED_FOR_NEXT_HOP;
                    Ip6AddrCopy (&NetIpv6RtInfo.Ip6Dst, &(NetAddr.Ip6Addr));
                    NetIpv6RtInfo.u1Prefixlen = u1PrefixLen;

                    if (NetIpv6GetRoute (&NetIpv6RtInfoQueryMsg,
                                         &NetIpv6RtInfo) != NETIPV6_SUCCESS)
                    {
                        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                                   " INT Exit - Route doesn't exist \n");
                        /* To discuss if failure to be returned from here. 
                           Also route delete should happen only in case it exists */
                    }
                    else
                    {
                        /* Reference - Rtm6FillIntInfo */
                        IntInfo.u1IfType = Ip6GetIfType (NetIpv6RtInfo.u4Index);
                        if (IntInfo.u1IfType == CFA_L3IPVLAN)
                        {
                            IntInfo.u4PhyIfIndex = NetIpv6RtInfo.u4Index;
                        }
                        if (IntInfo.u1IfType == CFA_PSEUDO_WIRE)
                        {
                            IntInfo.u4PhyIfIndex = NetIpv6RtInfo.u4Index;
                        }
                        if (IntInfo.u1IfType != IP6_ENET_INTERFACE_TYPE)
                        {
                            if (CfaGetVlanId
                                (IntInfo.u4PhyIfIndex,
                                 &(IntInfo.u2VlanId)) == CFA_FAILURE)
                            {
                                return MPLS_FAILURE;
                            }
                        }
                        else
                        {
                            IntInfo.u4PhyIfIndex = NetIpv6RtInfo.u4Index;
                            IntInfo.u2VlanId = 0;
                        }

                        /* check for the type before programming */
                        if (NetIpv6RtInfo.i1Type == IP6_ROUTE_TYPE_DIRECT)
                        {
                            u4NHType = NH_DIRECT;
                        }
                        else if (NetIpv6RtInfo.i1Type == IP6_ROUTE_TYPE_DISCARD)
                        {
                            /* Route type if set as discard type previously is
                             * now reverted to DIRECT Type */
                            u4NHType = NH_DIRECT;
                        }
                        else if (NetIpv6RtInfo.i1Type ==
                                 IP6_ROUTE_TYPE_INDIRECT)
                        {
                            u4NHType = NH_REMOTE;
                        }
                        else
                        {
                            u4NHType = NH_SENDTO_CP;
                        }

                        if (Ipv6FsNpIpv6UcRouteAdd (VCM_DEFAULT_CONTEXT,
                                                    (UINT1 *) &NetIpv6RtInfo.
                                                    Ip6Dst, u1PrefixLen,
                                                    (UINT1 *) &NetIpv6RtInfo.
                                                    NextHop, u4NHType,
                                                    &IntInfo) == FNP_FAILURE)

                        {
                            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                                       " INT Exit - Fast path IPv6 best route add failed\n");
                            return MPLS_FAILURE;
                        }
                    }
                }
                else
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               "FTN Deletion failed: MPLS Route does not exist \n");
                    /* Fast path route delete fail */
                    /* Failure Not returned explicitly */
                }
#endif
            }
        }
        else
#endif
        {
            NextHop.u4NextHopGt = u4NextHop;
            NextHop.u4IfIndex = u4EgrL3Intf;
            NextHop.u1RtCount = 1;

            CMNDB_DBG4 (DEBUG_DEBUG_LEVEL, "FEC %x %x GW %x OutIf %d\n",
                        MPLS_IPV4_U4_ADDR (NetAddr),
                        MPLS_IPV4_U4_ADDR (NetMask), NextHop.u4NextHopGt,
                        NextHop.u4IfIndex);
            /* For Static FTN Configuration */
            if (MPLS_IPV4_U4_ADDR (NetMask) == 0)
            {
                MPLS_IPV4_U4_ADDR (NetMask) =
                    MplsGetDefaultIpNetmask (MPLS_IPV4_U4_ADDR (NetAddr));
            }
#ifdef L2RED_WANTED
            if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
            {
#ifdef NPAPI_WANTED
#ifdef LDP_GR_WANTED
                if (pFTNHwListEntry != NULL)
                {
                    CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                                "MplsFTNHwDel: FTN Hw List Entry found for the FEC: %x\n",
                                NetAddr);

                    u4NpBitMask = MPLS_FTN_HW_LIST_NP_STATUS (pFTNHwListEntry);

                    /* Reset the MPLS Route NP bits */
                    MPLS_HW_LIST_RESET_NP_BIT (u4NpBitMask,
                                               (UINT4) (NPAPI_MPLS_ROUTE_CALLED
                                                        |
                                                        NPAPI_MPLS_ROUTE_PROGRAMMED));

                    /* Update the NP Bits in the existing the FTN Hw List Entry */
                    MplsHwListAddOrUpdateFTNHwListEntry (pFTNHwListEntry,
                                                         u4NpBitMask);
                }
#endif
                /* Delete the mpls route from fast path table */
                if (MplsFsNpIpv4UcDelRoute
                    (0 /* IP_VRID */ , MPLS_IPV4_U4_ADDR (NetAddr),
                     MPLS_IPV4_U4_ADDR (NetMask), NextHop,
                     &i4TblFull) == FNP_SUCCESS)
                {
                    pFtnEntry->u1HwStatus = MPLS_FALSE;
                    /* After deleting mpls based route, restore the best route as by
                     * maintained RTM in the fast path table.
                     */
                    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));
                    RtQuery.u4DestinationIpAddress =
                        MPLS_IPV4_U4_ADDR (NetAddr);
                    RtQuery.u4DestinationSubnetMask =
                        MPLS_IPV4_U4_ADDR (NetMask);
                    RtQuery.u1QueryFlag = RTM_QUERIED_FOR_EXACT_DEST;
#if 1
                    if (RtmGetAndProgrammeBestRoute (&RtQuery) == IP_FAILURE)
                    {
                        CMNDB_DBG
                            (DEBUG_DEBUG_LEVEL,
                             "FTN Deletion failed: IP best Route addition failed \n");

                        /* Explicitly not returning MPLS_FAILURE, since it is possible
                         * that no best route is available at this point of time
                         * since no concerned port is up for this route at this point of time */
                    }
#endif
                }
                else
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               "FTN Deletion failed: MPLS Route does not exist \n");
                    /* Fast path route delete fail */
                    /* Ignoring failure as RTM may have deleted the route already */
                }
#endif
            }
        }
    }
    /* Update the Next Hop Mac and the underlying Tunnel L3 Interface for 
     * the LSP. */
    MEMCPY (MplsHwL3FTNInfo.au1DstMac, pXcEntry->pOutIndex->au1NextHopMac,
            MAC_ADDR_LEN);
    MplsHwL3FTNInfo.u4EgrL3Intf = u4EgrL3Intf;
    /* Mac address is cleared from XC for the MPLS OUT tunnel during FTN 
     * deletion, and Arp resolution will be triggered again during FTN 
     * creation */
    MEMSET (pXcEntry->pOutIndex->au1NextHopMac, MPLS_ZERO, MAC_ADDR_LEN);

    pFtnEntry->u1HwStatus = MPLS_FALSE;

    if (pFtnEntry->i4ActionType == MPLS_FTN_ON_TE_LSP)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, " MplsFTNHwDel : Reset u1IsFtnMapped \n");

        pTeTnlInfo->u1IsFtnMapped = MPLS_FALSE;
    }

    if ((pFtnEntry->u1IsLspUsedByL2VPN) ||
        (pFtnEntry->i4ActionType == MPLS_FTN_ON_TE_LSP))
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "FTN Deletion avoided for "
                   "Redirect Tunnel/NonTeLspUsedByL2VPN\n");
        return MPLS_SUCCESS;
    }

    /* Fetch the L3 interface from the MPLS tunnel interface. */
    if (MplsGetL3Intf (MplsHwL3FTNInfo.u4EgrL3Intf, &u4L3Intf) != MPLS_SUCCESS)
    {
        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                    "MplsTnlNPAdd : Fetching L3 interface failed "
                    "for tunnel interface %d\t\n", MplsHwL3FTNInfo.u4EgrL3Intf);
    }

    /* The VLAN Id will get updated with valid value only if the L3 interface 
     * is IVR interface. */
    if (CfaGetVlanId (u4L3Intf, &(MplsHwL3FTNInfo.u2OutVlanId)) != CFA_SUCCESS)
    {
        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                    "MplsTnlNPAdd : CfaGetVlanId Failed for L3 "
                    "interface %d\r\n", u4L3Intf);
    }

    /* Get the physical port for the corresponding Logical L3 interface. */
    if (MplsGetPhyPortFromLogicalIfIndex (u4L3Intf,
                                          &(MplsHwL3FTNInfo.au1DstMac[0]),
                                          &(MplsHwL3FTNInfo.u4OutPort))
        != MPLS_SUCCESS)
    {
        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                    "Fetching Physical port for the L3 interface : %d"
                    "failed.\r\n", u4L3Intf);
    }

#ifdef MBSM_WANTED
    if (pSlotInfo != NULL)
    {
#ifdef LDP_GR_WANTED
        if (pFTNHwListEntry != NULL)
        {
            /* Reset the FTN Called and Programmed Bits */
            u4NpBitMask = MPLS_FTN_HW_LIST_NP_STATUS (pFTNHwListEntry);

            MPLS_HW_LIST_RESET_NP_BIT (u4NpBitMask,
                                       (UINT4) (NPAPI_FTN_PROGRAMMED |
                                                NPAPI_FTN_CALLED));

            /* Update the NP Bits in the existing the FTN Hw List Entry */
            MplsHwListAddOrUpdateFTNHwListEntry (pFTNHwListEntry, u4NpBitMask);
        }
#endif

        if (MplsFsMplsMbsmHwDeleteL3FTN (u4EgrL3Intf, pSlotInfo) == FNP_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsFTNHwDel : FsMplsMbsmHwDeleteL3FTN failed\n");
            return MPLS_FAILURE;
        }
#ifdef LDP_GR_WANTED
        /* Delete the FTN Hw list entry */
        if (pFTNHwListEntry != NULL)
        {
            if (MplsHwListDelFTNHwListEntry (pFTNHwListEntry) == MPLS_FAILURE)
            {
                CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                            "MplsFTNHwDel: Error in Deleting the Hw List Entry:"
                            "FEC:%x\n", NetAddr.u4Addr);
            }
            else
            {
                CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                            "MplsFTNHwDel: FTN Hw List Entry Deleted "
                            "successfully for the FEC: %x\n", NetAddr.u4Addr);

            }
        }
#endif
    }
    else
#endif
    {
#ifdef L2RED_WANTED
        if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
        {
#ifdef NPAPI_WANTED
#ifdef LDP_GR_WANTED
            if (pFTNHwListEntry != NULL)
            {
                /* Reset the FTN Called and Programmed Bits */
                u4NpBitMask = MPLS_FTN_HW_LIST_NP_STATUS (pFTNHwListEntry);

                MPLS_HW_LIST_RESET_NP_BIT (u4NpBitMask,
                                           (UINT4) (NPAPI_FTN_PROGRAMMED |
                                                    NPAPI_FTN_CALLED));

                /* Update the NP Bits in the existing the FTN Hw List Entry */
                MplsHwListAddOrUpdateFTNHwListEntry (pFTNHwListEntry,
                                                     u4NpBitMask);
            }
#endif
            if (MplsFsMplsWpHwDeleteL3FTN (u4VpnId, &MplsHwL3FTNInfo) ==
                FNP_FAILURE)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "MplsFTNHwDel : FsMplsWpHwDeleteL3FTN failed\n");
                return MPLS_FAILURE;
            }
#ifdef LDP_GR_WANTED
            /* Delete the FTN Hw list entry */
            if (pFTNHwListEntry != NULL)
            {
                if (MplsHwListDelFTNHwListEntry (pFTNHwListEntry) ==
                    MPLS_FAILURE)
                {
                    CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                                "MplsFTNHwDel: Error in Deleting the Hw List Entry:"
                                "FEC:%x\n", NetAddr.u4Addr);
                }
                else
                {
                    CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                                "MplsFTNHwDel: FTN Hw List Entry Deleted "
                                "successfully for the FEC: %x\n",
                                NetAddr.u4Addr);
                }
            }
#endif
#endif
        }
    }
    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsFTNHwDel: Exit\n");
    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsILMAdd
 *  Description     : Function to add ILM entry to NP
 *  Input           : InSegment and Tunnel Information
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE
 ************************************************************************/
INT4
MplsILMAdd (tInSegment * pInSegment, tTeTnlInfo * pTeTnlInfo)
{
    return (MplsILMHwAdd (pInSegment, pTeTnlInfo, NULL));
}

/************************************************************************
 *  Function Name   : MplsILMHwAdd
 *  Description     : Function to add ILM entry to NP
 *  Input           : InSegment, Tunnel and Slot Information
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE
 ************************************************************************/
INT4
MplsILMHwAdd (tInSegment * pInSegment, tTeTnlInfo * pTeTnlInfo, VOID *pSlotInfo)
{
    tMplsHwIlmInfo      MplsHwIlmInfo;
    tXcEntry           *pXcEntry = NULL;
    tLblEntry          *pLblEntry = NULL;
    tTeTrfcParams      *pTeTrfcParams = NULL;
    UINT4               u4Action = 0;
    UINT1               u1EncapType;
    UINT4               u4InL3If = 0;
    UINT4               u4InL3Intf = 0;
    UINT4               u4OutL3If = 0;
    static tCfaIfInfo   IfInfo;
    tMacAddr            SwitchMac;
    tMacAddr            UnNumMCastMac = { 0x01, 0x00, 0x5E, 0x90, 0x00, 0x00 };
    UINT1               au1ZeroMac[MAC_ADDR_LEN];
    UINT2               u2InVlanId = 0;
    UINT2               u2OutVlanId = 0;

#ifdef LDP_GR_WANTED
    tILMHwListEntry    *pILMHwListEntry = NULL;
    tILMHwListEntry     ILMHwListEntry;
    uLabel              ILMHwListOutLabel;
    tMplsIpAddress      ILMHwListNextHop;
    UINT4               u4OutTnlIntf = 0;
#endif
#if defined(NPAPI_WANTED) || defined(MBSM_WANTED)
#ifdef QOSX_WANTED
    INT4                i4HwExpMapId = 0;
#endif
#endif

#ifdef MPLS_IPV6_WANTED
    tIp6Addr            NextHopIpv6Addr;
    UINT4               u4DstIntfIdx = 0;
#endif

#ifndef MBSM_WANTED
    UNUSED_PARAM (pSlotInfo);
#endif

    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsILMHwAdd Entry\n");
    MEMSET (&IfInfo, MPLS_ZERO, sizeof (tCfaIfInfo));
    MEMSET (au1ZeroMac, MPLS_ZERO, MAC_ADDR_LEN);
#ifdef MPLS_IPV6_WANTED
    MEMSET (&NextHopIpv6Addr, 0, sizeof (tIp6Addr));
#endif

#ifdef LDP_GR_WANTED
    MEMSET (&ILMHwListEntry, 0, sizeof (tILMHwListEntry));
    MEMSET (&ILMHwListOutLabel, 0, sizeof (uLabel));
    MEMSET (&ILMHwListNextHop, 0, sizeof (tMplsIpAddress));
#endif

    if (pInSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsILMHwAdd : InSegment not present \n");
        return MPLS_FAILURE;
    }

    /*In MBSM scenario, No need to check Hardware Programming Status */
    if ((pSlotInfo == NULL) && (INSEGMENT_HW_STATUS (pInSegment)) == TRUE)
    {
        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                    "MplsILMHwAdd : Entry is already programmed"
                    "in HW for InSegIndex %d \n", pInSegment->u4Index);
        return MPLS_SUCCESS;
    }

    pXcEntry = pInSegment->pXcIndex;

    if (pXcEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsILMHwAdd XC not present \n");
        return MPLS_FAILURE;
    }

    if (pXcEntry->pOutIndex != NULL)
    {
        pXcEntry->pInIndex->u1LblAction = MPLS_LABEL_ACTION_SWAP;
    }
    else
    {
        pXcEntry->pInIndex->u1LblAction = MPLS_LABEL_ACTION_POP;
    }

    MEMSET (&MplsHwIlmInfo, 0, sizeof (tMplsHwIlmInfo));

    if ((pInSegment->u4Label == 0) &&
        (pInSegment->mplsLabelIndex != 0) && (pInSegment->i4NPop == 2))
    {
        if (TMO_SLL_Count (&(pInSegment->mplsLabelIndex->LblList)) != 2)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsILMHwAdd double label not present \n");
            return MPLS_FAILURE;
        }
        pLblEntry = (tLblEntry *)
            TMO_SLL_First (&(pInSegment->mplsLabelIndex->LblList));
        if (pLblEntry != NULL)
        {
            /* First In Label */
            MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel = pLblEntry->u4Label;
            MplsHwIlmInfo.InLabelList[0].MplsLabelType =
                MPLS_HW_LABEL_TYPE_GENERIC;
        }
        pLblEntry = (tLblEntry *)
            TMO_SLL_Last (&(pInSegment->mplsLabelIndex->LblList));
        if (pLblEntry != NULL)
        {
            /* Second In Label */
            MplsHwIlmInfo.InLabelList[1].u.MplsShimLabel = pLblEntry->u4Label;
            MplsHwIlmInfo.InLabelList[1].MplsLabelType =
                MPLS_HW_LABEL_TYPE_GENERIC;
        }
    }
    else
    {
        /* In Label */
        MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel = pInSegment->u4Label;
        MplsHwIlmInfo.InLabelList[0].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
    }

    /* Label to be swapped */
    if (((pXcEntry->pOutIndex) != NULL) &&
        (pXcEntry->pOutIndex->u1RowStatus == ACTIVE))
    {
        MplsHwIlmInfo.LspType = MPLS_HW_LSP_ILM;
        if (pXcEntry->pOutIndex->u1NHAddrType != GMPLS_UNKNOWN)
        {
#ifdef MPLS_IPV6_WANTED
            if (pXcEntry->pOutIndex->u1NHAddrType == MPLS_IPV6_ADDR_TYPE)
            {

                if (CfaUtilGetIfIndexFromMplsTnlIf
                    (pXcEntry->pOutIndex->u4IfIndex, &u4DstIntfIdx,
                     TRUE) == CFA_FAILURE)
                {
                    return MPLS_FAILURE;
                }

                /* Get the destination MAC for a given Peer address */
                MEMCPY (&NextHopIpv6Addr, pXcEntry->pOutIndex->NHAddr.u1_addr,
                        IPV6_ADDR_LENGTH);

                if ((NetIpv6Nd6Resolve
                     (u4DstIntfIdx, &NextHopIpv6Addr,
                      MplsHwIlmInfo.au1DstMac)) == NETIPV6_FAILURE)
                {
                    if (MplsStartArpResolveTimer ((UINT1) MPLS_ILM_ARP_TIMER) ==
                        MPLS_SUCCESS)
                    {
                        XC_ARP_STATUS (pXcEntry) = MPLS_ARP_RESOLVE_WAITING;
                        CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                                    "Arp Resolve Waiting for ILM with "
                                    "InSegIndex %d and Next Hop %s - Queued in ARP\n",
                                    pInSegment->u4Index,
                                    Ip6PrintAddr (&NextHopIpv6Addr));

                        return MPLS_FAILURE;
                    }

                    XC_ARP_STATUS (pXcEntry) = MPLS_ARP_RESOLVE_FAILED;
                    CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                                " INT Exit - ARP Resolve failed for ILM with "
                                "InSegIndex %d and Next Hop %x\n",
                                pInSegment->u4Index,
                                Ip6PrintAddr (&NextHopIpv6Addr));

                    return MPLS_FAILURE;
                }

            }
            else
#endif
            {
                if ((MEMCMP (pXcEntry->pOutIndex->au1NextHopMac,
                             au1ZeroMac, MAC_ADDR_LEN) == MPLS_ZERO) &&
                    (ArpResolve (pXcEntry->pOutIndex->NHAddr.u4_addr[0],
                                 (INT1 *) MplsHwIlmInfo.au1DstMac, &u1EncapType)
                     == ARP_FAILURE))
                {
                    if (MplsTriggerArpResolve
                        (pXcEntry->pOutIndex->NHAddr.u4_addr[0],
                         (UINT1) MPLS_ILM_ARP_TIMER) == MPLS_SUCCESS)
                    {
                        XC_ARP_STATUS (pXcEntry) = MPLS_ARP_RESOLVE_WAITING;
                        CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                                    "Arp Resolve Waiting for ILM with "
                                    "InSegIndex %d and Next Hop %x - Queued in ARP\n",
                                    pInSegment->u4Index,
                                    pXcEntry->pOutIndex->NHAddr.u4_addr[0]);
                        return MPLS_FAILURE;
                    }

                    XC_ARP_STATUS (pXcEntry) = MPLS_ARP_RESOLVE_FAILED;
                    CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                                " INT Exit - ARP Resolve failed for ILM with "
                                "InSegIndex %d and Next Hop %x\n",
                                pInSegment->u4Index,
                                pXcEntry->pOutIndex->NHAddr.u4_addr[0]);
                    return MPLS_FAILURE;
                }
            }
        }
        else
        {
            MplsGetIfUnnumPeerMac (pXcEntry->pOutIndex->u4IfIndex,
                                   (UINT1 *) MplsHwIlmInfo.au1DstMac);
        }
        if ((pTeTnlInfo != NULL) &&
            (pTeTnlInfo->u1TnlLocalProtectInUse == LOCAL_PROT_IN_USE) &&
            (MEMCMP (pXcEntry->pOutIndex->au1NextHopMac,
                     au1ZeroMac, MAC_ADDR_LEN) != MPLS_ZERO))
        {
            MEMCPY (MplsHwIlmInfo.au1DstMac,
                    pXcEntry->pOutIndex->au1NextHopMac, MAC_ADDR_LEN);
        }
        else
        {

            MEMCPY (pXcEntry->pOutIndex->au1NextHopMac,
                    MplsHwIlmInfo.au1DstMac, MAC_ADDR_LEN);
        }

        CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                    "Arp Resolve Succeded for ILM with InSegIndex %d and "
                    "Next Hop %x\n", pInSegment->u4Index,
                    pXcEntry->pOutIndex->NHAddr.u4_addr[0]);
        if (pXcEntry->mplsLabelIndex != NULL)
        {
            pLblEntry = (tLblEntry *)
                TMO_SLL_First (&(pXcEntry->mplsLabelIndex->LblList));
            if (pLblEntry != NULL)
            {
                /* First Label */
                MplsHwIlmInfo.MplsSwapLabel.u.MplsShimLabel =
                    pLblEntry->u4Label;
            }
            MplsHwIlmInfo.MplsPushLblList[0].u.MplsShimLabel =
                pXcEntry->pOutIndex->u4Label;
            /* If Label Stack is present the OutSegment should have 
             * Label */
            if (pXcEntry->pOutIndex->u4Label == 3)
            {
                MplsHwIlmInfo.LabelAction = MPLS_HW_ACTION_PHP;
            }
            else
            {
                if (pXcEntry->pOutIndex->u4Label == 0)
                {
                    MplsHwIlmInfo.LabelAction = MPLS_HW_ACTION_PHP;
                }
            }
            MplsHwIlmInfo.u4OutL3Intf = pXcEntry->pOutIndex->u4IfIndex;
        }
        else if (pXcEntry->pOutIndex->u4Label != 0)
        {
            /* Label 3 (PHP implicit case) */
            if (pXcEntry->pOutIndex->u4Label == 3)
            {
                MplsHwIlmInfo.LabelAction = MPLS_HW_ACTION_PHP;
            }
            MplsHwIlmInfo.MplsSwapLabel.u.MplsShimLabel =
                pXcEntry->pOutIndex->u4Label;
            if (MplsGetL3Intf
                (pXcEntry->pOutIndex->u4IfIndex,
                 &MplsHwIlmInfo.u4OutL3Intf) != MPLS_SUCCESS)
            {
                CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                            "MplsILMAdd : Fetching L3 interface failed "
                            "for tunnel interface %d\t\n",
                            pXcEntry->pOutIndex->u4IfIndex);
            }

            /* MplsHwIlmInfo.u4OutL3Intf = pXcEntry->pOutIndex->u4IfIndex; */
        }
        else if (pXcEntry->pOutIndex->u4Label == 0)
        {
            MplsHwIlmInfo.LabelAction = MPLS_HW_ACTION_PHP;
            MplsHwIlmInfo.u4OutL3Intf = pXcEntry->pOutIndex->u4IfIndex;
        }
    }
    else
    {
        /* Tunnel termination, so say what model the tunnel to operate on */

        MplsHwIlmInfo.LspType = MPLS_HW_LSP_TUN;
    }

    XC_ARP_STATUS (pXcEntry) = MPLS_ARP_RESOLVED;
    MplsHwIlmInfo.u4InL3Intf = pInSegment->u4IfIndex;
    /* Fetch the L3 interface from the MPLS tunnel interface. */
    if (MplsGetL3Intf (MplsHwIlmInfo.u4InL3Intf, &u4InL3Intf) != MPLS_SUCCESS)
    {
        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                    "FTNHwAdd : Fetching L3 interface failed "
                    "for tunnel interface %d\t\n", MplsHwIlmInfo.u4InL3Intf);
    }

    /* Get the Incoming Port from the corresponding L3 Interface */
    if (u4InL3Intf != 0)
    {
        /* Get the physical port for the corresponding Logical L3 interface. */
        if (MplsGetPhyPortFromLogicalIfIndex
            (u4InL3Intf, &(MplsHwIlmInfo.au1DstMac[0]),
             &(MplsHwIlmInfo.u4SrcPort)) != MPLS_SUCCESS)
        {
            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                        "Fetching Physical port for the L3 interface : %d"
                        "failed.\r\n", u4OutL3If);
        }
    }

    if (pTeTnlInfo != NULL)
    {
        /* Fill the diffserv related info from the tunnel */
        if (TE_DS_TNL_INFO (pTeTnlInfo) != NULL)
        {
            MplsFillQosInfo (pTeTnlInfo, &MplsHwIlmInfo.QosInfo[0]);
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       " Tunnel DIFFSRV info copied to ILM ds\n");
        }

        if (TeCheckTrfcParamInTrfcParamTable
            (TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo), &pTeTrfcParams)
            == TE_SUCCESS)
        {
            if (pTeTrfcParams->pRSVPTrfcParams != NULL)
            {
                MplsHwIlmInfo.MplsHwTeParams.u4MaxRate
                    = pTeTrfcParams->pRSVPTrfcParams->u4PeakDataRate;
            }
            else if (pTeTrfcParams->pCRLDPTrfcParams != NULL)
            {
                MplsHwIlmInfo.MplsHwTeParams.u4MaxRate
                    = pTeTrfcParams->pCRLDPTrfcParams->u4PeakDataRate;
            }
            else
            {
                MplsHwIlmInfo.MplsHwTeParams.u4MaxRate
                    = pTeTrfcParams->u4ResMaxRate;
            }
        }
    }

    /* For LDP the Incomming L3 If is MPLS Tnl IF.For RSVP,it is IVR If */
    if (CfaGetIfInfo (MplsHwIlmInfo.u4InL3Intf, &IfInfo) == CFA_SUCCESS)
    {
        if (IfInfo.u1IfType == CFA_MPLS_TUNNEL)
        {
            /* Source Interface */
            if (CfaUtilGetIfIndexFromMplsTnlIf
                ((UINT2) MplsHwIlmInfo.u4InL3Intf, &u4InL3If,
                 TRUE) == CFA_SUCCESS)
            {
                if (CfaGetVlanId (u4InL3If, &u2InVlanId) != CFA_SUCCESS)
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               "MplsILMAdd : CfaGetVlanId Failed\r\n");
                    return MPLS_FAILURE;
                }
            }
            MplsHwIlmInfo.u4InVlanId = (UINT4) u2InVlanId;
            /* Get the IVR L3 Interface for the Tnl IF */
            MplsHwIlmInfo.u4InL3Intf = u4InL3If;
            if (CfaGetIfInfo (MplsHwIlmInfo.u4InL3Intf, &IfInfo) == CFA_FAILURE)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "MplsILMAdd : CfaGetIfInfo Failed\r\n");
            }
        }
        else
        {
            /* For RSVP Incomming If is IVR If.So get directly VlanId from it */
            if (CfaGetVlanId ((UINT2) MplsHwIlmInfo.u4InL3Intf, &u2InVlanId)
                != CFA_SUCCESS)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "MplsILMAdd : CfaGetVlanId Failed\r\n");
                return MPLS_FAILURE;
            }
            MplsHwIlmInfo.u4InVlanId = (UINT4) u2InVlanId;
        }
    }
    else
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsILMAdd : CfaGetIfType Failed\r\n");
        return MPLS_FAILURE;
    }
    if (MEMCMP (IfInfo.au1PeerMacAddr, UnNumMCastMac, CFA_ENET_ADDR_LEN) == 0)
    {
        /* Multicast MAC Addresses MPLS-TP p2p - 0x01, 0x00, 0x5E, 0x90, 0x00, 0x00 */
        MEMCPY (MplsHwIlmInfo.au1InMacAddr, IfInfo.au1PeerMacAddr,
                CFA_ENET_ADDR_LEN);
    }
    else
    {
        CfaGetSysMacAddress (SwitchMac);
        MEMCPY (MplsHwIlmInfo.au1InMacAddr, SwitchMac, CFA_ENET_ADDR_LEN);
    }

    /* Fetch the L3 interface from the MPLS tunnel interface. */
    if (MplsGetL3Intf (MplsHwIlmInfo.u4OutL3Intf, &u4OutL3If) != MPLS_SUCCESS)
    {
        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                    "MplsILMAdd : Fetching L3 interface failed "
                    "for tunnel interface %d\t\n", MplsHwIlmInfo.u4OutL3Intf);
    }

    if (CfaGetVlanId (u4OutL3If, &u2OutVlanId) != CFA_SUCCESS)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsILMAdd : CfaGetVlanId Failed\r\n");
    }
    /* STATIC_HLSP */
    if (CfaGetIfInfo (u4OutL3If, &IfInfo) == CFA_SUCCESS)
    {
        if (IfInfo.u1IfType == CFA_MPLS_TUNNEL)
        {
            MplsHwIlmInfo.u4OutL3Intf = u4OutL3If;
        }
    }
    MplsHwIlmInfo.u4NewVlanId = (UINT4) u2OutVlanId;

    /* The Outgoing Interface will not be applicable for egress node 
     * and hence no need to get the physical interface. */
    if (u4OutL3If != 0)
    {
        /* Get the physical port for the corresponding Logical L3 interface. */
        if (MplsGetPhyPortFromLogicalIfIndex
            (u4OutL3If, &(MplsHwIlmInfo.au1DstMac[0]),
             &(MplsHwIlmInfo.u4OutPort)) != MPLS_SUCCESS)
        {
            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                        "Fetching Physical port for the L3 interface : %d"
                        "failed.\r\n", u4OutL3If);
        }
    }

    /* Fill the tunnel model and the ttl value for the LSP */

    switch (MPLS_DIFFSERV_TNL_MODEL (MPLS_DEF_INCARN))
    {
        case UNIFORM_MODEL:
            MplsHwIlmInfo.DsLspModel = MPLS_HW_DS_LSP_MODEL_UNIFORM;
            break;
        case PIPE_MODEL:
            MplsHwIlmInfo.DsLspModel = MPLS_HW_DS_LSP_MODEL_PIPE;
            break;
        case SHORT_PIPE_MODEL:
            MplsHwIlmInfo.DsLspModel = MPLS_HW_DS_LSP_MODEL_SHORTPIPE;
            break;
        default:
            CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Invalid Tunnel DIFFSRV model \n");
            return MPLS_FAILURE;
    }

    MplsHwIlmInfo.u4TTLVal = MPLS_TTL_VALUE (MPLS_DEF_INCARN);

    CMNDB_DBG4 (DEBUG_DEBUG_LEVEL, "ILM Add in HW started with In Vlan Id : %d"
                "In L3 Intf : %d In Label1 %d In Label2 %d\n",
                MplsHwIlmInfo.u4InVlanId, MplsHwIlmInfo.u4InL3Intf,
                MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel,
                MplsHwIlmInfo.InLabelList[1].u.MplsShimLabel);
    CMNDB_DBG4 (DEBUG_DEBUG_LEVEL,
                "Out Vlan Id : %d Out L3 Intf : %d Out Label1 : %d "
                "Out Label 2 : %d\n", MplsHwIlmInfo.u4NewVlanId,
                MplsHwIlmInfo.u4OutL3Intf,
                MplsHwIlmInfo.MplsSwapLabel.u.MplsShimLabel,
                MplsHwIlmInfo.MplsPushLblList[0].u.MplsShimLabel);

    CMNDB_DBG2 (DEBUG_DEBUG_LEVEL, "Tunnel Model : %d TTL Value : %d \n",
                MplsHwIlmInfo.DsLspModel, MplsHwIlmInfo.u4TTLVal);

    /* Check and fill the HSLP Tunnel Interface index if this tunnel is stacked over HLSP
     * Otherwise fill the self Tunnel Interface index */
    if (pTeTnlInfo != NULL)

    {
        MplsHwIlmInfo.MplsHwLspId = pTeTnlInfo->u4TnlIfIndex;
        if (TE_MAP_TNL_INFO (pTeTnlInfo) != NULL)
        {
            MplsHwIlmInfo.bIsStackedTunnel = TRUE;
            MplsHwIlmInfo.MplsHwLspId =
                ((TE_MAP_TNL_INFO (pTeTnlInfo))->u4TnlIfIndex);
        }
    }

#ifdef LDP_GR_WANTED
    /* The ILM Hw List entries currently contains only entries for NON-TE LDP LSP's */
    if (pTeTnlInfo == NULL)
    {
        /* Get and Validate ILM Hw List Entry only for the Dynamic LSP's */
        if (pInSegment->u1Owner == MPLS_OWNER_LDP)
        {
            /* Fetch the Entry from the Hw list */
            pILMHwListEntry =
                MplsGetILMHwListEntryFromFecAndInIntf (pInSegment->ILMHwListFec,
                                                       pInSegment->
                                                       u1ILMHwListPrefLen,
                                                       pInSegment->u4IfIndex);
            if ((pILMHwListEntry != NULL)
                && (MPLS_ILM_HW_LIST_STALE_STATUS (pILMHwListEntry) ==
                    MPLS_TRUE))
            {
#ifdef MPLS_IPV6_WANTED
                if (MPLS_ILM_HW_LIST_FEC_ADDR_TYPE (pILMHwListEntry) ==
                    MPLS_IPV6_ADDR_TYPE)
                {
                    CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                                "\nILM Hw List Entry found for FEC: %s,"
                                "and Interface index: %d\n",
                                Ip6PrintAddr (&MPLS_IPV6_ILM_HW_LIST_FEC
                                              (pILMHwListEntry)),
                                MPLS_ILM_HW_LIST_IN_L3_INTF (pILMHwListEntry));
                }
                else
#endif
                {
                    CMNDB_DBG5 (DEBUG_DEBUG_LEVEL,
                                "\nILM Hw List Entry found for FEC: %d.%d.%d.%d,"
                                "and Interface index: %d\n",
                                MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[0],
                                MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[1],
                                MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[2],
                                MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[3],
                                MPLS_ILM_HW_LIST_IN_L3_INTF (pILMHwListEntry));
                }

                /* If the entry is in swap mode, Fill the Out paramaters for validation */
                if (pXcEntry->pOutIndex != NULL)
                {

                    ILMHwListNextHop.i4IpAddrType =
                        pXcEntry->pOutIndex->u1NHAddrType;
#ifdef MPLS_IPV6_WANTED
                    if (pXcEntry->pOutIndex->u1NHAddrType ==
                        MPLS_IPV6_ADDR_TYPE)
                    {

                        MEMCPY (&ILMHwListNextHop.IpAddress.Ip6Addr,
                                pXcEntry->pOutIndex->NHAddr.u1_addr,
                                IPV6_ADDR_LENGTH);
                    }
                    else
#endif
                    {
                        MEMCPY (ILMHwListNextHop.IpAddress.au1Ipv4Addr,
                                &pXcEntry->pOutIndex->NHAddr.u4_addr[0],
                                IPV4_ADDR_LENGTH);
                    }

                    MEMCPY (&ILMHwListOutLabel.u4GenLbl,
                            &(pXcEntry->pOutIndex->u4Label), sizeof (uLabel));

                    if (ILMHwListOutLabel.u4GenLbl != MPLS_IMPLICIT_NULL_LABEL)
                    {
                        u4OutTnlIntf = pXcEntry->pOutIndex->u4IfIndex;
                    }
                }
                if ((MplsHwListValidateILMHwListEntry
                     (pILMHwListEntry, ILMHwListOutLabel, ILMHwListNextHop,
                      u4OutTnlIntf) == MPLS_TRUE)
                    && (MPLS_ILM_HW_LIST_STALE_STATUS (pILMHwListEntry) ==
                        MPLS_TRUE)
                    && (MPLS_ILM_HW_LIST_IN_LABEL (pILMHwListEntry) ==
                        pInSegment->u4Label))
                {
#ifdef MPLS_IPV6_WANTED
                    if (MPLS_ILM_HW_LIST_FEC_ADDR_TYPE (pILMHwListEntry) ==
                        MPLS_IPV6_ADDR_TYPE)
                    {
                        CMNDB_DBG3 (DEBUG_DEBUG_LEVEL,
                                    "MplsILMHwAdd: ILM Hw List Entry Validated "
                                    "for FEC: %s/%d, and Interface index: %d,"
                                    "Hence Marking the ILM Hw List Entry as UNSTALE\n",
                                    Ip6PrintAddr (&MPLS_IPV6_ILM_HW_LIST_FEC
                                                  (pILMHwListEntry)),
                                    MPLS_ILM_HW_LIST_FEC_PREFIX_LEN
                                    (pILMHwListEntry),
                                    MPLS_ILM_HW_LIST_IN_L3_INTF
                                    (pILMHwListEntry));
                    }
                    else
#endif
                    {
                        CMNDB_DBG6 (DEBUG_DEBUG_LEVEL,
                                    "MplsILMHwAdd: ILM Hw List Entry Validated "
                                    "for FEC: %d.%d.%d.%d/%d, and Interface index: %d,"
                                    "Hence Marking the ILM Hw List Entry as UNSTALE\n",
                                    MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[0],
                                    MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[1],
                                    MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[2],
                                    MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[3],
                                    MPLS_ILM_HW_LIST_FEC_PREFIX_LEN
                                    (pILMHwListEntry),
                                    MPLS_ILM_HW_LIST_IN_L3_INTF
                                    (pILMHwListEntry));

                    }

                    /* Mark the ILM Hw list Entry as unstale and return */
                    MPLS_ILM_HW_LIST_STALE_STATUS (pILMHwListEntry) =
                        MPLS_FALSE;
                    INSEGMENT_HW_STATUS (pInSegment) = TRUE;
                    return MPLS_SUCCESS;
                }
                else
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               "MplsILMHwAdd: ILM Hw List Entry Not Valid, Hence Deleting "
                               "it from the Hw\n");
                    if (MplsHwListDeleteILMEntryFromHw (pILMHwListEntry, NULL)
                        == MPLS_FAILURE)
                    {
#ifdef MPLS_IPV6_WANTED
                        if (MPLS_ILM_HW_LIST_FEC_ADDR_TYPE (pILMHwListEntry) ==
                            MPLS_IPV6_ADDR_TYPE)
                        {
                            CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                                        "MplsILMHwAdd: Error in Deleting the InValid ILM Hw List "
                                        "Entry for FEC: %s, and Interface index: %d\n",
                                        Ip6PrintAddr (&MPLS_IPV6_ILM_HW_LIST_FEC
                                                      (pILMHwListEntry)),
                                        MPLS_ILM_HW_LIST_IN_L3_INTF
                                        (pILMHwListEntry));
                        }
                        else
#endif
                        {
                            CMNDB_DBG6 (DEBUG_DEBUG_LEVEL,
                                        "MplsILMHwAdd: Error in Deleting the InValid ILM Hw List "
                                        "Entry for FEC: %d.%d.%d.%d/%d, and Interface index: %d\n",
                                        MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)
                                        [0],
                                        MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)
                                        [1],
                                        MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)
                                        [2],
                                        MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)
                                        [3],
                                        MPLS_ILM_HW_LIST_FEC_PREFIX_LEN
                                        (pILMHwListEntry),
                                        MPLS_ILM_HW_LIST_IN_L3_INTF
                                        (pILMHwListEntry));

                        }
                        return MPLS_FAILURE;

                    }

                }
            }
        }
    }
#endif
#ifdef MBSM_WANTED
    if (pSlotInfo != NULL)
    {
        u4Action = MPLS_HW_ACTION_L3_SWITCH;

        /* STATIC_HLSP */
        if (((pTeTnlInfo) != NULL)
            && (TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_HLSP))
        {
            if (((pTeTnlInfo->u4TnlMode ==
                  TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
                 && (TE_TNL_ROLE (pTeTnlInfo) != TE_INTERMEDIATE))
                || ((pTeTnlInfo->u4TnlMode == TE_TNL_MODE_UNIDIRECTIONAL)
                    && (TE_TNL_ROLE (pTeTnlInfo) == TE_EGRESS)))
            {
                MplsHwIlmInfo.LabelAction = MPLS_HW_ACTION_POP_SEARCH;
                u4Action = MPLS_HW_ACTION_POP_SEARCH;
            }
        }
#ifdef LDP_GR_WANTED
        if (pTeTnlInfo == NULL)
        {
            /* Add the entry to the Hw List only for NON-TE LSPs */
            if (MplsHwListInitILMHwListEntry (&ILMHwListEntry,
                                              pInSegment,
                                              pXcEntry) == MPLS_FAILURE)
            {
#ifdef MPLS_IPV6_WANTED
                if (MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry)) ==
                    MPLS_IPV6_ADDR_TYPE)
                {
                    CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                                "MplsILMHwAdd: ILM Hw List Entry Init Failed "
                                "for FEC: %s, and Interface index: %d\n",
                                Ip6PrintAddr (&MPLS_IPV6_ILM_HW_LIST_FEC
                                              ((&ILMHwListEntry))),
                                MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)));
                }
                else
#endif
                {
                    CMNDB_DBG5 (DEBUG_DEBUG_LEVEL,
                                "MplsILMHwAdd: ILM Hw List Entry Init Failed "
                                "for FEC: %d.%d.%d.%d, and Interface index: %d\n",
                                MPLS_ILM_HW_LIST_FEC ((&ILMHwListEntry))[0],
                                MPLS_ILM_HW_LIST_FEC ((&ILMHwListEntry))[1],
                                MPLS_ILM_HW_LIST_FEC ((&ILMHwListEntry))[2],
                                MPLS_ILM_HW_LIST_FEC ((&ILMHwListEntry))[3],
                                MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)));

                }

                return MPLS_FAILURE;
            }

            /* Add the entry to the ILM Hw List */
            if (MplsHwListAddOrUpdateILMHwListEntry (&ILMHwListEntry, 0) ==
                MPLS_FAILURE)
            {
#ifdef MPLS_IPV6_WANTED
                if (MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry)) ==
                    MPLS_IPV6_ADDR_TYPE)
                {

                    CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                                "MplsILMHwAdd: ILM Hw List Addition Failed "
                                "for FEC: %s, and Interface index: %d\n",
                                Ip6PrintAddr (&MPLS_IPV6_ILM_HW_LIST_FEC
                                              ((&ILMHwListEntry))),
                                MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)));
                }
                else
#endif
                {
                    CMNDB_DBG5 (DEBUG_DEBUG_LEVEL,
                                "MplsILMHwAdd: ILM Hw List Addition Failed "
                                "for FEC: %d.%d.%d.%d, and Interface index: %d\n",
                                MPLS_ILM_HW_LIST_FEC ((&ILMHwListEntry))[0],
                                MPLS_ILM_HW_LIST_FEC ((&ILMHwListEntry))[1],
                                MPLS_ILM_HW_LIST_FEC ((&ILMHwListEntry))[2],
                                MPLS_ILM_HW_LIST_FEC ((&ILMHwListEntry))[3],
                                MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)));
                }

                return MPLS_FAILURE;
            }
#ifdef MPLS_IPV6_WANTED
            if (MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry)) ==
                MPLS_IPV6_ADDR_TYPE)
            {
                CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                            "MplsILMHwAdd: ILM Hw List Entry added Successfully "
                            "for FEC: %s, and Interface index: %d\n",
                            Ip6PrintAddr (&MPLS_IPV6_ILM_HW_LIST_FEC
                                          ((&ILMHwListEntry))),
                            MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)));
            }
            else
#endif
            {

                CMNDB_DBG5 (DEBUG_DEBUG_LEVEL,
                            "MplsILMHwAdd: ILM Hw List Entry added Successfully "
                            "for FEC: %d.%d.%d.%d, and Interface index: %d\n",
                            MPLS_ILM_HW_LIST_FEC ((&ILMHwListEntry))[0],
                            MPLS_ILM_HW_LIST_FEC ((&ILMHwListEntry))[1],
                            MPLS_ILM_HW_LIST_FEC ((&ILMHwListEntry))[2],
                            MPLS_ILM_HW_LIST_FEC ((&ILMHwListEntry))[3],
                            MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)));

            }
        }
#endif
        /* Fetch the Qos Information for teh incoming port */
#ifdef QOSX_WANTED
        QosGetMplsExpProfileForPortAtIngress (MplsHwIlmInfo.u4SrcPort,
                                              &i4HwExpMapId);
        MplsHwIlmInfo.QosInfo[0].u.MplsQosPolicy.i4HwExpMapId = i4HwExpMapId;
        QosGetMplsExpProfileForPortAtEgress (MplsHwIlmInfo.u4OutPort,
                                             &i4HwExpMapId);
        MplsHwIlmInfo.QosInfo[0].u.MplsQosPolicy.i4HwPriMapId = i4HwExpMapId;
#endif
        if (MplsFsMplsMbsmHwCreateILM (&MplsHwIlmInfo, u4Action, pSlotInfo) ==
            FNP_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsILMAdd : FsMplsMbsmHwCreateILM failed \t\n");
            return MPLS_FAILURE;
        }
#ifdef LDP_GR_WANTED
        if (pTeTnlInfo == NULL)
        {
            /* Update the NP bit in the ILM hw List entry */
            if (MplsHwListAddOrUpdateILMHwListEntry (&ILMHwListEntry,
                                                     (NPAPI_ILM_CALLED) |
                                                     (NPAPI_ILM_PROGRAMMED)) ==
                MPLS_FAILURE)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "MplsILMHwAdd: Failed to update ILM Hw List Entry\n");
                return MPLS_FAILURE;
            }
#ifdef MPLS_IPV6_WANTED
            if (MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry)) ==
                MPLS_IPV6_ADDR_TYPE)
            {

                CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                            "MplsILMHwAdd: ILM Hw List Entry NP bit updated to ILM Programmed "
                            "for FEC: %s, and Interface index: %d\n",
                            Ip6PrintAddr (&MPLS_IPV6_ILM_HW_LIST_FEC
                                          ((&ILMHwListEntry))),
                            MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)));
            }
            else
#endif
            {

                CMNDB_DBG5 (DEBUG_DEBUG_LEVEL,
                            "MplsILMHwAdd: ILM Hw List Entry NP bit updated to ILM Programmed "
                            "for FEC: %d.%d.%d.%d, and Interface index: %d\n",
                            MPLS_ILM_HW_LIST_FEC ((&ILMHwListEntry))[0],
                            MPLS_ILM_HW_LIST_FEC ((&ILMHwListEntry))[1],
                            MPLS_ILM_HW_LIST_FEC ((&ILMHwListEntry))[2],
                            MPLS_ILM_HW_LIST_FEC ((&ILMHwListEntry))[3],
                            MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)));

            }
        }

#endif
    }
    else
#endif
    {
        /* Configure ILM in H/W */
#ifdef L2RED_WANTED
        if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
        {
            u4Action = MPLS_HW_ACTION_L3_SWITCH;

            /* STATIC_HLSP */
            if ((pTeTnlInfo != NULL)
                && (TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_HLSP))
            {
                if (((pTeTnlInfo->u4TnlMode ==
                      TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
                     && (TE_TNL_ROLE (pTeTnlInfo) != TE_INTERMEDIATE))
                    ||
                    ((pTeTnlInfo->u4TnlMode == TE_TNL_MODE_UNIDIRECTIONAL)
                     && (TE_TNL_ROLE (pTeTnlInfo) == TE_EGRESS)))
                {
                    MplsHwIlmInfo.LabelAction = MPLS_HW_ACTION_POP_SEARCH;
                    u4Action = MPLS_HW_ACTION_POP_SEARCH;
                }
            }

#ifdef NPAPI_WANTED
#ifdef LDP_GR_WANTED
            if (pTeTnlInfo == NULL)
            {
                /* Add the Entry in the ILM Hw List. The entry is added for both static and dynamic NON-TE LSPs */

                if (MplsHwListInitILMHwListEntry (&ILMHwListEntry,
                                                  pInSegment,
                                                  pXcEntry) == MPLS_FAILURE)
                {
#ifdef MPLS_IPV6_WANTED
                    if (MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry)) ==
                        MPLS_IPV6_ADDR_TYPE)
                    {

                        CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                                    "MplsILMHwAdd: ILM Hw List Entry Init Failed "
                                    "for FEC: %s, and Interface index: %d\n",
                                    Ip6PrintAddr (&MPLS_IPV6_ILM_HW_LIST_FEC
                                                  ((&ILMHwListEntry))),
                                    MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)));
                    }
                    else
#endif
                    {
                        CMNDB_DBG5 (DEBUG_DEBUG_LEVEL,
                                    "MplsILMHwAdd: ILM Hw List Entry Init Failed "
                                    "for FEC: %d.%d.%d.%d, and Interface index: %d\n",
                                    MPLS_ILM_HW_LIST_FEC ((&ILMHwListEntry))[0],
                                    MPLS_ILM_HW_LIST_FEC ((&ILMHwListEntry))[1],
                                    MPLS_ILM_HW_LIST_FEC ((&ILMHwListEntry))[2],
                                    MPLS_ILM_HW_LIST_FEC ((&ILMHwListEntry))[3],
                                    MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)));

                    }

                    return MPLS_FAILURE;
                }

                /* Add the entry to the ILM Hw List */
                if (MplsHwListAddOrUpdateILMHwListEntry (&ILMHwListEntry, 0) ==
                    MPLS_FAILURE)
                {
#ifdef MPLS_IPV6_WANTED
                    if (MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry)) ==
                        MPLS_IPV6_ADDR_TYPE)
                    {

                        CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                                    "MplsILMHwAdd: ILM Hw List Addition Failed "
                                    "for FEC: %s, and Interface index: %d\n",
                                    Ip6PrintAddr (&MPLS_IPV6_ILM_HW_LIST_FEC
                                                  ((&ILMHwListEntry))),
                                    MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)));
                    }
                    else
#endif
                    {
                        CMNDB_DBG5 (DEBUG_DEBUG_LEVEL,
                                    "MplsILMHwAdd: ILM Hw List Addition Failed "
                                    "for FEC: %d.%d.%d.%d, and Interface index: %d\n",
                                    MPLS_ILM_HW_LIST_FEC ((&ILMHwListEntry))[0],
                                    MPLS_ILM_HW_LIST_FEC ((&ILMHwListEntry))[1],
                                    MPLS_ILM_HW_LIST_FEC ((&ILMHwListEntry))[2],
                                    MPLS_ILM_HW_LIST_FEC ((&ILMHwListEntry))[3],
                                    MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)));

                    }

                    return MPLS_FAILURE;
                }

#ifdef MPLS_IPV6_WANTED
                if (MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry)) ==
                    MPLS_IPV6_ADDR_TYPE)
                {

                    CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                                "MplsILMHwAdd: ILM Hw List Entry added Successfully "
                                "for FEC: %s, and Interface index: %d\n",
                                Ip6PrintAddr (&MPLS_IPV6_ILM_HW_LIST_FEC
                                              ((&ILMHwListEntry))),
                                MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)));
                }
                else
#endif
                {
                    CMNDB_DBG5 (DEBUG_DEBUG_LEVEL,
                                "MplsILMHwAdd: ILM Hw List Entry added Successfully "
                                "for FEC: %d.%d.%d.%d, and Interface index: %d\n",
                                MPLS_ILM_HW_LIST_FEC ((&ILMHwListEntry))[0],
                                MPLS_ILM_HW_LIST_FEC ((&ILMHwListEntry))[1],
                                MPLS_ILM_HW_LIST_FEC ((&ILMHwListEntry))[2],
                                MPLS_ILM_HW_LIST_FEC ((&ILMHwListEntry))[3],
                                MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)));
                }
            }
#endif
            /* Fetch the Qos Information for teh incoming port */
#ifdef QOSX_WANTED
            QosGetMplsExpProfileForPortAtIngress (MplsHwIlmInfo.u4SrcPort,
                                                  &i4HwExpMapId);
            MplsHwIlmInfo.QosInfo[0].u.MplsQosPolicy.i4HwExpMapId =
                i4HwExpMapId;
            QosGetMplsExpProfileForPortAtEgress (MplsHwIlmInfo.u4OutPort,
                                                 &i4HwExpMapId);
            MplsHwIlmInfo.QosInfo[0].u.MplsQosPolicy.i4HwPriMapId =
                i4HwExpMapId;
#endif
            if (MplsFsMplsHwCreateILM (&MplsHwIlmInfo, u4Action) == FNP_FAILURE)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "MplsILMAdd : FsMplsHwCreateILM failed \t\n");
                return MPLS_FAILURE;
            }
#ifdef LDP_GR_WANTED
            if (pTeTnlInfo == NULL)
            {
                /* Update the NP bit in the ILM hw List entry */
                if (MplsHwListAddOrUpdateILMHwListEntry (&ILMHwListEntry,
                                                         (NPAPI_ILM_CALLED) |
                                                         (NPAPI_ILM_PROGRAMMED))
                    == MPLS_FAILURE)
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               "MplsILMHwAdd: Failed to update the ILM Hw List Entry\n");
                    return MPLS_FAILURE;
                }
#ifdef MPLS_IPV6_WANTED
                if (MPLS_ILM_HW_LIST_FEC_ADDR_TYPE ((&ILMHwListEntry)) ==
                    MPLS_IPV6_ADDR_TYPE)
                {

                    CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                                "MplsILMHwAdd: ILM Hw List Entry NP bit updated to ILM Programmed "
                                "for FEC: %s, and Interface index: %d\n",
                                Ip6PrintAddr (&MPLS_IPV6_ILM_HW_LIST_FEC
                                              ((&ILMHwListEntry))),
                                MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)));
                }
                else
#endif
                {
                    CMNDB_DBG5 (DEBUG_DEBUG_LEVEL,
                                "MplsILMHwAdd: ILM Hw List Entry NP bit updated to ILM Programmed "
                                "for FEC: %d.%d.%d.%d, and Interface index: %d\n",
                                MPLS_ILM_HW_LIST_FEC ((&ILMHwListEntry))[0],
                                MPLS_ILM_HW_LIST_FEC ((&ILMHwListEntry))[1],
                                MPLS_ILM_HW_LIST_FEC ((&ILMHwListEntry))[2],
                                MPLS_ILM_HW_LIST_FEC ((&ILMHwListEntry))[3],
                                MPLS_ILM_HW_LIST_IN_L3_INTF ((&ILMHwListEntry)));
                }
            }
#endif

#endif

        }
    }
    CMNDB_DBG1 (DEBUG_DEBUG_LEVEL, " ILM add in HW actios is %d\n", u4Action);
    INSEGMENT_HW_STATUS (pInSegment) = TRUE;
    CMNDB_DBG1 (DEBUG_DEBUG_LEVEL, "ILMAdd in HW for InSegIndex %d succeeded\n",
                pInSegment->u4Index);
    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsILMHwAdd - Exit \n");

    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsILMDel
 *  Description     : Function to delete ILM entry from NP
 *  Input           : LspInfo 
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE
 ************************************************************************/
INT4
MplsILMDel (tInSegment * pInSegment)
{
    return (MplsILMHwDel (pInSegment, NULL));
}

/************************************************************************
 *  Function Name   : MplsILMHwDel
 *  Description     : Function to delete ILM entry from NP
 *  Input           : InSegment and Slot Information 
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE
 ************************************************************************/
INT4
MplsILMHwDel (tInSegment * pInSegment, VOID *pSlotInfo)
{
    tMplsHwIlmInfo      MplsHwDelIlmParams;
    tXcEntry           *pXcEntry = NULL;
    tLblEntry          *pLblEntry = NULL;
    tCfaIfInfo          IfInfo;
#ifdef NPAPI_WANTED
    UINT4               u4Action = 0;
#endif
    UINT4               u4InL3If;
    UINT2               u2InVlanId = 0;
#ifdef LDP_GR_WANTED
    tILMHwListEntry    *pILMHwListEntry = NULL;
#endif

#ifndef MBSM_WANTED
    UNUSED_PARAM (pSlotInfo);
#endif
    MEMSET (&MplsHwDelIlmParams, 0, sizeof (tMplsHwIlmInfo));

    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsILMHwDel Entry\n");
    pXcEntry = pInSegment->pXcIndex;

    if (pXcEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsILMHwDel : XC entry not present\t\n");
        return MPLS_FAILURE;
    }

    if ((INSEGMENT_HW_STATUS (pInSegment)) == FALSE)
    {
        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL, "MplsILMHwDel : Entry is already deleted"
                    "in HW for InSegIndex %d \n", pInSegment->u4Index);
        return MPLS_SUCCESS;
    }

    /* Tunnel should not be deleted from NP for the following scenarios
     *   1. It is a protected tunnel
     *   2. BFD is monitoring the tunnel. 
     *   3. Protected tunnel in case application is ELPS. For FRR,
     *   it should be skipped since tunnel needs to be reprogrammed*/
    if ((pXcEntry->pTeTnlInfo != NULL) &&
        (MplsIsOamMonitoringTnl (pXcEntry->pTeTnlInfo) == TRUE))
    {
        return MPLS_SUCCESS;
    }
    if ((pXcEntry->pTeTnlInfo != NULL) &&
        (pXcEntry->pTeTnlInfo->u1TnlSwitchApp == TE_SWITCH_APP_ELPS) &&
        (TeCmnExtIsTnlProtected (pXcEntry->pTeTnlInfo) == TRUE))
    {
        return MPLS_SUCCESS;
    }

    if ((pInSegment->u4Label == 0) &&
        (pInSegment->mplsLabelIndex != 0) && (pInSegment->i4NPop == 2))
    {
        if (TMO_SLL_Count (&(pInSegment->mplsLabelIndex->LblList)) != 2)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsILMHwDel : Double stack labels not present\t\n");
            return MPLS_FAILURE;
        }
        pLblEntry = (tLblEntry *)
            TMO_SLL_First (&(pInSegment->mplsLabelIndex->LblList));
        if (pLblEntry != NULL)
        {
            /* First In Label */
            MplsHwDelIlmParams.InLabelList[0].u.MplsShimLabel =
                pLblEntry->u4Label;
            MplsHwDelIlmParams.InLabelList[0].MplsLabelType =
                MPLS_HW_LABEL_TYPE_GENERIC;
        }
        pLblEntry = (tLblEntry *)
            TMO_SLL_Last (&(pInSegment->mplsLabelIndex->LblList));
        if (pLblEntry != NULL)
        {
            /* Second In Label */
            MplsHwDelIlmParams.InLabelList[1].u.MplsShimLabel =
                pLblEntry->u4Label;
            MplsHwDelIlmParams.InLabelList[1].MplsLabelType =
                MPLS_HW_LABEL_TYPE_GENERIC;
        }
    }
    else
    {
        /* In Label */
        MplsHwDelIlmParams.InLabelList[0].u.MplsShimLabel = pInSegment->u4Label;
        MplsHwDelIlmParams.InLabelList[0].MplsLabelType =
            MPLS_HW_LABEL_TYPE_GENERIC;
    }

    MplsHwDelIlmParams.u4InL3Intf = pInSegment->u4IfIndex;

    /* For LDP the Incomming L3 If is MPLS Tnl IF.For RSVP,it is IVR If */
    if (CfaGetIfInfo (MplsHwDelIlmParams.u4InL3Intf, &IfInfo) == CFA_SUCCESS)
    {
        if (IfInfo.u1IfType == CFA_MPLS_TUNNEL)

        {
            /* Source Interface */
            if (CfaUtilGetIfIndexFromMplsTnlIf
                ((UINT2) MplsHwDelIlmParams.u4InL3Intf, &u4InL3If, TRUE)
                == CFA_SUCCESS)
            {
                if (CfaGetVlanId (u4InL3If, &u2InVlanId) != CFA_SUCCESS)
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               "MplsILMDel : CfaGetVlanId Failed\r\n");
                    return MPLS_FAILURE;
                }
            }
            MplsHwDelIlmParams.u4InVlanId = (UINT4) u2InVlanId;
            /* Get the IVR L3 Interface for the Tnl IF */
            MplsHwDelIlmParams.u4InL3Intf = u4InL3If;
        }
        else
        {
            /* For RSVP Incomming If is IVR If.So directly VlanId from it  */
            if (CfaGetVlanId
                ((UINT2) MplsHwDelIlmParams.u4InL3Intf,
                 &u2InVlanId) != CFA_SUCCESS)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "MplsILMDel : CfaGetVlanId Failed\r\n");
                return MPLS_FAILURE;
            }
            MplsHwDelIlmParams.u4InVlanId = (UINT4) u2InVlanId;
        }
    }
    else
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsILMDel : CfaGetIfType Failed\r\n");
        return MPLS_FAILURE;
    }

    CMNDB_DBG4 (DEBUG_DEBUG_LEVEL, "ILM Del in HW started with In Vlan Id : %d"
                "In L3 Intf : %d In Label1 %d In Label2 %d\n",
                MplsHwDelIlmParams.u4InVlanId, MplsHwDelIlmParams.u4InL3Intf,
                MplsHwDelIlmParams.InLabelList[0].u.MplsShimLabel,
                MplsHwDelIlmParams.InLabelList[1].u.MplsShimLabel);
    CMNDB_DBG4 (DEBUG_DEBUG_LEVEL,
                "Out Vlan Id : %d Out L3 Intf : %d Out Label1 : %d "
                "Out Label 2 : %d\n", MplsHwDelIlmParams.u4NewVlanId,
                MplsHwDelIlmParams.u4OutL3Intf,
                MplsHwDelIlmParams.MplsSwapLabel.u.MplsShimLabel,
                MplsHwDelIlmParams.MplsPushLblList[0].u.MplsShimLabel);

    /* Check and fill the HSLP Tunnel Interface index if this tunnel is stacked over HLSP
     * Otherwise fill the self Tunnel Interface index */
    if (pXcEntry->pTeTnlInfo != NULL)

    {
        MplsHwDelIlmParams.MplsHwLspId = pXcEntry->pTeTnlInfo->u4TnlIfIndex;
        if (TE_MAP_TNL_INFO (pXcEntry->pTeTnlInfo) != NULL)
        {
            MplsHwDelIlmParams.bIsStackedTunnel = TRUE;
            MplsHwDelIlmParams.MplsHwLspId =
                ((TE_MAP_TNL_INFO (pXcEntry->pTeTnlInfo))->u4TnlIfIndex);
        }
    }
#ifdef LDP_GR_WANTED
    /* Get the ILM Hw List Entry */
    pILMHwListEntry =
        MplsGetILMHwListEntryFromFecAndInIntf (pInSegment->ILMHwListFec,
                                               pInSegment->u1ILMHwListPrefLen,
                                               pInSegment->u4IfIndex);
    if (pILMHwListEntry != NULL)
    {
#ifdef MPLS_IPV6_WANTED
        if (MPLS_ILM_HW_LIST_FEC_ADDR_TYPE (pILMHwListEntry) ==
            MPLS_IPV6_ADDR_TYPE)
        {
            CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                        "MplsILMHwDel: ILM Hw List Entry found for"
                        " FEC: %s,and interface: %d\n",
                        Ip6PrintAddr (&MPLS_IPV6_ILM_HW_LIST_FEC
                                      (pILMHwListEntry)),
                        MPLS_ILM_HW_LIST_IN_L3_INTF (pILMHwListEntry));
        }
        else
#endif
        {

            CMNDB_DBG5 (DEBUG_DEBUG_LEVEL,
                        "MplsILMHwDel: ILM Hw List Entry found for"
                        " FEC: %d.%d.%d.%d,and interface: %d\n",
                        MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[0],
                        MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[1],
                        MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[2],
                        MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[3],
                        MPLS_ILM_HW_LIST_IN_L3_INTF (pILMHwListEntry));

        }
    }
#endif

#ifdef MBSM_WANTED
    if (pSlotInfo != NULL)
    {
#ifdef LDP_GR_WANTED
        if (pILMHwListEntry != NULL)
        {
            /* Reset the NP Bits to 0 */
            MplsHwListAddOrUpdateILMHwListEntry (pILMHwListEntry, 0);
        }
#endif
        if (MplsFsMplsMbsmHwDeleteILM (&MplsHwDelIlmParams, u4Action, pSlotInfo)
            == FNP_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsILMHwDel : FsMplsMbsmHwDeleteILM failed\t\n");
            return MPLS_FAILURE;
        }
#ifdef LDP_GR_WANTED
        if (pILMHwListEntry != NULL)
        {
            /* Delete the ILM Hw List Entry */
            if (MplsHwListDelILMHwListEntry (pILMHwListEntry) == MPLS_FAILURE)
            {
#ifdef MPLS_IPV6_WANTED
                if (MPLS_ILM_HW_LIST_FEC_ADDR_TYPE (pILMHwListEntry) ==
                    MPLS_IPV6_ADDR_TYPE)
                {

                    CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                                "MplsILMHwDel: Failed to Delete ILM Hw List "
                                "Entry for FEC: %s,and interface: %d\n",
                                Ip6PrintAddr (&MPLS_IPV6_ILM_HW_LIST_FEC
                                              (pILMHwListEntry)),
                                MPLS_ILM_HW_LIST_IN_L3_INTF (pILMHwListEntry));
                }
                else
#endif
                {

                    CMNDB_DBG5 (DEBUG_DEBUG_LEVEL,
                                "MplsILMHwDel: Failed to Delete ILM Hw List "
                                "Entry for FEC: %d.%d.%d.%d,and interface: %d\n",
                                MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[0],
                                MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[1],
                                MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[2],
                                MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[3],
                                MPLS_ILM_HW_LIST_IN_L3_INTF (pILMHwListEntry));

                }
                return MPLS_FAILURE;
            }
#ifdef MPLS_IPV6_WANTED
            if (MPLS_ILM_HW_LIST_FEC_ADDR_TYPE (pILMHwListEntry) ==
                MPLS_IPV6_ADDR_TYPE)
            {

                CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                            "MplsILMHwDel: ILM Hw List Entry Deleted Successfully for "
                            "FEC: %s,and interface: %d\n",
                            Ip6PrintAddr (&MPLS_IPV6_ILM_HW_LIST_FEC
                                          (pILMHwListEntry)),
                            MPLS_ILM_HW_LIST_IN_L3_INTF (pILMHwListEntry));
            }
            else
#endif
            {

                CMNDB_DBG5 (DEBUG_DEBUG_LEVEL,
                            "MplsILMHwDel: ILM Hw List Entry Deleted Successfully for "
                            "FEC: %d.%d.%d.%d,and interface: %d\n",
                            MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[0],
                            MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[1],
                            MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[2],
                            MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[3],
                            MPLS_ILM_HW_LIST_IN_L3_INTF (pILMHwListEntry));
            }

        }
#endif
    }
    else
#endif
    {
#ifdef L2RED_WANTED
        if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
        {
#ifdef NPAPI_WANTED
            if ((INSEGMENT_HW_STATUS (pInSegment)) == TRUE)
            {
#ifdef LDP_GR_WANTED
                if (pILMHwListEntry != NULL)
                {
                    /* Reset the NP Bits to 0 */
                    MplsHwListAddOrUpdateILMHwListEntry (pILMHwListEntry, 0);
                }
#endif
                if (MplsFsMplsHwDeleteILM (&MplsHwDelIlmParams, u4Action) ==
                    FNP_FAILURE)
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               "MplsILMHwDel : FsMplsHwDeleteILM failed\t\n");
                    return MPLS_FAILURE;
                }
#ifdef LDP_GR_WANTED
                if (pILMHwListEntry != NULL)
                {
                    /* Delete the ILM Hw List Entry */
                    if (MplsHwListDelILMHwListEntry (pILMHwListEntry) ==
                        MPLS_FAILURE)
                    {
#ifdef MPLS_IPV6_WANTED
                        if (MPLS_ILM_HW_LIST_FEC_ADDR_TYPE (pILMHwListEntry) ==
                            MPLS_IPV6_ADDR_TYPE)
                        {

                            CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                                        "MplsILMHwDel: Failed to Delete ILM Hw List "
                                        "Entry for FEC: %s,and interface: %d\n",
                                        Ip6PrintAddr (&MPLS_IPV6_ILM_HW_LIST_FEC
                                                      (pILMHwListEntry)),
                                        MPLS_ILM_HW_LIST_IN_L3_INTF
                                        (pILMHwListEntry));
                        }
                        else
#endif
                        {

                            CMNDB_DBG5 (DEBUG_DEBUG_LEVEL,
                                        "MplsILMHwDel: Failed to Delete ILM Hw List "
                                        "Entry for FEC: %d.%d.%d.%d,and interface: %d\n",
                                        MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)
                                        [0],
                                        MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)
                                        [1],
                                        MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)
                                        [2],
                                        MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)
                                        [3],
                                        MPLS_ILM_HW_LIST_IN_L3_INTF
                                        (pILMHwListEntry));

                        }
                        return MPLS_FAILURE;
                    }
#ifdef MPLS_IPV6_WANTED
                    if (MPLS_ILM_HW_LIST_FEC_ADDR_TYPE (pILMHwListEntry) ==
                        MPLS_IPV6_ADDR_TYPE)
                    {

                        CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                                    "MplsILMHwDel: ILM Hw List Entry Deleted Successfully for "
                                    "FEC: %s,and interface: %d\n",
                                    Ip6PrintAddr (&MPLS_IPV6_ILM_HW_LIST_FEC
                                                  (pILMHwListEntry)),
                                    MPLS_ILM_HW_LIST_IN_L3_INTF
                                    (pILMHwListEntry));
                    }
                    else
#endif

                    {

                        CMNDB_DBG5 (DEBUG_DEBUG_LEVEL,
                                    "MplsILMHwDel: ILM Hw List Entry Deleted Successfully for "
                                    "FEC: %d.%d.%d.%d,and interface: %d\n",
                                    MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[0],
                                    MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[1],
                                    MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[2],
                                    MPLS_ILM_HW_LIST_FEC (pILMHwListEntry)[3],
                                    MPLS_ILM_HW_LIST_IN_L3_INTF
                                    (pILMHwListEntry));

                    }

                }
#endif
            }
#endif
        }
    }
    if (pXcEntry->pOutIndex != NULL)
    {
        MEMSET (pXcEntry->pOutIndex->au1NextHopMac, MPLS_ZERO, MAC_ADDR_LEN);
    }
    INSEGMENT_HW_STATUS (pInSegment) = FALSE;

    CMNDB_DBG1 (DEBUG_DEBUG_LEVEL, "ILMDel in HW for InSegIndex %d succeeded\n",
                pInSegment->u4Index);
    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsILMHwDel - Exit \n");
    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsTnlNPAdd
 *  Description     : Used to Add a L3 FTN to NP
 *  Input           : pTeTnlInfo - Tunnel Info.
 *                    pXcEntry   - XcEntry associated with the Tunnel
 *  Output          : NONE
 *  Returns         : MPLS_FAILURE / MPLS_SUCCESS 
 ************************************************************************/
INT4
MplsTnlNPAdd (tTeTnlInfo * pTeTnlInfo, tXcEntry * pXcEntry,
              tMbsmSlotInfo * pSlotInfo)
{
    tMplsHwL3FTNInfo    MplsHwL3FTNInfo;
    tLblEntry          *pLblEntry = NULL;
    tTeTrfcParams      *pTeTrfcParams = NULL;
    UINT1               au1ZeroMac[MAC_ADDR_LEN];
    UINT1               au1NextMac[MAC_ADDR_LEN];
    UINT1               u1EncapType = 0;
#ifdef NPAPI_WANTED
    UINT4               u4VpnId = MPLS_DEF_VRF;
    UINT4               u4OutPort = 0;
#endif
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;
    UINT4               u4L3Intf = 0;
#if defined(NPAPI_WANTED) || defined(MBSM_WANTED)
    tHwPortInfo         pHwPortInfo;
#ifdef QOSX_WANTED
    INT4                i4HwExpMapId = 0;
#endif
#endif

    MEMSET (&MplsHwL3FTNInfo, 0, sizeof (tMplsHwL3FTNInfo));
    MEMSET (au1ZeroMac, MPLS_ZERO, MAC_ADDR_LEN);
    MEMSET (au1NextMac, MPLS_ZERO, MAC_ADDR_LEN);

#if defined(NPAPI_WANTED) || defined(MBSM_WANTED)
    MEMSET (&pHwPortInfo, 0, sizeof (tHwPortInfo));
#endif
    if (pTeTnlInfo->u1TnlSwitchApp == TE_SWITCH_APP_ELPS)
    {
        MplsHwL3FTNInfo.u1TnlHwPathType = pTeTnlInfo->u1TnlPathType;
    }

    if (pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType !=
        MPLS_TE_DEDICATED_ONE2ONE)
    {
        if (TE_TNL_TYPE_P2MP & TE_TNL_TYPE (pTeTnlInfo))
        {
            MEMCPY (au1NextMac, pXcEntry->pOutIndex->au1NextHopMac,
                    MAC_ADDR_LEN);
        }
        else
        {
            MEMCPY (au1NextMac, pTeTnlInfo->au1NextHopMac, MAC_ADDR_LEN);
        }
    }
    else
    {
        MEMCPY (au1NextMac, pXcEntry->pOutIndex->au1NextHopMac, MAC_ADDR_LEN);
    }

    if (MEMCMP (au1NextMac, au1ZeroMac, MAC_ADDR_LEN) == MPLS_ZERO)
    {
        CONVERT_TO_INTEGER ((pTeTnlInfo->TnlIngressLsrId), u4IngressId);
        CONVERT_TO_INTEGER ((pTeTnlInfo->TnlEgressLsrId), u4EgressId);
        CMNDB_DBG4 (DEBUG_DEBUG_LEVEL,
                    "MplsTnlNPAdd: ARP Unknown for Tunnel %d %d %x %x"
                    " - to be resolved\n", pTeTnlInfo->u4TnlIndex,
                    pTeTnlInfo->u4TnlInstance, OSIX_NTOHL (u4IngressId),
                    OSIX_NTOHL (u4EgressId));
        if (pXcEntry->pOutIndex->u1NHAddrType != GMPLS_UNKNOWN)
        {
            if (ArpResolve (pXcEntry->pOutIndex->NHAddr.u4_addr[0],
                            (INT1 *) MplsHwL3FTNInfo.au1DstMac, &u1EncapType)
                == ARP_FAILURE)
            {
                if (MplsTriggerArpResolve
                    (pXcEntry->pOutIndex->NHAddr.u4_addr[0],
                     (UINT1) MPLS_TNL_ARP_TIMER) == MPLS_SUCCESS)
                {
                    TeCmnExtUpdateArpResolveStatus (pTeTnlInfo,
                                                    MPLS_MLIB_TNL_CREATE,
                                                    (eDirection) 0,
                                                    MPLS_ARP_RESOLVE_WAITING);
                    /* MPLS_P2MP_LSP_CHANGES - S */
                    if (MPLS_SUCCESS ==
                        MplsCheckP2mpTnlRoleNotEgress (pTeTnlInfo))
                    {
                        XC_ARP_STATUS (pXcEntry) =
                            pTeTnlInfo->u1FwdArpResolveStatus;
                    }
                    /* MPLS_P2MP_LSP_CHANGES - E */

                    CONVERT_TO_INTEGER ((pTeTnlInfo->TnlIngressLsrId),
                                        u4IngressId);
                    CONVERT_TO_INTEGER ((pTeTnlInfo->TnlEgressLsrId),
                                        u4EgressId);
                    CMNDB_DBG5 (DEBUG_DEBUG_LEVEL,
                                "MplsTnlNPAdd: ARP Resolve Waiting for Tunnel "
                                "%d %d %x %x - Queued in ARP for NextHop %x\n",
                                pTeTnlInfo->u4TnlIndex,
                                pTeTnlInfo->u4TnlInstance,
                                OSIX_NTOHL (u4IngressId),
                                OSIX_NTOHL (u4EgressId),
                                pXcEntry->pOutIndex->NHAddr.u4_addr[0]);
                    return MPLS_FAILURE;
                }

                TeCmnExtUpdateArpResolveStatus (pTeTnlInfo,
                                                MPLS_MLIB_TNL_CREATE,
                                                (eDirection) 0,
                                                MPLS_ARP_RESOLVE_FAILED);
                /* MPLS_P2MP_LSP_CHANGES - S */
                if (MPLS_SUCCESS == MplsCheckP2mpTnlRoleNotEgress (pTeTnlInfo))
                {
                    XC_ARP_STATUS (pXcEntry) =
                        pTeTnlInfo->u1FwdArpResolveStatus;
                }
                /* MPLS_P2MP_LSP_CHANGES - E */

                CONVERT_TO_INTEGER ((pTeTnlInfo->TnlIngressLsrId), u4IngressId);
                CONVERT_TO_INTEGER ((pTeTnlInfo->TnlEgressLsrId), u4EgressId);
                CMNDB_DBG4 (DEBUG_DEBUG_LEVEL,
                            "MplsTnlNPAdd: INT Exit - ARP Resolve for Tunnel "
                            "%d %d %x %x - ARP Timer Retriggering Failed\n",
                            pTeTnlInfo->u4TnlIndex, pTeTnlInfo->u4TnlInstance,
                            OSIX_NTOHL (u4IngressId), OSIX_NTOHL (u4EgressId));
                return MPLS_FAILURE;
            }
        }
        else
        {
            MplsGetIfUnnumPeerMac (pXcEntry->pOutIndex->u4IfIndex,
                                   (UINT1 *) MplsHwL3FTNInfo.au1DstMac);
        }
        MEMCPY (pXcEntry->pOutIndex->au1NextHopMac,
                MplsHwL3FTNInfo.au1DstMac, MAC_ADDR_LEN);
        if (MPLS_SUCCESS != MplsCheckP2mpTnlRoleNotEgress (pTeTnlInfo))
        {
            MEMCPY (pTeTnlInfo->au1NextHopMac,
                    MplsHwL3FTNInfo.au1DstMac, MAC_ADDR_LEN);
        }
        else if (pTeTnlInfo->pTeP2mpTnlInfo != NULL)
        {
            MEMCPY (pTeTnlInfo->pTeP2mpTnlInfo->au1P2mpLastAddNextHop,
                    MplsHwL3FTNInfo.au1DstMac, MAC_ADDR_LEN);
        }
    }
    else
    {

        CMNDB_DBG4 (DEBUG_DEBUG_LEVEL,
                    "MplsTnlNPAdd: ARP Already known for Tunnel %d %d %x %x\n",
                    pTeTnlInfo->u4TnlIndex, pTeTnlInfo->u4TnlInstance,
                    OSIX_NTOHL (u4IngressId), OSIX_NTOHL (u4EgressId));

        if (pTeTnlInfo->u1DetourActive == MPLS_TRUE)
        {
            MEMCPY (MplsHwL3FTNInfo.au1DstMac, pTeTnlInfo->au1BkpNextHopMac,
                    MAC_ADDR_LEN);
        }
        else
        {
            MEMCPY (MplsHwL3FTNInfo.au1DstMac,
                    pXcEntry->pOutIndex->au1NextHopMac, MAC_ADDR_LEN);
        }
    }

    TeCmnExtUpdateArpResolveStatus (pTeTnlInfo, MPLS_MLIB_TNL_CREATE,
                                    (eDirection) 0, MPLS_ARP_RESOLVED);
    /* MPLS_P2MP_LSP_CHANGES - S */
    if (MPLS_SUCCESS == MplsCheckP2mpTnlRoleNotEgress (pTeTnlInfo))
    {
        XC_ARP_STATUS (pXcEntry) = pTeTnlInfo->u1FwdArpResolveStatus;
    }
    /* MPLS_P2MP_LSP_CHANGES - E */

    CONVERT_TO_INTEGER ((pTeTnlInfo->TnlIngressLsrId), u4IngressId);
    CONVERT_TO_INTEGER ((pTeTnlInfo->TnlEgressLsrId), u4EgressId);
    CMNDB_DBG4 (DEBUG_DEBUG_LEVEL,
                "MplsTnlNPAdd: ARP Resolve Succeeded for Tunnel"
                "%d %d %x %x\n", pTeTnlInfo->u4TnlIndex,
                pTeTnlInfo->u4TnlInstance, OSIX_NTOHL (u4IngressId),
                OSIX_NTOHL (u4EgressId));

    CMNDB_DBG6 (DEBUG_DEBUG_LEVEL,
                "Next hop MAC in Tunnel is %02x:%02x:%02x:%02x:%02x:%02x\n",
                MplsHwL3FTNInfo.au1DstMac[0], MplsHwL3FTNInfo.au1DstMac[1],
                MplsHwL3FTNInfo.au1DstMac[2], MplsHwL3FTNInfo.au1DstMac[3],
                MplsHwL3FTNInfo.au1DstMac[4], MplsHwL3FTNInfo.au1DstMac[5]);

    /*Check whether Tunnel is already programmed in NP */
    /*For P2MP tunnel, ARP should be triggered for all outsegments of P2MP tunnel */
    if ((pSlotInfo == NULL) && (pTeTnlInfo->u1TnlHwStatus == TRUE)
        && (pTeTnlInfo->u4TnlType != TE_TNL_TYPE_P2MP)
        && (pTeTnlInfo->u1FacFrrFlag != MPLS_TRUE))
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsTnlNPAdd: Tunnel is already Programmed or P2MP "
                   "Tunnel to be programmed - Tnl NP Add Skipped\n");
        return MPLS_SUCCESS;
    }

    /* if u1FacFrrFlag is set, still working/backup tunnel is not deleted by
     * the L2VPN module. Hence u4OrgTnlIfIndex should not be used here
     * */
    if ((pTeTnlInfo->u1FacFrrFlag != MPLS_TRUE) &&
        (TE_TNL_ROLE (pTeTnlInfo) == TE_INGRESS) &&
        (pTeTnlInfo->u1DetourActive == MPLS_TRUE))
    {
        MplsHwL3FTNInfo.u4EgrL3Intf = pTeTnlInfo->u4OrgTnlIfIndex;
    }
    else if ((pXcEntry->pOutIndex->GmplsOutSegment.OutSegmentDirection
              == MPLS_DIRECTION_FORWARD) ||
             (pTeTnlInfo->u1TnlSgnlPrtcl == TE_SIGPROTO_NONE))
    {
        if (MPLS_SUCCESS == MplsCheckP2mpTnlRoleNotEgress (pTeTnlInfo))
        {
            MplsHwL3FTNInfo.u4EgrL3Intf = pXcEntry->pOutIndex->u4IfIndex;
        }
        else
        {
            MplsHwL3FTNInfo.u4EgrL3Intf = pTeTnlInfo->u4TnlIfIndex;
        }
    }
    else
    {
        MplsHwL3FTNInfo.u4EgrL3Intf = pTeTnlInfo->u4RevTnlIfIndex;
    }

    MplsHwL3FTNInfo.MplsLabelList[0].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;
    MplsHwL3FTNInfo.MplsLabelList[0].u.MplsShimLabel =
        pXcEntry->pOutIndex->u4Label;

    if (pXcEntry->mplsLabelIndex != NULL)
    {
        pLblEntry = (tLblEntry *)
            TMO_SLL_First (&(pXcEntry->mplsLabelIndex->LblList));
        if (pLblEntry != NULL)
        {
            /* Second Label - Protected tunnel label */
            MplsHwL3FTNInfo.MplsLabelList[1].MplsLabelType =
                MPLS_HW_LABEL_TYPE_GENERIC;
            MplsHwL3FTNInfo.MplsLabelList[1].u.MplsShimLabel =
                pLblEntry->u4Label;
        }
    }

    /* Fill the diffserv related info from the tunnel */
    if (TE_DS_TNL_INFO (pTeTnlInfo) != NULL)
    {

        MplsFillQosInfo (pTeTnlInfo, &MplsHwL3FTNInfo.QosInfo[0]);
        /* Mark the QOS type as the Current supported QOS Type */
        MplsHwL3FTNInfo.QosInfo[0].QosType = MPLS_HW_QOS_POLICY_TYPE;

        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   " Tunnel DIFFSRV info copied to FTN ds\n");
    }

    if (TeCheckTrfcParamInTrfcParamTable
        (TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo), &pTeTrfcParams) == TE_SUCCESS)
    {
        if (pTeTrfcParams->pRSVPTrfcParams != NULL)
        {
            MplsHwL3FTNInfo.MplsHwTeParams.u4MaxRate
                = pTeTrfcParams->pRSVPTrfcParams->u4PeakDataRate;
        }
        else if (pTeTrfcParams->pCRLDPTrfcParams != NULL)
        {
            MplsHwL3FTNInfo.MplsHwTeParams.u4MaxRate
                = pTeTrfcParams->pCRLDPTrfcParams->u4PeakDataRate;
        }
        else
        {
            MplsHwL3FTNInfo.MplsHwTeParams.u4MaxRate
                = pTeTrfcParams->u4ResMaxRate;
        }
    }

    /* Fetch the L3 interface from the MPLS tunnel interface. */
    if (MplsGetL3Intf (MplsHwL3FTNInfo.u4EgrL3Intf, &u4L3Intf) != MPLS_SUCCESS)
    {
        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                    "MplsTnlNPAdd : Fetching L3 interface failed "
                    "for tunnel interface %d\t\n", MplsHwL3FTNInfo.u4EgrL3Intf);
    }

    /* The VLAN Id will get updated with valid value only if the L3 interface 
     * is IVR interface. */
    if (CfaGetVlanId (u4L3Intf, &(MplsHwL3FTNInfo.u2OutVlanId)) != CFA_SUCCESS)
    {
        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                    "MplsTnlNPAdd : CfaGetVlanId Failed for L3 "
                    "interface %d\r\n", u4L3Intf);
    }

    /* Get the physical port for the corresponding Logical L3 interface. */
    if (MplsGetPhyPortFromLogicalIfIndex (u4L3Intf,
                                          &(MplsHwL3FTNInfo.au1DstMac[0]),
                                          &(MplsHwL3FTNInfo.u4OutPort))
        != MPLS_SUCCESS)
    {
        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                    "Fetching Physical port for the L3 interface : %d"
                    "failed.\r\n", u4L3Intf);
    }

    CMNDB_DBG3 (DEBUG_DEBUG_LEVEL,
                "L3 FTN Programming started for Tunnel with Out L3 Intf : %d "
                "Label1 : %d Label2 : %d\n", MplsHwL3FTNInfo.u4EgrL3Intf,
                MplsHwL3FTNInfo.MplsLabelList[0].u.MplsShimLabel,
                MplsHwL3FTNInfo.MplsLabelList[1].u.MplsShimLabel);
#ifdef MBSM_WANTED
    if (pSlotInfo != NULL)
    {
        /*Fetch Qos Info for the Outgoing Interface */
        if (MplsHwL3FTNInfo.u4OutPort != 0)
        {
            u4OutPort = MplsHwL3FTNInfo.u4OutPort;
        }
        else
        {
            MEMCPY (pHwPortInfo.au1Mac,
                    MplsHwL3FTNInfo.au1DstMac, MAC_ADDR_LEN);
            pHwPortInfo.u2OutVlanId = MplsHwL3FTNInfo.u2OutVlanId;
            pHwPortInfo.u1MsgType = ISS_NP_GET_PORT_INFO;
            CfaNpGetHwPortInfo (&pHwPortInfo);
            u4OutPort = pHwPortInfo.u4OutPort;
        }
#ifdef QOSX_WANTED
        QosGetMplsExpProfileForPortAtEgress (MplsHwL3FTNInfo.u4OutPort,
                                             &i4HwExpMapId);
        MplsHwL3FTNInfo.QosInfo[0].u.MplsQosPolicy.i4HwExpMapId = i4HwExpMapId;
#endif
        if (MplsFsMplsMbsmHwCreateL3FTN (u4VpnId, &MplsHwL3FTNInfo, pSlotInfo)
            == FNP_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsTnlNPAdd : MplsFsMplsMbsmHwCreateL3FTN failed \t\n");
            return MPLS_FAILURE;
        }
    }
    else
#endif
    {
#ifdef L2RED_WANTED
        if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
        {
#ifdef NPAPI_WANTED
            /*Fetch Qos Info for the Outgoing Interface */
            if (MplsHwL3FTNInfo.u4OutPort != 0)
            {
                u4OutPort = MplsHwL3FTNInfo.u4OutPort;
            }
            else
            {
                MEMCPY (pHwPortInfo.au1Mac,
                        MplsHwL3FTNInfo.au1DstMac, MAC_ADDR_LEN);
                pHwPortInfo.u2OutVlanId = MplsHwL3FTNInfo.u2OutVlanId;
                pHwPortInfo.u1MsgType = ISS_NP_GET_PORT_INFO;
                CfaNpGetHwPortInfo (&pHwPortInfo);
                u4OutPort = pHwPortInfo.u4OutPort;
            }
#ifdef QOSX_WANTED
            QosGetMplsExpProfileForPortAtEgress (MplsHwL3FTNInfo.u4OutPort,
                                                 &i4HwExpMapId);
            MplsHwL3FTNInfo.QosInfo[0].u.MplsQosPolicy.i4HwExpMapId =
                i4HwExpMapId;
#endif
            if (MplsFsMplsHwCreateL3FTN (u4VpnId, &MplsHwL3FTNInfo) ==
                FNP_FAILURE)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "MplsTnlNPAdd : MplsFsMplsHwCreateL3FTN failed \t\n");
                return MPLS_FAILURE;
            }
#endif
        }
    }

    CONVERT_TO_INTEGER ((pTeTnlInfo->TnlIngressLsrId), u4IngressId);
    CONVERT_TO_INTEGER ((pTeTnlInfo->TnlEgressLsrId), u4EgressId);
    pTeTnlInfo->u1TnlHwStatus = TRUE;

    CMNDB_DBG4 (DEBUG_DEBUG_LEVEL,
                "L3 FTN Programming for Tunnel %d %d %x %x Succeeded\n",
                pTeTnlInfo->u4TnlIndex, pTeTnlInfo->u4TnlInstance,
                OSIX_NTOHL (u4IngressId), OSIX_NTOHL (u4EgressId));
    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsTunnelNPDeletion
 *  Description     : Function to delete Tunnel entry from NP
 *  Input           : pTeTnlInfo
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE
 ************************************************************************/
INT4
MplsTunnelNPDeletion (tTeTnlInfo * pTeTnlInfo)
{
    return MplsTnlNPDel (pTeTnlInfo, L2VPN_MODULE);
}

/************************************************************************
 *  Function Name   : MplsTnlNPDel 
 *  Description     : Used to Delete a L3 FTN from NP 
 *  Input           : pTeTnlInfo - Tunnel Info.
 *                    u4ModuleId - Module Id which is calling this
 *                                 function
 *  Output          : NONE
 *  Returns         : MPLS_FAILURE / MPLS_SUCCESS
 ************************************************************************/
INT4
MplsTnlNPDel (tTeTnlInfo * pTeTnlInfo, UINT4 u4ModuleId)
{
    tMplsHwL3FTNInfo    MplsHwL3FTNInfo;
    UINT4               u4TnlIfIndex = MPLS_ZERO;
    UINT4               u4L3Intf = 0;
#ifdef NPAPI_WANTED
    UINT4               u4VpnId = MPLS_DEF_VRF;
#endif

    MEMSET (&MplsHwL3FTNInfo, 0, sizeof (tMplsHwL3FTNInfo));

    /*Check whether tunnel entry is present in harware */
    if (pTeTnlInfo->u1TnlHwStatus == FALSE)
    {
        return MPLS_SUCCESS;

    }

    /* Tunnel should not be deleted from NP for the following scenarios
     *   1. It is a protected tunnel in case application is ELPS. For FRR,
     *   it should be skipped since tunnel needs to be reprogrammed
     *   2. Pseudowire is using this tunnel.
     *   3. BFD is monitoring the tunnel. */
    if (((u4ModuleId != L2VPN_MODULE) &&
         (TE_TNL_IN_USE_BY_VPN (pTeTnlInfo) & TE_TNL_INUSE_BY_L2VPN)) ||
        (MplsIsOamMonitoringTnl (pTeTnlInfo) == TRUE))
    {
        return MPLS_SUCCESS;
    }

    if ((pTeTnlInfo->u1TnlSwitchApp == TE_SWITCH_APP_ELPS) &&
        (TeCmnExtIsTnlProtected (pTeTnlInfo) == TRUE))
    {
        return MPLS_SUCCESS;
    }

    if (pTeTnlInfo->u1TnlSwitchApp == TE_SWITCH_APP_ELPS)
    {
        MplsHwL3FTNInfo.u1TnlHwPathType = pTeTnlInfo->u1TnlPathType;
    }

#ifdef L2RED_WANTED
    if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
    {
        if ((TE_TNL_ROLE (pTeTnlInfo) == TE_INGRESS) &&
            (pTeTnlInfo->u1DetourActive == MPLS_TRUE))
        {
            u4TnlIfIndex = pTeTnlInfo->u4OrgTnlIfIndex;
        }
        else if ((pTeTnlInfo->u1TnlRole == TE_INGRESS) ||
                 (pTeTnlInfo->u1TnlSgnlPrtcl == TE_SIGPROTO_NONE))
        {
            u4TnlIfIndex = pTeTnlInfo->u4TnlIfIndex;
        }
        else
        {
            u4TnlIfIndex = pTeTnlInfo->u4RevTnlIfIndex;
        }

        MplsHwL3FTNInfo.u4EgrL3Intf = u4TnlIfIndex;

        if (MPLS_SUCCESS != MplsCheckP2mpTnlRoleNotEgress (pTeTnlInfo))
        {
            MEMCPY (MplsHwL3FTNInfo.au1DstMac,
                    pTeTnlInfo->au1NextHopMac, MAC_ADDR_LEN);
        }
        else
        {
            /* P2MP tunnel programming is not handled currently. */
        }

        /* Fetch the L3 interface from the MPLS tunnel interface. */
        if (MplsGetL3Intf (MplsHwL3FTNInfo.u4EgrL3Intf,
                           &u4L3Intf) != MPLS_SUCCESS)
        {
            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                        "MplsTnlNPAdd : Fetching L3 interface failed "
                        "for tunnel interface %d\t\n",
                        MplsHwL3FTNInfo.u4EgrL3Intf);
        }

        /* The VLAN Id will get updated with valid value only if the L3 interface 
         * is IVR interface. */
        if (CfaGetVlanId (u4L3Intf, &(MplsHwL3FTNInfo.u2OutVlanId)) !=
            CFA_SUCCESS)
        {
            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                        "MplsTnlNPAdd : CfaGetVlanId Failed for L3 "
                        "interface %d\r\n", u4L3Intf);
        }

        /* Get the physical port for the corresponding Logical L3 interface. */
        if (MplsGetPhyPortFromLogicalIfIndex (u4L3Intf,
                                              &(MplsHwL3FTNInfo.au1DstMac[0]),
                                              &(MplsHwL3FTNInfo.u4OutPort))
            != MPLS_SUCCESS)
        {
            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                        "Fetching Physical port for the L3 interface : %d"
                        "failed.\r\n", u4L3Intf);
        }

        CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                    "MplsTnlNPDel : Tnl interface %d to be deleted\t\n",
                    u4TnlIfIndex);
#ifdef NPAPI_WANTED
        if (MplsFsMplsWpHwDeleteL3FTN (u4VpnId, &MplsHwL3FTNInfo) ==
            FNP_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsTnlNPDel : FsMplsWpHwDeleteL3FTN failed\t\n");
            return MPLS_FAILURE;
        }
#else
        UNUSED_PARAM (pTeTnlInfo);
#endif
    }
    /* Once the tunnel is deleted by the L2VPN module during switch back,
       u1DetourActive, u4OrgTnlIfIndex variables  need to be reset
     */
    if (pTeTnlInfo->u1FacFrrFlag == MPLS_TRUE)
    {
        pTeTnlInfo->u1DetourActive = MPLS_FALSE;
        pTeTnlInfo->u4OrgTnlIfIndex = MPLS_ZERO;
        pTeTnlInfo->u1FacFrrFlag = MPLS_FALSE;
    }

    pTeTnlInfo->u1TnlHwStatus = FALSE;

    CMNDB_DBG (DEBUG_DEBUG_LEVEL, " MplsTnlNPDel : Exit \n");

    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsDbUpdateTunnelInHw
 *  Description     : Used to Add & Delete  a L3 FTN in NP.
 *  Input           : u4Action     -  ADD/DELETE
 *                    pTeTnlInfo   -  Tunnel Info.
 *                    eDirection   -  Direction on which XC Entry to
 *                                    be updated.
 *  Output          : pbLlStatus   -  Indicates the Lower Layer interface
 *                                    status
 *  Returns         : MPLS_FAILURE / MPLS_SUCCESS
 ************************************************************************/
INT4
MplsDbUpdateTunnelInHw (UINT4 u4Action, tTeTnlInfo * pTeTnlInfo,
                        eDirection Direction, BOOL1 * pbLlStatus)
{
    tXcEntry           *pXcEntry = NULL;
    UINT1               u1ArpResolveStatus = MPLS_ZERO;
    INT4                i4RetVal = MPLS_SUCCESS;
    UINT4               u4TnlXcIndex = MPLS_ZERO;

    *pbLlStatus = XC_OPER_UP;

    if (pTeTnlInfo->u4OrgTnlXcIndex != MPLS_ZERO)
    {
        u4TnlXcIndex = pTeTnlInfo->u4OrgTnlXcIndex;
    }
    else
    {
        u4TnlXcIndex = pTeTnlInfo->u4TnlXcIndex;
    }

    pXcEntry = MplsGetXCEntryByDirection (u4TnlXcIndex, Direction);
    if (u4Action == MPLS_MLIB_TNL_CREATE)
    {
        if ((pXcEntry == NULL) || (pXcEntry->pOutIndex == NULL))
        {
            CMNDB_DBG
                (DEBUG_DEBUG_LEVEL,
                 "MplsDbUpdateTunnelInHw : Xc/XC->OUT is not present\t\n");
            return MPLS_FAILURE;
        }
        if (MplsSetIfInfo (pTeTnlInfo, pXcEntry->pOutIndex->u4IfIndex, IF_NAME)
            == RPTE_FAILURE)
        {
            return MPLS_FAILURE;
        }
        if (XC_OPER_STATUS (pXcEntry) == XC_OPER_UP)
        {
            pTeTnlInfo->u4TnlIfIndex = pXcEntry->pOutIndex->u4IfIndex;
            i4RetVal = MplsTnlNPAdd (pTeTnlInfo, pXcEntry, NULL);
        }
        else
        {
            *pbLlStatus = XC_OPER_STATUS (pXcEntry);
        }
    }
    else if (u4Action == MPLS_MLIB_TNL_DELETE)
    {
        i4RetVal = MplsTnlNPDel (pTeTnlInfo, MPLSDB_MODULE);
    }
    else if (u4Action == MPLS_MLIB_ILM_CREATE)
    {
        if ((pXcEntry == NULL) || (pXcEntry->pInIndex == NULL))
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsDbUpdateTunnelInHw : Xc/XC->IN is not present\t\n");
            return MPLS_FAILURE;
        }
        if (pXcEntry->pOutIndex != NULL)
        {
            if (MplsSetIfInfo
                (pTeTnlInfo, pXcEntry->pOutIndex->u4IfIndex,
                 IF_NAME) == MPLS_FAILURE)
            {
                return MPLS_FAILURE;
            }
        }
        if (XC_OPER_STATUS (pXcEntry) == XC_OPER_UP)
        {
            i4RetVal = MplsILMAdd (pXcEntry->pInIndex, pTeTnlInfo);
            u1ArpResolveStatus = XC_ARP_STATUS (pXcEntry);
            TeCmnExtUpdateArpResolveStatus (pTeTnlInfo, MPLS_MLIB_ILM_CREATE,
                                            Direction, u1ArpResolveStatus);
        }
        else
        {
            *pbLlStatus = XC_OPER_STATUS (pXcEntry);
        }
    }
    else if (u4Action == MPLS_MLIB_ILM_DELETE)
    {
        if ((pXcEntry != NULL) && (pXcEntry->pInIndex != NULL))
        {
            i4RetVal = MplsILMDel (pXcEntry->pInIndex);
            *pbLlStatus = XC_OPER_STATUS (pXcEntry);
        }
    }
    if (i4RetVal == MPLS_FAILURE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsDbUpdateTunnelInHw : Failed, EXIT\t\n");
    }
    return i4RetVal;
}

/************************************************************************
 *  Function Name   : MplsCheckLabelInGroup 
 *  Description     : Used to Check whether the label is already there in 
 *                    the Group.
 *  Input           : u4Label -  Label value to be checked
 *  Output          : NONE 
 *  Returns         : MPLS_FAILURE / MPLS_SUCCESS
 ************************************************************************/
INT4
MplsCheckLabelInGroup (UINT4 u4Label)
{
    if (LblMgrAvailableLblInLblGroup (gu2StaticId, 0, u4Label) == LBL_FAILURE)
    {
        return MPLS_FAILURE;
    }

    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsAssignLblToLblGroup 
 *  Description     : Used to assign the static label to the Label Space.
 *  Input           : u4Label - Label to be assigned.
 *  Output          : NONE 
 *  Returns         : MPLS_FAILURE / MPLS_SUCCESS
 ************************************************************************/
INT4
MplsAssignLblToLblGroup (UINT4 u4Label)
{
    if (LblMgrAssignLblToLblGroup (gu2StaticId, 0, u4Label) == LBL_FAILURE)
    {
        return MPLS_FAILURE;
    }
    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsReleaseLblToLblGroup 
 *  Description     : Used to deassign the static label to the Label Space.
 *  Input           : u4Label - Label to be assigned.
 *  Output          : NONE 
 *  Returns         : MPLS_FAILURE / MPLS_SUCCESS
 ************************************************************************/
INT4
MplsReleaseLblToLblGroup (UINT4 u4Label)
{
    if (LblMgrRelLblToLblGroup (gu2StaticId, 0, u4Label) == LBL_FAILURE)
    {
        return MPLS_FAILURE;
    }
    return MPLS_SUCCESS;
}

/************************************************************************
 *   Function Name   : CmnLock
 *   Description     : Function used in SNMP context to lock the LSR
 *                     module before accessing LSR SNMP APIs
 *                     (nmh routines)
 *   Input           : None
 *   Output          : None
 *   Returns         : SNMP_SUCCESS on Success otherwise SNMP_FAILURE
 *************************************************************************/
INT4
CmnLock (VOID)
{
    if (OsixSemTake (gLsrSemId) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/************************************************************************
 * Function Name   : CmnUnLock
 * Description     : Function used in SNMP context to unlock the LSR
 *                   module after accessing LSR SNMP APIs
 *                  (nmh routines)
 * Input           : None
 * Output          : None
 * Returns         : SNMP_SUCCESS on Success otherwise SNMP_FAILURE
 *************************************************************************/
INT4
CmnUnLock (VOID)
{
    if (OsixSemGive (gLsrSemId) != OSIX_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

#ifdef LANAI_WANTED
/*****************************************************************************/
/* Function Name : MplsUpdateAtmConnection                                   */
/* Description   : Function to Update ATM Connection                         */
/* Input(s)      : *pMplsAtmConnInfo - Atm Connection related params         */
/*               : u1Operation - Operation  Add or delete                    */
/* Output(s)     : None                                                      */
/* Return(s)     : MPLS_SUCCESS or MPLS_FAILURE                              */
/*****************************************************************************/
UINT1
MplsUpdateAtmConnection (tMplsAtmConnInfo * pMplsAtmConnInfo, UINT1 u1Operation)
{
    /* This function is to be ported once the ATM Interface is available */
    UNUSED_PARAM (pMplsAtmConnInfo);
    UNUSED_PARAM (u1Operation);
    return MPLS_SUCCESS;
}
#endif

/*****************************************************************************/
/* Function Name : MplsHandleFtnArpResolveTmrEvent                           */
/* Description   : This function handles the Arp Resolution Timer Expiry for */
/*                 FTN.                                                      */
/* Input(s)      : pArg - Pointer to expired timer (Unused Now)              */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
MplsHandleFtnArpResolveTmrEvent (VOID *pArg)
{
    tFtnEntry          *pFtnEntry = NULL;
    tXcEntry           *pXcEntry = NULL;
    UINT4               u4FtnIndex = MPLS_ZERO;
    tGenU4Addr          Prefix;

    UNUSED_PARAM (pArg);

    MEMSET (&Prefix, 0, sizeof (tGenU4Addr));

    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsHandleFtnArpResolveTmrEvent: ENTRY \n");

    /* Since this function is called from FM Module, CMN Lock is taken here. */
    MPLS_CMN_LOCK ();

    while (1)
    {
        pFtnEntry = MplsFtnTableNextEntry (u4FtnIndex);
        if (pFtnEntry == NULL)
        {
            break;
        }

        u4FtnIndex = pFtnEntry->u4FtnIndex;

        if (pFtnEntry->u1RowStatus != MPLS_STATUS_ACTIVE)
        {
            continue;
        }

        if (pFtnEntry->i4ActionType != MPLS_FTN_ON_NON_TE_LSP)
        {
            continue;
        }

        if (pFtnEntry->u1ArpResolveStatus != MPLS_ARP_RESOLVE_WAITING)
        {
            continue;
        }

        pXcEntry = pFtnEntry->pActionPtr;

        if ((pXcEntry == NULL) || (pXcEntry->pOutIndex == NULL))
        {
            continue;
        }

#ifdef MPLS_IPV6_WANTED
        if (pFtnEntry->u1AddrType == MPLS_IPV6_ADDR_TYPE)
        {
            MEMCPY (MPLS_IPV6_U4_ADDR (Prefix.Addr),
                    pFtnEntry->DestAddrMin.u1_addr, IPV6_ADDR_LENGTH);
        }
        else
#endif
        {
            MEMCPY (&MPLS_IPV4_U4_ADDR (Prefix.Addr),
                    pFtnEntry->DestAddrMin.u4_addr, IP_ADDR_LENGTH);
        }
        Prefix.u2AddrType = pFtnEntry->u1AddrType;

        if (MplsFTNHwAdd (pFtnEntry, NULL) == MPLS_FAILURE)
        {
            CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                        "FTN %x HW Addition Failed for Next Hop %x after "
                        "Resolving Arp\n",
                        OSIX_NTOHL (*(UINT4 *)
                                    &(pFtnEntry->DestAddrMin.u4_addr)),
                        OSIX_NTOHL (*(UINT4 *)
                                    &(pXcEntry->pOutIndex->NHAddr.u4_addr[0])));
            continue;
        }
#ifdef MPLS_L2VPN_WANTED
        /* Send Event to L2VPN task. L2VPN will check this
         * XC index in its data base and if present then it
         UINT4               u4Port = LDP_ZERO;
         * will make the Pseudowire Oper States to Up */
        MplsNonTePostEventForL2Vpn (pXcEntry->u4Index, &Prefix,
                                    L2VPN_MPLS_PWVC_LSP_UP);
#endif
    }

    MPLS_CMN_UNLOCK ();

    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsHandleFtnArpResolveTmrEvent: EXIT \n");
    return;
}

/*****************************************************************************/
/* Function Name : MplsHandleIlmArpResolveTmrEvent                           */
/* Description   : This function handles the Arp Resolution Timer Expiry for */
/*                 ILM.                                                      */
/* Input(s)      : pArg - Pointer to expired timer (Unused Now)              */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
MplsHandleIlmArpResolveTmrEvent (VOID *pArg)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tInSegment         *pInSegment = NULL;
    tXcEntry           *pXcEntry = NULL;
    tOutSegment        *pOutSegment = NULL;
    UINT4               u4OutIndex = MPLS_ZERO;
    UINT4               u4InIndex = MPLS_ZERO;
    UINT1               u1RetVal = TE_FAILURE;

    UNUSED_PARAM (pArg);

    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsHandleIlmArpResolveTmrEvent: ENTRY \n");

    /* Since, this function is called from FM Module, CMN Lock is taken here */
    MPLS_CMN_LOCK ();

    while (1)
    {
        pInSegment = MplsInSegmentTableNextEntry (u4InIndex);

        if (pInSegment == NULL)
        {
            break;
        }

        u4InIndex = pInSegment->u4Index;

        if (INSEGMENT_ROW_STATUS (pInSegment) != ACTIVE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL, "InSegment Not Active \n");
            continue;
        }

        pXcEntry = (tXcEntry *) pInSegment->pXcIndex;

        if (pXcEntry == NULL)
        {
            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                        "XC is NULL for the InSegment Entry with Label %d\n",
                        pInSegment->u4Label);
            continue;
        }

        if (XC_OUTINDEX (pXcEntry) == NULL)
        {
            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                        "OutSegmentEntry for the InSeg Label %d is NULL\n",
                        pInSegment->u4Label);
            continue;
        }

        if (XC_ARP_STATUS (pXcEntry) != MPLS_ARP_RESOLVE_WAITING)
        {
            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                        "Next Hop Arp is not in Resolve Waiting State for "
                        "InSeg Label %d\n", pInSegment->u4Label);
            continue;
        }

        /* MPLS_P2MP_LSP_CHANGES - S */
        if ((MplsCheckXCForP2mp (pXcEntry)) == MPLS_SUCCESS)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "XC entry is associated to P2MP tunnel \n");
            continue;
        }
        /* MPLS_P2MP_LSP_CHANGES - E */

        if (TeCheckXcIndex (pXcEntry->u4Index, &pTeTnlInfo) == TE_SUCCESS)
        {
            CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                        "ILM HW Addition with Tunnel Failed for Next Hop "
                        "%x after resolving  Arp for InSeg Label %d called\n",
                        (*(UINT4 *) &(pXcEntry->pOutIndex->NHAddr.u4_addr[0])),
                        pInSegment->u4Label);

            if (MplsILMHwAdd (pInSegment, pTeTnlInfo, NULL) == MPLS_FAILURE)
            {
                CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                            "ILM HW Addition with Tunnel Failed for Next Hop"
                            " %x after Resolving Arp for InSeg Label %d\n",
                            (*(UINT4 *)
                             &(pXcEntry->pOutIndex->NHAddr.u4_addr[0])),
                            pInSegment->u4Label);
                continue;
            }
            pTeTnlInfo->u1FwdArpResolveStatus = XC_ARP_STATUS (pXcEntry);
            if (pTeTnlInfo->u1FwdArpResolveStatus == MPLS_ARP_RESOLVED)
            {
                u1RetVal = TeCallUpdateTnlOperStatus (pTeTnlInfo, TE_OPER_UP);

                if (u1RetVal != TE_SUCCESS)
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               "TeCallUpdateTnlOperStatus failed "
                               "after ARP resolution\n");
                    continue;
                }
            }
            CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                        "ILM HW Addition with Tunnel Failed for Next Hop "
                        "%x after resolving  Arp for InSeg Label %d success\n",
                        (*(UINT4 *) &(pXcEntry->pOutIndex->NHAddr.u4_addr[0])),
                        pInSegment->u4Label);
        }
        else
        {
            CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                        "ILM HW Addition with Tunnel Failed for Next Hop "
                        "%x after resolving  Arp for InSeg Label %d called\n",
                        (*(UINT4 *) &(pXcEntry->pOutIndex->NHAddr.u4_addr[0])),
                        pInSegment->u4Label);

            if (MplsILMHwAdd (pInSegment, NULL, NULL) == MPLS_FAILURE)
            {
                CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                            "ILM HW Addition without Tnl Failed for Next Hop "
                            "%x after Resolving Arp for InSeg Label %d\n",
                            (*(UINT4 *)
                             &(pXcEntry->pOutIndex->NHAddr.u4_addr[0])),
                            pInSegment->u4Label);
                continue;
            }

            CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                        "ILM HW Addition with Tunnel Failed for Next Hop "
                        "%x after resolving  Arp for InSeg Label %d success\n",
                        (*(UINT4 *) &(pXcEntry->pOutIndex->NHAddr.u4_addr[0])),
                        pInSegment->u4Label);
        }
    }

    /* MPLS_P2MP_LSP_CHANGES - S */
    while (MPLS_ONE)
    {
        pOutSegment = MplsOutSegmentTableNextEntry (u4OutIndex);
        if (pOutSegment == NULL)
        {
            break;
        }
        u4OutIndex = pOutSegment->u4Index;

        if (OUTSEGMENT_ROW_STATUS (pOutSegment) != ACTIVE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL, "OutSegment Not Active \n");
            continue;
        }

        pXcEntry = (tXcEntry *) (pOutSegment->pXcIndex);
        if (pXcEntry == NULL)
        {
            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                        "XC is NULL for the OutSegment Entry with Label %d\n",
                        pOutSegment->u4Label);
            continue;
        }

        if ((MplsCheckXCForP2mp (pXcEntry)) != MPLS_SUCCESS)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "XC entry is not associated to P2MP tunnel \n");
            continue;
        }

        if (XC_ININDEX (pXcEntry) == NULL)
        {
            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                        "InSegmentEntry for the OutSeg Label %d is NULL\n",
                        pOutSegment->u4Label);
            continue;
        }

        if (XC_ARP_STATUS (pXcEntry) != MPLS_ARP_RESOLVE_WAITING)
        {
            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                        "Next Hop Arp is not in Resolve Waiting State for "
                        "OutSeg Label %d\n", pOutSegment->u4Label);
            continue;
        }

        if (MplsP2mpILMHwAdd (pOutSegment, NULL) == MPLS_FAILURE)
        {
            CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                        "ILM HW Addition with Tunnel Failed for Next Hop"
                        " %x after Resolving Arp for OutSeg Label %d\n",
                        (*(UINT4 *) &(pOutSegment->NHAddr.u4_addr[0])),
                        pOutSegment->u4Label);
            continue;
        }
        if (XC_ARP_STATUS (pXcEntry) != MPLS_ARP_RESOLVED)
        {
            CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                        "Next Hop ARP is not resolved for OutSeg Label %d\n",
                        pOutSegment->u4Label);
            continue;
        }

        pTeTnlInfo = XC_TNL_TBL_PTR (pXcEntry);
        if (XC_OPER_STATUS (pXcEntry) == XC_OPER_UP)
        {
            /* Update the count of operational P2MP branch out-segments and 
             * operational status for all destinations associated to this 
             * branch */
            MplsDbUpdateP2mpDestOperStatus (pTeTnlInfo, pXcEntry, TE_OPER_UP);

            /* Tunnel is made oper up when first out-segment is operationally 
             * up after successful ARP resolution. Tunnel oper status need not 
             * be updated when ARP gets resolved for subsequent out-segments 
             * of P2MP tunnel. */
            if (TE_P2MP_TNL_ACTIVE_BRANCH_COUNT (pTeTnlInfo) != MPLS_ONE)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "P2MP tunnel is already operational. "
                           "TeCallUpdateTnlOperStatus not invoked.\n");
                continue;
            }
            if (TeCallUpdateTnlOperStatus (pTeTnlInfo, TE_OPER_UP) !=
                TE_SUCCESS)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "TeCallUpdateTnlOperStatus failed after ARP "
                           "resolution\n");
                continue;
            }
        }
    }
    /* MPLS_P2MP_LSP_CHANGES - E */
    MPLS_CMN_UNLOCK ();

    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsHandleIlmArpResolveTmrEvent: EXIT \n");
    return;
}

/************************************************************************
 * Function Name   : MplsGetIfIndexAndLabelFromXcIndex
 * Description     : Function to get In Interface Index (MPLS Tunnel Intf)
 *                   and Label from XC Index. This function is an API to
 *                   be used by Applications like L2VPN. 
 *                   Hence, MPLS_CMN_LOCK is taken here.
 * Input           : u4XcIndex - XC Index
 * Output          : pu4InIntf - MPLS Tunnel Interface Index
 *                   pu4Label - MPLS Label
 * Returns         : MPLS_SUCCESS or MPLS_FAILURE
 ************************************************************************/
INT4
MplsGetIfIndexAndLabelFromXcIndex (UINT4 u4XcIndex, UINT4 *pu4InIntf,
                                   UINT4 *pu4Label)
{
    tXcEntry           *pXcEntry = NULL;

    MPLS_CMN_LOCK ();

    pXcEntry = MplsGetXCEntryByDirection (u4XcIndex, MPLS_DEF_DIRECTION);

    /* This function is called from L2VPN thread only */
    /* XCIndex of Protection tunnel should be used when Protection status
     * is LOCAL_PROT_IN_USE */
    if ((pXcEntry != NULL) && (pXcEntry->pTeTnlInfo != NULL) &&
        (pXcEntry->pTeTnlInfo->u1TnlLocalProtectInUse == LOCAL_PROT_IN_USE))
    {
        u4XcIndex = pXcEntry->pTeTnlInfo->u4TnlXcIndex;
        pXcEntry = MplsGetXCEntryByDirection (u4XcIndex, MPLS_DEF_DIRECTION);
    }

    if ((pXcEntry == NULL) || (pXcEntry->u1OperStatus != XC_OPER_UP))
    {
        MPLS_CMN_UNLOCK ();
        return MPLS_FAILURE;
    }

    if ((pXcEntry->pTeTnlInfo != NULL) &&
        (pXcEntry->pTeTnlInfo->u1TnlRole == TE_INGRESS) &&
        (pXcEntry->pTeTnlInfo->u4TnlMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL))
    {
        pXcEntry =
            MplsGetXCEntryByDirection (u4XcIndex, MPLS_DIRECTION_REVERSE);

        if ((pXcEntry == NULL) || (pXcEntry->u1OperStatus != XC_OPER_UP))
        {
            MPLS_CMN_UNLOCK ();
            return MPLS_FAILURE;
        }
    }
    if (pXcEntry->pInIndex == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return MPLS_FAILURE;
    }
    *pu4Label = pXcEntry->pInIndex->u4Label;
    *pu4InIntf = pXcEntry->pInIndex->u4IfIndex;

    MPLS_CMN_UNLOCK ();
    return MPLS_SUCCESS;
}

/************************************************************************
 * Function Name   : MplsDbUpdateTunnelXCIndex
 * Description     : This function updates the XC index in the structure
 *                   tTeTnlInfo and also updates the backpointer
 *                   information about the tunnel information in tXcEntry.
 * Input           : pTeTnlInfo   - Pointer to Tunnel Info
 *                   pInXcEntry   - Pointer to New XC Entry which is to
 *                                  be associated with pTeTnlInfo
 *                   u4XcIndex    - New Xc Index which is to be associated
 *                                  with pTeTnlInfo                                  
 * Output          : None
 * Returns         : None
 ************************************************************************/
VOID
MplsDbUpdateTunnelXCIndex (tTeTnlInfo * pTeTnlInfo, VOID *pInXcEntry,
                           UINT4 u4XcIndex)
{
    tXcEntry           *pXcEntry = NULL;
    tXcEntry           *pRevXcEntry = NULL;
    tXcEntry           *pNewXcEntry = (tXcEntry *) pInXcEntry;

    if (pTeTnlInfo->u4TnlXcIndex != 0)
    {
        /* If the XC is expected to be changed for tunnel then remove
           the tunnel back pointer from old XC entry */
        pXcEntry = MplsGetXCEntryByDirection (pTeTnlInfo->u4TnlXcIndex,
                                              MPLS_DIRECTION_FORWARD);
        if (pXcEntry != NULL)
        {
            pXcEntry->pTeTnlInfo = NULL;
        }

        if (pTeTnlInfo->u4TnlMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
        {
            pRevXcEntry = MplsGetXCEntryByDirection (pTeTnlInfo->u4TnlXcIndex,
                                                     MPLS_DIRECTION_REVERSE);

            if (pRevXcEntry != NULL)
            {
                pRevXcEntry->pTeTnlInfo = NULL;
            }
        }

        pTeTnlInfo->u4TnlXcIndex = 0;
    }

    if (pNewXcEntry != NULL)
    {
        pNewXcEntry->pTeTnlInfo = pTeTnlInfo;
        pTeTnlInfo->u4TnlXcIndex = pNewXcEntry->u4Index;
        pTeTnlInfo->u4OrgTnlXcIndex = pNewXcEntry->u4Index;
    }
    else if (u4XcIndex != 0)
    {
        pNewXcEntry = MplsGetXCEntryByDirection (u4XcIndex, MPLS_DEF_DIRECTION);

        if (pNewXcEntry != NULL)
        {
            pNewXcEntry->pTeTnlInfo = pTeTnlInfo;
        }

        if (pTeTnlInfo->u4TnlMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
        {
            pRevXcEntry = MplsGetXCEntryByDirection (u4XcIndex,
                                                     MPLS_DIRECTION_REVERSE);

            if (pRevXcEntry != NULL)
            {
                pRevXcEntry->pTeTnlInfo = pTeTnlInfo;
            }
        }

        pTeTnlInfo->u4TnlXcIndex = u4XcIndex;
        pTeTnlInfo->u4OrgTnlXcIndex = u4XcIndex;
    }

    return;
}

/* MS-PW */
/************************************************************************
 * Function Name   : MplsGetInorOutIfIndexAndLabelFromXcIndex
 * Description     : Function to get In or Out Segment In Interface Index
 *                  (MPLS Tunnel Intf) and Label from XC Index for MS PW. 
 *                This function is an API to be used by Applications like L2VPN. 
 *                   Hence, MPLS_CMN_LOCK is taken here.
 * Input           : u4XcIndex - XC Index
 * Output          : pu4Intf - MPLS Tunnel Interface Index
 *                   pu4Label - MPLS Label
 *                   u4InOutFlag -True mean Insegment else Out segment
 * Returns         : MPLS_SUCCESS or MPLS_FAILURE
 ************************************************************************/
INT4
MplsGetInorOutIfIndexAndLabelFromXcIndex (UINT4 u4XcIndex, UINT4 u4Direction,
                                          UINT4 *pu4Intf, UINT4 *pu4Label,
                                          UINT4 u4InOutFlag)
{
    tXcEntry           *pXcEntry = NULL;

    MPLS_CMN_LOCK ();

    pXcEntry = MplsGetXCEntryByDirection (u4XcIndex, (eDirection) u4Direction);

    if ((pXcEntry != NULL) && (pXcEntry->pTeTnlInfo != NULL) &&
        (((pXcEntry->pTeTnlInfo->u1TnlRole == TE_INGRESS)
          && (u4InOutFlag == MPLS_TRUE))
         || ((pXcEntry->pTeTnlInfo->u1TnlRole == TE_EGRESS)
             && (u4InOutFlag == MPLS_FALSE)))
        && (pXcEntry->pTeTnlInfo->u4TnlMode ==
            TE_TNL_MODE_COROUTED_BIDIRECTIONAL))
    {
        pXcEntry =
            MplsGetXCEntryByDirection (u4XcIndex, MPLS_DIRECTION_REVERSE);

        if ((pXcEntry == NULL) || (pXcEntry->u1OperStatus != XC_OPER_UP))
        {
            MPLS_CMN_UNLOCK ();
            return MPLS_FAILURE;
        }
    }

    if (pXcEntry == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return MPLS_FAILURE;
    }

    if ((u4InOutFlag == MPLS_TRUE) && (pXcEntry->pInIndex != NULL))
    {
        *pu4Label = pXcEntry->pInIndex->u4Label;
        *pu4Intf = pXcEntry->pInIndex->u4IfIndex;
    }
    else if ((u4InOutFlag == MPLS_FALSE) && (pXcEntry->pOutIndex != NULL))
    {
        *pu4Label = pXcEntry->pOutIndex->u4Label;
        *pu4Intf = pXcEntry->pOutIndex->u4IfIndex;
    }
    else
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsGetInorOutIfIndexAndLabelFromXcIndex: wrong input\n");
    }

    MPLS_CMN_UNLOCK ();
    return MPLS_SUCCESS;

}

/*MS-PW */
/* MPLS_P2MP_LSP_CHANGES - S */
/************************************************************************
 *  Function Name   : MplsP2mpILMHwAdd
 *  Description     : Function to add ILM entry to NP
 *  Input           : Out-Segment and Slot Information
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE
 ************************************************************************/
INT4
MplsP2mpILMHwAdd (tOutSegment * pOutSegment, VOID *pSlotInfo)
{
    tXcEntry           *pXcEntry = NULL;
    tInSegment         *pInSegment = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tMplsHwIlmInfo      MplsHwIlmInfo;
    UINT4               u4P2mpId = MPLS_ZERO;
    UINT4               u4InL3If = MPLS_ZERO;
    UINT4               u4OutL3If = MPLS_ZERO;
    UINT2               u2InVlanId = MPLS_ZERO;
    UINT2               u2OutVlanId = MPLS_ZERO;
    UINT1               u1EncapType = MPLS_ZERO;

#ifndef MBSM_WANTED
    UNUSED_PARAM (pSlotInfo);
#endif

    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsP2mpILMHwAdd: Entry\n");

    MEMSET (&MplsHwIlmInfo, MPLS_ZERO, sizeof (tMplsHwIlmInfo));
    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsP2mpILMHwAdd: OutSegment not present \n");
        return MPLS_FAILURE;
    }

    pXcEntry = (tXcEntry *) (pOutSegment->pXcIndex);
    if (pXcEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsP2mpILMHwAdd: XC not present \n");
        return MPLS_FAILURE;
    }

    pTeTnlInfo = XC_TNL_TBL_PTR (pXcEntry);
    if (pTeTnlInfo == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsP2mpILMHwAdd: Tunnel entry not present \n");
        return MPLS_FAILURE;
    }

    pInSegment = pXcEntry->pInIndex;
    if (NULL == pInSegment)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsP2mpILMHwAdd: InSeg entry not present \n");
        return MPLS_FAILURE;
    }

    if (OUTSEGMENT_HW_STATUS (pOutSegment) == MPLS_TRUE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsP2mpILMHwAdd: Out-segment HW status is TRUE. Exit \n");
        return MPLS_SUCCESS;
    }

    if (MPLS_ZERO == INSEGMENT_LABEL (pInSegment))
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsP2mpILMHwAdd: In-label is zero \n");
        return MPLS_FAILURE;
    }
    pInSegment->u1LblAction = MPLS_LABEL_ACTION_SWAP;

    /* In Label */
    MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel = pInSegment->u4Label;
    MplsHwIlmInfo.InLabelList[0].MplsLabelType = MPLS_HW_LABEL_TYPE_GENERIC;

    if (ACTIVE != OUTSEGMENT_ROW_STATUS (pOutSegment))
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsP2mpILMHwAdd: Out segment not active \n");
        return MPLS_FAILURE;
    }
    MplsHwIlmInfo.LspType = MPLS_HW_LSP_ILM;

    /*TODO Peer mac address to be updated here from CFA for 
     * unnumbered outgoing interface */
    if (ArpResolve (OUTSEGMENT_NH_ADDR (pOutSegment),
                    (INT1 *) MplsHwIlmInfo.au1DstMac,
                    &u1EncapType) == ARP_FAILURE)
    {
        if (MplsTriggerArpResolve (OUTSEGMENT_NH_ADDR (pOutSegment),
                                   (UINT1) MPLS_ILM_ARP_TIMER) == MPLS_SUCCESS)
        {
            XC_ARP_STATUS (pXcEntry) = MPLS_ARP_RESOLVE_WAITING;
            CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                        "INT EXIT - Arp Resolve Waiting for ILM with "
                        "InSegIndex %d and Next Hop %x - Queued in ARP\n",
                        INSEGMENT_INDEX (pInSegment),
                        OUTSEGMENT_NH_ADDR (pOutSegment));
            return MPLS_FAILURE;
        }

        XC_ARP_STATUS (pXcEntry) = MPLS_ARP_RESOLVE_FAILED;
        CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                    " INT Exit - ARP Resolve failed for ILM with "
                    "InSegIndex %d and Next Hop %x\n",
                    INSEGMENT_INDEX (pInSegment),
                    OUTSEGMENT_NH_ADDR (pOutSegment));
        return MPLS_FAILURE;
    }

    XC_ARP_STATUS (pXcEntry) = MPLS_ARP_RESOLVED;
    CMNDB_DBG2 (DEBUG_DEBUG_LEVEL,
                "Arp Resolve Succeeded for ILM with InSegIndex %d and "
                "Next Hop %x\n", INSEGMENT_INDEX (pInSegment),
                OUTSEGMENT_NH_ADDR (pOutSegment));
    /* Label to be swapped */

    if ((MPLS_ZERO == OUTSEGMENT_LABEL (pOutSegment)) ||
        (MPLS_THREE == OUTSEGMENT_LABEL (pOutSegment)))
    {
        MplsHwIlmInfo.LabelAction = (tLabelAction) MPLS_HW_ACTION_PHP;
    }
    else
    {
        MplsHwIlmInfo.LabelAction = (tLabelAction) MPLS_LABEL_ACTION_SWAP;
        MplsHwIlmInfo.MplsSwapLabel.u.MplsShimLabel =
            OUTSEGMENT_LABEL (pOutSegment);
    }

    MplsHwIlmInfo.u4OutL3Intf = OUTSEGMENT_IF_INDEX (pOutSegment);
    MplsHwIlmInfo.u4InL3Intf = INSEGMENT_IF_INDEX (pInSegment);

    /* Source Interface */
    if (CfaUtilGetIfIndexFromMplsTnlIf
        ((UINT2) MplsHwIlmInfo.u4InL3Intf, &u4InL3If, TRUE) == CFA_SUCCESS)
    {
        if (CfaGetVlanId (u4InL3If, &u2InVlanId) != CFA_SUCCESS)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "CfaGetVlanId Failed for insegment\r\n");
            return MPLS_FAILURE;
        }
    }
    else
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "CfaUtilGetIfIndexFromMplsTnlIf Failed for insegment\r\n");
    }
    MplsHwIlmInfo.u4InVlanId = (UINT4) u2InVlanId;
    MplsHwIlmInfo.u4InL3Intf = u4InL3If;

    /* Destination Interface */
    if (CfaUtilGetIfIndexFromMplsTnlIf ((UINT2) MplsHwIlmInfo.u4OutL3Intf,
                                        &u4OutL3If, TRUE) == CFA_SUCCESS)
    {
        if (CfaGetVlanId (u4OutL3If, &u2OutVlanId) != CFA_SUCCESS)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "CfaGetVlanId Failed for outsegment\r\n");
            return MPLS_FAILURE;
        }
    }
    else
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "CfaUtilGetIfIndexFromMplsTnlIf Failed for outsegment\r\n");
    }
    MplsHwIlmInfo.u4OutL3Intf = u4OutL3If;
    MplsHwIlmInfo.u4NewVlanId = (UINT4) u2OutVlanId;

    CMNDB_DBG4 (DEBUG_DEBUG_LEVEL, "ILM Add in HW started with In Vlan Id : %d"
                "In L3 Intf : %d In Label1 %d In Label2 %d\n",
                MplsHwIlmInfo.u4InVlanId, MplsHwIlmInfo.u4InL3Intf,
                MplsHwIlmInfo.InLabelList[0].u.MplsShimLabel,
                MplsHwIlmInfo.InLabelList[1].u.MplsShimLabel);
    CMNDB_DBG4 (DEBUG_DEBUG_LEVEL,
                "Out Vlan Id : %d Out L3 Intf : %d Out Label1 : %d "
                "Out Label 2 : %d\n", MplsHwIlmInfo.u4NewVlanId,
                MplsHwIlmInfo.u4OutL3Intf,
                MplsHwIlmInfo.MplsSwapLabel.u.MplsShimLabel,
                MplsHwIlmInfo.MplsPushLblList[0].u.MplsShimLabel);

    /* Update P2MP Identifier */
    MEMCPY (&u4P2mpId, &(TE_TNL_EGRESS_LSRID (pTeTnlInfo)), ROUTER_ID_LENGTH);

#ifdef MBSM_WANTED
    if (pSlotInfo != NULL)
    {
        if (MplsFsMplsMbsmHwP2mpAddILM (u4P2mpId, &MplsHwIlmInfo, pSlotInfo)
            == FNP_FAILURE)
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                       "MplsILMAdd : FsMplsMbsmHwP2mpCreateILM failed \t\n");
            return MPLS_FAILURE;
        }
    }
    else
#endif
    {
#ifdef L2RED_WANTED
        if (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE)
#endif
        {
#ifdef NPAPI_WANTED
            if (MplsFsMplsHwP2mpAddILM (u4P2mpId, &MplsHwIlmInfo) ==
                FNP_FAILURE)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                           "MplsILMAdd : FsMplsHwP2mpAddILM failed \t\n");
                return MPLS_FAILURE;
            }
            INSEGMENT_HW_STATUS (pInSegment) = TRUE;
#endif
        }
    }
    OUTSEGMENT_HW_STATUS (pOutSegment) = MPLS_TRUE;

    CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                "P2MP ILM Add in HW for OutSegIndex %d succeeded\n",
                pOutSegment->u4Index);
    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsP2mpILMHwAdd - Exit \n");
    return MPLS_SUCCESS;
}

/************************************************************************
 *  Function Name   : MplsP2mpILMHwDel
 *  Description     : Function to delete ILM entry from NP
 *  Input           : InSegment and Slot Information 
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE
 ************************************************************************/
INT4
MplsP2mpILMHwDel (tOutSegment * pOutSegment, VOID *pSlotInfo)
{
    tXcEntry           *pXcEntry = NULL;
    tInSegment         *pInSegment = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tMplsHwIlmInfo      MplsHwDelIlmParams;
    tCfaIfInfo          IfInfo;
    UINT4               u4InL3If = MPLS_ZERO;
    UINT2               u2InVlanId = MPLS_ZERO;
    UINT4               u4P2mpId = MPLS_ZERO;

#ifndef MBSM_WANTED
    UNUSED_PARAM (pSlotInfo);
#endif

    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsP2mpILMHwDel Entry\n");

    MEMSET (&MplsHwDelIlmParams, MPLS_ZERO, sizeof (tMplsHwIlmInfo));

    if (pOutSegment == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsP2mpILMHwDel OutSegment not present \n");
        return MPLS_FAILURE;
    }

    pXcEntry = (tXcEntry *) (pOutSegment->pXcIndex);
    if (pXcEntry == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsP2mpILMHwDel XC not present \n");
        return MPLS_FAILURE;
    }

    pTeTnlInfo = XC_TNL_TBL_PTR (pXcEntry);
    if (pTeTnlInfo == NULL)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsP2mpILMHwDel tunnel entry not present \n");
        return MPLS_FAILURE;
    }

    pInSegment = pXcEntry->pInIndex;
    if (NULL == pInSegment)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "MplsP2mpILMHwDel InSeg entry not present \n");
        return MPLS_FAILURE;
    }

    if (OUTSEGMENT_HW_STATUS (pOutSegment) == MPLS_TRUE)
    {
        /* In Label */
        MplsHwDelIlmParams.InLabelList[MPLS_ZERO].u.MplsShimLabel =
            pInSegment->u4Label;
        MplsHwDelIlmParams.InLabelList[MPLS_ZERO].MplsLabelType =
            MPLS_HW_LABEL_TYPE_GENERIC;
        MplsHwDelIlmParams.u4InL3Intf = pInSegment->u4IfIndex;

        /* For LDP the Incomming L3 If is MPLS Tnl IF.For RSVP,it is IVR If */
        if (CfaGetIfInfo (MplsHwDelIlmParams.u4InL3Intf, &IfInfo) ==
            CFA_SUCCESS)
        {
            if (IfInfo.u1IfType == CFA_MPLS_TUNNEL)
            {
                /* Source Interface */
                if (CfaUtilGetIfIndexFromMplsTnlIf
                    ((UINT2) MplsHwDelIlmParams.u4InL3Intf, &u4InL3If, TRUE)
                    == CFA_SUCCESS)
                {
                    MplsHwDelIlmParams.u4InL3Intf = u4InL3If;
                }
            }
            if (CfaGetVlanId
                ((UINT2) MplsHwDelIlmParams.u4InL3Intf,
                 &u2InVlanId) != CFA_SUCCESS)
            {
                CMNDB_DBG (DEBUG_DEBUG_LEVEL, "CfaGetVlanId Failed\r\n");
                return MPLS_FAILURE;
            }
            MplsHwDelIlmParams.u4InVlanId = (UINT4) u2InVlanId;
        }
        else
        {
            CMNDB_DBG (DEBUG_DEBUG_LEVEL, "CfaGetIfType Failed\r\n");
            return MPLS_FAILURE;
        }
        MEMCPY (&u4P2mpId, TE_TNL_EGRESS_LSRID (pTeTnlInfo), ROUTER_ID_LENGTH);
        MplsHwDelIlmParams.MplsSwapLabel.u.MplsShimLabel = pOutSegment->u4Label;
        MplsHwDelIlmParams.u4OutL3Intf = pOutSegment->u4IfIndex;

        CMNDB_DBG4 (DEBUG_DEBUG_LEVEL,
                    "ILM Del in HW started with In Vlan Id : %d"
                    "In L3 Intf : %d In Label1 %d In Label2 %d\n",
                    MplsHwDelIlmParams.u4InVlanId,
                    MplsHwDelIlmParams.u4InL3Intf,
                    MplsHwDelIlmParams.InLabelList[MPLS_ZERO].u.MplsShimLabel,
                    MplsHwDelIlmParams.InLabelList[MPLS_ONE].u.MplsShimLabel);
        CMNDB_DBG4 (DEBUG_DEBUG_LEVEL,
                    "Out Vlan Id : %d Out L3 Intf : %d Out Label1 : %d "
                    "Out Label 2 : %d\n", MplsHwDelIlmParams.u4NewVlanId,
                    MplsHwDelIlmParams.u4OutL3Intf,
                    MplsHwDelIlmParams.MplsSwapLabel.u.MplsShimLabel,
                    MplsHwDelIlmParams.MplsPushLblList[MPLS_ZERO].u.
                    MplsShimLabel);

#ifdef L2RED_WANTED
        if ((MPLS_SUCCESS == MplsCheckP2mpTnlRoleNotEgress (pTeTnlInfo)) &&
            (MPLS_IS_NP_PROGRAMMING_ALLOWED () == MPLS_TRUE))
#endif
        {
#ifdef MBSM_WANTED
            if (pSlotInfo != NULL)
            {
                if (MplsFsMplsMbsmHwP2mpRemoveILM
                    (u4P2mpId, &MplsHwDelIlmParams, pSlotInfo) == FNP_FAILURE)
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               "MplsILMHwDel : FsMplsMbsmHwP2mpDeleteILM "
                               "failed\t\n");
                    return MPLS_FAILURE;
                }
            }
            else
#endif
            {
#ifdef NPAPI_WANTED
                if (MplsFsMplsHwP2mpRemoveILM (u4P2mpId, &MplsHwDelIlmParams)
                    == FNP_FAILURE)
                {
                    CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                               "FsMplsHwP2mpRemoveILM failed \t\n");
                    return MPLS_FAILURE;
                }
#endif
            }

        }
        OUTSEGMENT_HW_STATUS (pOutSegment) = MPLS_FALSE;
    }

    CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                "P2MP ILMDel in HW for OutSegIndex %d succeeded\n",
                pOutSegment->u4Index);
    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsP2mpILMHwDel - Exit \n");
    return MPLS_SUCCESS;
}

/******************************************************************************
 *  Function Name   : MplsCheckP2mpTnlRoleNotEgress
 *  Description     : Function to check whether the tunnel is P2MP and role is 
 *                    not EGRESS 
 *  Input           : pTeTnlInfo - Tunnel Info
 *  Output          : None
 *  Returns         : MPLS_SUCCESS or MPLS_FAILURE
 *****************************************************************************/
INT1
MplsCheckP2mpTnlRoleNotEgress (tTeTnlInfo * pTeTnlInfo)
{
    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsCheckP2mpTnlRoleNotEgress - Entry\n");
    if ((NULL != pTeTnlInfo) && (TE_TNL_TYPE_P2MP & TE_TNL_TYPE (pTeTnlInfo)) &&
        (TE_EGRESS != TE_TNL_ROLE (pTeTnlInfo)))
    {
        return MPLS_SUCCESS;
    }
    CMNDB_DBG (DEBUG_DEBUG_LEVEL, "MplsCheckP2mpTnlRoleNotEgress - Exit \n");
    return MPLS_FAILURE;
}

/*****************************************************************************/
/* Function     : MplsDbUpdateP2mpTunnelInHw                                 */
/*                                                                           */
/* Description  : This function updates the common database and the          */
/*                operational status of static p2mp tunnel.                  */
/*                                                                           */
/* Input        : pTeTnlInfo         - Pointer to Tunnel Information         */
/*                pXcEntry           - Pointer to XC entry                   */
/*                u1TnlOperStatus    - Operational status to be set on       */
/*                                     the tunnel                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MPLS_SUCCESS or MPLS_FAILURE                               */
/*****************************************************************************/
INT4
MplsDbUpdateP2mpTunnelInHw (tTeTnlInfo * pTeTnlInfo,
                            tXcEntry * pXcEntry, UINT1 u1TnlOperStatus)
{
    INT4                i4RetVal = MPLS_FAILURE;
    BOOL1               bCallOperStatus = FALSE;

    /* Set the action to be taken for the tunnel 
     * based on the role and Operational status to be set. */

    if (TE_TNL_ROLE (pTeTnlInfo) == TE_INGRESS)
    {
        if (u1TnlOperStatus == TE_OPER_UP)
        {
            if (XC_OPER_STATUS (pXcEntry) == XC_OPER_UP)
            {
                i4RetVal = MplsTnlNPAdd (pTeTnlInfo, pXcEntry, NULL);
            }
            else
            {
                i4RetVal = MPLS_SUCCESS;
            }
        }
        else
        {
            i4RetVal = MplsTnlNPDel (pTeTnlInfo, MPLSDB_MODULE);
        }
    }
    else if (TE_TNL_ROLE (pTeTnlInfo) == TE_INTERMEDIATE)
    {
        if (u1TnlOperStatus == TE_OPER_UP)
        {
            if (XC_OPER_STATUS (pXcEntry) == XC_OPER_UP)
            {
                i4RetVal = MplsP2mpILMHwAdd (pXcEntry->pOutIndex, NULL);
            }
            else
            {
                i4RetVal = MPLS_SUCCESS;
            }
        }
        else
        {
            i4RetVal = MplsP2mpILMHwDel (pXcEntry->pOutIndex, NULL);
        }
    }
    else
    {
        if (pXcEntry->pInIndex != NULL)
        {
            if (u1TnlOperStatus == TE_OPER_UP)
            {
                i4RetVal = MplsILMAdd (pXcEntry->pInIndex, pTeTnlInfo);
            }
            else
            {
                i4RetVal = MplsILMDel (pXcEntry->pInIndex);
            }
            TeCmnExtUpdateArpResolveStatus (pTeTnlInfo, MPLS_MLIB_ILM_CREATE,
                                            MPLS_DIRECTION_FORWARD,
                                            XC_ARP_STATUS (pXcEntry));
        }
    }

    if ((TE_TNL_ROLE (pTeTnlInfo) == TE_INGRESS) ||
        (TE_TNL_ROLE (pTeTnlInfo) == TE_INTERMEDIATE))
    {
        if ((i4RetVal == MPLS_FAILURE) &&
            (XC_ARP_STATUS (pXcEntry) != MPLS_ARP_RESOLVE_WAITING))
        {
            return MPLS_FAILURE;
        }
    }
    else
    {
        if (i4RetVal == MPLS_FAILURE)
        {
            return MPLS_FAILURE;
        }
    }

    /* If the operational status to be set on the tunnel is UP, 
     * action done should return ARP_STATUS as ARP_RESOLVED or ARP_UNKNOWN
     * for the tunnel to be made operationally UP.
     *
     * If the operational status to be set on the tunnel is DOWN,
     * tunnel can be made operationally down without any such validations
     * on action. */
    if (u1TnlOperStatus == TE_OPER_UP)
    {
        if ((XC_ARP_STATUS (pXcEntry) == MPLS_ARP_RESOLVED) ||
            (XC_ARP_STATUS (pXcEntry) == MPLS_ARP_RESOLVE_UNKNOWN))
        {
            bCallOperStatus = TRUE;
        }
        else
        {
            bCallOperStatus = FALSE;
        }
    }
    else
    {
        bCallOperStatus = TRUE;
    }

    /* In case of ingress and intermediate P2MP LSR, tunnel oper status 
     * should be invoked based on the number of operational out-segments. 
     * Tunnel should be made oper up when first out-segment and in-segment are 
     * operational. It should be made oper down when the last out-segment is 
     * not operational. For egress P2MP LSR, update tunnel oper status without 
     * checking the above conditions */
    if (bCallOperStatus == TRUE)
    {
        /* Update the count of operational P2MP branch out-segments and 
         * operational status for all destinations associated to this branch */
        if ((TE_TNL_ROLE (pTeTnlInfo) == TE_INGRESS)
            || (TE_TNL_ROLE (pTeTnlInfo) == TE_INTERMEDIATE))
        {
            MplsDbUpdateP2mpDestOperStatus (pTeTnlInfo, pXcEntry,
                                            u1TnlOperStatus);
        }

        /* Update tunnel operational status */
        if ((TE_TNL_ROLE (pTeTnlInfo) == TE_EGRESS)
            || ((u1TnlOperStatus == TE_OPER_UP)
                && (TE_P2MP_TNL_ACTIVE_BRANCH_COUNT (pTeTnlInfo) == MPLS_ONE)
                && (XC_OPER_STATUS (pXcEntry) == XC_OPER_UP))
            || ((u1TnlOperStatus == TE_OPER_DOWN)
                && (TE_P2MP_TNL_ACTIVE_BRANCH_COUNT (pTeTnlInfo) == MPLS_ZERO)))
        {
            if (TeCallUpdateTnlOperStatus (pTeTnlInfo, u1TnlOperStatus) ==
                TE_FAILURE)
            {
                return MPLS_FAILURE;
            }
        }
    }

    return MPLS_SUCCESS;
}

/*****************************************************************************/
/* Function     : MplsP2mpUpdateBudNode                                      */
/*                                                                           */
/* Description  : This function updates the bud node configuration for the   */
/*                p2mp tunnel.                                               */
/*                                                                           */
/* Input        : pTeTnlInfo         - Pointer to Tunnel Information         */
/*                u1Status           - Operational status to be set on       */
/*                                     the tunnel                            */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : MPLS_SUCCESS or MPLS_FAILURE                               */
/*****************************************************************************/
INT1
MplsP2mpUpdateBudNode (tTeTnlInfo * pTeTnlInfo, UINT1 u1Status)
{
    UINT4               u4Action = MPLS_HW_ACTION_COPY_L3_SWITCH;
    UINT4               u4P2mpId = MPLS_ZERO;
#ifdef NPAPI_WANTED
    INT4                i4RetVal = FNP_SUCCESS;
#endif
#ifndef NPAPI_WANTED
    UNUSED_PARAM (u1Status);
#endif

    /* For ingress LSR, action to be taken is copy the packet and 
     * do L3 switch. For transit LSR, action to be taken is copy 
     * the packet and pop the MPLS label to do L3 switch 
     * Porting team has to take care of this actions */

    if (TE_TNL_ROLE (pTeTnlInfo) == TE_INTERMEDIATE)
    {
        u4Action = MPLS_HW_ACTION_L3_SWITCH;
    }
    MEMCPY (&u4P2mpId, TE_TNL_EGRESS_LSRID (pTeTnlInfo), ROUTER_ID_LENGTH);

#ifdef NPAPI_WANTED
    if (u1Status == TE_OPER_UP)
    {
        i4RetVal = MplsFsMplsHwP2mpAddBud (u4P2mpId, u4Action);
    }
    else
    {
        i4RetVal = MplsFsMplsHwP2mpRemoveBud (u4P2mpId, u4Action);
    }
    if (FNP_FAILURE == i4RetVal)
    {
        return MPLS_FAILURE;
    }
#endif
    CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                "MplsP2mpUpdateBudNode: Action taken is %d\r\n", u4Action);
    return MPLS_SUCCESS;
}

/* MPLS_P2MP_LSP_CHANGES - E */

/************************************************************************
 * Function Name   : MplsDbUpdateXcOperStatus
 * Description     : This function updates the oper status of the
 *                   XC Entry based on the underlying interface status.
 *                   If the status of underlying interface is down, 
 *                   XC oper status is set to lower-layer down, else it 
 *                   is set to UP.
 * Input           : pXcEntry       - Pointer to XC Entry
 *                   bCheckLlStatus - Indicates a boolean variable whether 
 *                                    to verify lower layer interface status
 *                                    or not.
 *                   u1OperStatus   - Oper status of the interface.
 *                                    This variable is applicable only if
 *                                    bCheckLlStatus is set to FALSE. Acceptable
 *                                    values for this variable are CFA_IF_UP and
 *                                    CFA_IF_DOWN.
 *                   u1Value        - Value to be updated in XC Oper
 *                                    status. This variable is applicable
 *                                    only if bCheckLlStatus is set to FALSE.
 * Output          : None
 * Returns         : None
 ************************************************************************/
VOID
MplsDbUpdateXcOperStatus (tXcEntry * pXcEntry, BOOL1 bCheckLlStatus,
                          UINT1 u1OperStatus, UINT1 u1Value)
{
    tCfaIfInfo          CfaIfInfo;

    MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

    if (bCheckLlStatus == FALSE)
    {
        if (u1OperStatus == CFA_IF_DOWN)
        {
            /* Set the variable passed (insegment or outsegment) as fault */
            pXcEntry->u1SegStatus |= u1Value;
        }
        else
        {
            /* Set the variable passed (insegment or outsegment) as not fault */
            pXcEntry->u1SegStatus &= ~(u1Value);
        }
    }
    else
    {
        if (((u1Value & XC_IN_SEGMENT) == XC_IN_SEGMENT) &&
            (pXcEntry->pInIndex != NULL))
        {
            if ((pXcEntry->pInIndex->u1RowStatus == ACTIVE) &&
                (CfaGetIfInfo (pXcEntry->pInIndex->u4IfIndex, &CfaIfInfo)
                 == CFA_SUCCESS) && (CfaIfInfo.u1IfOperStatus == CFA_IF_UP))
            {
                pXcEntry->u1SegStatus &= ~(XC_INSEGMENT_FAULT);
                CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                            "IN Interface %d is present and UP\r\n",
                            pXcEntry->pInIndex->u4IfIndex);
            }
            else
            {
                pXcEntry->u1SegStatus |= XC_INSEGMENT_FAULT;
            }
        }

        if (((u1Value & XC_OUT_SEGMENT) == XC_OUT_SEGMENT) &&
            (pXcEntry->pOutIndex != NULL))
        {
            MEMSET (&CfaIfInfo, 0, sizeof (tCfaIfInfo));

            if ((pXcEntry->pOutIndex->u1RowStatus == ACTIVE) &&
                (CfaGetIfInfo (pXcEntry->pOutIndex->u4IfIndex, &CfaIfInfo)
                 == CFA_SUCCESS) && (CfaIfInfo.u1IfOperStatus == CFA_IF_UP))
            {
                pXcEntry->u1SegStatus &= ~(XC_OUTSEGMENT_FAULT);
                CMNDB_DBG1 (DEBUG_DEBUG_LEVEL,
                            "OUT Interface %d is present and UP\r\n",
                            pXcEntry->pOutIndex->u4IfIndex);
            }
            else
            {
                pXcEntry->u1SegStatus |= XC_OUTSEGMENT_FAULT;
            }
        }
    }

    if (pXcEntry->u1SegStatus == XC_NO_FAULT)
    {
        XC_OPER_STATUS (pXcEntry) = XC_OPER_UP;
    }
    else
    {
        XC_OPER_STATUS (pXcEntry) = XC_LOWLAYER_DOWN;
    }

    return;
}

/*****************************************************************************/
/* Function     : MplsDbUpdateP2mpDestOperStatus                             */
/*                                                                           */
/* Description  : This function updates the operational status of P2MP       */
/*                destinations based on interface status. It also updates    */
/*                the count of operational P2MP branch out-segments.         */
/*                                                                           */
/* Input        : pTeTnlInfo         - Pointer to Tunnel Information         */
/*                pXcEntry           - Pointer to XC entry                   */
/*                u1OperStatus       - Operational status to be set on       */
/*                                     the tunnel                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
MplsDbUpdateP2mpDestOperStatus (tTeTnlInfo * pTeTnlInfo, tXcEntry * pXcEntry,
                                UINT1 u1OperStatus)
{
    tP2mpDestEntry     *pTeP2mpDestEntry = NULL;
    UINT4               u4OutSegIndex = MPLS_ZERO;

    if ((NULL == TE_P2MP_TNL_INFO (pTeTnlInfo))
        || (NULL == XC_OUTINDEX (pXcEntry)))
    {
        return;
    }

    /* Update the count of operational P2MP branch out-segments. Increment the
     * active P2MP branch count based on XC oper status. Decrement the active
     * P2MP branch count, until the tunnel is operationally up. */

    /* When incoming IF is down, HW configuration for each operational P2MP
     * branch out-segment is removed and active P2MP branch count is 
     * decremented. Tunnel is made oper down when active P2MP branch count is
     * zero. Subsequently when outgoing IF goes down, active P2MP branch count
     * should not be decremented, since it was already decremented when the
     * incoming IF went down */
    if ((u1OperStatus == TE_OPER_UP)
        && (XC_OPER_STATUS (pXcEntry) == XC_OPER_UP))
    {
        TE_P2MP_TNL_ACTIVE_BRANCH_COUNT (pTeTnlInfo) += MPLS_ONE;
    }
    else if ((u1OperStatus == TE_OPER_DOWN)
             && (TE_TNL_OPER_STATUS (pTeTnlInfo) == TE_OPER_UP))
    {
        TE_P2MP_TNL_ACTIVE_BRANCH_COUNT (pTeTnlInfo) -= MPLS_ONE;
    }

    /* Update operational status for all destinations associated to this 
     * branch out-segment */
    u4OutSegIndex = OUTSEGMENT_INDEX (XC_OUTINDEX (pXcEntry));
    TMO_SLL_Scan (&(TE_P2MP_TNL_DEST_LIST_INFO (pTeTnlInfo)), pTeP2mpDestEntry,
                  tP2mpDestEntry *)
    {
        if ((u4OutSegIndex ==
             TE_P2MP_DEST_BRANCH_OUTSEG_INDEX (pTeP2mpDestEntry))
            && (MPLS_ADMIN_STATUS_UP ==
                TE_P2MP_DEST_ADMIN_STATUS (pTeP2mpDestEntry)))
        {
            TE_P2MP_DEST_OPER_STATUS (pTeP2mpDestEntry) =
                (XC_OPER_STATUS (pXcEntry) ==
                 XC_OPER_UP) ? TE_OPER_UP : TE_OPER_DOWN;
        }
    }
    return;
}

/*****************************************************************************/
/* Function     : MplsDbInSegmentSignalRbTreeAdd                             */
/* Description  : This function adds the given Insegment entry to the Signal */
/*                RBTree.                                                    */
/* Input        : pInSegment         - Pointer to the Insegment entry        */
/* Output       : None                                                       */
/* Returns      : MPLS_SUCCESS/MPLS_FAILURE                                  */
/*****************************************************************************/
INT4
MplsDbInSegmentSignalRbTreeAdd (VOID *pInSegment)
{
    if (MplsInSegmentSignalRbTreeAdd
        ((tInSegment *) pInSegment) == MPLS_FAILURE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "TeDeleteILMInHw : InSegment Signal addition "
                   "in RBTree failed.\n");
        return MPLS_FAILURE;
    }

    return MPLS_SUCCESS;
}

/*****************************************************************************/
/* Function     : MplsDbInSegmentSignalRbTreeDelete                          */
/* Description  : This function deletes the given Insegment entry in the     */
/*                Signal RBTree.                                             */
/* Input        : pInSegment         - Pointer to the Insegment entry        */
/* Output       : None                                                       */
/* Returns      : MPLS_SUCCESS/MPLS_FAILURE                                  */
/*****************************************************************************/
INT4
MplsDbInSegmentSignalRbTreeDelete (VOID *pInSegment)
{
    if (MplsInSegmentSignalRbTreeDelete
        ((tInSegment *) pInSegment) == MPLS_FAILURE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL,
                   "TeDeleteILMInHw : InSegment Signal deletion "
                   "in RBTree failed.\n");
        return MPLS_FAILURE;
    }

    return MPLS_SUCCESS;
}

/*****************************************************************************/
/* Function     : MplsGetIfUnnumPeerMac                                      */
/* Description  : This function gets peer mac address of unnumbered I/F      */
/* Input        : u4IfIndex         - I/F Index                              */
/* Output       : pu1MacAddr        - Peer mac address                       */
/* Returns      : NONE                                                       */
/*****************************************************************************/

VOID
MplsGetIfUnnumPeerMac (UINT4 u4IfIndex, UINT1 *pu1MacAddr)
{
    UINT4               u4L3IntIf = MPLS_ZERO;
    tCfaIfInfo          IfInfo;
    MEMSET (&IfInfo, MPLS_ZERO, sizeof (tCfaIfInfo));
    if (MplsGetL3Intf (u4IfIndex, &u4L3IntIf) == MPLS_FAILURE)
    {
        return;
    }
    if (CfaGetIfInfo (u4L3IntIf, &IfInfo) == CFA_FAILURE)
    {
        return;
    }
    MEMCPY (pu1MacAddr, IfInfo.au1PeerMacAddr, CFA_ENET_ADDR_LEN);
    return;
}

/*****************************************************************************/
/* Function     : MplsDbFillQosInfo                                          */
/* Description  : This function fills the Qos Info                           */
/* Input        : pTeTnlInfo         - pointer to tunnel info                */
/*                pQosInfo           - pointer to Qos info                   */
/* Output       : pu1MacAddr         - NONE                                  */
/* Returns      : NONE                                                       */
/*****************************************************************************/

VOID
MplsFillQosInfo (tTeTnlInfo * pTeTnlInfo, tQosInfo * pQosInfo)
{
    UINT4               u4Count = MPLS_ZERO;
    tMplsDiffServElspInfo *pElspInfo = NULL;
    tElspMapRow        *pElspMap = NULL;

    pQosInfo[0].QosType = MPLS_HW_QOS_POLICY_TYPE;

    if (TE_DS_TNL_SERVICE_TYPE (TE_DS_TNL_INFO (pTeTnlInfo)) == TE_DS_ELSP)
    {
        if (TE_DS_TNL_ELSP_TYPE (TE_DS_TNL_INFO (pTeTnlInfo)) == TE_DS_SIG_ELSP)
        {
            u4Count = MPLS_ZERO;
            TMO_SLL_Scan (&(pTeTnlInfo->pMplsDiffServTnlInfo->
                            pMplsDiffServElspList)->ElspInfoList,
                          pElspInfo, tMplsDiffServElspInfo *)
            {
                pQosInfo[u4Count].u.MplsQosPolicy.
                    u4LabelPri = pElspInfo->u1ElspInfoIndex;
                u4Count++;
            }

        }
        else                    /* PreConfigured Elsp */
        {
            if (MPLS_PRECONF_ELSP_MAP (MPLS_DEF_INCARN) != NULL)
            {
                /* Get Default ElspMap Entry */
                pElspMap = MPLS_DIFFSERV_ELSP_MAP_ENTRY
                    (MPLS_DEF_INCARN,
                     MPLS_PRECONF_ELSP_MAP (MPLS_DEF_INCARN)->
                     u4ElspExpPhbMapIndex);
                /* Get the default exp for default PHB (0) */
                for (u4Count = MPLS_ZERO; u4Count < MPLS_DIFFSERV_MAX_EXP;
                     u4Count++)
                {
                    if ((pElspMap->aElspExpPhbMapArray[u4Count].
                         u1ElspPhbDscp == MPLS_ZERO) &&
                        (pElspMap->aElspExpPhbMapArray[u4Count].
                         u1IsExpValid == MPLS_TRUE) &&
                        (pElspMap->aElspExpPhbMapArray[u4Count].
                         u1RowStatus == MPLS_STATUS_ACTIVE))
                    {
                        pQosInfo[u4Count].u.MplsQosPolicy.u4LabelPri = u4Count;
                        break;
                    }
                }
            }
        }
    }
    else                        /* Llsp case */
    {
        pQosInfo[0].u.MplsQosPolicy.u4LabelPri =
            TE_DS_TNL_LLSP_PSC_DSCP (TE_DS_TNL_INFO (pTeTnlInfo));
    }
    return;
}

/*****************************************************************************/
/* Function     : MplsGetDnStrInfoUsingRecoveryLbl                           */
/* Description  : This function gets down stream label info using recovery   */
/*                label                                                      */
/* Input        : u4RecoveryLabel    - Recovery label                        */
/* Output       : pu4OutLabel        - downstream out label                  */
/*                pTeTnlInfo         - pointer to tunnel info                */
/* Returns      : NONE                                                       */
/*****************************************************************************/

VOID
MplsGetDnStrInfoUsingRecoveryLbl (UINT4 u4RecoveryLabel,
                                  tTeTnlInfo ** pTeTnlInfo, UINT4 *pu4OutLabel)
{
    tInSegment         *pInSegment = NULL;
    tXcEntry           *pXcEntry = NULL;

    MPLS_CMN_LOCK ();
    pInSegment = MplsSignalGetInSegmentTableEntry (MPLS_ZERO, u4RecoveryLabel);

    if (pInSegment != NULL)
    {
        pXcEntry = pInSegment->pXcIndex;
        if (pXcEntry != NULL)
        {
            *pTeTnlInfo = pXcEntry->pTeTnlInfo;
            if (pXcEntry->pOutIndex != NULL)
            {
                *pu4OutLabel = pXcEntry->pOutIndex->u4Label;
            }
        }
    }
    MPLS_CMN_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function     : MplsGetDnStrInfoUsingRecoveryPath                          */
/* Description  : This function gets down stream label info using recovery   */
/*                path message info                                          */
/* Input        : u4RecoveryLabel    - Recovery label                        */
/*                u4NextHop          - NextHop Address                       */
/* Output       : pu4InLabel         - downstream in label                   */
/*                pTeTnlInfo         - pointer to tunnel info                */
/* Returns      : NONE                                                       */
/*****************************************************************************/

VOID
MplsGetDnStrInfoUsingRecoveryPath (UINT4 u4RecoveryLabel, UINT4 u4NextHop,
                                   tTeTnlInfo ** pTeTnlInfo, UINT4 *pu4InLabel)
{
    tOutSegment        *pOutSegment = NULL;
    tXcEntry           *pXcEntry = NULL;

    MPLS_CMN_LOCK ();
    pOutSegment =
        MplsSignalGetOutSegmentTableEntry (u4NextHop, u4RecoveryLabel);

    if (pOutSegment != NULL)
    {
        pXcEntry = pOutSegment->pXcIndex;
        if (pXcEntry != NULL)
        {
            *pTeTnlInfo = pXcEntry->pTeTnlInfo;
            if (pXcEntry->pInIndex != NULL)
            {
                *pu4InLabel = pXcEntry->pInIndex->u4Label;
            }
        }
    }
    MPLS_CMN_UNLOCK ();
    return;
}

#if defined(VPLSADS_WANTED) || defined(MPLS_L3VPN_WANTED)
/*****************************************************************************/
/* Function     : MplsUpdateASN                                              */
/* Description  : This function updates ASN value and type in local mpls DB. */
/* Input        : u1AsnType    - ASN Type                                    */
/*                u4AsnValue   - ASN Value                                   */
/* Output       : NONE                                                       */
/* Returns      : NONE                                                       */
/*****************************************************************************/

VOID
MplsUpdateASN (UINT1 u1AsnType, UINT4 u4AsnValue)
{
    if (1 == u1AsnType)
    {
        gu1BgpAsnType = 2;
    }
    else
    {
        gu1BgpAsnType = 0;
    }
    gu4BgpAsnValue = u4AsnValue;
}
#endif
#ifdef VPLSADS_WANTED
/*****************************************************************************/
/* Function     : MplsGetASN                                                 */
/* Description  : This function returns ASN value and type stored in         */
/*                local mpls DB.                                             */
/* Input        : NONE                                                       */
/* Output       : pu1AsnType    - ASN Type                                   */
/*                pu4AsnValue   - ASN Value                                  */
/* Returns      : NONE                                                       */
/*****************************************************************************/

VOID
MplsGetASN (UINT1 *pu1AsnType, UINT4 *pu4AsnValue)
{
    *pu4AsnValue = gu4BgpAsnValue;
    *pu1AsnType = gu1BgpAsnType;
}

/*****************************************************************************/
/* Function     : MplsGetBgpAdminState                                       */
/* Description  : This function returns bgp admin status from local mpls DB. */
/* Input        : NONE                                                       */
/* Output       : pu4BgpAdminState    - BGP Admin state                      */
/* Returns      : NONE                                                       */
/*****************************************************************************/

VOID
MplsGetBgpAdminState (UINT4 *pu4BgpAdminState)
{
    *pu4BgpAdminState = gu4BgpAdminState;
}

/*****************************************************************************/
/* Function     : MplsSetBgpAdminState                                       */
/* Description  : This function sets bgp admin status in local mpls DB.      */
/* Input        : NONE                                                       */
/* Output       : pu4BgpAdminState    - BGP Admin state                      */
/* Returns      : NONE                                                       */
/*****************************************************************************/

VOID
MplsSetBgpAdminState (UINT4 u4BgpAdminState)
{
    gu4BgpAdminState = u4BgpAdminState;
}
#endif
#if defined(VPLSADS_WANTED) || defined(MPLS_L3VPN_WANTED)
/*****************************************************************************/
/* Function     : MplsValidateRdRtWithAsn                                    */
/* Description  : This function validates RD/RT value with the ASN value     */
/*                stored in local mpls DB.                                   */
/* Input        : pu1RdRt - RD/RT value to validate                          */
/* Output       : NONE                                                       */
/* Returns      : MPLS_SUCCESS/MPLS_FAILURE                                  */
/*****************************************************************************/

INT4
MplsValidateRdRtWithAsn (UINT1 *pu1RdRt)
{
    UINT4               u4AsnValue;
    UINT2               u2AsnValue;

#ifdef VPLSADS_WANTED
    if (L2VPN_BGP_ADMIN_DOWN == gu4BgpAdminState)
    {
        return MPLS_FAILURE;
    }
#endif

    if (0 == pu1RdRt[0])
    {
        if (0 != gu1BgpAsnType)
        {
            return MPLS_FAILURE;
        }
        MEMCPY (&u2AsnValue, &pu1RdRt[2], sizeof (UINT2));
        u2AsnValue = OSIX_NTOHS (u2AsnValue);
        u4AsnValue = u2AsnValue;
    }
    else if (2 == pu1RdRt[0])
    {
        if (2 != gu1BgpAsnType)
        {
            return MPLS_FAILURE;
        }
        MEMCPY (&u4AsnValue, &pu1RdRt[2], sizeof (UINT4));
        u4AsnValue = OSIX_NTOHL (u4AsnValue);
    }
    else
    {
        return MPLS_SUCCESS;
    }

    if (u4AsnValue != gu4BgpAsnValue)
    {
        return MPLS_FAILURE;
    }

    return MPLS_SUCCESS;
}
#endif

#ifdef MPLS_IPV6_WANTED
/******************************************************************************
 * Function Name : MplsValidateIPv6Addr
 * Description   : This routine is used to Validate IPv6 Addresses
 * Input(s)      : pu4ErrorCode - Error Code in case of Failure
 *                 pTestValIpv6Addr - Ipv6 Address
 * Output(s)     : None
 * Return(s)     : SNMP_SUCCESS/SNMP_FAILURE
 *******************************************************************************/
UINT1
MplsValidateIPv6Addr (UINT4 *pu4ErrorCode,
                      tSNMP_OCTET_STRING_TYPE * pTestValIpv6Addr)
{
    UINT1               u1Scope = 0;
    UINT1               u1ScopeFound = 0;
    INT4                i4CmpRet = 0;

    tIp6Addr            ValIpv6Addr;
    tIp6Addr            Ipv6CmpAddr;
    MEMSET (&ValIpv6Addr, 0, sizeof (tIp6Addr));
    MEMSET (&Ipv6CmpAddr, 0, sizeof (tIp6Addr));

    CMNDB_DBG (DEBUG_DEBUG_LEVEL, " Entered MplsValidateIPv6Addr \t\n");

    MEMCPY (&ValIpv6Addr.u1_addr,
            pTestValIpv6Addr->pu1_OctetList, IPV6_ADDR_LENGTH);

    if (pTestValIpv6Addr->i4_Length != IPV6_ADDR_LENGTH)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Mpls ValidateIPv6Addr Failed \t\n");
        return SNMP_FAILURE;
    }

    i4CmpRet =
        MEMCMP (ValIpv6Addr.u1_addr, Ipv6CmpAddr.u1_addr, IPV6_ADDR_LENGTH);

    if (i4CmpRet == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Mpls ValidateIPv6Addr Failed \t\n");
        return SNMP_FAILURE;
    }

    u1Scope = Ip6GetAddrScope (&ValIpv6Addr);

    if (u1Scope == ADDR6_SCOPE_LLOCAL)
    {
        u1ScopeFound++;
    }
    if (u1Scope == ADDR6_SCOPE_SITELOCAL)
    {
        u1ScopeFound++;
    }
    if (u1Scope == ADDR6_SCOPE_GLOBAL)
    {
        u1ScopeFound++;
    }

    if (u1ScopeFound == 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Mpls ValidateIPv6Addr Failed \t\n");
        return SNMP_FAILURE;
    }
    if ((IS_ADDR_MULTI (ValIpv6Addr)) || (IS_ADDR_LOOPBACK (ValIpv6Addr)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "Mpls ValidateIPv6Addr Failed \t\n");
        return SNMP_FAILURE;
    }
    CMNDB_DBG (DEBUG_DEBUG_LEVEL, " Exiting Mpls ValidateIPv6Addr \t\n");
    return SNMP_SUCCESS;
}

#endif
