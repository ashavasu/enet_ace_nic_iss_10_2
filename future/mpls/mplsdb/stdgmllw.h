/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdgmllw.h,v 1.1 2011/11/30 09:58:13 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for GmplsInterfaceTable. */
INT1
nmhValidateIndexInstanceGmplsInterfaceTable ARG_LIST((INT4 ));

/* Proto Type for Low Level GET FIRST fn for GmplsInterfaceTable  */

INT1
nmhGetFirstIndexGmplsInterfaceTable ARG_LIST((INT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexGmplsInterfaceTable ARG_LIST((INT4 , INT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetGmplsInterfaceSignalingCaps ARG_LIST((INT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetGmplsInterfaceRsvpHelloPeriod ARG_LIST((INT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetGmplsInterfaceSignalingCaps ARG_LIST((INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetGmplsInterfaceRsvpHelloPeriod ARG_LIST((INT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2GmplsInterfaceSignalingCaps ARG_LIST((UINT4 *  ,INT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2GmplsInterfaceRsvpHelloPeriod ARG_LIST((UINT4 *  ,INT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2GmplsInterfaceTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for GmplsInSegmentTable. */
INT1
nmhValidateIndexInstanceGmplsInSegmentTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for GmplsInSegmentTable  */

INT1
nmhGetFirstIndexGmplsInSegmentTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexGmplsInSegmentTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetGmplsInSegmentDirection ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetGmplsInSegmentExtraParamsPtr ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OID_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetGmplsInSegmentDirection ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetGmplsInSegmentExtraParamsPtr ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OID_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2GmplsInSegmentDirection ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2GmplsInSegmentExtraParamsPtr ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OID_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2GmplsInSegmentTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for GmplsOutSegmentTable. */
INT1
nmhValidateIndexInstanceGmplsOutSegmentTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for GmplsOutSegmentTable  */

INT1
nmhGetFirstIndexGmplsOutSegmentTable ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexGmplsOutSegmentTable ARG_LIST((tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetGmplsOutSegmentDirection ARG_LIST((tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetGmplsOutSegmentTTLDecrement ARG_LIST((tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetGmplsOutSegmentExtraParamsPtr ARG_LIST((tSNMP_OCTET_STRING_TYPE *,tSNMP_OID_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetGmplsOutSegmentDirection ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetGmplsOutSegmentTTLDecrement ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhSetGmplsOutSegmentExtraParamsPtr ARG_LIST((tSNMP_OCTET_STRING_TYPE * ,tSNMP_OID_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2GmplsOutSegmentDirection ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2GmplsOutSegmentTTLDecrement ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhTestv2GmplsOutSegmentExtraParamsPtr ARG_LIST((UINT4 *  ,tSNMP_OCTET_STRING_TYPE * ,tSNMP_OID_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2GmplsOutSegmentTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
