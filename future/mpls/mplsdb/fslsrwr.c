/* $Id: fslsrwr.c,v 1.4 2013/07/26 13:31:11 siva Exp $ */
# include  "lr.h"
# include  "fssnmp.h"
# include  "fslsrlw.h"
# include  "fslsrwr.h"
# include  "fslsrdb.h"

INT4
GetNextIndexFsMplsInSegmentTable (tSnmpIndex * pFirstMultiIndex,
                                  tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMplsInSegmentTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMplsInSegmentTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

VOID
RegisterFSLSR ()
{
    SNMPRegisterMib (&fslsrOID, &fslsrEntry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fslsrOID, (const UINT1 *) "fslsr");
}

VOID
UnRegisterFSLSR ()
{
    SNMPUnRegisterMib (&fslsrOID, &fslsrEntry);
    SNMPDelSysorEntry (&fslsrOID, (const UINT1 *) "fslsr");
}

INT4
FsMplsInSegmentDirectionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsInSegmentTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsInSegmentDirection
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMplsInSegmentDirectionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMplsInSegmentDirection
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsMplsInSegmentDirectionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                              tRetVal * pMultiData)
{
    return (nmhTestv2FsMplsInSegmentDirection (pu4Error,
                                               pMultiIndex->pIndex[0].
                                               pOctetStrValue,
                                               pMultiData->i4_SLongValue));

}

INT4
FsMplsInSegmentTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsInSegmentTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexFsMplsOutSegmentTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexFsMplsOutSegmentTable
            (pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexFsMplsOutSegmentTable
            (pFirstMultiIndex->pIndex[0].pOctetStrValue,
             pNextMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
FsMplsOutSegmentDirectionGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceFsMplsOutSegmentTable
        (pMultiIndex->pIndex[0].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetFsMplsOutSegmentDirection
            (pMultiIndex->pIndex[0].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
FsMplsOutSegmentDirectionSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetFsMplsOutSegmentDirection
            (pMultiIndex->pIndex[0].pOctetStrValue, pMultiData->i4_SLongValue));

}

INT4
FsMplsOutSegmentDirectionTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2FsMplsOutSegmentDirection (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                pOctetStrValue,
                                                pMultiData->i4_SLongValue));

}

INT4
FsMplsOutSegmentTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsOutSegmentTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsMplsLsrRfc6428CompatibleCodePointGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsMplsLsrRfc6428CompatibleCodePoint
            (&(pMultiData->i4_SLongValue)));
}

INT4
FsMplsLsrRfc6428CompatibleCodePointSet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsMplsLsrRfc6428CompatibleCodePoint
            (pMultiData->i4_SLongValue));
}

INT4
FsMplsLsrRfc6428CompatibleCodePointTest (UINT4 *pu4Error,
                                         tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsMplsLsrRfc6428CompatibleCodePoint
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsMplsLsrRfc6428CompatibleCodePointDep (UINT4 *pu4Error,
                                        tSnmpIndexList * pSnmpIndexList,
                                        tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsMplsLsrRfc6428CompatibleCodePoint
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
