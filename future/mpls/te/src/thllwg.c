/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: thllwg.c,v 1.7 2013/06/26 11:42:04 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include "teincs.h"
#include "fsmplslw.h"
#include "stdtelw.h"
#include "mplslsr.h"
#include "fsmpfrlw.h"
/* LOW LEVEL Routines for Table : FsMplsLSPMapTunnelTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsLSPMapTunnelTable
 Input       :  The Indices
                FsMplsLSPMapTunnelIndex
                FsMplsLSPMapTunnelInstance
                FsMplsLSPMapTunnelIngressLSRId
                FsMplsLSPMapTunnelEgressLSRId
                FsMplsLSPMapSubTunnelIndex
                FsMplsLSPMapSubTunnelInstance
                FsMplsLSPMapSubTunnelIngressLSRId
                FsMplsLSPMapSubTunnelEgressLSRId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceFsMplsLSPMapTunnelTable (UINT4
                                                 u4FsMplsLSPMapTunnelIndex,
                                                 UINT4
                                                 u4FsMplsLSPMapTunnelInstance,
                                                 UINT4
                                                 u4FsMplsLSPMapTunnelIngressLSRId,
                                                 UINT4
                                                 u4FsMplsLSPMapTunnelEgressLSRId,
                                                 UINT4
                                                 u4FsMplsLSPMapSubTunnelIndex,
                                                 UINT4
                                                 u4FsMplsLSPMapSubTunnelInstance,
                                                 UINT4
                                                 u4FsMplsLSPMapSubTunnelIngressLSRId,
                                                 UINT4
                                                 u4FsMplsLSPMapSubTunnelEgressLSRId)
{
    if (nmhValidateIndexInstanceMplsTunnelTable (u4FsMplsLSPMapTunnelIndex,
                                                 u4FsMplsLSPMapTunnelInstance,
                                                 u4FsMplsLSPMapTunnelIngressLSRId,
                                                 u4FsMplsLSPMapTunnelEgressLSRId)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (nmhValidateIndexInstanceMplsTunnelTable (u4FsMplsLSPMapSubTunnelIndex,
                                                 u4FsMplsLSPMapSubTunnelInstance,
                                                 u4FsMplsLSPMapSubTunnelIngressLSRId,
                                                 u4FsMplsLSPMapSubTunnelEgressLSRId)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsLSPMapTunnelTable
 Input       :  The Indices
                FsMplsLSPMapTunnelIndex
                FsMplsLSPMapTunnelInstance
                FsMplsLSPMapTunnelIngressLSRId
                FsMplsLSPMapTunnelEgressLSRId
                FsMplsLSPMapSubTunnelIndex
                FsMplsLSPMapSubTunnelInstance
                FsMplsLSPMapSubTunnelIngressLSRId
                FsMplsLSPMapSubTunnelEgressLSRId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMplsLSPMapTunnelTable (UINT4 *pu4FsMplsLSPMapTunnelIndex,
                                         UINT4 *pu4FsMplsLSPMapTunnelInstance,
                                         UINT4
                                         *pu4FsMplsLSPMapTunnelIngressLSRId,
                                         UINT4
                                         *pu4FsMplsLSPMapTunnelEgressLSRId,
                                         UINT4 *pu4FsMplsLSPMapSubTunnelIndex,
                                         UINT4
                                         *pu4FsMplsLSPMapSubTunnelInstance,
                                         UINT4
                                         *pu4FsMplsLSPMapSubTunnelIngressLSRId,
                                         UINT4
                                         *pu4FsMplsLSPMapSubTunnelEgressLSRId)
{
    tteFsMplsLSPMapTunnelTable *pteFsMplsLSPMapTunnelTable = NULL;

    pteFsMplsLSPMapTunnelTable = (tteFsMplsLSPMapTunnelTable *)
        RBTreeGetFirst (gTeLSPMapTnlGblInfo.pLSPMapTnlRBTree);

    if (pteFsMplsLSPMapTunnelTable == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pu4FsMplsLSPMapTunnelIndex =
        pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelIndex;

    *pu4FsMplsLSPMapTunnelInstance =
        pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelInstance;

    *pu4FsMplsLSPMapTunnelIngressLSRId =
        pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelIngressLSRId;

    *pu4FsMplsLSPMapTunnelEgressLSRId =
        pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelEgressLSRId;

    *pu4FsMplsLSPMapSubTunnelIndex =
        pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelIndex;

    *pu4FsMplsLSPMapSubTunnelInstance =
        pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelInstance;

    *pu4FsMplsLSPMapSubTunnelIngressLSRId =
        pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelIngressLSRId;

    *pu4FsMplsLSPMapSubTunnelEgressLSRId =
        pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelEgressLSRId;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMplsLSPMapTunnelTable
 Input       :  The Indices
                FsMplsLSPMapTunnelIndex
                nextFsMplsLSPMapTunnelIndex
                FsMplsLSPMapTunnelInstance
                nextFsMplsLSPMapTunnelInstance
                FsMplsLSPMapTunnelIngressLSRId
                nextFsMplsLSPMapTunnelIngressLSRId
                FsMplsLSPMapTunnelEgressLSRId
                nextFsMplsLSPMapTunnelEgressLSRId
                FsMplsLSPMapSubTunnelIndex
                nextFsMplsLSPMapSubTunnelIndex
                FsMplsLSPMapSubTunnelInstance
                nextFsMplsLSPMapSubTunnelInstance
                FsMplsLSPMapSubTunnelIngressLSRId
                nextFsMplsLSPMapSubTunnelIngressLSRId
                FsMplsLSPMapSubTunnelEgressLSRId
                nextFsMplsLSPMapSubTunnelEgressLSRId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMplsLSPMapTunnelTable (UINT4 u4FsMplsLSPMapTunnelIndex,
                                        UINT4 *pu4NextFsMplsLSPMapTunnelIndex,
                                        UINT4 u4FsMplsLSPMapTunnelInstance,
                                        UINT4
                                        *pu4NextFsMplsLSPMapTunnelInstance,
                                        UINT4 u4FsMplsLSPMapTunnelIngressLSRId,
                                        UINT4
                                        *pu4NextFsMplsLSPMapTunnelIngressLSRId,
                                        UINT4 u4FsMplsLSPMapTunnelEgressLSRId,
                                        UINT4
                                        *pu4NextFsMplsLSPMapTunnelEgressLSRId,
                                        UINT4 u4FsMplsLSPMapSubTunnelIndex,
                                        UINT4
                                        *pu4NextFsMplsLSPMapSubTunnelIndex,
                                        UINT4 u4FsMplsLSPMapSubTunnelInstance,
                                        UINT4
                                        *pu4NextFsMplsLSPMapSubTunnelInstance,
                                        UINT4
                                        u4FsMplsLSPMapSubTunnelIngressLSRId,
                                        UINT4
                                        *pu4NextFsMplsLSPMapSubTunnelIngressLSRId,
                                        UINT4
                                        u4FsMplsLSPMapSubTunnelEgressLSRId,
                                        UINT4
                                        *pu4NextFsMplsLSPMapSubTunnelEgressLSRId)
{
    tteFsMplsLSPMapTunnelTable teFsMplsLSPMapTunnelTable;
    tteFsMplsLSPMapTunnelTable *pNextteFsMplsLSPMapTunnelTable = NULL;

    MEMSET (&teFsMplsLSPMapTunnelTable, 0, sizeof (tteFsMplsLSPMapTunnelTable));

    /* Assign the index */

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelIndex =
        u4FsMplsLSPMapTunnelIndex;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelInstance =
        u4FsMplsLSPMapTunnelInstance;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelIngressLSRId =
        u4FsMplsLSPMapTunnelIngressLSRId;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelEgressLSRId =
        u4FsMplsLSPMapTunnelEgressLSRId;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelIndex =
        u4FsMplsLSPMapSubTunnelIndex;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelInstance =
        u4FsMplsLSPMapSubTunnelInstance;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelIngressLSRId =
        u4FsMplsLSPMapSubTunnelIngressLSRId;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelEgressLSRId =
        u4FsMplsLSPMapSubTunnelEgressLSRId;

    pNextteFsMplsLSPMapTunnelTable =
        (tteFsMplsLSPMapTunnelTable *) RBTreeGetNext (gTeLSPMapTnlGblInfo.
                                                      pLSPMapTnlRBTree,
                                                      (tRBElem *) &
                                                      teFsMplsLSPMapTunnelTable,
                                                      NULL);

    if (pNextteFsMplsLSPMapTunnelTable == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pu4NextFsMplsLSPMapTunnelIndex =
        pNextteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelIndex;

    *pu4NextFsMplsLSPMapTunnelInstance =
        pNextteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelInstance;

    *pu4NextFsMplsLSPMapTunnelIngressLSRId =
        pNextteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelIngressLSRId;

    *pu4NextFsMplsLSPMapTunnelEgressLSRId =
        pNextteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelEgressLSRId;

    *pu4NextFsMplsLSPMapSubTunnelIndex =
        pNextteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelIndex;

    *pu4NextFsMplsLSPMapSubTunnelInstance =
        pNextteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelInstance;

    *pu4NextFsMplsLSPMapSubTunnelIngressLSRId =
        pNextteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelIngressLSRId;

    *pu4NextFsMplsLSPMapSubTunnelEgressLSRId =
        pNextteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelEgressLSRId;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsLSPMaptunnelOperation
 Input       :  The Indices
                FsMplsLSPMapTunnelIndex
                FsMplsLSPMapTunnelInstance
                FsMplsLSPMapTunnelIngressLSRId
                FsMplsLSPMapTunnelEgressLSRId
                FsMplsLSPMapSubTunnelIndex
                FsMplsLSPMapSubTunnelInstance
                FsMplsLSPMapSubTunnelIngressLSRId
                FsMplsLSPMapSubTunnelEgressLSRId

                The Object 
                retValFsMplsLSPMaptunnelOperation
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLSPMaptunnelOperation (UINT4 u4FsMplsLSPMapTunnelIndex,
                                   UINT4 u4FsMplsLSPMapTunnelInstance,
                                   UINT4 u4FsMplsLSPMapTunnelIngressLSRId,
                                   UINT4 u4FsMplsLSPMapTunnelEgressLSRId,
                                   UINT4 u4FsMplsLSPMapSubTunnelIndex,
                                   UINT4 u4FsMplsLSPMapSubTunnelInstance,
                                   UINT4 u4FsMplsLSPMapSubTunnelIngressLSRId,
                                   UINT4 u4FsMplsLSPMapSubTunnelEgressLSRId,
                                   UINT4 *pu4RetValFsMplsLSPMaptunnelOperation)
{
    tteFsMplsLSPMapTunnelTable teFsMplsLSPMapTunnelTable;

    MEMSET (&teFsMplsLSPMapTunnelTable, 0, sizeof (tteFsMplsLSPMapTunnelTable));

    /* Assign the index */

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelIndex =
        u4FsMplsLSPMapTunnelIndex;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelInstance =
        u4FsMplsLSPMapTunnelInstance;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelIngressLSRId =
        u4FsMplsLSPMapTunnelIngressLSRId;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelEgressLSRId =
        u4FsMplsLSPMapTunnelEgressLSRId;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelIndex =
        u4FsMplsLSPMapSubTunnelIndex;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelInstance =
        u4FsMplsLSPMapSubTunnelInstance;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelIngressLSRId =
        u4FsMplsLSPMapSubTunnelIngressLSRId;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelEgressLSRId =
        u4FsMplsLSPMapSubTunnelEgressLSRId;

    if (TeGetAllFsMplsLSPMapTunnelTable (&teFsMplsLSPMapTunnelTable) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValFsMplsLSPMaptunnelOperation =
        teFsMplsLSPMapTunnelTable.u4FsMplsLSPMaptunnelOperation;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFsMplsLSPMaptunnelRowStatus
 Input       :  The Indices
                FsMplsLSPMapTunnelIndex
                FsMplsLSPMapTunnelInstance
                FsMplsLSPMapTunnelIngressLSRId
                FsMplsLSPMapTunnelEgressLSRId
                FsMplsLSPMapSubTunnelIndex
                FsMplsLSPMapSubTunnelInstance
                FsMplsLSPMapSubTunnelIngressLSRId
                FsMplsLSPMapSubTunnelEgressLSRId

                The Object 
                retValFsMplsLSPMaptunnelRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLSPMaptunnelRowStatus (UINT4 u4FsMplsLSPMapTunnelIndex,
                                   UINT4 u4FsMplsLSPMapTunnelInstance,
                                   UINT4 u4FsMplsLSPMapTunnelIngressLSRId,
                                   UINT4 u4FsMplsLSPMapTunnelEgressLSRId,
                                   UINT4 u4FsMplsLSPMapSubTunnelIndex,
                                   UINT4 u4FsMplsLSPMapSubTunnelInstance,
                                   UINT4 u4FsMplsLSPMapSubTunnelIngressLSRId,
                                   UINT4 u4FsMplsLSPMapSubTunnelEgressLSRId,
                                   INT4 *pi4RetValFsMplsLSPMaptunnelRowStatus)
{
    tteFsMplsLSPMapTunnelTable teFsMplsLSPMapTunnelTable;

    MEMSET (&teFsMplsLSPMapTunnelTable, 0, sizeof (tteFsMplsLSPMapTunnelTable));

    /* Assign the index */
    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelIndex =
        u4FsMplsLSPMapTunnelIndex;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelInstance =
        u4FsMplsLSPMapTunnelInstance;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelIngressLSRId =
        u4FsMplsLSPMapTunnelIngressLSRId;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelEgressLSRId =
        u4FsMplsLSPMapTunnelEgressLSRId;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelIndex =
        u4FsMplsLSPMapSubTunnelIndex;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelInstance =
        u4FsMplsLSPMapSubTunnelInstance;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelIngressLSRId =
        u4FsMplsLSPMapSubTunnelIngressLSRId;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelEgressLSRId =
        u4FsMplsLSPMapSubTunnelEgressLSRId;

    if (TeGetAllFsMplsLSPMapTunnelTable (&teFsMplsLSPMapTunnelTable) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValFsMplsLSPMaptunnelRowStatus =
        teFsMplsLSPMapTunnelTable.i4FsMplsLSPMaptunnelRowStatus;

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMplsLSPMaptunnelOperation
 Input       :  The Indices
                FsMplsLSPMapTunnelIndex
                FsMplsLSPMapTunnelInstance
                FsMplsLSPMapTunnelIngressLSRId
                FsMplsLSPMapTunnelEgressLSRId
                FsMplsLSPMapSubTunnelIndex
                FsMplsLSPMapSubTunnelInstance
                FsMplsLSPMapSubTunnelIngressLSRId
                FsMplsLSPMapSubTunnelEgressLSRId

                The Object 
                setValFsMplsLSPMaptunnelOperation
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsLSPMaptunnelOperation (UINT4 u4FsMplsLSPMapTunnelIndex,
                                   UINT4 u4FsMplsLSPMapTunnelInstance,
                                   UINT4 u4FsMplsLSPMapTunnelIngressLSRId,
                                   UINT4 u4FsMplsLSPMapTunnelEgressLSRId,
                                   UINT4 u4FsMplsLSPMapSubTunnelIndex,
                                   UINT4 u4FsMplsLSPMapSubTunnelInstance,
                                   UINT4 u4FsMplsLSPMapSubTunnelIngressLSRId,
                                   UINT4 u4FsMplsLSPMapSubTunnelEgressLSRId,
                                   UINT4 u4SetValFsMplsLSPMaptunnelOperation)
{
    tteFsMplsLSPMapTunnelTable teFsMplsLSPMapTunnelTable;
    tteIsSetFsMplsLSPMapTunnelTable teIsSetFsMplsLSPMapTunnelTable;

    /* Update FsMplsLSPMapTunnelTable */
    MEMSET (&teFsMplsLSPMapTunnelTable, 0, sizeof (tteFsMplsLSPMapTunnelTable));
    MEMSET (&teIsSetFsMplsLSPMapTunnelTable, 0,
            sizeof (tteIsSetFsMplsLSPMapTunnelTable));

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelIndex =
        u4FsMplsLSPMapTunnelIndex;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMapTunnelIndex = OSIX_TRUE;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelInstance =
        u4FsMplsLSPMapTunnelInstance;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMapTunnelInstance = OSIX_TRUE;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelIngressLSRId =
        u4FsMplsLSPMapTunnelIngressLSRId;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMapTunnelIngressLSRId = OSIX_TRUE;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelEgressLSRId =
        u4FsMplsLSPMapTunnelEgressLSRId;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMapTunnelEgressLSRId = OSIX_TRUE;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelIndex =
        u4FsMplsLSPMapSubTunnelIndex;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMapSubTunnelIndex = OSIX_TRUE;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelInstance =
        u4FsMplsLSPMapSubTunnelInstance;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMapSubTunnelInstance = OSIX_TRUE;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelIngressLSRId =
        u4FsMplsLSPMapSubTunnelIngressLSRId;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMapSubTunnelIngressLSRId =
        OSIX_TRUE;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelEgressLSRId =
        u4FsMplsLSPMapSubTunnelEgressLSRId;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMapSubTunnelEgressLSRId =
        OSIX_TRUE;
    /* Assign the Operation */
    TE_LSP_MAP_TNL_OPERATION ((&teFsMplsLSPMapTunnelTable)) =
        u4SetValFsMplsLSPMaptunnelOperation;
    TE_IS_SET_LSP_MAP_TNL_OPERATION ((&teIsSetFsMplsLSPMapTunnelTable)) =
        OSIX_TRUE;

    if (TeSetAllFsMplsLSPMapTunnelTable
        (&teFsMplsLSPMapTunnelTable, &teIsSetFsMplsLSPMapTunnelTable,
         OSIX_FALSE, OSIX_FALSE) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetFsMplsLSPMaptunnelRowStatus
 Input       :  The Indices
                FsMplsLSPMapTunnelIndex
                FsMplsLSPMapTunnelInstance
                FsMplsLSPMapTunnelIngressLSRId
                FsMplsLSPMapTunnelEgressLSRId
                FsMplsLSPMapSubTunnelIndex
                FsMplsLSPMapSubTunnelInstance
                FsMplsLSPMapSubTunnelIngressLSRId
                FsMplsLSPMapSubTunnelEgressLSRId

                The Object 
                setValFsMplsLSPMaptunnelRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsLSPMaptunnelRowStatus (UINT4 u4FsMplsLSPMapTunnelIndex,
                                   UINT4 u4FsMplsLSPMapTunnelInstance,
                                   UINT4 u4FsMplsLSPMapTunnelIngressLSRId,
                                   UINT4 u4FsMplsLSPMapTunnelEgressLSRId,
                                   UINT4 u4FsMplsLSPMapSubTunnelIndex,
                                   UINT4 u4FsMplsLSPMapSubTunnelInstance,
                                   UINT4 u4FsMplsLSPMapSubTunnelIngressLSRId,
                                   UINT4 u4FsMplsLSPMapSubTunnelEgressLSRId,
                                   INT4 i4SetValFsMplsLSPMaptunnelRowStatus)
{
    tteFsMplsLSPMapTunnelTable teFsMplsLSPMapTunnelTable;
    tteIsSetFsMplsLSPMapTunnelTable teIsSetFsMplsLSPMapTunnelTable;

    MEMSET (&teFsMplsLSPMapTunnelTable, 0, sizeof (tteFsMplsLSPMapTunnelTable));
    MEMSET (&teIsSetFsMplsLSPMapTunnelTable, 0,
            sizeof (tteIsSetFsMplsLSPMapTunnelTable));
    /* Assign the index */

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelIndex =
        u4FsMplsLSPMapTunnelIndex;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMapTunnelIndex = OSIX_TRUE;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelInstance =
        u4FsMplsLSPMapTunnelInstance;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMapTunnelInstance = OSIX_TRUE;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelIngressLSRId =
        u4FsMplsLSPMapTunnelIngressLSRId;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMapTunnelIngressLSRId = OSIX_TRUE;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelEgressLSRId =
        u4FsMplsLSPMapTunnelEgressLSRId;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMapTunnelEgressLSRId = OSIX_TRUE;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelIndex =
        u4FsMplsLSPMapSubTunnelIndex;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMapSubTunnelIndex = OSIX_TRUE;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelInstance =
        u4FsMplsLSPMapSubTunnelInstance;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMapSubTunnelInstance = OSIX_TRUE;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelIngressLSRId =
        u4FsMplsLSPMapSubTunnelIngressLSRId;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMapSubTunnelIngressLSRId =
        OSIX_TRUE;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelEgressLSRId =
        u4FsMplsLSPMapSubTunnelEgressLSRId;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMapSubTunnelEgressLSRId =
        OSIX_TRUE;
    /* Assign the value */
    TE_LSP_MAP_TNL_RS ((&teFsMplsLSPMapTunnelTable)) =
        i4SetValFsMplsLSPMaptunnelRowStatus;
    TE_IS_SET_LSP_MAP_TNL_RS ((&teIsSetFsMplsLSPMapTunnelTable)) = OSIX_TRUE;

    if (TeSetAllFsMplsLSPMapTunnelTable
        (&teFsMplsLSPMapTunnelTable, &teIsSetFsMplsLSPMapTunnelTable,
         OSIX_FALSE, OSIX_FALSE) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMplsLSPMaptunnelOperation
 Input       :  The Indices
                FsMplsLSPMapTunnelIndex
                FsMplsLSPMapTunnelInstance
                FsMplsLSPMapTunnelIngressLSRId
                FsMplsLSPMapTunnelEgressLSRId
                FsMplsLSPMapSubTunnelIndex
                FsMplsLSPMapSubTunnelInstance
                FsMplsLSPMapSubTunnelIngressLSRId
                FsMplsLSPMapSubTunnelEgressLSRId

                The Object 
                testValFsMplsLSPMaptunnelOperation
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsLSPMaptunnelOperation (UINT4 *pu4ErrorCode,
                                      UINT4 u4FsMplsLSPMapTunnelIndex,
                                      UINT4 u4FsMplsLSPMapTunnelInstance,
                                      UINT4 u4FsMplsLSPMapTunnelIngressLSRId,
                                      UINT4 u4FsMplsLSPMapTunnelEgressLSRId,
                                      UINT4 u4FsMplsLSPMapSubTunnelIndex,
                                      UINT4 u4FsMplsLSPMapSubTunnelInstance,
                                      UINT4 u4FsMplsLSPMapSubTunnelIngressLSRId,
                                      UINT4 u4FsMplsLSPMapSubTunnelEgressLSRId,
                                      UINT4
                                      u4TestValFsMplsLSPMaptunnelOperation)
{
    tteFsMplsLSPMapTunnelTable teFsMplsLSPMapTunnelTable;
    tteIsSetFsMplsLSPMapTunnelTable teIsSetFsMplsLSPMapTunnelTable;

    MEMSET (&teFsMplsLSPMapTunnelTable, 0, sizeof (tteFsMplsLSPMapTunnelTable));
    MEMSET (&teIsSetFsMplsLSPMapTunnelTable, 0,
            sizeof (tteIsSetFsMplsLSPMapTunnelTable));

    /* Assign the index */
    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelIndex =
        u4FsMplsLSPMapTunnelIndex;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMapTunnelIndex = OSIX_TRUE;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelInstance =
        u4FsMplsLSPMapTunnelInstance;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMapTunnelInstance = OSIX_TRUE;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelIngressLSRId =
        u4FsMplsLSPMapTunnelIngressLSRId;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMapTunnelIngressLSRId = OSIX_TRUE;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelEgressLSRId =
        u4FsMplsLSPMapTunnelEgressLSRId;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMapTunnelEgressLSRId = OSIX_TRUE;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelIndex =
        u4FsMplsLSPMapSubTunnelIndex;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMapSubTunnelIndex = OSIX_TRUE;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelInstance =
        u4FsMplsLSPMapSubTunnelInstance;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMapSubTunnelInstance = OSIX_TRUE;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelIngressLSRId =
        u4FsMplsLSPMapSubTunnelIngressLSRId;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMapSubTunnelIngressLSRId =
        OSIX_TRUE;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelEgressLSRId =
        u4FsMplsLSPMapSubTunnelEgressLSRId;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMapSubTunnelEgressLSRId =
        OSIX_TRUE;

    /* Assign the value */
    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMaptunnelOperation =
        u4TestValFsMplsLSPMaptunnelOperation;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMaptunnelOperation = OSIX_TRUE;

    if (TeTestAllFsMplsLSPMapTunnelTable
        (pu4ErrorCode, &teFsMplsLSPMapTunnelTable,
         &teIsSetFsMplsLSPMapTunnelTable, OSIX_FALSE,
         OSIX_FALSE) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsLSPMaptunnelRowStatus
 Input       :  The Indices
                FsMplsLSPMapTunnelIndex
                FsMplsLSPMapTunnelInstance
                FsMplsLSPMapTunnelIngressLSRId
                FsMplsLSPMapTunnelEgressLSRId
                FsMplsLSPMapSubTunnelIndex
                FsMplsLSPMapSubTunnelInstance
                FsMplsLSPMapSubTunnelIngressLSRId
                FsMplsLSPMapSubTunnelEgressLSRId

                The Object 
                testValFsMplsLSPMaptunnelRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsLSPMaptunnelRowStatus (UINT4 *pu4ErrorCode,
                                      UINT4 u4FsMplsLSPMapTunnelIndex,
                                      UINT4 u4FsMplsLSPMapTunnelInstance,
                                      UINT4 u4FsMplsLSPMapTunnelIngressLSRId,
                                      UINT4 u4FsMplsLSPMapTunnelEgressLSRId,
                                      UINT4 u4FsMplsLSPMapSubTunnelIndex,
                                      UINT4 u4FsMplsLSPMapSubTunnelInstance,
                                      UINT4 u4FsMplsLSPMapSubTunnelIngressLSRId,
                                      UINT4 u4FsMplsLSPMapSubTunnelEgressLSRId,
                                      INT4 i4TestValFsMplsLSPMaptunnelRowStatus)
{
    tteFsMplsLSPMapTunnelTable teFsMplsLSPMapTunnelTable;
    tteIsSetFsMplsLSPMapTunnelTable teIsSetFsMplsLSPMapTunnelTable;

    MEMSET (&teFsMplsLSPMapTunnelTable, 0, sizeof (tteFsMplsLSPMapTunnelTable));
    MEMSET (&teIsSetFsMplsLSPMapTunnelTable, 0,
            sizeof (tteIsSetFsMplsLSPMapTunnelTable));

    /* Assign the index */
    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelIndex =
        u4FsMplsLSPMapTunnelIndex;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMapTunnelIndex = OSIX_TRUE;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelInstance =
        u4FsMplsLSPMapTunnelInstance;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMapTunnelInstance = OSIX_TRUE;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelIngressLSRId =
        u4FsMplsLSPMapTunnelIngressLSRId;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMapTunnelIngressLSRId = OSIX_TRUE;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelEgressLSRId =
        u4FsMplsLSPMapTunnelEgressLSRId;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMapTunnelEgressLSRId = OSIX_TRUE;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelIndex =
        u4FsMplsLSPMapSubTunnelIndex;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMapSubTunnelIndex = OSIX_TRUE;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelInstance =
        u4FsMplsLSPMapSubTunnelInstance;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMapSubTunnelInstance = OSIX_TRUE;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelIngressLSRId =
        u4FsMplsLSPMapSubTunnelIngressLSRId;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMapSubTunnelIngressLSRId =
        OSIX_TRUE;

    teFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelEgressLSRId =
        u4FsMplsLSPMapSubTunnelEgressLSRId;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMapSubTunnelEgressLSRId =
        OSIX_TRUE;

    /* Assign the value */
    teFsMplsLSPMapTunnelTable.i4FsMplsLSPMaptunnelRowStatus =
        i4TestValFsMplsLSPMaptunnelRowStatus;
    teIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMaptunnelRowStatus = OSIX_TRUE;

    if (TeTestAllFsMplsLSPMapTunnelTable
        (pu4ErrorCode, &teFsMplsLSPMapTunnelTable,
         &teIsSetFsMplsLSPMapTunnelTable, OSIX_FALSE,
         OSIX_FALSE) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsLSPMapTunnelTable
 Input       :  The Indices
                FsMplsLSPMapTunnelIndex
                FsMplsLSPMapTunnelInstance
                FsMplsLSPMapTunnelIngressLSRId
                FsMplsLSPMapTunnelEgressLSRId
                FsMplsLSPMapSubTunnelIndex
                FsMplsLSPMapSubTunnelInstance
                FsMplsLSPMapSubTunnelIngressLSRId
                FsMplsLSPMapSubTunnelEgressLSRId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT1
nmhDepv2FsMplsLSPMapTunnelTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsHLSPTable
 Input       :  The Indices
                FsMplsHLSPIndex
                FsMplsHLSPInstance
                FsMplsHLSPIngressLSRId
                FsMplsHLSPEgressLSRId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMplsHLSPTable (UINT4 u4FsMplsHLSPTunnelIndex,
                                         UINT4 u4FsMplsHLSPTunnelInstance,
                                         UINT4 u4FsMplsHLSPTunnelIngressLSRId,
                                         UINT4 u4FsMplsHLSPTunnelEgressLSRId)
{
    if (nmhValidateIndexInstanceMplsTunnelTable (u4FsMplsHLSPTunnelIndex,
                                                 u4FsMplsHLSPTunnelInstance,
                                                 u4FsMplsHLSPTunnelIngressLSRId,
                                                 u4FsMplsHLSPTunnelEgressLSRId)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsHLSPTable
 Input       :  The Indices
                FsMplsHLSPIndex
                FsMplsHLSPInstance
                FsMplsHLSPIngressLSRId
                FsMplsHLSPEgressLSRId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMplsHLSPTable (UINT4 *pu4FsMplsHLSPIndex,
                                 UINT4 *pu4FsMplsHLSPInstance,
                                 UINT4 *pu4FsMplsHLSPIngressLSRId,
                                 UINT4 *pu4FsMplsHLSPEgressLSRId)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    if (SNMP_SUCCESS == nmhGetFirstIndexMplsTunnelTable (pu4FsMplsHLSPIndex,
                                                         pu4FsMplsHLSPInstance,
                                                         pu4FsMplsHLSPIngressLSRId,
                                                         pu4FsMplsHLSPEgressLSRId))
    {
        do
        {
            MPLS_CMN_LOCK ();
            pTeTnlInfo = TeGetTunnelInfo (*pu4FsMplsHLSPIndex,
                                          *pu4FsMplsHLSPInstance,
                                          *pu4FsMplsHLSPIngressLSRId,
                                          *pu4FsMplsHLSPEgressLSRId);
            if (pTeTnlInfo == NULL)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            if (TE_TNL_TYPE_HLSP & TE_TNL_TYPE (pTeTnlInfo))
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
            MPLS_CMN_UNLOCK ();
        }
        while (SNMP_SUCCESS ==
               (nmhGetNextIndexMplsTunnelTable
                (*pu4FsMplsHLSPIndex, pu4FsMplsHLSPIndex,
                 *pu4FsMplsHLSPInstance, pu4FsMplsHLSPInstance,
                 *pu4FsMplsHLSPIngressLSRId, pu4FsMplsHLSPIngressLSRId,
                 *pu4FsMplsHLSPEgressLSRId, pu4FsMplsHLSPEgressLSRId)));
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMplsHLSPTable
 Input       :  The Indices
                FsMplsHLSPIndex
                nextFsMplsHLSPIndex
                FsMplsHLSPInstance
                nextFsMplsHLSPInstance
                FsMplsHLSPIngressLSRId
                nextFsMplsHLSPIngressLSRId
                FsMplsHLSPEgressLSRId
                nextFsMplsHLSPEgressLSRId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMplsHLSPTable (UINT4 u4FsMplsHLSPIndex,
                                UINT4 *pu4NextFsMplsHLSPIndex,
                                UINT4 u4FsMplsHLSPInstance,
                                UINT4 *pu4NextFsMplsHLSPInstance,
                                UINT4 u4FsMplsHLSPIngressLSRId,
                                UINT4 *pu4NextFsMplsHLSPIngressLSRId,
                                UINT4 u4FsMplsHLSPEgressLSRId,
                                UINT4 *pu4NextFsMplsHLSPEgressLSRId)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlInfo         *pTeHlspTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    /* If the given tunnel is not HLSP, return Failure */
    pTeHlspTnlInfo = TeGetTunnelInfo (u4FsMplsHLSPIndex,
                                      u4FsMplsHLSPInstance,
                                      u4FsMplsHLSPIngressLSRId,
                                      *pu4NextFsMplsHLSPEgressLSRId);

    if (pTeHlspTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (!(TE_TNL_TYPE_HLSP & TE_TNL_TYPE (pTeHlspTnlInfo)))
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    /* Get the next tunnel indices */
    if (SNMP_SUCCESS == nmhGetNextIndexMplsTunnelTable (u4FsMplsHLSPIndex,
                                                        pu4NextFsMplsHLSPIndex,
                                                        u4FsMplsHLSPInstance,
                                                        pu4NextFsMplsHLSPInstance,
                                                        u4FsMplsHLSPIngressLSRId,
                                                        pu4NextFsMplsHLSPIngressLSRId,
                                                        u4FsMplsHLSPEgressLSRId,
                                                        pu4NextFsMplsHLSPEgressLSRId))
    {
        do
        {
            MPLS_CMN_LOCK ();
            pTeTnlInfo = TeGetTunnelInfo (*pu4NextFsMplsHLSPIndex,
                                          *pu4NextFsMplsHLSPInstance,
                                          *pu4NextFsMplsHLSPIngressLSRId,
                                          *pu4NextFsMplsHLSPEgressLSRId);
            if (pTeTnlInfo == NULL)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            if (TE_TNL_TYPE_HLSP & TE_TNL_TYPE (pTeTnlInfo))
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
            MPLS_CMN_UNLOCK ();
        }
        while (SNMP_SUCCESS ==
               (nmhGetNextIndexMplsTunnelTable
                (*pu4NextFsMplsHLSPIndex, pu4NextFsMplsHLSPIndex,
                 *pu4NextFsMplsHLSPInstance, pu4NextFsMplsHLSPInstance,
                 *pu4NextFsMplsHLSPIngressLSRId, pu4NextFsMplsHLSPIngressLSRId,
                 *pu4NextFsMplsHLSPEgressLSRId, pu4NextFsMplsHLSPEgressLSRId)));
    }
    return SNMP_FAILURE;
}                                /* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsHLSPAvailableBW
 Input       :  The Indices
                FsMplsHLSPIndex
                FsMplsHLSPInstance
                FsMplsHLSPIngressLSRId
                FsMplsHLSPEgressLSRId

                The Object 
                retValFsMplsHLSPAvailableBW
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsHLSPAvailableBW (UINT4 u4MplsTunnelIndex,
                             UINT4 u4MplsTunnelInstance,
                             UINT4 u4MplsTunnelIngressLSRId,
                             UINT4 u4MplsTunnelEgressLSRId,
                             UINT4 *pu4RetValFsMplsHLSPAvailableBW)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo ((UINT4) u4MplsTunnelIndex,
                                  u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        if (((TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_HLSP) ||
             (TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_SLSP)) &&
            (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_NONE))
        {
            *pu4RetValFsMplsHLSPAvailableBW = TE_HLSP_AVAILABLE_BW (pTeTnlInfo);
        }
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsHLSPNoOfStackedTunnels
 Input       :  The Indices
                FsMplsHLSPIndex
                FsMplsHLSPInstance
                FsMplsHLSPIngressLSRId
                FsMplsHLSPEgressLSRId

                The Object 
                retValFsMplsHLSPNoOfStackedTunnels
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsHLSPNoOfStackedTunnels (UINT4 u4FsMplsHLSPIndex,
                                    UINT4 u4FsMplsHLSPInstance,
                                    UINT4 u4FsMplsHLSPIngressLSRId,
                                    UINT4 u4FsMplsHLSPEgressLSRId,
                                    UINT4
                                    *pu4RetValFsMplsHLSPNoOfStackedTunnels)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo ((UINT4) u4FsMplsHLSPIndex,
                                  u4FsMplsHLSPInstance,
                                  u4FsMplsHLSPIngressLSRId,
                                  u4FsMplsHLSPEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        if ((TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_HLSP) &&
            (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_NONE))
        {
            *pu4RetValFsMplsHLSPNoOfStackedTunnels =
                TE_HLSP_NO_OF_STACKED_TNLS (pTeTnlInfo);
        }
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/*---------------------------------------------------------------------------*/
/*                       End of file fshlsplw.c                                */
/*---------------------------------------------------------------------------*/
