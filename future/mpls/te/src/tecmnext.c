
/********************************************************************
 * Copyright (C) 2010 Aricent Inc . All Rights Reserved
 *
 * $Id: tecmnext.c,v 1.59 2018/01/03 11:31:27 siva Exp $
 *
 * Description: This file contains all the functions commonly used for static
 *              and signalling tunnel operations.
 * ----------------------------------------------------------------------------*/

#include "teincs.h"
#include "rtm.h"
#include "mplsftn.h"
#include "mplslsr.h"

extern UINT2        gu2GenLblSpaceGrpId;

/*****************************************************************************/
/* Function     : TeCmnExtUpdateTnlProtStatus                                      */
/*                                                                           */
/* Description  : This function updates the protection status for tunnel     */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                pTeUpdateInfo - Pointer to the tunnel updation information */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : TE_SUCCESS or TE_FAILURE                                   */
/*****************************************************************************/
UINT1
TeCmnExtUpdateTnlProtStatus (tTeTnlInfo * pTeTnlInfo,
                             tTeUpdateInfo * pTeUpdateInfo)
{
    tTeUpdateInfo       TeUpdateInfo;
    tTeTnlInfo         *pBkpTnlInfo = NULL;
    tTeTnlInfo         *pAssocTnlInfo = NULL;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;
    UINT4               u4BkpTnlIndex = 0;
    UINT4               u4BkpTnlInstance = 0;
    UINT4               u4BkpIngressId = 0;
    UINT4               u4BkpEgressId = 0;
    UINT1               u1PrevProtStatus = 0;
    UINT4               u4TnlMode = 0;
    BOOL1               b1FwdLlStatus = XC_OPER_UP;
    BOOL1               b1RevLlStatus = XC_OPER_UP;

    /* Get the backup tunnel indices already available in working tunnel for
     * protection in use, protection not available, protection not applicable
     * case or from pTeUpdateInfo for protection available case */

    CONVERT_TO_INTEGER (pTeTnlInfo->TnlIngressLsrId, u4IngressId);
    u4IngressId = OSIX_NTOHL (u4IngressId);
    CONVERT_TO_INTEGER (pTeTnlInfo->TnlEgressLsrId, u4EgressId);
    u4EgressId = OSIX_NTOHL (u4EgressId);

    TE_DBG4 (TE_EXTN_PRCS,
             "TeCmnExtUpdateTnlProtStatus ENTRY for Tunnel %d %d %x %x\n",
             pTeTnlInfo->u4TnlIndex, pTeTnlInfo->u4TnlInstance,
             u4IngressId, u4EgressId);

    u4TnlMode = pTeTnlInfo->u4TnlMode;

    if (pTeUpdateInfo->u1LocalProtection != LOCAL_PROT_AVAIL)
    {
        u4BkpTnlIndex = pTeTnlInfo->u4BkpTnlIndex;
        u4BkpTnlInstance = pTeTnlInfo->u4BkpTnlInstance;
        CONVERT_TO_INTEGER (pTeTnlInfo->BkpTnlIngressLsrId, u4BkpIngressId);
        u4BkpIngressId = OSIX_NTOHL (u4BkpIngressId);
        CONVERT_TO_INTEGER (pTeTnlInfo->BkpTnlEgressLsrId, u4BkpEgressId);
        u4BkpEgressId = OSIX_NTOHL (u4BkpEgressId);
    }
    else
    {
        u4BkpTnlIndex = pTeUpdateInfo->u4BkpTnlIndex;
        u4BkpTnlInstance = pTeUpdateInfo->u4BkpTnlInstance;
        CONVERT_TO_INTEGER (pTeUpdateInfo->BkpTnlIngressLsrId, u4BkpIngressId);
        u4BkpIngressId = OSIX_NTOHL (u4BkpIngressId);
        CONVERT_TO_INTEGER (pTeUpdateInfo->BkpTnlEgressLsrId, u4BkpEgressId);
        u4BkpEgressId = OSIX_NTOHL (u4BkpEgressId);
    }

    /* Fetching the backup tunnel information */
    pBkpTnlInfo =
        TeGetTunnelInfo (u4BkpTnlIndex, u4BkpTnlInstance, u4BkpIngressId,
                         u4BkpEgressId);

    if (pBkpTnlInfo == NULL)
    {
        TE_DBG4 (TE_EXTN_PRCS,
                 "Backup Tunnel %d %d %x %x do not exist\n",
                 pTeUpdateInfo->u4BkpTnlIndex,
                 pTeUpdateInfo->u4BkpTnlInstance,
                 u4BkpIngressId, u4BkpEgressId);
        return TE_FAILURE;
    }

    switch (pTeUpdateInfo->u1LocalProtection)
    {
        case LOCAL_PROT_IN_USE:
            TE_DBG (TE_EXTN_PRCS, "Tunnel Processed for Protection In Use\n");

            if (TE_TNL_LOCAL_PROTECTION_IN_USE (pTeTnlInfo) ==
                LOCAL_PROT_IN_USE)
            {
                TE_DBG (TE_EXTN_PRCS,
                        "Protection status is already protection in use\n");
                return TE_SUCCESS;
            }

            if (TE_TNL_LOCAL_PROTECTION_IN_USE (pTeTnlInfo) != LOCAL_PROT_AVAIL)
            {
                TE_DBG (TE_EXTN_PRCS,
                        "EXTN : Protection status is not available - Failure\n");
                return TE_FAILURE;
            }

            /* Copying the TnlXcIndex of the working to OrgTnlXcIndex 
             * and updating the BackUp TnlXcIndex as the working TnlXcIndex */

            pTeTnlInfo->u4OrgTnlXcIndex = pTeTnlInfo->u4TnlXcIndex;
            pTeTnlInfo->u4TnlXcIndex = pBkpTnlInfo->u4TnlXcIndex;

            /* Reset IN_USE_BY_VPN flag for Working Tunnel and set 
             * for Protection tunnel */
            if (TE_TNL_IN_USE_BY_VPN (pTeTnlInfo) == TE_TNL_INUSE_BY_L2VPN)
            {
                TE_TNL_IN_USE_BY_VPN (pTeTnlInfo) = TE_ZERO;
                TE_TNL_IN_USE_BY_VPN (pBkpTnlInfo) = TE_TNL_INUSE_BY_L2VPN;
            }

            /* Sending event to L2VPN module to notify the XC Index Change */
            TePrcsL2vpnAssociation (pBkpTnlInfo, TE_TNL_REESTB);

            if (u4TnlMode == TE_TNL_MODE_ASSOCIATED_BIDIRECTIONAL)
            {
                pAssocTnlInfo = TeGetTunnelInfo (pBkpTnlInfo->u4DestTnlIndex,
                                                 pBkpTnlInfo->u4DestTnlInstance,
                                                 u4BkpEgressId, u4BkpIngressId);

                if (pAssocTnlInfo != NULL)
                {

                    TePrcsL2vpnAssociation (pAssocTnlInfo, TE_TNL_REESTB);
                }
            }

            TE_TNL_LOCAL_PROTECTION_IN_USE (pTeTnlInfo) = LOCAL_PROT_IN_USE;
            TE_DBG (TE_EXTN_PRCS,
                    "Protection Status set as Local Protection in Use\n");
            break;
        case LOCAL_PROT_AVAIL:
            TE_DBG (TE_EXTN_PRCS,
                    "Tunnel Processed for Protection Available\n");

            /* Get the backup tunnel indices passed into this function.
             * These indices needs to be associated with the working tunnel. */
            CONVERT_TO_INTEGER (pTeUpdateInfo->BkpTnlIngressLsrId, u4IngressId);
            u4IngressId = OSIX_NTOHL (u4IngressId);
            CONVERT_TO_INTEGER (pTeUpdateInfo->BkpTnlEgressLsrId, u4EgressId);
            u4EgressId = OSIX_NTOHL (u4EgressId);

            u1PrevProtStatus = TE_TNL_LOCAL_PROTECTION_IN_USE (pTeTnlInfo);

            /* Associate the backup tunnel indices with the working tunnel */
            pTeTnlInfo->u4BkpTnlIndex = pTeUpdateInfo->u4BkpTnlIndex;
            pTeTnlInfo->u4BkpTnlInstance = pTeUpdateInfo->u4BkpTnlInstance;
            MEMCPY (pTeTnlInfo->BkpTnlIngressLsrId,
                    pTeUpdateInfo->BkpTnlIngressLsrId, ROUTER_ID_LENGTH);
            MEMCPY (pTeTnlInfo->BkpTnlEgressLsrId,
                    pTeUpdateInfo->BkpTnlEgressLsrId, ROUTER_ID_LENGTH);

            /* Associate the working tunnel indices with the backup tunnel */
            pBkpTnlInfo->u4BkpTnlIndex = pTeTnlInfo->u4TnlIndex;
            pBkpTnlInfo->u4BkpTnlInstance = pTeTnlInfo->u4TnlInstance;
            MEMCPY (pBkpTnlInfo->BkpTnlIngressLsrId,
                    pTeTnlInfo->TnlIngressLsrId, ROUTER_ID_LENGTH);
            MEMCPY (pBkpTnlInfo->BkpTnlEgressLsrId,
                    pTeTnlInfo->TnlEgressLsrId, ROUTER_ID_LENGTH);

            pTeTnlInfo->u1TnlSwitchApp = pTeUpdateInfo->u1TnlSwitchApp;
            pBkpTnlInfo->u1TnlSwitchApp = pTeUpdateInfo->u1TnlSwitchApp;
            TE_TNL_LOCAL_PROTECTION_IN_USE (pTeTnlInfo) = LOCAL_PROT_AVAIL;

            if (pTeTnlInfo->u4OrgTnlXcIndex != 0)
            {
                /* Resetting the Working Tunnel's Xc Index */
                pTeTnlInfo->u4TnlXcIndex = pTeTnlInfo->u4OrgTnlXcIndex;
                pTeTnlInfo->u4OrgTnlXcIndex = TE_ZERO;

                /* Set IN_USE_BY_VPN flag for Working Tunnel and reset 
                 * for Protection tunnel */
                if (TE_TNL_IN_USE_BY_VPN (pBkpTnlInfo) == TE_TNL_INUSE_BY_L2VPN)
                {
                    TE_TNL_IN_USE_BY_VPN (pTeTnlInfo) = TE_TNL_INUSE_BY_L2VPN;
                    TE_TNL_IN_USE_BY_VPN (pBkpTnlInfo) = TE_ZERO;
                }
                /* Sending event to L2VPN module to notify the XC Index change */
                TePrcsL2vpnAssociation (pTeTnlInfo, TE_TNL_REESTB);
            }
            else if (u1PrevProtStatus == LOCAL_PROT_NOT_APPLICABLE)
            {
                if (pTeTnlInfo->u1TnlSwitchApp == TE_SWITCH_APP_ELPS)
                {
                    pBkpTnlInfo->u1TnlPathType = TE_TNL_PROTECTION_PATH;
                }
                /* Intimate the L2VPN so that it correctly uses the
                 * underlying PSN Tunnel */
                if ((pBkpTnlInfo->u1CPOrMgmtOperStatus == TE_OPER_UP) &&
                    (TE_TNL_IN_USE_BY_VPN (pBkpTnlInfo) ==
                     TE_TNL_INUSE_BY_L2VPN))
                {
                    TePrcsL2vpnAssociation (pBkpTnlInfo, TE_OPER_DOWN);

                    TePrcsL2vpnAssociation (pTeTnlInfo, TE_OPER_UP);
                }

                if (pTeTnlInfo->u1TnlSwitchApp == TE_SWITCH_APP_ELPS)
                {
                    pTeTnlInfo->u1TnlPathType = TE_TNL_WORKING_PATH;
                }
            }
            else if (u1PrevProtStatus == LOCAL_PROT_NOT_AVAIL)
            {
                TeUpdateInfo.u1TeUpdateOpr = TE_CP_OR_MGMT_OPR_STATUS_RELATED;
                TeUpdateInfo.u1TnlOperStatus = TE_OPER_UP;
                TeUpdateInfo.u1CPOrOamOperChgReqd = FALSE;

                if (TeUpdateTnlStatus (pTeTnlInfo, &TeUpdateInfo) == TE_FAILURE)
                {
                    TE_DBG (TE_EXTN_ETEXT,
                            "EXTN : TeCmnExtUpdateTnlProtStatus : "
                            "Tunnel status updation failed: INTMD-EXIT \n");
                    return TE_FAILURE;
                }

                if (pTeTnlInfo->u1TnlSwitchApp == TE_SWITCH_APP_ELPS)
                {
                    pTeTnlInfo->u1TnlPathType = TE_TNL_WORKING_PATH;
                    pBkpTnlInfo->u1TnlPathType = TE_TNL_PROTECTION_PATH;
                }
            }

            if (pTeTnlInfo->u4TnlMode == TE_TNL_MODE_ASSOCIATED_BIDIRECTIONAL)
            {
                pAssocTnlInfo = TeGetTunnelInfo (pTeTnlInfo->u4DestTnlIndex,
                                                 pTeTnlInfo->
                                                 u4DestTnlInstance,
                                                 u4EgressId, u4IngressId);

                if (pAssocTnlInfo != NULL)
                {
                    TePrcsL2vpnAssociation (pAssocTnlInfo, TE_TNL_REESTB);
                }
            }

            TE_DBG (TE_EXTN_PRCS,
                    "Protection Status set as Protection Available\n");
            break;
        case LOCAL_PROT_NOT_APPLICABLE:
            TE_DBG (TE_EXTN_PRCS,
                    "Tunnel Processed for Protection Not Applicable\n");

            pTeTnlInfo->u4BkpTnlIndex = 0;
            pTeTnlInfo->u4BkpTnlInstance = 0;
            MEMSET (pTeTnlInfo->BkpTnlIngressLsrId, 0, ROUTER_ID_LENGTH);
            MEMSET (pTeTnlInfo->BkpTnlEgressLsrId, 0, ROUTER_ID_LENGTH);

            pBkpTnlInfo->u4BkpTnlIndex = 0;
            pBkpTnlInfo->u4BkpTnlInstance = 0;
            MEMSET (pBkpTnlInfo->BkpTnlIngressLsrId, 0, ROUTER_ID_LENGTH);
            MEMSET (pBkpTnlInfo->BkpTnlEgressLsrId, 0, ROUTER_ID_LENGTH);

            u1PrevProtStatus = TE_TNL_LOCAL_PROTECTION_IN_USE (pTeTnlInfo);
            TE_TNL_LOCAL_PROTECTION_IN_USE (pTeTnlInfo) =
                LOCAL_PROT_NOT_APPLICABLE;
            pTeTnlInfo->u1TnlSwitchApp = 0;
            pBkpTnlInfo->u1TnlSwitchApp = 0;

            TE_DBG (TE_EXTN_PRCS,
                    "Protection Status set as Protection Not Applicable\n");
            /* Intentional fall through */
        case LOCAL_PROT_NOT_AVAIL:
            pTeTnlInfo->u1TnlPathType = TE_TNL_WORKING_PATH;

            if (pTeUpdateInfo->u1LocalProtection != LOCAL_PROT_NOT_AVAIL)
            {
                pBkpTnlInfo->u1TnlPathType = TE_TNL_WORKING_PATH;
            }
            if (pTeUpdateInfo->u1LocalProtection == LOCAL_PROT_NOT_AVAIL)
            {
                u1PrevProtStatus = TE_TNL_LOCAL_PROTECTION_IN_USE (pTeTnlInfo);
                TE_TNL_LOCAL_PROTECTION_IN_USE (pTeTnlInfo) =
                    LOCAL_PROT_NOT_AVAIL;
            }
            if ((u1PrevProtStatus == LOCAL_PROT_IN_USE) &&
                (pTeTnlInfo->u4OrgTnlXcIndex != 0))
            {
                /* Resetting the Working Tunnel's Xc Index */
                pTeTnlInfo->u4TnlXcIndex = pTeTnlInfo->u4OrgTnlXcIndex;
                pTeTnlInfo->u4OrgTnlXcIndex = TE_ZERO;

                /* Deleting working tunnel from hardware, when protection tunnel
                 * also goes DOWN followed by Working tunnel went DOWN */

                if ((pTeTnlInfo->u1CPOrMgmtOperStatus == TE_OPER_DOWN) &&
                    (TeCmnExtProgramTunnelInHw (pTeTnlInfo, TE_OPER_DOWN,
                                                &b1FwdLlStatus, &b1RevLlStatus)
                     == TE_FAILURE))
                {
                    TE_DBG (TE_EXTN_PRCS,
                            "Tunnel HW Programming failed when removing working "
                            "tunnel since protection tunnel is also down\n");
                    return TE_FAILURE;
                }

                TeUpdateInfo.u1TeUpdateOpr = TE_CP_OR_MGMT_OPR_STATUS_RELATED;
                TeUpdateInfo.u1TnlOperStatus =
                    TeCmnExtCalculateTnlOperStatus (pTeTnlInfo);
                TeUpdateInfo.u1CPOrOamOperChgReqd = FALSE;

                if (TeUpdateTnlStatus (pTeTnlInfo, &TeUpdateInfo) == TE_FAILURE)
                {
                    TE_DBG (TE_EXTN_ETEXT,
                            "EXTN : TeCmnExtUpdateTnlProtStatus : "
                            "Tunnel status updation failed: INTMD-EXIT \n");
                    return TE_FAILURE;
                }

                if ((pTeTnlInfo->u1CPOrMgmtOperStatus == TE_OPER_DOWN) &&
                    (TE_TNL_IN_USE_BY_VPN (pBkpTnlInfo) ==
                     TE_TNL_INUSE_BY_L2VPN))
                {
                    TE_TNL_IN_USE_BY_VPN (pTeTnlInfo) = TE_ZERO;
                    TE_TNL_IN_USE_BY_VPN (pBkpTnlInfo) = TE_ZERO;
                }
            }

            if (pTeUpdateInfo->u1LocalProtection == LOCAL_PROT_NOT_AVAIL)
            {
                TE_DBG (TE_EXTN_PRCS,
                        "Protection Status set as Protection Not Available\n");
            }
            break;
        default:
            TE_DBG (TE_EXTN_FAIL, "EXTN : Invalid Local Protection status \n");
            TE_DBG (TE_EXTN_ETEXT,
                    "EXTN : TeCmnExtUpdateTnlProtStatus : INTMD-EXIT \n");
            return TE_FAILURE;
    }
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function     : TeCmnExtUpdateTnlOperStatus                                */
/*                                                                           */
/* Description  : This function updates the control plane or managment's     */
/*                operational status of the tunnel                           */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                pTeUpdateInfo - Pointer to the tunnel updation information */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : TE_SUCCESS or TE_FAILURE                                   */
/*****************************************************************************/
UINT1
TeCmnExtUpdateTnlOperStatus (tTeTnlInfo * pTeTnlInfo,
                             tTeUpdateInfo * pTeUpdateInfo,
                             tTeTnlInfo * pInstance0Tnl)
{
#ifdef MPLS_SIG_WANTED
    tTeTnlInfo         *pPrevTnlInfo = NULL;
#endif
    tTeTnlInfo         *pWorkingTnl = NULL;
    tTeTnlInfo         *pBackupTnl = NULL;
    tFtnEntry          *pFtnEntry = NULL;
    tTMO_DLL_NODE      *pFtnNodeInfo = NULL;
    UINT4               u4EntityIndex = TE_ZERO;
    UINT4               u4TnlIndex = TE_ZERO;
    UINT4               u4TnlInstance = TE_ZERO;
    UINT4               u4SrcAddr = TE_ZERO;
    UINT4               u4DestAddr = TE_ZERO;
    UINT1               u1PrevCPOrMgmtStatus = TE_ZERO;
    UINT1               u1IsRpteAdminDown = TE_FALSE;

    u4EntityIndex = TE_LDP_OVER_RSVP_ENT_INDEX (pTeTnlInfo);
    u4TnlIndex = TE_TNL_TNL_INDEX (pTeTnlInfo);
    u4TnlInstance = TE_TNL_TNL_INSTANCE (pTeTnlInfo);
    CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTeTnlInfo)), u4SrcAddr);
    u4SrcAddr = TE_NTOHL (u4SrcAddr);
    CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTeTnlInfo)), u4DestAddr);
    u4DestAddr = TE_NTOHL (u4DestAddr);

    /* Get the Previous value of CPOrMgmtOperStatus */
    u1PrevCPOrMgmtStatus = pTeTnlInfo->u1CPOrMgmtOperStatus;

    /* Making the Tunnel Operstatus either to UP/DOWN */
    switch (pTeUpdateInfo->u1TnlOperStatus)
    {
        case TE_OPER_DOWN:
            if (TeCmnExtUpdateCPAndOamOperStatus
                (pTeTnlInfo, pTeUpdateInfo->u1TeUpdateOpr, TE_OPER_DOWN,
                 pTeUpdateInfo->u1CPOrOamOperChgReqd) == FALSE)
            {
                TE_DBG4 (TE_EXTN_PRCS,
                         "EXTN: CP Or OAM Oper Status of Tunnel %d %d %x %x "
                         "is already changed\n",
                         pTeTnlInfo->u4TnlIndex, pTeTnlInfo->u4TnlInstance,
                         u4SrcAddr, u4DestAddr);
                break;
            }

            /* BFD SUPPORT FOR RSVP-TE */
            if (((pTeTnlInfo->u1TnlRole == TE_INGRESS) ||
                 (pTeTnlInfo->u1TnlRole == TE_EGRESS)) &&
                (pTeTnlInfo->u1TnlSgnlPrtcl == TE_SIGPROTO_RSVP) &&
                pTeTnlInfo->bIsOamEnabled == TRUE &&
                pTeTnlInfo->u1MplsOamAppType == MPLS_OAM_TYPE_BFD &&
                pInstance0Tnl != NULL)
            {
                if (TeRpteTEEventHandler (TE_TNL_DOWN, pTeTnlInfo,
                                          &u1IsRpteAdminDown)
                    == TE_RPTE_FAILURE)
                {
                    TE_DBG (TE_EXTN_PRCS, "Posting to RSVP-TE failed\n");
                }
            }

            /* BFD SUPPORT FOR RSVP-TE */

            if (TeCmnExtIsTnlProtected (pTeTnlInfo) == TRUE)
            {
                TE_DBG4 (TE_EXTN_PRCS,
                         "EXTN: Tunnel %d %d %x %x is protected.\n",
                         pTeTnlInfo->u4TnlIndex, pTeTnlInfo->u4TnlInstance,
                         u4SrcAddr, u4DestAddr);

                TeCmnExtNotifyTnlStatusToMeg (pTeTnlInfo, pTeUpdateInfo,
                                              u1PrevCPOrMgmtStatus);

                if (pTeTnlInfo->u1TnlProtType != TE_TNL_FRR_PROT_LINK)
                {
                    TE_DBG (TE_EXTN_PRCS,
                            "Tunnel Down Indication to other HL skipped.\n");
                    break;
                }
            }

#if 0
            if (TE_TNL_OPER_STATUS (pTeTnlInfo) == TE_OPER_DOWN)
            {
                /* Tunnel is already operationally DOWN */
                break;
            }
#endif

            if ((pTeTnlInfo->u4TnlInstance == TE_ZERO) &&
                (pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
                 MPLS_TE_FULL_REROUTE))
            {
                pWorkingTnl =
                    TeGetTunnelInfo (TE_TNL_TNL_INDEX (pTeTnlInfo),
                                     pTeTnlInfo->
                                     u4TnlPrimaryInstance, u4SrcAddr,
                                     u4DestAddr);
                /* Create Re-routed tunnel if release status is not awaited */
                /* For Fast Re Route tunnels, MBB support is not done
                 * Hence New tunnel setup is blocked.
                 * */
                if (((pWorkingTnl == NULL) ||
                     ((pWorkingTnl != NULL) &&
                      (pWorkingTnl->u4TnlProtIfIndex == TE_ZERO))) &&
                    (pTeTnlInfo->bIsMbbRequired == FALSE) &&
                    (pTeTnlInfo->u1TnlRelStatus != TE_SIGMOD_TNLREL_AWAITED))
                {

                    pTeTnlInfo->bIsMbbRequired = TRUE;
                    if (pTeUpdateInfo->u1TeUpdateOpr ==
                        TE_OAM_OPR_STATUS_RELATED)
                    {
                        pTeTnlInfo->u1TnlRerouteFlag = FALSE;
                        pTeTnlInfo->bIsOamPathChange = TRUE;

                    }
                    else
                    {
                        pTeTnlInfo->u1TnlRerouteFlag = TRUE;
                    }

                    if (TeRpteTEEventHandler (TE_TNL_UP, pTeTnlInfo,
                                              &u1IsRpteAdminDown)
                        == TE_RPTE_FAILURE)
                    {
                        TE_DBG (TE_EXTN_PRCS, "Posting to RSVP-TE failed\n");
                    }
                    break;
                }
            }

            TE_TNL_OPER_STATUS (pTeTnlInfo) = TE_OPER_DOWN;

            TE_TNL_PRIMARY_INSTANCE_UP_TIME (pTeTnlInfo) =
                TE_TNL_INSTANCE_UP_TIME (pTeTnlInfo) = 0;
            (TE_TNL_STATE_CHG_COUNT (pTeTnlInfo))++;
            if (TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_P2MP)
            {
                TE_P2MP_ACTIVE_TNLS (gTeGblInfo) -= TE_ONE;
            }
            TeSendTnlTrapAndSyslog (pTeTnlInfo, TE_TNL_DOWN_TRAP);

            if (((pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
                  MPLS_TE_DEDICATED_ONE2ONE) && (pInstance0Tnl != NULL))
                &&
                (((pTeTnlInfo->u1TnlPathType == TE_TNL_PROTECTION_PATH)
                  && (pInstance0Tnl->u1WorkingUp == FALSE))
                 || ((pTeTnlInfo->u1TnlPathType == TE_TNL_WORKING_PATH)
                     && (pInstance0Tnl->u1BackupUp == FALSE))))
            {
                TeCmnExtNotifyTnlStatusToMeg (pTeTnlInfo, pTeUpdateInfo,
                                              u1PrevCPOrMgmtStatus);
            }
            else if (pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType !=
                     MPLS_TE_DEDICATED_ONE2ONE)
            {
                TeCmnExtNotifyTnlStatusToMeg (pTeTnlInfo, pTeUpdateInfo,
                                              u1PrevCPOrMgmtStatus);
            }

            if ((pTeUpdateInfo->u1TeUpdateOpr ==
                 TE_CP_OR_MGMT_OPR_STATUS_RELATED) &&
                (pTeTnlInfo->u1CPOrMgmtOperStatus == TE_OPER_DOWN))
            {
                TePrcsL2vpnAssociation (pTeTnlInfo, TE_OPER_DOWN);
            }

            if ((pTeUpdateInfo->u1TeUpdateOpr ==
                 TE_CP_OR_MGMT_OPR_STATUS_RELATED) &&
                (pTeUpdateInfo->u1CPOrOamOperChgReqd == TRUE))
            {
                MEMSET (pTeTnlInfo->au1NextHopMac, TE_ZERO, MAC_ADDR_LEN);
                MEMSET (pTeTnlInfo->au1BkpNextHopMac, TE_ZERO, MAC_ADDR_LEN);
            }

#ifdef MPLS_SIG_WANTED
            if ((u4EntityIndex != TE_ZERO) &&
                (pTeUpdateInfo->u1TeUpdateOpr ==
                 TE_CP_OR_MGMT_OPR_STATUS_RELATED) &&
                (pTeTnlInfo->u1CPOrMgmtOperStatus == TE_OPER_DOWN))
            {
                TE_DBG5 (TE_EXTN_PRCS,
                         "EXTN: Tunnel %d %d %x %x down Indication"
                         "given to LDP Entity %d\n",
                         u4TnlIndex, u4TnlInstance, u4SrcAddr,
                         u4DestAddr, u4EntityIndex);

                LdpTnlOperUpDnHdlForLdpOverRsvp
                    (u4TnlIndex, u4TnlInstance, u4SrcAddr,
                     u4DestAddr, u4EntityIndex,
                     pTeTnlInfo->u4TnlXcIndex, TE_OPER_DOWN,
                     pTeTnlInfo->u1TnlRole);
            }
#endif
            if ((pTeUpdateInfo->u1TeUpdateOpr ==
                 TE_CP_OR_MGMT_OPR_STATUS_RELATED) &&
                (pTeTnlInfo->u1CPOrMgmtOperStatus == TE_OPER_DOWN))
            {
                /* Tunnel is operationally DOWN now, 
                 * so delete all the FTN entries in h/w */
                TMO_DLL_Scan (&TE_TNL_FTN_LIST (pTeTnlInfo),
                              pFtnNodeInfo, tTMO_DLL_NODE *)
                {
                    pFtnEntry = (tFtnEntry *)
                        (((FS_ULONG) pFtnNodeInfo
                          - (TE_OFFSET (tFtnEntry, FtnInfoNode))));

                    if (MplsFTNDel (pFtnEntry) == MPLS_FAILURE)
                    {
                        TE_DBG1 (TE_EXTN_PRCS, "FTN %x Deletion Failed \n",
                                 OSIX_NTOHL (*(UINT4 *)
                                             &(pFtnEntry->DestAddrMin.
                                               u4_addr)));
                    }
                }
            }
            TE_DBG (TE_EXTN_PRCS,
                    "EXTN : Tunnel OperStatus is made OPER-DOWN \n");
            break;
        case TE_OPER_UP:
            if (pTeTnlInfo->u4TnlInstance == TE_ZERO)
            {
                if (pTeTnlInfo->pTeBackupPathInfo == NULL)
                {
                    pTeTnlInfo->u1WorkingUp = TRUE;
                }
            }
            /*RSVP-TE MPLS-L3VPN-TE 
             * For UnProtected  from TE we will notify RSVP-TE !!!*/
            if ((pTeTnlInfo->u1TnlRole == TE_INGRESS) &&
                (pTeTnlInfo->u4TnlInstance != TE_ZERO) &&
                (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_RSVP))
            {
                if (TeRpteL3VPNEventHandler (TE_OPER_UP, pTeTnlInfo) ==
                    TE_RPTE_FAILURE)
                {
                    TE_DBG (TE_EXTN_PRCS,
                            "Posting to RSVP-TE failed UnProtected Notify Failed\n");
                }
            }

            if ((pTeTnlInfo->u1TnlRole == TE_INGRESS) &&
                (pTeTnlInfo->u4TnlInstance != TE_ZERO) &&
                (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_RSVP) &&
                (pInstance0Tnl != NULL))
            {
                if (pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
                    MPLS_TE_DEDICATED_ONE2ONE)
                {
                    /* Send Oper-up indication for protected tunnel  
                       if ERO for backup path is not configured,
                       for ERO based backup is configured then
                       backup path will be triggered independently */
                    if ((pTeTnlInfo->u1TnlPathType == TE_TNL_WORKING_PATH) &&
                        (pTeTnlInfo->pTeBackupPathInfo == NULL))
                    {
                        if (TeRpteTEEventHandler (TE_TNL_UP, pInstance0Tnl,
                                                  &u1IsRpteAdminDown)
                            == TE_RPTE_FAILURE)
                        {
                            TE_DBG (TE_EXTN_PRCS,
                                    "Posting to RSVP-TE failed\n");
                        }
                    }
                    else if (pTeTnlInfo->u1TnlPathType ==
                             TE_TNL_PROTECTION_PATH)
                    {
                        pWorkingTnl =
                            TeGetTunnelInfo (TE_TNL_TNL_INDEX (pTeTnlInfo),
                                             pInstance0Tnl->
                                             u4TnlPrimaryInstance, u4SrcAddr,
                                             u4DestAddr);
                        if (pWorkingTnl != NULL)
                        {
                            pWorkingTnl->u1TnlLocalProtectInUse =
                                LOCAL_PROT_AVAIL;
                        }
                        pInstance0Tnl->u1BackupUp = TRUE;
                    }
                    else
                    {
                        pInstance0Tnl->u1WorkingUp = TRUE;
                        if ((pTeTnlInfo->u1TnlPathType == TE_TNL_WORKING_PATH)
                            && (pInstance0Tnl->u1BackupUp == TRUE))
                        {
                            pTeTnlInfo->u1TnlLocalProtectInUse =
                                LOCAL_PROT_AVAIL;
                        }
                    }
                }
                else if ((pTeTnlInfo->bIsMbbRequired == TRUE) ||
                         (TE_TNL_REOPTIMIZE_TRIGGER (pTeTnlInfo) == TRUE))
                {
                    pInstance0Tnl->u4OrgTnlInstance = pTeTnlInfo->u4TnlInstance;

                    pWorkingTnl =
                        TeGetTunnelInfo (TE_TNL_TNL_INDEX (pTeTnlInfo),
                                         pInstance0Tnl->u4TnlPrimaryInstance,
                                         u4SrcAddr, u4DestAddr);
                    if (pWorkingTnl != NULL)
                    {
                        pWorkingTnl->u1TnlPathType = TE_ZERO;
                        if (TeRpteTEEventHandler (TE_TNL_DESTROY, pWorkingTnl,
                                                  &u1IsRpteAdminDown)
                            == TE_RPTE_FAILURE)
                        {
                            TE_DBG (TE_EXTN_PRCS,
                                    "Posting to RSVP-TE failed\n");
                        }
                    }
                    pTeTnlInfo->u1TnlPathType = TE_TNL_WORKING_PATH;
                    pTeTnlInfo->bIsMbbRequired = FALSE;
                    pInstance0Tnl->bIsMbbRequired = FALSE;
                    TE_TNL_REOPTIMIZE_TRIGGER (pTeTnlInfo) = FALSE;
                    TE_TNL_REOPTIMIZE_TRIGGER (pInstance0Tnl) = FALSE;
                    pInstance0Tnl->u4TnlPrimaryInstance =
                        pTeTnlInfo->u4TnlInstance;
                }
            }
            else if ((pTeTnlInfo->u1TnlRole == TE_EGRESS) &&
                     (pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
                      MPLS_TE_DEDICATED_ONE2ONE))
            {
                if (pTeTnlInfo->u1TnlPathType == TE_TNL_PROTECTION_PATH)
                {
                    pWorkingTnl =
                        TeGetTunnelInfo (TE_TNL_TNL_INDEX (pTeTnlInfo),
                                         pTeTnlInfo->
                                         u4BkpTnlInstance, u4SrcAddr,
                                         u4DestAddr);
                    if (pWorkingTnl != NULL)
                    {
                        pWorkingTnl->u1TnlLocalProtectInUse = LOCAL_PROT_AVAIL;
                    }
                }
                else
                {
                    if (pTeTnlInfo->u1TnlPathType == TE_TNL_WORKING_PATH)
                    {
                        pBackupTnl =
                            TeGetTunnelInfo (TE_TNL_TNL_INDEX (pTeTnlInfo),
                                             pTeTnlInfo->u4BkpTnlInstance,
                                             u4SrcAddr, u4DestAddr);
                        if (pBackupTnl != NULL)
                        {

                            pBackupTnl->u4BkpTnlInstance =
                                pTeTnlInfo->u4TnlInstance;
                            pBackupTnl->u4BkpTnlIndex =
                                TE_TNL_TNL_INDEX (pTeTnlInfo);
                        }

                    }
                }

            }

            if (TeCmnExtUpdateCPAndOamOperStatus
                (pTeTnlInfo, pTeUpdateInfo->u1TeUpdateOpr, TE_OPER_UP,
                 pTeUpdateInfo->u1CPOrOamOperChgReqd) == FALSE)
            {
                TE_DBG4 (TE_EXTN_PRCS,
                         "EXTN: CP Or OAM Oper Status of Tunnel %d %d %x %x "
                         "is already changed\n",
                         pTeTnlInfo->u4TnlIndex, pTeTnlInfo->u4TnlInstance,
                         u4SrcAddr, u4DestAddr);
                break;
            }

            if (TeCmnExtIsTnlProtected (pTeTnlInfo) == TRUE)
            {
                if (TE_TNL_OPER_STATUS (pTeTnlInfo) == TE_OPER_UP)
                {
                    TE_DBG4 (TE_EXTN_PRCS,
                             "EXTN: Tunnel %d %d %x %x is protected. "
                             "CP and Operational Status are also UP. Notified to MEG\n",
                             pTeTnlInfo->u4TnlIndex, pTeTnlInfo->u4TnlInstance,
                             u4SrcAddr, u4DestAddr);
                    TeCmnExtNotifyTnlStatusToMeg (pTeTnlInfo, pTeUpdateInfo,
                                                  u1PrevCPOrMgmtStatus);
                }
            }

            if (TE_TNL_OPER_STATUS (pTeTnlInfo) == TE_OPER_UP)
            {
                /* Tunnel is already operationally UP */
                break;
            }

            if (TeCmnExtCalculateTnlOperStatus (pTeTnlInfo) == TE_OPER_DOWN)
            {
                break;
            }

            TE_TNL_OPER_STATUS (pTeTnlInfo) = TE_OPER_UP;

            OsixGetSysTime (&(TE_TNL_INSTANCE_UP_TIME (pTeTnlInfo)));
            if (TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_P2MP)
            {
                TE_P2MP_ACTIVE_TNLS (gTeGblInfo) += TE_ONE;
            }
            /* We are not supporting backup tunnels so, primary
             * up is equal to entry up time */
            TE_TNL_PRIMARY_INSTANCE_UP_TIME (pTeTnlInfo) =
                TE_TNL_INSTANCE_UP_TIME (pTeTnlInfo);
            (TE_TNL_STATE_CHG_COUNT (pTeTnlInfo))++;
            TeSendTnlTrapAndSyslog (pTeTnlInfo, TE_TNL_UP_TRAP);

            TeCmnExtNotifyTnlStatusToMeg (pTeTnlInfo, pTeUpdateInfo,
                                          u1PrevCPOrMgmtStatus);

            if ((pTeUpdateInfo->u1TeUpdateOpr ==
                 TE_CP_OR_MGMT_OPR_STATUS_RELATED) &&
                (u1PrevCPOrMgmtStatus == TE_OPER_DOWN) &&
                (pTeTnlInfo->u1CPOrMgmtOperStatus == TE_OPER_UP))
            {
                TePrcsL2vpnAssociation (pTeTnlInfo, TE_OPER_UP);
            }

            TeDeleteTnlErrorTable (pTeTnlInfo);

#ifdef MPLS_SIG_WANTED
            /* If any tunnel with older tunnel instance exists,
             * deregister the entity index from it so that when
             * that tunnel goes down indication won't be sent to
             * LDP Entity. */
            if ((u4TnlInstance < TE_DETOUR_TNL_INSTANCE) &&
                (u4TnlInstance != TE_ZERO) &&
                (pTeUpdateInfo->u1TeUpdateOpr ==
                 TE_CP_OR_MGMT_OPR_STATUS_RELATED) &&
                (u1PrevCPOrMgmtStatus == TE_OPER_DOWN) &&
                (pTeTnlInfo->u1CPOrMgmtOperStatus == TE_OPER_UP) &&
                (TeGetLdpRegisteredActiveTunnel (u4TnlIndex,
                                                 u4SrcAddr,
                                                 u4DestAddr,
                                                 &pPrevTnlInfo)
                 == TE_SUCCESS) &&
                (TE_TNL_TNL_INSTANCE (pPrevTnlInfo) != u4TnlInstance))
            {
                TE_DBG5 (TE_EXTN_PRCS,
                         "EXTN: Entity %d deregistered from the "
                         "previous Tunnel %d %d %x %x\n",
                         TE_LDP_OVER_RSVP_ENT_INDEX (pPrevTnlInfo),
                         u4TnlIndex, TE_TNL_TNL_INSTANCE (pPrevTnlInfo),
                         u4SrcAddr, u4DestAddr);

                TE_LDP_OVER_RSVP_ENT_INDEX (pPrevTnlInfo) = TE_ZERO;
            }

            TE_DBG5 (TE_EXTN_PRCS,
                     "EXTN: Tunnel %d %d %x %x up Indication "
                     "given to LDP Entity %d\n",
                     u4TnlIndex, u4TnlInstance, u4SrcAddr,
                     u4DestAddr, u4EntityIndex);

            if ((pTeUpdateInfo->u1TeUpdateOpr ==
                 TE_CP_OR_MGMT_OPR_STATUS_RELATED) &&
                (u1PrevCPOrMgmtStatus == TE_OPER_DOWN) &&
                (pTeTnlInfo->u1CPOrMgmtOperStatus == TE_OPER_UP))
            {
                LdpTnlOperUpDnHdlForLdpOverRsvp
                    (u4TnlIndex, u4TnlInstance, u4SrcAddr, u4DestAddr,
                     u4EntityIndex, pTeTnlInfo->u4TnlXcIndex, TE_OPER_UP,
                     pTeTnlInfo->u1TnlRole);
            }
#endif
            if ((pTeUpdateInfo->u1TeUpdateOpr ==
                 TE_CP_OR_MGMT_OPR_STATUS_RELATED) &&
                (u1PrevCPOrMgmtStatus == TE_OPER_DOWN) &&
                (pTeTnlInfo->u1CPOrMgmtOperStatus == TE_OPER_UP))
            {
                /* Tunnel is operationally UP now, 
                 * so add all the FTN entries in h/w */
                TMO_DLL_Scan (&TE_TNL_FTN_LIST (pTeTnlInfo),
                              pFtnNodeInfo, tTMO_DLL_NODE *)
                {
                    pFtnEntry = (tFtnEntry *)
                        (((FS_ULONG) pFtnNodeInfo
                          - (TE_OFFSET (tFtnEntry, FtnInfoNode))));
                    if (MplsFTNAdd (pFtnEntry) == MPLS_FAILURE)
                    {
                        TE_DBG1 (TE_EXTN_PRCS, "FTN %x Addition Failed \n",
                                 OSIX_NTOHL (*(UINT4 *)
                                             &(pFtnEntry->DestAddrMin.
                                               u4_addr)));
                    }
                }
            }

            TE_DBG (TE_EXTN_PRCS,
                    "EXTN : Tunnel OperStatus is made OPER-UP \n");
            break;
        default:
            TE_DBG (TE_EXTN_FAIL, "EXTN : Invalid OperStatus provided \n");
            TE_DBG (TE_EXTN_ETEXT, "EXTN : TeUpdateTnlStatus : INTMD-EXIT \n");
            return TE_FAILURE;
    }
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function     : TeCmnExtIsTnlProtected                                     */
/*                                                                           */
/* Description  : This function checks whether tunnel is protected or not.   */
/*                If the tunnel is protected, it returns TRUE else FALSE     */
/*                                                                           */
/* Input        : pTeTnlInfo - Pointer to Tunnel Information                 */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : TRUE or FALSE                                              */
/*****************************************************************************/
INT4
TeCmnExtIsTnlProtected (tTeTnlInfo * pTeTnlInfo)
{
    if ((pTeTnlInfo->u1TnlLocalProtectInUse == LOCAL_PROT_AVAIL) ||
        (pTeTnlInfo->u1TnlLocalProtectInUse == LOCAL_PROT_IN_USE))
    {
        return TRUE;
    }

    return FALSE;
}

/*****************************************************************************/
/* Function     : TeCmnExtUpdateCPAndOamOperStatus                           */
/*                                                                           */
/* Description  : This function checks and updates CPOrMgmt and Oam Oper     */
/*                status based on the application that calls it.             */
/*                If CPOrOAM Oper Status Change is not required, it returns  */
/*                TRUE. If there is no change in status it returns FALSE     */
/*                else it returns TRUE                                       */
/*                                                                           */
/* Input        : pTeTnlInfo            - Pointer to Tunnel Information      */
/*                u1TeUpdateOpr         - Update Operation                   */
/*                u1OperStatus          - Oper Status value that needs to be */
/*                                        updated in CP or OAM Oper status   */
/*                u1CPOrOamOperChgReqd  - Indicates Oper Change should be    */
/*                                        done or not.                       */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : TRUE or FALSE                                              */
/*****************************************************************************/
INT4
TeCmnExtUpdateCPAndOamOperStatus (tTeTnlInfo * pTeTnlInfo, UINT1 u1TeUpdateOpr,
                                  UINT1 u1OperStatus,
                                  UINT1 u1CPOrOamOperChgReqd)
{
    if (u1CPOrOamOperChgReqd == FALSE)
    {
        return TRUE;
    }

    if (u1TeUpdateOpr == TE_CP_OR_MGMT_OPR_STATUS_RELATED)
    {
        if ((pTeTnlInfo->u1CPOrMgmtOperStatus == u1OperStatus) &&
            (TE_TNL_OPER_STATUS (pTeTnlInfo) == u1OperStatus))
        {
            /* CP Status and Oper Status of Tunnel is already correct */
            return FALSE;
        }
        else
        {
            pTeTnlInfo->u1CPOrMgmtOperStatus = u1OperStatus;
        }
    }
    else if ((u1TeUpdateOpr == TE_OAM_OPR_STATUS_RELATED) &&
             (pTeTnlInfo->bIsOamEnabled == TRUE))
    {
        if ((pTeTnlInfo->u1OamOperStatus == u1OperStatus) &&
            (TE_TNL_OPER_STATUS (pTeTnlInfo) == u1OperStatus))
        {
            /* OAM Status and Oper Status of Tunnel is already correct */
            return FALSE;
        }
        else
        {
            pTeTnlInfo->u1OamOperStatus = u1OperStatus;
        }
    }

    return TRUE;
}

/*****************************************************************************/
/* Function     : TeCmnExtCalculateTnlOperStatus                             */
/*                                                                           */
/* Description  : This function calculates the operational status of the     */
/*                tunnel based on OAM Capability Enable / Disabled.          */
/*                It return Tunnel Oper Status value.                        */
/*                                                                           */
/*                If OAM Capability is enabled, both CP and OAM Status       */
/*                are UP, Tunnel Oper Status returned is OPER_UP else        */
/*                Tunnel Oper Status retuned is OPER_DOWN.                   */
/*                                                                           */
/*                If OAM Capability is disabled, if CP Status alone is UP,   */
/*                Tunnel Oper Status returned is OPER_UP else                */
/*                Tunnel Oper Status returned is OPER_DOWN.                  */
/*                                                                           */
/* Input        : pTeTnlInfo            - Pointer to Tunnel Information      */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : TE_OPER_UP or TE_OPER_DOWN                                 */
/*****************************************************************************/
UINT1
TeCmnExtCalculateTnlOperStatus (tTeTnlInfo * pTeTnlInfo)
{
    UINT1               u1TnlOperStatus = TE_OPER_DOWN;

    if (pTeTnlInfo->bIsOamEnabled == TRUE)
    {
        if ((pTeTnlInfo->u1OamOperStatus == TE_OPER_UP) &&
            (pTeTnlInfo->u1CPOrMgmtOperStatus == TE_OPER_UP))
        {
            u1TnlOperStatus = TE_OPER_UP;
        }
        else if (TeCmnExtIsTnlProtected (pTeTnlInfo) == TRUE)
        {
            u1TnlOperStatus = TE_OPER_UP;
        }
        else
        {
            /* CPorMgmt/OAM of the oper. status is down */
            u1TnlOperStatus = TE_OPER_DOWN;
        }
    }
    else if (pTeTnlInfo->u1CPOrMgmtOperStatus == TE_OPER_UP)
    {
        u1TnlOperStatus = TE_OPER_UP;
    }
    else if (TeCmnExtIsTnlProtected (pTeTnlInfo) == TRUE)
    {
        u1TnlOperStatus = TE_OPER_UP;
    }
    else
    {
        /* Invalid operational status up handling */
        u1TnlOperStatus = TE_OPER_DOWN;
    }

    return u1TnlOperStatus;
}

/*****************************************************************************/
/* Function     : TeCmnExtNotifyTnlStatusToMeg                               */
/*                                                                           */
/* Description  : This function notifies the tunnel status to OAM module     */
/*                                                                           */
/* Input        : pInMplsApiInfo - Pointer to the input API information.     */
/*                pTeUpdateInfo - Pointer to the tunnel updation information */
/*                u1PrevCPOrMgmtStatus - Previous Value of CPOrMgmtStatus    */
/*                                                                           */
/* Output       : None.                                                      */
/*                                                                           */
/* Returns      : TE_SUCCESS or TE_FAILURE                                   */
/*****************************************************************************/
UINT1
TeCmnExtNotifyTnlStatusToMeg (tTeTnlInfo * pTeTnlInfo,
                              tTeUpdateInfo * pTeUpdateInfo,
                              UINT1 u1PrevCPOrMgmtStatus)
{
    tMplsApiInInfo     *pMplsApiInInfo = NULL;
    tMplsApiOutInfo    *pMplsApiOutInfo = NULL;
    tMplsEventNotif     MplsEventNotif;
    UINT4               u4LsrId;
    UINT4               u4DestModId = 0;

    if (pTeUpdateInfo->u1TeUpdateOpr != TE_CP_OR_MGMT_OPR_STATUS_RELATED)
    {
        return TE_SUCCESS;
    }

    if (pTeTnlInfo->u1CPOrMgmtOperStatus == u1PrevCPOrMgmtStatus)
    {
        /* Previous and Current Value of CPOrMgmtOperStatus is same.
         * No need to indicate to MEG. */
        return TE_SUCCESS;
    }

    /* Do not indicate to OAM applications for Instance 0 Tunnel. */
    if (pTeTnlInfo->u4TnlInstance == TE_ZERO)
    {
        return TE_SUCCESS;
    }

#ifdef RFC6374_WANTED
    if ((pTeTnlInfo->u4ProactiveSessionIndex == 0) &&
        (pTeTnlInfo->u1MplsOamAppType == MPLS_OAM_TYPE_NONE) &&
        (pTeTnlInfo->u1MplsPmAppType == MPLS_OAM_TYPE_NONE))
#else
    if ((pTeTnlInfo->u4ProactiveSessionIndex == 0) &&
        (pTeTnlInfo->u1MplsOamAppType == MPLS_OAM_TYPE_NONE))
#endif
    {
        /* As of now, for MPLS-TP MEG will only 
         * monitor for Tnl/PW and notify to Tnl/PW to
         * update the OAM status, OAM status should not be
         * notified to MEG module as MEG is notifying 
         * the OAM status now. */
        if ((pMplsApiInInfo =
             MemAllocMemBlk (TE_MPLS_API_IN_INFO_POOL_ID)) == NULL)
        {
            return TE_FAILURE;
        }
        if ((pMplsApiOutInfo =
             MemAllocMemBlk (TE_MPLS_API_OUT_INFO_POOL_ID)) == NULL)
        {
            MemReleaseMemBlock (TE_MPLS_API_IN_INFO_POOL_ID,
                                (UINT1 *) pMplsApiInInfo);
            return TE_FAILURE;
        }
        MEMSET (pMplsApiInInfo, 0, sizeof (tMplsApiInInfo));
        MEMSET (pMplsApiOutInfo, 0, sizeof (tMplsApiOutInfo));

        pMplsApiInInfo->u4SrcModId = MPLSDB_MODULE;
        pMplsApiInInfo->u4ContextId = MPLS_DEF_CONTEXT_ID;
        /* If MEG is not associated MEG indices will be zero, 
         * then MEG OAM ME table's indices 
         * will be updated in the tunnel table */
        pMplsApiInInfo->InPathId.MegId.u4MegIndex = pTeTnlInfo->u4MegIndex;
        pMplsApiInInfo->InPathId.MegId.u4MeIndex = pTeTnlInfo->u4MeIndex;
        pMplsApiInInfo->InPathId.MegId.u4MpIndex = pTeTnlInfo->u4MpIndex;

        pMplsApiInInfo->InServicePathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
        pMplsApiInInfo->InServicePathId.TnlId.u4SrcTnlNum =
            pTeTnlInfo->u4TnlIndex;
        pMplsApiInInfo->InServicePathId.TnlId.u4LspNum =
            pTeTnlInfo->u4TnlInstance;

        MEMCPY (&u4LsrId, pTeTnlInfo->TnlIngressLsrId, ROUTER_ID_LENGTH);
        u4LsrId = OSIX_NTOHL (u4LsrId);
        pMplsApiInInfo->InServicePathId.TnlId.SrcNodeId.
            MplsRouterId.u4_addr[0] = u4LsrId;

        MEMCPY (&u4LsrId, pTeTnlInfo->TnlEgressLsrId, ROUTER_ID_LENGTH);
        u4LsrId = OSIX_NTOHL (u4LsrId);
        pMplsApiInInfo->InServicePathId.TnlId.DstNodeId.
            MplsRouterId.u4_addr[0] = u4LsrId;

        if (pTeUpdateInfo->u1TnlOperStatus == TE_OPER_UP)
        {
            pMplsApiInInfo->InOamPathStatus.u1PathStatus = MPLS_PATH_STATUS_UP;
        }
        else
        {
            pMplsApiInInfo->InOamPathStatus.u1PathStatus =
                MPLS_PATH_STATUS_DOWN;
        }
        if (MplsOamIfHdlPathStatusChgForMeg (pMplsApiInInfo, pMplsApiOutInfo)
            == OSIX_FAILURE)
        {
            TE_DBG (TE_EXTN_FAIL,
                    "EXTN : Path status notification to MEG failed \n");
            MemReleaseMemBlock (TE_MPLS_API_IN_INFO_POOL_ID,
                                (UINT1 *) pMplsApiInInfo);
            MemReleaseMemBlock (TE_MPLS_API_OUT_INFO_POOL_ID,
                                (UINT1 *) pMplsApiOutInfo);
            return TE_FAILURE;
        }

        if ((pTeTnlInfo->u4MegIndex == 0) &&
            (pTeTnlInfo->u4MeIndex == 0) && (pTeTnlInfo->u4MpIndex == 0))
        {
            pTeTnlInfo->u4MegIndex =
                pMplsApiOutInfo->OutPathId.MegId.u4MegIndex;
            pTeTnlInfo->u4MeIndex = pMplsApiOutInfo->OutPathId.MegId.u4MeIndex;
            pTeTnlInfo->u4MpIndex = pMplsApiOutInfo->OutPathId.MegId.u4MpIndex;
        }
        MemReleaseMemBlock (TE_MPLS_API_IN_INFO_POOL_ID,
                            (UINT1 *) pMplsApiInInfo);
        MemReleaseMemBlock (TE_MPLS_API_OUT_INFO_POOL_ID,
                            (UINT1 *) pMplsApiOutInfo);
    }
    else
    {
        MEMSET (&MplsEventNotif, 0, sizeof (tMplsEventNotif));

        MplsEventNotif.u4ContextId = MPLS_DEF_CONTEXT_ID;

        MplsEventNotif.PathId.u4PathType = MPLS_PATH_TYPE_TUNNEL;
        MplsEventNotif.PathId.TnlId.u4SrcTnlNum = pTeTnlInfo->u4TnlIndex;
        MplsEventNotif.PathId.TnlId.u4LspNum = pTeTnlInfo->u4TnlInstance;

        MEMCPY (&u4LsrId, pTeTnlInfo->TnlIngressLsrId, ROUTER_ID_LENGTH);
        u4LsrId = OSIX_HTONL (u4LsrId);
        MplsEventNotif.PathId.TnlId.SrcNodeId.MplsRouterId.u4_addr[0] = u4LsrId;

        MEMCPY (&u4LsrId, pTeTnlInfo->TnlEgressLsrId, ROUTER_ID_LENGTH);
        u4LsrId = OSIX_HTONL (u4LsrId);
        MplsEventNotif.PathId.TnlId.DstNodeId.MplsRouterId.u4_addr[0] = u4LsrId;
        if (pTeTnlInfo->u1MplsOamAppType == MPLS_OAM_TYPE_Y1731)
        {
            u4DestModId = MPLS_APPLICATION_Y1731;
        }
        else                    /* Notification to BFD */
        {
            MplsEventNotif.u4ProactiveSessIndex =
                pTeTnlInfo->u4ProactiveSessionIndex;
            u4DestModId = MPLS_APPLICATION_BFD;
        }

#ifdef RFC6374_WANTED
        if (pTeTnlInfo->u1MplsPmAppType == MPLS_PM_TYPE_RFC6374)    /* Notify to RFC6374 */
        {
            u4DestModId |= MPLS_APPLICATION_RFC6374;
        }
#endif

        if (pTeUpdateInfo->u1TnlOperStatus == TE_OPER_UP)
        {
            MplsEventNotif.u2Event = MPLS_TNL_UP_EVENT;
        }
        else
        {
            MplsEventNotif.u2Event = MPLS_TNL_DOWN_EVENT;
        }
        if (MplsPortEventNotification (u4DestModId,
                                       &MplsEventNotif) == OSIX_FAILURE)
        {
            TE_DBG (TE_EXTN_FAIL,
                    "EXTN : Path status notification to OAM Module failed \n");
            return TE_FAILURE;
        }
    }
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function     : TeCmnExtGetBkpTnlFrmInUseProtTnl                           */
/*                                                                           */
/* Description  : This function checks and gets the protection path          */
/*                information if the protection tunnel is in use for         */
/*                working path tunnel.                                       */
/*                                                                           */
/* Input        : pProtTnlInfo - Protection tunnel information               */
/*                                                                           */
/* Output       : pBkpTnlInfo - Backup tunnel information                    */
/*                                                                           */
/* Returns      : TE_SUCCESS or TE_FAILURE                                   */
/*****************************************************************************/
UINT1
TeCmnExtGetBkpTnlFrmInUseProtTnl (tTeTnlIndices * pProtTnlInfo,
                                  tTeTnlIndices * pBkpTnlInfo)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4TnlIngressId = 0;
    UINT4               u4TnlEgressId = 0;

    MEMCPY (&u4TnlIngressId, pProtTnlInfo->TnlIngressLsrId, sizeof (UINT4));
    u4TnlIngressId = OSIX_NTOHL (u4TnlIngressId);

    MEMCPY (&u4TnlEgressId, pProtTnlInfo->TnlEgressLsrId, sizeof (UINT4));
    u4TnlEgressId = OSIX_NTOHL (u4TnlEgressId);

    pTeTnlInfo =
        TeGetTunnelInfo (pProtTnlInfo->u4TnlIndex, pProtTnlInfo->u4TnlInstance,
                         u4TnlIngressId, u4TnlEgressId);
    if (pTeTnlInfo == NULL)
    {
        return TE_FAILURE;
    }
    if (TE_TNL_LOCAL_PROTECTION_IN_USE (pTeTnlInfo) == LOCAL_PROT_IN_USE)
    {
        pBkpTnlInfo->u4TnlIndex = pTeTnlInfo->u4BkpTnlIndex;
        pBkpTnlInfo->u4TnlInstance = pTeTnlInfo->u4BkpTnlInstance;
        MEMCPY (pBkpTnlInfo->TnlIngressLsrId, pTeTnlInfo->BkpTnlIngressLsrId,
                sizeof (UINT4));
        MEMCPY (pBkpTnlInfo->TnlEgressLsrId, pTeTnlInfo->BkpTnlEgressLsrId,
                sizeof (UINT4));
    }
    else
    {
        return TE_FAILURE;
    }
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function     : TeCmnExtProgramTunnelInHw                                  */
/*                                                                           */
/* Description  : This function updates the common database of a static      */
/*                tunnel                                                     */
/*                                                                           */
/* Input        : pTeTnlInfo         - Pointer to Tunnel Information         */
/*                u1TnlOperStatus    - Operational status to be set on       */
/*                                     the tunnel                            */
/*                                                                           */
/* Output       : pb1FwdLlStatus     - Forward XC Status                     */
/*                pb1RevLlStatus     - Reverse XC Status                     */
/*                                                                           */
/* Returns      : TE_SUCCESS or TE_FAILURE                                   */
/*****************************************************************************/
INT4
TeCmnExtProgramTunnelInHw (tTeTnlInfo * pTeTnlInfo, UINT1 u1TnlOperStatus,
                           BOOL1 * pb1FwdLlStatus, BOOL1 * pb1RevLlStatus)
{
    UINT4               u4ForwardAction = 0;
    UINT4               u4ReverseAction = 0;
    INT4                i4RetVal = MPLS_FAILURE;

    /* Set both the forward and reverse action to be taken for the tunnel 
     * based on the tunnel role and Operational status to be set. */

    if (TE_TNL_ROLE (pTeTnlInfo) == TE_INGRESS)
    {
        if (u1TnlOperStatus == TE_OPER_UP)
        {
            u4ForwardAction = MPLS_MLIB_TNL_CREATE;
            u4ReverseAction = MPLS_MLIB_ILM_CREATE;
        }
        else
        {
            u4ForwardAction = MPLS_MLIB_TNL_DELETE;
            u4ReverseAction = MPLS_MLIB_ILM_DELETE;
        }
    }
    else if (TE_TNL_ROLE (pTeTnlInfo) == TE_INTERMEDIATE)
    {
        if (u1TnlOperStatus == TE_OPER_UP)
        {
            u4ForwardAction = MPLS_MLIB_ILM_CREATE;
            u4ReverseAction = MPLS_MLIB_ILM_CREATE;
        }
        else
        {
            u4ForwardAction = MPLS_MLIB_ILM_DELETE;
            u4ReverseAction = MPLS_MLIB_ILM_DELETE;
        }
    }
    else
    {
        if (u1TnlOperStatus == TE_OPER_UP)
        {
            u4ForwardAction = MPLS_MLIB_ILM_CREATE;
            u4ReverseAction = MPLS_MLIB_TNL_CREATE;
        }
        else
        {
            u4ForwardAction = MPLS_MLIB_ILM_DELETE;
            u4ReverseAction = MPLS_MLIB_TNL_DELETE;
        }
    }
    /* Bandwidth reservation for Forward direction tunnel */
    if (TeTlmUpdateTrafficControl (pTeTnlInfo, u1TnlOperStatus,
                                   MPLS_DIRECTION_FORWARD) == TE_FAILURE)
    {
        return TE_FAILURE;
    }

    /* Do the action for the forward direction first. */
    i4RetVal = MplsDbUpdateTunnelInHw (u4ForwardAction, pTeTnlInfo,
                                       MPLS_DIRECTION_FORWARD, pb1FwdLlStatus);

    if ((TE_TNL_ROLE (pTeTnlInfo) == TE_INGRESS) ||
        (TE_TNL_ROLE (pTeTnlInfo) == TE_INTERMEDIATE))
    {
        if ((i4RetVal == MPLS_FAILURE) &&
            (pTeTnlInfo->u1FwdArpResolveStatus != MPLS_ARP_RESOLVE_WAITING))
        {
            return TE_FAILURE;
        }
    }
    else
    {
        if (i4RetVal == MPLS_FAILURE)
        {
            return TE_FAILURE;
        }
    }

    /* For the corouted bidirectional tunnel, do the action for the
     * reverse direction also. */
    if (pTeTnlInfo->u4TnlMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
    {
        /* Bandwidth reservation for reverse direction tunnel */
        if (TeTlmUpdateTrafficControl (pTeTnlInfo, u1TnlOperStatus,
                                       MPLS_DIRECTION_REVERSE) == TE_FAILURE)
        {
            return TE_FAILURE;
        }

        i4RetVal = MplsDbUpdateTunnelInHw (u4ReverseAction, pTeTnlInfo,
                                           MPLS_DIRECTION_REVERSE,
                                           pb1RevLlStatus);

        if ((TE_TNL_ROLE (pTeTnlInfo) == TE_EGRESS) ||
            (TE_TNL_ROLE (pTeTnlInfo) == TE_INTERMEDIATE))
        {
            if ((i4RetVal == MPLS_FAILURE) &&
                (pTeTnlInfo->u1RevArpResolveStatus != MPLS_ARP_RESOLVE_WAITING))
            {
                return TE_FAILURE;
            }
        }
        else
        {
            if (i4RetVal == MPLS_FAILURE)
            {
                return TE_FAILURE;
            }
        }
    }

    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function     : TeCmnExtSetOperStatusAndProgHw                             */
/*                                                                           */
/* Description  : This function updates the common database and the          */
/*                operational status of static tunnel.                       */
/*                                                                           */
/* Input        : pTeTnlInfo         - Pointer to Tunnel Information         */
/*                u1TnlOperStatus    - Operational status to be set on       */
/*                                     the tunnel                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : TE_SUCCESS or TE_FAILURE                                   */
/*****************************************************************************/
INT4
TeCmnExtSetOperStatusAndProgHw (tTeTnlInfo * pTeTnlInfo, UINT1 u1TnlOperStatus)
{
    BOOL1               bCallOperStatus = FALSE;
    BOOL1               bFwdLlStatus = XC_OPER_UP;
    BOOL1               bRevLlStatus = XC_OPER_UP;

    if (pTeTnlInfo->u4TnlInstance == TE_ZERO)
    {
        /* Do not handle label programming and bandwidth provisioning for
         * Instance 0 tunnels. */
        return TE_SUCCESS;
    }

    if (TeCmnExtProgramTunnelInHw (pTeTnlInfo, u1TnlOperStatus, &bFwdLlStatus,
                                   &bRevLlStatus) == TE_FAILURE)
    {
        return TE_FAILURE;
    }

    /* If the operational status to be set on the tunnel is UP, 
     * action done should return ARP_STATUS as ARP_RESOLVED or ARP_UNKNOWN
     * for the tunnel to be made operationally UP.
     *
     * If the operational status to be set on the tunnel is DOWN,
     * tunnel can be made operationally down without any such validations
     * on action. */
    if (u1TnlOperStatus == TE_OPER_UP)
    {
        if (((pTeTnlInfo->u1FwdArpResolveStatus == MPLS_ARP_RESOLVED) ||
             (pTeTnlInfo->u1FwdArpResolveStatus == MPLS_ARP_RESOLVE_UNKNOWN)) &&
            ((pTeTnlInfo->u1RevArpResolveStatus == MPLS_ARP_RESOLVED) ||
             (pTeTnlInfo->u1RevArpResolveStatus == MPLS_ARP_RESOLVE_UNKNOWN)) &&
            (bFwdLlStatus == XC_OPER_UP) && (bRevLlStatus == XC_OPER_UP))
        {
            bCallOperStatus = TRUE;
        }
        else
        {
            bCallOperStatus = FALSE;
        }
    }
    else
    {
        bCallOperStatus = TRUE;
    }

    if (bCallOperStatus == TRUE)
    {
        if (TeCallUpdateTnlOperStatus (pTeTnlInfo, u1TnlOperStatus) ==
            TE_FAILURE)
        {
            return TE_FAILURE;
        }
        /* STATIC_HLSP */
        if (TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_HLSP)
        {
            if (TeUpdateServiceTnlOperStatus (pTeTnlInfo, u1TnlOperStatus) ==
                TE_FAILURE)
            {
                return TE_FAILURE;
            }
        }

        /* Update the operationl status of the associated tunnel when the 
         * operational status of S-LSP tunnel changes.*/
        if (TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_SLSP)
        {
            if (TE_MAP_TNL_INFO (pTeTnlInfo) != NULL)
            {
                if (TeCallUpdateTnlOperStatus
                    (TE_MAP_TNL_INFO (pTeTnlInfo),
                     u1TnlOperStatus) == TE_FAILURE)
                {
                    return TE_FAILURE;
                }
            }
        }
    }

    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function     : TeCmnExtUpdateArpResolveStatus                             */
/*                                                                           */
/* Description  : This function updates the ARP resolution status of         */
/*                the tunnel based on the action passed, direction passed    */
/*                and tunnel role. Accepted actions are MLIB_TNL_CREATE and  */
/*                MLIB_ILM_CREATE. Accepted direction are FORWARD and        */
/*                REVERSE.                                                   */
/*                                                                           */
/* Input        : pTeTnlInfo         - Pointer to Tunnel Information         */
/*                u4Action           - MLIB Action                           */
/*                Direction          - XC Direction                          */
/*                u1ArpResolveStatus - ARP Resolution status                 */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
TeCmnExtUpdateArpResolveStatus (tTeTnlInfo * pTeTnlInfo, UINT4 u4Action,
                                eDirection Direction, UINT1 u1ArpResolveStatus)
{
    if (pTeTnlInfo == NULL)
    {
        return;
    }

    if ((u4Action == MPLS_MLIB_TNL_CREATE) ||
        (u4Action == MPLS_MLIB_TNL_DELETE))
    {
        if (TE_TNL_ROLE (pTeTnlInfo) == TE_INGRESS)
        {
            pTeTnlInfo->u1FwdArpResolveStatus = u1ArpResolveStatus;
        }
        else
        {
            pTeTnlInfo->u1RevArpResolveStatus = u1ArpResolveStatus;
        }
    }
    else if ((u4Action == MPLS_MLIB_ILM_CREATE) ||
             (u4Action == MPLS_MLIB_ILM_DELETE))
    {
        /* Action here should be ILM_CREATE */

        if (Direction == MPLS_DIRECTION_FORWARD)
        {
            pTeTnlInfo->u1FwdArpResolveStatus = u1ArpResolveStatus;
        }
        else
        {
            pTeTnlInfo->u1RevArpResolveStatus = u1ArpResolveStatus;
        }
    }
}

/*****************************************************************************/
/* Function     : TeCmnExtGetDirectionFromTnl                                */
/*                                                                           */
/* Description  : This function gets direction for which XC entry needs to   */
/*                be validated. This function returns the direction as       */
/*                reverse if tunnel is corouted bidirectional and tunnel     */
/*                role is egress. Else it returns direction as forward       */
/*                                                                           */
/* Input        : pTeTnlInfo         - Pointer to Tunnel Information         */
/*                                                                           */
/* Output       : pu4Direction       - Direction                             */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
TeCmnExtGetDirectionFromTnl (tTeTnlInfo * pTeTnlInfo, eDirection * pDirection)
{
    if ((pTeTnlInfo->u4TnlMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL) &&
        (pTeTnlInfo->u1TnlRole == TE_EGRESS))
    {
        *pDirection = MPLS_DIRECTION_REVERSE;
    }
    else
    {
        *pDirection = MPLS_DIRECTION_FORWARD;
    }
}

/* MPLS_P2MP_LSP_CHANGES - S */
/*****************************************************************************/
/* Function     : TeP2mpExtSetOperStatusAndProgHw                            */
/*                                                                           */
/* Description  : This function updates the common database and the          */
/*                operational status of static tunnel.                       */
/*                                                                           */
/* Input        : pTeTnlInfo         - Pointer to Tunnel Information         */
/*                u1TnlOperStatus    - Operational status to be set on       */
/*                                     the tunnel                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : TE_SUCCESS or TE_FAILURE                                   */
/*****************************************************************************/
INT4
TeP2mpExtSetOperStatusAndProgHw (tTeTnlInfo * pTeTnlInfo, UINT1 u1TnlOperStatus)
{
    tP2mpBranchEntry   *pTeP2mpBranchEntry = NULL;
    tXcEntry           *pXcEntry = NULL;
    tOutSegment        *pOutSegment = NULL;
    UINT4               u4OutSegIndex = TE_ZERO;

    if (pTeTnlInfo->u4TnlInstance == TE_ZERO)
    {
        /* Do not handle label programming and bandwidth provisioning for
         * Instance 0 tunnels. */
        return TE_SUCCESS;
    }

    if (TE_TNL_ROLE (pTeTnlInfo) == TE_EGRESS)
    {
        pXcEntry = MplsGetXCEntryByDirection (pTeTnlInfo->u4TnlXcIndex,
                                              MPLS_DIRECTION_FORWARD);
        if (pXcEntry == NULL)
        {
            return TE_FAILURE;
        }

        /*Configure ILM in HW */
        if (MplsDbUpdateP2mpTunnelInHw (pTeTnlInfo, pXcEntry,
                                        u1TnlOperStatus) == MPLS_FAILURE)
        {
            return TE_FAILURE;
        }
    }
    else
    {
        TMO_SLL_Scan (&(TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo)),
                      pTeP2mpBranchEntry, tP2mpBranchEntry *)
        {
            u4OutSegIndex = TE_P2MP_BRANCH_OUTSEG_INDEX (pTeP2mpBranchEntry);
            pOutSegment = MplsGetOutSegmentTableEntry (u4OutSegIndex);
            if (NULL == pOutSegment)
            {
                return TE_FAILURE;
            }

            pXcEntry = OUTSEGMENT_XC_INDEX (pOutSegment);
            if (NULL == pXcEntry)
            {
                return TE_FAILURE;
            }

            /*Configure P2MP ILM/Tunnel in HW */
            if (MplsDbUpdateP2mpTunnelInHw (pTeTnlInfo, pXcEntry,
                                            u1TnlOperStatus) == MPLS_FAILURE)
            {
                return TE_FAILURE;
            }
        }

        if (TE_P2MP_TNL_BRANCH_ROLE (pTeTnlInfo->pTeP2mpTnlInfo) == TE_P2MP_BUD)
        {
            /*Configure P2MP bud node in HW */
            if (MplsP2mpUpdateBudNode (pTeTnlInfo, u1TnlOperStatus)
                == MPLS_FAILURE)
            {
                return TE_FAILURE;
            }
        }
    }
    return TE_SUCCESS;
}

/* MPLS_P2MP_LSP_CHANGES - E */

/*****************************************************************************/
/* Function     : TeCmnExtHandleIfChange                                     */
/*                                                                           */
/* Description  : This function handles interface change events for static   */
/*                tunnels.                                                   */
/*                                                                           */
/* Input        : pIfInfo   - Pointer to Interface Information               */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : None                                                       */
/*****************************************************************************/
VOID
TeCmnExtHandleIfChange (tMplsIfEntryInfo * pIfInfo)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlInfo         *pNextTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    pTeTnlInfo = RBTreeGetFirst (gTeGblInfo.TnlInfoTbl);

    while (pTeTnlInfo != NULL)
    {
        pNextTeTnlInfo = RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                        (tRBElem *) pTeTnlInfo, NULL);

        if ((TE_TNL_OWNER (pTeTnlInfo) != TE_TNL_OWNER_SNMP) ||
            (TE_TNL_ROW_STATUS (pTeTnlInfo) != TE_ACTIVE) ||
            (TE_TNL_ADMIN_STATUS (pTeTnlInfo) != TE_ADMIN_UP))
        {
            pTeTnlInfo = pNextTeTnlInfo;
            continue;
        }

        if (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_NONE)
        {
            if (pTeTnlInfo->u4TnlInstance == TE_ZERO)
            {
                /* Do not handle if status change for Instance 0 tunnels. */
                pTeTnlInfo = pNextTeTnlInfo;
                continue;
            }

            if (TeCmnExtHandleIfChangeForStatic (pTeTnlInfo, pIfInfo)
                == TE_SUCCESS)
            {
                /* Processed interface status change event for static tunnel
                 * that matched the interface. So, break here. */
                break;
            }

        }
        else if ((TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_RSVP) ||
                 (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_LDP))
        {
            /* Break should not be done for signaling tunnels */
            TeReestablishOperDownTnls (pTeTnlInfo, pIfInfo->u4IfIndex,
                                       TE_TNL_SIGPROTO (pTeTnlInfo));
        }

        pTeTnlInfo = pNextTeTnlInfo;
    }

    MPLS_CMN_UNLOCK ();

    return;
}

/*****************************************************************************/
/* Function     : TeCmnExtGetNextP2mpBranchInfo                              */
/*                                                                           */
/* Description  : This function is used to get the next branch info          */
/*                                                                           */
/* Input        : pTeTnlInfo         - Pointer to Tunnel Information         */
/*                u1TnlOperStatus    - Operational status to be set on       */
/*                                     the tunnel                            */
/*                                                                           */
/* Output       : None                                                       */
/*                                                                           */
/* Returns      : TE_SUCCESS or TE_FAILURE                                   */
/*****************************************************************************/
INT4
TeCmnExtGetNextP2mpBranchInfo (UINT4 u4TnlIndex, UINT4 u4TnlInst,
                               UINT4 u4LclLsr, UINT4 u4PeerLsr,
                               UINT4 u4CurrIfIndex, UINT4 *pu4NextIfIndex,
                               UINT1 *pau1NextHop)
{

    tTeTnlInfo         *pTeTnlInfo = NULL;
    tP2mpBranchEntry   *pTeP2mpBranchEntry = NULL;
    tOutSegment        *pOutSegment = NULL;
    BOOL1               bFound = TE_FALSE;

    MPLS_CMN_LOCK ();

    if (TeGetTunnelInfoByPrimaryTnlInst (u4TnlIndex, u4TnlInst,
                                         u4LclLsr, u4PeerLsr,
                                         &pTeTnlInfo) != TE_SUCCESS)
    {
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }

    if (!((TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_P2MP) &&
          (NULL != TE_P2MP_TNL_INFO (pTeTnlInfo))))
    {
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }
    if (u4CurrIfIndex == 0)
    {
        pTeP2mpBranchEntry =
            TMO_SLL_First (&(TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo)));
        if (NULL == pTeP2mpBranchEntry)
        {
            MPLS_CMN_UNLOCK ();
            return TE_FAILURE;
        }
        bFound = TE_TRUE;
    }
    else
    {
        TMO_SLL_Scan (&(TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo)),
                      pTeP2mpBranchEntry, tP2mpBranchEntry *)
        {
            if (u4CurrIfIndex == TE_P2MP_BRANCH_OUTSEG_INDEX
                (pTeP2mpBranchEntry))
            {
                pTeP2mpBranchEntry = TMO_SLL_Next
                    (&(TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo)),
                     &(pTeP2mpBranchEntry->NextP2mpBranchEntry));
                if (NULL != pTeP2mpBranchEntry)
                {
                    bFound = TE_TRUE;
                    break;
                }
            }
        }
    }
    if (bFound == TE_FALSE)
    {
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }
    pOutSegment =
        MplsGetOutSegmentTableEntry (TE_P2MP_BRANCH_OUTSEG_INDEX
                                     (pTeP2mpBranchEntry));
    if ((NULL != pOutSegment) && (NULL != OUTSEGMENT_XC_INDEX (pOutSegment)))
    {
        *pu4NextIfIndex = pOutSegment->u4IfIndex;
        MEMCPY (pau1NextHop, pOutSegment->au1NextHopMac, MAC_ADDR_LEN);
        MPLS_CMN_UNLOCK ();
        return TE_SUCCESS;
    }
    else
    {
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }
}

/****************************************************************************/
/* Function Name   : TeTlmUpdateTrafficControl                            */
/* Description     : This function reserves or frees bandwidth for the      */
/*                   tunnel in in TLM module.                               */
/*                                                                          */
/*                   When bandwidth reservation is requested, unreserved    */
/*                   bandwidth is obtained for the tunnel setup priority    */
/*                   from TLM. If bandwidth is available it is reserved.    */
/* Input (s)       : pTeTnlInfo       - Pointer to Tunnel Info              */
/*                   u1ResvRsrc       - Oper Status of Tunnel               */
/* Output (s)      : NONE                                                   */
/* Returns         : TE_FAILURE / TE_SUCCESS                                */
/****************************************************************************/
INT4
TeTlmUpdateTrafficControl (tTeTnlInfo * pTeTnlInfo, UINT1 u1OperStatus,
                           eDirection Direction)
{
    tXcEntry           *pXcEntry = NULL;
    tTeGetAttributes    TeGetAttributes;
    FLOAT               reqResBw = TE_ZERO;
    FLOAT               unResvBw = TE_ZERO;
    FLOAT               availBw = TE_ZERO;
    UINT1               u1HoldingPriority = TE_ZERO;
    UINT1               u1SetupPriority = TE_ZERO;
    UINT1               u1ResvRsrc = TRUE;
    UINT4               u4TeIfIndex = TE_ZERO;
    UINT4               u4L3IfIndex = TE_ZERO;
    UINT4               u4CompIfIndex = TE_ZERO;
    UINT1               u1TeLinkOperStatus = TE_OPER_DOWN;

    MEMSET (&TeGetAttributes, TE_ZERO, sizeof (tTeGetAttributes));

    if ((pTeTnlInfo->pTeTrfcParams == NULL) &&
        (pTeTnlInfo->u4AttrParamIndex == TE_ZERO))
    {
        return TE_SUCCESS;
    }
    /* Get the required attribute values for Te-tunnel information
     * or Attribute list information if exists */
    TeGetAttrFromTnlOrAttrList (&TeGetAttributes, pTeTnlInfo);

    reqResBw = (FLOAT) TeGetAttributes.u4Bandwidth;
    u1HoldingPriority = TeGetAttributes.u1HoldingPriority;
    u1SetupPriority = TeGetAttributes.u1SetupPriority;

    if (reqResBw == TE_ZERO)
    {
        return TE_SUCCESS;
    }
    TE_CONVERT_KBPS_TO_BYTES_PER_SEC (reqResBw);

    if ((Direction == MPLS_DIRECTION_FORWARD) &&
        (pTeTnlInfo->u4DnStrDataTeLinkIf != TE_ZERO))
    {
        u4TeIfIndex = pTeTnlInfo->u4DnStrDataTeLinkIf;
        u4CompIfIndex = pTeTnlInfo->u4FwdComponentIfIndex;
    }
    else if ((Direction == MPLS_DIRECTION_REVERSE) &&
             (pTeTnlInfo->u4UpStrDataTeLinkIf != TE_ZERO))
    {
        u4TeIfIndex = pTeTnlInfo->u4UpStrDataTeLinkIf;
        u4CompIfIndex = pTeTnlInfo->u4RevComponentIfIndex;
    }
    else
    {
        pXcEntry =
            MplsGetXCEntryByDirection (pTeTnlInfo->u4TnlXcIndex, Direction);
        if ((pXcEntry == NULL) || (pXcEntry->pOutIndex == NULL))
        {
            return TE_SUCCESS;
        }

        if (MplsGetL3Intf (pXcEntry->pOutIndex->u4IfIndex, &u4L3IfIndex) ==
            MPLS_FAILURE)
        {
            return TE_FAILURE;
        }

#ifdef CFA_WANTED
#ifdef TLM_WANTED
        CfaApiGetTeLinkIfFromL3If (u4L3IfIndex, &u4TeIfIndex, TE_ONE, TRUE);
#endif
#endif
    }
    if ((u4TeIfIndex == TE_ZERO) || (reqResBw == TE_ZERO))
    {
        return TE_SUCCESS;
    }
    if (u1OperStatus == TE_OPER_DOWN)
    {
        if (pTeTnlInfo->b1IsResourceReserved != TE_TRUE)
        {
            return TE_SUCCESS;
        }
        u1ResvRsrc = FALSE;
        u4CompIfIndex = u4L3IfIndex;

        if (TePortTlmUpdateTeLinkUnResvBw (u4TeIfIndex, u1HoldingPriority,
                                           reqResBw, u1ResvRsrc,
                                           &u4CompIfIndex) == TE_FAILURE)
        {
            return TE_FAILURE;
        }
        pTeTnlInfo->b1IsResourceReserved = TE_FALSE;
        return TE_SUCCESS;
    }

    TePortTlmGetTeLinkUnResvBw (u4TeIfIndex, u1SetupPriority,
                                &unResvBw, &availBw, &u1TeLinkOperStatus);
    if (u1TeLinkOperStatus == TE_OPER_DOWN)
    {
        TE_DBG (TE_EXTN_PRCS,
                "EXTN : TE-LINK down ,returning without reserving bandwidth\n");
        return TE_SUCCESS;
    }
    if (unResvBw < reqResBw)
    {
        TeMainFillErrorTable (TNL_LOCAL_RESOURCES, TE_ZERO, pTeTnlInfo);
        return TE_FAILURE;
    }
    else if (TePortTlmUpdateTeLinkUnResvBw (u4TeIfIndex, u1HoldingPriority,
                                            reqResBw, u1ResvRsrc,
                                            &u4CompIfIndex) == TE_FAILURE)
    {
        TeMainFillErrorTable (TNL_LOCAL_RESOURCES, TE_ZERO, pTeTnlInfo);
        return TE_FAILURE;
    }
    pTeTnlInfo->b1IsResourceReserved = TE_TRUE;
    return TE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : TeCmnExtHandleIfChangeForStatic                        */
/* Description     : This function handles IF Change for Static and P2MP    */
/*                   Tunnels.                                               */
/*                                                                          */
/* Input (s)       : pTeTnlInfo       - Pointer to Tunnel Info              */
/*                   pIfInfo          - Pointer to If Info                  */
/* Output (s)      : NONE                                                   */
/* Returns         : TE_FAILURE / TE_SUCCESS                                */
/****************************************************************************/
INT4
TeCmnExtHandleIfChangeForStatic (tTeTnlInfo * pTeTnlInfo,
                                 tMplsIfEntryInfo * pIfInfo)
{
    tP2mpBranchEntry   *pTeP2mpBranchEntry = NULL;
    tXcEntry           *pXcEntry = NULL;
    tOutSegment        *pOutSegment = NULL;
    tInSegment         *pInSegment = NULL;
    UINT4               u4OutSegIndex = TE_ZERO;

    UINT1               u1OperStatus = TE_OPER_DOWN;
    BOOL1               bMatchFound = FALSE;
    UINT4               u4TnlXcIndex = TE_ZERO;

    if (pIfInfo->u1OperStatus == CFA_IF_UP)
    {
        u1OperStatus = TE_OPER_UP;
    }

    /* Handle interface status change event for ingress and transit LSR.
     * At the egress of P2MP tunnel, interface status change handling is
     * similar to point-to-poimt tunnel */
    if ((TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_P2MP)
        && ((TE_TNL_ROLE (pTeTnlInfo) == TE_INGRESS)
            || (TE_TNL_ROLE (pTeTnlInfo) == TE_INTERMEDIATE)))
    {
        TE_DBG1 (TE_MAIN_PRCS, "MAIN: P2MP tunnel index %d\n",
                 TE_TNL_TNL_INDEX (pTeTnlInfo));
        if (NULL == TE_P2MP_TNL_INFO (pTeTnlInfo))
        {
            TE_DBG (TE_MAIN_FAIL, "MAIN: P2MP tunnel info does not exist\n");

            return TE_FAILURE;
        }

        /* Scan P2MP branch out-segment list for interface status change */
        TMO_SLL_Scan (&(TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo)),
                      pTeP2mpBranchEntry, tP2mpBranchEntry *)
        {
            u4OutSegIndex = TE_P2MP_BRANCH_OUTSEG_INDEX (pTeP2mpBranchEntry);
            pOutSegment = MplsGetOutSegmentTableEntry (u4OutSegIndex);
            if (NULL == pOutSegment)
            {
                TE_DBG (TE_MAIN_FAIL,
                        "MAIN: Out-segment for the branch "
                        "entry does not exist\n");
                continue;
            }

            pXcEntry = OUTSEGMENT_XC_INDEX (pOutSegment);
            if (pXcEntry == NULL)
            {
                TE_DBG (TE_MAIN_FAIL, "MAIN: XC row entry does not " "exist\n");
                continue;
            }

            pInSegment = XC_ININDEX (pXcEntry);
            if ((NULL != pInSegment)
                && (INSEGMENT_IF_INDEX (pInSegment) == pIfInfo->u4IfIndex))
            {
                /* Do not break here. Update XC oper status for all XC rows 
                 * that share the same incoming IF */
                MplsDbUpdateXcOperStatus (pXcEntry, FALSE, u1OperStatus,
                                          XC_INSEGMENT_FAULT);
                bMatchFound = TRUE;
            }
            else if (OUTSEGMENT_IF_INDEX (pOutSegment) == pIfInfo->u4IfIndex)
            {
                MplsDbUpdateXcOperStatus (pXcEntry, FALSE, u1OperStatus,
                                          XC_OUTSEGMENT_FAULT);
                /* Configure P2MP ILM/Tunnel in HW */
                if (MplsDbUpdateP2mpTunnelInHw
                    (pTeTnlInfo, pXcEntry, u1OperStatus) == MPLS_FAILURE)
                {
                    TE_DBG (TE_MAIN_FAIL,
                            "MAIN: MplsDbUpdateP2mpTunnelInHw failed\r\n");
                }
                /* Processed interface status change event. Break from 
                 * branch out-segment list. */
                bMatchFound = TRUE;
                break;
            }
        }

        if (FALSE == bMatchFound)
        {
            return TE_FAILURE;
        }

        /* Update tunnel oper status in HW if incoming interface status has 
         * changed */
        if ((NULL != pInSegment)
            && (INSEGMENT_IF_INDEX (pInSegment) == pIfInfo->u4IfIndex))
        {
            /* When all outgoing interfaces are already down, do not 
             * program HW */
            if ((TE_ZERO == TE_P2MP_TNL_ACTIVE_BRANCH_COUNT (pTeTnlInfo))
                && (TE_OPER_DOWN == u1OperStatus))
            {
                TE_DBG (TE_MAIN_ETEXT,
                        "MAIN: Incoming interface status change. "
                        "No outgoing interface is operational. Exiting\n");

                /* Processed interface status change event for P2MP tunnel 
                 * that matched the incoming interface. So, return
                 * TE_SUCCESS */
                return TE_SUCCESS;
            }

            if (TeP2mpExtSetOperStatusAndProgHw (pTeTnlInfo, u1OperStatus)
                == TE_FAILURE)
            {
                TE_DBG (TE_MAIN_FAIL,
                        "MAIN: Incoming interface status change. Unable "
                        "to program tunnel operational status in HW\n");
            }
        }

        /* Processed interface status change event for P2MP tunnel 
         * that matched the interface. So, return TE_SUCCESS */
        return TE_SUCCESS;
    }

    /* Handle Normal static and P2MP Static at egress Case */
    /* Ignore the Oper UP validation for Working tunnel when LOCAL_PROT_IN_USE */
    if ((u1OperStatus == TE_OPER_UP) &&
        (pTeTnlInfo->u1CPOrMgmtOperStatus == TE_OPER_UP))
    {
        return TE_FAILURE;
    }

    if ((u1OperStatus == TE_OPER_DOWN) &&
        (pTeTnlInfo->u1CPOrMgmtOperStatus == TE_OPER_DOWN))
    {
        return TE_FAILURE;
    }
    if (pTeTnlInfo->u4OrgTnlXcIndex != TE_ZERO)
    {
        u4TnlXcIndex = pTeTnlInfo->u4OrgTnlXcIndex;
    }
    else
    {
        u4TnlXcIndex = pTeTnlInfo->u4TnlXcIndex;
    }

    pXcEntry = MplsGetXCEntryByDirection (u4TnlXcIndex, MPLS_DIRECTION_FORWARD);

    if (pXcEntry == NULL)
    {
        return TE_FAILURE;
    }

    if ((pXcEntry->pInIndex != NULL) &&
        (pXcEntry->pInIndex->u4IfIndex == pIfInfo->u4IfIndex))
    {
        MplsDbUpdateXcOperStatus (pXcEntry, FALSE, pIfInfo->u1OperStatus,
                                  XC_INSEGMENT_FAULT);
        bMatchFound = TRUE;
    }
    else if ((pXcEntry->pOutIndex != NULL) &&
             (pXcEntry->pOutIndex->u4IfIndex == pIfInfo->u4IfIndex))
    {
        MplsDbUpdateXcOperStatus (pXcEntry, FALSE, pIfInfo->u1OperStatus,
                                  XC_OUTSEGMENT_FAULT);
        bMatchFound = TRUE;
    }

    if (pTeTnlInfo->u4TnlMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
    {
        pXcEntry = MplsGetXCEntryByDirection (u4TnlXcIndex,
                                              MPLS_DIRECTION_REVERSE);

        if (pXcEntry == NULL)
        {
            return TE_FAILURE;
        }

        if ((pXcEntry->pInIndex != NULL) &&
            (pXcEntry->pInIndex->u4IfIndex == pIfInfo->u4IfIndex))
        {
            MplsDbUpdateXcOperStatus (pXcEntry, FALSE,
                                      pIfInfo->u1OperStatus,
                                      XC_INSEGMENT_FAULT);
            bMatchFound = TRUE;
        }
        else if ((pXcEntry->pOutIndex != NULL) &&
                 (pXcEntry->pOutIndex->u4IfIndex == pIfInfo->u4IfIndex))
        {
            MplsDbUpdateXcOperStatus (pXcEntry, FALSE,
                                      pIfInfo->u1OperStatus,
                                      XC_OUTSEGMENT_FAULT);
            bMatchFound = TRUE;
        }
    }

    if (bMatchFound == FALSE)
    {
        return TE_FAILURE;
    }

    if (TeCmnExtSetOperStatusAndProgHw (pTeTnlInfo, u1OperStatus) == TE_FAILURE)
    {
        return TE_FAILURE;
    }

    /* Processed the interface status change event for a tunnel
     * that matched the interface. So, return TE_SUCCESS */
    return TE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : TeGrDeleteNonSynchronizedTnls                          */
/* Description     : This function deletes non-synchronized tunnels         */
/* Input (s)       : NONE                                                   */
/* Output (s)      : NONE                                                   */
/* Returns         : NONE                                                   */
/****************************************************************************/

VOID
TeGrDeleteNonSynchronizedTnls ()
{
    tTeTnlInfo         *pTmpTeTnlInfo = NULL;
    tXcEntry           *pXcEntry = NULL;

    pTmpTeTnlInfo = RBTreeGetFirst (gTeGblInfo.TnlInfoTbl);

    while (pTmpTeTnlInfo != NULL)
    {
        if (pTmpTeTnlInfo->u1TnlSyncStatus == TNL_GR_NOT_SYNCHRONIZED)
        {
            /* Remove the H/W programming and release banwidth in TLM */
            if (TeCmnExtSetOperStatusAndProgHw (pTmpTeTnlInfo, TE_OPER_DOWN)
                == TE_FAILURE)
            {
                TE_DBG (TE_MAIN_FAIL,
                        "TeCmnExtSetOperStatusAndProgHw failed\r\n");
            }

            /* Release Label from Label manager */
            pXcEntry = MplsGetXCEntryByDirection (pTmpTeTnlInfo->u4TnlXcIndex,
                                                  MPLS_DIRECTION_FORWARD);
            if (pXcEntry != NULL)
            {
                if ((pXcEntry->pInIndex != NULL) &&
                    (pXcEntry->pInIndex->u4Label != MPLS_INVALID_LABEL))
                {
                    if (LblMgrRelLblToLblGroup (gu2GenLblSpaceGrpId,
                                                TE_ZERO,
                                                pXcEntry->pInIndex->u4Label)
                        == LBL_FAILURE)
                    {
                        return;
                    }
                }
                MplsDeleteXCTableEntry (pXcEntry);
                if (pXcEntry->pInIndex != NULL)
                {
                    MplsDeleteInSegmentTableEntry (pXcEntry->pInIndex);
                }
                if (pXcEntry->pOutIndex != NULL)
                {
                    MplsDeleteOutSegmentTableEntry (pXcEntry->pOutIndex);
                }
            }

            pXcEntry = MplsGetXCEntryByDirection (pTmpTeTnlInfo->u4TnlXcIndex,
                                                  MPLS_DIRECTION_REVERSE);
            if (pXcEntry != NULL)
            {
                if ((pXcEntry->pInIndex != NULL) &&
                    (pXcEntry->pInIndex->u4Label != MPLS_INVALID_LABEL))
                {
                    if (LblMgrRelLblToLblGroup (gu2GenLblSpaceGrpId,
                                                TE_ZERO,
                                                pXcEntry->pInIndex->u4Label)
                        == LBL_FAILURE)
                    {
                        return;
                    }
                }
                MplsDeleteXCTableEntry (pXcEntry);
                if (pXcEntry->pInIndex != NULL)
                {
                    MplsDeleteInSegmentTableEntry (pXcEntry->pInIndex);
                }
                if (pXcEntry->pOutIndex != NULL)
                {
                    MplsDeleteOutSegmentTableEntry (pXcEntry->pOutIndex);
                }
            }
            TeDeleteTnlInfo (pTmpTeTnlInfo, TE_TNL_CALL_FROM_SIG);
        }
        pTmpTeTnlInfo = RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                       (tRBElem *) pTmpTeTnlInfo, NULL);
    }
    return;
}

/****************************************************************************/
/* Function Name   : TeUpdateTunnelOperStatusAndProgHw                      */
/* Description     : This function updates common database and tunnel       */
/*                   operstatus.                                            */
/*                                                                          */
/* Input (s)       : pTeTnlInfo       - Pointer to Tunnel Info              */
/*                   u1TnlOperStatus  - Operational status to be set on     */
/*                                      the tunnel                          */
/* Output (s)      : NONE                                                   */
/* Returns         : TE_FAILURE / TE_SUCCESS                                */
/****************************************************************************/

INT4
TeUpdateTunnelOperStatusAndProgHw (tTeTnlInfo * pTeTnlInfo,
                                   UINT1 u1TnlOperStatus)
{

    UINT1               u1IsRpteAdminDown = TE_FALSE;

    switch (u1TnlOperStatus)
    {
        case TE_OPER_UP:

            if (TE_TNL_OPER_STATUS (pTeTnlInfo) == TE_OPER_UP)
            {
                break;
            }
            if (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_RSVP)
            {
                if (pTeTnlInfo->u1TnlMBBStatus == MPLS_TE_MBB_DISABLED)
                {
                    pTeTnlInfo->bIsMbbRequired = FALSE;
                }

                if (TeRpteTEEventHandler (TE_TNL_UP,
                                          pTeTnlInfo, &u1IsRpteAdminDown)
                    == TE_RPTE_FAILURE)
                {
                    return TE_FAILURE;
                }
            }
            else if (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_LDP)
            {
                if (TeLdpTEEventHandler (TE_DATA_TX_ENABLE,
                                         pTeTnlInfo) != TE_LDP_SUCCESS)
                {
                    return TE_FAILURE;
                }
            }
            else
            {
                /* MPLS_P2MP_LSP_CHANGES - S */
                if ((MplsCheckP2mpTnlRoleNotEgress (pTeTnlInfo))
                    == MPLS_SUCCESS)
                {
                    if (TeP2mpExtSetOperStatusAndProgHw (pTeTnlInfo, TE_OPER_UP)
                        == TE_FAILURE)
                    {
                        return TE_FAILURE;
                    }
                }

                /* MPLS_P2MP_LSP_CHANGES - E */
                else if (TeCmnExtSetOperStatusAndProgHw (pTeTnlInfo, TE_OPER_UP)
                         == TE_FAILURE)
                {
                    return TE_FAILURE;
                }
            }
            break;
        case TE_OPER_DOWN:

            if (TE_TNL_OPER_STATUS (pTeTnlInfo) == TE_OPER_DOWN)
            {
                break;
            }

            if (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_RSVP)
            {
                if ((TeRpteTEEventHandler (TE_TNL_DOWN,
                                           pTeTnlInfo, &u1IsRpteAdminDown))
                    == TE_RPTE_FAILURE)
                {
                    return TE_FAILURE;
                }
            }
            else if (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_LDP)
            {
                if (TeLdpTEEventHandler (TE_DATA_TX_DISABLE,
                                         pTeTnlInfo) != TE_LDP_SUCCESS)
                {
                    return TE_FAILURE;
                }
            }
            else
            {
                /* MPLS_P2MP_LSP_CHANGES - S */
                if ((MplsCheckP2mpTnlRoleNotEgress (pTeTnlInfo))
                    == MPLS_SUCCESS)
                {
                    if (TeP2mpExtSetOperStatusAndProgHw (pTeTnlInfo,
                                                         TE_OPER_DOWN) ==
                        TE_FAILURE)
                    {
                        return TE_FAILURE;
                    }
                }
                /* MPLS_P2MP_LSP_CHANGES - E */
                else if (TeCmnExtSetOperStatusAndProgHw (pTeTnlInfo,
                                                         TE_OPER_DOWN) ==
                         TE_FAILURE)
                {
                    return TE_FAILURE;
                }
            }
            break;
        default:
            return TE_FAILURE;
    }
    return TE_SUCCESS;
}
