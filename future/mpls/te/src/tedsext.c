
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tedsext.c,v 1.8 2016/07/22 09:45:48 siva Exp $
 *
 * Description: This file contains all the functions exported to
 *              the signalling protocols for the DiffServ Support.
 *              It includes
 *              Creation and Deletion of DiffServTunnel
 *              Creation and Deletion of ElspList
 *              Updation of the ElspList with associated traffic parameters.
 *----------------------------------------------------------------------------*/

#include "teincs.h"

/*****************************************************************************/
/* Function Name : TeSigCreateDiffServTnl                                       
 * Description   : This routine allocates memory for new TeDiffServ TunnelInfo,
 *                 copies the contents provided by the Temporary pointer.
 * Input(s)      : pNewDiffServTnlInfo - Pointer to Temporary DSTnlInfo.   
 * Output(s)     : ppMplsDiffServTnlInfo - Address of the Pointer of 
 *                 DSTnlInfo in case of successful allocation.      
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                 
 *****************************************************************************/
UINT1
TeSigCreateDiffServTnl (tMplsDiffServTnlInfo ** ppMplsDiffServTnlInfo,
                        tMplsDiffServTnlInfo * pNewDiffServTnlInfo)
{
    UINT1               u1RetVal = TE_FAILURE;

    MPLS_CMN_LOCK ();

    u1RetVal = TeCreateDiffServTnl (ppMplsDiffServTnlInfo, pNewDiffServTnlInfo);

    MPLS_CMN_UNLOCK ();

    return u1RetVal;
}

/*****************************************************************************/
/* Function Name : TeSigDeleteDiffServTnl
 * Description   : This function releases the memory of the DiffServTnlIfo
 *                 to the DiffServTnlPool and its associated 
 *                 ElspInfo's and TrfcParams.         
 * Input(s)      : pMplsDiffServTnlInfo - pointer to DsTnlInfo Structure
 * Output(s)     : NONE           
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                 
 *****************************************************************************/
UINT1
TeSigDeleteDiffServTnl (tMplsDiffServTnlInfo * pMplsDiffServTnlInfo)
{

    TE_DBG (TE_DIFF_ETEXT, "EXTN : TeSigDeleteDiffServTnl : ENTRY \n");
    /* Admin Status Check made in All Exported Functions */
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        TE_DBG (TE_DIFF_FAIL,
                "EXTN : TE Admin Status DOWN. No Further processing \n");
        TE_DBG (TE_DIFF_ETEXT, "EXTN : TeSigDeleteDiffServTnl : INTMD-EXIT \n");
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }
    /* Don't Allow to Hack the Code */
    if (pMplsDiffServTnlInfo == NULL)
    {
        TE_DBG (TE_DIFF_FAIL,
                "EXTN : Null Pointer provided for MplsDiffServTnlInfo\n");
        TE_DBG (TE_DIFF_ETEXT, "EXTN : TeSigDeleteDiffServTnl : INTMD-EXIT \n");
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }

    /* The ElspList and the associated ElspInfo are deleted */
    if (TE_DS_TNL_ELSP_INFO_LIST_INFO (pMplsDiffServTnlInfo) != NULL)
    {
        TE_DBG (TE_DIFF_PRCS, "EXTN : ELspList Deletion called \n");
        TeDeleteDiffServElspList
            (TE_DS_TNL_ELSP_INFO_LIST_INFO (pMplsDiffServTnlInfo));
        TE_DS_TNL_ELSP_INFO_LIST_INFO (pMplsDiffServTnlInfo) = NULL;
    }
    if (TE_REL_MEM_BLOCK (TE_DS_TNL_INFO_POOL_ID,
                          (UINT1 *) (pMplsDiffServTnlInfo)) == TE_MEM_FAILURE)
    {

        TE_DBG (TE_DIFF_FAIL, "EXTN : MplsDiffServTnlInfo Deletion failed \n");
        TE_DBG (TE_DIFF_ETEXT, "EXTN : TeSigDeleteDiffServTnl : EXIT \n");
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }

    TE_DBG (TE_DIFF_ETEXT, "EXTN : TeSigDeleteDiffServTnl : EXIT \n");
    MPLS_CMN_UNLOCK ();
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeSigCreateDiffServElspList                                  
 * Description   : This routine reserves a DiffServElspList Listpointer from    
 *                 the global array and copies the ELspInfoList provided by the 
 *                 signalling protocols.                                     
 * Input(s)      : pElspInfoList - Pointer to ElspInfoList   
 * Output(s)     : ppMplsDiffServElspList - Address of the Pointer to 
 *                 tMplsDiffServElspList structure    
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                   
 *****************************************************************************/
UINT1
TeSigCreateDiffServElspList (tMplsDiffServElspList ** ppMplsDiffServElspList,
                             tTeSll * pElspInfoList)
{
    UINT4               u4Count;
    tMplsDiffServElspInfo *pTempElspInfo = NULL;
    tMplsDiffServElspInfo *pTeElspInfo = NULL;

    TE_DBG (TE_DIFF_ETEXT, "EXTN : TeSigCreateDiffServElspList : ENTRY \n");
    MPLS_CMN_LOCK ();
    /* Admin Status Check made in All Exported Functions */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        TE_DBG (TE_DIFF_FAIL,
                "EXTN : TE Admin Status DOWN. No Further processing \n");
        TE_DBG (TE_DIFF_ETEXT,
                "EXTN : TeSigCreateDiffServElspList : INTMD-EXIT \n");
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }
    /* Don't Allow to Hack the Code */
    if ((ppMplsDiffServElspList == NULL) || (pElspInfoList == NULL))
    {
        TE_DBG (TE_DIFF_FAIL, "EXTN : Null Pointer provided in ElspList \n");
        TE_DBG (TE_DIFF_ETEXT,
                "EXTN : TeSigCreateDiffServElspList : INTMD-EXIT \n");
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }

    /* A valid ElspInfoListIndex is  obtained */
    for (u4Count = TE_ONE; u4Count <= TE_MAX_DS_ELSPS (gTeGblInfo); u4Count++)
    {
        if (TE_DS_GBL_ELSP_INFO_LIST_INDEX (u4Count) == TE_ZERO)
        {
            *ppMplsDiffServElspList = TE_DS_GBL_ELSP_INFO_LIST_ENTRY (u4Count);
            TE_DS_GBL_ELSP_INFO_LIST_INDEX (u4Count) = u4Count;
            TE_DBG1 (TE_DIFF_PRCS,
                     "EXTN : In TeSigCreateDiffServElspList allocated "
                     "ElspListIndex = %d \n", u4Count);
            break;
        }
        if (u4Count == TE_MAX_DS_ELSPS (gTeGblInfo))
        {
            TE_DBG (TE_DIFF_ETEXT,
                    "EXTN : TeSigCreateDiffServElspList : INTMD_EXIT \n");
            MPLS_CMN_UNLOCK ();
            return TE_FAILURE;
        }
    }

    /* The Elsp Info list is scanned and each node from the Templist is 
       copied into the Te allocated list */
    /*
       TE_SLL_INIT (&((tMplsDiffServElspList*)*ppMplsDiffServElspList)->ElspInfoList);
     */
    TE_SLL_INIT (TE_DS_GBL_ELSP_INFO_LIST (u4Count));
    TE_SLL_SCAN (pElspInfoList, pTempElspInfo, tMplsDiffServElspInfo *)
    {
        pTeElspInfo = (tMplsDiffServElspInfo *)
            TE_ALLOC_MEM_BLOCK (TE_DS_ELSP_INFO_POOL_ID);

        if (pTeElspInfo == NULL)
        {
            TE_DBG (TE_DIFF_FAIL,
                    "EXTN : MemAllocation failed for ElspInfo \n");
            TE_DBG (TE_DIFF_ETEXT,
                    "EXTN : TeSigCreateDiffServElspList : INTMD_EXIT \n");
            TeDeleteDiffServElspList (*ppMplsDiffServElspList);
            MPLS_CMN_UNLOCK ();
            return TE_FAILURE;
        }
        MEMCPY (pTeElspInfo, pTempElspInfo, sizeof (tMplsDiffServElspInfo));

        TE_SLL_ADD (TE_DS_GBL_ELSP_INFO_LIST (u4Count),
                    &(pTeElspInfo->ElspInfoNext));
    }

    MPLS_CMN_UNLOCK ();
    TE_DBG (TE_DIFF_ETEXT, "EXTN : TeSigCreateDiffServElspList : EXIT \n");
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeSigDeleteDiffServElspList 
 * Description   : Deletes memory for the DiffServElspList and releases the
 *                 memory associated to the ElspInfo and the Traffic Params
 *                 associated if any. This is called from Signal Module
 * Input(s)      : pMplsDiffServElspList - Pointer to tMplsDiffServElspList
 * Output(s)     : NONE              
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                   
 *****************************************************************************/
UINT1
TeSigDeleteDiffServElspList (tMplsDiffServElspList * pMplsDiffServElspList)
{
    UINT1               u1Return = TE_FAILURE;
    MPLS_CMN_LOCK ();
    u1Return = TeDeleteDiffServElspList (pMplsDiffServElspList);
    MPLS_CMN_UNLOCK ();
    return u1Return;
}

/*****************************************************************************/
/* Function Name : TeDeleteDiffServElspList 
 * Description   : Deletes memory for the DiffServElspList and releases the
 *                 memory associated to the ElspInfo and the Traffic Params
 *                 associated if any.
 * Input(s)      : pMplsDiffServElspList - Pointer to tMplsDiffServElspList
 * Output(s)     : NONE              
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                   
 *****************************************************************************/
UINT1
TeDeleteDiffServElspList (tMplsDiffServElspList * pMplsDiffServElspList)
{
    tMplsDiffServElspInfo *pTempElspInfo = NULL;
    tTeSll             *pElspInfoList = NULL;

    TE_DBG (TE_DIFF_ETEXT, "EXTN : TeDeleteDiffServElspList : ENTRY \n");
    /* Admin Status Check made in All Exported Functions */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        TE_DBG (TE_DIFF_FAIL,
                "EXTN : TE Admin Status DOWN. No Further processing \n");
        TE_DBG (TE_DIFF_ETEXT,
                "EXTN : TeDeleteDiffServElspList : INTMD-EXIT \n");
        return TE_FAILURE;
    }

    /* Don't Allow to Hack the Code */
    if (pMplsDiffServElspList == NULL)
    {
        TE_DBG (TE_DIFF_FAIL, "EXTN : Null Pointer provided for ElspList \n");
        TE_DBG (TE_DIFF_ETEXT,
                "EXTN : TeDeleteDiffServElspList : INTMD-EXIT \n");
        return TE_FAILURE;
    }
    pElspInfoList = TE_DS_ELSP_INFO_LIST (pMplsDiffServElspList);

    /*Check if the given TeArHopInfo is there in the TeArHopList */

    pTempElspInfo = (tMplsDiffServElspInfo *) TMO_SLL_First (pElspInfoList);
    while (pTempElspInfo != NULL)
    {
        if (TE_DS_ELSP_INFO_PSC_TRFC_PARAMS (pTempElspInfo) != NULL)
        {
            TeDeleteTrfcParams (TE_DS_ELSP_INFO_PSC_TRFC_PARAMS
                                (pTempElspInfo));
        }
        TE_SLL_DELETE (pElspInfoList, &(pTempElspInfo->ElspInfoNext));
        MEMSET (pTempElspInfo, TE_ZERO, sizeof (tMplsDiffServElspInfo));
        if (TE_REL_MEM_BLOCK (TE_DS_ELSP_INFO_POOL_ID,
                              (UINT1 *) pTempElspInfo) == TE_MEM_FAILURE)
        {
            TE_DBG (TE_DIFF_FAIL, "EXTN : ElspInfo Deletion failed -  Log \n");
        }
        pTempElspInfo = (tMplsDiffServElspInfo *) TMO_SLL_First (pElspInfoList);
    }
    INIT_TE_DS_ELSP_LIST_INFO (pMplsDiffServElspList);
    TE_DBG (TE_DIFF_PRCS, "EXTN : ElspList and ElspListInfo are removed \n");
    TE_DBG (TE_DIFF_ETEXT, "EXTN : TeDeleteDiffServElspList : EXIT \n");
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeSigUpdateTrfcParamsElspList
 * Description   : This function updates the Traffic params associated with the
 *                 ElspInfo & removes the previous traffic params if any.
 *                 The valid operation permitted are ADD/DEL/UPDATE.
 *                 Based on the Index the Traffic Params are added to the
 *                 appropriate ElspInfo.This is called from Signal Module
 * Input(s)      : pMplsDiffServElspList - Pointer to tMplsDiffServElspList
 *               : pTempTrfcParams       - Pointer to Temp traffic params
 *               : u1ElspInfoIndex       - ElspInfoIndex
 *               : u1Operation           - Opearation to be performed.
 * Output(s)     : NONE              
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                   
 *****************************************************************************/
UINT1
TeSigUpdateTrfcParamsElspList (tMplsDiffServElspList * pMplsDiffServElspList,
                               tTeTrfcParams * pTempTrfcParams,
                               UINT1 u1ElspInfoIndex, UINT1 u1Operation)
{
    UINT1               u1Return = TE_FAILURE;
    MPLS_CMN_LOCK ();
    u1Return =
        TeUpdateTrfcParamsElspList (pMplsDiffServElspList, pTempTrfcParams,
                                    u1ElspInfoIndex, u1Operation);
    MPLS_CMN_UNLOCK ();
    return u1Return;
}

/*****************************************************************************/
/* Function Name : TeUpdateTrfcParamsElspList
 * Description   : This function updates the Traffic params associated with the
 *                 ElspInfo & removes the previous traffic params if any.
 *                 The valid operation permitted are ADD/DEL/UPDATE.
 *                 Based on the Index the Traffic Params are added to the
 *                 appropriate ElspInfo.
 * Input(s)      : pMplsDiffServElspList - Pointer to tMplsDiffServElspList
 *               : pTempTrfcParams       - Pointer to Temp traffic params
 *               : u1ElspInfoIndex       - ElspInfoIndex
 *               : u1Operation           - Opearation to be performed.
 * Output(s)     : NONE              
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                   
 *****************************************************************************/
UINT1
TeUpdateTrfcParamsElspList (tMplsDiffServElspList * pMplsDiffServElspList,
                            tTeTrfcParams * pTempTrfcParams,
                            UINT1 u1ElspInfoIndex, UINT1 u1Operation)
{
    tMplsDiffServElspInfo *pTempElspInfo = NULL;
    tTeSll             *pElspInfoList = NULL;
    tTeTrfcParams      *pTeTrfcParms = NULL;
    UINT1               u1Proto = TE_SIGPROTO_NONE;
    UINT1               u1UpdationFlag = TE_FALSE;

    TE_DBG (TE_DIFF_ETEXT, "EXTN : TeUpdateTrfcParamsElspList : ENTRY \n");
    /* Admin Status Check made in All Exported Functions */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        TE_DBG (TE_DIFF_FAIL,
                "EXTN : TE Admin Status DOWN. No Further processing \n");
        TE_DBG (TE_DIFF_ETEXT,
                "EXTN : TeUpdateTrfcParamsElspList : INTMD-EXIT \n");
        return TE_FAILURE;
    }

    /* Don't Allow to Hack the Code */
    if ((pMplsDiffServElspList == NULL) || (pTempTrfcParams == NULL))
    {
        TE_DBG (TE_DIFF_FAIL, "EXTN : Null Pointer provided for ElspList \n");
        TE_DBG (TE_DIFF_ETEXT,
                "EXTN : TeUpdateTrfcParamsElspList : INTMD-EXIT \n");
        return TE_FAILURE;
    }
    if (u1ElspInfoIndex > MAX_DS_EXP) /* || (u1ElspInfoIndex < TE_DS_MIN_EXP)     u1ElspInfoIndex is UINT1 so therefore, TE_DS_MIN_EXP condition is explicit */
    {
        TE_DBG (TE_DIFF_FAIL, "EXTN : Invalid ElspInfoIndex provided \n");
        TE_DBG (TE_DIFF_ETEXT,
                "EXTN : TeUpdateTrfcParamsElspList : INTMD-EXIT \n");
        return TE_FAILURE;
    }
    pElspInfoList = TE_DS_ELSP_INFO_LIST (pMplsDiffServElspList);

    TE_SLL_SCAN (pElspInfoList, pTempElspInfo, tMplsDiffServElspInfo *)
    {
        if (TE_DS_ELSP_INFO_INDEX (pTempElspInfo) == u1ElspInfoIndex)
        {
            switch (u1Operation)
            {
                case TE_TP_ADD:
                {
                    /* Creation of Traffic Params from the TrafficParmPool
                     */
                    /* One Approach */
                    if ((TE_CRLDP_TRFC_PARAMS (pTempTrfcParams) != NULL) &&
                        (TE_RSVPTE_TRFC_PARAMS (pTempTrfcParams) != NULL))
                    {
                        TE_DBG (TE_DIFF_FAIL,
                                "EXTN : Improper traffic param received \n");
                        TE_DBG (TE_DIFF_ETEXT,
                                "EXTN : TeUpdateTrfcParamsElspList : "
                                "INTMD-EXIT \n");
                        return TE_FAILURE;
                    }
                    if (TE_CRLDP_TRFC_PARAMS (pTempTrfcParams) != NULL)
                    {
                        u1Proto = TE_SIGPROTO_LDP;
                    }
                    else if (TE_RSVPTE_TRFC_PARAMS (pTempTrfcParams) != NULL)
                    {
                        u1Proto = TE_SIGPROTO_RSVP;
                    }
                    else
                    {
                        TE_DBG (TE_DIFF_FAIL,
                                "EXTN : Invalid traffic param received \n");
                        TE_DBG (TE_DIFF_ETEXT,
                                "EXTN : TeUpdateTrfcParamsElspList : "
                                "INTMD-EXIT \n");
                        return TE_FAILURE;
                    }
                    if (TeCreateTrfcParams
                        (u1Proto, &pTeTrfcParms, pTempTrfcParams) == TE_SUCCESS)
                    {

                        TE_DS_ELSP_INFO_PSC_TRFC_PARAMS (pTempElspInfo)
                            = pTeTrfcParms;
                        TE_DBG (TE_DIFF_PRCS,
                                "EXTN : Traffic Params is Added properly \n");
                        u1UpdationFlag = TE_TRUE;
                    }
                    /* Other One  - Verify - Is it good this way */
                    /* 
                       TE_DS_ELSP_INFO_PSC_TRFC_PARAMS(pTempElspInfo) 
                       = pTempTrfcParams;
                       TE_DBG (TE_DIFF_PRCS, 
                       "EXTN : Traffic Params is Added properly \n"); 
                       u1UpdationFlag = TE_TRUE; 
                     */
                    break;
                }
                case TE_TP_DEL:
                {
                    if (TE_DS_ELSP_INFO_PSC_TRFC_PARAMS (pTempElspInfo) != NULL)
                    {
                        if (TeDeleteTrfcParams
                            (TE_DS_ELSP_INFO_PSC_TRFC_PARAMS (pTempElspInfo)) ==
                            TE_SUCCESS)
                        {
                            TE_DBG (TE_DIFF_PRCS,
                                    "EXTN : Traffic Params is Deleted properly \n");
                            TE_DS_ELSP_INFO_PSC_TRFC_PARAMS (pTempElspInfo) =
                                NULL;
                            u1UpdationFlag = TE_TRUE;
                        }
                        else
                        {
                            TE_DBG (TE_DIFF_FAIL,
                                    "EXTN : Traffic params can't be deleted \n");
                            TE_DBG (TE_DIFF_ETEXT,
                                    "EXTN : TeUpdateTrfcParamsElspList : "
                                    "INTMD-EXIT \n");
                            return TE_FAILURE;
                        }
                    }
                    else
                    {
                        TE_DBG (TE_DIFF_FAIL,
                                "EXTN : Invalid Traffic parameters \n");
                        TE_DBG (TE_DIFF_ETEXT,
                                "EXTN : TeUpdateTrfcParamsElspList : "
                                "INTMD-EXIT \n");
                        return TE_FAILURE;
                    }
                    break;
                }
                case TE_TP_UPDATE:
                {
                    if ((TeUpdateTrfcParamsElspList (pMplsDiffServElspList,
                                                     pTempTrfcParams,
                                                     u1ElspInfoIndex,
                                                     TE_TP_DEL) == TE_SUCCESS)
                        &&
                        (TeUpdateTrfcParamsElspList
                         (pMplsDiffServElspList, pTempTrfcParams,
                          u1ElspInfoIndex, TE_TP_ADD) == TE_SUCCESS))
                    {
                        TE_DBG (TE_DIFF_PRCS,
                                "EXTN : Traffic Params is updated properly \n");
                        u1UpdationFlag = TE_TRUE;
                    }
                    break;
                }
                default:
                    TE_DBG (TE_DIFF_FAIL,
                            "EXTN : Invalid ElspInfoIndex provided \n");
                    TE_DBG (TE_DIFF_ETEXT,
                            "EXTN : TeUpdateTrfcParamsElspList : INTMD-EXIT \n");
                    return TE_FAILURE;

            }
            if (u1UpdationFlag == TE_TRUE)
            {
                break;
            }
        }
    }
    if (u1UpdationFlag == TE_TRUE)
    {
        TE_DBG1 (TE_DIFF_PRCS,
                 "EXTN : Traffic Params is updated in ElspInfoIndex : %d \n",
                 u1ElspInfoIndex);
        TE_DBG (TE_DIFF_ETEXT, "EXTN : TeUpdateTrfcParamsElspList : EXIT \n");
        return TE_SUCCESS;
    }
    TE_DBG (TE_DIFF_FAIL, "EXTN : Traffic Params cannot be updated  \n");
    TE_DBG (TE_DIFF_ETEXT, "EXTN : TeUpdateTrfcParamsElspList : EXIT \n");
    return TE_FAILURE;
}

/******************************************************************************
* Function Name : teDiffServGetIndexForPsc                                 
* Description   : This routine is called to get the index for the PSC.
*
* Input(s)      : u1PhbDscp - Dscp value for which the index is needed
*                 
* Output(s)     : None                                                     
* Return(s)     : Index of the Dscp value                     
*******************************************************************************/
UINT1
TeSigDiffServGetPhbPsc (UINT1 u1PhbDscp)
{

    TE_DBG (TE_DIFF_ETEXT, "MAIN : TeSigDiffServGetPhbPsc : ENTRY \n");
    switch (u1PhbDscp)
    {
        case TE_DS_AF11_DSCP:
        case TE_DS_AF12_DSCP:
        case TE_DS_AF13_DSCP:
            TE_DBG (TE_DIFF_PRCS, "MAIN : DSCP = AF11 \n");
            TE_DBG (TE_DIFF_ETEXT, "MAIN : TeSigDiffServGetPhbPsc : EXIT \n");
            return TE_DS_AF11_DSCP;
        case TE_DS_AF21_DSCP:
        case TE_DS_AF22_DSCP:
        case TE_DS_AF23_DSCP:
            TE_DBG (TE_DIFF_PRCS, "MAIN : DSCP = AF21 \n");
            TE_DBG (TE_DIFF_ETEXT, "MAIN : TeSigDiffServGetPhbPsc : EXIT \n");
            return TE_DS_AF21_DSCP;
        case TE_DS_AF31_DSCP:
        case TE_DS_AF32_DSCP:
        case TE_DS_AF33_DSCP:
            TE_DBG (TE_DIFF_PRCS, "MAIN : DSCP = AF31 \n");
            TE_DBG (TE_DIFF_ETEXT, "MAIN : TeSigDiffServGetPhbPsc : EXIT \n");
            return TE_DS_AF31_DSCP;
        case TE_DS_AF41_DSCP:
        case TE_DS_AF42_DSCP:
        case TE_DS_AF43_DSCP:
            TE_DBG (TE_DIFF_PRCS, "MAIN : DSCP = AF41 \n");
            TE_DBG (TE_DIFF_ETEXT, "MAIN : TeSigDiffServGetPhbPsc : EXIT \n");
            return TE_DS_AF41_DSCP;
        default:
            TE_DBG1 (TE_DIFF_PRCS, "MAIN : DSCP = %d \n", u1PhbDscp);
            TE_DBG (TE_DIFF_ETEXT, "MAIN :TeSigDiffServGetPhbPsc : EXIT \n");
            return u1PhbDscp;
    }
}
