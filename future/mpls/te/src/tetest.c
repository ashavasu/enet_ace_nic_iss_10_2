#include "teincs.h"
#include "lblmgrex.h"

/* $Id: tetest.c,v 1.15 2016/01/22 11:25:00 siva Exp $ */

/****************************************************************************
 Function    :  TeTestAllGmplsTunnelTable
 Description :  This function tests GMPLS Tunnel MIB objects.
 Input       :  u4TnlIndex        - Tunnel Index
                u4TnlInstance     - Tunnel Instance
                u4IngressId       - Tunnel Ingress Id
                u4EgressId        - Tunnel Egress Id
                pTestGmplsTnlInfo - Pointer to GMPLS Tunnel Info
                u4ObjectId        - Object ID
 Output      :  pu4ErrorCode      - Error Code
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TeTestAllGmplsTunnelTable (UINT4 *pu4ErrorCode, UINT4 u4TnlIndex,
                           UINT4 u4TnlInstance, UINT4 u4IngressId,
                           UINT4 u4EgressId, tGmplsTnlInfo * pTestGmplsTnlInfo,
                           UINT4 u4ObjectId)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    pTeTnlInfo =
        TeGetTunnelInfo (u4TnlIndex, u4TnlInstance, u4IngressId, u4EgressId);

    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (u4ObjectId != TE_GMPLS_TUNNEL_ADMIN_STATUS_FLAGS)
    {
        if (pTeTnlInfo->u1TnlRowStatus == ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }

    switch (u4ObjectId)
    {
        case TE_GMPLS_TUNNEL_UNNUM_IF:
        {
            /* Unnumbered not supported now */
            if (pTestGmplsTnlInfo->bUnnumIf != GMPLS_UNNUM_FALSE)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            break;
        }

        case TE_GMPLS_TUNNEL_ATTRIBUTES:
        {
            if (pTestGmplsTnlInfo->u1AttrLen != TE_ONE)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return SNMP_FAILURE;
            }

            if ((pTeTnlInfo->u1TnlSsnAttr & TE_SSN_REC_ROUTE_BIT) == TE_ZERO)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            if ((pTestGmplsTnlInfo->u1Attributes & TE_SSN_LBL_RECORD_BIT) ==
                TE_ZERO)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            break;
        }

        case TE_GMPLS_TUNNEL_ENCODING_TYPE:
        {
            if ((pTestGmplsTnlInfo->u1EncodingType !=
                 GMPLS_TUNNEL_LSP_NOT_GMPLS) &&
                (pTestGmplsTnlInfo->u1EncodingType != GMPLS_TUNNEL_LSP_PACKET))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            break;
        }

        case TE_GMPLS_TUNNEL_SWITCHING_TYPE:
        {
            if ((pTestGmplsTnlInfo->u1SwitchingType !=
                 GMPLS_UNKNOWN_SWITCHING) &&
                (pTestGmplsTnlInfo->u1SwitchingType != GMPLS_PSC1))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            break;
        }

        case TE_GMPLS_TUNNEL_LINK_PROTECTION_TYPE:
        {
            /* Code Logic not written as protection is not concentrated now */
            break;
        }

        case TE_GMPLS_TUNNEL_GPID:
        {
            if (pTestGmplsTnlInfo->u2Gpid != GMPLS_GPID_ETHERNET)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            break;
        }

        case TE_GMPLS_TUNNEL_SECONDARY:
        {
            if ((pTestGmplsTnlInfo->u1Secondary != TE_SNMP_FALSE) &&
                (pTestGmplsTnlInfo->u1Secondary != TE_SNMP_TRUE))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            break;
        }

        case TE_GMPLS_TUNNEL_DIRECTION:
        {
            if ((pTestGmplsTnlInfo->u1Direction != GMPLS_BIDIRECTION) &&
                (pTestGmplsTnlInfo->u1Direction != GMPLS_FORWARD))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            break;
        }

        case TE_GMPLS_TUNNEL_PATH_COMP:
        {
            if ((pTestGmplsTnlInfo->PathComp !=
                 GMPLS_PATH_COMP_DYNAMIC_FULL) &&
                (pTestGmplsTnlInfo->PathComp != GMPLS_PATH_COMP_EXPLICIT) &&
                (pTestGmplsTnlInfo->PathComp !=
                 GMPLS_PATH_COMP_DYNAMIC_PARTIAL))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            break;
        }
        case TE_GMPLS_SEND_PATH_NOTIFY_RECEPIENT_TYPE:
        {
            if (pTestGmplsTnlInfo->SendPathNotifyRecipientType !=
                MPLS_LSR_ADDR_IPV4)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }
        case TE_GMPLS_SEND_PATH_NOTIFY_RECEPIENT:
        {
            break;
        }
        case TE_GMPLS_SEND_RESV_NOTIFY_RECEPIENT_TYPE:
        {
            if (pTestGmplsTnlInfo->SendResvNotifyRecipientType !=
                MPLS_LSR_ADDR_IPV4)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }
        case TE_GMPLS_SEND_RESV_NOTIFY_RECEPIENT:
        {
            break;
        }

        case TE_GMPLS_TUNNEL_ADMIN_STATUS_FLAGS:
        {
            if (((pTestGmplsTnlInfo->u4AdminStatus != GMPLS_ADMIN_UNKNOWN) &&
                 !(pTestGmplsTnlInfo->u4AdminStatus & GMPLS_ADMIN_ADMIN_DOWN) &&
                 !(pTestGmplsTnlInfo->u4AdminStatus & GMPLS_ADMIN_TESTING) &&
                 !(pTestGmplsTnlInfo->u4AdminStatus & GMPLS_ADMIN_REFLECT) &&
                 !(pTestGmplsTnlInfo->u4AdminStatus
                   & GMPLS_ADMIN_DELETE_IN_PROGRESS))
                || (TE_TNL_SIGPROTO (pTeTnlInfo) != TE_SIGPROTO_RSVP))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }

        default:
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
  Function     : TeTestAllGmplsTunnelHopTable
  Description  : This function tests the GMPLS Tunnel Hop Table Objects. 
  Input        : u4HopListIndex       - Hop List Index
                 u4PathOptionIndex    - Path Option Index
                 u4HopIndex           - Hop Index
                 pTestGmplsTnlHopInfo - Pointer to GMPLS Tunnel Hop Info
                 u4ObjectId           - Object Id
  Output      :  pu4ErrorCode         - Error Code
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TeTestAllGmplsTunnelHopTable (UINT4 *pu4ErrorCode, UINT4 u4HopListIndex,
                              UINT4 u4PathOptionIndex, UINT4 u4HopIndex,
                              tGmplsTnlHopInfo * pTestGmplsTnlHopInfo,
                              UINT4 u4ObjectId)
{
    tTeHopInfo         *pTeHopInfo = NULL;

    if (TeCheckHopInfo (u4HopListIndex, u4PathOptionIndex, u4HopIndex,
                        &pTeHopInfo) == TE_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (pTeHopInfo->u1RowStatus == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case TE_GMPLS_TNL_HOP_FORWARD_LBL:
        {
            if ((pTestGmplsTnlHopInfo->u4ForwardLbl <
                 gSystemSize.MplsSystemSize.u4MinRsvpTeLblRange) ||
                (pTestGmplsTnlHopInfo->u4ForwardLbl >
                 gSystemSize.MplsSystemSize.u4MaxRsvpTeLblRange))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            break;
        }

        case TE_GMPLS_TNL_HOP_REVERSE_LBL:
        {
            if ((pTestGmplsTnlHopInfo->u4ReverseLbl <
                 gSystemSize.MplsSystemSize.u4MinRsvpTeLblRange) ||
                (pTestGmplsTnlHopInfo->u4ReverseLbl >
                 gSystemSize.MplsSystemSize.u4MaxRsvpTeLblRange))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            break;
        }

        default:
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TeTestAllFsMplsTunnelTable
 Description :  This function tests FS MPLS Tunnel Table Objects
 Input       :  u4TnlInstance     - Tunnel Index
                u4TnlInstance     - Tunnel Instance
                u4IngressId       - Ingress LSR Id
                u4EgressId        - Egress LSR Id
                pTestTeTnlInfo    - Pointer to MPLS Tunnel Info
                u4ObjectId        - Object ID
 Output      :  pu4ErrorCode      - Error Code
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TeTestAllFsMplsTunnelTable (UINT4 *pu4ErrorCode, UINT4 u4TnlIndex,
                            UINT4 u4TnlInstance, UINT4 u4IngressId,
                            UINT4 u4EgressId, tTeTnlInfo * pTestTeTnlInfo,
                            UINT4 u4ObjectId)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlInfo         *pTeMapTnlInfo = NULL;
    tTeAttrListInfo    *pTeAttrListInfo = NULL;
    UINT4               u4IngressLsrId = TE_ZERO;
    UINT4               u4EgressLsrId = TE_ZERO;
    UINT1               u1IfType;

    pTeTnlInfo =
        TeGetTunnelInfo (u4TnlIndex, u4TnlInstance, u4IngressId, u4EgressId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if ((u4ObjectId != TE_FS_MPLS_END_TO_END_PROTECTION) &&
        (pTeTnlInfo->u1TnlRowStatus == ACTIVE))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case TE_FS_MPLS_END_TO_END_PROTECTION:
        {
            break;
        }

        case TE_FS_MPLS_PR_CONFIG_OPER_TYPE:
        {
            if (TE_TNL_ADMIN_STATUS (pTeTnlInfo) == TE_ADMIN_UP)
            {
                 *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                 return SNMP_FAILURE;
            }
            break;
        }
        case TE_FS_MPLS_MBB_STATUS:
        {
			    if(TE_TNL_ADMIN_STATUS(pTeTnlInfo) == TE_ADMIN_UP) 
			       
    			{
       				 *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        			 return SNMP_FAILURE;
    			}

            if ((pTestTeTnlInfo->u1TnlMBBStatus != MPLS_TE_MBB_ENABLED) &&
                (pTestTeTnlInfo->u1TnlMBBStatus != MPLS_TE_MBB_DISABLED))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }

        case TE_FS_MPLS_SRLG_TYPE:
        {
            if ((pTestTeTnlInfo->u1TnlSrlgType !=
                 TE_TNL_SRLG_INCLUDE_ANY_AFFINITY) &&
                (pTestTeTnlInfo->u1TnlSrlgType !=
                 TE_TNL_SRLG_INCLUDE_ALL_AFFINITY) &&
                (pTestTeTnlInfo->u1TnlSrlgType !=
                 TE_TNL_SRLG_EXCLUDE_ALL_AFFINITY))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            break;
        }

        case TE_FS_MPLS_ATTR_PTR:
        {
            if (pTestTeTnlInfo->u4AttrParamIndex == TE_ZERO)
            {
                break;
            }

            pTeAttrListInfo
                = TeGetAttrListInfo (pTestTeTnlInfo->u4AttrParamIndex);

            if (pTeAttrListInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            break;
        }

        case TE_FS_MPLS_TNL_PATH_TYPE:
        {
            if ((pTestTeTnlInfo->u1TnlPathType != TE_TNL_WORKING_PATH) &&
                (pTestTeTnlInfo->u1TnlPathType != TE_TNL_PROTECTION_PATH))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            break;
        }

        case TE_FS_MPLS_TUNNEL_IFINDEX:
        {
            if (CfaGetIfType (pTestTeTnlInfo->u4TnlIfIndex,
                              &u1IfType) == CFA_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            if (u1IfType != CFA_MPLS_TUNNEL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }

            TeGetTunnelFrmIfIdbasedTree (pTestTeTnlInfo->u4TnlIfIndex,
                                         &pTeMapTnlInfo);
            if (pTeMapTnlInfo != NULL)
            {
                CONVERT_TO_INTEGER (pTeMapTnlInfo->TnlEgressLsrId,
                                    u4EgressLsrId);
                u4EgressLsrId = OSIX_NTOHL (u4EgressLsrId);
                CONVERT_TO_INTEGER (pTeMapTnlInfo->TnlIngressLsrId,
                                    u4IngressLsrId);
                u4IngressLsrId = OSIX_NTOHL (u4IngressLsrId);
                if ((u4EgressLsrId == u4EgressId)
                    && (u4IngressLsrId == u4IngressId)
                    && (pTeMapTnlInfo->u4TnlIndex == u4TnlIndex)
                    && (pTeMapTnlInfo->u4TnlInstance != TE_ZERO))
                {

                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    return SNMP_FAILURE;
                }
            }

            break;
        }

        default:
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TeTestAllFsMplsAttributeTable
 Description :  This Routine tests the Attribute table objects
 Input       :  u4AttrListIndex    - Attribute List index
                pSetTeAttrListInfo - Pointer to the Attribute list info
                u4ObjectId         - Object Id
 Output      :  pu4ErrorCode   - Error code
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TeTestAllFsMplsAttributeTable (UINT4 *pu4ErrorCode, UINT4 u4AttrListIndex,
                               tTeAttrListInfo * pTestAttrListInfo,
                               UINT4 u4ObjectId)
{
    tTeAttrListInfo    *pTeAttrListInfo = NULL;

    if (gTeGblInfo.TeParams.u1TeAdminStatus == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pTeAttrListInfo = TeGetAttrListInfo (u4AttrListIndex);

    if (u4ObjectId != TE_FS_MPLS_ATTR_ROW_STATUS)
    {
        if (pTeAttrListInfo == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
        else if (pTestAttrListInfo->u1RowStatus == ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else if (pTestAttrListInfo->u1RowStatus != CREATE_AND_WAIT)
    {
        if (pTeAttrListInfo == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
    }
    else if (pTeAttrListInfo != NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case TE_FS_MPLS_ATTR_NAME:
        {
            if ((pTestAttrListInfo->i4ListNameLen < MIN_SNMP_STRING_LENGTH) ||
                (pTestAttrListInfo->i4ListNameLen > MAX_SNMP_STRING_LENGTH))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
                return SNMP_FAILURE;
            }
            break;
        }

        case TE_FS_MPLS_ATTR_SETUP_PRIOR:
        {
            if (pTestAttrListInfo->u1SetupPriority > (UINT1) TE_SETPRIO_MAXVAL)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            break;
        }

        case TE_FS_MPLS_ATTR_HOLDING_PRIOR:
        {
            if (pTestAttrListInfo->u1HoldingPriority >
                (UINT1) TE_ATTR_HLDPRIO_MAXVAL)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }

            break;
        }

        case TE_FS_MPLS_ATTR_INCLUDE_ANY:
        case TE_FS_MPLS_ATTR_INCLUDE_ALL:
        case TE_FS_MPLS_ATTR_EXCLUDE_ANY:
        case TE_FS_MPLS_ATTR_SSN_ATTR:
        case TE_FS_MPLS_ATTR_BANDWIDTH:
        case TE_FS_MPLS_ATTR_CLASS_TYPE:
        {
            break;
        }

        case TE_FS_MPLS_ATTR_SRLG_TYPE:
        {
            if ((pTestAttrListInfo->u1SrlgType !=
                 TE_TNL_SRLG_INCLUDE_ANY_AFFINITY) &&
                (pTestAttrListInfo->u1SrlgType !=
                 TE_TNL_SRLG_INCLUDE_ALL_AFFINITY) &&
                (pTestAttrListInfo->u1SrlgType !=
                 TE_TNL_SRLG_EXCLUDE_ALL_AFFINITY))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                return SNMP_FAILURE;
            }
            break;
        }

        case TE_FS_MPLS_ATTR_ROW_STATUS:
        {
            switch (pTestAttrListInfo->u1RowStatus)
            {
                case CREATE_AND_WAIT:
                case ACTIVE:
                {
                    break;
                }
                case DESTROY:
                case NOT_IN_SERVICE:
                {
                    if (pTeAttrListInfo->u2NumOfTunnels != TE_ZERO)
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        return SNMP_FAILURE;
                    }
                    break;
                }
                case NOT_READY:
                case CREATE_AND_GO:
                default:
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }
            }

            break;
        }

        default:
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TeTestAllFsMplsTunnelSrlgTable
 Description :  This function tests FS MPLS Tunnel SRLG Table Objects.
 Input       :  u4TnlIndex           - Tunnel Index
                u4TnlInstance        - Tunnel Instance
                u4IngressId          - Ingress LSR Id
                u4EgressId           - Egress LSR Id
                u4SrlgNo             - SRLG Value
                pTestTeTnlSrlg       - Pointer to Tunnel SRLG Info
                u4ObjectId           - Object Id
 Output      :  pu4ErrorCode         - Error Code
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TeTestAllFsMplsTunnelSrlgTable (UINT4 *pu4ErrorCode, UINT4 u4TnlIndex,
                                UINT4 u4TnlInstance, UINT4 u4IngressId,
                                UINT4 u4EgressId, UINT4 u4SrlgNo,
                                tTeTnlSrlg * pTestTeTnlSrlg, UINT4 u4ObjectId)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlSrlg         *pTeTnlSrlg = NULL;

    pTeTnlInfo =
        TeGetTunnelInfo (u4TnlIndex, u4TnlInstance, u4IngressId, u4EgressId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    pTeTnlSrlg = TeGetSrlgFromTnlSrlgList (u4SrlgNo, pTeTnlInfo);

    if (u4ObjectId != TE_FS_MPLS_SRLG_ROW_STATUS)
    {
        if (pTeTnlSrlg == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
        else if (pTeTnlSrlg->u4RowStatus == ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else if (pTestTeTnlSrlg->u4RowStatus != CREATE_AND_WAIT)
    {
        if (pTeTnlSrlg == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
    }
    else if (pTeTnlSrlg != NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case TE_FS_MPLS_SRLG_ROW_STATUS:
        {
            switch (pTestTeTnlSrlg->u4RowStatus)
            {
                case CREATE_AND_WAIT:
                case ACTIVE:
                case DESTROY:
                {
                    break;
                }
                case CREATE_AND_GO:
                case NOT_IN_SERVICE:
                case NOT_READY:
                default:
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }
            }

            break;
        }
        default:
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TeTestAllFsMplsAttributeSrlgTable
 Description :  This function tests FS MPLS Attribute SRLG Table Objects.
 Input       :  u4AttrListIndex      - Attribute List Index
                pTestTeAttrSrlg      - Pointer to Attribute SRLG Info
                u4ObjectId           - Object Id
 Output      :  pu4ErrorCode         - Error Code
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TeTestAllFsMplsAttributeSrlgTable (UINT4 *pu4ErrorCode, UINT4 u4AttrListIndex,
                                   UINT4 u4SrlgNo,
                                   tTeAttrSrlg * pTestTeAttrSrlg,
                                   UINT4 u4ObjectId)
{
    tTeAttrListInfo    *pTeAttrListInfo = NULL;
    tTeAttrSrlg        *pTeAttrSrlg = NULL;

    pTeAttrListInfo = TeGetAttrListInfo (u4AttrListIndex);

    if (pTeAttrListInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pTeAttrSrlg = TeGetSrlgFromAttrSrlgList (u4SrlgNo, pTeAttrListInfo);

    if (u4ObjectId != TE_FS_MPLS_ATTR_SRLG_ROW_STATUS)
    {
        if (pTeAttrSrlg == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
        else if (pTeAttrSrlg->u4RowStatus == ACTIVE)
        {
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            return SNMP_FAILURE;
        }
    }
    else if (pTestTeAttrSrlg->u4RowStatus != CREATE_AND_WAIT)
    {
        if (pTeAttrSrlg == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return SNMP_FAILURE;
        }
    }
    else if (pTeAttrSrlg != NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case TE_FS_MPLS_ATTR_SRLG_ROW_STATUS:
        {
            switch (pTestTeAttrSrlg->u4RowStatus)
            {
                case CREATE_AND_WAIT:
                case ACTIVE:
                case DESTROY:
                {
                    break;
                }
                case CREATE_AND_GO:
                case NOT_IN_SERVICE:
                case NOT_READY:
                default:
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    return SNMP_FAILURE;
                }
            }

            break;
        }
        default:
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}
