/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: teport.c,v 1.13 2014/01/25 13:57:02 siva Exp $
 *
 * Description: This file contains all the functions related to
 *              the Portable components.
 *----------------------------------------------------------------------------*/

#include "teincs.h"
#include "fsmplslw.h"
#include "include.h"
#include "rtm.h"

#ifndef MPLS_LDP_WANTED
/*****************************************************************************/
/* Function Name : TeLdpTEEventHandler                                     
 * Description   : This is a dummy routine to Hand over u4Event to LDP.   
 *                 This definition of TeLdpTEEventHandler is valid when no
 *                 LDP module is associated with the TE module.          
 * Input(s)      : u4Event    -  Event to LDP                           
 *                 pTeTnlInfo -  Tunnel Information
 * Output(s)     : NONE                
 * Return(s)     : TE_LDP_SUCCESS/TE_LDP_FAILURE                      
 *****************************************************************************/
UINT1
TeLdpTEEventHandler (UINT4 u4Event, tTeTnlInfo * pTeTnlInfo)
{
    TE_SUPPRESS_WARNING (u4Event);
    TE_SUPPRESS_WARNING (pTeTnlInfo);
    return TE_LDP_FAILURE;
}
#endif

#ifndef MPLS_RSVPTE_WANTED
/*****************************************************************************/
/* Function Name : TeRpteTEEventHandler                                      
 * Description   : This is a dummy routine to Hand over u4Event to RPTE.    
 *                 This definition of TeRpteTEEventHandler is valid when no
 *                 RPTE module is associated with the TE module.          
 * Input(s)      : u4Event    -  Event to RPTE                           
 *                 pTeTnlInfo -  Tunnel Information
 * Output(s)     : NONE                
 * Return(s)     : TE_RPTE_SUCCESS/TE_RPTE_FAILURE                     
 *****************************************************************************/
UINT1
TeRpteTEEventHandler (UINT4 u4Event, tTeTnlInfo * pTeTnlInfo)
{
    TE_SUPPRESS_WARNING (u4Event);
    TE_SUPPRESS_WARNING (pTeTnlInfo);
    return TE_RPTE_FAILURE;
}
#endif

/****************************************************************************/
/* Function Name   : TePortTlmGetTeLinkUnResvBw                           */
/* Description     : This function used to get the unreserved bandwidth     */
/*                   of a TE Link corresponding to setup priority           */
/* Input (s)       : u4TeIfIndex     - TE Link If Index                     */
/*                   u1TnlSetPrio    - Tunnel Setup priority                */
/* Output (s)      : *pAvailBw       - Available Bandwidth                  */
/*                   *pUnResvBw      - Unreserved Bandwidth                 */
/*                   *pu4CompIfIndex - Component If Index                   */
/* Returns         : None                                                   */
/****************************************************************************/
VOID
TePortTlmGetTeLinkUnResvBw (UINT4 u4TeIfIndex, UINT1 u1TnlSetPrio,
                            FLOAT * pUnResvBw, FLOAT * pAvailBw,
                            UINT1 *pTeLinkOperStatus)
{
#ifdef TLM_WANTED
    tInTlmTeLink        InTlmTeLinkInfo;
    tOutTlmTeLink       OutTlmTeLinkInfo;

    MEMSET (&InTlmTeLinkInfo, TE_ZERO, sizeof (tInTlmTeLink));
    MEMSET (&OutTlmTeLinkInfo, TE_ZERO, sizeof (tOutTlmTeLink));
    InTlmTeLinkInfo.u4TeClass = u1TnlSetPrio;
    InTlmTeLinkInfo.u4IfIndex = u4TeIfIndex;

    TlmApiGetTeLinkUnResvBw (&InTlmTeLinkInfo, &OutTlmTeLinkInfo);

    *pAvailBw = OutTlmTeLinkInfo.availBw;
    *pUnResvBw = OutTlmTeLinkInfo.unResBw;
    *pTeLinkOperStatus = OutTlmTeLinkInfo.u1OperStatus;
#else
    UNUSED_PARAM (u4TeIfIndex);
    UNUSED_PARAM (u1TnlSetPrio);
    UNUSED_PARAM (pUnResvBw);
    UNUSED_PARAM (pAvailBw);
    UNUSED_PARAM (pTeLinkOperStatus);
#endif

    return;
}

/****************************************************************************/
/* Function Name   : TePortTlmUpdateTeLinkUnResvBw                        */
/* Description     : This function used to update the TE Link Unreserved BW */
/* Input (s)       : u1TnlHoldPrio - Holding priority                       */
/*                   reqResBw      - Requested bw                           */
/*                   u1ResvRsrc    - Resource reservation / deletion        */
/* Output (s)      : NONE                                                   */
/* Returns         : TE_SUCCESS/TE_FAILURE                                   */
/****************************************************************************/

INT4
TePortTlmUpdateTeLinkUnResvBw (UINT4 u4TeIfIndex,
                               UINT1 u1TnlHoldPrio, FLOAT reqResBw,
                               UINT1 u1ResvRsrc, UINT4 *pu4CompIfIndex)
{
#ifdef TLM_WANTED
    tInTlmTeLink        InTlmTeLinkInfo;
    tOutTlmTeLink       OutTlmTeLinkInfo;

    MEMSET (&InTlmTeLinkInfo, TE_ZERO, sizeof (tInTlmTeLink));
    MEMSET (&OutTlmTeLinkInfo, TE_ZERO, sizeof (tOutTlmTeLink));

    InTlmTeLinkInfo.u4TeClass = u1TnlHoldPrio;
    InTlmTeLinkInfo.reqResBw = reqResBw;
    InTlmTeLinkInfo.u4IfIndex = u4TeIfIndex;
    InTlmTeLinkInfo.u4CompIfIndex = *pu4CompIfIndex;

    if (TlmApiUpdateTeLinkUnResvBw (u1ResvRsrc, &InTlmTeLinkInfo,
                                    &OutTlmTeLinkInfo) == TLM_FAILURE)
    {
        return TE_FAILURE;
    }
    *pu4CompIfIndex = OutTlmTeLinkInfo.u4CompIfIndex;
#else
    UNUSED_PARAM (u1TnlHoldPrio);
    UNUSED_PARAM (reqResBw);
    UNUSED_PARAM (u1ResvRsrc);
    UNUSED_PARAM (u4TeIfIndex);
    UNUSED_PARAM (pu4CompIfIndex);
#endif

    return TE_SUCCESS;
}

/****************************************************************************/
/* Function Name   : TePortIsOspfTeEnabled                                  */
/* Description     : This function is used to check whether OSPF-TE is      */
/*                   enabled or not                                         */
/* Input (s)       : None                                                   */
/* Output (s)      : None                                                   */
/* Returns         : TE_SUCCESS / TE_FAILURE                                 */
/****************************************************************************/
INT1
TePortIsOspfTeEnabled (VOID)
{
#ifdef OSPFTE_WANTED
    if (OspfTeIsEnabled () == TRUE)
    {
        return TE_SUCCESS;
    }
#endif

    return TE_FAILURE;
}

/****************************************************************************/
/* Function Name   : TePortTlmIsOurAddr                                     */
/* Description     : This function is used to check whether the address     */
/*                   is local TE-Link address                               */
/* Input (s)       : u4Addr  - Er hop address                               */
/* Output (s)      : None                                                   */
/* Returns         : TE_SUCCESS / TE_FAILURE                                 */
/****************************************************************************/
INT1
TePortTlmIsOurAddr (UINT4 u4Addr)
{
#ifdef TLM_WANTED
    tInTlmTeLink        InTlmTeLinkInfo;
    tOutTlmTeLink       OutTlmTeLinkInfo;
    UINT4               u4Flags = TE_ZERO;
    MEMSET (&InTlmTeLinkInfo, TE_ZERO, sizeof (tInTlmTeLink));
    MEMSET (&OutTlmTeLinkInfo, TE_ZERO, sizeof (tOutTlmTeLink));

    InTlmTeLinkInfo.LocalIpAddr.ip6_addr_u.u4WordAddr[0] = u4Addr;
    u4Flags = TLM_TE_LINK_LOCAL_IP_ADD;

    TlmApiGetTeLinkParams (u4Flags, &InTlmTeLinkInfo, &OutTlmTeLinkInfo);

    if (OutTlmTeLinkInfo.u4IfIndex != TE_ZERO)
    {
        return TE_SUCCESS;
    }
#else
    UNUSED_PARAM (u4Addr);
#endif
    return TE_FAILURE;
}

/*---------------------------------------------------------------------------*/
/*                       End of file teport.c                                */
/*---------------------------------------------------------------------------*/
