
/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tesigext.c,v 1.90 2018/02/19 11:48:41 siva Exp $
 *
 * Description: This file contains all the functions exported to
 *              the signalling protocols.
 *              It includes
 *              Creation and Deletion of Tunnel
 *              Creation and Deletion of ErHopList
 *              Creation and Deletion of ArHopList
 *              Creation and Deletion of TrafficParams
 *              Updation of the Tunnel with the associated parameters.
 *----------------------------------------------------------------------------*/

#include "teincs.h"
#include "rtm.h"
#include "mplsftn.h"
#include "mplslsr.h"

/*****************************************************************************/
/* Function Name : TeSigCreateNewTnl                                            
 * Description   : This routine creates a new entry in tunnel table by taking
 *                 MPLS_CMN_LOCK.
 * Input(s)      : pNewTeTnlInfo - Pointer to Temporary Tunnel Structure.   
 * Output(s)     : ppTeTnlInfo  - Address of the Pointer of tunnelinfo     
 *                 in case of successful allocation.                         
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                 
 *****************************************************************************/
UINT1
TeSigCreateNewTnl (tTeTnlInfo ** ppTeTnlInfo, tTeTnlInfo * pNewTeTnlInfo)
{
    UINT1               u1RetVal = TE_FAILURE;

    MPLS_CMN_LOCK ();

    u1RetVal = TeCreateNewTnl (ppTeTnlInfo, pNewTeTnlInfo);

    MPLS_CMN_UNLOCK ();

    return u1RetVal;
}

/*****************************************************************************/
/* Function Name : TeCreateNewTnl                                            
 * Description   : This routine allocates memory for new TE Tunnel, copies the
 *                 contents provided by the Temporary pointer. It only copies
 *                 the contents, if there is no entry exists in its database.
 * Input(s)      : pNewTeTnlInfo - Pointer to Temporary Tunnel Structure.   
 * Output(s)     : ppTeTnlInfo  - Address of the Pointer of tunnelinfo     
 *                 in case of successful allocation.                         
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                 
 *****************************************************************************/
UINT1
TeCreateNewTnl (tTeTnlInfo ** ppTeTnlInfo, tTeTnlInfo * pNewTeTnlInfo)
{
    tTeTnlInfo         *pTmpTeTnlInfo = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlInfo         *pTeInstance0Tnl = NULL;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;

    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeCreateNewTnl : ENTRY \n");

    /* Admin Status Check made in All Exported Functions */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        TE_DBG (TE_EXTN_FAIL,
                "EXTN : TE Admin Status DOWN. No Further processing \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeCreateNewTnl : INTMD-EXIT \n");
        return TE_FAILURE;
    }

    /* Don't Allow to Hack the Code */
    if ((ppTeTnlInfo == NULL) || (pNewTeTnlInfo == NULL))
    {
        TE_DBG (TE_EXTN_FAIL, "EXTN : Null Pointer provided for TnlInfo \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeCreateNewTnl : INTMD-EXIT \n");
        return TE_FAILURE;
    }

    CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pNewTeTnlInfo)), u4IngressId);
    CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pNewTeTnlInfo)), u4EgressId);
    pTeTnlInfo = TeGetTunnelInfo (TE_TNL_TNL_INDEX (pNewTeTnlInfo),
                                  TE_TNL_TNL_INSTANCE (pNewTeTnlInfo),
                                  TE_NTOHL (u4IngressId),
                                  TE_NTOHL (u4EgressId));
    pTeInstance0Tnl = TeGetTunnelInfo (TE_TNL_TNL_INDEX (pNewTeTnlInfo),
                                       TE_ZERO,
                                       TE_NTOHL (u4IngressId),
                                       TE_NTOHL (u4EgressId));

    if (pTeTnlInfo != NULL)
    {
        TE_DBG (TE_EXTN_ETEXT,
                "EXTN : TeCreateNewTnl : If the same instance exist. We will increment New Tnl Instance \n");
        TE_TNL_TNL_INSTANCE (pNewTeTnlInfo)++;
    }

    if ((TE_TNL_OWNER (pNewTeTnlInfo) != TE_TNL_OWNER_CRLDP) &&
        ((TE_TNL_OWNER (pNewTeTnlInfo) != TE_TNL_OWNER_RSVP) &&
         (TE_TNL_OWNER (pNewTeTnlInfo) != TE_TNL_OWNER_LDP)))
    {
        TE_DBG (TE_EXTN_FAIL, "EXTN : Tnl Owner Wrong Value | not  present \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeCreateNewTnl : INTMD-EXIT \n");
        return TE_FAILURE;
    }

    if (pTeInstance0Tnl == NULL)
    {
        if (gu4TeRpteTnlCount == TE_MAX_TERPTETNL)
        {
            TE_DBG (TE_EXTN_FAIL, "EXTN : Maximum tunnel value reached \n");
            TE_DBG (TE_EXTN_ETEXT, "EXTN : TeCreateNewTnl : INTMD-EXIT \n");
            return TE_FAILURE;
        }
    }

    pTmpTeTnlInfo = (tTeTnlInfo *) TE_ALLOC_MEM_BLOCK (TE_TNL_POOL_ID);

    if (pTmpTeTnlInfo == NULL)
    {
        TE_DBG (TE_EXTN_FAIL, "EXTN : Free TnlInfo does not exist\n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeCreateNewTnl : INTMD-EXIT \n");
        return TE_FAILURE;
    }

    INIT_TE_TNL_INFO (pTmpTeTnlInfo);

    /* MEM_COPY OF THE tunnel info recived as input is done here */
    MEMCPY (pTmpTeTnlInfo, pNewTeTnlInfo, sizeof (tTeTnlInfo));
    TE_DLL_INIT (&TE_TNL_FTN_LIST (pTmpTeTnlInfo));

    TE_TNL_PRIMARY_TNL_INSTANCE (pTmpTeTnlInfo) =
        TE_TNL_TNL_INSTANCE (pNewTeTnlInfo);

    pTmpTeTnlInfo->bTnlIntOamEnable = TRUE;
    pTmpTeTnlInfo->u1OamOperStatus = TE_OPER_UNKNOWN;
    pTmpTeTnlInfo->u1CPOrMgmtOperStatus = TE_OPER_DOWN;
    TE_TNL_REOPTIMIZE_STATUS (pTmpTeTnlInfo) = TE_REOPTIMIZE_DISABLE;

    if (pTmpTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType == TE_ZERO)
    {
        pTmpTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType = MPLS_TE_UNPROTECTED;
    }

    pTmpTeTnlInfo->GmplsTnlInfo.u1AdminStatusLen = sizeof (UINT4);

    OsixGetSysTime (&(TE_TNL_CREATE_TIME (pTmpTeTnlInfo)));

    *ppTeTnlInfo = pTmpTeTnlInfo;
    TeAddNodeToTnlTable (pTmpTeTnlInfo);
    /* One chunk is occupied from the tunnel pool, so increment 
     * the global tunnel conter by one. */
    if ((pTeInstance0Tnl == NULL)
        || ((pTeInstance0Tnl != NULL)
            && (pTmpTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
                MPLS_TE_DEDICATED_ONE2ONE)
            && (pTmpTeTnlInfo->u1TnlPathType == TE_TNL_PROTECTION_PATH)))
    {
        gu4TeRpteTnlCount++;
    }

    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeCreateNewTnl : EXIT \n");
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeSigDeleteTnlInfo                                           
 * Description   : checks the tunnel in tunnel table ,if present         
 *                 deletes the tunnel from the tunnel table and         
 *                 releases memory associated with it to the pool.           
 *                 This is called from Signal Module
 * Input(s)      : pTeTnlInfo - pointer to tTnlInfo  Structure               
 * Output(s)     : NONE           
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                 
 *****************************************************************************/
UINT1
TeSigDeleteTnlInfo (tTeTnlInfo * pTeTnlInfo, UINT1 u1CallerId)
{
    UINT1               u1Return = TE_FAILURE;
    MPLS_CMN_LOCK ();
    u1Return = TeDeleteTnlInfo (pTeTnlInfo, u1CallerId);
    MPLS_CMN_UNLOCK ();
    return u1Return;
}

/*****************************************************************************/
/* Function Name : TeDeleteTnlInfo                                           
 * Description   : checks the tunnel in tunnel table ,if present         
 *                 deletes the tunnel from the tunnel table and         
 *                 releases memory associated with it to the pool.           
 * Input(s)      : pTeTnlInfo - pointer to tTnlInfo  Structure               
 * Output(s)     : NONE           
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                 
 *****************************************************************************/
UINT1
TeDeleteTnlInfo (tTeTnlInfo * pTeTnlInfo, UINT1 u1CallerId)
{
    tTeTnlInfo         *pTempTeTnlInfo = NULL;
    tTeTnlInfo         *pInstance0Tnl = NULL;
    tTeTnlInfo         *pFAInstance0Tnl = NULL;
/*    tL3VpnRsvpTeLspEventInfo  L3VpnRsvpTeLspEventInfo;*/
    UINT1               u1DelTnlFrmHash = TE_FALSE;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;
    UINT4               u4FAIngressId = 0;
    UINT4               u4FAEgressId = 0;
    UINT4               u4OrgInstance = 0;

    TE_DBG2 (TE_EXTN_ETEXT, "EXTN : TeDeleteTnlInfo : ENTRY for tnl: %u %u\n",
             pTeTnlInfo->u4TnlIndex, pTeTnlInfo->u4TnlInstance);

    /* Admin Status Check made in All Exported Functions */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        TE_DBG (TE_EXTN_FAIL,
                "EXTN : TE Admin Status DOWN. No Further processing \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeDeleteTnlInfo : INTMD-EXIT \n");
        return TE_FAILURE;
    }
    /* Don't Allow to Hack the Code */
    if (pTeTnlInfo == NULL)
    {
        TE_DBG (TE_EXTN_FAIL, "EXTN : Null Pointer provided for pTeTnlInfo \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeDeleteTnlInfo : INTMD-EXIT \n");
        return TE_FAILURE;
    }
    CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTeTnlInfo)), u4IngressId);
    CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTeTnlInfo)), u4EgressId);
    u4IngressId = OSIX_NTOHL (u4IngressId);
    u4EgressId = OSIX_NTOHL (u4EgressId);

    pTempTeTnlInfo = TeGetTunnelInfo (TE_TNL_TNL_INDEX (pTeTnlInfo),
                                      TE_TNL_TNL_INSTANCE (pTeTnlInfo),
                                      u4IngressId, u4EgressId);

    if (pTempTeTnlInfo == NULL)
    {
        TE_DBG (TE_EXTN_FAIL, "EXTN : Tnl is not found in the TnlHashTable \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeDeleteTnlInfo : INTMD-EXIT \n");
        return TE_FAILURE;
    }

    u4OrgInstance = pTempTeTnlInfo->u4TnlInstance;

    if (TE_TNL_TNL_INSTANCE (pTeTnlInfo) != TE_ZERO)
    {
        pInstance0Tnl = TeGetTunnelInfo (TE_TNL_TNL_INDEX (pTeTnlInfo), TE_ZERO,
                                         u4IngressId, u4EgressId);
    }

    /* The Incoming ArHops (Arhops of Path Msg) are deleted */
    if (TE_IN_AR_HOP_LIST_INFO (pTempTeTnlInfo) != NULL)
    {
        TE_DBG (TE_EXTN_PRCS,
                "EXTN : Path message ArHopList deletion called \n");
        TeDeleteArHopListInfo (TE_IN_AR_HOP_LIST_INFO (pTempTeTnlInfo));
        TE_IN_AR_HOP_LIST_INFO (pTempTeTnlInfo) = NULL;
    }

    /* The Outgoing ArHops (Arhops of Resv Msg) are deleted */
    if (TE_OUT_AR_HOP_LIST_INFO (pTempTeTnlInfo) != NULL)
    {
        TE_DBG (TE_EXTN_PRCS,
                "EXTN : Resv message ArHopList deletion called \n");
        TeDeleteArHopListInfo (TE_OUT_AR_HOP_LIST_INFO (pTempTeTnlInfo));
        TE_OUT_AR_HOP_LIST_INFO (pTempTeTnlInfo) = NULL;
    }

    if (pTempTeTnlInfo->pTeCHopListInfo != NULL)
    {
        TE_DBG (TE_EXTN_PRCS, "EXTN : CHop List Info deletion called \n");
        TeDeleteCHopListInfo (pTempTeTnlInfo->pTeCHopListInfo);
        pTempTeTnlInfo->pTeCHopListInfo = NULL;
    }

    if (pTempTeTnlInfo->u1FrrGblRevertTnlFlag == TE_TRUE)
    {
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeDeleteTnlInfo : EXIT \n");
        return TE_SUCCESS;
    }

    /* Attribute List is associated with Instance 0, but it is copied to
     * non-zero instances, when non-zero instances are deleted for which
     * zero instance is present, the association should not be removed. */
    if ((pInstance0Tnl == NULL) && (pTempTeTnlInfo->pTeAttrListInfo != NULL))
    {
        pTempTeTnlInfo->pTeAttrListInfo->u2NumOfTunnels--;
        pTempTeTnlInfo->pTeAttrListInfo = NULL;
    }
    /* 
     * If the Tunnel Owner is admin then the Protocols cannot delete
     * the Traffic Params, ErHopLists and Tnl Entry. Hence Success 
     * is returned to ensure the entries are deleted in the protocols
     */
    if ((TE_TNL_OWNER (pTempTeTnlInfo) == TE_TNL_OWNER_RSVP) ||
        (TE_TNL_OWNER (pTempTeTnlInfo) == TE_TNL_OWNER_LDP) ||
        (TE_TNL_OWNER (pTempTeTnlInfo) == TE_TNL_OWNER_CRLDP))
    {
        TE_DBG (TE_EXTN_ETEXT, "Owner is RSVP \n");
        /* Resource parameters is associated with Instance 0, but it is copied 
         * to non-zero instances, when non-zero instances are deleted for which
         * zero instance is present, the association should not be removed. */
        if ((pInstance0Tnl == NULL) &&
            (TE_TNL_TRFC_PARAM (pTempTeTnlInfo) != NULL))
        {
            TE_DBG (TE_EXTN_PRCS,
                    "EXTN : Traffic parameters deletion called  \n");
            TeDeleteTrfcParams (TE_TNL_TRFC_PARAM (pTempTeTnlInfo));
            TE_TNL_TRFC_PARAM (pTempTeTnlInfo) = NULL;
        }

        /* Explicit Hop list is associated with Instance 0, but it is copied 
         * to non-zero instances, when non-zero instances are deleted for which
         * zero instance is present, the association should not be removed. */
        if ((pInstance0Tnl == NULL) &&
            (TE_TNL_PATH_LIST_INFO (pTempTeTnlInfo) != NULL))
        {
            TE_DBG (TE_EXTN_PRCS, "EXTN : ErHopList deletion called \n");
            TeDeleteHopListInfo (TE_TNL_PATH_LIST_INFO (pTempTeTnlInfo));
            TE_TNL_PATH_LIST_INFO (pTempTeTnlInfo) = NULL;
        }

        if ((pInstance0Tnl != NULL) &&
            (pTempTeTnlInfo->u1TnlPathType == TE_TNL_WORKING_PATH) &&
            (pTempTeTnlInfo->bIsMbbRequired == FALSE))
        {
            pInstance0Tnl->u1WorkingUp = FALSE;
        }

        if ((pInstance0Tnl != NULL) &&
            (pTempTeTnlInfo->u1TnlPathType == TE_TNL_PROTECTION_PATH))
        {
            pInstance0Tnl->u1BackupUp = FALSE;
        }

        if (pTempTeTnlInfo->u4TnlMapIndex != TE_ZERO)
        {
            /* Decrement the number of Stacked tunnel in the HLSP Tunnel */
            CONVERT_TO_INTEGER (pTeTnlInfo->TnlMapIngressLsrId, u4FAIngressId);
            CONVERT_TO_INTEGER (pTeTnlInfo->TnlMapEgressLsrId, u4FAEgressId);
            u4FAIngressId = OSIX_NTOHL (u4FAIngressId);
            u4FAEgressId = OSIX_NTOHL (u4FAEgressId);
            pFAInstance0Tnl =
                TeGetTunnelInfo (pTeTnlInfo->u4TnlMapIndex, TE_ZERO,
                                 u4FAIngressId, u4FAEgressId);
            if (pFAInstance0Tnl != NULL
                && pFAInstance0Tnl->u4NoOfStackedTunnels != TE_ZERO)
            {
                pFAInstance0Tnl->u4NoOfStackedTunnels--;
            }
        }

        if (TeCallUpdateTnlOperStatus (pTempTeTnlInfo, TE_OPER_DOWN)
            == TE_FAILURE)
        {
            TE_DBG (TE_EXTN_PRCS, "EXTN : Update Tunnel Oper Status failed.\n");
            return TE_FAILURE;
        }

        u1DelTnlFrmHash = TE_TRUE;
        TE_DBG (TE_EXTN_ETEXT, "u1DelTnlFrmHash is TRUE\n");
    }
    else                        /* Tnl owner may be Unknown/Other/Snmp/PolicyAgent -- Admin */
    {
        TE_DBG (TE_EXTN_ETEXT, "Owner is Unknown \n");
        /* To ensure that the No of Tunnels count is decremented in the Default
         * Traffic Params
         * Also the no of tunnels associated count in ErHopList is decremented
         */

        /* Resource parameters is associated with Instance 0, but it is copied 
         * to non-zero instances, when non-zero instances are deleted for which
         * zero instance is present, the association should not be removed. */
        if ((pInstance0Tnl == NULL) &&
            (TE_TNL_TRFC_PARAM (pTempTeTnlInfo) != NULL) &&
            (TE_TNLRSRC_NUM_OF_TUNNELS
             (TE_TNL_TRFC_PARAM (pTempTeTnlInfo)) != TE_ZERO))
        {
            TE_DBG (TE_EXTN_PRCS,
                    "EXTN : No of Tnls count decremented in TrfcParams \n");
            TE_TNLRSRC_NUM_OF_TUNNELS (TE_TNL_TRFC_PARAM (pTempTeTnlInfo))--;
        }

        /* Explicit Hop list is associated with Instance 0, but it is copied 
         * to non-zero instances, when non-zero instances are deleted for which
         * zero instance is present, the association should not be removed. */
        if ((pInstance0Tnl == NULL) &&
            (TE_TNL_PATH_INFO (pTempTeTnlInfo) != NULL) &&
            (TE_PO_NUM_TUNNELS (TE_TNL_PATH_INFO (pTempTeTnlInfo)) != TE_ZERO))
        {
            TE_DBG (TE_EXTN_PRCS,
                    "EXTN : No of Tnls count decremented in PathOptionInfo \n");
            TE_PO_NUM_TUNNELS (TE_TNL_PATH_INFO (pTempTeTnlInfo))--;
        }

        /* In the Ingress The Rows are created by the Manager, Hence
         * OperStatus of the Tunnel is made down and a success is returned
         * to the Signalling protocols.
         * Manager has to explicitly call DelTnlFromTnlHashTable to remove
         * the entry from HashTable (LOW LVL if called)
         */
        if (TeCallUpdateTnlOperStatus (pTempTeTnlInfo, TE_OPER_DOWN)
            == TE_FAILURE)
        {
            TE_DBG (TE_EXTN_PRCS, "EXTN : Update Tunnel Oper Status failed.\n");
            return TE_FAILURE;
        }

        TE_DBG2 (TE_EXTN_PRCS,
                 "EXTN : Tunnel: %u %u OperStatus is made OPER-DOWN \n",
                 pTempTeTnlInfo->u4TnlIndex, pTempTeTnlInfo->u4TnlInstance);
        TE_DBG (TE_EXTN_PRCS,
                "EXTN : Setting pTeTnlInfo->u4ProactiveSessionIndex to 0\n");
        pTeTnlInfo->u4ProactiveSessionIndex = 0;

        if (u1CallerId == TE_TNL_CALL_FROM_ADMIN)
        {
            TE_DBG (TE_EXTN_ETEXT, "Call from ADMIN \n");
            u1DelTnlFrmHash = TE_TRUE;
        }
        else if (TE_TNL_OWNER (pTempTeTnlInfo) == TE_TNL_OWNER_SNMP)
        {
            TE_DBG (TE_EXTN_ETEXT, "call from SNMP\n");
            /* Start 10 second timer for Tunnel Reestablishment with a
             * new instance. */
            if ((TE_TNL_ADMIN_STATUS (pTeTnlInfo) == TE_ADMIN_UP) &&
                (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE))
            {
                TE_DBG2 (TE_EXTN_ETEXT,
                         "MPLS_RPTE_TNL_RETRIGGER_EVENT started for tnl: %u %u\n",
                         pTempTeTnlInfo->u4TnlIndex,
                         pTempTeTnlInfo->u4TnlInstance);
                MplsStartRequestedTimers (MPLS_RPTE_TNL_RETRIGGER_EVENT,
                                          &(pTempTeTnlInfo->TnlRetrigTmr));
            }

            TeDeleteStackTnlInterface (pTempTeTnlInfo);
        }
    }

    if (u1DelTnlFrmHash == TE_FALSE)
    {
        pTempTeTnlInfo->u4TnlIfIndex = TE_ZERO;

        TE_DBG (TE_EXTN_ETEXT,
                "EXTN : u1DelTnlFrmHash is False TeDeleteTnlInfo : EXIT \n");
        return TE_SUCCESS;
    }

    TE_DBG2 (TE_EXTN_PRCS, "\n MPLS tnl if delete %d L2VPN status=%d\n",
             TE_TNL_IFINDEX (pTeTnlInfo),
             (TE_TNL_IN_USE_BY_VPN (pTeTnlInfo) & TE_TNL_INUSE_BY_L2VPN));

    TeDeleteStackTnlInterface (pTempTeTnlInfo);
    TeDeleteMplsDiffServTnlInfo (pTempTeTnlInfo, pInstance0Tnl);

    TeDeleteTnlErrorTable (pTempTeTnlInfo);

    MplsStopRequestedTimers (MPLS_RPTE_TNL_RETRIGGER_EVENT,
                             &(pTempTeTnlInfo->TnlRetrigTmr));

    /* Delete the Frr Constraint Information associated with 
     * this tunnel. */
    TeDeleteTnlFrrInfo (pTempTeTnlInfo, pInstance0Tnl);

    TeDeleteSrlgFromTnl (pTempTeTnlInfo, pInstance0Tnl);

    if (pTempTeTnlInfo->u4TnlInstance != TE_ZERO)
    {
        TE_TNL_SIG_STATE (pTempTeTnlInfo) = TE_SIG_NOT_DONE;
        pTempTeTnlInfo->u1TnlRelStatus = TE_SIGMOD_TNLREL_DONE;
    }

    if (TeDeleteTnlFromTnlTable (TE_TNL_TNL_INDEX (pTempTeTnlInfo),
                                 TE_TNL_TNL_INSTANCE (pTempTeTnlInfo),
                                 u4IngressId, u4EgressId) == TE_FAILURE)
    {
        TE_DBG (TE_EXTN_PRCS,
                "EXTN : -E- Tnl is not deleted from TnlHashTable \n");
        return TE_FAILURE;
    }

    /* If instance zero tunnel is not present, nothing more to do,
     * return from here. Else, delete or retrigger */
    if (pInstance0Tnl == NULL)
    {
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeDeleteTnlInfo : EXIT \n");
        return TE_SUCCESS;
    }

    pInstance0Tnl->u1NumInstances--;

    TE_DBG4 (TE_EXTN_ETEXT,
             "pInstance0Tnl->u1NumInstances: %u, pInstance0Tnl->u1TnlRelStatus: %u, pInstance0Tnl->u4OrgTnlInstance: %u, u4OrgInstance: %u\n",
             pInstance0Tnl->u1NumInstances, pInstance0Tnl->u1TnlRelStatus,
             pInstance0Tnl->u4OrgTnlInstance, u4OrgInstance);
    if ((pInstance0Tnl->u1NumInstances == TE_ZERO)
        && (pInstance0Tnl->u1TnlRelStatus == TE_SIGMOD_TNLREL_AWAITED))
    {
        if ((TE_TNL_TRFC_PARAM (pInstance0Tnl) != NULL) &&
            (TE_TNLRSRC_NUM_OF_TUNNELS
             (TE_TNL_TRFC_PARAM (pInstance0Tnl)) != TE_ZERO))
        {
            TE_TNLRSRC_NUM_OF_TUNNELS (TE_TNL_TRFC_PARAM (pInstance0Tnl))--;
        }

        if ((TE_TNL_PATH_INFO (pInstance0Tnl) != NULL) &&
            (TE_PO_NUM_TUNNELS (TE_TNL_PATH_INFO (pInstance0Tnl)) != TE_ZERO))
        {
            TE_PO_NUM_TUNNELS (TE_TNL_PATH_INFO (pInstance0Tnl))--;
        }

        TeDeleteStackTnlInterface (pInstance0Tnl);
        TeDeleteMplsDiffServTnlInfo (pInstance0Tnl, NULL);
        TeDeleteTnlFrrInfo (pInstance0Tnl, NULL);
        TeDeleteTnlErrorTable (pInstance0Tnl);
        TeDeleteSrlgFromTnl (pInstance0Tnl, NULL);

        /* Stop the Retrigger Timer for the tunnel. */
        MplsStopRequestedTimers (MPLS_RPTE_TNL_RETRIGGER_EVENT,
                                 &(pInstance0Tnl->TnlRetrigTmr));

        if (TeDeleteTnlFromTnlTable (pInstance0Tnl->u4TnlIndex,
                                     TE_ZERO, u4IngressId, u4EgressId)
            == TE_FAILURE)
        {
            return TE_FAILURE;
        }
    }
    else if (((pInstance0Tnl->u4OrgTnlInstance == TE_ZERO) ||
              (pInstance0Tnl->u4OrgTnlInstance == u4OrgInstance))
             ||
             ((pInstance0Tnl->GmplsTnlInfo.i4E2EProtectionType ==
               MPLS_TE_DEDICATED_ONE2ONE) &&
              (pInstance0Tnl->u4OrgTnlInstance != u4OrgInstance)))

    {
        TE_DBG3 (TE_EXTN_ETEXT,
                 "TE_TNL_SIGPROTO (pInstance0Tnl): %d, pInstance0Tnl->u1TnlAdminStatus: %d, pInstance0Tnl->u1TnlRowStatus: %u\n",
                 TE_TNL_SIGPROTO (pInstance0Tnl),
                 pInstance0Tnl->u1TnlAdminStatus,
                 pInstance0Tnl->u1TnlRowStatus);
        if ((TE_TNL_SIGPROTO (pInstance0Tnl) == TE_SIGPROTO_RSVP)
            && (pInstance0Tnl->u1TnlAdminStatus == TE_ADMIN_UP)
            && (pInstance0Tnl->u1TnlRowStatus == TE_ACTIVE)
            && (TE_TNL_REOPTIMIZE_TRIGGER (pInstance0Tnl) != TRUE))
        {
            TE_DBG2 (TE_EXTN_ETEXT,
                     "MPLS_RPTE_TNL_RETRIGGER_EVENT started for tnl: %u %u\n",
                     pInstance0Tnl->u4TnlIndex, pInstance0Tnl->u4TnlInstance);
            MplsStartRequestedTimers (MPLS_RPTE_TNL_RETRIGGER_EVENT,
                                      &(pInstance0Tnl->TnlRetrigTmr));
        }
    }

    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeDeleteTnlInfo : EXIT \n");
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeDeleteStackTnlInterface                                          
 * Description   : This function deletes tunnel interface stacked over 
 *                 MPLS interface
 * Input(s)      : pTeTnlInfo - pointer to tTnlInfo  Structure               
 * Output(s)     : NONE           
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                 
 *****************************************************************************/
UINT1
TeDeleteStackTnlInterface (tTeTnlInfo * pTeTnlInfo)
{
    UINT4               u4MplsTunnelIfIndex = TE_ZERO;
    UINT4               u4L3IfIndex = TE_ZERO;
    INT4                i4StorageType = TE_ZERO;

    TE_DBG2 (TE_EXTN_PRCS,
             "TeDeleteStackTnlInterface: Tnl if %d Org tnl if %d\n",
             TE_TNL_IFINDEX (pTeTnlInfo), pTeTnlInfo->u4OrgTnlIfIndex);

    if ((pTeTnlInfo->bIsMbbRequired == TRUE) ||
        (TE_TNL_REOPTIMIZE_TRIGGER (pTeTnlInfo) == TRUE))
    {
        TE_TNL_REOPTIMIZE_TRIGGER (pTeTnlInfo) = FALSE;
        return TE_SUCCESS;
    }

    TE_DBG4 (TE_EXTN_PRCS,
             "TeDeleteStackTnlInterface: isif %d proto %d inusevpn: %d, detour: %d\n",
             TE_TNL_ISIF (pTeTnlInfo), TE_TNL_SIGPROTO (pTeTnlInfo),
             TE_TNL_IN_USE_BY_VPN (pTeTnlInfo), pTeTnlInfo->u1DetourActive);

    if (((TE_TNL_ISIF (pTeTnlInfo) == TE_TRUE) &&
         (TE_TNL_SIGPROTO (pTeTnlInfo) != TE_SIGPROTO_NONE)) ||
        (pTeTnlInfo->u1DetourActive == MPLS_TRUE))
    {
        if ((TE_TNL_ROLE (pTeTnlInfo) == TE_INGRESS) &&
            (pTeTnlInfo->u1DetourActive == MPLS_TRUE))
        {
            TE_DBG (TE_EXTN_PRCS, "Detour Active Case\n");
            u4MplsTunnelIfIndex = pTeTnlInfo->u4OrgTnlIfIndex;
        }
        else
        {
            TE_DBG (TE_EXTN_PRCS, "Detour not active case\n");
            u4MplsTunnelIfIndex = TE_TNL_IFINDEX (pTeTnlInfo);
        }

        if (u4MplsTunnelIfIndex != TE_ZERO)
        {
            CfaGetIfMainStorageType (u4MplsTunnelIfIndex, &i4StorageType);
            if ((CfaUtilGetIfIndexFromMplsTnlIf (u4MplsTunnelIfIndex,
                                                 &u4L3IfIndex,
                                                 TRUE) == CFA_SUCCESS) &&
                (u4L3IfIndex != TE_ZERO))
            {
                TE_DBG1 (TE_EXTN_PRCS, "Fwd Intf deletion %d\n",
                         u4MplsTunnelIfIndex);

                if (CfaIfmDeleteStackMplsTunnelInterface
                    (u4L3IfIndex, u4MplsTunnelIfIndex) == CFA_FAILURE)
                {
                    TE_DBG (TE_EXTN_FAIL,
                            "EXTN : -E- MPLS Tnl Stack Deletion Failed \n");
                    return TE_FAILURE;
                }
            }
            else if (i4StorageType == TE_STORAGE_VOLATILE)
            {
                if (CfaIfmDeleteDynamicMplsTunnelInterface (u4MplsTunnelIfIndex,
                                                            NULL, TRUE) ==
                    CFA_FAILURE)
                {
                    TE_DBG (TE_EXTN_FAIL,
                            "EXTN : -E- MPLS Tnl Intf Deletion failed\n");
                    return TE_FAILURE;
                }
            }

            if (!((pTeTnlInfo->u4TnlInstance == TE_ZERO) ||
                  ((TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_HLSP) &&
                   (pTeTnlInfo->u1TnlRole == TE_EGRESS))))
            {
                TE_TNL_IFINDEX (pTeTnlInfo) = TE_ZERO;
            }

            pTeTnlInfo->u4OrgTnlIfIndex = TE_ZERO;
        }

        if ((pTeTnlInfo->u4RevTnlIfIndex != TE_ZERO) &&
            (CfaUtilGetIfIndexFromMplsTnlIf (pTeTnlInfo->u4RevTnlIfIndex,
                                             &u4L3IfIndex, TRUE)
             == CFA_SUCCESS))
        {
            TE_DBG1 (TE_EXTN_PRCS, "Rev Intf deletion %d\n",
                     u4MplsTunnelIfIndex);
            if (CfaIfmDeleteStackMplsTunnelInterface
                (u4L3IfIndex, pTeTnlInfo->u4RevTnlIfIndex) == CFA_FAILURE)
            {
                TE_DBG (TE_EXTN_FAIL,
                        "EXTN : -E- MPLS Tnl Stack Deletion Failed \n");
                return TE_FAILURE;
            }
            else
            {
                pTeTnlInfo->u4RevTnlIfIndex = TE_ZERO;
            }
        }
    }
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeUpdateTnlStatus                                         
 * Description   : checks the tunnel in tunnel table ,if present         
 *                 Updates the tunnel oper status provided.         
 * Input(s)      : pTeTnlInfo - pointer to tTnlInfo  Structure               
 *               : pTeUpdateUInfo - pointer to the tTeUpdateInfo structure 
 * Output(s)     : NONE           
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                 
 *****************************************************************************/
UINT1
TeUpdateTnlStatus (tTeTnlInfo * pTeTnlInfo, tTeUpdateInfo * pTeUpdateInfo)
{
    tTeTnlInfo         *pTempTeTnlInfo = NULL;
    tTeTnlInfo         *pInstance0Tnl = NULL;
    tTeTnlInfo         *pWorkingTnlInfo = NULL;
    UINT4               u4SrcAddr = TE_ZERO;
    UINT4               u4DestAddr = TE_ZERO;
    UINT1               u1IsRpteAdminDown = TE_FALSE;

    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeUpdateTnlStatus : ENTRY \n");
    /* Admin Status Check made in All Exported Functions */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        TE_DBG (TE_EXTN_FAIL,
                "EXTN : TE Admin Status DOWN. No Further processing \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeUpdateTnlStatus : INTMD-EXIT \n");
        return TE_FAILURE;
    }
    /* Don't Allow to Hack the Code */
    if ((pTeTnlInfo == NULL) || (pTeUpdateInfo == NULL))
    {
        TE_DBG (TE_EXTN_FAIL, "EXTN : Null Pointer provided for pTeTnlInfo \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeUpdateTnlStatus : INTMD-EXIT \n");
        return TE_FAILURE;
    }

    CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTeTnlInfo)), u4SrcAddr);
    CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTeTnlInfo)), u4DestAddr);
    u4SrcAddr = TE_NTOHL (u4SrcAddr);
    u4DestAddr = TE_NTOHL (u4DestAddr);

    /* It is better to scan the Hash Table and then set the OperStatus
     * since this function is exported
     */
    pTempTeTnlInfo = TeGetTunnelInfo (TE_TNL_TNL_INDEX (pTeTnlInfo),
                                      TE_TNL_TNL_INSTANCE (pTeTnlInfo),
                                      u4SrcAddr, u4DestAddr);

    if (pTempTeTnlInfo == NULL)
    {
        TE_DBG (TE_EXTN_FAIL, "EXTN : Tnl is not found in the TnlHashTable \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeUpdateTnlStatus : INTMD-EXIT \n");
        return TE_FAILURE;
    }

    pInstance0Tnl = TeGetTunnelInfo (TE_TNL_TNL_INDEX (pTeTnlInfo),
                                     TE_ZERO, u4SrcAddr, u4DestAddr);

    pWorkingTnlInfo = TeGetTunnelInfo (pTeTnlInfo->u4BkpTnlIndex,
                                       pTeTnlInfo->u4BkpTnlInstance,
                                       u4SrcAddr, u4DestAddr);

    switch (pTeUpdateInfo->u1TeUpdateOpr)
    {
        case TE_CP_OR_MGMT_OPR_STATUS_RELATED:
        case TE_OAM_OPR_STATUS_RELATED:    /* Intentional fall through */
            /*   TeCmnExtUpdateTnlOperStatus (pTempTeTnlInfo, pTeUpdateInfo,
               pInstance0Tnl); */

            TE_DBG3 (TE_EXTN_PRCS,
                     "u4SrcModule: %u, bIsOamEnabled: %d, pTeUpdateInfo->u1TeUpdateOpr:%u\n",
                     pTeUpdateInfo->u4SrcModule, pTeTnlInfo->bIsOamEnabled,
                     pTeUpdateInfo->u1TeUpdateOpr);
            if (pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType !=
                MPLS_TE_FULL_REROUTE)
            {
                if ((pTeUpdateInfo->u1TeUpdateOpr == TE_OAM_OPR_STATUS_RELATED)
                    && (pTeUpdateInfo->u4SrcModule == BFD_MODULE)
                    && (pTeTnlInfo->u1TnlSgnlPrtcl == TE_SIGPROTO_RSVP))
                {
                    if (pTeUpdateInfo->u1TnlOperStatus ==
                        pTeTnlInfo->u1TnlOperStatus)
                    {
                        TE_DBG (TE_EXTN_PRCS,
                                "oper rxed by BFD already updated\n");
                        break;
                    }

                    if (pTeUpdateInfo->u1TnlOperStatus == TE_OPER_DOWN)
                    {
                        TE_DBG2 (TE_EXTN_PRCS,
                                 "oper down rxed by BFD for tunnel: " "%u %u\n",
                                 pTeTnlInfo->u4TnlIndex,
                                 pTeTnlInfo->u4TnlInstance);
                        if ((pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
                             MPLS_TE_DEDICATED_ONE2ONE)
                            && (pTeTnlInfo->u1TnlPathType ==
                                TE_TNL_WORKING_PATH)
                            && (pTeTnlInfo->u1TnlLocalProtectInUse ==
                                LOCAL_PROT_AVAIL))
                        {
                            if (TeRpteTEEventHandler (TE_PROTECTION_SWITCH_OVER,
                                                      pTeTnlInfo,
                                                      &u1IsRpteAdminDown) ==
                                TE_RPTE_FAILURE)
                            {
                                TE_DBG (TE_EXTN_PRCS,
                                        "Posting to RSVP-TE failed\n");
                            }
                        }
                        else
                        {

                            if ((pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
                                 MPLS_TE_DEDICATED_ONE2ONE) &&
                                (pTeTnlInfo->u1TnlPathType ==
                                 TE_TNL_WORKING_PATH)
                                && (pTeTnlInfo->u1TnlLocalProtectInUse ==
                                    LOCAL_PROT_IN_USE))
                            {
                                TE_DBG2 (TE_EXTN_PRCS,
                                         "BFD: Rxed Invalid Event from BFD "
                                         "for tnl: %u %u\n",
                                         pTeTnlInfo->u4TnlIndex,
                                         pTeTnlInfo->u4TnlInstance);
                            }
                            else
                            {
                                if ((pTeTnlInfo->GmplsTnlInfo.
                                     i4E2EProtectionType ==
                                     MPLS_TE_DEDICATED_ONE2ONE)
                                    && (pTeTnlInfo->u1TnlPathType ==
                                        TE_TNL_PROTECTION_PATH)
                                    && (pWorkingTnlInfo != NULL)
                                    && (pWorkingTnlInfo->
                                        u1TnlLocalProtectInUse ==
                                        LOCAL_PROT_IN_USE))
                                {
                                    TE_DBG2 (TE_EXTN_PRCS,
                                             "Sending Oper-down Event from BFD "
                                             "for working tnl: %u %u\n",
                                             pWorkingTnlInfo->u4TnlIndex,
                                             pWorkingTnlInfo->u4TnlInstance);

                                    if (TeRpteTEEventHandler
                                        (TE_TNL_DOWN, pWorkingTnlInfo,
                                         &u1IsRpteAdminDown) == TE_RPTE_FAILURE)
                                    {
                                        TE_DBG (TE_EXTN_PRCS,
                                                "Posting to RSVP-TE failed "
                                                "for working tunnel\n");
                                    }
                                }
                                if (TeRpteTEEventHandler
                                    (TE_TNL_DOWN, pTeTnlInfo,
                                     &u1IsRpteAdminDown) == TE_RPTE_FAILURE)
                                {
                                    TE_DBG (TE_EXTN_PRCS,
                                            "Posting to RSVP-TE failed\n");
                                }
                            }
                        }
                    }
                }                /* BFD SUPPORT FOR RSVP-TE */
            }
            TeCmnExtUpdateTnlOperStatus (pTempTeTnlInfo, pTeUpdateInfo,
                                         pInstance0Tnl);

            /* If Zero Instance is present, call updation of tunnel
             * oper status for that tunnel too. */
            if ((pInstance0Tnl != NULL) &&
                ((pInstance0Tnl->u4OrgTnlInstance == TE_ZERO) ||
                 ((pInstance0Tnl->u4OrgTnlInstance ==
                   pTempTeTnlInfo->u4TnlInstance)
                  && (pTempTeTnlInfo->u1TnlPathType !=
                      TE_TNL_PROTECTION_PATH))))
            {
                /* If the protection in use bit is set, Oper status of Instance zero tunnel
                 * should not be made down
                 * */
                if ((((pTempTeTnlInfo->u1TnlLocalProtectInUse ==
                       LOCAL_PROT_AVAIL)
                      || (pTempTeTnlInfo->u1TnlLocalProtectInUse ==
                          LOCAL_PROT_IN_USE))
                     || (pInstance0Tnl->u1BackupUp == TRUE))
                    && (pTeUpdateInfo->u1TnlOperStatus == TE_OPER_DOWN))
                {
                    break;
                }
                TeCmnExtUpdateTnlOperStatus (pInstance0Tnl, pTeUpdateInfo,
                                             NULL);
            }
            else if ((pTempTeTnlInfo->pTeBackupPathInfo != NULL) &&
                     (pTempTeTnlInfo->u1TnlPathType == TE_TNL_PROTECTION_PATH)
                     && (pInstance0Tnl != NULL)
                     && (pInstance0Tnl->u1BackupUp == TRUE))
            {

                if ((pInstance0Tnl->u1WorkingUp == TRUE)
                    && (pTeUpdateInfo->u1TnlOperStatus == TE_OPER_DOWN))
                {
                    break;
                }

                TeCmnExtUpdateTnlOperStatus (pInstance0Tnl, pTeUpdateInfo,
                                             NULL);
            }
            break;
        case TE_LOCAL_PROT_RELATED:
            /* Checking the Local Protection status */
            TeCmnExtUpdateTnlProtStatus (pTempTeTnlInfo, pTeUpdateInfo);
            break;

        case TE_TNL_ROLE_RELATED:
            /* Making the Tunnel Role to EGRESS/INTERMEDIATE */
            switch (pTeUpdateInfo->u1TnlRole)
            {
                case TE_INTERMEDIATE:
                    TE_TNL_ROLE (pTempTeTnlInfo) = TE_INTERMEDIATE;
                    TE_DBG (TE_EXTN_PRCS,
                            "EXTN : Tunnel Role made to Intermediate \n");
                    break;
                case TE_EGRESS:
                    TE_TNL_ROLE (pTempTeTnlInfo) = TE_EGRESS;
                    TE_DBG (TE_EXTN_PRCS,
                            "EXTN : Tunnel Role made to Egress \n");
                    break;
                default:
                    TE_DBG (TE_EXTN_FAIL, "EXTN : Invalid Role Requested \n");
                    TE_DBG (TE_EXTN_ETEXT,
                            "EXTN : TeUpdateTnlStatus : INTMD-EXIT \n");
                    return TE_FAILURE;
            }
            break;
        default:
            TE_DBG (TE_EXTN_FAIL, "EXTN : Invalid Operation Requested \n");
            TE_DBG (TE_EXTN_ETEXT, "EXTN : TeUpdateTnlStatus : INTMD-EXIT \n");
    }
    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeUpdateTnlStatus : EXIT \n");
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeSigCreateArHopListInfo                                     
 * Description   : This routine reserves a tArHopListInfo structure from     
 *                 the global array and copies the ArHopList provided by the 
 *                 signalling protocols.                                     
 * Input(s)      : pArHopList - Pointer to ArHopList   
 * Output(s)     : ppArHopListInfo - Address of the Pointer to 
 *                 tTeArHopListInfo structure    
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                   
 *****************************************************************************/
UINT1
TeSigCreateArHopListInfo (tTeArHopListInfo ** ppArHopListInfo,
                          tTeSll * pArHopList)
{
    UINT4               u4Count;
    tTeArHopInfo       *pTeTempArHopInfo = NULL;
    tTeArHopInfo       *pTeArHopInfo = NULL;

    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeSigCreateArHopListInfo : ENTRY \n");
    MPLS_CMN_LOCK ();
    /* Admin Status Check made in All Exported Functions */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        TE_DBG (TE_EXTN_FAIL,
                "EXTN : TE Admin Status DOWN. No Further processing \n");
        TE_DBG (TE_EXTN_ETEXT,
                "EXTN : TeSigCreateArHopListInfo : INTMD-EXIT \n");
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }
    /* Don't Allow to Hack the Code */
    if ((ppArHopListInfo == NULL) || (pArHopList == NULL))
    {
        TE_DBG (TE_EXTN_FAIL, "EXTN : Null Pointer provided in ArHopList \n");
        TE_DBG (TE_EXTN_ETEXT,
                "EXTN : TeSigCreateArHopListInfo : INTMD-EXIT \n");
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }

    /* A valid HopListIndex is  obtained */
    for (u4Count = TE_ONE; u4Count <= TE_MAX_ARHOP_LIST (gTeGblInfo); u4Count++)
    {
        if (TE_ARHOP_LIST_INDEX (u4Count) == TE_ZERO)
        {
            *ppArHopListInfo = TE_ARHOP_LISTINFO (u4Count);
            TE_ARHOP_LIST_INDEX (u4Count) = u4Count;
            TE_DBG1 (TE_EXTN_PRCS,
                     "EXTN : ArHopListInfo allocated ArHopListIndex = %d \n",
                     u4Count);
            break;
        }
    }
    if (u4Count > TE_MAX_ARHOP_LIST (gTeGblInfo))
    {
        TE_DBG (TE_EXTN_ETEXT,
                "EXTN : TeSigCreateArHopListInfo : INTMD_EXIT \n");
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }

    if (*ppArHopListInfo == NULL)
    {
        TE_DBG (TE_EXTN_ETEXT,
                "EXTN : TeSigCreateArHopListInfo : INTMD_EXIT \n");
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }

    /* The AR Hop list is scanned and each node from the list is 
       copied into the list */
    TE_SLL_INIT (TE_ARHOP_LIST (u4Count));
    TE_SLL_SCAN (pArHopList, pTeTempArHopInfo, tTeArHopInfo *)
    {
        pTeArHopInfo = (tTeArHopInfo *) TE_ALLOC_MEM_BLOCK (TE_ARHOP_POOL_ID);

        if (pTeArHopInfo == NULL)
        {
            TE_DBG (TE_EXTN_FAIL, "EXTN : MemAllocation failed for ArHops \n");
            TE_DBG (TE_EXTN_ETEXT,
                    "EXTN : TeSigCreateArHopListInfo : INTMD_EXIT \n");
            TeDeleteArHopListInfo (*ppArHopListInfo);
            MPLS_CMN_UNLOCK ();
            return TE_FAILURE;
        }
        MEMCPY (pTeArHopInfo, pTeTempArHopInfo, sizeof (tTeArHopInfo));
        if ((TE_ARHOP_FLAG (pTeTempArHopInfo) & FRR_NODE_PROT) == FRR_NODE_PROT)
        {
            TE_FRR_ARHOP_PROT_TYPE (pTeArHopInfo) |= TE_TNL_FRR_PROTECTION_NODE;
        }
        else if ((TE_ARHOP_FLAG (pTeTempArHopInfo) & LOCAL_PROT_AVAIL) ==
                 LOCAL_PROT_AVAIL)
        {
            TE_FRR_ARHOP_PROT_TYPE (pTeArHopInfo) |= TE_TNL_FRR_PROTECTION_LINK;
        }
        if ((TE_ARHOP_FLAG (pTeTempArHopInfo) & LOCAL_PROT_IN_USE) ==
            LOCAL_PROT_IN_USE)
        {
            TE_FRR_ARHOP_PROT_TYPE_IN_USE (pTeArHopInfo) |=
                TE_FRR_ARHOP_PROT_TYPE (pTeArHopInfo);
        }
        if ((TE_ARHOP_FLAG (pTeTempArHopInfo) & FRR_BW_PROT) == FRR_BW_PROT)
        {
            TE_FRR_ARHOP_BW_PROT_AVAIL (pTeArHopInfo) = TE_SNMP_TRUE;
        }
        else
        {
            TE_FRR_ARHOP_BW_PROT_AVAIL (pTeArHopInfo) = TE_SNMP_FALSE;
        }
        TE_SLL_ADD (TE_ARHOP_LIST (u4Count), &(pTeArHopInfo->NextHop));
    }
    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeSigCreateArHopListInfo : EXIT \n");
    MPLS_CMN_UNLOCK ();
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeSigDeleteArHopListInfo                                        
 * Description   : Deletes memory for all the ArHopInfo structure and        
 *                 releases it to the memory pool and releases the
 *                 ArHopListInfo. Called from Signal Module
 * Input(s)      : pTeArHopListInfo - Pointer to tTeArHopListInfo       
 * Output(s)     : NONE              
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                   
 *****************************************************************************/
UINT1
TeSigDeleteArHopListInfo (tTeArHopListInfo * pArHopListInfo)
{
    UINT1               u1Return = TE_FAILURE;
    MPLS_CMN_LOCK ();
    u1Return = TeDeleteArHopListInfo (pArHopListInfo);
    MPLS_CMN_UNLOCK ();
    return u1Return;
}

/*****************************************************************************/
/* Function Name : TeDeleteArHopListInfo                                        
 * Description   : Deletes memory for all the ArHopInfo structure and        
 *                 releases it to the memory pool and releases the
 *                 ArHopListInfo
 * Input(s)      : pTeArHopListInfo - Pointer to tTeArHopListInfo       
 * Output(s)     : NONE              
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                   
 *****************************************************************************/
UINT1
TeDeleteArHopListInfo (tTeArHopListInfo * pArHopListInfo)
{
    tTeArHopInfo       *pTempTeArHopInfo = NULL;
    tTeSll             *pTeArHopList = NULL;

    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeDeleteArHopListInfo : ENTRY \n");
    /* Admin Status Check made in All Exported Functions */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        TE_DBG (TE_EXTN_FAIL,
                "EXTN : TE Admin Status DOWN. No Further processing \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeDeleteArHopListInfo : INTMD-EXIT \n");
        return TE_FAILURE;
    }

    /* Don't Allow to Hack the Code */
    if (pArHopListInfo == NULL)
    {
        TE_DBG (TE_EXTN_FAIL,
                "EXTN : Null Pointer provided for ArHopListInfo \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeDeleteArHopListInfo : INTMD-EXIT \n");
        return TE_FAILURE;
    }
    if ((pArHopListInfo->u4ArHopListIndex
         < TE_TNL_ARHOPLSTINDEX_MINVAL) ||
        (pArHopListInfo->u4ArHopListIndex > TE_MAX_ARHOP_LIST (gTeGblInfo)))
    {
        TE_DBG (TE_EXTN_FAIL, "EXTN : Invalid ArHopList Index provided \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeDeleteArHopListInfo : INTMD-EXIT \n");
        return TE_FAILURE;
    }

    pTeArHopList = TE_ARHOP_LIST (pArHopListInfo->u4ArHopListIndex);

    /*Check if the given TeArHopInfo is there in the TeArHopList */

    pTempTeArHopInfo = (tTeArHopInfo *) TMO_SLL_First (pTeArHopList);
    while (pTempTeArHopInfo != NULL)
    {
        TE_SLL_DELETE (pTeArHopList, &(pTempTeArHopInfo->NextHop));
        MEMSET (pTempTeArHopInfo, TE_ZERO, sizeof (tTeArHopInfo));
        if (TE_REL_MEM_BLOCK (TE_ARHOP_POOL_ID,
                              (UINT1 *) pTempTeArHopInfo) == TE_MEM_FAILURE)
        {
            TE_DBG (TE_EXTN_FAIL,
                    "EXTN : ArHop Deletion failed - Log and Continue \n");
        }
        pTempTeArHopInfo = (tTeArHopInfo *) TMO_SLL_First (pTeArHopList);
    }
    INIT_TE_ARHOP_LIST_INFO (pArHopListInfo);

    TE_DBG (TE_EXTN_PRCS, "EXTN : ArHopListInfo and ArHops removed \n");
    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeDeleteArHopListInfo : EXIT \n");
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeSigUpdateArHopListInfo                                     
 * Description   : checks the tunnel in tunnel table ,if present         
 *                 Updates the modified ArHopList to the TnlInfo.         
 * Input(s)      : pTeTnlInfo - pointer to tTnlInfo  Structure               
 *               : u1TnlOperStatus - Operational Status of the Tunnel        
 * Output(s)     : NONE           
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                 
 *****************************************************************************/
UINT1
TeSigUpdateArHopListInfo (tTeTnlInfo * pTeTnlInfo,
                          tTeArHopListInfo * pArHopListInfo, UINT1 u1Direction)
{

    tTeTnlInfo         *pTempTeTnlInfo = NULL;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;

    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeSigUpdateArHopListInfo : ENTRY \n");
    MPLS_CMN_LOCK ();
    /* Admin Status Check made in All Exported Functions */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        TE_DBG (TE_EXTN_FAIL,
                "EXTN : TE Admin Status DOWN. No Further processing \n");
        TE_DBG (TE_EXTN_ETEXT,
                "EXTN : TeSigUpdateArHopListInfo : INTMD-EXIT \n");
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }
    /* Don't Allow to Hack the Code */
    if ((pTeTnlInfo == NULL) || (pArHopListInfo == NULL))
    {
        TE_DBG (TE_EXTN_FAIL, "EXTN : Null Pointer provided for pTeTnlInfo \n");
        TE_DBG (TE_EXTN_ETEXT,
                "EXTN : TeSigUpdateArHopListInfo : INTMD-EXIT \n");
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }
    if ((pArHopListInfo->u4ArHopListIndex
         < TE_TNL_ARHOPLSTINDEX_MINVAL) ||
        (pArHopListInfo->u4ArHopListIndex > TE_MAX_ARHOP_LIST (gTeGblInfo)))
    {
        TE_DBG (TE_EXTN_FAIL, "EXTN : Invalid ArHopList Index provided \n");
        TE_DBG (TE_EXTN_ETEXT,
                "EXTN : TeSigUpdateArHopListInfo : INTMD-EXIT \n");
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }
    /* It is better to scan the Hash Table and then copy the contents
     * since this function is exported
     */
    CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTeTnlInfo)), u4IngressId);
    CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTeTnlInfo)), u4EgressId);
    pTempTeTnlInfo = TeGetTunnelInfo (TE_TNL_TNL_INDEX (pTeTnlInfo),
                                      TE_TNL_TNL_INSTANCE (pTeTnlInfo),
                                      TE_NTOHL (u4IngressId),
                                      TE_NTOHL (u4EgressId));

    if (pTempTeTnlInfo == NULL)
    {
        TE_DBG (TE_EXTN_FAIL, "EXTN : Tnl is not found in the TnlHashTable \n");
        TE_DBG (TE_EXTN_ETEXT,
                "EXTN : TeSigUpdateArHopListInfo : INTMD-EXIT \n");
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }
    /* Making the Tunnel Operstatus either to UP/DOWN */
    switch (u1Direction)
    {
        case TE_TNL_DIR_IN:
        {
            OsixGetSysTime ((&TE_TNL_LAST_PATH_CHG_TIME (pTempTeTnlInfo)));
            TE_TNL_LAST_PATH_CHG_COUNT (pTempTeTnlInfo)++;
            if (TE_IN_AR_HOP_LIST_INFO (pTempTeTnlInfo) != NULL)
            {
                /* ArHop changed so, send a trap */
                TeSendTnlTrapAndSyslog (pTeTnlInfo, TE_TNL_REROUTE_TRAP);
            }
            TE_IN_AR_HOP_LIST_INFO (pTempTeTnlInfo) = pArHopListInfo;
            TE_DBG (TE_EXTN_PRCS,
                    "EXTN : Incoming ArHopList is added successfully \n");
            break;
        }
        case TE_TNL_DIR_OUT:
        {
            OsixGetSysTime ((&TE_TNL_LAST_PATH_CHG_TIME (pTempTeTnlInfo)));
            TE_TNL_LAST_PATH_CHG_COUNT (pTempTeTnlInfo)++;
            if (TE_OUT_AR_HOP_LIST_INFO (pTempTeTnlInfo) != NULL)
            {
                /* ArHop changed so, send a trap */
                TeSendTnlTrapAndSyslog (pTeTnlInfo, TE_TNL_REROUTE_TRAP);
            }
            TE_TNL_ARHOP_TABLE_INDEX (pTempTeTnlInfo) =
                pArHopListInfo->u4ArHopListIndex;
            TE_OUT_AR_HOP_LIST_INFO (pTempTeTnlInfo) = pArHopListInfo;
            TE_DBG (TE_EXTN_PRCS,
                    "EXTN : OutGoing ArHopList is added successfully \n");
            break;
        }
        default:
            TE_DBG (TE_EXTN_FAIL,
                    "EXTN : Invalid Direction given for the "
                    "ArHopList addition \n");
            TE_DBG (TE_EXTN_ETEXT,
                    "EXTN : TeSigUpdateArHopListInfo : INTMD-EXIT \n");
            MPLS_CMN_UNLOCK ();
            return TE_FAILURE;
    }
    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeSigUpdateArHopListInfo : EXIT \n");
    MPLS_CMN_UNLOCK ();
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeSigCreateHopListInfo                                      
 * Description   : This routine reserves a tTePathListInfo structure from    
 *                 the global array and copies the ArHopList provided by the
 *                 signalling protocols.
 * Input(s)      : pErHopList - Ponter to the ErHopList                      
 * Output(s)     : ppTePathListInfo - Address of the pointer of TePathListInfo  
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                   
 *****************************************************************************/
UINT1
TeSigCreateHopListInfo (tTePathListInfo ** ppTePathListInfo,
                        tTeSll * pErHopList)
{
    UINT2               u2DefPathIndex = 1;
    tTePathInfo        *pTePathInfo = NULL;
    tTeHopInfo         *pTeTempHopInfo = NULL;
    tTeHopInfo         *pTeHopInfo = NULL;
    tTePathListInfo    *pTeTempPathListInfo = NULL;

    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeSigCreateHopListInfo : ENTRY \n");
    MPLS_CMN_LOCK ();
    /* Admin Status Check made in All Exported Functions */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        TE_DBG (TE_EXTN_FAIL,
                "EXTN : TE Admin Status DOWN. No Further processing \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeSigCreateHopListInfo : INTMD-EXIT \n");
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }

    /* Don't Allow to Hack the Code */
    if ((ppTePathListInfo == NULL) || (pErHopList == NULL))
    {
        TE_DBG (TE_EXTN_FAIL, "EXTN : Null Pointer provided in ErHopList \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeSigCreateHopListInfo : INTMD-EXIT \n");
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }
    /* A valid HopListIndex is  obtained */

    if (TeCreatePathListInfo (ppTePathListInfo) == TE_FAILURE)
    {
        TE_DBG (TE_EXTN_FAIL, "EXTN : PathListInfo allocation Failed \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeSigCreateHopListInfo : INTMD-EXIT \n");
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }

    pTeTempPathListInfo = *ppTePathListInfo;

    /*  The PathOptionInfo is created */
    if (TeCreatePathOptionInfo (TE_HOPLIST_INDEX (pTeTempPathListInfo),
                                u2DefPathIndex, &pTePathInfo) == TE_FAILURE)
    {
        TE_DBG (TE_EXTN_FAIL, "EXTN : PathOptionInfo allocation Failed \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeSigCreateHopListInfo : INTMD-EXIT \n");
        TeDeleteHopListInfo (*ppTePathListInfo);
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }

    TE_SLL_SCAN (pErHopList, pTeTempHopInfo, tTeHopInfo *)
    {
        pTeHopInfo = (tTeHopInfo *) TE_ALLOC_MEM_BLOCK (TE_HOP_POOL_ID);

        if (pTeHopInfo == NULL)
        {
            TE_DBG (TE_EXTN_FAIL, "EXTN : TeHopInfo allocation Failed \n");
            TE_DBG (TE_EXTN_ETEXT,
                    "EXTN : TeSigCreateHopListInfo : INTMD-EXIT \n");
            /* The DeleteHopListInfo will delete the HopListIndex,
             * PathOptionInfo and the ErHops so far created, Hence we 
             * are not calling DeletePathOptionInfo
             */
            TeDeleteHopListInfo (*ppTePathListInfo);
            MPLS_CMN_UNLOCK ();
            return TE_FAILURE;
        }

        MEMCPY (pTeHopInfo, pTeTempHopInfo, sizeof (tTeHopInfo));

        if (TeIsLsrPartOfErHop (pTeHopInfo, &pTeHopInfo->u1LsrPartOfErHop)
            == TE_FAILURE)
        {

            TE_DBG (TE_EXTN_PRCS, "EXTN : TeIsLsrPartOfErHop failed \n");
        }

        TE_SLL_INIT_NODE (&TE_ERHOP_NEXT_NODE (pTeHopInfo));

        TE_SLL_ADD (TE_PO_HOP_LIST (((tTePathInfo *) pTePathInfo)),
                    &(pTeHopInfo->NextHop));
    }

    TE_DBG (TE_EXTN_PRCS,
            "EXTN : HopListInfo allocated and ErHops are copied \n");
    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeCreatePathOptionInfo : EXIT \n");
    MPLS_CMN_UNLOCK ();
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeSigCreatePathListAndPathOption                                      
 * Description   : This routine creates the path list and path option
 * Input(s)      : ppTePathListInfo - Address of the pointer of TePathListInfo                      
 * Output(s)     : ppTePathListInfo - Address of the pointer of TePathListInfo  
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                   
 *****************************************************************************/
UINT1
TeSigCreatePathListAndPathOption (tTePathListInfo ** ppTePathListInfo)
{
    tTePathInfo        *pTePathInfo = NULL;
    tTePathListInfo    *pTeTempPathListInfo = NULL;
    UINT2               u2DefPathIndex = TE_ONE;

    TE_DBG (TE_EXTN_ETEXT,
            "EXTN : TeSigCreatePathListAndPathOption : ENTRY \n");
    MPLS_CMN_LOCK ();
    /* Admin Status Check made in All Exported Functions */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        TE_DBG (TE_EXTN_FAIL,
                "EXTN : TE Admin Status DOWN. No Further processing \n");
        TE_DBG (TE_EXTN_ETEXT,
                "EXTN : TeSigCreatePathListAndPathOption : INTMD-EXIT \n");
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }
    /* Don't Allow to Hack the Code */
    if (ppTePathListInfo == NULL)
    {
        TE_DBG (TE_EXTN_FAIL, "EXTN : Null Pointer provided in ErHopList \n");
        TE_DBG (TE_EXTN_ETEXT,
                "EXTN : TeSigCreatePathListAndPathOptiono : INTMD-EXIT \n");
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }
    /* A valid HopListIndex is  obtained */

    if (TeCreatePathListInfo (ppTePathListInfo) == TE_FAILURE)
    {
        TE_DBG (TE_EXTN_FAIL, "EXTN : PathListInfo allocation Failed \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeSigCreateHopListInfo : INTMD-EXIT \n");
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }
    pTeTempPathListInfo = *ppTePathListInfo;
    /*  The PathOptionInfo is created */
    if (TeCreatePathOptionInfo (TE_HOPLIST_INDEX (pTeTempPathListInfo),
                                u2DefPathIndex, &pTePathInfo) == TE_FAILURE)
    {
        TE_DBG (TE_EXTN_FAIL, "EXTN : PathOptionInfo allocation Failed \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeSigCreateHopListInfo : INTMD-EXIT \n");
        TeDeleteHopListInfo (*ppTePathListInfo);
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }
    TE_DBG (TE_EXTN_PRCS,
            "EXTN : HopListInfo allocated and ErHops are copied \n");
    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeCreatePathOptionInfo : EXIT \n");
    MPLS_CMN_UNLOCK ();
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeSigCreatePathListAndPathOption                                      
 * Description   : This routine reserves a tTePathListInfo structure from    
 *                 the global array and copies the ErHopList provided by the
 *                 signalling protocols.
 * Input(s)      : pErHopList - Ponter to the ErHopList                      
 * Output(s)     : ppTePathListInfo - Address of the pointer of TePathListInfo  
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                   
 *****************************************************************************/
UINT1
TeSigCreateErHopsForCspfComputedHops (tTePathListInfo ** ppTePathListInfo,
                                      tOsTeAppPath * pCspfPath)
{
    tTePathInfo        *pTePathInfo = NULL;
    tTeHopInfo         *pTeHopInfo = NULL;
    tTeHopInfo         *pTePrevHopInfo = NULL;
    tTeHopInfo         *pTempTeHopInfo = NULL;
    tTePathListInfo    *pTePathListInfo = NULL;
    tTMO_SLL           *pErHopList = NULL;
    UINT1               u1PathCnt = TE_ZERO;
    UINT4               u4TempTeHopIndx = TE_ONE;
    UINT4               u4TempAddr = TE_ZERO;

    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeSigCreateHopListInfo : ENTRY \n");
    MPLS_CMN_LOCK ();
    /* Admin Status Check made in All Exported Functions */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        TE_DBG (TE_EXTN_FAIL,
                "EXTN : TE Admin Status DOWN. No Further processing \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeSigCreateHopListInfo : INTMD-EXIT \n");
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }
    /* Don't Allow to Hack the Code */
    if ((ppTePathListInfo == NULL) || (pCspfPath == NULL))
    {
        TE_DBG (TE_EXTN_FAIL, "EXTN : Null Pointer provided in ErHopList \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeSigCreateHopListInfo : INTMD-EXIT \n");
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }

    pTePathListInfo = *ppTePathListInfo;
    pTePathInfo = (tTePathInfo *) (TMO_SLL_First
                                   (&((pTePathListInfo)->PathOptionList)));
    if (pTePathInfo == NULL)
    {
        TE_DBG (TE_EXTN_FAIL, "EXTN : Null Pointer provided for Path Info \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeSigCreateHopListInfo : INTMD-EXIT \n");
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }
    pErHopList = TE_PO_HOP_LIST (((tTePathInfo *) pTePathInfo));

    for (u1PathCnt = TE_ZERO; u1PathCnt < (pCspfPath->u2HopCount); u1PathCnt++)
    {
        pTeHopInfo = (tTeHopInfo *) TE_ALLOC_MEM_BLOCK (TE_HOP_POOL_ID);

        if (pTeHopInfo == NULL)
        {
            TE_DBG (TE_EXTN_FAIL, "EXTN : TeHopInfo allocation Failed \n");
            TE_DBG (TE_EXTN_ETEXT,
                    "EXTN : TeSigCreateHopListInfo : INTMD-EXIT \n");
            /* The DeleteHopListInfo will delete the HopListIndex,
             * PathOptionInfo and the ErHops so far created, Hence we 
             * are not calling DeletePathOptionInfo
             */
            TeDeleteHopListInfo (pTePathListInfo);
            MPLS_CMN_UNLOCK ();
            return TE_FAILURE;
        }

        MEMSET (pTeHopInfo, TE_ZERO, sizeof (tTeHopInfo));
        TE_ERHOP_ROW_STATUS (pTeHopInfo) = TE_ACTIVE;
        TE_ERHOP_TYPE (pTeHopInfo) = TE_STRICT_ER;
        TE_ERHOP_ADDR_TYPE (pTeHopInfo) = TE_ERHOP_IPV4_TYPE;
        TE_ERHOP_ADDR_PRFX_LEN (pTeHopInfo) = MAX_TE_HOP_ADDRESS_SIZE;
        u4TempAddr =
            OSIX_HTONL (pCspfPath->aNextHops[u1PathCnt].u4NextHopIpAddr);
        pTeHopInfo->GmplsTnlHopInfo.u4ForwardLbl = MPLS_INVALID_LABEL;
        pTeHopInfo->GmplsTnlHopInfo.u4ReverseLbl = MPLS_INVALID_LABEL;
        MEMCPY (&TE_ERHOP_IP_ADDR (pTeHopInfo), &u4TempAddr, sizeof (UINT4));
        TE_SLL_INIT_NODE (&TE_ERHOP_NEXT_NODE (pTeHopInfo));

        if (u1PathCnt == TE_ZERO)
        {
            TE_SLL_INSERT (pErHopList, NULL, &(pTeHopInfo->NextHop));
        }
        else
        {
            TE_SLL_INSERT (pErHopList, &(pTePrevHopInfo->NextHop),
                           &(pTeHopInfo->NextHop));
        }
        pTePrevHopInfo = pTeHopInfo;
        TE_ERHOP_INDEX (pTeHopInfo) = u4TempTeHopIndx;
        u4TempTeHopIndx++;
    }
    pTempTeHopInfo = (tTeHopInfo *) TE_SLL_NEXT (pErHopList,
                                                 &(pTePrevHopInfo->NextHop));
    while (pTempTeHopInfo != NULL)
    {
        TE_ERHOP_INDEX (pTempTeHopInfo) = u4TempTeHopIndx++;
        pTempTeHopInfo = (tTeHopInfo *) TE_SLL_NEXT (pErHopList,
                                                     &(pTempTeHopInfo->
                                                       NextHop));
    }
    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeCreatePathOptionInfo : EXIT \n");
    MPLS_CMN_UNLOCK ();
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeSigDeleteHopInfo                                   
 * Description   : This routine releases the tTeHopInfo structure to the
 *                 Memory Pool.                                        
 * Input(s)      : pPathListInfo - Pointer to tTePathListInfo
 *               : pPathInfo - Pointer to tTePathInfo
 *               : pTeHopInfo - Pointer to tTeHopInfo
 * Output(s)     : NONE                                              
 * Return(s)     : TE_SUCCESS / TE_FAILURE                          
 *****************************************************************************/
UINT1
TeSigDeleteHopInfo (tTePathListInfo * pPathListInfo,
                    tTePathInfo * pPathInfo, tTeHopInfo * pTeHopInfo)
{
    UINT1               u1Return = TE_FAILURE;
    MPLS_CMN_LOCK ();
    u1Return = TeDeleteHopInfo (pPathListInfo, pPathInfo, pTeHopInfo);
    MPLS_CMN_UNLOCK ();
    return u1Return;
}

/*****************************************************************************/
/* Function Name : TeDeleteHopInfo                                   
 * Description   : This routine releases the tTeHopInfo structure to the
 *                 Memory Pool.                                        
 * Input(s)      : pPathListInfo - Pointer to tTePathListInfo
 *               : pPathInfo - Pointer to tTePathInfo
 *               : pTeHopInfo - Pointer to tTeHopInfo
 * Output(s)     : NONE                                              
 * Return(s)     : TE_SUCCESS / TE_FAILURE                          
 *****************************************************************************/
UINT1
TeDeleteHopInfo (tTePathListInfo * pPathListInfo,
                 tTePathInfo * pPathInfo, tTeHopInfo * pTeHopInfo)
{
    tTeHopInfo         *pTempHopInfo = NULL;

    TE_DBG (TE_EXTN_ETEXT, "TeDeleteHopInfo : ENTRY \n");
    /* Admin Status Check made in All Exported Functions */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        TE_DBG (TE_EXTN_FAIL,
                "EXTN : TE Admin Status DOWN. No Further processing \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeDeleteHopInfo : INTMD-EXIT \n");
        return TE_FAILURE;
    }

    /* Don't Allow to Hack the Code */
    if ((pPathListInfo == NULL) || (pPathInfo == NULL) || (pTeHopInfo == NULL))
    {
        TE_DBG (TE_EXTN_FAIL,
                "EXTN : Null Pointer provided for TeDeleteHopInfo \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeDeleteHopInfo : INTMD-EXIT \n");
        return TE_FAILURE;
    }

    if ((TE_HOPLIST_INDEX (pPathListInfo)
         < TE_TNL_HOPLSTINDEX_MINVAL) ||
        (TE_HOPLIST_INDEX (pPathListInfo) > TE_MAX_HOP_LIST (gTeGblInfo)))
    {
        TE_DBG (TE_EXTN_FAIL, "EXTN : Invalid HopListIndex provided \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeDeleteHopListInfo : INTMD-EXIT \n");
        return TE_FAILURE;
    }

    TE_SLL_SCAN (TE_HOP_PO_LIST (pPathListInfo), pPathInfo, tTePathInfo *)
    {
        TE_SLL_SCAN (TE_PO_HOP_LIST (pPathInfo), pTempHopInfo, tTeHopInfo *)
        {

            if (TE_ERHOP_INDEX (pTempHopInfo) == TE_ERHOP_INDEX (pTeHopInfo))
            {
                if (MEMCMP (&TE_ERHOP_IPV4_ADDR (pTempHopInfo),
                            &TE_ERHOP_IPV4_ADDR (pTeHopInfo),
                            IPV4_ADDR_LENGTH) == TE_ZERO)
                {

                    TE_SLL_DELETE (TE_PO_HOP_LIST (pPathInfo),
                                   &(pTempHopInfo->NextHop));

                    MEMSET (pTempHopInfo, TE_ZERO, sizeof (tTeHopInfo));
                    /* Releasing the memory back to the memory pool */
                    if ((TE_REL_MEM_BLOCK (TE_HOP_POOL_ID,
                                           (UINT1 *) (pTeHopInfo))) ==
                        TE_MEM_FAILURE)
                    {
                        TE_DBG (TE_EXTN_FAIL,
                                "EXTN : ErHop Deletion failed  \n");
                        TE_DBG (TE_EXTN_ETEXT,
                                "EXTN : TeDeleteHopInfo : INTMD-EXIT \n");
                        return TE_FAILURE;
                    }
                    (TE_PO_HOP_COUNT (pPathInfo))--;
                    TE_DBG (TE_EXTN_PRCS, "EXTN : ErHop Deletion Success  \n");
                    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeDeleteHopInfo : EXIT \n");
                    return TE_SUCCESS;
                }

            }
        }
    }

    TE_DBG (TE_EXTN_FAIL, "EXTN : ErHop Deletion failed - Hop Not Found \n");
    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeDeleteHopInfo : INTMD-EXIT \n");
    return TE_FAILURE;
}

/*****************************************************************************/
/* Function Name : TeSigDeleteHopListInfo                               
 * Description   : This routine releases the PathListInfo and all the 
 *                 associated PathOptionInfo, HopInfo's. This is called
 *                 from signalling module                 
 * Input(s)      : pPathListInfo - Pointer to tTePathListInfo         
 * Output(s)     : NONE                                              
 * Return(s)     : TE_SUCCESS / TE_FAILURE                          
 *****************************************************************************/
UINT1
TeSigDeleteHopListInfo (tTePathListInfo * pPathListInfo)
{
    UINT1               u1Return = TE_FAILURE;
    MPLS_CMN_LOCK ();
    u1Return = TeDeleteHopListInfo (pPathListInfo);
    MPLS_CMN_UNLOCK ();
    return u1Return;
}

/*****************************************************************************/
/* Function Name : TeDeleteHopListInfo                               
 * Description   : This routine releases the PathListInfo and all the 
 *                 associated PathOptionInfo, HopInfo's
 * Input(s)      : pPathListInfo - Pointer to tTePathListInfo         
 * Output(s)     : NONE                                              
 * Return(s)     : TE_SUCCESS / TE_FAILURE                          
 *****************************************************************************/
UINT1
TeDeleteHopListInfo (tTePathListInfo * pPathListInfo)
{
    tTePathInfo        *pPathInfo = NULL;
    tTeHopInfo         *pTempHopInfo = NULL;

    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeDeleteHopListInfo : ENTRY \n");
    /* Admin Status Check made in All Exported Functions */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        TE_DBG (TE_EXTN_FAIL,
                "EXTN : TE Admin Status DOWN. No Further processing \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeDeleteHopListInfo : INTMD-EXIT \n");
        return TE_FAILURE;
    }

    /* Don't Allow to Hack the Code */
    if (pPathListInfo == NULL)
    {
        TE_DBG (TE_EXTN_FAIL,
                "EXTN : Null Pointer provided for TeSigDeleteHopInfo \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeDeleteHopListInfo : INTMD-EXIT \n");
        return TE_FAILURE;
    }

    if ((TE_HOPLIST_INDEX (pPathListInfo)
         < TE_TNL_HOPLSTINDEX_MINVAL) ||
        (TE_HOPLIST_INDEX (pPathListInfo) > TE_MAX_HOP_LIST (gTeGblInfo)))
    {
        TE_DBG (TE_EXTN_FAIL, "EXTN : Invalid HopListIndex provided \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeDeleteHopListInfo : INTMD-EXIT \n");
        return TE_FAILURE;
    }

    pPathInfo = (tTePathInfo *) TMO_SLL_First (TE_HOP_PO_LIST (pPathListInfo));
    while (pPathInfo != NULL)
    {
        if ((TE_PO_NUM_TUNNELS (pPathInfo) == TE_ONE) ||
            (TE_PO_NUM_TUNNELS (pPathInfo) == TE_ZERO))
        {
            pTempHopInfo = (tTeHopInfo *) TMO_SLL_First
                (TE_PO_HOP_LIST (pPathInfo));
            while (pTempHopInfo != NULL)
            {
                TE_SLL_DELETE (TE_PO_HOP_LIST (pPathInfo),
                               &(pTempHopInfo->NextHop));

                MEMSET (pTempHopInfo, TE_ZERO, sizeof (tTeHopInfo));
                /* Releasing the memory back to the memory pool */
                if ((TE_REL_MEM_BLOCK (TE_HOP_POOL_ID,
                                       (UINT1 *) (pTempHopInfo))) ==
                    TE_MEM_FAILURE)
                {
                    TE_DBG (TE_EXTN_FAIL,
                            "EXTN : ErHop Deletion failed -  "
                            "Log and Continue \n");
                }
                pTempHopInfo = (tTeHopInfo *) TMO_SLL_First
                    (TE_PO_HOP_LIST (pPathInfo));
            }
            TE_SLL_DELETE (TE_HOP_PO_LIST (pPathListInfo),
                           &(pPathInfo->NextPathOption));
            TE_PO_NUM_TUNNELS (pPathInfo) = TE_ZERO;

            MEMSET (pPathInfo, TE_ZERO, sizeof (tTePathInfo));
            if ((TE_REL_MEM_BLOCK (TE_PO_POOL_ID,
                                   (UINT1 *) (pPathInfo))) == TE_MEM_FAILURE)
            {
                TE_DBG (TE_EXTN_FAIL,
                        "EXTN : PathOptionInfo Deletion failed "
                        "- Log and Continue \n");
            }
        }
        else
        {
            TE_DBG (TE_EXTN_PRCS,
                    "EXTN : No of Tunnels using this HopList is reduced\n");
            TE_PO_NUM_TUNNELS (pPathInfo)--;
            return TE_FAILURE;
        }
        pPathInfo = (tTePathInfo *) TMO_SLL_First
            (TE_HOP_PO_LIST (pPathListInfo));
    }
    if (TE_SLL_COUNT (TE_HOP_PO_LIST (pPathListInfo)) == TE_ZERO)
    {
        MEMSET ((VOID *) (TE_PATH_LIST_ENTRY
                          (TE_HOPLIST_INDEX (pPathListInfo))), TE_ZERO,
                sizeof (tTePathListInfo));
    }
    TE_DBG (TE_EXTN_PRCS,
            "EXTN : PathListInfo, PathOptionInfo and ErHops removed \n");
    TE_DBG (TE_EXTN_ETEXT, "TeDeleteHopListInfo : EXIT \n");
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeSigCreateTrfcParams                                        
 * Description   : Allocates memory for the a resource info structure and,    
 *                 initialises all its fields and adds it to the             
 *                 Resource table. Called from Signalling Module.
 *                 It also copies the Traffic Parameters provided by the
 *                 Signalling protocols to the Protocol specific TrfcParams.
 * Input(s)      : u1Protocol       - Tunnel Signalling Protocol             
 *               : pTeTrfcParams    - Pointer to the TrfcParams    
 * Output(s)     : ppTeTrfcParams   - Address of the TrfcParams in case of 
 *                 successful allocation.                    
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                 
 *****************************************************************************/
UINT1
TeSigCreateTrfcParams (UINT1 u1Protocol,
                       tTeTrfcParams ** ppTeTrfcParams,
                       tTeTrfcParams * pTempTeTrfcParams)
{
    UINT1               u1Return = TE_FAILURE;
    MPLS_CMN_LOCK ();
    u1Return =
        TeCreateTrfcParams (u1Protocol, ppTeTrfcParams, pTempTeTrfcParams);
    MPLS_CMN_UNLOCK ();
    return u1Return;
}

/*****************************************************************************/
/* Function Name : TeCreateTrfcParams                                        
 * Description   : Allocates memory for the a resource info structure and,    
 *                 initialises all its fields and adds it to the             
 *                 Resource table.
 *                 It also copies the Traffic Parameters provided by the
 *                 Signalling protocols to the Protocol specific TrfcParams.
 * Input(s)      : u1Protocol       - Tunnel Signalling Protocol             
 *               : pTeTrfcParams    - Pointer to the TrfcParams    
 * Output(s)     : ppTeTrfcParams   - Address of the TrfcParams in case of 
 *                 successful allocation.                    
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                 
 *****************************************************************************/
UINT1
TeCreateTrfcParams (UINT1 u1Protocol,
                    tTeTrfcParams ** ppTeTrfcParams,
                    tTeTrfcParams * pTempTeTrfcParams)
{
    tTeTrfcParams      *pTmpTeTrfcParams = NULL;
    UINT4               u4TrfcParamArrayIndex;
    UINT4               u4AvailTrfcParamIndex = TE_ZERO;

    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeCreateTrfcParams : ENTRY \n");
    /* Admin Status Check made in All Exported Functions */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        TE_DBG (TE_EXTN_FAIL,
                "EXTN : TE Admin Status DOWN. No Further processing \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeCreateTrfcParams : INTMD-EXIT \n");
        return TE_FAILURE;
    }

    /* Don't Allow to Hack the Code */
    if ((ppTeTrfcParams == NULL) || (pTempTeTrfcParams == NULL))
    {
        TE_DBG (TE_EXTN_FAIL,
                "EXTN : Null Pointer provided for TeTrfcParams \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeCreateTrfcParams : INTMD-EXIT \n");
        return TE_FAILURE;
    }
    for (u4TrfcParamArrayIndex = TE_ONE;
         u4TrfcParamArrayIndex <= TE_MAX_TRFC_PARAMS (gTeGblInfo);
         u4TrfcParamArrayIndex++)
    {
        if (TE_TRFC_PARAMS_TNL_RESINDEX (u4TrfcParamArrayIndex) == TE_ZERO)
        {
            u4AvailTrfcParamIndex = u4TrfcParamArrayIndex;
            break;
        }
    }
    if (u4AvailTrfcParamIndex == TE_ZERO)
    {
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeCreateTrfcParams : INTMD_EXIT \n");
        return TE_FAILURE;
    }
    TE_DBG1 (TE_EXTN_PRCS,
             "EXTN : TrafficParm allocated successfully TrfcParamIndex : %d \n",
             u4TrfcParamArrayIndex);
    pTmpTeTrfcParams = TE_TRFC_PARAMS_PTR (u4AvailTrfcParamIndex);
    INIT_TE_TRFC_PARAMS (pTmpTeTrfcParams);
    switch (u1Protocol)
    {
        case TE_SIGPROTO_LDP:
        {
            TE_CRLDP_TRFC_PARAMS (pTmpTeTrfcParams) = (tCRLDPTrfcParams *)
                TE_ALLOC_MEM_BLOCK (TE_CRLDP_TRFC_PARAM_POOL_ID);

            if (TE_CRLDP_TRFC_PARAMS (pTmpTeTrfcParams) == NULL)
            {
                TE_DBG (TE_EXTN_FAIL,
                        "EXTN : CrLdp TrfcParam allocation failed \n");
                TE_DBG (TE_EXTN_ETEXT,
                        "EXTN : TeCreateTrfcParams : INTMD_EXIT \n");
                return TE_FAILURE;
            }
            INIT_TE_CRLDP_TRFC_PARAMS (TE_CRLDP_TRFC_PARAMS (pTmpTeTrfcParams));
            MEMCPY (TE_CRLDP_TRFC_PARAMS (pTmpTeTrfcParams),
                    TE_CRLDP_TRFC_PARAMS (pTempTeTrfcParams),
                    sizeof (tCRLDPTrfcParams));

            TE_TNLRSRC_INDEX (pTmpTeTrfcParams) = u4AvailTrfcParamIndex;
            TE_TNLRSRC_ROLE (pTmpTeTrfcParams) =
                TE_TNLRSRC_ROLE (pTempTeTrfcParams);
            TE_TNLRSRC_OWNER (pTmpTeTrfcParams) = TE_TNL_OWNER_CRLDP;
            TE_TNLRSRC_ROW_STATUS (pTmpTeTrfcParams) = TE_ACTIVE;
            TE_DBG (TE_EXTN_PRCS,
                    "EXTN : CrLdp TrfcParam allocated successfully \n");
            *ppTeTrfcParams = pTmpTeTrfcParams;
            break;
        }
        case TE_SIGPROTO_RSVP:
        {
            TE_RSVPTE_TRFC_PARAMS (pTmpTeTrfcParams) = (tRSVPTrfcParams *)
                TE_ALLOC_MEM_BLOCK (TE_RSVP_TRFC_PARAM_POOL_ID);

            if (TE_RSVPTE_TRFC_PARAMS (pTmpTeTrfcParams) == NULL)
            {
                TE_DBG (TE_EXTN_FAIL,
                        "EXTN : RSVP TrfcParam allocation failed \n");
                TE_DBG (TE_EXTN_ETEXT,
                        "EXTN : TeCreateTrfcParams : INTMD_EXIT \n");
                return TE_FAILURE;
            }
            INIT_TE_RSVPTE_TRFC_PARAMS (TE_RSVPTE_TRFC_PARAMS
                                        (pTmpTeTrfcParams));

            MEMCPY (TE_RSVPTE_TRFC_PARAMS (pTmpTeTrfcParams),
                    TE_RSVPTE_TRFC_PARAMS (pTempTeTrfcParams),
                    sizeof (tRSVPTrfcParams));

            TE_TNLRSRC_INDEX (pTmpTeTrfcParams) = u4AvailTrfcParamIndex;
            TE_TNLRSRC_OWNER (pTmpTeTrfcParams) = TE_TNL_OWNER_RSVP;
            TE_TNLRSRC_ROW_STATUS (pTmpTeTrfcParams) = TE_ACTIVE;
            TE_DBG (TE_EXTN_PRCS,
                    "EXTN : RSVP TrfcParam allocated successfully \n");
            *ppTeTrfcParams = pTmpTeTrfcParams;
            break;
        }
        default:
            TE_DBG (TE_EXTN_ETEXT, "EXTN : TeCreateTrfcParams : INTMD_EXIT \n");
            return TE_FAILURE;
    }
    TE_DBG (TE_EXTN_PRCS,
            "EXTN : TeTrfcParam allocated and contents copied \n");
    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeCreateTrfcParams : EXIT \n");
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeSigDeleteTrfcParams                                        
 * Description   : Deletes the memory of a resourceinfo structure and,    
 *                 initialises all its fields. Also deletes the Signalling
 *                 specific TrafficParameters. This API is called from 
 *                 signalling
 *                 If the Number of Tunnels associated with this 
 *                 Resource is more than 1 then only TnlCount alone is
 *                 decremented.
 * Input(s)      : pTeTrfcParams - Pointer to Pointer of the TrfcParams
 * Output(s)     : NONE
 * Return(s)     : TE_SUCCESS / TE_FAILURE
 *****************************************************************************/
UINT1
TeSigDeleteTrfcParams (tTeTrfcParams * pTeTrfcParams)
{
    UINT1               u1Return = TE_FAILURE;
    MPLS_CMN_LOCK ();
    u1Return = TeDeleteTrfcParams (pTeTrfcParams);
    MPLS_CMN_UNLOCK ();
    return u1Return;
}

/*****************************************************************************/
/* Function Name : TeDeleteTrfcParams                                        
 * Description   : Deletes the memory of a resourceinfo structure and,    
 *                 initialises all its fields. Also deletes the Signalling
 *                 specific TrafficParameters.
 *                 If the Number of Tunnels associated with this 
 *                 Resource is more than 1 then only TnlCount alone is
 *                 decremented.
 * Input(s)      : pTeTrfcParams - Pointer to Pointer of the TrfcParams
 * Output(s)     : NONE
 * Return(s)     : TE_SUCCESS / TE_FAILURE
 *****************************************************************************/
UINT1
TeDeleteTrfcParams (tTeTrfcParams * pTeTrfcParams)
{

    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeDeleteTrfcParams : ENTRY \n");
    /* Admin Status Check made in All Exported Functions */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        TE_DBG (TE_EXTN_FAIL,
                "EXTN : TE Admin Status DOWN. No Further processing \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeDeleteTrfcParams : INTMD-EXIT \n");
        return TE_FAILURE;
    }

    /* Don't Allow to Hack the Code */
    if (pTeTrfcParams == NULL)
    {
        TE_DBG (TE_EXTN_FAIL,
                "EXTN : Null Pointer provided for TeTrfcParams \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeDeleteTrfcParams : INTMD-EXIT \n");
        return TE_FAILURE;
    }

    if ((TE_TNLRSRC_INDEX (pTeTrfcParams) < TE_TNL_RSRC_INDEX_MINVAL) ||
        (TE_TNLRSRC_INDEX (pTeTrfcParams) > TE_MAX_TRFC_PARAMS (gTeGblInfo)))
    {
        TE_DBG (TE_EXTN_FAIL, "EXTN : Invalid TrfcRsrcIndex provided \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeDeleteTrfcParams : INTMD-EXIT \n");
        return TE_FAILURE;
    }
    /* 
     * The Traffic Params can be deleted only when the No of Tunnels utilizing
     * the Traffic params is (zero) or (one in case of Intermediate scenario)
     * Other cases the no of tunnels count is decremented
     */
    if ((TE_TNLRSRC_NUM_OF_TUNNELS (pTeTrfcParams) == TE_ONE) ||
        (TE_TNLRSRC_NUM_OF_TUNNELS (pTeTrfcParams) == TE_ZERO))
    {
        if (TE_TNLRSRC_INDEX (pTeTrfcParams) != TE_ONE)
        {
            if (pTeTrfcParams->pRSVPTrfcParams != NULL)
            {
                MEMSET (pTeTrfcParams->pRSVPTrfcParams,
                        TE_ZERO, sizeof (tRSVPTrfcParams));
                if ((TE_REL_MEM_BLOCK (TE_RSVP_TRFC_PARAM_POOL_ID,
                                       (UINT1 *) (pTeTrfcParams->
                                                  pRSVPTrfcParams))) ==
                    TE_MEM_FAILURE)
                {
                    TE_DBG (TE_EXTN_FAIL,
                            "EXTN : RSVP TrfcParam Deletion failed \n");
                    TE_DBG (TE_EXTN_ETEXT,
                            "EXTN : TeDeleteTrfcParams : INTMD-EXIT \n");
                    return TE_FAILURE;
                }
                pTeTrfcParams->pRSVPTrfcParams = NULL;
                TE_TNLRSRC_NUM_OF_TUNNELS (pTeTrfcParams) = TE_ZERO;
            }
            if (pTeTrfcParams->pCRLDPTrfcParams != NULL)
            {
                MEMSET (pTeTrfcParams->pCRLDPTrfcParams,
                        TE_ZERO, sizeof (tCRLDPTrfcParams));
                if ((TE_REL_MEM_BLOCK (TE_CRLDP_TRFC_PARAM_POOL_ID,
                                       (UINT1 *) (pTeTrfcParams->
                                                  pCRLDPTrfcParams))) ==
                    TE_MEM_FAILURE)
                {
                    TE_DBG (TE_EXTN_FAIL,
                            "EXTN : CRLDP TrfcParam Deletion failed \n");
                    TE_DBG (TE_EXTN_ETEXT,
                            "EXTN : TeDeleteTrfcParams : INTMD-EXIT \n");
                    return TE_FAILURE;
                }
                pTeTrfcParams->pCRLDPTrfcParams = NULL;
                TE_TNLRSRC_NUM_OF_TUNNELS (pTeTrfcParams) = TE_ZERO;
            }
            TE_DBG (TE_EXTN_PRCS,
                    "EXTN : TeTrfcParameter Deleted successfully \n");
            TE_TNLRSRC_INDEX (pTeTrfcParams) = TE_ZERO;
        }
        else if (TE_TNLRSRC_NUM_OF_TUNNELS (pTeTrfcParams) != TE_ZERO)
        {
            TE_DBG (TE_EXTN_PRCS,
                    "EXTN : No of Tnls count decremented in TrfcParams \n");
            TE_TNLRSRC_NUM_OF_TUNNELS (pTeTrfcParams)--;
        }
    }
    else if (TE_TNLRSRC_NUM_OF_TUNNELS (pTeTrfcParams) != TE_ZERO)
    {
        TE_DBG (TE_EXTN_PRCS,
                "EXTN : No of Tnls count decremented in TrfcParams \n");
        TE_TNLRSRC_NUM_OF_TUNNELS (pTeTrfcParams)--;
    }
    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeDeleteTrfcParams : EXIT \n");
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeSigUpdateTrfcParams
 * Description   : This routine updates the Traffic Parameter with
 *                 specified values.
 *                 This function is called by ignaling protocol when it needs
 *                 update the traffic parameter already created, with a new
 *                 set of values.
 * Input(s)      : pTeTrfcParams - Pointer to the Current Traffic Parameter.
 *                 pTempTeTrfcParams - Pointer to the temp Traffic Parameter
 *                                     that contains the changed values.
 *                 pTeTnlInfo - Pointer to the TE Tunnel Info with which the
 *                              Traffic Parameter is associated.
 * Output(s)     : None
 * Return(s)     : TE_SUCCESS / TE_FAILURE
 *****************************************************************************/
UINT1
TeSigUpdateTrfcParams (tTeTrfcParams * pTeTrfcParams,
                       tTeTrfcParams * pTempTeTrfcParams,
                       tTeTnlInfo * pTeTnlInfo)
{
    tTeTrfcParams      *pNewTrfcParams = NULL;

    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeSigUpdateTrfcParams : ENTRY \n");
    MPLS_CMN_LOCK ();
    /* Admin Status Check made in All Exported Functions */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        TE_DBG (TE_EXTN_FAIL,
                "EXTN : TE Admin Status DOWN. No Further processing \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeSigUpdateTrfcParams : INTMD-EXIT \n");
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }

    /* Don't Allow to Hack the Code */
    if ((pTeTrfcParams == NULL) || (pTempTeTrfcParams == NULL) ||
        (pTeTnlInfo == NULL))
    {
        TE_DBG (TE_EXTN_FAIL,
                "EXTN : Null Pointer provided for TeTrfcParams \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeCreateTrfcParams : INTMD-EXIT \n");
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }
    /* This function is now called only by LDP */
    if (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_LDP)
    {
        if (TE_TNLRSRC_ROLE (pTeTrfcParams) != TE_INGRESS)
        {
            /* At the Intermediate, just the value in the temp Traffic 
             * Parameter is copied onto the Current Traffic Parameter. */
            MEMCPY ((UINT1 *) TE_CRLDP_TRFC_PARAMS (pTeTrfcParams),
                    (UINT1 *) TE_CRLDP_TRFC_PARAMS (pTempTeTrfcParams),
                    sizeof (tCRLDPTrfcParams));
            TE_DBG (TE_EXTN_PRCS,
                    "EXTN : CrLdp Traffic Params is overwritten \n");
            TE_DBG (TE_EXTN_ETEXT, "EXTN : TeSigUpdateTrfcParams : EXIT \n");
            MPLS_CMN_UNLOCK ();
            return TE_SUCCESS;
        }
        else
        {
            /* This is the case of Ingress
             * A New Traffic Parameter is created */
            if (TeCreateTrfcParams (TE_SIGPROTO_LDP, &pNewTrfcParams,
                                    pTempTeTrfcParams) == TE_FAILURE)
            {
                TE_DBG (TE_EXTN_FAIL,
                        "EXTN : A New TeTrfcParam cannot be created \n");
                TE_DBG (TE_EXTN_ETEXT,
                        "EXTN : TeSigUpdateTrfcParams : INTMD-EXIT \n");
                MPLS_CMN_UNLOCK ();
                return TE_FAILURE;
            }
            /* Number of tunnels associated with the Current Traffic Parameter
             * is decremented by one. */

            (TE_TNLRSRC_NUM_OF_TUNNELS (pTeTrfcParams))--;

            /* The New Traffic Parameter is associated with the Tunnel 
             * and the Number of tunnels pointing to the new traffic
             * Parameter is incremented by one. */
            TE_TNLRSRC_ROLE (pNewTrfcParams) = TE_INGRESS;
            (TE_TNLRSRC_NUM_OF_TUNNELS (pNewTrfcParams))++;
            TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo) =
                TE_TNLRSRC_INDEX (pNewTrfcParams);
            TE_TNL_TRFC_PARAM (pTeTnlInfo) = pNewTrfcParams;
            TE_DBG (TE_EXTN_PRCS,
                    "EXTN : New TeTrafficParam is created for CR-LDP \n");
            TE_DBG (TE_EXTN_ETEXT, "EXTN : TeSigUpdateTrfcParams : EXIT \n");
            MPLS_CMN_UNLOCK ();
            return TE_SUCCESS;
        }
    }
    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeSigUpdateTrfcParams : INTMD-EXIT \n");
    MPLS_CMN_UNLOCK ();
    return TE_FAILURE;
}

/*****************************************************************************/
/* Function Name : TeSigUpdateTeTnl
 * Description   : This routine updates TE Tunnel information
 * Input(s)      : pTeTnlInfo - Pointer to the TE Tunnel Info with which the
 *                              Traffic Parameter is associated.
 * Output(s)     : None
 * Return(s)     : TE_SUCCESS / TE_FAILURE
 *****************************************************************************/
UINT1
TeSigUpdateTeTnl (tTeTnlInfo * pTeTnlInfo)
{
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;
    BOOL1               bIsPathChanged = TE_FALSE;

    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeSigUpdateTeTnl : ENTRY \n");
    MPLS_CMN_LOCK ();
    /* Admin Status Check made in All Exported Functions */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        TE_DBG (TE_EXTN_FAIL,
                "EXTN : TE Admin Status DOWN. No Further processing \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeSigUpdateTeTnl : INTMD-EXIT \n");
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }

    /* Don't Allow to Hack the Code */
    if (pTeTnlInfo == NULL)
    {
        TE_DBG (TE_EXTN_FAIL, "EXTN : Null Pointer provided for TnlInfo \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeSigUpdateTeTnl : INTMD-EXIT \n");
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }
    CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTeTnlInfo)), u4IngressId);
    CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTeTnlInfo)), u4EgressId);

    pTeTnlInfo = TeGetTunnelInfo (TE_TNL_TNL_INDEX (pTeTnlInfo),
                                  TE_TNL_TNL_INSTANCE (pTeTnlInfo),
                                  TE_NTOHL (u4IngressId),
                                  TE_NTOHL (u4EgressId));

    if (pTeTnlInfo == NULL)
    {
        TE_DBG (TE_EXTN_FAIL,
                "EXTN : Tnl With the Same Indexes is present already \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeSigUpdateTeTnl : INTMD-EXIT \n");
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }

    if ((TE_TNL_OWNER (pTeTnlInfo) != TE_TNL_OWNER_CRLDP) &&
        ((TE_TNL_OWNER (pTeTnlInfo) != TE_TNL_OWNER_RSVP) &&
         (TE_TNL_OWNER (pTeTnlInfo) != TE_TNL_OWNER_LDP)))
    {
        TE_DBG (TE_EXTN_FAIL, "EXTN : Tnl Owner Wrong Value | not  present \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeSigUpdateTeTnl : INTMD-EXIT \n");
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }
    if (TE_TNL_PATH_LIST_INFO (pTeTnlInfo) != NULL)
    {
        TE_TNL_PATH_INFO (pTeTnlInfo) =
            (tTePathInfo *) TMO_SLL_First (TE_HOP_PO_LIST
                                           (TE_TNL_PATH_LIST_INFO
                                            (pTeTnlInfo)));
        if (TE_TNL_PATH_INFO (pTeTnlInfo) != NULL)
        {
            if (TeIsAllErHopsActive (TE_TNL_PATH_INFO (pTeTnlInfo),
                                     &bIsPathChanged) == TE_TRUE)
            {
                /* Incrementing the number of tunnels associated with 
                   the Er-hop List.  */

                TE_PO_NUM_TUNNELS (TE_TNL_PATH_INFO (pTeTnlInfo))++;
                TE_DBG (TE_EXTN_PRCS,
                        "EXTN : No of Tnls count incremented in "
                        "PathListInfo \n");
            }
            else
            {
                if ((TE_REL_MEM_BLOCK (TE_TNL_POOL_ID,
                                       (UINT1 *) (pTeTnlInfo))) ==
                    TE_MEM_FAILURE)
                {
                    TE_DBG (TE_EXTN_FAIL,
                            "EXTN : MemRelease Memblock failed "
                            "for TnlInfo \n");
                }
                TE_DBG (TE_EXTN_ETEXT,
                        "EXTN : TeSigCreateNewTnl : INTMD-EXIT \n");
                MPLS_CMN_UNLOCK ();
                return TE_FAILURE;
            }
        }
    }

    /* Finding the Resource table with the Index and assigning 
       the Corresponding Traffic params to the Tunnel structure */

    if ((TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo)) != TE_ZERO)
    {
        if ((TE_TRFC_PARAMS_TNL_RESINDEX
             ((TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo))) ==
             (TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo))) &&
            (TE_TRFC_PARAMS_TNL_RESROWSTATUS
             ((TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo))) == TE_ACTIVE))
        {
            if (TE_TNL_TRFC_PARAM (pTeTnlInfo) != NULL)
            {
                TE_TNLRSRC_NUM_OF_TUNNELS ((TE_TNL_TRFC_PARAM (pTeTnlInfo)))++;
                TE_DBG (TE_EXTN_PRCS,
                        "EXTN : No of Tnls count incremented "
                        "in TrfcParams \n");
            }
        }
        else
        {
            if (TE_TNL_PATH_INFO (pTeTnlInfo) != NULL)
            {
                TE_PO_NUM_TUNNELS (TE_TNL_PATH_INFO (pTeTnlInfo))--;
            }
            if ((TE_REL_MEM_BLOCK (TE_TNL_POOL_ID,
                                   (UINT1 *) (pTeTnlInfo))) == TE_MEM_FAILURE)
            {
                TE_DBG (TE_EXTN_FAIL,
                        "EXTN : MemRelease Memblock failed for TnlInfo \n");
            }
            TE_DBG (TE_EXTN_ETEXT, "EXTN : TeSigCreateNewTnl : INTMD-EXIT \n");

            MPLS_CMN_UNLOCK ();
            return TE_FAILURE;
        }
    }

    TE_TNL_ISIF (pTeTnlInfo) = TE_TRUE;
    /* The following code is used as an indirect registration
     * mechanism for the signalling protocols
     */
    TE_TNL_OPER_STATUS (pTeTnlInfo) = TE_OPER_DOWN;
    if ((TE_TNL_OWNER (pTeTnlInfo) == TE_TNL_OWNER_RSVP) &&
        (!(TE_SIG_ADMIN_FLAG (gTeGblInfo) & 0x0f)))
    {
        TeAdminStatus (TE_TNL_OWNER_RSVP, TE_ADMIN_UP);
        TE_DBG (TE_EXTN_PRCS, "EXTN : RSVP protocol is registered \n");
    }

    if ((TE_TNL_OWNER (pTeTnlInfo) == TE_TNL_OWNER_LDP) &&
        (!(TE_SIG_ADMIN_FLAG (gTeGblInfo) & 0xf0)))
    {
        TeAdminStatus (TE_TNL_OWNER_LDP, TE_ADMIN_UP);
        TE_DBG (TE_EXTN_PRCS, "EXTN : LDP protocol is registered \n");
    }
    MPLS_CMN_UNLOCK ();
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeSigAdminStatus                                        
 * Description   : This function is indirectly used as a registration 
 *                 mechanism by the signalling protocols for receiving
 *                 the TE Events(TE_ADMIN_DOWN event)    
 * Input(s)      : u1Protocol       - Tunnel Signalling Protocol
 *               : u1AdminStatus    - Admin Status    
 * Output(s)     : NONE 
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                 
 *****************************************************************************/
UINT1
TeSigAdminStatus (UINT1 u1Protocol, UINT1 u1AdminStatus)
{
    UINT1               u1Return = TE_FAILURE;
    u1Return = TeAdminStatus (u1Protocol, u1AdminStatus);
    return u1Return;
}

/*****************************************************************************/
/* Function Name : TeAdminStatus                                        
 * Description   : This function is indirectly used as a registration 
 *                 mechanism by the signalling protocols for receiving
 *                 the TE Events(TE_ADMIN_DOWN event)    
 * Input(s)      : u1Protocol       - Tunnel Signalling Protocol
 *               : u1AdminStatus    - Admin Status    
 * Output(s)     : NONE 
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                 
 *****************************************************************************/
UINT1
TeAdminStatus (UINT1 u1Protocol, UINT1 u1AdminStatus)
{
    UINT1               u1IsRpteAdminDown = TE_FALSE;
    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeAdminStatus : ENTRY \n");
    /* Admin Status Check made in All Exported Functions */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        TE_DBG (TE_EXTN_FAIL,
                "EXTN : TE Admin Status DOWN. No Further processing \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeAdminStatus : INTMD-EXIT \n");
        return TE_FAILURE;
    }

    if ((u1Protocol == TE_SIGPROTO_NONE) && (u1AdminStatus != TE_ADMIN_DOWN))
    {
        TE_DBG (TE_EXTN_FAIL, "EXTN : Invalid Admin Status requested \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeAdminStatus : INTMD-EXIT \n");
        return TE_FAILURE;
    }
    switch (u1Protocol)
    {
        case TE_SIGPROTO_NONE:
            if ((TE_SIG_ADMIN_FLAG (gTeGblInfo) & TE_SIG_PROT_RSVP_SET)
                == TE_SIG_PROT_RSVP_SET)
            {
                TE_DBG (TE_EXTN_PRCS,
                        "EXTN : RSVPTE is ACTIVE and Admin Down event sent \n");
                if (TeRpteTEEventHandler (TE_GOING_DOWN, TE_ZERO,
                                          &u1IsRpteAdminDown)
                    == TE_RPTE_FAILURE)
                {
                    TE_DBG (TE_EXTN_PRCS, "Posting to RSVP-TE failed\n");
                }
            }
            if ((TE_SIG_ADMIN_FLAG (gTeGblInfo) & TE_SIG_PROT_LDP_SET)
                == TE_SIG_PROT_LDP_SET)
            {
                TE_DBG (TE_EXTN_PRCS,
                        "EXTN : CR-LDP is ACTIVE and Admin Down event sent \n");
                TeLdpTEEventHandler (TE_GOING_DOWN, TE_ZERO);
            }
            break;

        case TE_SIGPROTO_RSVP:
        {
            if (u1AdminStatus == TE_ADMIN_UP)
            {
                TE_SIG_ADMIN_FLAG (gTeGblInfo) |= TE_SIG_PROT_RSVP_SET;
                TE_DBG (TE_EXTN_PRCS,
                        "EXTN : RSVPTE is ACTIVE and received the Event \n");
            }
            else if (u1AdminStatus == TE_ADMIN_DOWN)
            {
                TE_SIG_ADMIN_FLAG (gTeGblInfo) &= TE_SIG_PROT_LDP_SET;
                TE_DBG (TE_EXTN_PRCS,
                        "EXTN : RSVPTE is ACTIVE and Removed all Entries \n");
            }
            else
            {
                TE_DBG (TE_EXTN_FAIL,
                        "EXTN : Invalid Admin Status requested from RSVPTE \n");
                TE_DBG (TE_EXTN_ETEXT, "EXTN : TeAdminStatus : INTMD-EXIT \n");
                return TE_FAILURE;
            }
            break;
        }

        case TE_SIGPROTO_LDP:
        {
            if (u1AdminStatus == TE_ADMIN_UP)
            {
                TE_SIG_ADMIN_FLAG (gTeGblInfo) |= TE_SIG_PROT_LDP_SET;
                TE_DBG (TE_EXTN_PRCS,
                        "EXTN : RSVPTE is ACTIVE and received the Event \n");
            }
            else if (u1AdminStatus == TE_ADMIN_DOWN)
            {
                TE_SIG_ADMIN_FLAG (gTeGblInfo) &= TE_SIG_PROT_RSVP_SET;
                TE_DBG (TE_EXTN_PRCS,
                        "EXTN : RSVPTE is ACTIVE and Removed all Entries \n");
            }
            else
            {
                TE_DBG (TE_EXTN_FAIL,
                        "EXTN : Invalid Admin Status requested from RSVPTE \n");
                TE_DBG (TE_EXTN_ETEXT, "EXTN : TeAdminStatus : INTMD-EXIT \n");
                return TE_FAILURE;
            }
            break;
        }
        default:
            TE_DBG (TE_EXTN_FAIL, "EXTN : Invalid Signalling Protocol \n");
            TE_DBG (TE_EXTN_ETEXT, "EXTN : TeAdminStatus : INTMD-EXIT \n");
            return TE_FAILURE;
    }
    TE_DBG (TE_EXTN_PRCS, "EXTN : Admin Status Sent/Received \n");
    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeAdminStatus : EXIT \n");
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeL2VpnEventHandler                                        
 * Description   : This function is made available to L2Vpn module to 
 *                 verify the operstatus of the tunnel in querry or to
 *                 set/reset the tunnel usage of L2Vpn module
 * Input(s)      : pMsg       - pointer to message
 *               : u4Event    - Event type    
 * Output(s)     : NONE 
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                 
 *****************************************************************************/
UINT1
TeL2VpnEventHandler (UINT1 *pMsg, UINT4 u4Event)
{
    tPwVcMplsTeTnlIndex *pPwVcMplsTeTnlIndex;

    UINT4               u4TnlIndex;
    UINT2               u2TnlInstance;
    UINT4               u4TnlIngressLSRId;
    UINT4               u4LspId = 0;
    UINT4               u4TnlEgressLSRId;
    UINT1               u1AssociateFlag;
    UINT1               u1Status = TE_FAILURE;

    pPwVcMplsTeTnlIndex = (tPwVcMplsTeTnlIndex *) (VOID *) pMsg;

    u4TnlIndex = pPwVcMplsTeTnlIndex->u4TnlIndex;
    u2TnlInstance = pPwVcMplsTeTnlIndex->u2TnlInstance;
    CONVERT_TO_INTEGER ((pPwVcMplsTeTnlIndex->TnlLclLSR), u4TnlIngressLSRId);
    CONVERT_TO_INTEGER ((pPwVcMplsTeTnlIndex->TnlPeerLSR), u4TnlEgressLSRId);
    if (u4Event == TE_L2VPN_TNL_STAT_REQ)
    {
        u1Status = TeChkTnlOperStatusByPrimaryInst (u4TnlIndex,
                                                    u2TnlInstance,
                                                    u4TnlIngressLSRId,
                                                    u4TnlEgressLSRId, &u4LspId);
        if (u4LspId != TE_ZERO)
        {
            ((tPwVcMplsTeTnlIndex *) (VOID *) pMsg)->u4LspId = u4LspId;
        }

    }
    else if (u4Event == TE_L2VPN_TNL_ASSOCIATE_REQ)
    {
        u1AssociateFlag = pPwVcMplsTeTnlIndex->u1AssociateFlag;

        u1Status = TeUpdateTnlInUsebyL2Vpn (u1AssociateFlag,
                                            u4TnlIndex,
                                            u2TnlInstance,
                                            u4TnlIngressLSRId,
                                            u4TnlEgressLSRId);
    }
    return u1Status;
}

/*****************************************************************************/
/* Function Name : TePostEvent                                        
 * Description   : This function is used by TE module to post TE events
 *                 to L2Vpn module                                    
 * Input(s)      : pTeTnlInfo  - pointer to tunnel info structure
 *                 u4BkpTnlMPXcIndex - Backup tunnel XC index
 *               : u4EvtType   - Event type    
 * Output(s)     : NONE 
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                 
 *****************************************************************************/
UINT4
TePostEvent (tTeTnlInfo * pTeTnlInfo, UINT4 u4BkpTnlMPXcIndex, UINT4 u4EvtType)
{
    tPwVcMplsTeTnlIndex PwVcMplsTeTnlIndex;
    tXcEntry           *pXcEntry = NULL;
    tInSegment         *pInSegment = NULL;
    tLblEntry          *pLblEntry = NULL;
    tTeTnlInfo         *pWorkingTnlInfo = NULL;
    UINT4               u4Status;
    UINT4               u4TnlIngressLSRId = 0;
    UINT4               u4TnlEgressLSRId = 0;
    UINT4               u4TnlLclLsr = TE_ZERO;
    UINT4               u4TnlPeerLsr = TE_ZERO;
    INT4                i4StorageType = 0;
    UINT1              *pTmpMsg = NULL;
    tTeTnlInfo         *pInstance0Tnl = NULL;

    CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTeTnlInfo)), u4TnlIngressLSRId);
    CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTeTnlInfo)), u4TnlEgressLSRId);

    u4TnlIngressLSRId = OSIX_NTOHL (u4TnlIngressLSRId);
    u4TnlEgressLSRId = OSIX_NTOHL (u4TnlEgressLSRId);

    CONVERT_TO_INTEGER (pTeTnlInfo->BkpTnlIngressLsrId, u4TnlLclLsr);
    CONVERT_TO_INTEGER (pTeTnlInfo->BkpTnlEgressLsrId, u4TnlPeerLsr);

    u4TnlLclLsr = OSIX_NTOHL (u4TnlLclLsr);
    u4TnlPeerLsr = OSIX_NTOHL (u4TnlPeerLsr);

    pWorkingTnlInfo = TeGetTunnelInfo (pTeTnlInfo->u4BkpTnlIndex,
                                       pTeTnlInfo->u4BkpTnlInstance,
                                       u4TnlLclLsr, u4TnlPeerLsr);

    /* OperDown indication for Protection tunnel is not sent to L2VPN
     * when WTR is in progress */
    if ((u4EvtType == TE_OPER_DOWN) &&
        (pTeTnlInfo->u1TnlPathType == TE_TNL_PROTECTION_PATH))
    {

        if ((pWorkingTnlInfo != NULL) &&
            (pWorkingTnlInfo->u1CPOrMgmtOperStatus == TE_OPER_UP) &&
            (pWorkingTnlInfo->u1TnlLocalProtectInUse == LOCAL_PROT_IN_USE))
        {
            TE_DBG5 (TE_EXTN_PRCS,
                     "EXTN : TE event posting to L2VPN not "
                     "required for backup tunnel when WTR is in progress, "
                     "TE TnlId=%d TnlInstance=%d TnlIngressId=%x "
                     "TnlEgressId=%x and Event=%d\n",
                     TE_TNL_TNL_INDEX (pTeTnlInfo),
                     TE_TNL_PRIMARY_TNL_INSTANCE (pTeTnlInfo),
                     u4TnlIngressLSRId, u4TnlEgressLSRId, u4EvtType);

            return TE_SUCCESS;
        }
    }

    pInstance0Tnl = TeGetTunnelInfo (TE_TNL_TNL_INDEX (pTeTnlInfo),
                                     TE_ZERO, u4TnlIngressLSRId,
                                     u4TnlEgressLSRId);

    if ((TE_TNL_PRIMARY_TNL_INSTANCE (pTeTnlInfo) >= TE_DETOUR_TNL_INSTANCE) ||
        ((pTeTnlInfo->u1TnlPathType == TE_TNL_PROTECTION_PATH) &&
         (TE_TNL_IN_USE_BY_VPN (pTeTnlInfo) & TE_TNL_INUSE_BY_L2VPN) ==
         TE_ZERO))
    {

        if ((pTeTnlInfo->u1TnlRole == TE_EGRESS) &&
            (pTeTnlInfo->pTeBackupPathInfo == NULL) &&
            (pWorkingTnlInfo != NULL) &&
            (pWorkingTnlInfo->u1TnlLocalProtectInUse == LOCAL_PROT_AVAIL))
        {
            /*Backup tunnel's status change should not be 
             * notified to the applications */
            TE_DBG5 (TE_EXTN_PRCS, "EXTN : TE event posting to L2VPN not "
                     "required for backup tunnels, "
                     "TE TnlId=%d TnlInstance=%d TnlIngressId=%x "
                     "TnlEgressId=%x and Event=%d\n",
                     TE_TNL_TNL_INDEX (pTeTnlInfo),
                     TE_TNL_PRIMARY_TNL_INSTANCE (pTeTnlInfo),
                     u4TnlIngressLSRId, u4TnlEgressLSRId, u4EvtType);

            return TE_SUCCESS;
        }
    }

    if ((u4EvtType == TE_OPER_UP) &&
        (pTeTnlInfo->u1TnlPathType == TE_TNL_PROTECTION_PATH) &&
        (pInstance0Tnl != NULL) && (pInstance0Tnl->u1WorkingUp == TRUE))
    {

        TE_DBG5 (TE_EXTN_PRCS, "EXTN : TE event posting to L2VPN not "
                 "required for ERO backup tunnels, if working is up, "
                 "TE TnlId=%d TnlInstance=%d TnlIngressId=%x "
                 "TnlEgressId=%x and Event=%d\n",
                 TE_TNL_TNL_INDEX (pTeTnlInfo),
                 TE_TNL_PRIMARY_TNL_INSTANCE (pTeTnlInfo),
                 u4TnlIngressLSRId, u4TnlEgressLSRId, u4EvtType);
        return TE_SUCCESS;

    }

    if (pTeTnlInfo->u1TnlRole == TE_INTERMEDIATE)
    {
        /* Posting to L2VPN module is not required for intermediate
         * tunnels. */
        TE_DBG5 (TE_EXTN_PRCS,
                 "EXTN: Posting to L2VPN module skipped for intermediate "
                 "tunnel %d %d %x %x Event: %d\n",
                 TE_TNL_TNL_INDEX (pTeTnlInfo),
                 TE_TNL_PRIMARY_TNL_INSTANCE (pTeTnlInfo),
                 u4TnlIngressLSRId, u4TnlEgressLSRId, u4EvtType);
        return TE_SUCCESS;
    }

    MEMSET (&PwVcMplsTeTnlIndex, 0, sizeof (tPwVcMplsTeTnlIndex));
    pTmpMsg = (UINT1 *) TE_ALLOC_MEM_BLOCK (TE_L2VPN_IF_POOL_ID);
    if (pTmpMsg == NULL)
    {
        TE_DBG (TE_EXTN_FAIL, "EXTN : Free L2Vpn If pool mem does not exist\n");
        return TE_FAILURE;
    }

    PwVcMplsTeTnlIndex.u4TnlIndex = TE_TNL_TNL_INDEX (pTeTnlInfo);
    PwVcMplsTeTnlIndex.u2TnlInstance =
        (UINT2) TE_TNL_PRIMARY_TNL_INSTANCE (pTeTnlInfo);

    PwVcMplsTeTnlIndex.u2BkpTnlInstance =
        (UINT2) TE_TNL_BKP_TNL_INSTANCE (pTeTnlInfo);

    PwVcMplsTeTnlIndex.u1TnlSwitchingType = pTeTnlInfo->u1TnlSwitchingType;

    MEMCPY ((UINT1 *) &PwVcMplsTeTnlIndex.TnlLclLSR,
            (UINT1 *) (TE_TNL_INGRESS_LSRID (pTeTnlInfo)), IPV4_ADDR_LENGTH);
    MEMCPY ((UINT1 *) &PwVcMplsTeTnlIndex.TnlPeerLSR,
            (UINT1 *) (TE_TNL_EGRESS_LSRID (pTeTnlInfo)), IPV4_ADDR_LENGTH);

    /* For corouted bidrectional tunnels role given to L2VPN is both
     * ingress and egress. Tunnel UP / DOWN indication should be handled by
     * L2VPN for both ingress and egress. */
    if (pTeTnlInfo->u4TnlMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
    {
        PwVcMplsTeTnlIndex.u1TnlRole = TE_INGRESS_EGRESS;
    }
    else
    {
        PwVcMplsTeTnlIndex.u1TnlRole = TE_TNL_ROLE (pTeTnlInfo);
    }

    if (TE_TNL_TYPE_P2MP & pTeTnlInfo->u4TnlType)
    {
        PwVcMplsTeTnlIndex.u4TnlIfIndex =
            pTeTnlInfo->pTeP2mpTnlInfo->u4P2mpLastAddOrRmvdBranchIf;
        MEMCPY (PwVcMplsTeTnlIndex.au1NextHop,
                pTeTnlInfo->pTeP2mpTnlInfo->au1P2mpLastAddNextHop,
                MAC_ADDR_LEN);
    }
    else
    {
        MEMCPY (PwVcMplsTeTnlIndex.au1NextHop,
                pTeTnlInfo->au1NextHopMac, MAC_ADDR_LEN);

        if ((TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_RSVP) &&
            (TE_TNL_ROLE (pTeTnlInfo) == TE_EGRESS))
        {
            PwVcMplsTeTnlIndex.u4TnlIfIndex = pTeTnlInfo->u4RevTnlIfIndex;
        }

        else
        {
            PwVcMplsTeTnlIndex.u4TnlIfIndex = TE_TNL_IFINDEX (pTeTnlInfo);
        }

        CfaGetIfMainStorageType (PwVcMplsTeTnlIndex.u4TnlIfIndex,
                                 &i4StorageType);

        if ((u4EvtType == TE_OPER_DOWN) &&
            (pTeTnlInfo->u1TnlPathType == TE_TNL_PROTECTION_PATH))
        {
            PwVcMplsTeTnlIndex.u1DeleteTnlAction =
                L2VPN_PSN_NO_DEL_L3FTN_MPLS_TNL_IF;
        }
        else if ((u4EvtType == TE_OPER_DOWN) &&
                 (i4StorageType == TE_STORAGE_VOLATILE))
        {
            /*Tunnel interface should not be deleted for OAM Operstatus 
               Change */
            PwVcMplsTeTnlIndex.u1DeleteTnlAction =
                L2VPN_PSN_DEL_L3FTN_MPLS_TNL_IF;
        }
        else if ((pTeTnlInfo->u1OamOperStatus == TE_OPER_DOWN) &&
                 (pTeTnlInfo->u1CPOrMgmtOperStatus == TE_OPER_UP))
        {
            /*When OperDown is received from OAM, tunnel interfaces
             * should not be deleted*/
            PwVcMplsTeTnlIndex.u1DeleteTnlAction =
                L2VPN_PSN_NO_DEL_L3FTN_MPLS_TNL_IF;
        }
        else if (u4EvtType == TE_OPER_DOWN)
        {
            PwVcMplsTeTnlIndex.u1DeleteTnlAction = L2VPN_PSN_DEL_L3FTN;
        }
        else if (u4EvtType == TE_FRR_TNL_PLR_DEL)
        {
            PwVcMplsTeTnlIndex.u1DeleteTnlAction = L2VPN_PSN_DEL_L3FTN;
        }
        else
        {
            PwVcMplsTeTnlIndex.u1DeleteTnlAction =
                L2VPN_PSN_NO_DEL_L3FTN_MPLS_TNL_IF;
        }
    }
    PwVcMplsTeTnlIndex.u4TnlXcIndex = pTeTnlInfo->u4TnlXcIndex;

    PwVcMplsTeTnlIndex.u4TnlBkpMPLabel1 = MPLS_INVALID_LABEL;
    PwVcMplsTeTnlIndex.u4TnlBkpMPLabel2 = MPLS_INVALID_LABEL;
    PwVcMplsTeTnlIndex.u4TnlBkpMPLabel3 = MPLS_INVALID_LABEL;

    TE_DBG5 (TE_EXTN_PRCS, "EXTN : TE event posting to applications, "
             "TE TnlId=%d TnlInstance=%d TnlIngressId=%x TnlEgressId=%x and Event=%d\n",
             PwVcMplsTeTnlIndex.u4TnlIndex, PwVcMplsTeTnlIndex.u2TnlInstance,
             u4TnlIngressLSRId, u4TnlEgressLSRId, u4EvtType);

    if (u4EvtType == TE_FRR_TNL_MP_ADD)
    {
        PwVcMplsTeTnlIndex.u4TnlXcIndex = u4BkpTnlMPXcIndex;
        MEMCPY (pTmpMsg, (UINT1 *) &PwVcMplsTeTnlIndex,
                sizeof (tPwVcMplsTeTnlIndex));

        u4Status = L2VpnTeEventHandler (pTmpMsg, u4EvtType);
        if (u4Status == L2VPN_FAILURE)
        {
            TE_DBG (TE_EXTN_FAIL,
                    "EXTN : Te event handling in L2VPN failed \n");
        }
    }
    else if (u4EvtType == TE_FRR_TNL_MP_DEL)
    {
        pXcEntry = MplsGetXCEntryByDirection (u4BkpTnlMPXcIndex,
                                              MPLS_DEF_DIRECTION);
        if ((pXcEntry == NULL) || (pXcEntry->pInIndex == NULL))
        {
            TE_DBG (TE_EXTN_FAIL, "EXTN : Backup tnl XC does not exist\n");
            TE_REL_MEM_BLOCK (TE_L2VPN_IF_POOL_ID, (UINT1 *) pTmpMsg);
            return TE_FAILURE;
        }
        pInSegment = pXcEntry->pInIndex;
        if (pInSegment->i4NPop == TE_ONE)
        {
            /* Fill the incoming tunnel label */
            PwVcMplsTeTnlIndex.u4TnlBkpMPLabel1 = pInSegment->u4Label;
        }
        else if (pInSegment->i4NPop == TE_TWO)
        {
            /* Fill the incoming tunnel labels... 
             * for example LDP over RSVP scenario */
            pLblEntry = (tLblEntry *)
                TMO_SLL_First (&(pInSegment->mplsLabelIndex->LblList));
            if (pLblEntry == NULL)
            {
                TE_REL_MEM_BLOCK (TE_L2VPN_IF_POOL_ID, (UINT1 *) pTmpMsg);
                return TE_FAILURE;
            }
            PwVcMplsTeTnlIndex.u4TnlBkpMPLabel1 = pLblEntry->u4Label;

            pLblEntry = (tLblEntry *)
                TMO_SLL_Last (&(pInSegment->mplsLabelIndex->LblList));
            if (pLblEntry == NULL)
            {
                TE_REL_MEM_BLOCK (TE_L2VPN_IF_POOL_ID, (UINT1 *) pTmpMsg);
                return TE_FAILURE;
            }
            PwVcMplsTeTnlIndex.u4TnlBkpMPLabel2 = pLblEntry->u4Label;
        }

        MEMCPY (pTmpMsg, (UINT1 *) &PwVcMplsTeTnlIndex,
                sizeof (tPwVcMplsTeTnlIndex));

        u4Status = L2VpnTeEventHandler (pTmpMsg, u4EvtType);
        if (u4Status == L2VPN_FAILURE)
        {
            TE_DBG (TE_EXTN_FAIL,
                    "EXTN : Te event handling in L2VPN failed \n");
        }
    }
    else
    {
        MEMCPY (pTmpMsg, (UINT1 *) &PwVcMplsTeTnlIndex,
                sizeof (tPwVcMplsTeTnlIndex));

        u4Status = L2VpnTeEventHandler (pTmpMsg, u4EvtType);
        if (u4Status == L2VPN_FAILURE)
        {
            TE_DBG (TE_EXTN_FAIL, "EXTN : Te event handling "
                    "in L2VPN failed \n");
        }
    }
    if ((TE_REL_MEM_BLOCK (TE_L2VPN_IF_POOL_ID,
                           (UINT1 *) pTmpMsg)) == TE_MEM_FAILURE)
    {
        TE_DBG (TE_EXTN_FAIL, "EXTN : MemRelease Memblock failed "
                "for L2Vpn If query \n");
    }

    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : MplsLspStatusReq
 * Description   : This function is made available to BGP module to query
 *                 MPLS for presence of LSP between given two nodes
 * Input(s)      : u4LspSrcAddr  - Address of Ingress end LSR
 *               : u4LspDestAddr - Address of Egress end LSR
 *               : u4PriLspIdentifier - Primary Tunnel identifier
 * Output(s)     : u4LspIdentifier - Tunnel identifier (Tunnel Index)
 *                 u1LspStatus     - Tunnel operational status
 * Return(s)     : TE_SUCCESS / TE_FAILURE
 *****************************************************************************/
UINT1
MplsLspStatusReq (UINT4 u4LspSrcAddr, UINT4 u4LspDestAddr,
                  UINT4 u4PriLspIdentifier, UINT4 *pu4LspIdentifier,
                  UINT1 *pu1LspStatus)
{
    UINT1               u1Status = TE_FAILURE;

    u1Status = TeGetTnlOperStatusBySrcAndDest (u4LspSrcAddr, u4LspDestAddr,
                                               u4PriLspIdentifier,
                                               pu4LspIdentifier, pu1LspStatus);

    return u1Status;
}

/*****************************************************************************/
/* Function Name : TeSigSetTnlAppDown
 * Description   : This function deletes the FTN informations 
 *                 associated with the tunnel
 * Input(s)      : pTeTnlInfo  - pointer to tunnel info structure
 * Output(s)     : NONE
 * Return(s)     : TE_SUCCESS / TE_FAILURE
 *****************************************************************************/
UINT1
TeSigSetTnlAppDown (tTeTnlInfo * pTeTnlInfo)
{
    tFtnEntry          *pFtnEntry = NULL;
    tTMO_DLL_NODE      *pFtnNodeInfo = NULL;
    UINT4               u4SrcAddr = TE_ZERO;
    UINT4               u4DestAddr = TE_ZERO;

    MPLS_CMN_LOCK ();
    CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTeTnlInfo)), u4SrcAddr);
    CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTeTnlInfo)), u4DestAddr);

    u4SrcAddr = TE_NTOHL (u4SrcAddr);
    u4DestAddr = TE_NTOHL (u4DestAddr);

    TePrcsL2vpnAssociation (pTeTnlInfo, TE_FRR_TNL_PLR_DEL);

    if (TE_LDP_OVER_RSVP_ENT_INDEX (pTeTnlInfo) != TE_ZERO)
    {
#ifdef MPLS_SIG_WANTED
        LdpTnlOperUpDnHdlForLdpOverRsvp
            (TE_TNL_TNL_INDEX (pTeTnlInfo),
             TE_TNL_TNL_INSTANCE (pTeTnlInfo),
             u4SrcAddr, u4DestAddr,
             TE_LDP_OVER_RSVP_ENT_INDEX (pTeTnlInfo),
             pTeTnlInfo->u4TnlXcIndex, TE_FRR_TNL_PLR_DEL,
             pTeTnlInfo->u1TnlRole);
#endif
    }
    /* Tunnel is operationally DOWN now, 
     * so delete all the FTN entries in h/w */

    if (TMO_DLL_Count (&TE_TNL_FTN_LIST (pTeTnlInfo)) == TE_ZERO)
    {
        pTeTnlInfo = TeGetTunnelInfo (TE_TNL_TNL_INDEX (pTeTnlInfo),
                                      TE_ZERO, u4SrcAddr, u4DestAddr);
        if (pTeTnlInfo == NULL)
        {
            MPLS_CMN_UNLOCK ();
            return TE_SUCCESS;
        }
    }

    TMO_DLL_Scan (&TE_TNL_FTN_LIST (pTeTnlInfo), pFtnNodeInfo, tTMO_DLL_NODE *)
    {
        pFtnEntry = (tFtnEntry *)
            (((FS_ULONG) pFtnNodeInfo - (TE_OFFSET (tFtnEntry, FtnInfoNode))));
        if (MplsFTNDel (pFtnEntry) == MPLS_FAILURE)
        {
            MPLS_CMN_UNLOCK ();
            return TE_FAILURE;
        }
    }
    MPLS_CMN_UNLOCK ();
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeSigHandleGblRevTnlUp
 * Description   : 
 * Input(s)      : 
 * Output(s)     : 
 * Return(s)     : TE_SUCCESS / TE_FAILURE
 *****************************************************************************/
UINT1
TeSigHandleGblRevTnlUp (tTeTnlInfo * pTeProtTnlInfo,
                        tTeTnlInfo * pTeSigGblRevTnlInfo)
{
    tFtnEntry          *pFtnEntry = NULL;
    tTMO_DLL_NODE      *pFtnNodeInfo = NULL;
    UINT4               u4SrcAddr = TE_ZERO;
    UINT4               u4DestAddr = TE_ZERO;

    MPLS_CMN_LOCK ();
    CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTeProtTnlInfo)), u4SrcAddr);
    CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTeProtTnlInfo)), u4DestAddr);
    u4SrcAddr = TE_NTOHL (u4SrcAddr);
    u4DestAddr = TE_NTOHL (u4DestAddr);

    TePrcsL2vpnAssociation (pTeProtTnlInfo, TE_OPER_DOWN);

    TePrcsL2vpnAssociation (pTeSigGblRevTnlInfo, TE_OPER_UP);
    if (TE_TNL_IN_USE_BY_VPN (pTeProtTnlInfo) & TE_TNL_INUSE_BY_L2VPN)
    {
        TE_TNL_IN_USE_BY_VPN (pTeSigGblRevTnlInfo) |= TE_TNL_INUSE_BY_L2VPN;
    }

    TE_DBG2 (TE_EXTN_ETEXT,
             "\n Rev behaviour prot inst=%d bkp inst=%d\n",
             TE_TNL_TNL_INSTANCE (pTeProtTnlInfo),
             TE_TNL_TNL_INSTANCE (pTeSigGblRevTnlInfo));

#ifdef MPLS_SIG_WANTED
    if (TE_LDP_OVER_RSVP_ENT_INDEX (pTeProtTnlInfo) != TE_ZERO)
    {
        LdpTnlOperUpDnHdlForLdpOverRsvp
            (TE_TNL_TNL_INDEX (pTeProtTnlInfo),
             TE_TNL_TNL_INSTANCE (pTeProtTnlInfo),
             TE_NTOHL (u4SrcAddr),
             TE_NTOHL (u4DestAddr),
             TE_LDP_OVER_RSVP_ENT_INDEX (pTeProtTnlInfo),
             pTeProtTnlInfo->u4TnlXcIndex, TE_OPER_DOWN,
             pTeProtTnlInfo->u1TnlRole);

        TE_LDP_OVER_RSVP_ENT_INDEX (pTeSigGblRevTnlInfo) =
            TE_LDP_OVER_RSVP_ENT_INDEX (pTeProtTnlInfo);
        CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTeSigGblRevTnlInfo)),
                            u4SrcAddr);
        CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTeSigGblRevTnlInfo)),
                            u4DestAddr);
        LdpTnlOperUpDnHdlForLdpOverRsvp
            (TE_TNL_TNL_INDEX (pTeSigGblRevTnlInfo),
             TE_TNL_TNL_INSTANCE (pTeSigGblRevTnlInfo),
             TE_NTOHL (u4SrcAddr),
             TE_NTOHL (u4DestAddr),
             TE_LDP_OVER_RSVP_ENT_INDEX (pTeSigGblRevTnlInfo),
             pTeSigGblRevTnlInfo->u4TnlXcIndex, TE_OPER_UP,
             pTeSigGblRevTnlInfo->u1TnlRole);
    }
#endif
    /* Tunnel is operationally DOWN now, 
     * so delete all the FTN entries in h/w */

    TMO_DLL_Scan (&TE_TNL_FTN_LIST (pTeProtTnlInfo), pFtnNodeInfo,
                  tTMO_DLL_NODE *)
    {
        pFtnEntry = (tFtnEntry *)
            (((FS_ULONG) pFtnNodeInfo - (TE_OFFSET (tFtnEntry, FtnInfoNode))));
        if (MplsFTNDel (pFtnEntry) == MPLS_FAILURE)
        {
            continue;
        }

        pFtnEntry->pActionPtr = pTeSigGblRevTnlInfo;
        if (MplsFTNAdd (pFtnEntry) == MPLS_FAILURE)
        {
            continue;
        }
    }
    MPLS_CMN_UNLOCK ();
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeSigNotifyFrrTnlMpHwIlmAddOrDel
 * Description   : Handles the ILM add or delete for the FRR merge point 
 * Input(s)      : pTeInProtTnlInfo - Protected tunnel info 
 *                 pTeInBkpTnlInfo - Backup tunnel info
 *                 bIsIlmAdd -> TRUE -> ILM add
 *                              FALSE -> ILM delete
 * Output(s)     : None
 * Return(s)     : TE_SUCCESS / TE_FAILURE
 *****************************************************************************/
UINT1
TeSigNotifyFrrTnlMpHwIlmAddOrDel (tTeTnlInfo * pTeInProtTnlInfo,
                                  tTeTnlInfo * pTeInBkpTnlInfo, BOOL1 bIsIlmAdd)
{
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;

    MPLS_CMN_LOCK ();
    /* Notify to the applications for proper MP ILM programming 
     * prior to the link/node failure */
    if (bIsIlmAdd)
    {
        /* Atleast we can a merge point with two FRR backup tnls 
         * This situation comes when protected desires to have node protection, 
         * but in the penultimate node if we can achieve only link protection 
         * then as per RFC 4090 we have to provide the link protection. 
         * So, we may have two backup paths for protected tunnel 
         * at a time for protected tunnel. */
        TE_DBG1 (TE_EXTN_ETEXT,
                 "XC Index of backup tunnel %d\n",
                 pTeInBkpTnlInfo->u4TnlXcIndex);

        if (pTeInProtTnlInfo->u4BkpTnlMPXcIndex1 == TE_ZERO)
        {
            pTeInProtTnlInfo->u4BkpTnlMPXcIndex1 =
                pTeInBkpTnlInfo->u4TnlXcIndex;
        }
        else
        {
            pTeInProtTnlInfo->u4BkpTnlMPXcIndex2 =
                pTeInBkpTnlInfo->u4TnlXcIndex;
        }

        TE_DBG2 (TE_EXTN_ETEXT,
                 "BkpTnlMPXcIndex1 = %d BkpTnlMPXcIndex2 = %d\n",
                 pTeInProtTnlInfo->u4BkpTnlMPXcIndex1,
                 pTeInProtTnlInfo->u4BkpTnlMPXcIndex2);

        /* Application 1: L2VPN */
        if (TE_TNL_IN_USE_BY_VPN (pTeInProtTnlInfo) & TE_TNL_INUSE_BY_L2VPN)
        {
            TePostEvent (pTeInProtTnlInfo, pTeInBkpTnlInfo->u4TnlXcIndex,
                         TE_FRR_TNL_MP_ADD);
        }

#ifdef MPLS_SIG_WANTED
        /* Application 2: LDP over RSVP */
        if (TE_LDP_OVER_RSVP_ENT_INDEX (pTeInProtTnlInfo) != TE_ZERO)
        {
            CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTeInProtTnlInfo)),
                                u4IngressId);
            CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTeInProtTnlInfo)),
                                u4EgressId);
            LdpTnlOperUpDnHdlForLdpOverRsvp
                (TE_TNL_TNL_INDEX (pTeInProtTnlInfo),
                 TE_TNL_TNL_INSTANCE (pTeInProtTnlInfo),
                 TE_NTOHL (u4IngressId),
                 TE_NTOHL (u4EgressId),
                 TE_LDP_OVER_RSVP_ENT_INDEX (pTeInProtTnlInfo),
                 pTeInBkpTnlInfo->u4TnlXcIndex, TE_FRR_TNL_MP_ADD,
                 pTeInBkpTnlInfo->u1TnlRole);
        }
#endif
    }
    else
    {
        /* Send the ILM MP delete notifications to Applications 
         * like L2VPN, LDP over RSVP etc.. */

        /* Application 1: L2VPN */
        if (TE_TNL_IN_USE_BY_VPN (pTeInProtTnlInfo) & TE_TNL_INUSE_BY_L2VPN)
        {
            TePostEvent (pTeInProtTnlInfo, pTeInBkpTnlInfo->u4TnlXcIndex,
                         TE_FRR_TNL_MP_DEL);
        }

#ifdef MPLS_SIG_WANTED
        /* Application 2: LDP over RSVP */
        if (TE_LDP_OVER_RSVP_ENT_INDEX (pTeInProtTnlInfo) != TE_ZERO)
        {
            CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTeInProtTnlInfo)),
                                u4IngressId);
            CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTeInProtTnlInfo)),
                                u4EgressId);
            LdpTnlOperUpDnHdlForLdpOverRsvp
                (TE_TNL_TNL_INDEX (pTeInProtTnlInfo),
                 TE_TNL_TNL_INSTANCE (pTeInProtTnlInfo),
                 TE_NTOHL (u4IngressId),
                 TE_NTOHL (u4EgressId),
                 TE_LDP_OVER_RSVP_ENT_INDEX (pTeInProtTnlInfo),
                 pTeInBkpTnlInfo->u4TnlXcIndex, TE_FRR_TNL_MP_DEL,
                 pTeInBkpTnlInfo->u1TnlRole);
        }
#endif
        if (pTeInProtTnlInfo->u4BkpTnlMPXcIndex1 ==
            pTeInBkpTnlInfo->u4TnlXcIndex)
        {
            pTeInProtTnlInfo->u4BkpTnlMPXcIndex1 = TE_ZERO;
        }
        else if (pTeInProtTnlInfo->u4BkpTnlMPXcIndex2 ==
                 pTeInBkpTnlInfo->u4TnlXcIndex)
        {
            pTeInProtTnlInfo->u4BkpTnlMPXcIndex2 = TE_ZERO;
        }
    }
    MPLS_CMN_UNLOCK ();
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeSigSetTnlAppUp
 * Description   : This function adds the FTN informations 
 *                 associated with the tunnel
 * Input(s)      : pTeTnlInfo  - pointer to tunnel info structure
 * Output(s)     : NONE
 * Return(s)     : TE_SUCCESS / TE_FAILURE
 *****************************************************************************/
UINT1
TeSigSetTnlAppUp (tTeTnlInfo * pTeTnlInfo)
{
    tFtnEntry          *pFtnEntry = NULL;
    tTMO_DLL_NODE      *pFtnNodeInfo = NULL;
    UINT4               u4SrcAddr = TE_ZERO;
    UINT4               u4DestAddr = TE_ZERO;

    MPLS_CMN_LOCK ();
    CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTeTnlInfo)), u4SrcAddr);
    CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTeTnlInfo)), u4DestAddr);
    u4SrcAddr = TE_NTOHL (u4SrcAddr);
    u4DestAddr = TE_NTOHL (u4DestAddr);

    TePrcsL2vpnAssociation (pTeTnlInfo, TE_FRR_TNL_PLR_ADD);

#ifdef MPLS_SIG_WANTED
    if (TE_LDP_OVER_RSVP_ENT_INDEX (pTeTnlInfo) != TE_ZERO)
    {
        LdpTnlOperUpDnHdlForLdpOverRsvp
            (TE_TNL_TNL_INDEX (pTeTnlInfo),
             TE_TNL_TNL_INSTANCE (pTeTnlInfo),
             TE_NTOHL (u4SrcAddr),
             TE_NTOHL (u4DestAddr),
             TE_LDP_OVER_RSVP_ENT_INDEX (pTeTnlInfo),
             TE_TNL_IFINDEX (pTeTnlInfo), TE_FRR_TNL_PLR_ADD,
             pTeTnlInfo->u1TnlRole);
    }
#endif

    if (TMO_DLL_Count (&TE_TNL_FTN_LIST (pTeTnlInfo)) == TE_ZERO)
    {
        pTeTnlInfo = TeGetTunnelInfo (TE_TNL_TNL_INDEX (pTeTnlInfo),
                                      TE_ZERO, u4SrcAddr, u4DestAddr);
        if (pTeTnlInfo == NULL)
        {
            MPLS_CMN_UNLOCK ();
            return TE_SUCCESS;
        }
    }

    TMO_DLL_Scan (&TE_TNL_FTN_LIST (pTeTnlInfo), pFtnNodeInfo, tTMO_DLL_NODE *)
    {
        pFtnEntry = (tFtnEntry *)
            (((FS_ULONG) pFtnNodeInfo - (TE_OFFSET (tFtnEntry, FtnInfoNode))));

        if (MplsFTNAdd (pFtnEntry) == MPLS_FAILURE)
        {
            MPLS_CMN_UNLOCK ();
            return TE_FAILURE;
        }
    }

    MPLS_CMN_UNLOCK ();
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeSigSetTnlOperStatus
 * Description   : This function is used by TE module to update Tunnels
 *                 Operational status and to register Tunnel Interface
 *                 with CFA module
 * Input(s)      : pTeTnlInfo  - pointer to tunnel info structure
 *               : u1TnlOperStatus - u1TnlOperStatus
 * Output(s)     : NONE
 * Return(s)     : TE_SUCCESS / TE_FAILURE
 *****************************************************************************/
UINT1
TeSigSetTnlOperStatus (tTeTnlInfo * pTeTnlInfo, UINT1 u1TnlOperStatus)
{
    UINT1               u1RetVal;
    MPLS_CMN_LOCK ();
    u1RetVal = TeCallUpdateTnlOperStatus (pTeTnlInfo, u1TnlOperStatus);

    if (u1RetVal == TE_SUCCESS)
    {
        /* Comment to be removed after CFA team updates for TnlAsIf */
/*
        TE_REG_MPLS_TNL_IF_WITH_CFA (
         *((UINT4 *)&TE_TNL_INGRESS_LSRID(pTeTnlInfo)),
         TE_TNL_IFINDEX(pTeTnlInfo));
*/
        MPLS_CMN_UNLOCK ();
        return u1RetVal;
    }
    MPLS_CMN_UNLOCK ();
    return u1RetVal;
}

/*****************************************************************************/
/* Function Name : TeCallUpdateTnlOperStatus
 * Description   : This function is used by TE module to update Tunnels
 *                 Operational status and to register Tunnel Interface
 *                 with CFA module
 * Input(s)      : pTeTnlInfo  - pointer to tunnel info structure
 *               : u1TnlOperStatus - u1TnlOperStatus
 * Output(s)     : NONE
 * Return(s)     : TE_SUCCESS / TE_FAILURE
 *****************************************************************************/
UINT1
TeCallUpdateTnlOperStatus (tTeTnlInfo * pTeTnlInfo, UINT1 u1TnlOperStatus)
{
    tTeUpdateInfo       TeUpdateInfo;
    UINT1               u1RetVal = TE_FAILURE;

    MEMSET (&TeUpdateInfo, 0, sizeof (tTeUpdateInfo));

    TeUpdateInfo.u1TnlOperStatus = u1TnlOperStatus;
    TeUpdateInfo.u1TeUpdateOpr = TE_CP_OR_MGMT_OPR_STATUS_RELATED;
    TeUpdateInfo.u1CPOrOamOperChgReqd = TRUE;

    u1RetVal = TeUpdateTnlStatus (pTeTnlInfo, &TeUpdateInfo);

    return u1RetVal;
}

/*****************************************************************************/
/* Function Name : MplsTeTunnelIsRsrcRoleIngress
 * Description   : This function is used by TE module to update Tunnels
 *                 Operational status and to register Tunnel Interface
 *                 with CFA module
 * Input(s)      : pTeTnlInfo  - pointer to tunnel info structure
 *               : u1TnlOperStatus - u1TnlOperStatus
 * Output(s)     : NONE
 * Return(s)     : TE_SUCCESS / TE_FAILURE
 *****************************************************************************/
UINT1
MplsTeTunnelIsRsrcRoleIngress (UINT4 u4TrfcParamIndex)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    UINT4               u4TrfcParamArrayIndex = 0;

    MPLS_CMN_LOCK ();
    for (u4TrfcParamArrayIndex = TE_ONE;
         u4TrfcParamArrayIndex <= TE_MAX_TRFC_PARAMS (gTeGblInfo);
         u4TrfcParamArrayIndex++)
    {
        if (TE_TRFC_PARAMS_TNL_RESINDEX (u4TrfcParamArrayIndex) ==
            u4TrfcParamIndex)
        {
            pTeTrfcParams = TE_TRFC_PARAMS_PTR (u4TrfcParamArrayIndex);
            if (pTeTrfcParams->u1TrfcParamRole == TE_INGRESS)
            {
                MPLS_CMN_UNLOCK ();
                return TE_SUCCESS;
            }
            break;
        }
    }
    MPLS_CMN_UNLOCK ();
    return TE_FAILURE;
}

/*****************************************************************************/
/* Function Name : MplsTeTunnelIsHopListRoleIngress
 * Description   : This function is used by TE module to update Tunnels
 *                 Operational status and to register Tunnel Interface
 *                 with CFA module
 * Input(s)      : pTeTnlInfo  - pointer to tunnel info structure
 *               : u1TnlOperStatus - u1TnlOperStatus
 * Output(s)     : NONE
 * Return(s)     : TE_SUCCESS / TE_FAILURE
 *****************************************************************************/
UINT1
MplsTeTunnelIsHopListRoleIngress (UINT4 u4MplsTunnelHopListIndex)
{
    MPLS_CMN_LOCK ();
    if (TE_PATH_HOP_ROLE (u4MplsTunnelHopListIndex) == TE_INGRESS)
    {
        MPLS_CMN_UNLOCK ();
        return TE_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return TE_FAILURE;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name : TeIsLsrPartOfErHop                                        
 * Description   : This routine is called to check whether the given Node      
 *                 is part of the Er-Hop or not.                               
 *                                                                             
 * Input(s)      : pTeErHopInfo - Pointer to tTeHopInfo structure.        
 *                 pu1IsEgrOfHop  - Pointer to IsEgrOfHop flag.                
 *                                                                             
 * Output(s)     : pu1IsEgrOfHop - Indicates whether the Lsr is Egress of the  
 *                 Er-Hop in context.                                          
 *                                                                             
 * Return(s)     : TE_FALSE when NOT part of given Er-Hop.                   
 *                 TE_TRUE when part of given Er-Hop.                        
 */
/*---------------------------------------------------------------------------*/
UINT1
TeIsLsrPartOfErHop (const tTeHopInfo * pTeErHopInfo, UINT1 *pu1IsEgrOfHop)
{
    UINT4               u4ErHopAddr = TE_ZERO;
    UINT4               u4ErHopMask = TE_ZERO;
    UINT4               u4HostMask = 0xffffffff;
    switch (TE_ERHOP_ADDR_TYPE (pTeErHopInfo))
    {
        case TE_ERHOP_IPV4_TYPE:
        case TE_ERHOP_UNNUM_TYPE:
            TE_GET_MASK_FROM_PRFX_LEN (pTeErHopInfo->u1AddrPrefixLen,
                                       u4ErHopMask);
            MEMCPY ((UINT1 *) (&(u4ErHopAddr)), &pTeErHopInfo->IpAddr,
                    IPV4_ADDR_LENGTH);

            u4ErHopAddr = OSIX_NTOHL (u4ErHopAddr);
            if (u4ErHopMask == u4HostMask)
            {
                if (NetIpv4IfIsOurAddress (u4ErHopAddr) == NETIPV4_SUCCESS)
                {
                    *pu1IsEgrOfHop = TE_TRUE;
                    return TE_SUCCESS;
                }
            }
            else
            {
                if (NetIpv4IpIsLocalNet (u4ErHopAddr) == NETIPV4_SUCCESS)
                {
                    *pu1IsEgrOfHop = TE_TRUE;
                    return TE_SUCCESS;
                }
            }
            /* For Numbered Bundle case */
            if ((*pu1IsEgrOfHop != TE_TRUE) &&
                (TePortTlmIsOurAddr (u4ErHopAddr) == TE_SUCCESS))
            {
                *pu1IsEgrOfHop = TE_TRUE;
                return TE_SUCCESS;
            }
            /* NOT Part of Er-Hop(Ipv4Addr Type) */
            break;

        default:
            break;
    }
    *pu1IsEgrOfHop = TE_FALSE;
    return TE_FAILURE;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name : TeIsLsrPartOfCHop                                        
 * Description   : This routine is called to check whether the given Node      
 *                 is part of the C-Hop or not.                               
 *                                                                             
 * Input(s)      : pTeCHopInfo - Pointer to tTeCHopInfo structure.        
 *                 pu1IsEgrOfHop  - Pointer to IsEgrOfHop flag.                
 *                                                                             
 * Output(s)     : pu1IsEgrOfHop - Indicates whether the Lsr is Egress of the  
 *                 C-Hop in context.                                                                           
 *                 TE_FALSE when NOT part of given Er-Hop.                   
 *                 TE_TRUE when part of given Er-Hop.
 *
 * Return(s)     : None
 */
/*---------------------------------------------------------------------------*/
VOID
TeIsLsrPartOfCHop (const tTeCHopInfo * pTeCHopInfo, UINT1 *pu1IsEgrOfHop)
{
    UINT4               u4CHopAddr = TE_ZERO;
    UINT4               u4CHopMask = TE_ZERO;
    UINT4               u4HostMask = 0xffffffff;

    switch (pTeCHopInfo->u1AddressType)
    {
        case TE_ERHOP_IPV4_TYPE:
        case TE_ERHOP_UNNUM_TYPE:
            TE_GET_MASK_FROM_PRFX_LEN (pTeCHopInfo->u1AddrPrefixLen,
                                       u4CHopMask);
            MEMCPY ((UINT1 *) &u4CHopAddr, &pTeCHopInfo->IpAddr,
                    IPV4_ADDR_LENGTH);
            u4CHopAddr = OSIX_NTOHL (u4CHopAddr);

            if (u4CHopMask == u4HostMask)
            {
                if (NetIpv4IfIsOurAddress (u4CHopAddr) == NETIPV4_SUCCESS)
                {
                    *pu1IsEgrOfHop = TE_TRUE;
                    return;
                }
            }
            else
            {
                if (NetIpv4IpIsLocalNet (u4CHopAddr) == NETIPV4_SUCCESS)
                {
                    *pu1IsEgrOfHop = TE_TRUE;
                    return;
                }
            }
            /* NOT Part of Er-Hop(Ipv4Addr Type) */
            break;

        default:
            break;
    }

    *pu1IsEgrOfHop = TE_FALSE;
    return;
}

/******************************************************************************
 Function Name  :  TeReEstOperDownTnlsFromRouteChg
 Input          :  pRouteInfo - Route information 
 Output         :  NONE
 Returns        :  NONE
****************************************************************************/
VOID
TeReEstOperDownTnlsFromRouteChg (tMplsRtEntryInfo * pRouteInfo)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlInfo         *pNextTeTnlInfo = NULL;
    UINT4               u4TnlDestNetAddr = 0;
    UINT4               u4DestAddr = 0;
    BOOL1               bIsCspfReqd = TE_FALSE;

    if (pRouteInfo->u1CmdType != NETIPV4_ADD_ROUTE)
    {
        return;
    }
    MPLS_CMN_LOCK ();
    u4TnlDestNetAddr =
        (MPLS_IPV4_U4_ADDR (pRouteInfo->DestAddr)) &
        (MPLS_IPV4_U4_ADDR (pRouteInfo->DestMask));

    pTeTnlInfo = RBTreeGetFirst (gTeGblInfo.TnlInfoTbl);

    while (pTeTnlInfo != NULL)
    {
        pNextTeTnlInfo = RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                        (tRBElem *) pTeTnlInfo, NULL);

        bIsCspfReqd = TeMainIsCspfComputationReqd (pTeTnlInfo);
        if ((TE_TNL_OPER_STATUS (pTeTnlInfo) != TE_OPER_DOWN) ||
            (TE_TNL_OWNER (pTeTnlInfo) != TE_TNL_OWNER_SNMP) ||
            (TE_TNL_ROW_STATUS (pTeTnlInfo) != TE_ACTIVE) ||
            (TE_TNL_ROLE (pTeTnlInfo) != TE_INGRESS) ||
            (bIsCspfReqd == TE_TRUE) ||
            (TE_TNL_SIG_STATE (pTeTnlInfo) != TE_SIG_NOT_DONE) ||
            (!((TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_RSVP) ||
               (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_LDP))))
        {
            pTeTnlInfo = pNextTeTnlInfo;
            continue;
        }

        if (pTeTnlInfo->u1TnlRelStatus == TE_SIGMOD_TNLREL_AWAITED)
        {
            /* Don't try to re-establish the already 
             * release awaited tunnel */
            pTeTnlInfo = pNextTeTnlInfo;
            continue;
        }
        u4DestAddr = TeSigGetTnlDest (pTeTnlInfo);

        if (u4TnlDestNetAddr !=
            ((u4DestAddr) & (MPLS_IPV4_U4_ADDR (pRouteInfo->DestMask))))
        {
            pTeTnlInfo = pNextTeTnlInfo;
            continue;
        }

        TeReestablishSigTunnel (pTeTnlInfo);

        pTeTnlInfo = pNextTeTnlInfo;
    }
    MPLS_CMN_UNLOCK ();
}

/******************************************************************************
 Function Name  :  TeReestablishOperDownTnls
 Input          :  pTeTnlInfo - Pointer to Tunnel Info
                   u4IfIndex  - L3 interface,
                   u1SigProto - Signalling protocols (RSVP, LDP)
 Output         :  NONE
 Returns        :  NONE
****************************************************************************/
VOID
TeReestablishOperDownTnls (tTeTnlInfo * pTeTnlInfo,
                           UINT4 u4IfIndex, UINT1 u1SigProto)
{
    UINT4               u4L3IfIndex = 0;
    UINT4               u4DestAddr = 0;
    UINT4               u4TnlHopTableIndex = TE_ZERO;
    BOOL1               bIsCspfReqd = TE_FALSE;

    bIsCspfReqd = TeMainIsCspfComputationReqd (pTeTnlInfo);

    if ((TE_TNL_OPER_STATUS (pTeTnlInfo) == TE_OPER_UP) ||
        (TE_TNL_ROLE (pTeTnlInfo) != TE_INGRESS) ||
        (TE_TNL_SIG_STATE (pTeTnlInfo) != TE_SIG_NOT_DONE) ||
        (bIsCspfReqd == TE_TRUE))
    {
        return;
    }

    if ((u1SigProto != 0) && (TE_TNL_SIGPROTO (pTeTnlInfo) != u1SigProto))
    {
        return;
    }

    if (pTeTnlInfo->u1TnlRelStatus == TE_SIGMOD_TNLREL_AWAITED)
    {
        /* Don't try to re-establish the already release awaited tunnel */
        return;
    }

    u4TnlHopTableIndex = TE_TNL_HOP_TABLE_INDEX (pTeTnlInfo);
    if (((TE_TNL_SSN_ATTR (pTeTnlInfo) & TE_SSN_FAST_REROUTE_BIT)
         == TE_SSN_FAST_REROUTE_BIT) && (u4TnlHopTableIndex == TE_ZERO))
    {
        return;
    }

    if ((TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_LDP) &&
        (u4TnlHopTableIndex == TE_ZERO))
    {
        return;
    }

    u4DestAddr = TeSigGetTnlDest (pTeTnlInfo);

    u4L3IfIndex = TeSigGetL3IntfFromDestAddr (u4DestAddr);

    if (u4IfIndex != u4L3IfIndex)
    {
        return;
    }

    TeReestablishSigTunnel (pTeTnlInfo);

    return;
}

/******************************************************************************
 Function Name  :  TeReestablishSigTunnel
 Input          :  pTeTnlInfo - Pointer to the tunnel for the re-establishment
 Output         :  NONE
 Returns        :  NONE
****************************************************************************/
VOID
TeReestablishSigTunnel (tTeTnlInfo * pTeTnlInfo)
{
    tTePathInfo        *pTePathInfo = NULL;
    tTePathListInfo    *pTePathListInfo = NULL;
    tTeTnlInfo         *pInstance0Tnl = NULL;
    tRpteEnqueueMsgInfo RpteEnqueueMsgInfo;
    UINT4               u4TnlHopTableIndex = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    BOOL1               bIsPathChanged = TE_FALSE;
    UINT1               u1IsRpteAdminDown = TE_FALSE;

    CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTeTnlInfo)), u4IngressId);
    CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTeTnlInfo)), u4EgressId);
    u4IngressId = TE_NTOHL (u4IngressId);
    u4EgressId = TE_NTOHL (u4EgressId);

    MEMSET (&RpteEnqueueMsgInfo, 0, sizeof (tRpteEnqueueMsgInfo));

    /* Stop the Tunnel Retrigger Timer if it is running. */
    MplsStopRequestedTimers (MPLS_RPTE_TNL_RETRIGGER_EVENT,
                             &(pTeTnlInfo->TnlRetrigTmr));

    if (TE_TNL_ROW_STATUS (pTeTnlInfo) != TE_ACTIVE)
    {
        return;
    }

    if (TE_TNL_PATH_INFO (pTeTnlInfo) != NULL)
    {
        /* Construct the fresh path info. from hop list, 
         * here, there is a possiblity of change in HOP table */
        TE_PO_NUM_TUNNELS (TE_TNL_PATH_INFO (pTeTnlInfo)) = 0;
    }
    if (TE_TNL_TRFC_PARAM (pTeTnlInfo) != NULL)
    {
        /* Construct the fresh TE info. from resource list, 
         * here, there is a possiblity of change in Resource table */
        TE_TNLRSRC_NUM_OF_TUNNELS ((TE_TNL_TRFC_PARAM (pTeTnlInfo))) = 0;
    }
    u4TnlHopTableIndex = TE_TNL_HOP_TABLE_INDEX (pTeTnlInfo);
    if (u4TnlHopTableIndex != TE_ZERO)
    {
        pTePathListInfo = TE_PATH_LIST_ENTRY (u4TnlHopTableIndex);

        if (pTePathListInfo != NULL)
        {
            TE_SLL_SCAN (TE_HOP_PO_LIST (pTePathListInfo),
                         pTePathInfo, tTePathInfo *)
            {
                if (pTePathInfo->u4PathOptionIndex ==
                    TE_TNL_PATH_IN_USE (pTeTnlInfo))
                {
                    TE_TNL_PATH_INFO (pTeTnlInfo) = pTePathInfo;
                    pTeTnlInfo->pTePathListInfo = pTePathListInfo;
                    break;
                }
            }

            if (pTePathInfo == NULL)
            {
                return;
            }
            /* Check whether all the Er-Hops are Active */
            if (TeIsAllErHopsActive (TE_TNL_PATH_INFO (pTeTnlInfo),
                                     &bIsPathChanged) == TE_TRUE)
            {
                /* Incrementing the number of tunnels associated 
                 * with  the Er-hop List.  */
                TE_PO_NUM_TUNNELS (TE_TNL_PATH_INFO (pTeTnlInfo))++;
            }
            else
            {
                return;
            }
        }
    }
    /*Checking whether resource index is 0 or 1 */
    if ((TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_LDP) &&
        ((TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo) == TE_ZERO) ||
         (TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo) == TE_ONE)))
    {
        return;
    }

    /* Finding the Resource table with the Index and 
     * assigning the Corresponding Traffic params to 
     * the Tunnel structure */

    if ((TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo)) != TE_ZERO)
    {
        if ((TE_TRFC_PARAMS_TNL_RESINDEX
             ((TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo))) ==
             (TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo))) &&
            (TE_TRFC_PARAMS_TNL_RESROWSTATUS
             ((TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo))) == TE_ACTIVE))
        {
            if ((TE_TNL_SIGPROTO (pTeTnlInfo)
                 == TE_SIGPROTO_RSVP)
                && (TE_RSVPTE_TRFC_PARAMS (TE_TRFC_PARAMS_PTR
                                           (TE_TNL_TRFC_PARAM_INDEX
                                            (pTeTnlInfo))) != NULL))
            {
                if (TE_TRFC_PARAMS_PTR
                    (TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo)) != NULL)
                {
                    TE_TNL_TRFC_PARAM (pTeTnlInfo) =
                        TE_TRFC_PARAMS_PTR ((TE_TNL_TRFC_PARAM_INDEX
                                             (pTeTnlInfo)));
                    TE_TNLRSRC_NUM_OF_TUNNELS ((TE_TNL_TRFC_PARAM
                                                (pTeTnlInfo)))++;
                }
                else if (TE_TNL_PATH_INFO (pTeTnlInfo) != NULL)
                {
                    TE_PO_NUM_TUNNELS (TE_TNL_PATH_INFO (pTeTnlInfo))--;
                    return;
                }
            }
            else if ((TE_TNL_SIGPROTO (pTeTnlInfo)
                      == TE_SIGPROTO_LDP) &&
                     (TE_CRLDP_TRFC_PARAMS
                      (TE_TRFC_PARAMS_PTR
                       (TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo))) != NULL))
            {
                if (TE_CRLDP_TPARAM_ROW_STATUS
                    (TE_TRFC_PARAMS_PTR
                     (TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo))) == TE_ACTIVE)
                {
                    TE_TNL_TRFC_PARAM (pTeTnlInfo) =
                        TE_TRFC_PARAMS_PTR ((TE_TNL_TRFC_PARAM_INDEX
                                             (pTeTnlInfo)));
                    TE_TNLRSRC_NUM_OF_TUNNELS ((TE_TNL_TRFC_PARAM
                                                (pTeTnlInfo)))++;
                }
                else if (TE_TNL_PATH_INFO (pTeTnlInfo) != NULL)
                {
                    TE_PO_NUM_TUNNELS (TE_TNL_PATH_INFO (pTeTnlInfo))--;
                    return;
                }
            }
            else
            {
                /* Tunnel Should support Static Resouce Also */
                TE_TNL_TRFC_PARAM (pTeTnlInfo) =
                    TE_TRFC_PARAMS_PTR ((TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo)));
                TE_TNLRSRC_NUM_OF_TUNNELS ((TE_TNL_TRFC_PARAM
                                            (pTeTnlInfo))) = 0;
            }
        }
        else
        {
            if (TE_TNL_PATH_INFO (pTeTnlInfo) != NULL)
            {
                TE_PO_NUM_TUNNELS (TE_TNL_PATH_INFO (pTeTnlInfo))--;
            }
            return;
        }
    }
    else
    {
        TE_TNL_TRFC_PARAM (pTeTnlInfo) =
            TE_TRFC_PARAMS_PTR (TE_DFLT_TRFC_PRAM_INDEX);
        TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo) = TE_DFLT_TRFC_PRAM_INDEX;
        TE_TNLRSRC_NUM_OF_TUNNELS ((TE_TNL_TRFC_PARAM (pTeTnlInfo)))++;
    }

    if (pTeTnlInfo->u4TnlInstance != TE_ZERO)
    {
        TE_TNL_PRIMARY_TNL_INSTANCE (pTeTnlInfo) =
            (TE_TNL_PRIMARY_TNL_INSTANCE (pTeTnlInfo) + 1) % TE_TNLINST_MAXVAL;
    }

    TE_DBG5 (TE_EXTN_PRCS,
             "EXTN: Signalled instance for the tunnel %d %d %x %x incremented"
             " - New Value : %d \n",
             TE_TNL_TNL_INDEX (pTeTnlInfo), TE_TNL_TNL_INSTANCE (pTeTnlInfo),
             u4IngressId, u4EgressId, TE_TNL_PRIMARY_TNL_INSTANCE (pTeTnlInfo));

    TePrcsL2vpnAssociation (pTeTnlInfo, TE_TNL_REESTB);

    TE_DBG4 (TE_EXTN_PRCS,
             "EXTN: Signalled instance increment notified to L2VPN for "
             "Tunnel %d %d %x %x\n",
             TE_TNL_TNL_INDEX (pTeTnlInfo),
             TE_TNL_TNL_INSTANCE (pTeTnlInfo), u4IngressId, u4EgressId);

    /* call functions from CRLDP or RSVPTE  */
    if (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_LDP)
    {
        /* CRLDP Function Call */
        if (TeLdpTEEventHandler (TE_TNL_UP, pTeTnlInfo) == TE_LDP_FAILURE
            && (TE_TNL_PATH_INFO (pTeTnlInfo) != NULL))
        {
            TE_PO_NUM_TUNNELS (TE_TNL_PATH_INFO (pTeTnlInfo))--;
            TE_TNLRSRC_NUM_OF_TUNNELS ((TE_TNL_TRFC_PARAM (pTeTnlInfo)))--;
            return;
        }
    }
    else if (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_RSVP)
    {
        TE_TNL_FRR_FAC_TNL_STATUS (pTeTnlInfo) = TE_TNL_FRR_PROT_STATUS_PARTIAL;
        TE_TNL_FRR_DETOUR_ACTIVE (pTeTnlInfo) = TE_FALSE;
        TE_TNL_FRR_DETOUR_MERGING (pTeTnlInfo) = TE_TNL_FRR_DETOUR_MERGE_NONE;

        if (((TE_TNL_SSN_ATTR (pTeTnlInfo) & TE_SSN_FAST_REROUTE_BIT)
             == TE_SSN_FAST_REROUTE_BIT))
        {
            if (TE_TNL_FRR_CONST_INFO (pTeTnlInfo) != NULL)
            {
                TE_TNL_FRR_PROT_METHOD (pTeTnlInfo) =
                    TE_TNL_FRR_CONST_PROT_METHOD (TE_TNL_FRR_CONST_INFO
                                                  (pTeTnlInfo));
            }
            else
            {
                TE_TNL_FRR_PROT_METHOD (pTeTnlInfo) = TE_TNL_FRR_ONE2ONE_METHOD;
            }
        }

        CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTeTnlInfo)), u4IngressId);
        CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTeTnlInfo)), u4EgressId);
        u4IngressId = TE_NTOHL (u4IngressId);
        u4EgressId = TE_NTOHL (u4EgressId);

        pInstance0Tnl = TeGetTunnelInfo (TE_TNL_TNL_INDEX (pTeTnlInfo),
                                         TE_ZERO, u4IngressId, u4EgressId);
        if (pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
            MPLS_TE_DEDICATED_ONE2ONE)
        {
            if ((pTeTnlInfo->pTeBackupPathInfo != NULL) &&
                (pInstance0Tnl != NULL) &&
                (pInstance0Tnl->u1WorkingUp == FALSE))
            {
                if (TeRpteTEEventHandler
                    (TE_TNL_UP, pTeTnlInfo, &u1IsRpteAdminDown) == RPTE_FAILURE)
                {
                    /* Failure will happen if RSVP-TE is not enabled globally,
                     * So, retrigger after some time. */
                    MplsStartRequestedTimers (MPLS_RPTE_TNL_RETRIGGER_EVENT,
                                              &(pTeTnlInfo->TnlRetrigTmr));

                    return;
                }
            }
            else if (pTeTnlInfo->pTeBackupPathInfo == NULL)
            {
                if (TeRpteTEEventHandler
                    (TE_TNL_UP, pTeTnlInfo, &u1IsRpteAdminDown) == RPTE_FAILURE)
                {
                    /* Failure will happen if RSVP-TE is not enabled globally,
                     * So, retrigger after some time. */
                    MplsStartRequestedTimers (MPLS_RPTE_TNL_RETRIGGER_EVENT,
                                              &(pTeTnlInfo->TnlRetrigTmr));

                    return;
                }
            }
        }
        else
        {
            if (TeRpteTEEventHandler (TE_TNL_UP, pTeTnlInfo, &u1IsRpteAdminDown)
                == RPTE_FAILURE)
            {
                /* Failure will happen if RSVP-TE is not enabled globally,
                 * So, retrigger after some time. */
                MplsStartRequestedTimers (MPLS_RPTE_TNL_RETRIGGER_EVENT,
                                          &(pTeTnlInfo->TnlRetrigTmr));

                return;
            }
        }

        if ((pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
             MPLS_TE_DEDICATED_ONE2ONE) &&
            (pTeTnlInfo->pTeBackupPathInfo != NULL) &&
            (pInstance0Tnl != NULL) && (pInstance0Tnl->u1BackupUp == FALSE))
        {
            TE_DBG2 (TE_MAIN_SNMP, "Triggering backup-path for "
                     "tnl: %u %u\n", pTeTnlInfo->u4TnlIndex,
                     pTeTnlInfo->u4TnlInstance);
            if (TeRpteTEEventHandler (TE_TNL_UP,
                                      pTeTnlInfo, &u1IsRpteAdminDown)
                == TE_RPTE_FAILURE)
            {
                /* Failure will happen if RSVP-TE is not enabled globally,
                 * So, retrigger after some time. */
                MplsStartRequestedTimers (MPLS_RPTE_TNL_RETRIGGER_EVENT,
                                          &(pTeTnlInfo->TnlRetrigTmr));
                return;
            }
        }

        TE_TNL_SIG_STATE (pTeTnlInfo) = TE_SIG_AWAITED;
    }

    return;
}

/*****************************************************************************/
/* Function Name : MplsHandleTnlArpResolveTmrEvent                           */
/* Description   : This function handles the Arp Resolution Timer Expiry for */
/*                 Tunnel.                                                   */
/* Input(s)      : pArg - Pointer to expired timer (Unused Now)              */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
MplsHandleTnlArpResolveTmrEvent (VOID *pArg)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlInfo         *pNextTeTnlInfo = NULL;
    tOutSegment        *pOutSegment = NULL;
    tXcEntry           *pXcEntry = NULL;
    tP2mpBranchEntry   *pTeP2mpBranchEntry = NULL;

    UINT4               u4OutSegIndex = TE_ZERO;
    eDirection          Direction = MPLS_DIRECTION_FORWARD;
    BOOL1               bLlStatus = XC_OPER_UP;

    UNUSED_PARAM (pArg);

    /* Since, this function is called FM Module, CMN Lock is taken here. */
    MPLS_CMN_LOCK ();

    pTeTnlInfo = RBTreeGetFirst (gTeGblInfo.TnlInfoTbl);

    while (pTeTnlInfo != NULL)
    {
        pNextTeTnlInfo = RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                        (tRBElem *) pTeTnlInfo, NULL);

        if (TE_TNL_ROW_STATUS (pTeTnlInfo) != TE_ACTIVE)
        {
            pTeTnlInfo = pNextTeTnlInfo;
            continue;
        }

        if (pTeTnlInfo->u4TnlInstance == TE_ZERO)
        {
            pTeTnlInfo = pNextTeTnlInfo;
            continue;
        }

        /* For P2MP tunnels, irrespective of the tunnel operational status,
         * hardware addition should be invoked for remaining out-segments whose
         * ARP has not been resolved */
        if ((pTeTnlInfo->u1CPOrMgmtOperStatus != TE_OPER_DOWN)
            && (!(TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_P2MP)))
        {
            pTeTnlInfo = pNextTeTnlInfo;
            continue;
        }

        /* Tunnel Hardware Add should not consider intermediate tunnels */
        if (pTeTnlInfo->u1TnlRole == TE_INTERMEDIATE)
        {
            pTeTnlInfo = pNextTeTnlInfo;
            continue;
        }

        /* P2MP tunnel is uni-directional. Tunnel hardware addition is 
         * applicable only at the ingress LSR */
        if ((TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_P2MP)
            && (TE_TNL_ROLE (pTeTnlInfo) == TE_INGRESS)
            && (TE_P2MP_TNL_INFO (pTeTnlInfo) != NULL))
        {
            if (TE_P2MP_TNL_INFO (pTeTnlInfo) == NULL)
            {
                continue;
            }

            TMO_SLL_Scan (&(TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo)),
                          pTeP2mpBranchEntry, tP2mpBranchEntry *)
            {
                u4OutSegIndex =
                    TE_P2MP_BRANCH_OUTSEG_INDEX (pTeP2mpBranchEntry);
                pOutSegment = MplsGetOutSegmentTableEntry (u4OutSegIndex);
                if (NULL == pOutSegment)
                {
                    continue;
                }

                pXcEntry = (tXcEntry *) OUTSEGMENT_XC_INDEX (pOutSegment);
                if (NULL == pXcEntry)
                {
                    continue;
                }

                if (XC_ARP_STATUS (pXcEntry) != MPLS_ARP_RESOLVE_WAITING)
                {
                    continue;
                }

                /* Configure P2MP Tunnel in HW */
                if (MplsDbUpdateP2mpTunnelInHw
                    (pTeTnlInfo, pXcEntry, TE_OPER_UP) == MPLS_FAILURE)
                {
                    continue;
                }
            }

            /* After processing the ARP status of all out-segments of the 
             * current P2MP tunnel, scan the next tunnel */
            pTeTnlInfo = pNextTeTnlInfo;
            continue;
        }

        if (((pTeTnlInfo->u1TnlRole == TE_INGRESS) &&
             (pTeTnlInfo->u1FwdArpResolveStatus != MPLS_ARP_RESOLVE_WAITING)) ||
            ((pTeTnlInfo->u1TnlRole == TE_EGRESS) &&
             (pTeTnlInfo->u1RevArpResolveStatus != MPLS_ARP_RESOLVE_WAITING)))
        {
            pTeTnlInfo = pNextTeTnlInfo;
            continue;
        }

        if (pTeTnlInfo->u1TnlRole == TE_INGRESS)
        {
            Direction = MPLS_DIRECTION_FORWARD;
        }
        else
        {
            Direction = MPLS_DIRECTION_REVERSE;
        }

        if (MplsDbUpdateTunnelInHw (MPLS_MLIB_TNL_CREATE, pTeTnlInfo,
                                    Direction, &bLlStatus) == MPLS_FAILURE)
        {
            pTeTnlInfo = pNextTeTnlInfo;
            continue;
        }
        if ((((pTeTnlInfo->u1TnlRole == TE_INGRESS) &&
              (pTeTnlInfo->u1FwdArpResolveStatus == MPLS_ARP_RESOLVED)) ||
             ((pTeTnlInfo->u1TnlRole == TE_EGRESS) &&
              (pTeTnlInfo->u1RevArpResolveStatus == MPLS_ARP_RESOLVED))) &&
            (bLlStatus == XC_OPER_UP))
        {
            if (TeCallUpdateTnlOperStatus (pTeTnlInfo,
                                           TE_OPER_UP) != TE_SUCCESS)
            {
                pTeTnlInfo = pNextTeTnlInfo;
                continue;
            }
        }
        pTeTnlInfo = pNextTeTnlInfo;
    }

    MPLS_CMN_UNLOCK ();
    return;
}

#ifdef NPAPI_WANTED
/*****************************************************************************/
/* Function Name      : TeGetTunnelEntryFromLabel                            */
/*                                                                           */
/* Description        : This function checks whether the Tunnel Hw entry is  */
/*                      present in Tunnel Sw entry                           */
/*                                                                           */
/* Input(s)           : Label of the particular FTN Entry                    */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None.                                                */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None.                                                */
/*                                                                           */
/* Return Value(s)    : Pointer of the Tunnel Entry or NULL                  */
/*****************************************************************************/

tTeTnlInfo         *
TeGetTunnelEntryFromLabel (UINT4 MplsShimLabel)
{
    tXcEntry           *pXcEntry = NULL;
    tOutSegment        *pOutSegment = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlInfo         *pNextTeTnlInfo = NULL;
    eDirection          Direction = MPLS_DIRECTION_ANY;

    pTeTnlInfo = RBTreeGetFirst (gTeGblInfo.TnlInfoTbl);

    while (pTeTnlInfo != NULL)
    {
        pNextTeTnlInfo = RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                        (tRBElem *) pTeTnlInfo, NULL);

        if (TE_TNL_ROW_STATUS (pTeTnlInfo) != TE_ACTIVE)
        {
            pTeTnlInfo = pNextTeTnlInfo;
            continue;
        }

        if (pTeTnlInfo->u4TnlInstance == TE_ZERO)
        {
            pTeTnlInfo = pNextTeTnlInfo;
            continue;
        }

        if (pTeTnlInfo->u1TnlRole == TE_INTERMEDIATE)
        {
            pTeTnlInfo = pNextTeTnlInfo;
            continue;
        }

        if (pTeTnlInfo->u1TnlRole == TE_INGRESS)
        {
            Direction = MPLS_DIRECTION_FORWARD;
        }
        else
        {
            Direction = MPLS_DIRECTION_REVERSE;
        }

        pXcEntry = MplsGetXCEntryByDirection (pTeTnlInfo->u4TnlXcIndex,
                                              Direction);
        if ((pXcEntry == NULL) || (pXcEntry->pOutIndex == NULL))
        {
            pTeTnlInfo = pNextTeTnlInfo;
            continue;
        }

        /* TODO For multiple label stacking case, labels should be
         * fetched appropriately and validation should be based on 
         * that. */
        pOutSegment = pXcEntry->pOutIndex;
        if (pOutSegment->u4Label == MplsShimLabel)
        {
            return pTeTnlInfo;
        }

        pTeTnlInfo = pNextTeTnlInfo;
    }

    return NULL;
}
#endif

/*****************************************************************************/
/* Function Name : TeTnlRetriggerTmrEvent                                    */
/* Description   : This function starts the triggering of the tunnel.        */
/* Input(s)      : pArg - Pointer to the tunnel information for which the    */
/*                        Retriggering timer has expired.                    */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
TeTnlRetriggerTmrEvent (VOID *pArg)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlInfo         *pTeTmpTnlInfo = NULL;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;

    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeTnlRetriggerTmrEvent : ENTRY\n");

    pTeTnlInfo = (tTeTnlInfo *) pArg;
    CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTeTnlInfo)), u4IngressId);
    CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTeTnlInfo)), u4EgressId);
    u4IngressId = TE_NTOHL (u4IngressId);
    u4EgressId = TE_NTOHL (u4EgressId);

    /* CMN Lock is taken here. */
    MPLS_CMN_LOCK ();

    pTeTmpTnlInfo = TeGetTunnelInfo (TE_TNL_TNL_INDEX (pTeTnlInfo),
                                     TE_TNL_TNL_INSTANCE (pTeTnlInfo),
                                     u4IngressId, u4EgressId);

    if (pTeTmpTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();

        TE_DBG4 (TE_EXTN_FAIL, "EXTN: Tunnel %d %d %x %x not found\n",
                 TE_TNL_TNL_INDEX (pTeTnlInfo),
                 TE_TNL_TNL_INSTANCE (pTeTnlInfo), u4IngressId, u4EgressId);
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeTnlRetriggerTmrEvent : INTMD-EXIT\n");
        return;
    }

    if (pTeTmpTnlInfo->u1TnlAdminStatus == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        TE_DBG4 (TE_EXTN_FAIL, "EXTN: Tunnel %d %d %x %x Admin down\n",
                 TE_TNL_TNL_INDEX (pTeTnlInfo),
                 TE_TNL_TNL_INSTANCE (pTeTnlInfo), u4IngressId, u4EgressId);
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeTnlRetriggerTmrEvent : INTMD-EXIT\n");
        return;
    }

    TeReestablishSigTunnel (pTeTnlInfo);

    TE_DBG4 (TE_EXTN_PRCS,
             "EXTN: Tunnel %d %d %x %x reestablishment Initiated\n",
             TE_TNL_TNL_INDEX (pTeTnlInfo), TE_TNL_TNL_INSTANCE (pTeTnlInfo),
             u4IngressId, u4EgressId);

    MPLS_CMN_UNLOCK ();

    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeTnlRetriggerTmrEvent : EXIT\n");
    return;
}

/*****************************************************************************/
/* Function Name : TeIsTnlOperStatusUp                                       */
/* Description   : This function is used to check for the tunnel             */
/*                 operation status                                          */
/* Input(s)      : u4TnlId - Tunnel index                                    */
/*                 u4TnlInst - Tunnel instance                               */
/*                 u4TnlIngress - Tunnel ingress address                     */
/*                 u4TnlEgress - Tunnel egress address                       */
/* Output(s)     : None                                                      */
/* Return(s)     : TRUE/FALSE                                                */
/*****************************************************************************/
BOOL1
TeIsTnlOperStatusUp (UINT4 u4TnlId, UINT4 u4TnlInst, UINT4 u4TnlIngress,
                     UINT4 u4TnlEgress)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    pTeTnlInfo =
        TeGetTunnelInfo (u4TnlId, u4TnlInst, u4TnlIngress, u4TnlEgress);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return FALSE;
    }

    if (TE_TNL_OPER_STATUS (pTeTnlInfo) == TE_OPER_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return FALSE;
    }

    MPLS_CMN_UNLOCK ();
    return TRUE;
}

/*****************************************************************************/
/* Function Name : TeDeleteTnlFrrInfo                                        */
/* Description   : This function is used to delete the Frr Constraint info   */
/*            associated with the Tunnel. It releases the memory        */
/*                 allocated for Frr constraint information.                 */
/* Input(s)      : pTeTnlInfo - Pointer to Tunnel Information                */
/*                 pInstance0Tnl - Pointer to Instance 0 tunnel              */
/* Output(s)     : None                                                      */
/* Return(s)     : None                                                      */
/*****************************************************************************/
VOID
TeDeleteTnlFrrInfo (tTeTnlInfo * pTeTnlInfo, tTeTnlInfo * pInstance0Tnl)
{
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    if (pInstance0Tnl != NULL)
    {
        return;
    }

    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);

    if (pTeFrrConstInfo == NULL)
    {
        return;
    }

    if (TE_REL_MEM_BLOCK (TE_TNL_FRR_CONST_INFO_POOL_ID,
                          (UINT1 *) (pTeFrrConstInfo)) == MEM_FAILURE)
    {
        return;
    }

    TE_TNL_FRR_CONST_INFO (pTeTnlInfo) = NULL;
    return;
}

/*****************************************************************************/
/* Function Name : TeSigGetTnlDest                                           */
/* Description   : This function is used to get the destination to which     */
/*                 tunnel to be established.                                 */
/* Input(s)      : pTeTnlInfo - Pointer to Tunnel Information                */
/* Output(s)     : None                                                      */
/* Return(s)     : u4DestAddr - Tunnel Destination Address                   */
/*****************************************************************************/
UINT4
TeSigGetTnlDest (tTeTnlInfo * pTeTnlInfo)
{
    UINT4               u4DestAddr = TE_ZERO;
    tTeHopInfo         *pTeErHopInfo = NULL;
    UINT1               u1IsEgrOfHop = TE_FALSE;

    if (TE_TNL_PATH_LIST_INFO (pTeTnlInfo) == NULL)
    {
        CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTeTnlInfo)), u4DestAddr);
        u4DestAddr = TE_NTOHL (u4DestAddr);
    }
    else
    {
        /* Search list of ERO's */
        TMO_SLL_Scan (&TE_HOP_LIST (pTeTnlInfo), pTeErHopInfo, tTeHopInfo *)
        {
            if (TeIsLsrPartOfErHop (pTeErHopInfo, &u1IsEgrOfHop) == TE_SUCCESS)
            {
                continue;
            }
            else
            {
                MEMCPY ((UINT1 *) &u4DestAddr,
                        (UINT1 *) &(TE_ERHOP_IPV4_ADDR
                                    (pTeErHopInfo)), IPV4_ADDR_LENGTH);
                u4DestAddr = OSIX_NTOHL (u4DestAddr);
                break;
            }
        }
        if (pTeErHopInfo == NULL)
        {
            CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTeTnlInfo)), u4DestAddr);
            u4DestAddr = TE_NTOHL (u4DestAddr);
        }
    }

    return u4DestAddr;
}

/*****************************************************************************/
/* Function Name : TeSigGetL3IntfFromDestAddr                                */
/* Description   : This function is used to get the L3 Interface associated  */
/*                 with the Destination Address                              */
/* Input(s)      : u4DestAddr - Destination Address                          */
/* Output(s)     : None                                                      */
/* Return(s)     : u4L3Intf   - L3 Interface Index                           */
/*****************************************************************************/

UINT4
TeSigGetL3IntfFromDestAddr (UINT4 u4DestAddr)
{
    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;
    UINT4               u4L3Intf = TE_ZERO;

    MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
    MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));

    RtQuery.u4DestinationSubnetMask = 0xFFFFFFFF;
    RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;
    RtQuery.u4DestinationIpAddress = u4DestAddr;

    if (NetIpv4IfIsOurAddress (RtQuery.u4DestinationIpAddress) ==
        NETIPV4_SUCCESS)
    {
        return u4L3Intf;
    }

    if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) != NETIPV4_SUCCESS)
    {
        return u4L3Intf;
    }

    if (NetIpv4GetCfaIfIndexFromPort (NetIpRtInfo.u4RtIfIndx,
                                      &u4L3Intf) == NETIPV4_SUCCESS)
    {
        return u4L3Intf;
    }

    return u4L3Intf;
}

/*****************************************************************************/
/* Function Name : TeSigGetTnlIfIndex
 * Description   : This routine gets the tunnel if index for the passed
 *                 tunnel indices.
 * Input(s)      : u4TnlIndex    - Tunnel Index
 *                 u4TnlInstance - Tunnel Instance
 *                 u4IngressId   - Tunnel Ingress LSR Id
 *                 u4EgressId    - Tunnel Egress LSR Id
 * Output(s)     : pu4TnlIfIndex - Tunnel If Index
 * Return(s)     : TE_SUCCESS/TE_FAILURE
 *****************************************************************************/
UINT1
TeSigGetTnlIfIndex (UINT4 u4TnlIndex, UINT4 u4TnlInstance,
                    UINT4 u4IngressId, UINT4 u4EgressId, UINT4 *pu4TnlIfIndex)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    pTeTnlInfo =
        TeGetTunnelInfo (u4TnlIndex, u4TnlInstance, u4IngressId, u4EgressId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }

    if (TE_TNL_OPER_STATUS (pTeTnlInfo) == TE_OPER_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }

    *pu4TnlIfIndex = TE_TNL_IFINDEX (pTeTnlInfo);

    MPLS_CMN_UNLOCK ();
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeSigCreateCHopListInfo
 * Description   : This routine reserves a tTeCHopListInfo structure from
 *                 the global array and a pointer to the structure is
 *                 returned.
 *                 If all the tTeCHopListInfo in the global array are
 *                 reserved then the routine returns failure.
 * Input(s)      : NONE
 * Output(s)     : ppTeCHopListInfo - Pointer to pointer of tTeCHopListInfo
 * Return(s)     : TE_SUCCESS / TE_FAILURE
 ******************************************************************************/
INT4
TeSigCreateCHopListInfo (tTeCHopListInfo ** ppTeCHopListInfo)
{
    INT4                i4RetVal = TE_FAILURE;

    MPLS_CMN_LOCK ();

    i4RetVal = TeCreateCHopListInfo (ppTeCHopListInfo);

    MPLS_CMN_UNLOCK ();

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name : TeSigDeleteCHopListInfo
 * Description   : This routine releases the PathListInfo and all the
 *                 associated  CHopInfo's
 *
 * Input(s)      : pTeCHopListInfo - Pointer to tTeCHopListInfo
 * Output(s)     : 
 * Return(s)     : TE_SUCCESS / TE_FAILURE
 ******************************************************************************/
INT4
TeSigDeleteCHopListInfo (tTeCHopListInfo * pTeCHopListInfo)
{
    INT4                i4RetVal = TE_FAILURE;

    MPLS_CMN_LOCK ();

    i4RetVal = TeDeleteCHopListInfo (pTeCHopListInfo);

    MPLS_CMN_UNLOCK ();

    return i4RetVal;
}

/*****************************************************************************/
 /* Function Name : TeSigCreateCHopsForCspfPaths                                    
  * Description   : This routine populates the computed Hop List with set of
  *                 hops given by CSPF.
  * Input(s)      : pTeTnlInfo       - Pointer to the Tunnel Info.
  *                 pCspfPath        - Pointer to the CSPF Path.  *
  * Output(s)     : ppTeCHopListInfo - Computed Hop List with set of hops
  * Return(s)     : TE_SUCCESS / TE_FAILURE
  ******************************************************************************/
INT4
TeSigCreateCHopsForCspfPaths (tTeTnlInfo * pTeTnlInfo,
                              tOsTeAppPath * pCspfPath,
                              tTeCHopListInfo ** ppTeCHopListInfo)
{
    INT4                i4RetVal = TE_FAILURE;

    MPLS_CMN_LOCK ();

    i4RetVal = TeCreateCHopsForCspfPaths (pTeTnlInfo, pCspfPath,
                                          ppTeCHopListInfo);

    MPLS_CMN_UNLOCK ();

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name : TeSigGetAttrListIndexFromName                             */
/* Description   : This function is used to get the Attribute list Index     */
/*                 Attribute List Name.                                      */
/* Input(s)      : pInTeAttrListInfo - Pointer to Attribute List Info        */
/*                                     containing Name as key                */
/* Output(s)     : pInTeAttrListInfo - Pointer to Attribute List Info        */
/*                                     containing List Index as output       */
/* Return(s)     : TE_SUCCESS / TE_FAILURE                                   */
/*****************************************************************************/
INT4
TeSigGetAttrListIndexFromName (tTeAttrListInfo * pInTeAttrListInfo)
{
    INT4                i4RetVal = TE_ZERO;

    MPLS_CMN_LOCK ();

    i4RetVal = TeGetAttrListIndexFromName (pInTeAttrListInfo);

    MPLS_CMN_UNLOCK ();

    return i4RetVal;
}

/*****************************************************************************/
 /* Function Name : TeSigExtSetAttributeListMask
  * Description   : This routine sets the attribute list mask.
  * Input(s)      : u4AttrListIndex  - Attribute List Index.
  *                 u4BitMask        - Bit Mask value.
  * Output(s)     : None
  * Return(s)     : TE_SUCCESS / TE_FAILURE
  ******************************************************************************/
VOID
TeSigExtSetAttributeListMask (UINT4 u4AttrListIndex, UINT4 u4BitMask)
{
    MPLS_CMN_LOCK ();

    TeSetAttributeListMask (u4AttrListIndex, u4BitMask);

    MPLS_CMN_UNLOCK ();

    return;
}

/*****************************************************************************/
 /* Function Name : TeSigExtResetAttributeListMask
  * Description   : This routine resets the attribute list mask.
  * Input(s)      : u4AttrListIndex  - Attribute List Index.
  *                 u4BitMask        - Bit Mask value.
  * Output(s)     : None
  * Return(s)     : TE_SUCCESS / TE_FAILURE
  ******************************************************************************/
VOID
TeSigExtResetAttributeListMask (UINT4 u4AttrListIndex, UINT4 u4BitMask)
{
    MPLS_CMN_LOCK ();

    TeResetAttributeListMask (u4AttrListIndex, u4BitMask);

    MPLS_CMN_UNLOCK ();

    return;
}

/*****************************************************************************/
 /* Function Name : TeSigExtGetAttributeListMask
  * Description   : This routine returns the attribute list mask.
  * Input(s)      : u4AttrListIndex  - Attribute List Index.
  * Output(s)     : None
  * Return(s)     : u4AttrListMask   - Attribute List Mask.
  ******************************************************************************/
UINT4
TeSigExtGetAttributeListMask (UINT4 u4AttrListIndex)
{
    UINT4               u4AttrListMask = TE_ZERO;

    MPLS_CMN_LOCK ();

    u4AttrListMask = TeGetAttributeListMask (u4AttrListIndex);

    MPLS_CMN_UNLOCK ();

    return u4AttrListMask;
}

/******************************************************************************
 Function Name  : TeSigGetAttrFromTnlOrAttrListByMask
 Description    : This routine is used to get the attributes for Tunnel of
                  Attribute list
 Input          : pTeTnlInfo        - Pointer to Te-tnl Info
                  u4ListBitMask     - Mask value
 Output         : None
 Returns        : u4RetVal          - Attribute value
****************************************************************************/
UINT4
TeSigGetAttrFromTnlOrAttrListByMask (tTeTnlInfo * pTeTnlInfo,
                                     UINT4 u4ListBitMask)
{
    UINT4               u4RetVal;

    MPLS_CMN_LOCK ();

    u4RetVal = TeGetAttrFromTnlOrAttrListByMask (pTeTnlInfo, u4ListBitMask);

    MPLS_CMN_UNLOCK ();

    return u4RetVal;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : TeSigSendAdminStatusTrapAndSyslog     
 * Description     : This function sends Admin Status trap                                                         
 * Input (s)       : pTeTnlInfo - Pointer to TeTnlInfo Structure.
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
TeSigSendAdminStatusTrapAndSyslog (tTeTnlInfo * pTeTnlInfo)
{
    MPLS_CMN_LOCK ();
    TeSendAdminStatusTrapAndSyslog (pTeTnlInfo);
    MPLS_CMN_UNLOCK ();
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : TeSigCreateTnlErrorTable     
 * Description     : This function creates tunnel error table
 * Input (s)       : pTeTnlInfo - Pointer to TeTnlInfo Structure.
 *                   TunnelErrInfo - Pointer to tTunnelErrInfo
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
TeSigCreateTnlErrorTable (tTunnelErrInfo * pTunnelErrInfo,
                          tTeTnlInfo * pTeTnlInfo)
{
    MPLS_CMN_LOCK ();
    TeCreateTnlErrorTable (pTunnelErrInfo, pTeTnlInfo);
    MPLS_CMN_UNLOCK ();
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : TeCreateTnlErrorTable   
 * Description     : This function creates tunnel error table
 * Input (s)       : pTeTnlInfo - Pointer to TeTnlInfo Structure.
 *                   pTunnelErrInfo - Pointer to tTunnelErrInfo   
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
TeCreateTnlErrorTable (tTunnelErrInfo * pTunnelErrInfo, tTeTnlInfo * pTeTnlInfo)
{
    tTeTnlInfo         *pTempTeTnlInfo = NULL;
    tTeTnlInfo         *pInstance0Tnl = NULL;
    UINT4               u4TnlIngress = TE_ZERO;
    UINT4               u4TnlEgress = TE_ZERO;

    TE_DBG (TE_MAIN_PRCS, "TeCreateTnlErrorTable: ENTRY \n");
    CONVERT_TO_INTEGER ((pTeTnlInfo->TnlIngressLsrId), u4TnlIngress);
    u4TnlIngress = OSIX_NTOHL (u4TnlIngress);
    CONVERT_TO_INTEGER ((pTeTnlInfo->TnlEgressLsrId), u4TnlEgress);
    u4TnlEgress = OSIX_NTOHL (u4TnlEgress);

    pTempTeTnlInfo = TeGetTunnelInfo (TE_TNL_TNL_INDEX (pTeTnlInfo),
                                      TE_TNL_TNL_INSTANCE (pTeTnlInfo),
                                      u4TnlIngress, u4TnlEgress);
    if (pTempTeTnlInfo == NULL)
    {
        TE_DBG (TE_MAIN_PRCS, "EXTN : Tnl is not found in the TnlHashTable \n");
        return;
    }

    pInstance0Tnl =
        TeGetTunnelInfo (TE_TNL_TNL_INDEX (pTeTnlInfo), TE_ZERO, u4TnlIngress,
                         u4TnlEgress);

    if (pInstance0Tnl != NULL)
    {
        pTempTeTnlInfo = pInstance0Tnl;
    }

    if (pTempTeTnlInfo->pTunnelErrInfo == NULL)
    {
        pTempTeTnlInfo->pTunnelErrInfo =
            (tTunnelErrInfo *) TE_ALLOC_MEM_BLOCK (TE_TNL_ERROR_TBL_POOL_ID);
    }
    if (pTempTeTnlInfo->pTunnelErrInfo == NULL)
    {
        TE_DBG (TE_MAIN_PRCS,
                "EXTN : MemAllocation failed for Tnl Err Info \n");
        TE_DBG (TE_MAIN_PRCS, "TeCreateTnlErrorTable: INTMD-EXIT \n");
        return;
    }
    MEMCPY (pTempTeTnlInfo->pTunnelErrInfo, pTunnelErrInfo,
            sizeof (tTunnelErrInfo));
    TE_DBG (TE_MAIN_PRCS, "TeCreateTnlErrorTable: EXIT \n");
    KW_FALSEPOSITIVE_FIX (pTempTeTnlInfo->pTunnelErrInfo);
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : TeSigDeleteTnlErrorTable     
 * Description     : This function creates tunnel error table
 * Input (s)       : pTeTnlInfo - Pointer to TeTnlInfo Structure.
 *                   TunnelErrInfo - Pointer to tTunnelErrInfo
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
TeSigDeleteTnlErrorTable (tTeTnlInfo * pTeTnlInfo)
{
    MPLS_CMN_LOCK ();
    TeDeleteTnlErrorTable (pTeTnlInfo);
    MPLS_CMN_UNLOCK ();
    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : TeDeleteTnlErrorTable   
 * Description     : This function creates tunnel error table
 * Input (s)       : pTeTnlInfo - Pointer to TeTnlInfo Structure.
 *                   pTunnelErrInfo - Pointer to tTunnelErrInfo   
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
TeDeleteTnlErrorTable (tTeTnlInfo * pTeTnlInfo)
{
    tTeTnlInfo         *pTempTeTnlInfo = NULL;
    tTeTnlInfo         *pInstance0Tnl = NULL;
    UINT4               u4TnlIngress = TE_ZERO;
    UINT4               u4TnlEgress = TE_ZERO;

    TE_DBG (TE_MAIN_PRCS, "TeDeleteTnlErrorTable: ENTRY \n");
    CONVERT_TO_INTEGER ((pTeTnlInfo->TnlIngressLsrId), u4TnlIngress);
    u4TnlIngress = OSIX_NTOHL (u4TnlIngress);
    CONVERT_TO_INTEGER ((pTeTnlInfo->TnlEgressLsrId), u4TnlEgress);
    u4TnlEgress = OSIX_NTOHL (u4TnlEgress);

    pTempTeTnlInfo = TeGetTunnelInfo (TE_TNL_TNL_INDEX (pTeTnlInfo),
                                      TE_TNL_TNL_INSTANCE (pTeTnlInfo),
                                      u4TnlIngress, u4TnlEgress);
    if (pTempTeTnlInfo == NULL)
    {
        TE_DBG (TE_MAIN_PRCS, "EXTN : Tnl is not found in the TnlHashTable \n");
        return;
    }

    pInstance0Tnl =
        TeGetTunnelInfo (TE_TNL_TNL_INDEX (pTempTeTnlInfo), TE_ZERO,
                         u4TnlIngress, u4TnlEgress);

    if (pInstance0Tnl != NULL)
    {
        pTempTeTnlInfo = pInstance0Tnl;
    }

    if ((pTempTeTnlInfo->pTunnelErrInfo != NULL) &&
        (pTempTeTnlInfo->u1TnlRelStatus == TE_SIGMOD_TNLREL_AWAITED))
    {
        TE_REL_MEM_BLOCK (TE_TNL_ERROR_TBL_POOL_ID,
                          (UINT1 *) (pTempTeTnlInfo->pTunnelErrInfo));
        pTempTeTnlInfo->pTunnelErrInfo = NULL;
    }

    TE_DBG (TE_MAIN_PRCS, "TeDeleteTnlErrorTable: EXIT \n");
    return;
}

/*****************************************************************************
 * Function Name : TeSigCreateTunnel                                            
 * Description   : This function takes MPLS_CMN_LOCK and creates tunnel entry
 *                 in TE module.
 * Input(s)      : pInstance0Tnl  - Pointer to Zero Instance Tunnel Entry               
 * Output(s)     : pNewTeTnl      - Address of the Pointer to new tunnel entry
 *                                  in case of successful allocation.
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                 
 *****************************************************************************/
UINT1
TeSigCreateTunnel (tTeTnlInfo * pInstance0Tnl, tTeTnlInfo ** pNewTeTnl)
{
    UINT1               u1RetVal = TE_FAILURE;

    MPLS_CMN_LOCK ();

    u1RetVal = TeCreateTunnel (pInstance0Tnl, pNewTeTnl);

    MPLS_CMN_UNLOCK ();

    return u1RetVal;
}

/*****************************************************************************/
/* Function Name : TeSigSetDetourStatus
 * Description   : This routine sets the detour active status in TE by taking
 *                 MPLS_CMN_LOCK.
 * Input(s)      : pTeTnlInfo     - Pointer to Tunnel Info.   
 *                 u1DetourStatus - Detour Status
 * Output(s)     : None                         
 * Return(s)     : None
 *****************************************************************************/
VOID
TeSigSetDetourStatus (tTeTnlInfo * pTeTnlInfo, UINT1 u1DetourStatus)
{
    MPLS_CMN_LOCK ();

    TeSetDetourStatus (pTeTnlInfo, u1DetourStatus);

    MPLS_CMN_UNLOCK ();
}

/*****************************************************************************/
/* Function Name : TeSigUpdateMapTnlIndex                                       
 * Description   : checks the tunnel in tunnel table ,if present         
 *                 updates the MapTnl prperties and increments the      
 *                 number of stacked tunnels.           
 * Input(s)      : u4MplsTnlIf    - Tunnel Interface Index               
 * Output(s)     : pu4TnlIndex    - Pointer to Tunnel Index
 *                 pu4TnlInstance - Pointer to Tunnel Instance
 *                 pIngressLsrId  - Pointer to Tunnel Ingress LsrId
 *                 pEgressLsrId   - Pointer to Tunnel Egress LsrId
 *                 pu1TnlRole     - Pointer to Tunnel Role
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                 
 *****************************************************************************/
UINT1
TeSigUpdateMapTnlIndex (UINT4 u4MplsTnlIf, UINT4 *pu4TnlIndex,
                        UINT4 *pu4TnlInstance, tTeRouterId * pIngressLsrId,
                        tTeRouterId * pEgressLsrId, UINT1 *pu1TnlRole)
{
    UINT1               u1Return = TE_FAILURE;
    MPLS_CMN_LOCK ();
    u1Return = TeUpdateMapTnlIndex (u4MplsTnlIf, pu4TnlIndex,
                                    pu4TnlInstance, pIngressLsrId,
                                    pEgressLsrId, pu1TnlRole);
    MPLS_CMN_UNLOCK ();
    return u1Return;
}

/*****************************************************************************/
/* Function Name : TeUpdateMapTnlIndex                                       
 * Description   : checks the tunnel in tunnel table ,if present         
 *                 copies its index, instance, igress and egress LSR Ids
 * Input(s)      : u4MplsTnlIf    - Tunnel Interface Index
 * Output(s)     : pu4TnlIndex    - Pointer to Tunnel Index
 *                 pu4TnlInstance - Pointer to Tunnel Instance
 *                 pIngressLsrId  - Pointer to Tunnel Ingress LsrId
 *                 pEgressLsrId   - Pointer to Tunnel Egress LsrId
 *                 pu1TnlRole     - Pointer to Tunnel Role
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                 
 *****************************************************************************/
UINT1
TeUpdateMapTnlIndex (UINT4 u4MplsTnlIf, UINT4 *pu4TnlIndex,
                     UINT4 *pu4TnlInstance, tTeRouterId * pIngressLsrId,
                     tTeRouterId * pEgressLsrId, UINT1 *pu1TnlRole)
{
    tTeTnlInfo         *pFATeTnlInfo = NULL;

    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeUpdateMapTnlIndex : ENTRY \n");

    TeGetTunnelFrmIfIdbasedTree (u4MplsTnlIf, &pFATeTnlInfo);

    if (pFATeTnlInfo == NULL)
    {
        TE_DBG (TE_EXTN_FAIL,
                "EXTN : Tunnel Entry not found. No Further processing \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeUpdateMapTnlIndex : INTMD-EXIT \n");
        return TE_FAILURE;

    }
    *pu4TnlIndex = pFATeTnlInfo->u4TnlIndex;
    *pu4TnlInstance = pFATeTnlInfo->u4TnlPrimaryInstance;
    MEMCPY (pIngressLsrId, &pFATeTnlInfo->TnlIngressLsrId, IPV4_ADDR_LENGTH);
    MEMCPY (pEgressLsrId, &pFATeTnlInfo->TnlEgressLsrId, IPV4_ADDR_LENGTH);
    *pu1TnlRole = TE_TNL_ROLE (pFATeTnlInfo);
    pFATeTnlInfo->u4NoOfStackedTunnels++;

    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeSigAddToTeIfIdxbasedTable                                       
 * Description   : Adds the tunnel in global ifIndex based RBTree        
 * Input(s)      : tTeTnlInfo     - Pointer to TeTnlInfo Structured      
 * Output(s)     : NONE
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                 
 *****************************************************************************/
UINT1
TeSigAddToTeIfIdxbasedTable (tTeTnlInfo * pTeTnlInfo)
{
    UINT1               u1RetVal = TE_ZERO;

    MPLS_CMN_LOCK ();

    u1RetVal = TeAddToTeIfIdxbasedTable (pTeTnlInfo);

    MPLS_CMN_UNLOCK ();

    return u1RetVal;
}

/*****************************************************************************/
/* Function Name : TeSigDelFromTeIfIdxbasedTable                                       
 * Description   : Deletes Tunnel If Index from in global ifIndex based RBTree
 * Input(s)      : tTeTnlInfo     - Pointer to TeTnlInfo Structured      
 * Output(s)     : NONE
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                 
 *****************************************************************************/
UINT1
TeSigDelFromTeIfIdxbasedTable (tTeTnlInfo * pTeTnlInfo)
{
    UINT1               u1RetVal = TE_ZERO;

    MPLS_CMN_LOCK ();

    u1RetVal = TeDelFromTeIfIdxbasedTable (pTeTnlInfo);

    MPLS_CMN_UNLOCK ();

    return u1RetVal;
}

/************************************************************************
 *  Function Name   : TeSigCopyOutLabel
 *  Description     : Copies the Out label from the Source TeTnlInfo
 *                    to the Destination TeTnlInfo
 *  Input           : pSrcTeTnlInfo -> Source Tunnel Info
 *                    pDestTeTnlInfo -> Destination Tunnel Info
 *  Output          : None
 *  Returns         : TE_SUCCESS / TE_FAILURE
 ************************************************************************/
UINT1
TeSigCopyOutLabel (tTeTnlInfo * pSrcTeTnlInfo, tTeTnlInfo * pDestTeTnlInfo)
{
    UINT1               u1RetVal = TE_ZERO;

    MPLS_CMN_LOCK ();

    u1RetVal = TeCopyOutLabel (pSrcTeTnlInfo, pDestTeTnlInfo);

    MPLS_CMN_UNLOCK ();

    return u1RetVal;
}

/************************************************************************
 *  Function Name   : TeSigDeleteStackTnlIf
 *  Description     : Deletes the tunnel interface and their stacking
 *  Input           : pTeTnlInfo -> Pointer to Tunnel Info
 *  Output          : None
 *  Returns         : TE_SUCCESS / TE_FAILURE
 ************************************************************************/
UINT1
TeSigDeleteStackTnlIf (tTeTnlInfo * pTeTnlInfo)
{
    UINT1               u1RetVal = TE_ZERO;

    MPLS_CMN_LOCK ();

    u1RetVal = TeDeleteStackTnlIf (pTeTnlInfo);

    MPLS_CMN_UNLOCK ();

    return u1RetVal;
}

/*****************************************************************************/
/* Function Name : TeSigAddToTeMsgIdBasedTable
 * Description   : Adds the tunnel in global MsgId based RBTree
 * Input(s)      : tTeTnlInfo     - Pointer to TeTnlInfo Structure
 *                 u4MsgId        - Message Id
 * Output(s)     : NONE
 * Return(s)     : None
 *****************************************************************************/

VOID
TeSigAddToTeMsgIdBasedTable (tTeTnlInfo * pTeTnlInfo, UINT4 u4MsgId)
{
    MPLS_CMN_LOCK ();

    TeAddToTeMsgIdBasedTable (pTeTnlInfo, u4MsgId);

    MPLS_CMN_UNLOCK ();

    return;
}

/*****************************************************************************/
/* Function Name : TeSigGetTunnelInfoFromOutMsgId
 * Description   : Gets the tunnel from global MsgId based RBTree
 * Input(s)      : tTeTnlInfo     - Pointer to TeTnlInfo Structure
 *                 u4MsgId        - Message Id
 * Output(s)     : NONE
 * Return(s)     : None
 *****************************************************************************/

VOID
TeSigGetTunnelInfoFromOutMsgId (tTeTnlInfo ** ppTeTnlInfo, UINT4 u4MsgId)
{
    tTeTnlInfo         *pTeTnlInfo = *ppTeTnlInfo;
    MPLS_CMN_LOCK ();

    TeGetTunnelInfoFromOutMsgId (&pTeTnlInfo, u4MsgId);

    *ppTeTnlInfo = pTeTnlInfo;

    MPLS_CMN_UNLOCK ();

    return;
}

/*****************************************************************************/
/* Function Name : TeSigDelFromTeMsgIdBasedTable
 * Description   : Deletes the tunnel in global MsgId based RBTree
 * Input(s)      : tTeTnlInfo     - Pointer to TeTnlInfo Structure
 * Output(s)     : NONE
 * Return(s)     : None
 *****************************************************************************/

VOID
TeSigDelFromTeMsgIdBasedTable (tTeTnlInfo * pTeTnlInfo)
{
    MPLS_CMN_LOCK ();

    TeDelFromTeMsgIdBasedTable (pTeTnlInfo);
    MPLS_CMN_UNLOCK ();

    return;
}

/*****************************************************************************/
/* Function Name : TeSigSetGrSynchronizedStatus
 * Description   : This function is used to Synchronized status for a tunnel.
 * Input(s)      : tTeTnlInfo            - Pointer to TeTnlInfo Structure
 * Output(s)     : u1TnlSynStatus        - Synchronized status
 * Return(s)     : None
 *****************************************************************************/
VOID
TeSigSetGrSynchronizedStatus (tTeTnlInfo * pTeTnlInfo, UINT1 u1TnlSynStatus)
{
    MPLS_CMN_LOCK ();

    TeSetGrSynchronizedStatus (pTeTnlInfo, u1TnlSynStatus);

    MPLS_CMN_UNLOCK ();

    return;
}

/*****************************************************************************/
/* Function Name : TeSigGrDeleteNonSynchronizedTnls
 * Description   : This function deletes the non synchronized tunnel.
 * Input(s)      : None
 * Output(s)     : None
 * Return(s)     : None
 *****************************************************************************/

VOID
TeSigGrDeleteNonSynchronizedTnls ()
{
    MPLS_CMN_LOCK ();

    TeGrDeleteNonSynchronizedTnls ();

    MPLS_CMN_UNLOCK ();

    return;
}

/*****************************************************************************/
/* Function Name : TeSigGetTrafficParams
 * Description   : This function is used to retrieve the traffic parameters
 *                 information from TE for a tunnel.
 * Input(s)      : tTeTnlInfo            - Pointer to TeTnlInfo Structure                 
 * Output(s)     : pu4PeakDataRate       - Peak Data Rate
 *                 pu4PeakBurstSize      - Peak Burst Size
 *                 pu4CommittedDataRate  - Committed Data Rate
 *                 pu4CommittedBurstSize - Committed Burst Size
 *                 pu4ExcessBurstSize    - Excess Burst Size
 * Return(s)     : None
 *****************************************************************************/
VOID
TeSigGetTrafficParams (tTeTnlInfo * pTeTnlInfo, UINT4 *pu4PeakDataRate,
                       UINT4 *pu4PeakBurstSize, UINT4 *pu4CommittedDataRate,
                       UINT4 *pu4CommittedBurstSize, UINT4 *pu4ExcessBurstSize)
{
    MPLS_CMN_LOCK ();

    TeGetTrafficParams (pTeTnlInfo, pu4PeakDataRate, pu4PeakBurstSize,
                        pu4CommittedDataRate, pu4CommittedBurstSize,
                        pu4ExcessBurstSize);

    MPLS_CMN_UNLOCK ();

    return;
}

/*****************************************************************************/
/* Function     : TeSigGetLabelInfoUsingTnl                                  */
/* Description  : This function gets label info using te tunnel              */
/* Input        : pTeTnlInfo         - pointer to tunnel info                */
/*                u1Direction        - Direction                             */
/* Output       : pu4InLabel         - input label                           */
/*                pu4OutLbl          - output label                          */
/* Returns      : NONE                                                       */
/*****************************************************************************/

VOID
TeSigGetLabelInfoUsingTnl (tTeTnlInfo * pTeTnlInfo, UINT4 *pu4OutLbl,
                           UINT4 *pu4InLbl, UINT1 u1Direction)
{
    MPLS_CMN_LOCK ();

    TeGetLabelInfoUsingTnl (pTeTnlInfo, pu4OutLbl, pu4InLbl, u1Direction);

    MPLS_CMN_UNLOCK ();

    return;
}

/*****************************************************************************/
/* Function     : TeSigGetNoOfStackedTnls                                    */
/* Description  : This function gets number of tunnels stacked over the      */
/*                FA LSP signalled by RSVP                                   */
/* Input        : u4TnlIndex         - Index of the tunnel                   */
/*                u4TnlInstance      - Instance of the tunnel                */
/*                u4TnlIngressId     - Ingress of the tunnel                 */
/*                u4TnlEgressId      - Egress of the tunnel                  */
/* Output       : pu4NoOfStackedTnl  - Number of Stacked Tunnels             */
/* Returns      : NONE                                                       */
/*****************************************************************************/
VOID
TeSigGetNoOfStackedTnls (UINT4 u4TnlIndex, UINT4 u4TnlInstance,
                         UINT4 u4TnlIngressId, UINT4 u4TnlEgressId,
                         UINT4 *pu4NoOfStackedTnl)
{

    MPLS_CMN_LOCK ();

    TeGetNoOfStackedTnls (u4TnlIndex, u4TnlInstance, u4TnlIngressId,
                          u4TnlEgressId, pu4NoOfStackedTnl);

    MPLS_CMN_UNLOCK ();

    return;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : TeUtlFetchTnlInfoFromTnlTable
 * Description     : This function will check the TE-RB Tree and fetch the
 *                   tunnel info based on tunnel index.
 * Input (s)       : UINT4 u4TunnelIndex - Fetch tunnel info based on this index
 * Output (s)      : tTeTnlInfo * pTeTnlInfo
 * Returns         : TE_SUCCESS/TE_FAILURE
 */
/*---------------------------------------------------------------------------*/
UINT1
TeUtlFetchTnlInfoFromTnlTable (UINT4 u4TunnelIndex, tTeTnlInfo * pTeTnlInfo)
{
    tTeTnlInfo         *pTempTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    pTempTeTnlInfo = (tTeTnlInfo *) RBTreeGetFirst (gTeGblInfo.TnlInfoTbl);

    while (pTempTeTnlInfo != NULL)
    {
        if (u4TunnelIndex == pTempTeTnlInfo->u4TnlIndex)
        {
            MEMCPY (pTeTnlInfo, pTempTeTnlInfo, sizeof (tTeTnlInfo));
            MPLS_CMN_UNLOCK ();
            return TE_SUCCESS;
        }

        pTempTeTnlInfo
            = (tTeTnlInfo *) RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                            (tRBElem *) pTempTeTnlInfo, NULL);
    }

    MPLS_CMN_UNLOCK ();
    return TE_FAILURE;
}

/*****************************************************************************/
/* Function     : TeSigUpdateMaxErhopsPerTnl                                 */
/* Description  : This function sets Erhop limit per tunnel of RSVP          */
/*                signalling in TE                                           */
/* Input        : i4TeRpteMaxErhopsPerTnl - Erhop limit of RSVP              */
/*                signalling.                                                */
/* Output       : NONE                                                       */
/* Returns      : NONE                                                       */
/*****************************************************************************/

VOID
TeSigUpdateMaxErhopsPerTnl (INT4 i4TeRpteMaxErhopsPerTnl)
{
    MPLS_CMN_LOCK ();
    TE_MAX_RPTE_ERPERTNL (gTeGblInfo) = (UINT2) i4TeRpteMaxErhopsPerTnl;
    MPLS_CMN_UNLOCK ();
}

/*****************************************************************************/
/* Function     : TeGetMaxErhopsPerTnl                                       */
/* Description  : This function gets Erhop limit per tunnel of RSVP in TE    */
/* Input        : NONE                                                       */
/* Output       : NONE                                                       */
/* Returns      : Maximum Erhops per tunnel                                  */
/*****************************************************************************/

UINT2
TeGetMaxErhopsPerTnl (VOID)
{
    return TE_MAX_RPTE_ERPERTNL (gTeGblInfo);
}

/*****************************************************************************/
/* Function     : TeSigCheckMaxErhopInTnlTbl                                 */
/* Description  : This function checks the Erhop count in tunnel, that is    */
/*                greater than signalling max erhop                          */
/* Input        : i4TeRpteMaxErhopsPerTnl    -value for MaxErhops per tunnel */
/*                                            got from RSVP.                 */
/* Output       : NONE                                                       */
/* Returns      : TE_SUCCESS / TE_FAILURE                                    */
/*****************************************************************************/

UINT1
TeSigCheckMaxErhopInTnlTbl (INT4 i4TeRpteMaxErhopsPerTnl)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();
    pTeTnlInfo = RBTreeGetFirst (gTeGblInfo.TnlInfoTbl);

    while (pTeTnlInfo != NULL)
    {
        /* verifying only for the tunnels which are Admin up because,
         * some tunnels might have failed in Row status active Test,
         * so not considering those tunnels. */
        if ((TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_RSVP) &&
            ((TE_TNL_PATH_INFO (pTeTnlInfo)) != NULL) &&
            (TE_TNL_ADMIN_STATUS (pTeTnlInfo) == TE_ADMIN_UP))
        {
            if (TE_PO_HOP_COUNT (TE_TNL_PATH_INFO (pTeTnlInfo)) >
                (UINT2) i4TeRpteMaxErhopsPerTnl)
            {
                MPLS_CMN_UNLOCK ();
                return TE_FAILURE;
            }
        }
        pTeTnlInfo = RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                    (tRBElem *) pTeTnlInfo, NULL);
    }
    MPLS_CMN_UNLOCK ();
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeSigProcessL2VpnAssociation                     */
/* Description   : This routine calls the TePrcsL2VpnAssociation Function to */
/*                 Post Event to L2Vpn For Tunnel Events                     */
/* Input(s)      : tTeTnlInfo - Pointer to  Tunnel Structure.                */
/*              : u4Action  -  Tunnel Event                                 */
/*                 in case of successful allocation.                         */
/*****************************************************************************/
VOID
TeSigProcessL2VpnAssociation (tTeTnlInfo * pTeTnlInfo, UINT4 u4EvtType)
{

    TePrcsL2vpnAssociation (pTeTnlInfo, u4EvtType);

}

/*****************************************************************************/
/* Function Name : TeUpdateReoptimizeStatus                                    */
/* Description   : This routine check whether Reoptimization is enabled or   */
/*                    disabled on Transit Node or Egress Node                   */
/* Input(s)      : tTeTnlInfo - Pointer to  Tunnel Structure.                */
/*                 in case of successful allocation.                         */
/* Returns       : TE_SUCCESS/TE_FAILURE                                     */
/*****************************************************************************/
VOID
TeSigUpdateReoptimizeStatus (tTeTnlInfo * pTeTnlInfo)
{
    tTeReoptTnlInfo    *pTeGetReoptTnlInfo = NULL;
    tTeReoptTnlInfo     TeReoptTnlInfo;
    tTeTnlInfo         *pIns0TeTnlInfo = NULL;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;

    MPLS_CMN_LOCK ();

    MEMSET (&TeReoptTnlInfo, TE_ZERO, sizeof (tTeReoptTnlInfo));

    CONVERT_TO_INTEGER (pTeTnlInfo->TnlIngressLsrId, u4IngressId);
    CONVERT_TO_INTEGER (pTeTnlInfo->TnlEgressLsrId, u4EgressId);

    u4IngressId = OSIX_NTOHL (u4IngressId);
    u4EgressId = OSIX_NTOHL (u4EgressId);

    if (TE_TNL_ROLE (pTeTnlInfo) == TE_INGRESS)
    {

        pIns0TeTnlInfo = TeGetTunnelInfo (pTeTnlInfo->u4TnlIndex, TE_ZERO,
                                          u4IngressId, u4EgressId);
        if (pIns0TeTnlInfo == NULL)
        {
            MPLS_CMN_UNLOCK ();
            return;
        }
    }

    TeReoptTnlInfo.u4ReoptTnlIndex = pTeTnlInfo->u4TnlIndex;
    MEMCPY (TeReoptTnlInfo.ReoptTnlIngressLsrId,
            pTeTnlInfo->TnlIngressLsrId, sizeof (UINT4));
    MEMCPY (TeReoptTnlInfo.ReoptTnlEgressLsrId,
            pTeTnlInfo->TnlEgressLsrId, sizeof (UINT4));

    pTeGetReoptTnlInfo =
        (tTeReoptTnlInfo *) RBTreeGet (gTeGblInfo.ReoptimizeTnlList,
                                       (tRBElem *) (&TeReoptTnlInfo));

    if (pTeGetReoptTnlInfo != NULL)
    {
        TE_TNL_REOPTIMIZE_STATUS (pTeTnlInfo) = TE_REOPTIMIZE_ENABLE;
        if (TE_TNL_ROLE (pTeTnlInfo) == TE_INGRESS)
        {
            TE_TNL_REOPTIMIZE_STATUS (pIns0TeTnlInfo) = TE_REOPTIMIZE_ENABLE;
        }
        MPLS_CMN_UNLOCK ();
        return;
    }
    TE_TNL_REOPTIMIZE_STATUS (pTeTnlInfo) = TE_REOPTIMIZE_DISABLE;
    if (TE_TNL_ROLE (pTeTnlInfo) == TE_INGRESS)
    {
        TE_TNL_REOPTIMIZE_STATUS (pIns0TeTnlInfo) = TE_REOPTIMIZE_DISABLE;
        pIns0TeTnlInfo->bIsMbbRequired = FALSE;
    }
    MPLS_CMN_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name : TeSigGetTunnelInfo
 * Description   : This routine Check the presence of a TE Tunnel info
 *                 in the Tunnel table.
 * Input(s)      : u4TunnelIndex    - Tunnel Index
 *                 u4TunnelInstance - Tunnel Instance value
 *                 u4TunnelIngressLSRId  - Tunnel Ingress LSR Id
 *                 u4TunnelEgressLSRId - Tunnel Egress LSR Id
 * Output(s)     : ppTeTnlInfo  - Pointer to  Pointer of the tunnel info
 *                 whose index matches u2TunnelIndex.
 * Return(s)     : TE_SUCCESS in case of tunnel being present, otherwise
 *                 TE_FAILURE
 *****************************************************************************/
tTeTnlInfo         *
TeSigGetTunnelInfo (UINT4 u4TunnelIndex, UINT4 u4TunnelInstance,
                    UINT4 u4TnlIngressLSRId, UINT4 u4TnlEgressLSRId)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4TunnelIndex, u4TunnelInstance,
                                  u4TnlIngressLSRId, u4TnlEgressLSRId);

    MPLS_CMN_UNLOCK ();
    return pTeTnlInfo;
}

/*****************************************************************************/
/* Function Name : TeSigCompareCHopInfo
 * Description   : This routine compare the Chop Info of two tunnels.
 *               
 * Input(s)      : pTeTnlInfo
 *                    pMapTeTnlInfo
 * Output(s)     : 
 * Return(s)     : TE_EQUAL in case of ChopInfo of both tunnels are equal.
 *                 TE_NOT_EQUAL
 *****************************************************************************/

INT1
TeSigCompareCHopInfo (tTeTnlInfo * pTeTnlInfo, tTeTnlInfo * pMapTeTnlInfo)
{

    tTMO_SLL           *pCHopList = NULL;
    tTeCHopInfo        *pTeCHopInfo = NULL;
    tTMO_SLL           *pMapTnlCHopList = NULL;
    tTeCHopInfo        *pMapTnlTeCHopInfo = NULL;
    UINT4               u4HopListIndex = TE_ZERO;
    UINT4               u4MapTnlHopListIndex = TE_ZERO;

    u4HopListIndex = pTeTnlInfo->u4TnlCHopTableIndex;
    u4MapTnlHopListIndex = pMapTeTnlInfo->u4TnlCHopTableIndex;

    pCHopList = &(gTeGblInfo.pCHopListEntry[u4HopListIndex - TE_ONE].CHopList);
    pMapTnlCHopList =
        &(gTeGblInfo.pCHopListEntry[u4MapTnlHopListIndex - TE_ONE].CHopList);

    if (TMO_SLL_Count ((pCHopList)) != TMO_SLL_Count ((pMapTnlCHopList)))
    {
        return TE_NOT_EQUAL;
    }

    pTeCHopInfo = (tTeCHopInfo *) TMO_SLL_First (pCHopList);
    pMapTnlTeCHopInfo = (tTeCHopInfo *) TMO_SLL_First (pMapTnlCHopList);

    while ((pTeCHopInfo != NULL) && (pMapTnlTeCHopInfo != NULL))
    {
        if ((pTeCHopInfo->u1AddressType == pMapTnlTeCHopInfo->u1AddressType) &&
            !(MEMCMP
              (pTeCHopInfo->IpAddr.au1Ipv4Addr,
               pMapTnlTeCHopInfo->IpAddr.au1Ipv4Addr, sizeof (UINT4)))
            && (pTeCHopInfo->u1AddrPrefixLen ==
                pMapTnlTeCHopInfo->u1AddrPrefixLen)
            && (pTeCHopInfo->u4UnnumIf == pMapTnlTeCHopInfo->u4UnnumIf)
            && (pTeCHopInfo->u2AsNumber == pMapTnlTeCHopInfo->u2AsNumber)
            && (pTeCHopInfo->u4LocalLspId == pMapTnlTeCHopInfo->u4LocalLspId)
            && (pTeCHopInfo->u1CHopType == pMapTnlTeCHopInfo->u1CHopType))
        {
            pTeCHopInfo =
                (tTeCHopInfo *) TMO_SLL_Next (pCHopList,
                                              ((tTMO_SLL_NODE *) (VOID
                                                                  *)
                                               (pTeCHopInfo)));
            pMapTnlTeCHopInfo =
                (tTeCHopInfo *) TMO_SLL_Next (pMapTnlCHopList,
                                              ((tTMO_SLL_NODE *) (VOID
                                                                  *)
                                               (pMapTnlTeCHopInfo)));
        }
        else
        {
            return TE_NOT_EQUAL;
        }
    }
    return TE_EQUAL;
}

/*****************************************************************************/
/* Function Name : TeSigCompareEroCacheAndCHopInfo
 * Description   : This routine compare the Ero Cache Hop Info with it's 
 *                 CHop Info.
 * Input(s)      : u4TnlEroCacheTableIndex
 *                    u4TnlCHopTableIndex
 * Output(s)     : 
 * Return(s)     : TE_EQUAL in case of ChopInfo of both tunnels are equal.
 *                 TE_NOT_EQUAL
 *****************************************************************************/
INT1
TeSigCompareEroCacheAndCHopInfo (UINT4 u4TnlEroCacheTableIndex,
                                 UINT4 u4TnlCHopTableIndex)
{

    tTMO_SLL           *pCHopList = NULL;
    tTeCHopInfo        *pTeCHopInfo = NULL;
    tTMO_SLL           *pEroCacheList = NULL;
    tTeCHopInfo        *pEroCacheListInfo = NULL;

    pEroCacheList =
        &(gTeGblInfo.pCHopListEntry[u4TnlEroCacheTableIndex - TE_ONE].CHopList);
    pCHopList =
        &(gTeGblInfo.pCHopListEntry[u4TnlCHopTableIndex - TE_ONE].CHopList);

    if (TMO_SLL_Count ((pEroCacheList)) != TMO_SLL_Count ((pCHopList)))
    {
        return TE_NOT_EQUAL;
    }

    pEroCacheListInfo = (tTeCHopInfo *) TMO_SLL_First (pEroCacheList);
    pTeCHopInfo = (tTeCHopInfo *) TMO_SLL_First (pCHopList);

    while ((pEroCacheListInfo != NULL) && (pTeCHopInfo != NULL))
    {
        if ((pEroCacheListInfo->u1AddressType == pTeCHopInfo->u1AddressType) &&
            !(MEMCMP
              (pEroCacheListInfo->IpAddr.au1Ipv4Addr,
               pTeCHopInfo->IpAddr.au1Ipv4Addr, sizeof (UINT4)))
            && (pEroCacheListInfo->u1AddrPrefixLen ==
                pTeCHopInfo->u1AddrPrefixLen)
            && (pEroCacheListInfo->u4UnnumIf == pTeCHopInfo->u4UnnumIf)
            && (pEroCacheListInfo->u2AsNumber == pTeCHopInfo->u2AsNumber)
            && (pEroCacheListInfo->u4LocalLspId == pTeCHopInfo->u4LocalLspId)
            && (pEroCacheListInfo->u1CHopType == pTeCHopInfo->u1CHopType))
        {
            pEroCacheListInfo =
                (tTeCHopInfo *) TMO_SLL_Next (pEroCacheList,
                                              ((tTMO_SLL_NODE *) (VOID
                                                                  *)
                                               (pEroCacheListInfo)));
            pTeCHopInfo =
                (tTeCHopInfo *) TMO_SLL_Next (pCHopList,
                                              ((tTMO_SLL_NODE *) (VOID
                                                                  *)
                                               (pTeCHopInfo)));
        }
        else
        {
            return TE_NOT_EQUAL;
        }
    }
    return TE_EQUAL;
}

/*****************************************************************************/
/* Function Name : TeSigCreateReoptimizeTunnel
 * Description   : This routine create a new optimal LSP if available
 *               
 * Input(s)      : u4TunnelIndex    - Tunnel Index
 *                 u4TunnelIngressLSRId  - Tunnel Ingress LSR Id
 *                 u4TunnelEgressLSRId - Tunnel Egress LSR Id
 * Output(s)     : 
 * Return(s)     : TE_SUCCESS in case of successful tunnel creation
 *                 TE_FAILURE
 *****************************************************************************/

INT1
TeSigCreateReoptimizeTunnel (UINT4 u4TunnelIndex,
                             UINT4 u4TunnelIngressLSRId,
                             UINT4 u4TunnelEgressLSRId)
{
    tTeTnlInfo         *pIns0TeTnlInfo = NULL;
    tRpteEnqueueMsgInfo RpteEnqueueMsgInfo;
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MEMSET (&RpteEnqueueMsgInfo, 0, sizeof (tRpteEnqueueMsgInfo));

    MPLS_CMN_LOCK ();

    pIns0TeTnlInfo = TeGetTunnelInfo (u4TunnelIndex, TE_ZERO,
                                      u4TunnelIngressLSRId,
                                      u4TunnelEgressLSRId);
    if (pIns0TeTnlInfo == NULL)
    {
        return TE_FAILURE;
    }

    if ((TeCheckTrfcParamInTrfcParamTable
         (pIns0TeTnlInfo->u4ResourceIndex, &pTeTrfcParams) == TE_SUCCESS)
        && (TE_RSVPTE_TRFC_PARAMS (pTeTrfcParams) != NULL))
    {
        TE_RSVPTE_TOLDPARAM_PDR (pTeTrfcParams) =
            pIns0TeTnlInfo->u4CurrentResvBw;
    }
    MPLS_CMN_UNLOCK ();

    if (pIns0TeTnlInfo != NULL)
    {
        TE_TNL_REOPTIMIZE_TRIGGER (pIns0TeTnlInfo) = TE_TRUE;
        RpteEnqueueMsgInfo.u.pTeTnlInfo = pIns0TeTnlInfo;
        RpteEnqueueMsgInfo.u4RpteEvent = TE_TNL_UP;
        if (TeRpteProcessTeEvent (&RpteEnqueueMsgInfo) == TE_FAILURE)
        {
            return TE_FAILURE;
        }
    }
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeSigSendReoptTnlManualTriggerEvt
 * Description   : This routine send an event to RSVP-TE to trigger Manual
 *                 LSP Reoptimization.
 * Input(s)      : pIns0TeTnlInfo - Pointer to instance zero tunnel info
 * Output(s)     : 
 * Return(s)     : TE_SUCCESS in case of successful tunnel creation
 *                 TE_FAILURE
 *****************************************************************************/

INT1
TeSigSendReoptTnlManualTriggerEvt (tTeTnlInfo * pTeTnlInfo)
{
    UINT1               u1IsRpteAdminDown = TE_ZERO;

    if (pTeTnlInfo != NULL)
    {
        if (TeRpteTEEventHandler
            (TE_TNL_MANUAL_REOPTIMIZATION, pTeTnlInfo,
             &u1IsRpteAdminDown) == TE_FAILURE)
        {
            return TE_FAILURE;
        }
    }
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : IsTeSigReoptimizeTnlListEmpty
 * Description   : This routine check whether any tunnel is present or not,
 *                     which has reoptimization enabled.
 *               
 * Input(s)      : 
 * Output(s)     : 
 * Return(s)     : TRUE in case at lease one Reoptimization tunnel
 *                 FALSE
 *****************************************************************************/

UINT1
IsTeSigReoptimizeTnlListEmpty ()
{
    UINT4               u4ReoptimizeTunnelListCount = TE_ZERO;

    if (RBTreeCount (gTeGblInfo.ReoptimizeTnlList,
                     &u4ReoptimizeTunnelListCount) == 1)
    {
        return FALSE;
    }

    if (u4ReoptimizeTunnelListCount == TE_ZERO)
    {
        return FALSE;
    }

    return TRUE;
}

/*****************************************************************************/
/* Function Name : TeSigUpdatePrimaryInstance
 * Description   : This routine update the primary instance to MapTunnel 
 *                    Instance, because Reoptimization triggered tunnel got
 *                    the same path.
 *               
 * Input(s)      : u4TunnelIndex    - Tunnel Index
 *                    u4TunnelInstance - Tunnel Instance
 *                    u4TunnelIngressLSRId  - Tunnel Ingress LSR Id
 *                    u4TunnelEgressLSRId - Tunnel Egress LSR Id
 * Output(s)     : 
 * Return(s)     : 
 *****************************************************************************/

VOID
TeSigUpdatePrimaryInstance (UINT4 u4TunnelIndex,
                            UINT4 u4TunnelInstance,
                            UINT4 u4TunnelIngressLSRId,
                            UINT4 u4TunnelEgressLSRId)
{
    tTeTnlInfo         *pIns0TeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    pIns0TeTnlInfo = TeGetTunnelInfo (u4TunnelIndex, TE_ZERO,
                                      u4TunnelIngressLSRId,
                                      u4TunnelEgressLSRId);

    if (pIns0TeTnlInfo != NULL)
    {
        pIns0TeTnlInfo->u4TnlPrimaryInstance = u4TunnelInstance;
    }

    MPLS_CMN_UNLOCK ();

}

VOID
TeSigUpdateOriginalInstance (UINT4 u4TunnelIndex,
                             UINT4 u4TunnelInstance,
                             UINT4 u4TunnelIngressLSRId,
                             UINT4 u4TunnelEgressLSRId)
{
    tTeTnlInfo         *pIns0TeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    pIns0TeTnlInfo = TeGetTunnelInfo (u4TunnelIndex, TE_ZERO,
                                      u4TunnelIngressLSRId,
                                      u4TunnelEgressLSRId);

    if (pIns0TeTnlInfo != NULL)
    {
        pIns0TeTnlInfo->u4OrgTnlInstance = u4TunnelInstance;
    }

    MPLS_CMN_UNLOCK ();

}

VOID
TeSigUpdateReoptTriggerState (UINT4 u4TunnelIndex,
                              UINT4 u4TunnelInstance,
                              UINT4 u4TunnelIngressLSRId,
                              UINT4 u4TunnelEgressLSRId)
{
    tTeTnlInfo         *pIns0TeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    pIns0TeTnlInfo = TeGetTunnelInfo (u4TunnelIndex, u4TunnelInstance,
                                      u4TunnelIngressLSRId,
                                      u4TunnelEgressLSRId);

    if (pIns0TeTnlInfo != NULL)
    {
        TE_TNL_REOPTIMIZE_TRIGGER (pIns0TeTnlInfo) = FALSE;
    }

    MPLS_CMN_UNLOCK ();

}

INT1
TeSigReleaseTnlInfo (tTeTnlInfo * pTeTnlInfo)
{

    MPLS_CMN_LOCK ();
    if ((TE_REL_MEM_BLOCK (TE_TNL_POOL_ID,
                           (UINT1 *) (pTeTnlInfo))) == TE_MEM_FAILURE)
    {
        TE_DBG (TE_EXTN_FAIL,
                "EXTN : MemRelease Memblock failed " "for TnlInfo \n");

        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    return TE_SUCCESS;

}
