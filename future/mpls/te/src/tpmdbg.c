/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: tpmdbg.c,v 1.17 2015/01/07 12:30:50 siva Exp $
*
* Description: This file contains the routines for the protocol Database Access for the module Te 
*********************************************************************/

#include "teincs.h"
#include "mplslsr.h"
#include "mplsnp.h"
#include "oamext.h"
#include "mplscli.h"
#include "oamlwg.h"

/****************************************************************************
 Function    :  TeGetAllMplsTeP2mpTunnelTable
 Input       :  The Indices
                pTeGetMplsTeP2mpTunnelTable
 Output      :  This Routine Take the Indices &
                Gets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT4
TeGetAllMplsTeP2mpTunnelTable (tTeMplsTeP2mpTunnelTable *
                               pTeGetMplsTeP2mpTunnelTable)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeP2mpTnlInfo     *pTeP2mpTnlInfo = NULL;
    tP2mpBranchEntry   *pTeP2mpBranchEntry = NULL;

    tOutSegment        *pOutSegment = NULL;
    tXcEntry           *pXcEntry = NULL;
    UINT4               u4XcIndex = TE_ZERO;

    TE_DBG (TE_MAIN_ETEXT, "TeGetAllMplsTeP2mpTunnelTable : ENTRY \n");

    MPLS_CMN_LOCK ();
    /* Check whether the p2mp node is already present */
    if (TeCheckP2mpTableEntry
        (pTeGetMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelIndex,
         pTeGetMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelInstance,
         pTeGetMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelIngressLSRId,
         pTeGetMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelEgressLSRId,
         &pTeTnlInfo) == TE_FAILURE)
    {
        TE_DBG (TE_LLVL_GET_FAIL, "P2MP Tunnel row does not exist\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (NULL == TE_P2MP_TNL_INFO (pTeTnlInfo))
    {
        TE_DBG (TE_LLVL_GET_FAIL, "P2MP Tunnel "
                "does not exist in TE Tunnel Table \n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pTeP2mpTnlInfo = TE_P2MP_TNL_INFO (pTeTnlInfo);
    /* Assign values from the existing node */
    pTeGetMplsTeP2mpTunnelTable->MibObject.i4MplsTeP2mpTunnelP2mpIntegrity =
        TE_P2MP_TNL_INTEGRITY (pTeP2mpTnlInfo);

    pTeGetMplsTeP2mpTunnelTable->MibObject.i4MplsTeP2mpTunnelBranchRole =
        TE_P2MP_TNL_BRANCH_ROLE (pTeP2mpTnlInfo);

    pTeGetMplsTeP2mpTunnelTable->MibObject.i4MplsTeP2mpTunnelRowStatus =
        TE_TNL_ROW_STATUS (pTeTnlInfo);

    if (TMO_SLL_Count (&(TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo))) > TE_ZERO)
    {
        pTeP2mpBranchEntry =
            TMO_SLL_First (&(TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo)));
        if (NULL != pTeP2mpBranchEntry)
        {
            pOutSegment =
                MplsGetOutSegmentTableEntry (TE_P2MP_BRANCH_OUTSEG_INDEX
                                             (pTeP2mpBranchEntry));
            if ((NULL != pOutSegment)
                && (NULL != OUTSEGMENT_XC_INDEX (pOutSegment)))
            {
                pXcEntry = (tXcEntry *) (OUTSEGMENT_XC_INDEX (pOutSegment));
                u4XcIndex = TE_NTOHL (XC_INDEX (pXcEntry));
                MEMCPY (pTeGetMplsTeP2mpTunnelTable->MibObject.
                        au1MplsTeP2mpTunnelP2mpXcIndex, &u4XcIndex,
                        sizeof (UINT4));
            }
        }
    }
    pTeGetMplsTeP2mpTunnelTable->MibObject.i4MplsTeP2mpTunnelP2mpXcIndexLen =
        sizeof (UINT4);

    pTeGetMplsTeP2mpTunnelTable->MibObject.i4MplsTeP2mpTunnelStorageType =
        TE_TNL_STORAGE_TYPE (pTeTnlInfo);

    MPLS_CMN_UNLOCK ();

    TE_DBG (TE_MAIN_ETEXT, "TeGetAllMplsTeP2mpTunnelTable : EXIT \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TeTestAllMplsTeP2mpTunnelTable
 Input       :  The Indices
                pu4ErrorCode
                pTeMplsTeP2mpTunnelTable
                pTeIsSetMplsTeP2mpTunnelTable
 Output      :  This Routine Take the Indices &
                Test the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT4
TeTestAllMplsTeP2mpTunnelTable (UINT4 *pu4ErrorCode,
                                tTeMplsTeP2mpTunnelTable *
                                pTeSetMplsTeP2mpTunnelTable,
                                tTeIsSetMplsTeP2mpTunnelTable *
                                pTeIsSetMplsTeP2mpTunnelTable,
                                INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);

    TE_DBG (TE_MAIN_ETEXT, "TeTestAllMplsTeP2mpTunnelTable : ENTRY \n");

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* Check whether the p2mp node is already present */
    if (TeCheckP2mpTableEntry
        (pTeSetMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelIndex,
         pTeSetMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelInstance,
         pTeSetMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelIngressLSRId,
         pTeSetMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelEgressLSRId,
         &pTeTnlInfo) == TE_FAILURE)
    {
        TE_DBG (TE_LLVL_TEST_FAIL, "P2MP Tunnel "
                " does not exist in TE Tunnel Table \t\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_TNL_OWNER (pTeTnlInfo) != TE_TNL_OWNER_SNMP)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (pTeIsSetMplsTeP2mpTunnelTable->bMplsTeP2mpTunnelRowStatus != OSIX_FALSE)
    {
        switch (pTeSetMplsTeP2mpTunnelTable->MibObject.
                i4MplsTeP2mpTunnelRowStatus)
        {
            case TE_CREATEANDWAIT:
                if (TE_P2MP_TNL_INFO (pTeTnlInfo) != NULL)
                {
                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
                break;
            case TE_ACTIVE:
                if (TE_P2MP_TNL_INFO (pTeTnlInfo) == NULL)
                {
                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }

                if (TE_EGRESS != TE_TNL_ROLE (pTeTnlInfo))
                {
                    if (TE_ZERO == TMO_SLL_Count
                        (&(TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo))))
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        MPLS_CMN_UNLOCK ();
                        return SNMP_FAILURE;
                    }

                    if (TE_ZERO == TMO_SLL_Count
                        (&(TE_P2MP_TNL_DEST_LIST_INFO (pTeTnlInfo))))
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        MPLS_CMN_UNLOCK ();
                        return SNMP_FAILURE;
                    }
                }
                break;
            case TE_NOTINSERVICE:
                if (TE_P2MP_TNL_INFO (pTeTnlInfo) == NULL)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
                break;
            case TE_DESTROY:
                if (TE_P2MP_TNL_INFO (pTeTnlInfo) == NULL)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }

                if (TE_ZERO != TMO_SLL_Count
                    (&(TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo))))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }

                if (TE_ZERO != TMO_SLL_Count
                    (&(TE_P2MP_TNL_DEST_LIST_INFO (pTeTnlInfo))))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
                break;
            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
        }
    }
    if (pTeIsSetMplsTeP2mpTunnelTable->bMplsTeP2mpTunnelP2mpIntegrity !=
        OSIX_FALSE)
    {
        if ((TE_TNL_ROW_STATUS (pTeTnlInfo) == ACTIVE))
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }

        if (TE_P2MP_TNL_INFO (pTeTnlInfo) == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }
        switch (pTeSetMplsTeP2mpTunnelTable->MibObject.
                i4MplsTeP2mpTunnelP2mpIntegrity)
        {
            case TE_SNMP_FALSE:
            case TE_SNMP_TRUE:
                break;
            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
        }
    }
    if (pTeIsSetMplsTeP2mpTunnelTable->bMplsTeP2mpTunnelBranchRole !=
        OSIX_FALSE)
    {
        if ((TE_TNL_ROW_STATUS (pTeTnlInfo) == ACTIVE))
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }
        if (TE_P2MP_TNL_INFO (pTeTnlInfo) == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }
        switch (pTeSetMplsTeP2mpTunnelTable->MibObject.
                i4MplsTeP2mpTunnelBranchRole)
        {
            case TE_P2MP_NOT_BRANCH:
            case TE_P2MP_BRANCH:
                break;
            case TE_P2MP_BUD:
                if ((TE_TNL_ROLE (pTeTnlInfo) != TE_INGRESS) &&
                    (TE_TNL_ROLE (pTeTnlInfo) != TE_INTERMEDIATE))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
                break;
            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
        }
    }
    if (pTeIsSetMplsTeP2mpTunnelTable->bMplsTeP2mpTunnelStorageType !=
        OSIX_FALSE)
    {
        if (TE_P2MP_TNL_INFO (pTeTnlInfo) == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }
        switch (pTeSetMplsTeP2mpTunnelTable->MibObject.
                i4MplsTeP2mpTunnelStorageType)
        {
            case TE_STORAGE_VOLATILE:
                /* Intentional fallthrough */
            case TE_STORAGE_NONVOLATILE:
                break;
            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
        }
    }
    MPLS_CMN_UNLOCK ();
    TE_DBG (TE_MAIN_ETEXT, "TeTestAllMplsTeP2mpTunnelTable : EXIT \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TeSetAllMplsTeP2mpTunnelTable
 Input       :  The Indices
                pTeMplsTeP2mpTunnelTable
                pTeIsSetMplsTeP2mpTunnelTable
                i4RowStatusLogic
                i4RowCreateOption
 Output      :  This Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT4
TeSetAllMplsTeP2mpTunnelTable (tTeMplsTeP2mpTunnelTable *
                               pTeSetMplsTeP2mpTunnelTable,
                               tTeIsSetMplsTeP2mpTunnelTable *
                               pTeIsSetMplsTeP2mpTunnelTable,
                               INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    /* MPLS_P2MP_LSP_CHANGES - S */
    UINT4               u4MplsTunnelIndex = TE_ZERO;
    UINT4               u4MplsTunnelInstance = TE_ZERO;
    UINT4               u4MplsTunnelIngressLSRId = TE_ZERO;
    UINT4               u4MplsTunnelEgressLSRId = TE_ZERO;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeP2mpTnlInfo     *pTeP2mpTnlInfo = NULL;

    UINT1               u1TnlBranchRole = TE_P2MP_NOT_BRANCH;
    UINT1               u1Status = TE_OPER_DOWN;
    /* MPLS_P2MP_LSP_CHANGES - E */
    tTeMplsTeP2mpTunnelTable *pTeMplsTeP2mpTunnelTable = NULL;
    tTeMplsTeP2mpTunnelTable TeOldMplsTeP2mpTunnelTable;
    tTeMplsTeP2mpTunnelTable TeTrgMplsTeP2mpTunnelTable;
    tTeMplsTeP2mpTunnelTable TeTempMplsTeP2mpTunnelTable;
    tTeIsSetMplsTeP2mpTunnelTable TeTrgIsSetMplsTeP2mpTunnelTable;
    INT4                i4RowMakeActive = FALSE;

    TE_DBG (TE_MAIN_ETEXT, "TeSetAllMplsTeP2mpTunnelTable : ENTRY \n");

    MEMSET (&TeOldMplsTeP2mpTunnelTable, TE_ZERO,
            sizeof (tTeMplsTeP2mpTunnelTable));
    MEMSET (&TeTrgMplsTeP2mpTunnelTable, TE_ZERO,
            sizeof (tTeMplsTeP2mpTunnelTable));
    MEMSET (&TeTempMplsTeP2mpTunnelTable, TE_ZERO,
            sizeof (tTeMplsTeP2mpTunnelTable));
    MEMSET (&TeTrgIsSetMplsTeP2mpTunnelTable, TE_ZERO,
            sizeof (tTeIsSetMplsTeP2mpTunnelTable));

    u4MplsTunnelIndex = pTeSetMplsTeP2mpTunnelTable->MibObject.
        u4MplsTunnelIndex;
    u4MplsTunnelInstance = pTeSetMplsTeP2mpTunnelTable->MibObject.
        u4MplsTunnelInstance;
    u4MplsTunnelIngressLSRId = pTeSetMplsTeP2mpTunnelTable->MibObject.
        u4MplsTunnelIngressLSRId;
    u4MplsTunnelEgressLSRId = pTeSetMplsTeP2mpTunnelTable->MibObject.
        u4MplsTunnelEgressLSRId;
    pTeMplsTeP2mpTunnelTable = &TeTempMplsTeP2mpTunnelTable;

    MPLS_CMN_LOCK ();
    if (TeCheckP2mpTableEntry (u4MplsTunnelIndex, u4MplsTunnelInstance,
                               u4MplsTunnelIngressLSRId,
                               u4MplsTunnelEgressLSRId,
                               &pTeTnlInfo) != TE_SUCCESS)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* Check whether the node is already present */
    pTeP2mpTnlInfo = TE_P2MP_TNL_INFO (pTeTnlInfo);

    if (pTeP2mpTnlInfo == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pTeSetMplsTeP2mpTunnelTable->MibObject.
             i4MplsTeP2mpTunnelRowStatus == CREATE_AND_WAIT)
            || (pTeSetMplsTeP2mpTunnelTable->MibObject.
                i4MplsTeP2mpTunnelRowStatus == CREATE_AND_GO)
            ||
            ((pTeSetMplsTeP2mpTunnelTable->MibObject.
              i4MplsTeP2mpTunnelRowStatus == ACTIVE)
             && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pTeP2mpTnlInfo = (tTeP2mpTnlInfo *)
                TE_ALLOC_MEM_BLOCK (TE_TNL_P2MP_INFO_POOL_ID);
            if (pTeP2mpTnlInfo == NULL)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            if ((TeInitializeMplsTeP2mpTunnelTable (pTeP2mpTnlInfo))
                == SNMP_FAILURE)
            {
                TE_REL_MEM_BLOCK (TE_TNL_P2MP_INFO_POOL_ID,
                                  (UINT1 *) pTeP2mpTnlInfo);
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            /* Assign values for the new node */
            if (pTeIsSetMplsTeP2mpTunnelTable->bMplsTeP2mpTunnelP2mpIntegrity !=
                OSIX_FALSE)
            {
                TE_P2MP_TNL_INTEGRITY (pTeP2mpTnlInfo) =
                    (BOOL1) pTeSetMplsTeP2mpTunnelTable->MibObject.
                    i4MplsTeP2mpTunnelP2mpIntegrity;
                pTeMplsTeP2mpTunnelTable->MibObject.
                    i4MplsTeP2mpTunnelP2mpIntegrity =
                    pTeSetMplsTeP2mpTunnelTable->MibObject.
                    i4MplsTeP2mpTunnelP2mpIntegrity;
            }

            if (pTeIsSetMplsTeP2mpTunnelTable->bMplsTeP2mpTunnelBranchRole !=
                OSIX_FALSE)
            {
                TE_P2MP_TNL_BRANCH_ROLE (pTeP2mpTnlInfo) =
                    (UINT1) pTeSetMplsTeP2mpTunnelTable->MibObject.
                    i4MplsTeP2mpTunnelBranchRole;
                pTeMplsTeP2mpTunnelTable->MibObject.
                    i4MplsTeP2mpTunnelBranchRole =
                    pTeSetMplsTeP2mpTunnelTable->MibObject.
                    i4MplsTeP2mpTunnelBranchRole;
            }

            if (pTeIsSetMplsTeP2mpTunnelTable->bMplsTeP2mpTunnelRowStatus !=
                OSIX_FALSE)
            {
                TE_TNL_ROW_STATUS (pTeTnlInfo) = NOT_READY;
                pTeMplsTeP2mpTunnelTable->MibObject.
                    i4MplsTeP2mpTunnelRowStatus =
                    pTeSetMplsTeP2mpTunnelTable->MibObject.
                    i4MplsTeP2mpTunnelRowStatus;
            }

            if (pTeIsSetMplsTeP2mpTunnelTable->bMplsTeP2mpTunnelStorageType !=
                OSIX_FALSE)
            {
                TE_TNL_STORAGE_TYPE (pTeTnlInfo) =
                    (UINT1) pTeSetMplsTeP2mpTunnelTable->MibObject.
                    i4MplsTeP2mpTunnelStorageType;
                pTeMplsTeP2mpTunnelTable->MibObject.
                    i4MplsTeP2mpTunnelStorageType =
                    pTeSetMplsTeP2mpTunnelTable->MibObject.
                    i4MplsTeP2mpTunnelStorageType;
            }

            if (pTeIsSetMplsTeP2mpTunnelTable->bMplsTunnelIndex != OSIX_FALSE)
            {
                pTeMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelIndex =
                    pTeSetMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelIndex;
            }

            if (pTeIsSetMplsTeP2mpTunnelTable->bMplsTunnelInstance !=
                OSIX_FALSE)
            {
                pTeMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelInstance =
                    pTeSetMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelInstance;
            }

            if (pTeIsSetMplsTeP2mpTunnelTable->bMplsTunnelIngressLSRId !=
                OSIX_FALSE)
            {
                pTeMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelIngressLSRId =
                    pTeSetMplsTeP2mpTunnelTable->MibObject.
                    u4MplsTunnelIngressLSRId;
            }

            if (pTeIsSetMplsTeP2mpTunnelTable->bMplsTunnelEgressLSRId !=
                OSIX_FALSE)
            {
                pTeMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelEgressLSRId =
                    pTeSetMplsTeP2mpTunnelTable->MibObject.
                    u4MplsTunnelEgressLSRId;
            }

            if ((pTeSetMplsTeP2mpTunnelTable->MibObject.
                 i4MplsTeP2mpTunnelRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1) &&
                    (pTeSetMplsTeP2mpTunnelTable->MibObject.
                     i4MplsTeP2mpTunnelRowStatus == ACTIVE)))
            {
                pTeMplsTeP2mpTunnelTable->MibObject.
                    i4MplsTeP2mpTunnelRowStatus = ACTIVE;

                pTeSetMplsTeP2mpTunnelTable->MibObject.
                    i4MplsTeP2mpTunnelRowStatus = ACTIVE;
                /* For MSR and RM Trigger */
                TeTrgMplsTeP2mpTunnelTable.MibObject.
                    i4MplsTeP2mpTunnelRowStatus = CREATE_AND_WAIT;
                TeTrgIsSetMplsTeP2mpTunnelTable.bMplsTeP2mpTunnelRowStatus =
                    OSIX_TRUE;

                TeTrgMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIndex =
                    pTeSetMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelIndex;

                TeTrgMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelInstance
                    =
                    pTeSetMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelInstance;

                TeTrgMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIngressLSRId
                    =
                    pTeSetMplsTeP2mpTunnelTable->MibObject.
                    u4MplsTunnelIngressLSRId;

                TeTrgMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelEgressLSRId
                    =
                    pTeSetMplsTeP2mpTunnelTable->MibObject.
                    u4MplsTunnelEgressLSRId;

                if (TeSetAllMplsTeP2mpTunnelTableTrigger
                    (&TeTrgMplsTeP2mpTunnelTable,
                     &TeTrgIsSetMplsTeP2mpTunnelTable,
                     SNMP_SUCCESS) != SNMP_SUCCESS)
                {
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
            }
            else if (pTeSetMplsTeP2mpTunnelTable->MibObject.
                     i4MplsTeP2mpTunnelRowStatus == CREATE_AND_WAIT)
            {
                pTeMplsTeP2mpTunnelTable->MibObject.
                    i4MplsTeP2mpTunnelRowStatus = NOT_READY;
            }

            /* Add the new node to the database */
            TE_P2MP_TNL_INFO (pTeTnlInfo) = pTeP2mpTnlInfo;

            /* Initialize branch and destination SLL */
            TMO_SLL_Init (&(TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo)));
            TMO_SLL_Init (&(TE_P2MP_TNL_DEST_LIST_INFO (pTeTnlInfo)));

            if (TeUtilUpdateMplsTeP2mpTunnelTable
                (NULL, pTeMplsTeP2mpTunnelTable) != SNMP_SUCCESS)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            if (TeSetAllMplsTeP2mpTunnelTableTrigger
                (pTeSetMplsTeP2mpTunnelTable, pTeIsSetMplsTeP2mpTunnelTable,
                 SNMP_SUCCESS) != SNMP_SUCCESS)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;

        }
        else
        {
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else if ((pTeSetMplsTeP2mpTunnelTable->MibObject.
              i4MplsTeP2mpTunnelRowStatus == CREATE_AND_WAIT)
             || (pTeSetMplsTeP2mpTunnelTable->MibObject.
                 i4MplsTeP2mpTunnelRowStatus == CREATE_AND_GO))
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (&TeOldMplsTeP2mpTunnelTable, pTeMplsTeP2mpTunnelTable,
            sizeof (tTeMplsTeP2mpTunnelTable));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pTeSetMplsTeP2mpTunnelTable->MibObject.i4MplsTeP2mpTunnelRowStatus ==
        DESTROY)
    {
        pTeMplsTeP2mpTunnelTable->MibObject.i4MplsTeP2mpTunnelRowStatus =
            DESTROY;

        if (TeUtilUpdateMplsTeP2mpTunnelTable (&TeOldMplsTeP2mpTunnelTable,
                                               pTeMplsTeP2mpTunnelTable) !=
            SNMP_SUCCESS)
        {
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }

        if (TE_ZERO !=
            TMO_SLL_Count (&(TE_P2MP_TNL_DEST_LIST_INFO (pTeTnlInfo))))
        {
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }
        if (TE_TNL_ADMIN_STATUS (pTeTnlInfo) == TE_ADMIN_UP)
        {
            if (TeP2mpExtSetOperStatusAndProgHw (pTeTnlInfo,
                                                 TE_OPER_DOWN) == TE_FAILURE)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
        }
        TE_REL_MEM_BLOCK (TE_TNL_P2MP_INFO_POOL_ID, (UINT1 *) pTeP2mpTnlInfo);
        TE_P2MP_TNL_INFO (pTeTnlInfo) = NULL;
        if (TE_ACTIVE == TE_TNL_ROW_STATUS (pTeTnlInfo))
        {
            TE_P2MP_CONFIGURED_TNLS (gTeGblInfo) -= TE_ONE;
        }
        if (TeSetAllMplsTeP2mpTunnelTableTrigger (pTeSetMplsTeP2mpTunnelTable,
                                                  pTeIsSetMplsTeP2mpTunnelTable,
                                                  SNMP_SUCCESS) != SNMP_SUCCESS)
        {
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (MplsTeP2mpTunnelTableFilterInputs
        (pTeMplsTeP2mpTunnelTable, pTeSetMplsTeP2mpTunnelTable,
         pTeIsSetMplsTeP2mpTunnelTable) == OSIX_FALSE)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pTeMplsTeP2mpTunnelTable->MibObject.i4MplsTeP2mpTunnelRowStatus ==
         ACTIVE))
    {
        pTeMplsTeP2mpTunnelTable->MibObject.i4MplsTeP2mpTunnelRowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        TeTrgMplsTeP2mpTunnelTable.MibObject.i4MplsTeP2mpTunnelRowStatus =
            NOT_IN_SERVICE;
        TeTrgIsSetMplsTeP2mpTunnelTable.bMplsTeP2mpTunnelRowStatus = OSIX_TRUE;

        TeTrgMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIndex =
            pTeSetMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelIndex;

        TeTrgMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelInstance
            = pTeSetMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelInstance;

        TeTrgMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIngressLSRId
            = pTeSetMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelIngressLSRId;

        TeTrgMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelEgressLSRId
            = pTeSetMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelEgressLSRId;

        if (TeUtilUpdateMplsTeP2mpTunnelTable (&TeOldMplsTeP2mpTunnelTable,
                                               pTeMplsTeP2mpTunnelTable) !=
            SNMP_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pTeMplsTeP2mpTunnelTable, &TeOldMplsTeP2mpTunnelTable,
                    sizeof (tTeMplsTeP2mpTunnelTable));
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }
        if (TeSetAllMplsTeP2mpTunnelTableTrigger (&TeTrgMplsTeP2mpTunnelTable,
                                                  &TeTrgIsSetMplsTeP2mpTunnelTable,
                                                  SNMP_SUCCESS) != SNMP_SUCCESS)
        {
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    if (pTeSetMplsTeP2mpTunnelTable->MibObject.i4MplsTeP2mpTunnelRowStatus ==
        ACTIVE)
    {
        if (TE_ACTIVE != TE_TNL_ROW_STATUS (pTeTnlInfo))
        {
            TE_P2MP_CONFIGURED_TNLS (gTeGblInfo) += TE_ONE;
            if (TE_TNL_ADMIN_STATUS (pTeTnlInfo) == TE_ADMIN_UP)
            {
                if (TeP2mpExtSetOperStatusAndProgHw (pTeTnlInfo,
                                                     TE_OPER_UP) == TE_FAILURE)
                {
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
            }
        }
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pTeIsSetMplsTeP2mpTunnelTable->bMplsTeP2mpTunnelP2mpIntegrity !=
        OSIX_FALSE)
    {
        TE_P2MP_TNL_INTEGRITY (pTeP2mpTnlInfo) =
            (BOOL1) pTeSetMplsTeP2mpTunnelTable->MibObject.
            i4MplsTeP2mpTunnelP2mpIntegrity;
        pTeMplsTeP2mpTunnelTable->MibObject.i4MplsTeP2mpTunnelP2mpIntegrity =
            pTeSetMplsTeP2mpTunnelTable->MibObject.
            i4MplsTeP2mpTunnelP2mpIntegrity;
    }
    if (pTeIsSetMplsTeP2mpTunnelTable->bMplsTeP2mpTunnelBranchRole !=
        OSIX_FALSE)
    {
        u1TnlBranchRole =
            (UINT1) pTeSetMplsTeP2mpTunnelTable->MibObject.
            i4MplsTeP2mpTunnelBranchRole;
        if ((TE_P2MP_TNL_BRANCH_ROLE (pTeP2mpTnlInfo) != u1TnlBranchRole) &&
            (TE_TNL_ADMIN_STATUS (pTeTnlInfo) == TE_ADMIN_UP))
        {
            /*Configure P2MP bud node in HW */
            if (u1TnlBranchRole == TE_P2MP_BUD)
            {
                u1Status = TE_OPER_UP;
            }

            if (MplsP2mpUpdateBudNode (pTeTnlInfo, u1Status) == MPLS_FAILURE)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
        }

        TE_P2MP_TNL_BRANCH_ROLE (pTeP2mpTnlInfo) =
            (UINT1) pTeSetMplsTeP2mpTunnelTable->MibObject.
            i4MplsTeP2mpTunnelBranchRole;
        pTeMplsTeP2mpTunnelTable->MibObject.i4MplsTeP2mpTunnelBranchRole =
            pTeSetMplsTeP2mpTunnelTable->MibObject.i4MplsTeP2mpTunnelBranchRole;
    }
    if (pTeIsSetMplsTeP2mpTunnelTable->bMplsTeP2mpTunnelRowStatus != OSIX_FALSE)
    {
        if ((TE_ACTIVE == TE_TNL_ROW_STATUS (pTeTnlInfo))
            && (NOT_IN_SERVICE == pTeSetMplsTeP2mpTunnelTable->MibObject.
                i4MplsTeP2mpTunnelRowStatus))
        {
            if (TE_TNL_ADMIN_STATUS (pTeTnlInfo) == TE_ADMIN_UP)
            {
                if (TeP2mpExtSetOperStatusAndProgHw (pTeTnlInfo,
                                                     TE_OPER_DOWN) ==
                    TE_FAILURE)
                {
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
            }
            TE_P2MP_CONFIGURED_TNLS (gTeGblInfo) -= TE_ONE;
        }
        TE_TNL_ROW_STATUS (pTeTnlInfo) =
            (UINT1) pTeSetMplsTeP2mpTunnelTable->MibObject.
            i4MplsTeP2mpTunnelRowStatus;
        pTeMplsTeP2mpTunnelTable->MibObject.i4MplsTeP2mpTunnelRowStatus =
            pTeSetMplsTeP2mpTunnelTable->MibObject.i4MplsTeP2mpTunnelRowStatus;
    }

    if (pTeIsSetMplsTeP2mpTunnelTable->bMplsTeP2mpTunnelStorageType !=
        OSIX_FALSE)
    {
        TE_TNL_STORAGE_TYPE (pTeTnlInfo) =
            (UINT1) pTeSetMplsTeP2mpTunnelTable->MibObject.
            i4MplsTeP2mpTunnelStorageType;
        pTeMplsTeP2mpTunnelTable->MibObject.i4MplsTeP2mpTunnelStorageType =
            pTeSetMplsTeP2mpTunnelTable->MibObject.
            i4MplsTeP2mpTunnelStorageType;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pTeMplsTeP2mpTunnelTable->MibObject.i4MplsTeP2mpTunnelRowStatus =
            ACTIVE;
    }
    if (TeUtilUpdateMplsTeP2mpTunnelTable (&TeOldMplsTeP2mpTunnelTable,
                                           pTeMplsTeP2mpTunnelTable) !=
        SNMP_SUCCESS)
    {
        /*Restore back with previous values */
        MEMCPY (pTeMplsTeP2mpTunnelTable, &TeOldMplsTeP2mpTunnelTable,
                sizeof (tTeMplsTeP2mpTunnelTable));
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TeSetAllMplsTeP2mpTunnelTableTrigger (pTeSetMplsTeP2mpTunnelTable,
                                              pTeIsSetMplsTeP2mpTunnelTable,
                                              SNMP_SUCCESS) != SNMP_SUCCESS)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    TE_DBG (TE_MAIN_ETEXT, "TeSetAllMplsTeP2mpTunnelTable : EXIT \n");
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  TeGetAllMplsTeP2mpTunnelDestTable
 Input       :  The Indices
                pTeGetMplsTeP2mpTunnelDestTable
 Output      :  This Routine Take the Indices &
                Gets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT4
TeGetAllMplsTeP2mpTunnelDestTable (tTeMplsTeP2mpTunnelDestTable *
                                   pTeGetMplsTeP2mpTunnelDestTable)
{
    tP2mpDestEntry     *pTeP2mpDestEntry = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4DestId = TE_ZERO;
    UINT4               u4OutSegIndex = TE_ZERO;
    UINT4               u4DestIdInList = TE_ZERO;

    TE_DBG (TE_MAIN_ETEXT, "TeGetAllMplsTeP2mpTunnelDestTable : ENTRY \n");

    MPLS_CMN_LOCK ();
    /* Check whether the node is already present */
    if (TeCheckP2mpTableEntry
        (pTeGetMplsTeP2mpTunnelDestTable->MibObject.u4MplsTunnelIndex,
         pTeGetMplsTeP2mpTunnelDestTable->MibObject.u4MplsTunnelInstance,
         pTeGetMplsTeP2mpTunnelDestTable->MibObject.u4MplsTunnelIngressLSRId,
         pTeGetMplsTeP2mpTunnelDestTable->MibObject.u4MplsTunnelEgressLSRId,
         &pTeTnlInfo) == TE_FAILURE)
    {
        TE_DBG (TE_LLVL_GET_FAIL, "P2MP Tunnel "
                "does not exist in TE Tunnel Table \t\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (NULL == TE_P2MP_TNL_INFO (pTeTnlInfo))
    {
        TE_DBG (TE_LLVL_GET_FAIL, "P2MP Tunnel row does not exist\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* Assign values from the existing node */
    MEMCPY (&u4DestId, pTeGetMplsTeP2mpTunnelDestTable->MibObject.
            au1MplsTeP2mpTunnelDestDestination, sizeof (UINT4));
    TMO_SLL_Scan (&(TE_P2MP_TNL_DEST_LIST_INFO (pTeTnlInfo)),
                  pTeP2mpDestEntry, tP2mpDestEntry *)
    {
        CONVERT_TO_INTEGER ((TE_P2MP_DEST_ADDRESS (pTeP2mpDestEntry)),
                            u4DestIdInList);
        if (u4DestId == TE_NTOHL (u4DestIdInList))
        {
            u4OutSegIndex =
                TE_NTOHL (TE_P2MP_DEST_BRANCH_OUTSEG_INDEX (pTeP2mpDestEntry));
            MEMCPY (pTeGetMplsTeP2mpTunnelDestTable->MibObject.
                    au1MplsTeP2mpTunnelDestBranchOutSegment, &u4OutSegIndex,
                    sizeof (UINT4));
            pTeGetMplsTeP2mpTunnelDestTable->MibObject.
                i4MplsTeP2mpTunnelDestBranchOutSegmentLen = sizeof (UINT4);

            pTeGetMplsTeP2mpTunnelDestTable->MibObject.
                u4MplsTeP2mpTunnelDestHopTableIndex =
                TE_P2MP_DEST_HOP_TABLE_INDEX (pTeP2mpDestEntry);

            pTeGetMplsTeP2mpTunnelDestTable->MibObject.
                u4MplsTeP2mpTunnelDestPathInUse =
                TE_P2MP_DEST_PATH_IN_USE (pTeP2mpDestEntry);

            pTeGetMplsTeP2mpTunnelDestTable->MibObject.
                u4MplsTeP2mpTunnelDestCHopTableIndex =
                TE_P2MP_DEST_CHOP_TABLE_INDEX (pTeP2mpDestEntry);

            pTeGetMplsTeP2mpTunnelDestTable->MibObject.
                u4MplsTeP2mpTunnelDestARHopTableIndex =
                TE_P2MP_DEST_ARHOP_TABLE_INDEX (pTeP2mpDestEntry);

            pTeGetMplsTeP2mpTunnelDestTable->MibObject.
                u4MplsTeP2mpTunnelDestTotalUpTime =
                TE_P2MP_DEST_TOTAL_UP_TIME (pTeP2mpDestEntry);

            pTeGetMplsTeP2mpTunnelDestTable->MibObject.
                u4MplsTeP2mpTunnelDestInstanceUpTime =
                TE_P2MP_DEST_INSTANCE_UP_TIME (pTeP2mpDestEntry);

            pTeGetMplsTeP2mpTunnelDestTable->MibObject.
                u4MplsTeP2mpTunnelDestPathChanges =
                TE_P2MP_DEST_PATH_CHG (pTeP2mpDestEntry);

            pTeGetMplsTeP2mpTunnelDestTable->MibObject.
                u4MplsTeP2mpTunnelDestLastPathChange =
                TE_P2MP_DEST_LAST_PATH_CHG (pTeP2mpDestEntry);

            pTeGetMplsTeP2mpTunnelDestTable->MibObject.
                u4MplsTeP2mpTunnelDestCreationTime =
                TE_P2MP_DEST_CREATE_TIME (pTeP2mpDestEntry);

            pTeGetMplsTeP2mpTunnelDestTable->MibObject.
                u4MplsTeP2mpTunnelDestStateTransitions =
                TE_P2MP_DEST_STATE_CHG_COUNT (pTeP2mpDestEntry);

            pTeGetMplsTeP2mpTunnelDestTable->MibObject.
                u4MplsTeP2mpTunnelDestDiscontinuityTime =
                TE_P2MP_DEST_DISCONTINUITY_TIME (pTeP2mpDestEntry);

            pTeGetMplsTeP2mpTunnelDestTable->MibObject.
                i4MplsTeP2mpTunnelDestAdminStatus =
                TE_P2MP_DEST_ADMIN_STATUS (pTeP2mpDestEntry);

            pTeGetMplsTeP2mpTunnelDestTable->MibObject.
                i4MplsTeP2mpTunnelDestOperStatus =
                TE_P2MP_DEST_OPER_STATUS (pTeP2mpDestEntry);

            pTeGetMplsTeP2mpTunnelDestTable->MibObject.
                i4MplsTeP2mpTunnelDestRowStatus =
                TE_P2MP_DEST_ROW_STATUS (pTeP2mpDestEntry);

            pTeGetMplsTeP2mpTunnelDestTable->MibObject.
                i4MplsTeP2mpTunnelDestStorageType =
                TE_P2MP_DEST_STORAGE_TYPE (pTeP2mpDestEntry);
            MPLS_CMN_UNLOCK ();
            TE_DBG (TE_MAIN_ETEXT, "TeGetAllMplsTeP2mpTunnelDestTable : "
                    "EXIT \n");
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    TE_DBG (TE_LLVL_GET_FAIL, "P2MP destination "
            "does not exist in TE dest Table \t\n");
    TE_DBG (TE_MAIN_ETEXT, "TeGetAllMplsTeP2mpTunnelDestTable : EXIT \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  TeTestAllMplsTeP2mpTunnelDestTable
 Input       :  The Indices
                pu4ErrorCode
                pTeMplsTeP2mpTunnelDestTable
                pTeIsSetMplsTeP2mpTunnelDestTable
 Output      :  This Routine Take the Indices &
                Test the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT4
TeTestAllMplsTeP2mpTunnelDestTable (UINT4 *pu4ErrorCode,
                                    tTeMplsTeP2mpTunnelDestTable *
                                    pTeSetMplsTeP2mpTunnelDestTable,
                                    tTeIsSetMplsTeP2mpTunnelDestTable *
                                    pTeIsSetMplsTeP2mpTunnelDestTable,
                                    INT4 i4RowStatusLogic,
                                    INT4 i4RowCreateOption)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tP2mpDestEntry     *pTeP2mpDestEntry = NULL;
    tInSegment         *pInSegment = NULL;
    tOutSegment        *pOutSegment = NULL;
    tXcEntry           *pXcEntry = NULL;

    UINT4               u4ContextId = MPLS_DEF_CONTEXT_ID;
    UINT4               u4GlobalId = TE_ZERO;
    UINT4               u4NodeId = TE_ZERO;
    UINT4               u4LocalMapNum = TE_ZERO;

    UINT4               u4OutIndex = TE_ZERO;
    UINT4               u4DestId = TE_ZERO;
    UINT4               u4DestIdInList = TE_ZERO;
    BOOL1               bIsFoundDestEntry = FALSE;

    TE_DBG (TE_MAIN_ETEXT, "TeTestAllMplsTeP2mpTunnelDestTable : ENTRY \n");

    MPLS_CMN_LOCK ();

    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TeCheckP2mpTableEntry
        (pTeSetMplsTeP2mpTunnelDestTable->MibObject.u4MplsTunnelIndex,
         pTeSetMplsTeP2mpTunnelDestTable->MibObject.u4MplsTunnelInstance,
         pTeSetMplsTeP2mpTunnelDestTable->MibObject.u4MplsTunnelIngressLSRId,
         pTeSetMplsTeP2mpTunnelDestTable->MibObject.u4MplsTunnelEgressLSRId,
         &pTeTnlInfo) == TE_FAILURE)
    {
        TE_DBG (TE_LLVL_TEST_FAIL, "P2MP Tunnel "
                "does not exist in TE Tunnel Table \t\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_TNL_OWNER (pTeTnlInfo) != TE_TNL_OWNER_SNMP)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_TNL_ROLE (pTeTnlInfo) == TE_EGRESS)
    {
        TE_DBG (TE_LLVL_TEST_FAIL, "Destination configuration is not "
                "applicable at P2MP egress LSR\r\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_P2MP_TNL_INFO (pTeTnlInfo) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TMO_SLL_Count (&(TE_P2MP_TNL_DEST_LIST_INFO (pTeTnlInfo)))
        >= MAX_DEST_PER_P2MP_LSP)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MEMCPY (&u4DestId, pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            au1MplsTeP2mpTunnelDestDestination,
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestDestinationLen);
    u4DestId = TE_NTOHL (u4DestId);
    TMO_SLL_Scan (&(TE_P2MP_TNL_DEST_LIST_INFO (pTeTnlInfo)),
                  pTeP2mpDestEntry, tP2mpDestEntry *)
    {
        /* Check if input Dest ID is present in P2MP Dest list */
        CONVERT_TO_INTEGER ((TE_P2MP_DEST_ADDRESS (pTeP2mpDestEntry)),
                            u4DestIdInList);
        if (u4DestId == u4DestIdInList)
        {
            bIsFoundDestEntry = TRUE;
            break;
        }
    }

    if (pTeIsSetMplsTeP2mpTunnelDestTable->
        bMplsTeP2mpTunnelDestBranchOutSegment != OSIX_FALSE)
    {
        /* Scan the tunnel branch table. If the out-segment is present, 
         * return failure */
        if (FALSE == bIsFoundDestEntry)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }

        if (TE_ACTIVE == TE_P2MP_DEST_ROW_STATUS (pTeP2mpDestEntry))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }

        MEMCPY (&u4OutIndex, pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                au1MplsTeP2mpTunnelDestBranchOutSegment,
                pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                i4MplsTeP2mpTunnelDestBranchOutSegmentLen);
        u4OutIndex = TE_NTOHL (u4OutIndex);
        pOutSegment = MplsGetOutSegmentTableEntry (u4OutIndex);

        /* Out-segment entry should exist */
        if (NULL == pOutSegment)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }

        /* Out-segment should not be configured in reverse direction */
        if (MPLS_DIRECTION_REVERSE == OUTSEGMENT_DIRECTION (pOutSegment))
        {
            TE_DBG (TE_LLVL_TEST_FAIL, "Reverse out-segment association "
                    "to P2MP destination is not permitted.\r\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }

        /* XC entry corresponding to out-segment should exist */
        pXcEntry = (tXcEntry *) OUTSEGMENT_XC_INDEX (pOutSegment);
        if (NULL == pXcEntry)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }

        /* In-segment should not be configured in reverse direction */
        pInSegment = XC_ININDEX (pXcEntry);
        if ((NULL != pInSegment)
            && (MPLS_DIRECTION_REVERSE == INSEGMENT_DIRECTION (pInSegment)))
        {
            TE_DBG (TE_LLVL_TEST_FAIL, "Reverse in-segment association "
                    "to P2MP destination is not permitted.\r\n");
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }

        /* Check if tunnel pointer is associated to current tunnel */
        if ((XC_TNL_TBL_PTR (pXcEntry) != NULL) &&
            (XC_TNL_TBL_PTR (pXcEntry) != pTeTnlInfo))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    if (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTeP2mpTunnelDestHopTableIndex !=
        OSIX_FALSE)
    {
        if (FALSE == bIsFoundDestEntry)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }

        if (TE_ACTIVE == TE_P2MP_DEST_ROW_STATUS (pTeP2mpDestEntry))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }

        if (((pTeSetMplsTeP2mpTunnelDestTable->MibObject.
              u4MplsTeP2mpTunnelDestCHopTableIndex) > TE_TNL_HOPLSTINDEX_MAXVAL)
            || ((pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                 u4MplsTeP2mpTunnelDestCHopTableIndex) <
                TE_TNL_HOPLSTINDEX_MINVAL))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }

        if ((pTeSetMplsTeP2mpTunnelDestTable->MibObject.
             u4MplsTeP2mpTunnelDestCHopTableIndex) >
            (TE_MAX_HOP_LIST (gTeGblInfo) / TE_TWO))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    if (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTeP2mpTunnelDestPathInUse !=
        OSIX_FALSE)
    {
        if (FALSE == bIsFoundDestEntry)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }

        if (TE_ACTIVE == TE_P2MP_DEST_ROW_STATUS (pTeP2mpDestEntry))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }

        if ((pTeSetMplsTeP2mpTunnelDestTable->MibObject.
             u4MplsTeP2mpTunnelDestPathInUse) > TE_UINT2_SIZE)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    if (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTeP2mpTunnelDestAdminStatus !=
        OSIX_FALSE)
    {
        if (FALSE == bIsFoundDestEntry)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }

        if (TE_ACTIVE != TE_P2MP_DEST_ROW_STATUS (pTeP2mpDestEntry))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }

        switch (pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                i4MplsTeP2mpTunnelDestAdminStatus)
        {
            case TE_ADMIN_UP:
            case TE_ADMIN_DOWN:
            case TE_ADMIN_TESTING:
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                break;
        }
    }

    if (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTeP2mpTunnelDestRowStatus !=
        OSIX_FALSE)
    {
        switch (pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                i4MplsTeP2mpTunnelDestRowStatus)
        {
            case TE_CREATEANDWAIT:
                if (TRUE == bIsFoundDestEntry)
                {
                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                    CLI_SET_ERR (MPLS_CLI_TE_ERR_P2MP_DEST_EXISTS);
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
                /* Check if P2MP destination type is same as ingress LSR 
                 * type (IP address or local map number) */
                if (pTeTnlInfo->u4TnlLsrIdMapInfo & TE_TNL_INGRESSID_MAP_INFO)
                {
                    /* Ingress LSR is configured as local map number. Check 
                     * if P2MP destination local map number is configured in 
                     * the node map table */
                    if (OamUtilGetGlobalIdNodeId
                        (u4ContextId, u4DestId, &u4GlobalId,
                         &u4NodeId) != OSIX_SUCCESS)
                    {
                        TE_DBG (TE_LLVL_TEST_FAIL,
                                "P2MP destination local map number is not "
                                "configured in node map table\t\n");
                        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                        CLI_SET_ERR (MPLS_CLI_TE_ERR_MAP_NUM_NOT_EXIST);
                        MPLS_CMN_UNLOCK ();
                        return SNMP_FAILURE;
                    }

                    /* Fetch Global ID and Node ID of local LSR */
                    if (nmhGetFsMplsTpGlobalId (u4ContextId, &u4GlobalId) ==
                        SNMP_FAILURE)
                    {
                        TE_DBG (TE_LLVL_TEST_FAIL,
                                "Unable to fetch Global ID of local LSR\t\n");
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        MPLS_CMN_UNLOCK ();
                        return SNMP_FAILURE;
                    }
                    if (nmhGetFsMplsTpNodeIdentifier (u4ContextId, &u4NodeId) ==
                        SNMP_FAILURE)
                    {
                        TE_DBG (TE_LLVL_TEST_FAIL,
                                "Unable to fetch Node ID of local LSR\t\n");
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        MPLS_CMN_UNLOCK ();
                        return SNMP_FAILURE;
                    }

                    /* Block addition of local map number as P2MP destination */
                    if (OamUtilGetLocalMapNum
                        (u4ContextId, u4GlobalId, u4NodeId,
                         &u4LocalMapNum) == OSIX_SUCCESS)
                    {
                        if (u4LocalMapNum == u4DestId)
                        {
                            TE_DBG (TE_LLVL_TEST_FAIL,
                                    "Local map number should not be configured "
                                    "as P2MP destination address \t\n");
                            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                            CLI_SET_ERR
                                (MPLS_CLI_TE_ERR_P2MP_DEST_LOCAL_MAP_NUM);
                            MPLS_CMN_UNLOCK ();
                            return SNMP_FAILURE;
                        }
                    }
                    else
                    {
                        TE_DBG (TE_LLVL_TEST_FAIL,
                                "Unable to fetch local map number of this node "
                                "from Global ID and Node ID\t\n");
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        CLI_SET_ERR (MPLS_CLI_TE_ERR_LOCAL_MAP_NUM_NOT_CONF);
                        MPLS_CMN_UNLOCK ();
                        return SNMP_FAILURE;
                    }
                }
                else
                {
                    /* Ingress LSR is configured as IP address. Check if valid 
                     * IP address is configured for P2MP destination */
                    if (!
                        ((IP_IS_ADDR_CLASS_A (u4DestId)
                          ? (IP_IS_ZERO_NETWORK (u4DestId) ? TE_ZERO : TE_ONE) :
                          TE_ZERO) || (IP_IS_ADDR_CLASS_B (u4DestId))
                         || (IP_IS_ADDR_CLASS_C (u4DestId))))
                    {
                        TE_DBG (TE_LLVL_TEST_FAIL,
                                "Invalid P2MP destination IP address\t\n");
                        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                        CLI_SET_ERR (MPLS_CLI_TE_ERR_WRONG_SUBNET);
                        MPLS_CMN_UNLOCK ();
                        return SNMP_FAILURE;
                    }

                    /* Block addition of P2MP destination address if same IP 
                     * address has been assigned to an interface */
                    if (NetIpv4IfIsOurAddress (u4DestId) == NETIPV4_SUCCESS)
                    {
                        TE_DBG (TE_LLVL_TEST_FAIL,
                                "IP address of local interface is configured "
                                "as P2MP destination address\t\n");
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        CLI_SET_ERR (MPLS_CLI_TE_ERR_LOCAL_INTF_ADDR);
                        MPLS_CMN_UNLOCK ();
                        return SNMP_FAILURE;
                    }
                }
                break;
            case TE_ACTIVE:
                if (FALSE == bIsFoundDestEntry)
                {
                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }

                if (TE_ZERO == TE_P2MP_DEST_BRANCH_OUTSEG_INDEX
                    (pTeP2mpDestEntry))
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
                break;
            case TE_NOTINSERVICE:
                if (FALSE == bIsFoundDestEntry)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
                if (TE_ACTIVE != TE_P2MP_DEST_ROW_STATUS (pTeP2mpDestEntry))
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
                break;
            case TE_DESTROY:
                if (FALSE == bIsFoundDestEntry)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
                break;
            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
        }
    }

    if (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTeP2mpTunnelDestStorageType !=
        OSIX_FALSE)
    {
        if (FALSE == bIsFoundDestEntry)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }

        switch (pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                i4MplsTeP2mpTunnelDestStorageType)
        {
            case TE_STORAGE_VOLATILE:
                /* Intentional fallthrough */
            case TE_STORAGE_NONVOLATILE:
                break;
            default:
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
        }
    }

    UNUSED_PARAM (i4RowStatusLogic);
    UNUSED_PARAM (i4RowCreateOption);
    MPLS_CMN_UNLOCK ();

    TE_DBG (TE_MAIN_ETEXT, "TeTestAllMplsTeP2mpTunnelDestTable : EXIT \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TeSetAllMplsTeP2mpTunnelDestTable
 Input       :  The Indices
                pTeMplsTeP2mpTunnelDestTable
                pTeIsSetMplsTeP2mpTunnelDestTable
                i4RowStatusLogic
                i4RowCreateOption
 Output      :  This Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT4
TeSetAllMplsTeP2mpTunnelDestTable (tTeMplsTeP2mpTunnelDestTable *
                                   pTeSetMplsTeP2mpTunnelDestTable,
                                   tTeIsSetMplsTeP2mpTunnelDestTable *
                                   pTeIsSetMplsTeP2mpTunnelDestTable,
                                   INT4 i4RowStatusLogic,
                                   INT4 i4RowCreateOption)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tP2mpBranchEntry   *pTeP2mpBranchEntry = NULL;
    tP2mpBranchEntry   *pTeP2mpNextBranchEntry = NULL;
    tP2mpDestEntry     *pTeP2mpDestEntry = NULL;
    tOutSegment        *pOutSegment = NULL;
    tXcEntry           *pXcEntry = NULL;

    tTeMplsTeP2mpTunnelDestTable *pTeMplsTeP2mpTunnelDestTable = NULL;
    tTeMplsTeP2mpTunnelDestTable *pTeOldMplsTeP2mpTunnelDestTable = NULL;
    tTeMplsTeP2mpTunnelDestTable TeTrgMplsTeP2mpTunnelDestTable;
    tTeIsSetMplsTeP2mpTunnelDestTable TeTrgIsSetMplsTeP2mpTunnelDestTable;

    INT4                i4RowMakeActive = FALSE;
    UINT4               u4DestSrcSubGrpOriginId = TE_ZERO;
    UINT4               u4DestSubGrpOriginId = TE_ZERO;
    UINT4               u4DestId = TE_ZERO;
    UINT4               u4DestIdInList = TE_ZERO;
    UINT4               u4OutSegIndex = TE_ZERO;

    UINT1               u1PrevP2mpDestOperStatus = TE_ZERO;

    BOOL1               bIsFoundBranchEntry = FALSE;
    BOOL1               bIsFoundDestEntry = FALSE;
    BOOL1               bIsFoundNewBranchEntry = FALSE;

    TE_DBG (TE_MAIN_ETEXT, "TeSetAllMplsTeP2mpTunnelDestTable : ENTRY \n");

    MEMSET (&TeTrgMplsTeP2mpTunnelDestTable, TE_ZERO,
            sizeof (tTeMplsTeP2mpTunnelDestTable));
    MEMSET (&TeTrgIsSetMplsTeP2mpTunnelDestTable, TE_ZERO,
            sizeof (tTeIsSetMplsTeP2mpTunnelDestTable));

    MPLS_CMN_LOCK ();
    /* Check whether the node is already present */
    if (TeCheckP2mpTableEntry
        (pTeSetMplsTeP2mpTunnelDestTable->MibObject.u4MplsTunnelIndex,
         pTeSetMplsTeP2mpTunnelDestTable->MibObject.u4MplsTunnelInstance,
         pTeSetMplsTeP2mpTunnelDestTable->MibObject.u4MplsTunnelIngressLSRId,
         pTeSetMplsTeP2mpTunnelDestTable->MibObject.u4MplsTunnelEgressLSRId,
         &pTeTnlInfo) == TE_FAILURE)
    {
        TE_DBG (TE_LLVL_SET_FAIL, "P2MP Tunnel "
                "does not exist in TE Tunnel Table \r\n");
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MEMCPY (&u4DestId, pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            au1MplsTeP2mpTunnelDestDestination,
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestDestinationLen);
    u4DestId = TE_NTOHL (u4DestId);
    TMO_SLL_Scan (&(TE_P2MP_TNL_DEST_LIST_INFO (pTeTnlInfo)),
                  pTeP2mpDestEntry, tP2mpDestEntry *)
    {
        /* Check if input Dest ID is present in P2MP Dest list */
        CONVERT_TO_INTEGER ((TE_P2MP_DEST_ADDRESS (pTeP2mpDestEntry)),
                            u4DestIdInList);
        if (u4DestId == u4DestIdInList)
        {
            bIsFoundDestEntry = TRUE;
            u4OutSegIndex = TE_P2MP_DEST_BRANCH_OUTSEG_INDEX (pTeP2mpDestEntry);
            break;
        }
    }

    pTeMplsTeP2mpTunnelDestTable = (tTeMplsTeP2mpTunnelDestTable *)
        TE_ALLOC_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID);
    if (pTeMplsTeP2mpTunnelDestTable == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pTeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));
    if (FALSE == bIsFoundDestEntry)
    {
        /* Create the node if the RowStatus given is 
         * CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pTeSetMplsTeP2mpTunnelDestTable->MibObject.
             i4MplsTeP2mpTunnelDestRowStatus == CREATE_AND_WAIT)
            || (pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                i4MplsTeP2mpTunnelDestRowStatus == CREATE_AND_GO)
            || ((pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                 i4MplsTeP2mpTunnelDestRowStatus == ACTIVE)
                && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pTeP2mpDestEntry = TeAddP2mpDestEntry (pTeTnlInfo, u4DestId);
            if (pTeP2mpDestEntry == NULL)
            {
                MPLS_CMN_UNLOCK ();
                TE_DBG (TE_LLVL_SET_FAIL, "TeAddP2mpDestEntry failed\r\n");
                TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                  (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
                return SNMP_FAILURE;
            }
            OsixGetSysTime (&(TE_P2MP_DEST_CREATE_TIME (pTeP2mpDestEntry)));
            /* Assign values for the new node */
            if (pTeIsSetMplsTeP2mpTunnelDestTable->
                bMplsTeP2mpTunnelDestSrcSubGroupOriginType != OSIX_FALSE)
            {
                TE_P2MP_DEST_SRC_SUBGRP_ORIGTYPE (pTeP2mpDestEntry) =
                    (UINT1) pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;
                pTeMplsTeP2mpTunnelDestTable->MibObject.
                    i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
                    pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;
            }

            if (pTeIsSetMplsTeP2mpTunnelDestTable->
                bMplsTeP2mpTunnelDestSrcSubGroupOrigin != OSIX_FALSE)
            {
                MEMCPY (&u4DestSrcSubGrpOriginId,
                        pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                        au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
                        pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen);
                u4DestSrcSubGrpOriginId = TE_NTOHL (u4DestSrcSubGrpOriginId);
                MEMCPY ((UINT1 *)
                        &(TE_P2MP_DEST_SRC_SUBGRP_ORIGIN (pTeP2mpDestEntry)),
                        (UINT1 *) &(u4DestSrcSubGrpOriginId), sizeof (UINT4));
                MEMCPY (pTeMplsTeP2mpTunnelDestTable->MibObject.
                        au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
                        pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                        au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
                        pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen);
            }

            if (pTeIsSetMplsTeP2mpTunnelDestTable->
                bMplsTeP2mpTunnelDestSrcSubGroupID != OSIX_FALSE)
            {
                TE_P2MP_DEST_SRC_SUBGRP_ID (pTeP2mpDestEntry) =
                    (UINT1) pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    u4MplsTeP2mpTunnelDestSrcSubGroupID;
                pTeMplsTeP2mpTunnelDestTable->MibObject.
                    u4MplsTeP2mpTunnelDestSrcSubGroupID =
                    pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    u4MplsTeP2mpTunnelDestSrcSubGroupID;
            }

            if (pTeIsSetMplsTeP2mpTunnelDestTable->
                bMplsTeP2mpTunnelDestSubGroupOriginType != OSIX_FALSE)
            {
                TE_P2MP_DEST_SUBGRP_ORIGTYPE (pTeP2mpDestEntry) =
                    (UINT1) pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    i4MplsTeP2mpTunnelDestSubGroupOriginType;
                pTeMplsTeP2mpTunnelDestTable->MibObject.
                    i4MplsTeP2mpTunnelDestSubGroupOriginType =
                    pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    i4MplsTeP2mpTunnelDestSubGroupOriginType;
            }

            if (pTeIsSetMplsTeP2mpTunnelDestTable->
                bMplsTeP2mpTunnelDestSubGroupOrigin != OSIX_FALSE)
            {
                MEMCPY (&u4DestSubGrpOriginId,
                        pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                        au1MplsTeP2mpTunnelDestSubGroupOrigin,
                        pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                        i4MplsTeP2mpTunnelDestSubGroupOriginLen);
                u4DestSubGrpOriginId = TE_NTOHL (u4DestSubGrpOriginId);
                MEMCPY ((UINT1 *)
                        &(TE_P2MP_DEST_SUBGRP_ORIGIN (pTeP2mpDestEntry)),
                        (UINT1 *) &(u4DestSubGrpOriginId), sizeof (UINT4));
                MEMCPY (pTeMplsTeP2mpTunnelDestTable->MibObject.
                        au1MplsTeP2mpTunnelDestSubGroupOrigin,
                        pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                        au1MplsTeP2mpTunnelDestSubGroupOrigin,
                        pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                        i4MplsTeP2mpTunnelDestSubGroupOriginLen);
            }

            if (pTeIsSetMplsTeP2mpTunnelDestTable->
                bMplsTeP2mpTunnelDestSubGroupID != OSIX_FALSE)
            {
                TE_P2MP_DEST_SUBGRP_ID (pTeP2mpDestEntry) =
                    (UINT1) pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    u4MplsTeP2mpTunnelDestSubGroupID;
                pTeMplsTeP2mpTunnelDestTable->MibObject.
                    u4MplsTeP2mpTunnelDestSubGroupID =
                    pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    u4MplsTeP2mpTunnelDestSubGroupID;
            }

            if (pTeIsSetMplsTeP2mpTunnelDestTable->
                bMplsTeP2mpTunnelDestDestinationType != OSIX_FALSE)
            {
                TE_P2MP_DEST_ADDR_TYPE (pTeP2mpDestEntry) =
                    (UINT1) pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    i4MplsTeP2mpTunnelDestDestinationType;
                pTeMplsTeP2mpTunnelDestTable->MibObject.
                    i4MplsTeP2mpTunnelDestDestinationType =
                    pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    i4MplsTeP2mpTunnelDestDestinationType;
            }

            if (pTeIsSetMplsTeP2mpTunnelDestTable->
                bMplsTeP2mpTunnelDestDestination != OSIX_FALSE)
            {
                MEMCPY ((UINT1 *) &(TE_P2MP_DEST_ADDRESS (pTeP2mpDestEntry)),
                        (UINT1 *) &(u4DestId), sizeof (UINT4));
                MEMCPY (pTeMplsTeP2mpTunnelDestTable->MibObject.
                        au1MplsTeP2mpTunnelDestDestination,
                        pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                        au1MplsTeP2mpTunnelDestDestination,
                        pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                        i4MplsTeP2mpTunnelDestDestinationLen);
            }

            if (pTeIsSetMplsTeP2mpTunnelDestTable->
                bMplsTeP2mpTunnelDestBranchOutSegment != OSIX_FALSE)
            {
                MEMCPY (&(TE_P2MP_DEST_BRANCH_OUTSEG_INDEX (pTeP2mpDestEntry)),
                        pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                        au1MplsTeP2mpTunnelDestBranchOutSegment,
                        pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                        i4MplsTeP2mpTunnelDestBranchOutSegmentLen);
                TE_P2MP_DEST_BRANCH_OUTSEG_INDEX (pTeP2mpDestEntry) =
                    TE_NTOHL (TE_P2MP_DEST_BRANCH_OUTSEG_INDEX
                              (pTeP2mpDestEntry));
                MEMCPY (pTeMplsTeP2mpTunnelDestTable->MibObject.
                        au1MplsTeP2mpTunnelDestBranchOutSegment,
                        pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                        au1MplsTeP2mpTunnelDestBranchOutSegment,
                        pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                        i4MplsTeP2mpTunnelDestBranchOutSegmentLen);
            }

            if (pTeIsSetMplsTeP2mpTunnelDestTable->
                bMplsTeP2mpTunnelDestHopTableIndex != OSIX_FALSE)
            {
                TE_P2MP_DEST_HOP_TABLE_INDEX (pTeP2mpDestEntry) =
                    pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    u4MplsTeP2mpTunnelDestHopTableIndex;
                pTeMplsTeP2mpTunnelDestTable->MibObject.
                    u4MplsTeP2mpTunnelDestHopTableIndex =
                    pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    u4MplsTeP2mpTunnelDestHopTableIndex;
            }

            if (pTeIsSetMplsTeP2mpTunnelDestTable->
                bMplsTeP2mpTunnelDestPathInUse != OSIX_FALSE)
            {
                TE_P2MP_DEST_PATH_IN_USE (pTeP2mpDestEntry) =
                    pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    u4MplsTeP2mpTunnelDestPathInUse;
                pTeMplsTeP2mpTunnelDestTable->MibObject.
                    u4MplsTeP2mpTunnelDestPathInUse =
                    pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    u4MplsTeP2mpTunnelDestPathInUse;
            }

            if (pTeIsSetMplsTeP2mpTunnelDestTable->
                bMplsTeP2mpTunnelDestAdminStatus != OSIX_FALSE)
            {
                TE_P2MP_DEST_ADMIN_STATUS (pTeP2mpDestEntry) =
                    (UINT1) pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    i4MplsTeP2mpTunnelDestAdminStatus;
                pTeMplsTeP2mpTunnelDestTable->MibObject.
                    i4MplsTeP2mpTunnelDestAdminStatus =
                    pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    i4MplsTeP2mpTunnelDestAdminStatus;
            }

            if (pTeIsSetMplsTeP2mpTunnelDestTable->
                bMplsTeP2mpTunnelDestRowStatus != OSIX_FALSE)
            {
                TE_P2MP_DEST_ROW_STATUS (pTeP2mpDestEntry) =
                    (UINT1) pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    i4MplsTeP2mpTunnelDestRowStatus;
                pTeMplsTeP2mpTunnelDestTable->MibObject.
                    i4MplsTeP2mpTunnelDestRowStatus =
                    pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    i4MplsTeP2mpTunnelDestRowStatus;
            }

            if (pTeIsSetMplsTeP2mpTunnelDestTable->
                bMplsTeP2mpTunnelDestStorageType != OSIX_FALSE)
            {
                TE_P2MP_DEST_STORAGE_TYPE (pTeP2mpDestEntry) =
                    (UINT1) pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    i4MplsTeP2mpTunnelDestStorageType;
                pTeMplsTeP2mpTunnelDestTable->MibObject.
                    i4MplsTeP2mpTunnelDestStorageType =
                    pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    i4MplsTeP2mpTunnelDestStorageType;
            }

            if (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTunnelIndex !=
                OSIX_FALSE)
            {
                pTeMplsTeP2mpTunnelDestTable->MibObject.u4MplsTunnelIndex =
                    pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    u4MplsTunnelIndex;
            }

            if (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTunnelInstance !=
                OSIX_FALSE)
            {
                pTeMplsTeP2mpTunnelDestTable->MibObject.u4MplsTunnelInstance =
                    pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    u4MplsTunnelInstance;
            }

            if (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTunnelIngressLSRId !=
                OSIX_FALSE)
            {
                pTeMplsTeP2mpTunnelDestTable->MibObject.
                    u4MplsTunnelIngressLSRId =
                    pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    u4MplsTunnelIngressLSRId;
            }

            if (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTunnelEgressLSRId !=
                OSIX_FALSE)
            {
                pTeMplsTeP2mpTunnelDestTable->MibObject.
                    u4MplsTunnelEgressLSRId =
                    pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    u4MplsTunnelEgressLSRId;
            }

            if ((pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                 i4MplsTeP2mpTunnelDestRowStatus == CREATE_AND_GO)
                || ((i4RowCreateOption == 1) &&
                    (pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                     i4MplsTeP2mpTunnelDestRowStatus == ACTIVE)))
            {
                pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    i4MplsTeP2mpTunnelDestRowStatus = ACTIVE;

                TE_P2MP_DEST_ROW_STATUS (pTeP2mpDestEntry) = ACTIVE;
                pTeMplsTeP2mpTunnelDestTable->MibObject.
                    i4MplsTeP2mpTunnelDestRowStatus = ACTIVE;
                /* For MSR and RM Trigger */
                TeTrgMplsTeP2mpTunnelDestTable.MibObject.
                    i4MplsTeP2mpTunnelDestRowStatus = CREATE_AND_WAIT;

                TeTrgIsSetMplsTeP2mpTunnelDestTable.
                    bMplsTeP2mpTunnelDestRowStatus = OSIX_TRUE;

                TeTrgMplsTeP2mpTunnelDestTable.MibObject.
                    u4MplsTunnelIndex =
                    pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    u4MplsTunnelIndex;

                TeTrgMplsTeP2mpTunnelDestTable.MibObject.
                    u4MplsTunnelInstance =
                    pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    u4MplsTunnelInstance;

                TeTrgMplsTeP2mpTunnelDestTable.MibObject.
                    u4MplsTunnelIngressLSRId =
                    pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    u4MplsTunnelIngressLSRId;

                TeTrgMplsTeP2mpTunnelDestTable.MibObject.
                    u4MplsTunnelEgressLSRId =
                    pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    u4MplsTunnelEgressLSRId;

                TeTrgMplsTeP2mpTunnelDestTable.MibObject.
                    i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
                    pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;

                TeTrgMplsTeP2mpTunnelDestTable.MibObject.
                    i4MplsTeP2mpTunnelDestSubGroupOriginType =
                    pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    i4MplsTeP2mpTunnelDestSubGroupOriginType;

                TeTrgMplsTeP2mpTunnelDestTable.MibObject.
                    u4MplsTeP2mpTunnelDestSrcSubGroupID =
                    pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    u4MplsTeP2mpTunnelDestSrcSubGroupID;

                TeTrgMplsTeP2mpTunnelDestTable.MibObject.
                    u4MplsTeP2mpTunnelDestSubGroupID =
                    pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    u4MplsTeP2mpTunnelDestSubGroupID;

                TeTrgMplsTeP2mpTunnelDestTable.MibObject.
                    i4MplsTeP2mpTunnelDestDestinationType =
                    pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    i4MplsTeP2mpTunnelDestDestinationType;

                MEMCPY (&TeTrgMplsTeP2mpTunnelDestTable.MibObject.
                        au1MplsTeP2mpTunnelDestDestination,
                        pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                        au1MplsTeP2mpTunnelDestDestination,
                        pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                        i4MplsTeP2mpTunnelDestDestinationLen);

                MEMCPY (&TeTrgMplsTeP2mpTunnelDestTable.MibObject.
                        au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
                        pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                        au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
                        pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen);

                MEMCPY (&TeTrgMplsTeP2mpTunnelDestTable.MibObject.
                        au1MplsTeP2mpTunnelDestSubGroupOrigin,
                        pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                        au1MplsTeP2mpTunnelDestSubGroupOrigin,
                        pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                        i4MplsTeP2mpTunnelDestSubGroupOriginLen);

                TeTrgMplsTeP2mpTunnelDestTable.MibObject.
                    i4MplsTeP2mpTunnelDestSubGroupOriginLen =
                    pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    i4MplsTeP2mpTunnelDestSubGroupOriginLen;

                TeTrgMplsTeP2mpTunnelDestTable.MibObject.
                    i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen =
                    pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen;

                TeTrgMplsTeP2mpTunnelDestTable.MibObject.
                    i4MplsTeP2mpTunnelDestDestinationLen =
                    pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                    i4MplsTeP2mpTunnelDestDestinationLen;

                if (TeSetAllMplsTeP2mpTunnelDestTableTrigger
                    (&TeTrgMplsTeP2mpTunnelDestTable,
                     &TeTrgIsSetMplsTeP2mpTunnelDestTable,
                     SNMP_SUCCESS) != SNMP_SUCCESS)
                {
                    MPLS_CMN_UNLOCK ();
                    TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                      (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
                    return SNMP_FAILURE;
                }
            }
            else if (pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                     i4MplsTeP2mpTunnelDestRowStatus == CREATE_AND_WAIT)
            {
                TE_P2MP_DEST_ROW_STATUS (pTeP2mpDestEntry) = NOT_READY;
                pTeMplsTeP2mpTunnelDestTable->MibObject.
                    i4MplsTeP2mpTunnelDestRowStatus = NOT_READY;
            }

            if (TeUtilUpdateMplsTeP2mpTunnelDestTable
                (NULL, pTeMplsTeP2mpTunnelDestTable) != SNMP_SUCCESS)
            {
                TeRemoveP2mpDestEntry (pTeTnlInfo, u4DestId);
                MPLS_CMN_UNLOCK ();
                TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                  (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
                return SNMP_FAILURE;
            }
            if (TeSetAllMplsTeP2mpTunnelDestTableTrigger
                (pTeSetMplsTeP2mpTunnelDestTable,
                 pTeIsSetMplsTeP2mpTunnelDestTable,
                 SNMP_SUCCESS) != SNMP_SUCCESS)
            {
                TeRemoveP2mpDestEntry (pTeTnlInfo, u4DestId);
                MPLS_CMN_UNLOCK ();
                TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                  (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
                return SNMP_FAILURE;
            }
            MPLS_CMN_UNLOCK ();
            TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                              (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
            return SNMP_SUCCESS;

        }
        else
        {
            MPLS_CMN_UNLOCK ();
            TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                              (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
            return SNMP_FAILURE;
        }
    }
    else if ((pTeSetMplsTeP2mpTunnelDestTable->MibObject.
              i4MplsTeP2mpTunnelDestRowStatus == CREATE_AND_WAIT)
             || (pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                 i4MplsTeP2mpTunnelDestRowStatus == CREATE_AND_GO))
    {
        MPLS_CMN_UNLOCK ();
        TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                          (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
        return SNMP_FAILURE;
    }

    TMO_SLL_Scan (&(TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo)),
                  pTeP2mpBranchEntry, tP2mpBranchEntry *)
    {
        if (u4OutSegIndex == TE_P2MP_BRANCH_OUTSEG_INDEX (pTeP2mpBranchEntry))
        {
            bIsFoundBranchEntry = TRUE;
            break;
        }
    }
    if (FALSE == bIsFoundBranchEntry)
    {
        pTeP2mpBranchEntry = NULL;
    }

    pTeOldMplsTeP2mpTunnelDestTable = (tTeMplsTeP2mpTunnelDestTable *)
        TE_ALLOC_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID);
    if (pTeOldMplsTeP2mpTunnelDestTable == NULL)
    {
        TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                          (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
        return SNMP_FAILURE;
    }
    MEMSET (pTeOldMplsTeP2mpTunnelDestTable, TE_ZERO,
            sizeof (tTeMplsTeP2mpTunnelDestTable));
    /* Copy the previous values before setting the new values */
    MEMCPY (pTeOldMplsTeP2mpTunnelDestTable, pTeMplsTeP2mpTunnelDestTable,
            sizeof (tTeMplsTeP2mpTunnelDestTable));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pTeSetMplsTeP2mpTunnelDestTable->MibObject.
        i4MplsTeP2mpTunnelDestRowStatus == DESTROY)
    {
        pTeMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestRowStatus = DESTROY;

        if (TeUtilUpdateMplsTeP2mpTunnelDestTable
            (pTeOldMplsTeP2mpTunnelDestTable,
             pTeMplsTeP2mpTunnelDestTable) != SNMP_SUCCESS)
        {
            MPLS_CMN_UNLOCK ();
            TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                              (UINT1 *) pTeOldMplsTeP2mpTunnelDestTable);
            TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                              (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
            return SNMP_FAILURE;
        }
        if ((TeRemoveP2mpDestEntry (pTeTnlInfo, u4DestId)) == TE_SUCCESS)
        {
            if (TRUE == bIsFoundBranchEntry)
            {
                /* Update branch destination count */
                TE_P2MP_BRANCH_DEST_COUNT (pTeP2mpBranchEntry) -= TE_ONE;
                if (TE_ACTIVE == TE_P2MP_DEST_ROW_STATUS (pTeP2mpDestEntry))
                {
                    TE_P2MP_BRANCH_ACTIVE_DEST_COUNT (pTeP2mpBranchEntry) -= TE_ONE;
                }

                /* Remove ILM in HW when no operational destinations are 
                 * associated */
                if (TE_ZERO ==
                    TE_P2MP_BRANCH_ACTIVE_DEST_COUNT (pTeP2mpBranchEntry))
                {
                    if ((TE_ADMIN_UP == TE_TNL_ADMIN_STATUS (pTeTnlInfo)) &&
                        (TE_ACTIVE == TE_TNL_ROW_STATUS (pTeTnlInfo)))
                    {
                        pOutSegment = MplsGetOutSegmentTableEntry
                            (u4OutSegIndex);
                        if (NULL == pOutSegment)
                        {
                            MPLS_CMN_UNLOCK ();
                            TE_DBG (TE_LLVL_SET_FAIL,
                                    "MplsGetOutSegmentTableEntry failed\r\n");
                            TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                              (UINT1 *)
                                              pTeOldMplsTeP2mpTunnelDestTable);
                            TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                              (UINT1 *)
                                              pTeMplsTeP2mpTunnelDestTable);
                            return SNMP_FAILURE;
                        }

                        pXcEntry = (tXcEntry *) (pOutSegment->pXcIndex);
                        if (NULL == pXcEntry)
                        {
                            MPLS_CMN_UNLOCK ();
                            TE_DBG (TE_LLVL_SET_FAIL,
                                    "XC entry not available \r\n");
                            TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                              (UINT1 *)
                                              pTeOldMplsTeP2mpTunnelDestTable);
                            TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                              (UINT1 *)
                                              pTeMplsTeP2mpTunnelDestTable);
                            return SNMP_FAILURE;
                        }

                        if (MplsDbUpdateP2mpTunnelInHw (pTeTnlInfo, pXcEntry,
                                                        TE_OPER_DOWN)
                            == MPLS_FAILURE)
                        {
                            MPLS_CMN_UNLOCK ();
                            TE_DBG (TE_LLVL_SET_FAIL,
                                    "MplsDbUpdateP2mpTunnelInHw failed\r\n");
                            TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                              (UINT1 *)
                                              pTeOldMplsTeP2mpTunnelDestTable);
                            TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                              (UINT1 *)
                                              pTeMplsTeP2mpTunnelDestTable);
                            return SNMP_FAILURE;
                        }
                    }
                }
                if (TE_ZERO == TE_P2MP_BRANCH_DEST_COUNT (pTeP2mpBranchEntry))
                {
                    if ((TeRemoveP2mpBranchEntry (pTeTnlInfo, u4OutSegIndex))
                        == TE_FAILURE)
                    {
                        MPLS_CMN_UNLOCK ();
                        TE_DBG (TE_LLVL_SET_FAIL,
                                "TeRemoveP2mpBranchEntry failed\r\n");
                        TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                          (UINT1 *)
                                          pTeOldMplsTeP2mpTunnelDestTable);
                        TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                          (UINT1 *)
                                          pTeMplsTeP2mpTunnelDestTable);
                        return SNMP_FAILURE;
                    }
                }
            }
        }
        else
        {
            MPLS_CMN_UNLOCK ();
            TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                              (UINT1 *) pTeOldMplsTeP2mpTunnelDestTable);
            TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                              (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
            return SNMP_FAILURE;
        }
        if (TeSetAllMplsTeP2mpTunnelDestTableTrigger
            (pTeSetMplsTeP2mpTunnelDestTable, pTeIsSetMplsTeP2mpTunnelDestTable,
             SNMP_SUCCESS) != SNMP_SUCCESS)
        {
            MPLS_CMN_UNLOCK ();
            TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                              (UINT1 *) pTeOldMplsTeP2mpTunnelDestTable);
            TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                              (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
            return SNMP_FAILURE;
        }
        MPLS_CMN_UNLOCK ();
        TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                          (UINT1 *) pTeOldMplsTeP2mpTunnelDestTable);
        TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                          (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
        return SNMP_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (MplsTeP2mpTunnelDestTableFilterInputs
        (pTeMplsTeP2mpTunnelDestTable, pTeSetMplsTeP2mpTunnelDestTable,
         pTeIsSetMplsTeP2mpTunnelDestTable) == OSIX_FALSE)
    {
        MPLS_CMN_UNLOCK ();
        TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                          (UINT1 *) pTeOldMplsTeP2mpTunnelDestTable);
        TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                          (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
        return SNMP_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TRUE) &&
        (pTeMplsTeP2mpTunnelDestTable->MibObject.
         i4MplsTeP2mpTunnelDestRowStatus == ACTIVE))
    {
        pTeMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestRowStatus = NOT_IN_SERVICE;
        i4RowMakeActive = TRUE;

        /* For MSR and RM Trigger */
        TeTrgMplsTeP2mpTunnelDestTable.MibObject.
            i4MplsTeP2mpTunnelDestRowStatus = NOT_IN_SERVICE;
        TeTrgIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestRowStatus =
            OSIX_TRUE;

        TeTrgMplsTeP2mpTunnelDestTable.MibObject.
            u4MplsTunnelIndex =
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.u4MplsTunnelIndex;

        TeTrgMplsTeP2mpTunnelDestTable.MibObject.
            u4MplsTunnelInstance =
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.u4MplsTunnelInstance;

        TeTrgMplsTeP2mpTunnelDestTable.MibObject.
            u4MplsTunnelIngressLSRId =
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.u4MplsTunnelIngressLSRId;

        TeTrgMplsTeP2mpTunnelDestTable.MibObject.
            u4MplsTunnelEgressLSRId =
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.u4MplsTunnelEgressLSRId;

        TeTrgMplsTeP2mpTunnelDestTable.MibObject.
            i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;

        TeTrgMplsTeP2mpTunnelDestTable.MibObject.
            i4MplsTeP2mpTunnelDestSubGroupOriginType =
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestSubGroupOriginType;

        TeTrgMplsTeP2mpTunnelDestTable.MibObject.
            u4MplsTeP2mpTunnelDestSrcSubGroupID =
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            u4MplsTeP2mpTunnelDestSrcSubGroupID;

        TeTrgMplsTeP2mpTunnelDestTable.MibObject.
            u4MplsTeP2mpTunnelDestSubGroupID =
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            u4MplsTeP2mpTunnelDestSubGroupID;

        TeTrgMplsTeP2mpTunnelDestTable.MibObject.
            i4MplsTeP2mpTunnelDestDestinationType =
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestDestinationType;

        MEMCPY (&TeTrgMplsTeP2mpTunnelDestTable.MibObject.
                au1MplsTeP2mpTunnelDestDestination,
                pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                au1MplsTeP2mpTunnelDestDestination,
                pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                i4MplsTeP2mpTunnelDestDestinationLen);

        MEMCPY (&TeTrgMplsTeP2mpTunnelDestTable.MibObject.
                au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
                pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
                pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen);

        MEMCPY (&TeTrgMplsTeP2mpTunnelDestTable.MibObject.
                au1MplsTeP2mpTunnelDestSubGroupOrigin,
                pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                au1MplsTeP2mpTunnelDestSubGroupOrigin,
                pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                i4MplsTeP2mpTunnelDestSubGroupOriginLen);

        TeTrgMplsTeP2mpTunnelDestTable.MibObject.
            i4MplsTeP2mpTunnelDestSubGroupOriginLen =
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestSubGroupOriginLen;

        TeTrgMplsTeP2mpTunnelDestTable.MibObject.
            i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen =
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen;

        TeTrgMplsTeP2mpTunnelDestTable.MibObject.
            i4MplsTeP2mpTunnelDestDestinationLen =
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestDestinationLen;

        if (TeUtilUpdateMplsTeP2mpTunnelDestTable
            (pTeOldMplsTeP2mpTunnelDestTable,
             pTeMplsTeP2mpTunnelDestTable) != SNMP_SUCCESS)
        {
            /*Restore back with previous values */
            MEMCPY (pTeMplsTeP2mpTunnelDestTable,
                    pTeOldMplsTeP2mpTunnelDestTable,
                    sizeof (tTeMplsTeP2mpTunnelDestTable));
            MPLS_CMN_UNLOCK ();
            TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                              (UINT1 *) pTeOldMplsTeP2mpTunnelDestTable);
            TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                              (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
            return SNMP_FAILURE;
        }

        if (TeSetAllMplsTeP2mpTunnelDestTableTrigger
            (&TeTrgMplsTeP2mpTunnelDestTable,
             &TeTrgIsSetMplsTeP2mpTunnelDestTable,
             SNMP_SUCCESS) != SNMP_SUCCESS)
        {
            MPLS_CMN_UNLOCK ();
            TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                              (UINT1 *) pTeOldMplsTeP2mpTunnelDestTable);
            TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                              (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
            return SNMP_FAILURE;
        }
    }

    if (pTeSetMplsTeP2mpTunnelDestTable->MibObject.
        i4MplsTeP2mpTunnelDestRowStatus == ACTIVE)
    {
        if (TE_ACTIVE != TE_P2MP_DEST_ROW_STATUS (pTeP2mpDestEntry))
        {
            if (NULL == pTeP2mpBranchEntry)
            {
                MPLS_CMN_UNLOCK ();
                TE_DBG (TE_LLVL_SET_FAIL, "Failed to make row status active. "
                        "Branch entry does not exist \r\n");
                TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                  (UINT1 *) pTeOldMplsTeP2mpTunnelDestTable);
                TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                  (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
                return SNMP_FAILURE;
            }
            TE_P2MP_BRANCH_ACTIVE_DEST_COUNT (pTeP2mpBranchEntry) += TE_ONE;
            if ((TE_ADMIN_UP == TE_TNL_ADMIN_STATUS (pTeTnlInfo)) &&
                (TE_ACTIVE == TE_TNL_ROW_STATUS (pTeTnlInfo)) &&
                (TE_ADMIN_UP == TE_P2MP_DEST_ADMIN_STATUS (pTeP2mpDestEntry)))
            {
                pOutSegment = MplsGetOutSegmentTableEntry (u4OutSegIndex);
                if (NULL == pOutSegment)
                {
                    MPLS_CMN_UNLOCK ();
                    TE_DBG (TE_LLVL_SET_FAIL,
                            "MplsGetOutSegmentTableEntry failed\r\n");
                    TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                      (UINT1 *)
                                      pTeOldMplsTeP2mpTunnelDestTable);
                    TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                      (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
                    return SNMP_FAILURE;
                }
                /* When ILM is already added in HW, when out-segment is 
                 * associated to first destination, subsequent ILM HW Add will 
                 * return success without configuring the HW again */
                pXcEntry = (tXcEntry *) (pOutSegment->pXcIndex);
                if (NULL == pXcEntry)
                {
                    MPLS_CMN_UNLOCK ();
                    TE_DBG (TE_LLVL_SET_FAIL, "XC entry not available \r\n");
                    TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                      (UINT1 *)
                                      pTeOldMplsTeP2mpTunnelDestTable);
                    TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                      (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
                    return SNMP_FAILURE;
                }

                if (MplsDbUpdateP2mpTunnelInHw (pTeTnlInfo, pXcEntry,
                                                TE_OPER_UP) == MPLS_FAILURE)
                {
                    MPLS_CMN_UNLOCK ();
                    TE_DBG (TE_LLVL_SET_FAIL,
                            "MplsDbUpdateP2mpTunnelInHw failed\r\n");
                    TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                      (UINT1 *)
                                      pTeOldMplsTeP2mpTunnelDestTable);
                    TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                      (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
                    return SNMP_FAILURE;
                }
                TE_P2MP_DEST_OPER_STATUS (pTeP2mpDestEntry) =
                    XC_OPER_STATUS (pXcEntry);
            }
        }
        i4RowMakeActive = TRUE;
    }

    /* Assign values for the existing node */
    if (pTeIsSetMplsTeP2mpTunnelDestTable->
        bMplsTeP2mpTunnelDestBranchOutSegment != OSIX_FALSE)
    {
        MEMCPY (&(TE_P2MP_DEST_BRANCH_OUTSEG_INDEX (pTeP2mpDestEntry)),
                pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                au1MplsTeP2mpTunnelDestBranchOutSegment,
                pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                i4MplsTeP2mpTunnelDestBranchOutSegmentLen);
        TE_P2MP_DEST_BRANCH_OUTSEG_INDEX (pTeP2mpDestEntry) =
            TE_NTOHL (TE_P2MP_DEST_BRANCH_OUTSEG_INDEX (pTeP2mpDestEntry));

        /* Decrement P2MP destination count in branch out-segment node */
        if ((TRUE == bIsFoundBranchEntry) &&
            (TE_ONE == TE_P2MP_BRANCH_DEST_COUNT (pTeP2mpBranchEntry)))
        {
            if ((TeRemoveP2mpBranchEntry (pTeTnlInfo, u4OutSegIndex))
                == TE_FAILURE)
            {
                TE_P2MP_DEST_BRANCH_OUTSEG_INDEX (pTeP2mpDestEntry) =
                    u4OutSegIndex;
                MPLS_CMN_UNLOCK ();
                TE_DBG (TE_LLVL_SET_FAIL, "TeRemoveP2mpBranchEntry failed\r\n");
                TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                  (UINT1 *) pTeOldMplsTeP2mpTunnelDestTable);
                TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                  (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
                return SNMP_FAILURE;
            }

            /* Last P2MP destination associated to this branch out-segment 
             * has been removed. Remove the ILM in HW */
            pOutSegment = MplsGetOutSegmentTableEntry (u4OutSegIndex);
            if (NULL == pOutSegment)
            {
                MPLS_CMN_UNLOCK ();
                TE_DBG (TE_LLVL_SET_FAIL,
                        "MplsGetOutSegmentTableEntry failed\r\n");
                TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                  (UINT1 *) pTeOldMplsTeP2mpTunnelDestTable);
                TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                  (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
                return SNMP_FAILURE;
            }
            pXcEntry = (tXcEntry *) OUTSEGMENT_XC_INDEX (pOutSegment);
            if (NULL == pXcEntry)
            {
                MPLS_CMN_UNLOCK ();
                TE_DBG (TE_LLVL_SET_FAIL, "XC entry not available \r\n");
                TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                  (UINT1 *) pTeOldMplsTeP2mpTunnelDestTable);
                TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                  (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
                return SNMP_FAILURE;
            }
            if (MplsDbUpdateP2mpTunnelInHw (pTeTnlInfo, pXcEntry, TE_OPER_DOWN)
                == MPLS_FAILURE)
            {
                MPLS_CMN_UNLOCK ();
                TE_DBG (TE_LLVL_SET_FAIL,
                        "MplsDbUpdateP2mpTunnelInHw failed\r\n");
                TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                  (UINT1 *) pTeOldMplsTeP2mpTunnelDestTable);
                TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                  (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
                return SNMP_FAILURE;
            }
        }
        else if (TRUE == bIsFoundBranchEntry)
        {
            TE_P2MP_BRANCH_DEST_COUNT (pTeP2mpBranchEntry) -= TE_ONE;
        }

        /* When the P2MP destination is associated to a previously existing 
         * out-segment, increment P2MP destination count to that branch 
         * out-segment node */
        TMO_SLL_Scan (&(TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo)),
                      pTeP2mpNextBranchEntry, tP2mpBranchEntry *)
        {
            if (TE_P2MP_DEST_BRANCH_OUTSEG_INDEX (pTeP2mpDestEntry)
                == TE_P2MP_BRANCH_OUTSEG_INDEX (pTeP2mpNextBranchEntry))
            {
                bIsFoundNewBranchEntry = TRUE;
                break;
            }
        }
        if (FALSE == bIsFoundNewBranchEntry)
        {
            pTeP2mpNextBranchEntry = NULL;
            pTeP2mpNextBranchEntry =
                TeAddP2mpBranchEntry (pTeTnlInfo,
                                      TE_P2MP_DEST_BRANCH_OUTSEG_INDEX
                                      (pTeP2mpDestEntry));
            if (NULL == pTeP2mpNextBranchEntry)
            {
                TE_P2MP_DEST_BRANCH_OUTSEG_INDEX (pTeP2mpDestEntry) =
                    u4OutSegIndex;
                MPLS_CMN_UNLOCK ();
                TE_DBG (TE_LLVL_SET_FAIL, "TeAddP2mpBranchEntry failed\r\n");
                TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                  (UINT1 *) pTeOldMplsTeP2mpTunnelDestTable);
                TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                  (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
                return SNMP_FAILURE;
            }
        }
        if (NULL != pTeP2mpNextBranchEntry)
        {
            TE_P2MP_BRANCH_DEST_COUNT (pTeP2mpNextBranchEntry) += TE_ONE;
        }
        MEMCPY (pTeMplsTeP2mpTunnelDestTable->MibObject.
                au1MplsTeP2mpTunnelDestBranchOutSegment,
                pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                au1MplsTeP2mpTunnelDestBranchOutSegment,
                pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                i4MplsTeP2mpTunnelDestBranchOutSegmentLen);
    }
    if (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTeP2mpTunnelDestHopTableIndex !=
        OSIX_FALSE)
    {
        TE_P2MP_DEST_HOP_TABLE_INDEX (pTeP2mpDestEntry) =
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            u4MplsTeP2mpTunnelDestHopTableIndex;
        pTeMplsTeP2mpTunnelDestTable->MibObject.
            u4MplsTeP2mpTunnelDestHopTableIndex =
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            u4MplsTeP2mpTunnelDestHopTableIndex;
    }
    if (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTeP2mpTunnelDestPathInUse !=
        OSIX_FALSE)
    {
        TE_P2MP_DEST_PATH_IN_USE (pTeP2mpDestEntry) =
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            u4MplsTeP2mpTunnelDestPathInUse;
        pTeMplsTeP2mpTunnelDestTable->MibObject.
            u4MplsTeP2mpTunnelDestPathInUse =
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            u4MplsTeP2mpTunnelDestPathInUse;
    }
    if (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTeP2mpTunnelDestAdminStatus !=
        OSIX_FALSE)
    {
        if (TE_P2MP_DEST_ADMIN_STATUS (pTeP2mpDestEntry) ==
            (UINT1) pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestAdminStatus)
        {
            MPLS_CMN_UNLOCK ();
            TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                              (UINT1 *) pTeOldMplsTeP2mpTunnelDestTable);
            TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                              (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
            return SNMP_SUCCESS;
        }

        if ((TE_ACTIVE != TE_TNL_ROW_STATUS (pTeTnlInfo))
            || (TE_ADMIN_UP != TE_TNL_ADMIN_STATUS (pTeTnlInfo)))
        {
            /* Update destination admin status without programming the hardware.
             * Oper status will be down, since tunnel is not operational */
            TE_P2MP_DEST_ADMIN_STATUS (pTeP2mpDestEntry) =
                (UINT1) pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                i4MplsTeP2mpTunnelDestAdminStatus;
            TE_P2MP_DEST_OPER_STATUS (pTeP2mpDestEntry) = TE_OPER_DOWN;
            MPLS_CMN_UNLOCK ();
            TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                              (UINT1 *) pTeOldMplsTeP2mpTunnelDestTable);
            TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                              (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
            return SNMP_SUCCESS;
        }

        if (NULL == pTeP2mpBranchEntry)
        {
            MPLS_CMN_UNLOCK ();
            TE_DBG (TE_LLVL_SET_FAIL,
                    "Failed to set destination admin status. Out-segment is "
                    "not associated \r\n");
            TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                              (UINT1 *) pTeOldMplsTeP2mpTunnelDestTable);
            TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                              (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
            return SNMP_FAILURE;
        }

        pOutSegment = MplsGetOutSegmentTableEntry (u4OutSegIndex);
        if ((NULL == pOutSegment)
            || (NULL == OUTSEGMENT_XC_INDEX (pOutSegment)))
        {
            MPLS_CMN_UNLOCK ();
            TE_DBG (TE_LLVL_SET_FAIL, "Out-segment/ XC entry unavailable\r\n");
            TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                              (UINT1 *) pTeOldMplsTeP2mpTunnelDestTable);
            TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                              (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
            return SNMP_FAILURE;
        }
        pXcEntry = (tXcEntry *) OUTSEGMENT_XC_INDEX (pOutSegment);

        /* Configure P2MP ILM/Tunnel in HW */
        u1PrevP2mpDestOperStatus = TE_P2MP_DEST_OPER_STATUS (pTeP2mpDestEntry);
        if (TE_ADMIN_UP ==
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestAdminStatus)
        {
            TE_P2MP_BRANCH_ACTIVE_DEST_COUNT (pTeP2mpBranchEntry) += TE_ONE;
            if (TE_ONE == TE_P2MP_BRANCH_ACTIVE_DEST_COUNT (pTeP2mpBranchEntry))
            {
                if (MplsDbUpdateP2mpTunnelInHw
                    (pTeTnlInfo, pXcEntry, TE_OPER_UP) == MPLS_FAILURE)
                {
                    MPLS_CMN_UNLOCK ();
                    TE_DBG (TE_LLVL_SET_FAIL,
                            "MplsDbUpdateP2mpTunnelInHw failed\r\n");
                    TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                      (UINT1 *)
                                      pTeOldMplsTeP2mpTunnelDestTable);
                    TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                      (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
                    return SNMP_FAILURE;
                }
            }
            TE_P2MP_DEST_OPER_STATUS (pTeP2mpDestEntry) =
                XC_OPER_STATUS (pXcEntry);
            if (FALSE == bIsFoundNewBranchEntry)
            {
                /* Inform L2VPN to add the entry */
                TePrcsL2vpnAssociation (pTeTnlInfo, TE_P2MP_LSP_BRANCH_ADD);
            }
        }
        else if (TE_ADMIN_DOWN ==
                 pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                 i4MplsTeP2mpTunnelDestAdminStatus)
        {
            TE_P2MP_BRANCH_ACTIVE_DEST_COUNT (pTeP2mpBranchEntry) -= TE_ONE;
            if (TE_ZERO ==
                TE_P2MP_BRANCH_ACTIVE_DEST_COUNT (pTeP2mpBranchEntry))
            {
                if (MplsDbUpdateP2mpTunnelInHw
                    (pTeTnlInfo, pXcEntry, TE_OPER_DOWN) == MPLS_FAILURE)
                {
                    MPLS_CMN_UNLOCK ();
                    TE_DBG (TE_LLVL_SET_FAIL,
                            "MplsDbUpdateP2mpTunnelInHw failed\r\n");
                    TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                      (UINT1 *)
                                      pTeOldMplsTeP2mpTunnelDestTable);
                    TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                      (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
                    return SNMP_FAILURE;
                }
            }
            TE_P2MP_DEST_OPER_STATUS (pTeP2mpDestEntry) = TE_OPER_DOWN;
        }

        TE_P2MP_DEST_ADMIN_STATUS (pTeP2mpDestEntry) =
            (UINT1) pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestAdminStatus;

        pTeMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestAdminStatus =
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestAdminStatus;
        if (TE_P2MP_DEST_OPER_STATUS (pTeP2mpDestEntry) == TE_OPER_UP)
        {
            OsixGetSysTime (&
                            (TE_P2MP_DEST_INSTANCE_UP_TIME (pTeP2mpDestEntry)));
        }
        else
        {
            TE_P2MP_DEST_INSTANCE_UP_TIME (pTeP2mpDestEntry) = TE_ZERO;
        }
        if (u1PrevP2mpDestOperStatus !=
            TE_P2MP_DEST_OPER_STATUS (pTeP2mpDestEntry))
        {
            TE_P2MP_DEST_STATE_CHG_COUNT (pTeP2mpDestEntry) += TE_ONE;
        }
    }
    if (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTeP2mpTunnelDestRowStatus !=
        OSIX_FALSE)
    {
        if (NULL == pTeP2mpBranchEntry)
        {
            MPLS_CMN_UNLOCK ();
            TE_DBG (TE_LLVL_SET_FAIL, "Failed to make row status as Not in "
                    "service. Branch entry does not exist \r\n");
            TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                              (UINT1 *) pTeOldMplsTeP2mpTunnelDestTable);
            TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                              (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
            return SNMP_FAILURE;
        }
        if ((TE_ACTIVE == TE_P2MP_DEST_ROW_STATUS (pTeP2mpDestEntry))
            && (TE_NOTINSERVICE == pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                i4MplsTeP2mpTunnelDestRowStatus))
        {
            if (((TE_ADMIN_UP == TE_TNL_ADMIN_STATUS (pTeTnlInfo)) &&
                 (TE_ACTIVE == TE_TNL_ROW_STATUS (pTeTnlInfo))) &&
                (TE_ONE ==
                 TE_P2MP_BRANCH_ACTIVE_DEST_COUNT (pTeP2mpBranchEntry)))
            {
                pOutSegment = MplsGetOutSegmentTableEntry (u4OutSegIndex);
                if (NULL == pOutSegment)
                {
                    MPLS_CMN_UNLOCK ();
                    TE_DBG (TE_LLVL_SET_FAIL,
                            "MplsGetOutSegmentTableEntry failed\r\n");
                    TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                      (UINT1 *)
                                      pTeOldMplsTeP2mpTunnelDestTable);
                    TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                      (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
                    return SNMP_FAILURE;
                }

                pXcEntry = (tXcEntry *) (pOutSegment->pXcIndex);
                if (NULL == pXcEntry)
                {
                    MPLS_CMN_UNLOCK ();
                    TE_DBG (TE_LLVL_SET_FAIL, "XC entry not available \r\n");
                    TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                      (UINT1 *)
                                      pTeOldMplsTeP2mpTunnelDestTable);
                    TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                      (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
                    return SNMP_FAILURE;
                }

                if (MplsDbUpdateP2mpTunnelInHw (pTeTnlInfo, pXcEntry,
                                                TE_OPER_DOWN) == MPLS_FAILURE)
                {
                    MPLS_CMN_UNLOCK ();
                    TE_DBG (TE_LLVL_SET_FAIL,
                            "MplsDbUpdateP2mpTunnelInHw failed\r\n");
                    TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                      (UINT1 *)
                                      pTeOldMplsTeP2mpTunnelDestTable);
                    TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                                      (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
                    return SNMP_FAILURE;
                }
            }
            TE_P2MP_BRANCH_ACTIVE_DEST_COUNT (pTeP2mpBranchEntry) -= TE_ONE;
        }

        TE_P2MP_DEST_ROW_STATUS (pTeP2mpDestEntry) =
            (UINT1) pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestRowStatus;
        pTeMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestRowStatus =
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestRowStatus;
    }
    if (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTeP2mpTunnelDestStorageType !=
        OSIX_FALSE)
    {
        TE_P2MP_DEST_STORAGE_TYPE (pTeP2mpDestEntry) =
            (UINT1) pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestStorageType;
        pTeMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestStorageType =
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestStorageType;
    }

    /*This condtion is to make the row back to ACTIVE after modifying the values */
    if (i4RowMakeActive == TRUE)
    {
        pTeMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestRowStatus = ACTIVE;
    }

    if (TeUtilUpdateMplsTeP2mpTunnelDestTable (pTeOldMplsTeP2mpTunnelDestTable,
                                               pTeMplsTeP2mpTunnelDestTable) !=
        SNMP_SUCCESS)
    {
        /*Restore back with previous values */
        MEMCPY (pTeMplsTeP2mpTunnelDestTable, pTeOldMplsTeP2mpTunnelDestTable,
                sizeof (tTeMplsTeP2mpTunnelDestTable));
        MPLS_CMN_UNLOCK ();
        TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                          (UINT1 *) pTeOldMplsTeP2mpTunnelDestTable);
        TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                          (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
        return SNMP_FAILURE;
    }

    if (TeSetAllMplsTeP2mpTunnelDestTableTrigger
        (pTeSetMplsTeP2mpTunnelDestTable, pTeIsSetMplsTeP2mpTunnelDestTable,
         SNMP_SUCCESS) != SNMP_SUCCESS)
    {
        MPLS_CMN_UNLOCK ();
        TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                          (UINT1 *) pTeOldMplsTeP2mpTunnelDestTable);
        TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                          (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
        return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    TE_DBG (TE_MAIN_ETEXT, "TeSetAllMplsTeP2mpTunnelDestTable : EXIT \n");
    TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                      (UINT1 *) pTeOldMplsTeP2mpTunnelDestTable);
    TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                      (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TeGetAllMplsTeP2mpTunnelBranchPerfTable
 Input       :  The Indices
                pTeMplsTeP2mpTunnelBranchPerfTable
 Output      :  This Routine Take the Indices &
                Gets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT4
TeGetAllMplsTeP2mpTunnelBranchPerfTable (tTeMplsTeP2mpTunnelBranchPerfTable *
                                         pTeGetMplsTeP2mpTunnelBranchPerfTable)
{
    tOutSegment        *pOutSegment = NULL;
    UINT4               u4OutIndex = TE_ZERO;

    TE_DBG (TE_MAIN_ETEXT, "TeGetAllMplsTeP2mpTunnelBranchPerfTable: ENTRY \n");

    /* Check whether the node is already present */
    MEMCPY (&u4OutIndex, pTeGetMplsTeP2mpTunnelBranchPerfTable->MibObject.
            au1MplsTeP2mpTunnelBranchPerfBranch, sizeof (UINT4));

    u4OutIndex = OSIX_NTOHL (u4OutIndex);
    MPLS_CMN_LOCK ();
    pOutSegment = MplsGetOutSegmentTableEntry (u4OutIndex);
    if (NULL == pOutSegment)
    {
        MPLS_CMN_UNLOCK ();
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n MplsGetOutSegmentTableEntry :"
                   "Indexed Entry Unavailable \r\n");
        return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    if (MplsGetOutSegmentPerfStats (u4OutIndex) == MPLS_FAILURE)
    {
        CMNDB_DBG (DEBUG_DEBUG_LEVEL, "\r\n MplsGetOutSegmentPerfStats :"
                   "Stats retrieval failed \r\n");
        return SNMP_FAILURE;
    }

    /* Assign values from the existing node */
    pTeGetMplsTeP2mpTunnelBranchPerfTable->MibObject.
        u4MplsTeP2mpTunnelBranchPerfPackets =
        FSAP_U8_FETCH_LO (&(OUTSEGMENT_PERF_HC_PKTS (pOutSegment)));

    pTeGetMplsTeP2mpTunnelBranchPerfTable->MibObject.
        u8MplsTeP2mpTunnelBranchPerfHCPackets.msn =
        FSAP_U8_FETCH_HI (&(OUTSEGMENT_PERF_HC_PKTS (pOutSegment)));

    pTeGetMplsTeP2mpTunnelBranchPerfTable->MibObject.
        u8MplsTeP2mpTunnelBranchPerfHCPackets.lsn =
        FSAP_U8_FETCH_LO (&(OUTSEGMENT_PERF_HC_PKTS (pOutSegment)));

    pTeGetMplsTeP2mpTunnelBranchPerfTable->MibObject.
        u4MplsTeP2mpTunnelBranchPerfErrors =
        OUTSEGMENT_PERF_ERRORS (pOutSegment);

    pTeGetMplsTeP2mpTunnelBranchPerfTable->MibObject.
        u4MplsTeP2mpTunnelBranchPerfBytes =
        FSAP_U8_FETCH_LO (&(OUTSEGMENT_PERF_HC_OCTETS (pOutSegment)));

    pTeGetMplsTeP2mpTunnelBranchPerfTable->MibObject.
        u8MplsTeP2mpTunnelBranchPerfHCBytes.msn =
        FSAP_U8_FETCH_HI (&(OUTSEGMENT_PERF_HC_OCTETS (pOutSegment)));

    pTeGetMplsTeP2mpTunnelBranchPerfTable->MibObject.
        u8MplsTeP2mpTunnelBranchPerfHCBytes.lsn =
        FSAP_U8_FETCH_LO (&(OUTSEGMENT_PERF_HC_OCTETS (pOutSegment)));

    pTeGetMplsTeP2mpTunnelBranchPerfTable->MibObject.
        u4MplsTeP2mpTunnelBranchDiscontinuityTime =
        OUTSEGMENT_PERF_DISC_TIME (pOutSegment);

    TE_DBG (TE_MAIN_ETEXT, "TeGetAllMplsTeP2mpTunnelBranchPerfTable : EXIT \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TeTestMplsTeP2mpTunnelNotificationEnable
 Input       :  The Indices
                pu4ErrorCode
                i4MplsTeP2mpTunnelNotificationEnable
 Output      :  This Routine will Test 
                the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT4
TeTestMplsTeP2mpTunnelNotificationEnable (UINT4 *pu4ErrorCode,
                                          INT4
                                          i4MplsTeP2mpTunnelNotificationEnable)
{
    TE_DBG (TE_MAIN_ETEXT, "TeTestMplsTeP2mpTunnelNotificationEnable: ENTRY\n");

    /* MPLS_P2MP_LSP_CHANGES - S */
    if ((TE_SNMP_TRUE == i4MplsTeP2mpTunnelNotificationEnable) ||
        (TE_SNMP_FALSE == i4MplsTeP2mpTunnelNotificationEnable))
    {
        return SNMP_SUCCESS;
    }

    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    TE_DBG (TE_MAIN_ETEXT, "TeTestMplsTeP2mpTunnelNotificationEnable: EXIT\n");
    return SNMP_FAILURE;
    /* MPLS_P2MP_LSP_CHANGES - E */
}
