/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tedslow.c,v 1.23 2016/07/22 09:45:48 siva Exp $
 *
 ********************************************************************/

# include  "include.h"
# include  "teincs.h"
# include  "fsmplslw.h"
# include  "stdtelw.h"
# include  "midconst.h"

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsDiffServTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhValidateIndexInstanceFsMplsDiffServTable (UINT4 u4MplsTunnelIndex,
                                             UINT4 u4MplsTunnelInstance,
                                             UINT4 u4MplsTunnelIngressLSRId,
                                             UINT4 u4MplsTunnelEgressLSRId)
{
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) != TE_ADMIN_UP)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    if (nmhValidateIndexInstanceMplsTunnelTable (u4MplsTunnelIndex,
                                                 u4MplsTunnelInstance,
                                                 u4MplsTunnelIngressLSRId,
                                                 u4MplsTunnelEgressLSRId) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsDiffServTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexFsMplsDiffServTable (UINT4 *pu4MplsTunnelIndex,
                                     UINT4 *pu4MplsTunnelInstance,
                                     UINT4 *pu4MplsTunnelIngressLSRId,
                                     UINT4 *pu4MplsTunnelEgressLSRId)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    *pu4MplsTunnelIndex = TE_ZERO;
    *pu4MplsTunnelInstance = TE_ZERO;
    *pu4MplsTunnelIngressLSRId = TE_ZERO;
    *pu4MplsTunnelEgressLSRId = TE_ZERO;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) != TE_ADMIN_UP)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    if (nmhGetFirstIndexMplsTunnelTable
        (pu4MplsTunnelIndex, pu4MplsTunnelInstance, pu4MplsTunnelIngressLSRId,
         pu4MplsTunnelEgressLSRId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo (*pu4MplsTunnelIndex, *pu4MplsTunnelInstance,
                                  *pu4MplsTunnelIngressLSRId,
                                  *pu4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_DS_TNL_INFO (pTeTnlInfo) == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMplsDiffServTable
 Input       :  The Indices
                MplsTunnelIndex
                nextMplsTunnelIndex
                MplsTunnelInstance
                nextMplsTunnelInstance
                MplsTunnelIngressLSRId
                nextMplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                nextMplsTunnelEgressLSRId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexFsMplsDiffServTable (UINT4 u4MplsTunnelIndex,
                                    UINT4 *pu4NextMplsTunnelIndex,
                                    UINT4 u4MplsTunnelInstance,
                                    UINT4 *pu4NextMplsTunnelInstance,
                                    UINT4 u4MplsTunnelIngressLSRId,
                                    UINT4 *pu4NextMplsTunnelIngressLSRId,
                                    UINT4 u4MplsTunnelEgressLSRId,
                                    UINT4 *pu4NextMplsTunnelEgressLSRId)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    *pu4NextMplsTunnelIndex = TE_ZERO;
    *pu4NextMplsTunnelInstance = TE_ZERO;
    *pu4NextMplsTunnelIngressLSRId = TE_ZERO;
    *pu4NextMplsTunnelEgressLSRId = TE_ZERO;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) != TE_ADMIN_UP)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    if (nmhGetNextIndexMplsTunnelTable
        (u4MplsTunnelIndex, pu4NextMplsTunnelIndex, u4MplsTunnelInstance,
         pu4NextMplsTunnelInstance, u4MplsTunnelIngressLSRId,
         pu4NextMplsTunnelIngressLSRId, u4MplsTunnelEgressLSRId,
         pu4NextMplsTunnelEgressLSRId) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    MPLS_CMN_LOCK ();
    pTeTnlInfo =
        TeGetTunnelInfo (*pu4NextMplsTunnelIndex, *pu4NextMplsTunnelInstance,
                         *pu4NextMplsTunnelIngressLSRId,
                         *pu4NextMplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_DS_TNL_INFO (pTeTnlInfo) == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsDiffServClassType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsDiffServClassType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsDiffServClassType (UINT4 u4MplsTunnelIndex,
                               UINT4 u4MplsTunnelInstance,
                               UINT4 u4MplsTunnelIngressLSRId,
                               UINT4 u4MplsTunnelEgressLSRId,
                               INT4 *pi4RetValFsMplsDiffServClassType)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (TE_DS_TNL_INFO (pTeTnlInfo) != NULL)
    {
        *pi4RetValFsMplsDiffServClassType =
            TE_DS_TNL_CLASS_TYPE ((TE_DS_TNL_INFO (pTeTnlInfo)));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsDiffServServiceType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsDiffServServiceType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsDiffServServiceType (UINT4 u4MplsTunnelIndex,
                                 UINT4 u4MplsTunnelInstance,
                                 UINT4 u4MplsTunnelIngressLSRId,
                                 UINT4 u4MplsTunnelEgressLSRId,
                                 INT4 *pi4RetValFsMplsDiffServServiceType)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (TE_DS_TNL_INFO (pTeTnlInfo) != NULL)
    {
        *pi4RetValFsMplsDiffServServiceType =
            TE_DS_TNL_SERVICE_TYPE ((TE_DS_TNL_INFO (pTeTnlInfo)));
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsDiffServLlspPsc
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsDiffServLlspPsc
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsDiffServLlspPsc (UINT4 u4MplsTunnelIndex,
                             UINT4 u4MplsTunnelInstance,
                             UINT4 u4MplsTunnelIngressLSRId,
                             UINT4 u4MplsTunnelEgressLSRId,
                             INT4 *pi4RetValFsMplsDiffServLlspPsc)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (TE_DS_TNL_INFO (pTeTnlInfo) != NULL)
    {
        *pi4RetValFsMplsDiffServLlspPsc =
            TE_DS_TNL_LLSP_PSC_DSCP ((TE_DS_TNL_INFO (pTeTnlInfo)));

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsDiffServElspType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsDiffServElspType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsDiffServElspType (UINT4 u4MplsTunnelIndex,
                              UINT4 u4MplsTunnelInstance,
                              UINT4 u4MplsTunnelIngressLSRId,
                              UINT4 u4MplsTunnelEgressLSRId,
                              INT4 *pi4RetValFsMplsDiffServElspType)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (TE_DS_TNL_INFO (pTeTnlInfo) == NULL)
    {
        return SNMP_FAILURE;
    }
    /*If servicetype is ELSP then only manager can get the E-lsp Type */
    if (TE_DS_TNL_SERVICE_TYPE ((TE_DS_TNL_INFO (pTeTnlInfo))) == TE_DS_ELSP)
    {
        *pi4RetValFsMplsDiffServElspType =
            TE_DS_TNL_ELSP_TYPE ((TE_DS_TNL_INFO (pTeTnlInfo)));
    }
    else
    {
        *pi4RetValFsMplsDiffServElspType = -1;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsDiffServElspListIndex
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsDiffServElspListIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsDiffServElspListIndex (UINT4 u4MplsTunnelIndex,
                                   UINT4 u4MplsTunnelInstance,
                                   UINT4 u4MplsTunnelIngressLSRId,
                                   UINT4 u4MplsTunnelEgressLSRId,
                                   INT4 *pi4RetValFsMplsDiffServElspListIndex)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /*The list index may be 0 also if the ELSP is not the signalled or 
     * if preconfigured doesn't configure per OA basis resources */

    if (TE_DS_TNL_INFO (pTeTnlInfo) != NULL)
    {
        *pi4RetValFsMplsDiffServElspListIndex =
            (INT4)TE_DS_TNL_ELSP_LIST_INDEX ((TE_DS_TNL_INFO (pTeTnlInfo)));

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsDiffServRowStatus
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsDiffServRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsDiffServRowStatus (UINT4 u4MplsTunnelIndex,
                               UINT4 u4MplsTunnelInstance,
                               UINT4 u4MplsTunnelIngressLSRId,
                               UINT4 u4MplsTunnelEgressLSRId,
                               INT4 *pi4RetValFsMplsDiffServRowStatus)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (TE_DS_TNL_INFO (pTeTnlInfo) != NULL)
    {
        *pi4RetValFsMplsDiffServRowStatus =
            TE_DS_TNL_ROW_STATUS ((TE_DS_TNL_INFO (pTeTnlInfo)));
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsDiffServStorageType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsDiffServStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsDiffServStorageType (UINT4 u4MplsTunnelIndex,
                                 UINT4 u4MplsTunnelInstance,
                                 UINT4 u4MplsTunnelIngressLSRId,
                                 UINT4 u4MplsTunnelEgressLSRId,
                                 INT4 *pi4RetValFsMplsDiffServStorageType)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (TE_DS_TNL_INFO (pTeTnlInfo) != NULL)
    {
        *pi4RetValFsMplsDiffServStorageType =
            TE_DS_TNL_STORAGE_TYPE ((TE_DS_TNL_INFO (pTeTnlInfo)));

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsMplsDiffServClassType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValFsMplsDiffServClassType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsDiffServClassType (UINT4 u4MplsTunnelIndex,
                               UINT4 u4MplsTunnelInstance,
                               UINT4 u4MplsTunnelIngressLSRId,
                               UINT4 u4MplsTunnelEgressLSRId,
                               INT4 i4SetValFsMplsDiffServClassType)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    else
    {
        if (TE_TNL_ROW_STATUS (pTeTnlInfo) != TE_ACTIVE)
        {
    if (TE_DS_TNL_INFO (pTeTnlInfo) != NULL)
    {
                if (pTeTnlInfo->pMplsDiffServTnlInfo->u1ClassType != i4SetValFsMplsDiffServClassType)
                {
                    pTeTnlInfo->bIsMbbRequired = TRUE;
                }
                TE_DS_TNL_CLASS_TYPE (TE_DS_TNL_INFO (pTeTnlInfo))
            = (UINT1) i4SetValFsMplsDiffServClassType;
        return SNMP_SUCCESS;
    }
        }
    }	

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsDiffServServiceType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValFsMplsDiffServServiceType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsDiffServServiceType (UINT4 u4MplsTunnelIndex,
                                 UINT4 u4MplsTunnelInstance,
                                 UINT4 u4MplsTunnelIngressLSRId,
                                 UINT4 u4MplsTunnelEgressLSRId,
                                 INT4 i4SetValFsMplsDiffServServiceType)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (TE_DS_TNL_INFO (pTeTnlInfo) != NULL)
    {
        TE_DS_TNL_SERVICE_TYPE ((TE_DS_TNL_INFO (pTeTnlInfo))) = (UINT1)
            i4SetValFsMplsDiffServServiceType;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsDiffServLlspPsc
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValFsMplsDiffServLlspPsc
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsDiffServLlspPsc (UINT4 u4MplsTunnelIndex,
                             UINT4 u4MplsTunnelInstance,
                             UINT4 u4MplsTunnelIngressLSRId,
                             UINT4 u4MplsTunnelEgressLSRId,
                             INT4 i4SetValFsMplsDiffServLlspPsc)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (TE_DS_TNL_INFO (pTeTnlInfo) != NULL)
    {
        TE_DS_TNL_LLSP_PSC_DSCP ((TE_DS_TNL_INFO (pTeTnlInfo)))
            = (UINT1) i4SetValFsMplsDiffServLlspPsc;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsDiffServElspType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValFsMplsDiffServElspType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsDiffServElspType (UINT4 u4MplsTunnelIndex,
                              UINT4 u4MplsTunnelInstance,
                              UINT4 u4MplsTunnelIngressLSRId,
                              UINT4 u4MplsTunnelEgressLSRId,
                              INT4 i4SetValFsMplsDiffServElspType)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (TE_DS_TNL_INFO (pTeTnlInfo) != NULL)
    {
        TE_DS_TNL_ELSP_TYPE ((TE_DS_TNL_INFO (pTeTnlInfo))) =
            (UINT1) i4SetValFsMplsDiffServElspType;
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsDiffServElspListIndex
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValFsMplsDiffServElspListIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsDiffServElspListIndex (UINT4 u4MplsTunnelIndex,
                                   UINT4 u4MplsTunnelInstance,
                                   UINT4 u4MplsTunnelIngressLSRId,
                                   UINT4 u4MplsTunnelEgressLSRId,
                                   INT4 i4SetValFsMplsDiffServElspListIndex)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    /*Validate whether the Elsp list index exists or not */
    if (TE_DS_GBL_ELSP_INFO_LIST_INDEX (i4SetValFsMplsDiffServElspListIndex)
        != (UINT4) i4SetValFsMplsDiffServElspListIndex)
    {
        return SNMP_FAILURE;
    }
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (TE_DS_TNL_INFO (pTeTnlInfo) != NULL)
    {
        TE_DS_TNL_ELSP_LIST_INDEX ((TE_DS_TNL_INFO (pTeTnlInfo))) =
            (UINT4)i4SetValFsMplsDiffServElspListIndex;

        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsDiffServRowStatus
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValFsMplsDiffServRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsDiffServRowStatus (UINT4 u4MplsTunnelIndex,
                               UINT4 u4MplsTunnelInstance,
                               UINT4 u4MplsTunnelIngressLSRId,
                               UINT4 u4MplsTunnelEgressLSRId,
                               INT4 i4SetValFsMplsDiffServRowStatus)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tMplsDiffServTnlInfo *pMplsDsTnlInfo = NULL;

    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        return SNMP_FAILURE;
    }
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    if ((TE_TNL_ROW_STATUS (pTeTnlInfo) == ACTIVE))
    {
        return SNMP_FAILURE;
    }
    pMplsDsTnlInfo = TE_DS_TNL_INFO (pTeTnlInfo);
    if ((pMplsDsTnlInfo == NULL) &&
        (i4SetValFsMplsDiffServRowStatus != CREATE_AND_WAIT))
    {
        return SNMP_FAILURE;
    }

    switch (i4SetValFsMplsDiffServRowStatus)
    {
        case ACTIVE:

            if (TE_DS_TNL_ROW_STATUS (pMplsDsTnlInfo) == ACTIVE)
            {
                return SNMP_SUCCESS;
            }

            /*Check if the service type is intserv or nondiffserv ,then
             * diffServ related parameters should not be configured*/

            if ((TE_DS_TNL_SERVICE_TYPE (pMplsDsTnlInfo) == TE_NON_DIFFSERV_LSP)
                || (TE_DS_TNL_SERVICE_TYPE (pMplsDsTnlInfo) == TE_INTSERV))
            {
                if ((TE_DS_TNL_ELSP_LIST_INDEX (pMplsDsTnlInfo) != TE_ZERO) ||
                    (TE_DS_TNL_CLASS_TYPE (pMplsDsTnlInfo) != TE_ZERO) ||
                    (TE_DS_TNL_LLSP_PSC_DSCP (pMplsDsTnlInfo) != TE_ZERO) ||
                    (TE_DS_TNL_ELSP_TYPE (pMplsDsTnlInfo) != TE_ZERO))
                {
                    return SNMP_FAILURE;
                }
            }

            /*Check if the ElspType is signalled , then ElspListIndex 
             *should not be zero */

            if ((TE_DS_TNL_ELSP_TYPE (pMplsDsTnlInfo) == TE_DS_SIG_ELSP) &&
                (TE_DS_TNL_ELSP_LIST_INDEX (pMplsDsTnlInfo)) == TE_ZERO)
            {
                return SNMP_FAILURE;
            }

            /*If ElspList != 0, then all the nodes within the list 
             *should be active */

            if (TE_DS_TNL_ELSP_LIST_INDEX (pMplsDsTnlInfo) != TE_ZERO)
            {
                if (TeDiffServElspListRowStatusActive
                    ((INT4)TE_DS_TNL_ELSP_LIST_INDEX (pMplsDsTnlInfo)) == TE_FAILURE)
                {
                    return SNMP_FAILURE;
                }

                TE_DS_TNL_ELSP_INFO_LIST_INFO (pMplsDsTnlInfo) =
                    TE_DS_GBL_ELSP_INFO_LIST_ENTRY ((TE_DS_TNL_ELSP_LIST_INDEX
                                                     (pMplsDsTnlInfo)));
            }

            /*If the servicetype is L-lsp and Elspindex is not zero ,
             * then return SNMP_FAILURE */

            if ((TE_DS_TNL_SERVICE_TYPE (pMplsDsTnlInfo) == TE_DS_LLSP) &&
                (TE_DS_TNL_ELSP_LIST_INDEX (pMplsDsTnlInfo) != TE_ZERO))
            {
                return SNMP_FAILURE;
            }

            if ((TE_DS_TNL_SERVICE_TYPE (pMplsDsTnlInfo) != TE_NON_DIFFSERV_LSP)
                && (TE_DS_TNL_SERVICE_TYPE (pMplsDsTnlInfo) != TE_INTSERV))
            {
                /*if the servicetype is L-lsp or signalled E-lsp set 
                 *the flags */
                if ((TE_DS_TNL_SERVICE_TYPE (pMplsDsTnlInfo) == TE_DS_LLSP)
                    || (TE_DS_TNL_SERVICE_TYPE (pMplsDsTnlInfo) == TE_DS_ELSP))
                {

                    TE_DS_TNL_CFG_FLAG (pMplsDsTnlInfo) |= TE_DS_DIFFSERV_TLV;
                }

                /*If classType is not equal to 0 , then set the flags */
                if (TE_DS_TNL_CLASS_TYPE (pMplsDsTnlInfo) != TE_ZERO)
                {
                    TE_DS_TNL_CFG_FLAG (pMplsDsTnlInfo) |= TE_DS_CLASSTYPE_TLV;
                }

                /*check if the resources are configured, 
                 *then set the flag */
                if (TE_DS_TNL_ELSP_LIST_INDEX (pMplsDsTnlInfo) != TE_ZERO)
                {
                    if (TeDiffServChkIsPerOAResrcConf
                        ((INT4)TE_DS_TNL_ELSP_LIST_INDEX (pMplsDsTnlInfo)) ==
                        TE_SUCCESS)
                    {
                        /* Set the flags */
                        TE_DS_TNL_CFG_FLAG (pMplsDsTnlInfo) |=
                            TE_DS_RSRC_PEROA_TLV;
                    }
                }
            }
            /*Increase the count for the number of tunnels */
            if (TE_DS_TNL_ELSP_LIST_INDEX (pMplsDsTnlInfo) != TE_ZERO)
            {
                ++TE_DS_GBL_ELSP_INFO_LIST_TNL_COUNT
                    (TE_DS_TNL_ELSP_LIST_INDEX (pMplsDsTnlInfo));
            }

            TE_DS_TNL_ROW_STATUS (pMplsDsTnlInfo) = ACTIVE;
            return SNMP_SUCCESS;

        case CREATE_AND_WAIT:

            pMplsDsTnlInfo = (tMplsDiffServTnlInfo *)
                TE_ALLOC_MEM_BLOCK (TE_DS_TNL_INFO_POOL_ID);

            if (pMplsDsTnlInfo == NULL)
            {
                return SNMP_FAILURE;
            }

            INIT_TE_DS_TNL_INFO (pMplsDsTnlInfo);

            TE_DS_TNL_INFO (pTeTnlInfo) = pMplsDsTnlInfo;
            TE_DS_TNL_STORAGE_TYPE (pMplsDsTnlInfo) = TE_STORAGE_VOLATILE;
            TE_DS_TNL_ROW_STATUS (pMplsDsTnlInfo) = NOT_READY;
            return SNMP_SUCCESS;

        case NOT_IN_SERVICE:

            if ((TE_TNL_ROW_STATUS (pTeTnlInfo) == NOT_READY) ||
                (TE_TNL_ROW_STATUS (pTeTnlInfo) == NOT_IN_SERVICE))
            {
                TE_DS_TNL_ROW_STATUS (pMplsDsTnlInfo) = NOT_IN_SERVICE;
                return SNMP_SUCCESS;
            }

            if (TE_DS_TNL_ROW_STATUS (pMplsDsTnlInfo) == ACTIVE)
            {
                TE_DS_TNL_CFG_FLAG (pMplsDsTnlInfo) = TE_ZERO;

                TE_DS_TNL_ROW_STATUS (pMplsDsTnlInfo) = NOT_IN_SERVICE;
                return SNMP_SUCCESS;
            }

            return SNMP_FAILURE;

        case DESTROY:

            if (TE_DS_TNL_ELSP_LIST_INDEX (pMplsDsTnlInfo) != TE_ZERO)
            {
                --TE_DS_GBL_ELSP_INFO_LIST_TNL_COUNT
                    (TE_DS_TNL_ELSP_LIST_INDEX (pMplsDsTnlInfo));
            }

            TE_REL_MEM_BLOCK (TE_DS_TNL_INFO_POOL_ID, (UINT1 *) pMplsDsTnlInfo);

            TE_DS_TNL_INFO (pTeTnlInfo) = NULL;

            return SNMP_SUCCESS;
	default :
	    return SNMP_FAILURE;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsDiffServStorageType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValFsMplsDiffServStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsDiffServStorageType (UINT4 u4MplsTunnelIndex,
                                 UINT4 u4MplsTunnelInstance,
                                 UINT4 u4MplsTunnelIngressLSRId,
                                 UINT4 u4MplsTunnelEgressLSRId,
                                 INT4 i4SetValFsMplsDiffServStorageType)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        return SNMP_FAILURE;
    }
    if (TE_DS_TNL_INFO (pTeTnlInfo) != NULL)
    {
        if (TE_DS_TNL_ROW_STATUS ((TE_DS_TNL_INFO (pTeTnlInfo))) == NOT_READY)
        {
            TE_DS_TNL_STORAGE_TYPE (TE_DS_TNL_INFO (pTeTnlInfo)) = (UINT1)
                i4SetValFsMplsDiffServStorageType;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMplsDiffServClassType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValFsMplsDiffServClassType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsDiffServClassType (UINT4 *pu4ErrorCode,
                                  UINT4 u4MplsTunnelIndex,
                                  UINT4 u4MplsTunnelInstance,
                                  UINT4 u4MplsTunnelIngressLSRId,
                                  UINT4 u4MplsTunnelEgressLSRId,
                                  INT4 i4TestValFsMplsDiffServClassType)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_AUTHORIZATION_ERROR;
        return SNMP_FAILURE;
    }

    switch (i4TestValFsMplsDiffServClassType)
    {
        case TE_DS_CLASS_TYPE0:
        case TE_DS_CLASS_TYPE1:
        case TE_DS_CLASS_TYPE2:
        case TE_DS_CLASS_TYPE3:
        case TE_DS_CLASS_TYPE4:
        case TE_DS_CLASS_TYPE5:
        case TE_DS_CLASS_TYPE6:
        case TE_DS_CLASS_TYPE7:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (TE_DS_TNL_INFO (pTeTnlInfo) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (TE_DS_TNL_ROW_STATUS (TE_DS_TNL_INFO (pTeTnlInfo)) == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsDiffServServiceType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValFsMplsDiffServServiceType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsDiffServServiceType (UINT4 *pu4ErrorCode,
                                    UINT4 u4MplsTunnelIndex,
                                    UINT4 u4MplsTunnelInstance,
                                    UINT4 u4MplsTunnelIngressLSRId,
                                    UINT4 u4MplsTunnelEgressLSRId,
                                    INT4 i4TestValFsMplsDiffServServiceType)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_AUTHORIZATION_ERROR;
        return SNMP_FAILURE;
    }

    switch (i4TestValFsMplsDiffServServiceType)
    {
        case TE_DS_ELSP:
        case TE_DS_LLSP:
        case TE_NON_DIFFSERV_LSP:
        case TE_INTSERV:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;

    }

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (TE_DS_TNL_INFO (pTeTnlInfo) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (TE_DS_TNL_ROW_STATUS ((TE_DS_TNL_INFO (pTeTnlInfo))) == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsDiffServLlspPsc
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValFsMplsDiffServLlspPsc
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsDiffServLlspPsc (UINT4 *pu4ErrorCode,
                                UINT4 u4MplsTunnelIndex,
                                UINT4 u4MplsTunnelInstance,
                                UINT4 u4MplsTunnelIngressLSRId,
                                UINT4 u4MplsTunnelEgressLSRId,
                                INT4 i4TestValFsMplsDiffServLlspPsc)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_AUTHORIZATION_ERROR;
        return SNMP_FAILURE;
    }

    switch (i4TestValFsMplsDiffServLlspPsc)
    {
        case TE_DS_DF_DSCP:
        case TE_DS_CS1_DSCP:
        case TE_DS_CS2_DSCP:
        case TE_DS_CS3_DSCP:
        case TE_DS_CS4_DSCP:
        case TE_DS_CS5_DSCP:
        case TE_DS_CS6_DSCP:
        case TE_DS_CS7_DSCP:
        case TE_DS_EF_DSCP:
        case TE_DS_AF11_DSCP:
        case TE_DS_AF21_DSCP:
        case TE_DS_AF31_DSCP:
        case TE_DS_AF41_DSCP:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (TE_DS_TNL_INFO (pTeTnlInfo) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (TE_DS_TNL_ROW_STATUS ((TE_DS_TNL_INFO (pTeTnlInfo))) == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsDiffServElspType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValFsMplsDiffServElspType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsDiffServElspType (UINT4 *pu4ErrorCode,
                                 UINT4 u4MplsTunnelIndex,
                                 UINT4 u4MplsTunnelInstance,
                                 UINT4 u4MplsTunnelIngressLSRId,
                                 UINT4 u4MplsTunnelEgressLSRId,
                                 INT4 i4TestValFsMplsDiffServElspType)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_AUTHORIZATION_ERROR;
        return SNMP_FAILURE;
    }

    switch (i4TestValFsMplsDiffServElspType)
    {
        case TE_DS_PRECONF_ELSP:
        case TE_DS_SIG_ELSP:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (TE_DS_TNL_INFO (pTeTnlInfo) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

    if (TE_DS_TNL_ROW_STATUS ((TE_DS_TNL_INFO (pTeTnlInfo))) == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsDiffServElspListIndex
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValFsMplsDiffServElspListIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsDiffServElspListIndex (UINT4 *pu4ErrorCode,
                                      UINT4 u4MplsTunnelIndex,
                                      UINT4 u4MplsTunnelInstance,
                                      UINT4 u4MplsTunnelIngressLSRId,
                                      UINT4 u4MplsTunnelEgressLSRId,
                                      INT4 i4TestValFsMplsDiffServElspListIndex)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_AUTHORIZATION_ERROR;
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMplsDiffServElspListIndex >
         (INT4) TE_MAX_DS_ELSPS (gTeGblInfo)) ||
        (i4TestValFsMplsDiffServElspListIndex < TE_ONE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    /*Validate whether the list index is valid or not */
    /*Whether it is needed to check that each node is in active state or not */

    if (TE_DS_GBL_ELSP_INFO_LIST_INDEX (i4TestValFsMplsDiffServElspListIndex)
        == TE_ZERO)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }

    if (TE_DS_TNL_INFO (pTeTnlInfo) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }
    if (TE_DS_TNL_ROW_STATUS ((TE_DS_TNL_INFO (pTeTnlInfo))) == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsDiffServRowStatus
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValFsMplsDiffServRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsDiffServRowStatus (UINT4 *pu4ErrorCode,
                                  UINT4 u4MplsTunnelIndex,
                                  UINT4 u4MplsTunnelInstance,
                                  UINT4 u4MplsTunnelIngressLSRId,
                                  UINT4 u4MplsTunnelEgressLSRId,
                                  INT4 i4TestValFsMplsDiffServRowStatus)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tMplsDiffServTnlInfo *pMplsDsTnlInfo = NULL;

    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        return SNMP_FAILURE;
    }
    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    pMplsDsTnlInfo = TE_DS_TNL_INFO (pTeTnlInfo);
    switch (i4TestValFsMplsDiffServRowStatus)
    {
        case CREATE_AND_WAIT:
            if (pMplsDsTnlInfo != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                return SNMP_FAILURE;
            }
            break;

        case ACTIVE:
        case NOT_IN_SERVICE:
        case DESTROY:
            if (pMplsDsTnlInfo == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return SNMP_FAILURE;
            }
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsDiffServStorageType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValFsMplsDiffServStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsDiffServStorageType (UINT4 *pu4ErrorCode,
                                    UINT4 u4MplsTunnelIndex,
                                    UINT4 u4MplsTunnelInstance,
                                    UINT4 u4MplsTunnelIngressLSRId,
                                    UINT4 u4MplsTunnelEgressLSRId,
                                    INT4 i4TestValFsMplsDiffServStorageType)
{
    UNUSED_PARAM (u4MplsTunnelIndex);
    UNUSED_PARAM (u4MplsTunnelInstance);
    UNUSED_PARAM (u4MplsTunnelIngressLSRId);
    UNUSED_PARAM (u4MplsTunnelEgressLSRId);

    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_AUTHORIZATION_ERROR;
        return SNMP_FAILURE;
    }

    switch (i4TestValFsMplsDiffServStorageType)
    {
        case TE_STORAGE_VOLATILE:
        case TE_STORAGE_NONVOLATILE:
        case TE_STORAGE_OTHER:
        case TE_STORAGE_PERMANENT:
        case TE_STORAGE_READONLY:
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsDiffServTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsDiffServTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMplsDiffServElspInfoTable. */
/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsDiffServElspInfoTable
 Input       :  The Indices
                FsMplsDiffServElspInfoListIndex
                FsMplsDiffServElspInfoIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhValidateIndexInstanceFsMplsDiffServElspInfoTable (INT4
                                                     i4FsMplsDiffServElspInfoListIndex,
                                                     INT4
                                                     i4FsMplsDiffServElspInfoIndex)
{

    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        return SNMP_FAILURE;
    }

    if ((i4FsMplsDiffServElspInfoListIndex > (INT4)
         TE_MAX_DS_ELSPS (gTeGblInfo)) ||
        (i4FsMplsDiffServElspInfoListIndex < TE_ONE))
    {
        return SNMP_FAILURE;
    }

    if ((i4FsMplsDiffServElspInfoIndex >
         MAX_DS_EXP) || (i4FsMplsDiffServElspInfoIndex < TE_DS_MIN_EXP))
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsDiffServElspInfoTable
 Input       :  The Indices
                FsMplsDiffServElspInfoListIndex
                FsMplsDiffServElspInfoIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexFsMplsDiffServElspInfoTable (INT4
                                             *pi4FsMplsDiffServElspInfoListIndex,
                                             INT4
                                             *pi4FsMplsDiffServElspInfoIndex)
{
    UINT1               u1FirstElspInfoIndexFound = TE_FALSE;
    UINT4               u4ElspInfoArrayIndex;
    tTeSll             *pList = NULL;
    tMplsDiffServElspInfo *pElspInfoNode = NULL;

    *pi4FsMplsDiffServElspInfoListIndex = TE_ZERO;
    *pi4FsMplsDiffServElspInfoIndex = TE_ZERO;

    if (TE_ADMIN_STATUS (gTeGblInfo) != TE_ADMIN_UP)
    {
        return SNMP_FAILURE;
    }

    for (u4ElspInfoArrayIndex = TE_ONE;
         u4ElspInfoArrayIndex <= TE_MAX_DS_ELSPS (gTeGblInfo);
         u4ElspInfoArrayIndex++)
    {
        if (TE_DS_GBL_ELSP_INFO_LIST_INDEX (u4ElspInfoArrayIndex) ==
            u4ElspInfoArrayIndex)
        {
            if (TE_DS_GBL_ELSP_CONFIG_FLAG(u4ElspInfoArrayIndex) == TE_ONE)
            {	
            *pi4FsMplsDiffServElspInfoListIndex = (INT4)u4ElspInfoArrayIndex;

            /* Scan through the ElspInfoNode to find out the least ElspIndex. */

            pList = (TE_DS_GBL_ELSP_INFO_LIST (u4ElspInfoArrayIndex));
            TE_SLL_SCAN (pList, pElspInfoNode, tMplsDiffServElspInfo *)
            {
                if (u1FirstElspInfoIndexFound == TE_FALSE)
                {
                    *pi4FsMplsDiffServElspInfoIndex =
                        TE_DS_ELSP_INFO_INDEX (pElspInfoNode);
                    u1FirstElspInfoIndexFound = TE_TRUE;
                }

                else if (TE_DS_ELSP_INFO_INDEX (pElspInfoNode)
                         < *pi4FsMplsDiffServElspInfoIndex)
                {
                    *pi4FsMplsDiffServElspInfoIndex =
                        TE_DS_ELSP_INFO_INDEX (pElspInfoNode);
                }
                return SNMP_SUCCESS;
            }
        }
    }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMplsDiffServElspInfoTable
 Input       :  The Indices
                FsMplsDiffServElspInfoListIndex
                nextFsMplsDiffServElspInfoListIndex
                FsMplsDiffServElspInfoIndex
                nextFsMplsDiffServElspInfoIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexFsMplsDiffServElspInfoTable (INT4
                                            i4FsMplsDiffServElspInfoListIndex,
                                            INT4
                                            *pi4NextFsMplsDiffServElspInfoListIndex,
                                            INT4 i4FsMplsDiffServElspInfoIndex,
                                            INT4
                                            *pi4NextFsMplsDiffServElspInfoIndex)
{
    UINT1               u1FirstElspInfoIndexFound = TE_FALSE;
    UINT1               u1FirstLoopEntry = TE_TRUE;
    UINT4               u4ElspInfoArrayIndex;
    tTeSll             *pList = NULL;
    tMplsDiffServElspInfo *pElspInfoNode = NULL;

    *pi4NextFsMplsDiffServElspInfoListIndex = TE_ZERO;
    *pi4NextFsMplsDiffServElspInfoIndex = TE_ZERO;

    if (TE_ADMIN_STATUS (gTeGblInfo) != TE_ADMIN_UP)
    {
        return SNMP_FAILURE;
    }

    pList = (TE_DS_GBL_ELSP_INFO_LIST (i4FsMplsDiffServElspInfoListIndex));

    TE_SLL_SCAN (pList, pElspInfoNode, tMplsDiffServElspInfo *)
    {
        if (TE_DS_ELSP_INFO_INDEX (pElspInfoNode) >
            i4FsMplsDiffServElspInfoIndex)
        {
            if (u1FirstLoopEntry == TE_TRUE)
            {
                *pi4NextFsMplsDiffServElspInfoIndex =
                    TE_DS_ELSP_INFO_INDEX (pElspInfoNode);
                u1FirstLoopEntry = TE_FALSE;
                continue;
            }

            if (TE_DS_ELSP_INFO_INDEX (pElspInfoNode) <
                *pi4NextFsMplsDiffServElspInfoIndex)
            {
                *pi4NextFsMplsDiffServElspInfoIndex =
                    TE_DS_ELSP_INFO_INDEX (pElspInfoNode);
            }
        }
    }

    if (u1FirstLoopEntry == TE_FALSE)
    {
        *pi4NextFsMplsDiffServElspInfoListIndex =
            i4FsMplsDiffServElspInfoListIndex;
        return SNMP_SUCCESS;
    }

    /* In the current List, the ElspIndex was the largest. 
     * The next ElspIndex is the first ElspIndex for the remaining Lists */

    for (u4ElspInfoArrayIndex = (UINT4)(i4FsMplsDiffServElspInfoListIndex + TE_ONE);
         u4ElspInfoArrayIndex <= TE_MAX_DS_ELSPS (gTeGblInfo);
         u4ElspInfoArrayIndex++)
    {

        /* If a Elsp List is configured then the ElspIndex will be the
         * same as the Array Index (the array index runs from 1 to MAX).
         */

        if (TE_DS_GBL_ELSP_INFO_LIST_INDEX (u4ElspInfoArrayIndex) ==
            u4ElspInfoArrayIndex)
        {
            if (TE_DS_GBL_ELSP_CONFIG_FLAG(u4ElspInfoArrayIndex) == TE_ONE)
            {
            /* Scan through the ElspNodes to find out the least Elsp Index. */
            pList = (TE_DS_GBL_ELSP_INFO_LIST (u4ElspInfoArrayIndex));
            TE_SLL_SCAN (pList, pElspInfoNode, tMplsDiffServElspInfo *)
            {
                if (u1FirstElspInfoIndexFound == TE_FALSE)
                {
                    *pi4NextFsMplsDiffServElspInfoIndex =
                        TE_DS_ELSP_INFO_INDEX (pElspInfoNode);
                    *pi4NextFsMplsDiffServElspInfoListIndex =
                        (INT4)u4ElspInfoArrayIndex;
                    u1FirstElspInfoIndexFound = TE_TRUE;
                    continue;
                }
                if (TE_DS_ELSP_INFO_INDEX (pElspInfoNode)
                    < *pi4NextFsMplsDiffServElspInfoIndex)
                {
                    *pi4NextFsMplsDiffServElspInfoIndex =
                        TE_DS_ELSP_INFO_INDEX (pElspInfoNode);
                    *pi4NextFsMplsDiffServElspInfoListIndex =
                        (INT4)u4ElspInfoArrayIndex;
                }
            return SNMP_SUCCESS;
        }
    }
        }
    }

    return SNMP_FAILURE;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsDiffServElspInfoPHB
 Input       :  The Indices
                FsMplsDiffServElspInfoListIndex
                FsMplsDiffServElspInfoIndex

                The Object 
                retValFsMplsDiffServElspInfoPHB
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsDiffServElspInfoPHB (INT4 i4FsMplsDiffServElspInfoListIndex,
                                 INT4 i4FsMplsDiffServElspInfoIndex,
                                 INT4 *pi4RetValFsMplsDiffServElspInfoPHB)
{
    tMplsDiffServElspInfo *pElspInfoNode = NULL;

    if (TeDiffServGetElspInfoNode (i4FsMplsDiffServElspInfoListIndex,
                                   i4FsMplsDiffServElspInfoIndex,
                                   &pElspInfoNode) == TE_SUCCESS)
    {

        *pi4RetValFsMplsDiffServElspInfoPHB =
            TE_DS_ELSP_INFO_PHB_DSCP (pElspInfoNode);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsDiffServElspInfoResourcePointer
 Input       :  The Indices
                FsMplsDiffServElspInfoListIndex
                FsMplsDiffServElspInfoIndex

                The Object 
                retValFsMplsDiffServElspInfoResourcePointer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsDiffServElspInfoResourcePointer (INT4
                                             i4FsMplsDiffServElspInfoListIndex,
                                             INT4 i4FsMplsDiffServElspInfoIndex,
                                             tSNMP_OID_TYPE *
                                             pRetValFsMplsDiffServElspInfoResourcePointer)
{
    tMplsDiffServElspInfo *pElspInfoNode = NULL;

    if (TeDiffServGetElspInfoNode (i4FsMplsDiffServElspInfoListIndex,
                                   i4FsMplsDiffServElspInfoIndex,
                                   &pElspInfoNode) == TE_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /*To check: */
    if (pElspInfoNode->pTeTrfcParams == NULL)
    {
        CPY_TO_OID (pRetValFsMplsDiffServElspInfoResourcePointer,
                    "    ", TE_ONE);
        return SNMP_SUCCESS;

    }

    au4DiffServElspInfoTableOid[TE_DS_ELSPINFO_TABLE_DEF_OFFSET - TE_ONE] =
        (UINT4) (TE_DS_ELSP_INFO_PSC_TRFC_PARAMS (pElspInfoNode)->
                 u4TrfcParamIndex);

    CPY_TO_OID (pRetValFsMplsDiffServElspInfoResourcePointer,
                au4DiffServElspInfoTableOid, TE_DS_ELSPINFO_TABLE_DEF_OFFSET);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsDiffServElspInfoRowStatus
 Input       :  The Indices
                FsMplsDiffServElspInfoListIndex
                FsMplsDiffServElspInfoIndex

                The Object 
                retValFsMplsDiffServElspInfoRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsDiffServElspInfoRowStatus (INT4 i4FsMplsDiffServElspInfoListIndex,
                                       INT4 i4FsMplsDiffServElspInfoIndex,
                                       INT4
                                       *pi4RetValFsMplsDiffServElspInfoRowStatus)
{
    tMplsDiffServElspInfo *pElspInfoNode = NULL;
    if (TeDiffServGetElspInfoNode (i4FsMplsDiffServElspInfoListIndex,
                                   i4FsMplsDiffServElspInfoIndex,
                                   &pElspInfoNode) == TE_SUCCESS)
    {
        *pi4RetValFsMplsDiffServElspInfoRowStatus =
            (INT4) TE_DS_ELSP_INFO_ROW_STATUS (pElspInfoNode);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsDiffServElspInfoStorageType
 Input       :  The Indices
                FsMplsDiffServElspInfoListIndex
                FsMplsDiffServElspInfoIndex

                The Object 
                retValFsMplsDiffServElspInfoStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsDiffServElspInfoStorageType (INT4 i4FsMplsDiffServElspInfoListIndex,
                                         INT4 i4FsMplsDiffServElspInfoIndex,
                                         INT4
                                         *pi4RetValFsMplsDiffServElspInfoStorageType)
{
    tMplsDiffServElspInfo *pElspInfoNode = NULL;

    if (TeDiffServGetElspInfoNode (i4FsMplsDiffServElspInfoListIndex,
                                   i4FsMplsDiffServElspInfoIndex,
                                   &pElspInfoNode) == TE_SUCCESS)
    {
        *pi4RetValFsMplsDiffServElspInfoStorageType =
            (INT4) TE_DS_ELSP_INFO_STORAGE_TYPE (pElspInfoNode);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsMplsDiffServElspInfoPHB
 Input       :  The Indices
                FsMplsDiffServElspInfoListIndex
                FsMplsDiffServElspInfoIndex

                The Object 
                setValFsMplsDiffServElspInfoPHB
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsDiffServElspInfoPHB (INT4 i4FsMplsDiffServElspInfoListIndex,
                                 INT4 i4FsMplsDiffServElspInfoIndex,
                                 INT4 i4SetValFsMplsDiffServElspInfoPHB)
{
    tMplsDiffServElspInfo *pElspInfoNode = NULL;

    /* For Backward Compatiability only, remove in 9_2_0 */
    if(MAX_EXP_BIT_VAL_8 == i4FsMplsDiffServElspInfoIndex)
    {
        return SNMP_SUCCESS;
    }

    if (TeDiffServGetElspInfoNode (i4FsMplsDiffServElspInfoListIndex,
                                   i4FsMplsDiffServElspInfoIndex,
                                   &pElspInfoNode) == TE_SUCCESS)
    {
        if (TE_DS_ELSP_INFO_ROW_STATUS (pElspInfoNode) == NOT_READY)
        {
            TE_DS_ELSP_INFO_PHB_DSCP (pElspInfoNode) =
                (UINT1) i4SetValFsMplsDiffServElspInfoPHB;
            return SNMP_SUCCESS;
        }
    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsDiffServElspInfoResourcePointer
 Input       :  The Indices
                FsMplsDiffServElspInfoListIndex
                FsMplsDiffServElspInfoIndex

                The Object 
                setValFsMplsDiffServElspInfoResourcePointer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsDiffServElspInfoResourcePointer (INT4
                                             i4FsMplsDiffServElspInfoListIndex,
                                             INT4
                                             i4FsMplsDiffServElspInfoIndex,
                                             tSNMP_OID_TYPE *
                                             pSetValFsMplsDiffServElspInfoResourcePointer)
{
    tMplsDiffServElspInfo *pElspInfoNode = NULL;
    UINT4               u4ResourceIndex;
    UINT4               u4Length;

    if (TeDiffServGetElspInfoNode (i4FsMplsDiffServElspInfoListIndex,
                                   i4FsMplsDiffServElspInfoIndex,
                                   &pElspInfoNode) == TE_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /*If resource manager is not configured for perOA based resources, 
     * return SNMP_FAILURE */

    /* if (RESOURCE_MANAGEMENT_TYPE != TE_DS_PEROABASED_RESOURCES)
       return SNMP_FAILURE; */

    u4Length = pSetValFsMplsDiffServElspInfoResourcePointer->u4_Length;
    u4ResourceIndex =
        (pSetValFsMplsDiffServElspInfoResourcePointer->
         pu4_OidList[u4Length - TE_ONE]);

    if (TE_DS_ELSP_INFO_ROW_STATUS (pElspInfoNode) == NOT_READY)
    {

        if (u4ResourceIndex == TE_ZERO)
        {
            pElspInfoNode->u4ResourceIndex = (UINT2) u4ResourceIndex;
            pElspInfoNode->pTeTrfcParams = NULL;
            return SNMP_SUCCESS;
        }

        pElspInfoNode->u4ResourceIndex = (UINT2) u4ResourceIndex;
        pElspInfoNode->pTeTrfcParams = TE_TRFC_PARAMS_PTR (u4ResourceIndex);
        return SNMP_SUCCESS;

    }

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsDiffServElspInfoRowStatus
 Input       :  The Indices
                FsMplsDiffServElspInfoListIndex
                FsMplsDiffServElspInfoIndex

                The Object 
                setValFsMplsDiffServElspInfoRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsDiffServElspInfoRowStatus (INT4 i4FsMplsDiffServElspInfoListIndex,
                                       INT4 i4FsMplsDiffServElspInfoIndex,
                                       INT4
                                       i4SetValFsMplsDiffServElspInfoRowStatus)
{
    tMplsDiffServElspInfo *pElspInfoNode = NULL;

    /* For Backward Compatiability only, remove in 9_2_0 */
    if(MAX_EXP_BIT_VAL_8 == i4FsMplsDiffServElspInfoIndex)
    {
        return SNMP_SUCCESS;
    }

    switch (i4SetValFsMplsDiffServElspInfoRowStatus)
    {
        case CREATE_AND_WAIT:

            if (TeDiffServGetElspInfoNode (i4FsMplsDiffServElspInfoListIndex,
                                           i4FsMplsDiffServElspInfoIndex,
                                           &pElspInfoNode) == TE_SUCCESS)
            {
                return SNMP_FAILURE;
            }

            /* No tunnel should be associated with this ElspList. */
            if (TE_DS_GBL_ELSP_INFO_LIST_TNL_COUNT
                (i4FsMplsDiffServElspInfoListIndex) == TE_ZERO)
            {
                if (TE_SLL_COUNT (TE_DS_GBL_ELSP_INFO_LIST
                                  (i4FsMplsDiffServElspInfoListIndex))
                    == TE_ZERO)
                {
                    /* Manager has to create Elsp List table Row by calling the 
                     * get ElspIndexNext function and has to configure that 
                     * Elsp Index.
                     */
                    /* The Elsp List is Empty. */
                    TE_SLL_INIT (TE_DS_GBL_ELSP_INFO_LIST
                                 (i4FsMplsDiffServElspInfoListIndex));

                    TE_DS_GBL_ELSP_INFO_LIST_INDEX
                        (i4FsMplsDiffServElspInfoListIndex)
                        = (UINT4)i4FsMplsDiffServElspInfoListIndex;
                }

                pElspInfoNode = (tMplsDiffServElspInfo *)
                    TE_ALLOC_MEM_BLOCK (TE_DS_ELSP_INFO_POOL_ID);

                if (pElspInfoNode == NULL)
                {
                    MEMSET ((UINT1 *)
                            TE_DS_GBL_ELSP_INFO_LIST_ENTRY
                            (i4FsMplsDiffServElspInfoListIndex), TE_ZERO,
                            sizeof (tMplsDiffServElspList));
                    return SNMP_FAILURE;
                }

                TE_SLL_INIT_NODE (&(pElspInfoNode->ElspInfoNext));
                TE_DS_GBL_ELSP_CONFIG_FLAG (i4FsMplsDiffServElspInfoListIndex) = TE_ONE;
                TE_DS_ELSP_INFO_INDEX (pElspInfoNode) = (UINT1)
                    i4FsMplsDiffServElspInfoIndex;
                TE_DS_ELSP_INFO_ROW_STATUS (pElspInfoNode) = NOT_READY;
                TE_DS_ELSP_INFO_STORAGE_TYPE (pElspInfoNode) =
                    TE_STORAGE_VOLATILE;
                TE_SLL_ADD (TE_DS_GBL_ELSP_INFO_LIST
                            (i4FsMplsDiffServElspInfoListIndex),
                            &(pElspInfoNode->ElspInfoNext));
                return SNMP_SUCCESS;
            }
            break;

        case ACTIVE:

            if (TeDiffServGetElspInfoNode (i4FsMplsDiffServElspInfoListIndex,
                                           i4FsMplsDiffServElspInfoIndex,
                                           &pElspInfoNode) == TE_FAILURE)
            {
                return SNMP_FAILURE;
            }

            /* If the ElspInfo Row Status is going to be ACTIVE, 
             * then the present 
             * Row status must be NOTREADY or NOT_IN _SERVICE state.
             */

            if ((TE_DS_ELSP_INFO_ROW_STATUS (pElspInfoNode) == NOT_READY)
                || (TE_DS_ELSP_INFO_ROW_STATUS (pElspInfoNode)
                    == NOT_IN_SERVICE))
            {
                TE_DS_ELSP_INFO_ROW_STATUS (pElspInfoNode) = ACTIVE;
                return SNMP_SUCCESS;
            }
            return SNMP_FAILURE;

        case NOT_IN_SERVICE:

            if (TeDiffServGetElspInfoNode (i4FsMplsDiffServElspInfoListIndex,
                                           i4FsMplsDiffServElspInfoIndex,
                                           &pElspInfoNode) == TE_FAILURE)
            {
                return SNMP_FAILURE;
            }
            /* If the ElspInfo Row Status is going to be NOTINSERVICE, then the 
             * present Row status must be ACTIVE and no tunnel should be 
             * associated with the Elsp list. Or the Status should be in 
             * NOTINSERVICE.
             */

            if (TE_DS_ELSP_INFO_ROW_STATUS (pElspInfoNode) == ACTIVE)
            {
                if ((TE_DS_GBL_ELSP_INFO_LIST_TNL_COUNT
                     (i4FsMplsDiffServElspInfoListIndex)) == TE_ZERO)
                {
                    TE_DS_ELSP_INFO_ROW_STATUS (pElspInfoNode) = NOT_IN_SERVICE;
                    return SNMP_SUCCESS;
                }
            }

            else if ((TE_DS_ELSP_INFO_ROW_STATUS (pElspInfoNode) ==
                      NOT_IN_SERVICE) ||
                     (TE_DS_ELSP_INFO_ROW_STATUS (pElspInfoNode) ==
                      CREATE_AND_WAIT))

            {
                TE_DS_ELSP_INFO_ROW_STATUS (pElspInfoNode) = NOT_IN_SERVICE;
                return SNMP_SUCCESS;
            }
            return SNMP_FAILURE;

        case DESTROY:

            if (TeDiffServGetElspInfoNode (i4FsMplsDiffServElspInfoListIndex,
                                           i4FsMplsDiffServElspInfoIndex,
                                           &pElspInfoNode) == TE_FAILURE)
            {
                return SNMP_FAILURE;
            }

            if ((TE_DS_ELSP_INFO_ROW_STATUS (pElspInfoNode) == ACTIVE)
                || (TE_DS_ELSP_INFO_ROW_STATUS (pElspInfoNode) ==
                    NOT_IN_SERVICE)
                || (TE_DS_ELSP_INFO_ROW_STATUS (pElspInfoNode) == NOT_READY))
            {
                if (TE_DS_GBL_ELSP_INFO_LIST_TNL_COUNT
                    (i4FsMplsDiffServElspInfoListIndex) == TE_ZERO)
                {

                    TE_SLL_DELETE (TE_DS_GBL_ELSP_INFO_LIST
                                   (i4FsMplsDiffServElspInfoListIndex),
                                   &(pElspInfoNode->ElspInfoNext));

                    MEMSET (pElspInfoNode, TE_ZERO,
                            sizeof (tMplsDiffServElspInfo));
                    TE_REL_MEM_BLOCK (TE_DS_ELSP_INFO_POOL_ID,
                                      (UINT1 *) pElspInfoNode);

                    if (TE_SLL_COUNT
                        (TE_DS_GBL_ELSP_INFO_LIST
                         (i4FsMplsDiffServElspInfoListIndex)) == TE_ZERO)
                    {

                        MEMSET ((VOID *)
                                (TE_DS_GBL_ELSP_INFO_LIST_ENTRY
                                 (i4FsMplsDiffServElspInfoListIndex)),
                                TE_ZERO, sizeof (tMplsDiffServElspList));
                    }

                    return SNMP_SUCCESS;
                }
            }
            return SNMP_FAILURE;

        default:
            return SNMP_FAILURE;

    }
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsDiffServElspInfoStorageType
 Input       :  The Indices
                FsMplsDiffServElspInfoListIndex
                FsMplsDiffServElspInfoIndex

                The Object 
                setValFsMplsDiffServElspInfoStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsDiffServElspInfoStorageType (INT4
                                         i4FsMplsDiffServElspInfoListIndex,
                                         INT4 i4FsMplsDiffServElspInfoIndex,
                                         INT4
                                         i4SetValFsMplsDiffServElspInfoStorageType)
{
    tMplsDiffServElspInfo *pElspInfoNode = NULL;

    if (TeDiffServGetElspInfoNode (i4FsMplsDiffServElspInfoListIndex,
                                   i4FsMplsDiffServElspInfoIndex,
                                   &pElspInfoNode) == TE_SUCCESS)
    {

        TE_DS_ELSP_INFO_STORAGE_TYPE (pElspInfoNode) =
            (UINT1) (i4SetValFsMplsDiffServElspInfoStorageType);
        return SNMP_SUCCESS;
    }

    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMplsDiffServElspInfoPHB
 Input       :  The Indices
                FsMplsDiffServElspInfoListIndex
                FsMplsDiffServElspInfoIndex

                The Object 
                testValFsMplsDiffServElspInfoPHB
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsDiffServElspInfoPHB (UINT4 *pu4ErrorCode,
                                    INT4 i4FsMplsDiffServElspInfoListIndex,
                                    INT4 i4FsMplsDiffServElspInfoIndex,
                                    INT4 i4TestValFsMplsDiffServElspInfoPHB)
{

    tMplsDiffServElspInfo *pElspInfoNode = NULL;

    /* For Backward Compatiability only, remove in 9_2_0 */
    if(MAX_EXP_BIT_VAL_8 == i4FsMplsDiffServElspInfoIndex)
    {
        return SNMP_SUCCESS;
    }

    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_AUTHORIZATION_ERROR;
        return SNMP_FAILURE;
    }

    switch (i4TestValFsMplsDiffServElspInfoPHB)
    {
        case TE_DS_DF_DSCP:
        case TE_DS_CS1_DSCP:
        case TE_DS_CS2_DSCP:
        case TE_DS_CS3_DSCP:
        case TE_DS_CS4_DSCP:
        case TE_DS_CS5_DSCP:
        case TE_DS_CS6_DSCP:
        case TE_DS_CS7_DSCP:
        case TE_DS_EF_DSCP:
        case TE_DS_AF11_DSCP:
        case TE_DS_AF12_DSCP:
        case TE_DS_AF13_DSCP:
        case TE_DS_AF21_DSCP:
        case TE_DS_AF22_DSCP:
        case TE_DS_AF23_DSCP:
        case TE_DS_AF31_DSCP:
        case TE_DS_AF32_DSCP:
        case TE_DS_AF33_DSCP:
        case TE_DS_AF41_DSCP:
        case TE_DS_AF42_DSCP:
        case TE_DS_AF43_DSCP:
	case TE_DS_EF1_DSCP:

            break;
        default:

            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    if (TeDiffServGetElspInfoNode (i4FsMplsDiffServElspInfoListIndex,
                                   i4FsMplsDiffServElspInfoIndex,
                                   &pElspInfoNode) != TE_SUCCESS)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    if ((TE_DS_ELSP_INFO_ROW_STATUS (pElspInfoNode) == ACTIVE) &&
        (TE_DS_ELSP_INFO_PHB_DSCP (pElspInfoNode) ==
         i4TestValFsMplsDiffServElspInfoPHB))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsDiffServElspInfoResourcePointer
 Input       :  The Indices
                FsMplsDiffServElspInfoListIndex
                FsMplsDiffServElspInfoIndex

                The Object 
                testValFsMplsDiffServElspInfoResourcePointer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsDiffServElspInfoResourcePointer (UINT4 *pu4ErrorCode,
                                                INT4
                                                i4FsMplsDiffServElspInfoListIndex,
                                                INT4
                                                i4FsMplsDiffServElspInfoIndex,
                                                tSNMP_OID_TYPE *
                                                pTestValFsMplsDiffServElspInfoResourcePointer)
{
    UINT1               u1Length;

    UNUSED_PARAM (i4FsMplsDiffServElspInfoListIndex);
    UNUSED_PARAM (i4FsMplsDiffServElspInfoIndex);

    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_AUTHORIZATION_ERROR;
        return SNMP_FAILURE;
    }

    u1Length = (UINT1) pTestValFsMplsDiffServElspInfoResourcePointer->u4_Length;

    /*
     * Here we expect the OID length to be equal to a value of
     * TE_DS_ELSPINFO_TABLE_DEF_OFFSET = 14 The Row pointer is expected 
     * to have a value of 1.3.6.1.2.1.10.166.3.2.6.1.2.'x' where the value x 
     * represents the Resource index in the TunnelResource Table.
     */
    if (u1Length == TE_DS_ELSPINFO_TABLE_DEF_OFFSET)
    {
        /* Checking for the matching against the Offset defined for the
         * ElspInfo table in the TE-MIB.
         */

        if ((pTestValFsMplsDiffServElspInfoResourcePointer->
             pu4_OidList[u1Length - TE_ONE]) != TE_ZERO)
        {
            return SNMP_SUCCESS;
        }
    }
    else
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsDiffServElspInfoRowStatus
 Input       :  The Indices
                FsMplsDiffServElspInfoListIndex
                FsMplsDiffServElspInfoIndex

                The Object 
                testValFsMplsDiffServElspInfoRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsDiffServElspInfoRowStatus (UINT4 *pu4ErrorCode,
                                          INT4
                                          i4FsMplsDiffServElspInfoListIndex,
                                          INT4 i4FsMplsDiffServElspInfoIndex,
                                          INT4
                                          i4TestValFsMplsDiffServElspInfoRowStatus)
{

    /* For Backward Compatiability only, remove in 9_2_0 */
    if(MAX_EXP_BIT_VAL_8 == i4FsMplsDiffServElspInfoIndex)
    {
        return SNMP_SUCCESS;
    }

    if (nmhValidateIndexInstanceFsMplsDiffServElspInfoTable
        (i4FsMplsDiffServElspInfoListIndex,
         i4FsMplsDiffServElspInfoIndex) == SNMP_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_AUTHORIZATION_ERROR;
        return SNMP_FAILURE;
    }

    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_AUTHORIZATION_ERROR;
        return SNMP_FAILURE;
    }

    switch (i4TestValFsMplsDiffServElspInfoRowStatus)
    {
        case CREATE_AND_WAIT:
        case ACTIVE:
        case NOT_IN_SERVICE:
        case DESTROY:

            break;

        default:

            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsDiffServElspInfoStorageType
 Input       :  The Indices
                FsMplsDiffServElspInfoListIndex
                FsMplsDiffServElspInfoIndex

                The Object 
                testValFsMplsDiffServElspInfoStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsDiffServElspInfoStorageType (UINT4 *pu4ErrorCode,
                                            INT4
                                            i4FsMplsDiffServElspInfoListIndex,
                                            INT4 i4FsMplsDiffServElspInfoIndex,
                                            INT4
                                            i4TestValFsMplsDiffServElspInfoStorageType)
{
    UNUSED_PARAM (i4FsMplsDiffServElspInfoListIndex);
    UNUSED_PARAM (i4FsMplsDiffServElspInfoIndex);

    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_AUTHORIZATION_ERROR;
        return SNMP_FAILURE;
    }

    switch (i4TestValFsMplsDiffServElspInfoStorageType)
    {
        case TE_STORAGE_VOLATILE:
        case TE_STORAGE_NONVOLATILE:
        case TE_STORAGE_OTHER:
        case TE_STORAGE_PERMANENT:
        case TE_STORAGE_READONLY:

            break;

        default:

            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsDiffServElspInfoListIndexNext
 Input       :  The Indices

                The Object 
                retValFsMplsDiffServElspInfoListIndexNext
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsDiffServElspInfoListIndexNext (INT4
                                           *pi4RetValFsMplsDiffServElspInfoListIndexNext)
{
    UINT4               u4Index;

    if (TE_ADMIN_STATUS (gTeGblInfo) != TE_ADMIN_UP)
    {
        return SNMP_SUCCESS;
    }

    if (TeDiffServGetNextAvailableIndex (&u4Index) == TE_FAILURE)
    {
        return SNMP_FAILURE;
    }

    *pi4RetValFsMplsDiffServElspInfoListIndexNext = (INT4)u4Index;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsActiveRsvpTeTnls
 Input       :  The Indices

                The Object 
                retValFsMplsActiveRsvpTeTnls
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsActiveRsvpTeTnls (INT4 *pi4RetValFsMplsActiveRsvpTeTnls)
{
#ifdef MPLS_RSVPTE_WANTED
    RpteDumpAllRsvpTeTnls (pi4RetValFsMplsActiveRsvpTeTnls);
#else
    *pi4RetValFsMplsActiveRsvpTeTnls = 0;
#endif /* MPLS_RSVPTE_WANTED */

    return SNMP_SUCCESS;
}
