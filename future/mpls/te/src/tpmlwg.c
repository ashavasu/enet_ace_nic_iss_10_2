/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: tpmlwg.c,v 1.4 2012/03/30 13:17:42 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/

/* MPLS_P2MP_LSP_CHANGES - S */
#include "teincs.h"
#include "tpmlwg.h"
#include "mplslsr.h"
#include "stdtelw.h"
extern INT1         nmhValidateIndexInstanceMplsOutSegmentTable
ARG_LIST ((tSNMP_OCTET_STRING_TYPE *));
/* MPLS_P2MP_LSP_CHANGES - E */

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsTeP2mpTunnelConfigured
 Input       :  The Indices

                The Object 
                retValMplsTeP2mpTunnelConfigured
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTeP2mpTunnelConfigured (UINT4 *pu4RetValMplsTeP2mpTunnelConfigured)
{
    if (TeGetMplsTeP2mpTunnelConfigured (pu4RetValMplsTeP2mpTunnelConfigured) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsTeP2mpTunnelActive
 Input       :  The Indices

                The Object 
                retValMplsTeP2mpTunnelActive
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTeP2mpTunnelActive (UINT4 *pu4RetValMplsTeP2mpTunnelActive)
{
    if (TeGetMplsTeP2mpTunnelActive (pu4RetValMplsTeP2mpTunnelActive) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsTeP2mpTunnelTotalMaxHops
 Input       :  The Indices

                The Object 
                retValMplsTeP2mpTunnelTotalMaxHops
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTeP2mpTunnelTotalMaxHops (UINT4
                                    *pu4RetValMplsTeP2mpTunnelTotalMaxHops)
{
    if (TeGetMplsTeP2mpTunnelTotalMaxHops
        (pu4RetValMplsTeP2mpTunnelTotalMaxHops) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : MplsTeP2mpTunnelTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsTeP2mpTunnelTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsTeP2mpTunnelTable (UINT4 u4MplsTunnelIndex,
                                               UINT4 u4MplsTunnelInstance,
                                               UINT4 u4MplsTunnelIngressLSRId,
                                               UINT4 u4MplsTunnelEgressLSRId)
{
    /* MPLS_P2MP_LSP_CHANGES - S */
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal = nmhValidateIndexInstanceMplsTunnelTable (u4MplsTunnelIndex,
                                                        u4MplsTunnelInstance,
                                                        u4MplsTunnelIngressLSRId,
                                                        u4MplsTunnelEgressLSRId);
    return i1RetVal;
    /* MPLS_P2MP_LSP_CHANGES - E */
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsTeP2mpTunnelTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsTeP2mpTunnelTable (UINT4 *pu4MplsTunnelIndex,
                                       UINT4 *pu4MplsTunnelInstance,
                                       UINT4 *pu4MplsTunnelIngressLSRId,
                                       UINT4 *pu4MplsTunnelEgressLSRId)
{
    /* MPLS_P2MP_LSP_CHANGES - S */
    tTeMplsTeP2mpTunnelTable TeMplsTeP2mpTunnelTable;
    INT4                i4RetVal = SNMP_FAILURE;
    /* MPLS_P2MP_LSP_CHANGES - E */

    MEMSET (&TeMplsTeP2mpTunnelTable, TE_ZERO,
            sizeof (tTeMplsTeP2mpTunnelTable));

    i4RetVal = TeGetFirstMplsTeP2mpTunnelTable (&TeMplsTeP2mpTunnelTable);

    if (SNMP_FAILURE == i4RetVal)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pu4MplsTunnelIndex = TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIndex;

    *pu4MplsTunnelInstance =
        TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelInstance;

    *pu4MplsTunnelIngressLSRId =
        TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIngressLSRId;

    *pu4MplsTunnelEgressLSRId =
        TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelEgressLSRId;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsTeP2mpTunnelTable
 Input       :  The Indices
                MplsTunnelIndex
                nextMplsTunnelIndex
                MplsTunnelInstance
                nextMplsTunnelInstance
                MplsTunnelIngressLSRId
                nextMplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                nextMplsTunnelEgressLSRId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMplsTeP2mpTunnelTable (UINT4 u4MplsTunnelIndex,
                                      UINT4 *pu4NextMplsTunnelIndex,
                                      UINT4 u4MplsTunnelInstance,
                                      UINT4 *pu4NextMplsTunnelInstance,
                                      UINT4 u4MplsTunnelIngressLSRId,
                                      UINT4 *pu4NextMplsTunnelIngressLSRId,
                                      UINT4 u4MplsTunnelEgressLSRId,
                                      UINT4 *pu4NextMplsTunnelEgressLSRId)
{
    /* MPLS_P2MP_LSP_CHANGES - S */
    tTeMplsTeP2mpTunnelTable TeMplsTeP2mpTunnelTable;
    tTeMplsTeP2mpTunnelTable NextTeMplsTeP2mpTunnelTable;
    INT4                i4RetVal = SNMP_FAILURE;
    /* MPLS_P2MP_LSP_CHANGES - E */

    MEMSET (&TeMplsTeP2mpTunnelTable, 0, sizeof (tTeMplsTeP2mpTunnelTable));
    MEMSET (&NextTeMplsTeP2mpTunnelTable, 0, sizeof (tTeMplsTeP2mpTunnelTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;

    i4RetVal =
        TeGetNextMplsTeP2mpTunnelTable (&TeMplsTeP2mpTunnelTable,
                                        &NextTeMplsTeP2mpTunnelTable);

    if (i4RetVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pu4NextMplsTunnelIndex =
        NextTeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIndex;

    *pu4NextMplsTunnelInstance =
        NextTeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelInstance;

    *pu4NextMplsTunnelIngressLSRId =
        NextTeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIngressLSRId;

    *pu4NextMplsTunnelEgressLSRId =
        NextTeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelEgressLSRId;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsTeP2mpTunnelP2mpIntegrity
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTeP2mpTunnelP2mpIntegrity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTeP2mpTunnelP2mpIntegrity (UINT4 u4MplsTunnelIndex,
                                     UINT4 u4MplsTunnelInstance,
                                     UINT4 u4MplsTunnelIngressLSRId,
                                     UINT4 u4MplsTunnelEgressLSRId,
                                     INT4
                                     *pi4RetValMplsTeP2mpTunnelP2mpIntegrity)
{
    tTeMplsTeP2mpTunnelTable TeMplsTeP2mpTunnelTable;

    MEMSET (&TeMplsTeP2mpTunnelTable, 0, sizeof (tTeMplsTeP2mpTunnelTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;

    if (TeGetAllMplsTeP2mpTunnelTable (&TeMplsTeP2mpTunnelTable) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValMplsTeP2mpTunnelP2mpIntegrity =
        TeMplsTeP2mpTunnelTable.MibObject.i4MplsTeP2mpTunnelP2mpIntegrity;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsTeP2mpTunnelBranchRole
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTeP2mpTunnelBranchRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTeP2mpTunnelBranchRole (UINT4 u4MplsTunnelIndex,
                                  UINT4 u4MplsTunnelInstance,
                                  UINT4 u4MplsTunnelIngressLSRId,
                                  UINT4 u4MplsTunnelEgressLSRId,
                                  INT4 *pi4RetValMplsTeP2mpTunnelBranchRole)
{
    tTeMplsTeP2mpTunnelTable TeMplsTeP2mpTunnelTable;

    MEMSET (&TeMplsTeP2mpTunnelTable, 0, sizeof (tTeMplsTeP2mpTunnelTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;

    if (TeGetAllMplsTeP2mpTunnelTable (&TeMplsTeP2mpTunnelTable) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValMplsTeP2mpTunnelBranchRole =
        TeMplsTeP2mpTunnelTable.MibObject.i4MplsTeP2mpTunnelBranchRole;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsTeP2mpTunnelP2mpXcIndex
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTeP2mpTunnelP2mpXcIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTeP2mpTunnelP2mpXcIndex (UINT4 u4MplsTunnelIndex,
                                   UINT4 u4MplsTunnelInstance,
                                   UINT4 u4MplsTunnelIngressLSRId,
                                   UINT4 u4MplsTunnelEgressLSRId,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValMplsTeP2mpTunnelP2mpXcIndex)
{
    tTeMplsTeP2mpTunnelTable TeMplsTeP2mpTunnelTable;

    MEMSET (&TeMplsTeP2mpTunnelTable, 0, sizeof (tTeMplsTeP2mpTunnelTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;

    if (TeGetAllMplsTeP2mpTunnelTable (&TeMplsTeP2mpTunnelTable) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValMplsTeP2mpTunnelP2mpXcIndex->pu1_OctetList,
            TeMplsTeP2mpTunnelTable.MibObject.au1MplsTeP2mpTunnelP2mpXcIndex,
            TeMplsTeP2mpTunnelTable.MibObject.i4MplsTeP2mpTunnelP2mpXcIndexLen);
    pRetValMplsTeP2mpTunnelP2mpXcIndex->i4_Length =
        TeMplsTeP2mpTunnelTable.MibObject.i4MplsTeP2mpTunnelP2mpXcIndexLen;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsTeP2mpTunnelRowStatus
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTeP2mpTunnelRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTeP2mpTunnelRowStatus (UINT4 u4MplsTunnelIndex,
                                 UINT4 u4MplsTunnelInstance,
                                 UINT4 u4MplsTunnelIngressLSRId,
                                 UINT4 u4MplsTunnelEgressLSRId,
                                 INT4 *pi4RetValMplsTeP2mpTunnelRowStatus)
{
    tTeMplsTeP2mpTunnelTable TeMplsTeP2mpTunnelTable;

    MEMSET (&TeMplsTeP2mpTunnelTable, 0, sizeof (tTeMplsTeP2mpTunnelTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;

    if (TeGetAllMplsTeP2mpTunnelTable (&TeMplsTeP2mpTunnelTable) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValMplsTeP2mpTunnelRowStatus =
        TeMplsTeP2mpTunnelTable.MibObject.i4MplsTeP2mpTunnelRowStatus;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsTeP2mpTunnelStorageType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTeP2mpTunnelStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTeP2mpTunnelStorageType (UINT4 u4MplsTunnelIndex,
                                   UINT4 u4MplsTunnelInstance,
                                   UINT4 u4MplsTunnelIngressLSRId,
                                   UINT4 u4MplsTunnelEgressLSRId,
                                   INT4 *pi4RetValMplsTeP2mpTunnelStorageType)
{
    tTeMplsTeP2mpTunnelTable TeMplsTeP2mpTunnelTable;

    MEMSET (&TeMplsTeP2mpTunnelTable, 0, sizeof (tTeMplsTeP2mpTunnelTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;

    if (TeGetAllMplsTeP2mpTunnelTable (&TeMplsTeP2mpTunnelTable) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValMplsTeP2mpTunnelStorageType =
        TeMplsTeP2mpTunnelTable.MibObject.i4MplsTeP2mpTunnelStorageType;

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsTeP2mpTunnelP2mpIntegrity
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValMplsTeP2mpTunnelP2mpIntegrity
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTeP2mpTunnelP2mpIntegrity (UINT4 u4MplsTunnelIndex,
                                     UINT4 u4MplsTunnelInstance,
                                     UINT4 u4MplsTunnelIngressLSRId,
                                     UINT4 u4MplsTunnelEgressLSRId,
                                     INT4 i4SetValMplsTeP2mpTunnelP2mpIntegrity)
{
    tTeMplsTeP2mpTunnelTable TeMplsTeP2mpTunnelTable;
    tTeIsSetMplsTeP2mpTunnelTable TeIsSetMplsTeP2mpTunnelTable;

    MEMSET (&TeMplsTeP2mpTunnelTable, 0, sizeof (tTeMplsTeP2mpTunnelTable));
    MEMSET (&TeIsSetMplsTeP2mpTunnelTable, 0,
            sizeof (tTeIsSetMplsTeP2mpTunnelTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTunnelIndex = OSIX_TRUE;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTunnelInstance = OSIX_TRUE;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTunnelIngressLSRId = OSIX_TRUE;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTunnelEgressLSRId = OSIX_TRUE;

    /* Assign the value */
    TeMplsTeP2mpTunnelTable.MibObject.i4MplsTeP2mpTunnelP2mpIntegrity =
        i4SetValMplsTeP2mpTunnelP2mpIntegrity;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTeP2mpTunnelP2mpIntegrity = OSIX_TRUE;

    if (TeSetAllMplsTeP2mpTunnelTable
        (&TeMplsTeP2mpTunnelTable, &TeIsSetMplsTeP2mpTunnelTable, OSIX_FALSE,
         OSIX_FALSE) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetMplsTeP2mpTunnelBranchRole
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValMplsTeP2mpTunnelBranchRole
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTeP2mpTunnelBranchRole (UINT4 u4MplsTunnelIndex,
                                  UINT4 u4MplsTunnelInstance,
                                  UINT4 u4MplsTunnelIngressLSRId,
                                  UINT4 u4MplsTunnelEgressLSRId,
                                  INT4 i4SetValMplsTeP2mpTunnelBranchRole)
{
    tTeMplsTeP2mpTunnelTable TeMplsTeP2mpTunnelTable;
    tTeIsSetMplsTeP2mpTunnelTable TeIsSetMplsTeP2mpTunnelTable;

    MEMSET (&TeMplsTeP2mpTunnelTable, 0, sizeof (tTeMplsTeP2mpTunnelTable));
    MEMSET (&TeIsSetMplsTeP2mpTunnelTable, 0,
            sizeof (tTeIsSetMplsTeP2mpTunnelTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTunnelIndex = OSIX_TRUE;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTunnelInstance = OSIX_TRUE;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTunnelIngressLSRId = OSIX_TRUE;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTunnelEgressLSRId = OSIX_TRUE;

    /* Assign the value */
    TeMplsTeP2mpTunnelTable.MibObject.i4MplsTeP2mpTunnelBranchRole =
        i4SetValMplsTeP2mpTunnelBranchRole;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTeP2mpTunnelBranchRole = OSIX_TRUE;

    if (TeSetAllMplsTeP2mpTunnelTable
        (&TeMplsTeP2mpTunnelTable, &TeIsSetMplsTeP2mpTunnelTable, OSIX_FALSE,
         OSIX_FALSE) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetMplsTeP2mpTunnelRowStatus
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValMplsTeP2mpTunnelRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTeP2mpTunnelRowStatus (UINT4 u4MplsTunnelIndex,
                                 UINT4 u4MplsTunnelInstance,
                                 UINT4 u4MplsTunnelIngressLSRId,
                                 UINT4 u4MplsTunnelEgressLSRId,
                                 INT4 i4SetValMplsTeP2mpTunnelRowStatus)
{
    tTeMplsTeP2mpTunnelTable TeMplsTeP2mpTunnelTable;
    tTeIsSetMplsTeP2mpTunnelTable TeIsSetMplsTeP2mpTunnelTable;

    MEMSET (&TeMplsTeP2mpTunnelTable, 0, sizeof (tTeMplsTeP2mpTunnelTable));
    MEMSET (&TeIsSetMplsTeP2mpTunnelTable, 0,
            sizeof (tTeIsSetMplsTeP2mpTunnelTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTunnelIndex = OSIX_TRUE;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTunnelInstance = OSIX_TRUE;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTunnelIngressLSRId = OSIX_TRUE;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTunnelEgressLSRId = OSIX_TRUE;

    /* Assign the value */
    TeMplsTeP2mpTunnelTable.MibObject.i4MplsTeP2mpTunnelRowStatus =
        i4SetValMplsTeP2mpTunnelRowStatus;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTeP2mpTunnelRowStatus = OSIX_TRUE;

    if (TeSetAllMplsTeP2mpTunnelTable
        (&TeMplsTeP2mpTunnelTable, &TeIsSetMplsTeP2mpTunnelTable, OSIX_FALSE,
         OSIX_FALSE) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetMplsTeP2mpTunnelStorageType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValMplsTeP2mpTunnelStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTeP2mpTunnelStorageType (UINT4 u4MplsTunnelIndex,
                                   UINT4 u4MplsTunnelInstance,
                                   UINT4 u4MplsTunnelIngressLSRId,
                                   UINT4 u4MplsTunnelEgressLSRId,
                                   INT4 i4SetValMplsTeP2mpTunnelStorageType)
{
    tTeMplsTeP2mpTunnelTable TeMplsTeP2mpTunnelTable;
    tTeIsSetMplsTeP2mpTunnelTable TeIsSetMplsTeP2mpTunnelTable;

    MEMSET (&TeMplsTeP2mpTunnelTable, 0, sizeof (tTeMplsTeP2mpTunnelTable));
    MEMSET (&TeIsSetMplsTeP2mpTunnelTable, 0,
            sizeof (tTeIsSetMplsTeP2mpTunnelTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTunnelIndex = OSIX_TRUE;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTunnelInstance = OSIX_TRUE;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTunnelIngressLSRId = OSIX_TRUE;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTunnelEgressLSRId = OSIX_TRUE;

    /* Assign the value */
    TeMplsTeP2mpTunnelTable.MibObject.i4MplsTeP2mpTunnelStorageType =
        i4SetValMplsTeP2mpTunnelStorageType;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTeP2mpTunnelStorageType = OSIX_TRUE;

    if (TeSetAllMplsTeP2mpTunnelTable
        (&TeMplsTeP2mpTunnelTable, &TeIsSetMplsTeP2mpTunnelTable, OSIX_FALSE,
         OSIX_FALSE) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsTeP2mpTunnelP2mpIntegrity
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValMplsTeP2mpTunnelP2mpIntegrity
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTeP2mpTunnelP2mpIntegrity (UINT4 *pu4ErrorCode,
                                        UINT4 u4MplsTunnelIndex,
                                        UINT4 u4MplsTunnelInstance,
                                        UINT4 u4MplsTunnelIngressLSRId,
                                        UINT4 u4MplsTunnelEgressLSRId,
                                        INT4
                                        i4TestValMplsTeP2mpTunnelP2mpIntegrity)
{
    tTeMplsTeP2mpTunnelTable TeMplsTeP2mpTunnelTable;
    tTeIsSetMplsTeP2mpTunnelTable TeIsSetMplsTeP2mpTunnelTable;

    MEMSET (&TeMplsTeP2mpTunnelTable, 0, sizeof (tTeMplsTeP2mpTunnelTable));
    MEMSET (&TeIsSetMplsTeP2mpTunnelTable, 0,
            sizeof (tTeIsSetMplsTeP2mpTunnelTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTunnelIndex = OSIX_TRUE;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTunnelInstance = OSIX_TRUE;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTunnelIngressLSRId = OSIX_TRUE;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTunnelEgressLSRId = OSIX_TRUE;

    /* Assign the value */
    TeMplsTeP2mpTunnelTable.MibObject.i4MplsTeP2mpTunnelP2mpIntegrity =
        i4TestValMplsTeP2mpTunnelP2mpIntegrity;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTeP2mpTunnelP2mpIntegrity = OSIX_TRUE;

    if (TeTestAllMplsTeP2mpTunnelTable (pu4ErrorCode, &TeMplsTeP2mpTunnelTable,
                                        &TeIsSetMplsTeP2mpTunnelTable,
                                        OSIX_FALSE, OSIX_FALSE) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2MplsTeP2mpTunnelBranchRole
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValMplsTeP2mpTunnelBranchRole
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTeP2mpTunnelBranchRole (UINT4 *pu4ErrorCode,
                                     UINT4 u4MplsTunnelIndex,
                                     UINT4 u4MplsTunnelInstance,
                                     UINT4 u4MplsTunnelIngressLSRId,
                                     UINT4 u4MplsTunnelEgressLSRId,
                                     INT4 i4TestValMplsTeP2mpTunnelBranchRole)
{
    tTeMplsTeP2mpTunnelTable TeMplsTeP2mpTunnelTable;
    tTeIsSetMplsTeP2mpTunnelTable TeIsSetMplsTeP2mpTunnelTable;

    MEMSET (&TeMplsTeP2mpTunnelTable, 0, sizeof (tTeMplsTeP2mpTunnelTable));
    MEMSET (&TeIsSetMplsTeP2mpTunnelTable, 0,
            sizeof (tTeIsSetMplsTeP2mpTunnelTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTunnelIndex = OSIX_TRUE;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTunnelInstance = OSIX_TRUE;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTunnelIngressLSRId = OSIX_TRUE;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTunnelEgressLSRId = OSIX_TRUE;

    /* Assign the value */
    TeMplsTeP2mpTunnelTable.MibObject.i4MplsTeP2mpTunnelBranchRole =
        i4TestValMplsTeP2mpTunnelBranchRole;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTeP2mpTunnelBranchRole = OSIX_TRUE;

    if (TeTestAllMplsTeP2mpTunnelTable (pu4ErrorCode, &TeMplsTeP2mpTunnelTable,
                                        &TeIsSetMplsTeP2mpTunnelTable,
                                        OSIX_FALSE, OSIX_FALSE) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2MplsTeP2mpTunnelRowStatus
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValMplsTeP2mpTunnelRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTeP2mpTunnelRowStatus (UINT4 *pu4ErrorCode,
                                    UINT4 u4MplsTunnelIndex,
                                    UINT4 u4MplsTunnelInstance,
                                    UINT4 u4MplsTunnelIngressLSRId,
                                    UINT4 u4MplsTunnelEgressLSRId,
                                    INT4 i4TestValMplsTeP2mpTunnelRowStatus)
{
    tTeMplsTeP2mpTunnelTable TeMplsTeP2mpTunnelTable;
    tTeIsSetMplsTeP2mpTunnelTable TeIsSetMplsTeP2mpTunnelTable;

    MEMSET (&TeMplsTeP2mpTunnelTable, 0, sizeof (tTeMplsTeP2mpTunnelTable));
    MEMSET (&TeIsSetMplsTeP2mpTunnelTable, 0,
            sizeof (tTeIsSetMplsTeP2mpTunnelTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTunnelIndex = OSIX_TRUE;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTunnelInstance = OSIX_TRUE;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTunnelIngressLSRId = OSIX_TRUE;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTunnelEgressLSRId = OSIX_TRUE;

    /* Assign the value */
    TeMplsTeP2mpTunnelTable.MibObject.i4MplsTeP2mpTunnelRowStatus =
        i4TestValMplsTeP2mpTunnelRowStatus;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTeP2mpTunnelRowStatus = OSIX_TRUE;

    if (TeTestAllMplsTeP2mpTunnelTable (pu4ErrorCode, &TeMplsTeP2mpTunnelTable,
                                        &TeIsSetMplsTeP2mpTunnelTable,
                                        OSIX_FALSE, OSIX_FALSE) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2MplsTeP2mpTunnelStorageType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValMplsTeP2mpTunnelStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTeP2mpTunnelStorageType (UINT4 *pu4ErrorCode,
                                      UINT4 u4MplsTunnelIndex,
                                      UINT4 u4MplsTunnelInstance,
                                      UINT4 u4MplsTunnelIngressLSRId,
                                      UINT4 u4MplsTunnelEgressLSRId,
                                      INT4 i4TestValMplsTeP2mpTunnelStorageType)
{
    tTeMplsTeP2mpTunnelTable TeMplsTeP2mpTunnelTable;
    tTeIsSetMplsTeP2mpTunnelTable TeIsSetMplsTeP2mpTunnelTable;

    MEMSET (&TeMplsTeP2mpTunnelTable, 0, sizeof (tTeMplsTeP2mpTunnelTable));
    MEMSET (&TeIsSetMplsTeP2mpTunnelTable, 0,
            sizeof (tTeIsSetMplsTeP2mpTunnelTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTunnelIndex = OSIX_TRUE;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTunnelInstance = OSIX_TRUE;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTunnelIngressLSRId = OSIX_TRUE;

    TeMplsTeP2mpTunnelTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTunnelEgressLSRId = OSIX_TRUE;

    /* Assign the value */
    TeMplsTeP2mpTunnelTable.MibObject.i4MplsTeP2mpTunnelStorageType =
        i4TestValMplsTeP2mpTunnelStorageType;
    TeIsSetMplsTeP2mpTunnelTable.bMplsTeP2mpTunnelStorageType = OSIX_TRUE;

    if (TeTestAllMplsTeP2mpTunnelTable (pu4ErrorCode, &TeMplsTeP2mpTunnelTable,
                                        &TeIsSetMplsTeP2mpTunnelTable,
                                        OSIX_FALSE, OSIX_FALSE) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2MplsTeP2mpTunnelTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MplsTeP2mpTunnelTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsTeP2mpTunnelSubGroupIDNext
 Input       :  The Indices

                The Object 
                retValMplsTeP2mpTunnelSubGroupIDNext
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTeP2mpTunnelSubGroupIDNext (UINT4
                                      *pu4RetValMplsTeP2mpTunnelSubGroupIDNext)
{
    if (TeGetMplsTeP2mpTunnelSubGroupIDNext
        (pu4RetValMplsTeP2mpTunnelSubGroupIDNext) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : MplsTeP2mpTunnelDestTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelDestSrcSubGroupOriginType
                MplsTeP2mpTunnelDestSrcSubGroupOrigin
                MplsTeP2mpTunnelDestSrcSubGroupID
                MplsTeP2mpTunnelDestSubGroupOriginType
                MplsTeP2mpTunnelDestSubGroupOrigin
                MplsTeP2mpTunnelDestSubGroupID
                MplsTeP2mpTunnelDestDestinationType
                MplsTeP2mpTunnelDestDestination
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable (UINT4 u4MplsTunnelIndex,
                                                   UINT4 u4MplsTunnelInstance,
                                                   UINT4
                                                   u4MplsTunnelIngressLSRId,
                                                   UINT4
                                                   u4MplsTunnelEgressLSRId,
                                                   INT4
                                                   i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                                                   tSNMP_OCTET_STRING_TYPE *
                                                   pMplsTeP2mpTunnelDestSrcSubGroupOrigin,
                                                   UINT4
                                                   u4MplsTeP2mpTunnelDestSrcSubGroupID,
                                                   INT4
                                                   i4MplsTeP2mpTunnelDestSubGroupOriginType,
                                                   tSNMP_OCTET_STRING_TYPE *
                                                   pMplsTeP2mpTunnelDestSubGroupOrigin,
                                                   UINT4
                                                   u4MplsTeP2mpTunnelDestSubGroupID,
                                                   INT4
                                                   i4MplsTeP2mpTunnelDestDestinationType,
                                                   tSNMP_OCTET_STRING_TYPE *
                                                   pMplsTeP2mpTunnelDestDestination)
{
    /* MPLS_P2MP_LSP_CHANGES - S */
    UINT4               u4IPAddr;
    UINT4               u4Index;
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal = nmhValidateIndexInstanceMplsTunnelTable (u4MplsTunnelIndex,
                                                        u4MplsTunnelInstance,
                                                        u4MplsTunnelIngressLSRId,
                                                        u4MplsTunnelEgressLSRId);
    if (SNMP_SUCCESS != i1RetVal)
    {
        return i1RetVal;
    }
    /* Validation for i4MplsTeP2mpTunnelDestSrcSubGroupOriginType */
    switch (i4MplsTeP2mpTunnelDestSrcSubGroupOriginType)
    {
        case LSR_ADDR_IPV4:
            break;
        case LSR_ADDR_IPV6:
            return SNMP_FAILURE;
        case LSR_ADDR_UNKNOWN:
            return SNMP_FAILURE;
        default:
            return SNMP_FAILURE;
    }
    /* validation for pMplsTeP2mpTunnelDestSrcSubGroupOrigin */

    MPLS_OCTETSTRING_TO_INTEGER (pMplsTeP2mpTunnelDestSrcSubGroupOrigin,
                                 u4Index);
    if ((TE_ZERO != u4Index))
    {
        return SNMP_FAILURE;
    }

    /* validation for u4MplsTeP2mpTunnelDestSrcSubGroupID */

    if (TE_ZERO != u4MplsTeP2mpTunnelDestSrcSubGroupID)
    {
        return SNMP_FAILURE;
    }

    /* Validation for i4MplsTeP2mpTunnelDestSubGroupOriginType */
    switch (i4MplsTeP2mpTunnelDestSubGroupOriginType)
    {
        case LSR_ADDR_IPV4:
            break;
        case LSR_ADDR_IPV6:
            return SNMP_FAILURE;
        case LSR_ADDR_UNKNOWN:
            return SNMP_FAILURE;
        default:
            return SNMP_FAILURE;
    }

    /* validation for pMplsTeP2mpTunnelDestSubGroupOrigin */

    MPLS_OCTETSTRING_TO_INTEGER (pMplsTeP2mpTunnelDestSubGroupOrigin, u4Index);
    if ((u4Index != TE_ZERO))
    {
        return SNMP_FAILURE;
    }

    /* validation for u4MplsTeP2mpTunnelDestSubGroupID */
    if (TE_ONE != u4MplsTeP2mpTunnelDestSubGroupID)
    {
        return SNMP_FAILURE;
    }

    /* Validation for i4MplsTeP2mpTunnelDestDestinationType */
    switch (i4MplsTeP2mpTunnelDestDestinationType)
    {
        case LSR_ADDR_IPV4:
            break;
        case LSR_ADDR_IPV6:
            return SNMP_FAILURE;
        case LSR_ADDR_UNKNOWN:
            return SNMP_FAILURE;
        default:
            return SNMP_FAILURE;
    }

    /* validation for pMplsTeP2mpTunnelDestDestination */
    if ((pMplsTeP2mpTunnelDestDestination->i4_Length < MIN_OUTSEG_HOP_ADDR_LEN)
        || (pMplsTeP2mpTunnelDestDestination->i4_Length >
            MAX_OUTSEG_HOP_ADDR_LEN))
    {
        return SNMP_FAILURE;
    }
    MEMCPY ((UINT1 *) &u4IPAddr,
            pMplsTeP2mpTunnelDestDestination->pu1_OctetList, ROUTER_ID_LENGTH);
    u4IPAddr = OSIX_NTOHL (u4IPAddr);

    if (!((IP_IS_ADDR_CLASS_A (u4IPAddr)) || (IP_IS_ADDR_CLASS_B (u4IPAddr)) ||
          (IP_IS_ADDR_CLASS_C (u4IPAddr))))
    {

        return SNMP_FAILURE;
    }
    /* MPLS_P2MP_LSP_CHANGES - E */
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsTeP2mpTunnelDestTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelDestSrcSubGroupOriginType
                MplsTeP2mpTunnelDestSrcSubGroupOrigin
                MplsTeP2mpTunnelDestSrcSubGroupID
                MplsTeP2mpTunnelDestSubGroupOriginType
                MplsTeP2mpTunnelDestSubGroupOrigin
                MplsTeP2mpTunnelDestSubGroupID
                MplsTeP2mpTunnelDestDestinationType
                MplsTeP2mpTunnelDestDestination
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsTeP2mpTunnelDestTable (UINT4 *pu4MplsTunnelIndex,
                                           UINT4 *pu4MplsTunnelInstance,
                                           UINT4 *pu4MplsTunnelIngressLSRId,
                                           UINT4 *pu4MplsTunnelEgressLSRId,
                                           INT4
                                           *pi4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pMplsTeP2mpTunnelDestSrcSubGroupOrigin,
                                           UINT4
                                           *pu4MplsTeP2mpTunnelDestSrcSubGroupID,
                                           INT4
                                           *pi4MplsTeP2mpTunnelDestSubGroupOriginType,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pMplsTeP2mpTunnelDestSubGroupOrigin,
                                           UINT4
                                           *pu4MplsTeP2mpTunnelDestSubGroupID,
                                           INT4
                                           *pi4MplsTeP2mpTunnelDestDestinationType,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pMplsTeP2mpTunnelDestDestination)
{
    tTeMplsTeP2mpTunnelDestTable TeMplsTeP2mpTunnelDestTable;
    INT4                i4RetVal = SNMP_FAILURE;

    MEMSET (&TeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));

    i4RetVal =
        TeGetFirstMplsTeP2mpTunnelDestTable (&TeMplsTeP2mpTunnelDestTable);

    if (i4RetVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pu4MplsTunnelIndex =
        TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIndex;

    *pu4MplsTunnelInstance =
        TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelInstance;

    *pu4MplsTunnelIngressLSRId =
        TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIngressLSRId;

    *pu4MplsTunnelEgressLSRId =
        TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelEgressLSRId;

    *pi4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
        TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;

    MEMCPY (pMplsTeP2mpTunnelDestSrcSubGroupOrigin->pu1_OctetList,
            &(TeMplsTeP2mpTunnelDestTable.MibObject.
              au1MplsTeP2mpTunnelDestSrcSubGroupOrigin),
            TeMplsTeP2mpTunnelDestTable.MibObject.
            i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen);
    pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length =
        TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen;

    *pu4MplsTeP2mpTunnelDestSrcSubGroupID =
        TeMplsTeP2mpTunnelDestTable.MibObject.
        u4MplsTeP2mpTunnelDestSrcSubGroupID;

    *pi4MplsTeP2mpTunnelDestSubGroupOriginType =
        TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginType;

    MEMCPY (pMplsTeP2mpTunnelDestSubGroupOrigin->pu1_OctetList,
            &(TeMplsTeP2mpTunnelDestTable.MibObject.
              au1MplsTeP2mpTunnelDestSubGroupOrigin),
            TeMplsTeP2mpTunnelDestTable.MibObject.
            i4MplsTeP2mpTunnelDestSubGroupOriginLen);
    pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length =
        TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginLen;

    *pu4MplsTeP2mpTunnelDestSubGroupID =
        TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSubGroupID;

    *pi4MplsTeP2mpTunnelDestDestinationType =
        TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationType;

    MEMCPY (pMplsTeP2mpTunnelDestDestination->pu1_OctetList,
            &(TeMplsTeP2mpTunnelDestTable.MibObject.
              au1MplsTeP2mpTunnelDestDestination),
            TeMplsTeP2mpTunnelDestTable.MibObject.
            i4MplsTeP2mpTunnelDestDestinationLen);
    pMplsTeP2mpTunnelDestDestination->i4_Length =
        TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationLen;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsTeP2mpTunnelDestTable
 Input       :  The Indices
                MplsTunnelIndex
                nextMplsTunnelIndex
                MplsTunnelInstance
                nextMplsTunnelInstance
                MplsTunnelIngressLSRId
                nextMplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                nextMplsTunnelEgressLSRId
                MplsTeP2mpTunnelDestSrcSubGroupOriginType
                nextMplsTeP2mpTunnelDestSrcSubGroupOriginType
                MplsTeP2mpTunnelDestSrcSubGroupOrigin
                nextMplsTeP2mpTunnelDestSrcSubGroupOrigin
                MplsTeP2mpTunnelDestSrcSubGroupID
                nextMplsTeP2mpTunnelDestSrcSubGroupID
                MplsTeP2mpTunnelDestSubGroupOriginType
                nextMplsTeP2mpTunnelDestSubGroupOriginType
                MplsTeP2mpTunnelDestSubGroupOrigin
                nextMplsTeP2mpTunnelDestSubGroupOrigin
                MplsTeP2mpTunnelDestSubGroupID
                nextMplsTeP2mpTunnelDestSubGroupID
                MplsTeP2mpTunnelDestDestinationType
                nextMplsTeP2mpTunnelDestDestinationType
                MplsTeP2mpTunnelDestDestination
                nextMplsTeP2mpTunnelDestDestination
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMplsTeP2mpTunnelDestTable (UINT4 u4MplsTunnelIndex,
                                          UINT4 *pu4NextMplsTunnelIndex,
                                          UINT4 u4MplsTunnelInstance,
                                          UINT4 *pu4NextMplsTunnelInstance,
                                          UINT4 u4MplsTunnelIngressLSRId,
                                          UINT4 *pu4NextMplsTunnelIngressLSRId,
                                          UINT4 u4MplsTunnelEgressLSRId,
                                          UINT4 *pu4NextMplsTunnelEgressLSRId,
                                          INT4
                                          i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                                          INT4
                                          *pi4NextMplsTeP2mpTunnelDestSrcSubGroupOriginType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsTeP2mpTunnelDestSrcSubGroupOrigin,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pNextMplsTeP2mpTunnelDestSrcSubGroupOrigin,
                                          UINT4
                                          u4MplsTeP2mpTunnelDestSrcSubGroupID,
                                          UINT4
                                          *pu4NextMplsTeP2mpTunnelDestSrcSubGroupID,
                                          INT4
                                          i4MplsTeP2mpTunnelDestSubGroupOriginType,
                                          INT4
                                          *pi4NextMplsTeP2mpTunnelDestSubGroupOriginType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsTeP2mpTunnelDestSubGroupOrigin,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pNextMplsTeP2mpTunnelDestSubGroupOrigin,
                                          UINT4
                                          u4MplsTeP2mpTunnelDestSubGroupID,
                                          UINT4
                                          *pu4NextMplsTeP2mpTunnelDestSubGroupID,
                                          INT4
                                          i4MplsTeP2mpTunnelDestDestinationType,
                                          INT4
                                          *pi4NextMplsTeP2mpTunnelDestDestinationType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsTeP2mpTunnelDestDestination,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pNextMplsTeP2mpTunnelDestDestination)
{
    tTeMplsTeP2mpTunnelDestTable *pTeMplsTeP2mpTunnelDestTable = NULL;
    tTeMplsTeP2mpTunnelDestTable NextTeMplsTeP2mpTunnelDestTable;

    INT4                i4RetVal = SNMP_FAILURE;

    MEMSET (&NextTeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));

    pTeMplsTeP2mpTunnelDestTable = (tTeMplsTeP2mpTunnelDestTable *)
        TE_ALLOC_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID);
    if (pTeMplsTeP2mpTunnelDestTable == NULL)
    {
        return SNMP_FAILURE;
    }
    MEMSET (pTeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));

    /* Assign the index */
    pTeMplsTeP2mpTunnelDestTable->MibObject.u4MplsTunnelIndex =
        u4MplsTunnelIndex;

    pTeMplsTeP2mpTunnelDestTable->MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;

    pTeMplsTeP2mpTunnelDestTable->MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;

    pTeMplsTeP2mpTunnelDestTable->MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;

    pTeMplsTeP2mpTunnelDestTable->MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;

    MEMCPY ((pTeMplsTeP2mpTunnelDestTable->MibObject.
             au1MplsTeP2mpTunnelDestSrcSubGroupOrigin),
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length);
    pTeMplsTeP2mpTunnelDestTable->MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length;

    pTeMplsTeP2mpTunnelDestTable->MibObject.
        u4MplsTeP2mpTunnelDestSrcSubGroupID =
        u4MplsTeP2mpTunnelDestSrcSubGroupID;

    pTeMplsTeP2mpTunnelDestTable->MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSubGroupOriginType;

    MEMCPY ((pTeMplsTeP2mpTunnelDestTable->MibObject.
             au1MplsTeP2mpTunnelDestSubGroupOrigin),
            pMplsTeP2mpTunnelDestSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length);
    pTeMplsTeP2mpTunnelDestTable->MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length;

    pTeMplsTeP2mpTunnelDestTable->MibObject.u4MplsTeP2mpTunnelDestSubGroupID =
        u4MplsTeP2mpTunnelDestSubGroupID;

    pTeMplsTeP2mpTunnelDestTable->MibObject.
        i4MplsTeP2mpTunnelDestDestinationType =
        i4MplsTeP2mpTunnelDestDestinationType;

    MEMCPY ((pTeMplsTeP2mpTunnelDestTable->MibObject.
             au1MplsTeP2mpTunnelDestDestination),
            pMplsTeP2mpTunnelDestDestination->pu1_OctetList,
            pMplsTeP2mpTunnelDestDestination->i4_Length);
    pTeMplsTeP2mpTunnelDestTable->MibObject.
        i4MplsTeP2mpTunnelDestDestinationLen =
        pMplsTeP2mpTunnelDestDestination->i4_Length;

    i4RetVal =
        TeGetNextMplsTeP2mpTunnelDestTable (pTeMplsTeP2mpTunnelDestTable,
                                            &NextTeMplsTeP2mpTunnelDestTable);

    TE_REL_MEM_BLOCK (TE_P2MP_TNL_DEST_TBL_POOL_ID,
                      (UINT1 *) pTeMplsTeP2mpTunnelDestTable);
    if (i4RetVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pu4NextMplsTunnelIndex =
        NextTeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIndex;

    *pu4NextMplsTunnelInstance =
        NextTeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelInstance;

    *pu4NextMplsTunnelIngressLSRId =
        NextTeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIngressLSRId;

    *pu4NextMplsTunnelEgressLSRId =
        NextTeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelEgressLSRId;

    *pi4NextMplsTeP2mpTunnelDestSrcSubGroupOriginType =
        NextTeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;

    MEMCPY (pNextMplsTeP2mpTunnelDestSrcSubGroupOrigin->pu1_OctetList,
            &(NextTeMplsTeP2mpTunnelDestTable.MibObject.
              au1MplsTeP2mpTunnelDestSrcSubGroupOrigin),
            NextTeMplsTeP2mpTunnelDestTable.MibObject.
            i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen);
    pNextMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length =
        NextTeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen;

    *pu4NextMplsTeP2mpTunnelDestSrcSubGroupID =
        NextTeMplsTeP2mpTunnelDestTable.MibObject.
        u4MplsTeP2mpTunnelDestSrcSubGroupID;

    *pi4NextMplsTeP2mpTunnelDestSubGroupOriginType =
        NextTeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginType;

    MEMCPY (pNextMplsTeP2mpTunnelDestSubGroupOrigin->pu1_OctetList,
            &(NextTeMplsTeP2mpTunnelDestTable.MibObject.
              au1MplsTeP2mpTunnelDestSubGroupOrigin),
            NextTeMplsTeP2mpTunnelDestTable.MibObject.
            i4MplsTeP2mpTunnelDestSubGroupOriginLen);
    pNextMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length =
        NextTeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginLen;

    *pu4NextMplsTeP2mpTunnelDestSubGroupID =
        NextTeMplsTeP2mpTunnelDestTable.MibObject.
        u4MplsTeP2mpTunnelDestSubGroupID;

    *pi4NextMplsTeP2mpTunnelDestDestinationType =
        NextTeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationType;

    MEMCPY (pNextMplsTeP2mpTunnelDestDestination->pu1_OctetList,
            &(NextTeMplsTeP2mpTunnelDestTable.MibObject.
              au1MplsTeP2mpTunnelDestDestination),
            NextTeMplsTeP2mpTunnelDestTable.MibObject.
            i4MplsTeP2mpTunnelDestDestinationLen);
    pNextMplsTeP2mpTunnelDestDestination->i4_Length =
        NextTeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationLen;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsTeP2mpTunnelDestBranchOutSegment
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelDestSrcSubGroupOriginType
                MplsTeP2mpTunnelDestSrcSubGroupOrigin
                MplsTeP2mpTunnelDestSrcSubGroupID
                MplsTeP2mpTunnelDestSubGroupOriginType
                MplsTeP2mpTunnelDestSubGroupOrigin
                MplsTeP2mpTunnelDestSubGroupID
                MplsTeP2mpTunnelDestDestinationType
                MplsTeP2mpTunnelDestDestination

                The Object 
                retValMplsTeP2mpTunnelDestBranchOutSegment
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTeP2mpTunnelDestBranchOutSegment (UINT4 u4MplsTunnelIndex,
                                            UINT4 u4MplsTunnelInstance,
                                            UINT4 u4MplsTunnelIngressLSRId,
                                            UINT4 u4MplsTunnelEgressLSRId,
                                            INT4
                                            i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pMplsTeP2mpTunnelDestSrcSubGroupOrigin,
                                            UINT4
                                            u4MplsTeP2mpTunnelDestSrcSubGroupID,
                                            INT4
                                            i4MplsTeP2mpTunnelDestSubGroupOriginType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pMplsTeP2mpTunnelDestSubGroupOrigin,
                                            UINT4
                                            u4MplsTeP2mpTunnelDestSubGroupID,
                                            INT4
                                            i4MplsTeP2mpTunnelDestDestinationType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pMplsTeP2mpTunnelDestDestination,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pRetValMplsTeP2mpTunnelDestBranchOutSegment)
{
    tTeMplsTeP2mpTunnelDestTable TeMplsTeP2mpTunnelDestTable;

    MEMSET (&TeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSrcSubGroupID =
        u4MplsTeP2mpTunnelDestSrcSubGroupID;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSubGroupOriginType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSubGroupOrigin,
            pMplsTeP2mpTunnelDestSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSubGroupID =
        u4MplsTeP2mpTunnelDestSubGroupID;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationType =
        i4MplsTeP2mpTunnelDestDestinationType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestDestination,
            pMplsTeP2mpTunnelDestDestination->pu1_OctetList,
            pMplsTeP2mpTunnelDestDestination->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.i4MplsTeP2mpTunnelDestDestinationLen =
        pMplsTeP2mpTunnelDestDestination->i4_Length;

    if (TeGetAllMplsTeP2mpTunnelDestTable (&TeMplsTeP2mpTunnelDestTable) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    MEMCPY (pRetValMplsTeP2mpTunnelDestBranchOutSegment->pu1_OctetList,
            TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestBranchOutSegment,
            TeMplsTeP2mpTunnelDestTable.MibObject.
            i4MplsTeP2mpTunnelDestBranchOutSegmentLen);
    pRetValMplsTeP2mpTunnelDestBranchOutSegment->i4_Length =
        TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestBranchOutSegmentLen;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsTeP2mpTunnelDestHopTableIndex
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelDestSrcSubGroupOriginType
                MplsTeP2mpTunnelDestSrcSubGroupOrigin
                MplsTeP2mpTunnelDestSrcSubGroupID
                MplsTeP2mpTunnelDestSubGroupOriginType
                MplsTeP2mpTunnelDestSubGroupOrigin
                MplsTeP2mpTunnelDestSubGroupID
                MplsTeP2mpTunnelDestDestinationType
                MplsTeP2mpTunnelDestDestination

                The Object 
                retValMplsTeP2mpTunnelDestHopTableIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTeP2mpTunnelDestHopTableIndex (UINT4 u4MplsTunnelIndex,
                                         UINT4 u4MplsTunnelInstance,
                                         UINT4 u4MplsTunnelIngressLSRId,
                                         UINT4 u4MplsTunnelEgressLSRId,
                                         INT4
                                         i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pMplsTeP2mpTunnelDestSrcSubGroupOrigin,
                                         UINT4
                                         u4MplsTeP2mpTunnelDestSrcSubGroupID,
                                         INT4
                                         i4MplsTeP2mpTunnelDestSubGroupOriginType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pMplsTeP2mpTunnelDestSubGroupOrigin,
                                         UINT4 u4MplsTeP2mpTunnelDestSubGroupID,
                                         INT4
                                         i4MplsTeP2mpTunnelDestDestinationType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pMplsTeP2mpTunnelDestDestination,
                                         UINT4
                                         *pu4RetValMplsTeP2mpTunnelDestHopTableIndex)
{
    tTeMplsTeP2mpTunnelDestTable TeMplsTeP2mpTunnelDestTable;

    MEMSET (&TeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSrcSubGroupID =
        u4MplsTeP2mpTunnelDestSrcSubGroupID;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSubGroupOriginType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSubGroupOrigin,
            pMplsTeP2mpTunnelDestSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSubGroupID =
        u4MplsTeP2mpTunnelDestSubGroupID;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationType =
        i4MplsTeP2mpTunnelDestDestinationType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestDestination,
            pMplsTeP2mpTunnelDestDestination->pu1_OctetList,
            pMplsTeP2mpTunnelDestDestination->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.i4MplsTeP2mpTunnelDestDestinationLen =
        pMplsTeP2mpTunnelDestDestination->i4_Length;

    if (TeGetAllMplsTeP2mpTunnelDestTable (&TeMplsTeP2mpTunnelDestTable) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValMplsTeP2mpTunnelDestHopTableIndex =
        TeMplsTeP2mpTunnelDestTable.MibObject.
        u4MplsTeP2mpTunnelDestHopTableIndex;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsTeP2mpTunnelDestPathInUse
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelDestSrcSubGroupOriginType
                MplsTeP2mpTunnelDestSrcSubGroupOrigin
                MplsTeP2mpTunnelDestSrcSubGroupID
                MplsTeP2mpTunnelDestSubGroupOriginType
                MplsTeP2mpTunnelDestSubGroupOrigin
                MplsTeP2mpTunnelDestSubGroupID
                MplsTeP2mpTunnelDestDestinationType
                MplsTeP2mpTunnelDestDestination

                The Object 
                retValMplsTeP2mpTunnelDestPathInUse
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTeP2mpTunnelDestPathInUse (UINT4 u4MplsTunnelIndex,
                                     UINT4 u4MplsTunnelInstance,
                                     UINT4 u4MplsTunnelIngressLSRId,
                                     UINT4 u4MplsTunnelEgressLSRId,
                                     INT4
                                     i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMplsTeP2mpTunnelDestSrcSubGroupOrigin,
                                     UINT4 u4MplsTeP2mpTunnelDestSrcSubGroupID,
                                     INT4
                                     i4MplsTeP2mpTunnelDestSubGroupOriginType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMplsTeP2mpTunnelDestSubGroupOrigin,
                                     UINT4 u4MplsTeP2mpTunnelDestSubGroupID,
                                     INT4 i4MplsTeP2mpTunnelDestDestinationType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMplsTeP2mpTunnelDestDestination,
                                     UINT4
                                     *pu4RetValMplsTeP2mpTunnelDestPathInUse)
{
    tTeMplsTeP2mpTunnelDestTable TeMplsTeP2mpTunnelDestTable;

    MEMSET (&TeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSrcSubGroupID =
        u4MplsTeP2mpTunnelDestSrcSubGroupID;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSubGroupOriginType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSubGroupOrigin,
            pMplsTeP2mpTunnelDestSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSubGroupID =
        u4MplsTeP2mpTunnelDestSubGroupID;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationType =
        i4MplsTeP2mpTunnelDestDestinationType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestDestination,
            pMplsTeP2mpTunnelDestDestination->pu1_OctetList,
            pMplsTeP2mpTunnelDestDestination->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.i4MplsTeP2mpTunnelDestDestinationLen =
        pMplsTeP2mpTunnelDestDestination->i4_Length;

    if (TeGetAllMplsTeP2mpTunnelDestTable (&TeMplsTeP2mpTunnelDestTable) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValMplsTeP2mpTunnelDestPathInUse =
        TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestPathInUse;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsTeP2mpTunnelDestCHopTableIndex
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelDestSrcSubGroupOriginType
                MplsTeP2mpTunnelDestSrcSubGroupOrigin
                MplsTeP2mpTunnelDestSrcSubGroupID
                MplsTeP2mpTunnelDestSubGroupOriginType
                MplsTeP2mpTunnelDestSubGroupOrigin
                MplsTeP2mpTunnelDestSubGroupID
                MplsTeP2mpTunnelDestDestinationType
                MplsTeP2mpTunnelDestDestination

                The Object 
                retValMplsTeP2mpTunnelDestCHopTableIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTeP2mpTunnelDestCHopTableIndex (UINT4 u4MplsTunnelIndex,
                                          UINT4 u4MplsTunnelInstance,
                                          UINT4 u4MplsTunnelIngressLSRId,
                                          UINT4 u4MplsTunnelEgressLSRId,
                                          INT4
                                          i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsTeP2mpTunnelDestSrcSubGroupOrigin,
                                          UINT4
                                          u4MplsTeP2mpTunnelDestSrcSubGroupID,
                                          INT4
                                          i4MplsTeP2mpTunnelDestSubGroupOriginType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsTeP2mpTunnelDestSubGroupOrigin,
                                          UINT4
                                          u4MplsTeP2mpTunnelDestSubGroupID,
                                          INT4
                                          i4MplsTeP2mpTunnelDestDestinationType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsTeP2mpTunnelDestDestination,
                                          UINT4
                                          *pu4RetValMplsTeP2mpTunnelDestCHopTableIndex)
{
    tTeMplsTeP2mpTunnelDestTable TeMplsTeP2mpTunnelDestTable;

    MEMSET (&TeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSrcSubGroupID =
        u4MplsTeP2mpTunnelDestSrcSubGroupID;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSubGroupOriginType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSubGroupOrigin,
            pMplsTeP2mpTunnelDestSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSubGroupID =
        u4MplsTeP2mpTunnelDestSubGroupID;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationType =
        i4MplsTeP2mpTunnelDestDestinationType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestDestination,
            pMplsTeP2mpTunnelDestDestination->pu1_OctetList,
            pMplsTeP2mpTunnelDestDestination->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.i4MplsTeP2mpTunnelDestDestinationLen =
        pMplsTeP2mpTunnelDestDestination->i4_Length;

    if (TeGetAllMplsTeP2mpTunnelDestTable (&TeMplsTeP2mpTunnelDestTable) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValMplsTeP2mpTunnelDestCHopTableIndex =
        TeMplsTeP2mpTunnelDestTable.MibObject.
        u4MplsTeP2mpTunnelDestCHopTableIndex;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsTeP2mpTunnelDestARHopTableIndex
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelDestSrcSubGroupOriginType
                MplsTeP2mpTunnelDestSrcSubGroupOrigin
                MplsTeP2mpTunnelDestSrcSubGroupID
                MplsTeP2mpTunnelDestSubGroupOriginType
                MplsTeP2mpTunnelDestSubGroupOrigin
                MplsTeP2mpTunnelDestSubGroupID
                MplsTeP2mpTunnelDestDestinationType
                MplsTeP2mpTunnelDestDestination

                The Object 
                retValMplsTeP2mpTunnelDestARHopTableIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTeP2mpTunnelDestARHopTableIndex (UINT4 u4MplsTunnelIndex,
                                           UINT4 u4MplsTunnelInstance,
                                           UINT4 u4MplsTunnelIngressLSRId,
                                           UINT4 u4MplsTunnelEgressLSRId,
                                           INT4
                                           i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pMplsTeP2mpTunnelDestSrcSubGroupOrigin,
                                           UINT4
                                           u4MplsTeP2mpTunnelDestSrcSubGroupID,
                                           INT4
                                           i4MplsTeP2mpTunnelDestSubGroupOriginType,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pMplsTeP2mpTunnelDestSubGroupOrigin,
                                           UINT4
                                           u4MplsTeP2mpTunnelDestSubGroupID,
                                           INT4
                                           i4MplsTeP2mpTunnelDestDestinationType,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pMplsTeP2mpTunnelDestDestination,
                                           UINT4
                                           *pu4RetValMplsTeP2mpTunnelDestARHopTableIndex)
{
    tTeMplsTeP2mpTunnelDestTable TeMplsTeP2mpTunnelDestTable;

    MEMSET (&TeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSrcSubGroupID =
        u4MplsTeP2mpTunnelDestSrcSubGroupID;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSubGroupOriginType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSubGroupOrigin,
            pMplsTeP2mpTunnelDestSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSubGroupID =
        u4MplsTeP2mpTunnelDestSubGroupID;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationType =
        i4MplsTeP2mpTunnelDestDestinationType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestDestination,
            pMplsTeP2mpTunnelDestDestination->pu1_OctetList,
            pMplsTeP2mpTunnelDestDestination->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.i4MplsTeP2mpTunnelDestDestinationLen =
        pMplsTeP2mpTunnelDestDestination->i4_Length;

    if (TeGetAllMplsTeP2mpTunnelDestTable (&TeMplsTeP2mpTunnelDestTable) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValMplsTeP2mpTunnelDestARHopTableIndex =
        TeMplsTeP2mpTunnelDestTable.MibObject.
        u4MplsTeP2mpTunnelDestARHopTableIndex;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsTeP2mpTunnelDestTotalUpTime
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelDestSrcSubGroupOriginType
                MplsTeP2mpTunnelDestSrcSubGroupOrigin
                MplsTeP2mpTunnelDestSrcSubGroupID
                MplsTeP2mpTunnelDestSubGroupOriginType
                MplsTeP2mpTunnelDestSubGroupOrigin
                MplsTeP2mpTunnelDestSubGroupID
                MplsTeP2mpTunnelDestDestinationType
                MplsTeP2mpTunnelDestDestination

                The Object 
                retValMplsTeP2mpTunnelDestTotalUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTeP2mpTunnelDestTotalUpTime (UINT4 u4MplsTunnelIndex,
                                       UINT4 u4MplsTunnelInstance,
                                       UINT4 u4MplsTunnelIngressLSRId,
                                       UINT4 u4MplsTunnelEgressLSRId,
                                       INT4
                                       i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMplsTeP2mpTunnelDestSrcSubGroupOrigin,
                                       UINT4
                                       u4MplsTeP2mpTunnelDestSrcSubGroupID,
                                       INT4
                                       i4MplsTeP2mpTunnelDestSubGroupOriginType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMplsTeP2mpTunnelDestSubGroupOrigin,
                                       UINT4 u4MplsTeP2mpTunnelDestSubGroupID,
                                       INT4
                                       i4MplsTeP2mpTunnelDestDestinationType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMplsTeP2mpTunnelDestDestination,
                                       UINT4
                                       *pu4RetValMplsTeP2mpTunnelDestTotalUpTime)
{
    tTeMplsTeP2mpTunnelDestTable TeMplsTeP2mpTunnelDestTable;

    MEMSET (&TeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSrcSubGroupID =
        u4MplsTeP2mpTunnelDestSrcSubGroupID;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSubGroupOriginType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSubGroupOrigin,
            pMplsTeP2mpTunnelDestSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSubGroupID =
        u4MplsTeP2mpTunnelDestSubGroupID;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationType =
        i4MplsTeP2mpTunnelDestDestinationType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestDestination,
            pMplsTeP2mpTunnelDestDestination->pu1_OctetList,
            pMplsTeP2mpTunnelDestDestination->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.i4MplsTeP2mpTunnelDestDestinationLen =
        pMplsTeP2mpTunnelDestDestination->i4_Length;

    if (TeGetAllMplsTeP2mpTunnelDestTable (&TeMplsTeP2mpTunnelDestTable) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValMplsTeP2mpTunnelDestTotalUpTime =
        TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestTotalUpTime;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsTeP2mpTunnelDestInstanceUpTime
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelDestSrcSubGroupOriginType
                MplsTeP2mpTunnelDestSrcSubGroupOrigin
                MplsTeP2mpTunnelDestSrcSubGroupID
                MplsTeP2mpTunnelDestSubGroupOriginType
                MplsTeP2mpTunnelDestSubGroupOrigin
                MplsTeP2mpTunnelDestSubGroupID
                MplsTeP2mpTunnelDestDestinationType
                MplsTeP2mpTunnelDestDestination

                The Object 
                retValMplsTeP2mpTunnelDestInstanceUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTeP2mpTunnelDestInstanceUpTime (UINT4 u4MplsTunnelIndex,
                                          UINT4 u4MplsTunnelInstance,
                                          UINT4 u4MplsTunnelIngressLSRId,
                                          UINT4 u4MplsTunnelEgressLSRId,
                                          INT4
                                          i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsTeP2mpTunnelDestSrcSubGroupOrigin,
                                          UINT4
                                          u4MplsTeP2mpTunnelDestSrcSubGroupID,
                                          INT4
                                          i4MplsTeP2mpTunnelDestSubGroupOriginType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsTeP2mpTunnelDestSubGroupOrigin,
                                          UINT4
                                          u4MplsTeP2mpTunnelDestSubGroupID,
                                          INT4
                                          i4MplsTeP2mpTunnelDestDestinationType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsTeP2mpTunnelDestDestination,
                                          UINT4
                                          *pu4RetValMplsTeP2mpTunnelDestInstanceUpTime)
{
    tTeMplsTeP2mpTunnelDestTable TeMplsTeP2mpTunnelDestTable;

    MEMSET (&TeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSrcSubGroupID =
        u4MplsTeP2mpTunnelDestSrcSubGroupID;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSubGroupOriginType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSubGroupOrigin,
            pMplsTeP2mpTunnelDestSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSubGroupID =
        u4MplsTeP2mpTunnelDestSubGroupID;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationType =
        i4MplsTeP2mpTunnelDestDestinationType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestDestination,
            pMplsTeP2mpTunnelDestDestination->pu1_OctetList,
            pMplsTeP2mpTunnelDestDestination->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.i4MplsTeP2mpTunnelDestDestinationLen =
        pMplsTeP2mpTunnelDestDestination->i4_Length;

    if (TeGetAllMplsTeP2mpTunnelDestTable (&TeMplsTeP2mpTunnelDestTable) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValMplsTeP2mpTunnelDestInstanceUpTime =
        TeMplsTeP2mpTunnelDestTable.MibObject.
        u4MplsTeP2mpTunnelDestInstanceUpTime;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsTeP2mpTunnelDestPathChanges
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelDestSrcSubGroupOriginType
                MplsTeP2mpTunnelDestSrcSubGroupOrigin
                MplsTeP2mpTunnelDestSrcSubGroupID
                MplsTeP2mpTunnelDestSubGroupOriginType
                MplsTeP2mpTunnelDestSubGroupOrigin
                MplsTeP2mpTunnelDestSubGroupID
                MplsTeP2mpTunnelDestDestinationType
                MplsTeP2mpTunnelDestDestination

                The Object 
                retValMplsTeP2mpTunnelDestPathChanges
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTeP2mpTunnelDestPathChanges (UINT4 u4MplsTunnelIndex,
                                       UINT4 u4MplsTunnelInstance,
                                       UINT4 u4MplsTunnelIngressLSRId,
                                       UINT4 u4MplsTunnelEgressLSRId,
                                       INT4
                                       i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMplsTeP2mpTunnelDestSrcSubGroupOrigin,
                                       UINT4
                                       u4MplsTeP2mpTunnelDestSrcSubGroupID,
                                       INT4
                                       i4MplsTeP2mpTunnelDestSubGroupOriginType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMplsTeP2mpTunnelDestSubGroupOrigin,
                                       UINT4 u4MplsTeP2mpTunnelDestSubGroupID,
                                       INT4
                                       i4MplsTeP2mpTunnelDestDestinationType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMplsTeP2mpTunnelDestDestination,
                                       UINT4
                                       *pu4RetValMplsTeP2mpTunnelDestPathChanges)
{
    tTeMplsTeP2mpTunnelDestTable TeMplsTeP2mpTunnelDestTable;

    MEMSET (&TeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSrcSubGroupID =
        u4MplsTeP2mpTunnelDestSrcSubGroupID;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSubGroupOriginType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSubGroupOrigin,
            pMplsTeP2mpTunnelDestSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSubGroupID =
        u4MplsTeP2mpTunnelDestSubGroupID;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationType =
        i4MplsTeP2mpTunnelDestDestinationType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestDestination,
            pMplsTeP2mpTunnelDestDestination->pu1_OctetList,
            pMplsTeP2mpTunnelDestDestination->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.i4MplsTeP2mpTunnelDestDestinationLen =
        pMplsTeP2mpTunnelDestDestination->i4_Length;

    if (TeGetAllMplsTeP2mpTunnelDestTable (&TeMplsTeP2mpTunnelDestTable) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValMplsTeP2mpTunnelDestPathChanges =
        TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestPathChanges;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsTeP2mpTunnelDestLastPathChange
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelDestSrcSubGroupOriginType
                MplsTeP2mpTunnelDestSrcSubGroupOrigin
                MplsTeP2mpTunnelDestSrcSubGroupID
                MplsTeP2mpTunnelDestSubGroupOriginType
                MplsTeP2mpTunnelDestSubGroupOrigin
                MplsTeP2mpTunnelDestSubGroupID
                MplsTeP2mpTunnelDestDestinationType
                MplsTeP2mpTunnelDestDestination

                The Object 
                retValMplsTeP2mpTunnelDestLastPathChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTeP2mpTunnelDestLastPathChange (UINT4 u4MplsTunnelIndex,
                                          UINT4 u4MplsTunnelInstance,
                                          UINT4 u4MplsTunnelIngressLSRId,
                                          UINT4 u4MplsTunnelEgressLSRId,
                                          INT4
                                          i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsTeP2mpTunnelDestSrcSubGroupOrigin,
                                          UINT4
                                          u4MplsTeP2mpTunnelDestSrcSubGroupID,
                                          INT4
                                          i4MplsTeP2mpTunnelDestSubGroupOriginType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsTeP2mpTunnelDestSubGroupOrigin,
                                          UINT4
                                          u4MplsTeP2mpTunnelDestSubGroupID,
                                          INT4
                                          i4MplsTeP2mpTunnelDestDestinationType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsTeP2mpTunnelDestDestination,
                                          UINT4
                                          *pu4RetValMplsTeP2mpTunnelDestLastPathChange)
{
    tTeMplsTeP2mpTunnelDestTable TeMplsTeP2mpTunnelDestTable;

    MEMSET (&TeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSrcSubGroupID =
        u4MplsTeP2mpTunnelDestSrcSubGroupID;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSubGroupOriginType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSubGroupOrigin,
            pMplsTeP2mpTunnelDestSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSubGroupID =
        u4MplsTeP2mpTunnelDestSubGroupID;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationType =
        i4MplsTeP2mpTunnelDestDestinationType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestDestination,
            pMplsTeP2mpTunnelDestDestination->pu1_OctetList,
            pMplsTeP2mpTunnelDestDestination->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.i4MplsTeP2mpTunnelDestDestinationLen =
        pMplsTeP2mpTunnelDestDestination->i4_Length;

    if (TeGetAllMplsTeP2mpTunnelDestTable (&TeMplsTeP2mpTunnelDestTable) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValMplsTeP2mpTunnelDestLastPathChange =
        TeMplsTeP2mpTunnelDestTable.MibObject.
        u4MplsTeP2mpTunnelDestLastPathChange;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsTeP2mpTunnelDestCreationTime
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelDestSrcSubGroupOriginType
                MplsTeP2mpTunnelDestSrcSubGroupOrigin
                MplsTeP2mpTunnelDestSrcSubGroupID
                MplsTeP2mpTunnelDestSubGroupOriginType
                MplsTeP2mpTunnelDestSubGroupOrigin
                MplsTeP2mpTunnelDestSubGroupID
                MplsTeP2mpTunnelDestDestinationType
                MplsTeP2mpTunnelDestDestination

                The Object 
                retValMplsTeP2mpTunnelDestCreationTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTeP2mpTunnelDestCreationTime (UINT4 u4MplsTunnelIndex,
                                        UINT4 u4MplsTunnelInstance,
                                        UINT4 u4MplsTunnelIngressLSRId,
                                        UINT4 u4MplsTunnelEgressLSRId,
                                        INT4
                                        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pMplsTeP2mpTunnelDestSrcSubGroupOrigin,
                                        UINT4
                                        u4MplsTeP2mpTunnelDestSrcSubGroupID,
                                        INT4
                                        i4MplsTeP2mpTunnelDestSubGroupOriginType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pMplsTeP2mpTunnelDestSubGroupOrigin,
                                        UINT4 u4MplsTeP2mpTunnelDestSubGroupID,
                                        INT4
                                        i4MplsTeP2mpTunnelDestDestinationType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pMplsTeP2mpTunnelDestDestination,
                                        UINT4
                                        *pu4RetValMplsTeP2mpTunnelDestCreationTime)
{
    tTeMplsTeP2mpTunnelDestTable TeMplsTeP2mpTunnelDestTable;

    MEMSET (&TeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSrcSubGroupID =
        u4MplsTeP2mpTunnelDestSrcSubGroupID;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSubGroupOriginType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSubGroupOrigin,
            pMplsTeP2mpTunnelDestSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSubGroupID =
        u4MplsTeP2mpTunnelDestSubGroupID;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationType =
        i4MplsTeP2mpTunnelDestDestinationType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestDestination,
            pMplsTeP2mpTunnelDestDestination->pu1_OctetList,
            pMplsTeP2mpTunnelDestDestination->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.i4MplsTeP2mpTunnelDestDestinationLen =
        pMplsTeP2mpTunnelDestDestination->i4_Length;

    if (TeGetAllMplsTeP2mpTunnelDestTable (&TeMplsTeP2mpTunnelDestTable) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValMplsTeP2mpTunnelDestCreationTime =
        TeMplsTeP2mpTunnelDestTable.MibObject.
        u4MplsTeP2mpTunnelDestCreationTime;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsTeP2mpTunnelDestStateTransitions
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelDestSrcSubGroupOriginType
                MplsTeP2mpTunnelDestSrcSubGroupOrigin
                MplsTeP2mpTunnelDestSrcSubGroupID
                MplsTeP2mpTunnelDestSubGroupOriginType
                MplsTeP2mpTunnelDestSubGroupOrigin
                MplsTeP2mpTunnelDestSubGroupID
                MplsTeP2mpTunnelDestDestinationType
                MplsTeP2mpTunnelDestDestination

                The Object 
                retValMplsTeP2mpTunnelDestStateTransitions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTeP2mpTunnelDestStateTransitions (UINT4 u4MplsTunnelIndex,
                                            UINT4 u4MplsTunnelInstance,
                                            UINT4 u4MplsTunnelIngressLSRId,
                                            UINT4 u4MplsTunnelEgressLSRId,
                                            INT4
                                            i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pMplsTeP2mpTunnelDestSrcSubGroupOrigin,
                                            UINT4
                                            u4MplsTeP2mpTunnelDestSrcSubGroupID,
                                            INT4
                                            i4MplsTeP2mpTunnelDestSubGroupOriginType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pMplsTeP2mpTunnelDestSubGroupOrigin,
                                            UINT4
                                            u4MplsTeP2mpTunnelDestSubGroupID,
                                            INT4
                                            i4MplsTeP2mpTunnelDestDestinationType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pMplsTeP2mpTunnelDestDestination,
                                            UINT4
                                            *pu4RetValMplsTeP2mpTunnelDestStateTransitions)
{
    tTeMplsTeP2mpTunnelDestTable TeMplsTeP2mpTunnelDestTable;

    MEMSET (&TeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSrcSubGroupID =
        u4MplsTeP2mpTunnelDestSrcSubGroupID;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSubGroupOriginType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSubGroupOrigin,
            pMplsTeP2mpTunnelDestSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSubGroupID =
        u4MplsTeP2mpTunnelDestSubGroupID;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationType =
        i4MplsTeP2mpTunnelDestDestinationType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestDestination,
            pMplsTeP2mpTunnelDestDestination->pu1_OctetList,
            pMplsTeP2mpTunnelDestDestination->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.i4MplsTeP2mpTunnelDestDestinationLen =
        pMplsTeP2mpTunnelDestDestination->i4_Length;

    if (TeGetAllMplsTeP2mpTunnelDestTable (&TeMplsTeP2mpTunnelDestTable) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValMplsTeP2mpTunnelDestStateTransitions =
        TeMplsTeP2mpTunnelDestTable.MibObject.
        u4MplsTeP2mpTunnelDestStateTransitions;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsTeP2mpTunnelDestDiscontinuityTime
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelDestSrcSubGroupOriginType
                MplsTeP2mpTunnelDestSrcSubGroupOrigin
                MplsTeP2mpTunnelDestSrcSubGroupID
                MplsTeP2mpTunnelDestSubGroupOriginType
                MplsTeP2mpTunnelDestSubGroupOrigin
                MplsTeP2mpTunnelDestSubGroupID
                MplsTeP2mpTunnelDestDestinationType
                MplsTeP2mpTunnelDestDestination

                The Object 
                retValMplsTeP2mpTunnelDestDiscontinuityTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTeP2mpTunnelDestDiscontinuityTime (UINT4 u4MplsTunnelIndex,
                                             UINT4 u4MplsTunnelInstance,
                                             UINT4 u4MplsTunnelIngressLSRId,
                                             UINT4 u4MplsTunnelEgressLSRId,
                                             INT4
                                             i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pMplsTeP2mpTunnelDestSrcSubGroupOrigin,
                                             UINT4
                                             u4MplsTeP2mpTunnelDestSrcSubGroupID,
                                             INT4
                                             i4MplsTeP2mpTunnelDestSubGroupOriginType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pMplsTeP2mpTunnelDestSubGroupOrigin,
                                             UINT4
                                             u4MplsTeP2mpTunnelDestSubGroupID,
                                             INT4
                                             i4MplsTeP2mpTunnelDestDestinationType,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pMplsTeP2mpTunnelDestDestination,
                                             UINT4
                                             *pu4RetValMplsTeP2mpTunnelDestDiscontinuityTime)
{
    tTeMplsTeP2mpTunnelDestTable TeMplsTeP2mpTunnelDestTable;

    MEMSET (&TeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSrcSubGroupID =
        u4MplsTeP2mpTunnelDestSrcSubGroupID;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSubGroupOriginType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSubGroupOrigin,
            pMplsTeP2mpTunnelDestSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSubGroupID =
        u4MplsTeP2mpTunnelDestSubGroupID;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationType =
        i4MplsTeP2mpTunnelDestDestinationType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestDestination,
            pMplsTeP2mpTunnelDestDestination->pu1_OctetList,
            pMplsTeP2mpTunnelDestDestination->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.i4MplsTeP2mpTunnelDestDestinationLen =
        pMplsTeP2mpTunnelDestDestination->i4_Length;

    if (TeGetAllMplsTeP2mpTunnelDestTable (&TeMplsTeP2mpTunnelDestTable) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValMplsTeP2mpTunnelDestDiscontinuityTime =
        TeMplsTeP2mpTunnelDestTable.MibObject.
        u4MplsTeP2mpTunnelDestDiscontinuityTime;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsTeP2mpTunnelDestAdminStatus
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelDestSrcSubGroupOriginType
                MplsTeP2mpTunnelDestSrcSubGroupOrigin
                MplsTeP2mpTunnelDestSrcSubGroupID
                MplsTeP2mpTunnelDestSubGroupOriginType
                MplsTeP2mpTunnelDestSubGroupOrigin
                MplsTeP2mpTunnelDestSubGroupID
                MplsTeP2mpTunnelDestDestinationType
                MplsTeP2mpTunnelDestDestination

                The Object 
                retValMplsTeP2mpTunnelDestAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTeP2mpTunnelDestAdminStatus (UINT4 u4MplsTunnelIndex,
                                       UINT4 u4MplsTunnelInstance,
                                       UINT4 u4MplsTunnelIngressLSRId,
                                       UINT4 u4MplsTunnelEgressLSRId,
                                       INT4
                                       i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMplsTeP2mpTunnelDestSrcSubGroupOrigin,
                                       UINT4
                                       u4MplsTeP2mpTunnelDestSrcSubGroupID,
                                       INT4
                                       i4MplsTeP2mpTunnelDestSubGroupOriginType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMplsTeP2mpTunnelDestSubGroupOrigin,
                                       UINT4 u4MplsTeP2mpTunnelDestSubGroupID,
                                       INT4
                                       i4MplsTeP2mpTunnelDestDestinationType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMplsTeP2mpTunnelDestDestination,
                                       INT4
                                       *pi4RetValMplsTeP2mpTunnelDestAdminStatus)
{
    tTeMplsTeP2mpTunnelDestTable TeMplsTeP2mpTunnelDestTable;

    MEMSET (&TeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSrcSubGroupID =
        u4MplsTeP2mpTunnelDestSrcSubGroupID;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSubGroupOriginType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSubGroupOrigin,
            pMplsTeP2mpTunnelDestSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSubGroupID =
        u4MplsTeP2mpTunnelDestSubGroupID;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationType =
        i4MplsTeP2mpTunnelDestDestinationType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestDestination,
            pMplsTeP2mpTunnelDestDestination->pu1_OctetList,
            pMplsTeP2mpTunnelDestDestination->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.i4MplsTeP2mpTunnelDestDestinationLen =
        pMplsTeP2mpTunnelDestDestination->i4_Length;

    if (TeGetAllMplsTeP2mpTunnelDestTable (&TeMplsTeP2mpTunnelDestTable) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValMplsTeP2mpTunnelDestAdminStatus =
        TeMplsTeP2mpTunnelDestTable.MibObject.i4MplsTeP2mpTunnelDestAdminStatus;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsTeP2mpTunnelDestOperStatus
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelDestSrcSubGroupOriginType
                MplsTeP2mpTunnelDestSrcSubGroupOrigin
                MplsTeP2mpTunnelDestSrcSubGroupID
                MplsTeP2mpTunnelDestSubGroupOriginType
                MplsTeP2mpTunnelDestSubGroupOrigin
                MplsTeP2mpTunnelDestSubGroupID
                MplsTeP2mpTunnelDestDestinationType
                MplsTeP2mpTunnelDestDestination

                The Object 
                retValMplsTeP2mpTunnelDestOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTeP2mpTunnelDestOperStatus (UINT4 u4MplsTunnelIndex,
                                      UINT4 u4MplsTunnelInstance,
                                      UINT4 u4MplsTunnelIngressLSRId,
                                      UINT4 u4MplsTunnelEgressLSRId,
                                      INT4
                                      i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsTeP2mpTunnelDestSrcSubGroupOrigin,
                                      UINT4 u4MplsTeP2mpTunnelDestSrcSubGroupID,
                                      INT4
                                      i4MplsTeP2mpTunnelDestSubGroupOriginType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsTeP2mpTunnelDestSubGroupOrigin,
                                      UINT4 u4MplsTeP2mpTunnelDestSubGroupID,
                                      INT4
                                      i4MplsTeP2mpTunnelDestDestinationType,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pMplsTeP2mpTunnelDestDestination,
                                      INT4
                                      *pi4RetValMplsTeP2mpTunnelDestOperStatus)
{
    tTeMplsTeP2mpTunnelDestTable TeMplsTeP2mpTunnelDestTable;

    MEMSET (&TeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSrcSubGroupID =
        u4MplsTeP2mpTunnelDestSrcSubGroupID;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSubGroupOriginType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSubGroupOrigin,
            pMplsTeP2mpTunnelDestSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSubGroupID =
        u4MplsTeP2mpTunnelDestSubGroupID;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationType =
        i4MplsTeP2mpTunnelDestDestinationType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestDestination,
            pMplsTeP2mpTunnelDestDestination->pu1_OctetList,
            pMplsTeP2mpTunnelDestDestination->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.i4MplsTeP2mpTunnelDestDestinationLen =
        pMplsTeP2mpTunnelDestDestination->i4_Length;

    if (TeGetAllMplsTeP2mpTunnelDestTable (&TeMplsTeP2mpTunnelDestTable) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValMplsTeP2mpTunnelDestOperStatus =
        TeMplsTeP2mpTunnelDestTable.MibObject.i4MplsTeP2mpTunnelDestOperStatus;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsTeP2mpTunnelDestRowStatus
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelDestSrcSubGroupOriginType
                MplsTeP2mpTunnelDestSrcSubGroupOrigin
                MplsTeP2mpTunnelDestSrcSubGroupID
                MplsTeP2mpTunnelDestSubGroupOriginType
                MplsTeP2mpTunnelDestSubGroupOrigin
                MplsTeP2mpTunnelDestSubGroupID
                MplsTeP2mpTunnelDestDestinationType
                MplsTeP2mpTunnelDestDestination

                The Object 
                retValMplsTeP2mpTunnelDestRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTeP2mpTunnelDestRowStatus (UINT4 u4MplsTunnelIndex,
                                     UINT4 u4MplsTunnelInstance,
                                     UINT4 u4MplsTunnelIngressLSRId,
                                     UINT4 u4MplsTunnelEgressLSRId,
                                     INT4
                                     i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMplsTeP2mpTunnelDestSrcSubGroupOrigin,
                                     UINT4 u4MplsTeP2mpTunnelDestSrcSubGroupID,
                                     INT4
                                     i4MplsTeP2mpTunnelDestSubGroupOriginType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMplsTeP2mpTunnelDestSubGroupOrigin,
                                     UINT4 u4MplsTeP2mpTunnelDestSubGroupID,
                                     INT4 i4MplsTeP2mpTunnelDestDestinationType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMplsTeP2mpTunnelDestDestination,
                                     INT4
                                     *pi4RetValMplsTeP2mpTunnelDestRowStatus)
{
    tTeMplsTeP2mpTunnelDestTable TeMplsTeP2mpTunnelDestTable;

    MEMSET (&TeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSrcSubGroupID =
        u4MplsTeP2mpTunnelDestSrcSubGroupID;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSubGroupOriginType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSubGroupOrigin,
            pMplsTeP2mpTunnelDestSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSubGroupID =
        u4MplsTeP2mpTunnelDestSubGroupID;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationType =
        i4MplsTeP2mpTunnelDestDestinationType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestDestination,
            pMplsTeP2mpTunnelDestDestination->pu1_OctetList,
            pMplsTeP2mpTunnelDestDestination->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.i4MplsTeP2mpTunnelDestDestinationLen =
        pMplsTeP2mpTunnelDestDestination->i4_Length;

    if (TeGetAllMplsTeP2mpTunnelDestTable (&TeMplsTeP2mpTunnelDestTable) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValMplsTeP2mpTunnelDestRowStatus =
        TeMplsTeP2mpTunnelDestTable.MibObject.i4MplsTeP2mpTunnelDestRowStatus;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsTeP2mpTunnelDestStorageType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelDestSrcSubGroupOriginType
                MplsTeP2mpTunnelDestSrcSubGroupOrigin
                MplsTeP2mpTunnelDestSrcSubGroupID
                MplsTeP2mpTunnelDestSubGroupOriginType
                MplsTeP2mpTunnelDestSubGroupOrigin
                MplsTeP2mpTunnelDestSubGroupID
                MplsTeP2mpTunnelDestDestinationType
                MplsTeP2mpTunnelDestDestination

                The Object 
                retValMplsTeP2mpTunnelDestStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTeP2mpTunnelDestStorageType (UINT4 u4MplsTunnelIndex,
                                       UINT4 u4MplsTunnelInstance,
                                       UINT4 u4MplsTunnelIngressLSRId,
                                       UINT4 u4MplsTunnelEgressLSRId,
                                       INT4
                                       i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMplsTeP2mpTunnelDestSrcSubGroupOrigin,
                                       UINT4
                                       u4MplsTeP2mpTunnelDestSrcSubGroupID,
                                       INT4
                                       i4MplsTeP2mpTunnelDestSubGroupOriginType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMplsTeP2mpTunnelDestSubGroupOrigin,
                                       UINT4 u4MplsTeP2mpTunnelDestSubGroupID,
                                       INT4
                                       i4MplsTeP2mpTunnelDestDestinationType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMplsTeP2mpTunnelDestDestination,
                                       INT4
                                       *pi4RetValMplsTeP2mpTunnelDestStorageType)
{
    tTeMplsTeP2mpTunnelDestTable TeMplsTeP2mpTunnelDestTable;

    MEMSET (&TeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSrcSubGroupID =
        u4MplsTeP2mpTunnelDestSrcSubGroupID;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSubGroupOriginType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSubGroupOrigin,
            pMplsTeP2mpTunnelDestSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSubGroupID =
        u4MplsTeP2mpTunnelDestSubGroupID;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationType =
        i4MplsTeP2mpTunnelDestDestinationType;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestDestination,
            pMplsTeP2mpTunnelDestDestination->pu1_OctetList,
            pMplsTeP2mpTunnelDestDestination->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.i4MplsTeP2mpTunnelDestDestinationLen =
        pMplsTeP2mpTunnelDestDestination->i4_Length;

    if (TeGetAllMplsTeP2mpTunnelDestTable (&TeMplsTeP2mpTunnelDestTable) !=
        SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pi4RetValMplsTeP2mpTunnelDestStorageType =
        TeMplsTeP2mpTunnelDestTable.MibObject.i4MplsTeP2mpTunnelDestStorageType;

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsTeP2mpTunnelDestBranchOutSegment
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelDestSrcSubGroupOriginType
                MplsTeP2mpTunnelDestSrcSubGroupOrigin
                MplsTeP2mpTunnelDestSrcSubGroupID
                MplsTeP2mpTunnelDestSubGroupOriginType
                MplsTeP2mpTunnelDestSubGroupOrigin
                MplsTeP2mpTunnelDestSubGroupID
                MplsTeP2mpTunnelDestDestinationType
                MplsTeP2mpTunnelDestDestination

                The Object 
                setValMplsTeP2mpTunnelDestBranchOutSegment
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTeP2mpTunnelDestBranchOutSegment (UINT4 u4MplsTunnelIndex,
                                            UINT4 u4MplsTunnelInstance,
                                            UINT4 u4MplsTunnelIngressLSRId,
                                            UINT4 u4MplsTunnelEgressLSRId,
                                            INT4
                                            i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pMplsTeP2mpTunnelDestSrcSubGroupOrigin,
                                            UINT4
                                            u4MplsTeP2mpTunnelDestSrcSubGroupID,
                                            INT4
                                            i4MplsTeP2mpTunnelDestSubGroupOriginType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pMplsTeP2mpTunnelDestSubGroupOrigin,
                                            UINT4
                                            u4MplsTeP2mpTunnelDestSubGroupID,
                                            INT4
                                            i4MplsTeP2mpTunnelDestDestinationType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pMplsTeP2mpTunnelDestDestination,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pSetValMplsTeP2mpTunnelDestBranchOutSegment)
{
    tTeMplsTeP2mpTunnelDestTable TeMplsTeP2mpTunnelDestTable;
    tTeIsSetMplsTeP2mpTunnelDestTable TeIsSetMplsTeP2mpTunnelDestTable;

    MEMSET (&TeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));
    MEMSET (&TeIsSetMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeIsSetMplsTeP2mpTunnelDestTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelIndex = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelInstance = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelIngressLSRId = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelEgressLSRId = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;
    TeIsSetMplsTeP2mpTunnelDestTable.
        bMplsTeP2mpTunnelDestSrcSubGroupOriginType = OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSrcSubGroupOrigin =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSrcSubGroupID =
        u4MplsTeP2mpTunnelDestSrcSubGroupID;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSrcSubGroupID =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSubGroupOriginType;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupOriginType =
        OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSubGroupOrigin,
            pMplsTeP2mpTunnelDestSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupOrigin =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSubGroupID =
        u4MplsTeP2mpTunnelDestSubGroupID;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupID =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationType =
        i4MplsTeP2mpTunnelDestDestinationType;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestDestinationType =
        OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestDestination,
            pMplsTeP2mpTunnelDestDestination->pu1_OctetList,
            pMplsTeP2mpTunnelDestDestination->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationLen =
        pMplsTeP2mpTunnelDestDestination->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestDestination =
        OSIX_TRUE;

    /* Assign the value */
    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestBranchOutSegment,
            pSetValMplsTeP2mpTunnelDestBranchOutSegment->pu1_OctetList,
            pSetValMplsTeP2mpTunnelDestBranchOutSegment->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestBranchOutSegmentLen =
        pSetValMplsTeP2mpTunnelDestBranchOutSegment->i4_Length;

    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestBranchOutSegment =
        OSIX_TRUE;

    if (TeSetAllMplsTeP2mpTunnelDestTable
        (&TeMplsTeP2mpTunnelDestTable, &TeIsSetMplsTeP2mpTunnelDestTable,
         OSIX_FALSE, OSIX_FALSE) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetMplsTeP2mpTunnelDestHopTableIndex
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelDestSrcSubGroupOriginType
                MplsTeP2mpTunnelDestSrcSubGroupOrigin
                MplsTeP2mpTunnelDestSrcSubGroupID
                MplsTeP2mpTunnelDestSubGroupOriginType
                MplsTeP2mpTunnelDestSubGroupOrigin
                MplsTeP2mpTunnelDestSubGroupID
                MplsTeP2mpTunnelDestDestinationType
                MplsTeP2mpTunnelDestDestination

                The Object 
                setValMplsTeP2mpTunnelDestHopTableIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTeP2mpTunnelDestHopTableIndex (UINT4 u4MplsTunnelIndex,
                                         UINT4 u4MplsTunnelInstance,
                                         UINT4 u4MplsTunnelIngressLSRId,
                                         UINT4 u4MplsTunnelEgressLSRId,
                                         INT4
                                         i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pMplsTeP2mpTunnelDestSrcSubGroupOrigin,
                                         UINT4
                                         u4MplsTeP2mpTunnelDestSrcSubGroupID,
                                         INT4
                                         i4MplsTeP2mpTunnelDestSubGroupOriginType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pMplsTeP2mpTunnelDestSubGroupOrigin,
                                         UINT4 u4MplsTeP2mpTunnelDestSubGroupID,
                                         INT4
                                         i4MplsTeP2mpTunnelDestDestinationType,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pMplsTeP2mpTunnelDestDestination,
                                         UINT4
                                         u4SetValMplsTeP2mpTunnelDestHopTableIndex)
{
    tTeMplsTeP2mpTunnelDestTable TeMplsTeP2mpTunnelDestTable;
    tTeIsSetMplsTeP2mpTunnelDestTable TeIsSetMplsTeP2mpTunnelDestTable;

    MEMSET (&TeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));
    MEMSET (&TeIsSetMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeIsSetMplsTeP2mpTunnelDestTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelIndex = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelInstance = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelIngressLSRId = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelEgressLSRId = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;
    TeIsSetMplsTeP2mpTunnelDestTable.
        bMplsTeP2mpTunnelDestSrcSubGroupOriginType = OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSrcSubGroupOrigin =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSrcSubGroupID =
        u4MplsTeP2mpTunnelDestSrcSubGroupID;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSrcSubGroupID =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSubGroupOriginType;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupOriginType =
        OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSubGroupOrigin,
            pMplsTeP2mpTunnelDestSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupOrigin =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSubGroupID =
        u4MplsTeP2mpTunnelDestSubGroupID;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupID =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationType =
        i4MplsTeP2mpTunnelDestDestinationType;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestDestinationType =
        OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestDestination,
            pMplsTeP2mpTunnelDestDestination->pu1_OctetList,
            pMplsTeP2mpTunnelDestDestination->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationLen =
        pMplsTeP2mpTunnelDestDestination->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestDestination =
        OSIX_TRUE;

    /* Assign the value */
    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestHopTableIndex =
        u4SetValMplsTeP2mpTunnelDestHopTableIndex;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestHopTableIndex =
        OSIX_TRUE;

    if (TeSetAllMplsTeP2mpTunnelDestTable
        (&TeMplsTeP2mpTunnelDestTable, &TeIsSetMplsTeP2mpTunnelDestTable,
         OSIX_FALSE, OSIX_FALSE) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetMplsTeP2mpTunnelDestPathInUse
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelDestSrcSubGroupOriginType
                MplsTeP2mpTunnelDestSrcSubGroupOrigin
                MplsTeP2mpTunnelDestSrcSubGroupID
                MplsTeP2mpTunnelDestSubGroupOriginType
                MplsTeP2mpTunnelDestSubGroupOrigin
                MplsTeP2mpTunnelDestSubGroupID
                MplsTeP2mpTunnelDestDestinationType
                MplsTeP2mpTunnelDestDestination

                The Object 
                setValMplsTeP2mpTunnelDestPathInUse
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTeP2mpTunnelDestPathInUse (UINT4 u4MplsTunnelIndex,
                                     UINT4 u4MplsTunnelInstance,
                                     UINT4 u4MplsTunnelIngressLSRId,
                                     UINT4 u4MplsTunnelEgressLSRId,
                                     INT4
                                     i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMplsTeP2mpTunnelDestSrcSubGroupOrigin,
                                     UINT4 u4MplsTeP2mpTunnelDestSrcSubGroupID,
                                     INT4
                                     i4MplsTeP2mpTunnelDestSubGroupOriginType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMplsTeP2mpTunnelDestSubGroupOrigin,
                                     UINT4 u4MplsTeP2mpTunnelDestSubGroupID,
                                     INT4 i4MplsTeP2mpTunnelDestDestinationType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMplsTeP2mpTunnelDestDestination,
                                     UINT4
                                     u4SetValMplsTeP2mpTunnelDestPathInUse)
{
    tTeMplsTeP2mpTunnelDestTable TeMplsTeP2mpTunnelDestTable;
    tTeIsSetMplsTeP2mpTunnelDestTable TeIsSetMplsTeP2mpTunnelDestTable;

    MEMSET (&TeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));
    MEMSET (&TeIsSetMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeIsSetMplsTeP2mpTunnelDestTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelIndex = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelInstance = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelIngressLSRId = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelEgressLSRId = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;
    TeIsSetMplsTeP2mpTunnelDestTable.
        bMplsTeP2mpTunnelDestSrcSubGroupOriginType = OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSrcSubGroupOrigin =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSrcSubGroupID =
        u4MplsTeP2mpTunnelDestSrcSubGroupID;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSrcSubGroupID =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSubGroupOriginType;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupOriginType =
        OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSubGroupOrigin,
            pMplsTeP2mpTunnelDestSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupOrigin =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSubGroupID =
        u4MplsTeP2mpTunnelDestSubGroupID;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupID =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationType =
        i4MplsTeP2mpTunnelDestDestinationType;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestDestinationType =
        OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestDestination,
            pMplsTeP2mpTunnelDestDestination->pu1_OctetList,
            pMplsTeP2mpTunnelDestDestination->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationLen =
        pMplsTeP2mpTunnelDestDestination->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestDestination =
        OSIX_TRUE;

    /* Assign the value */
    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestPathInUse =
        u4SetValMplsTeP2mpTunnelDestPathInUse;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestPathInUse = OSIX_TRUE;

    if (TeSetAllMplsTeP2mpTunnelDestTable
        (&TeMplsTeP2mpTunnelDestTable, &TeIsSetMplsTeP2mpTunnelDestTable,
         OSIX_FALSE, OSIX_FALSE) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetMplsTeP2mpTunnelDestAdminStatus
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelDestSrcSubGroupOriginType
                MplsTeP2mpTunnelDestSrcSubGroupOrigin
                MplsTeP2mpTunnelDestSrcSubGroupID
                MplsTeP2mpTunnelDestSubGroupOriginType
                MplsTeP2mpTunnelDestSubGroupOrigin
                MplsTeP2mpTunnelDestSubGroupID
                MplsTeP2mpTunnelDestDestinationType
                MplsTeP2mpTunnelDestDestination

                The Object 
                setValMplsTeP2mpTunnelDestAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTeP2mpTunnelDestAdminStatus (UINT4 u4MplsTunnelIndex,
                                       UINT4 u4MplsTunnelInstance,
                                       UINT4 u4MplsTunnelIngressLSRId,
                                       UINT4 u4MplsTunnelEgressLSRId,
                                       INT4
                                       i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMplsTeP2mpTunnelDestSrcSubGroupOrigin,
                                       UINT4
                                       u4MplsTeP2mpTunnelDestSrcSubGroupID,
                                       INT4
                                       i4MplsTeP2mpTunnelDestSubGroupOriginType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMplsTeP2mpTunnelDestSubGroupOrigin,
                                       UINT4 u4MplsTeP2mpTunnelDestSubGroupID,
                                       INT4
                                       i4MplsTeP2mpTunnelDestDestinationType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMplsTeP2mpTunnelDestDestination,
                                       INT4
                                       i4SetValMplsTeP2mpTunnelDestAdminStatus)
{
    tTeMplsTeP2mpTunnelDestTable TeMplsTeP2mpTunnelDestTable;
    tTeIsSetMplsTeP2mpTunnelDestTable TeIsSetMplsTeP2mpTunnelDestTable;

    MEMSET (&TeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));
    MEMSET (&TeIsSetMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeIsSetMplsTeP2mpTunnelDestTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelIndex = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelInstance = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelIngressLSRId = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelEgressLSRId = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;
    TeIsSetMplsTeP2mpTunnelDestTable.
        bMplsTeP2mpTunnelDestSrcSubGroupOriginType = OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSrcSubGroupOrigin =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSrcSubGroupID =
        u4MplsTeP2mpTunnelDestSrcSubGroupID;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSrcSubGroupID =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSubGroupOriginType;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupOriginType =
        OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSubGroupOrigin,
            pMplsTeP2mpTunnelDestSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupOrigin =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSubGroupID =
        u4MplsTeP2mpTunnelDestSubGroupID;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupID =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationType =
        i4MplsTeP2mpTunnelDestDestinationType;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestDestinationType =
        OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestDestination,
            pMplsTeP2mpTunnelDestDestination->pu1_OctetList,
            pMplsTeP2mpTunnelDestDestination->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationLen =
        pMplsTeP2mpTunnelDestDestination->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestDestination =
        OSIX_TRUE;

    /* Assign the value */
    TeMplsTeP2mpTunnelDestTable.MibObject.i4MplsTeP2mpTunnelDestAdminStatus =
        i4SetValMplsTeP2mpTunnelDestAdminStatus;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestAdminStatus =
        OSIX_TRUE;

    if (TeSetAllMplsTeP2mpTunnelDestTable
        (&TeMplsTeP2mpTunnelDestTable, &TeIsSetMplsTeP2mpTunnelDestTable,
         OSIX_FALSE, OSIX_FALSE) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetMplsTeP2mpTunnelDestRowStatus
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelDestSrcSubGroupOriginType
                MplsTeP2mpTunnelDestSrcSubGroupOrigin
                MplsTeP2mpTunnelDestSrcSubGroupID
                MplsTeP2mpTunnelDestSubGroupOriginType
                MplsTeP2mpTunnelDestSubGroupOrigin
                MplsTeP2mpTunnelDestSubGroupID
                MplsTeP2mpTunnelDestDestinationType
                MplsTeP2mpTunnelDestDestination

                The Object 
                setValMplsTeP2mpTunnelDestRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTeP2mpTunnelDestRowStatus (UINT4 u4MplsTunnelIndex,
                                     UINT4 u4MplsTunnelInstance,
                                     UINT4 u4MplsTunnelIngressLSRId,
                                     UINT4 u4MplsTunnelEgressLSRId,
                                     INT4
                                     i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMplsTeP2mpTunnelDestSrcSubGroupOrigin,
                                     UINT4 u4MplsTeP2mpTunnelDestSrcSubGroupID,
                                     INT4
                                     i4MplsTeP2mpTunnelDestSubGroupOriginType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMplsTeP2mpTunnelDestSubGroupOrigin,
                                     UINT4 u4MplsTeP2mpTunnelDestSubGroupID,
                                     INT4 i4MplsTeP2mpTunnelDestDestinationType,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pMplsTeP2mpTunnelDestDestination,
                                     INT4 i4SetValMplsTeP2mpTunnelDestRowStatus)
{
    tTeMplsTeP2mpTunnelDestTable TeMplsTeP2mpTunnelDestTable;
    tTeIsSetMplsTeP2mpTunnelDestTable TeIsSetMplsTeP2mpTunnelDestTable;

    MEMSET (&TeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));
    MEMSET (&TeIsSetMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeIsSetMplsTeP2mpTunnelDestTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelIndex = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelInstance = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelIngressLSRId = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelEgressLSRId = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;
    TeIsSetMplsTeP2mpTunnelDestTable.
        bMplsTeP2mpTunnelDestSrcSubGroupOriginType = OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSrcSubGroupOrigin =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSrcSubGroupID =
        u4MplsTeP2mpTunnelDestSrcSubGroupID;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSrcSubGroupID =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSubGroupOriginType;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupOriginType =
        OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSubGroupOrigin,
            pMplsTeP2mpTunnelDestSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupOrigin =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSubGroupID =
        u4MplsTeP2mpTunnelDestSubGroupID;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupID =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationType =
        i4MplsTeP2mpTunnelDestDestinationType;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestDestinationType =
        OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestDestination,
            pMplsTeP2mpTunnelDestDestination->pu1_OctetList,
            pMplsTeP2mpTunnelDestDestination->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationLen =
        pMplsTeP2mpTunnelDestDestination->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestDestination =
        OSIX_TRUE;

    /* Assign the value */
    TeMplsTeP2mpTunnelDestTable.MibObject.i4MplsTeP2mpTunnelDestRowStatus =
        i4SetValMplsTeP2mpTunnelDestRowStatus;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestRowStatus = OSIX_TRUE;

    if (TeSetAllMplsTeP2mpTunnelDestTable
        (&TeMplsTeP2mpTunnelDestTable, &TeIsSetMplsTeP2mpTunnelDestTable,
         OSIX_FALSE, OSIX_FALSE) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhSetMplsTeP2mpTunnelDestStorageType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelDestSrcSubGroupOriginType
                MplsTeP2mpTunnelDestSrcSubGroupOrigin
                MplsTeP2mpTunnelDestSrcSubGroupID
                MplsTeP2mpTunnelDestSubGroupOriginType
                MplsTeP2mpTunnelDestSubGroupOrigin
                MplsTeP2mpTunnelDestSubGroupID
                MplsTeP2mpTunnelDestDestinationType
                MplsTeP2mpTunnelDestDestination

                The Object 
                setValMplsTeP2mpTunnelDestStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTeP2mpTunnelDestStorageType (UINT4 u4MplsTunnelIndex,
                                       UINT4 u4MplsTunnelInstance,
                                       UINT4 u4MplsTunnelIngressLSRId,
                                       UINT4 u4MplsTunnelEgressLSRId,
                                       INT4
                                       i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMplsTeP2mpTunnelDestSrcSubGroupOrigin,
                                       UINT4
                                       u4MplsTeP2mpTunnelDestSrcSubGroupID,
                                       INT4
                                       i4MplsTeP2mpTunnelDestSubGroupOriginType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMplsTeP2mpTunnelDestSubGroupOrigin,
                                       UINT4 u4MplsTeP2mpTunnelDestSubGroupID,
                                       INT4
                                       i4MplsTeP2mpTunnelDestDestinationType,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMplsTeP2mpTunnelDestDestination,
                                       INT4
                                       i4SetValMplsTeP2mpTunnelDestStorageType)
{
    tTeMplsTeP2mpTunnelDestTable TeMplsTeP2mpTunnelDestTable;
    tTeIsSetMplsTeP2mpTunnelDestTable TeIsSetMplsTeP2mpTunnelDestTable;

    MEMSET (&TeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));
    MEMSET (&TeIsSetMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeIsSetMplsTeP2mpTunnelDestTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelIndex = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelInstance = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelIngressLSRId = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelEgressLSRId = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;
    TeIsSetMplsTeP2mpTunnelDestTable.
        bMplsTeP2mpTunnelDestSrcSubGroupOriginType = OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSrcSubGroupOrigin =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSrcSubGroupID =
        u4MplsTeP2mpTunnelDestSrcSubGroupID;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSrcSubGroupID =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSubGroupOriginType;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupOriginType =
        OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSubGroupOrigin,
            pMplsTeP2mpTunnelDestSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupOrigin =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSubGroupID =
        u4MplsTeP2mpTunnelDestSubGroupID;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupID =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationType =
        i4MplsTeP2mpTunnelDestDestinationType;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestDestinationType =
        OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestDestination,
            pMplsTeP2mpTunnelDestDestination->pu1_OctetList,
            pMplsTeP2mpTunnelDestDestination->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationLen =
        pMplsTeP2mpTunnelDestDestination->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestDestination =
        OSIX_TRUE;

    /* Assign the value */
    TeMplsTeP2mpTunnelDestTable.MibObject.i4MplsTeP2mpTunnelDestStorageType =
        i4SetValMplsTeP2mpTunnelDestStorageType;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestStorageType =
        OSIX_TRUE;

    if (TeSetAllMplsTeP2mpTunnelDestTable
        (&TeMplsTeP2mpTunnelDestTable, &TeIsSetMplsTeP2mpTunnelDestTable,
         OSIX_FALSE, OSIX_FALSE) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsTeP2mpTunnelDestBranchOutSegment
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelDestSrcSubGroupOriginType
                MplsTeP2mpTunnelDestSrcSubGroupOrigin
                MplsTeP2mpTunnelDestSrcSubGroupID
                MplsTeP2mpTunnelDestSubGroupOriginType
                MplsTeP2mpTunnelDestSubGroupOrigin
                MplsTeP2mpTunnelDestSubGroupID
                MplsTeP2mpTunnelDestDestinationType
                MplsTeP2mpTunnelDestDestination

                The Object 
                testValMplsTeP2mpTunnelDestBranchOutSegment
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTeP2mpTunnelDestBranchOutSegment (UINT4 *pu4ErrorCode,
                                               UINT4 u4MplsTunnelIndex,
                                               UINT4 u4MplsTunnelInstance,
                                               UINT4 u4MplsTunnelIngressLSRId,
                                               UINT4 u4MplsTunnelEgressLSRId,
                                               INT4
                                               i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pMplsTeP2mpTunnelDestSrcSubGroupOrigin,
                                               UINT4
                                               u4MplsTeP2mpTunnelDestSrcSubGroupID,
                                               INT4
                                               i4MplsTeP2mpTunnelDestSubGroupOriginType,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pMplsTeP2mpTunnelDestSubGroupOrigin,
                                               UINT4
                                               u4MplsTeP2mpTunnelDestSubGroupID,
                                               INT4
                                               i4MplsTeP2mpTunnelDestDestinationType,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pMplsTeP2mpTunnelDestDestination,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pTestValMplsTeP2mpTunnelDestBranchOutSegment)
{
    tTeMplsTeP2mpTunnelDestTable TeMplsTeP2mpTunnelDestTable;
    tTeIsSetMplsTeP2mpTunnelDestTable TeIsSetMplsTeP2mpTunnelDestTable;

    MEMSET (&TeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));
    MEMSET (&TeIsSetMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeIsSetMplsTeP2mpTunnelDestTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelIndex = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelInstance = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelIngressLSRId = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelEgressLSRId = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;
    TeIsSetMplsTeP2mpTunnelDestTable.
        bMplsTeP2mpTunnelDestSrcSubGroupOriginType = OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSrcSubGroupOrigin =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSrcSubGroupID =
        u4MplsTeP2mpTunnelDestSrcSubGroupID;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSrcSubGroupID =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSubGroupOriginType;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupOriginType =
        OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSubGroupOrigin,
            pMplsTeP2mpTunnelDestSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupOrigin =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSubGroupID =
        u4MplsTeP2mpTunnelDestSubGroupID;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupID =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationType =
        i4MplsTeP2mpTunnelDestDestinationType;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestDestinationType =
        OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestDestination,
            pMplsTeP2mpTunnelDestDestination->pu1_OctetList,
            pMplsTeP2mpTunnelDestDestination->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationLen =
        pMplsTeP2mpTunnelDestDestination->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestDestination =
        OSIX_TRUE;

    /* Assign the value */
    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestBranchOutSegment,
            pTestValMplsTeP2mpTunnelDestBranchOutSegment->pu1_OctetList,
            pTestValMplsTeP2mpTunnelDestBranchOutSegment->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestBranchOutSegmentLen =
        pTestValMplsTeP2mpTunnelDestBranchOutSegment->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestBranchOutSegment =
        OSIX_TRUE;

    if (TeTestAllMplsTeP2mpTunnelDestTable
        (pu4ErrorCode, &TeMplsTeP2mpTunnelDestTable,
         &TeIsSetMplsTeP2mpTunnelDestTable, OSIX_FALSE,
         OSIX_FALSE) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2MplsTeP2mpTunnelDestHopTableIndex
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelDestSrcSubGroupOriginType
                MplsTeP2mpTunnelDestSrcSubGroupOrigin
                MplsTeP2mpTunnelDestSrcSubGroupID
                MplsTeP2mpTunnelDestSubGroupOriginType
                MplsTeP2mpTunnelDestSubGroupOrigin
                MplsTeP2mpTunnelDestSubGroupID
                MplsTeP2mpTunnelDestDestinationType
                MplsTeP2mpTunnelDestDestination

                The Object 
                testValMplsTeP2mpTunnelDestHopTableIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTeP2mpTunnelDestHopTableIndex (UINT4 *pu4ErrorCode,
                                            UINT4 u4MplsTunnelIndex,
                                            UINT4 u4MplsTunnelInstance,
                                            UINT4 u4MplsTunnelIngressLSRId,
                                            UINT4 u4MplsTunnelEgressLSRId,
                                            INT4
                                            i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pMplsTeP2mpTunnelDestSrcSubGroupOrigin,
                                            UINT4
                                            u4MplsTeP2mpTunnelDestSrcSubGroupID,
                                            INT4
                                            i4MplsTeP2mpTunnelDestSubGroupOriginType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pMplsTeP2mpTunnelDestSubGroupOrigin,
                                            UINT4
                                            u4MplsTeP2mpTunnelDestSubGroupID,
                                            INT4
                                            i4MplsTeP2mpTunnelDestDestinationType,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pMplsTeP2mpTunnelDestDestination,
                                            UINT4
                                            u4TestValMplsTeP2mpTunnelDestHopTableIndex)
{
    tTeMplsTeP2mpTunnelDestTable TeMplsTeP2mpTunnelDestTable;
    tTeIsSetMplsTeP2mpTunnelDestTable TeIsSetMplsTeP2mpTunnelDestTable;

    MEMSET (&TeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));
    MEMSET (&TeIsSetMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeIsSetMplsTeP2mpTunnelDestTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelIndex = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelInstance = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelIngressLSRId = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelEgressLSRId = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;
    TeIsSetMplsTeP2mpTunnelDestTable.
        bMplsTeP2mpTunnelDestSrcSubGroupOriginType = OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSrcSubGroupOrigin =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSrcSubGroupID =
        u4MplsTeP2mpTunnelDestSrcSubGroupID;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSrcSubGroupID =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSubGroupOriginType;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupOriginType =
        OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSubGroupOrigin,
            pMplsTeP2mpTunnelDestSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupOrigin =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSubGroupID =
        u4MplsTeP2mpTunnelDestSubGroupID;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupID =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationType =
        i4MplsTeP2mpTunnelDestDestinationType;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestDestinationType =
        OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestDestination,
            pMplsTeP2mpTunnelDestDestination->pu1_OctetList,
            pMplsTeP2mpTunnelDestDestination->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationLen =
        pMplsTeP2mpTunnelDestDestination->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestDestination =
        OSIX_TRUE;

    /* Assign the value */
    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestHopTableIndex =
        u4TestValMplsTeP2mpTunnelDestHopTableIndex;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestHopTableIndex =
        OSIX_TRUE;

    if (TeTestAllMplsTeP2mpTunnelDestTable
        (pu4ErrorCode, &TeMplsTeP2mpTunnelDestTable,
         &TeIsSetMplsTeP2mpTunnelDestTable, OSIX_FALSE,
         OSIX_FALSE) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2MplsTeP2mpTunnelDestPathInUse
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelDestSrcSubGroupOriginType
                MplsTeP2mpTunnelDestSrcSubGroupOrigin
                MplsTeP2mpTunnelDestSrcSubGroupID
                MplsTeP2mpTunnelDestSubGroupOriginType
                MplsTeP2mpTunnelDestSubGroupOrigin
                MplsTeP2mpTunnelDestSubGroupID
                MplsTeP2mpTunnelDestDestinationType
                MplsTeP2mpTunnelDestDestination

                The Object 
                testValMplsTeP2mpTunnelDestPathInUse
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTeP2mpTunnelDestPathInUse (UINT4 *pu4ErrorCode,
                                        UINT4 u4MplsTunnelIndex,
                                        UINT4 u4MplsTunnelInstance,
                                        UINT4 u4MplsTunnelIngressLSRId,
                                        UINT4 u4MplsTunnelEgressLSRId,
                                        INT4
                                        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pMplsTeP2mpTunnelDestSrcSubGroupOrigin,
                                        UINT4
                                        u4MplsTeP2mpTunnelDestSrcSubGroupID,
                                        INT4
                                        i4MplsTeP2mpTunnelDestSubGroupOriginType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pMplsTeP2mpTunnelDestSubGroupOrigin,
                                        UINT4 u4MplsTeP2mpTunnelDestSubGroupID,
                                        INT4
                                        i4MplsTeP2mpTunnelDestDestinationType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pMplsTeP2mpTunnelDestDestination,
                                        UINT4
                                        u4TestValMplsTeP2mpTunnelDestPathInUse)
{
    tTeMplsTeP2mpTunnelDestTable TeMplsTeP2mpTunnelDestTable;
    tTeIsSetMplsTeP2mpTunnelDestTable TeIsSetMplsTeP2mpTunnelDestTable;

    MEMSET (&TeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));
    MEMSET (&TeIsSetMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeIsSetMplsTeP2mpTunnelDestTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelIndex = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelInstance = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelIngressLSRId = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelEgressLSRId = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;
    TeIsSetMplsTeP2mpTunnelDestTable.
        bMplsTeP2mpTunnelDestSrcSubGroupOriginType = OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSrcSubGroupOrigin =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSrcSubGroupID =
        u4MplsTeP2mpTunnelDestSrcSubGroupID;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSrcSubGroupID =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSubGroupOriginType;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupOriginType =
        OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSubGroupOrigin,
            pMplsTeP2mpTunnelDestSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupOrigin =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSubGroupID =
        u4MplsTeP2mpTunnelDestSubGroupID;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupID =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationType =
        i4MplsTeP2mpTunnelDestDestinationType;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestDestinationType =
        OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestDestination,
            pMplsTeP2mpTunnelDestDestination->pu1_OctetList,
            pMplsTeP2mpTunnelDestDestination->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationLen =
        pMplsTeP2mpTunnelDestDestination->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestDestination =
        OSIX_TRUE;

    /* Assign the value */
    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestPathInUse =
        u4TestValMplsTeP2mpTunnelDestPathInUse;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestPathInUse = OSIX_TRUE;

    if (TeTestAllMplsTeP2mpTunnelDestTable
        (pu4ErrorCode, &TeMplsTeP2mpTunnelDestTable,
         &TeIsSetMplsTeP2mpTunnelDestTable, OSIX_FALSE,
         OSIX_FALSE) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2MplsTeP2mpTunnelDestAdminStatus
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelDestSrcSubGroupOriginType
                MplsTeP2mpTunnelDestSrcSubGroupOrigin
                MplsTeP2mpTunnelDestSrcSubGroupID
                MplsTeP2mpTunnelDestSubGroupOriginType
                MplsTeP2mpTunnelDestSubGroupOrigin
                MplsTeP2mpTunnelDestSubGroupID
                MplsTeP2mpTunnelDestDestinationType
                MplsTeP2mpTunnelDestDestination

                The Object 
                testValMplsTeP2mpTunnelDestAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTeP2mpTunnelDestAdminStatus (UINT4 *pu4ErrorCode,
                                          UINT4 u4MplsTunnelIndex,
                                          UINT4 u4MplsTunnelInstance,
                                          UINT4 u4MplsTunnelIngressLSRId,
                                          UINT4 u4MplsTunnelEgressLSRId,
                                          INT4
                                          i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsTeP2mpTunnelDestSrcSubGroupOrigin,
                                          UINT4
                                          u4MplsTeP2mpTunnelDestSrcSubGroupID,
                                          INT4
                                          i4MplsTeP2mpTunnelDestSubGroupOriginType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsTeP2mpTunnelDestSubGroupOrigin,
                                          UINT4
                                          u4MplsTeP2mpTunnelDestSubGroupID,
                                          INT4
                                          i4MplsTeP2mpTunnelDestDestinationType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsTeP2mpTunnelDestDestination,
                                          INT4
                                          i4TestValMplsTeP2mpTunnelDestAdminStatus)
{
    tTeMplsTeP2mpTunnelDestTable TeMplsTeP2mpTunnelDestTable;
    tTeIsSetMplsTeP2mpTunnelDestTable TeIsSetMplsTeP2mpTunnelDestTable;

    MEMSET (&TeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));
    MEMSET (&TeIsSetMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeIsSetMplsTeP2mpTunnelDestTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelIndex = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelInstance = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelIngressLSRId = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelEgressLSRId = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;
    TeIsSetMplsTeP2mpTunnelDestTable.
        bMplsTeP2mpTunnelDestSrcSubGroupOriginType = OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSrcSubGroupOrigin =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSrcSubGroupID =
        u4MplsTeP2mpTunnelDestSrcSubGroupID;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSrcSubGroupID =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSubGroupOriginType;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupOriginType =
        OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSubGroupOrigin,
            pMplsTeP2mpTunnelDestSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupOrigin =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSubGroupID =
        u4MplsTeP2mpTunnelDestSubGroupID;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupID =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationType =
        i4MplsTeP2mpTunnelDestDestinationType;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestDestinationType =
        OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestDestination,
            pMplsTeP2mpTunnelDestDestination->pu1_OctetList,
            pMplsTeP2mpTunnelDestDestination->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationLen =
        pMplsTeP2mpTunnelDestDestination->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestDestination =
        OSIX_TRUE;

    /* Assign the value */
    TeMplsTeP2mpTunnelDestTable.MibObject.i4MplsTeP2mpTunnelDestAdminStatus =
        i4TestValMplsTeP2mpTunnelDestAdminStatus;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestAdminStatus =
        OSIX_TRUE;

    if (TeTestAllMplsTeP2mpTunnelDestTable
        (pu4ErrorCode, &TeMplsTeP2mpTunnelDestTable,
         &TeIsSetMplsTeP2mpTunnelDestTable, OSIX_FALSE,
         OSIX_FALSE) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2MplsTeP2mpTunnelDestRowStatus
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelDestSrcSubGroupOriginType
                MplsTeP2mpTunnelDestSrcSubGroupOrigin
                MplsTeP2mpTunnelDestSrcSubGroupID
                MplsTeP2mpTunnelDestSubGroupOriginType
                MplsTeP2mpTunnelDestSubGroupOrigin
                MplsTeP2mpTunnelDestSubGroupID
                MplsTeP2mpTunnelDestDestinationType
                MplsTeP2mpTunnelDestDestination

                The Object 
                testValMplsTeP2mpTunnelDestRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTeP2mpTunnelDestRowStatus (UINT4 *pu4ErrorCode,
                                        UINT4 u4MplsTunnelIndex,
                                        UINT4 u4MplsTunnelInstance,
                                        UINT4 u4MplsTunnelIngressLSRId,
                                        UINT4 u4MplsTunnelEgressLSRId,
                                        INT4
                                        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pMplsTeP2mpTunnelDestSrcSubGroupOrigin,
                                        UINT4
                                        u4MplsTeP2mpTunnelDestSrcSubGroupID,
                                        INT4
                                        i4MplsTeP2mpTunnelDestSubGroupOriginType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pMplsTeP2mpTunnelDestSubGroupOrigin,
                                        UINT4 u4MplsTeP2mpTunnelDestSubGroupID,
                                        INT4
                                        i4MplsTeP2mpTunnelDestDestinationType,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pMplsTeP2mpTunnelDestDestination,
                                        INT4
                                        i4TestValMplsTeP2mpTunnelDestRowStatus)
{
    tTeMplsTeP2mpTunnelDestTable TeMplsTeP2mpTunnelDestTable;
    tTeIsSetMplsTeP2mpTunnelDestTable TeIsSetMplsTeP2mpTunnelDestTable;

    MEMSET (&TeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));
    MEMSET (&TeIsSetMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeIsSetMplsTeP2mpTunnelDestTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelIndex = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelInstance = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelIngressLSRId = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelEgressLSRId = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;
    TeIsSetMplsTeP2mpTunnelDestTable.
        bMplsTeP2mpTunnelDestSrcSubGroupOriginType = OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSrcSubGroupOrigin =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSrcSubGroupID =
        u4MplsTeP2mpTunnelDestSrcSubGroupID;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSrcSubGroupID =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSubGroupOriginType;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupOriginType =
        OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSubGroupOrigin,
            pMplsTeP2mpTunnelDestSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupOrigin =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSubGroupID =
        u4MplsTeP2mpTunnelDestSubGroupID;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupID =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationType =
        i4MplsTeP2mpTunnelDestDestinationType;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestDestinationType =
        OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestDestination,
            pMplsTeP2mpTunnelDestDestination->pu1_OctetList,
            pMplsTeP2mpTunnelDestDestination->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationLen =
        pMplsTeP2mpTunnelDestDestination->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestDestination =
        OSIX_TRUE;

    /* Assign the value */
    TeMplsTeP2mpTunnelDestTable.MibObject.i4MplsTeP2mpTunnelDestRowStatus =
        i4TestValMplsTeP2mpTunnelDestRowStatus;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestRowStatus = OSIX_TRUE;

    if (TeTestAllMplsTeP2mpTunnelDestTable
        (pu4ErrorCode, &TeMplsTeP2mpTunnelDestTable,
         &TeIsSetMplsTeP2mpTunnelDestTable, OSIX_FALSE,
         OSIX_FALSE) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2MplsTeP2mpTunnelDestStorageType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelDestSrcSubGroupOriginType
                MplsTeP2mpTunnelDestSrcSubGroupOrigin
                MplsTeP2mpTunnelDestSrcSubGroupID
                MplsTeP2mpTunnelDestSubGroupOriginType
                MplsTeP2mpTunnelDestSubGroupOrigin
                MplsTeP2mpTunnelDestSubGroupID
                MplsTeP2mpTunnelDestDestinationType
                MplsTeP2mpTunnelDestDestination

                The Object 
                testValMplsTeP2mpTunnelDestStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTeP2mpTunnelDestStorageType (UINT4 *pu4ErrorCode,
                                          UINT4 u4MplsTunnelIndex,
                                          UINT4 u4MplsTunnelInstance,
                                          UINT4 u4MplsTunnelIngressLSRId,
                                          UINT4 u4MplsTunnelEgressLSRId,
                                          INT4
                                          i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsTeP2mpTunnelDestSrcSubGroupOrigin,
                                          UINT4
                                          u4MplsTeP2mpTunnelDestSrcSubGroupID,
                                          INT4
                                          i4MplsTeP2mpTunnelDestSubGroupOriginType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsTeP2mpTunnelDestSubGroupOrigin,
                                          UINT4
                                          u4MplsTeP2mpTunnelDestSubGroupID,
                                          INT4
                                          i4MplsTeP2mpTunnelDestDestinationType,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pMplsTeP2mpTunnelDestDestination,
                                          INT4
                                          i4TestValMplsTeP2mpTunnelDestStorageType)
{
    tTeMplsTeP2mpTunnelDestTable TeMplsTeP2mpTunnelDestTable;
    tTeIsSetMplsTeP2mpTunnelDestTable TeIsSetMplsTeP2mpTunnelDestTable;

    MEMSET (&TeMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeMplsTeP2mpTunnelDestTable));
    MEMSET (&TeIsSetMplsTeP2mpTunnelDestTable, 0,
            sizeof (tTeIsSetMplsTeP2mpTunnelDestTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIndex = u4MplsTunnelIndex;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelIndex = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelInstance = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelIngressLSRId = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTunnelEgressLSRId = OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;
    TeIsSetMplsTeP2mpTunnelDestTable.
        bMplsTeP2mpTunnelDestSrcSubGroupOriginType = OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSrcSubGroupOrigin->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSrcSubGroupOrigin =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSrcSubGroupID =
        u4MplsTeP2mpTunnelDestSrcSubGroupID;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSrcSubGroupID =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginType =
        i4MplsTeP2mpTunnelDestSubGroupOriginType;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupOriginType =
        OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestSubGroupOrigin,
            pMplsTeP2mpTunnelDestSubGroupOrigin->pu1_OctetList,
            pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginLen =
        pMplsTeP2mpTunnelDestSubGroupOrigin->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupOrigin =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.u4MplsTeP2mpTunnelDestSubGroupID =
        u4MplsTeP2mpTunnelDestSubGroupID;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestSubGroupID =
        OSIX_TRUE;

    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationType =
        i4MplsTeP2mpTunnelDestDestinationType;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestDestinationType =
        OSIX_TRUE;

    MEMCPY (TeMplsTeP2mpTunnelDestTable.MibObject.
            au1MplsTeP2mpTunnelDestDestination,
            pMplsTeP2mpTunnelDestDestination->pu1_OctetList,
            pMplsTeP2mpTunnelDestDestination->i4_Length);
    TeMplsTeP2mpTunnelDestTable.MibObject.
        i4MplsTeP2mpTunnelDestDestinationLen =
        pMplsTeP2mpTunnelDestDestination->i4_Length;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestDestination =
        OSIX_TRUE;

    /* Assign the value */
    TeMplsTeP2mpTunnelDestTable.MibObject.i4MplsTeP2mpTunnelDestStorageType =
        i4TestValMplsTeP2mpTunnelDestStorageType;
    TeIsSetMplsTeP2mpTunnelDestTable.bMplsTeP2mpTunnelDestStorageType =
        OSIX_TRUE;

    if (TeTestAllMplsTeP2mpTunnelDestTable
        (pu4ErrorCode, &TeMplsTeP2mpTunnelDestTable,
         &TeIsSetMplsTeP2mpTunnelDestTable, OSIX_FALSE,
         OSIX_FALSE) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2MplsTeP2mpTunnelDestTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelDestSrcSubGroupOriginType
                MplsTeP2mpTunnelDestSrcSubGroupOrigin
                MplsTeP2mpTunnelDestSrcSubGroupID
                MplsTeP2mpTunnelDestSubGroupOriginType
                MplsTeP2mpTunnelDestSubGroupOrigin
                MplsTeP2mpTunnelDestSubGroupID
                MplsTeP2mpTunnelDestDestinationType
                MplsTeP2mpTunnelDestDestination
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MplsTeP2mpTunnelDestTable (UINT4 *pu4ErrorCode,
                                   tSnmpIndexList * pSnmpIndexList,
                                   tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MplsTeP2mpTunnelBranchPerfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsTeP2mpTunnelBranchPerfTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelBranchPerfBranch
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsTeP2mpTunnelBranchPerfTable (UINT4
                                                         u4MplsTunnelIndex,
                                                         UINT4
                                                         u4MplsTunnelInstance,
                                                         UINT4
                                                         u4MplsTunnelIngressLSRId,
                                                         UINT4
                                                         u4MplsTunnelEgressLSRId,
                                                         tSNMP_OCTET_STRING_TYPE
                                                         *
                                                         pMplsTeP2mpTunnelBranchPerfBranch)
{
    /* MPLS_P2MP_LSP_CHANGES - S */
    INT1                i1RetVal = SNMP_FAILURE;
    i1RetVal = nmhValidateIndexInstanceMplsTunnelTable (u4MplsTunnelIndex,
                                                        u4MplsTunnelInstance,
                                                        u4MplsTunnelIngressLSRId,
                                                        u4MplsTunnelEgressLSRId);
    if (SNMP_SUCCESS != i1RetVal)
    {
        return i1RetVal;
    }
    i1RetVal = nmhValidateIndexInstanceMplsOutSegmentTable
        (pMplsTeP2mpTunnelBranchPerfBranch);
    return i1RetVal;
    /* MPLS_P2MP_LSP_CHANGES - E */
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsTeP2mpTunnelBranchPerfTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelBranchPerfBranch
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsTeP2mpTunnelBranchPerfTable (UINT4 *pu4MplsTunnelIndex,
                                                 UINT4 *pu4MplsTunnelInstance,
                                                 UINT4
                                                 *pu4MplsTunnelIngressLSRId,
                                                 UINT4
                                                 *pu4MplsTunnelEgressLSRId,
                                                 tSNMP_OCTET_STRING_TYPE *
                                                 pMplsTeP2mpTunnelBranchPerfBranch)
{
    tTeMplsTeP2mpTunnelBranchPerfTable TeMplsTeP2mpTunnelBranchPerfTable;
    INT4                i4RetVal = SNMP_FAILURE;

    MEMSET (&TeMplsTeP2mpTunnelBranchPerfTable, 0,
            sizeof (tTeMplsTeP2mpTunnelBranchPerfTable));

    i4RetVal =
        TeGetFirstMplsTeP2mpTunnelBranchPerfTable
        (&TeMplsTeP2mpTunnelBranchPerfTable);

    if (i4RetVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* Assign the index */
    *pu4MplsTunnelIndex =
        TeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelIndex;

    *pu4MplsTunnelInstance =
        TeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelInstance;

    *pu4MplsTunnelIngressLSRId =
        TeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelIngressLSRId;

    *pu4MplsTunnelEgressLSRId =
        TeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelEgressLSRId;

    MEMCPY (pMplsTeP2mpTunnelBranchPerfBranch->pu1_OctetList,
            &(TeMplsTeP2mpTunnelBranchPerfTable.MibObject.
              au1MplsTeP2mpTunnelBranchPerfBranch),
            TeMplsTeP2mpTunnelBranchPerfTable.MibObject.
            i4MplsTeP2mpTunnelBranchPerfBranchLen);
    pMplsTeP2mpTunnelBranchPerfBranch->i4_Length =
        TeMplsTeP2mpTunnelBranchPerfTable.MibObject.
        i4MplsTeP2mpTunnelBranchPerfBranchLen;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsTeP2mpTunnelBranchPerfTable
 Input       :  The Indices
                MplsTunnelIndex
                nextMplsTunnelIndex
                MplsTunnelInstance
                nextMplsTunnelInstance
                MplsTunnelIngressLSRId
                nextMplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                nextMplsTunnelEgressLSRId
                MplsTeP2mpTunnelBranchPerfBranch
                nextMplsTeP2mpTunnelBranchPerfBranch
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMplsTeP2mpTunnelBranchPerfTable (UINT4 u4MplsTunnelIndex,
                                                UINT4 *pu4NextMplsTunnelIndex,
                                                UINT4 u4MplsTunnelInstance,
                                                UINT4
                                                *pu4NextMplsTunnelInstance,
                                                UINT4 u4MplsTunnelIngressLSRId,
                                                UINT4
                                                *pu4NextMplsTunnelIngressLSRId,
                                                UINT4 u4MplsTunnelEgressLSRId,
                                                UINT4
                                                *pu4NextMplsTunnelEgressLSRId,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pMplsTeP2mpTunnelBranchPerfBranch,
                                                tSNMP_OCTET_STRING_TYPE *
                                                pNextMplsTeP2mpTunnelBranchPerfBranch)
{
    tTeMplsTeP2mpTunnelBranchPerfTable TeMplsTeP2mpTunnelBranchPerfTable;
    tTeMplsTeP2mpTunnelBranchPerfTable NextTeMplsTeP2mpTunnelBranchPerfTable;
    INT4                i4RetVal = SNMP_FAILURE;

    MEMSET (&NextTeMplsTeP2mpTunnelBranchPerfTable, 0,
            sizeof (tTeMplsTeP2mpTunnelBranchPerfTable));

    MEMSET (&TeMplsTeP2mpTunnelBranchPerfTable, 0,
            sizeof (tTeMplsTeP2mpTunnelBranchPerfTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelIndex =
        u4MplsTunnelIndex;

    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;

    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;

    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;

    MEMCPY (&(TeMplsTeP2mpTunnelBranchPerfTable.MibObject.
              au1MplsTeP2mpTunnelBranchPerfBranch),
            pMplsTeP2mpTunnelBranchPerfBranch->pu1_OctetList,
            pMplsTeP2mpTunnelBranchPerfBranch->i4_Length);
    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.
        i4MplsTeP2mpTunnelBranchPerfBranchLen =
        pMplsTeP2mpTunnelBranchPerfBranch->i4_Length;

    i4RetVal =
        TeGetNextMplsTeP2mpTunnelBranchPerfTable
        (&TeMplsTeP2mpTunnelBranchPerfTable,
         &NextTeMplsTeP2mpTunnelBranchPerfTable);

    if (i4RetVal == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* Assign the next index */
    *pu4NextMplsTunnelIndex =
        NextTeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelIndex;

    *pu4NextMplsTunnelInstance =
        NextTeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelInstance;

    *pu4NextMplsTunnelIngressLSRId =
        NextTeMplsTeP2mpTunnelBranchPerfTable.MibObject.
        u4MplsTunnelIngressLSRId;

    *pu4NextMplsTunnelEgressLSRId =
        NextTeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelEgressLSRId;

    MEMCPY (pNextMplsTeP2mpTunnelBranchPerfBranch->pu1_OctetList,
            &(NextTeMplsTeP2mpTunnelBranchPerfTable.MibObject.
              au1MplsTeP2mpTunnelBranchPerfBranch),
            NextTeMplsTeP2mpTunnelBranchPerfTable.MibObject.
            i4MplsTeP2mpTunnelBranchPerfBranchLen);
    pNextMplsTeP2mpTunnelBranchPerfBranch->i4_Length =
        NextTeMplsTeP2mpTunnelBranchPerfTable.MibObject.
        i4MplsTeP2mpTunnelBranchPerfBranchLen;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsTeP2mpTunnelBranchPerfPackets
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelBranchPerfBranch

                The Object 
                retValMplsTeP2mpTunnelBranchPerfPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTeP2mpTunnelBranchPerfPackets (UINT4 u4MplsTunnelIndex,
                                         UINT4 u4MplsTunnelInstance,
                                         UINT4 u4MplsTunnelIngressLSRId,
                                         UINT4 u4MplsTunnelEgressLSRId,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pMplsTeP2mpTunnelBranchPerfBranch,
                                         UINT4
                                         *pu4RetValMplsTeP2mpTunnelBranchPerfPackets)
{

    tTeMplsTeP2mpTunnelBranchPerfTable TeMplsTeP2mpTunnelBranchPerfTable;

    MEMSET (&TeMplsTeP2mpTunnelBranchPerfTable, 0,
            sizeof (tTeMplsTeP2mpTunnelBranchPerfTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelIndex =
        u4MplsTunnelIndex;

    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;

    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;

    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;

    MEMCPY (TeMplsTeP2mpTunnelBranchPerfTable.MibObject.
            au1MplsTeP2mpTunnelBranchPerfBranch,
            pMplsTeP2mpTunnelBranchPerfBranch->pu1_OctetList,
            pMplsTeP2mpTunnelBranchPerfBranch->i4_Length);
    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.
        i4MplsTeP2mpTunnelBranchPerfBranchLen =
        pMplsTeP2mpTunnelBranchPerfBranch->i4_Length;

    if (TeGetAllMplsTeP2mpTunnelBranchPerfTable
        (&TeMplsTeP2mpTunnelBranchPerfTable) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValMplsTeP2mpTunnelBranchPerfPackets =
        TeMplsTeP2mpTunnelBranchPerfTable.MibObject.
        u4MplsTeP2mpTunnelBranchPerfPackets;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsTeP2mpTunnelBranchPerfHCPackets
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelBranchPerfBranch

                The Object 
                retValMplsTeP2mpTunnelBranchPerfHCPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTeP2mpTunnelBranchPerfHCPackets (UINT4 u4MplsTunnelIndex,
                                           UINT4 u4MplsTunnelInstance,
                                           UINT4 u4MplsTunnelIngressLSRId,
                                           UINT4 u4MplsTunnelEgressLSRId,
                                           tSNMP_OCTET_STRING_TYPE *
                                           pMplsTeP2mpTunnelBranchPerfBranch,
                                           tSNMP_COUNTER64_TYPE *
                                           pu8RetValMplsTeP2mpTunnelBranchPerfHCPackets)
{

    tTeMplsTeP2mpTunnelBranchPerfTable TeMplsTeP2mpTunnelBranchPerfTable;

    MEMSET (&TeMplsTeP2mpTunnelBranchPerfTable, 0,
            sizeof (tTeMplsTeP2mpTunnelBranchPerfTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelIndex =
        u4MplsTunnelIndex;

    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;

    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;

    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;

    MEMCPY (TeMplsTeP2mpTunnelBranchPerfTable.MibObject.
            au1MplsTeP2mpTunnelBranchPerfBranch,
            pMplsTeP2mpTunnelBranchPerfBranch->pu1_OctetList,
            pMplsTeP2mpTunnelBranchPerfBranch->i4_Length);
    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.
        i4MplsTeP2mpTunnelBranchPerfBranchLen =
        pMplsTeP2mpTunnelBranchPerfBranch->i4_Length;

    if (TeGetAllMplsTeP2mpTunnelBranchPerfTable
        (&TeMplsTeP2mpTunnelBranchPerfTable) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu8RetValMplsTeP2mpTunnelBranchPerfHCPackets =
        TeMplsTeP2mpTunnelBranchPerfTable.MibObject.
        u8MplsTeP2mpTunnelBranchPerfHCPackets;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsTeP2mpTunnelBranchPerfErrors
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelBranchPerfBranch

                The Object 
                retValMplsTeP2mpTunnelBranchPerfErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTeP2mpTunnelBranchPerfErrors (UINT4 u4MplsTunnelIndex,
                                        UINT4 u4MplsTunnelInstance,
                                        UINT4 u4MplsTunnelIngressLSRId,
                                        UINT4 u4MplsTunnelEgressLSRId,
                                        tSNMP_OCTET_STRING_TYPE *
                                        pMplsTeP2mpTunnelBranchPerfBranch,
                                        UINT4
                                        *pu4RetValMplsTeP2mpTunnelBranchPerfErrors)
{

    tTeMplsTeP2mpTunnelBranchPerfTable TeMplsTeP2mpTunnelBranchPerfTable;

    MEMSET (&TeMplsTeP2mpTunnelBranchPerfTable, 0,
            sizeof (tTeMplsTeP2mpTunnelBranchPerfTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelIndex =
        u4MplsTunnelIndex;

    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;

    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;

    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;

    MEMCPY (TeMplsTeP2mpTunnelBranchPerfTable.MibObject.
            au1MplsTeP2mpTunnelBranchPerfBranch,
            pMplsTeP2mpTunnelBranchPerfBranch->pu1_OctetList,
            pMplsTeP2mpTunnelBranchPerfBranch->i4_Length);
    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.
        i4MplsTeP2mpTunnelBranchPerfBranchLen =
        pMplsTeP2mpTunnelBranchPerfBranch->i4_Length;

    if (TeGetAllMplsTeP2mpTunnelBranchPerfTable
        (&TeMplsTeP2mpTunnelBranchPerfTable) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValMplsTeP2mpTunnelBranchPerfErrors =
        TeMplsTeP2mpTunnelBranchPerfTable.MibObject.
        u4MplsTeP2mpTunnelBranchPerfErrors;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsTeP2mpTunnelBranchPerfBytes
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelBranchPerfBranch

                The Object 
                retValMplsTeP2mpTunnelBranchPerfBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTeP2mpTunnelBranchPerfBytes (UINT4 u4MplsTunnelIndex,
                                       UINT4 u4MplsTunnelInstance,
                                       UINT4 u4MplsTunnelIngressLSRId,
                                       UINT4 u4MplsTunnelEgressLSRId,
                                       tSNMP_OCTET_STRING_TYPE *
                                       pMplsTeP2mpTunnelBranchPerfBranch,
                                       UINT4
                                       *pu4RetValMplsTeP2mpTunnelBranchPerfBytes)
{

    tTeMplsTeP2mpTunnelBranchPerfTable TeMplsTeP2mpTunnelBranchPerfTable;

    MEMSET (&TeMplsTeP2mpTunnelBranchPerfTable, 0,
            sizeof (tTeMplsTeP2mpTunnelBranchPerfTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelIndex =
        u4MplsTunnelIndex;

    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;

    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;

    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;

    MEMCPY (TeMplsTeP2mpTunnelBranchPerfTable.MibObject.
            au1MplsTeP2mpTunnelBranchPerfBranch,
            pMplsTeP2mpTunnelBranchPerfBranch->pu1_OctetList,
            pMplsTeP2mpTunnelBranchPerfBranch->i4_Length);
    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.
        i4MplsTeP2mpTunnelBranchPerfBranchLen =
        pMplsTeP2mpTunnelBranchPerfBranch->i4_Length;

    if (TeGetAllMplsTeP2mpTunnelBranchPerfTable
        (&TeMplsTeP2mpTunnelBranchPerfTable) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValMplsTeP2mpTunnelBranchPerfBytes =
        TeMplsTeP2mpTunnelBranchPerfTable.MibObject.
        u4MplsTeP2mpTunnelBranchPerfBytes;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsTeP2mpTunnelBranchPerfHCBytes
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelBranchPerfBranch

                The Object 
                retValMplsTeP2mpTunnelBranchPerfHCBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTeP2mpTunnelBranchPerfHCBytes (UINT4 u4MplsTunnelIndex,
                                         UINT4 u4MplsTunnelInstance,
                                         UINT4 u4MplsTunnelIngressLSRId,
                                         UINT4 u4MplsTunnelEgressLSRId,
                                         tSNMP_OCTET_STRING_TYPE *
                                         pMplsTeP2mpTunnelBranchPerfBranch,
                                         tSNMP_COUNTER64_TYPE *
                                         pu8RetValMplsTeP2mpTunnelBranchPerfHCBytes)
{

    tTeMplsTeP2mpTunnelBranchPerfTable TeMplsTeP2mpTunnelBranchPerfTable;

    MEMSET (&TeMplsTeP2mpTunnelBranchPerfTable, 0,
            sizeof (tTeMplsTeP2mpTunnelBranchPerfTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelIndex =
        u4MplsTunnelIndex;

    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;

    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;

    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;

    MEMCPY (TeMplsTeP2mpTunnelBranchPerfTable.MibObject.
            au1MplsTeP2mpTunnelBranchPerfBranch,
            pMplsTeP2mpTunnelBranchPerfBranch->pu1_OctetList,
            pMplsTeP2mpTunnelBranchPerfBranch->i4_Length);
    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.
        i4MplsTeP2mpTunnelBranchPerfBranchLen =
        pMplsTeP2mpTunnelBranchPerfBranch->i4_Length;

    if (TeGetAllMplsTeP2mpTunnelBranchPerfTable
        (&TeMplsTeP2mpTunnelBranchPerfTable) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu8RetValMplsTeP2mpTunnelBranchPerfHCBytes =
        TeMplsTeP2mpTunnelBranchPerfTable.MibObject.
        u8MplsTeP2mpTunnelBranchPerfHCBytes;

    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetMplsTeP2mpTunnelBranchDiscontinuityTime
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                MplsTeP2mpTunnelBranchPerfBranch

                The Object 
                retValMplsTeP2mpTunnelBranchDiscontinuityTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTeP2mpTunnelBranchDiscontinuityTime (UINT4 u4MplsTunnelIndex,
                                               UINT4 u4MplsTunnelInstance,
                                               UINT4 u4MplsTunnelIngressLSRId,
                                               UINT4 u4MplsTunnelEgressLSRId,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pMplsTeP2mpTunnelBranchPerfBranch,
                                               UINT4
                                               *pu4RetValMplsTeP2mpTunnelBranchDiscontinuityTime)
{

    tTeMplsTeP2mpTunnelBranchPerfTable TeMplsTeP2mpTunnelBranchPerfTable;

    MEMSET (&TeMplsTeP2mpTunnelBranchPerfTable, 0,
            sizeof (tTeMplsTeP2mpTunnelBranchPerfTable));

    /* Assign the index */
    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelIndex =
        u4MplsTunnelIndex;

    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelInstance =
        u4MplsTunnelInstance;

    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelIngressLSRId =
        u4MplsTunnelIngressLSRId;

    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.u4MplsTunnelEgressLSRId =
        u4MplsTunnelEgressLSRId;

    MEMCPY (TeMplsTeP2mpTunnelBranchPerfTable.MibObject.
            au1MplsTeP2mpTunnelBranchPerfBranch,
            pMplsTeP2mpTunnelBranchPerfBranch->pu1_OctetList,
            pMplsTeP2mpTunnelBranchPerfBranch->i4_Length);
    TeMplsTeP2mpTunnelBranchPerfTable.MibObject.
        i4MplsTeP2mpTunnelBranchPerfBranchLen =
        pMplsTeP2mpTunnelBranchPerfBranch->i4_Length;

    if (TeGetAllMplsTeP2mpTunnelBranchPerfTable
        (&TeMplsTeP2mpTunnelBranchPerfTable) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    /* Assign the value */
    *pu4RetValMplsTeP2mpTunnelBranchDiscontinuityTime =
        TeMplsTeP2mpTunnelBranchPerfTable.MibObject.
        u4MplsTeP2mpTunnelBranchDiscontinuityTime;

    return SNMP_SUCCESS;

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsTeP2mpTunnelNotificationEnable
 Input       :  The Indices

                The Object 
                retValMplsTeP2mpTunnelNotificationEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTeP2mpTunnelNotificationEnable (INT4
                                          *pi4RetValMplsTeP2mpTunnelNotificationEnable)
{
    if (TeGetMplsTeP2mpTunnelNotificationEnable
        (pi4RetValMplsTeP2mpTunnelNotificationEnable) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsTeP2mpTunnelNotificationEnable
 Input       :  The Indices

                The Object 
                setValMplsTeP2mpTunnelNotificationEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTeP2mpTunnelNotificationEnable (INT4
                                          i4SetValMplsTeP2mpTunnelNotificationEnable)
{
    if (TeSetMplsTeP2mpTunnelNotificationEnable
        (i4SetValMplsTeP2mpTunnelNotificationEnable) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsTeP2mpTunnelNotificationEnable
 Input       :  The Indices

                The Object 
                testValMplsTeP2mpTunnelNotificationEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTeP2mpTunnelNotificationEnable (UINT4 *pu4ErrorCode,
                                             INT4
                                             i4TestValMplsTeP2mpTunnelNotificationEnable)
{
    if (TeTestMplsTeP2mpTunnelNotificationEnable
        (pu4ErrorCode,
         i4TestValMplsTeP2mpTunnelNotificationEnable) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;

}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2MplsTeP2mpTunnelNotificationEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MplsTeP2mpTunnelNotificationEnable (UINT4 *pu4ErrorCode,
                                            tSnmpIndexList * pSnmpIndexList,
                                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
