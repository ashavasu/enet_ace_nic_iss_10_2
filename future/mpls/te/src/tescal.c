/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: 
 *
 * Description: This file contains the low level routines
 *              for the TE confiuration scalars.
 *******************************************************************/

#include "teincs.h"
#include "fsmplslw.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsMaxTnls
 Input       :  The Indices

                The Object 
                retValFsMplsMaxTnls
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsMaxTnls (UINT4 *pu4RetValFsMplsMaxTnls)
{
    *pu4RetValFsMplsMaxTnls = TE_MAX_TNLS (gTeGblInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsMaxHopLists
 Input       :  The Indices

                The Object 
                retValFsMplsMaxHopLists
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsMaxHopLists (UINT4 *pu4RetValFsMplsMaxHopLists)
{
    *pu4RetValFsMplsMaxHopLists = TE_MAX_HOP_LIST (gTeGblInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsMaxPathOptPerList
 Input       :  The Indices

                The Object 
                retValFsMplsMaxPathOptPerList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsMaxPathOptPerList (UINT4 *pu4RetValFsMplsMaxPathOptPerList)
{
    *pu4RetValFsMplsMaxPathOptPerList = TE_MAX_PO_PER_HOP_LIST (gTeGblInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsMaxHopsPerPathOption
 Input       :  The Indices

                The Object 
                retValFsMplsMaxHopsPerPathOption
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsMaxHopsPerPathOption (UINT4 *pu4RetValFsMplsMaxHopsPerPathOption)
{
    *pu4RetValFsMplsMaxHopsPerPathOption = TE_MAX_HOP_PER_PO (gTeGblInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsMaxArHopLists
 Input       :  The Indices

                The Object 
                retValFsMplsMaxArHopLists
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsMaxArHopLists (UINT4 *pu4RetValFsMplsMaxArHopLists)
{
    *pu4RetValFsMplsMaxArHopLists = TE_MAX_ARHOP_LIST (gTeGblInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsMaxHopsPerArHopList
 Input       :  The Indices

                The Object 
                retValFsMplsMaxHopsPerArHopList
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsMaxHopsPerArHopList (UINT4 *pu4RetValFsMplsMaxHopsPerArHopList)
{
    *pu4RetValFsMplsMaxHopsPerArHopList = TE_MAX_ARHOP_PER_LIST (gTeGblInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsMaxRsvpTrfcParams
 Input       :  The Indices

                The Object 
                retValFsMplsMaxRsvpTrfcParams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsMaxRsvpTrfcParams (UINT4 *pu4RetValFsMplsMaxRsvpTrfcParams)
{
    *pu4RetValFsMplsMaxRsvpTrfcParams = TE_MAX_RPTE_TRFC_PARAMS (gTeGblInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsMaxCrLdpTrfcParams
 Input       :  The Indices

                The Object 
                retValFsMplsMaxCrLdpTrfcParams
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsMaxCrLdpTrfcParams (UINT4 *pu4RetValFsMplsMaxCrLdpTrfcParams)
{
    *pu4RetValFsMplsMaxCrLdpTrfcParams = TE_MAX_CRLDP_TRFC_PARAMS (gTeGblInfo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsMaxDServElsps
 Input       :  The Indices

                The Object 
                retValFsMplsMaxDServElsps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsMaxDServElsps (UINT4 *pu4RetValFsMplsMaxDServElsps)
{
    *pu4RetValFsMplsMaxDServElsps = TE_MAX_DS_ELSPS (gTeGblInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsMaxDServLlsps
 Input       :  The Indices

                The Object 
                retValFsMplsMaxDServLlsps
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsMaxDServLlsps (UINT4 *pu4RetValFsMplsMaxDServLlsps)
{
    *pu4RetValFsMplsMaxDServLlsps = TE_MAX_DS_LLSPS (gTeGblInfo);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTeDebugLevel
 Input       :  The Indices

                The Object 
                retValFsMplsTeDebugLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTeDebugLevel (UINT4 *pu4RetValFsMplsTeDebugLevel)
{
    *pu4RetValFsMplsTeDebugLevel = gu4TeDbgFlag;

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMplsTeDebugLevel
 Input       :  The Indices

                The Object 
                setValFsMplsTeDebugLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsTeDebugLevel (UINT4 u4SetValFsMplsTeDebugLevel)
{
    gu4TeDbgFlag = u4SetValFsMplsTeDebugLevel;

    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMplsTeDebugLevel
 Input       :  The Indices

                The Object 
                testValFsMplsTeDebugLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsTeDebugLevel (UINT4 *pu4ErrorCode,
                             UINT4 u4TestValFsMplsTeDebugLevel)
{
    TE_SUPPRESS_WARNING (u4TestValFsMplsTeDebugLevel);
    TE_SUPPRESS_WARNING (pu4ErrorCode);

    return SNMP_SUCCESS;
}

UINT1
TeVerifyCfgParams ()
{

    if ((TE_MAX_DS_LLSPS (gTeGblInfo) + TE_MAX_DS_ELSPS (gTeGblInfo)) >
        TE_MAX_TNLS (gTeGblInfo))
    {
        return TE_FAILURE;
    }

    if ((TE_MAX_RPTE_TRFC_PARAMS (gTeGblInfo) +
         TE_MAX_CRLDP_TRFC_PARAMS (gTeGblInfo)) > TE_TNL_RSRC_INDEX_MAXVAL)
    {
        return TE_FAILURE;
    }
    TE_MAX_TRFC_PARAMS (gTeGblInfo) = TE_MAX_RPTE_TRFC_PARAMS (gTeGblInfo) +
        TE_MAX_CRLDP_TRFC_PARAMS (gTeGblInfo);

    return TE_SUCCESS;
}
