/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: teset1.c,v 1.73 2017/07/11 13:15:38 siva Exp $
 *
 * Description: This file contains the low level SET routines
 *              for the following TE MIB tables.
 *              - MplsTunnelTable
 *              - FsMplsFrrConstTable
 *              - FsMplsTunnelExtTable
 *******************************************************************/

#include "teincs.h"
#include "fsmplslw.h"
#include "stdtelw.h"
#include "mplslsr.h"
#include "fsmpfrlw.h"
#include "mplscli.h"
/* LOW LEVEL Routines for Table : MplsTunnelTable. */

/****************************************************************************
 Function    :  nmhSetMplsTunnelName
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValMplsTunnelName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelName (UINT4 u4MplsTunnelIndex, UINT4 u4MplsTunnelInstance,
                      UINT4 u4MplsTunnelIngressLSRId,
                      UINT4 u4MplsTunnelEgressLSRId,
                      tSNMP_OCTET_STRING_TYPE * pSetValMplsTunnelName)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        CPY_FROM_SNMP (TE_TNL_NAME (pTeTnlInfo), pSetValMplsTunnelName,
                       pSetValMplsTunnelName->i4_Length);
        TE_TNL_NAME (pTeTnlInfo)[pSetValMplsTunnelName->i4_Length] = 0;
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelDescr
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValMplsTunnelDescr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelDescr (UINT4 u4MplsTunnelIndex, UINT4 u4MplsTunnelInstance,
                       UINT4 u4MplsTunnelIngressLSRId,
                       UINT4 u4MplsTunnelEgressLSRId,
                       tSNMP_OCTET_STRING_TYPE * pSetValMplsTunnelDescr)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        CPY_FROM_SNMP (&TE_TNL_DESCR (pTeTnlInfo),
                       pSetValMplsTunnelDescr,
                       pSetValMplsTunnelDescr->i4_Length);
        TE_TNL_DESCR (pTeTnlInfo)[pSetValMplsTunnelDescr->i4_Length] = 0;
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelIsIf
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValMplsTunnelIsIf
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelIsIf (UINT4 u4MplsTunnelIndex, UINT4 u4MplsTunnelInstance,
                      UINT4 u4MplsTunnelIngressLSRId,
                      UINT4 u4MplsTunnelEgressLSRId,
                      INT4 i4SetValMplsTunnelIsIf)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        if (i4SetValMplsTunnelIsIf == TE_SNMP_TRUE)
        {
            TE_TNL_ISIF (pTeTnlInfo) = TE_TRUE;
        }
        else
        {
            TE_TNL_ISIF (pTeTnlInfo) = TE_FALSE;
        }
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelXCPointer
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValMplsTunnelXCPointer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelXCPointer (UINT4 u4MplsTunnelIndex,
                           UINT4 u4MplsTunnelInstance,
                           UINT4 u4MplsTunnelIngressLSRId,
                           UINT4 u4MplsTunnelEgressLSRId,
                           tSNMP_OID_TYPE * pSetValMplsTunnelXCPointer)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4XCIndex = 0;

    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {

        if (TE_TNL_ROW_STATUS (pTeTnlInfo) != TE_ACTIVE)
        {
            u4XCIndex = u4XCIndex |
                ((pSetValMplsTunnelXCPointer->
                  pu4_OidList[TE_TNL_XC_TABLE_DEF_OFFSET]) << 24);
            u4XCIndex = u4XCIndex |
                ((pSetValMplsTunnelXCPointer->
                  pu4_OidList[TE_TNL_XC_TABLE_DEF_OFFSET + 1]) << 16);
            u4XCIndex = u4XCIndex |
                ((pSetValMplsTunnelXCPointer->
                  pu4_OidList[TE_TNL_XC_TABLE_DEF_OFFSET + 2]) << 8);
            u4XCIndex = u4XCIndex |
                (pSetValMplsTunnelXCPointer->
                 pu4_OidList[TE_TNL_XC_TABLE_DEF_OFFSET + 3]);

            /* Tunnel back pointer reference from XC is added. */
            MplsDbUpdateTunnelXCIndex (pTeTnlInfo, NULL, u4XCIndex);
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelSignallingProto
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValMplsTunnelSignallingProto
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelSignallingProto (UINT4 u4MplsTunnelIndex,
                                 UINT4 u4MplsTunnelInstance,
                                 UINT4 u4MplsTunnelIngressLSRId,
                                 UINT4 u4MplsTunnelEgressLSRId,
                                 INT4 i4SetValMplsTunnelSignallingProto)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo ((UINT4) u4MplsTunnelIndex,
                                  u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        if (TE_TNL_ROW_STATUS (pTeTnlInfo) != TE_ACTIVE)
        {
            TE_TNL_SIGPROTO (pTeTnlInfo) =
                (UINT1) i4SetValMplsTunnelSignallingProto;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelSetupPrio
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValMplsTunnelSetupPrio
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelSetupPrio (UINT4 u4MplsTunnelIndex,
                           UINT4 u4MplsTunnelInstance,
                           UINT4 u4MplsTunnelIngressLSRId,
                           UINT4 u4MplsTunnelEgressLSRId,
                           INT4 i4SetValMplsTunnelSetupPrio)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex,
                                  u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        if (TE_TNL_ROW_STATUS (pTeTnlInfo) != TE_ACTIVE)
        {
            if (pTeTnlInfo->u1TnlSetPrio != i4SetValMplsTunnelSetupPrio)
            {
                pTeTnlInfo->bIsMbbRequired = TRUE;
            }

            TE_TNL_SETUP_PRIO (pTeTnlInfo) =
                (UINT1) i4SetValMplsTunnelSetupPrio;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelHoldingPrio
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValMplsTunnelHoldingPrio
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelHoldingPrio (UINT4 u4MplsTunnelIndex,
                             UINT4 u4MplsTunnelInstance,
                             UINT4 u4MplsTunnelIngressLSRId,
                             UINT4 u4MplsTunnelEgressLSRId,
                             INT4 i4SetValMplsTunnelHoldingPrio)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex,
                                  u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        if (TE_TNL_ROW_STATUS (pTeTnlInfo) != TE_ACTIVE)
        {
            if (TE_TNL_HLDNG_PRIO (pTeTnlInfo) !=
                (UINT1) i4SetValMplsTunnelHoldingPrio)
            {
                pTeTnlInfo->bIsMbbRequired = TRUE;
            }

            TE_TNL_HLDNG_PRIO (pTeTnlInfo) =
                (UINT1) i4SetValMplsTunnelHoldingPrio;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelSessionAttributes
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValMplsTunnelSessionAttributes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelSessionAttributes (UINT4 u4MplsTunnelIndex,
                                   UINT4 u4MplsTunnelInstance,
                                   UINT4 u4MplsTunnelIngressLSRId,
                                   UINT4 u4MplsTunnelEgressLSRId,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pSetValMplsTunnelSessionAttributes)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT1               u1IsRpteAdminDown = TE_FALSE;

    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex,
                                  u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (pSetValMplsTunnelSessionAttributes->pu1_OctetList[0] ==
        pTeTnlInfo->u1TnlSsnAttr)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }

    if (((!(pTeTnlInfo->u1TnlSsnAttr & TE_SSN_REC_ROUTE_BIT)) &&
         (pSetValMplsTunnelSessionAttributes->pu1_OctetList[0] &
          TE_SSN_REC_ROUTE_BIT)) ||
        ((pTeTnlInfo->u1TnlSsnAttr & TE_SSN_REC_ROUTE_BIT) &&
         (!(pSetValMplsTunnelSessionAttributes->pu1_OctetList[0] &
            TE_SSN_REC_ROUTE_BIT))))
    {
        /* For enabling or disabling Record Route, The tunnel need
         * not be not in service */
        TE_TNL_SSN_ATTR (pTeTnlInfo) =
            pSetValMplsTunnelSessionAttributes->pu1_OctetList[0];

        if (TE_TNL_ADMIN_STATUS (pTeTnlInfo) == TE_ADMIN_UP)
        {
            if (TeRpteTEEventHandler (TE_TNL_TX_PATH_MSG,
                                      pTeTnlInfo, &u1IsRpteAdminDown)
                == TE_RPTE_FAILURE)
            {
                TE_DBG (TE_MAIN_SNMP, "Posting to RSVP-TE failed\n");
            }
        }
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }

    else
    {
        if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
        {
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    if (!(pSetValMplsTunnelSessionAttributes->pu1_OctetList[0] &
          TE_SSN_REC_ROUTE_BIT))
    {
        pTeTnlInfo->bIsMbbRequired = TRUE;
    }

    TE_TNL_SSN_ATTR (pTeTnlInfo) =
        pSetValMplsTunnelSessionAttributes->pu1_OctetList[0];

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelLocalProtectInUse
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValMplsTunnelLocalProtectInUse
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelLocalProtectInUse (UINT4 u4MplsTunnelIndex,
                                   UINT4 u4MplsTunnelInstance,
                                   UINT4 u4MplsTunnelIngressLSRId,
                                   UINT4 u4MplsTunnelEgressLSRId,
                                   INT4 i4SetValMplsTunnelLocalProtectInUse)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex,
                                  u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        if (TE_TNL_ROW_STATUS (pTeTnlInfo) != TE_ACTIVE)
        {
            TE_TNL_LOCAL_PROTECTION_IN_USE (pTeTnlInfo) =
                (i4SetValMplsTunnelLocalProtectInUse ==
                 TE_SNMP_TRUE) ? LOCAL_PROT_IN_USE : LOCAL_PROT_NOT_APPLICABLE;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelResourcePointer
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValMplsTunnelResourcePointer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelResourcePointer (UINT4 u4MplsTunnelIndex,
                                 UINT4 u4MplsTunnelInstance,
                                 UINT4 u4MplsTunnelIngressLSRId,
                                 UINT4 u4MplsTunnelEgressLSRId,
                                 tSNMP_OID_TYPE *
                                 pSetValMplsTunnelResourcePointer)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4ResourceIndex = 0;

    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex,
                                  u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {

        if (TE_TNL_ROW_STATUS (pTeTnlInfo) != TE_ACTIVE)
        {
            if (TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo) == TE_DFLT_TRFC_PRAM_INDEX)
            {
                TE_TNLRSRC_NUM_OF_TUNNELS ((TE_TNL_TRFC_PARAM (pTeTnlInfo)))--;
            }
            u4ResourceIndex =
                pSetValMplsTunnelResourcePointer->
                pu4_OidList[pSetValMplsTunnelResourcePointer->u4_Length - 1];

            if (TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo) != u4ResourceIndex)
            {
                pTeTnlInfo->bIsMbbRequired = TRUE;
            }

            TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo) = u4ResourceIndex;
            /* TE_TNL_TRFC_PARAM will be updated when activate
             * RowStatus */
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelInstancePriority
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValMplsTunnelInstancePriority
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelInstancePriority (UINT4 u4MplsTunnelIndex,
                                  UINT4 u4MplsTunnelInstance,
                                  UINT4 u4MplsTunnelIngressLSRId,
                                  UINT4 u4MplsTunnelEgressLSRId,
                                  UINT4 u4SetValMplsTunnelInstancePriority)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex,
                                  u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        if (TE_TNL_ROW_STATUS (pTeTnlInfo) != TE_ACTIVE)
        {
            TE_TNL_INST_PRIO (pTeTnlInfo) = u4SetValMplsTunnelInstancePriority;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelHopTableIndex
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValMplsTunnelHopTableIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelHopTableIndex (UINT4 u4MplsTunnelIndex,
                               UINT4 u4MplsTunnelInstance,
                               UINT4 u4MplsTunnelIngressLSRId,
                               UINT4 u4MplsTunnelEgressLSRId,
                               UINT4 u4SetValMplsTunnelHopTableIndex)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex,
                                  u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        if (TE_TNL_ROW_STATUS (pTeTnlInfo) != TE_ACTIVE)
        {
            if (TE_TNL_HOP_TABLE_INDEX (pTeTnlInfo) !=
                u4SetValMplsTunnelHopTableIndex)
            {
                pTeTnlInfo->bIsMbbRequired = TRUE;
            }

            TE_TNL_HOP_TABLE_INDEX (pTeTnlInfo) =
                u4SetValMplsTunnelHopTableIndex;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelIncludeAnyAffinity
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValMplsTunnelIncludeAnyAffinity
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelIncludeAnyAffinity (UINT4 u4MplsTunnelIndex,
                                    UINT4 u4MplsTunnelInstance,
                                    UINT4 u4MplsTunnelIngressLSRId,
                                    UINT4 u4MplsTunnelEgressLSRId,
                                    UINT4 u4SetValMplsTunnelIncludeAnyAffinity)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex,
                                  u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        if (TE_TNL_ROW_STATUS (pTeTnlInfo) != TE_ACTIVE)
        {
            if (TE_TNL_INC_ANY_AFFINITY (pTeTnlInfo) !=
                u4SetValMplsTunnelIncludeAnyAffinity)
            {
                pTeTnlInfo->bIsMbbRequired = TRUE;
            }

            TE_TNL_INC_ANY_AFFINITY (pTeTnlInfo) =
                u4SetValMplsTunnelIncludeAnyAffinity;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelIncludeAllAffinity
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValMplsTunnelIncludeAllAffinity
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelIncludeAllAffinity (UINT4 u4MplsTunnelIndex,
                                    UINT4 u4MplsTunnelInstance,
                                    UINT4 u4MplsTunnelIngressLSRId,
                                    UINT4 u4MplsTunnelEgressLSRId,
                                    UINT4 u4SetValMplsTunnelIncludeAllAffinity)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex,
                                  u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        if (TE_TNL_ROW_STATUS (pTeTnlInfo) != TE_ACTIVE)
        {
            if (TE_TNL_INC_ALL_AFFINITY (pTeTnlInfo) !=
                u4SetValMplsTunnelIncludeAllAffinity)
            {
                pTeTnlInfo->bIsMbbRequired = TRUE;
            }

            TE_TNL_INC_ALL_AFFINITY (pTeTnlInfo) =
                u4SetValMplsTunnelIncludeAllAffinity;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelExcludeAnyAffinity
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValMplsTunnelExcludeAnyAffinity
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelExcludeAnyAffinity (UINT4 u4MplsTunnelIndex,
                                    UINT4 u4MplsTunnelInstance,
                                    UINT4 u4MplsTunnelIngressLSRId,
                                    UINT4 u4MplsTunnelEgressLSRId,
                                    UINT4 u4SetValMplsTunnelExcludeAnyAffinity)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        if (TE_TNL_ROW_STATUS (pTeTnlInfo) != TE_ACTIVE)
        {
            if (TE_TNL_EXC_ANY_AFFINITY (pTeTnlInfo) !=
                u4SetValMplsTunnelExcludeAnyAffinity)
            {
                pTeTnlInfo->bIsMbbRequired = TRUE;
            }

            TE_TNL_EXC_ANY_AFFINITY (pTeTnlInfo) =
                u4SetValMplsTunnelExcludeAnyAffinity;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelPathInUse
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValMplsTunnelPathInUse
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelPathInUse (UINT4 u4MplsTunnelIndex,
                           UINT4 u4MplsTunnelInstance,
                           UINT4 u4MplsTunnelIngressLSRId,
                           UINT4 u4MplsTunnelEgressLSRId,
                           UINT4 u4SetValMplsTunnelPathInUse)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        /* Association of pTepathInfo with TetnlInfo is done here */
        if (TE_TNL_ROW_STATUS (pTeTnlInfo) != TE_ACTIVE)
        {
            TE_TNL_PATH_IN_USE (pTeTnlInfo) =
                (UINT1) u4SetValMplsTunnelPathInUse;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelRole
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValMplsTunnelRole
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelRole (UINT4 u4MplsTunnelIndex, UINT4 u4MplsTunnelInstance,
                      UINT4 u4MplsTunnelIngressLSRId,
                      UINT4 u4MplsTunnelEgressLSRId,
                      INT4 i4SetValMplsTunnelRole)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        if (TE_TNL_ROW_STATUS (pTeTnlInfo) != TE_ACTIVE)
        {
            TE_TNL_ROLE (pTeTnlInfo) = (UINT1) i4SetValMplsTunnelRole;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelAdminStatus
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValMplsTunnelAdminStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelAdminStatus (UINT4 u4MplsTunnelIndex,
                             UINT4 u4MplsTunnelInstance,
                             UINT4 u4MplsTunnelIngressLSRId,
                             UINT4 u4MplsTunnelEgressLSRId,
                             INT4 i4SetValMplsTunnelAdminStatus)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT1               u1IsRpteAdminDown = TE_FALSE;

    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (TE_TNL_OWNER (pTeTnlInfo) != TE_TNL_OWNER_SNMP)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (TE_TNL_ROW_STATUS (pTeTnlInfo) != TE_ACTIVE)
    {
        TE_TNL_ADMIN_STATUS (pTeTnlInfo) =
            (UINT1) i4SetValMplsTunnelAdminStatus;
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }

    switch (i4SetValMplsTunnelAdminStatus)
    {
        case TE_ADMIN_UP:

            if (TE_TNL_ADMIN_STATUS (pTeTnlInfo) == TE_ADMIN_UP)
            {
                break;
            }

            TE_TNL_ADMIN_STATUS (pTeTnlInfo) = TE_ADMIN_UP;

            if ((TE_TNL_SIGPROTO (pTeTnlInfo) != TE_SIGPROTO_NONE) &&
                (u4MplsTunnelInstance != TE_ZERO))
            {
                if (TE_TNL_SIG_STATE (pTeTnlInfo) == TE_SIG_NOT_DONE)
                {
                    TE_TNL_PRIMARY_TNL_INSTANCE (pTeTnlInfo) =
                        (TE_TNL_PRIMARY_TNL_INSTANCE (pTeTnlInfo) +
                         1) % TE_TNLINST_MAXVAL;

                    TePrcsL2vpnAssociation (pTeTnlInfo, TE_TNL_REESTB);
                }
            }

            if (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_RSVP)
            {
                /*if (pTeTnlInfo->u1TnlMBBStatus == MPLS_TE_MBB_DISABLED) */
                pTeTnlInfo->bIsMbbRequired = FALSE;

                if (TeRpteTEEventHandler (TE_TNL_UP,
                                          pTeTnlInfo, &u1IsRpteAdminDown)
                    == TE_RPTE_FAILURE)
                {
                    TE_TNL_ADMIN_STATUS (pTeTnlInfo) = TE_ADMIN_DOWN;
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }

                if ((pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
                     MPLS_TE_DEDICATED_ONE2ONE) &&
                    (pTeTnlInfo->pTeBackupPathInfo != NULL))
                {
                    TE_DBG2 (TE_MAIN_SNMP, "Triggering backup-path for "
                             "tnl: %u %u\n", pTeTnlInfo->u4TnlIndex,
                             pTeTnlInfo->u4TnlInstance);
                    if (TeRpteTEEventHandler (TE_TNL_UP,
                                              pTeTnlInfo, &u1IsRpteAdminDown)
                        == TE_RPTE_FAILURE)
                    {
                        TE_TNL_ADMIN_STATUS (pTeTnlInfo) = TE_ADMIN_DOWN;
                        MPLS_CMN_UNLOCK ();
                        return SNMP_FAILURE;
                    }
                }

            }
            else if (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_LDP)
            {
                if (TeLdpTEEventHandler (TE_DATA_TX_ENABLE,
                                         pTeTnlInfo) != TE_LDP_SUCCESS)
                {
                    TE_TNL_ADMIN_STATUS (pTeTnlInfo) = TE_ADMIN_DOWN;
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
            }
            else
            {
                /* MPLS_P2MP_LSP_CHANGES - S */
                if ((MplsCheckP2mpTnlRoleNotEgress (pTeTnlInfo))
                    == MPLS_SUCCESS)
                {
                    if (TeP2mpExtSetOperStatusAndProgHw (pTeTnlInfo,
                                                         TE_OPER_UP) ==
                        TE_FAILURE)
                    {
                        TE_TNL_ADMIN_STATUS (pTeTnlInfo) = TE_ADMIN_DOWN;
                        MPLS_CMN_UNLOCK ();
                        return SNMP_FAILURE;
                    }
                }
                /* MPLS_P2MP_LSP_CHANGES - E */
                else if (TeCmnExtSetOperStatusAndProgHw (pTeTnlInfo, TE_OPER_UP)
                         == TE_FAILURE)
                {
                    TE_TNL_ADMIN_STATUS (pTeTnlInfo) = TE_ADMIN_DOWN;
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
            }
            break;

        case TE_ADMIN_DOWN:

            if (TE_TNL_ADMIN_STATUS (pTeTnlInfo) == TE_ADMIN_DOWN)
            {
                break;
            }

            TE_TNL_ADMIN_STATUS (pTeTnlInfo) = TE_ADMIN_DOWN;
            if (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_RSVP)
            {
                if ((TeRpteTEEventHandler (TE_TNL_DOWN,
                                           pTeTnlInfo, &u1IsRpteAdminDown))
                    == TE_RPTE_FAILURE)
                {
                    TE_TNL_ADMIN_STATUS (pTeTnlInfo) = TE_ADMIN_UP;
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
            }
            else if (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_LDP)
            {
                if (TeLdpTEEventHandler (TE_DATA_TX_DISABLE,
                                         pTeTnlInfo) != TE_LDP_SUCCESS)
                {
                    TE_TNL_ADMIN_STATUS (pTeTnlInfo) = TE_ADMIN_UP;
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
            }
            else
            {
                /* MPLS_P2MP_LSP_CHANGES - S */
                if ((MplsCheckP2mpTnlRoleNotEgress (pTeTnlInfo))
                    == MPLS_SUCCESS)
                {
                    if (TeP2mpExtSetOperStatusAndProgHw (pTeTnlInfo,
                                                         TE_OPER_DOWN) ==
                        TE_FAILURE)
                    {
                        TE_TNL_ADMIN_STATUS (pTeTnlInfo) = TE_ADMIN_UP;
                        MPLS_CMN_UNLOCK ();
                        return SNMP_FAILURE;
                    }
                }
                /* MPLS_P2MP_LSP_CHANGES - E */
                else if (TeCmnExtSetOperStatusAndProgHw (pTeTnlInfo,
                                                         TE_OPER_DOWN) ==
                         TE_FAILURE)
                {
                    TE_TNL_ADMIN_STATUS (pTeTnlInfo) = TE_ADMIN_UP;
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
            }
            break;
        default:
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelRowStatus
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValMplsTunnelRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelRowStatus (UINT4 u4MplsTunnelIndex,
                           UINT4 u4MplsTunnelInstance,
                           UINT4 u4MplsTunnelIngressLSRId,
                           UINT4 u4MplsTunnelEgressLSRId,
                           INT4 i4SetValMplsTunnelRowStatus)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlInfo         *pInstance0Tnl = NULL;
    UINT4               u4TnlHopTableIndex = TE_ZERO;
    UINT4               u4TnlBackupHopTableIndex = TE_ZERO;
    UINT4               u4Flag = TE_ZERO;
    UINT4               u4TnlXcIndex = 0;
    UINT1               u1RetVal;
    UINT1               u1TempTnlRowStatus;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;
    UINT4               u4Index;
    BOOL1               bIsPathChanged = TE_FALSE;
    UINT1               u1IsRpteAdminDown = TE_FALSE;

    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);

    if ((pTeTnlInfo == NULL) &&
        (i4SetValMplsTunnelRowStatus != TE_CREATEANDWAIT))
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pInstance0Tnl = TeGetTunnelInfo (u4MplsTunnelIndex, TE_ZERO,
                                     u4MplsTunnelIngressLSRId,
                                     u4MplsTunnelEgressLSRId);

    switch (i4SetValMplsTunnelRowStatus)
    {
        case TE_ACTIVE:
            if (pTeTnlInfo == NULL)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
            if ((TE_TNL_OWNER (pTeTnlInfo) != TE_TNL_OWNER_SNMP)
                ||
                ((TE_TNL_ROW_STATUS (pTeTnlInfo) != TE_NOTREADY) &&
                 (TE_TNL_ROW_STATUS (pTeTnlInfo) != TE_NOTINSERVICE)))
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            if (((TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_RSVP) ||
                 (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_LDP)) &&
                (TE_TNL_ROLE (pTeTnlInfo) != TE_INGRESS))
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            if (((TE_TNL_SSN_ATTR (pTeTnlInfo) & TE_SSN_FAST_REROUTE_BIT)
                 == TE_ZERO) && (TE_TNL_FRR_CONST_INFO (pTeTnlInfo) != NULL))
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            u1TempTnlRowStatus = TE_TNL_ROW_STATUS (pTeTnlInfo);
            u4TnlHopTableIndex = TE_TNL_HOP_TABLE_INDEX (pTeTnlInfo);

            /*Backup Path-Option Number For One-to-One Protected Tunnel */
            u4TnlBackupHopTableIndex =
                TE_TNL_BACKUPHOP_TABLE_INDEX (pTeTnlInfo);

            if (((TE_TNL_SSN_ATTR (pTeTnlInfo) & TE_SSN_FAST_REROUTE_BIT)
                 == TE_SSN_FAST_REROUTE_BIT) && (u4TnlHopTableIndex == TE_ZERO))
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            if ((TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_LDP) &&
                (u4TnlHopTableIndex == TE_ZERO))
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            if ((pTeTnlInfo->u4TnlMode == TE_TNL_MODE_ASSOCIATED_BIDIRECTIONAL)
                && (pTeTnlInfo->u4DestTnlInstance == 0))
            {
                pTeTnlInfo->u4DestTnlInstance = u4MplsTunnelInstance;
            }
            if (u4TnlHopTableIndex != TE_ZERO)
            {
                u4Flag = TE_ZERO;

                if (TE_FAILURE ==
                    TeSetEROPathStructure (u4TnlHopTableIndex, u4Flag,
                                           pTeTnlInfo))
                {
                    MPLS_CMN_UNLOCK ();
                    CLI_SET_ERR (MPLS_CLI_TE_ERR_PATH_OPT);
                    return SNMP_FAILURE;
                }
            }
            else
            {
                TE_TNL_PATH_LIST_INFO (pTeTnlInfo) = NULL;

                /*If no ERO is specified then make structure null */
                TE_TNL_PATH_INFO (pTeTnlInfo) = NULL;
            }

            /*Backup One-to-One Protection */
            if ((u4TnlBackupHopTableIndex != TE_ZERO)
                && (pInstance0Tnl != NULL))
            {
                u4Flag = TE_ONE;
                if (TE_FAILURE ==
                    TeSetEROPathStructure (u4TnlBackupHopTableIndex, u4Flag,
                                           pTeTnlInfo))
                {
                    MPLS_CMN_UNLOCK ();
                    CLI_SET_ERR (MPLS_CLI_TE_ERR_PATH_OPT);
                    return SNMP_FAILURE;
                }

            }
            else
            {
                /*If Backup ERO is NULL then make structure NULL */
                TE_TNL_BACKUPPATH_INFO (pTeTnlInfo) = NULL;
            }
            /*Checking whether resource index is 0 or 1 */
            if ((TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_LDP) &&
                ((TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo) == TE_ZERO) ||
                 (TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo) == TE_ONE)))
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            if (u4MplsTunnelInstance != TE_ZERO)
            {
                if (pInstance0Tnl != NULL)
                {
                    pTeTnlInfo->u4ResourceIndex
                        = pInstance0Tnl->u4ResourceIndex;
                    pTeTnlInfo->u1TnlSetPrio = pInstance0Tnl->u1TnlSetPrio;
                    pTeTnlInfo->u1TnlHoldPrio = pInstance0Tnl->u1TnlHoldPrio;
                    pTeTnlInfo->pTeAttrListInfo
                        = pInstance0Tnl->pTeAttrListInfo;
                    pTeTnlInfo->pTeTrfcParams = pInstance0Tnl->pTeTrfcParams;

                    pTeTnlInfo->u4DestTnlIndex = pInstance0Tnl->u4DestTnlIndex;
                    pTeTnlInfo->u4DestTnlInstance
                        = pInstance0Tnl->u4DestTnlInstance;

                    if (pTeTnlInfo->u1TnlPathType == TE_TNL_WORKING_PATH)
                    {
                        pTeTnlInfo->u4TnlIfIndex = pInstance0Tnl->u4TnlIfIndex;
                        pInstance0Tnl->u4TnlPrimaryInstance =
                            u4MplsTunnelInstance;
                    }
                    else if (pTeTnlInfo->u1TnlPathType ==
                             TE_TNL_PROTECTION_PATH)
                    {
                        pInstance0Tnl->u4BkpTnlInstance = u4MplsTunnelInstance;
                    }

                }
            }

            /* Finding the Resource table with the Index and 
             * assigning the Corresponding Traffic params to 
             * the Tunnel structure */

            if ((TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo)) != TE_ZERO)
            {
                if ((TE_TRFC_PARAMS_INFO (gTeGblInfo) != NULL) &&
                    (TE_TRFC_PARAMS_TNL_RESINDEX
                     ((TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo))) ==
                     (TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo))) &&
                    (TE_TRFC_PARAMS_TNL_RESROWSTATUS
                     ((TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo))) == TE_ACTIVE))
                {
                    if ((TE_TNL_SIGPROTO (pTeTnlInfo)
                         == TE_SIGPROTO_RSVP)
                        && (TE_RSVPTE_TRFC_PARAMS (TE_TRFC_PARAMS_PTR
                                                   (TE_TNL_TRFC_PARAM_INDEX
                                                    (pTeTnlInfo))) != NULL))
                    {
                        if (TE_TRFC_PARAMS_PTR
                            (TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo)) != NULL)
                        {
                            pTeTnlInfo->u4CurrentResvBw =
                                TE_RSVPTE_TPARAM_PDR (TE_TRFC_PARAMS_PTR
                                                      ((TE_TNL_TRFC_PARAM_INDEX
                                                        (pTeTnlInfo))));
                            TE_TNL_TRFC_PARAM (pTeTnlInfo) =
                                TE_TRFC_PARAMS_PTR ((TE_TNL_TRFC_PARAM_INDEX
                                                     (pTeTnlInfo)));
                            if (TE_TNL_TRFC_PARAM (pTeTnlInfo) != NULL)
                            {
                                TE_TNLRSRC_NUM_OF_TUNNELS ((TE_TNL_TRFC_PARAM
                                                            (pTeTnlInfo)))++;
                            }
                        }
                        else
                        {
                            TE_PO_NUM_TUNNELS (pTeTnlInfo->pTePathInfo)--;
                            MPLS_CMN_UNLOCK ();
                            return SNMP_FAILURE;
                        }
                    }
                    else if ((TE_TNL_SIGPROTO (pTeTnlInfo)
                              == TE_SIGPROTO_LDP) &&
                             (TE_CRLDP_TRFC_PARAMS
                              (TE_TRFC_PARAMS_PTR
                               (TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo))) != NULL))
                    {
                        if (TE_CRLDP_TPARAM_ROW_STATUS
                            (TE_TRFC_PARAMS_PTR
                             (TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo)))
                            == TE_ACTIVE)
                        {
                            TE_TNL_TRFC_PARAM (pTeTnlInfo) =
                                TE_TRFC_PARAMS_PTR ((TE_TNL_TRFC_PARAM_INDEX
                                                     (pTeTnlInfo)));
                            TE_TNLRSRC_NUM_OF_TUNNELS ((TE_TNL_TRFC_PARAM
                                                        (pTeTnlInfo)))++;
                        }
                        else
                        {
                            TE_PO_NUM_TUNNELS (pTeTnlInfo->pTePathInfo)--;
                            MPLS_CMN_UNLOCK ();
                            return SNMP_FAILURE;
                        }
                    }
                    else
                    {
                        /* Tunnel Should support Static Resouce Also */
                        TE_TNL_TRFC_PARAM (pTeTnlInfo) =
                            TE_TRFC_PARAMS_PTR ((TE_TNL_TRFC_PARAM_INDEX
                                                 (pTeTnlInfo)));
                        TE_TNLRSRC_NUM_OF_TUNNELS ((TE_TNL_TRFC_PARAM
                                                    (pTeTnlInfo))) = 0;
                    }
                }
                else
                {
                    if (pTeTnlInfo->pTePathInfo != NULL)
                    {
                        TE_PO_NUM_TUNNELS (pTeTnlInfo->pTePathInfo)--;
                    }
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
            }
            else
            {
                TE_TNL_TRFC_PARAM (pTeTnlInfo) =
                    TE_TRFC_PARAMS_PTR (TE_DFLT_TRFC_PRAM_INDEX);
                TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo) = TE_DFLT_TRFC_PRAM_INDEX;
                TE_TNLRSRC_NUM_OF_TUNNELS ((TE_TNL_TRFC_PARAM (pTeTnlInfo)))++;
            }

            if ((TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_NOTINSERVICE) &&
                (u4MplsTunnelInstance != TE_ZERO))
            {
                if (TE_TNL_SIGPROTO (pTeTnlInfo) != TE_SIGPROTO_NONE)
                {
                    TE_TNL_PRIMARY_TNL_INSTANCE (pTeTnlInfo) =
                        (TE_TNL_PRIMARY_TNL_INSTANCE (pTeTnlInfo) +
                         1) % TE_TNLINST_MAXVAL;

                    TePrcsL2vpnAssociation (pTeTnlInfo, TE_TNL_REESTB);
                }
            }
            else if (u4MplsTunnelInstance != TE_ZERO)
            {
                TE_TNL_PRIMARY_TNL_INSTANCE (pTeTnlInfo) =
                    TE_TNL_TNL_INSTANCE (pTeTnlInfo);
            }

            /* call functions from CRLDP or RSVPTE  */
            if (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_LDP)
            {
                TE_TNL_ROW_STATUS (pTeTnlInfo) = TE_ACTIVE;
                /* CRLDP Function Call */
                if (TeLdpTEEventHandler (TE_TNL_UP,
                                         pTeTnlInfo) == TE_LDP_FAILURE)
                {
                    TE_PO_NUM_TUNNELS (pTeTnlInfo->pTePathInfo)--;
                    (pTeTnlInfo->pTeTrfcParams->u2NumOfTunnels)--;
                    TE_TNL_ROW_STATUS (pTeTnlInfo) = u1TempTnlRowStatus;
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
            }
            else if (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_RSVP)
            {
                TE_TNL_ROW_STATUS (pTeTnlInfo) = TE_ACTIVE;

                TE_TNL_FRR_FAC_TNL_STATUS (pTeTnlInfo)
                    = TE_TNL_FRR_PROT_STATUS_PARTIAL;
                TE_TNL_FRR_DETOUR_ACTIVE (pTeTnlInfo) = TE_FALSE;
                TE_TNL_FRR_DETOUR_MERGING (pTeTnlInfo)
                    = TE_TNL_FRR_DETOUR_MERGE_NONE;

                if (((TE_TNL_SSN_ATTR (pTeTnlInfo) & TE_SSN_FAST_REROUTE_BIT)
                     == TE_SSN_FAST_REROUTE_BIT))
                {
                    if (TE_TNL_FRR_CONST_INFO (pTeTnlInfo) != NULL)
                    {
                        TE_TNL_FRR_PROT_METHOD (pTeTnlInfo) =
                            TE_TNL_FRR_CONST_PROT_METHOD (TE_TNL_FRR_CONST_INFO
                                                          (pTeTnlInfo));
                    }
                    else
                    {
                        TE_TNL_FRR_PROT_METHOD (pTeTnlInfo)
                            = TE_TNL_FRR_ONE2ONE_METHOD;
                    }
                }

                if (u4MplsTunnelInstance != TE_ZERO)
                {
                    TE_TNL_SIG_STATE (pTeTnlInfo) = TE_SIG_AWAITED;
                }

                /* Set MBB as required when Attributes are modified */
                if (u1TempTnlRowStatus == NOT_IN_SERVICE)
                {
                    if ((pTeTnlInfo->pTeAttrListInfo != NULL) &&
                        (pTeTnlInfo->pTeAttrListInfo->bIsMbbRequired == TRUE))
                    {
                        pTeTnlInfo->bIsMbbRequired = TRUE;
                        pTeTnlInfo->pTeAttrListInfo->bIsMbbRequired = FALSE;
                    }

                    if ((pTeTnlInfo->pTeTrfcParams != NULL) &&
                        (pTeTnlInfo->pTeTrfcParams->bIsMbbRequired == TRUE))
                    {
                        pTeTnlInfo->bIsMbbRequired = TRUE;
                        pTeTnlInfo->pTeTrfcParams->bIsMbbRequired = FALSE;
                    }

                    if (bIsPathChanged == TE_TRUE)
                    {
                        pTeTnlInfo->bIsMbbRequired = TRUE;
                    }
                }
                else
                {
                    pTeTnlInfo->bIsMbbRequired = FALSE;
                }

                if (pTeTnlInfo->u1TnlMBBStatus == MPLS_TE_MBB_DISABLED)
                {
                    pTeTnlInfo->bIsMbbRequired = FALSE;
                }

                /* Send TUNNEL UP EVENT to RSVP-TE to establish/re-establish 
                 * the tunnel only when it is administratively up.*/
                if (TE_TNL_ADMIN_STATUS (pTeTnlInfo) == TE_ADMIN_UP)
                {
                    /* Tunnel Up event should not be sent when MBB 
                     * is not required */

                    if ((u1TempTnlRowStatus == NOT_IN_SERVICE) &&
                        (pTeTnlInfo->u1TnlMBBStatus == MPLS_TE_MBB_ENABLED) &&
                        ((pTeTnlInfo->bIsMbbRequired != TRUE) ||
                         (pTeTnlInfo->u1TnlOperStatus != TE_OPER_UP)))
                    {
                        pTeTnlInfo->bIsMbbRequired = FALSE;
                        MPLS_CMN_UNLOCK ();
                        return SNMP_SUCCESS;
                    }

                    /* RSVP Function Call */
                    if (TeRpteTEEventHandler (TE_TNL_UP,
                                              pTeTnlInfo, &u1IsRpteAdminDown)
                        == TE_RPTE_FAILURE)
                    {
                        if (pTeTnlInfo->pTePathInfo != NULL)
                        {
                            TE_PO_NUM_TUNNELS (pTeTnlInfo->pTePathInfo)--;
                        }
                        if (pTeTnlInfo->pTeTrfcParams != NULL)
                        {
                            (pTeTnlInfo->pTeTrfcParams->u2NumOfTunnels)--;
                        }
                        TE_TNL_ROW_STATUS (pTeTnlInfo) = u1TempTnlRowStatus;
                        MPLS_CMN_UNLOCK ();
                        return SNMP_FAILURE;
                    }

                    if ((pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
                         MPLS_TE_DEDICATED_ONE2ONE) &&
                        (pTeTnlInfo->pTeBackupPathInfo != NULL))
                    {
                        TE_DBG2 (TE_MAIN_SNMP, "Triggering backup-path for "
                                 "tnl: %u %u\n", pTeTnlInfo->u4TnlIndex,
                                 pTeTnlInfo->u4TnlInstance);
                        pTeTnlInfo->u1TnlPathType = TE_TNL_PROTECTION_PATH;
                        if (TeRpteTEEventHandler (TE_TNL_UP,
                                                  pTeTnlInfo,
                                                  &u1IsRpteAdminDown) ==
                            TE_RPTE_FAILURE)
                        {
                            if (pTeTnlInfo->pTePathInfo != NULL)
                            {
                                TE_PO_NUM_TUNNELS (pTeTnlInfo->pTePathInfo)--;
                            }
                            if (pTeTnlInfo->pTeTrfcParams != NULL)
                            {
                                (pTeTnlInfo->pTeTrfcParams->u2NumOfTunnels)--;
                            }
                            TE_TNL_ROW_STATUS (pTeTnlInfo) = u1TempTnlRowStatus;
                            MPLS_CMN_UNLOCK ();
                            return SNMP_FAILURE;
                        }
                        pTeTnlInfo->u1TnlPathType = TE_TNL_WORKING_PATH;
                    }
                }
            }
            else if (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_NONE)
            {
                u4TnlXcIndex = pTeTnlInfo->u4TnlXcIndex;
                pTeTnlInfo->u4TnlXcIndex = 0;

                MplsDbUpdateTunnelXCIndex (pTeTnlInfo, NULL, u4TnlXcIndex);

                TE_TNL_ROW_STATUS (pTeTnlInfo) = TE_ACTIVE;

                if ((TE_TNL_TYPE_P2MP & TE_TNL_TYPE (pTeTnlInfo))
                    && (TE_EGRESS == TE_TNL_ROLE (pTeTnlInfo)))
                {
                    TE_P2MP_CONFIGURED_TNLS (gTeGblInfo) += TE_ONE;
                }

                if (TE_TNL_ADMIN_STATUS (pTeTnlInfo) == TE_ADMIN_UP)
                {
                    if (TeCmnExtSetOperStatusAndProgHw (pTeTnlInfo, TE_OPER_UP)
                        == TE_FAILURE)
                    {
                        MPLS_CMN_UNLOCK ();
                        return SNMP_FAILURE;
                    }
                }
            }
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;

        case TE_CREATEANDWAIT:
            if (pTeTnlInfo != NULL)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            pTeTnlInfo = (tTeTnlInfo *) TE_ALLOC_MEM_BLOCK (TE_TNL_POOL_ID);

            if (pTeTnlInfo == NULL)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            INIT_TE_TNL_INFO (pTeTnlInfo);
            INIT_TE_TNL_WITH_DEF_VALS (pTeTnlInfo);

            TE_TNL_TNL_INDEX (pTeTnlInfo) = (UINT4) u4MplsTunnelIndex;
            TE_TNL_TNL_INSTANCE (pTeTnlInfo) = u4MplsTunnelInstance;
            u4IngressId = TE_HTONL (u4MplsTunnelIngressLSRId);
            MEMCPY ((UINT1 *) &(TE_TNL_INGRESS_LSRID (pTeTnlInfo)),
                    (UINT1 *) &(u4IngressId), sizeof (UINT4));
            u4EgressId = TE_HTONL (u4MplsTunnelEgressLSRId);
            MEMCPY ((UINT1 *) &(TE_TNL_EGRESS_LSRID (pTeTnlInfo)),
                    (UINT1 *) &(u4EgressId), sizeof (UINT4));
            /* We are not suporting backup tunnels so, primary
             * tunnel instance is equal to zero */
            TE_TNL_PRIMARY_TNL_INSTANCE (pTeTnlInfo) = u4MplsTunnelInstance;

            /* For instance zero tunnel entries, keep the sig state as DONE
             * always. */
            if (u4MplsTunnelInstance == TE_ZERO)
            {
                TE_TNL_SIG_STATE (pTeTnlInfo) = TE_SIG_DONE;
            }

            TE_TNL_OWNER (pTeTnlInfo) = TE_TNL_OWNER_SNMP;
            TE_DLL_INIT (&TE_TNL_FTN_LIST (pTeTnlInfo));
            TMO_SLL_Init (&pTeTnlInfo->SrlgList);
            OsixGetSysTime (&(TE_TNL_CREATE_TIME (pTeTnlInfo)));
            TE_TNL_ROW_STATUS (pTeTnlInfo) = TE_NOTREADY;
            pTeTnlInfo->u1CPOrMgmtOperStatus = TE_OPER_DOWN;

            TeAddNodeToTnlTable (pTeTnlInfo);
            /* Increment the global tunnel count variable against the memory 
             * allocation from Tunnel pool. */
            if ((u4MplsTunnelInstance == TE_ZERO) || (pInstance0Tnl == NULL))
            {
                gu4TeRpteTnlCount++;
            }

            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;

        case TE_NOTINSERVICE:
            if (pTeTnlInfo == NULL)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            if (TE_TNL_OWNER (pTeTnlInfo) != TE_TNL_OWNER_SNMP)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            if ((TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_NOTINSERVICE) ||
                (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_NOTREADY))
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }

            TE_TNL_ROW_STATUS (pTeTnlInfo) = TE_NOTINSERVICE;

            /* call functions from CRLDP or RsvpTe  */
            if (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_LDP)
            {
                /* CRLDP Function Call */
                if (TeLdpTEEventHandler
                    (TE_TNL_DOWN, pTeTnlInfo) == TE_LDP_SUCCESS)
                {
                    if ((TE_TNL_TRFC_PARAM (pTeTnlInfo) != NULL) &&
                        (TE_TNLRSRC_NUM_OF_TUNNELS
                         (TE_TNL_TRFC_PARAM (pTeTnlInfo)) != TE_ZERO))
                    {
                        TE_TNLRSRC_NUM_OF_TUNNELS (TE_TNL_TRFC_PARAM
                                                   (pTeTnlInfo))--;
                    }
                    if ((TE_TNL_PATH_INFO (pTeTnlInfo) != NULL) &&
                        (TE_PO_NUM_TUNNELS (TE_TNL_PATH_INFO (pTeTnlInfo))
                         != TE_ZERO))
                    {
                        TE_PO_NUM_TUNNELS (TE_TNL_PATH_INFO (pTeTnlInfo))--;
                    }

                    MPLS_CMN_UNLOCK ();
                    return SNMP_SUCCESS;
                }
            }
            else if (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_RSVP)
            {
                if (TE_TNL_ADMIN_STATUS (pTeTnlInfo) == TE_ADMIN_UP)
                {
                    /* RSVP Function Call */
                    if ((TeRpteTEEventHandler
                         (TE_TNL_DOWN, pTeTnlInfo,
                          &u1IsRpteAdminDown)) == TE_RPTE_FAILURE)
                    {
                        MPLS_CMN_UNLOCK ();
                        return SNMP_FAILURE;
                    }
                }

                if ((TE_TNL_TRFC_PARAM (pTeTnlInfo) != NULL) &&
                    (TE_TNLRSRC_NUM_OF_TUNNELS
                     (TE_TNL_TRFC_PARAM (pTeTnlInfo)) != TE_ZERO))
                {
                    TE_TNLRSRC_NUM_OF_TUNNELS (TE_TNL_TRFC_PARAM
                                               (pTeTnlInfo))--;
                }
                if ((TE_TNL_PATH_INFO (pTeTnlInfo) != NULL) &&
                    (TE_PO_NUM_TUNNELS (TE_TNL_PATH_INFO (pTeTnlInfo))
                     != TE_ZERO))
                {
                    TE_PO_NUM_TUNNELS (TE_TNL_PATH_INFO (pTeTnlInfo))--;
                }

                if ((TE_TNL_BACKUPPATH_INFO (pTeTnlInfo) != NULL) &&
                    (TE_PO_NUM_TUNNELS (TE_TNL_BACKUPPATH_INFO (pTeTnlInfo))
                     != TE_ZERO))
                {
                    TE_PO_NUM_TUNNELS (TE_TNL_BACKUPPATH_INFO (pTeTnlInfo))--;
                }

                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
            else if (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_NONE)
            {
                if (TE_TNL_ADMIN_STATUS (pTeTnlInfo) == TE_ADMIN_UP)
                {
                    if (TeCmnExtSetOperStatusAndProgHw (pTeTnlInfo,
                                                        TE_OPER_DOWN)
                        == TE_FAILURE)
                    {
                        MPLS_CMN_UNLOCK ();
                        return SNMP_FAILURE;
                    }
                }

                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
            break;

        case TE_DESTROY:
            if (pTeTnlInfo == NULL)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            if (TE_TNL_OWNER (pTeTnlInfo) != TE_TNL_OWNER_SNMP)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            /* MPLS_P2MP_LSP_CHANGES - S */
            if (TE_P2MP_TNL_INFO (pTeTnlInfo) != NULL)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            /* MPLS_P2MP_LSP_CHANGES - E */

            if (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_LDP)
            {
                pTeTnlInfo->u1TnlRelStatus = TE_SIGMOD_TNLREL_AWAITED;
                /* CRLDP Function Call */
                u1RetVal = TeLdpTEEventHandler (TE_TNL_DESTROY, pTeTnlInfo);
                if (u1RetVal == TE_LDP_SUCCESS)
                {
                    /* let the crldp delete the TE tunnel after deleting
                     * corresponding crlsp and did neccessory actions.
                     */

                    if ((TE_TNL_TRFC_PARAM (pTeTnlInfo) != NULL) &&
                        (TE_TNLRSRC_NUM_OF_TUNNELS
                         (TE_TNL_TRFC_PARAM (pTeTnlInfo)) != TE_ZERO))
                    {
                        TE_TNLRSRC_NUM_OF_TUNNELS (TE_TNL_TRFC_PARAM
                                                   (pTeTnlInfo))--;
                    }
                    if ((TE_TNL_PATH_INFO (pTeTnlInfo) != NULL) &&
                        (TE_PO_NUM_TUNNELS (TE_TNL_PATH_INFO (pTeTnlInfo))
                         != TE_ZERO))
                    {
                        TE_PO_NUM_TUNNELS (TE_TNL_PATH_INFO (pTeTnlInfo))--;
                    }
                    MPLS_CMN_UNLOCK ();
                    return SNMP_SUCCESS;
                }
            }
            else if (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_RSVP)
            {
                pTeTnlInfo->u1TnlRelStatus = TE_SIGMOD_TNLREL_AWAITED;
                pTeTnlInfo->bIsMbbRequired = FALSE;
                TE_TNL_REOPTIMIZE_TRIGGER (pTeTnlInfo) = FALSE;

                u1RetVal = TE_RPTE_SUCCESS;
                /* if Adminstatus flag is set as GMPLS_ADMIN_DELETE_IN_PROGRESS,
                 * tunnel will be deleted using graceful deletion mechanism
                 * */
                if (!(pTeTnlInfo->GmplsTnlInfo.u4AdminStatus &
                      GMPLS_ADMIN_DELETE_IN_PROGRESS))
                {
                    /* RSVP Function Call Event posting to Signaling module */
                    u1RetVal =
                        TeRpteTEEventHandler (TE_TNL_DESTROY, pTeTnlInfo,
                                              &u1IsRpteAdminDown);
                    u4Index = pTeTnlInfo->u4MappedArrayndx;
                    TE_TNL_INDEX_LIST (gTeGblInfo)[u4Index - 1] = TE_FALSE;
                    if (u1IsRpteAdminDown == TE_TRUE)
                    {
                        if (TeDeleteTnlInfo (pTeTnlInfo, TE_TNL_CALL_FROM_ADMIN)
                            == TE_FAILURE)
                        {
                            MPLS_CMN_UNLOCK ();
                            return SNMP_FAILURE;
                        }

                        MPLS_CMN_UNLOCK ();
                        return SNMP_SUCCESS;
                    }
                }

                if (u1RetVal == TE_RPTE_SUCCESS)
                {
                    /* let the RPTE Delete the Tunnel on Backoff Timers
                     * Expiry or immd if no backoff timers are running.
                     */
                    if ((TE_TNL_TRFC_PARAM (pTeTnlInfo) != NULL) &&
                        (TE_TNLRSRC_NUM_OF_TUNNELS
                         (TE_TNL_TRFC_PARAM (pTeTnlInfo)) != TE_ZERO))
                    {
                        TE_TNLRSRC_NUM_OF_TUNNELS (TE_TNL_TRFC_PARAM
                                                   (pTeTnlInfo))--;
                    }

                    if ((TE_TNL_PATH_INFO (pTeTnlInfo) != NULL) &&
                        (TE_PO_NUM_TUNNELS (TE_TNL_PATH_INFO (pTeTnlInfo))
                         != TE_ZERO))
                    {
                        TE_PO_NUM_TUNNELS (TE_TNL_PATH_INFO (pTeTnlInfo))--;
                    }

                    if ((TE_TNL_BACKUPPATH_INFO (pTeTnlInfo) != NULL) &&
                        (TE_PO_NUM_TUNNELS (TE_TNL_BACKUPPATH_INFO (pTeTnlInfo))
                         != TE_ZERO))
                    {
                        TE_PO_NUM_TUNNELS (TE_TNL_BACKUPPATH_INFO
                                           (pTeTnlInfo))--;
                    }

                    MPLS_CMN_UNLOCK ();
                    return SNMP_SUCCESS;
                }
            }
            else if (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_NONE)
            {
                if (TE_TNL_ADMIN_STATUS (pTeTnlInfo) == TE_ADMIN_UP)
                {
                    if (TeCmnExtSetOperStatusAndProgHw
                        (pTeTnlInfo, TE_OPER_DOWN) == TE_FAILURE)
                    {
                        MPLS_CMN_UNLOCK ();
                        return SNMP_FAILURE;
                    }
                }

                /* STATIC_HLSP */
                if ((TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_HLSP) ||
                    (TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_SLSP))
                {
                    TeDeInitHLSPParams (pTeTnlInfo);
                }

                /* Delete the Te Tunnel info, since there are no users */
                u4Index = pTeTnlInfo->u4MappedArrayndx;
                TE_TNL_INDEX_LIST (gTeGblInfo)[u4Index - 1] = TE_FALSE;
                if (TeDeleteTnlInfo (pTeTnlInfo, TE_TNL_CALL_FROM_ADMIN) ==
                    TE_FAILURE)
                {
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }

                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
            break;
        default:
            break;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelStorageType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValMplsTunnelStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelStorageType (UINT4 u4MplsTunnelIndex,
                             UINT4 u4MplsTunnelInstance,
                             UINT4 u4MplsTunnelIngressLSRId,
                             UINT4 u4MplsTunnelEgressLSRId,
                             INT4 i4SetValMplsTunnelStorageType)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        if (TE_TNL_ROW_STATUS (pTeTnlInfo) != TE_ACTIVE)
        {
            TE_TNL_STORAGE_TYPE (pTeTnlInfo) =
                (UINT1) i4SetValMplsTunnelStorageType;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2MplsTunnelTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MplsTunnelTable (UINT4 *pu4ErrorCode,
                         tSnmpIndexList * pSnmpIndexList,
                         tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMplsFrrConstProtectionMethod
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValFsMplsFrrConstProtectionMethod
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsFrrConstProtectionMethod (UINT4 u4MplsTunnelIndex,
                                      UINT4 u4MplsTunnelInstance,
                                      UINT4 u4MplsTunnelIngressLSRId,
                                      UINT4 u4MplsTunnelEgressLSRId,
                                      INT4
                                      i4SetValFsMplsFrrConstProtectionMethod)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);

    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeTnlInfo should not be active */
    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be NULL */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);
    if (pTeFrrConstInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be active */
    if (TE_TNL_FRR_CONST_ROW_STATUS (pTeFrrConstInfo) == TE_ACTIVE)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    TE_TNL_FRR_CONST_PROT_METHOD (pTeFrrConstInfo) =
        (UINT1) i4SetValFsMplsFrrConstProtectionMethod;

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsFrrConstProtectionType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValFsMplsFrrConstProtectionType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsFrrConstProtectionType (UINT4 u4MplsTunnelIndex,
                                    UINT4 u4MplsTunnelInstance,
                                    UINT4 u4MplsTunnelIngressLSRId,
                                    UINT4 u4MplsTunnelEgressLSRId,
                                    INT4 i4SetValFsMplsFrrConstProtectionType)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);

    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeTnlInfo should not be active */
    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be NULL */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);
    if (pTeFrrConstInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be active */
    if (TE_TNL_FRR_CONST_ROW_STATUS (pTeFrrConstInfo) == TE_ACTIVE)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    TE_TNL_FRR_CONST_PROT_TYPE (pTeFrrConstInfo) =
        (UINT1) i4SetValFsMplsFrrConstProtectionType;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsFrrConstSetupPrio
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValFsMplsFrrConstSetupPrio
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsFrrConstSetupPrio (UINT4 u4MplsTunnelIndex,
                               UINT4 u4MplsTunnelInstance,
                               UINT4 u4MplsTunnelIngressLSRId,
                               UINT4 u4MplsTunnelEgressLSRId,
                               UINT4 u4SetValFsMplsFrrConstSetupPrio)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);

    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeTnlInfo should not be active */
    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be NULL */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);
    if (pTeFrrConstInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be active */
    if (TE_TNL_FRR_CONST_ROW_STATUS (pTeFrrConstInfo) == TE_ACTIVE)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    TE_TNL_FRR_CONST_SETUP_PRIO (pTeFrrConstInfo) =
        (UINT1) u4SetValFsMplsFrrConstSetupPrio;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsFrrConstHoldingPrio
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValFsMplsFrrConstHoldingPrio
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsFrrConstHoldingPrio (UINT4 u4MplsTunnelIndex,
                                 UINT4 u4MplsTunnelInstance,
                                 UINT4 u4MplsTunnelIngressLSRId,
                                 UINT4 u4MplsTunnelEgressLSRId,
                                 UINT4 u4SetValFsMplsFrrConstHoldingPrio)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);

    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeTnlInfo should not be active */
    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be NULL */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);
    if (pTeFrrConstInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be active */
    if (TE_TNL_FRR_CONST_ROW_STATUS (pTeFrrConstInfo) == TE_ACTIVE)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    TE_TNL_FRR_CONST_HOLD_PRIO (pTeFrrConstInfo) =
        (UINT1) u4SetValFsMplsFrrConstHoldingPrio;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsFrrConstSEStyle
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValFsMplsFrrConstSEStyle
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsFrrConstSEStyle (UINT4 u4MplsTunnelIndex,
                             UINT4 u4MplsTunnelInstance,
                             UINT4 u4MplsTunnelIngressLSRId,
                             UINT4 u4MplsTunnelEgressLSRId,
                             INT4 i4SetValFsMplsFrrConstSEStyle)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);

    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeTnlInfo should not be active */
    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be NULL */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);
    if (pTeFrrConstInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be active */
    if (TE_TNL_FRR_CONST_ROW_STATUS (pTeFrrConstInfo) == TE_ACTIVE)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    TE_TNL_FRR_CONST_SE_STYLE (pTeFrrConstInfo)
        = (UINT1) i4SetValFsMplsFrrConstSEStyle;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsFrrConstInclAnyAffinity
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValFsMplsFrrConstInclAnyAffinity
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsFrrConstInclAnyAffinity (UINT4 u4MplsTunnelIndex,
                                     UINT4 u4MplsTunnelInstance,
                                     UINT4 u4MplsTunnelIngressLSRId,
                                     UINT4 u4MplsTunnelEgressLSRId,
                                     UINT4
                                     u4SetValFsMplsFrrConstInclAnyAffinity)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);

    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeTnlInfo should not be active */
    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be NULL */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);
    if (pTeFrrConstInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be active */
    if (TE_TNL_FRR_CONST_ROW_STATUS (pTeFrrConstInfo) == TE_ACTIVE)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    TE_TNL_FRR_CONST_INCANY_AFFINITY (pTeFrrConstInfo) =
        u4SetValFsMplsFrrConstInclAnyAffinity;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsFrrConstInclAllAffinity
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValFsMplsFrrConstInclAllAffinity
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsFrrConstInclAllAffinity (UINT4 u4MplsTunnelIndex,
                                     UINT4 u4MplsTunnelInstance,
                                     UINT4 u4MplsTunnelIngressLSRId,
                                     UINT4 u4MplsTunnelEgressLSRId,
                                     UINT4
                                     u4SetValFsMplsFrrConstInclAllAffinity)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);

    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeTnlInfo should not be active */
    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be NULL */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);
    if (pTeFrrConstInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be active */
    if (TE_TNL_FRR_CONST_ROW_STATUS (pTeFrrConstInfo) == TE_ACTIVE)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    TE_TNL_FRR_CONST_INCALL_AFFINITY (pTeFrrConstInfo) =
        u4SetValFsMplsFrrConstInclAllAffinity;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsFrrConstExclAnyAffinity
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValFsMplsFrrConstExclAnyAffinity
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsFrrConstExclAnyAffinity (UINT4 u4MplsTunnelIndex,
                                     UINT4 u4MplsTunnelInstance,
                                     UINT4 u4MplsTunnelIngressLSRId,
                                     UINT4 u4MplsTunnelEgressLSRId,
                                     UINT4
                                     u4SetValFsMplsFrrConstExclAnyAffinity)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);

    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeTnlInfo should not be active */
    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be NULL */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);
    if (pTeFrrConstInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be active */
    if (TE_TNL_FRR_CONST_ROW_STATUS (pTeFrrConstInfo) == TE_ACTIVE)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    TE_TNL_FRR_CONST_EXANY_AFFINITY (pTeFrrConstInfo) =
        u4SetValFsMplsFrrConstExclAnyAffinity;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsFrrConstHopLimit
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValFsMplsFrrConstHopLimit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsFrrConstHopLimit (UINT4 u4MplsTunnelIndex,
                              UINT4 u4MplsTunnelInstance,
                              UINT4 u4MplsTunnelIngressLSRId,
                              UINT4 u4MplsTunnelEgressLSRId,
                              UINT4 u4SetValFsMplsFrrConstHopLimit)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);

    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeTnlInfo should not be active */
    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be NULL */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);
    if (pTeFrrConstInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be active */
    if (TE_TNL_FRR_CONST_ROW_STATUS (pTeFrrConstInfo) == TE_ACTIVE)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    TE_TNL_FRR_CONST_HOP_LIMIT (pTeFrrConstInfo) =
        (UINT1) u4SetValFsMplsFrrConstHopLimit;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsFrrConstBandwidth
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValFsMplsFrrConstBandwidth
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsFrrConstBandwidth (UINT4 u4MplsTunnelIndex,
                               UINT4 u4MplsTunnelInstance,
                               UINT4 u4MplsTunnelIngressLSRId,
                               UINT4 u4MplsTunnelEgressLSRId,
                               UINT4 u4SetValFsMplsFrrConstBandwidth)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);

    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeTnlInfo should not be active */
    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be NULL */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);
    if (pTeFrrConstInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be active */
    if (TE_TNL_FRR_CONST_ROW_STATUS (pTeFrrConstInfo) == TE_ACTIVE)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    TE_TNL_FRR_CONST_BANDWIDTH (pTeFrrConstInfo) =
        u4SetValFsMplsFrrConstBandwidth;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsFrrConstRowStatus
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValFsMplsFrrConstRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsFrrConstRowStatus (UINT4 u4MplsTunnelIndex,
                               UINT4 u4MplsTunnelInstance,
                               UINT4 u4MplsTunnelIngressLSRId,
                               UINT4 u4MplsTunnelEgressLSRId,
                               INT4 i4SetValFsMplsFrrConstRowStatus)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Perform any Action on this Table, An Entry in Tunnel Table 
     * is required */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);

    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeTnlInfo should not be active */
    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);

    /* For Actions other than CREATE_AND_WAIT on this Table, 
     * FrrConstInfo Pointer should not be NULL */
    if ((i4SetValFsMplsFrrConstRowStatus != TE_CREATEANDWAIT) &&
        (pTeFrrConstInfo == NULL))
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    switch (i4SetValFsMplsFrrConstRowStatus)
    {
        case TE_ACTIVE:
        {
            if ((TE_TNL_FRR_CONST_PROT_METHOD (pTeFrrConstInfo) ==
                 TE_TNL_FRR_FACILITY_METHOD) &&
                (TE_TNL_FRR_CONST_SE_STYLE (pTeFrrConstInfo) != TE_SNMP_TRUE))
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            TE_TNL_FRR_CONST_ROW_STATUS (pTeFrrConstInfo) = TE_ACTIVE;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
        case TE_NOTINSERVICE:
        {
            TE_TNL_FRR_CONST_ROW_STATUS (pTeFrrConstInfo) = TE_NOTINSERVICE;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
        case TE_NOTREADY:
        {
            TE_TNL_FRR_CONST_ROW_STATUS (pTeFrrConstInfo) = TE_NOTREADY;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
        case TE_CREATEANDGO:
        {
            /* UnSupported Action */
            break;
        }
        case TE_CREATEANDWAIT:
        {
            /* For this Action, pTeFrrConstInfo should be NULL */
            if (pTeFrrConstInfo != NULL)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            /* Memory Allocation for pTeFrrConstInfo */
            pTeFrrConstInfo = (tTeFrrConstInfo *)
                TE_ALLOC_MEM_BLOCK (TE_TNL_FRR_CONST_INFO_POOL_ID);

            if (pTeFrrConstInfo == NULL)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            /* Initialising pTeFrrConstInfo */
            INIT_TE_TNL_FRR_CONST_INFO (pTeFrrConstInfo);
            INIT_TE_TNL_FRR_CONST_WITH_DEF_VALS (pTeFrrConstInfo);
            TE_TNL_FRR_CONST_ROW_STATUS (pTeFrrConstInfo) = TE_NOTREADY;
            TE_TNL_FRR_CONST_INFO (pTeTnlInfo) = pTeFrrConstInfo;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
        case TE_DESTROY:
        {
            /* Delete the Frr Constraint information associated with
             * this tunnel. */
            TeDeleteTnlFrrInfo (pTeTnlInfo, NULL);

            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }

        default:
        {
            /* UnSupported Action */
            break;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsFrrConstTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsFrrConstTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsTunnelExtMaxGblRevertTime
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object
                setValFsMplsTunnelExtMaxGblRevertTime
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsTunnelExtMaxGblRevertTime (UINT4 u4MplsTunnelIndex,
                                       UINT4 u4MplsTunnelInstance,
                                       UINT4 u4MplsTunnelIngressLSRId,
                                       UINT4 u4MplsTunnelEgressLSRId,
                                       INT4
                                       i4SetValFsMplsTunnelExtMaxGblRevertTime)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    /* Verifying for Extreme Values */
    if ((i4SetValFsMplsTunnelExtMaxGblRevertTime <
         TE_TNL_FRR_GBL_REVERT_MIN_TIME) ||
        (i4SetValFsMplsTunnelExtMaxGblRevertTime >
         TE_TNL_FRR_GBL_REVERT_MAX_TIME))
    {
        return SNMP_FAILURE;
    }

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);

    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeTnlInfo should not be active */
    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    TE_TNL_FRR_MAX_GBL_REVERT_TIME (pTeTnlInfo) =
        (UINT4) i4SetValFsMplsTunnelExtMaxGblRevertTime;

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsTunnelExtTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsTunnelExtTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/*                       End of file teset1.c                                */
/*---------------------------------------------------------------------------*/
