/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: teget1.c,v 1.25 2015/03/20 12:14:51 siva Exp $
 *
 * Description: This file contains the low level GET routines
 *              for the following TE MIB tables.
 *              - MplsTunnelTable
 *              - FsMplsFrrConstTable
 *              - FsMplsTunnelExtTable
 *******************************************************************/

#include "teincs.h"
#include "fsmplslw.h"
#include "stdtelw.h"
#include "mplslsr.h"
#include "fsmpfrlw.h"

/* LOW LEVEL Routines for Table : MplsTunnelTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsTunnelTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhValidateIndexInstanceMplsTunnelTable (UINT4 u4MplsTunnelIndex,
                                         UINT4 u4MplsTunnelInstance,
                                         UINT4 u4MplsTunnelIngressLSRId,
                                         UINT4 u4MplsTunnelEgressLSRId)
{
    /* Curently all the Tunnel index value, Tunnel instance value are
     * checked against a range. 
     */
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    UNUSED_PARAM (u4MplsTunnelInstance);
    /* Allowed tunnel indices 1 to 65535 as per MPLS-TC-STD-MIB [RFC 3811] */
    if ((u4MplsTunnelIndex < TE_TNLINDEX_MINVAL) ||
        (u4MplsTunnelIndex > TE_TNLINDEX_MAXVAL))
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* In RFC 3812, IngressLSRId and EgressLSRId types are modified
     * to MplsExtendedTunnelId from MplsLsrId so, It can take any value
     * from 0 to max of unsigned32. But In Our Implementation we are
     * using it as IpAddress of Ingress and Egress LSR */
    if (u4MplsTunnelIngressLSRId == TE_MIN_UINT4_VALUE)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (u4MplsTunnelEgressLSRId == TE_MIN_UINT4_VALUE)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsTunnelTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexMplsTunnelTable (UINT4 *pu4MplsTunnelIndex,
                                 UINT4 *pu4MplsTunnelInstance,
                                 UINT4 *pu4MplsTunnelIngressLSRId,
                                 UINT4 *pu4MplsTunnelEgressLSRId)
{
    UINT4               u4TmpTnlIngressLsrId = TE_ZERO;
    UINT4               u4TmpTnlEgressLsrId = TE_ZERO;
    tTeTnlInfo         *pFirstTeTnlInfo = NULL;

    *pu4MplsTunnelIndex = TE_ZERO;
    *pu4MplsTunnelInstance = TE_ZERO;
    *pu4MplsTunnelIngressLSRId = TE_ZERO;
    *pu4MplsTunnelEgressLSRId = TE_ZERO;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pFirstTeTnlInfo = (tTeTnlInfo *) RBTreeGetFirst (gTeGblInfo.TnlInfoTbl);

    if (pFirstTeTnlInfo != NULL)
    {
        *pu4MplsTunnelIndex = pFirstTeTnlInfo->u4TnlIndex;
        *pu4MplsTunnelInstance = pFirstTeTnlInfo->u4TnlInstance;
        CONVERT_TO_INTEGER ((pFirstTeTnlInfo->TnlIngressLsrId),
                            u4TmpTnlIngressLsrId);
        CONVERT_TO_INTEGER ((pFirstTeTnlInfo->TnlEgressLsrId),
                            u4TmpTnlEgressLsrId);
        u4TmpTnlEgressLsrId = OSIX_NTOHL (u4TmpTnlEgressLsrId);
        u4TmpTnlIngressLsrId = OSIX_NTOHL (u4TmpTnlIngressLsrId);

        *pu4MplsTunnelIngressLSRId = u4TmpTnlIngressLsrId;
        *pu4MplsTunnelEgressLSRId = u4TmpTnlEgressLsrId;
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsTunnelTable
 Input       :  The Indices
                MplsTunnelIndex
                nextMplsTunnelIndex
                MplsTunnelInstance
                nextMplsTunnelInstance
                MplsTunnelIngressLSRId
                nextMplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                nextMplsTunnelEgressLSRId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexMplsTunnelTable (UINT4 u4MplsTunnelIndex,
                                UINT4 *pu4NextMplsTunnelIndex,
                                UINT4 u4MplsTunnelInstance,
                                UINT4 *pu4NextMplsTunnelInstance,
                                UINT4 u4MplsTunnelIngressLSRId,
                                UINT4 *pu4NextMplsTunnelIngressLSRId,
                                UINT4 u4MplsTunnelEgressLSRId,
                                UINT4 *pu4NextMplsTunnelEgressLSRId)
{
    UINT4               u4TmpNextTnlIngressLsrId = TE_ZERO;
    UINT4               u4TmpNextTnlEgressLsrId = TE_ZERO;
    tTeTnlInfo          InputTeTnlInfo;
    tTeTnlInfo         *pNextTeTnlInfo = NULL;

    *pu4NextMplsTunnelIndex = TE_ZERO;
    *pu4NextMplsTunnelInstance = TE_ZERO;
    *pu4NextMplsTunnelIngressLSRId = TE_ZERO;
    *pu4NextMplsTunnelEgressLSRId = TE_ZERO;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    InputTeTnlInfo.u4TnlIndex = u4MplsTunnelIndex;
    InputTeTnlInfo.u4TnlInstance = u4MplsTunnelInstance;
    u4MplsTunnelIngressLSRId = OSIX_HTONL (u4MplsTunnelIngressLSRId);
    MEMCPY (InputTeTnlInfo.TnlIngressLsrId, &(u4MplsTunnelIngressLSRId),
            IPV4_ADDR_LENGTH);
    u4MplsTunnelEgressLSRId = OSIX_HTONL (u4MplsTunnelEgressLSRId);
    MEMCPY (InputTeTnlInfo.TnlEgressLsrId, &(u4MplsTunnelEgressLSRId),
            IPV4_ADDR_LENGTH);

    pNextTeTnlInfo = (tTeTnlInfo *) RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                                   (tRBElem *) & InputTeTnlInfo,
                                                   NULL);
    if (pNextTeTnlInfo != NULL)
    {
        *pu4NextMplsTunnelIndex = pNextTeTnlInfo->u4TnlIndex;
        *pu4NextMplsTunnelInstance = pNextTeTnlInfo->u4TnlInstance;
        CONVERT_TO_INTEGER ((pNextTeTnlInfo->TnlIngressLsrId),
                            u4TmpNextTnlIngressLsrId);
        CONVERT_TO_INTEGER ((pNextTeTnlInfo->TnlEgressLsrId),
                            u4TmpNextTnlEgressLsrId);
        u4TmpNextTnlIngressLsrId = OSIX_NTOHL (u4TmpNextTnlIngressLsrId);
        u4TmpNextTnlEgressLsrId = OSIX_NTOHL (u4TmpNextTnlEgressLsrId);

        *pu4NextMplsTunnelIngressLSRId = u4TmpNextTnlIngressLsrId;
        *pu4NextMplsTunnelEgressLSRId = u4TmpNextTnlEgressLsrId;

        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelIndexNext
 Input       :  The Indices

                The Object 
                retValMplsTunnelIndexNext
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelIndexNext (UINT4 *pu4RetValMplsTunnelIndexNext)
{
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4RetValMplsTunnelIndexNext = 0;
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }

    if (TeGetNextAvailableIndex ((INT4 *) pu4RetValMplsTunnelIndexNext) ==
        TE_FAILURE)
    {
        *pu4RetValMplsTunnelIndexNext = 0;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelName
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelName (UINT4 u4MplsTunnelIndex, UINT4 u4MplsTunnelInstance,
                      UINT4 u4MplsTunnelIngressLSRId,
                      UINT4 u4MplsTunnelEgressLSRId,
                      tSNMP_OCTET_STRING_TYPE * pRetValMplsTunnelName)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        CPY_TO_SNMP (pRetValMplsTunnelName, &TE_TNL_NAME (pTeTnlInfo),
                     (INT4)STRLEN ((INT1 *) TE_TNL_NAME (pTeTnlInfo)));
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelDescr
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelDescr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelDescr (UINT4 u4MplsTunnelIndex, UINT4 u4MplsTunnelInstance,
                       UINT4 u4MplsTunnelIngressLSRId,
                       UINT4 u4MplsTunnelEgressLSRId,
                       tSNMP_OCTET_STRING_TYPE * pRetValMplsTunnelDescr)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        CPY_TO_SNMP (pRetValMplsTunnelDescr, &TE_TNL_DESCR (pTeTnlInfo),
                     (INT4)STRLEN ((INT1 *) TE_TNL_DESCR (pTeTnlInfo)));
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelIsIf
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelIsIf
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelIsIf (UINT4 u4MplsTunnelIndex, UINT4 u4MplsTunnelInstance,
                      UINT4 u4MplsTunnelIngressLSRId,
                      UINT4 u4MplsTunnelEgressLSRId,
                      INT4 *pi4RetValMplsTunnelIsIf)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        *pi4RetValMplsTunnelIsIf = (TE_TNL_ISIF (pTeTnlInfo) == TE_TRUE
                                    ? TE_SNMP_TRUE : TE_SNMP_FALSE);

        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelIfIndex
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelIfIndex (UINT4 u4MplsTunnelIndex, UINT4 u4MplsTunnelInstance,
                         UINT4 u4MplsTunnelIngressLSRId,
                         UINT4 u4MplsTunnelEgressLSRId,
                         INT4 *pi4RetValMplsTunnelIfIndex)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        *pi4RetValMplsTunnelIfIndex = (INT4)TE_TNL_IFINDEX (pTeTnlInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelXCPointer
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelXCPointer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelXCPointer (UINT4 u4MplsTunnelIndex, UINT4 u4MplsTunnelInstance,
                           UINT4 u4MplsTunnelIngressLSRId,
                           UINT4 u4MplsTunnelEgressLSRId,
                           tSNMP_OID_TYPE * pRetValMplsTunnelXCPointer)
{
    UINT1              *pu1Index = NULL;
    UINT4               u4Temp = 0;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tXcEntry           *pXcEntry = NULL;
    UINT4               u4TnlXcIndex = 0;

    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        if (pTeTnlInfo->u4OrgTnlXcIndex != 0)
        {
            u4TnlXcIndex = pTeTnlInfo->u4OrgTnlXcIndex;
        }
        else
        {
            u4TnlXcIndex = pTeTnlInfo->u4TnlXcIndex;
        }
        if (u4TnlXcIndex == 0)
        {
            /*  0.0 -> No LSP has been associated */
            MEMSET (pRetValMplsTunnelXCPointer->pu4_OidList, 0,
                    TE_TNL_XC_TABLE_MAX_OFFSET * sizeof (UINT4));
            pRetValMplsTunnelXCPointer->u4_Length = 2;
        }
        else
        {
            if ((pXcEntry = MplsGetXCEntryByDirection
                 (u4TnlXcIndex, (eDirection) MPLS_DIRECTION_ANY)) == NULL)
            {
                /*  0.0 -> No LSP has been associated */
                MEMSET (pRetValMplsTunnelXCPointer->pu4_OidList, 0,
                        sizeof (UINT4) * 2);
                pRetValMplsTunnelXCPointer->u4_Length = 2;
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }

            pRetValMplsTunnelXCPointer->u4_Length = TE_TNL_XC_TABLE_DEF_OFFSET;
            MEMCPY (pRetValMplsTunnelXCPointer->pu4_OidList, au4TeTnlXCTableOid,
                    TE_TNL_XC_TABLE_DEF_OFFSET * sizeof (UINT4));

            u4Temp = OSIX_HTONL (pXcEntry->u4Index);
            pu1Index = (UINT1 *) &u4Temp;

            pRetValMplsTunnelXCPointer->
                pu4_OidList[pRetValMplsTunnelXCPointer->u4_Length++] =
                (UINT4) (*(pu1Index++));
            pRetValMplsTunnelXCPointer->pu4_OidList[pRetValMplsTunnelXCPointer->
                                                    u4_Length++] =
                (UINT4) (*(pu1Index++));
            pRetValMplsTunnelXCPointer->pu4_OidList[pRetValMplsTunnelXCPointer->
                                                    u4_Length++] =
                (UINT4) (*(pu1Index++));
            pRetValMplsTunnelXCPointer->pu4_OidList[pRetValMplsTunnelXCPointer->
                                                    u4_Length++] =
                (UINT4) (*(pu1Index++));

            if (pXcEntry->pInIndex != NULL)
            {
                pRetValMplsTunnelXCPointer->
                    pu4_OidList[pRetValMplsTunnelXCPointer->u4_Length++] =
                    sizeof (UINT4);

                u4Temp = OSIX_HTONL (pXcEntry->pInIndex->u4Index);
                pu1Index = (UINT1 *) &u4Temp;

                pRetValMplsTunnelXCPointer->
                    pu4_OidList[pRetValMplsTunnelXCPointer->u4_Length++] =
                    (UINT4) (*(pu1Index++));
                pRetValMplsTunnelXCPointer->
                    pu4_OidList[pRetValMplsTunnelXCPointer->u4_Length++] =
                    (UINT4) (*(pu1Index++));
                pRetValMplsTunnelXCPointer->
                    pu4_OidList[pRetValMplsTunnelXCPointer->u4_Length++] =
                    (UINT4) (*(pu1Index++));
                pRetValMplsTunnelXCPointer->
                    pu4_OidList[pRetValMplsTunnelXCPointer->u4_Length++] =
                    (UINT4) (*(pu1Index++));
            }
            else
            {
                pRetValMplsTunnelXCPointer->
                    pu4_OidList[pRetValMplsTunnelXCPointer->u4_Length++] =
                    sizeof (UINT1);

                pRetValMplsTunnelXCPointer->
                    pu4_OidList[pRetValMplsTunnelXCPointer->u4_Length++] = 0;
            }

            if (pXcEntry->pOutIndex != NULL)
            {
                pRetValMplsTunnelXCPointer->
                    pu4_OidList[pRetValMplsTunnelXCPointer->u4_Length++] =
                    sizeof (UINT4);

                u4Temp = OSIX_HTONL (pXcEntry->pOutIndex->u4Index);
                pu1Index = (UINT1 *) &u4Temp;

                pRetValMplsTunnelXCPointer->
                    pu4_OidList[pRetValMplsTunnelXCPointer->u4_Length++] =
                    (UINT4) (*(pu1Index++));
                pRetValMplsTunnelXCPointer->
                    pu4_OidList[pRetValMplsTunnelXCPointer->u4_Length++] =
                    (UINT4) (*(pu1Index++));
                pRetValMplsTunnelXCPointer->
                    pu4_OidList[pRetValMplsTunnelXCPointer->u4_Length++] =
                    (UINT4) (*(pu1Index++));
                pRetValMplsTunnelXCPointer->
                    pu4_OidList[pRetValMplsTunnelXCPointer->u4_Length++] =
                    (UINT4) (*(pu1Index));
            }
            else
            {
                pRetValMplsTunnelXCPointer->
                    pu4_OidList[pRetValMplsTunnelXCPointer->u4_Length++] =
                    sizeof (UINT1);

                pRetValMplsTunnelXCPointer->
                    pu4_OidList[pRetValMplsTunnelXCPointer->u4_Length++] = 0;
            }
        }

        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelSignallingProto
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelSignallingProto
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelSignallingProto (UINT4 u4MplsTunnelIndex,
                                 UINT4 u4MplsTunnelInstance,
                                 UINT4 u4MplsTunnelIngressLSRId,
                                 UINT4 u4MplsTunnelEgressLSRId,
                                 INT4 *pi4RetValMplsTunnelSignallingProto)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        *pi4RetValMplsTunnelSignallingProto = TE_TNL_SIGPROTO (pTeTnlInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetMplsTunnelSetupPrio
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelSetupPrio
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelSetupPrio (UINT4 u4MplsTunnelIndex, UINT4 u4MplsTunnelInstance,
                           UINT4 u4MplsTunnelIngressLSRId,
                           UINT4 u4MplsTunnelEgressLSRId,
                           INT4 *pi4RetValMplsTunnelSetupPrio)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        *pi4RetValMplsTunnelSetupPrio = TE_TNL_SETUP_PRIO (pTeTnlInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelHoldingPrio
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelHoldingPrio
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelHoldingPrio (UINT4 u4MplsTunnelIndex,
                             UINT4 u4MplsTunnelInstance,
                             UINT4 u4MplsTunnelIngressLSRId,
                             UINT4 u4MplsTunnelEgressLSRId,
                             INT4 *pi4RetValMplsTunnelHoldingPrio)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        *pi4RetValMplsTunnelHoldingPrio = TE_TNL_HLDNG_PRIO (pTeTnlInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelSessionAttributes
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelSessionAttributes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelSessionAttributes (UINT4 u4MplsTunnelIndex,
                                   UINT4 u4MplsTunnelInstance,
                                   UINT4 u4MplsTunnelIngressLSRId,
                                   UINT4 u4MplsTunnelEgressLSRId,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValMplsTunnelSessionAttributes)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        pRetValMplsTunnelSessionAttributes->pu1_OctetList[0] =
            TE_TNL_SSN_ATTR (pTeTnlInfo);
        pRetValMplsTunnelSessionAttributes->i4_Length = sizeof (UINT1);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelOwner
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelOwner
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelOwner (UINT4 u4MplsTunnelIndex, UINT4 u4MplsTunnelInstance,
                       UINT4 u4MplsTunnelIngressLSRId,
                       UINT4 u4MplsTunnelEgressLSRId,
                       INT4 *pi4RetValMplsTunnelOwner)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        *pi4RetValMplsTunnelOwner = TE_TNL_OWNER (pTeTnlInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelLocalProtectInUse
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelLocalProtectInUse
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelLocalProtectInUse (UINT4 u4MplsTunnelIndex,
                                   UINT4 u4MplsTunnelInstance,
                                   UINT4 u4MplsTunnelIngressLSRId,
                                   UINT4 u4MplsTunnelEgressLSRId,
                                   INT4 *pi4RetValMplsTunnelLocalProtectInUse)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        *pi4RetValMplsTunnelLocalProtectInUse
            = (TE_TNL_LOCAL_PROTECTION_IN_USE (pTeTnlInfo) ==
               LOCAL_PROT_IN_USE ? TE_SNMP_TRUE : TE_SNMP_FALSE);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelResourcePointer
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelResourcePointer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelResourcePointer (UINT4 u4MplsTunnelIndex,
                                 UINT4 u4MplsTunnelInstance,
                                 UINT4 u4MplsTunnelIngressLSRId,
                                 UINT4 u4MplsTunnelEgressLSRId,
                                 tSNMP_OID_TYPE *
                                 pRetValMplsTunnelResourcePointer)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {

        if (TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo) == 0)
        {
            /* Best Effort 0.0 */
            MEMSET (pRetValMplsTunnelResourcePointer->pu4_OidList, 0,
                    sizeof (UINT4) * 2);
            pRetValMplsTunnelResourcePointer->u4_Length = 2;
        }
        else
        {
            pRetValMplsTunnelResourcePointer->u4_Length =
                TE_TNL_RSRC_TABLE_DEF_OFFSET;
            MEMCPY (pRetValMplsTunnelResourcePointer->pu4_OidList,
                    au4TeTnlResourceTableOid,
                    TE_TNL_RSRC_TABLE_DEF_OFFSET * sizeof (UINT4));
            pRetValMplsTunnelResourcePointer->
                pu4_OidList[TE_TNL_RSRC_TABLE_DEF_OFFSET - 1] =
                TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo);
        }
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelInstancePriority
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelInstancePriority
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelInstancePriority (UINT4 u4MplsTunnelIndex,
                                  UINT4 u4MplsTunnelInstance,
                                  UINT4 u4MplsTunnelIngressLSRId,
                                  UINT4 u4MplsTunnelEgressLSRId,
                                  UINT4 *pu4RetValMplsTunnelInstancePriority)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        *pu4RetValMplsTunnelInstancePriority = TE_TNL_INST_PRIO (pTeTnlInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelHopTableIndex
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelHopTableIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelHopTableIndex (UINT4 u4MplsTunnelIndex,
                               UINT4 u4MplsTunnelInstance,
                               UINT4 u4MplsTunnelIngressLSRId,
                               UINT4 u4MplsTunnelEgressLSRId,
                               UINT4 *pu4RetValMplsTunnelHopTableIndex)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        *pu4RetValMplsTunnelHopTableIndex = TE_TNL_HOP_TABLE_INDEX (pTeTnlInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelARHopTableIndex
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelARHopTableIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelARHopTableIndex (UINT4 u4MplsTunnelIndex,
                                 UINT4 u4MplsTunnelInstance,
                                 UINT4 u4MplsTunnelIngressLSRId,
                                 UINT4 u4MplsTunnelEgressLSRId,
                                 UINT4 *pu4RetValMplsTunnelARHopTableIndex)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        *pu4RetValMplsTunnelARHopTableIndex
            = TE_TNL_ARHOP_TABLE_INDEX (pTeTnlInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelCHopTableIndex
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelCHopTableIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelCHopTableIndex (UINT4 u4MplsTunnelIndex,
                                UINT4 u4MplsTunnelInstance,
                                UINT4 u4MplsTunnelIngressLSRId,
                                UINT4 u4MplsTunnelEgressLSRId,
                                UINT4 *pu4RetValMplsTunnelCHopTableIndex)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        *pu4RetValMplsTunnelCHopTableIndex = pTeTnlInfo->u4TnlCHopTableIndex;
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelPrimaryInstance
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelPrimaryInstance
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelPrimaryInstance (UINT4 u4MplsTunnelIndex,
                                 UINT4 u4MplsTunnelInstance,
                                 UINT4 u4MplsTunnelIngressLSRId,
                                 UINT4 u4MplsTunnelEgressLSRId,
                                 UINT4 *pu4RetValMplsTunnelPrimaryInstance)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        *pu4RetValMplsTunnelPrimaryInstance =
            TE_TNL_PRIMARY_TNL_INSTANCE (pTeTnlInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelPrimaryUpTime
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelPrimaryUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelPrimaryUpTime (UINT4 u4MplsTunnelIndex,
                               UINT4 u4MplsTunnelInstance,
                               UINT4 u4MplsTunnelIngressLSRId,
                               UINT4 u4MplsTunnelEgressLSRId,
                               UINT4 *pu4RetValMplsTunnelPrimaryUpTime)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4CurrTime = 0;
    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        if (TE_TNL_PRIMARY_INSTANCE_UP_TIME (pTeTnlInfo) == 0)
        {
            /* Tunnel is in down state */
            *pu4RetValMplsTunnelPrimaryUpTime = 0;
        }
        else
        {
            OsixGetSysTime (&u4CurrTime);
            *pu4RetValMplsTunnelPrimaryUpTime = u4CurrTime -
                TE_TNL_PRIMARY_INSTANCE_UP_TIME (pTeTnlInfo);
        }
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelPathChanges
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelPathChanges
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelPathChanges (UINT4 u4MplsTunnelIndex,
                             UINT4 u4MplsTunnelInstance,
                             UINT4 u4MplsTunnelIngressLSRId,
                             UINT4 u4MplsTunnelEgressLSRId,
                             UINT4 *pu4RetValMplsTunnelPathChanges)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        *pu4RetValMplsTunnelPathChanges =
            TE_TNL_LAST_PATH_CHG_COUNT (pTeTnlInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelLastPathChange
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelLastPathChange
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelLastPathChange (UINT4 u4MplsTunnelIndex,
                                UINT4 u4MplsTunnelInstance,
                                UINT4 u4MplsTunnelIngressLSRId,
                                UINT4 u4MplsTunnelEgressLSRId,
                                UINT4 *pu4RetValMplsTunnelLastPathChange)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4CurrTime = 0;
    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        if (TE_TNL_LAST_PATH_CHG_TIME (pTeTnlInfo) == 0)
        {
            /* Tunnel is not up */
            *pu4RetValMplsTunnelLastPathChange = 0;
        }
        else
        {
            OsixGetSysTime (&u4CurrTime);
            *pu4RetValMplsTunnelLastPathChange = u4CurrTime -
                TE_TNL_LAST_PATH_CHG_TIME (pTeTnlInfo);
        }
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelCreationTime
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelCreationTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelCreationTime (UINT4 u4MplsTunnelIndex,
                              UINT4 u4MplsTunnelInstance,
                              UINT4 u4MplsTunnelIngressLSRId,
                              UINT4 u4MplsTunnelEgressLSRId,
                              UINT4 *pu4RetValMplsTunnelCreationTime)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)

    {
        *pu4RetValMplsTunnelCreationTime = TE_TNL_CREATE_TIME (pTeTnlInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelStateTransitions
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelStateTransitions
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelStateTransitions (UINT4 u4MplsTunnelIndex,
                                  UINT4 u4MplsTunnelInstance,
                                  UINT4 u4MplsTunnelIngressLSRId,
                                  UINT4 u4MplsTunnelEgressLSRId,
                                  UINT4 *pu4RetValMplsTunnelStateTransitions)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        *pu4RetValMplsTunnelStateTransitions =
            TE_TNL_STATE_CHG_COUNT (pTeTnlInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelIncludeAnyAffinity
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelIncludeAnyAffinity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelIncludeAnyAffinity (UINT4 u4MplsTunnelIndex,
                                    UINT4 u4MplsTunnelInstance,
                                    UINT4 u4MplsTunnelIngressLSRId,
                                    UINT4 u4MplsTunnelEgressLSRId,
                                    UINT4
                                    *pu4RetValMplsTunnelIncludeAnyAffinity)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        *pu4RetValMplsTunnelIncludeAnyAffinity
            = TE_TNL_INC_ANY_AFFINITY (pTeTnlInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelIncludeAllAffinity
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelIncludeAllAffinity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelIncludeAllAffinity (UINT4 u4MplsTunnelIndex,
                                    UINT4 u4MplsTunnelInstance,
                                    UINT4 u4MplsTunnelIngressLSRId,
                                    UINT4 u4MplsTunnelEgressLSRId,
                                    UINT4
                                    *pu4RetValMplsTunnelIncludeAllAffinity)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        *pu4RetValMplsTunnelIncludeAllAffinity
            = TE_TNL_INC_ALL_AFFINITY (pTeTnlInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelExcludeAnyAffinity
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelExcludeAnyAffinity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelExcludeAnyAffinity (UINT4 u4MplsTunnelIndex,
                                    UINT4 u4MplsTunnelInstance,
                                    UINT4 u4MplsTunnelIngressLSRId,
                                    UINT4 u4MplsTunnelEgressLSRId,
                                    UINT4
                                    *pu4RetValMplsTunnelExcludeAnyAffinity)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        *pu4RetValMplsTunnelExcludeAnyAffinity
            = TE_TNL_EXC_ANY_AFFINITY (pTeTnlInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelPathInUse
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelPathInUse
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelPathInUse (UINT4 u4MplsTunnelIndex, UINT4 u4MplsTunnelInstance,
                           UINT4 u4MplsTunnelIngressLSRId,
                           UINT4 u4MplsTunnelEgressLSRId,
                           UINT4 *pu4RetValMplsTunnelPathInUse)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        *pu4RetValMplsTunnelPathInUse
            = (UINT4) (TE_TNL_PATH_IN_USE (pTeTnlInfo));
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetMplsTunnelRole
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelRole
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelRole (UINT4 u4MplsTunnelIndex, UINT4 u4MplsTunnelInstance,
                      UINT4 u4MplsTunnelIngressLSRId,
                      UINT4 u4MplsTunnelEgressLSRId,
                      INT4 *pi4RetValMplsTunnelRole)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        *pi4RetValMplsTunnelRole = TE_TNL_ROLE (pTeTnlInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelTotalUpTime
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelTotalUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelTotalUpTime (UINT4 u4MplsTunnelIndex,
                             UINT4 u4MplsTunnelInstance,
                             UINT4 u4MplsTunnelIngressLSRId,
                             UINT4 u4MplsTunnelEgressLSRId,
                             UINT4 *pu4RetValMplsTunnelTotalUpTime)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        *pu4RetValMplsTunnelTotalUpTime = TE_TNL_TOTAL_UP_TIME (pTeTnlInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelInstanceUpTime
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelInstanceUpTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetMplsTunnelInstanceUpTime ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetMplsTunnelInstanceUpTime (UINT4 u4MplsTunnelIndex,
                                UINT4 u4MplsTunnelInstance,
                                UINT4 u4MplsTunnelIngressLSRId,
                                UINT4 u4MplsTunnelEgressLSRId,
                                UINT4 *pu4RetValMplsTunnelInstanceUpTime)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4CurrTime = 0;
    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        if (TE_TNL_INSTANCE_UP_TIME (pTeTnlInfo) == 0)
        {
            /* Tunnel is in down state */
            *pu4RetValMplsTunnelInstanceUpTime = 0;
        }
        else
        {
            OsixGetSysTime (&u4CurrTime);
            *pu4RetValMplsTunnelInstanceUpTime = u4CurrTime -
                TE_TNL_INSTANCE_UP_TIME (pTeTnlInfo);
        }
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelAdminStatus
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelAdminStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelAdminStatus (UINT4 u4MplsTunnelIndex,
                             UINT4 u4MplsTunnelInstance,
                             UINT4 u4MplsTunnelIngressLSRId,
                             UINT4 u4MplsTunnelEgressLSRId,
                             INT4 *pi4RetValMplsTunnelAdminStatus)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        *pi4RetValMplsTunnelAdminStatus = TE_TNL_ADMIN_STATUS (pTeTnlInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelOperStatus
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelOperStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelOperStatus (UINT4 u4MplsTunnelIndex, UINT4 u4MplsTunnelInstance,
                            UINT4 u4MplsTunnelIngressLSRId,
                            UINT4 u4MplsTunnelEgressLSRId,
                            INT4 *pi4RetValMplsTunnelOperStatus)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        *pi4RetValMplsTunnelOperStatus = TE_TNL_OPER_STATUS (pTeTnlInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelRowStatus
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/*** $$TRACE_PROCEDURE_NAME = nmhGetMplsTunnelRowStatus ***/
/*** $$TRACE_PROCEDURE_LEVEL = LOW ***/
INT1
nmhGetMplsTunnelRowStatus (UINT4 u4MplsTunnelIndex, UINT4 u4MplsTunnelInstance,
                           UINT4 u4MplsTunnelIngressLSRId,
                           UINT4 u4MplsTunnelEgressLSRId,
                           INT4 *pi4RetValMplsTunnelRowStatus)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        *pi4RetValMplsTunnelRowStatus = TE_TNL_ROW_STATUS (pTeTnlInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelStorageType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelStorageType (UINT4 u4MplsTunnelIndex,
                             UINT4 u4MplsTunnelInstance,
                             UINT4 u4MplsTunnelIngressLSRId,
                             UINT4 u4MplsTunnelEgressLSRId,
                             INT4 *pi4RetValMplsTunnelStorageType)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        *pi4RetValMplsTunnelStorageType = TE_TNL_STORAGE_TYPE (pTeTnlInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FsMplsFrrConstTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsFrrConstTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMplsFrrConstTable (UINT4 u4MplsTunnelIndex,
                                             UINT4 u4MplsTunnelInstance,
                                             UINT4 u4MplsTunnelIngressLSRId,
                                             UINT4 u4MplsTunnelEgressLSRId)
{
    if (nmhValidateIndexInstanceMplsTunnelTable (u4MplsTunnelIndex,
                                                 u4MplsTunnelInstance,
                                                 u4MplsTunnelIngressLSRId,
                                                 u4MplsTunnelEgressLSRId)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsFrrConstTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMplsFrrConstTable (UINT4 *pu4MplsTunnelIndex,
                                     UINT4 *pu4MplsTunnelInstance,
                                     UINT4 *pu4MplsTunnelIngressLSRId,
                                     UINT4 *pu4MplsTunnelEgressLSRId)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    if (nmhGetFirstIndexMplsTunnelTable (pu4MplsTunnelIndex,
                                         pu4MplsTunnelInstance,
                                         pu4MplsTunnelIngressLSRId,
                                         pu4MplsTunnelEgressLSRId)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    /* Check the Frr constant information is configured for the given tunnel */
    pTeTnlInfo = TeGetTunnelInfo (*pu4MplsTunnelIndex, *pu4MplsTunnelInstance,
                                  *pu4MplsTunnelIngressLSRId,
                                  *pu4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Fill the Frr constant information */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);

    if (pTeFrrConstInfo != NULL)
    {
        return SNMP_SUCCESS;    
    }
    else
    {
        return ( nmhGetNextIndexFsMplsFrrConstTable(*pu4MplsTunnelIndex,
                                                     pu4MplsTunnelIndex,
                                                     *pu4MplsTunnelInstance,
                                                     pu4MplsTunnelInstance,
                                                     *pu4MplsTunnelIngressLSRId,
                                                     pu4MplsTunnelIngressLSRId,
                                                     *pu4MplsTunnelEgressLSRId,
                                                     pu4MplsTunnelEgressLSRId) );
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMplsFrrConstTable
 Input       :  The Indices
                MplsTunnelIndex
                nextMplsTunnelIndex
                MplsTunnelInstance
                nextMplsTunnelInstance
                MplsTunnelIngressLSRId
                nextMplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                nextMplsTunnelEgressLSRId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMplsFrrConstTable (UINT4 u4MplsTunnelIndex,
                                    UINT4 *pu4NextMplsTunnelIndex,
                                    UINT4 u4MplsTunnelInstance,
                                    UINT4 *pu4NextMplsTunnelInstance,
                                    UINT4 u4MplsTunnelIngressLSRId,
                                    UINT4 *pu4NextMplsTunnelIngressLSRId,
                                    UINT4 u4MplsTunnelEgressLSRId,
                                    UINT4 *pu4NextMplsTunnelEgressLSRId)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    if (nmhGetNextIndexMplsTunnelTable (u4MplsTunnelIndex,
                                        pu4NextMplsTunnelIndex,
                                        u4MplsTunnelInstance,
                                        pu4NextMplsTunnelInstance,
                                        u4MplsTunnelIngressLSRId,
                                        pu4NextMplsTunnelIngressLSRId,
                                        u4MplsTunnelEgressLSRId,
                                        pu4NextMplsTunnelEgressLSRId)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    
    /* Check the Frr constant information is configured for the given tunnel */    
    pTeTnlInfo = TeGetTunnelInfo (*pu4NextMplsTunnelIndex, *pu4NextMplsTunnelInstance,
                                  *pu4NextMplsTunnelIngressLSRId,
                                  *pu4NextMplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Fill the Frr constant information */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);

    if (pTeFrrConstInfo != NULL)
    {
        return SNMP_SUCCESS;    
    }
    else
    {
        return ( nmhGetNextIndexFsMplsFrrConstTable(*pu4NextMplsTunnelIndex,
                                                    pu4NextMplsTunnelIndex,
                                                    *pu4NextMplsTunnelInstance,
                                                    pu4NextMplsTunnelInstance,
                                                    *pu4NextMplsTunnelIngressLSRId,
                                                    pu4NextMplsTunnelIngressLSRId,
                                                    *pu4NextMplsTunnelEgressLSRId,
                                                    pu4NextMplsTunnelEgressLSRId) );
    }
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsFrrConstIfIndex
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsFrrConstIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsFrrConstIfIndex (UINT4 u4MplsTunnelIndex,
                             UINT4 u4MplsTunnelInstance,
                             UINT4 u4MplsTunnelIngressLSRId,
                             UINT4 u4MplsTunnelEgressLSRId,
                             INT4 *pi4RetValFsMplsFrrConstIfIndex)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, pTeFrrConstInfo should not be NULL */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);

    if (pTeFrrConstInfo != NULL)
    {

        *pi4RetValFsMplsFrrConstIfIndex =
            (INT4) TE_TNL_FRR_CONST_PROT_IFINDEX (pTeFrrConstInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsFrrConstProtectionMethod
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsFrrConstProtectionMethod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsFrrConstProtectionMethod (UINT4 u4MplsTunnelIndex,
                                      UINT4 u4MplsTunnelInstance,
                                      UINT4 u4MplsTunnelIngressLSRId,
                                      UINT4 u4MplsTunnelEgressLSRId,
                                      INT4
                                      *pi4RetValFsMplsFrrConstProtectionMethod)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, pTeFrrConstInfo should not be NULL */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);
    
    if (pTeFrrConstInfo != NULL)
    {
        *pi4RetValFsMplsFrrConstProtectionMethod =
            (INT4) TE_TNL_FRR_CONST_PROT_METHOD (pTeFrrConstInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsFrrConstProtectionType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsFrrConstProtectionType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsFrrConstProtectionType (UINT4 u4MplsTunnelIndex,
                                    UINT4 u4MplsTunnelInstance,
                                    UINT4 u4MplsTunnelIngressLSRId,
                                    UINT4 u4MplsTunnelEgressLSRId,
                                    INT4 *pi4RetValFsMplsFrrConstProtectionType)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, pTeFrrConstInfo should not be NULL */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);

    if (pTeFrrConstInfo != NULL)
    {
        *pi4RetValFsMplsFrrConstProtectionType =
            (INT4) TE_TNL_FRR_CONST_PROT_TYPE (pTeFrrConstInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsFrrConstSetupPrio
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsFrrConstSetupPrio
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsFrrConstSetupPrio (UINT4 u4MplsTunnelIndex,
                               UINT4 u4MplsTunnelInstance,
                               UINT4 u4MplsTunnelIngressLSRId,
                               UINT4 u4MplsTunnelEgressLSRId,
                               UINT4 *pu4RetValFsMplsFrrConstSetupPrio)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, pTeFrrConstInfo should not be NULL */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);

    if (pTeFrrConstInfo != NULL)
    {
        *pu4RetValFsMplsFrrConstSetupPrio =
            (INT4) TE_TNL_FRR_CONST_SETUP_PRIO (pTeFrrConstInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsFrrConstHoldingPrio
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsFrrConstHoldingPrio
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsFrrConstHoldingPrio (UINT4 u4MplsTunnelIndex,
                                 UINT4 u4MplsTunnelInstance,
                                 UINT4 u4MplsTunnelIngressLSRId,
                                 UINT4 u4MplsTunnelEgressLSRId,
                                 UINT4 *pu4RetValFsMplsFrrConstHoldingPrio)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, pTeFrrConstInfo should not be NULL */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);

    if (pTeFrrConstInfo != NULL)
    {
        *pu4RetValFsMplsFrrConstHoldingPrio =
            (INT4) TE_TNL_FRR_CONST_HOLD_PRIO (pTeFrrConstInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsFrrConstSEStyle
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsFrrConstSEStyle
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsFrrConstSEStyle (UINT4 u4MplsTunnelIndex,
                             UINT4 u4MplsTunnelInstance,
                             UINT4 u4MplsTunnelIngressLSRId,
                             UINT4 u4MplsTunnelEgressLSRId,
                             INT4 *pi4RetValFsMplsFrrConstSEStyle)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, pTeFrrConstInfo should not be NULL */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);
    if (pTeFrrConstInfo != NULL)
    {
        *pi4RetValFsMplsFrrConstSEStyle =
            (INT4) TE_TNL_FRR_CONST_SE_STYLE (pTeFrrConstInfo);
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsFrrConstInclAnyAffinity
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsFrrConstInclAnyAffinity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsFrrConstInclAnyAffinity (UINT4 u4MplsTunnelIndex,
                                     UINT4 u4MplsTunnelInstance,
                                     UINT4 u4MplsTunnelIngressLSRId,
                                     UINT4 u4MplsTunnelEgressLSRId,
                                     UINT4
                                     *pu4RetValFsMplsFrrConstInclAnyAffinity)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, pTeFrrConstInfo should not be NULL */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);
    if (pTeFrrConstInfo != NULL)
    {
        *pu4RetValFsMplsFrrConstInclAnyAffinity =
            TE_TNL_FRR_CONST_INCANY_AFFINITY (pTeFrrConstInfo);
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsFrrConstInclAllAffinity
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsFrrConstInclAllAffinity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsFrrConstInclAllAffinity (UINT4 u4MplsTunnelIndex,
                                     UINT4 u4MplsTunnelInstance,
                                     UINT4 u4MplsTunnelIngressLSRId,
                                     UINT4 u4MplsTunnelEgressLSRId,
                                     UINT4
                                     *pu4RetValFsMplsFrrConstInclAllAffinity)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, pTeFrrConstInfo should not be NULL */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);
    if (pTeFrrConstInfo != NULL)
    {
        *pu4RetValFsMplsFrrConstInclAllAffinity =
            TE_TNL_FRR_CONST_INCALL_AFFINITY (pTeFrrConstInfo);
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsFrrConstExclAnyAffinity
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsFrrConstExclAnyAffinity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsFrrConstExclAnyAffinity (UINT4 u4MplsTunnelIndex,
                                     UINT4 u4MplsTunnelInstance,
                                     UINT4 u4MplsTunnelIngressLSRId,
                                     UINT4 u4MplsTunnelEgressLSRId,
                                     UINT4
                                     *pu4RetValFsMplsFrrConstExclAnyAffinity)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, pTeFrrConstInfo should not be NULL */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);
    if (pTeFrrConstInfo != NULL)
    {
        *pu4RetValFsMplsFrrConstExclAnyAffinity =
            TE_TNL_FRR_CONST_EXANY_AFFINITY (pTeFrrConstInfo);
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsFrrConstHopLimit
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsFrrConstHopLimit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsFrrConstHopLimit (UINT4 u4MplsTunnelIndex,
                              UINT4 u4MplsTunnelInstance,
                              UINT4 u4MplsTunnelIngressLSRId,
                              UINT4 u4MplsTunnelEgressLSRId,
                              UINT4 *pu4RetValFsMplsFrrConstHopLimit)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, pTeFrrConstInfo should not be NULL */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);
    if (pTeFrrConstInfo != NULL)
    {
        *pu4RetValFsMplsFrrConstHopLimit =
            (UINT4) TE_TNL_FRR_CONST_HOP_LIMIT (pTeFrrConstInfo);
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsFrrConstBandwidth
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsFrrConstBandwidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsFrrConstBandwidth (UINT4 u4MplsTunnelIndex,
                               UINT4 u4MplsTunnelInstance,
                               UINT4 u4MplsTunnelIngressLSRId,
                               UINT4 u4MplsTunnelEgressLSRId,
                               UINT4 *pu4RetValFsMplsFrrConstBandwidth)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, pTeFrrConstInfo should not be NULL */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);
    if (pTeFrrConstInfo != NULL)
    {
        *pu4RetValFsMplsFrrConstBandwidth =
            TE_TNL_FRR_CONST_BANDWIDTH (pTeFrrConstInfo);
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsFrrConstRowStatus
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsFrrConstRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsFrrConstRowStatus (UINT4 u4MplsTunnelIndex,
                               UINT4 u4MplsTunnelInstance,
                               UINT4 u4MplsTunnelIngressLSRId,
                               UINT4 u4MplsTunnelEgressLSRId,
                               INT4 *pi4RetValFsMplsFrrConstRowStatus)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, pTeFrrConstInfo should not be NULL */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);

    if (pTeFrrConstInfo != NULL)
    {
        *pi4RetValFsMplsFrrConstRowStatus =
            (INT4) TE_TNL_FRR_CONST_ROW_STATUS (pTeFrrConstInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;    
    }
    
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FsMplsTunnelExtTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsTunnelExtTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMplsTunnelExtTable (UINT4 u4MplsTunnelIndex,
                                              UINT4 u4MplsTunnelInstance,
                                              UINT4 u4MplsTunnelIngressLSRId,
                                              UINT4 u4MplsTunnelEgressLSRId)
{
    if (nmhValidateIndexInstanceMplsTunnelTable (u4MplsTunnelIndex,
                                                 u4MplsTunnelInstance,
                                                 u4MplsTunnelIngressLSRId,
                                                 u4MplsTunnelEgressLSRId)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsTunnelExtTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMplsTunnelExtTable (UINT4 *pu4MplsTunnelIndex,
                                      UINT4 *pu4MplsTunnelInstance,
                                      UINT4 *pu4MplsTunnelIngressLSRId,
                                      UINT4 *pu4MplsTunnelEgressLSRId)
{
    if (nmhGetFirstIndexMplsTunnelTable (pu4MplsTunnelIndex,
                                         pu4MplsTunnelInstance,
                                         pu4MplsTunnelIngressLSRId,
                                         pu4MplsTunnelEgressLSRId)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMplsTunnelExtTable
 Input       :  The Indices
                MplsTunnelIndex
                nextMplsTunnelIndex
                MplsTunnelInstance
                nextMplsTunnelInstance
                MplsTunnelIngressLSRId
                nextMplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                nextMplsTunnelEgressLSRId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMplsTunnelExtTable (UINT4 u4MplsTunnelIndex,
                                     UINT4 *pu4NextMplsTunnelIndex,
                                     UINT4 u4MplsTunnelInstance,
                                     UINT4 *pu4NextMplsTunnelInstance,
                                     UINT4 u4MplsTunnelIngressLSRId,
                                     UINT4 *pu4NextMplsTunnelIngressLSRId,
                                     UINT4 u4MplsTunnelEgressLSRId,
                                     UINT4 *pu4NextMplsTunnelEgressLSRId)
{
    if (nmhGetNextIndexMplsTunnelTable (u4MplsTunnelIndex,
                                        pu4NextMplsTunnelIndex,
                                        u4MplsTunnelInstance,
                                        pu4NextMplsTunnelInstance,
                                        u4MplsTunnelIngressLSRId,
                                        pu4NextMplsTunnelIngressLSRId,
                                        u4MplsTunnelEgressLSRId,
                                        pu4NextMplsTunnelEgressLSRId)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelExtProtIfIndex
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsTunnelExtProtIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelExtProtIfIndex (UINT4 u4MplsTunnelIndex,
                                  UINT4 u4MplsTunnelInstance,
                                  UINT4 u4MplsTunnelIngressLSRId,
                                  UINT4 u4MplsTunnelEgressLSRId,
                                  INT4 *pi4RetValFsMplsTunnelExtProtIfIndex)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    *pi4RetValFsMplsTunnelExtProtIfIndex
        = (INT4) TE_TNL_FRR_PROT_IFINDEX (pTeTnlInfo);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelExtProtectionType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsTunnelExtProtectionType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelExtProtectionType (UINT4 u4MplsTunnelIndex,
                                     UINT4 u4MplsTunnelInstance,
                                     UINT4 u4MplsTunnelIngressLSRId,
                                     UINT4 u4MplsTunnelEgressLSRId,
                                     INT4
                                     *pi4RetValFsMplsTunnelExtProtectionType)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    *pi4RetValFsMplsTunnelExtProtectionType
        = (INT4) TE_TNL_FRR_PROT_TYPE (pTeTnlInfo);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelExtBkpTunIdx
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsTunnelExtBkpTunIdx
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelExtBkpTunIdx (UINT4 u4MplsTunnelIndex,
                                UINT4 u4MplsTunnelInstance,
                                UINT4 u4MplsTunnelIngressLSRId,
                                UINT4 u4MplsTunnelEgressLSRId,
                                UINT4 *pu4RetValFsMplsTunnelExtBkpTunIdx)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    *pu4RetValFsMplsTunnelExtBkpTunIdx = TE_TNL_FRR_BKP_TNL_INDEX (pTeTnlInfo);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelExtBkpInst
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsTunnelExtBkpInst
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelExtBkpInst (UINT4 u4MplsTunnelIndex,
                              UINT4 u4MplsTunnelInstance,
                              UINT4 u4MplsTunnelIngressLSRId,
                              UINT4 u4MplsTunnelEgressLSRId,
                              UINT4 *pu4RetValFsMplsTunnelExtBkpInst)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    *pu4RetValFsMplsTunnelExtBkpInst = TE_TNL_FRR_BKP_TNL_INST (pTeTnlInfo);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelExtBkpIngrLSRId
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsTunnelExtBkpIngrLSRId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelExtBkpIngrLSRId (UINT4 u4MplsTunnelIndex,
                                   UINT4 u4MplsTunnelInstance,
                                   UINT4 u4MplsTunnelIngressLSRId,
                                   UINT4 u4MplsTunnelEgressLSRId,
                                   UINT4 *pu4RetValFsMplsTunnelExtBkpIngrLSRId)
{
    UINT4               u4TmpTnlIngressLsrId = TE_ZERO;
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    CONVERT_TO_INTEGER ((TE_TNL_FRR_BKP_TNL_INGRESS_ID (pTeTnlInfo)),
                        u4TmpTnlIngressLsrId);
    u4TmpTnlIngressLsrId = OSIX_NTOHL (u4TmpTnlIngressLsrId);

    *pu4RetValFsMplsTunnelExtBkpIngrLSRId = u4TmpTnlIngressLsrId;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelExtBkpEgrLSRId
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsTunnelExtBkpEgrLSRId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelExtBkpEgrLSRId (UINT4 u4MplsTunnelIndex,
                                  UINT4 u4MplsTunnelInstance,
                                  UINT4 u4MplsTunnelIngressLSRId,
                                  UINT4 u4MplsTunnelEgressLSRId,
                                  UINT4 *pu4RetValFsMplsTunnelExtBkpEgrLSRId)
{
    UINT4               u4TmpTnlEgressLsrId = TE_ZERO;
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    CONVERT_TO_INTEGER ((TE_TNL_FRR_BKP_TNL_EGRESS_ID (pTeTnlInfo)),
                        u4TmpTnlEgressLsrId);
    u4TmpTnlEgressLsrId = OSIX_NTOHL (u4TmpTnlEgressLsrId);

    *pu4RetValFsMplsTunnelExtBkpEgrLSRId = u4TmpTnlEgressLsrId;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelExtOne2OnePlrId
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsTunnelExtOne2OnePlrId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelExtOne2OnePlrId (UINT4 u4MplsTunnelIndex,
                                   UINT4 u4MplsTunnelInstance,
                                   UINT4 u4MplsTunnelIngressLSRId,
                                   UINT4 u4MplsTunnelEgressLSRId,
                                   tSNMP_OCTET_STRING_TYPE
                                   * pRetValFsMplsTunnelExtOne2OnePlrId)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_INTEGER_TO_OCTETSTRING (TE_TNL_FRR_ONE2ONE_PLR_ID (pTeTnlInfo),
                                 pRetValFsMplsTunnelExtOne2OnePlrId);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelExtOne2OnePlrSenderAddrType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsTunnelExtOne2OnePlrSenderAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelExtOne2OnePlrSenderAddrType (UINT4 u4MplsTunnelIndex,
                                               UINT4 u4MplsTunnelInstance,
                                               UINT4 u4MplsTunnelIngressLSRId,
                                               UINT4 u4MplsTunnelEgressLSRId,
                                               INT4
                                               *pi4RetValFsMplsTunnelExtOne2OnePlrSenderAddrType)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    *pi4RetValFsMplsTunnelExtOne2OnePlrSenderAddrType =
        (INT4) TE_TNL_FRR_ONE2ONE_SENDER_ADDR_TYPE (pTeTnlInfo);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelExtOne2OnePlrSenderAddr
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsTunnelExtOne2OnePlrSenderAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelExtOne2OnePlrSenderAddr (UINT4 u4MplsTunnelIndex,
                                           UINT4 u4MplsTunnelInstance,
                                           UINT4 u4MplsTunnelIngressLSRId,
                                           UINT4 u4MplsTunnelEgressLSRId,
                                           tSNMP_OCTET_STRING_TYPE
                                           *
                                           pRetValFsMplsTunnelExtOne2OnePlrSenderAddr)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_INTEGER_TO_OCTETSTRING (TE_TNL_FRR_ONE2ONE_SENDER_ADDR (pTeTnlInfo),
                                 pRetValFsMplsTunnelExtOne2OnePlrSenderAddr);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelExtOne2OnePlrAvoidNAddrType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsTunnelExtOne2OnePlrAvoidNAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelExtOne2OnePlrAvoidNAddrType (UINT4 u4MplsTunnelIndex,
                                               UINT4 u4MplsTunnelInstance,
                                               UINT4 u4MplsTunnelIngressLSRId,
                                               UINT4 u4MplsTunnelEgressLSRId,
                                               INT4
                                               *pi4RetValFsMplsTunnelExtOne2OnePlrAvoidNAddrType)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    *pi4RetValFsMplsTunnelExtOne2OnePlrAvoidNAddrType =
        (INT4) TE_TNL_FRR_ONE2ONE_AVOID_ADDR_TYPE (pTeTnlInfo);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelExtOne2OnePlrAvoidNAddr
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsTunnelExtOne2OnePlrAvoidNAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelExtOne2OnePlrAvoidNAddr (UINT4 u4MplsTunnelIndex,
                                           UINT4 u4MplsTunnelInstance,
                                           UINT4 u4MplsTunnelIngressLSRId,
                                           UINT4 u4MplsTunnelEgressLSRId,
                                           tSNMP_OCTET_STRING_TYPE
                                           *
                                           pRetValFsMplsTunnelExtOne2OnePlrAvoidNAddr)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_INTEGER_TO_OCTETSTRING (TE_TNL_FRR_ONE2ONE_AVOID_ADDR (pTeTnlInfo),
                                 pRetValFsMplsTunnelExtOne2OnePlrAvoidNAddr);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelExtDetourActive
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsTunnelExtDetourActive
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelExtDetourActive (UINT4 u4MplsTunnelIndex,
                                   UINT4 u4MplsTunnelInstance,
                                   UINT4 u4MplsTunnelIngressLSRId,
                                   UINT4 u4MplsTunnelEgressLSRId,
                                   INT4 *pi4RetValFsMplsTunnelExtDetourActive)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    *pi4RetValFsMplsTunnelExtDetourActive =
        (INT4) TE_TNL_FRR_DETOUR_ACTIVE (pTeTnlInfo);

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelExtDetourMerging
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsTunnelExtDetourMerging
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelExtDetourMerging (UINT4 u4MplsTunnelIndex,
                                    UINT4 u4MplsTunnelInstance,
                                    UINT4 u4MplsTunnelIngressLSRId,
                                    UINT4 u4MplsTunnelEgressLSRId,
                                    INT4 *pi4RetValFsMplsTunnelExtDetourMerging)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    *pi4RetValFsMplsTunnelExtDetourMerging =
        (INT4) TE_TNL_FRR_DETOUR_MERGING (pTeTnlInfo);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelExtFacRouteDBProtTunStatus
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsTunnelExtFacRouteDBProtTunStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelExtFacRouteDBProtTunStatus (UINT4 u4MplsTunnelIndex,
                                              UINT4 u4MplsTunnelInstance,
                                              UINT4 u4MplsTunnelIngressLSRId,
                                              UINT4 u4MplsTunnelEgressLSRId,
                                              INT4
                                              *pi4RetValFsMplsTunnelExtFacRouteDBProtTunStatus)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    *pi4RetValFsMplsTunnelExtFacRouteDBProtTunStatus =
        (INT4) TE_TNL_FRR_FAC_TNL_STATUS (pTeTnlInfo);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelExtFacRouteDBProtTunResvBw
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsTunnelExtFacRouteDBProtTunResvBw
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelExtFacRouteDBProtTunResvBw (UINT4 u4MplsTunnelIndex,
                                              UINT4 u4MplsTunnelInstance,
                                              UINT4 u4MplsTunnelIngressLSRId,
                                              UINT4 u4MplsTunnelEgressLSRId,
                                              UINT4
                                              *pu4RetValFsMplsTunnelExtFacRouteDBProtTunResvBw)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    *pu4RetValFsMplsTunnelExtFacRouteDBProtTunResvBw =
        TE_TNL_FRR_FAC_TNL_RESV_BANDWIDTH (pTeTnlInfo);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelExtProtectionMethod
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object
                retValFsMplsTunnelExtProtectionMethod
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelExtProtectionMethod (UINT4 u4MplsTunnelIndex,
                                       UINT4 u4MplsTunnelInstance,
                                       UINT4 u4MplsTunnelIngressLSRId,
                                       UINT4 u4MplsTunnelEgressLSRId,
                                       INT4
                                       *pi4RetValFsMplsTunnelExtProtectionMethod)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    *pi4RetValFsMplsTunnelExtProtectionMethod =
        TE_TNL_FRR_PROT_METHOD (pTeTnlInfo);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelExtMaxGblRevertTime
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object
                retValFsMplsTunnelExtMaxGblRevertTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelExtMaxGblRevertTime (UINT4 u4MplsTunnelIndex,
                                       UINT4 u4MplsTunnelInstance,
                                       UINT4 u4MplsTunnelIngressLSRId,
                                       UINT4 u4MplsTunnelEgressLSRId,
                                       INT4
                                       *pi4RetValFsMplsTunnelExtMaxGblRevertTime)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    *pi4RetValFsMplsTunnelExtMaxGblRevertTime =
        (INT4) TE_TNL_FRR_MAX_GBL_REVERT_TIME (pTeTnlInfo);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/*                       End of file teget1.c                                */
/*---------------------------------------------------------------------------*/
