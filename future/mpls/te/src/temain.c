/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: temain.c,v 1.94 2017/12/22 09:42:23 siva Exp $
 *
 * Description: This file contains all the functions related to
 *              memory allocation of all structures and their
 *              initialisation in the TE-module. It also includes 
 *              functions related to tunnel, traffic params,
 *              Er-Hops and Ar-Hops. 
 *----------------------------------------------------------------------------*/

#include "teincs.h"
#include "tegbl.h"
#include "snmputil.h"
#include "mplslsr.h"

extern UINT2        gu2GenLblSpaceGrpId;

/*****************************************************************************/
/* Function Name : TeInit
 * Description   : This function initializes the TE module. It allocates
 *                 the mempools for the various structures used and allocates
 *                 memory for the static array stuctures used.
 * Input(s)      : NONE
 * Output(s)     : NONE
 * Return(s)     : TE_SUCCESS / TE_FAILURE
 *****************************************************************************/
UINT1
TeInit (VOID)
{
    /* Dummy pointers for system sizing */
    tMplsDiffServElspListSize *pMplsDiffServElspListSize = NULL;
    tTePathListInfoSize *pTePathListInfoSize = NULL;
    tTeArHopListInfoSize *pTeArHopListInfoSize = NULL;
    tTeTrfcParamsSize  *pTeTrfcParamsSize = NULL;
    tTeTunnelIndexListSize *pTeTunnelIndexListSize = NULL;

    UNUSED_PARAM (pMplsDiffServElspListSize);
    UNUSED_PARAM (pTePathListInfoSize);
    UNUSED_PARAM (pTeArHopListInfoSize);
    UNUSED_PARAM (pTeTrfcParamsSize);
    UNUSED_PARAM (pTeTunnelIndexListSize);

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeInit : ENTRY \n");
    if (gTeGblInfo.TnlInfoTbl == NULL)
    {
        TE_DBG (TE_MAIN_FAIL, "MAIN : RBTree is not created \n");
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeInit : INTMD_EXIT \n");
        return TE_FAILURE;
    }
    if (gTeGblInfo.TnlIfIndexBasedTbl == NULL)
    {
        TE_DBG (TE_MAIN_FAIL, "MAIN : Index based RBTree is not created \n");
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeInit : INTMD_EXIT \n");
        return TE_FAILURE;
    }
    if (gTeGblInfo.TnlMsgIdBasedTbl == NULL)
    {
        TE_DBG (TE_MAIN_FAIL, "MAIN : MsgId based RBTree is not created \n");
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeInit : INTMD_EXIT \n");
        return TE_FAILURE;
    }
    /* Creation of Memory Pools */
    if (TeSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        return TE_FAILURE;
    }

    /* Allocation of the Memory for structures in the Global Info */
    if (TeAllocateGblMemory () == TE_FAILURE)
    {
        TE_DBG (TE_MAIN_FAIL, "MAIN : Failure in AllocateGblMemory \n");
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeInit : INTMD_EXIT \n");
        return TE_FAILURE;
    }

    gTeGblInfo.TeParams.i4NotificationEnable = MPLS_SNMP_FALSE;
    TE_ADMIN_STATUS (gTeGblInfo) = TE_ADMIN_UP;
    gTeGblInfo.TeParams.u2MaxRpteErHops = (UINT2) MAX_TE_HOP_PER_PO;

    if (TeSetDefaultRsvpTeParams () == TE_FAILURE)
    {
        TeDeInit ();
        TE_DBG (TE_MAIN_FAIL, "MAIN : Failure in TeSetDefaultRsvpTeParams \n");
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeInit : INTMD_EXIT \n");
        return TE_FAILURE;
    }
    /* STATIC_HLSP */
    if (TeFsMplsLSPMapTunnelTableCreate () == TE_FAILURE)
    {
        TeDeInit ();
        TE_DBG (TE_MAIN_FAIL,
                "MAIN : Failure in TeFsMplsLSPMapTunnelTableCreate \n");
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeInit : INTMD_EXIT \n");
        return TE_FAILURE;
    }
    MEMSET (gaTnlCliArgs, 0, (CLI_MAX_SESSIONS * sizeof (tTnlCliArgs)));
    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeInit : EXIT \n");
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeDeInit
 * Description   : This function perform graceful shutdown of the TE module.
 * Input(s)      : NONE
 * Output(s)     : NONE
 * Return(s)     : NONE
 *****************************************************************************/
VOID
TeDeInit (VOID)
{
    /* This function should release of all the memory utilised in the module.
     * The function should also make sure that before shutting down the TE
     * module, the signalling protocols releases off all the tunnels
     * established.
     * This function needs the support of handling a single event for
     * deletion of all the tunnels passing through the router by the signaling
     * protocol.
     * The current assumption is that the TE module will always be UP.
     */
    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeInit : ENTRY \n");
#ifdef MPLS_SIG_WANTED
    if ((TE_SIG_ADMIN_FLAG (gTeGblInfo) & TE_SIG_PROT_RSVP_SET)
        == TE_SIG_PROT_RSVP_SET)
    {
        RpteProcessTeGoingDownEvent ();
    }
    if ((TE_SIG_ADMIN_FLAG (gTeGblInfo) & TE_SIG_PROT_LDP_SET)
        == TE_SIG_PROT_LDP_SET)
    {
        LdpProcessTEGoingDownEvent ();
    }
#endif

    TeDeleteAllTnls ();

    RBTreeDestroy (gTeGblInfo.TnlIfIndexBasedTbl, NULL, 0);
    RBTreeDestroy (gTeGblInfo.TnlMsgIdBasedTbl, NULL, 0);
    RBTreeDestroy (gTeGblInfo.TnlInfoTbl, TeReleaseTnlInfo, 0);

    TE_DBG (TE_MAIN_PRCS,
            "MAIN : Assocoiated memory in Signalling proto removed \n");
    TeDeAllocateGblMemory ();
    TeDeleteMemPools ();
    TeFsMplsLSPMapTunnelTableDelete ();    /* STATIC_HLSP */
    TE_ADMIN_STATUS (gTeGblInfo) = TE_ADMIN_DOWN;
    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeInit : EXIT \n");
    return;
}

/*****************************************************************************/
/* Function Name : TeSetDefaultRsvpTeParams
 * Description   : This function initializes the Default RsvpTe Traffic
 *                 Parameters (INTSERV NULL SERVICE). These Traffic Params are
 *                 associated whenever no valid traffic params are configured.
 * Input(s)      : NONE
 * Output(s)     : NONE
 * Return(s)     : TE_SUCCESS/TE_FAILURE
 *****************************************************************************/
UINT1
TeSetDefaultRsvpTeParams (VOID)
{
    tRSVPTrfcParams     RSVPTrfcParms;
    tTeTrfcParams       TeTrfcParms;
    tTeTrfcParams      *pTeTrfcParms = NULL;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeSetDefaultRsvpTeParams : ENTRY \n");
    MEMSET (&RSVPTrfcParms, TE_ZERO, sizeof (tRSVPTrfcParams));
    MEMSET (&TeTrfcParms, TE_ZERO, sizeof (tTeTrfcParams));

    TE_RSVPTE_TRFC_PARAMS ((&TeTrfcParms)) = &RSVPTrfcParms;
    TE_RSVPTE_TPARAM_TBR ((&TeTrfcParms)) = TE_ZERO;
    TE_RSVPTE_TPARAM_TBS ((&TeTrfcParms)) = TE_ZERO;
    TE_RSVPTE_TPARAM_PDR ((&TeTrfcParms)) = TE_ZERO;
    TE_RSVPTE_TPARAM_MPU ((&TeTrfcParms)) = TE_ZERO;
    TE_RSVPTE_TPARAM_MPS ((&TeTrfcParms)) = TE_RPTE_MPS_DEFVAL;

    if (TeCreateTrfcParams (TE_SIGPROTO_RSVP,
                            &pTeTrfcParms, &TeTrfcParms) == TE_FAILURE)
    {
        TE_DBG (TE_MAIN_FAIL, "MAIN : Failure in Creating TeParams \n");
        TE_DBG (TE_MAIN_ETEXT,
                "MAIN : TeSetDefaultRsvpTeParams : INTMD_EXIT \n");
        return TE_FAILURE;
    }
    TE_TNLRSRC_ROLE ((pTeTrfcParms)) = TE_INGRESS;
    TE_TNLRSRC_NUM_OF_TUNNELS ((pTeTrfcParms)) = TE_ZERO;
    TE_TNLRSRC_ROW_STATUS ((pTeTrfcParms)) = TE_ACTIVE;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeSetDefaultRsvpTeParams : EXIT \n");
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeSetCfgParams
 * Description   : This function initializes the configuration parameters with
 *                 the default values.
 * Input(s)      : NONE
 * Output(s)     : NONE
 * Return(s)     : NONE
 *****************************************************************************/
VOID
TeSetCfgParams (VOID)
{
    UINT4               u4RBNodeOffset = 0;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeSetCfgParams : ENTRY \n");

    /* ArHop related max values */
    if (FsTESizingParams[MAX_TE_AR_HOP_LIST_INFO_SIZING_ID].
        u4PreAllocatedUnits != MAX_TE_AR_HOP_LIST_INFO)
    {
        TE_MAX_ARHOP_LIST (gTeGblInfo)
            = FsTESizingParams[MAX_TE_AR_HOP_LIST_INFO_SIZING_ID].
            u4PreAllocatedUnits;
        FsTESizingParams[MAX_TE_AR_HOP_LIST_INFO_SIZING_ID].u4StructSize
            = (sizeof (tTeArHopListInfo) * TE_MAX_ARHOP_LIST (gTeGblInfo));
        FsTESizingParams[MAX_TE_AR_HOP_LIST_INFO_SIZING_ID].u4PreAllocatedUnits
            = MAX_TE_AR_HOP_LIST_INFO;
    }
    else
    {
        TE_MAX_ARHOP_LIST (gTeGblInfo) = MAX_TE_ARHOP_LIST;
    }
    TE_MAX_ARHOP_PER_LIST (gTeGblInfo)
        = FsTESizingParams[MAX_TE_AR_HOP_INFO_SIZING_ID].u4PreAllocatedUnits;

    /* Attribute list related max values */
    if (FsTESizingParams[MAX_TE_ATTR_LIST_INFO_SIZING_ID].u4PreAllocatedUnits
        != MAX_TE_ATTR_LIST_INFO)
    {
        gTeGblInfo.TeParams.u4MaxAttrList
            = FsTESizingParams[MAX_TE_ATTR_LIST_INFO_SIZING_ID].
            u4PreAllocatedUnits;
        FsTESizingParams[MAX_TE_ATTR_LIST_INFO_SIZING_ID].u4StructSize
            = (sizeof (tTeAttrListInfo) * gTeGblInfo.TeParams.u4MaxAttrList);
        FsTESizingParams[MAX_TE_ATTR_LIST_INFO_SIZING_ID].u4PreAllocatedUnits
            = MAX_TE_ATTR_LIST_INFO;
    }
    else
    {
        gTeGblInfo.TeParams.u4MaxAttrList = MAX_TE_ATTR_LIST;
    }

    /* C-HOP related max values */
    if (FsTESizingParams[MAX_TE_CHOP_LIST_INFO_SIZING_ID].u4PreAllocatedUnits
        != MAX_TE_CHOP_LIST_INFO)
    {
        gTeGblInfo.TeParams.u4MaxCHopList =
            FsTESizingParams[MAX_TE_CHOP_LIST_INFO_SIZING_ID].
            u4PreAllocatedUnits;
        FsTESizingParams[MAX_TE_CHOP_LIST_INFO_SIZING_ID].u4StructSize
            = (sizeof (tTeCHopListInfo) * gTeGblInfo.TeParams.u4MaxCHopList);
        FsTESizingParams[MAX_TE_CHOP_LIST_INFO_SIZING_ID].u4PreAllocatedUnits
            = MAX_TE_CHOP_LIST_INFO;
    }
    else
    {
        gTeGblInfo.TeParams.u4MaxCHopList = MAX_TE_CHOP_LIST;
    }
    gTeGblInfo.TeParams.u4MaxCHopPerList
        = FsTESizingParams[MAX_TE_CHOP_INFO_SIZING_ID].u4PreAllocatedUnits;

    /* Assigning the Max values in the config params with the default values */
    /* Tunnel related max values */

    TE_MAX_TNLS (gTeGblInfo)
        = FsTESizingParams[MAX_TE_TUNNEL_INFO_SIZING_ID].u4PreAllocatedUnits;

    TE_MAX_REOPT_TNLS (gTeGblInfo)
        =
        FsTESizingParams[MAX_TE_REOPT_TUNNEL_INFO_SIZING_ID].
        u4PreAllocatedUnits;

    if (FsTESizingParams[MAX_TE_TNL_INDEX_LIST_SIZING_ID].u4PreAllocatedUnits
        != MAX_TE_TNL_INDEX_LIST)
    {
        FsTESizingParams[MAX_TE_TNL_INDEX_LIST_SIZING_ID].u4StructSize
            = (sizeof (UINT1) *
               FsTESizingParams[MAX_TE_TNL_INDEX_LIST_SIZING_ID].
               u4PreAllocatedUnits);
        FsTESizingParams[MAX_TE_TNL_INDEX_LIST_SIZING_ID].u4PreAllocatedUnits
            = MAX_TE_TNL_INDEX_LIST;
    }

    /* ErHop related max values */
    if (FsTESizingParams[MAX_TE_PATH_LIST_SIZING_ID].u4PreAllocatedUnits
        != MAX_TE_PATH_LIST)
    {
        TE_MAX_HOP_LIST (gTeGblInfo)
            = FsTESizingParams[MAX_TE_PATH_LIST_SIZING_ID].u4PreAllocatedUnits;
        FsTESizingParams[MAX_TE_PATH_LIST_SIZING_ID].u4StructSize
            = (sizeof (tTePathListInfo) * TE_MAX_HOP_LIST (gTeGblInfo));
        FsTESizingParams[MAX_TE_PATH_LIST_SIZING_ID].u4PreAllocatedUnits
            = MAX_TE_PATH_LIST;
    }
    else
    {
        TE_MAX_HOP_LIST (gTeGblInfo) = MAX_TE_HOP_LIST;
    }
    TE_MAX_PO_PER_HOP_LIST (gTeGblInfo)
        = FsTESizingParams[MAX_TE_PATH_INFO_SIZING_ID].u4PreAllocatedUnits;
    TE_MAX_HOP_PER_PO (gTeGblInfo)
        = FsTESizingParams[MAX_TE_HOP_INFO_SIZING_ID].u4PreAllocatedUnits;

    /* Traffic Param related max values */
    if (FsTESizingParams[MAX_TE_TRFC_PARAMS_SIZING_ID].u4PreAllocatedUnits
        != MAX_TE_TRFC_PARAMS)
    {
        TE_MAX_TRFC_PARAMS (gTeGblInfo)
            = FsTESizingParams[MAX_TE_TRFC_PARAMS_SIZING_ID].
            u4PreAllocatedUnits;
        FsTESizingParams[MAX_TE_TRFC_PARAMS_SIZING_ID].u4StructSize
            = (sizeof (tTeTrfcParams) * TE_MAX_TRFC_PARAMS (gTeGblInfo));
        FsTESizingParams[MAX_TE_TRFC_PARAMS_SIZING_ID].u4PreAllocatedUnits
            = MAX_TE_TRFC_PARAMS;
    }
    else
    {
        TE_MAX_TRFC_PARAMS (gTeGblInfo) = MAX_TE_TRAFFIC_PARAMS;
    }
    TE_MAX_RPTE_TRFC_PARAMS (gTeGblInfo)
        = FsTESizingParams[MAX_TE_RSVP_TRFC_PARAMS_SIZING_ID].
        u4PreAllocatedUnits;
    TE_MAX_CRLDP_TRFC_PARAMS (gTeGblInfo)
        = FsTESizingParams[MAX_TE_CRLD_TRFC_PARAMS_SIZING_ID].
        u4PreAllocatedUnits;

    /* DIFFSERV RELATED MAX_VALUES */
    if (FsTESizingParams[MAX_TE_DIFFSERV_ELSP_LIST_SIZING_ID].
        u4PreAllocatedUnits != MAX_TE_DIFFSERV_ELSP_LIST)
    {
        TE_MAX_DS_ELSPS (gTeGblInfo)
            = FsTESizingParams[MAX_TE_DIFFSERV_ELSP_LIST_SIZING_ID].
            u4PreAllocatedUnits;
        FsTESizingParams[MAX_TE_DIFFSERV_ELSP_LIST_SIZING_ID].u4StructSize
            = (sizeof (tMplsDiffServElspList) *
               FsTESizingParams[MAX_TE_DIFFSERV_ELSP_LIST_SIZING_ID].
               u4PreAllocatedUnits);
        FsTESizingParams[MAX_TE_DIFFSERV_ELSP_LIST_SIZING_ID].
            u4PreAllocatedUnits = TE_ONE;
    }
    else
    {
        TE_MAX_DS_ELSPS (gTeGblInfo) = MAX_TE_DS_ELSPS;
    }
    TE_MAX_DS_LLSPS (gTeGblInfo) = MAX_TE_DS_LLSPS;

    gu4TeDbgFlag = TE_DEF_DBG_FLAG;

    /* Number of Notifications per second. 0 means no throttling */
    TE_NOTIFICATION_MAX_RATE (gTeGblInfo) = 0;    /* Default Value */

    TE_ADMIN_STATUS (gTeGblInfo) = TE_ADMIN_DOWN;

    /* MPLS_P2MP_LSP_CHANGES - S */
    TE_P2MP_TNL_MAX_HOP (gTeGblInfo) = TE_MAX_HOP_PER_PO (gTeGblInfo);
    TE_P2MP_TNL_NOTIFICATION (gTeGblInfo) = TE_SNMP_FALSE;
    /* MPLS_P2MP_LSP_CHANGES - E */

    /* RBTree creation for storing tunnel info */
    u4RBNodeOffset = FSAP_OFFSETOF (tTeTnlInfo, TnlInfoRBNode);

    if ((gTeGblInfo.TnlInfoTbl =
         RBTreeCreateEmbedded (u4RBNodeOffset,
                               (tRBCompareFn) TnlIndicesCmp)) == NULL)
    {
        TE_DBG (TE_MAIN_FAIL, "MAIN : Failure in Creating RBTree \n");
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeSetCfgParams : INTMD_EXIT \n");
    }
    if ((gTeGblInfo.TnlIfIndexBasedTbl =
         RBTreeCreateEmbedded (FSAP_OFFSETOF (tTeTnlInfo, TnlIfIndexbasedBNode),
                               (tRBCompareFn) TnlIfIndexCmp)) == NULL)
    {
        TE_DBG (TE_MAIN_FAIL, "MAIN : Failure in Creating Indexed RBTree \n");
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeSetCfgParams : INTMD_EXIT \n");
    }
    if ((gTeGblInfo.TnlMsgIdBasedTbl =
         RBTreeCreateEmbedded (FSAP_OFFSETOF (tTeTnlInfo, TnlMsgIdBasedRBNode),
                               (tRBCompareFn) TnlMsgIdCmp)) == NULL)
    {
        TE_DBG (TE_MAIN_FAIL, "MAIN : Failure in Creating MsgId RBTree \n");
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeSetCfgParams : INTMD_EXIT \n");
    }
    if ((gTeGblInfo.ReoptimizeTnlList =
         RBTreeCreateEmbedded (FSAP_OFFSETOF
                               (tTeReoptTnlInfo, ReoptTnlInfoRBNode),
                               (tRBCompareFn) ReoptTnlListCmp)) == NULL)
    {
        TE_DBG (TE_MAIN_FAIL,
                "MAIN : Failure in Creating Reoptimize Tnl List RBTree \n");
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeSetCfgParams : INTMD_EXIT \n");
    }

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeSetCfgParams : EXIT \n");
    return;
}

/*****************************************************************************/
/* Function Name : TeAllocateGblMemory
 * Description   : This function allocates memory for the tunnel table and
 *                 Hop, ArHop and Traffic Param Infos.
 * Input(s)      : NONE
 * Output(s)     : NONE
 * Return(s)     : TE_SUCCESS / TE_FAILURE
 *****************************************************************************/
UINT1
TeAllocateGblMemory (VOID)
{
    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeAllocateGblMemory : ENTRY \n");

    /* TE_TNL_INDEX_LIST is used as a array index so we need an extra element */
    TE_TNL_INDEX_LIST (gTeGblInfo) = TE_ALLOC_MEM_BLOCK (TE_TNL_INDEX_POOL_ID);
    if (TE_TNL_INDEX_LIST (gTeGblInfo) != NULL)
    {
        MEMSET (TE_TNL_INDEX_LIST (gTeGblInfo), TE_ZERO,
                (FsTESizingParams[MAX_TE_TNL_INDEX_LIST_SIZING_ID].
                 u4StructSize *
                 FsTESizingParams[MAX_TE_TNL_INDEX_LIST_SIZING_ID].
                 u4PreAllocatedUnits));

    }
    else
    {
        TE_DBG (TE_MAIN_FAIL, "MAIN : Failure in MemAlloc TnlIndexList \n");
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeAllocateGblMemory : INTMD_EXIT \n");
        return TE_FAILURE;
    }

    /* Creation of Memory for Path List Info, ArHop List Info 
     * and Traffic Param
     */
    TE_PATH_LIST_INFO (gTeGblInfo) = TE_ALLOC_MEM_BLOCK (TE_PATH_LIST_POOL_ID);
    if (TE_PATH_LIST_INFO (gTeGblInfo) != NULL)
    {
        MEMSET (TE_PATH_LIST_INFO (gTeGblInfo), TE_ZERO,
                (FsTESizingParams[MAX_TE_PATH_LIST_SIZING_ID].u4StructSize *
                 FsTESizingParams[MAX_TE_PATH_LIST_SIZING_ID].
                 u4PreAllocatedUnits));
    }
    else
    {
        TE_REL_MEM_BLOCK (TE_TNL_INDEX_POOL_ID,
                          (UINT1 *) TE_TNL_INDEX_LIST (gTeGblInfo));
        TE_DBG (TE_MAIN_FAIL, "MAIN : Failure in MemAlloc PathListIndex \n");
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeAllocateGblMemory : INTMD_EXIT \n");
        return TE_FAILURE;
    }

    TE_ARHOP_LIST_INFO (gTeGblInfo) =
        TE_ALLOC_MEM_BLOCK (TE_ARHOP_LIST_POOL_ID);
    if (TE_ARHOP_LIST_INFO (gTeGblInfo) != NULL)
    {
        MEMSET (TE_ARHOP_LIST_INFO (gTeGblInfo), TE_ZERO,
                (FsTESizingParams[MAX_TE_AR_HOP_LIST_INFO_SIZING_ID].
                 u4StructSize *
                 FsTESizingParams[MAX_TE_AR_HOP_LIST_INFO_SIZING_ID].
                 u4PreAllocatedUnits));
    }
    else
    {
        TE_REL_MEM_BLOCK (TE_PATH_LIST_POOL_ID,
                          (UINT1 *) TE_PATH_LIST_INFO (gTeGblInfo));
        TE_REL_MEM_BLOCK (TE_TNL_INDEX_POOL_ID,
                          (UINT1 *) TE_TNL_INDEX_LIST (gTeGblInfo));

        TE_DBG (TE_MAIN_FAIL, "MAIN : Failure in MemAlloc ArHopListIndex \n");
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeAllocateGblMemory : INTMD_EXIT \n");
        return TE_FAILURE;
    }

    TE_TRFC_PARAMS_INFO (gTeGblInfo) =
        TE_ALLOC_MEM_BLOCK (TE_TRFC_PARAMS_POOL_ID);

    if (TE_TRFC_PARAMS_INFO (gTeGblInfo) != NULL)
    {
        MEMSET (TE_TRFC_PARAMS_INFO (gTeGblInfo), TE_ZERO,
                (FsTESizingParams[MAX_TE_TRFC_PARAMS_SIZING_ID].u4StructSize *
                 FsTESizingParams[MAX_TE_TRFC_PARAMS_SIZING_ID].
                 u4PreAllocatedUnits));
    }
    else
    {
        TE_REL_MEM_BLOCK (TE_PATH_LIST_POOL_ID,
                          (UINT1 *) TE_PATH_LIST_INFO (gTeGblInfo));

        TE_REL_MEM_BLOCK (TE_ARHOP_LIST_POOL_ID,
                          (UINT1 *) TE_ARHOP_LIST_INFO (gTeGblInfo));

        TE_REL_MEM_BLOCK (TE_TNL_INDEX_POOL_ID,
                          (UINT1 *) TE_TNL_INDEX_LIST (gTeGblInfo));

        TE_DBG (TE_MAIN_FAIL, "MAIN : Failure in MemAlloc TrafficParms \n");
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeAllocateGblMemory : INTMD_EXIT \n");
        return TE_FAILURE;
    }

    TE_DS_GBL_ELSP_LIST_INFO (gTeGblInfo) =
        TE_ALLOC_MEM_BLOCK (TE_DIFFSERV_POOL_ID);

    if (TE_DS_GBL_ELSP_LIST_INFO (gTeGblInfo) != NULL)
    {
        MEMSET (TE_DS_GBL_ELSP_LIST_INFO (gTeGblInfo), TE_ZERO,
                (FsTESizingParams[MAX_TE_DIFFSERV_ELSP_LIST_SIZING_ID].
                 u4StructSize *
                 FsTESizingParams[MAX_TE_DIFFSERV_ELSP_LIST_SIZING_ID].
                 u4PreAllocatedUnits));
    }
    else
    {
        TE_REL_MEM_BLOCK (TE_TRFC_PARAMS_POOL_ID,
                          (UINT1 *) TE_TRFC_PARAMS_INFO (gTeGblInfo));

        TE_REL_MEM_BLOCK (TE_PATH_LIST_POOL_ID,
                          (UINT1 *) TE_PATH_LIST_INFO (gTeGblInfo));

        TE_REL_MEM_BLOCK (TE_ARHOP_LIST_POOL_ID,
                          (UINT1 *) TE_ARHOP_LIST_INFO (gTeGblInfo));

        TE_REL_MEM_BLOCK (TE_TNL_INDEX_POOL_ID,
                          (UINT1 *) TE_TNL_INDEX_LIST (gTeGblInfo));

        TE_DBG (TE_MAIN_FAIL, "MAIN : Failure in MemAlloc ELSP List \n");
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeAllocateGblMemory : INTMD_EXIT \n");
        return TE_FAILURE;
    }

    gTeGblInfo.pCHopListEntry = TE_ALLOC_MEM_BLOCK (TE_CHOP_LIST_POOL_ID);
    if (gTeGblInfo.pCHopListEntry != NULL)
    {
        MEMSET (gTeGblInfo.pCHopListEntry, TE_ZERO,
                (FsTESizingParams[MAX_TE_CHOP_LIST_INFO_SIZING_ID].
                 u4StructSize *
                 FsTESizingParams[MAX_TE_CHOP_LIST_INFO_SIZING_ID].
                 u4PreAllocatedUnits));
    }
    else
    {
        TE_REL_MEM_BLOCK (TE_DIFFSERV_POOL_ID,
                          (UINT1 *) TE_DS_GBL_ELSP_LIST_INFO (gTeGblInfo));

        TE_REL_MEM_BLOCK (TE_TRFC_PARAMS_POOL_ID,
                          (UINT1 *) TE_TRFC_PARAMS_INFO (gTeGblInfo));

        TE_REL_MEM_BLOCK (TE_PATH_LIST_POOL_ID,
                          (UINT1 *) TE_PATH_LIST_INFO (gTeGblInfo));
        TE_REL_MEM_BLOCK (TE_TNL_INDEX_POOL_ID,
                          (UINT1 *) TE_TNL_INDEX_LIST (gTeGblInfo));

        TE_DBG (TE_MAIN_FAIL, "MAIN : Failure in MemAlloc CHopListIndex \n");
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeAllocateGblMemory : INTMD_EXIT \n");
        return TE_FAILURE;
    }

    gTeGblInfo.pTeAttrListInfo = TE_ALLOC_MEM_BLOCK (TE_ATTR_LIST_POOL_ID);
    if (gTeGblInfo.pTeAttrListInfo != NULL)
    {
        MEMSET (gTeGblInfo.pTeAttrListInfo, TE_ZERO,
                (FsTESizingParams[MAX_TE_ATTR_LIST_INFO_SIZING_ID].
                 u4StructSize *
                 FsTESizingParams[MAX_TE_ATTR_LIST_INFO_SIZING_ID].
                 u4PreAllocatedUnits));
    }
    else
    {
        TE_REL_MEM_BLOCK (TE_CHOP_LIST_POOL_ID,
                          (UINT1 *) gTeGblInfo.pCHopListEntry);
        TE_REL_MEM_BLOCK (TE_DIFFSERV_POOL_ID,
                          (UINT1 *) TE_DS_GBL_ELSP_LIST_INFO (gTeGblInfo));

        TE_REL_MEM_BLOCK (TE_TRFC_PARAMS_POOL_ID,
                          (UINT1 *) TE_TRFC_PARAMS_INFO (gTeGblInfo));

        TE_REL_MEM_BLOCK (TE_PATH_LIST_POOL_ID,
                          (UINT1 *) TE_PATH_LIST_INFO (gTeGblInfo));
        TE_REL_MEM_BLOCK (TE_TNL_INDEX_POOL_ID,
                          (UINT1 *) TE_TNL_INDEX_LIST (gTeGblInfo));

        TE_DBG (TE_MAIN_FAIL, "MAIN : Failure in MemAlloc u4MaxAttrList \n");
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeAllocateGblMemory : INTMD_EXIT \n");
        return TE_FAILURE;
    }

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeAllocateGblMemory : EXIT \n");
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeDeAllocateGblMemory
 * Description   : This function releases all the memory allocated in the global
 *                 Info structure.
 * Input(s)      : NONE
 * Output(s)     : NONE
 * Return(s)     : NONE
 *****************************************************************************/
VOID
TeDeAllocateGblMemory (VOID)
{
    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeDeAllocateGblMemory : ENTRY \n");

    /* Releasing memory allocated for Tunnel Index Next object */
    if (TE_TNL_INDEX_LIST (gTeGblInfo) != NULL)
    {
        TE_REL_MEM_BLOCK (TE_TNL_INDEX_POOL_ID,
                          (UINT1 *) TE_TNL_INDEX_LIST (gTeGblInfo));
        TE_TNL_INDEX_LIST (gTeGblInfo) = NULL;
    }

    /* Releasing Memory allocated for Path List Info, ArHop List Info 
     * and Traffic Param.
     */
    if (TE_PATH_LIST_INFO (gTeGblInfo) != NULL)
    {
        TE_REL_MEM_BLOCK (TE_PATH_LIST_POOL_ID,
                          (UINT1 *) TE_PATH_LIST_INFO (gTeGblInfo));
        TE_PATH_LIST_INFO (gTeGblInfo) = NULL;
    }

    if (TE_ARHOP_LIST_INFO (gTeGblInfo) != NULL)
    {
        TE_REL_MEM_BLOCK (TE_ARHOP_LIST_POOL_ID,
                          (UINT1 *) TE_ARHOP_LIST_INFO (gTeGblInfo));
        TE_ARHOP_LIST_INFO (gTeGblInfo) = NULL;
    }

    if (TE_TRFC_PARAMS_INFO (gTeGblInfo) != NULL)
    {
        TE_REL_MEM_BLOCK (TE_TRFC_PARAMS_POOL_ID,
                          (UINT1 *) TE_TRFC_PARAMS_INFO (gTeGblInfo));
        TE_TRFC_PARAMS_INFO (gTeGblInfo) = NULL;
    }

    if (TE_DS_GBL_ELSP_LIST_INFO (gTeGblInfo) != NULL)
    {
        TE_REL_MEM_BLOCK (TE_DIFFSERV_POOL_ID,
                          (UINT1 *) TE_DS_GBL_ELSP_LIST_INFO (gTeGblInfo));
        TE_DS_GBL_ELSP_LIST_INFO (gTeGblInfo) = NULL;
    }

    if (gTeGblInfo.pCHopListEntry != NULL)
    {
        TE_REL_MEM_BLOCK (TE_CHOP_LIST_POOL_ID,
                          (UINT1 *) gTeGblInfo.pCHopListEntry);
        gTeGblInfo.pCHopListEntry = NULL;
    }

    if (gTeGblInfo.pTeAttrListInfo != NULL)
    {
        TE_REL_MEM_BLOCK (TE_ATTR_LIST_POOL_ID,
                          (UINT1 *) gTeGblInfo.pTeAttrListInfo);
        gTeGblInfo.pTeAttrListInfo = NULL;
    }

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeDeAllocateGblMemory : EXIT \n");
}

/*****************************************************************************/
/* Function Name : TeDeleteMemPools                                          
 * Description   : This routine deletes memory pools                         
 * Input(s)      : NONE                                                      
 * Output(s)     : NONE                                                      
 * Return(s)     : NONE                                                      
 *****************************************************************************/
VOID
TeDeleteMemPools (VOID)
{
    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeDeleteMemPools : ENTRY \n");
    TeSizingMemDeleteMemPools ();
    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeDeleteMemPools : EXIT \n");
}

/*****************************************************************************/
/* Function Name : TeAddNodeToTnlTable                                       
 * Description   : adds the given tunnel info structure on                   
 *                 to the tunnel table.                                        
 * Input(s)      : pTeTnlInfo - Pointer to tTnlInfo structure                
 * Output(s)     : NONE
 * Return(s)     : NONE 
 *****************************************************************************/
VOID
TeAddNodeToTnlTable (tTeTnlInfo * pTeTnlInfo)
{
    UINT4               i4Index;
    UINT4               u4MappedIndx;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeAddNodeToTnlTable : ENTRY \n");
    if (RBTreeAdd ((gTeGblInfo.TnlInfoTbl), (tRBElem *) pTeTnlInfo)
        != RB_SUCCESS)
    {
        TE_DBG (TE_MAIN_ETEXT, "Failed to Add :EXIT TeTnlInfo to TnlInfoTbl\n");
    }

    u4MappedIndx = pTeTnlInfo->u4MappedArrayndx;
    if (u4MappedIndx == 0)
    {
        if (TeGetNextAvailableIndex ((INT4 *) &i4Index) == TE_SUCCESS)
        {
            pTeTnlInfo->u4MappedArrayndx = i4Index;
            TE_TNL_INDEX_LIST (gTeGblInfo)[i4Index - 1] = TE_TRUE;
        }
    }
    else
    {
        TE_TNL_INDEX_LIST (gTeGblInfo)[u4MappedIndx - 1] = TE_TRUE;
    }
    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeAddTnlToTnlTable : EXIT \n");

}

/*****************************************************************************/
/* Function Name : TeDeleteTnlFromTnlTable                               
 * Description   : deletes the tunnel from the tunnel  table and         
 *                 releases memory associated with it to the pool.           
 * Input(s)      : u4TunnelIndex    - Tunnel Index                           
 *                 u4TunnelInstance - Tunnel Instance value                  
 *                     u4TunnelIngressLSRId  - Tunnel Ingress LSR Id             
 *                 u4TunnelEgressLSRId - Tunnel Egress LSR Id                
 * Output(s)     : NONE      
 * Return(s)     : TE_SUCCESS / TE_FAILURE
 *****************************************************************************/
UINT1
TeDeleteTnlFromTnlTable (UINT4 u4TunnelIndex, UINT4 u4TunnelInstance,
                         UINT4 u4TunnelIngressLSRId, UINT4 u4TunnelEgressLSRId)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlInfo         *pTeInstance0Tnl = NULL;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeDeleteTnlFromTnlTable : ENTRY \n");

    pTeTnlInfo = TeGetTunnelInfo (u4TunnelIndex, u4TunnelInstance,
                                  u4TunnelIngressLSRId, u4TunnelEgressLSRId);

    pTeInstance0Tnl = TeGetTunnelInfo (u4TunnelIndex, TE_ZERO,
                                       u4TunnelIngressLSRId,
                                       u4TunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        TE_DBG (TE_MAIN_FAIL, "MAIN : Tunnel does exist in database \n");
        return TE_FAILURE;
    }

    MplsDbUpdateTunnelXCIndex (pTeTnlInfo, NULL, TE_ZERO);

    if (RBTreeRemove (gTeGblInfo.TnlInfoTbl, pTeTnlInfo) == RB_FAILURE)
    {
        TE_DBG (TE_MAIN_ETEXT, "MAIN : RBTree Deletion failed  \n");
    }

    /* To Remove the Tunnel Entry from IfIndexbased RBTree. 
     * In dynamic case, this RBTree will not be populated with the Tunnel
     * Entry. Hence RBTreeRem is desired.*/

    /* The RBTree is also populated in case of HLSP, EGRESS and 
     * BiDirectional Tunnel */

    if ((pTeTnlInfo->u4TnlIfIndex != TE_ZERO) &&
        ((pTeTnlInfo->u4TnlInstance == TE_ZERO) ||
         ((TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_HLSP) &&
          (pTeTnlInfo->u1TnlRole == TE_EGRESS))))
    {
        RBTreeRem (gTeGblInfo.TnlIfIndexBasedTbl, pTeTnlInfo);
    }
    if (pTeTnlInfo->u4OutPathMsgId != TE_ZERO)
    {
        RBTreeRem (gTeGblInfo.TnlMsgIdBasedTbl, pTeTnlInfo);
    }

    if ((pTeInstance0Tnl != NULL)
        && (pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
            MPLS_TE_DEDICATED_ONE2ONE)
        && (pTeTnlInfo->u1TnlPathType == TE_TNL_PROTECTION_PATH))
    {
        gu4TeRpteTnlCount--;
    }

    if (TeReleaseTnlInfo (pTeTnlInfo, 0) != TE_SUCCESS)
    {
        TE_DBG (TE_MAIN_FAIL, "MAIN : mem_block release failed for TnlInfo \n");
        return TE_FAILURE;
    }
    /* One tunnel info is released from tunnel pool, So decrement the global 
     * tunnel counter by one */
    if ((u4TunnelInstance == TE_ZERO) || (pTeInstance0Tnl == NULL))
    {
        gu4TeRpteTnlCount--;
    }
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeGetTunnelInfo
 * Description   : This routine Check the presence of a TE Tunnel info    
 *                 in the Tunnel table.                              
 * Input(s)      : u4TunnelIndex    - Tunnel Index                          
 *                 u4TunnelInstance - Tunnel Instance value                 
 *                 u4TunnelIngressLSRId  - Tunnel Ingress LSR Id            
 *                 u4TunnelEgressLSRId - Tunnel Egress LSR Id               
 * Output(s)     : ppTeTnlInfo  - Pointer to  Pointer of the tunnel info    
 *                 whose index matches u2TunnelIndex.                       
 * Return(s)     : TE_SUCCESS in case of tunnel being present, otherwise    
 *                 TE_FAILURE                                               
 *****************************************************************************/
tTeTnlInfo         *
TeGetTunnelInfo (UINT4 u4TunnelIndex, UINT4 u4TunnelInstance,
                 UINT4 u4TnlIngressLSRId, UINT4 u4TnlEgressLSRId)
{
    tTeTnlInfo          TeTnlInfo;
    tTeTnlInfo         *ppTeTnlInfo;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCheckTnlInTnlTunnelTable : ENTRY \n");

    TeTnlInfo.u4TnlIndex = u4TunnelIndex;
    TeTnlInfo.u4TnlInstance = u4TunnelInstance;
    u4TnlIngressLSRId = OSIX_HTONL (u4TnlIngressLSRId);
    u4TnlEgressLSRId = OSIX_HTONL (u4TnlEgressLSRId);
    MEMCPY (TeTnlInfo.TnlIngressLsrId, &u4TnlIngressLSRId, sizeof (UINT4));
    MEMCPY (TeTnlInfo.TnlEgressLsrId, &u4TnlEgressLSRId, sizeof (UINT4));

    ppTeTnlInfo =
        (tTeTnlInfo *) RBTreeGet (gTeGblInfo.TnlInfoTbl,
                                  (tRBElem *) & TeTnlInfo);

    if (ppTeTnlInfo != NULL)
    {
        TE_DBG (TE_MAIN_PRCS, "MAIN : Valid TnlInfo found in Tunnel Table \n");
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeGetTunnelInfo : EXIT \n");
        return ppTeTnlInfo;
    }

    TE_DBG (TE_MAIN_FAIL, "MAIN : Valid TnlInfo not found in Tunnel Table \n");
    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeGetTunnelInfo : INTMD_EXIT \n");

    ppTeTnlInfo = NULL;
    return ppTeTnlInfo;
}

/*****************************************************************************/
/* Function Name : TeCheckTrfcParamInTrfcParamTable                            
 * Description   : This routine Check the presence of a Traffic Param info   
 *                 in the Traffic param  table.                              
 * Input(s)      : u4TrfcParamIndex   - Traffic Param Index                   
 * Output(s)     : ppTeTrfcParams  - Pointer to Pointer of the traffic param   
 *                 info whose index matches i4TrfcParamIndex.                 
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                      
 *****************************************************************************/
UINT1
TeCheckTrfcParamInTrfcParamTable (UINT4 u4TrfcParamIndex,
                                  tTeTrfcParams ** ppTeTrfcParams)
{
    TE_DBG (TE_MAIN_ETEXT,
            "MAIN : TeCheckTrfcParamInTrfcParamTable : ENTRY \n");
    if (u4TrfcParamIndex == 0)
    {
        TE_DBG (TE_MAIN_FAIL, "MAIN : u4TrfcParamIndex cannot be zero\n");
        TE_DBG (TE_MAIN_ETEXT,
                "MAIN : TeCheckTrfcParamInTrfcParamTable : INTMD_EXIT \n");
        return TE_FAILURE;
    }
    if (TE_TRFC_PARAMS_TNL_RESINDEX (u4TrfcParamIndex) == u4TrfcParamIndex)
    {
        *ppTeTrfcParams = TE_TRFC_PARAMS_PTR (u4TrfcParamIndex);
        TE_DBG1 (TE_MAIN_PRCS,
                 "MAIN : Valid TrafficParams found for index : %d \n",
                 u4TrfcParamIndex);
        TE_DBG (TE_MAIN_ETEXT,
                "MAIN : TeCheckTrfcParamInTrfcParamTable : EXIT \n");
        return TE_SUCCESS;
    }
    TE_DBG (TE_MAIN_FAIL, "MAIN : Valid TrafficParams cannot be found \n");
    TE_DBG (TE_MAIN_ETEXT,
            "MAIN : TeCheckTrfcParamInTrfcParamTable : INTMD_EXIT \n");
    return TE_FAILURE;
}

/*****************************************************************************/
/* Function Name : TeIsAllErHopsActive                                       
 * Description   : Routine that returns true if all the Erhops (if any) in   
 *                 the Er-Hop List is in Active state.                       
 * Input(s)      : pTePathInfo - Er-Hop List Index.                          
 * Output(s)     : bIsPathChanged - If PATH is changed, this is set to TRUE
 * Return(s)     : TE_TRUE / TE_FALSE                                          
 *****************************************************************************/
UINT1
TeIsAllErHopsActive (tTePathInfo * pTePathInfo, BOOL1 * pbIsPathChanged)
{
    tTeHopInfo         *pNHInfo = NULL;

    *pbIsPathChanged = TE_FALSE;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeIsAllErHopsActive : ENTRY \n");

    if (TE_SLL_COUNT (TE_PO_HOP_LIST (pTePathInfo)) == TE_ZERO)
    {
        TE_DBG (TE_MAIN_FAIL,
                "MAIN : ErHop Count in PathOptionList is zero \n");
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeIsAllErHopsActive : INTMD_EXIT \n");
        return TE_FALSE;
    }

    TE_SLL_SCAN (TE_PO_HOP_LIST (pTePathInfo), pNHInfo, tTeHopInfo *)
    {
        if (pNHInfo->bIsMbbRequired == TRUE)
        {
            *pbIsPathChanged = TE_TRUE;
        }

        if (TE_ERHOP_ROW_STATUS (pNHInfo) != TE_ACTIVE)
        {
            TE_DBG1 (TE_MAIN_FAIL,
                     "MAIN : ErHop is not ACTIVE for HopIndex : %d \n",
                     pNHInfo->u4TnlHopIndex);
            TE_DBG (TE_MAIN_ETEXT,
                    "MAIN : TeIsAllErHopsActive : INTMD_EXIT \n");
            return TE_FALSE;
        }
    }

    /* Er-hop Entry is present and all are in ACTIVE state */
    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeIsAllErHopsActive : EXIT \n");
    return TE_TRUE;
}

/*****************************************************************************/
/* Function Name : TeSetEROPathStructure
 * Description   : Routine that Set ERO hops to Path List Info Structure
 *            for Tunnel.
 * Input(s)      : u4TnlHopTableIndex- Er-Hop List Index.
 * Output(s)     : NULL
 * Return(s)     : TE_TRUE / TE_FALSE
 ******************************************************************************/
UINT1
TeSetEROPathStructure (UINT4 u4TnlHopTableIndex, UINT4 u4Flag,
                       tTeTnlInfo * pTeTnlInfo)
{
    tTePathListInfo    *pTePathListInfo = NULL;
    tTePathInfo        *pTePathInfo = NULL;
    /*tTePathInfo        *pTeBackupPathInfo = NULL; */
    BOOL1               bIsPathChanged = TE_FALSE;

    pTePathListInfo = TE_PATH_LIST_ENTRY (u4TnlHopTableIndex);

    if (pTePathListInfo != NULL)
    {
        /*Scan PathListInfo */
        TE_SLL_SCAN (TE_HOP_PO_LIST (pTePathListInfo),
                     pTePathInfo, tTePathInfo *)
        {
            /*Set Path List Info for ERO of Tunnel */
            if ((u4Flag == TE_ZERO) && (pTePathInfo != NULL))
            {
                if (pTePathInfo->u4PathOptionIndex ==
                    TE_TNL_PATH_IN_USE (pTeTnlInfo))
                {
                    pTeTnlInfo->pTePathInfo = pTePathInfo;
                    pTeTnlInfo->pTePathListInfo = pTePathListInfo;
                    break;
                }
            }
            /*Set Path List Info For Backup ERO for One-To-One
             *        Protection Tunnel*/
            else if ((u4Flag == TE_ONE) && (pTePathInfo != NULL))
            {
                if (pTePathInfo->u4PathOptionIndex ==
                    TE_TNL_BACKUPPATH_IN_USE (pTeTnlInfo))
                {
                    pTeTnlInfo->pTeBackupPathInfo = pTePathInfo;
                    break;
                }
            }
            else
            {
                MPLS_CMN_UNLOCK ();
                return TE_FAILURE;

            }
        }
        if (pTePathInfo == NULL)
        {
            return TE_FAILURE;
        }

        /* Check whether all the Er-Hops are Active */
        if (u4Flag == TE_ZERO)
        {
            if (TeIsAllErHopsActive (pTeTnlInfo->pTePathInfo,
                                     &bIsPathChanged) == TE_TRUE)
            {
                /* Incrementing the number of tunnels associated
                 ** with  the Er-hop List.  */
                TE_PO_NUM_TUNNELS (pTeTnlInfo->pTePathInfo)++;

            }
            else
            {
                return TE_FAILURE;
            }
        }
        else
        {
            if (TeIsAllErHopsActive (pTeTnlInfo->pTeBackupPathInfo,
                                     &bIsPathChanged) == TE_TRUE)
            {
                TE_PO_NUM_TUNNELS (pTeTnlInfo->pTeBackupPathInfo)++;

            }
            else
            {
                return TE_FAILURE;
            }

        }
    }
    return TE_SUCCESS;
}

/****************************************************************************
 Function    : TeCheckArHopInfo
 Description : This routine Check the presence of a ARHop info.     
 Input       : u4ArHopListindex - Tunnel HopListIndex
               u4ArHopIndex - Tunnel ArHopIndex  
 Output      : ppTeArHopInfo - Pointer to Pointer of the ARHop info whose
               index matches u4ArHopIndex 
 Returns     : TE_SUCCESS or TE_FAILURE
****************************************************************************/
UINT1
TeCheckArHopInfo (UINT4 u4ArHopListIndex,
                  UINT4 u4ArHopIndex, tTeArHopInfo ** ppTeArHopInfo)
{
    tTeArHopInfo       *pTeArHopInfo = NULL;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCheckArHopInfo : ENTRY \n");

    if (TE_ARHOP_LIST_INDEX (u4ArHopListIndex) != u4ArHopListIndex)
    {
        /* Hop List Info with the corresponding Index is not found */
        TE_DBG1 (TE_MAIN_FAIL,
                 "MAIN : ArHopListIndex %d does not exist \n",
                 u4ArHopListIndex);
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCheckArHopInfo : INTMD_EXIT \n");
        return TE_FAILURE;
    }

    TE_SLL_SCAN (TE_ARHOP_LIST (u4ArHopListIndex), pTeArHopInfo, tTeArHopInfo *)
    {
        if (TE_ARHOP_INDEX (pTeArHopInfo) == u4ArHopIndex)
        {
            *ppTeArHopInfo = pTeArHopInfo;
            TE_DBG (TE_MAIN_PRCS, "MAIN : Valid ArHop Found \n");
            TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCheckArHopInfo : EXIT \n");
            return TE_SUCCESS;
        }
    }
    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCheckArHopInfo : INTMD_EXIT \n");
    return TE_FAILURE;
}

/*****************************************************************************/
/* Function Name : TeCreatePathListInfo                                      
 * Description   : This routine reserves a tTePathListInfo structure from    
 *                 the global array and a pointer to the structure is        
 *                 returned.                                                 
 *                 If all the tTePathListInfo in the global array are        
 *                 reserved then the routine returns failure.                
 * Input(s)      : NONE                                                      
 * Output(s)     : ppTePathListInfo - Pointer to pointer of tTePathListInfo  
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                   
 *****************************************************************************/
UINT1
TeCreatePathListInfo (tTePathListInfo ** ppTePathListInfo)
{
    UINT4               u4PathListIndex;
    tTePathListInfo    *pTePathListInfo = NULL;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCreatePathListInfo : ENTRY \n");

    /* First half of hop list is reserved for administratively created tunnels,
     * second half of hop list is reserved for the signalling tunnels */
    for (u4PathListIndex = (TE_MAX_HOP_LIST (gTeGblInfo) / 2) + 1;
         u4PathListIndex <= TE_MAX_HOP_LIST (gTeGblInfo); u4PathListIndex++)
    {
        if (TE_HOPLIST_INDEX (TE_PATH_LIST_ENTRY (u4PathListIndex)) == TE_ZERO)
        {
            pTePathListInfo = TE_PATH_LIST_ENTRY (u4PathListIndex);
            break;
        }
    }

    if (pTePathListInfo == NULL)
    {
        /* All the HopList structures in the global info are reserved */
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCreatePathListInfo : INTMD_EXIT \n");
        return TE_FAILURE;
    }

    MEMSET ((VOID *) pTePathListInfo, TE_ZERO, sizeof (tTePathListInfo));

    /* Assigning the Hop List Index */
    TE_HOPLIST_INDEX (pTePathListInfo) = u4PathListIndex;
    TE_SLL_INIT (TE_HOP_PO_LIST (pTePathListInfo));

    *ppTePathListInfo = pTePathListInfo;
    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCreatePathListInfo : EXIT \n");
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeCreatePathOptionInfo                                    
 * Description   : This routine allocates a tTePathInfo structure from the   
 *                 Memory Pool.                                               
 *                 This structure is added to the Path Option List present    
 *                 in tTePathListInfo specified by the index                 
 * Input(s)      : u4HopListIndex    - Hop List Index                        
 *                 u4PathOptionIndex - Path Option Index                     
 * Output(s)     : ppTePathInfo - Pointer to pointer of tTePathInfo          
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                   
 *****************************************************************************/
UINT1
TeCreatePathOptionInfo (UINT4 u4HopListIndex, UINT4 u4PathOptionIndex,
                        tTePathInfo ** ppTePathInfo)
{
    tTePathListInfo    *pTePathListInfo = NULL;
    tTePathInfo        *pTePathInfo = NULL;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCreatePathOptionInfo : ENTRY \n");

    pTePathListInfo = TE_PATH_LIST_ENTRY (u4HopListIndex);

    if (TE_HOPLIST_INDEX (pTePathListInfo) != u4HopListIndex)
    {
        /* Hop List Info with the corresponding Index is not found */
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCreatePathOptionInfo : INTMD_EXIT \n");
        return TE_FAILURE;
    }

    /* Allocating memory for tTePathInfo from the memory pool */
    pTePathInfo = (tTePathInfo *) TE_ALLOC_MEM_BLOCK (TE_PO_POOL_ID);

    if (pTePathInfo == NULL)
    {
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCreatePathOptionInfo : INTMD_EXIT \n");
        return TE_FAILURE;
    }
    MEMSET ((VOID *) pTePathInfo, TE_ZERO, sizeof (tTePathInfo));

    /* Initialising the TePathInfo structure */
    TE_SLL_INIT_NODE (&(TE_PO_NEXT_NODE (pTePathInfo)));
    TE_PO_INDEX (pTePathInfo) = u4PathOptionIndex;
    TE_SLL_INIT (TE_PO_HOP_LIST (pTePathInfo));

    /* Adding the Path Option Info in the Path Option List present in Path List
     * Info structure */
    TE_SLL_ADD ((TE_HOP_PO_LIST (pTePathListInfo)),
                &(pTePathInfo->NextPathOption));
    TE_HOP_PO_COUNT (pTePathListInfo)++;

    *ppTePathInfo = pTePathInfo;
    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCreatePathOptionInfo : EXIT \n");
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeCreateHopInfo
 * Description   : This routine allocates a tTeHopInfo structure from the
 *                 Memory Pool.
 *                 This structure is added to the Hop List present in
 *                 tTePathInfo specified by the index
 * Input(s)      : u4HopListIndex    - Hop List Index
 *                 u4PathOptionIndex - Path Option Index
 *                 u4HopIndex        - Hop Index
 * Output(s)     : ppTeHopInfo - Pointer to pointer of tTeHopInfo
 * Return(s)     : TE_SUCCESS / TE_FAILURE
 *****************************************************************************/
UINT1
TeCreateHopInfo (UINT4 u4HopListIndex, UINT4 u4PathOptionIndex,
                 UINT4 u4HopIndex, tTeHopInfo ** ppTeHopInfo)
{
    tTePathInfo        *pTePathInfo = NULL;
    tTeHopInfo         *pTeHopInfo = NULL;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCreateHopInfo : ENTRY \n");

    if (TeCheckPathOptionInfo (u4HopListIndex, u4PathOptionIndex, &pTePathInfo)
        == TE_FAILURE)
    {
        /* Path Option Info with the specified index is not found */
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCreateHopInfo : INTMD_EXIT \n");
        return TE_FAILURE;
    }

    /* Allocating memory for tTeHopInfo from the memory pool */
    pTeHopInfo = (tTeHopInfo *) TE_ALLOC_MEM_BLOCK (TE_HOP_POOL_ID);

    if (pTeHopInfo == NULL)
    {
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCreateHopInfo : INTMD_EXIT \n");
        return TE_FAILURE;
    }
    MEMSET ((VOID *) pTeHopInfo, TE_ZERO, sizeof (tTeHopInfo));

    pTeHopInfo->GmplsTnlHopInfo.u4ForwardLbl = MPLS_INVALID_LABEL;
    pTeHopInfo->GmplsTnlHopInfo.u4ReverseLbl = MPLS_INVALID_LABEL;
    /* Initialising the TeHopInfo structure */
    TE_SLL_INIT_NODE (&TE_ERHOP_NEXT_NODE (pTeHopInfo));
    TE_ERHOP_INDEX (pTeHopInfo) = u4HopIndex;

    /* Adding the Hop Info in the Hop List present in Path Option Info 
     * structure */
    TE_SLL_ADD ((TE_PO_HOP_LIST (pTePathInfo)), &(pTeHopInfo->NextHop));
    (TE_PO_HOP_COUNT (pTePathInfo))++;

    *ppTeHopInfo = pTeHopInfo;
    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCreateHopInfo : EXIT \n");
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeDeletePathListInfo
 * Description   : This routine releases tTePathListInfo structure to
 *                 the global array.
 * Input(s)      : u4PathListInfoIndex - Index of the Path List
 * Output(s)     : None                                       
 * Return(s)     : None                       
 *****************************************************************************/
VOID
TeDeletePathListInfo (UINT4 u4PathListInfoIndex)
{
    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeDeletePathListInfo : ENTRY \n");
    /* Hop List Index is made zero */
    TE_HOPLIST_INDEX (TE_PATH_LIST_ENTRY (u4PathListInfoIndex)) = 0;
    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeDeletePathListInfo : EXIT \n");
}

/*****************************************************************************/
/* Function Name : TeDeletePathOptionInfo                             
 * Description   : This routine releases the tTePathInfo structure to the 
 *                 Memory Pool.                                          
 * Input(s)      : pTePathInfo - Pointer to tTePathInfo                 
 * Output(s)     : NONE                                                
 * Return(s)     : NONE                                               
 *****************************************************************************/
VOID
TeDeletePathOptionInfo (tTePathInfo * pTePathInfo)
{
    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeDeletePathOptionInfo : ENTRY \n");

    /* Releasing the memory back to the memory pool */

    MEMSET (pTePathInfo, TE_ZERO, sizeof (tTePathInfo));
    if ((TE_REL_MEM_BLOCK (TE_PO_POOL_ID,
                           (UINT1 *) (pTePathInfo))) == TE_MEM_FAILURE)
    {
        TE_DBG (TE_MAIN_FAIL, "MAIN : mem_block release failed \n");
    }

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeDeletePathOptionInfo : EXIT \n");
}

/*****************************************************************************/
/* Function Name : TeCheckPathOptionInfo                           
 * Description   : This routine checks whether a tTePathInfo structure with
 *                 the given indices is present or not.                   
 *                 If present the pointer to the structure is returned in
 *                 pTePathInfo.                                         
 * Input(s)      : u4HopListIndex    - Hop List Index                  
 *                 u4PathOptionIndex - Path Option Index              
 * Output(s)     : ppTePathInfo - Pointer to pointer of tTePathInfo  
 * Return(s)     : TE_SUCCESS / TE_FAILURE                          
 *****************************************************************************/
UINT1
TeCheckPathOptionInfo (UINT4 u4HopListIndex, UINT4 u4PathOptionIndex,
                       tTePathInfo ** ppTePathInfo)
{
    tTePathListInfo    *pTePathListInfo = NULL;
    tTePathInfo        *pTePathInfo = NULL;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCheckPathOptionInfo : ENTRY \n");

    pTePathListInfo = TE_PATH_LIST_ENTRY (u4HopListIndex);

    if (TE_HOPLIST_INDEX (pTePathListInfo) != u4HopListIndex)
    {
        /* Hop List Info with the corresponding Index is not found */
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCheckPathOptionInfo : INTMD_EXIT \n");
        return TE_FAILURE;
    }

    TE_SLL_SCAN ((TE_HOP_PO_LIST (pTePathListInfo)), pTePathInfo, tTePathInfo *)
    {
        if (TE_PO_INDEX (pTePathInfo) == u4PathOptionIndex)
        {
            *ppTePathInfo = pTePathInfo;
            TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCheckPathOptionInfo : EXIT \n");
            return TE_SUCCESS;
        }
    }
    /* Path Option Info with the specified index is not found */
    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCheckPathOptionInfo : INTMD_EXIT \n");
    return TE_FAILURE;
}

/*****************************************************************************/
/* Function Name : TeCheckHopInfo                                          
 * Description   : This routine checks whether a tTeHopInfo structure with
 *                 the given indices is present or not.                  
 *                 If present the pointer to the structure is returned in
 *                 pTeHopInfo.                                          
 * Input(s)      : u4HopListIndex    - Hop List Index                  
 *                 u4PathOptionIndex - Path Option Index              
 *                 u4HopIndex        - Hop Index                     
 * Output(s)     : ppTeHopInfo - Pointer to pointer of tTeHopInfo   
 * Return(s)     : TE_SUCCESS / TE_FAILURE                         
 *****************************************************************************/
UINT1
TeCheckHopInfo (UINT4 u4HopListIndex, UINT4 u4PathOptionIndex,
                UINT4 u4HopIndex, tTeHopInfo ** ppTeHopInfo)
{
    tTePathListInfo    *pTePathListInfo = NULL;
    tTePathInfo        *pTePathInfo = NULL;
    tTeHopInfo         *pTeHopInfo = NULL;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCheckHopInfo : ENTRY \n");

    if (u4HopListIndex == TE_ZERO)
    {
        /* Hop List Info with the corresponding Index is not found */
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCheckHopInfo : INTMD_EXIT \n");
        return TE_FAILURE;
    }
    pTePathListInfo = TE_PATH_LIST_ENTRY (u4HopListIndex);

    if (TE_HOPLIST_INDEX (pTePathListInfo) != u4HopListIndex)
    {
        /* Hop List Info with the corresponding Index is not found */
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCheckHopInfo : INTMD_EXIT \n");
        return TE_FAILURE;
    }

    TE_SLL_SCAN ((TE_HOP_PO_LIST (pTePathListInfo)), pTePathInfo, tTePathInfo *)
    {
        if (TE_PO_INDEX (pTePathInfo) == u4PathOptionIndex)
        {
            TE_SLL_SCAN ((TE_PO_HOP_LIST (pTePathInfo)),
                         pTeHopInfo, tTeHopInfo *)
            {
                if (TE_ERHOP_INDEX (pTeHopInfo) == u4HopIndex)
                {
                    *ppTeHopInfo = pTeHopInfo;
                    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCheckHopInfo : EXIT \n");
                    return TE_SUCCESS;
                }
            }
            /* Hop Info with the specified index is not found */
            TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCheckHopInfo : INTMD_EXIT \n");
            return TE_FAILURE;
        }
    }
    /* Path Option Info with the specified index is not found */
    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCheckHopInfo : INTMD_EXIT \n");
    return (INT1) TE_FAILURE;
}

/*****************************************************************************/
/* Function Name : TeGetNextAvailableIndex                           
 * Description   : This routine gets the free available Index for the tunnel.
 * Input(s)      : NONE
 * Output(s)     : pi4Index - pointer of the next available Tnl Index
 * Return(s)     : TE_SUCCESS/TE_FAILURE                            
 *****************************************************************************/
UINT1
TeGetNextAvailableIndex (INT4 *pi4Index)
{
    UINT4               u4Count;

    TE_DBG (TE_LLVL_ETEXT, "MAIN : TeGetNextAvailableIndex : ENTRY \n");
    for (u4Count = TE_ONE; u4Count <= TE_MAX_TNLS (gTeGblInfo); u4Count++)
    {
        if (TE_TNL_INDEX_LIST (gTeGblInfo)[u4Count - 1] == TE_ZERO)
        {
            *pi4Index = (INT4) u4Count;
            TE_DBG1 (TE_LLVL_PRCS,
                     "EXTN : Valid DSGetNextAvailableIndex : %d \n", u4Count);
            TE_DBG (TE_LLVL_ETEXT, "MAIN : TeGetNextAvailableIndex : EXIT \n");
            return TE_SUCCESS;
        }

    }
    TE_DBG (TE_LLVL_ETEXT, "MAIN : TeGetNextAvailableIndex : INTMD_EXIT \n");
    return TE_FAILURE;
}

/*****************************************************************************/
/* Function Name : TeUpdateTnlInUsebyL2Vpn                           
 * Description   : This routine updates the usage of tunnel by L2Vpn module  
 * Input(s)      : u1AssociateFlag   - Tunnel association flag with L2Vpn
 *                 u4TunnelIndex     - Tunnel Index                          
 *                 u4TunnelInstance  - Tunnel Instance value                 
 *                 u4TnlIngressLSRId - Tunnel Ingress LSR Id            
 *                 u4TnlEgressLSRId  - Tunnel Egress LSR Id               
 * Output(s)     : pi4Index - pointer of the next available Tnl Index
 * Return(s)     : TE_SUCCESS/TE_FAILURE                            
 *****************************************************************************/
UINT1
TeUpdateTnlInUsebyL2Vpn (UINT1 u1AssociateFlag,
                         UINT4 u4TunnelIndex,
                         UINT4 u4TunnelInstance,
                         UINT4 u4TnlIngressLSRId, UINT4 u4TnlEgressLSRId)
{
    tTeTnlInfo          TmpTeTnlInfo;
    tTeTnlInfo         *pTmpTeTnlInfo = NULL;
    tTeTnlInfo         *pInstance0Tnl = NULL;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;
    UINT1               u1Instance0FlagSet = TE_ZERO;    /* To indicate TNL_IN_USE_BY_VPN bit is already set for Instance 0 */

    TE_DBG3 (TE_MAIN_ETEXT,
             "MAIN : TeUpdateTnlInUsebyL2Vpn : ENTRY for tnl: %u %u AssociateFlag: %u\n",
             u4TunnelIndex, u4TunnelInstance, u1AssociateFlag);
    MEMSET (&TmpTeTnlInfo, 0, sizeof (tTeTnlInfo));

    MPLS_CMN_LOCK ();
    TmpTeTnlInfo.u4TnlIndex = u4TunnelIndex;

    pTmpTeTnlInfo = RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                   (tRBElem *) & TmpTeTnlInfo, NULL);

    while (pTmpTeTnlInfo != NULL)
    {
        if (u4TunnelIndex != TE_TNL_TNL_INDEX (pTmpTeTnlInfo))
        {
            break;
        }
        CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTmpTeTnlInfo)),
                            u4IngressId);
        CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTmpTeTnlInfo)), u4EgressId);

        if (((u4TunnelInstance == TE_TNL_PRIMARY_TNL_INSTANCE (pTmpTeTnlInfo))
             || ((u4TunnelInstance == pTmpTeTnlInfo->u4TnlInstance)
                 && (u1AssociateFlag == 0)))
            && (u4TnlIngressLSRId == u4IngressId)
            && (u4TnlEgressLSRId == u4EgressId)
            && (pTmpTeTnlInfo->u4TnlInstance != TE_ZERO))
        {
            TE_DBG (TE_MAIN_PRCS,
                    "MAIN : Valid TnlInfo found in TunnelTable \n");

            if (u1AssociateFlag == 1)
            {
                TE_TNL_IN_USE_BY_VPN (pTmpTeTnlInfo) |= TE_TNL_INUSE_BY_L2VPN;
            }
            else if (u1AssociateFlag == 0)
            {
                TE_TNL_IN_USE_BY_VPN (pTmpTeTnlInfo) &=
                    (UINT1) (~(TE_TNL_INUSE_BY_L2VPN));
            }

            /*If TE_TNL_IN_USE_BY_VPN bit is not set for Instance 0 then set, 
             * else it is already set for Instance 0 of Tunnel*/
            if (u1Instance0FlagSet == TE_ZERO)
            {
                pInstance0Tnl =
                    TeGetTunnelInfo (u4TunnelIndex, TE_ZERO,
                                     u4TnlIngressLSRId, u4TnlEgressLSRId);
                if (pInstance0Tnl != NULL)
                {
                    if (u1AssociateFlag == 1)
                    {
                        TE_TNL_IN_USE_BY_VPN (pInstance0Tnl) |=
                            TE_TNL_INUSE_BY_L2VPN;
                    }
                    else if (u1AssociateFlag == 0)
                    {
                        TE_TNL_IN_USE_BY_VPN (pInstance0Tnl) &=
                            (UINT1) (~(TE_TNL_INUSE_BY_L2VPN));
                    }
                }
            }
            TE_DBG6 (TE_MAIN_PRCS, "TeUpdateTnlInUsebyL2Vpn: Tnl index=%d "
                     "In Tnl instance=%d Actual Tnl insta=%d "
                     "Source=%x Dest=%x AssoFlag=%d\r\n",
                     u4TunnelIndex, u4TunnelInstance,
                     TE_TNL_TNL_INSTANCE (pTmpTeTnlInfo), u4TnlIngressLSRId,
                     u4TnlEgressLSRId, u1AssociateFlag);

            TE_DBG (TE_MAIN_ETEXT, "MAIN : TeUpdateTnlInUsebyL2Vpn : EXIT \n");

            MPLS_CMN_UNLOCK ();
            return TE_SUCCESS;
        }
        else if (((u4TunnelInstance ==
                   TE_TNL_PRIMARY_TNL_INSTANCE (pTmpTeTnlInfo))
                  || (pTmpTeTnlInfo->u1TnlOperStatus == TE_OPER_DOWN))
                 && (u4TnlIngressLSRId == u4IngressId)
                 && (u4TnlEgressLSRId == u4EgressId)
                 && (pTmpTeTnlInfo->u4TnlInstance == TE_ZERO))
        {
            pInstance0Tnl =
                TeGetTunnelInfo (u4TunnelIndex, TE_ZERO,
                                 u4TnlIngressLSRId, u4TnlEgressLSRId);
            if (pInstance0Tnl != NULL)
            {
                if (u1AssociateFlag == 1)
                {
                    TE_TNL_IN_USE_BY_VPN (pInstance0Tnl) |=
                        TE_TNL_INUSE_BY_L2VPN;
                }
                else if (u1AssociateFlag == 0)
                {
                    TE_TNL_IN_USE_BY_VPN (pInstance0Tnl) &=
                        (UINT1) (~(TE_TNL_INUSE_BY_L2VPN));
                }
                u1Instance0FlagSet = TE_ONE;
            }
        }
        else if ((pTmpTeTnlInfo->u4TnlInstance == TE_ZERO)
                 && (u1AssociateFlag == 0))
        {
            if (u1Instance0FlagSet == TE_ZERO)
            {
                pInstance0Tnl =
                    TeGetTunnelInfo (u4TunnelIndex, TE_ZERO,
                                     u4TnlIngressLSRId, u4TnlEgressLSRId);
                if (pInstance0Tnl != NULL)
                {
                    TE_TNL_IN_USE_BY_VPN (pInstance0Tnl) &=
                        (UINT1) (~(TE_TNL_INUSE_BY_L2VPN));
                    u1Instance0FlagSet = TE_ONE;
                    TE_DBG6 (TE_MAIN_PRCS,
                             "TeUpdateTnlInUsebyL2Vpn: Unset u1TnlInUseByVPN in case when  index=%d "
                             "In Tnl instance=%d Actual Tnl insta=%d "
                             "Source=%x Dest=%x AssoFlag=%d \r\n",
                             u4TunnelIndex, u4TunnelInstance,
                             TE_TNL_TNL_INSTANCE (pTmpTeTnlInfo),
                             u4TnlIngressLSRId, u4TnlEgressLSRId,
                             u1AssociateFlag);
                    TE_DBG2 (TE_MAIN_PRCS,
                             "TeUpdateTnlInUsebyL2Vpn: Line = %d "
                             "pInstance0Tnl->u1TnlInUseByVPN=%d\r\n ", __LINE__,
                             pInstance0Tnl->u1TnlInUseByVPN);
                }
            }
        }
        pTmpTeTnlInfo = RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                       (tRBElem *) pTmpTeTnlInfo, NULL);
    }
    MPLS_CMN_UNLOCK ();

    TE_DBG (TE_MAIN_FAIL, "MAIN : Valid TnlInfo not found in HashTable \n");
    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeUpdateTnlInUsebyL2Vpn : INTMD_EXIT \n");
    return TE_FAILURE;
}

/*****************************************************************************/
/* Function Name : TeGetTnlOperStatusBySrcAndDest
 * Description   : This routine checks for the presence of tunnel in tunnel
 *                 table and returns tunnel identifier, its oper state
 * Input(s)      : u4TnlIngressLSRId - Tunnel Ingress LSR Id
 *                 u4TnlEgressLSRId  - Tunnel Egress LSR Id
 *                 u4PriTunnelIndex  - Tunnel Identifier of Primary LSP
 * Output(s)     : u4TunnelIndex      - Tunnel Identifier
 *                 u1TunnelOperStatus - Operational status of tunnel
 * Return(s)     : TE_SUCCESS/TE_FAILURE
 *****************************************************************************/
UINT1
TeGetTnlOperStatusBySrcAndDest (UINT4 u4TnlIngressLSRId,
                                UINT4 u4TnlEgressLSRId,
                                UINT4 u4PriTunnelIndex,
                                UINT4 *pu4TunnelIndex,
                                UINT1 *pu1TunnelOperStatus)
{
    tTeTnlInfo         *pTmpTeTnlInfo = NULL;
    tTeTnlInfo         *pNextTeTnlInfo = NULL;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeGetTnlOperStatusBySrcAndDest : ENTRY \n");

    pTmpTeTnlInfo = RBTreeGetFirst (gTeGblInfo.TnlInfoTbl);

    while (pTmpTeTnlInfo != NULL)
    {
        pNextTeTnlInfo = RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                        (tRBElem *) pTmpTeTnlInfo, NULL);
        CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTmpTeTnlInfo)),
                            u4IngressId);
        CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTmpTeTnlInfo)), u4EgressId);

        if (pTmpTeTnlInfo->u4TnlInstance == TE_ZERO)
        {
            pTmpTeTnlInfo = pNextTeTnlInfo;
            continue;
        }

        if ((u4TnlIngressLSRId == u4IngressId) &&
            (u4TnlEgressLSRId == u4EgressId))
        {
            if ((u4PriTunnelIndex != TE_ZERO) &&
                (u4PriTunnelIndex == TE_TNL_TNL_INDEX (pTmpTeTnlInfo)))
            {
                pTmpTeTnlInfo = pNextTeTnlInfo;
                continue;
            }

            TE_DBG (TE_MAIN_PRCS,
                    "MAIN : Valid TnlInfo found in TunnelTable \n");
            TE_DBG (TE_MAIN_ETEXT,
                    "MAIN : TeGetTnlOperStatusBySrcAndDest : EXIT \n");
            *pu4TunnelIndex = TE_TNL_TNL_INDEX (pTmpTeTnlInfo);
            *pu1TunnelOperStatus = TE_TNL_OPER_STATUS (pTmpTeTnlInfo);

            /* To indicate L2VPN/BGP had placed a query */
            TE_TNL_IN_USE_BY_VPN (pTmpTeTnlInfo) |= TE_TNL_QUERY_MADE;
            return TE_SUCCESS;
        }
        pTmpTeTnlInfo = pNextTeTnlInfo;
    }

    TE_DBG (TE_MAIN_FAIL, "MAIN : Valid TnlInfo not found in TunnelTable \n");
    TE_DBG (TE_MAIN_ETEXT,
            "MAIN : TeGetTnlOperStatusBySrcAndDest : INTMD_EXIT \n");
    return TE_FAILURE;
}

/*****************************************************************************
 * Function Name : TeSendTnlTrapAndSyslog
 * Description   : This routine sends the Notification Information to the 
 *                 manager and Syslog message to syslog server.
 * Input(s)      : pTeTnlInfo - Pointer to Tunnel Information Structure.
 *                 u1TrapType - Trap Type.
 * Output(s)     : u4TunnelIndex      - Tunnel Identifier
 *                 u1TunnelOperStatus - Operational status of tunnel
 * Return(s)     : TE_SUCCESS/TE_FAILURE
 *****************************************************************************/
INT4
TeSendTnlTrapAndSyslog (tTeTnlInfo * pTeTnlInfo, UINT1 u1TrapType)
{
    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL, *pOidValue = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    UINT4               u4Len = 0, u4TnlIngress = 0, u4TnlEgress = 0;
    UINT4               au4snmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };

    UINT4               au4TeNotification[] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 3, 0, 0 };
    UINT4               au4TeTnlAdminStatus[] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 3, 2, 2, 1, 34, 0, 0, 0, 0 };
    UINT4               au4TeTnlOperStatus[] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 3, 2, 2, 1, 35, 0, 0, 0, 0 };

    CHR1                ac1TeSysLogMsg[MPLS_OBJECT_NAME_LEN];
    CHR1               *pac1Status[] = {
        "Null",
        "Up",
        "Down",
        "Testing",
        "Unknown",
        "Dormant",
        "Not Present",
        "Lower Layer Down"
    };

    CHR1               *pac1SyslogType[] = {
        "Null",
        "TUNNEL UP",
        "TUNNEL DOWN",
        "TUNNEL REROUTED",
        "TUNNEL REOPTIMIZED",
    };

    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;
    CONVERT_TO_INTEGER ((pTeTnlInfo->TnlIngressLsrId), u4TnlIngress);
    u4TnlIngress = OSIX_NTOHL (u4TnlIngress);
    CONVERT_TO_INTEGER ((pTeTnlInfo->TnlEgressLsrId), u4TnlEgress);
    u4TnlEgress = OSIX_NTOHL (u4TnlEgress);

    MEMSET (ac1TeSysLogMsg, 0, sizeof (ac1TeSysLogMsg));

    if (gTeGblInfo.TeParams.i4NotificationEnable == MPLS_SNMP_FALSE)
    {
        TE_DBG (TE_MAIN_PRCS, "Send Trap: Notification not enabled \n");
        return TE_FAILURE;
    }

    if (pTeTnlInfo->GmplsTnlInfo.u1EncodingType != GMPLS_TUNNEL_LSP_NOT_GMPLS)
    {
        return (TeSendGmplsTnlTrapAndSyslog (pTeTnlInfo, u1TrapType));
    }

    SNPRINTF (ac1TeSysLogMsg, sizeof (ac1TeSysLogMsg),
              "%s: Tunnel %d %d %x %x is Administratively %s and "
              "Operationally %s",
              pac1SyslogType[u1TrapType],
              pTeTnlInfo->u4TnlIndex, pTeTnlInfo->u4TnlPrimaryInstance,
              u4TnlIngress, u4TnlEgress,
              pac1Status[pTeTnlInfo->u1TnlAdminStatus],
              pac1Status[pTeTnlInfo->u1TnlOperStatus]);

    /* Trap OID construction. Telling Manager that you have received
     * trap for mplsTeNotifications.x */
    pOid = alloc_oid (sizeof (au4snmpTrapOid) / sizeof (UINT4));
    if (pOid == NULL)
    {
        TE_DBG (TE_MAIN_SNMP, "Send Trap : Alloc OID Failed \n");
        return TE_FAILURE;
    }
    MEMCPY (pOid->pu4_OidList, au4snmpTrapOid, sizeof (au4snmpTrapOid));
    pOid->u4_Length = sizeof (au4snmpTrapOid) / sizeof (UINT4);

    pOidValue = alloc_oid (sizeof (au4TeNotification) / sizeof (UINT4));
    if (pOidValue == NULL)
    {
        TE_DBG (TE_MAIN_SNMP, "Send Trap : Alloc OID Failed \n");
        free_oid (pOid);
        return TE_FAILURE;
    }
    u4Len = sizeof (au4TeNotification) / sizeof (UINT4);
    au4TeNotification[u4Len - 1] = u1TrapType;
    MEMCPY (pOidValue->pu4_OidList, au4TeNotification,
            sizeof (au4TeNotification));
    pOidValue->u4_Length = u4Len;

    pVbList = ((tSNMP_VAR_BIND *)
               SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OBJECT_ID,
                                     0, 0, NULL, pOidValue, SnmpCnt64Type));
    if (pVbList == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        TE_DBG (TE_MAIN_SNMP, "Send Trap : Alloc Var Bind Failed \n");
        return TE_FAILURE;
    }

    pStartVb = pVbList;

    /* Value of the Trap  -> mplsTunnelAdminStatus */
    pOid = alloc_oid (sizeof (au4TeTnlAdminStatus) / sizeof (UINT4));
    if (pOid == NULL)
    {
        TE_DBG (TE_MAIN_SNMP, "Send Trap : Alloc OID Failed \n");
        SNMP_free_snmp_vb_list (pStartVb);
        return TE_FAILURE;
    }
    u4Len = sizeof (au4TeTnlAdminStatus) / sizeof (UINT4);
    au4TeTnlAdminStatus[u4Len - 4] = pTeTnlInfo->u4TnlIndex;
    au4TeTnlAdminStatus[u4Len - 3] = pTeTnlInfo->u4TnlInstance;
    au4TeTnlAdminStatus[u4Len - 2] = u4TnlIngress;
    au4TeTnlAdminStatus[u4Len - 1] = u4TnlEgress;
    MEMCPY (pOid->pu4_OidList, au4TeTnlAdminStatus,
            sizeof (au4TeTnlAdminStatus));
    pOid->u4_Length = u4Len;

    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_INTEGER,
                                                  0,
                                                  pTeTnlInfo->u1TnlAdminStatus,
                                                  NULL, NULL, SnmpCnt64Type));
    if (pVbList->pNextVarBind == NULL)
    {
        free_oid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        TE_DBG (TE_MAIN_SNMP, "Send Trap : Alloc VarBind Failed \n");
        return TE_FAILURE;
    }

    pVbList = pVbList->pNextVarBind;

    /* Value of the Trap  -> mplsTunnelOperStatus */
    pOid = alloc_oid (sizeof (au4TeTnlOperStatus) / sizeof (UINT4));
    if (pOid == NULL)
    {
        TE_DBG (TE_MAIN_SNMP, "Send Trap : Alloc OID Failed \n");
        SNMP_free_snmp_vb_list (pStartVb);
        return TE_FAILURE;
    }
    u4Len = sizeof (au4TeTnlOperStatus) / sizeof (UINT4);
    au4TeTnlOperStatus[u4Len - 4] = pTeTnlInfo->u4TnlIndex;
    au4TeTnlOperStatus[u4Len - 3] = pTeTnlInfo->u4TnlInstance;
    au4TeTnlOperStatus[u4Len - 2] = u4TnlIngress;
    au4TeTnlOperStatus[u4Len - 1] = u4TnlEgress;
    MEMCPY (pOid->pu4_OidList, au4TeTnlOperStatus, sizeof (au4TeTnlOperStatus));
    pOid->u4_Length = u4Len;

    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_INTEGER,
                                                  0,
                                                  pTeTnlInfo->u1TnlOperStatus,
                                                  NULL, NULL, SnmpCnt64Type));
    if (pVbList->pNextVarBind == NULL)
    {
        free_oid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        TE_DBG (TE_MAIN_SNMP, "Send Trap : Alloc VarBind Failed \n");
        return TE_FAILURE;
    }

    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;

    MplsPortFmNotifyFaults (pStartVb, ac1TeSysLogMsg);

    return (TE_SUCCESS);
}

/****************************************************************************
 Function    :  TnlIndicesCmp
 Input       :  First and Next TE Tunnel Info. for comparison.  
 Output      :  TnlIndicesCmp function returns positive value 
                if the first tunnel index is greater than next 
                tunnel index else returns negative value.
 Returns     :  Positive or Negative  Integer value
****************************************************************************/

INT4
TnlIndicesCmp (tTeTnlInfo * pFirstTeTnlInfo, tTeTnlInfo * pNextTeTnlInfo)
{

    INT4                i4Return = 0;

    if (pFirstTeTnlInfo->u4TnlIndex > pNextTeTnlInfo->u4TnlIndex)
    {
        return 1;
    }
    else if (pFirstTeTnlInfo->u4TnlIndex < pNextTeTnlInfo->u4TnlIndex)
    {
        return (-1);
    }

    if (pFirstTeTnlInfo->u4TnlInstance > pNextTeTnlInfo->u4TnlInstance)
    {
        return 1;
    }
    else if (pFirstTeTnlInfo->u4TnlInstance < pNextTeTnlInfo->u4TnlInstance)
    {
        return (-1);
    }

    if ((i4Return = MEMCMP (pFirstTeTnlInfo->TnlIngressLsrId,
                            pNextTeTnlInfo->TnlIngressLsrId,
                            IPV4_ADDR_LENGTH)) != 0)
    {
        return i4Return;
    }
    if ((i4Return = MEMCMP (pFirstTeTnlInfo->TnlEgressLsrId,
                            pNextTeTnlInfo->TnlEgressLsrId,
                            IPV4_ADDR_LENGTH)) != 0)
    {
        return i4Return;
    }

    return 0;
}

INT4
TeGetTnlCount (UINT1 u1Type)
{
    INT4                i4Count = 0;
    tTeTnlInfo         *pTeTnlInfo = NULL;

    pTeTnlInfo = (tTeTnlInfo *) RBTreeGetFirst (gTeGblInfo.TnlInfoTbl);

    while (pTeTnlInfo != NULL)
    {
        if (TE_TNL_SIGPROTO (pTeTnlInfo) == u1Type)
        {
            i4Count++;
        }
        pTeTnlInfo = RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                    (tRBElem *) pTeTnlInfo, NULL);
    }
    return i4Count;
}

/******************************************************************************
* Function Name : TeGetTnlInfoByOwnerAndRole
 Input          : pu1TunnelName  - Name of the tunnel.
                  u4TnlOwner     - Tunnel Owner (Has to be TE_TNL_OWNER_SNMP
                                   or TE_ZERO).
                  u4TnlRole      - Tunnel Role.
 Output         : pTmpTeTnlInfo - Tunnel Information structure.
 Returns        : pTmpTeTnlInfo or NULL
****************************************************************************/
tTeTnlInfo         *
TeGetTnlInfoByOwnerAndRole (UINT4 u4TnlIndex, UINT4 u4TnlInst,
                            UINT4 u4IngressId, UINT4 u4EgressId,
                            UINT4 u4TnlOwner, UINT4 u4TnlRole)
{
    tTeTnlInfo          TeTnlInfo;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlInfo         *pNextTeTnlInfo = NULL;
    UINT4               u4TnlLsrId = 0;
    UINT4               u4TnlIngressId = 0;
    UINT4               u4TnlEgressId = 0;

    TeTnlInfo.u4TnlIndex = u4TnlIndex;
    TeTnlInfo.u4TnlInstance = u4TnlInst;
    u4TnlLsrId = u4IngressId;
    u4TnlLsrId = OSIX_HTONL (u4TnlLsrId);
    MEMCPY (TeTnlInfo.TnlIngressLsrId, &u4TnlLsrId, sizeof (UINT4));
    u4TnlLsrId = u4EgressId;
    u4TnlLsrId = OSIX_HTONL (u4TnlLsrId);
    MEMCPY (TeTnlInfo.TnlEgressLsrId, &u4TnlLsrId, sizeof (UINT4));

    if ((u4TnlIndex != 0) && (u4TnlInst != 0) && (u4IngressId != 0) &&
        (u4EgressId != 0))
    {
        /* This routine is to return the complete tnl information 
         * based on partial tnl indices */
        return NULL;
    }

    pTeTnlInfo = (tTeTnlInfo *) RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                               (tRBElem *) & TeTnlInfo, NULL);

    if (pTeTnlInfo == NULL)
    {
        return NULL;
    }
    while (pTeTnlInfo != NULL)
    {
        pNextTeTnlInfo = RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                        (tRBElem *) pTeTnlInfo, NULL);
        if (u4TnlIndex != TE_TNL_TNL_INDEX (pTeTnlInfo))
        {
            break;
        }

        if ((u4TnlInst != TE_ZERO) &&
            (u4TnlInst != TE_TNL_TNL_INSTANCE (pTeTnlInfo)))
        {
            break;
        }
        CONVERT_TO_INTEGER (TE_TNL_INGRESS_LSRID (pTeTnlInfo), u4TnlIngressId);

        if ((u4IngressId != 0) && (u4IngressId != TE_NTOHL (u4TnlIngressId)))
        {
            pTeTnlInfo = pNextTeTnlInfo;
            continue;
        }
        CONVERT_TO_INTEGER (TE_TNL_EGRESS_LSRID (pTeTnlInfo), u4TnlEgressId);

        if ((u4EgressId != 0) && (u4EgressId != TE_NTOHL (u4TnlEgressId)))
        {
            pTeTnlInfo = pNextTeTnlInfo;
            continue;
        }

        if ((u4TnlOwner != TE_ZERO) &&
            (u4TnlOwner != (UINT4) TE_TNL_OWNER (pTeTnlInfo)))
        {
            pTeTnlInfo = pNextTeTnlInfo;
            continue;
        }
        if ((u4TnlRole != TE_ZERO) &&
            (u4TnlRole != (UINT4) TE_TNL_ROLE (pTeTnlInfo)))
        {
            pTeTnlInfo = pNextTeTnlInfo;
            continue;
        }
        return pTeTnlInfo;
    }
    return NULL;
}

/******************************************************************************
* Function Name : TeGetActiveTnlInfo
 Input          : u4TnlIndex  - Tunnel Index
                  u4TnlInst   - Tunnel Instance
                  u4IngressId - Tunnel Ingress Id
          u4EgressId  - Tunnel Egress Id 
 Returns        : pTeActiveTnlInfo or NULL
****************************************************************************/
tTeTnlInfo         *
TeGetActiveTnlInfo (UINT4 u4TnlIndex, UINT4 u4TnlInst,
                    UINT4 u4IngressId, UINT4 u4EgressId)
{
    tTeTnlInfo          TeTnlInfo;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlInfo         *pNextTeTnlInfo = NULL;
    tTeTnlInfo         *pWorkingTnlInfo = NULL;
    tTeTnlInfo         *pTeActiveTnlInfo = NULL;
    tTeTnlInfo         *pBkpTnlInfo = NULL;
    UINT4               u4TnlLsrId = 0;
    UINT4               u4TnlIngressId = 0;
    UINT4               u4TnlEgressId = 0;

    TeTnlInfo.u4TnlIndex = u4TnlIndex;
    TeTnlInfo.u4TnlInstance = u4TnlInst;
    u4TnlLsrId = u4IngressId;
    u4TnlLsrId = OSIX_HTONL (u4TnlLsrId);
    MEMCPY (TeTnlInfo.TnlIngressLsrId, &u4TnlLsrId, sizeof (UINT4));
    u4TnlLsrId = u4EgressId;
    u4TnlLsrId = OSIX_HTONL (u4TnlLsrId);
    MEMCPY (TeTnlInfo.TnlEgressLsrId, &u4TnlLsrId, sizeof (UINT4));

    if ((u4TnlIndex != 0) && (u4TnlInst != 0) && (u4IngressId != 0) &&
        (u4EgressId != 0))
    {
        /* This routine is to return the complete tnl information 
         * based on partial tnl indices */
        TE_DBG (TE_MAIN_FAIL, "MAIN : Invalid index\r\n");
        return NULL;
    }

    pTeTnlInfo = (tTeTnlInfo *) RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                               (tRBElem *) & TeTnlInfo, NULL);

    if (pTeTnlInfo == NULL)
    {
        TE_DBG (TE_MAIN_FAIL, "MAIN : Tunnel not found\r\n");
        return NULL;
    }

    while (pTeTnlInfo != NULL)
    {
        pNextTeTnlInfo = RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                        (tRBElem *) pTeTnlInfo, NULL);
        if (u4TnlIndex != TE_TNL_TNL_INDEX (pTeTnlInfo))
        {
            break;
        }

        if ((TE_TNL_TNL_INSTANCE (pTeTnlInfo) >= TE_DETOUR_TNL_INSTANCE) ||
            (pTeTnlInfo->u4TnlInstance == TE_ZERO))
        {
            /* Don't consider the backup tunnels and Instance 0 tunnels. */
            pTeTnlInfo = pNextTeTnlInfo;
            continue;
        }

        CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTeTnlInfo)),
                            u4TnlIngressId);
        CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTeTnlInfo)), u4TnlEgressId);

        u4TnlIngressId = TE_NTOHL (u4TnlIngressId);
        u4TnlEgressId = TE_NTOHL (u4TnlEgressId);
        if (pTeTnlInfo->u1TnlPathType == TE_TNL_PROTECTION_PATH)
        {
            pWorkingTnlInfo = TeGetTunnelInfo (pTeTnlInfo->u4BkpTnlIndex,
                                               pTeTnlInfo->u4BkpTnlInstance,
                                               u4TnlIngressId, u4TnlEgressId);
            if ((pWorkingTnlInfo != NULL) &&
                (pWorkingTnlInfo->u1TnlLocalProtectInUse == LOCAL_PROT_AVAIL))
            {
                pTeTnlInfo = pNextTeTnlInfo;
                continue;
            }
        }

        if (((u4IngressId == u4TnlIngressId) &&
             (u4EgressId == u4TnlEgressId))
            || ((u4IngressId == 0) || (u4EgressId == 0)))
        {
            pTeActiveTnlInfo = pTeTnlInfo;
            break;
        }
        pTeTnlInfo = pNextTeTnlInfo;
    }

    if ((pTeActiveTnlInfo != NULL) &&
        (TE_TNL_LOCAL_PROTECTION_IN_USE (pTeActiveTnlInfo) ==
         LOCAL_PROT_IN_USE))
    {
        CONVERT_TO_INTEGER (pTeActiveTnlInfo->BkpTnlIngressLsrId, u4IngressId);
        CONVERT_TO_INTEGER (pTeActiveTnlInfo->BkpTnlEgressLsrId, u4EgressId);

        u4IngressId = OSIX_NTOHL (u4IngressId);
        u4EgressId = OSIX_NTOHL (u4EgressId);

        pBkpTnlInfo = TeGetTunnelInfo (pTeActiveTnlInfo->u4BkpTnlIndex,
                                       pTeActiveTnlInfo->u4BkpTnlInstance,
                                       u4IngressId, u4EgressId);
        if (pBkpTnlInfo == NULL)
        {
            TE_DBG (TE_MAIN_FAIL,
                    "MAIN : Backup Tunnel does not exist in database\r\n");
            return NULL;
        }
        pTeActiveTnlInfo = pBkpTnlInfo;
    }

    return pTeActiveTnlInfo;
}

/******************************************************************************
 Function Name  : TeGetLatestTunnelInfoFromTunnelId
 Input          : pu1TunnelName  - Name of the tunnel.
                  u4TnlOwner     - Tunnel Owner (Has to be TE_TNL_OWNER_SNMP
                                   or TE_ZERO).
                  u4TnlRole      - Tunnel Role.
 Output         : pTmpTeTnlInfo - Tunnel Information structure.
 Returns        : pTmpTeTnlInfo or NULL
****************************************************************************/
tTeTnlInfo         *
TeGetLatestTunnelInfoFromTunnelId (UINT4 u4TunnelId, UINT4 u4TnlOwner,
                                   UINT4 u4TnlRole)
{
    tTeTnlInfo          TmpTeTnlInfo;
    tTeTnlInfo         *pTmpTeTnlInfo = NULL;
    tTeTnlInfo         *pNextTeTnlInfo = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MEMSET (&TmpTeTnlInfo, 0, sizeof (tTeTnlInfo));
    TmpTeTnlInfo.u4TnlIndex = u4TunnelId;
    pTmpTeTnlInfo = RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                   (tRBElem *) & TmpTeTnlInfo, NULL);

    while (pTmpTeTnlInfo != NULL)
    {
        pNextTeTnlInfo = RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                        (tRBElem *) pTmpTeTnlInfo, NULL);
        if (u4TunnelId != TE_TNL_TNL_INDEX (pTmpTeTnlInfo))
        {
            break;
        }

        if (pTmpTeTnlInfo->u4TnlInstance == TE_ZERO)
        {
            pTmpTeTnlInfo = pNextTeTnlInfo;
            continue;
        }

        if ((u4TnlOwner != TE_ZERO) &&
            (u4TnlOwner != (UINT4) TE_TNL_OWNER (pTmpTeTnlInfo)))
        {
            pTmpTeTnlInfo = pNextTeTnlInfo;
            continue;
        }

        if ((u4TnlRole != TE_ZERO) &&
            (u4TnlRole != (UINT4) TE_TNL_ROLE (pTmpTeTnlInfo)))
        {
            pTmpTeTnlInfo = pNextTeTnlInfo;
            continue;
        }

        if (TE_TNL_PRIMARY_TNL_INSTANCE (pTmpTeTnlInfo) >=
            TE_DETOUR_TNL_INSTANCE)
        {
            pTmpTeTnlInfo = pNextTeTnlInfo;
            continue;
        }

        if (pTeTnlInfo == NULL)
        {
            pTeTnlInfo = pTmpTeTnlInfo;
            pTmpTeTnlInfo = pNextTeTnlInfo;
            continue;
        }

        if (TE_TNL_TNL_INSTANCE (pTeTnlInfo) !=
            TE_TNL_TNL_INSTANCE (pTmpTeTnlInfo))
        {
            pTeTnlInfo = pTmpTeTnlInfo;
        }
        pTmpTeTnlInfo = pNextTeTnlInfo;
    }

    return pTeTnlInfo;
}

/******************************************************************************
 Function Name  :  TeCheckXcIndex 
 Input          :  u4XcIndex - Cross Connect Index.
 Output         :  pTeTnlInfo - Matching Tunnel Entry
 Returns        :  TE_SUCCESS / TE_FAILURE
****************************************************************************/
UINT1
TeCheckXcIndex (UINT4 u4XcIndex, tTeTnlInfo ** pTeTnlInfo)
{
    tXcEntry           *pXcEntry = NULL;

    pXcEntry = MplsGetXCEntryByDirection (u4XcIndex, MPLS_DEF_DIRECTION);

    if ((pXcEntry == NULL) || (pXcEntry->pTeTnlInfo == NULL))
    {
        return TE_FAILURE;
    }

    *pTeTnlInfo = pXcEntry->pTeTnlInfo;
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeChkTnlOperStatusByPrimaryInst                           
 * Description   : This routine checks the operstatus of tunnel in tunnel
 *                 table                                            
 * Input(s)      : u4TunnelIndex     - Tunnel Index                          
 *                 u4TunnelInstance  - Tunnel Instance value                 
 *                 u4TnlIngressLSRId - Tunnel Ingress LSR Id            
 *                 u4TnlEgressLSRId  - Tunnel Egress LSR Id               
 * Output(s)     : pi4Index - pointer of the next available Tnl Index
 * Return(s)     : TE_SUCCESS/TE_FAILURE                            
 *****************************************************************************/
UINT1
TeChkTnlOperStatusByPrimaryInst (UINT4 u4TunnelIndex,
                                 UINT4 u4TunnelInstance,
                                 UINT4 u4TnlIngressLSRId,
                                 UINT4 u4TnlEgressLSRId, UINT4 *pu4MplsLspId)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeChkTnlOperStatusByPrimaryInst : ENTRY \n");

    MPLS_CMN_LOCK ();

    if (TeGetTunnelInfoByPrimaryTnlInst (u4TunnelIndex, u4TunnelInstance,
                                         u4TnlIngressLSRId, u4TnlEgressLSRId,
                                         &pTeTnlInfo) == TE_FAILURE)
    {
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }

    if ((TE_TNL_OPER_STATUS (pTeTnlInfo) == TE_OPER_DOWN) ||
        (pTeTnlInfo->u1CPOrMgmtOperStatus == TE_OPER_DOWN))
    {
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }

    if (TE_TNL_ISIF (pTeTnlInfo) == TE_TRUE)
    {
        *pu4MplsLspId = TE_TNL_IFINDEX (pTeTnlInfo);
    }

    MPLS_CMN_UNLOCK ();

    TE_DBG (TE_MAIN_PRCS, "MAIN : Valid TnlInfo found in Tunnel Table \n");
    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeChkTnlOperStatusByPrimaryInst : EXIT \n");
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeGetActiveTnlInst                                        */
/* Description   : This function is used to get the active tunnel instance   */
/*                 of the latest tunnel available for the provided tunnel    */
/*                 indices.                                                  */
/* Input(s)      : u4TnlId - Tunnel index                                    */
/*                 u4TnlIngress - Tunnel ingress address                     */
/*                 u4TnlEgress - Tunnel egress address                       */
/* Output(s)     : u4TnlInst - Tunnel instance                               */
/*                 u4TnlXcIndex - Tunnel XC Index                            */
/*                 u4TnlIfIndex - Tunnel Interface Index                     */
/* Return(s)     : TE_SUCCESS/TE_SUCCESS                                     */
/*****************************************************************************/
UINT4
TeGetActiveTnlInst (UINT4 u4TnlId, UINT4 *pu4TnlInst, UINT4 u4TnlIngress,
                    UINT4 u4TnlEgress, UINT4 *pu4TnlXcIndex,
                    UINT4 *pu4TnlIfIndex)
{
    tTeTnlInfo          TmpTeTnlInfo;
    tTeTnlInfo         *pTmpTeTnlInfo = NULL;
    tTeTnlInfo         *pNextTeTnlInfo = NULL;
    tTeTnlInfo         *pBkpTnlInfo = NULL;
    tTeTnlInfo         *pTeActiveTnlInfo = NULL;
    tTeTnlInfo         *pWorkingTnlInfo = NULL;
    tP2mpBranchEntry   *pTeP2mpBranchEntry = NULL;
    tOutSegment        *pOutSegment = NULL;
    tXcEntry           *pXcEntry = NULL;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;
    UINT4               u4OutSegIndex = 0;

    *pu4TnlInst = TE_ZERO;
    *pu4TnlXcIndex = TE_ZERO;
    *pu4TnlIfIndex = TE_ZERO;

    MPLS_CMN_LOCK ();

    MEMSET (&TmpTeTnlInfo, 0, sizeof (tTeTnlInfo));
    TmpTeTnlInfo.u4TnlIndex = u4TnlId;
    pTmpTeTnlInfo = RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                   (tRBElem *) & TmpTeTnlInfo, NULL);

    while (pTmpTeTnlInfo != NULL)
    {
        pNextTeTnlInfo = RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                        (tRBElem *) pTmpTeTnlInfo, NULL);
        if (u4TnlId != TE_TNL_TNL_INDEX (pTmpTeTnlInfo))
        {
            break;
        }

        if ((TE_TNL_TNL_INSTANCE (pTmpTeTnlInfo) >= TE_DETOUR_TNL_INSTANCE) ||
            (pTmpTeTnlInfo->u4TnlInstance == TE_ZERO))
        {
            /* Don't consider the backup tunnels and Instance 0 tunnels. */
            pTmpTeTnlInfo = pNextTeTnlInfo;
            continue;
        }

        CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTmpTeTnlInfo)),
                            u4IngressId);
        CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTmpTeTnlInfo)), u4EgressId);

        u4IngressId = TE_NTOHL (u4IngressId);
        u4EgressId = TE_NTOHL (u4EgressId);
        if (pTmpTeTnlInfo->u1TnlPathType == TE_TNL_PROTECTION_PATH)
        {
            pWorkingTnlInfo = TeGetTunnelInfo (pTmpTeTnlInfo->u4BkpTnlIndex,
                                               pTmpTeTnlInfo->u4BkpTnlInstance,
                                               u4IngressId, u4EgressId);
            if ((pWorkingTnlInfo != NULL) &&
                (pWorkingTnlInfo->u1TnlLocalProtectInUse == LOCAL_PROT_AVAIL))
            {
                pTmpTeTnlInfo = pNextTeTnlInfo;
                continue;
            }
        }

        if ((pTmpTeTnlInfo->u1TnlPathType == TE_TNL_WORKING_PATH) &&
            (pTmpTeTnlInfo->u1TnlLocalProtectInUse == LOCAL_PROT_IN_USE))
        {
            pTmpTeTnlInfo = pNextTeTnlInfo;
            continue;
        }

        if ((u4TnlIngress == u4IngressId) && (u4TnlEgress == u4EgressId))
        {
            /* In case of P2MP */
            if (TE_TNL_TYPE_P2MP & TE_TNL_TYPE (pTmpTeTnlInfo))
            {
                if (TE_TNL_ROLE (pTmpTeTnlInfo) != TE_EGRESS)
                {
                    TMO_SLL_Scan (&
                                  (TE_P2MP_TNL_BRANCH_LIST_INFO
                                   (pTmpTeTnlInfo)), pTeP2mpBranchEntry,
                                  tP2mpBranchEntry *)
                    {
                        u4OutSegIndex =
                            TE_P2MP_BRANCH_OUTSEG_INDEX (pTeP2mpBranchEntry);
                        pOutSegment =
                            MplsGetOutSegmentTableEntry (u4OutSegIndex);
                        if (NULL == pOutSegment)
                        {
                            continue;
                        }
                        pXcEntry = OUTSEGMENT_XC_INDEX (pOutSegment);
                        break;
                    }
                    if ((NULL == pXcEntry) || (NULL == pOutSegment))
                    {
                        MPLS_CMN_UNLOCK ();
                        return TE_FAILURE;
                    }

                    *pu4TnlIfIndex = OUTSEGMENT_IF_INDEX (pOutSegment);
                    *pu4TnlInst = TE_TNL_TNL_INSTANCE (pTmpTeTnlInfo);
                    *pu4TnlXcIndex = pXcEntry->u4Index;
                }
                else
                {
                    pXcEntry =
                        MplsGetXCEntryByDirection (pTmpTeTnlInfo->u4TnlXcIndex,
                                                   MPLS_DIRECTION_FORWARD);
                    if (NULL == pXcEntry)
                    {
                        MPLS_CMN_UNLOCK ();
                        return TE_FAILURE;
                    }
                    *pu4TnlIfIndex = TE_TNL_IFINDEX (pTmpTeTnlInfo);
                    *pu4TnlInst = TE_TNL_PRIMARY_TNL_INSTANCE (pTmpTeTnlInfo);
                    *pu4TnlXcIndex = pXcEntry->u4Index;
                }
            }
            else
            {
                *pu4TnlIfIndex = TE_TNL_IFINDEX (pTmpTeTnlInfo);
                *pu4TnlXcIndex = pTmpTeTnlInfo->u4TnlXcIndex;
                *pu4TnlInst = TE_TNL_PRIMARY_TNL_INSTANCE (pTmpTeTnlInfo);

                pTeActiveTnlInfo = pTmpTeTnlInfo;

                break;
            }
        }
        pTmpTeTnlInfo = pNextTeTnlInfo;
    }

    if ((pTeActiveTnlInfo != NULL) && (*pu4TnlIfIndex == 0) &&
        (TE_TNL_LOCAL_PROTECTION_IN_USE (pTeActiveTnlInfo) ==
         LOCAL_PROT_IN_USE))
    {
        CONVERT_TO_INTEGER (pTeActiveTnlInfo->BkpTnlIngressLsrId, u4IngressId);
        CONVERT_TO_INTEGER (pTeActiveTnlInfo->BkpTnlEgressLsrId, u4EgressId);

        u4IngressId = OSIX_NTOHL (u4IngressId);
        u4EgressId = OSIX_NTOHL (u4EgressId);

        pBkpTnlInfo = TeGetTunnelInfo (pTeActiveTnlInfo->u4BkpTnlIndex,
                                       pTeActiveTnlInfo->u4BkpTnlInstance,
                                       u4IngressId, u4EgressId);
        if (pBkpTnlInfo == NULL)
        {
            TE_DBG (TE_MAIN_FAIL,
                    "MAIN : Backup Tunnel does not exist in database\r\n");
            MPLS_CMN_UNLOCK ();
            return TE_FAILURE;
        }

        *pu4TnlIfIndex = TE_TNL_IFINDEX (pBkpTnlInfo);
        *pu4TnlXcIndex = pBkpTnlInfo->u4TnlXcIndex;
        *pu4TnlInst = TE_TNL_PRIMARY_TNL_INSTANCE (pBkpTnlInfo);
    }

    MPLS_CMN_UNLOCK ();

    if ((*pu4TnlInst == TE_ZERO) || (*pu4TnlXcIndex == TE_ZERO))
    {
        return TE_FAILURE;
    }

    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeGetLdpRegisteredActiveTunnel                            */
/* Description   : This function is an API to be used by applications like   */
/*                 LDP that has been by tunnel to fetch the Active           */
/*                 Tunnel Information.                                       */
/* Input(s)      : u4TnlId               - Tunnel index                      */
/*                 u4TnlIngress          - Tunnel Ingress                    */
/*                 u4TnlEgress           - Tunnel egress address             */
/* Output(s)     : pTeTnlInfo            - Pointer to Tunnel Information     */
/* Return(s)     : TE_SUCCESS/TE_SUCCESS                                     */
/*****************************************************************************/
UINT4
TeGetLdpRegisteredActiveTunnel (UINT4 u4TnlId,
                                UINT4 u4TnlIngress, UINT4 u4TnlEgress,
                                tTeTnlInfo ** ppTeTnlInfo)
{
    tTeTnlInfo          TmpTeTnlInfo;
    tTeTnlInfo         *pTmpTeTnlInfo = NULL;
    tTeTnlInfo         *pNextTeTnlInfo = NULL;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;

    MEMSET (&TmpTeTnlInfo, 0, sizeof (tTeTnlInfo));
    TmpTeTnlInfo.u4TnlIndex = u4TnlId;
    pTmpTeTnlInfo = RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                   (tRBElem *) & TmpTeTnlInfo, NULL);

    while (pTmpTeTnlInfo != NULL)
    {
        pNextTeTnlInfo = RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                        (tRBElem *) pTmpTeTnlInfo, NULL);

        if (u4TnlId != TE_TNL_TNL_INDEX (pTmpTeTnlInfo))
        {
            break;
        }
        CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTmpTeTnlInfo)),
                            u4IngressId);
        CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTmpTeTnlInfo)), u4EgressId);

        if ((u4TnlIngress != TE_NTOHL (u4IngressId)) ||
            (u4TnlEgress != TE_NTOHL (u4EgressId)))
        {
            pTmpTeTnlInfo = pNextTeTnlInfo;
            continue;
        }

        if (TE_LDP_OVER_RSVP_ENT_INDEX (pTmpTeTnlInfo) != TE_ZERO)
        {
            *ppTeTnlInfo = pTmpTeTnlInfo;
            return TE_SUCCESS;
        }
        pTmpTeTnlInfo = pNextTeTnlInfo;
    }

    return TE_FAILURE;
}

/*****************************************************************************/
/* Function Name : TeGetTunnelInfoByPrimaryTnlInst                           */
/* Description   : This function is an API to be used by applications like   */
/*                 LDP, L2VPN fetch the Tunnel Information by primary tunnel */
/*                 instance.                                                 */
/* Input(s)      : u4TnlId               - Tunnel index                      */
/*                 u4PrimaryTnlInst      - Tunnel Primary Instance           */
/*                 u4TnlIngress          - Tunnel Ingress                    */
/*                 u4TnlEgress           - Tunnel egress address             */
/* Output(s)     : ppTeTnlInfo           - Pointer to Tunnel Information     */
/* Return(s)     : TE_SUCCESS/TE_SUCCESS                                     */
/*****************************************************************************/
UINT4
TeGetTunnelInfoByPrimaryTnlInst (UINT4 u4TnlId, UINT4 u4PrimaryTnlInst,
                                 UINT4 u4TnlIngress, UINT4 u4TnlEgress,
                                 tTeTnlInfo ** ppTeTnlInfo)
{
    tTeTnlInfo          TmpTeTnlInfo;
    tTeTnlInfo         *pTmpTeTnlInfo = NULL;
    tTeTnlInfo         *pNextTeTnlInfo = NULL;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;

    MEMSET (&TmpTeTnlInfo, 0, sizeof (tTeTnlInfo));
    TmpTeTnlInfo.u4TnlIndex = u4TnlId;
    pTmpTeTnlInfo = RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                   (tRBElem *) & TmpTeTnlInfo, NULL);

    while (pTmpTeTnlInfo != NULL)
    {
        pNextTeTnlInfo = RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                        (tRBElem *) pTmpTeTnlInfo, NULL);

        if (u4TnlId != TE_TNL_TNL_INDEX (pTmpTeTnlInfo))
        {
            break;
        }

        if (pTmpTeTnlInfo->u4TnlInstance == TE_ZERO)
        {
            pTmpTeTnlInfo = pNextTeTnlInfo;
            continue;
        }

        CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTmpTeTnlInfo)),
                            u4IngressId);
        CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTmpTeTnlInfo)), u4EgressId);

        if ((u4PrimaryTnlInst != TE_TNL_PRIMARY_TNL_INSTANCE (pTmpTeTnlInfo)) ||
            (u4TnlIngress != TE_NTOHL (u4IngressId)) ||
            (u4TnlEgress != TE_NTOHL (u4EgressId)))
        {
            pTmpTeTnlInfo = pNextTeTnlInfo;
            continue;
        }

        *ppTeTnlInfo = pTmpTeTnlInfo;
        return TE_SUCCESS;
    }

    return TE_FAILURE;
}

/*****************************************************************************/
/* Function Name : TePrcsL2vpnAssociation                                    */
/* Description   : This function verifies whether indication needs to be     */
/*                 given to L2VPN. If yes, it gives indication to L2VPN.     */
/* Input(s)      : pTeTnlInfo            - Pointer to Tunnel Information     */
/*                 u4EvtType     - Event that will be posted to L2VPN        */
/*                                 TE_OPER_DOWN/TE_OPER_UP/TE_TNL_REESTB     */
/* Output(s)     : None.                                                     */
/* Return(s)     : None.                                                     */
/*****************************************************************************/
VOID
TePrcsL2vpnAssociation (tTeTnlInfo * pTeTnlInfo, UINT4 u4EvtType)
{
    UINT4               u4TnlIndex = TE_ZERO;
    UINT4               u4TnlInstance = TE_ZERO;
    UINT4               u4SrcAddr = TE_ZERO;
    UINT4               u4DestAddr = TE_ZERO;

    u4TnlIndex = TE_TNL_TNL_INDEX (pTeTnlInfo);
    u4TnlInstance = TE_TNL_TNL_INSTANCE (pTeTnlInfo);
    CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTeTnlInfo)), u4SrcAddr);
    CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTeTnlInfo)), u4DestAddr);

    u4SrcAddr = TE_NTOHL (u4SrcAddr);
    u4DestAddr = TE_NTOHL (u4DestAddr);

    /* Do not indicate to L2VPN Application for Instance 0 Tunnel */
    if (pTeTnlInfo->u4TnlInstance == TE_ZERO)
    {
        TE_DBG (TE_EXTN_PRCS,
                "Indication not given to L2VPN for Instance 0 tunnel ");
        return;
    }

    if (TE_TNL_IN_USE_BY_VPN (pTeTnlInfo) & TE_TNL_INUSE_BY_L2VPN)
    {
        TE_DBG5 (TE_EXTN_PRCS,
                 "Indication %d given to L2VPN for registered Tunnel "
                 "%d %d %x %x\n",
                 u4EvtType, u4TnlIndex, u4TnlInstance, u4SrcAddr, u4DestAddr);

        TePostEvent (pTeTnlInfo, TE_ZERO, u4EvtType);
    }
    else
    {
        TE_DBG5 (TE_EXTN_PRCS,
                 "Tunnel %d %d %x %x unregistered with L2VPN is processed "
                 "to give indication %d\n",
                 u4TnlIndex, u4TnlInstance, u4SrcAddr, u4DestAddr, u4EvtType);

        if ((u4EvtType == TE_OPER_DOWN) || (u4EvtType == TE_FRR_TNL_PLR_DEL))
        {
            TE_DBG4 (TE_EXTN_PRCS,
                     "Oper Down Indication is not given to L2VPN for unregistered "
                     "%d %d %x %x not registered with L2VPN\n",
                     u4TnlIndex, u4TnlInstance, u4SrcAddr, u4DestAddr);
            return;
        }

        TePostEvent (pTeTnlInfo, TE_ZERO, u4EvtType);

        TE_DBG5 (TE_EXTN_PRCS,
                 "Indication %d given to L2VPN for the Tunnel "
                 "%d %d %x %x\n",
                 u4EvtType, u4TnlIndex, u4TnlInstance, u4SrcAddr, u4DestAddr);
    }

    return;
}

/***************************************************************************
 * FUNCTION NAME    : TeReleaseTnlInfo
 *
 * DESCRIPTION      : This is a callback function registered with RBTree.
 *                    Whenever a RBTree node is deleted this function is called.
 *                    This function clean all the
 *                    links present within the TeTnl Node and release the
 *                    memory used by the TeTnl node.
 * 
 * INPUT            : pRBElem - pointer to the TeTnl Node whose memory needs
 *                            to be freed.
 *                    u4Arg   - Not used.
 *
 * OUTPUT           : None
 *
 * RETURNS          : TE_SUCCESS / TE_FAILURE
 *
 ***************************************************************************/

PUBLIC INT4
TeReleaseTnlInfo (tRBElem * pRBElem, UINT4 u4Arg)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    pTeTnlInfo = (tTeTnlInfo *) pRBElem;

    UNUSED_PARAM (u4Arg);
    MEMSET (pTeTnlInfo, TE_ZERO, sizeof (tTeTnlInfo));

    if (TE_REL_MEM_BLOCK (TE_TNL_POOL_ID,
                          (UINT1 *) (pTeTnlInfo)) != TE_MEM_FAILURE)
    {
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeDeleteTnlFromTnlTable : EXIT \n");
        return TE_SUCCESS;
    }
    else
    {
        TE_DBG (TE_MAIN_FAIL, "MAIN : mem_block release failed for TnlInfo \n");
        return TE_FAILURE;
    }
}

/*******************************************************************************
 * Function Name : MplsGetSegFromTunnelXC
 * Description   : This routine is used to display tunnel information
 * Input(s)      : pXCIndex        - XC Index Octet string
 * Output(s)     : pXCSegmentIndex - XCsegment Index Octet string
 *                 pInSegmentIndex - Insegment Index Octet string
 *                 pOutSegmentIndex - Outsegment Index Octet string
 *                 pu4XCInd         - XC Index 
 *                 pu4InIndex       - Insegment Index
 *                 pu4OutIndex      - Outsegment Index
 * Return(s)     : NONE
 ********************************************************************************/

VOID
MplsGetSegFromTunnelXC (tSNMP_OID_TYPE * pXCIndex,
                        tSNMP_OCTET_STRING_TYPE * pXCSegmentIndex,
                        tSNMP_OCTET_STRING_TYPE * pInSegmentIndex,
                        tSNMP_OCTET_STRING_TYPE * pOutSegmentIndex,
                        UINT4 *pu4XCInd, UINT4 *pu4InIndex, UINT4 *pu4OutIndex)
{
    UINT4               u4Offset;
    static UINT1        au1InSegmentIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegmentIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCSegmentIndex[MPLS_INDEX_LENGTH];

    /* The XCSegment starts from offset 13, InSegment from offset - 13+ 
     * XCSegment's length and OutSegment from offset - 13+XCSegment's length+
     * InSegment's length. The macro GET_SEG_FROM_TUNNEL_XC
     * fills the XCSegmentIndex, InSegmentIndex and OutSegmentIndex
     * entries by copying from the oid list of XCIndex using the offsets
     * specified.*/

    u4Offset = TE_TNL_XC_TABLE_DEF_OFFSET - 1;
    GET_SEG_FROM_TUNNEL_XC (au1XCSegmentIndex, pXCSegmentIndex, pXCIndex,
                            u4Offset);
    MPLS_OCTETSTRING_TO_INTEGER (pXCSegmentIndex, (*pu4XCInd));

    u4Offset = u4Offset + 1;
    GET_SEG_FROM_TUNNEL_XC (au1InSegmentIndex, pInSegmentIndex, pXCIndex,
                            u4Offset);
    MPLS_OCTETSTRING_TO_INTEGER (pInSegmentIndex, (*pu4InIndex));

    u4Offset = u4Offset + 1;
    GET_SEG_FROM_TUNNEL_XC (au1OutSegmentIndex, pOutSegmentIndex,
                            pXCIndex, u4Offset);
    MPLS_OCTETSTRING_TO_INTEGER (pOutSegmentIndex, (*pu4OutIndex));
}

/*****************************************************************************/
/* Function Name : TeCheckOrUpdateHopInfo
 * Description   : This routine checks or updates Hop table variables into
 *                 Path Info structure based on flag
 * Input(s)      : u4PathListIndex   - Path List Index
 *                 u4PathOptionIndex - Path Option Index
 *                 u4ObjectId        - Object Id
 *                 u4ObjValue        - Object Value
 * Output(s)     : None
 * Return(s)     : TE_SUCCESS / TE_FAILURE
 ******************************************************************************/
INT4
TeCheckOrUpdateHopInfo (UINT4 u4PathListIndex, UINT4 u4PathOptionIndex,
                        UINT4 u4ObjValue1, UINT4 u4ObjValue2)
{
    tTePathListInfo    *pTePathListInfo = NULL;
    tTePathInfo        *pTePathInfo = NULL;

    pTePathListInfo = TE_PATH_LIST_ENTRY (u4PathListIndex);

    if (TE_PATH_LIST_INFO (gTeGblInfo) == NULL)
    {
        return TE_FAILURE;
    }

    TMO_SLL_Scan (TE_HOP_PO_LIST (pTePathListInfo), pTePathInfo, tTePathInfo *)
    {
        if (TE_PO_INDEX (pTePathInfo) != u4PathOptionIndex)
        {
            continue;
        }

        if (((pTePathInfo->u1HopIncludeExclude != TE_ZERO) &&
             (pTePathInfo->u1HopIncludeExclude != (UINT1) u4ObjValue1)) ||
            ((pTePathInfo->u1PathCompType != TE_ZERO) &&
             (pTePathInfo->u1PathCompType != (UINT1) u4ObjValue2)))
        {
            return TE_FAILURE;
        }
        pTePathInfo->u1HopIncludeExclude = (UINT1) u4ObjValue1;
        pTePathInfo->u1PathCompType = (UINT1) u4ObjValue2;
        return TE_SUCCESS;
    }

    return TE_FAILURE;
}

/*****************************************************************************/
/* Function Name : TeCreateCHopListInfo
 * Description   : This routine reserves a tTeCHopListInfo structure from
 *                 the global array and a pointer to the structure is
 *                 returned.
 *                 If all the tTeCHopListInfo in the global array are
 *                 reserved then the routine returns failure.
 * Input(s)      : NONE
 * Output(s)     : ppTeCHopListInfo - Pointer to pointer of tTeCHopListInfo
 * Return(s)     : TE_SUCCESS / TE_FAILURE
 ******************************************************************************/
INT4
TeCreateCHopListInfo (tTeCHopListInfo ** ppTeCHopListInfo)
{
    UINT4               u4CHopListIndex = TE_ONE;
    tTeCHopListInfo    *pTeCHopListInfo = NULL;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCreateCHopListInfo : ENTRY \n");

    for (u4CHopListIndex = TE_ONE;
         u4CHopListIndex <= gTeGblInfo.TeParams.u4MaxCHopList;
         u4CHopListIndex++)
    {
        if (gTeGblInfo.pCHopListEntry[u4CHopListIndex - TE_ONE].u4CHopListIndex
            == TE_ZERO)
        {
            pTeCHopListInfo
                = &(gTeGblInfo.pCHopListEntry[u4CHopListIndex - TE_ONE]);
            break;
        }
    }

    if (pTeCHopListInfo == NULL)
    {
        /* All the CHopList structures in the global info are reserved */
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCreateCHopListInfo : INTMD_EXIT \n");
        return TE_FAILURE;
    }

    MEMSET (pTeCHopListInfo, TE_ZERO, sizeof (tTeCHopListInfo));

    /* Assigning the CHop List Index */
    gTeGblInfo.pCHopListEntry[u4CHopListIndex - TE_ONE].u4CHopListIndex
        = u4CHopListIndex;
    TMO_SLL_Init (&pTeCHopListInfo->CHopList);
    *ppTeCHopListInfo = pTeCHopListInfo;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCreateCHopListInfo : EXIT \n");
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeDeleteCHopListInfo
 * Description   : This routine releases the PathListInfo and all the
 *                    associated  CHopInfo's
 * Input(s)      : pTeCHopListInfo - Pointer to tTeCHopListInfo
 * Output(s)     : NONE
 * Return(s)     : TE_SUCCESS / TE_FAILURE
 ******************************************************************************/
INT4
TeDeleteCHopListInfo (tTeCHopListInfo * pCHopListInfo)
{
    tTeCHopInfo        *pTempTeCHopInfo = NULL;
    UINT4               u4CHopListIndex = TE_ZERO;
    tTeSll             *pTeCHopList = NULL;

    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeDeleteCHopListInfo : ENTRY \n");

    if (pCHopListInfo == NULL)
    {
        TE_DBG (TE_EXTN_FAIL,
                "EXTN : Null Pointer provided for CHopListInfo \n");
        return TE_FAILURE;
    }

    if ((pCHopListInfo->u4CHopListIndex < TE_TNL_CHOPLSTINDEX_MINVAL) ||
        (pCHopListInfo->u4CHopListIndex > gTeGblInfo.TeParams.u4MaxCHopList))
    {
        TE_DBG (TE_EXTN_FAIL, "EXTN : Invalid CHopList Index provided \n");
        return TE_FAILURE;
    }

    u4CHopListIndex = pCHopListInfo->u4CHopListIndex - TE_ONE;
    pTeCHopList = &(gTeGblInfo.pCHopListEntry[u4CHopListIndex].CHopList);

    /*Check if the given TeArHopInfo is there in the TeArHopList */

    pTempTeCHopInfo = (tTeCHopInfo *) TMO_SLL_First (pTeCHopList);
    while (pTempTeCHopInfo != NULL)
    {
        TMO_SLL_Delete (pTeCHopList, &pTempTeCHopInfo->NextHop);
        MEMSET (pTempTeCHopInfo, TE_ZERO, sizeof (tTeCHopInfo));

        TE_REL_MEM_BLOCK (TE_CHOP_POOL_ID, (UINT1 *) pTempTeCHopInfo);
        pTempTeCHopInfo = (tTeCHopInfo *) TMO_SLL_First (pTeCHopList);
    }

    MEMSET (pCHopListInfo, TE_ZERO, sizeof (tTeCHopListInfo));

    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeDeleteCHopListInfo : EXIT \n");

    return TE_SUCCESS;
}

/*****************************************************************************/
 /* Function Name : TeCreateCHop
  * Description   : This routine creates a computed hop 
  * Input(s)      : u4RemoteId       - Remote Unnumbered Identifier value
  *                 u4NextHopIp      - Next Hop IP
  *                 u4RouterId       - Remote Router ID
  *                 u1HopType        - Hop Type
  * Output(s)     : ppTeCHopInfo     - Pointer to computed hop
  * Return(s)     : TE_SUCCESS / TE_FAILURE
  ******************************************************************************/
INT4
TeCreateCHop (UINT4 u4RemoteId, UINT4 u4NextHopIp, UINT4 u4RouterId,
              UINT1 u1HopType, tTeCHopInfo ** ppTeCHopInfo)
{
    tTeCHopInfo        *pTeCHopInfo = NULL;

    pTeCHopInfo = (tTeCHopInfo *) TE_ALLOC_MEM_BLOCK (TE_CHOP_POOL_ID);
    if (pTeCHopInfo == NULL)
    {
        return TE_FAILURE;
    }

    pTeCHopInfo->u1CHopType = u1HopType;
    pTeCHopInfo->GmplsTnlCHopInfo.u4ForwardLbl = MPLS_INVALID_LABEL;
    pTeCHopInfo->GmplsTnlCHopInfo.u4ReverseLbl = MPLS_INVALID_LABEL;

    /* numbered computed hop insertion */
    if (u4RemoteId == TE_ZERO)
    {
        pTeCHopInfo->u1AddressType = TE_ERHOP_IPV4_TYPE;
        pTeCHopInfo->u1AddrPrefixLen = TE_ERHOP_IPV4_PREFIX_LEN;
        u4NextHopIp = OSIX_HTONL (u4NextHopIp);
        MEMCPY (&pTeCHopInfo->IpAddr, &u4NextHopIp, sizeof (UINT4));
        u4RouterId = OSIX_HTONL (u4RouterId);
        MEMCPY (&pTeCHopInfo->RouterId, &u4RouterId, sizeof (UINT4));
    }
    /* unnumbered computed hop insertion */
    else
    {
        pTeCHopInfo->u1AddressType = TE_ERHOP_UNNUM_TYPE;
        pTeCHopInfo->u1AddrPrefixLen = TE_ZERO;
        pTeCHopInfo->u4UnnumIf = u4RemoteId;
        u4RouterId = OSIX_HTONL (u4RouterId);
        MEMCPY (&pTeCHopInfo->RouterId, &u4RouterId, sizeof (UINT4));
        MEMCPY (&pTeCHopInfo->IpAddr, &u4RouterId, sizeof (UINT4));
    }

    TeIsLsrPartOfCHop (pTeCHopInfo, &pTeCHopInfo->u1LsrPartOfCHop);

    *ppTeCHopInfo = pTeCHopInfo;
    return TE_SUCCESS;
}

/*****************************************************************************/
 /* Function Name : TeGetCHop
  * Description   : This routine finds the computed hop 
  * Input(s)      : u4HopListIndex   - Hop List Index
  *                 u4HopIndex       - Hop Index
  * Output(s)     : None
  * Return(s)     : pTeCHopInfo (Pointer to computed hop) / NULL
  ******************************************************************************/
tTeCHopInfo        *
TeGetCHop (UINT4 u4HopListIndex, UINT4 u4HopIndex)
{
    tTMO_SLL           *pCHopList = NULL;
    tTeCHopInfo        *pTeCHopInfo = NULL;

    if (gTeGblInfo.pCHopListEntry[u4HopListIndex - TE_ONE].u4CHopListIndex
        != u4HopListIndex)
    {
        return NULL;
    }

    pCHopList = &(gTeGblInfo.pCHopListEntry[u4HopListIndex - TE_ONE].CHopList);

    TMO_SLL_Scan (pCHopList, pTeCHopInfo, tTeCHopInfo *)
    {
        if (pTeCHopInfo->u4TnlCHopIndex == u4HopIndex)
        {
            break;
        }
    }

    return pTeCHopInfo;
}

/*****************************************************************************/
 /* Function Name : TeCreateCHopsForCspfPaths                                    
  * Description   : This routine populates the computed Hop List with set of
  *                 hops given by CSPF.
  * Input(s)      : pTeTnlInfo       - Pointer to the Tunnel Info.
  *                 pCspfPath        - Pointer to the CSPF Path.  *
  * Output(s)     : ppTeCHopListInfo - Computed Hop List with set of hops
  * Return(s)     : TE_SUCCESS / TE_FAILURE
  ******************************************************************************/
INT4
TeCreateCHopsForCspfPaths (tTeTnlInfo * pTeTnlInfo,
                           tOsTeAppPath * pCspfPath,
                           tTeCHopListInfo ** ppTeCHopListInfo)
{
    tTeCHopInfo        *pTeCHopInfo = NULL;
    tTeCHopInfo        *pTePrevCHopInfo = NULL;
    tTeCHopListInfo    *pTeCHopListInfo = NULL;
    tTMO_SLL           *pCHopList = NULL;
    tTeHopInfo         *pTeHopInfo = NULL;
    UINT1               u1PathCnt = TE_ZERO;
    UINT4               u4TempAddr = TE_ZERO;
    UINT4               u4RouterId = TE_ZERO;
    UINT4               u4TempTeCHopIndx = TE_ONE;
    tTePathInfo        *pTePathInfo = NULL;
    BOOL1               bERHopsToBeCopied = FALSE;
    BOOL1               bStartCopying = FALSE;

    if (pTeTnlInfo->u1TnlPathType == TE_TNL_PROTECTION_PATH)
    {
        if ((pTeTnlInfo->pTeBackupPathInfo == NULL) &&
            (pTeTnlInfo->u1TnlRole == TE_INTERMEDIATE))
        {
            pTeTnlInfo->pTeBackupPathInfo = pTeTnlInfo->pTePathInfo;
        }
        pTePathInfo = pTeTnlInfo->pTeBackupPathInfo;
    }
    else
    {
        pTePathInfo = pTeTnlInfo->pTePathInfo;
    }

    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeCreateCHopsForCspfPaths: ENTRY \n");

    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        TE_DBG (TE_EXTN_FAIL,
                "EXTN : TE Admin Status DOWN. No Further processing \n");
        return TE_FAILURE;
    }

    if ((ppTeCHopListInfo == NULL) || (pCspfPath == NULL))
    {
        TE_DBG (TE_EXTN_FAIL, "EXTN : Null Pointer provided in ErHopList \n");
        return TE_FAILURE;
    }

    pTeCHopListInfo = *ppTeCHopListInfo;
    if ((pTeCHopListInfo == NULL) || (pCspfPath->u2HopCount == TE_ZERO))
    {
        TE_DBG (TE_EXTN_FAIL, "EXTN : Null Pointer provided in cHopList \n");
        return TE_FAILURE;
    }

    pCHopList = &(gTeGblInfo.pCHopListEntry
                  [pTeCHopListInfo->u4CHopListIndex - TE_ONE].CHopList);

    /* Insert the computed hops into the computed hop list */
    for (u1PathCnt = TE_ZERO; u1PathCnt < pCspfPath->u2HopCount; u1PathCnt++)
    {
        if (TeCreateCHop (pCspfPath->aNextHops[u1PathCnt].u4RemoteIdentifier,
                          pCspfPath->aNextHops[u1PathCnt].u4NextHopIpAddr,
                          pCspfPath->aNextHops[u1PathCnt].u4RouterId,
                          TE_STRICT_ER, &pTeCHopInfo) == TE_FAILURE)
        {
            TE_DBG (TE_EXTN_FAIL, "EXTN : TeCHopInfo allocation Failed\n");

            TeDeleteCHopListInfo (pTeCHopListInfo);
            return TE_FAILURE;
        }

        /* Filling Label information into the computed hop
         * from Explcit route hop in case of Explicit label control */
        if ((pTePathInfo != NULL) && (TE_PO_HOP_LIST (pTePathInfo)))
        {
            TMO_SLL_Scan ((TE_PO_HOP_LIST (pTePathInfo)), pTeHopInfo,
                          tTeHopInfo *)
            {
                /* Get the ER hop one by one and check the address of 
                 * Er hop with current C-Hop
                 * If it is equal then fill the gmpls c-hop information*/
                CONVERT_TO_INTEGER (pTeHopInfo->IpAddr.au1Ipv4Addr, u4TempAddr);

                if (u4TempAddr == pCspfPath->aNextHops[u1PathCnt].u4RouterId)
                {
                    pTeCHopInfo->GmplsTnlCHopInfo.u1LblStatus =
                        pTeHopInfo->GmplsTnlHopInfo.u1LblStatus;
                    pTeCHopInfo->GmplsTnlCHopInfo.u4ForwardLbl =
                        pTeHopInfo->GmplsTnlHopInfo.u4ForwardLbl;
                    pTeCHopInfo->GmplsTnlCHopInfo.u4ReverseLbl =
                        pTeHopInfo->GmplsTnlHopInfo.u4ReverseLbl;

                }
            }
        }
        /* Inserting c-hop into c-hop list */
        TMO_SLL_Init_Node (&pTeCHopInfo->NextHop);
        if (u1PathCnt == TE_ZERO)
        {
            TMO_SLL_Insert (pCHopList, NULL, &pTeCHopInfo->NextHop);
        }
        else
        {
            TMO_SLL_Insert (pCHopList, &pTePrevCHopInfo->NextHop,
                            &pTeCHopInfo->NextHop);
        }
        pTeCHopInfo->u4TnlCHopIndex = u4TempTeCHopIndx++;
        pTePrevCHopInfo = pTeCHopInfo;
    }

    if ((pTePathInfo == NULL) || (TE_PO_HOP_LIST (pTePathInfo) == NULL))
    {
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeCreateCHopsForCspfPaths: EXIT \n");

        return TE_SUCCESS;
    }
    /* This is for Loose-Hop scenario.
     * In this case, the computed hop list will not have 
     * the whole explicit route hops. it will have only some 
     * hops (last hop of the computed hop list is first loose hop configured).
     * The remaining hops in the explicit route hop list
     * should be copied into the computed hop list */

    TE_SLL_SCAN (TE_PO_HOP_LIST (pTePathInfo), pTeHopInfo, tTeHopInfo *)
    {
        /* the ip address of last computed hop is compared with 
         * explict route hop. If it is equal, then the flag is set and the 
         * next explicit route hop is copied into the computed hop list */
        CONVERT_TO_INTEGER (pTeHopInfo->IpAddr.au1Ipv4Addr, u4TempAddr);

        u4TempAddr = OSIX_HTONL (u4TempAddr);
        if (u4TempAddr == pCspfPath->aNextHops[u1PathCnt - 1].u4RouterId)
        {
            bERHopsToBeCopied = TRUE;
        }

        if (bERHopsToBeCopied == FALSE)
        {
            continue;
        }

        if (bStartCopying == FALSE)
        {
            bStartCopying = TRUE;
            continue;
        }

        CONVERT_TO_INTEGER (pTeHopInfo->IpAddr.au1Ipv4Addr, u4TempAddr);
        CONVERT_TO_INTEGER (pTeHopInfo->IpAddr.au1Ipv4Addr, u4RouterId);
        u4TempAddr = OSIX_HTONL (u4TempAddr);
        u4RouterId = OSIX_HTONL (u4RouterId);
        TE_DBG4 (TE_EXTN_ETEXT,
                 "tnl: %u %u u4TempAddr: 0x%x u4RouterId: 0x%x\n",
                 pTeTnlInfo->u4TnlIndex, pTeTnlInfo->u4TnlInstance, u4TempAddr,
                 u4RouterId);

        if (TeCreateCHop (pTeHopInfo->u4UnnumIf, u4TempAddr, u4RouterId,
                          pTeHopInfo->u1HopType, &pTeCHopInfo) == TE_FAILURE)
        {
            TeDeleteCHopListInfo (pTeCHopListInfo);
            return TE_FAILURE;
        }

        pTeCHopInfo->GmplsTnlCHopInfo.u1LblStatus =
            pTeHopInfo->GmplsTnlHopInfo.u1LblStatus;
        pTeCHopInfo->GmplsTnlCHopInfo.u4ForwardLbl =
            pTeHopInfo->GmplsTnlHopInfo.u4ForwardLbl;
        pTeCHopInfo->GmplsTnlCHopInfo.u4ReverseLbl =
            pTeHopInfo->GmplsTnlHopInfo.u4ReverseLbl;

        TMO_SLL_Insert (pCHopList, &pTePrevCHopInfo->NextHop,
                        &pTeCHopInfo->NextHop);
        pTeCHopInfo->u4TnlCHopIndex = u4TempTeCHopIndx++;
        pTePrevCHopInfo = pTeCHopInfo;
    }

    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeCreateCHopsForCspfPaths: EXIT \n");

    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeGetFreeAttrListIndex
 * Description   : This routine returns the next available attribute list
 *                 index
 * Input(s)      : None
 * Output(s)     : None
 * Return(s)     : TE_ZERO or Valid Attribute List Index
 ******************************************************************************/
UINT4
TeGetFreeAttrListIndex ()
{
    UINT4               u4AttrListIndex = TE_ZERO;

    for (u4AttrListIndex = TE_ONE;
         u4AttrListIndex <= gTeGblInfo.TeParams.u4MaxAttrList;
         u4AttrListIndex++)
    {
        if (gTeGblInfo.pTeAttrListInfo[u4AttrListIndex - TE_ONE].u4ListIndex
            == TE_ZERO)
        {
            return u4AttrListIndex;
        }
    }

    return TE_ZERO;
}

/*****************************************************************************/
/* Function Name : TeGetNextAttrListIndex
 * Description   : This routine returns the next attribute list index
 * Input(s)      : u4CurAttrListIndex - Current Attribute List Index
 * Output(s)     : None
 * Return(s)     : TE_ZERO or Next Attribute List Index
 ******************************************************************************/
UINT4
TeGetNextAttrListIndex (UINT4 u4CurAttrListIndex)
{
    UINT4               u4AttrListIndex = TE_ZERO;

    for (u4AttrListIndex = (u4CurAttrListIndex + TE_ONE);
         u4AttrListIndex <= gTeGblInfo.TeParams.u4MaxAttrList;
         u4AttrListIndex++)
    {
        if (gTeGblInfo.pTeAttrListInfo[u4AttrListIndex - TE_ONE].u4ListIndex
            != TE_ZERO)
        {
            return u4AttrListIndex;
        }
    }

    return TE_ZERO;
}

/*****************************************************************************/
/* Function Name : TeCreateAttrListInfo
 * Description   : This routine reserves a tTeAttrListInfo structure from
 *                 the global array and a pointer to the structure is
 *                 returned.
 *                 If all the tTeAttrListInfo in the global array are
 *                 reserved then the routine returns failure.
 * Input(s)      : NONE
 * Output(s)     : ppTeAttrListInfo - Pointer to pointer of tTeAttrListInfo
 * Return(s)     : TE_SUCCESS / TE_FAILURE
 ******************************************************************************/
INT4
TeCreateAttrListInfo (tTeAttrListInfo ** ppTeAttrListInfo,
                      UINT4 u4AttrListIndex)
{
    tTeAttrListInfo    *pTeAttrListInfo = NULL;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCreateAttrListInfo : ENTRY \n");

    if ((u4AttrListIndex < TE_ONE) ||
        (u4AttrListIndex > gTeGblInfo.TeParams.u4MaxAttrList))
    {
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCreateAttrListInfo : INTMD_EXIT \n");
        return TE_FAILURE;
    }

    pTeAttrListInfo = &(gTeGblInfo.pTeAttrListInfo[u4AttrListIndex - TE_ONE]);

    MEMSET (pTeAttrListInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    /* Assigning the CHop List Index */
    gTeGblInfo.pTeAttrListInfo[u4AttrListIndex - TE_ONE].u4ListIndex
        = u4AttrListIndex;

    TMO_SLL_Init (&pTeAttrListInfo->SrlgList);

    *ppTeAttrListInfo = pTeAttrListInfo;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCreateAttrListInfo : EXIT \n");
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeDeleteAttrListInfo
 * Description   : This routine releases the AttrListInfo
 * Input(s)      : pTeAttrListInfo - Pointer to tTeAttrListInfo
 * Output(s)     : NONE
 * Return(s)     : TE_SUCCESS / TE_FAILURE
 ******************************************************************************/
UINT1
TeDeleteAttrListInfo (tTeAttrListInfo * pTeAttrListInfo)
{
    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeDeleteAttrListInfo : ENTRY \n");

    if (pTeAttrListInfo == NULL)
    {
        TE_DBG (TE_EXTN_FAIL,
                "EXTN : Null Pointer provided for pTeAttrListInfo \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeDeleteAttrListInfo : INTMD-EXIT \n");
        return TE_FAILURE;
    }

    if ((pTeAttrListInfo->u4ListIndex < TE_ONE) ||
        (pTeAttrListInfo->u4ListIndex > gTeGblInfo.TeParams.u4MaxAttrList))
    {
        TE_DBG (TE_EXTN_FAIL, "EXTN : Invalid attr index Index provided \n");
        TE_DBG (TE_EXTN_ETEXT, "EXTN : TeDeleteAttrListInfo : INTMD-EXIT \n");
        return TE_FAILURE;
    }

    MEMSET (pTeAttrListInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    TE_DBG (TE_EXTN_PRCS, "EXTN : pTeAttrListInfo removed \n");
    TE_DBG (TE_EXTN_ETEXT, "EXTN : TeDeleteAttrListInfo : EXIT \n");
    return TE_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeGetAttrListInfo                            
 * Description   : This routine finds the presence of Attribute List Index
 *                 and returns corresponding Attribute List Info
 * Input(s)      : u4AttrListIndex   - Traffic Param Index                   
 * Output(s)     : None
 * Return(s)     : pTeAttrListInfo or Null
 * **************************************************************************/
tTeAttrListInfo    *
TeGetAttrListInfo (UINT4 u4AttrListIndex)
{
    tTeAttrListInfo    *pTeAttrListInfo = NULL;

    if ((u4AttrListIndex != TE_ZERO) &&
        (gTeGblInfo.pTeAttrListInfo[u4AttrListIndex - TE_ONE].u4ListIndex
         == u4AttrListIndex))
    {
        pTeAttrListInfo
            = &(gTeGblInfo.pTeAttrListInfo[u4AttrListIndex - TE_ONE]);
    }

    return pTeAttrListInfo;
}

/*****************************************************************************
 * Function Name : TeGetAttrListIndexFromName
 * Description   : This routine gets the attribute list index from Attibute
 *                 List Name
 * Input(s)      : pInTeAttrListInfo - Pointer to Attribute List Info containing
 *                                     name as key
 * Output(s)     : pInTeAttrListInfo - Pointer to Attribute List Info containing
 *                                     List Index as output
 * Return(s)     : TE_FAILURE or TE_SUCCESS
 * **************************************************************************/
INT4
TeGetAttrListIndexFromName (tTeAttrListInfo * pInTeAttrListInfo)
{
    UINT4               u4AttrListIndex = TE_ONE;

    for (u4AttrListIndex = TE_ONE;
         u4AttrListIndex <= gTeGblInfo.TeParams.u4MaxAttrList;
         u4AttrListIndex++)
    {
        if (pInTeAttrListInfo->i4ListNameLen !=
            gTeGblInfo.pTeAttrListInfo[u4AttrListIndex - TE_ONE].i4ListNameLen)
        {
            continue;
        }

        if (STRCMP (pInTeAttrListInfo->au1ListName,
                    gTeGblInfo.pTeAttrListInfo[u4AttrListIndex - TE_ONE].
                    au1ListName) != TE_ZERO)
        {
            continue;
        }

        break;
    }

    if (u4AttrListIndex > gTeGblInfo.TeParams.u4MaxAttrList)
    {
        return TE_FAILURE;
    }

    pInTeAttrListInfo->u4ListIndex = u4AttrListIndex;

    return TE_SUCCESS;
}

/*****************************************************************************/
 /* Function Name : TeSetAttributeListMask
  * Description   : This routine sets the attribute list mask.
  * Input(s)      : u4AttrListIndex  - Attribute List Index
  *                 u4BitMask        - Bit Mask value.
  * Output(s)     : None
  * Return(s)     : TE_SUCCESS / TE_FAILURE
  ******************************************************************************/
VOID
TeSetAttributeListMask (UINT4 u4AttrListIndex, UINT4 u4BitMask)
{
    tTeAttrListInfo    *pTeAttrListInfo = NULL;

    pTeAttrListInfo = TeGetAttrListInfo (u4AttrListIndex);

    if (pTeAttrListInfo != NULL)
    {
        pTeAttrListInfo->u4ListBitmask |= u4BitMask;
    }

    return;
}

/*****************************************************************************/
 /* Function Name : TeResetAttributeListMask
  * Description   : This routine resets the attribute list mask.
  * Input(s)      : u4AttrListIndex  - Attribute List Index
  *                 u4BitMask        - Bit Mask value.
  * Output(s)     : None
  * Return(s)     : TE_SUCCESS / TE_FAILURE
  ******************************************************************************/
VOID
TeResetAttributeListMask (UINT4 u4AttrListIndex, UINT4 u4BitMask)
{
    tTeAttrListInfo    *pTeAttrListInfo = NULL;

    pTeAttrListInfo = TeGetAttrListInfo (u4AttrListIndex);

    if (pTeAttrListInfo != NULL)
    {
        pTeAttrListInfo->u4ListBitmask &= ~(u4BitMask);
    }

    return;
}

/*****************************************************************************/
 /* Function Name : TeGetAttributeListMask
  * Description   : This routine returns the attribute list mask.
  * Input(s)      : u4AttrListIndex  - Attribute List Index
  * Output(s)     : None
  * Return(s)     : u4ListBitMask    - Attribute List Mask
  ******************************************************************************/
UINT4
TeGetAttributeListMask (UINT4 u4AttrListIndex)
{
    tTeAttrListInfo    *pTeAttrListInfo = NULL;
    UINT4               u4ListBitMask = TE_ZERO;

    pTeAttrListInfo = TeGetAttrListInfo (u4AttrListIndex);

    if (pTeAttrListInfo != NULL)
    {
        u4ListBitMask = pTeAttrListInfo->u4ListBitmask;
    }

    return u4ListBitMask;
}

/******************************************************************************
 Function Name  : TeAddSrlgToSortTnlSrlgList
 Description    : This routine is used to add Srlg to the tunnel Srlg list.
 Input          : pInTnlSrlg   - Pointer to Tunnel Srlg.
                : pTeTnlInfo   - Pointer to tunnel information.
 Output         : None
 Returns        : None
****************************************************************************/
VOID
TeAddSrlgToSortTnlSrlgList (tTeTnlSrlg * pInTnlSrlg, tTeTnlInfo * pTeTnlInfo)
{
    tTeTnlSrlg         *pTnlSrlg = NULL;
    tTMO_SLL_NODE      *pPrevNode = NULL;

    TMO_SLL_Init_Node (&pInTnlSrlg->NextSrlgNode);

    TMO_SLL_Scan (&pTeTnlInfo->SrlgList, pTnlSrlg, tTeTnlSrlg *)
    {
        if (pInTnlSrlg->u4SrlgNo < pTnlSrlg->u4SrlgNo)
        {
            break;
        }

        pPrevNode = &pTnlSrlg->NextSrlgNode;
    }

    TMO_SLL_Insert (&pTeTnlInfo->SrlgList, pPrevNode,
                    &pInTnlSrlg->NextSrlgNode);

    TE_DBG1 (TE_MAIN_PRCS,
             "TeAddSrlgToSortTnlSrlgList: SRLG No: %d added in "
             "Tunnel SRLG List\n", pInTnlSrlg->u4SrlgNo);

    return;
}

/******************************************************************************
 Function Name  : TeRemoveSrlgFromSortTnlSrlgList
 Description    : This routine is used to remove Srlg from tunnel Srlg list.
 Input          : pTnlSrlg   - Pointer to Tunnel Srlg.
                : pTeTnlInfo - Pointer to tunnel information.
 Output         : None
 Returns        : None
****************************************************************************/
VOID
TeRemoveSrlgFromSortTnlSrlgList (tTeTnlSrlg * pTnlSrlg, tTeTnlInfo * pTeTnlInfo)
{
    TMO_SLL_Delete (&pTeTnlInfo->SrlgList, &pTnlSrlg->NextSrlgNode);

    return;
}

/******************************************************************************
 Function Name  : TeDeleteSrlgFromTnl
 Description    : This routine scans the SRLG list of tunnel and removes each
                  one of them.
 Input          : pTeTnlInfo    - Pointer to tunnel information.
                  pInstance0Tnl - Pointer to Instance 0 tunnel.
 Output         : None
 Returns        : None
****************************************************************************/
VOID
TeDeleteSrlgFromTnl (tTeTnlInfo * pTeTnlInfo, tTeTnlInfo * pInstance0Tnl)
{
    tTeTnlSrlg         *pTnlSrlg = NULL;
    tTeTnlSrlg         *pTempTnlSrlg = NULL;

    if (pInstance0Tnl != NULL)
    {
        return;
    }

    TMO_DYN_SLL_Scan (&(pTeTnlInfo->SrlgList), pTnlSrlg, pTempTnlSrlg,
                      tTeTnlSrlg *)
    {
        TeRemoveSrlgFromSortTnlSrlgList (pTnlSrlg, pTeTnlInfo);

        MEMSET (pTnlSrlg, TE_ZERO, sizeof (tTeTnlSrlg));

        TE_REL_MEM_BLOCK (TE_TNL_SRLG_POOL_ID, (UINT1 *) pTnlSrlg);
    }

    return;
}

/******************************************************************************
 Function Name  : TeGetSrlgFromTnlSrlgList
 Description    : This routine is used to find the SRLG Info from Tunnel SRLG
                  List
 Input          : u4SrlgNo  - Srlg value
                : pTeTnlInfo     - Pointer to tunnel information.
 Output         : None
 Returns        : Pointer to the Srlg (pTnlSrlg) / NULL
****************************************************************************/
tTeTnlSrlg         *
TeGetSrlgFromTnlSrlgList (UINT4 u4SrlgNo, tTeTnlInfo * pTeTnlInfo)
{
    tTeTnlSrlg         *pTnlSrlg = NULL;

    TMO_SLL_Scan (&pTeTnlInfo->SrlgList, pTnlSrlg, tTeTnlSrlg *)
    {
        if (pTnlSrlg->u4SrlgNo == u4SrlgNo)
        {
            break;
        }
    }

    return (pTnlSrlg);
}

/******************************************************************************
 Function Name  : TeAddSrlgToSortAttrSrlgList
 Description    : This routine is used to add Srlg to the Attribute Srlg list.
 Input          : pInAttrSrlg       - Pointer to Attribute SRLG Info
                : pTeAttrListInfo   - Pointer to Attribute List Info
 Output         : None
 Returns        : None
****************************************************************************/
VOID
TeAddSrlgToSortAttrSrlgList (tTeAttrSrlg * pInAttrSrlg,
                             tTeAttrListInfo * pTeAttrListInfo)
{
    tTeAttrSrlg        *pAttrSrlg = NULL;
    tTMO_SLL_NODE      *pPrevNode = NULL;

    TMO_SLL_Init_Node (&pInAttrSrlg->NextSrlgNode);

    TMO_SLL_Scan (&pTeAttrListInfo->SrlgList, pAttrSrlg, tTeAttrSrlg *)
    {
        if (pInAttrSrlg->u4SrlgNo < pAttrSrlg->u4SrlgNo)
        {
            break;
        }

        pPrevNode = &pAttrSrlg->NextSrlgNode;
    }

    TMO_SLL_Insert (&pTeAttrListInfo->SrlgList, pPrevNode,
                    &pInAttrSrlg->NextSrlgNode);

    TE_DBG1 (TE_MAIN_PRCS,
             "TeAddSrlgToSortAttrSrlgList: SRLG No: %d added in "
             "Attribute SRLG List\n", pInAttrSrlg->u4SrlgNo);

    return;
}

/******************************************************************************
 Function Name  : TeRemoveSrlgFromSortAttrSrlgList
 Description    : This routine is used to remove Srlg from Attribute Srlg list.
 Input          : pTeAttrSrlg     - Pointer to Attribute Srlg.
                : pTeAttrListInfo - Pointer to Attribute List Info.
 Output         : None
 Returns        : None
****************************************************************************/
VOID
TeRemoveSrlgFromSortAttrSrlgList (tTeAttrSrlg * pTeAttrSrlg,
                                  tTeAttrListInfo * pTeAttrListInfo)
{
    TMO_SLL_Delete (&pTeAttrListInfo->SrlgList, &pTeAttrSrlg->NextSrlgNode);

    return;
}

/******************************************************************************
 Function Name  : TeGetSrlgFromAttrSrlgList
 Description    : This routine is used to find the SRLG Number from Attribute
                  SRLG List
 Input          : u4SrlgNo         - Srlg value
                : pTeAttrListInfo  - Pointer to Attribute List Info
 Output         : None
 Returns        : Pointer to the Srlg (pAttrSrlg) / NULL
****************************************************************************/
tTeAttrSrlg        *
TeGetSrlgFromAttrSrlgList (UINT4 u4SrlgNo, tTeAttrListInfo * pTeAttrListInfo)
{
    tTeAttrSrlg        *pAttrSrlg = NULL;

    TMO_SLL_Scan (&pTeAttrListInfo->SrlgList, pAttrSrlg, tTeAttrSrlg *)
    {
        if (pAttrSrlg->u4SrlgNo == u4SrlgNo)
        {
            break;
        }
    }

    return (pAttrSrlg);
}

/******************************************************************************
 Function Name  : TeGetAttrFromTnlOrAttrList
 Description    : This routine is used to get the attributes for Tunnel of 
                  Attribute list
 Input          : pTeTnlInfo        - Pointer to Te-tnl Info
 Output         : pTeGetAttributes  - Pointer to the attributes info
 Returns        : None
****************************************************************************/
VOID
TeGetAttrFromTnlOrAttrList (tTeGetAttributes * pTeGetAttributes,
                            tTeTnlInfo * pTeTnlInfo)
{
    tTeAttrListInfo    *pTeAttrListInfo = NULL;
    UINT4               u4ListBitMask = TE_ZERO;

    pTeGetAttributes->u1SetupPriority = pTeTnlInfo->u1TnlSetPrio;
    pTeGetAttributes->u1HoldingPriority = pTeTnlInfo->u1TnlHoldPrio;
    pTeGetAttributes->u1SsnAttr = pTeTnlInfo->u1TnlSsnAttr;
    if (pTeTnlInfo->pMplsDiffServTnlInfo != NULL)
    {
        pTeGetAttributes->u4ClassType =
            (UINT4) pTeTnlInfo->pMplsDiffServTnlInfo->u1ClassType;
    }
    pTeGetAttributes->u4IncludeAnyAffinity =
        pTeTnlInfo->u4TnlIncludeAnyAffinity;
    pTeGetAttributes->u4IncludeAllAffinity =
        pTeTnlInfo->u4TnlIncludeAllAffinity;
    pTeGetAttributes->u4ExcludeAnyAffinity =
        pTeTnlInfo->u4TnlExcludeAnyAffinity;
    if (pTeTnlInfo->pTeTrfcParams != NULL)
    {
        if (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_RSVP)
        {
            pTeGetAttributes->u4Bandwidth = pTeTnlInfo->pTeTrfcParams->
                pRSVPTrfcParams->u4PeakDataRate;
        }
        else if (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_LDP)
        {
            pTeGetAttributes->u4Bandwidth = pTeTnlInfo->pTeTrfcParams->
                pCRLDPTrfcParams->u4PeakDataRate;
        }
        else if (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_NONE)
        {
            pTeGetAttributes->u4Bandwidth = pTeTnlInfo->pTeTrfcParams->
                u4ResMaxRate;
        }

    }
    pTeGetAttributes->u1SrlgType = pTeTnlInfo->u1TnlSrlgType;

    pTeAttrListInfo = pTeTnlInfo->pTeAttrListInfo;

    if (pTeAttrListInfo == NULL)
    {
        return;
    }
    u4ListBitMask = pTeAttrListInfo->u4ListBitmask;

    if (u4ListBitMask & TE_ATTR_SETUPPRI_BITMASK)
    {
        pTeGetAttributes->u1SetupPriority = pTeAttrListInfo->u1SetupPriority;
    }
    if (u4ListBitMask & TE_ATTR_HOLDPRI_BITMASK)
    {
        pTeGetAttributes->u1HoldingPriority
            = pTeAttrListInfo->u1HoldingPriority;
    }
    if (u4ListBitMask & TE_ATTR_LSP_SSN_ATTR_BITMASK)
    {
        pTeGetAttributes->u1SsnAttr = pTeAttrListInfo->u1SsnAttr;
    }
    if (u4ListBitMask & TE_ATTR_INCLUDE_ANY_AFFINITY_BITMASK)
    {
        pTeGetAttributes->u4IncludeAnyAffinity
            = pTeAttrListInfo->u4IncludeAnyAffinity;
    }
    if (u4ListBitMask & TE_ATTR_INCLUDE_ALL_AFFINITY_BITMASK)
    {
        pTeGetAttributes->u4IncludeAllAffinity
            = pTeAttrListInfo->u4IncludeAllAffinity;
    }
    if (u4ListBitMask & TE_ATTR_EXCLUDE_ANY_AFFINITY_BITMASK)
    {
        pTeGetAttributes->u4ExcludeAnyAffinity
            = pTeAttrListInfo->u4ExcludeAnyAffinity;
    }
    if (u4ListBitMask & TE_ATTR_BANDWIDTH_BITMASK)
    {
        pTeGetAttributes->u4Bandwidth = pTeAttrListInfo->u4Bandwidth;
    }
    if (u4ListBitMask & TE_ATTR_SRLG_TYPE_BITMASK)
    {
        pTeGetAttributes->u1SrlgType = pTeAttrListInfo->u1SrlgType;
    }
    if (u4ListBitMask & TE_ATTR_CLASS_TYPE_BITMASK)
    {
        pTeGetAttributes->u4ClassType = pTeAttrListInfo->u4ClassType;
    }
    return;

}

/************************************************************************
 *  Function Name   : TeMainIsCspfComputationReqd
 *  Description     : Function used to find whether CSPF is required or
 *                    not
 *  Input           : pTeTnlInfo - pointer to tunnel info
 *  Output          : None
 *  Returns         : bIsCspfReqd - TRUE: CSPF required
 ************************************************************************/
BOOL1
TeMainIsCspfComputationReqd (tTeTnlInfo * pTeTnlInfo)
{
    BOOL1               bIsCspfReqd = TE_FALSE;

    if ((pTeTnlInfo->pTePathInfo != NULL) &&
        (pTeTnlInfo->u4TnlPathInUse != TE_ZERO))
    {
        if (pTeTnlInfo->pTePathInfo->u1PathCompType == TE_DYNAMIC)
        {
            bIsCspfReqd = TE_TRUE;
        }
    }
    else if (pTeTnlInfo->u4TnlPathInUse != TE_ZERO)
    {
        bIsCspfReqd = TE_TRUE;
    }
    if (bIsCspfReqd == TE_TRUE)
    {
        if (TePortIsOspfTeEnabled () == TE_FAILURE)
        {
            bIsCspfReqd = TE_FALSE;
        }
    }
    return bIsCspfReqd;
}

/*---------------------------------------------------------------------------*/
/*
 * Function Name   : TeSendAdminStatusTrapAndSyslog                                                                * Description     : This function sends Admin Status trap                                                         * Input (s)       : pTeTnlInfo - Pointer to TeTnlInfo Structure.
 * Output (s)      : None
 * Returns         : None
 */
/*---------------------------------------------------------------------------*/
VOID
TeSendAdminStatusTrapAndSyslog (tTeTnlInfo * pTeTnlInfo)
{
    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL, *pOidValue = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    UINT4               u4Len = 0, u4TnlIngress = 0, u4TnlEgress = 0;
    UINT4               au4snmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };
    UINT4               au4AdminStatusFlagTrapid[] =
        { 1, 3, 6, 1, 4, 1, 2076, 13, 12, 0, 2 };
    UINT4               au4AdminStatusFlagOid[] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 13, 2, 1, 1, 18 };
    CHR1                ac1TeSysLogMsg[256];
    tTeTnlInfo         *pTempTeTnlInfo = NULL;

    TE_DBG (TE_MAIN_PRCS, "TeSendAdminStatusTrapAndSyslog: ENTRY \n");

    if (gTeGblInfo.TeParams.i4NotificationEnable == MPLS_SNMP_FALSE)
    {
        TE_DBG (TE_MAIN_PRCS, "Send Trap: Notification not enabled \n");
        return;
    }

    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;
    CONVERT_TO_INTEGER ((pTeTnlInfo->TnlIngressLsrId), u4TnlIngress);
    u4TnlIngress = OSIX_NTOHL (u4TnlIngress);
    CONVERT_TO_INTEGER ((pTeTnlInfo->TnlEgressLsrId), u4TnlEgress);
    u4TnlEgress = OSIX_NTOHL (u4TnlEgress);

    pTempTeTnlInfo = TeGetTunnelInfo (TE_TNL_TNL_INDEX (pTeTnlInfo),
                                      TE_TNL_TNL_INSTANCE (pTeTnlInfo),
                                      u4TnlIngress, u4TnlEgress);
    if (pTempTeTnlInfo == NULL)
    {
        TE_DBG (TE_MAIN_PRCS, "EXTN : Tnl is not found in the TnlHashTable \n");
        return;
    }

    MEMSET (ac1TeSysLogMsg, 0, 256);

    SNPRINTF (ac1TeSysLogMsg, 256,
              "Admin Status: Tunnel Index [%d] Tunnel Instance [%d] Tunnel Ingress [%x] Tunnel Egress [%x] Admin-Status Value[0x%x]",
              pTempTeTnlInfo->u4TnlIndex, pTempTeTnlInfo->u4TnlPrimaryInstance,
              u4TnlIngress, u4TnlEgress,
              pTempTeTnlInfo->GmplsTnlInfo.u4AdminStatus);

    /* OID generation for SnmpTrap */
    pOid = alloc_oid (sizeof (au4snmpTrapOid) / sizeof (UINT4));
    if (pOid == NULL)
    {
        TE_DBG (TE_MAIN_SNMP, "Send AdminStatus Trap : Alloc OID Failed \n");
        return;
    }
    MEMCPY (pOid->pu4_OidList, au4snmpTrapOid, sizeof (au4snmpTrapOid));
    pOid->u4_Length = sizeof (au4snmpTrapOid) / sizeof (UINT4);

    /* OID generation for AdminStatusTrap */
    pOidValue = alloc_oid (sizeof (au4AdminStatusFlagTrapid) / sizeof (UINT4));
    if (pOidValue == NULL)
    {
        TE_DBG (TE_MAIN_SNMP, "Send AdminStatus Trap : Alloc OID Failed \n");
        free_oid (pOid);
        return;
    }
    u4Len = sizeof (au4AdminStatusFlagTrapid) / sizeof (UINT4);
    MEMCPY (pOidValue->pu4_OidList, au4AdminStatusFlagTrapid,
            sizeof (au4AdminStatusFlagTrapid));
    pOidValue->u4_Length = u4Len;

    pVbList = ((tSNMP_VAR_BIND *)
               SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OBJECT_ID,
                                     0, 0, NULL, pOidValue, SnmpCnt64Type));
    if (pVbList == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        TE_DBG (TE_MAIN_SNMP,
                "Send AdminStatus Trap : Alloc Var Bind Failed \n");
        return;
    }

    pStartVb = pVbList;

    pOid = alloc_oid (sizeof (au4AdminStatusFlagOid) / sizeof (UINT4));
    if (pOid == NULL)
    {
        TE_DBG (TE_MAIN_SNMP, "Send AdminStatus Trap : Alloc OID Failed \n");
        SNMP_free_snmp_vb_list (pStartVb);
        return;
    }

    u4Len = sizeof (au4AdminStatusFlagOid) / sizeof (UINT4);
    au4AdminStatusFlagOid[u4Len - 4] = pTempTeTnlInfo->u4TnlIndex;
    au4AdminStatusFlagOid[u4Len - 3] = pTempTeTnlInfo->u4TnlInstance;
    au4AdminStatusFlagOid[u4Len - 2] = u4TnlIngress;
    au4AdminStatusFlagOid[u4Len - 1] = u4TnlEgress;
    MEMCPY (pOid->pu4_OidList, au4AdminStatusFlagOid,
            sizeof (au4AdminStatusFlagOid));
    pOid->u4_Length = u4Len;

    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_INTEGER,
                                                  0,
                                                  (INT4) pTempTeTnlInfo->
                                                  GmplsTnlInfo.u4AdminStatus,
                                                  NULL, NULL, SnmpCnt64Type));
    if (pVbList->pNextVarBind == NULL)
    {
        free_oid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        TE_DBG (TE_MAIN_SNMP,
                "Send AdminStatus Trap : Alloc Var Bind Failed \n");
        return;
    }

    pVbList = pVbList->pNextVarBind;
    pVbList->pNextVarBind = NULL;

    MplsPortFmNotifyFaults (pStartVb, ac1TeSysLogMsg);

    TE_DBG (TE_MAIN_PRCS, "TeSendAdminStatusTrapAndSyslog: EXIT \n");

    return;
}

/*****************************************************************************
 * Function Name : TeSendGmplsTnlTrapAndSyslog
 * Description   : This routine sends the Notification Information to the 
 *                 manager and Syslog message to syslog server.
 * Input(s)      : pTeTnlInfo - Pointer to Tunnel Information Structure.
 *                 u1TrapType - Trap Type.
 * Output(s)     : u4TunnelIndex      - Tunnel Identifier
 *                 u1TunnelOperStatus - Operational status of tunnel
 * Return(s)     : TE_SUCCESS/TE_FAILURE
 *****************************************************************************/
INT4
TeSendGmplsTnlTrapAndSyslog (tTeTnlInfo * pTeTnlInfo, UINT1 u1TrapType)
{
    tSNMP_VAR_BIND     *pVbList = NULL, *pStartVb = NULL;
    tSNMP_OID_TYPE     *pOid = NULL, *pOidValue = NULL;
    tSNMP_COUNTER64_TYPE SnmpCnt64Type;
    UINT4               u4Len = 0, u4TnlIngress = 0, u4TnlEgress = 0;
    UINT4               u4LastErrType = 0;
    UINT4               u4ErrorReporterType = 0;
    UINT4               u4ErrorIpAddress = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4SubErrorCode = 0;
    UINT4               au4snmpTrapOid[] = { 1, 3, 6, 1, 6, 3, 1, 1, 4, 1, 0 };

    UINT4               au4TeNotification[] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 3, 0, 0 };
    UINT4               au4TeTnlAdminStatus[] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 3, 2, 2, 1, 34, 0, 0, 0, 0 };
    UINT4               au4TeTnlOperStatus[] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 3, 2, 2, 1, 35, 0, 0, 0, 0 };
    UINT4               au4gmplsTnlErrorLastErrorType[] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 13, 2, 6, 1, 1, 1, 0, 0, 0, 0 };
    UINT4               au4gmplsTnlErrorReporterType[] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 13, 2, 6, 1, 1, 3, 0, 0, 0, 0 };
    UINT4               au4gmplsTnlErrorReporter[] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 13, 2, 6, 1, 1, 4, 0, 0, 0, 0 };
    UINT4               au4gmplsTnlErrorCode[] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 13, 2, 6, 1, 1, 5, 0, 0, 0, 0 };
    UINT4               au4gmplsTnlErrorSubcode[] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 13, 2, 6, 1, 1, 6, 0, 0, 0, 0 };

    CHR1                ac1TeSysLogMsg[MPLS_OBJECT_NAME_LEN];
    CHR1               *pac1Status[] = {
        "Null",
        "Up",
        "Down",
        "Testing",
        "Unknown",
        "Dormant",
        "Not Present",
        "Lower Layer Down"
    };

    CHR1               *pac1SyslogType[] = {
        "Null",
        "TUNNEL UP",
        "TUNNEL DOWN",
        "TUNNEL REROUTED",
        "TUNNEL REOPTIMIZED",
    };

    SnmpCnt64Type.msn = 0;
    SnmpCnt64Type.lsn = 0;
    CONVERT_TO_INTEGER ((pTeTnlInfo->TnlIngressLsrId), u4TnlIngress);
    u4TnlIngress = OSIX_NTOHL (u4TnlIngress);
    CONVERT_TO_INTEGER ((pTeTnlInfo->TnlEgressLsrId), u4TnlEgress);
    u4TnlEgress = OSIX_NTOHL (u4TnlEgress);

    MEMSET (ac1TeSysLogMsg, 0, sizeof (ac1TeSysLogMsg));

    SNPRINTF (ac1TeSysLogMsg, sizeof (ac1TeSysLogMsg),
              "%s: Tunnel %d %d %x %x is Administratively %s and "
              "Operationally %s",
              pac1SyslogType[u1TrapType],
              pTeTnlInfo->u4TnlIndex, pTeTnlInfo->u4TnlPrimaryInstance,
              u4TnlIngress, u4TnlEgress,
              pac1Status[pTeTnlInfo->u1TnlAdminStatus],
              pac1Status[pTeTnlInfo->u1TnlOperStatus]);

    /* Trap OID construction. Telling Manager that you have received
     * trap for mplsTeNotifications.x */
    pOid = alloc_oid (sizeof (au4snmpTrapOid) / sizeof (UINT4));
    if (pOid == NULL)
    {
        TE_DBG (TE_MAIN_SNMP, "Send Trap : Alloc OID Failed \n");
        return TE_FAILURE;
    }
    MEMCPY (pOid->pu4_OidList, au4snmpTrapOid, sizeof (au4snmpTrapOid));
    pOid->u4_Length = sizeof (au4snmpTrapOid) / sizeof (UINT4);

    pOidValue = alloc_oid (sizeof (au4TeNotification) / sizeof (UINT4));
    if (pOidValue == NULL)
    {
        TE_DBG (TE_MAIN_SNMP, "Send Trap : Alloc OID Failed \n");
        free_oid (pOid);
        return TE_FAILURE;
    }
    u4Len = sizeof (au4TeNotification) / sizeof (UINT4);
    au4TeNotification[u4Len - 1] = u1TrapType;
    MEMCPY (pOidValue->pu4_OidList, au4TeNotification,
            sizeof (au4TeNotification));
    pOidValue->u4_Length = u4Len;

    pVbList = ((tSNMP_VAR_BIND *)
               SNMP_AGT_FormVarBind (pOid, SNMP_DATA_TYPE_OBJECT_ID,
                                     0, 0, NULL, pOidValue, SnmpCnt64Type));
    if (pVbList == NULL)
    {
        free_oid (pOid);
        free_oid (pOidValue);
        TE_DBG (TE_MAIN_SNMP, "Send Trap : Alloc Var Bind Failed \n");
        return TE_FAILURE;
    }

    pStartVb = pVbList;

    /* Value of the Trap  -> mplsTunnelAdminStatus */
    pOid = alloc_oid (sizeof (au4TeTnlAdminStatus) / sizeof (UINT4));
    if (pOid == NULL)
    {
        TE_DBG (TE_MAIN_SNMP, "Send Trap : Alloc OID Failed \n");
        SNMP_free_snmp_vb_list (pStartVb);
        return TE_FAILURE;
    }
    u4Len = sizeof (au4TeTnlAdminStatus) / sizeof (UINT4);
    au4TeTnlAdminStatus[u4Len - 4] = pTeTnlInfo->u4TnlIndex;
    au4TeTnlAdminStatus[u4Len - 3] = pTeTnlInfo->u4TnlInstance;
    au4TeTnlAdminStatus[u4Len - 2] = u4TnlIngress;
    au4TeTnlAdminStatus[u4Len - 1] = u4TnlEgress;
    MEMCPY (pOid->pu4_OidList, au4TeTnlAdminStatus,
            sizeof (au4TeTnlAdminStatus));
    pOid->u4_Length = u4Len;

    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_INTEGER,
                                                  0,
                                                  pTeTnlInfo->u1TnlAdminStatus,
                                                  NULL, NULL, SnmpCnt64Type));
    if (pVbList->pNextVarBind == NULL)
    {
        free_oid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        TE_DBG (TE_MAIN_SNMP, "Send Trap : Alloc VarBind Failed \n");
        return TE_FAILURE;
    }

    pVbList = pVbList->pNextVarBind;

    /* Value of the Trap  -> mplsTunnelOperStatus */
    pOid = alloc_oid (sizeof (au4TeTnlOperStatus) / sizeof (UINT4));
    if (pOid == NULL)
    {
        TE_DBG (TE_MAIN_SNMP, "Send Trap : Alloc OID Failed \n");
        SNMP_free_snmp_vb_list (pStartVb);
        return TE_FAILURE;
    }
    u4Len = sizeof (au4TeTnlOperStatus) / sizeof (UINT4);
    au4TeTnlOperStatus[u4Len - 4] = pTeTnlInfo->u4TnlIndex;
    au4TeTnlOperStatus[u4Len - 3] = pTeTnlInfo->u4TnlInstance;
    au4TeTnlOperStatus[u4Len - 2] = u4TnlIngress;
    au4TeTnlOperStatus[u4Len - 1] = u4TnlEgress;
    MEMCPY (pOid->pu4_OidList, au4TeTnlOperStatus, sizeof (au4TeTnlOperStatus));
    pOid->u4_Length = u4Len;

    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_INTEGER,
                                                  0,
                                                  pTeTnlInfo->u1TnlOperStatus,
                                                  NULL, NULL, SnmpCnt64Type));
    if (pVbList->pNextVarBind == NULL)
    {
        free_oid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        TE_DBG (TE_MAIN_SNMP, "Send Trap : Alloc VarBind Failed \n");
        return TE_FAILURE;
    }

    pVbList = pVbList->pNextVarBind;

    /* Value of the Trap  -> gmplsTunnelErrorLastErrorType */
    pOid = alloc_oid (sizeof (au4gmplsTnlErrorLastErrorType) / sizeof (UINT4));
    if (pOid == NULL)
    {
        TE_DBG (TE_MAIN_SNMP, "Send Trap : Alloc OID Failed \n");
        SNMP_free_snmp_vb_list (pStartVb);
        return TE_FAILURE;
    }
    u4Len = sizeof (au4TeTnlOperStatus) / sizeof (UINT4);
    au4gmplsTnlErrorLastErrorType[u4Len - 4] = pTeTnlInfo->u4TnlIndex;
    au4gmplsTnlErrorLastErrorType[u4Len - 3] = pTeTnlInfo->u4TnlInstance;
    au4gmplsTnlErrorLastErrorType[u4Len - 2] = u4TnlIngress;
    au4gmplsTnlErrorLastErrorType[u4Len - 1] = u4TnlEgress;
    MEMCPY (pOid->pu4_OidList, au4gmplsTnlErrorLastErrorType,
            sizeof (au4gmplsTnlErrorLastErrorType));
    pOid->u4_Length = u4Len;

    if (pTeTnlInfo->pTunnelErrInfo != NULL)
    {
        u4LastErrType = pTeTnlInfo->pTunnelErrInfo->LastErrType;
        u4ErrorReporterType = pTeTnlInfo->pTunnelErrInfo->ErrorReporterType;
        u4ErrorIpAddress = pTeTnlInfo->pTunnelErrInfo->u4ErrorIpAddress;
        u4ErrorCode = pTeTnlInfo->pTunnelErrInfo->u4ErrorCode;
        u4SubErrorCode = pTeTnlInfo->pTunnelErrInfo->u4SubErrorCode;
    }

    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_INTEGER,
                                                  0,
                                                  (INT4) u4LastErrType,
                                                  NULL, NULL, SnmpCnt64Type));
    if (pVbList->pNextVarBind == NULL)
    {
        free_oid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        TE_DBG (TE_MAIN_SNMP, "Send Trap : Alloc VarBind Failed \n");
        return TE_FAILURE;
    }

    pVbList = pVbList->pNextVarBind;

    /* Value of the Trap  -> gmplsTunnelErrorReporterType */
    pOid = alloc_oid (sizeof (au4gmplsTnlErrorReporterType) / sizeof (UINT4));
    if (pOid == NULL)
    {
        TE_DBG (TE_MAIN_SNMP, "Send Trap : Alloc OID Failed \n");
        SNMP_free_snmp_vb_list (pStartVb);
        return TE_FAILURE;
    }
    u4Len = sizeof (au4gmplsTnlErrorReporterType) / sizeof (UINT4);
    au4gmplsTnlErrorReporterType[u4Len - 4] = pTeTnlInfo->u4TnlIndex;
    au4gmplsTnlErrorReporterType[u4Len - 3] = pTeTnlInfo->u4TnlInstance;
    au4gmplsTnlErrorReporterType[u4Len - 2] = u4TnlIngress;
    au4gmplsTnlErrorReporterType[u4Len - 1] = u4TnlEgress;
    MEMCPY (pOid->pu4_OidList, au4gmplsTnlErrorReporterType,
            sizeof (au4gmplsTnlErrorReporterType));
    pOid->u4_Length = u4Len;

    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_INTEGER,
                                                  0,
                                                  (INT4) u4ErrorReporterType,
                                                  NULL, NULL, SnmpCnt64Type));
    if (pVbList->pNextVarBind == NULL)
    {
        free_oid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        TE_DBG (TE_MAIN_SNMP, "Send Trap : Alloc VarBind Failed \n");
        return TE_FAILURE;
    }

    pVbList = pVbList->pNextVarBind;

    /* Value of the Trap  -> gmplsTunnelErrorReporter */
    pOid = alloc_oid (sizeof (au4gmplsTnlErrorReporter) / sizeof (UINT4));
    if (pOid == NULL)
    {
        TE_DBG (TE_MAIN_SNMP, "Send Trap : Alloc OID Failed \n");
        SNMP_free_snmp_vb_list (pStartVb);
        return TE_FAILURE;
    }
    u4Len = sizeof (au4gmplsTnlErrorReporter) / sizeof (UINT4);
    au4gmplsTnlErrorReporter[u4Len - 4] = pTeTnlInfo->u4TnlIndex;
    au4gmplsTnlErrorReporter[u4Len - 3] = pTeTnlInfo->u4TnlInstance;
    au4gmplsTnlErrorReporter[u4Len - 2] = u4TnlIngress;
    au4gmplsTnlErrorReporter[u4Len - 1] = u4TnlEgress;
    MEMCPY (pOid->pu4_OidList, au4gmplsTnlErrorReporter,
            sizeof (au4gmplsTnlErrorReporter));
    pOid->u4_Length = u4Len;

    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_INTEGER,
                                                  0,
                                                  (INT4) u4ErrorIpAddress,
                                                  NULL, NULL, SnmpCnt64Type));
    if (pVbList->pNextVarBind == NULL)
    {
        free_oid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        TE_DBG (TE_MAIN_SNMP, "Send Trap : Alloc VarBind Failed \n");
        return TE_FAILURE;
    }

    pVbList = pVbList->pNextVarBind;

    /* Value of the Trap  -> gmplsTunnelErrorCode */
    pOid = alloc_oid (sizeof (au4gmplsTnlErrorCode) / sizeof (UINT4));
    if (pOid == NULL)
    {
        TE_DBG (TE_MAIN_SNMP, "Send Trap : Alloc OID Failed \n");
        SNMP_free_snmp_vb_list (pStartVb);
        return TE_FAILURE;
    }
    u4Len = sizeof (au4gmplsTnlErrorCode) / sizeof (UINT4);
    au4gmplsTnlErrorCode[u4Len - 4] = pTeTnlInfo->u4TnlIndex;
    au4gmplsTnlErrorCode[u4Len - 3] = pTeTnlInfo->u4TnlInstance;
    au4gmplsTnlErrorCode[u4Len - 2] = u4TnlIngress;
    au4gmplsTnlErrorCode[u4Len - 1] = u4TnlEgress;
    MEMCPY (pOid->pu4_OidList, au4gmplsTnlErrorCode,
            sizeof (au4gmplsTnlErrorCode));
    pOid->u4_Length = u4Len;

    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_INTEGER,
                                                  0,
                                                  (INT4) u4ErrorCode,
                                                  NULL, NULL, SnmpCnt64Type));
    if (pVbList->pNextVarBind == NULL)
    {
        free_oid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        TE_DBG (TE_MAIN_SNMP, "Send Trap : Alloc VarBind Failed \n");
        return TE_FAILURE;
    }

    pVbList = pVbList->pNextVarBind;

    /* Value of the Trap  -> gmplsTunnelErrorSubcode */
    pOid = alloc_oid (sizeof (au4gmplsTnlErrorSubcode) / sizeof (UINT4));
    if (pOid == NULL)
    {
        TE_DBG (TE_MAIN_SNMP, "Send Trap : Alloc OID Failed \n");
        SNMP_free_snmp_vb_list (pStartVb);
        return TE_FAILURE;
    }
    u4Len = sizeof (au4gmplsTnlErrorSubcode) / sizeof (UINT4);
    au4gmplsTnlErrorSubcode[u4Len - 4] = pTeTnlInfo->u4TnlIndex;
    au4gmplsTnlErrorSubcode[u4Len - 3] = pTeTnlInfo->u4TnlInstance;
    au4gmplsTnlErrorSubcode[u4Len - 2] = u4TnlIngress;
    au4gmplsTnlErrorSubcode[u4Len - 1] = u4TnlEgress;
    MEMCPY (pOid->pu4_OidList, au4gmplsTnlErrorSubcode,
            sizeof (au4gmplsTnlErrorSubcode));
    pOid->u4_Length = u4Len;

    pVbList->pNextVarBind =
        ((tSNMP_VAR_BIND *) SNMP_AGT_FormVarBind (pOid,
                                                  SNMP_DATA_TYPE_INTEGER,
                                                  0,
                                                  (INT4) u4SubErrorCode,
                                                  NULL, NULL, SnmpCnt64Type));
    if (pVbList->pNextVarBind == NULL)
    {
        free_oid (pOid);
        SNMP_free_snmp_vb_list (pStartVb);
        TE_DBG (TE_MAIN_SNMP, "Send Trap : Alloc VarBind Failed \n");
        return TE_FAILURE;
    }

    pVbList = pVbList->pNextVarBind;

    pVbList->pNextVarBind = NULL;

    MplsPortFmNotifyFaults (pStartVb, ac1TeSysLogMsg);

    return (TE_SUCCESS);
}

/*****************************************************************************/
/* Function Name : TeGetTunnelFrmIfIdbasedTree
 * Description   : This routine Check the presence of a TE Tunnel info    
 *                 in the Tunnel table.                              
 * Input(s)      : u4TnlIfIndex    - Tunnel Interface Index                          
 * Output(s)     : ppTeTnlInfo  - Pointer to  Pointer of the tunnel info    
 *                 whose index matches u4TnlIfIndex.                       
 * Return(s)     : TE_SUCCESS in case of tunnel being present, otherwise    
 *                 TE_FAILURE                                               
 *****************************************************************************/
UINT1
TeGetTunnelFrmIfIdbasedTree (UINT4 u4TnlIfIndex, tTeTnlInfo ** ppTeTnlInfo)
{
    tTeTnlInfo          TeTnlInfo;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeGetTunnelFrmIfIdbasedTree : ENTRY \n");

    TeTnlInfo.u4TnlIfIndex = u4TnlIfIndex;

    *ppTeTnlInfo =
        (tTeTnlInfo *) RBTreeGet (gTeGblInfo.TnlIfIndexBasedTbl,
                                  (tRBElem *) & TeTnlInfo);

    if (*ppTeTnlInfo != NULL)
    {
        TE_DBG (TE_MAIN_PRCS, "MAIN : Valid TnlInfo found in Tunnel Table \n");
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeGetTunnelFrmIfIdbasedTree : EXIT \n");
        return TE_SUCCESS;
    }
    TE_DBG (TE_MAIN_FAIL, "MAIN : Valid TnlInfo not found in Tunnel Table \n");
    TE_DBG (TE_MAIN_ETEXT,
            "MAIN : TeGetTunnelFrmIfIdbasedTree : INTMD_EXIT \n");
    return TE_FAILURE;
}

/****************************************************************************
 Function    :  TnlIfIndexCmp
 Input       :  First and Next TE Tunnel Info. for comparison.  
 Output      :  TnlIndicesCmp function returns positive value 
                if the first tunnel Interface index is greater than next 
                tunnel Interface index else returns negative value.
 Returns     :  Positive or Negative  Integer value
****************************************************************************/

INT4
TnlIfIndexCmp (tTeTnlInfo * pFirstTeTnlInfo, tTeTnlInfo * pNextTeTnlInfo)
{

    if (pFirstTeTnlInfo->u4TnlIfIndex > pNextTeTnlInfo->u4TnlIfIndex)
    {
        return 1;
    }
    else if (pFirstTeTnlInfo->u4TnlIfIndex < pNextTeTnlInfo->u4TnlIfIndex)
    {
        return (-1);
    }

    return 0;
}

/************************************************************************
 *  Function Name   : TeMainFillErrorTable
 *  Description     : Function used to fill the error table
 *  Input           : pErrorSpec        - Pointer to Error spec structure
 *                    pRsvpTeTnlInfo    - Pointer to RSVP-TE tnl structure
 *  Output          : None
 *  Returns         : None
 ************************************************************************/

VOID
TeMainFillErrorTable (UINT4 u4LastErrorType, UINT4 u4ErrNodeAddr,
                      tTeTnlInfo * pTeTnlInfo)
{
    tTunnelErrInfo      TunnelErrInfo;
    MEMSET (&TunnelErrInfo, TE_ZERO, sizeof (tTunnelErrInfo));
    OsixGetSysTime ((tOsixSysTime *) & (TunnelErrInfo.u4LastErrTime));
    TunnelErrInfo.LastErrType = (eTunnelErrType) u4LastErrorType;
    TunnelErrInfo.ErrorReporterType = (eInetAddrType) IPV4;
    TunnelErrInfo.u4ErrorCode = TE_ZERO;
    TunnelErrInfo.u4SubErrorCode = TE_ZERO;
    TunnelErrInfo.u4ErrorIpAddress = u4ErrNodeAddr;
    TunnelErrInfo.u1ErrorIpAddressLen = IPV4_ADDR_LENGTH;

    TeCreateTnlErrorTable (&TunnelErrInfo, pTeTnlInfo);
    return;
}

/*****************************************************************************/
/* Function Name : TeSetDetourStatus
 * Description   : This routine sets the detour active status for instance 0
 *                 tunnel in TE.
 * Input(s)      : pTeTnlInfo     - Pointer to Tunnel Info.   
 *                 u1DetourStatus - Detour Status
 * Output(s)     : None                         
 * Return(s)     : None
 *****************************************************************************/
VOID
TeSetDetourStatus (tTeTnlInfo * pTeTnlInfo, UINT1 u1DetourStatus)
{
    tTeTnlInfo         *pInstance0Tnl = NULL;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;

    CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTeTnlInfo)), u4IngressId);
    CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTeTnlInfo)), u4EgressId);
    u4IngressId = TE_NTOHL (u4IngressId);
    u4EgressId = TE_NTOHL (u4EgressId);

    pInstance0Tnl = TeGetTunnelInfo (TE_TNL_TNL_INDEX (pTeTnlInfo), TE_ZERO,
                                     u4IngressId, u4EgressId);

    if (pInstance0Tnl != NULL)
    {
        pInstance0Tnl->u1DetourActive = u1DetourStatus;
    }
}

/*****************************************************************************
 * Function Name : TeCreateTunnel                                            
 * Description   : This function creates a new tunnel entry (working or
 *                 protection) in TE module and copies the properties of 
 *                 Zero Instance tunnel to it.
 * Input(s)      : pInstance0Tnl  - Pointer to Zero Instance Tunnel Entry               
 * Output(s)     : pNewTeTnl      - Address of the Pointer to new tunnel entry
 *                                  in case of successful allocation.
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                 
 *****************************************************************************/
UINT1
TeCreateTunnel (tTeTnlInfo * pInstance0Tnl, tTeTnlInfo ** pNewTeTnl)
{
    UINT1               u1RetVal = TE_FAILURE;
    tTeTnlInfo          TempTnlInfo;

    TE_DBG (TE_DIFF_ETEXT, "EXTN : TeCreateTunnel : ENTRY \n");

    MEMSET (&TempTnlInfo, TE_ZERO, sizeof (tTeTnlInfo));

    MEMCPY (&TempTnlInfo, pInstance0Tnl, sizeof (tTeTnlInfo));

    if (pInstance0Tnl->u1WorkingUp == FALSE)
    {
        TE_DBG (TE_DIFF_ETEXT, "EXTN : Working is down\n");

        TempTnlInfo.u1TnlPathType = TE_TNL_WORKING_PATH;
        /*TempTnlInfo.u4TnlInstance
           = ((pInstance0Tnl->u4TnlPrimaryInstance + 1) % TE_TNLINST_MAXVAL);

           if( TempTnlInfo.u4TnlInstance == pInstance0Tnl->u4BkpTnlInstance)
           {
           TempTnlInfo.u4TnlInstance
           = ((pInstance0Tnl->u4BkpTnlInstance + 1) % TE_TNLINST_MAXVAL);    
           printf("new instance is equal to  backup instance, so incremented: %u\n", TempTnlInfo.u4TnlInstance);
           } */

        if (pInstance0Tnl->u4BkpTnlInstance == TE_ZERO)
        {
            TempTnlInfo.u4TnlInstance =
                ((pInstance0Tnl->u4TnlPrimaryInstance + 1) % TE_TNLINST_MAXVAL);
        }
        else
        {
            TempTnlInfo.u4TnlInstance =
                ((pInstance0Tnl->u4BkpTnlInstance + 1) % TE_TNLINST_MAXVAL);

            if (TempTnlInfo.u4TnlInstance ==
                pInstance0Tnl->u4TnlPrimaryInstance)
            {
                TempTnlInfo.u4TnlInstance
                    =
                    ((pInstance0Tnl->u4TnlPrimaryInstance +
                      1) % TE_TNLINST_MAXVAL);
            }

        }
    }
    else
    {
        if (pInstance0Tnl->u4BkpTnlInstance == TE_ZERO)
        {
            TempTnlInfo.u4TnlInstance =
                ((pInstance0Tnl->u4TnlPrimaryInstance + 1) % TE_TNLINST_MAXVAL);
        }
        else
        {
            TempTnlInfo.u4TnlInstance =
                ((pInstance0Tnl->u4BkpTnlInstance + 1) % TE_TNLINST_MAXVAL);

            if (TempTnlInfo.u4TnlInstance ==
                pInstance0Tnl->u4TnlPrimaryInstance)
            {
                TempTnlInfo.u4TnlInstance
                    =
                    ((pInstance0Tnl->u4TnlPrimaryInstance +
                      1) % TE_TNLINST_MAXVAL);
            }
        }

        if (pInstance0Tnl->GmplsTnlInfo.i4E2EProtectionType ==
            MPLS_TE_DEDICATED_ONE2ONE)
        {
            TE_DBG2 (TE_DIFF_ETEXT,
                     "EXTN : Protection path for tunnel index: %u and instance: %u\n",
                     TempTnlInfo.u4TnlIndex, TempTnlInfo.u4TnlInstance);
            TempTnlInfo.u1TnlPathType = TE_TNL_PROTECTION_PATH;
            TempTnlInfo.pTePathListInfo = NULL;
            TempTnlInfo.u1TnlRerouteFlag = FALSE;
            TempTnlInfo.u4TnlIfIndex = 0;

        }
        else
        {
            TempTnlInfo.u1TnlPathType = TE_TNL_WORKING_PATH;
        }
    }

    TempTnlInfo.u4TnlPrimaryInstance = TempTnlInfo.u4TnlInstance;
    TempTnlInfo.u1TnlOwner = TE_TNL_OWNER_RSVP;
    TempTnlInfo.u1TnlRole = TE_INGRESS;

    /*Holding Priority should be zero for HLSP tunnel */

    if (pInstance0Tnl->u4TnlType & MPLS_TE_TNL_TYPE_HLSP)
    {
        TempTnlInfo.u1TnlHoldPrio = TE_ZERO;
    }

    TempTnlInfo.u1TnlOperStatus = TE_OPER_DOWN;

    TempTnlInfo.u1CPOrMgmtOperStatus = TE_OPER_DOWN;

    TempTnlInfo.u1OamOperStatus = TE_OPER_DOWN;

    u1RetVal = TeCreateNewTnl (pNewTeTnl, &TempTnlInfo);

    if (u1RetVal == TE_FAILURE)
    {
        return u1RetVal;
    }

    pInstance0Tnl->u1NumInstances++;

    if (pInstance0Tnl->u1WorkingUp == FALSE)
    {
        pInstance0Tnl->u4TnlPrimaryInstance = TempTnlInfo.u4TnlInstance;
        pInstance0Tnl->u4OrgTnlInstance = TempTnlInfo.u4TnlInstance;

        if (pInstance0Tnl->GmplsTnlInfo.i4E2EProtectionType ==
            MPLS_TE_DEDICATED_ONE2ONE)
        {
            pInstance0Tnl->u1WorkingUp = WORKING_PATH_INITIATED;
        }
    }
    else if (pInstance0Tnl->GmplsTnlInfo.i4E2EProtectionType ==
             MPLS_TE_DEDICATED_ONE2ONE)
    {
        pInstance0Tnl->u4BkpTnlInstance = TempTnlInfo.u4TnlInstance;
        pInstance0Tnl->u1BackupUp = BACKUP_PATH_INITIATED;
    }

    TE_DBG (TE_DIFF_ETEXT, "EXTN : TeCreateTunnel : EXIT \n");
    return u1RetVal;
}

/*****************************************************************************
 * Function Name : TeCreateFrrConstInfo
 * Description   : This function creates a new FRR constraint info for 
 *                 non-zero instance and copies and copies the properties of 
 *                 FRR const info of Zero Instance tunnel to it.
 * Input(s)      : pTmpTeTnlInfo    - Pointer to Tunnel Info
 *                 pTmpFrrConstInfo - Pointer to FRR Constraint Info
 * Output(s)     : None.
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                 
 *****************************************************************************/
UINT1
TeCreateFrrConstInfo (tTeTnlInfo * pTmpTeTnlInfo,
                      tTeFrrConstInfo * pTmpFrrConstInfo)
{
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    if (pTmpFrrConstInfo == NULL)
    {
        return TE_SUCCESS;
    }

    /* Memory Allocation for pTeFrrConstInfo */
    pTeFrrConstInfo = (tTeFrrConstInfo *)
        TE_ALLOC_MEM_BLOCK (TE_TNL_FRR_CONST_INFO_POOL_ID);

    if (pTeFrrConstInfo == NULL)
    {
        return TE_FAILURE;
    }

    pTmpTeTnlInfo->pTeFrrConstInfo = pTeFrrConstInfo;
    MEMCPY (pTmpTeTnlInfo->pTeFrrConstInfo, pTmpFrrConstInfo,
            sizeof (tTeFrrConstInfo));

    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeCreateDiffServTnl                                       
 * Description   : This routine allocates memory for new TeDiffServ TunnelInfo,
 *                 copies the contents provided by the Temporary pointer.
 * Input(s)      : pNewDiffServTnlInfo - Pointer to Temporary DSTnlInfo.   
 * Output(s)     : ppMplsDiffServTnlInfo - Address of the Pointer of 
 *                 DSTnlInfo in case of successful allocation.      
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                 
 *****************************************************************************/
UINT1
TeCreateDiffServTnl (tMplsDiffServTnlInfo ** ppMplsDiffServTnlInfo,
                     tMplsDiffServTnlInfo * pNewDiffServTnlInfo)
{
    tMplsDiffServTnlInfo *pTmpDiffServTnlInfo = NULL;

    TE_DBG (TE_DIFF_ETEXT, "EXTN : TeCreateDiffServTnl : ENTRY \n");

    /* Admin Status Check made in All Exported Functions */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        TE_DBG (TE_DIFF_FAIL,
                "EXTN : TE Admin Status DOWN. No Further processing \n");
        TE_DBG (TE_DIFF_ETEXT, "EXTN : TeCreateDiffServTnl : INTMD-EXIT \n");
        return TE_FAILURE;
    }

    if ((ppMplsDiffServTnlInfo == NULL) || (pNewDiffServTnlInfo == NULL))
    {
        TE_DBG (TE_DIFF_FAIL,
                "EXTN : Null Pointer provided for DiffServTnlInfo \n");
        TE_DBG (TE_DIFF_ETEXT, "EXTN : TeCreateDiffServTnl : INTMD-EXIT \n");
        return TE_FAILURE;
    }

    pTmpDiffServTnlInfo = (tMplsDiffServTnlInfo *)
        TE_ALLOC_MEM_BLOCK (TE_DS_TNL_INFO_POOL_ID);

    if (pTmpDiffServTnlInfo == NULL)
    {
        TE_DBG (TE_DIFF_FAIL,
                "EXTN : Free MplsDiffServTnlInfo does not exist\n");
        TE_DBG (TE_DIFF_ETEXT, "EXTN : TeCreateDiffServTnl : INTMD-EXIT \n");
        return TE_FAILURE;
    }

    INIT_TE_DS_TNL_INFO (pTmpDiffServTnlInfo);

    /* Copying the contents received in the temp structure to the one that
     * is allocated by TE module
     */
    MEMCPY (pTmpDiffServTnlInfo, pNewDiffServTnlInfo,
            sizeof (tMplsDiffServTnlInfo));

    if (TE_DS_TNL_ELSP_INFO_LIST_INFO (pTmpDiffServTnlInfo) != NULL)
    {
        TE_DBG (TE_DIFF_PRCS, "EXTN : MplsElspList is present\n");
        TE_DS_ELSP_INFO_LIST_TNL_COUNT (TE_DS_TNL_ELSP_INFO_LIST_INFO
                                        (pTmpDiffServTnlInfo))++;
    }
    *ppMplsDiffServTnlInfo = pTmpDiffServTnlInfo;

    TE_DBG (TE_DIFF_ETEXT, "EXTN : TeCreateDiffServTnl : EXIT \n");
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeGetNextTunnelInfo
 * Description   : This routine gets the next instance of the tunnel that
 *                 matches Index, Ingress and Egress
 * Input(s)      : pTeTnlInfo       - Pointer to the current Tunnel info                         
 * Output(s)     : None 
 * Return(s)     : Pointer to Next tunnel if present otherwise NULL.
 *****************************************************************************/
tTeTnlInfo         *
TeGetNextTunnelInfo (tTeTnlInfo * pTeTnlInfo)
{
    tTeTnlInfo         *pNextTnlInfo = NULL;

    pNextTnlInfo =
        (tTeTnlInfo *) RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                      (tRBElem *) pTeTnlInfo, NULL);

    return pNextTnlInfo;
}

/*****************************************************************************/
/* Function Name : TeAddToTeIfIdxbasedTable
 * Description   : Adds the tunnel in global ifIndex based RBTree        
 * Input(s)      : tTeTnlInfo     - Pointer to TeTnlInfo Structured      
 * Output(s)     : NONE
 * Return(s)     : TE_SUCCESS / TE_FAILURE                                 
 *****************************************************************************/
UINT1
TeAddToTeIfIdxbasedTable (tTeTnlInfo * pTeTnlInfo)
{
    tTeTnlInfo         *pTempTeTnlInfo = NULL;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;

    /* Admin Status Check made in All Exported Functions */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        return TE_FAILURE;
    }

    /* Don't Allow to Hack the Code */
    if (pTeTnlInfo == NULL)
    {
        return TE_FAILURE;
    }

    CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTeTnlInfo)), u4IngressId);
    CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTeTnlInfo)), u4EgressId);
    u4IngressId = OSIX_NTOHL (u4IngressId);
    u4EgressId = OSIX_NTOHL (u4EgressId);

    pTempTeTnlInfo = TeGetTunnelInfo (TE_TNL_TNL_INDEX (pTeTnlInfo),
                                      TE_TNL_TNL_INSTANCE (pTeTnlInfo),
                                      u4IngressId, u4EgressId);

    if (pTempTeTnlInfo == NULL)
    {
        return TE_FAILURE;
    }

    if (pTempTeTnlInfo->u1TnlRole != TE_EGRESS)
    {
        return TE_SUCCESS;
    }

    pTempTeTnlInfo->u4TnlIfIndex = pTempTeTnlInfo->u4RevTnlIfIndex;

    if (RBTreeAdd ((gTeGblInfo.TnlIfIndexBasedTbl), (tRBElem *) pTempTeTnlInfo)
        == RB_FAILURE)
    {
        return TE_FAILURE;
    }

    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeDelFromTeIfIdxbasedTable
 * Description   : Deletes the tunnel in global ifIndex based RBTree
 * Input(s)      : tTeTnlInfo     - Pointer to TeTnlInfo Structure
 * Output(s)     : NONE
 * Return(s)     : TE_FAILURE / TE_SUCCESS
 *****************************************************************************/
UINT1
TeDelFromTeIfIdxbasedTable (tTeTnlInfo * pTeTnlInfo)
{
    tTeTnlInfo         *pTempTeTnlInfo = NULL;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;

    /* Admin Status Check made in All Exported Functions */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        return TE_FAILURE;
    }

    /* Don't Allow to Hack the Code */
    if (pTeTnlInfo == NULL)
    {
        return TE_FAILURE;
    }

    CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTeTnlInfo)), u4IngressId);
    CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTeTnlInfo)), u4EgressId);
    u4IngressId = OSIX_NTOHL (u4IngressId);
    u4EgressId = OSIX_NTOHL (u4EgressId);

    pTempTeTnlInfo = TeGetTunnelInfo (TE_TNL_TNL_INDEX (pTeTnlInfo),
                                      TE_TNL_TNL_INSTANCE (pTeTnlInfo),
                                      u4IngressId, u4EgressId);

    if (pTempTeTnlInfo == NULL)
    {
        return TE_FAILURE;
    }

    if (pTempTeTnlInfo->u1TnlRole != TE_EGRESS)
    {
        return TE_SUCCESS;
    }

    pTempTeTnlInfo->u4TnlIfIndex = pTempTeTnlInfo->u4RevTnlIfIndex;

    RBTreeRem (gTeGblInfo.TnlIfIndexBasedTbl, pTeTnlInfo);

    return TE_SUCCESS;
}

/************************************************************************
 *  Function Name   : TeCopyOutLabel
 *  Description     : Copies the Out label from the Source TeTnlInfo
 *                    to the Destination TeTnlInfo
 *  Input           : pSrcTeTnlInfo -> Source Tunnel Info
 *                    pDestTeTnlInfo -> Destination Tunnel Info
 *  Output          : None
 *  Returns         : TE_SUCCESS / TE_FAILURE
 ************************************************************************/
UINT1
TeCopyOutLabel (tTeTnlInfo * pSrcTeTnlInfo, tTeTnlInfo * pDestTeTnlInfo)
{
    tTeTnlInfo         *pTempSrcTeTnlInfo = NULL;
    tTeTnlInfo         *pTempDestTeTnlInfo = NULL;
    tXcEntry           *pSrcXcEntry = NULL;
    tXcEntry           *pDestXcEntry = NULL;
    eDirection          Direction = MPLS_DIRECTION_FORWARD;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;

    /* Admin Status Check made in All Exported Functions */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        return TE_FAILURE;
    }

    /* Don't Allow to Hack the Code */
    if ((pSrcTeTnlInfo == NULL) || (pDestTeTnlInfo == NULL))
    {
        return TE_FAILURE;
    }

    CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pSrcTeTnlInfo)), u4IngressId);
    CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pSrcTeTnlInfo)), u4EgressId);
    u4IngressId = OSIX_NTOHL (u4IngressId);
    u4EgressId = OSIX_NTOHL (u4EgressId);

    pTempSrcTeTnlInfo = TeGetTunnelInfo (TE_TNL_TNL_INDEX (pSrcTeTnlInfo),
                                         TE_TNL_TNL_INSTANCE (pSrcTeTnlInfo),
                                         u4IngressId, u4EgressId);

    if (pTempSrcTeTnlInfo == NULL)
    {
        return TE_FAILURE;
    }

    CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pDestTeTnlInfo)), u4IngressId);
    CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pDestTeTnlInfo)), u4EgressId);
    u4IngressId = OSIX_NTOHL (u4IngressId);
    u4EgressId = OSIX_NTOHL (u4EgressId);

    pTempDestTeTnlInfo = TeGetTunnelInfo (TE_TNL_TNL_INDEX (pDestTeTnlInfo),
                                          TE_TNL_TNL_INSTANCE (pDestTeTnlInfo),
                                          u4IngressId, u4EgressId);

    if (pTempDestTeTnlInfo == NULL)
    {
        return TE_FAILURE;
    }

    if (pTempSrcTeTnlInfo->u1TnlRole == TE_INGRESS)
    {
        Direction = MPLS_DIRECTION_FORWARD;
    }
    else if (pTempSrcTeTnlInfo->u1TnlRole == TE_EGRESS)
    {
        Direction = MPLS_DIRECTION_REVERSE;
    }

    pSrcXcEntry = MplsGetXCEntryByDirection (pTempSrcTeTnlInfo->u4TnlXcIndex,
                                             Direction);

    if (pTempDestTeTnlInfo->u1TnlRole == TE_INGRESS)
    {
        Direction = MPLS_DIRECTION_FORWARD;
    }
    else if (pDestTeTnlInfo->u1TnlRole == TE_EGRESS)
    {
        Direction = MPLS_DIRECTION_REVERSE;
    }

    pDestXcEntry = MplsGetXCEntryByDirection (pDestTeTnlInfo->u4TnlXcIndex,
                                              Direction);

    if ((pDestXcEntry != NULL) && (pSrcXcEntry != NULL) &&
        (pDestXcEntry->pOutIndex != NULL) && (pSrcXcEntry->pOutIndex != NULL))
    {
        /* copy the outlabel of newly formed tunnel to
         * all the associated stacked tunnel */
        pDestXcEntry->pOutIndex->u4Label = pSrcXcEntry->pOutIndex->u4Label;
        MEMCPY (pDestXcEntry->pOutIndex->au1NextHopMac,
                pSrcXcEntry->pOutIndex->au1NextHopMac, MAC_ADDR_LEN);
        pDestXcEntry->pOutIndex->NHAddr.ip6_addr_u.u4WordAddr[0] =
            pSrcXcEntry->pOutIndex->NHAddr.ip6_addr_u.u4WordAddr[0];

    }
    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeDeleteStackTnlIf
 * Description   : Deletes the tunnel interface and its statcking
 * Input(s)      : tTeTnlInfo     - Pointer to TeTnlInfo Structure
 * Output(s)     : NONE
 * Return(s)     : TE_FAILURE / TE_SUCCESS
 *****************************************************************************/
UINT1
TeDeleteStackTnlIf (tTeTnlInfo * pTeTnlInfo)
{
    tTeTnlInfo         *pTempTeTnlInfo = NULL;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;

    /* Admin Status Check made in All Exported Functions */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        return TE_FAILURE;
    }

    /* Don't Allow to Hack the Code */
    if (pTeTnlInfo == NULL)
    {
        return TE_FAILURE;
    }

    CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTeTnlInfo)), u4IngressId);
    CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTeTnlInfo)), u4EgressId);
    u4IngressId = OSIX_NTOHL (u4IngressId);
    u4EgressId = OSIX_NTOHL (u4EgressId);

    pTempTeTnlInfo = TeGetTunnelInfo (TE_TNL_TNL_INDEX (pTeTnlInfo),
                                      TE_TNL_TNL_INSTANCE (pTeTnlInfo),
                                      u4IngressId, u4EgressId);

    if (pTempTeTnlInfo == NULL)
    {
        return TE_FAILURE;
    }
    TeDeleteStackTnlInterface (pTempTeTnlInfo);

    return TE_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TeAddToTeMsgIdBasedTable
 * Description   : Adds the tunnel in global MsgId based RBTree
 * Input(s)      : tTeTnlInfo     - Pointer to TeTnlInfo Structure
 *                 u4OutPathMsgId - Message Id
 * Output(s)     : NONE
 * Return(s)     : NONE
 *****************************************************************************/
VOID
TeAddToTeMsgIdBasedTable (tTeTnlInfo * pTeTnlInfo, UINT4 u4OutPathMsgId)
{
    tTeTnlInfo         *pTempTeTnlInfo = NULL;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;

    /* Admin Status Check made in All Exported Functions */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        return;
    }

    /* Don't Allow to Hack the Code */
    if (pTeTnlInfo == NULL)
    {
        return;
    }

    CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTeTnlInfo)), u4IngressId);
    CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTeTnlInfo)), u4EgressId);
    u4IngressId = OSIX_NTOHL (u4IngressId);
    u4EgressId = OSIX_NTOHL (u4EgressId);

    pTempTeTnlInfo = TeGetTunnelInfo (TE_TNL_TNL_INDEX (pTeTnlInfo),
                                      TE_TNL_TNL_INSTANCE (pTeTnlInfo),
                                      u4IngressId, u4EgressId);

    if (pTempTeTnlInfo == NULL)
    {
        return;
    }

    pTempTeTnlInfo->u4OutPathMsgId = u4OutPathMsgId;

    if (RBTreeAdd ((gTeGblInfo.TnlMsgIdBasedTbl), (tRBElem *) pTempTeTnlInfo)
        != RB_SUCCESS)
    {
        TE_DBG (TE_MAIN_ETEXT,
                "Failed to Add :EXIT TeTnlInfo to TnlMsgIdBasedTbl\n");
    }

    return;
}

/*****************************************************************************/
/* Function Name : TeGetTunnelInfoFromOutMsgId
 * Description   : Gets the tunnel from global MsgId based RBTree
 * Input(s)      : tTeTnlInfo     - Pointer to TeTnlInfo Structure
 *                 u4OutPathMsgId - Message Id
 * Output(s)     : NONE
 * Return(s)     : NONE
 *****************************************************************************/
VOID
TeGetTunnelInfoFromOutMsgId (tTeTnlInfo ** pTeTnlInfo, UINT4 u4MsgId)
{
    tTeTnlInfo          pTempTeTnlInfo;
    pTempTeTnlInfo.u4OutPathMsgId = u4MsgId;
    *pTeTnlInfo = (tTeTnlInfo *) RBTreeGet (gTeGblInfo.TnlMsgIdBasedTbl,
                                            (tRBElem *) & pTempTeTnlInfo);
    return;
}

/*****************************************************************************/
/* Function Name : TeDelFromTeMsgIdBasedTable
 * Description   : Removes the tunnel from global MsgId based RBTree
 * Input(s)      : tTeTnlInfo     - Pointer to TeTnlInfo Structure
 * Output(s)     : NONE
 * Return(s)     : NONE
 *****************************************************************************/
VOID
TeDelFromTeMsgIdBasedTable (tTeTnlInfo * pTeTnlInfo)
{
    tTeTnlInfo         *pTempTeTnlInfo = NULL;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;

    /* Admin Status Check made in All Exported Functions */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        return;
    }

    /* Don't Allow to Hack the Code */
    if (pTeTnlInfo == NULL)
    {
        return;
    }

    CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTeTnlInfo)), u4IngressId);
    CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTeTnlInfo)), u4EgressId);
    u4IngressId = OSIX_NTOHL (u4IngressId);
    u4EgressId = OSIX_NTOHL (u4EgressId);

    pTempTeTnlInfo = TeGetTunnelInfo (TE_TNL_TNL_INDEX (pTeTnlInfo),
                                      TE_TNL_TNL_INSTANCE (pTeTnlInfo),
                                      u4IngressId, u4EgressId);

    if (pTempTeTnlInfo == NULL)
    {
        return;
    }

    if (pTempTeTnlInfo->u4OutPathMsgId == TE_ZERO)
    {
        return;
    }

    RBTreeRem (gTeGblInfo.TnlMsgIdBasedTbl, pTempTeTnlInfo);
    pTempTeTnlInfo->u4OutPathMsgId = TE_ZERO;

    return;
}

/****************************************************************************
 Function    :  TnlMsgIdCmp
 Input       :  First and Next TE Tunnel Info. for comparison.
 Output      :  TnlMsgIdCmp function returns positive value
                if the first tunnel message id is greater than next
                tunnel message id else returns negative value.
 Returns     :  Positive or Negative  Integer value
****************************************************************************/

INT4
TnlMsgIdCmp (tTeTnlInfo * pFirstTeTnlInfo, tTeTnlInfo * pNextTeTnlInfo)
{

    if (pFirstTeTnlInfo->u4OutPathMsgId > pNextTeTnlInfo->u4OutPathMsgId)
    {
        return 1;
    }
    else if (pFirstTeTnlInfo->u4OutPathMsgId < pNextTeTnlInfo->u4OutPathMsgId)
    {
        return (-1);
    }

    return 0;
}

/*****************************************************************************/
/* Function Name : TeSetGrSynchronizedStatus
 * Description   : This function is used to Synchronized status for a tunnel.
 * Input(s)      : tTeTnlInfo            - Pointer to TeTnlInfo Structure
 * Output(s)     : u1TnlSynStatus        - Synchronized status
 * Return(s)     : None
 *****************************************************************************/
VOID
TeSetGrSynchronizedStatus (tTeTnlInfo * pTeTnlInfo, UINT1 u1TnlSynStatus)
{
    tTeTnlInfo         *pTempTeTnlInfo = NULL;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;

    /* Admin Status Check made in All Exported Functions */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        return;
    }

    /* Don't Allow to Hack the Code */
    if (pTeTnlInfo == NULL)
    {
        return;
    }

    CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTeTnlInfo)), u4IngressId);
    CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTeTnlInfo)), u4EgressId);
    u4IngressId = OSIX_NTOHL (u4IngressId);
    u4EgressId = OSIX_NTOHL (u4EgressId);

    pTempTeTnlInfo = TeGetTunnelInfo (TE_TNL_TNL_INDEX (pTeTnlInfo),
                                      TE_TNL_TNL_INSTANCE (pTeTnlInfo),
                                      u4IngressId, u4EgressId);

    if (pTempTeTnlInfo == NULL)
    {
        return;
    }
    pTempTeTnlInfo->u1TnlSyncStatus = u1TnlSynStatus;
    return;
}

/***************************************************************************/
/*  Function Name   : MplsHandleRpteMaxWaitTmrEvent                         */
/*  Description     : This Function handles the RSVP-TE max wait timer     */
/*  Input           : None                                                 */
/*  Output          : None                                                 */
/*  Returns         : None                                                 */
/***************************************************************************/
VOID
MplsHandleRpteMaxWaitTmrEvent (VOID *ptr)
{
    UNUSED_PARAM (ptr);

    MPLS_CMN_LOCK ();

    TeGrDeleteNonSynchronizedTnls ();

    MPLS_CMN_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name : TeGetTrafficParams
 * Description   : This function is used to retrieve the traffic parameters
 *                 information from TE for a tunnel.
 * Input(s)      : tTeTnlInfo            - Pointer to TeTnlInfo Structure                 
 * Output(s)     : pu4PeakDataRate       - Peak Data Rate
 *                 pu4PeakBurstSize      - Peak Burst Size
 *                 pu4CommittedDataRate  - Committed Data Rate
 *                 pu4CommittedBurstSize - Committed Burst Size
 *                 pu4ExcessBurstSize    - Excess Burst Size
 * Return(s)     : None
 *****************************************************************************/
VOID
TeGetTrafficParams (tTeTnlInfo * pTeTnlInfo, UINT4 *pu4PeakDataRate,
                    UINT4 *pu4PeakBurstSize, UINT4 *pu4CommittedDataRate,
                    UINT4 *pu4CommittedBurstSize, UINT4 *pu4ExcessBurstSize)
{
    tTeTnlInfo         *pTempTeTnlInfo = NULL;
    tRSVPTrfcParams    *pRSVPTrfcParams = NULL;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;

    /* Admin Status Check made in All Exported Functions */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        return;
    }

    if (pTeTnlInfo == NULL)
    {
        return;
    }

    CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTeTnlInfo)), u4IngressId);
    CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTeTnlInfo)), u4EgressId);
    u4IngressId = OSIX_NTOHL (u4IngressId);
    u4EgressId = OSIX_NTOHL (u4EgressId);

    pTempTeTnlInfo = TeGetTunnelInfo (TE_TNL_TNL_INDEX (pTeTnlInfo),
                                      TE_TNL_TNL_INSTANCE (pTeTnlInfo),
                                      u4IngressId, u4EgressId);

    if (pTempTeTnlInfo == NULL)
    {
        return;
    }

    if ((pTempTeTnlInfo->pTeTrfcParams != NULL) &&
        (pTempTeTnlInfo->pTeTrfcParams->pRSVPTrfcParams != NULL))
    {
        pRSVPTrfcParams = pTempTeTnlInfo->pTeTrfcParams->pRSVPTrfcParams;

        *pu4PeakDataRate = TeGetAttrFromTnlOrAttrListByMask (pTempTeTnlInfo,
                                                             TE_ATTR_BANDWIDTH_BITMASK);
        *pu4CommittedDataRate = pRSVPTrfcParams->u4TokenBktRate;
        *pu4CommittedBurstSize = pRSVPTrfcParams->u4TokenBktSize;
        *pu4PeakBurstSize = pRSVPTrfcParams->u4MaxPktSize;
        *pu4ExcessBurstSize = pRSVPTrfcParams->u4MinPolicedUnit;
    }

    return;
}

/******************************************************************************
 Function Name  : TeGetAttrFromTnlOrAttrListByMask
 Description    : This routine is used to get the attributes for Tunnel of
                  Attribute list for the specified Mask
 Input          : pTeTnlInfo        - Pointer to Te-tnl Info
                  u4ListBitMask     - Mask value
 Output         : None
 Returns        : u4RetVal          - Attribute value
****************************************************************************/
UINT4
TeGetAttrFromTnlOrAttrListByMask (tTeTnlInfo * pTeTnlInfo, UINT4 u4ListBitMask)
{
    tTeTnlInfo         *pTempTeTnlInfo = NULL;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    UINT4               u4TempListBitMask = TE_ZERO;
    UINT4               u4RetVal = TE_ZERO;

    /* Admin Status Check made in All Exported Functions */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        return TE_FAILURE;
    }

    /* Don't Allow to Hack the Code */
    if (pTeTnlInfo == NULL)
    {
        return TE_FAILURE;
    }

    CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTeTnlInfo)), u4IngressId);
    CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTeTnlInfo)), u4EgressId);
    u4IngressId = OSIX_NTOHL (u4IngressId);
    u4EgressId = OSIX_NTOHL (u4EgressId);

    pTempTeTnlInfo = TeGetTunnelInfo (TE_TNL_TNL_INDEX (pTeTnlInfo),
                                      TE_TNL_TNL_INSTANCE (pTeTnlInfo),
                                      u4IngressId, u4EgressId);

    if (pTempTeTnlInfo == NULL)
    {
        return TE_FAILURE;
    }

    if (pTempTeTnlInfo->pTeAttrListInfo != NULL)
    {
        u4TempListBitMask = pTempTeTnlInfo->pTeAttrListInfo->u4ListBitmask;
    }
    switch (u4ListBitMask)
    {
        case TE_ATTR_SETUPPRI_BITMASK:
            if (u4TempListBitMask & TE_ATTR_SETUPPRI_BITMASK)
            {
                u4RetVal = pTempTeTnlInfo->pTeAttrListInfo->u1SetupPriority;
            }
            else
            {
                u4RetVal = pTempTeTnlInfo->u1TnlSetPrio;
            }
            break;
        case TE_ATTR_HOLDPRI_BITMASK:
            if (u4TempListBitMask & TE_ATTR_HOLDPRI_BITMASK)
            {
                u4RetVal = pTempTeTnlInfo->pTeAttrListInfo->u1HoldingPriority;
            }
            else
            {
                u4RetVal = pTempTeTnlInfo->u1TnlHoldPrio;
            }
            break;
        case TE_ATTR_LSP_SSN_ATTR_BITMASK:
            if (u4TempListBitMask & TE_ATTR_LSP_SSN_ATTR_BITMASK)
            {
                u4RetVal = pTempTeTnlInfo->pTeAttrListInfo->u1SsnAttr;
            }
            else
            {
                u4RetVal = pTempTeTnlInfo->u1TnlSsnAttr;
            }
            break;
        case TE_ATTR_INCLUDE_ANY_AFFINITY_BITMASK:
            if (u4TempListBitMask & TE_ATTR_INCLUDE_ANY_AFFINITY_BITMASK)
            {
                u4RetVal =
                    pTempTeTnlInfo->pTeAttrListInfo->u4IncludeAnyAffinity;
            }
            else
            {
                u4RetVal = pTempTeTnlInfo->u4TnlIncludeAnyAffinity;
            }
            break;
        case TE_ATTR_INCLUDE_ALL_AFFINITY_BITMASK:
            if (u4TempListBitMask & TE_ATTR_INCLUDE_ALL_AFFINITY_BITMASK)
            {
                u4RetVal =
                    pTempTeTnlInfo->pTeAttrListInfo->u4IncludeAllAffinity;
            }
            else
            {
                u4RetVal = pTempTeTnlInfo->u4TnlIncludeAllAffinity;
            }
            break;
        case TE_ATTR_EXCLUDE_ANY_AFFINITY_BITMASK:
            if (u4TempListBitMask & TE_ATTR_EXCLUDE_ANY_AFFINITY_BITMASK)
            {
                u4RetVal =
                    pTempTeTnlInfo->pTeAttrListInfo->u4ExcludeAnyAffinity;
            }
            else
            {
                u4RetVal = pTempTeTnlInfo->u4TnlExcludeAnyAffinity;
            }
            break;

        case TE_ATTR_BANDWIDTH_BITMASK:
            if (u4TempListBitMask & TE_ATTR_BANDWIDTH_BITMASK)
            {
                u4RetVal = pTempTeTnlInfo->pTeAttrListInfo->u4Bandwidth;
            }
            else
            {
                if (pTempTeTnlInfo->pTeTrfcParams != NULL)
                {
                    if (pTempTeTnlInfo->pTeTrfcParams->pRSVPTrfcParams != NULL)
                    {
                        u4RetVal =
                            pTempTeTnlInfo->pTeTrfcParams->pRSVPTrfcParams->
                            u4PeakDataRate;
                    }
                    else if (pTempTeTnlInfo->pTeTrfcParams->pCRLDPTrfcParams !=
                             NULL)
                    {
                        u4RetVal =
                            pTempTeTnlInfo->pTeTrfcParams->pCRLDPTrfcParams->
                            u4PeakDataRate;
                    }
                }
            }
            break;
        case TE_ATTR_SRLG_TYPE_BITMASK:
            if (u4TempListBitMask & TE_ATTR_SRLG_TYPE_BITMASK)
            {
                u4RetVal = pTempTeTnlInfo->pTeAttrListInfo->u1SrlgType;
            }
            else
            {
                u4RetVal = pTempTeTnlInfo->u1TnlSrlgType;
            }
            break;
        case TE_ATTR_CLASS_TYPE_BITMASK:
            if (u4TempListBitMask & TE_ATTR_CLASS_TYPE_BITMASK)
            {
                u4RetVal = pTempTeTnlInfo->pTeAttrListInfo->u4ClassType;
            }
            else
            {
                if (pTempTeTnlInfo->pMplsDiffServTnlInfo != NULL)
                {
                    u4RetVal =
                        pTempTeTnlInfo->pMplsDiffServTnlInfo->u1ClassType;
                }
            }
            break;

        default:
            break;
    }

    return u4RetVal;
}

/*****************************************************************************/
/* Function     : TeGetLabelInfoUsingTnl                                     */
/* Description  : This function gets label info using te tunnel              */
/* Input        : pTeTnlInfo         - pointer to tunnel info                */
/*                u1Direction        - Direction                             */
/* Output       : pu4InLabel         - input label                           */
/*                pu4OutLbl          - output label                          */
/* Returns      : NONE                                                       */
/*****************************************************************************/

VOID
TeGetLabelInfoUsingTnl (tTeTnlInfo * pTeTnlInfo, UINT4 *pu4OutLbl,
                        UINT4 *pu4InLbl, UINT1 u1Direction)
{
    tXcEntry           *pXcEntry = NULL;

    if (pTeTnlInfo == NULL)
    {
        return;
    }

    pXcEntry = MplsGetXCEntryByDirection (pTeTnlInfo->u4TnlXcIndex,
                                          (eDirection) u1Direction);

    if (pXcEntry == NULL)
    {
        return;
    }
    if (pXcEntry->pInIndex != NULL)
    {
        *pu4InLbl = pXcEntry->pInIndex->u4Label;
    }
    if (pXcEntry->pOutIndex != NULL)
    {
        *pu4OutLbl = pXcEntry->pOutIndex->u4Label;
    }
    return;
}

/*****************************************************************************/
/* Function     : TeGetNoOfStackedTnls                                       */
/* Description  : This function gets number of tunnels stacked over the      */
/*                FA LSP signalled by RSVP                                   */
/* Input        : u4TnlIndex         - Index of the tunnel                   */
/*                u4TnlInstance      - Instance of the tunnel                */
/*                u4TnlIngressId     - Ingress of the tunnel                 */
/*                u4TnlEgressId      - Egress of the tunnel                  */
/* Output       : pu4NoOfStackedTnl  - Number of Stacked Tunnels             */
/* Returns      : NONE                                                       */
/*****************************************************************************/
VOID
TeGetNoOfStackedTnls (UINT4 u4TnlIndex, UINT4 u4TnlInstance,
                      UINT4 u4TnlIngressId, UINT4 u4TnlEgressId,
                      UINT4 *pu4NoOfStackedTnl)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    pTeTnlInfo = TeGetTunnelInfo (u4TnlIndex, u4TnlInstance, u4TnlIngressId,
                                  u4TnlEgressId);
    if (pTeTnlInfo == NULL)
    {
        return;
    }

    *pu4NoOfStackedTnl = pTeTnlInfo->u4NoOfStackedTunnels;

    return;
}

/****************************************************************************/
/* Function Name   : TeDeleteAllTnls                                        */
/* Description     : This function deletes all the tunnels present in TE    */
/* Input (s)       : NONE                                                   */
/* Output (s)      : NONE                                                   */
/* Returns         : NONE                                                   */
/****************************************************************************/

VOID
TeDeleteAllTnls ()
{
    tTeTnlInfo         *pTmpTeTnlInfo = NULL;
    tXcEntry           *pXcEntry = NULL;

    pTmpTeTnlInfo = RBTreeGetFirst (gTeGblInfo.TnlInfoTbl);

    while (pTmpTeTnlInfo != NULL)
    {
        /* Remove the H/W programming and release banwidth in TLM */
        if (TeCmnExtSetOperStatusAndProgHw (pTmpTeTnlInfo, TE_OPER_DOWN)
            == TE_FAILURE)
        {
            TE_DBG (TE_MAIN_FAIL, "TeCmnExtSetOperStatusAndProgHw failed\r\n");
        }

        /* Release Label from Label manager */
        pXcEntry = MplsGetXCEntryByDirection (pTmpTeTnlInfo->u4TnlXcIndex,
                                              MPLS_DIRECTION_FORWARD);
        if (pXcEntry != NULL)
        {
            if ((pXcEntry->pInIndex != NULL) &&
                (pXcEntry->pInIndex->u4Label != MPLS_INVALID_LABEL))
            {
                if (LblMgrRelLblToLblGroup (gu2GenLblSpaceGrpId,
                                            TE_ZERO,
                                            pXcEntry->pInIndex->u4Label)
                    == LBL_FAILURE)
                {
                    return;
                }
            }
            MplsDeleteXCTableEntry (pXcEntry);
            if (pXcEntry->pInIndex != NULL)
            {
                MplsDeleteInSegmentTableEntry (pXcEntry->pInIndex);
            }
            if (pXcEntry->pOutIndex != NULL)
            {
                MplsDeleteOutSegmentTableEntry (pXcEntry->pOutIndex);
            }
        }

        pXcEntry = MplsGetXCEntryByDirection (pTmpTeTnlInfo->u4TnlXcIndex,
                                              MPLS_DIRECTION_REVERSE);
        if (pXcEntry != NULL)
        {
            if ((pXcEntry->pInIndex != NULL) &&
                (pXcEntry->pInIndex->u4Label != MPLS_INVALID_LABEL))
            {
                if (LblMgrRelLblToLblGroup (gu2GenLblSpaceGrpId,
                                            TE_ZERO,
                                            pXcEntry->pInIndex->u4Label)
                    == LBL_FAILURE)
                {
                    return;
                }
            }
            MplsDeleteXCTableEntry (pXcEntry);
            if (pXcEntry->pInIndex != NULL)
            {
                MplsDeleteInSegmentTableEntry (pXcEntry->pInIndex);
            }
            if (pXcEntry->pOutIndex != NULL)
            {
                MplsDeleteOutSegmentTableEntry (pXcEntry->pOutIndex);
            }
        }
        TeDeleteTnlInfo (pTmpTeTnlInfo, TE_TNL_CALL_FROM_ADMIN);

        pTmpTeTnlInfo = RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                       (tRBElem *) pTmpTeTnlInfo, NULL);
    }
    return;
}

/****************************************************************************
 Function    :  ReoptTnlListCmp
 Input       :  First and Next TE Reopt Tunnel Info. for comparison.  
 Output      :  ReoptTnlListCmp function returns positive value 
                if the first tunnel index is greater than next 
                tunnel index else returns negative value.
 Returns     :  Positive or Negative  Integer value
****************************************************************************/

INT4
ReoptTnlListCmp (tTeReoptTnlInfo * pFirstReoptTeTnlInfo,
                 tTeReoptTnlInfo * pNextReoptTeTnlInfo)
{

    INT4                i4Return = 0;

    if (pFirstReoptTeTnlInfo->u4ReoptTnlIndex >
        pNextReoptTeTnlInfo->u4ReoptTnlIndex)
    {
        return 1;
    }
    else if (pFirstReoptTeTnlInfo->u4ReoptTnlIndex <
             pNextReoptTeTnlInfo->u4ReoptTnlIndex)
    {
        return (-1);
    }

    if ((i4Return = MEMCMP (pFirstReoptTeTnlInfo->ReoptTnlIngressLsrId,
                            pNextReoptTeTnlInfo->ReoptTnlIngressLsrId,
                            IPV4_ADDR_LENGTH)) != 0)
    {
        return i4Return;
    }
    if ((i4Return = MEMCMP (pFirstReoptTeTnlInfo->ReoptTnlEgressLsrId,
                            pNextReoptTeTnlInfo->ReoptTnlEgressLsrId,
                            IPV4_ADDR_LENGTH)) != 0)
    {
        return i4Return;
    }

    return 0;
}

/*---------------------------------------------------------------------------*/
/*                       End of file temain.c                                */
/*---------------------------------------------------------------------------*/
