/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: tpmutlg.c,v 1.7 2013/06/26 11:42:04 siva Exp $
*
* Description: This file contains utility functions used by protocol Te
*********************************************************************/

#include "teincs.h"
#include "mplslsr.h"
#include "cli.h"

extern UINT4        MplsTeP2mpTunnelP2mpIntegrity[];
extern UINT4        MplsTeP2mpTunnelBranchRole[];
extern UINT4        MplsTeP2mpTunnelRowStatus[];
extern UINT4        MplsTeP2mpTunnelStorageType[];
extern UINT4        MplsTeP2mpTunnelDestBranchOutSegment[];
extern UINT4        MplsTeP2mpTunnelDestHopTableIndex[];
extern UINT4        MplsTeP2mpTunnelDestPathInUse[];
extern UINT4        MplsTeP2mpTunnelDestAdminStatus[];
extern UINT4        MplsTeP2mpTunnelDestRowStatus[];
extern UINT4        MplsTeP2mpTunnelDestStorageType[];

/****************************************************************************
 Function    :  TeSetAllMplsTeP2mpTunnelTableTrigger
 Input       :  The Indices
                pTeSetMplsTeP2mpTunnelTable
                pTeIsSetMplsTeP2mpTunnelTable
 Output      :  This Routine is used to send 
                MSR and RM indication
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
TeSetAllMplsTeP2mpTunnelTableTrigger (tTeMplsTeP2mpTunnelTable *
                                      pTeSetMplsTeP2mpTunnelTable,
                                      tTeIsSetMplsTeP2mpTunnelTable *
                                      pTeIsSetMplsTeP2mpTunnelTable,
                                      INT4 i4SetOption)
{
    if (pTeIsSetMplsTeP2mpTunnelTable->bMplsTeP2mpTunnelP2mpIntegrity ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (MplsTeP2mpTunnelP2mpIntegrity, 13, CmnLock,
                      CmnUnLock, 0, 0, 4, i4SetOption, "%u %u %u %u %i",
                      pTeSetMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelIndex,
                      pTeSetMplsTeP2mpTunnelTable->MibObject.
                      u4MplsTunnelInstance,
                      pTeSetMplsTeP2mpTunnelTable->MibObject.
                      u4MplsTunnelIngressLSRId,
                      pTeSetMplsTeP2mpTunnelTable->MibObject.
                      u4MplsTunnelEgressLSRId,
                      pTeSetMplsTeP2mpTunnelTable->MibObject.
                      i4MplsTeP2mpTunnelP2mpIntegrity);
    }
    if (pTeIsSetMplsTeP2mpTunnelTable->bMplsTeP2mpTunnelBranchRole == OSIX_TRUE)
    {
        nmhSetCmnNew (MplsTeP2mpTunnelBranchRole, 13, CmnLock,
                      CmnUnLock, 0, 0, 4, i4SetOption, "%u %u %u %u %i",
                      pTeSetMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelIndex,
                      pTeSetMplsTeP2mpTunnelTable->MibObject.
                      u4MplsTunnelInstance,
                      pTeSetMplsTeP2mpTunnelTable->MibObject.
                      u4MplsTunnelIngressLSRId,
                      pTeSetMplsTeP2mpTunnelTable->MibObject.
                      u4MplsTunnelEgressLSRId,
                      pTeSetMplsTeP2mpTunnelTable->MibObject.
                      i4MplsTeP2mpTunnelBranchRole);
    }
    if (pTeIsSetMplsTeP2mpTunnelTable->bMplsTeP2mpTunnelRowStatus == OSIX_TRUE)
    {
        nmhSetCmnNew (MplsTeP2mpTunnelRowStatus, 13, CmnLock,
                      CmnUnLock, 0, 1, 4, i4SetOption, "%u %u %u %u %i",
                      pTeSetMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelIndex,
                      pTeSetMplsTeP2mpTunnelTable->MibObject.
                      u4MplsTunnelInstance,
                      pTeSetMplsTeP2mpTunnelTable->MibObject.
                      u4MplsTunnelIngressLSRId,
                      pTeSetMplsTeP2mpTunnelTable->MibObject.
                      u4MplsTunnelEgressLSRId,
                      pTeSetMplsTeP2mpTunnelTable->MibObject.
                      i4MplsTeP2mpTunnelRowStatus);
    }
    if (pTeIsSetMplsTeP2mpTunnelTable->bMplsTeP2mpTunnelStorageType ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (MplsTeP2mpTunnelStorageType, 13, CmnLock,
                      CmnUnLock, 0, 0, 4, i4SetOption, "%u %u %u %u %i",
                      pTeSetMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelIndex,
                      pTeSetMplsTeP2mpTunnelTable->MibObject.
                      u4MplsTunnelInstance,
                      pTeSetMplsTeP2mpTunnelTable->MibObject.
                      u4MplsTunnelIngressLSRId,
                      pTeSetMplsTeP2mpTunnelTable->MibObject.
                      u4MplsTunnelEgressLSRId,
                      pTeSetMplsTeP2mpTunnelTable->MibObject.
                      i4MplsTeP2mpTunnelStorageType);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  MplsTeP2mpTunnelTableFilterInputs
 Input       :  The Indices
                pTeMplsTeP2mpTunnelTable
                pTeSetMplsTeP2mpTunnelTable
                pTeIsSetMplsTeP2mpTunnelTable
 Output      :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
MplsTeP2mpTunnelTableFilterInputs (tTeMplsTeP2mpTunnelTable *
                                   pTeMplsTeP2mpTunnelTable,
                                   tTeMplsTeP2mpTunnelTable *
                                   pTeSetMplsTeP2mpTunnelTable,
                                   tTeIsSetMplsTeP2mpTunnelTable *
                                   pTeIsSetMplsTeP2mpTunnelTable)
{
    if (pTeIsSetMplsTeP2mpTunnelTable->bMplsTeP2mpTunnelP2mpIntegrity ==
        OSIX_TRUE)
    {
        if (pTeMplsTeP2mpTunnelTable->MibObject.
            i4MplsTeP2mpTunnelP2mpIntegrity ==
            pTeSetMplsTeP2mpTunnelTable->MibObject.
            i4MplsTeP2mpTunnelP2mpIntegrity)
            pTeIsSetMplsTeP2mpTunnelTable->bMplsTeP2mpTunnelP2mpIntegrity =
                OSIX_FALSE;
    }
    if (pTeIsSetMplsTeP2mpTunnelTable->bMplsTeP2mpTunnelBranchRole == OSIX_TRUE)
    {
        if (pTeMplsTeP2mpTunnelTable->MibObject.i4MplsTeP2mpTunnelBranchRole ==
            pTeSetMplsTeP2mpTunnelTable->MibObject.i4MplsTeP2mpTunnelBranchRole)
            pTeIsSetMplsTeP2mpTunnelTable->bMplsTeP2mpTunnelBranchRole =
                OSIX_FALSE;
    }
    if (pTeIsSetMplsTeP2mpTunnelTable->bMplsTeP2mpTunnelRowStatus == OSIX_TRUE)
    {
        if (pTeMplsTeP2mpTunnelTable->MibObject.i4MplsTeP2mpTunnelRowStatus ==
            pTeSetMplsTeP2mpTunnelTable->MibObject.i4MplsTeP2mpTunnelRowStatus)
            pTeIsSetMplsTeP2mpTunnelTable->bMplsTeP2mpTunnelRowStatus =
                OSIX_FALSE;
    }
    if (pTeIsSetMplsTeP2mpTunnelTable->bMplsTeP2mpTunnelStorageType ==
        OSIX_TRUE)
    {
        if (pTeMplsTeP2mpTunnelTable->MibObject.i4MplsTeP2mpTunnelStorageType ==
            pTeSetMplsTeP2mpTunnelTable->MibObject.
            i4MplsTeP2mpTunnelStorageType)
            pTeIsSetMplsTeP2mpTunnelTable->bMplsTeP2mpTunnelStorageType =
                OSIX_FALSE;
    }
    if (pTeIsSetMplsTeP2mpTunnelTable->bMplsTunnelIndex == OSIX_TRUE)
    {
        if (pTeMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelIndex ==
            pTeSetMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelIndex)
            pTeIsSetMplsTeP2mpTunnelTable->bMplsTunnelIndex = OSIX_FALSE;
    }
    if (pTeIsSetMplsTeP2mpTunnelTable->bMplsTunnelInstance == OSIX_TRUE)
    {
        if (pTeMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelInstance ==
            pTeSetMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelInstance)
            pTeIsSetMplsTeP2mpTunnelTable->bMplsTunnelInstance = OSIX_FALSE;
    }
    if (pTeIsSetMplsTeP2mpTunnelTable->bMplsTunnelIngressLSRId == OSIX_TRUE)
    {
        if (pTeMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelIngressLSRId ==
            pTeSetMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelIngressLSRId)
            pTeIsSetMplsTeP2mpTunnelTable->bMplsTunnelIngressLSRId = OSIX_FALSE;
    }
    if (pTeIsSetMplsTeP2mpTunnelTable->bMplsTunnelEgressLSRId == OSIX_TRUE)
    {
        if (pTeMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelEgressLSRId ==
            pTeSetMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelEgressLSRId)
            pTeIsSetMplsTeP2mpTunnelTable->bMplsTunnelEgressLSRId = OSIX_FALSE;
    }

    if ((pTeIsSetMplsTeP2mpTunnelTable->bMplsTeP2mpTunnelP2mpIntegrity ==
         OSIX_FALSE)
        && (pTeIsSetMplsTeP2mpTunnelTable->bMplsTeP2mpTunnelP2mpIntegrity ==
            OSIX_FALSE)
        && (pTeIsSetMplsTeP2mpTunnelTable->bMplsTeP2mpTunnelBranchRole ==
            OSIX_FALSE)
        && (pTeIsSetMplsTeP2mpTunnelTable->bMplsTeP2mpTunnelRowStatus ==
            OSIX_FALSE)
        && (pTeIsSetMplsTeP2mpTunnelTable->bMplsTeP2mpTunnelStorageType ==
            OSIX_FALSE)
        && (pTeIsSetMplsTeP2mpTunnelTable->bMplsTunnelIndex == OSIX_FALSE)
        && (pTeIsSetMplsTeP2mpTunnelTable->bMplsTunnelInstance == OSIX_FALSE)
        && (pTeIsSetMplsTeP2mpTunnelTable->bMplsTunnelIngressLSRId ==
            OSIX_FALSE)
        && (pTeIsSetMplsTeP2mpTunnelTable->bMplsTunnelEgressLSRId ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 * Function    :  TeUtilUpdateMplsTeP2mpTunnelTable
 * Input       :  The Indices
                pTeMplsTeP2mpTunnelTable
                pTeSetMplsTeP2mpTunnelTable
 * Output      :  This Routine checks set value 
                with that of the value in database
                and do the necessary protocol operation
 * Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
TeUtilUpdateMplsTeP2mpTunnelTable (tTeMplsTeP2mpTunnelTable *
                                   pTeOldMplsTeP2mpTunnelTable,
                                   tTeMplsTeP2mpTunnelTable *
                                   pTeMplsTeP2mpTunnelTable)
{

    UNUSED_PARAM (pTeOldMplsTeP2mpTunnelTable);
    UNUSED_PARAM (pTeMplsTeP2mpTunnelTable);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TeSetAllMplsTeP2mpTunnelDestTableTrigger
 Input       :  The Indices
                pTeSetMplsTeP2mpTunnelDestTable
                pTeIsSetMplsTeP2mpTunnelDestTable
 Output      :  This Routine is used to send 
                MSR and RM indication
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
TeSetAllMplsTeP2mpTunnelDestTableTrigger (tTeMplsTeP2mpTunnelDestTable *
                                          pTeSetMplsTeP2mpTunnelDestTable,
                                          tTeIsSetMplsTeP2mpTunnelDestTable *
                                          pTeIsSetMplsTeP2mpTunnelDestTable,
                                          INT4 i4SetOption)
{

    tSNMP_OCTET_STRING_TYPE MplsTeP2mpTunnelDestAddr;
    tSNMP_OCTET_STRING_TYPE MplsTeP2mpTunnelDestSrcSubGroupOrigin;
    tSNMP_OCTET_STRING_TYPE MplsTeP2mpTunnelDestSubGroupOrigin;
    tSNMP_OCTET_STRING_TYPE MplsTeP2mpTunnelBranchOutSegment;

    UINT1               au1DestAdddr[MPLS_INDEX_LENGTH];
    UINT1               au1SrcSubGroupOrigin[MPLS_INDEX_LENGTH];
    UINT1               au1SubGroupOrigin[MPLS_INDEX_LENGTH];
    UINT1               au1BranchOutSegment[MPLS_INDEX_LENGTH];

    MEMSET (au1DestAdddr, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1SrcSubGroupOrigin, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1SubGroupOrigin, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1BranchOutSegment, 0, MPLS_INDEX_LENGTH);

    MplsTeP2mpTunnelDestAddr.pu1_OctetList = au1DestAdddr;

    MEMCPY (MplsTeP2mpTunnelDestAddr.pu1_OctetList,
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            au1MplsTeP2mpTunnelDestDestination,
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestDestinationLen);

    MplsTeP2mpTunnelDestAddr.i4_Length =
        pTeSetMplsTeP2mpTunnelDestTable->MibObject.
        i4MplsTeP2mpTunnelDestDestinationLen;

    MplsTeP2mpTunnelDestSrcSubGroupOrigin.pu1_OctetList = au1SrcSubGroupOrigin;

    MEMCPY (MplsTeP2mpTunnelDestSrcSubGroupOrigin.pu1_OctetList,
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen);

    MplsTeP2mpTunnelDestSrcSubGroupOrigin.i4_Length =
        pTeSetMplsTeP2mpTunnelDestTable->MibObject.
        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen;

    MplsTeP2mpTunnelDestSubGroupOrigin.pu1_OctetList = au1SubGroupOrigin;

    MEMCPY (MplsTeP2mpTunnelDestSubGroupOrigin.pu1_OctetList,
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            au1MplsTeP2mpTunnelDestSubGroupOrigin,
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestSubGroupOriginLen);

    MplsTeP2mpTunnelDestSubGroupOrigin.i4_Length =
        pTeSetMplsTeP2mpTunnelDestTable->MibObject.
        i4MplsTeP2mpTunnelDestSubGroupOriginLen;

    if (pTeIsSetMplsTeP2mpTunnelDestTable->
        bMplsTeP2mpTunnelDestBranchOutSegment == OSIX_TRUE)
    {
        MplsTeP2mpTunnelBranchOutSegment.pu1_OctetList = au1BranchOutSegment;

        MEMCPY (MplsTeP2mpTunnelBranchOutSegment.pu1_OctetList,
                pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                au1MplsTeP2mpTunnelDestBranchOutSegment,
                pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                i4MplsTeP2mpTunnelDestBranchOutSegmentLen);

        MplsTeP2mpTunnelBranchOutSegment.i4_Length =
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestBranchOutSegmentLen;

        nmhSetCmnNew (MplsTeP2mpTunnelDestBranchOutSegment, 13, CmnLock,
                      CmnUnLock, 0, 0, 12, i4SetOption,
                      "%u %u %u %u %i %s %u %i %s %u %i %s %s",
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTunnelIndex,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTunnelInstance,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTunnelIngressLSRId,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTunnelEgressLSRId,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                      &MplsTeP2mpTunnelDestSrcSubGroupOrigin,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTeP2mpTunnelDestSrcSubGroupID,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      i4MplsTeP2mpTunnelDestSubGroupOriginType,
                      &MplsTeP2mpTunnelDestSubGroupOrigin,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTeP2mpTunnelDestSubGroupID,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      i4MplsTeP2mpTunnelDestDestinationType,
                      &MplsTeP2mpTunnelDestAddr,
                      &MplsTeP2mpTunnelBranchOutSegment);
    }
    if (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTeP2mpTunnelDestHopTableIndex ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (MplsTeP2mpTunnelDestHopTableIndex, 13, CmnLock,
                      CmnUnLock, 0, 0, 12, i4SetOption,
                      "%u %u %u %u %i %s %u %i %s %u %i %s %u",
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTunnelIndex,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTunnelInstance,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTunnelIngressLSRId,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTunnelEgressLSRId,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                      &MplsTeP2mpTunnelDestSrcSubGroupOrigin,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTeP2mpTunnelDestSrcSubGroupID,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      i4MplsTeP2mpTunnelDestSubGroupOriginType,
                      &MplsTeP2mpTunnelDestSubGroupOrigin,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTeP2mpTunnelDestSubGroupID,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      i4MplsTeP2mpTunnelDestDestinationType,
                      &MplsTeP2mpTunnelDestAddr,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTeP2mpTunnelDestHopTableIndex);
    }
    if (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTeP2mpTunnelDestPathInUse ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (MplsTeP2mpTunnelDestPathInUse, 13, CmnLock,
                      CmnUnLock, 0, 0, 12, i4SetOption,
                      "%u %u %u %u %i %s %u %i %s %u %i %s %u",
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTunnelIndex,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTunnelInstance,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTunnelIngressLSRId,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTunnelEgressLSRId,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                      &MplsTeP2mpTunnelDestSrcSubGroupOrigin,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTeP2mpTunnelDestSrcSubGroupID,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      i4MplsTeP2mpTunnelDestSubGroupOriginType,
                      &MplsTeP2mpTunnelDestSubGroupOrigin,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTeP2mpTunnelDestSubGroupID,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      i4MplsTeP2mpTunnelDestDestinationType,
                      &MplsTeP2mpTunnelDestAddr,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTeP2mpTunnelDestPathInUse);
    }
    if (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTeP2mpTunnelDestAdminStatus ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (MplsTeP2mpTunnelDestAdminStatus, 13, CmnLock,
                      CmnUnLock, 0, 0, 12, i4SetOption,
                      "%u %u %u %u %i %s %u %i %s %u %i %s %i",
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTunnelIndex,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTunnelInstance,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTunnelIngressLSRId,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTunnelEgressLSRId,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                      &MplsTeP2mpTunnelDestSrcSubGroupOrigin,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTeP2mpTunnelDestSrcSubGroupID,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      i4MplsTeP2mpTunnelDestSubGroupOriginType,
                      &MplsTeP2mpTunnelDestSubGroupOrigin,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTeP2mpTunnelDestSubGroupID,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      i4MplsTeP2mpTunnelDestDestinationType,
                      &MplsTeP2mpTunnelDestAddr,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      i4MplsTeP2mpTunnelDestAdminStatus);
    }
    if (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTeP2mpTunnelDestRowStatus ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (MplsTeP2mpTunnelDestRowStatus, 13, CmnLock,
                      CmnUnLock, 0, 1, 12, i4SetOption,
                      "%u %u %u %u %i %s %u %i %s %u %i %s %i",
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTunnelIndex,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTunnelInstance,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTunnelIngressLSRId,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTunnelEgressLSRId,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                      &MplsTeP2mpTunnelDestSrcSubGroupOrigin,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTeP2mpTunnelDestSrcSubGroupID,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      i4MplsTeP2mpTunnelDestSubGroupOriginType,
                      &MplsTeP2mpTunnelDestSubGroupOrigin,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTeP2mpTunnelDestSubGroupID,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      i4MplsTeP2mpTunnelDestDestinationType,
                      &MplsTeP2mpTunnelDestAddr,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      i4MplsTeP2mpTunnelDestRowStatus);
    }
    if (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTeP2mpTunnelDestStorageType ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (MplsTeP2mpTunnelDestStorageType, 13, CmnLock,
                      CmnUnLock, 0, 0, 12, i4SetOption,
                      "%u %u %u %u %i %s %u %i %s %u %i %s %i",
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTunnelIndex,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTunnelInstance,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTunnelIngressLSRId,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTunnelEgressLSRId,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      i4MplsTeP2mpTunnelDestSrcSubGroupOriginType,
                      &MplsTeP2mpTunnelDestSrcSubGroupOrigin,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTeP2mpTunnelDestSrcSubGroupID,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      i4MplsTeP2mpTunnelDestSubGroupOriginType,
                      &MplsTeP2mpTunnelDestSubGroupOrigin,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      u4MplsTeP2mpTunnelDestSubGroupID,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      i4MplsTeP2mpTunnelDestDestinationType,
                      &MplsTeP2mpTunnelDestAddr,
                      pTeSetMplsTeP2mpTunnelDestTable->MibObject.
                      i4MplsTeP2mpTunnelDestStorageType);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  MplsTeP2mpTunnelDestTableFilterInputs
 Input       :  The Indices
                pTeMplsTeP2mpTunnelDestTable
                pTeSetMplsTeP2mpTunnelDestTable
                pTeIsSetMplsTeP2mpTunnelDestTable
 Output      :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
MplsTeP2mpTunnelDestTableFilterInputs (tTeMplsTeP2mpTunnelDestTable *
                                       pTeMplsTeP2mpTunnelDestTable,
                                       tTeMplsTeP2mpTunnelDestTable *
                                       pTeSetMplsTeP2mpTunnelDestTable,
                                       tTeIsSetMplsTeP2mpTunnelDestTable *
                                       pTeIsSetMplsTeP2mpTunnelDestTable)
{
    if (pTeIsSetMplsTeP2mpTunnelDestTable->
        bMplsTeP2mpTunnelDestSrcSubGroupOriginType == OSIX_TRUE)
    {
        if (pTeMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestSrcSubGroupOriginType ==
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestSrcSubGroupOriginType)
            pTeIsSetMplsTeP2mpTunnelDestTable->
                bMplsTeP2mpTunnelDestSrcSubGroupOriginType = OSIX_FALSE;
    }
    if (pTeIsSetMplsTeP2mpTunnelDestTable->
        bMplsTeP2mpTunnelDestSrcSubGroupOrigin == OSIX_TRUE)
    {
        if (MEMCMP
            (pTeMplsTeP2mpTunnelDestTable->MibObject.
             au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
             pTeSetMplsTeP2mpTunnelDestTable->MibObject.
             au1MplsTeP2mpTunnelDestSrcSubGroupOrigin,
             pTeSetMplsTeP2mpTunnelDestTable->MibObject.
             i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen) == 0)
            pTeIsSetMplsTeP2mpTunnelDestTable->
                bMplsTeP2mpTunnelDestSrcSubGroupOrigin = OSIX_FALSE;
    }
    if (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTeP2mpTunnelDestSrcSubGroupID ==
        OSIX_TRUE)
    {
        if (pTeMplsTeP2mpTunnelDestTable->MibObject.
            u4MplsTeP2mpTunnelDestSrcSubGroupID ==
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            u4MplsTeP2mpTunnelDestSrcSubGroupID)
            pTeIsSetMplsTeP2mpTunnelDestTable->
                bMplsTeP2mpTunnelDestSrcSubGroupID = OSIX_FALSE;
    }
    if (pTeIsSetMplsTeP2mpTunnelDestTable->
        bMplsTeP2mpTunnelDestSubGroupOriginType == OSIX_TRUE)
    {
        if (pTeMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestSubGroupOriginType ==
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestSubGroupOriginType)
            pTeIsSetMplsTeP2mpTunnelDestTable->
                bMplsTeP2mpTunnelDestSubGroupOriginType = OSIX_FALSE;
    }
    if (pTeIsSetMplsTeP2mpTunnelDestTable->
        bMplsTeP2mpTunnelDestSubGroupOrigin == OSIX_TRUE)
    {
        if (MEMCMP
            (pTeMplsTeP2mpTunnelDestTable->MibObject.
             au1MplsTeP2mpTunnelDestSubGroupOrigin,
             pTeSetMplsTeP2mpTunnelDestTable->MibObject.
             au1MplsTeP2mpTunnelDestSubGroupOrigin,
             pTeSetMplsTeP2mpTunnelDestTable->MibObject.
             i4MplsTeP2mpTunnelDestSubGroupOriginLen) == 0)
            pTeIsSetMplsTeP2mpTunnelDestTable->
                bMplsTeP2mpTunnelDestSubGroupOrigin = OSIX_FALSE;
    }
    if (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTeP2mpTunnelDestSubGroupID ==
        OSIX_TRUE)
    {
        if (pTeMplsTeP2mpTunnelDestTable->MibObject.
            u4MplsTeP2mpTunnelDestSubGroupID ==
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            u4MplsTeP2mpTunnelDestSubGroupID)
            pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTeP2mpTunnelDestSubGroupID =
                OSIX_FALSE;
    }
    if (pTeIsSetMplsTeP2mpTunnelDestTable->
        bMplsTeP2mpTunnelDestDestinationType == OSIX_TRUE)
    {
        if (pTeMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestDestinationType ==
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestDestinationType)
            pTeIsSetMplsTeP2mpTunnelDestTable->
                bMplsTeP2mpTunnelDestDestinationType = OSIX_FALSE;
    }
    if (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTeP2mpTunnelDestDestination ==
        OSIX_TRUE)
    {
        if (MEMCMP
            (pTeMplsTeP2mpTunnelDestTable->MibObject.
             au1MplsTeP2mpTunnelDestDestination,
             pTeSetMplsTeP2mpTunnelDestTable->MibObject.
             au1MplsTeP2mpTunnelDestDestination,
             pTeSetMplsTeP2mpTunnelDestTable->MibObject.
             i4MplsTeP2mpTunnelDestDestinationLen) == 0)
            pTeIsSetMplsTeP2mpTunnelDestTable->
                bMplsTeP2mpTunnelDestDestination = OSIX_FALSE;
    }
    if (pTeIsSetMplsTeP2mpTunnelDestTable->
        bMplsTeP2mpTunnelDestBranchOutSegment == OSIX_TRUE)
    {
        if (MEMCMP
            (pTeMplsTeP2mpTunnelDestTable->MibObject.
             au1MplsTeP2mpTunnelDestBranchOutSegment,
             pTeSetMplsTeP2mpTunnelDestTable->MibObject.
             au1MplsTeP2mpTunnelDestBranchOutSegment,
             pTeSetMplsTeP2mpTunnelDestTable->MibObject.
             i4MplsTeP2mpTunnelDestBranchOutSegmentLen) == 0)
            pTeIsSetMplsTeP2mpTunnelDestTable->
                bMplsTeP2mpTunnelDestBranchOutSegment = OSIX_FALSE;
    }
    if (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTeP2mpTunnelDestHopTableIndex ==
        OSIX_TRUE)
    {
        if (pTeMplsTeP2mpTunnelDestTable->MibObject.
            u4MplsTeP2mpTunnelDestHopTableIndex ==
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            u4MplsTeP2mpTunnelDestHopTableIndex)
            pTeIsSetMplsTeP2mpTunnelDestTable->
                bMplsTeP2mpTunnelDestHopTableIndex = OSIX_FALSE;
    }
    if (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTeP2mpTunnelDestPathInUse ==
        OSIX_TRUE)
    {
        if (pTeMplsTeP2mpTunnelDestTable->MibObject.
            u4MplsTeP2mpTunnelDestPathInUse ==
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            u4MplsTeP2mpTunnelDestPathInUse)
            pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTeP2mpTunnelDestPathInUse =
                OSIX_FALSE;
    }
    if (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTeP2mpTunnelDestAdminStatus ==
        OSIX_TRUE)
    {
        if (pTeMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestAdminStatus ==
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestAdminStatus)
            pTeIsSetMplsTeP2mpTunnelDestTable->
                bMplsTeP2mpTunnelDestAdminStatus = OSIX_FALSE;
    }
    if (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTeP2mpTunnelDestRowStatus ==
        OSIX_TRUE)
    {
        if (pTeMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestRowStatus ==
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestRowStatus)
            pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTeP2mpTunnelDestRowStatus =
                OSIX_FALSE;
    }
    if (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTeP2mpTunnelDestStorageType ==
        OSIX_TRUE)
    {
        if (pTeMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestStorageType ==
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.
            i4MplsTeP2mpTunnelDestStorageType)
            pTeIsSetMplsTeP2mpTunnelDestTable->
                bMplsTeP2mpTunnelDestStorageType = OSIX_FALSE;
    }
    if (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTunnelIndex == OSIX_TRUE)
    {
        if (pTeMplsTeP2mpTunnelDestTable->MibObject.u4MplsTunnelIndex ==
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.u4MplsTunnelIndex)
            pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTunnelIndex = OSIX_FALSE;
    }
    if (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTunnelInstance == OSIX_TRUE)
    {
        if (pTeMplsTeP2mpTunnelDestTable->MibObject.u4MplsTunnelInstance ==
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.u4MplsTunnelInstance)
            pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTunnelInstance = OSIX_FALSE;
    }
    if (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTunnelIngressLSRId == OSIX_TRUE)
    {
        if (pTeMplsTeP2mpTunnelDestTable->MibObject.u4MplsTunnelIngressLSRId ==
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.u4MplsTunnelIngressLSRId)
            pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTunnelIngressLSRId =
                OSIX_FALSE;
    }
    if (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTunnelEgressLSRId == OSIX_TRUE)
    {
        if (pTeMplsTeP2mpTunnelDestTable->MibObject.u4MplsTunnelEgressLSRId ==
            pTeSetMplsTeP2mpTunnelDestTable->MibObject.u4MplsTunnelEgressLSRId)
            pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTunnelEgressLSRId =
                OSIX_FALSE;
    }

    if ((pTeIsSetMplsTeP2mpTunnelDestTable->
         bMplsTeP2mpTunnelDestSrcSubGroupOriginType == OSIX_FALSE)
        && (pTeIsSetMplsTeP2mpTunnelDestTable->
            bMplsTeP2mpTunnelDestSrcSubGroupOriginType == OSIX_FALSE)
        && (pTeIsSetMplsTeP2mpTunnelDestTable->
            bMplsTeP2mpTunnelDestSrcSubGroupOrigin == OSIX_FALSE)
        && (pTeIsSetMplsTeP2mpTunnelDestTable->
            bMplsTeP2mpTunnelDestSrcSubGroupID == OSIX_FALSE)
        && (pTeIsSetMplsTeP2mpTunnelDestTable->
            bMplsTeP2mpTunnelDestSubGroupOriginType == OSIX_FALSE)
        && (pTeIsSetMplsTeP2mpTunnelDestTable->
            bMplsTeP2mpTunnelDestSubGroupOrigin == OSIX_FALSE)
        && (pTeIsSetMplsTeP2mpTunnelDestTable->
            bMplsTeP2mpTunnelDestSubGroupID == OSIX_FALSE)
        && (pTeIsSetMplsTeP2mpTunnelDestTable->
            bMplsTeP2mpTunnelDestDestinationType == OSIX_FALSE)
        && (pTeIsSetMplsTeP2mpTunnelDestTable->
            bMplsTeP2mpTunnelDestDestination == OSIX_FALSE)
        && (pTeIsSetMplsTeP2mpTunnelDestTable->
            bMplsTeP2mpTunnelDestBranchOutSegment == OSIX_FALSE)
        && (pTeIsSetMplsTeP2mpTunnelDestTable->
            bMplsTeP2mpTunnelDestHopTableIndex == OSIX_FALSE)
        && (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTeP2mpTunnelDestPathInUse ==
            OSIX_FALSE)
        && (pTeIsSetMplsTeP2mpTunnelDestTable->
            bMplsTeP2mpTunnelDestAdminStatus == OSIX_FALSE)
        && (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTeP2mpTunnelDestRowStatus ==
            OSIX_FALSE)
        && (pTeIsSetMplsTeP2mpTunnelDestTable->
            bMplsTeP2mpTunnelDestStorageType == OSIX_FALSE)
        && (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTunnelIndex == OSIX_FALSE)
        && (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTunnelInstance ==
            OSIX_FALSE)
        && (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTunnelIngressLSRId ==
            OSIX_FALSE)
        && (pTeIsSetMplsTeP2mpTunnelDestTable->bMplsTunnelEgressLSRId ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 * Function    :  TeUtilUpdateMplsTeP2mpTunnelDestTable
 * Input       :  The Indices
                pTeMplsTeP2mpTunnelDestTable
                pTeSetMplsTeP2mpTunnelDestTable
 * Output      :  This Routine checks set value 
                with that of the value in database
                and do the necessary protocol operation
 * Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
TeUtilUpdateMplsTeP2mpTunnelDestTable (tTeMplsTeP2mpTunnelDestTable *
                                       pTeOldMplsTeP2mpTunnelDestTable,
                                       tTeMplsTeP2mpTunnelDestTable *
                                       pTeMplsTeP2mpTunnelDestTable)
{

    UNUSED_PARAM (pTeOldMplsTeP2mpTunnelDestTable);
    UNUSED_PARAM (pTeMplsTeP2mpTunnelDestTable);
    return SNMP_SUCCESS;
}

/* MPLS_P2MP_LSP_CHANGES - S */
/*******************************************************************************
* Function Name : TeAddP2mpBranchEntry
* Description   : Routine to add P2MP branch entry
* Input(s)      : pTeTnlInfo - Pointer to tunnel table entry
*                 u4OutIndex - Out-segment index
* Output(s)     : None
* Return(s)     : Pointer to branch entry or NULL
*******************************************************************************/
tP2mpBranchEntry   *
TeAddP2mpBranchEntry (tTeTnlInfo * pTeTnlInfo, UINT4 u4OutIndex)
{
    UINT2               au2BranchOffSet[TE_ONE] =
        { TE_FIELD_OFFSET (tP2mpBranchEntry,
                           u4P2mpOutSegmentIndex)
    };
    UINT2               au2BranchLength[TE_ONE] =
        { TE_FIELD_LENGTH (tP2mpBranchEntry,
                           u4P2mpOutSegmentIndex)
    };
    UINT1               au1BranchByteOrder[TE_ONE] = { HOST_ORDER };
    UINT2               u2BranchArrLen = TE_ONE;
    UINT2               u2NodeOffset = TE_ZERO;

    tTeP2mpTnlInfo     *pTeP2mpTnlInfo = NULL;
    tP2mpBranchEntry   *pTeP2mpBranchEntry = NULL;
    tOutSegment        *pOutSegment = NULL;
    tXcEntry           *pXcEntry = NULL;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeAddP2mpBranchEntry : ENTRY \n");
    if (NULL == TE_P2MP_TNL_INFO (pTeTnlInfo))
    {
        TE_DBG (TE_MAIN_FAIL, "P2MP branch entry addition : P2MP Tnl info "
                "not present \n");
        return NULL;
    }

    /* Check if out-segment entry exists */
    pOutSegment = MplsGetOutSegmentTableEntry (u4OutIndex);
    if (NULL == pOutSegment)
    {
        TE_DBG (TE_MAIN_FAIL, "P2MP branch entry addition : Out-segment entry "
                "does not exist\n");
        return NULL;
    }

    /* Check if XC entry corresponding to out-segment index exists */
    pXcEntry = (tXcEntry *) OUTSEGMENT_XC_INDEX (pOutSegment);
    if (NULL == pXcEntry)
    {
        TE_DBG (TE_MAIN_FAIL, "P2MP branch entry addition : XC entry "
                "corresponding to out-segment does not exist\n");
        return NULL;
    }

    /* Check if tunnel back pointer in XC entry is not previously associated 
     * to tunnel */
    if (XC_TNL_TBL_PTR (pXcEntry) != NULL)
    {
        TE_DBG (TE_MAIN_FAIL, "P2MP branch entry addition : Tunnel back "
                "pointer in XC entry is already associated " "to tunnel\n");
        return NULL;
    }

    pTeP2mpTnlInfo = TE_P2MP_TNL_INFO (pTeTnlInfo);
    pTeP2mpBranchEntry = (tP2mpBranchEntry *)
        TE_ALLOC_MEM_BLOCK (TE_TNL_P2MP_BRANCH_POOL_ID);
    if (NULL == pTeP2mpBranchEntry)
    {
        TE_DBG (TE_MAIN_FAIL, "P2MP branch entry addition : Failed to "
                "allocate memory to store branch info\n");
        return NULL;
    }
    INIT_TE_P2MP_BRANCH_ENTRY (pTeP2mpBranchEntry);
    TE_P2MP_BRANCH_OUTSEG_INDEX (pTeP2mpBranchEntry) = u4OutIndex;

    /* Insert in ascending order of branch out-segment index */
    if ((SLLInsertInOrder (au2BranchOffSet, au2BranchLength, au1BranchByteOrder,
                           u2BranchArrLen,
                           &(TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo)),
                           (VOID *) pTeP2mpBranchEntry,
                           u2NodeOffset)) == MPLS_FAILURE)
    {
        TE_REL_MEM_BLOCK (TE_TNL_P2MP_BRANCH_POOL_ID,
                          (UINT1 *) pTeP2mpBranchEntry);
        TE_DBG (TE_MAIN_FAIL, "P2MP branch entry addition : Failed to add "
                "branch entry into SLL\n");
        return NULL;
    }
    /* Update the Added Index */
    pTeP2mpTnlInfo->u4P2mpLastAddOrRmvdBranchIf = pOutSegment->u4IfIndex;
    /* Update P2MP tunnel branch role */
    switch (TE_P2MP_TNL_BRANCH_ROLE (pTeP2mpTnlInfo))
    {
        case TE_P2MP_BUD:
            break;

        case TE_P2MP_BRANCH:
            break;

        case TE_P2MP_NOT_BRANCH:
            if (TMO_SLL_Count (&(TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo)))
                > TE_ONE)
            {
                TE_P2MP_TNL_BRANCH_ROLE (pTeP2mpTnlInfo) = TE_P2MP_BRANCH;
            }
            break;

        default:
            KW_FALSEPOSITIVE_FIX (pTeP2mpBranchEntry);
            return NULL;
    }

    /* Update tunnel back pointer in XC entry */
    XC_TNL_TBL_PTR (pXcEntry) = pTeTnlInfo;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeAddP2mpBranchEntry : EXIT \n");
    KW_FALSEPOSITIVE_FIX (pTeP2mpBranchEntry);
    return pTeP2mpBranchEntry;
}

/*******************************************************************************
* Function Name : TeRemoveP2mpBranchEntry
* Description   : Routine to remove P2MP branch entry
* Input(s)      : pTeTnlInfo - Pointer to tunnel table entry
*                 u4OutIndex - Out-segment index
* Output(s)     : None
* Return(s)     : TE_SUCCESS or TE_FAILURE
*******************************************************************************/
UINT1
TeRemoveP2mpBranchEntry (tTeTnlInfo * pTeTnlInfo, UINT4 u4OutIndex)
{
    tTeP2mpTnlInfo     *pTeP2mpTnlInfo = NULL;
    tP2mpBranchEntry   *pTeP2mpBranchEntry = NULL;
    tOutSegment        *pOutSegment = NULL;
    tXcEntry           *pXcEntry = NULL;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeRemoveP2mpBranchEntry : ENTRY \n");
    if (NULL == TE_P2MP_TNL_INFO (pTeTnlInfo))
    {
        TE_DBG (TE_MAIN_FAIL, "P2MP branch entry removal : P2MP Tnl info "
                "not present \n");
        return TE_FAILURE;
    }

    /* Check if out-segment entry exists */
    pOutSegment = MplsGetOutSegmentTableEntry (u4OutIndex);
    if (NULL == pOutSegment)
    {
        TE_DBG (TE_MAIN_FAIL, "P2MP branch entry removal : Out-segment entry "
                "does not exist\n");
        return TE_FAILURE;
    }

    /* Check if XC entry corresponding to out-segment index exists */
    pXcEntry = (tXcEntry *) OUTSEGMENT_XC_INDEX (pOutSegment);
    if (NULL == pXcEntry)
    {
        TE_DBG (TE_MAIN_FAIL, "P2MP branch entry removal : XC entry "
                "corresponding to out-segment does not exist\n");
        return TE_FAILURE;
    }

    pTeP2mpTnlInfo = TE_P2MP_TNL_INFO (pTeTnlInfo);
    /* Check if out-segment index is already present in P2MP branch table */
    TMO_SLL_Scan (&(TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo)),
                  pTeP2mpBranchEntry, tP2mpBranchEntry *)
    {
        if (u4OutIndex == TE_P2MP_BRANCH_OUTSEG_INDEX (pTeP2mpBranchEntry))
        {
            if (TE_ZERO != TE_P2MP_BRANCH_DEST_COUNT (pTeP2mpBranchEntry))
            {
                TE_DBG (TE_MAIN_FAIL, "P2MP branch entry removal : "
                        "Out-segment associated. Cannot remove entry \n");
                return TE_FAILURE;
            }
            /* Update tunnel back pointer in XC entry */
            XC_TNL_TBL_PTR (pXcEntry) = NULL;

            TMO_SLL_Delete (&(TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo)),
                            &(pTeP2mpBranchEntry->NextP2mpBranchEntry));
            TE_REL_MEM_BLOCK (TE_TNL_P2MP_BRANCH_POOL_ID,
                              (UINT1 *) pTeP2mpBranchEntry);

            /* Update P2MP tunnel branch role */
            switch (TE_P2MP_TNL_BRANCH_ROLE (pTeP2mpTnlInfo))
            {
                case TE_P2MP_BUD:
                    break;

                case TE_P2MP_NOT_BRANCH:
                    break;

                case TE_P2MP_BRANCH:
                    if (TMO_SLL_Count
                        (&(TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo))) <=
                        TE_ONE)
                    {
                        TE_P2MP_TNL_BRANCH_ROLE (pTeP2mpTnlInfo)
                            = TE_P2MP_NOT_BRANCH;
                    }
                    break;

                default:
                    return TE_FAILURE;
            }
            /* Update the Removed Index */
            pTeP2mpTnlInfo->u4P2mpLastAddOrRmvdBranchIf =
                pOutSegment->u4IfIndex;
            /* Inform L2VPN to remove the entry */
            TePrcsL2vpnAssociation (pTeTnlInfo, TE_P2MP_LSP_BRANCH_DELETE);

            TE_DBG (TE_MAIN_ETEXT, "MAIN : TeRemoveP2mpBranchEntry : EXIT \n");
            return TE_SUCCESS;
        }
    }
    TE_DBG (TE_MAIN_FAIL, "P2MP branch entry removal : Branch "
            "entry does not exist \n");

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeRemoveP2mpBranchEntry : EXIT \n");
    return TE_FAILURE;
}

/*******************************************************************************
* Function Name : TeAddP2mpDestEntry
* Description   : Routine to add P2MP destination entry
* Input(s)      : pTeTnlInfo - Pointer to tunnel table entry
*                 u4DestId - P2MP Destination ID
* Output(s)     : None
* Return(s)     : Pointer to destination entry or NULL
*******************************************************************************/
tP2mpDestEntry     *
TeAddP2mpDestEntry (tTeTnlInfo * pTeTnlInfo, UINT4 u4DestId)
{
    UINT2               au2DestOffSet[TE_ONE] =
        { TE_FIELD_OFFSET (tP2mpDestEntry,
                           P2mpDestLsrId)
    };
    UINT2               au2DestLength[TE_ONE] =
        { TE_FIELD_LENGTH (tP2mpDestEntry,
                           P2mpDestLsrId)
    };
    UINT1               au1DestByteOrder[TE_ONE] = { HOST_ORDER };
    UINT2               u2DestArrLen = TE_ONE;
    UINT2               u2NodeOffset = TE_ZERO;

    tP2mpDestEntry     *pTeP2mpDestEntry = NULL;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeAddP2mpDestEntry : ENTRY \n");

    if (NULL == TE_P2MP_TNL_INFO (pTeTnlInfo))
    {
        TE_DBG (TE_MAIN_FAIL, "P2MP destination entry addition : P2MP Tnl info "
                "not present \n");
        return NULL;
    }
    pTeP2mpDestEntry = (tP2mpDestEntry *)
        TE_ALLOC_MEM_BLOCK (TE_TNL_P2MP_DEST_POOL_ID);
    if (NULL == pTeP2mpDestEntry)
    {
        TE_DBG (TE_MAIN_FAIL, "P2MP destination entry addition : Failed to "
                "allocate memory to store destination info\n");
        return NULL;
    }
    if ((TeInitializeMplsTeP2mpTunnelDestTable (pTeP2mpDestEntry))
        == SNMP_FAILURE)
    {
        TE_REL_MEM_BLOCK (TE_TNL_P2MP_DEST_POOL_ID, (UINT1 *) pTeP2mpDestEntry);
        TE_DBG (TE_MAIN_FAIL, "P2MP destination entry addition : Failed to "
                "initialize destination entry\n");
        return NULL;
    }
    MEMCPY ((UINT1 *) &(TE_P2MP_DEST_ADDRESS (pTeP2mpDestEntry)),
            (UINT1 *) &(u4DestId), sizeof (UINT4));
    /* Insert in ascending order of destination IP address */
    if ((SLLInsertInOrder (au2DestOffSet, au2DestLength, au1DestByteOrder,
                           u2DestArrLen,
                           &(TE_P2MP_TNL_DEST_LIST_INFO (pTeTnlInfo)),
                           (VOID *) pTeP2mpDestEntry,
                           u2NodeOffset)) == MPLS_FAILURE)
    {
        TE_REL_MEM_BLOCK (TE_TNL_P2MP_DEST_POOL_ID, (UINT1 *) pTeP2mpDestEntry);
        TE_DBG (TE_MAIN_FAIL, "P2MP destination entry addition : Failed to "
                "add destination entry into SLL\n");
        return NULL;
    }

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeAddP2mpDestEntry : EXIT \n");
    KW_FALSEPOSITIVE_FIX (pTeP2mpDestEntry);
    return pTeP2mpDestEntry;
}

/*******************************************************************************
* Function Name : TeRemoveP2mpDestEntry
* Description   : Routine to add P2MP destination entry
* Input(s)      : pTeTnlInfo - Pointer to tunnel table entry
*                 u4DestId - P2MP Destination ID
* Output(s)     : None
* Return(s)     : TE_SUCCESS or TE_FAILURE
*******************************************************************************/
UINT1
TeRemoveP2mpDestEntry (tTeTnlInfo * pTeTnlInfo, UINT4 u4DestId)
{
    tP2mpDestEntry     *pTeP2mpDestEntry = NULL;
    UINT4               u4DestIdInList = TE_ZERO;
    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeRemoveP2mpDestEntry : ENTRY \n");

    if (NULL == TE_P2MP_TNL_INFO (pTeTnlInfo))
    {
        TE_DBG (TE_MAIN_FAIL, "P2MP destination entry removal : P2MP Tnl info "
                "not present \n");
        return TE_FAILURE;
    }

    /* Check if destination is already present in P2MP destination table */
    TMO_SLL_Scan (&(TE_P2MP_TNL_DEST_LIST_INFO (pTeTnlInfo)),
                  pTeP2mpDestEntry, tP2mpDestEntry *)
    {
        CONVERT_TO_INTEGER ((TE_P2MP_DEST_ADDRESS (pTeP2mpDestEntry)),
                            u4DestIdInList);
        if (u4DestId == u4DestIdInList)
        {
            TMO_SLL_Delete (&(TE_P2MP_TNL_DEST_LIST_INFO (pTeTnlInfo)),
                            &(pTeP2mpDestEntry->NextP2mpDestEntry));
            TE_REL_MEM_BLOCK (TE_TNL_P2MP_DEST_POOL_ID,
                              (UINT1 *) pTeP2mpDestEntry);
            TE_DBG (TE_MAIN_ETEXT, "MAIN : TeRemoveP2mpDestEntry : EXIT \n");
            return TE_SUCCESS;
        }
    }
    TE_DBG (TE_MAIN_FAIL, "P2MP destination entry removal : Destination "
            "entry does not exist \n");

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeRemoveP2mpDestEntry : EXIT \n");
    return TE_FAILURE;
}

/******************************************************************************
* Function Name : TeCheckP2mpTableEntry
* Description   : To check P2MP table entry in the tunnel table
* Input(s)      : u4TunnelIndex    - Tunnel Index
*                 u4TunnelInstance - Tunnel Instance value
*                 u4TunnelIngressLSRId  - Tunnel Ingress LSR Id
*                 u4TunnelEgressLSRId - Tunnel Egress LSR Id
* Output(s)     : ppTeTnlInfo  - Pointer to  Pointer of the tunnel info
*                 whose index matches u2TunnelIndex.
* Return(s)     : TE_SUCCESS or TE_FAILURE
******************************************************************************/
UINT1
TeCheckP2mpTableEntry (UINT4 u4TunnelIndex, UINT4 u4TunnelInstance,
                       UINT4 u4TnlIngressLSRId, UINT4 u4TnlEgressLSRId,
                       tTeTnlInfo ** ppTeTnlInfo)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCheckP2mpTableEntry : ENTRY \n");

    pTeTnlInfo = TeGetTunnelInfo (u4TunnelIndex, u4TunnelInstance,
                                  u4TnlIngressLSRId, u4TnlEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        if (TE_TNL_TYPE_P2MP & TE_TNL_TYPE (pTeTnlInfo))
        {
            *ppTeTnlInfo = pTeTnlInfo;
            TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCheckP2mpTableEntry : EXIT \n");
            return TE_SUCCESS;
        }
    }
    *ppTeTnlInfo = NULL;
    TE_DBG (TE_MAIN_FAIL, "P2MP table entry not found \n");
    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCheckP2mpTableEntry : EXIT \n");
    return TE_FAILURE;
}

/* MPLS_P2MP_LSP_CHANGES - E */

/******************************** end of tep2mputlg.c *************************/
