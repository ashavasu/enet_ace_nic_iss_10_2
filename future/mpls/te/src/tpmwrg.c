/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: tpmwrg.c,v 1.1 2010/11/16 07:29:17 prabuc Exp $
*
* Description: This file contains utility functions used by protocol 
*********************************************************************/

#include "lr.h"
#include "fssnmp.h"
#include "tpmtdfsg.h"
#include "tpmlwg.h"
#include "tpmwrg.h"
#include "tpmdb.h"

VOID
RegisterP2MP ()
{
    SNMPRegisterMib (&fsp2mpOID, &fsp2mpEntry, SNMP_MSR_TGR_TRUE);
    SNMPAddSysorEntry (&fsp2mpOID, (const UINT1 *) "p2mp");
}

VOID
UnRegisterP2MP ()
{
    SNMPUnRegisterMib (&fsp2mpOID, &fsp2mpEntry);
    SNMPDelSysorEntry (&fsp2mpOID, (const UINT1 *) "p2mp");
}

INT4
MplsTeP2mpTunnelConfiguredGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetMplsTeP2mpTunnelConfigured (&(pMultiData->u4_ULongValue)));
}

INT4
MplsTeP2mpTunnelActiveGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetMplsTeP2mpTunnelActive (&(pMultiData->u4_ULongValue)));
}

INT4
MplsTeP2mpTunnelTotalMaxHopsGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetMplsTeP2mpTunnelTotalMaxHops (&(pMultiData->u4_ULongValue)));
}

INT4
GetNextIndexMplsTeP2mpTunnelTable (tSnmpIndex * pFirstMultiIndex,
                                   tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMplsTeP2mpTunnelTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMplsTeP2mpTunnelTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue)) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
MplsTeP2mpTunnelP2mpIntegrityGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsTeP2mpTunnelP2mpIntegrity
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsTeP2mpTunnelBranchRoleGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsTeP2mpTunnelBranchRole
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsTeP2mpTunnelP2mpXcIndexGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsTeP2mpTunnelP2mpXcIndex
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue, pMultiData->pOctetStrValue));

}

INT4
MplsTeP2mpTunnelRowStatusGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsTeP2mpTunnelRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsTeP2mpTunnelStorageTypeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsTeP2mpTunnelStorageType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsTeP2mpTunnelP2mpIntegrityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2MplsTeP2mpTunnelP2mpIntegrity (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[1].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[2].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[3].
                                                    u4_ULongValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
MplsTeP2mpTunnelBranchRoleTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                tRetVal * pMultiData)
{
    return (nmhTestv2MplsTeP2mpTunnelBranchRole (pu4Error,
                                                 pMultiIndex->pIndex[0].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[1].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[2].
                                                 u4_ULongValue,
                                                 pMultiIndex->pIndex[3].
                                                 u4_ULongValue,
                                                 pMultiData->i4_SLongValue));

}

INT4
MplsTeP2mpTunnelRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                               tRetVal * pMultiData)
{
    return (nmhTestv2MplsTeP2mpTunnelRowStatus (pu4Error,
                                                pMultiIndex->pIndex[0].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[1].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[2].
                                                u4_ULongValue,
                                                pMultiIndex->pIndex[3].
                                                u4_ULongValue,
                                                pMultiData->i4_SLongValue));

}

INT4
MplsTeP2mpTunnelStorageTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                 tRetVal * pMultiData)
{
    return (nmhTestv2MplsTeP2mpTunnelStorageType (pu4Error,
                                                  pMultiIndex->pIndex[0].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[1].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[2].
                                                  u4_ULongValue,
                                                  pMultiIndex->pIndex[3].
                                                  u4_ULongValue,
                                                  pMultiData->i4_SLongValue));

}

INT4
MplsTeP2mpTunnelP2mpIntegritySet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetMplsTeP2mpTunnelP2mpIntegrity
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
MplsTeP2mpTunnelBranchRoleSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsTeP2mpTunnelBranchRole
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
MplsTeP2mpTunnelRowStatusSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsTeP2mpTunnelRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
MplsTeP2mpTunnelStorageTypeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    return (nmhSetMplsTeP2mpTunnelStorageType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue, pMultiData->i4_SLongValue));

}

INT4
MplsTeP2mpTunnelTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MplsTeP2mpTunnelTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
MplsTeP2mpTunnelSubGroupIDNextGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetMplsTeP2mpTunnelSubGroupIDNext
            (&(pMultiData->u4_ULongValue)));
}

INT4
GetNextIndexMplsTeP2mpTunnelDestTable (tSnmpIndex * pFirstMultiIndex,
                                       tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMplsTeP2mpTunnelDestTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             &(pNextMultiIndex->pIndex[4].i4_SLongValue),
             pNextMultiIndex->pIndex[5].pOctetStrValue,
             &(pNextMultiIndex->pIndex[6].u4_ULongValue),
             &(pNextMultiIndex->pIndex[7].i4_SLongValue),
             pNextMultiIndex->pIndex[8].pOctetStrValue,
             &(pNextMultiIndex->pIndex[9].u4_ULongValue),
             &(pNextMultiIndex->pIndex[10].i4_SLongValue),
             pNextMultiIndex->pIndex[11].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMplsTeP2mpTunnelDestTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             pFirstMultiIndex->pIndex[4].i4_SLongValue,
             &(pNextMultiIndex->pIndex[4].i4_SLongValue),
             pFirstMultiIndex->pIndex[5].pOctetStrValue,
             pNextMultiIndex->pIndex[5].pOctetStrValue,
             pFirstMultiIndex->pIndex[6].u4_ULongValue,
             &(pNextMultiIndex->pIndex[6].u4_ULongValue),
             pFirstMultiIndex->pIndex[7].i4_SLongValue,
             &(pNextMultiIndex->pIndex[7].i4_SLongValue),
             pFirstMultiIndex->pIndex[8].pOctetStrValue,
             pNextMultiIndex->pIndex[8].pOctetStrValue,
             pFirstMultiIndex->pIndex[9].u4_ULongValue,
             &(pNextMultiIndex->pIndex[9].u4_ULongValue),
             pFirstMultiIndex->pIndex[10].i4_SLongValue,
             &(pNextMultiIndex->pIndex[10].i4_SLongValue),
             pFirstMultiIndex->pIndex[11].pOctetStrValue,
             pNextMultiIndex->pIndex[11].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
MplsTeP2mpTunnelDestBranchOutSegmentGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue,
         pMultiIndex->pIndex[6].u4_ULongValue,
         pMultiIndex->pIndex[7].i4_SLongValue,
         pMultiIndex->pIndex[8].pOctetStrValue,
         pMultiIndex->pIndex[9].u4_ULongValue,
         pMultiIndex->pIndex[10].i4_SLongValue,
         pMultiIndex->pIndex[11].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsTeP2mpTunnelDestBranchOutSegment
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiIndex->pIndex[6].u4_ULongValue,
             pMultiIndex->pIndex[7].i4_SLongValue,
             pMultiIndex->pIndex[8].pOctetStrValue,
             pMultiIndex->pIndex[9].u4_ULongValue,
             pMultiIndex->pIndex[10].i4_SLongValue,
             pMultiIndex->pIndex[11].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
MplsTeP2mpTunnelDestHopTableIndexGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue,
         pMultiIndex->pIndex[6].u4_ULongValue,
         pMultiIndex->pIndex[7].i4_SLongValue,
         pMultiIndex->pIndex[8].pOctetStrValue,
         pMultiIndex->pIndex[9].u4_ULongValue,
         pMultiIndex->pIndex[10].i4_SLongValue,
         pMultiIndex->pIndex[11].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsTeP2mpTunnelDestHopTableIndex
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiIndex->pIndex[6].u4_ULongValue,
             pMultiIndex->pIndex[7].i4_SLongValue,
             pMultiIndex->pIndex[8].pOctetStrValue,
             pMultiIndex->pIndex[9].u4_ULongValue,
             pMultiIndex->pIndex[10].i4_SLongValue,
             pMultiIndex->pIndex[11].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsTeP2mpTunnelDestPathInUseGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue,
         pMultiIndex->pIndex[6].u4_ULongValue,
         pMultiIndex->pIndex[7].i4_SLongValue,
         pMultiIndex->pIndex[8].pOctetStrValue,
         pMultiIndex->pIndex[9].u4_ULongValue,
         pMultiIndex->pIndex[10].i4_SLongValue,
         pMultiIndex->pIndex[11].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsTeP2mpTunnelDestPathInUse
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiIndex->pIndex[6].u4_ULongValue,
             pMultiIndex->pIndex[7].i4_SLongValue,
             pMultiIndex->pIndex[8].pOctetStrValue,
             pMultiIndex->pIndex[9].u4_ULongValue,
             pMultiIndex->pIndex[10].i4_SLongValue,
             pMultiIndex->pIndex[11].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsTeP2mpTunnelDestCHopTableIndexGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue,
         pMultiIndex->pIndex[6].u4_ULongValue,
         pMultiIndex->pIndex[7].i4_SLongValue,
         pMultiIndex->pIndex[8].pOctetStrValue,
         pMultiIndex->pIndex[9].u4_ULongValue,
         pMultiIndex->pIndex[10].i4_SLongValue,
         pMultiIndex->pIndex[11].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsTeP2mpTunnelDestCHopTableIndex
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiIndex->pIndex[6].u4_ULongValue,
             pMultiIndex->pIndex[7].i4_SLongValue,
             pMultiIndex->pIndex[8].pOctetStrValue,
             pMultiIndex->pIndex[9].u4_ULongValue,
             pMultiIndex->pIndex[10].i4_SLongValue,
             pMultiIndex->pIndex[11].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsTeP2mpTunnelDestARHopTableIndexGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue,
         pMultiIndex->pIndex[6].u4_ULongValue,
         pMultiIndex->pIndex[7].i4_SLongValue,
         pMultiIndex->pIndex[8].pOctetStrValue,
         pMultiIndex->pIndex[9].u4_ULongValue,
         pMultiIndex->pIndex[10].i4_SLongValue,
         pMultiIndex->pIndex[11].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsTeP2mpTunnelDestARHopTableIndex
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiIndex->pIndex[6].u4_ULongValue,
             pMultiIndex->pIndex[7].i4_SLongValue,
             pMultiIndex->pIndex[8].pOctetStrValue,
             pMultiIndex->pIndex[9].u4_ULongValue,
             pMultiIndex->pIndex[10].i4_SLongValue,
             pMultiIndex->pIndex[11].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsTeP2mpTunnelDestTotalUpTimeGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue,
         pMultiIndex->pIndex[6].u4_ULongValue,
         pMultiIndex->pIndex[7].i4_SLongValue,
         pMultiIndex->pIndex[8].pOctetStrValue,
         pMultiIndex->pIndex[9].u4_ULongValue,
         pMultiIndex->pIndex[10].i4_SLongValue,
         pMultiIndex->pIndex[11].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsTeP2mpTunnelDestTotalUpTime
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiIndex->pIndex[6].u4_ULongValue,
             pMultiIndex->pIndex[7].i4_SLongValue,
             pMultiIndex->pIndex[8].pOctetStrValue,
             pMultiIndex->pIndex[9].u4_ULongValue,
             pMultiIndex->pIndex[10].i4_SLongValue,
             pMultiIndex->pIndex[11].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsTeP2mpTunnelDestInstanceUpTimeGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue,
         pMultiIndex->pIndex[6].u4_ULongValue,
         pMultiIndex->pIndex[7].i4_SLongValue,
         pMultiIndex->pIndex[8].pOctetStrValue,
         pMultiIndex->pIndex[9].u4_ULongValue,
         pMultiIndex->pIndex[10].i4_SLongValue,
         pMultiIndex->pIndex[11].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsTeP2mpTunnelDestInstanceUpTime
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiIndex->pIndex[6].u4_ULongValue,
             pMultiIndex->pIndex[7].i4_SLongValue,
             pMultiIndex->pIndex[8].pOctetStrValue,
             pMultiIndex->pIndex[9].u4_ULongValue,
             pMultiIndex->pIndex[10].i4_SLongValue,
             pMultiIndex->pIndex[11].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsTeP2mpTunnelDestPathChangesGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue,
         pMultiIndex->pIndex[6].u4_ULongValue,
         pMultiIndex->pIndex[7].i4_SLongValue,
         pMultiIndex->pIndex[8].pOctetStrValue,
         pMultiIndex->pIndex[9].u4_ULongValue,
         pMultiIndex->pIndex[10].i4_SLongValue,
         pMultiIndex->pIndex[11].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsTeP2mpTunnelDestPathChanges
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiIndex->pIndex[6].u4_ULongValue,
             pMultiIndex->pIndex[7].i4_SLongValue,
             pMultiIndex->pIndex[8].pOctetStrValue,
             pMultiIndex->pIndex[9].u4_ULongValue,
             pMultiIndex->pIndex[10].i4_SLongValue,
             pMultiIndex->pIndex[11].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsTeP2mpTunnelDestLastPathChangeGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue,
         pMultiIndex->pIndex[6].u4_ULongValue,
         pMultiIndex->pIndex[7].i4_SLongValue,
         pMultiIndex->pIndex[8].pOctetStrValue,
         pMultiIndex->pIndex[9].u4_ULongValue,
         pMultiIndex->pIndex[10].i4_SLongValue,
         pMultiIndex->pIndex[11].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsTeP2mpTunnelDestLastPathChange
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiIndex->pIndex[6].u4_ULongValue,
             pMultiIndex->pIndex[7].i4_SLongValue,
             pMultiIndex->pIndex[8].pOctetStrValue,
             pMultiIndex->pIndex[9].u4_ULongValue,
             pMultiIndex->pIndex[10].i4_SLongValue,
             pMultiIndex->pIndex[11].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsTeP2mpTunnelDestCreationTimeGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue,
         pMultiIndex->pIndex[6].u4_ULongValue,
         pMultiIndex->pIndex[7].i4_SLongValue,
         pMultiIndex->pIndex[8].pOctetStrValue,
         pMultiIndex->pIndex[9].u4_ULongValue,
         pMultiIndex->pIndex[10].i4_SLongValue,
         pMultiIndex->pIndex[11].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsTeP2mpTunnelDestCreationTime
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiIndex->pIndex[6].u4_ULongValue,
             pMultiIndex->pIndex[7].i4_SLongValue,
             pMultiIndex->pIndex[8].pOctetStrValue,
             pMultiIndex->pIndex[9].u4_ULongValue,
             pMultiIndex->pIndex[10].i4_SLongValue,
             pMultiIndex->pIndex[11].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsTeP2mpTunnelDestStateTransitionsGet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue,
         pMultiIndex->pIndex[6].u4_ULongValue,
         pMultiIndex->pIndex[7].i4_SLongValue,
         pMultiIndex->pIndex[8].pOctetStrValue,
         pMultiIndex->pIndex[9].u4_ULongValue,
         pMultiIndex->pIndex[10].i4_SLongValue,
         pMultiIndex->pIndex[11].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsTeP2mpTunnelDestStateTransitions
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiIndex->pIndex[6].u4_ULongValue,
             pMultiIndex->pIndex[7].i4_SLongValue,
             pMultiIndex->pIndex[8].pOctetStrValue,
             pMultiIndex->pIndex[9].u4_ULongValue,
             pMultiIndex->pIndex[10].i4_SLongValue,
             pMultiIndex->pIndex[11].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsTeP2mpTunnelDestDiscontinuityTimeGet (tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue,
         pMultiIndex->pIndex[6].u4_ULongValue,
         pMultiIndex->pIndex[7].i4_SLongValue,
         pMultiIndex->pIndex[8].pOctetStrValue,
         pMultiIndex->pIndex[9].u4_ULongValue,
         pMultiIndex->pIndex[10].i4_SLongValue,
         pMultiIndex->pIndex[11].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsTeP2mpTunnelDestDiscontinuityTime
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiIndex->pIndex[6].u4_ULongValue,
             pMultiIndex->pIndex[7].i4_SLongValue,
             pMultiIndex->pIndex[8].pOctetStrValue,
             pMultiIndex->pIndex[9].u4_ULongValue,
             pMultiIndex->pIndex[10].i4_SLongValue,
             pMultiIndex->pIndex[11].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsTeP2mpTunnelDestAdminStatusGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue,
         pMultiIndex->pIndex[6].u4_ULongValue,
         pMultiIndex->pIndex[7].i4_SLongValue,
         pMultiIndex->pIndex[8].pOctetStrValue,
         pMultiIndex->pIndex[9].u4_ULongValue,
         pMultiIndex->pIndex[10].i4_SLongValue,
         pMultiIndex->pIndex[11].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsTeP2mpTunnelDestAdminStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiIndex->pIndex[6].u4_ULongValue,
             pMultiIndex->pIndex[7].i4_SLongValue,
             pMultiIndex->pIndex[8].pOctetStrValue,
             pMultiIndex->pIndex[9].u4_ULongValue,
             pMultiIndex->pIndex[10].i4_SLongValue,
             pMultiIndex->pIndex[11].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsTeP2mpTunnelDestOperStatusGet (tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue,
         pMultiIndex->pIndex[6].u4_ULongValue,
         pMultiIndex->pIndex[7].i4_SLongValue,
         pMultiIndex->pIndex[8].pOctetStrValue,
         pMultiIndex->pIndex[9].u4_ULongValue,
         pMultiIndex->pIndex[10].i4_SLongValue,
         pMultiIndex->pIndex[11].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsTeP2mpTunnelDestOperStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiIndex->pIndex[6].u4_ULongValue,
             pMultiIndex->pIndex[7].i4_SLongValue,
             pMultiIndex->pIndex[8].pOctetStrValue,
             pMultiIndex->pIndex[9].u4_ULongValue,
             pMultiIndex->pIndex[10].i4_SLongValue,
             pMultiIndex->pIndex[11].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsTeP2mpTunnelDestRowStatusGet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue,
         pMultiIndex->pIndex[6].u4_ULongValue,
         pMultiIndex->pIndex[7].i4_SLongValue,
         pMultiIndex->pIndex[8].pOctetStrValue,
         pMultiIndex->pIndex[9].u4_ULongValue,
         pMultiIndex->pIndex[10].i4_SLongValue,
         pMultiIndex->pIndex[11].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsTeP2mpTunnelDestRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiIndex->pIndex[6].u4_ULongValue,
             pMultiIndex->pIndex[7].i4_SLongValue,
             pMultiIndex->pIndex[8].pOctetStrValue,
             pMultiIndex->pIndex[9].u4_ULongValue,
             pMultiIndex->pIndex[10].i4_SLongValue,
             pMultiIndex->pIndex[11].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsTeP2mpTunnelDestStorageTypeGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].i4_SLongValue,
         pMultiIndex->pIndex[5].pOctetStrValue,
         pMultiIndex->pIndex[6].u4_ULongValue,
         pMultiIndex->pIndex[7].i4_SLongValue,
         pMultiIndex->pIndex[8].pOctetStrValue,
         pMultiIndex->pIndex[9].u4_ULongValue,
         pMultiIndex->pIndex[10].i4_SLongValue,
         pMultiIndex->pIndex[11].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsTeP2mpTunnelDestStorageType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiIndex->pIndex[6].u4_ULongValue,
             pMultiIndex->pIndex[7].i4_SLongValue,
             pMultiIndex->pIndex[8].pOctetStrValue,
             pMultiIndex->pIndex[9].u4_ULongValue,
             pMultiIndex->pIndex[10].i4_SLongValue,
             pMultiIndex->pIndex[11].pOctetStrValue,
             &(pMultiData->i4_SLongValue)));

}

INT4
MplsTeP2mpTunnelDestBranchOutSegmentTest (UINT4 *pu4Error,
                                          tSnmpIndex * pMultiIndex,
                                          tRetVal * pMultiData)
{
    return (nmhTestv2MplsTeP2mpTunnelDestBranchOutSegment (pu4Error,
                                                           pMultiIndex->
                                                           pIndex[0].
                                                           u4_ULongValue,
                                                           pMultiIndex->
                                                           pIndex[1].
                                                           u4_ULongValue,
                                                           pMultiIndex->
                                                           pIndex[2].
                                                           u4_ULongValue,
                                                           pMultiIndex->
                                                           pIndex[3].
                                                           u4_ULongValue,
                                                           pMultiIndex->
                                                           pIndex[4].
                                                           i4_SLongValue,
                                                           pMultiIndex->
                                                           pIndex[5].
                                                           pOctetStrValue,
                                                           pMultiIndex->
                                                           pIndex[6].
                                                           u4_ULongValue,
                                                           pMultiIndex->
                                                           pIndex[7].
                                                           i4_SLongValue,
                                                           pMultiIndex->
                                                           pIndex[8].
                                                           pOctetStrValue,
                                                           pMultiIndex->
                                                           pIndex[9].
                                                           u4_ULongValue,
                                                           pMultiIndex->
                                                           pIndex[10].
                                                           i4_SLongValue,
                                                           pMultiIndex->
                                                           pIndex[11].
                                                           pOctetStrValue,
                                                           pMultiData->
                                                           pOctetStrValue));

}

INT4
MplsTeP2mpTunnelDestHopTableIndexTest (UINT4 *pu4Error,
                                       tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    return (nmhTestv2MplsTeP2mpTunnelDestHopTableIndex (pu4Error,
                                                        pMultiIndex->pIndex[0].
                                                        u4_ULongValue,
                                                        pMultiIndex->pIndex[1].
                                                        u4_ULongValue,
                                                        pMultiIndex->pIndex[2].
                                                        u4_ULongValue,
                                                        pMultiIndex->pIndex[3].
                                                        u4_ULongValue,
                                                        pMultiIndex->pIndex[4].
                                                        i4_SLongValue,
                                                        pMultiIndex->pIndex[5].
                                                        pOctetStrValue,
                                                        pMultiIndex->pIndex[6].
                                                        u4_ULongValue,
                                                        pMultiIndex->pIndex[7].
                                                        i4_SLongValue,
                                                        pMultiIndex->pIndex[8].
                                                        pOctetStrValue,
                                                        pMultiIndex->pIndex[9].
                                                        u4_ULongValue,
                                                        pMultiIndex->pIndex[10].
                                                        i4_SLongValue,
                                                        pMultiIndex->pIndex[11].
                                                        pOctetStrValue,
                                                        pMultiData->
                                                        u4_ULongValue));

}

INT4
MplsTeP2mpTunnelDestPathInUseTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2MplsTeP2mpTunnelDestPathInUse (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[1].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[2].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[3].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[4].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[5].
                                                    pOctetStrValue,
                                                    pMultiIndex->pIndex[6].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[7].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[8].
                                                    pOctetStrValue,
                                                    pMultiIndex->pIndex[9].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[10].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[11].
                                                    pOctetStrValue,
                                                    pMultiData->u4_ULongValue));

}

INT4
MplsTeP2mpTunnelDestAdminStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2MplsTeP2mpTunnelDestAdminStatus (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      u4_ULongValue,
                                                      pMultiIndex->pIndex[1].
                                                      u4_ULongValue,
                                                      pMultiIndex->pIndex[2].
                                                      u4_ULongValue,
                                                      pMultiIndex->pIndex[3].
                                                      u4_ULongValue,
                                                      pMultiIndex->pIndex[4].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[5].
                                                      pOctetStrValue,
                                                      pMultiIndex->pIndex[6].
                                                      u4_ULongValue,
                                                      pMultiIndex->pIndex[7].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[8].
                                                      pOctetStrValue,
                                                      pMultiIndex->pIndex[9].
                                                      u4_ULongValue,
                                                      pMultiIndex->pIndex[10].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[11].
                                                      pOctetStrValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
MplsTeP2mpTunnelDestRowStatusTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                   tRetVal * pMultiData)
{
    return (nmhTestv2MplsTeP2mpTunnelDestRowStatus (pu4Error,
                                                    pMultiIndex->pIndex[0].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[1].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[2].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[3].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[4].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[5].
                                                    pOctetStrValue,
                                                    pMultiIndex->pIndex[6].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[7].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[8].
                                                    pOctetStrValue,
                                                    pMultiIndex->pIndex[9].
                                                    u4_ULongValue,
                                                    pMultiIndex->pIndex[10].
                                                    i4_SLongValue,
                                                    pMultiIndex->pIndex[11].
                                                    pOctetStrValue,
                                                    pMultiData->i4_SLongValue));

}

INT4
MplsTeP2mpTunnelDestStorageTypeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    return (nmhTestv2MplsTeP2mpTunnelDestStorageType (pu4Error,
                                                      pMultiIndex->pIndex[0].
                                                      u4_ULongValue,
                                                      pMultiIndex->pIndex[1].
                                                      u4_ULongValue,
                                                      pMultiIndex->pIndex[2].
                                                      u4_ULongValue,
                                                      pMultiIndex->pIndex[3].
                                                      u4_ULongValue,
                                                      pMultiIndex->pIndex[4].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[5].
                                                      pOctetStrValue,
                                                      pMultiIndex->pIndex[6].
                                                      u4_ULongValue,
                                                      pMultiIndex->pIndex[7].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[8].
                                                      pOctetStrValue,
                                                      pMultiIndex->pIndex[9].
                                                      u4_ULongValue,
                                                      pMultiIndex->pIndex[10].
                                                      i4_SLongValue,
                                                      pMultiIndex->pIndex[11].
                                                      pOctetStrValue,
                                                      pMultiData->
                                                      i4_SLongValue));

}

INT4
MplsTeP2mpTunnelDestBranchOutSegmentSet (tSnmpIndex * pMultiIndex,
                                         tRetVal * pMultiData)
{
    return (nmhSetMplsTeP2mpTunnelDestBranchOutSegment
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiIndex->pIndex[6].u4_ULongValue,
             pMultiIndex->pIndex[7].i4_SLongValue,
             pMultiIndex->pIndex[8].pOctetStrValue,
             pMultiIndex->pIndex[9].u4_ULongValue,
             pMultiIndex->pIndex[10].i4_SLongValue,
             pMultiIndex->pIndex[11].pOctetStrValue,
             pMultiData->pOctetStrValue));

}

INT4
MplsTeP2mpTunnelDestHopTableIndexSet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    return (nmhSetMplsTeP2mpTunnelDestHopTableIndex
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiIndex->pIndex[6].u4_ULongValue,
             pMultiIndex->pIndex[7].i4_SLongValue,
             pMultiIndex->pIndex[8].pOctetStrValue,
             pMultiIndex->pIndex[9].u4_ULongValue,
             pMultiIndex->pIndex[10].i4_SLongValue,
             pMultiIndex->pIndex[11].pOctetStrValue,
             pMultiData->u4_ULongValue));

}

INT4
MplsTeP2mpTunnelDestPathInUseSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetMplsTeP2mpTunnelDestPathInUse
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiIndex->pIndex[6].u4_ULongValue,
             pMultiIndex->pIndex[7].i4_SLongValue,
             pMultiIndex->pIndex[8].pOctetStrValue,
             pMultiIndex->pIndex[9].u4_ULongValue,
             pMultiIndex->pIndex[10].i4_SLongValue,
             pMultiIndex->pIndex[11].pOctetStrValue,
             pMultiData->u4_ULongValue));

}

INT4
MplsTeP2mpTunnelDestAdminStatusSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetMplsTeP2mpTunnelDestAdminStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiIndex->pIndex[6].u4_ULongValue,
             pMultiIndex->pIndex[7].i4_SLongValue,
             pMultiIndex->pIndex[8].pOctetStrValue,
             pMultiIndex->pIndex[9].u4_ULongValue,
             pMultiIndex->pIndex[10].i4_SLongValue,
             pMultiIndex->pIndex[11].pOctetStrValue,
             pMultiData->i4_SLongValue));

}

INT4
MplsTeP2mpTunnelDestRowStatusSet (tSnmpIndex * pMultiIndex,
                                  tRetVal * pMultiData)
{
    return (nmhSetMplsTeP2mpTunnelDestRowStatus
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiIndex->pIndex[6].u4_ULongValue,
             pMultiIndex->pIndex[7].i4_SLongValue,
             pMultiIndex->pIndex[8].pOctetStrValue,
             pMultiIndex->pIndex[9].u4_ULongValue,
             pMultiIndex->pIndex[10].i4_SLongValue,
             pMultiIndex->pIndex[11].pOctetStrValue,
             pMultiData->i4_SLongValue));

}

INT4
MplsTeP2mpTunnelDestStorageTypeSet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    return (nmhSetMplsTeP2mpTunnelDestStorageType
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].i4_SLongValue,
             pMultiIndex->pIndex[5].pOctetStrValue,
             pMultiIndex->pIndex[6].u4_ULongValue,
             pMultiIndex->pIndex[7].i4_SLongValue,
             pMultiIndex->pIndex[8].pOctetStrValue,
             pMultiIndex->pIndex[9].u4_ULongValue,
             pMultiIndex->pIndex[10].i4_SLongValue,
             pMultiIndex->pIndex[11].pOctetStrValue,
             pMultiData->i4_SLongValue));

}

INT4
MplsTeP2mpTunnelDestTableDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MplsTeP2mpTunnelDestTable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
GetNextIndexMplsTeP2mpTunnelBranchPerfTable (tSnmpIndex * pFirstMultiIndex,
                                             tSnmpIndex * pNextMultiIndex)
{
    if (pFirstMultiIndex == NULL)
    {
        if (nmhGetFirstIndexMplsTeP2mpTunnelBranchPerfTable
            (&(pNextMultiIndex->pIndex[0].u4_ULongValue),
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             pNextMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }
    else
    {
        if (nmhGetNextIndexMplsTeP2mpTunnelBranchPerfTable
            (pFirstMultiIndex->pIndex[0].u4_ULongValue,
             &(pNextMultiIndex->pIndex[0].u4_ULongValue),
             pFirstMultiIndex->pIndex[1].u4_ULongValue,
             &(pNextMultiIndex->pIndex[1].u4_ULongValue),
             pFirstMultiIndex->pIndex[2].u4_ULongValue,
             &(pNextMultiIndex->pIndex[2].u4_ULongValue),
             pFirstMultiIndex->pIndex[3].u4_ULongValue,
             &(pNextMultiIndex->pIndex[3].u4_ULongValue),
             pFirstMultiIndex->pIndex[4].pOctetStrValue,
             pNextMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

INT4
MplsTeP2mpTunnelBranchPerfPacketsGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelBranchPerfTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsTeP2mpTunnelBranchPerfPackets
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsTeP2mpTunnelBranchPerfHCPacketsGet (tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelBranchPerfTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsTeP2mpTunnelBranchPerfHCPackets
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].pOctetStrValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
MplsTeP2mpTunnelBranchPerfErrorsGet (tSnmpIndex * pMultiIndex,
                                     tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelBranchPerfTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsTeP2mpTunnelBranchPerfErrors
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsTeP2mpTunnelBranchPerfBytesGet (tSnmpIndex * pMultiIndex,
                                    tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelBranchPerfTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsTeP2mpTunnelBranchPerfBytes
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsTeP2mpTunnelBranchPerfHCBytesGet (tSnmpIndex * pMultiIndex,
                                      tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelBranchPerfTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsTeP2mpTunnelBranchPerfHCBytes
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].pOctetStrValue,
             &(pMultiData->u8_Counter64Value)));

}

INT4
MplsTeP2mpTunnelBranchDiscontinuityTimeGet (tSnmpIndex * pMultiIndex,
                                            tRetVal * pMultiData)
{
    if (nmhValidateIndexInstanceMplsTeP2mpTunnelBranchPerfTable
        (pMultiIndex->pIndex[0].u4_ULongValue,
         pMultiIndex->pIndex[1].u4_ULongValue,
         pMultiIndex->pIndex[2].u4_ULongValue,
         pMultiIndex->pIndex[3].u4_ULongValue,
         pMultiIndex->pIndex[4].pOctetStrValue) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return (nmhGetMplsTeP2mpTunnelBranchDiscontinuityTime
            (pMultiIndex->pIndex[0].u4_ULongValue,
             pMultiIndex->pIndex[1].u4_ULongValue,
             pMultiIndex->pIndex[2].u4_ULongValue,
             pMultiIndex->pIndex[3].u4_ULongValue,
             pMultiIndex->pIndex[4].pOctetStrValue,
             &(pMultiData->u4_ULongValue)));

}

INT4
MplsTeP2mpTunnelNotificationEnableGet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetMplsTeP2mpTunnelNotificationEnable
            (&(pMultiData->i4_SLongValue)));
}

INT4
MplsTeP2mpTunnelNotificationEnableTest (UINT4 *pu4Error,
                                        tSnmpIndex * pMultiIndex,
                                        tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2MplsTeP2mpTunnelNotificationEnable
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
MplsTeP2mpTunnelNotificationEnableSet (tSnmpIndex * pMultiIndex,
                                       tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetMplsTeP2mpTunnelNotificationEnable
            (pMultiData->i4_SLongValue));
}

INT4
MplsTeP2mpTunnelNotificationEnableDep (UINT4 *pu4Error,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2MplsTeP2mpTunnelNotificationEnable
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
