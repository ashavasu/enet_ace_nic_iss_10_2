/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: teset2.c,v 1.27 2015/06/26 05:14:56 siva Exp $
 *
 * Description: This file contains the low level SET routines
 *              for the following TE MIB tables.
 *              - MplsTunnelHopTable
 *              - MplsTunnelResourceTable
 *              - MplsTunnelCRLDPResTable
 *              - FsMplsTunnelCRLDPResTable
 *              - FsMplsTunnelRSVPResTable
 *******************************************************************/

#include "teincs.h"
#include "fsmplslw.h"
#include "stdtelw.h"

/* LOW LEVEL Routines for Table : MplsTunnelHopTable. */

/****************************************************************************
 Function    :  nmhSetMplsTunnelHopAddrType
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                setValMplsTunnelHopAddrType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelHopAddrType (UINT4 u4MplsTunnelHopListIndex,
                             UINT4 u4MplsTunnelHopPathOptionIndex,
                             UINT4 u4MplsTunnelHopIndex,
                             INT4 i4SetValMplsTunnelHopAddrType)
{
    tTeHopInfo         *pTeHopInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) == TE_SUCCESS)
    {
        if (TE_ERHOP_ROW_STATUS (pTeHopInfo) != TE_ACTIVE)
        {
            TE_ERHOP_ADDR_TYPE (pTeHopInfo) =
                (UINT1) i4SetValMplsTunnelHopAddrType;
        }
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelHopIpAddr
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                setValMplsTunnelHopIpAddr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelHopIpAddr (UINT4 u4MplsTunnelHopListIndex,
                           UINT4 u4MplsTunnelHopPathOptionIndex,
                           UINT4 u4MplsTunnelHopIndex,
                           tSNMP_OCTET_STRING_TYPE * pSetValMplsTunnelHopIpAddr)
{
    tTeHopInfo         *pTeHopInfo = NULL;
    UINT4               u4ConfHopAddr = 0;
    UINT4               u4InHopAddr = 0;

    MPLS_CMN_LOCK ();

    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) == TE_SUCCESS)
    {
        if (TE_ERHOP_ROW_STATUS (pTeHopInfo) != TE_ACTIVE)
        {
            CONVERT_TO_INTEGER (pSetValMplsTunnelHopIpAddr->pu1_OctetList,
                                u4InHopAddr);
            u4InHopAddr = OSIX_NTOHL (u4InHopAddr);

            CONVERT_TO_INTEGER (pTeHopInfo->IpAddr.au1Ipv4Addr, u4ConfHopAddr);
            u4ConfHopAddr = OSIX_NTOHL (u4ConfHopAddr);

            if (u4InHopAddr != u4ConfHopAddr)
            {
                pTeHopInfo->bIsMbbRequired = TRUE;
            }

            u4InHopAddr = OSIX_HTONL (u4InHopAddr);
            MEMCPY (pTeHopInfo->IpAddr.au1Ipv4Addr,
                    (UINT1 *) &u4InHopAddr, sizeof (UINT4));

            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }

    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelHopIpPrefixLen
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                setValMplsTunnelHopIpPrefixLen
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelHopIpPrefixLen (UINT4 u4MplsTunnelHopListIndex,
                                UINT4 u4MplsTunnelHopPathOptionIndex,
                                UINT4 u4MplsTunnelHopIndex,
                                UINT4 u4SetValMplsTunnelHopIpPrefixLen)
{
    tTeHopInfo         *pTeHopInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) == TE_SUCCESS)
    {
        if (TE_ERHOP_ROW_STATUS (pTeHopInfo) != TE_ACTIVE)
        {
            TE_ERHOP_ADDR_PRFX_LEN (pTeHopInfo) =
                (UINT1) u4SetValMplsTunnelHopIpPrefixLen;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelHopAsNumber
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                setValMplsTunnelHopAsNumber
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelHopAsNumber (UINT4 u4MplsTunnelHopListIndex,
                             UINT4 u4MplsTunnelHopPathOptionIndex,
                             UINT4 u4MplsTunnelHopIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValMplsTunnelHopAsNumber)
{
    tTeHopInfo         *pTeHopInfo = NULL;
    UINT4               u4Local = 0;
    MPLS_CMN_LOCK ();
    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) == TE_SUCCESS)
    {
        if (TE_ERHOP_ROW_STATUS (pTeHopInfo) != TE_ACTIVE)
        {
            MEMCPY (&u4Local, pSetValMplsTunnelHopAsNumber->pu1_OctetList,
                    sizeof (UINT4));
            u4Local = OSIX_NTOHL (u4Local);
            TE_ERHOP_AS_NUM (pTeHopInfo) = (UINT2) u4Local;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelHopAddrUnnum
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object
                setValMplsTunnelHopAddrUnnum
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelHopAddrUnnum (UINT4 u4MplsTunnelHopListIndex,
                              UINT4 u4MplsTunnelHopPathOptionIndex,
                              UINT4 u4MplsTunnelHopIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pSetValMplsTunnelHopAddrUnnum)
{

    tTeHopInfo         *pTeHopInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) == TE_SUCCESS)
    {
        if (TE_ERHOP_ROW_STATUS (pTeHopInfo) != TE_ACTIVE)
        {
            MPLS_OCTETSTRING_TO_INTEGER (pSetValMplsTunnelHopAddrUnnum,
                                         pTeHopInfo->u4UnnumIf);
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelHopLspId
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                setValMplsTunnelHopLspId
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelHopLspId (UINT4 u4MplsTunnelHopListIndex,
                          UINT4 u4MplsTunnelHopPathOptionIndex,
                          UINT4 u4MplsTunnelHopIndex,
                          tSNMP_OCTET_STRING_TYPE * pSetValMplsTunnelHopLspId)
{
    tTeHopInfo         *pTeHopInfo = NULL;
    UINT2               u2LspId = 0;
    UINT4               u4Local = 0;
    MPLS_CMN_LOCK ();
    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) == TE_SUCCESS)
    {
        if (TE_ERHOP_ROW_STATUS (pTeHopInfo) != TE_ACTIVE)
        {
            MEMCPY (&u2LspId, pSetValMplsTunnelHopLspId->pu1_OctetList,
                    sizeof (UINT2));
            u2LspId = OSIX_NTOHS (u2LspId);
            TE_ERHOP_LSPID (pTeHopInfo) = (UINT4) u2LspId;
            if (pSetValMplsTunnelHopLspId->i4_Length == 6)
            {
                /* Copy last 4 bytes of LSPID to ErHop Address */
                MEMCPY (&u4Local,
                        pSetValMplsTunnelHopLspId->pu1_OctetList + 2,
                        sizeof (UINT4));
                *(UINT4 *) (VOID *) &TE_ERHOP_IP_ADDR (pTeHopInfo) = u4Local;
                TE_ERHOP_ADDR_PRFX_LEN (pTeHopInfo) = 32;
            }
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelHopType
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                setValMplsTunnelHopType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelHopType (UINT4 u4MplsTunnelHopListIndex,
                         UINT4 u4MplsTunnelHopPathOptionIndex,
                         UINT4 u4MplsTunnelHopIndex,
                         INT4 i4SetValMplsTunnelHopType)
{
    tTeHopInfo         *pTeHopInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) == TE_SUCCESS)
    {
        if (TE_ERHOP_ROW_STATUS (pTeHopInfo) != TE_ACTIVE)
        {
            /*MBB is required is set to TRUE, if Hop Type is modified */
            if (TE_ERHOP_TYPE (pTeHopInfo) != (UINT1) i4SetValMplsTunnelHopType)
            {
                pTeHopInfo->bIsMbbRequired = TRUE;
            }
            TE_ERHOP_TYPE (pTeHopInfo) = (UINT1) i4SetValMplsTunnelHopType;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelHopInclude
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                setValMplsTunnelHopInclude
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelHopInclude (UINT4 u4MplsTunnelHopListIndex,
                            UINT4 u4MplsTunnelHopPathOptionIndex,
                            UINT4 u4MplsTunnelHopIndex,
                            INT4 i4SetValMplsTunnelHopInclude)
{
    tTeHopInfo         *pTeHopInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) == TE_SUCCESS)
    {
        if (TE_ERHOP_ROW_STATUS (pTeHopInfo) != TE_ACTIVE)
        {
            TE_ERHOP_INCLD_EXCLD (pTeHopInfo) =
                (UINT1) i4SetValMplsTunnelHopInclude;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelHopPathOptionName
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                setValMplsTunnelHopPathOptionName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelHopPathOptionName (UINT4 u4MplsTunnelHopListIndex,
                                   UINT4 u4MplsTunnelHopPathOptionIndex,
                                   UINT4 u4MplsTunnelHopIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pSetValMplsTunnelHopPathOptionName)
{
    tTeHopInfo         *pTeHopInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) == TE_SUCCESS)
    {
        if (TE_ERHOP_ROW_STATUS (pTeHopInfo) != TE_ACTIVE)
        {
            MEMCPY (pTeHopInfo->au1PathOptionName,
                    pSetValMplsTunnelHopPathOptionName->pu1_OctetList,
                    pSetValMplsTunnelHopPathOptionName->i4_Length);
            pTeHopInfo->au1PathOptionName
                [pSetValMplsTunnelHopPathOptionName->i4_Length] = 0;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelHopEntryPathComp
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                setValMplsTunnelHopEntryPathComp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelHopEntryPathComp (UINT4 u4MplsTunnelHopListIndex,
                                  UINT4 u4MplsTunnelHopPathOptionIndex,
                                  UINT4 u4MplsTunnelHopIndex,
                                  INT4 i4SetValMplsTunnelHopEntryPathComp)
{
    tTeHopInfo         *pTeHopInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) == TE_SUCCESS)
    {
        if (TE_ERHOP_ROW_STATUS (pTeHopInfo) != TE_ACTIVE)
        {
            pTeHopInfo->u1HopEntryPathComp
                = (UINT1) i4SetValMplsTunnelHopEntryPathComp;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelHopRowStatus
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                setValMplsTunnelHopRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelHopRowStatus (UINT4 u4MplsTunnelHopListIndex,
                              UINT4 u4MplsTunnelHopPathOptionIndex,
                              UINT4 u4MplsTunnelHopIndex,
                              INT4 i4SetValMplsTunnelHopRowStatus)
{
    UINT1               u1PathOptionIndexFound = TE_FALSE;
    tTeSll             *pPathOptionList = NULL;
    tTeHopInfo         *pTeHopInfo = NULL;
    tTeHopInfo         *pTempHopInfo = NULL;
    tTeHopInfo         *pPrevHopInfo = NULL;
    tTePathInfo        *pTePathInfo = NULL;

    MPLS_CMN_LOCK ();
    switch (i4SetValMplsTunnelHopRowStatus)
    {
        case TE_CREATEANDWAIT:

            if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                                u4MplsTunnelHopPathOptionIndex,
                                u4MplsTunnelHopIndex,
                                &pTeHopInfo) == TE_SUCCESS)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            if (TE_PATH_LIST_INDEX (u4MplsTunnelHopListIndex) == TE_ZERO)
            {
                /* Allocation for the Path option Index */

                pTePathInfo = (tTePathInfo *)
                    TE_ALLOC_MEM_BLOCK (TE_PO_POOL_ID);

                if (pTePathInfo == NULL)
                {
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
                /* If Allocation is successful both the Pathlist entry and 
                 * the path option structure are initialized.
                 */
                /* Initialization of the Path List Index */
                MEMSET ((VOID
                         *) (TE_PATH_LIST_ENTRY (u4MplsTunnelHopListIndex)),
                        TE_ZERO, sizeof (tTePathListInfo));
                TE_PATH_HOP_ROLE (u4MplsTunnelHopListIndex) = TE_INGRESS;
                TE_PATH_LIST_INDEX (u4MplsTunnelHopListIndex) =
                    u4MplsTunnelHopListIndex;
                TE_SLL_INIT (TE_PATH_OPTION_LIST (u4MplsTunnelHopListIndex));

                /* Initialization of the Path Option Index */
                MEMSET (pTePathInfo, TE_ZERO, sizeof (tTePathInfo));
                TE_SLL_INIT_NODE (&pTePathInfo->NextPathOption);
                TE_PO_INDEX (pTePathInfo) = u4MplsTunnelHopPathOptionIndex;
                TE_SLL_ADD (TE_PATH_OPTION_LIST (u4MplsTunnelHopListIndex),
                            &(pTePathInfo->NextPathOption));
                TE_SLL_INIT (TE_PO_HOP_LIST (pTePathInfo));

                /* Allocation for the Erhop option Index */
                pTeHopInfo = (tTeHopInfo *) TE_ALLOC_MEM_BLOCK (TE_HOP_POOL_ID);

                if (pTeHopInfo == NULL)
                {
                    TE_REL_MEM_BLOCK (TE_PO_POOL_ID, (UINT1 *) pTePathInfo);
                    MEMSET ((VOID
                             *) (TE_PATH_LIST_ENTRY (u4MplsTunnelHopListIndex)),
                            TE_ZERO, sizeof (tTePathListInfo));
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
                MEMSET (pTeHopInfo, TE_ZERO, sizeof (tTeHopInfo));
                TE_SLL_INIT_NODE (&pTeHopInfo->NextHop);
                TE_ERHOP_INDEX (pTeHopInfo) = u4MplsTunnelHopIndex;
                pTeHopInfo->GmplsTnlHopInfo.u4ForwardLbl = MPLS_INVALID_LABEL;
                pTeHopInfo->GmplsTnlHopInfo.u4ReverseLbl = MPLS_INVALID_LABEL;
                TE_SLL_ADD (TE_PO_HOP_LIST (pTePathInfo),
                            &(pTeHopInfo->NextHop));
                (TE_PO_HOP_COUNT (pTePathInfo))++;
                TE_ERHOP_ROW_STATUS (pTeHopInfo) = TE_NOTREADY;
                TE_ERHOP_STORAGE_TYPE (pTeHopInfo) = TE_STORAGE_VOLATILE;
                TE_ERHOP_TYPE (pTeHopInfo) = TE_LOOSE_ER;
                TE_ERHOP_ADDR_TYPE (pTeHopInfo) = TE_ERHOP_IPV4_TYPE;
                TE_ERHOP_ADDR_PRFX_LEN (pTeHopInfo) = MAX_TE_HOP_ADDRESS_SIZE;
                TE_ERHOP_INCLD_EXCLD (pTeHopInfo) = TE_SNMP_TRUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
            else
            {
                pPathOptionList =
                    TE_PATH_OPTION_LIST (u4MplsTunnelHopListIndex);

                TE_SLL_SCAN (pPathOptionList, pTePathInfo, tTePathInfo *)
                {
                    if (u4MplsTunnelHopPathOptionIndex
                        == TE_PO_INDEX (pTePathInfo))
                    {
                        if (TE_PO_NUM_TUNNELS (pTePathInfo) == TE_ZERO)
                        {
                            pTeHopInfo = (tTeHopInfo *)
                                TE_ALLOC_MEM_BLOCK (TE_HOP_POOL_ID);

                            if (pTeHopInfo == NULL)
                            {
                                MPLS_CMN_UNLOCK ();
                                return SNMP_FAILURE;
                            }
                            TE_SLL_INIT_NODE (&pTeHopInfo->NextHop);
                            TE_ERHOP_INDEX (pTeHopInfo) = u4MplsTunnelHopIndex;
                            pTeHopInfo->GmplsTnlHopInfo.u4ForwardLbl =
                                MPLS_INVALID_LABEL;
                            pTeHopInfo->GmplsTnlHopInfo.u4ReverseLbl =
                                MPLS_INVALID_LABEL;
                            TE_SLL_SCAN (TE_PO_HOP_LIST (pTePathInfo),
                                         pTempHopInfo, tTeHopInfo *)
                            {

                                if (TE_ERHOP_INDEX (pTempHopInfo) <
                                    TE_ERHOP_INDEX (pTeHopInfo))
                                {
                                    pPrevHopInfo = pTempHopInfo;
                                }
                            }
                            TE_SLL_INSERT (TE_PO_HOP_LIST (pTePathInfo),
                                           &(pPrevHopInfo->NextHop),
                                           &(pTeHopInfo->NextHop));
                            (TE_PO_HOP_COUNT (pTePathInfo))++;
                            u1PathOptionIndexFound = TE_TRUE;
                            TE_ERHOP_ROW_STATUS (pTeHopInfo) = TE_NOTREADY;
                            TE_ERHOP_STORAGE_TYPE (pTeHopInfo) =
                                TE_STORAGE_VOLATILE;
                            TE_ERHOP_ADDR_TYPE (pTeHopInfo) =
                                TE_ERHOP_IPV4_TYPE;
                            TE_ERHOP_ADDR_PRFX_LEN (pTeHopInfo) =
                                MAX_TE_HOP_ADDRESS_SIZE;
                            TE_ERHOP_TYPE (pTeHopInfo) = TE_LOOSE_ER;
                            TE_ERHOP_INCLD_EXCLD (pTeHopInfo) = TE_SNMP_TRUE;

                            break;
                        }
                        else
                        {
                            /* Some tunnel is pointing to the Path option */
                            MPLS_CMN_UNLOCK ();
                            return SNMP_FAILURE;
                        }
                    }
                }
                if (u1PathOptionIndexFound == TE_FALSE)
                {
                    /* Another path option already exists for this list Index. 
                     * Hence new path option is allocated and added to the 
                     * path option list.
                     */
                    pTePathInfo = (tTePathInfo *)
                        TE_ALLOC_MEM_BLOCK (TE_PO_POOL_ID);

                    if (pTePathInfo == NULL)
                    {
                        MPLS_CMN_UNLOCK ();
                        return SNMP_FAILURE;
                    }
                    /* Initialization of the Path Option Index */
                    MEMSET (pTePathInfo, TE_ZERO, sizeof (tTePathInfo));
                    TE_SLL_INIT_NODE (&pTePathInfo->NextPathOption);
                    TE_PO_INDEX (pTePathInfo) = u4MplsTunnelHopPathOptionIndex;
                    TE_SLL_ADD (TE_PATH_OPTION_LIST (u4MplsTunnelHopListIndex),
                                &(pTePathInfo->NextPathOption));
                    TE_SLL_INIT (TE_PO_HOP_LIST (pTePathInfo));

                    /* Allocation for the Erhop option Index */

                    pTeHopInfo = (tTeHopInfo *)
                        TE_ALLOC_MEM_BLOCK (TE_HOP_POOL_ID);

                    if (pTeHopInfo == NULL)
                    {
                        TE_REL_MEM_BLOCK (TE_PO_POOL_ID, (UINT1 *) pTePathInfo);
                        MPLS_CMN_UNLOCK ();
                        return SNMP_FAILURE;
                    }
                    MEMSET (pTeHopInfo, TE_ZERO, sizeof (tTeHopInfo));
                    TE_SLL_INIT_NODE (&pTeHopInfo->NextHop);
                    TE_ERHOP_INDEX (pTeHopInfo) = u4MplsTunnelHopIndex;
                    pTeHopInfo->GmplsTnlHopInfo.u4ForwardLbl =
                        MPLS_INVALID_LABEL;
                    pTeHopInfo->GmplsTnlHopInfo.u4ReverseLbl =
                        MPLS_INVALID_LABEL;
                    TE_SLL_ADD (TE_PO_HOP_LIST (pTePathInfo),
                                &(pTeHopInfo->NextHop));
                    (TE_PO_HOP_COUNT (pTePathInfo))++;
                    TE_ERHOP_ROW_STATUS (pTeHopInfo) = TE_NOTREADY;
                    TE_ERHOP_STORAGE_TYPE (pTeHopInfo) = TE_STORAGE_VOLATILE;
                    TE_ERHOP_TYPE (pTeHopInfo) = TE_LOOSE_ER;
                    TE_ERHOP_ADDR_TYPE (pTeHopInfo) = TE_ERHOP_IPV4_TYPE;
                    TE_ERHOP_ADDR_PRFX_LEN (pTeHopInfo) =
                        MAX_TE_HOP_ADDRESS_SIZE;
                    TE_ERHOP_INCLD_EXCLD (pTeHopInfo) = TE_SNMP_TRUE;
                    MPLS_CMN_UNLOCK ();
                    return SNMP_SUCCESS;
                }
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }

        case TE_ACTIVE:
            if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                                u4MplsTunnelHopPathOptionIndex,
                                u4MplsTunnelHopIndex,
                                &pTeHopInfo) == TE_FAILURE)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            if (TeCheckOrUpdateHopInfo (u4MplsTunnelHopListIndex,
                                        u4MplsTunnelHopPathOptionIndex,
                                        (UINT4) pTeHopInfo->u1HopIncludeExclude,
                                        (UINT4) pTeHopInfo->u1HopEntryPathComp)
                == TE_FAILURE)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            TE_ERHOP_ROW_STATUS (pTeHopInfo) = TE_ACTIVE;

            if (TeIsLsrPartOfErHop (pTeHopInfo, &pTeHopInfo->u1LsrPartOfErHop)
                == TE_FAILURE)
            {
                /*Failure Condition not handled here */
            }
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;

        case TE_NOTINSERVICE:
            if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                                u4MplsTunnelHopPathOptionIndex,
                                u4MplsTunnelHopIndex,
                                &pTeHopInfo) == TE_FAILURE)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            /* If the Erhop Row Status is going to be NOT_IN_SERVICE, then 
             * the present Row status must be ACTIVE and no tunnel should be
             * associated with the Erhop list. Or the Status should be in
             * NOT_IN_SERVICE.
             */

            if (TE_ERHOP_ROW_STATUS (pTeHopInfo) == TE_ACTIVE)
            {
                if (TE_PATH_HOP_ROLE (u4MplsTunnelHopListIndex) == TE_INGRESS)
                {
                    pPathOptionList =
                        TE_PATH_OPTION_LIST (u4MplsTunnelHopListIndex);
                    TE_SLL_SCAN (pPathOptionList, pTePathInfo, tTePathInfo *)
                    {
                        if (u4MplsTunnelHopPathOptionIndex
                            == TE_PO_INDEX (pTePathInfo))
                        {
                            if (TE_PO_NUM_TUNNELS (pTePathInfo) == TE_ZERO)
                            {
                                TE_ERHOP_ROW_STATUS (pTeHopInfo) =
                                    TE_NOTINSERVICE;
                                MPLS_CMN_UNLOCK ();
                                return SNMP_SUCCESS;
                            }
                            MPLS_CMN_UNLOCK ();
                            return SNMP_FAILURE;
                        }
                    }
                }
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            break;

        case TE_DESTROY:

            if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                                u4MplsTunnelHopPathOptionIndex,
                                u4MplsTunnelHopIndex,
                                &pTeHopInfo) == TE_FAILURE)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            if (((TE_ERHOP_ROW_STATUS (pTeHopInfo) == TE_ACTIVE) ||
                 (TE_ERHOP_ROW_STATUS (pTeHopInfo) == TE_NOTINSERVICE) ||
                 (TE_ERHOP_ROW_STATUS (pTeHopInfo) == TE_NOTREADY)) &&
                (TE_PATH_HOP_ROLE (u4MplsTunnelHopListIndex) == TE_INGRESS))
            {
                pPathOptionList =
                    TE_PATH_OPTION_LIST (u4MplsTunnelHopListIndex);
                TE_SLL_SCAN (pPathOptionList, pTePathInfo, tTePathInfo *)
                {
                    if (u4MplsTunnelHopPathOptionIndex
                        == TE_PO_INDEX (pTePathInfo))
                    {

                        if (TE_PO_NUM_TUNNELS (pTePathInfo) == TE_ZERO)
                        {
                            TE_SLL_SCAN (TE_PO_HOP_LIST (pTePathInfo),
                                         pTeHopInfo, tTeHopInfo *)
                            {
                                if (TE_ERHOP_INDEX (pTeHopInfo) ==
                                    u4MplsTunnelHopIndex)
                                {
                                    if (TeDeleteHopInfo
                                        (TE_PATH_LIST_ENTRY
                                         (u4MplsTunnelHopListIndex),
                                         pTePathInfo, pTeHopInfo) != TE_SUCCESS)
                                    {
                                        MPLS_CMN_UNLOCK ();
                                        return SNMP_FAILURE;
                                    }
                                    if (TE_SLL_COUNT
                                        (TE_PO_HOP_LIST (pTePathInfo)) ==
                                        TE_ZERO)
                                    {

                                        if (TeDeleteHopListInfo
                                            (TE_PATH_LIST_ENTRY
                                             (u4MplsTunnelHopListIndex))
                                            != TE_SUCCESS)
                                        {
                                            MPLS_CMN_UNLOCK ();
                                            return SNMP_FAILURE;

                                        }
                                    }
                                    MPLS_CMN_UNLOCK ();
                                    return SNMP_SUCCESS;
                                }
                                else
                                {
                                    continue;
                                }
                            }
                            break;

                        }
                        MPLS_CMN_UNLOCK ();
                        return SNMP_FAILURE;
                    }
                }
            }
	default :
	    break;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelHopStorageType
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                setValMplsTunnelHopStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelHopStorageType (UINT4 u4MplsTunnelHopListIndex,
                                UINT4 u4MplsTunnelHopPathOptionIndex,
                                UINT4 u4MplsTunnelHopIndex,
                                INT4 i4SetValMplsTunnelHopStorageType)
{
    tTeHopInfo         *pTeHopInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) == TE_SUCCESS)
    {
        if (TE_ERHOP_ROW_STATUS (pTeHopInfo) != TE_ACTIVE)
        {
            TE_ERHOP_STORAGE_TYPE (pTeHopInfo) =
                (UINT1) i4SetValMplsTunnelHopStorageType;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2MplsTunnelHopTable
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MplsTunnelHopTable (UINT4 *pu4ErrorCode,
                            tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MplsTunnelResourceTable. */

/****************************************************************************
 Function    :  nmhSetMplsTunnelResourceMaxRate
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                setValMplsTunnelResourceMaxRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelResourceMaxRate (UINT4 u4MplsTunnelResourceIndex,
                                 UINT4 u4SetValMplsTunnelResourceMaxRate)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();
    if ((TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                           &pTeTrfcParams) == TE_SUCCESS) &&
        (TE_TNLRSRC_ROW_STATUS (pTeTrfcParams) != TE_ACTIVE))
    {
        TE_TNLRSRC_MAX_RATE (pTeTrfcParams) = u4SetValMplsTunnelResourceMaxRate;
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelResourceMeanRate
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                setValMplsTunnelResourceMeanRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelResourceMeanRate (UINT4 u4MplsTunnelResourceIndex,
                                  UINT4 u4SetValMplsTunnelResourceMeanRate)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();
    if ((TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                           &pTeTrfcParams) == TE_SUCCESS) &&
        (TE_TNLRSRC_ROW_STATUS (pTeTrfcParams) != TE_ACTIVE))
    {
        /* TODO Not Supported */
        UNUSED_PARAM (u4SetValMplsTunnelResourceMeanRate);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetMplsTunnelResourceMaxBurstSize
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                setValMplsTunnelResourceMaxBurstSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelResourceMaxBurstSize (UINT4 u4MplsTunnelResourceIndex,
                                      UINT4
                                      u4SetValMplsTunnelResourceMaxBurstSize)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();
    if ((TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                           &pTeTrfcParams) == TE_SUCCESS) &&
        (TE_TNLRSRC_ROW_STATUS (pTeTrfcParams) != TE_ACTIVE))
    {
        /* TODO Not Supported */
        UNUSED_PARAM (u4SetValMplsTunnelResourceMaxBurstSize);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetMplsTunnelResourceMeanBurstSize
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                setValMplsTunnelResourceMeanBurstSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelResourceMeanBurstSize (UINT4 u4MplsTunnelResourceIndex,
                                       UINT4
                                       u4SetValMplsTunnelResourceMeanBurstSize)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();
    if ((TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                           &pTeTrfcParams) == TE_SUCCESS) &&
        (TE_TNLRSRC_ROW_STATUS (pTeTrfcParams) != TE_ACTIVE))
    {
        /* TODO Not Supported */
        UNUSED_PARAM (u4SetValMplsTunnelResourceMeanBurstSize);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetMplsTunnelResourceExBurstSize
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                setValMplsTunnelResourceExBurstSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelResourceExBurstSize (UINT4 u4MplsTunnelResourceIndex,
                                     UINT4
                                     u4SetValMplsTunnelResourceExBurstSize)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();
    if ((TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                           &pTeTrfcParams) == TE_SUCCESS) &&
        (TE_TNLRSRC_ROW_STATUS (pTeTrfcParams) != TE_ACTIVE))
    {
        /* TODO Not Supported */
        UNUSED_PARAM (u4SetValMplsTunnelResourceExBurstSize);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetMplsTunnelResourceFrequency
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                setValMplsTunnelResourceFrequency
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelResourceFrequency (UINT4 u4MplsTunnelResourceIndex,
                                   INT4 i4SetValMplsTunnelResourceFrequency)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();
    if ((TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                           &pTeTrfcParams) == TE_SUCCESS) &&
        (TE_TNLRSRC_ROW_STATUS (pTeTrfcParams) != TE_ACTIVE))
    {
        /* TODO Not Supported */
        UNUSED_PARAM (i4SetValMplsTunnelResourceFrequency);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetMplsTunnelResourceWeight
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                setValMplsTunnelResourceWeight
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelResourceWeight (UINT4 u4MplsTunnelResourceIndex,
                                UINT4 u4SetValMplsTunnelResourceWeight)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();
    if ((TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                           &pTeTrfcParams) == TE_SUCCESS) &&
        (TE_TNLRSRC_ROW_STATUS (pTeTrfcParams) != TE_ACTIVE))
    {
        /* TODO Not Supported */
        UNUSED_PARAM (u4SetValMplsTunnelResourceWeight);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelResourceRowStatus
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                setValMplsTunnelResourceRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelResourceRowStatus (UINT4 u4MplsTunnelResourceIndex,
                                   INT4 i4SetValMplsTunnelResourceRowStatus)
{
    UINT1               u1RetVal;
    tTeTrfcParams      *pTeTrfcParams = NULL;
    tRSVPTrfcParams    *pRSVPTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    u1RetVal = TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                                 &pTeTrfcParams);
    switch (i4SetValMplsTunnelResourceRowStatus)
    {
        case TE_ACTIVE:
            if (u1RetVal == TE_SUCCESS)
            {
                if (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) != NULL)
                {
                    if (TE_CRLDP_TPARAM_ROW_STATUS (pTeTrfcParams) == TE_ACTIVE)
                    {
                        if (TE_TNLRSRC_ROW_STATUS (pTeTrfcParams) ==
                            TE_NOTREADY)
                        {
                            TE_TNLRSRC_ROLE (pTeTrfcParams) = TE_INGRESS;
                            TE_TNLRSRC_ROW_STATUS (pTeTrfcParams) = TE_ACTIVE;
                            MPLS_CMN_UNLOCK ();
                            return SNMP_SUCCESS;
                        }
                    }
                }
                else 
                {
                    if ((TE_TNLRSRC_ROW_STATUS (pTeTrfcParams) == TE_NOTREADY) || 
                        (TE_TNLRSRC_ROW_STATUS (pTeTrfcParams) == TE_NOTINSERVICE))
                    {
                        TE_TNLRSRC_ROLE (pTeTrfcParams) = TE_INGRESS;
                        TE_TNLRSRC_ROW_STATUS (pTeTrfcParams) = TE_ACTIVE;
                        MPLS_CMN_UNLOCK ();
                        return SNMP_SUCCESS;
                    }
                }
            }
            break;
        case TE_NOTINSERVICE:
	     if (u1RetVal == TE_SUCCESS)
             {
                if (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) != NULL)
                {
                    if (TE_CRLDP_TPARAM_ROW_STATUS (pTeTrfcParams) == TE_ACTIVE)
                    {
                        if (TE_TNLRSRC_ROW_STATUS (pTeTrfcParams) ==
                            TE_ACTIVE)
                        {
                            TE_TNLRSRC_ROW_STATUS (pTeTrfcParams) = TE_NOTINSERVICE;
                            MPLS_CMN_UNLOCK ();
                            return SNMP_SUCCESS;
                        }
                    }
                }
                else 
                {
                    if (TE_TNLRSRC_ROW_STATUS (pTeTrfcParams) == TE_ACTIVE)
                    {
                        TE_TNLRSRC_ROW_STATUS (pTeTrfcParams) = TE_NOTINSERVICE;
                        MPLS_CMN_UNLOCK ();
                        return SNMP_SUCCESS;
                    }
                }
            }
	    break;
        case TE_CREATEANDWAIT:

            if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            if (u1RetVal != TE_SUCCESS)
            {
                if (TE_TRFC_PARAMS_TNL_RESINDEX (u4MplsTunnelResourceIndex)
                    == TE_ZERO)
                {
                    /* Allocate memory for fsMplsTunnelRSVPResTable */
                    pRSVPTrfcParams = (tRSVPTrfcParams *)
                        TE_ALLOC_MEM_BLOCK (TE_RSVP_TRFC_PARAM_POOL_ID);

                    if (pRSVPTrfcParams == NULL)
                    {
                        MPLS_CMN_UNLOCK ();
                        return SNMP_FAILURE;
                    }

                    TE_TRFC_PARAMS_TNL_RESINDEX (u4MplsTunnelResourceIndex) =
                        u4MplsTunnelResourceIndex;
                    TE_TRFC_PARAMS_TNL_RESROWSTATUS (u4MplsTunnelResourceIndex)
                        = TE_NOTREADY;
                    pTeTrfcParams =
                        TE_TRFC_PARAMS_PTR (u4MplsTunnelResourceIndex);
                    pRSVPTrfcParams->u2CfgFlag = TE_ZERO;
                    TE_RSVPTE_TRFC_PARAMS (pTeTrfcParams) = pRSVPTrfcParams;
                    TE_TNLRSRC_STORAGE_TYPE (pTeTrfcParams) =
                        TE_STORAGE_VOLATILE;
                    MPLS_CMN_UNLOCK ();
                    return SNMP_SUCCESS;
                }
            }
            break;
        case TE_DESTROY:
            if (u4MplsTunnelResourceIndex == TE_ONE)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            if (u1RetVal == TE_SUCCESS)
            {
                if (pTeTrfcParams->u2NumOfTunnels == TE_ZERO)
                {
                    if (TeDeleteTrfcParams (pTeTrfcParams) == TE_SUCCESS)
                    {
                        MPLS_CMN_UNLOCK ();
                        return SNMP_SUCCESS;
                    }
                    else
                    {
                        MPLS_CMN_UNLOCK ();
                        return SNMP_FAILURE;
                    }
                }
            }
            break;

        default:
            break;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelResourceStorageType
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                setValMplsTunnelResourceStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelResourceStorageType (UINT4 u4MplsTunnelResourceIndex,
                                     INT4 i4SetValMplsTunnelResourceStorageType)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        TE_TNLRSRC_STORAGE_TYPE (pTeTrfcParams) = (UINT1)
            i4SetValMplsTunnelResourceStorageType;
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2MplsTunnelResourceTable
 Input       :  The Indices
                MplsTunnelResourceIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MplsTunnelResourceTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : MplsTunnelCRLDPResTable. */

/****************************************************************************
 Function    :  nmhSetMplsTunnelCRLDPResMeanBurstSize
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                setValMplsTunnelCRLDPResMeanBurstSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelCRLDPResMeanBurstSize (UINT4 u4MplsTunnelResourceIndex,
                                       UINT4
                                       u4SetValMplsTunnelCRLDPResMeanBurstSize)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) != NULL)
        {
            if (TE_CRLDP_TPARAM_ROW_STATUS (pTeTrfcParams) != TE_ACTIVE)
            {
                TE_CRLDP_TPARAM_CBS (pTeTrfcParams) =
                    u4SetValMplsTunnelCRLDPResMeanBurstSize;
                TE_CRLDP_TPARAM_CFG_FLAGS (pTeTrfcParams) |=
                    TE_CRLDP_CFG_CBS_FLAG;
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelCRLDPResExBurstSize
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                setValMplsTunnelCRLDPResExBurstSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelCRLDPResExBurstSize (UINT4 u4MplsTunnelResourceIndex,
                                     UINT4
                                     u4SetValMplsTunnelCRLDPResExBurstSize)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if ((TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) != NULL) &&
            (TE_CRLDP_TPARAM_ROW_STATUS (pTeTrfcParams) != TE_ACTIVE))
        {
            TE_CRLDP_TPARAM_EBS (pTeTrfcParams) =
                u4SetValMplsTunnelCRLDPResExBurstSize;
            TE_CRLDP_TPARAM_CFG_FLAGS (pTeTrfcParams) |= TE_CRLDP_CFG_EBS_FLAG;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelCRLDPResFrequency
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                setValMplsTunnelCRLDPResFrequency
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelCRLDPResFrequency (UINT4 u4MplsTunnelResourceIndex,
                                   INT4 i4SetValMplsTunnelCRLDPResFrequency)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) != NULL)
        {
            if (TE_CRLDP_TPARAM_ROW_STATUS (pTeTrfcParams) != TE_ACTIVE)
            {
                TE_CRLDP_TPARAM_FREQ (pTeTrfcParams) = (UINT1)
                    i4SetValMplsTunnelCRLDPResFrequency;
                TE_CRLDP_TPARAM_CFG_FLAGS (pTeTrfcParams) |=
                    TE_CRLDP_CFG_FREQUENCY_FLAG;
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelCRLDPResWeight
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                setValMplsTunnelCRLDPResWeight
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelCRLDPResWeight (UINT4 u4MplsTunnelResourceIndex,
                                UINT4 u4SetValMplsTunnelCRLDPResWeight)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) != NULL)
        {
            if (TE_CRLDP_TPARAM_ROW_STATUS (pTeTrfcParams) != TE_ACTIVE)
            {
                TE_CRLDP_TPARAM_WEIGHT (pTeTrfcParams) = (UINT1)
                    u4SetValMplsTunnelCRLDPResWeight;
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelCRLDPResFlags
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                setValMplsTunnelCRLDPResFlags
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelCRLDPResFlags (UINT4 u4MplsTunnelResourceIndex,
                               UINT4 u4SetValMplsTunnelCRLDPResFlags)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) != NULL)
        {
            if (TE_CRLDP_TPARAM_ROW_STATUS (pTeTrfcParams) != TE_ACTIVE)
            {
                TE_CRLDP_TPARAM_FLAGS (pTeTrfcParams) = (UINT1)
                    u4SetValMplsTunnelCRLDPResFlags;
                TE_CRLDP_TPARAM_CFG_FLAGS (pTeTrfcParams) |=
                    TE_CRLDP_CFG_RESFLAGS_FLAG;
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelCRLDPResRowStatus
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                setValMplsTunnelCRLDPResRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelCRLDPResRowStatus (UINT4 u4MplsTunnelResourceIndex,
                                   INT4 i4SetValMplsTunnelCRLDPResRowStatus)
{
    UINT1               u1RetVal;
    tTeTrfcParams      *pTeTrfcParams = NULL;
    tCRLDPTrfcParams   *pCRLDPTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    u1RetVal = TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                                 &pTeTrfcParams);

    switch (i4SetValMplsTunnelCRLDPResRowStatus)
    {
        case TE_ACTIVE:
            if (u1RetVal == TE_SUCCESS)
            {
                if (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) != NULL)
                {
                    if ((TE_CRLDP_TPARAM_ROW_STATUS (pTeTrfcParams) ==
                         TE_NOTREADY) &&
                        (TE_CRLDP_TPARAM_CFG_FLAGS (pTeTrfcParams) ==
                         TE_CRLDP_DESIRED_CFG_FLAG))
                    {
                        TE_CRLDP_TPARAM_ROW_STATUS (pTeTrfcParams) = TE_ACTIVE;
                        MPLS_CMN_UNLOCK ();
                        return SNMP_SUCCESS;
                    }
                }
            }
            break;
        case TE_CREATEANDWAIT:

            if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            pTeTrfcParams =
                &gTeGblInfo.pTrfcParams[u4MplsTunnelResourceIndex - TE_ONE];
            TE_TNLRSRC_ROW_STATUS (pTeTrfcParams) = TE_NOTREADY;
            TE_TNLRSRC_STORAGE_TYPE (pTeTrfcParams) = TE_STORAGE_VOLATILE;

            if (u1RetVal != TE_SUCCESS)
            {

                if ((TE_TRFC_PARAMS_TNL_RESINDEX (u4MplsTunnelResourceIndex)
                     == TE_ZERO))
                {

                    TE_TRFC_PARAMS_TNL_RESINDEX (u4MplsTunnelResourceIndex) =
                        u4MplsTunnelResourceIndex;

                    pCRLDPTrfcParams = (tCRLDPTrfcParams *)
                        TE_ALLOC_MEM_BLOCK (TE_CRLDP_TRFC_PARAM_POOL_ID);

                    if (pCRLDPTrfcParams == NULL)
                    {
                        MPLS_CMN_UNLOCK ();
                        return SNMP_FAILURE;
                    }

                    pCRLDPTrfcParams->u2CfgFlag = TE_ZERO;
                    TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) = pCRLDPTrfcParams;
                    TE_CRLDP_TPARAM_ROW_STATUS (pTeTrfcParams) = TE_NOTREADY;
                    TE_CRLDP_TPARAM_WEIGHT (pTeTrfcParams) = TE_MIN_WGT_VAL;
                    TE_CRLDP_TPARAM_FLAGS (pTeTrfcParams) = TE_MIN_RES_FLAG_VAL;
                    TE_CRLDP_TPARAM_STORAGE_TYPE (pTeTrfcParams) =
                        TE_STORAGE_VOLATILE;
                    MPLS_CMN_UNLOCK ();
                    return SNMP_SUCCESS;
                }
            }
            break;
        case TE_DESTROY:
            if (u1RetVal == TE_SUCCESS)
            {
                if (pTeTrfcParams->u2NumOfTunnels == TE_ZERO)
                {
                    if (pTeTrfcParams->pCRLDPTrfcParams != NULL)
                    {
                        TE_REL_MEM_BLOCK
                            (TE_CRLDP_TRFC_PARAM_POOL_ID,
                             (UINT1 *) (pTeTrfcParams->pCRLDPTrfcParams));
                        TE_TNLRSRC_INDEX (pTeTrfcParams) = 0;
                        pTeTrfcParams->pCRLDPTrfcParams = NULL;
                        MPLS_CMN_UNLOCK ();
                        return SNMP_SUCCESS;
                    }
                }
            }
            break;
         default: 
	    break;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhSetMplsTunnelCRLDPResStorageType
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                setValMplsTunnelCRLDPResStorageType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelCRLDPResStorageType (UINT4 u4MplsTunnelResourceIndex,
                                     INT4 i4SetValMplsTunnelCRLDPResStorageType)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) != NULL)
        {
            TE_CRLDP_TPARAM_STORAGE_TYPE (pTeTrfcParams) = (UINT1)
                i4SetValMplsTunnelCRLDPResStorageType;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2MplsTunnelCRLDPResTable
 Input       :  The Indices
                MplsTunnelResourceIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MplsTunnelCRLDPResTable (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMplsTunnelCRLDPResTable. */

/****************************************************************************
 Function    :  nmhSetFsMplsTunnelCRLDPResPeakDataRate
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                setValFsMplsTunnelCRLDPResPeakDataRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsTunnelCRLDPResPeakDataRate (UINT4 u4MplsTunnelResourceIndex,
                                        UINT4
                                        u4SetValFsMplsTunnelCRLDPResPeakDataRate)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) != NULL)
        {
            if (TE_CRLDP_TPARAM_ROW_STATUS (pTeTrfcParams) != TE_ACTIVE)
            {
                TE_CRLDP_TPARAM_PDR (pTeTrfcParams) =
                    u4SetValFsMplsTunnelCRLDPResPeakDataRate;
                TE_CRLDP_TPARAM_CFG_FLAGS (pTeTrfcParams) |=
                    TE_CRLDP_CFG_PDR_FLAG;
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsTunnelCRLDPResCommittedDataRate
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                setValFsMplsTunnelCRLDPResCommittedDataRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsTunnelCRLDPResCommittedDataRate (UINT4 u4MplsTunnelResourceIndex,
                                             UINT4
                                             u4SetValFsMplsTunnelCRLDPResCommittedDataRate)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) != NULL)
        {
            if (TE_CRLDP_TPARAM_ROW_STATUS (pTeTrfcParams) != TE_ACTIVE)
            {
                TE_CRLDP_TPARAM_CDR (pTeTrfcParams) =
                    u4SetValFsMplsTunnelCRLDPResCommittedDataRate;
                TE_CRLDP_TPARAM_CFG_FLAGS (pTeTrfcParams) |=
                    TE_CRLDP_CFG_CDR_FLAG;
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsTunnelCRLDPResPeakBurstSize
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                setValFsMplsTunnelCRLDPResPeakBurstSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsTunnelCRLDPResPeakBurstSize (UINT4 u4MplsTunnelResourceIndex,
                                         UINT4
                                         u4SetValFsMplsTunnelCRLDPResPeakBurstSize)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) != NULL)
        {
            if (TE_CRLDP_TPARAM_ROW_STATUS (pTeTrfcParams) != TE_ACTIVE)
            {
                TE_CRLDP_TPARAM_PBS (pTeTrfcParams) =
                    u4SetValFsMplsTunnelCRLDPResPeakBurstSize;
                TE_CRLDP_TPARAM_CFG_FLAGS (pTeTrfcParams) |=
                    TE_CRLDP_CFG_PBS_FLAG;
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsTunnelCRLDPResCommittedBurstSize
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                setValFsMplsTunnelCRLDPResCommittedBurstSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsTunnelCRLDPResCommittedBurstSize (UINT4 u4MplsTunnelResourceIndex,
                                              UINT4
                                              u4SetValFsMplsTunnelCRLDPResCommittedBurstSize)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) != NULL)
        {
            if (TE_CRLDP_TPARAM_ROW_STATUS (pTeTrfcParams) != TE_ACTIVE)
            {
                TE_CRLDP_TPARAM_CBS (pTeTrfcParams) =
                    u4SetValFsMplsTunnelCRLDPResCommittedBurstSize;
                TE_CRLDP_TPARAM_CFG_FLAGS (pTeTrfcParams) |=
                    TE_CRLDP_CFG_CBS_FLAG;
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsTunnelCRLDPResExcessBurstSize
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                setValFsMplsTunnelCRLDPResExcessBurstSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsTunnelCRLDPResExcessBurstSize (UINT4 u4MplsTunnelResourceIndex,
                                           UINT4
                                           u4SetValFsMplsTunnelCRLDPResExcessBurstSize)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if ((TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) != NULL) &&
            (TE_CRLDP_TPARAM_ROW_STATUS (pTeTrfcParams) != TE_ACTIVE))
        {
            TE_CRLDP_TPARAM_EBS (pTeTrfcParams) =
                u4SetValFsMplsTunnelCRLDPResExcessBurstSize;
            TE_CRLDP_TPARAM_CFG_FLAGS (pTeTrfcParams) |= TE_CRLDP_CFG_EBS_FLAG;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsTunnelRSVPResTable
 Input       :  The Indices
                MplsTunnelResourceIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsTunnelRSVPResTable (UINT4 *pu4ErrorCode,
                                  tSnmpIndexList * pSnmpIndexList,
                                  tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMplsTunnelRSVPResTable. */

/****************************************************************************
 Function    :  nmhSetFsMplsTunnelRSVPResTokenBucketRate
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                setValFsMplsTunnelRSVPResTokenBucketRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsTunnelRSVPResTokenBucketRate (UINT4 u4MplsTunnelResourceIndex,
                                          UINT4
                                          u4SetValFsMplsTunnelRSVPResTokenBucketRate)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if ((TE_RSVPTE_TRFC_PARAMS (pTeTrfcParams) != NULL) &&
            (TE_TRFC_PARAMS_TNL_RESROWSTATUS (u4MplsTunnelResourceIndex) !=
             TE_ACTIVE))
        {
            TE_RSVPTE_TPARAM_TBR (pTeTrfcParams) =
                u4SetValFsMplsTunnelRSVPResTokenBucketRate;
            TE_RSVPTE_TPARAM_CFG_FLAG (pTeTrfcParams) |= TE_RSVPTE_CFG_TBR_FLAG;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsTunnelRSVPResTokenBucketSize
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                setValFsMplsTunnelRSVPResTokenBucketSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsTunnelRSVPResTokenBucketSize (UINT4 u4MplsTunnelResourceIndex,
                                          UINT4
                                          u4SetValFsMplsTunnelRSVPResTokenBucketSize)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if ((TE_RSVPTE_TRFC_PARAMS (pTeTrfcParams) != NULL) &&
            (TE_TRFC_PARAMS_TNL_RESROWSTATUS (u4MplsTunnelResourceIndex) !=
             TE_ACTIVE))
        {
            TE_RSVPTE_TPARAM_TBS (pTeTrfcParams) =
                u4SetValFsMplsTunnelRSVPResTokenBucketSize;
            TE_RSVPTE_TPARAM_CFG_FLAG (pTeTrfcParams) |= TE_RSVPTE_CFG_TBS_FLAG;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsTunnelRSVPResPeakDataRate
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                setValFsMplsTunnelRSVPResPeakDataRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsTunnelRSVPResPeakDataRate (UINT4 u4MplsTunnelResourceIndex,
                                       UINT4
                                       u4SetValFsMplsTunnelRSVPResPeakDataRate)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if ((TE_RSVPTE_TRFC_PARAMS (pTeTrfcParams) != NULL) &&
            (TE_TRFC_PARAMS_TNL_RESROWSTATUS (u4MplsTunnelResourceIndex) !=
             TE_ACTIVE))
        {
            if (TE_RSVPTE_TPARAM_PDR (pTeTrfcParams) !=
                u4SetValFsMplsTunnelRSVPResPeakDataRate)
            {
                pTeTrfcParams->bIsMbbRequired = TRUE;
            }

            TE_RSVPTE_TPARAM_PDR (pTeTrfcParams) =
                u4SetValFsMplsTunnelRSVPResPeakDataRate;
            TE_RSVPTE_TPARAM_CFG_FLAG (pTeTrfcParams) |= TE_RSVPTE_CFG_PDR_FLAG;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsTunnelRSVPResMinimumPolicedUnit
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                setValFsMplsTunnelRSVPResMinimumPolicedUnit
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsTunnelRSVPResMinimumPolicedUnit (UINT4 u4MplsTunnelResourceIndex,
                                             INT4
                                             i4SetValFsMplsTunnelRSVPResMinimumPolicedUnit)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if ((TE_RSVPTE_TRFC_PARAMS (pTeTrfcParams) != NULL) &&
            (TE_TRFC_PARAMS_TNL_RESROWSTATUS (u4MplsTunnelResourceIndex) !=
             TE_ACTIVE))
        {
            TE_RSVPTE_TPARAM_MPU (pTeTrfcParams) =
                (UINT4)i4SetValFsMplsTunnelRSVPResMinimumPolicedUnit;
            TE_RSVPTE_TPARAM_CFG_FLAG (pTeTrfcParams) |= TE_RSVPTE_CFG_MPU_FLAG;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsTunnelRSVPResMaximumPacketSize
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                setValFsMplsTunnelRSVPResMaximumPacketSize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsTunnelRSVPResMaximumPacketSize (UINT4 u4MplsTunnelResourceIndex,
                                            INT4
                                            i4SetValFsMplsTunnelRSVPResMaximumPacketSize)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if ((TE_RSVPTE_TRFC_PARAMS (pTeTrfcParams) != NULL) &&
            (TE_TRFC_PARAMS_TNL_RESROWSTATUS (u4MplsTunnelResourceIndex) !=
             TE_ACTIVE))
        {
            TE_RSVPTE_TPARAM_MPS (pTeTrfcParams) =
                (UINT4)i4SetValFsMplsTunnelRSVPResMaximumPacketSize;
            TE_RSVPTE_TPARAM_CFG_FLAG (pTeTrfcParams) |= TE_RSVPTE_CFG_MPS_FLAG;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetMplsTunnelNotificationMaxRate
 Input       :  The Indices

                The Object
                setValMplsTunnelNotificationMaxRate
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelNotificationMaxRate (UINT4
                                     u4SetValMplsTunnelNotificationMaxRate)
{
    MPLS_CMN_LOCK ();
    TE_NOTIFICATION_MAX_RATE (gTeGblInfo) =
        u4SetValMplsTunnelNotificationMaxRate;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2MplsTunnelNotificationMaxRate
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MplsTunnelNotificationMaxRate (UINT4 *pu4ErrorCode,
                                       tSnmpIndexList * pSnmpIndexList,
                                       tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetMplsTunnelNotificationEnable
 Input       :  The Indices

                The Object
                setValMplsTunnelNotificationEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetMplsTunnelNotificationEnable (INT4 i4SetValMplsTunnelNotificationEnable)
{
    gTeGblInfo.TeParams.i4NotificationEnable =
        i4SetValMplsTunnelNotificationEnable;
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2MplsTunnelNotificationEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2MplsTunnelNotificationEnable (UINT4 *pu4ErrorCode,
                                      tSnmpIndexList * pSnmpIndexList,
                                      tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/*                       End of file teset2.c                                */
/*---------------------------------------------------------------------------*/
