/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: thlutlg.c,v 1.6 2014/07/12 12:26:53 siva Exp $
*
* Description: This file contains utility functions used by protocol 
*********************************************************************/

#include "teincs.h"
#include "rmgr.h"

extern UINT4        FsMplsLSPMaptunnelOperation[];
extern UINT4        FsMplsLSPMaptunnelRowStatus[];

/*****************************************************************************
 * Function Name : TeFsMplsLSPMapTunnelTableCreate
 * Description     : This routine creates the memory pool and
 *                       RB Tree node for storing entries of 
 *                       FsMplsLSPMapTunnelTable.
 * Input(s)      : None
 * Output(s)     : None
 * Return(s)     : TE_SUCCESS/TE_FAILURE
 *****************************************************************************/

INT4
TeFsMplsLSPMapTunnelTableCreate ()
{
    gTeLSPMapTnlGblInfo.pLSPMapTnlRBTree =
        RBTreeCreate (MAX_TE_DEF_HLSP, FsMplsLSPMapTunnelTableRBCmp);

    if (gTeLSPMapTnlGblInfo.pLSPMapTnlRBTree == NULL)
    {
        TE_DBG (TE_MAIN_FAIL,
                "FsMplsLSPMapTunnelTable  RB Tree Create Failed \t\n");
        return TE_FAILURE;
    }
    return TE_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeFsMplsLSPMapTunnelTableDelete
 * Description     : This routine deletes the memory pool and
 *                         RB Tree node for storing entries of 
 *                         FsMplsLSPMapTunnelTable.
 * Input(s)      : None
 * Output(s)     : None
 * Return(s)     : TE_SUCCESS/TE_FAILURE
 *****************************************************************************/

INT4
TeFsMplsLSPMapTunnelTableDelete ()
{
    if (gTeLSPMapTnlGblInfo.pLSPMapTnlRBTree != NULL)
    {
        RBTreeDelete (gTeLSPMapTnlGblInfo.pLSPMapTnlRBTree);
        gTeLSPMapTnlGblInfo.pLSPMapTnlRBTree = NULL;
    }
    return TE_SUCCESS;
}

/************************************************************************
 *  Function Name   : FsMplsLSPMapTunnelTableRBCmp 
 *  Description     : RBTree Compare function for Mpls LSP Map Tunnel Table
 *  Input           : Two RBTree Nodes to be compared
 *  Output          : None
 *  Returns         : 1/(-1)/0 
 ************************************************************************/

PUBLIC INT4
FsMplsLSPMapTunnelTableRBCmp (tRBElem * pRBElem1, tRBElem * pRBElem2)
{

    tteFsMplsLSPMapTunnelTable *pFsMplsLSPMapTunnelTable1 =
        (tteFsMplsLSPMapTunnelTable *) pRBElem1;
    tteFsMplsLSPMapTunnelTable *pFsMplsLSPMapTunnelTable2 =
        (tteFsMplsLSPMapTunnelTable *) pRBElem2;

    if (pFsMplsLSPMapTunnelTable1->u4FsMplsLSPMapTunnelIndex >
        pFsMplsLSPMapTunnelTable2->u4FsMplsLSPMapTunnelIndex)
    {
        return 1;
    }
    else if (pFsMplsLSPMapTunnelTable1->u4FsMplsLSPMapTunnelIndex <
             pFsMplsLSPMapTunnelTable2->u4FsMplsLSPMapTunnelIndex)
    {
        return -1;
    }

    if (pFsMplsLSPMapTunnelTable1->u4FsMplsLSPMapTunnelInstance >
        pFsMplsLSPMapTunnelTable2->u4FsMplsLSPMapTunnelInstance)
    {
        return 1;
    }
    else if (pFsMplsLSPMapTunnelTable1->u4FsMplsLSPMapTunnelInstance <
             pFsMplsLSPMapTunnelTable2->u4FsMplsLSPMapTunnelInstance)
    {
        return -1;
    }

    if (pFsMplsLSPMapTunnelTable1->u4FsMplsLSPMapTunnelIngressLSRId >
        pFsMplsLSPMapTunnelTable2->u4FsMplsLSPMapTunnelIngressLSRId)
    {
        return 1;
    }
    else if (pFsMplsLSPMapTunnelTable1->u4FsMplsLSPMapTunnelIngressLSRId <
             pFsMplsLSPMapTunnelTable2->u4FsMplsLSPMapTunnelIngressLSRId)
    {
        return -1;
    }

    if (pFsMplsLSPMapTunnelTable1->u4FsMplsLSPMapTunnelEgressLSRId >
        pFsMplsLSPMapTunnelTable2->u4FsMplsLSPMapTunnelEgressLSRId)
    {
        return 1;
    }
    else if (pFsMplsLSPMapTunnelTable1->u4FsMplsLSPMapTunnelEgressLSRId <
             pFsMplsLSPMapTunnelTable2->u4FsMplsLSPMapTunnelEgressLSRId)
    {
        return -1;
    }

    if (pFsMplsLSPMapTunnelTable1->u4FsMplsLSPMapSubTunnelIndex >
        pFsMplsLSPMapTunnelTable2->u4FsMplsLSPMapSubTunnelIndex)
    {
        return 1;
    }
    else if (pFsMplsLSPMapTunnelTable1->u4FsMplsLSPMapSubTunnelIndex <
             pFsMplsLSPMapTunnelTable2->u4FsMplsLSPMapSubTunnelIndex)
    {
        return -1;
    }

    if (pFsMplsLSPMapTunnelTable1->u4FsMplsLSPMapSubTunnelInstance >
        pFsMplsLSPMapTunnelTable2->u4FsMplsLSPMapSubTunnelInstance)
    {
        return 1;
    }
    else if (pFsMplsLSPMapTunnelTable1->u4FsMplsLSPMapSubTunnelInstance <
             pFsMplsLSPMapTunnelTable2->u4FsMplsLSPMapSubTunnelInstance)
    {
        return -1;
    }

    if (pFsMplsLSPMapTunnelTable1->u4FsMplsLSPMapSubTunnelIngressLSRId >
        pFsMplsLSPMapTunnelTable2->u4FsMplsLSPMapSubTunnelIngressLSRId)
    {
        return 1;
    }
    else if (pFsMplsLSPMapTunnelTable1->u4FsMplsLSPMapSubTunnelIngressLSRId <
             pFsMplsLSPMapTunnelTable2->u4FsMplsLSPMapSubTunnelIngressLSRId)
    {
        return -1;
    }

    if (pFsMplsLSPMapTunnelTable1->u4FsMplsLSPMapSubTunnelEgressLSRId >
        pFsMplsLSPMapTunnelTable2->u4FsMplsLSPMapSubTunnelEgressLSRId)
    {
        return 1;
    }
    else if (pFsMplsLSPMapTunnelTable1->u4FsMplsLSPMapSubTunnelEgressLSRId <
             pFsMplsLSPMapTunnelTable2->u4FsMplsLSPMapSubTunnelEgressLSRId)
    {
        return -1;
    }

    return 0;
}

#if 0
INT1
nmhSetCmnNew (UINT4 au4ObjectID[], INT4 i4OidLen,
              INT4 (*LockPtr) (VOID),
              INT4 (*UnlockPtr) (VOID),
              UINT4 u4Deprecated,
              UINT4 u4RowStatus, INT4 i4Indices, INT4 i4SetOption,
              CHR1 * fmt, ...)
{
    UINT4               u4SeqNum;
    tSnmpNotifyInfo     SnmpNotifyInfo;
    UNUSED_PARAM (i4OidLen);
    UNUSED_PARAM (u4Deprecated);
    UNUSED_PARAM (i4SetOption);
    RM_GET_SEQ_NUM (&u4SeqNum);

    SNMP_SET_NOTIFY_INFO (SnmpNotifyInfo, au4ObjectID, u4SeqNum,
                          (UINT1) u4RowStatus, LockPtr, UnlockPtr,
                          (UINT4) i4Indices, SNMP_SUCCESS);

    SNMP_NOTIFY_CFG ((SnmpNotifyInfo, fmt));

    return SNMP_SUCCESS;
}
#endif
/****************************************************************************
 Function    :  TeSetAllFsMplsLSPMapTunnelTableTrigger
 Input       :  The Indices
                pTeSetFsMplsLSPMapTunnelTable
                pTeIsSetFsMplsLSPMapTunnelTable
 Output      :  This Routine is used to send 
                MSR and RM indication
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
TeSetAllFsMplsLSPMapTunnelTableTrigger (tteFsMplsLSPMapTunnelTable
                                        * pTeSetFsMplsLSPMapTunnelTable,
                                        tteIsSetFsMplsLSPMapTunnelTable *
                                        pTeIsSetFsMplsLSPMapTunnelTable,
                                        INT4 i4SetOption)
{
    if (pTeIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMaptunnelOperation ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsMplsLSPMaptunnelOperation, 13, CmnLock,
                      CmnUnLock, 0, 0, 8, i4SetOption,
                      "%u %u %u %u %u %u %u %u %u",
                      pTeSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelIndex,
                      pTeSetFsMplsLSPMapTunnelTable->
                      u4FsMplsLSPMapTunnelInstance,
                      pTeSetFsMplsLSPMapTunnelTable->
                      u4FsMplsLSPMapTunnelIngressLSRId,
                      pTeSetFsMplsLSPMapTunnelTable->
                      u4FsMplsLSPMapTunnelEgressLSRId,
                      pTeSetFsMplsLSPMapTunnelTable->
                      u4FsMplsLSPMapSubTunnelIndex,
                      pTeSetFsMplsLSPMapTunnelTable->
                      u4FsMplsLSPMapSubTunnelInstance,
                      pTeSetFsMplsLSPMapTunnelTable->
                      u4FsMplsLSPMapSubTunnelIngressLSRId,
                      pTeSetFsMplsLSPMapTunnelTable->
                      u4FsMplsLSPMapSubTunnelEgressLSRId,
                      pTeSetFsMplsLSPMapTunnelTable->
                      u4FsMplsLSPMaptunnelOperation);
    }
    if (pTeIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMaptunnelRowStatus ==
        OSIX_TRUE)
    {
        nmhSetCmnNew (FsMplsLSPMaptunnelRowStatus, 13, CmnLock,
                      CmnUnLock, 0, 1, 8, i4SetOption,
                      "%u %u %u %u %u %u %u %u %i",
                      pTeSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelIndex,
                      pTeSetFsMplsLSPMapTunnelTable->
                      u4FsMplsLSPMapTunnelInstance,
                      pTeSetFsMplsLSPMapTunnelTable->
                      u4FsMplsLSPMapTunnelIngressLSRId,
                      pTeSetFsMplsLSPMapTunnelTable->
                      u4FsMplsLSPMapTunnelEgressLSRId,
                      pTeSetFsMplsLSPMapTunnelTable->
                      u4FsMplsLSPMapSubTunnelIndex,
                      pTeSetFsMplsLSPMapTunnelTable->
                      u4FsMplsLSPMapSubTunnelInstance,
                      pTeSetFsMplsLSPMapTunnelTable->
                      u4FsMplsLSPMapSubTunnelIngressLSRId,
                      pTeSetFsMplsLSPMapTunnelTable->
                      u4FsMplsLSPMapSubTunnelEgressLSRId,
                      pTeSetFsMplsLSPMapTunnelTable->
                      i4FsMplsLSPMaptunnelRowStatus);
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  FsMplsLSPMapTunnelTableFilterInputs
 Input       :  The Indices
                pTeFsMplsLSPMapTunnelTable
                pTeSetFsMplsLSPMapTunnelTable
                pTeIsSetFsMplsLSPMapTunnelTable
 Output      :  This Routine checks set value 
                with that of the value in database
                to find whether the same input is given
 Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
FsMplsLSPMapTunnelTableFilterInputs (tteFsMplsLSPMapTunnelTable *
                                     pTeFsMplsLSPMapTunnelTable,
                                     tteFsMplsLSPMapTunnelTable *
                                     pTeSetFsMplsLSPMapTunnelTable,
                                     tteIsSetFsMplsLSPMapTunnelTable *
                                     pTeIsSetFsMplsLSPMapTunnelTable)
{
    if (pTeIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapTunnelIndex == OSIX_TRUE)
    {
        if (pTeFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelIndex ==
            pTeSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelIndex)
            pTeIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapTunnelIndex =
                OSIX_FALSE;
    }
    if (pTeIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapTunnelInstance ==
        OSIX_TRUE)
    {
        if (pTeFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelInstance ==
            pTeSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelInstance)
            pTeIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapTunnelInstance =
                OSIX_FALSE;
    }
    if (pTeIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapTunnelIngressLSRId ==
        OSIX_TRUE)
    {
        if (pTeFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelIngressLSRId ==
            pTeSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelIngressLSRId)
            pTeIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapTunnelIngressLSRId =
                OSIX_FALSE;
    }
    if (pTeIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapTunnelEgressLSRId ==
        OSIX_TRUE)
    {
        if (pTeFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelEgressLSRId ==
            pTeSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelEgressLSRId)
            pTeIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapTunnelEgressLSRId =
                OSIX_FALSE;
    }
    if (pTeIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapSubTunnelIndex ==
        OSIX_TRUE)
    {
        if (pTeFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelIndex ==
            pTeSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelIndex)
            pTeIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapSubTunnelIndex =
                OSIX_FALSE;
    }
    if (pTeIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapSubTunnelInstance ==
        OSIX_TRUE)
    {
        if (pTeFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelInstance ==
            pTeSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelInstance)
            pTeIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapSubTunnelInstance =
                OSIX_FALSE;
    }
    if (pTeIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapSubTunnelIngressLSRId ==
        OSIX_TRUE)
    {
        if (pTeFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelIngressLSRId ==
            pTeSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelIngressLSRId)
            pTeIsSetFsMplsLSPMapTunnelTable->
                bFsMplsLSPMapSubTunnelIngressLSRId = OSIX_FALSE;
    }
    if (pTeIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapSubTunnelEgressLSRId ==
        OSIX_TRUE)
    {
        if (pTeFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelEgressLSRId ==
            pTeSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelEgressLSRId)
            pTeIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapSubTunnelEgressLSRId =
                OSIX_FALSE;
    }
    if (pTeIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMaptunnelOperation ==
        OSIX_TRUE)
    {
        if (pTeFsMplsLSPMapTunnelTable->u4FsMplsLSPMaptunnelOperation ==
            pTeSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMaptunnelOperation)
            pTeIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMaptunnelOperation =
                OSIX_FALSE;
    }
    if (pTeIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMaptunnelRowStatus ==
        OSIX_TRUE)
    {
        if (pTeFsMplsLSPMapTunnelTable->i4FsMplsLSPMaptunnelRowStatus ==
            pTeSetFsMplsLSPMapTunnelTable->i4FsMplsLSPMaptunnelRowStatus)
            pTeIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMaptunnelRowStatus =
                OSIX_FALSE;
    }

    if ((pTeIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapTunnelIndex ==
         OSIX_FALSE)
        && (pTeIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapTunnelIndex ==
            OSIX_FALSE)
        && (pTeIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapTunnelInstance ==
            OSIX_FALSE)
        && (pTeIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapTunnelIngressLSRId ==
            OSIX_FALSE)
        && (pTeIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapTunnelEgressLSRId ==
            OSIX_FALSE)
        && (pTeIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapSubTunnelIndex ==
            OSIX_FALSE)
        && (pTeIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapSubTunnelInstance ==
            OSIX_FALSE)
        && (pTeIsSetFsMplsLSPMapTunnelTable->
            bFsMplsLSPMapSubTunnelIngressLSRId == OSIX_FALSE)
        && (pTeIsSetFsMplsLSPMapTunnelTable->
            bFsMplsLSPMapSubTunnelEgressLSRId == OSIX_FALSE)
        && (pTeIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMaptunnelOperation ==
            OSIX_FALSE)
        && (pTeIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMaptunnelRowStatus ==
            OSIX_FALSE))
    {
        return OSIX_FALSE;
    }
    else
    {
        return OSIX_TRUE;
    }
}

/****************************************************************************
 * Function    :  TeUtilUpdateFsMplsLSPMapTunnelTable
 * Input       :  The Indices
                pTeFsMplsLSPMapTunnelTable
                pTeSetFsMplsLSPMapTunnelTable
 * Output      :  This Routine checks set value 
                with that of the value in database
                and do the necessary protocol operation
 * Returns     :  OSIX_TRUE or OSIX_FALSE
****************************************************************************/
INT4
TeUtilUpdateFsMplsLSPMapTunnelTable (tteFsMplsLSPMapTunnelTable *
                                     pTeOldFsMplsLSPMapTunnelTable,
                                     tteFsMplsLSPMapTunnelTable *
                                     pTeFsMplsLSPMapTunnelTable)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    /* Retrieve the service tunnel info */
    pTeTnlInfo =
        TeGetTunnelInfo (TE_LSP_MAP_SUB_TNL_INDEX (pTeFsMplsLSPMapTunnelTable),
                         TE_LSP_MAP_SUB_TNL_INST (pTeFsMplsLSPMapTunnelTable),
                         TE_LSP_MAP_SUB_TNL_INGRESS
                         (pTeFsMplsLSPMapTunnelTable),
                         TE_LSP_MAP_SUB_TNL_EGRESS
                         (pTeFsMplsLSPMapTunnelTable));
    if (pTeTnlInfo == NULL)
    {
        return TE_FAILURE;
    }

    switch (pTeFsMplsLSPMapTunnelTable->i4FsMplsLSPMaptunnelRowStatus)
    {
        case TE_ACTIVE:
            /* Update the HLSP Params if the operation is stack */
            if (TE_LSP_MAP_TNL_OPERATION (pTeFsMplsLSPMapTunnelTable) ==
                TE_OPER_STACK)
            {
                if (TE_FAILURE ==
                    TeUpdateHLSPParams (pTeFsMplsLSPMapTunnelTable))
                {
                    return TE_FAILURE;
                }
            }
            else
            {
                /* The operation is stitch, hence stitch the e2e LSP with 
                 * the S-LSP tunnel.*/
                if (TeUpdateSLSPParams (pTeFsMplsLSPMapTunnelTable) ==
                    TE_FAILURE)
                {
                    return TE_FAILURE;
                }
            }
            break;

        case TE_DESTROY:
            if ((pTeOldFsMplsLSPMapTunnelTable != NULL) &&
                (pTeOldFsMplsLSPMapTunnelTable->i4FsMplsLSPMaptunnelRowStatus ==
                 TE_ACTIVE))
            {
                /* Delete the HLSP Params if the operation is stack */
                if (TE_LSP_MAP_TNL_OPERATION (pTeFsMplsLSPMapTunnelTable) ==
                    TE_OPER_STACK)
                {
                    if (TE_FAILURE ==
                        TeDeleteHLSPParams (pTeFsMplsLSPMapTunnelTable))
                    {
                        return TE_FAILURE;
                    }
                }
                else
                {
                    if (TeDeleteSLSPParams (pTeFsMplsLSPMapTunnelTable) ==
                        TE_FAILURE)
                    {
                        return TE_FAILURE;
                    }
                }

                /* Update the Service/e2e LSP tunnel oper status to DOWN when 
                 *  stacking/stitching is removed.*/
                if (TE_SUCCESS !=
                    TeCallUpdateTnlOperStatus (pTeTnlInfo, TE_OPER_DOWN))
                {
                    return TE_FAILURE;
                }
            }
            break;
        default:
            break;
    }
    return TE_SUCCESS;
}
