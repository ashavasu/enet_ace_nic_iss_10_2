/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: thlmain.c,v 1.20 2014/12/24 10:58:31 siva Exp $
*
* Description: This file contains hlsp related functions 
*********************************************************************/

#include "teincs.h"
#include "tegblex.h"
#include "snmputil.h"
#include "mplslsr.h"
#include "stdlsrlw.h"

UINT4
 
            TeDeleteILMInHw (tTeTnlInfo * pTeTnlInfo, tInSegment * pInSegment);
UINT4
              TeAddILMInHw (tTeTnlInfo * pTeTnlInfo, tInSegment * pInSegment);

/***************************************************************************
 * Name : TeInitHLSPParams
 * Description   : This routine initialises the H-LSP/S-LSP related params 
 *                     when the tunnel type is configured as HLSP/S-LSP.
 * Input(s)      : pTeTnlInfo - Pointer to Tunnel Information Structure.
 * Output(s)     : None
 * Return(s)     : TE_SUCCESS/TE_FAILURE
 *****************************************************************************/

UINT4
TeInitHLSPParams (tTeTnlInfo * pTeTnlInfo)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    tTeTnlInfo         *pInstance0Tnl = NULL;
    tTeTnlInfo         *pTeTmpTnlInfo = NULL;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;

    TE_DBG (TE_MAIN_ETEXT, "TeInitHLSPParams : ENTRY \n");
    if (pTeTnlInfo == NULL)
    {
        return TE_FAILURE;
    }

    if (pTeTnlInfo->u4TnlInstance == TE_ZERO)
    {
        return TE_SUCCESS;
    }

    TE_HLSP_INFO (pTeTnlInfo) = (tHlspParams *)
        TE_ALLOC_MEM_BLOCK (TE_TNL_HLSP_INFO_POOL_ID);

    if (TE_HLSP_INFO (pTeTnlInfo) == NULL)
    {
        return TE_FAILURE;
    }

    /* Initialise the DLL StackList to Store stacked tunnel pointers */
    TE_DLL_INIT (&TE_HLSP_STACK_LIST (pTeTnlInfo));

    CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTeTnlInfo)), u4IngressId);
    CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTeTnlInfo)), u4EgressId);
    u4IngressId = OSIX_NTOHL (u4IngressId);
    u4EgressId = OSIX_NTOHL (u4EgressId);

    pInstance0Tnl = TeGetTunnelInfo (TE_TNL_TNL_INDEX (pTeTnlInfo), TE_ZERO,
                                     u4IngressId, u4EgressId);

    pTeTmpTnlInfo = pTeTnlInfo;
    if (pInstance0Tnl != NULL)
    {
        pTeTmpTnlInfo = pInstance0Tnl;
    }

    /* Get the Traffic parameters for H-LSP/S-LSP Tunnel */
    if (TeCheckTrfcParamInTrfcParamTable
        (TE_TNL_TRFC_PARAM_INDEX (pTeTmpTnlInfo), &pTeTrfcParams) != TE_SUCCESS)
    {
        TE_DBG (TE_MAIN_FAIL,
                "TeInitHLSPParams : unable to retieve traffic Params \n");
        TE_REL_MEM_BLOCK (TE_TNL_HLSP_INFO_POOL_ID,
                          (UINT1 *) TE_HLSP_INFO (pTeTnlInfo));
        return TE_FAILURE;
    }

    /* Update the H-LSP/S-LSP Tunnel Available Bandwidth as the max Bandwidth 
     * Initialise the number of stacked tunnels */
    TE_HLSP_AVAILABLE_BW (pTeTnlInfo) = TE_TNLRSRC_MAX_RATE (pTeTrfcParams);
    TE_HLSP_NO_OF_STACKED_TNLS (pTeTnlInfo) = TE_ZERO;

    if (TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_HLSP)
    {
        /* updating total number of H-LSP in the system */
        gu4TeNoOfHlsp++;
    }

    if (TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_SLSP)
    {
        /* updating total number of S-LSP in the system */
        gu4TeNoOfSlsp++;
    }

    TE_DBG (TE_MAIN_ETEXT, "TeInitHLSPParams : EXIT \n");
    return TE_SUCCESS;
}

/***************************************************************************
 * Name : TeDeInitHLSPParams
 * Description   : This routine de-initialises the H-LSP/S-LSP related params 
 *                     when the tunnel type is configured as H-LSP/S-LSP.
 * Input(s)      : pTeTnlInfo - Pointer to H-LSP/S-LSP Tunnel Information.
 * Output(s)     : None
 * Return(s)     : TE_SUCCESS/TE_FAILURE
 *****************************************************************************/
UINT4
TeDeInitHLSPParams (tTeTnlInfo * pTeTnlInfo)
{
    TE_DBG (TE_MAIN_ETEXT, "TeDeInitSLSPParams : ENTRY \n");

    if (pTeTnlInfo->u4TnlInstance == TE_ZERO)
    {
        return TE_SUCCESS;
    }

    if (TE_HLSP_INFO (pTeTnlInfo) == NULL)
    {
        return TE_SUCCESS;
    }

    /* Release the allocated memory for maintaining H-LSP/S-LSP specific
     * information.*/
    TE_REL_MEM_BLOCK (TE_TNL_HLSP_INFO_POOL_ID,
                      (UINT1 *) TE_HLSP_INFO (pTeTnlInfo));

    if (TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_HLSP)
    {
        /* updating total number of H-LSP in the system */
        gu4TeNoOfHlsp--;
    }

    if (TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_SLSP)
    {
        /* updating total number of S-LSP in the system */
        gu4TeNoOfSlsp--;
    }

    TE_DBG (TE_MAIN_ETEXT, "TeDeInitHLSPParams : EXIT \n");
    return TE_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeUpdateHLSPParams
 * Description   : This routine updates the HLSP related information 
 *                     when the service tunnel is stacked on to HLSP.
 * Input(s)      : pteFsMplsLSPMapTunnelTable - Pointer to 
 *                     TeLSPMapTunnelTable
 * Output(s)     : None
 * Return(s)     : TE_SUCCESS/TE_FAILURE
 *****************************************************************************/

UINT4
TeUpdateHLSPParams (tteFsMplsLSPMapTunnelTable * pteFsMplsLSPMapTunnelTable)
{
    tOutSegment        *pOutSegment = NULL;
    tOutSegment        *pHlspOutSegment = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlInfo         *pTeHlspTnlInfo = NULL;
    tTeTnlInfo         *pTeTmpTnlInfo = NULL;
    tTeTnlInfo         *pInstance0Tnl = NULL;
    tTeTrfcParams      *pTeTrfcParams = NULL;
    tTeTrfcParams      *pTeHlspTrfcParams = NULL;
    tXcEntry           *pHlspXcEntry = NULL;
    tXcEntry           *pXcEntry = NULL;
    eDirection          Direction = MPLS_DIRECTION_ANY;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;

    TE_DBG (TE_MAIN_ETEXT, "TeUpdateHLSPParams : ENTRY \n");

    /* Get the HLSP and Service tunnel Info */
    pTeTnlInfo =
        TeGetTunnelInfo (TE_LSP_MAP_SUB_TNL_INDEX (pteFsMplsLSPMapTunnelTable),
                         TE_LSP_MAP_SUB_TNL_INST (pteFsMplsLSPMapTunnelTable),
                         TE_LSP_MAP_SUB_TNL_INGRESS
                         (pteFsMplsLSPMapTunnelTable),
                         TE_LSP_MAP_SUB_TNL_EGRESS
                         (pteFsMplsLSPMapTunnelTable));
    if (pTeTnlInfo == NULL)
    {
        TE_DBG (TE_MAIN_FAIL,
                "TeUpdateHLSPParams : Failure to retrieve Tunnel info \n");
        return TE_FAILURE;
    }
    pTeHlspTnlInfo =
        TeGetTunnelInfo (TE_LSP_MAP_TNL_INDEX (pteFsMplsLSPMapTunnelTable),
                         TE_LSP_MAP_TNL_INST (pteFsMplsLSPMapTunnelTable),
                         TE_LSP_MAP_TNL_INGRESS (pteFsMplsLSPMapTunnelTable),
                         TE_LSP_MAP_TNL_EGRESS (pteFsMplsLSPMapTunnelTable));
    if (pTeHlspTnlInfo == NULL)
    {
        TE_DBG (TE_MAIN_FAIL,
                "TeUpdateHLSPParams:Failure to retrieve HLSP Tunnel info \n");
        return TE_FAILURE;
    }

    /* Get the outsegment of HLSP */
    TeCmnExtGetDirectionFromTnl (pTeHlspTnlInfo, &Direction);

    pHlspXcEntry =
        MplsGetXCEntryByDirection (TE_TNL_XCINDEX (pTeHlspTnlInfo), Direction);
    if (pHlspXcEntry == NULL)
    {
        TE_DBG (TE_MAIN_FAIL,
                "TeUpdateHLSPParams : HLSP Tnl XC entry not exist\t\n");
        return MPLS_FAILURE;
    }
    pHlspOutSegment = XC_OUTINDEX (pHlspXcEntry);
    if (pHlspOutSegment == NULL)
    {
        TE_DBG (TE_MAIN_FAIL,
                "TeUpdateHLSPParams: H-LSP outsegment doesn't exists\n");
        return TE_FAILURE;
    }

    /* Get the Outsegment of the service tunnel */
    /* Service tunnel should be in the same direction as the HLSP tunnel */
    /* Take pXCEntry for both the directions for e2e tunnel. 
     * Check for the presence of mplsLabelIndex in the pXCEntry and utilise the
     * corresponding pXCEntry
     */
    pXcEntry = MplsGetXCEntryByLabelStack (TE_TNL_XCINDEX (pTeTnlInfo));
    if (pXcEntry == NULL)
    {
        TE_DBG (TE_MAIN_FAIL,
                "TeUpdateHLSPParams : Service Tnl XC entry not exist\t\n");
        return TE_FAILURE;
    }
    pOutSegment = XC_OUTINDEX (pXcEntry);
    if (pOutSegment == NULL)
    {
        TE_DBG (TE_MAIN_FAIL,
                "TeUpdateHLSPParams : Failure in getting Outsegment entry "
                "for service tunnel  \n");
        return TE_FAILURE;
    }

    /* update Service tunnel NextHop same as HLSP NextHop */
    OUTSEGMENT_PUSH_FLAG (pOutSegment) = OUTSEGMENT_PUSH_FLAG (pHlspOutSegment);
    if (OUTSEGMENT_PUSH_FLAG (pOutSegment) == MPLS_SNMP_TRUE)
    {
        OUTSEGMENT_LABEL (pOutSegment) = OUTSEGMENT_LABEL (pHlspOutSegment);
    }
    else
    {
        /*If Push Label Flag is FALSE then this value should be "0" */
        OUTSEGMENT_LABEL (pOutSegment) = 0;
    }
    OUTSEGMENT_NH_ADDR (pOutSegment) = OUTSEGMENT_NH_ADDR (pHlspOutSegment);

    OUTSEGMENT_NH_ADDRTYPE (pOutSegment) =
        OUTSEGMENT_NH_ADDRTYPE (pHlspOutSegment);

    /* Retrieve the Traffic Parameters for HLSP and service tunnel */
    if (TeCheckTrfcParamInTrfcParamTable
        (TE_TNL_TRFC_PARAM_INDEX (pTeHlspTnlInfo),
         &pTeHlspTrfcParams) != TE_SUCCESS)
    {
        TE_DBG (TE_MAIN_FAIL,
                "TeUpdateHLSPParams :unable to retieve traffic Params "
                "for HLSP\n");
        return TE_FAILURE;
    }

    CONVERT_TO_INTEGER ((TE_TNL_INGRESS_LSRID (pTeTnlInfo)), u4IngressId);
    CONVERT_TO_INTEGER ((TE_TNL_EGRESS_LSRID (pTeTnlInfo)), u4EgressId);
    u4IngressId = OSIX_NTOHL (u4IngressId);
    u4EgressId = OSIX_NTOHL (u4EgressId);

    pInstance0Tnl = TeGetTunnelInfo (TE_TNL_TNL_INDEX (pTeTnlInfo), TE_ZERO,
                                     u4IngressId, u4EgressId);

    pTeTmpTnlInfo = pTeTnlInfo;
    if (pInstance0Tnl != NULL)
    {
        pTeTmpTnlInfo = pInstance0Tnl;
    }

    if (TeCheckTrfcParamInTrfcParamTable
        (TE_TNL_TRFC_PARAM_INDEX (pTeTmpTnlInfo), &pTeTrfcParams) != TE_SUCCESS)
    {
        TE_DBG (TE_MAIN_FAIL,
                "TeUpdateHLSPParams :unable to retieve traffic Params "
                "for service tunnel \n");
        return TE_FAILURE;
    }

    /* Update the available Bandwidth and the Number of stacked tunnels 
     * for HLSP */
    if (TE_HLSP_INFO(pTeHlspTnlInfo) != NULL)
    {
        TE_HLSP_AVAILABLE_BW (pTeHlspTnlInfo) -=
            TE_TNLRSRC_MAX_RATE (pTeTrfcParams);
        TE_HLSP_NO_OF_STACKED_TNLS (pTeHlspTnlInfo)++;
    

        /* Add the service tunnel pointer to the HLSP Stacklist */
        TMO_DLL_Init_Node ((tTMO_DLL_NODE *) & pTeTnlInfo->NextServiceTnl);
        TMO_DLL_Add (&TE_HLSP_STACK_LIST (pTeHlspTnlInfo),
                     &(pTeTnlInfo->NextServiceTnl));
    }
    /* BackPointer from the Service tunnel to point to HLSP tunnel */
    TE_MAP_TNL_INFO (pTeTnlInfo) = pTeHlspTnlInfo;

    TE_DBG (TE_MAIN_ETEXT, "TeUpdateHLSPParams : EXIT \n");
    return TE_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeDeleteILMInHw
 * Description   : This routine deletes the ILM entry in the hardware and 
 *               : software forwarding module.
 * Input(s)      : pTeTnlInfo - Pointer to the tunnel info structure
 *               : pInSegment  - Pointer to the InSegment entry
 * Output(s)     : None
 * Return(s)     : TE_SUCCESS/TE_FAILURE
 *****************************************************************************/
UINT4
TeDeleteILMInHw (tTeTnlInfo * pTeTnlInfo, tInSegment * pInSegment)
{
    BOOL1               bLlStatus = XC_OPER_DOWN;

    if ((pInSegment == NULL) || (pTeTnlInfo == NULL))
    {
        TE_DBG (TE_MAIN_FAIL,
                "TeDeleteILMInHw : Insegment or Tunnel pointer is NULL\t\n");
        return TE_FAILURE;
    }

    if (MplsDbUpdateTunnelInHw
        (MPLS_MLIB_ILM_DELETE, pTeTnlInfo,
         pInSegment->GmplsInSegment.InSegmentDirection,
         &bLlStatus) == MPLS_FAILURE)
    {
        TE_DBG (TE_MAIN_FAIL,
                "TeDeleteILMInHw : MplsDbUpdateTunnelInHw Failed \t\n");
        return TE_FAILURE;
    }

    if (MplsDbInSegmentSignalRbTreeDelete ((VOID *) pInSegment) == MPLS_FAILURE)
    {
        TE_DBG (TE_MAIN_FAIL,
                "TeDeleteILMInHw : InSegment Signal deletion "
                "in RBTree failed.\n");
        return TE_FAILURE;
    }

    return TE_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeAddILMInHw
 * Description   : This routine adds the ILM entry in the hardware and 
 *               : software forwarding module.
 * Input(s)      : pTeTnlInfo - Pointer to the tunnel info structure
 *               : pInSegment  - Pointer to the InSegment entry
 * Output(s)     : None
 * Return(s)     : TE_SUCCESS/TE_FAILURE
 *****************************************************************************/
UINT4
TeAddILMInHw (tTeTnlInfo * pTeTnlInfo, tInSegment * pInSegment)
{
    BOOL1               bLlStatus = XC_OPER_DOWN;

    if ((pInSegment == NULL) || (pTeTnlInfo == NULL))
    {
        TE_DBG (TE_MAIN_FAIL,
                "TeDeleteILMInHw : Insegment or Tunnel pointer is NULL\t\n");
        return TE_FAILURE;
    }

    if (MplsDbUpdateTunnelInHw
        (MPLS_MLIB_ILM_CREATE, pTeTnlInfo,
         pInSegment->GmplsInSegment.InSegmentDirection,
         &bLlStatus) == MPLS_FAILURE)
    {
        TE_DBG (TE_MAIN_FAIL,
                "TeDeleteILMInHw : MplsDbUpdateTunnelInHw Failed \t\n");
        return TE_FAILURE;
    }

    if (MplsDbInSegmentSignalRbTreeAdd ((VOID *) pInSegment) == MPLS_FAILURE)
    {
        TE_DBG (TE_MAIN_FAIL,
                "TeAddILMInHw : InSegment Signal addition "
                "in RBTree failed.\n");
        return TE_FAILURE;
    }

    return TE_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeUpdateSLSPParams
 * Description   : This routine updates the S-LSP related information when 
 *               : the e2e LSP tunnel is stitched to S-LSP.
 * Input(s)      : pteFsMplsLSPMapTunnelTable - Pointer to 
 *                     TeLSPMapTunnelTable
 * Output(s)     : None
 * Return(s)     : TE_SUCCESS/TE_FAILURE
 *****************************************************************************/
UINT4
TeUpdateSLSPParams (tteFsMplsLSPMapTunnelTable * pteFsMplsLSPMapTunnelTable)
{

    tTeTnlInfo         *pTeE2eLSPInfo = NULL;
    tTeTnlInfo         *pTeSlspTnlInfo = NULL;
    tXcEntry           *pFwdSlspXcEntry = NULL;
    tXcEntry           *pFwdE2eLspXcEntry = NULL;
    tXcEntry           *pRevSlspXcEntry = NULL;
    tXcEntry           *pRevE2eLspXcEntry = NULL;
    tOutSegment        *pE2eLspOutSegment = NULL;
    tOutSegment        *pSlspOutSegment = NULL;
    tInSegment         *pE2eLspInSegment = NULL;
    tInSegment         *pSlspInSegment = NULL;
    tTeTrfcParams      *pTeTrfcParams = NULL;

    TE_DBG (TE_MAIN_ETEXT, "TeUpdateSLSPParams : ENTRY \n");

    /* Get the S-LSP and e2e LSP tunnel Info */
    pTeE2eLSPInfo =
        TeGetTunnelInfo (TE_LSP_MAP_SUB_TNL_INDEX (pteFsMplsLSPMapTunnelTable),
                         TE_LSP_MAP_SUB_TNL_INST (pteFsMplsLSPMapTunnelTable),
                         TE_LSP_MAP_SUB_TNL_INGRESS
                         (pteFsMplsLSPMapTunnelTable),
                         TE_LSP_MAP_SUB_TNL_EGRESS
                         (pteFsMplsLSPMapTunnelTable));
    if (pTeE2eLSPInfo == NULL)
    {
        TE_DBG (TE_MAIN_FAIL,
                "TeUpdateSLSPParams : Failure to retrieve e2e LSP Tnl info \n");
        return TE_FAILURE;
    }
    pTeSlspTnlInfo =
        TeGetTunnelInfo (TE_LSP_MAP_TNL_INDEX (pteFsMplsLSPMapTunnelTable),
                         TE_LSP_MAP_TNL_INST (pteFsMplsLSPMapTunnelTable),
                         TE_LSP_MAP_TNL_INGRESS (pteFsMplsLSPMapTunnelTable),
                         TE_LSP_MAP_TNL_EGRESS (pteFsMplsLSPMapTunnelTable));
    if (pTeSlspTnlInfo == NULL)
    {
        TE_DBG (TE_MAIN_FAIL,
                "TeUpdateSLSPParams:Failure to retrieve S-LSP Tnl info \n");
        return TE_FAILURE;
    }

    /* 1. Update the Fwd out-segment information from the S-LSP to e2e LSP 
     *    when e2e LSP is stitched to S-LSP at ingress/intermediate of e2e LSP 
     *    and S-LSP being the ingress.
     * 2. Update the Fwd in-segment of e2e LSP from S-LSP when e2e LSP is 
     *    stitched to S-LSP at intermediate node of e2e LSP and S-LSP being the 
     *    egress node.
     * 3. Update the Fwd In-segment of e2e LSP from S-LSP when the stitching 
     *    is done at egress node of e2e LSP and S-LSP is also being the egress.
     *    */
    pFwdSlspXcEntry = MplsGetXCEntryByDirection
        (TE_TNL_XCINDEX (pTeSlspTnlInfo), MPLS_DIRECTION_FORWARD);
    if (pFwdSlspXcEntry == NULL)
    {
        TE_DBG (TE_MAIN_FAIL,
                "TeUpdateSLSPParams : Forward XC doesn't exists "
                "for S-LSP.\n");
        return TE_FAILURE;
    }

    pSlspOutSegment = XC_OUTINDEX (pFwdSlspXcEntry);

    pFwdE2eLspXcEntry = MplsGetXCEntryByDirection
        (TE_TNL_XCINDEX (pTeE2eLSPInfo), MPLS_DIRECTION_FORWARD);

    if (pFwdE2eLspXcEntry == NULL)
    {
        TE_DBG (TE_MAIN_FAIL,
                "TeUpdateSLSPParams : Forward XC doesn't exists "
                "for e2e LSP.\n");
        return TE_FAILURE;
    }

    pE2eLspOutSegment = XC_OUTINDEX (pFwdE2eLspXcEntry);

    if (((TE_TNL_ROLE (pTeE2eLSPInfo) == TE_INGRESS) ||
         (TE_TNL_ROLE (pTeE2eLSPInfo) == TE_INTERMEDIATE)) &&
        (TE_TNL_ROLE (pTeSlspTnlInfo) == TE_INGRESS))
    {
        if ((pSlspOutSegment != NULL) && (pE2eLspOutSegment != NULL))
        {
            /* Copy the S-LSP out-segment to E2e LSP out-segment. */
            OUTSEGMENT_PUSH_FLAG (pE2eLspOutSegment) =
                OUTSEGMENT_PUSH_FLAG (pSlspOutSegment);

            OUTSEGMENT_LABEL (pE2eLspOutSegment) =
                OUTSEGMENT_LABEL (pSlspOutSegment);

            OUTSEGMENT_NH_ADDR (pE2eLspOutSegment) =
                OUTSEGMENT_NH_ADDR (pSlspOutSegment);

            OUTSEGMENT_NH_ADDRTYPE (pE2eLspOutSegment) =
                OUTSEGMENT_NH_ADDRTYPE (pSlspOutSegment);

        }
    }

    if (((TE_TNL_ROLE (pTeE2eLSPInfo) == TE_EGRESS) ||
         (TE_TNL_ROLE (pTeE2eLSPInfo) == TE_INTERMEDIATE)) &&
        (TE_TNL_ROLE (pTeSlspTnlInfo) == TE_EGRESS))

    {
        /* Copy the S-LSP in-segment to E2E LSP in-segment. */
        pSlspInSegment = XC_ININDEX (pFwdSlspXcEntry);
        if (pSlspInSegment != NULL)
        {
            pE2eLspInSegment = XC_ININDEX (pFwdE2eLspXcEntry);

            INSEGMENT_LABEL (pE2eLspInSegment) =
                INSEGMENT_LABEL (pSlspInSegment);

            INSEGMENT_NPOP (pE2eLspInSegment) = INSEGMENT_NPOP (pSlspInSegment);

            INSEGMENT_ADDR_FAMILY (pE2eLspInSegment) =
                INSEGMENT_ADDR_FAMILY (pSlspInSegment);

            /* Delete the ILM entry of S-LSP in the hardware. */
            if (TeDeleteILMInHw (pTeSlspTnlInfo, pSlspInSegment) == TE_FAILURE)
            {
                TE_DBG (TE_MAIN_FAIL,
                        "TeUpdateSLSPParams : ILM entry deletion failed "
                        "for S-LSP.\n");
                return TE_FAILURE;
            }

        }
    }

    /* This is applicable only when the tunnels are co-routed bi-directional.
     * 
     * 1. Update the Rev out-segment information from the S-LSP to e2e LSP 
     *    when e2e LSP is stitched to S-LSP at egress/intermediate of e2e LSP 
     *    and S-LSP being the egress.
     * 2. Update the Rev out-segment of e2e LSP from S-LSP when e2e LSP is 
     *    stitched to S-LSP at intermediate node of e2e LSP and S-LSP being the 
     *    ingress node.
     * 3. Update the Rev In-segment of e2e LSP from S-LSP when the stitching 
     *    is done at ingress node of e2e LSP and S-LSP is also being the 
     *    ingress.
     *    */

    if (pTeSlspTnlInfo->u4TnlMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
    {
        pRevSlspXcEntry = MplsGetXCEntryByDirection
            (TE_TNL_XCINDEX (pTeSlspTnlInfo), MPLS_DIRECTION_REVERSE);
        if (pRevSlspXcEntry == NULL)
        {
            TE_DBG (TE_MAIN_FAIL,
                    "TeUpdateSLSPParams : Reverse XC doesn't exists "
                    "for S-LSP.\n");
            return TE_FAILURE;
        }
        pSlspOutSegment = XC_OUTINDEX (pRevSlspXcEntry);

        pRevE2eLspXcEntry = MplsGetXCEntryByDirection
            (TE_TNL_XCINDEX (pTeE2eLSPInfo), MPLS_DIRECTION_REVERSE);

        if (pRevE2eLspXcEntry == NULL)
        {
            TE_DBG (TE_MAIN_FAIL,
                    "TeUpdateSLSPParams : Reverse XC doesn't exists "
                    "for e2e LSP.\n");
            return TE_FAILURE;
        }

        pE2eLspOutSegment = XC_OUTINDEX (pRevE2eLspXcEntry);

        if (((TE_TNL_ROLE (pTeE2eLSPInfo) == TE_EGRESS) ||
             (TE_TNL_ROLE (pTeE2eLSPInfo) == TE_INTERMEDIATE)) &&
            (TE_TNL_ROLE (pTeSlspTnlInfo) == TE_EGRESS))
        {
            if ((pSlspOutSegment != NULL) && (pE2eLspOutSegment != NULL))
            {
                /* Copy the S-LSP out-segment to E2e LSP out-segment. */
                OUTSEGMENT_PUSH_FLAG (pE2eLspOutSegment) =
                    OUTSEGMENT_PUSH_FLAG (pSlspOutSegment);

                OUTSEGMENT_LABEL (pE2eLspOutSegment) =
                    OUTSEGMENT_LABEL (pSlspOutSegment);

                OUTSEGMENT_NH_ADDR (pE2eLspOutSegment) =
                    OUTSEGMENT_NH_ADDR (pSlspOutSegment);

                OUTSEGMENT_NH_ADDRTYPE (pE2eLspOutSegment) =
                    OUTSEGMENT_NH_ADDRTYPE (pSlspOutSegment);

            }
        }

        if (((TE_TNL_ROLE (pTeE2eLSPInfo) == TE_INGRESS) ||
             (TE_TNL_ROLE (pTeE2eLSPInfo) == TE_INTERMEDIATE)) &&
            (TE_TNL_ROLE (pTeSlspTnlInfo) == TE_INGRESS))
        {
            /* Copy the S-LSP in-segment to E2E LSP in-segment. */
            pSlspInSegment = XC_ININDEX (pRevSlspXcEntry);
            if (pSlspInSegment != NULL)
            {
                pE2eLspInSegment = XC_ININDEX (pRevE2eLspXcEntry);
                if (pE2eLspInSegment != NULL)
                {
                    INSEGMENT_LABEL (pE2eLspInSegment) =
                        INSEGMENT_LABEL (pSlspInSegment);

                    INSEGMENT_NPOP (pE2eLspInSegment) =
                        INSEGMENT_NPOP (pSlspInSegment);

                    INSEGMENT_ADDR_FAMILY (pE2eLspInSegment) =
                        INSEGMENT_ADDR_FAMILY (pSlspInSegment);

                    /* Delete the ILM entry of S-LSP in the hardware. */
                    if (TeDeleteILMInHw (pTeSlspTnlInfo,
                                         pSlspInSegment) == TE_FAILURE)
                    {
                        TE_DBG (TE_MAIN_FAIL,
                                "TeUpdateSLSPParams : ILM entry deletion failed "
                                "for S-LSP.\n");
                        return TE_FAILURE;
                    }

                }
            }
        }
    }

    TE_MAP_TNL_INFO (pTeE2eLSPInfo) = pTeSlspTnlInfo;
    TE_MAP_TNL_INFO (pTeSlspTnlInfo) = pTeE2eLSPInfo;

    if (TeCheckTrfcParamInTrfcParamTable
        (TE_TNL_TRFC_PARAM_INDEX (pTeE2eLSPInfo), &pTeTrfcParams) != TE_SUCCESS)
    {
        TE_DBG (TE_MAIN_FAIL, "TeUpdateSLSPParams: unable to retieve "
                "traffic Params for e2e LSP tunnel \n");
        return TE_FAILURE;
    }

    /* Update the available Bandwidth of S-LSP */
    TE_HLSP_AVAILABLE_BW (pTeSlspTnlInfo) -=
        TE_TNLRSRC_MAX_RATE (pTeTrfcParams);

    TE_DBG (TE_MAIN_ETEXT, "TeUpdateSLSPParams : EXIT \n");
    return TE_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeDeleteHLSPParams
 * Description     : This routine deletes the HLSP related information 
 *                       when the service tunnel is removed from HLSP.
 * Input(s)      : pteFsMplsLSPMapTunnelTable - Pointer to 
 *                       TeLSPMapTunnelTable
 * Output(s)     : None
 * Return(s)     : TE_SUCCESS/TE_FAILURE
 *****************************************************************************/
UINT4
TeDeleteHLSPParams (tteFsMplsLSPMapTunnelTable * pteFsMplsLSPMapTunnelTable)
{
    UINT4               u4IPAddr = TE_ZERO;
    INT4                i4NextHopAddrType = TE_ZERO;
    tOutSegment        *pOutSegment = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlInfo         *pTeHlspTnlInfo = NULL;
    tTeTrfcParams      *pTeTrfcParams = NULL;
    tTeTrfcParams      *pTeHlspTrfcParams = NULL;
    tXcEntry           *pXcEntry = NULL;
    eDirection          Direction = MPLS_DIRECTION_ANY;
    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeDeleteHLSPParams : ENTRY \n");

    /* Get the HLSP and service tunnel info */
    pTeTnlInfo = TeGetTunnelInfo
        ((UINT4) pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelIndex,
         pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelInstance,
         pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelIngressLSRId,
         pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        TE_DBG (TE_MAIN_FAIL,
                "TeDeleteHLSPParams :Failure to retrieve Tunnel info \n");
        return TE_FAILURE;
    }
    pTeHlspTnlInfo = TeGetTunnelInfo
        ((UINT4) pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelIndex,
         pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelInstance,
         pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelIngressLSRId,
         pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelEgressLSRId);
    if (pTeHlspTnlInfo == NULL)
    {
        TE_DBG (TE_MAIN_FAIL,
                "TeDeleteHLSPParams :Failure to retrieve HLSP Tunnel info \n");
        return TE_FAILURE;
    }

    /* Get the Outsegment of the service tunnel 
     * Service tunnel direction should be same as HLSP direction 
     * Direction of HLSP is hence used to retrive service tunnel info */

    TeCmnExtGetDirectionFromTnl (pTeHlspTnlInfo, &Direction);
    pXcEntry =
        MplsGetXCEntryByDirection (TE_TNL_XCINDEX (pTeTnlInfo), Direction);
    if (pXcEntry == NULL)
    {
        TE_DBG (TE_MAIN_FAIL,
                "TeDeleteHLSPParams : Service Tnl XC entry not exist\t\n");
        return TE_FAILURE;
    }
    pOutSegment = pXcEntry->pOutIndex;
    if (pOutSegment == NULL)
    {
        TE_DBG (TE_MAIN_FAIL,
                "TeDeleteHLSPParams:Failure in getting Outsegment "
                "entry for service tunnel    \n");
        return TE_FAILURE;
    }

    OUTSEGMENT_PUSH_FLAG (pOutSegment) = (BOOL1) MPLS_SNMP_FALSE;
    OUTSEGMENT_LABEL (pOutSegment) = 0;
    OUTSEGMENT_NH_ADDR (pOutSegment) = u4IPAddr;
    OUTSEGMENT_NH_ADDRTYPE (pOutSegment) = (UINT1) i4NextHopAddrType;

    /* Get the traffic parameters for HLSP and service tunnel */
    if (TeCheckTrfcParamInTrfcParamTable
        (TE_TNL_TRFC_PARAM_INDEX (pTeHlspTnlInfo),
         &pTeHlspTrfcParams) != TE_SUCCESS)
    {
        TE_DBG (TE_MAIN_FAIL,
                "TeDeleteHLSPParams :unable to retieve traffic Params \n");
        return TE_FAILURE;
    }
    if (TeCheckTrfcParamInTrfcParamTable (TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo),
                                          &pTeTrfcParams) != TE_SUCCESS)
    {
        TE_DBG (TE_MAIN_FAIL,
                "TeDeleteHLSPParams :unable to retieve traffic Params \n");
        return TE_FAILURE;
    }

    /* Update HLSP Bandwidth and number of stacked tunnels */
    TE_HLSP_AVAILABLE_BW (pTeHlspTnlInfo) +=
        TE_TNLRSRC_MAX_RATE (pTeTrfcParams);
    TE_HLSP_NO_OF_STACKED_TNLS (pTeHlspTnlInfo)--;

    /* Delete the service tunnel pointer form StackList of HLSP */
    TMO_DLL_Delete (&TE_HLSP_STACK_LIST (pTeHlspTnlInfo),
                    &(pTeTnlInfo->NextServiceTnl));

    /* remove the Backpointer to HLSP Tunnel */
    TE_MAP_TNL_INFO (pTeTnlInfo) = NULL;

    TE_DBG (TE_MAIN_ETEXT, "TeDeleteHLSPParams : EXIT \n");
    return TE_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeDeleteSLSPParams
 * Description   : This routine updates the XC entries of S-LSP and e2e LSP 
 *               : when the stitching between them is removed.
 * Input(s)      : pteFsMplsLSPMapTunnelTable - Pointer to 
 *                     TeLSPMapTunnelTable
 * Output(s)     : None
 * Return(s)     : TE_SUCCESS/TE_FAILURE
 *****************************************************************************/
UINT4
TeDeleteSLSPParams (tteFsMplsLSPMapTunnelTable * pteFsMplsLSPMapTunnelTable)
{

    tTeTnlInfo         *pTeE2eLSPInfo = NULL;
    tTeTnlInfo         *pTeSlspTnlInfo = NULL;
    tXcEntry           *pFwdE2eLspXcEntry = NULL;
    tXcEntry           *pRevE2eLspXcEntry = NULL;
    tXcEntry           *pFwdSlspXcEntry = NULL;
    tXcEntry           *pRevSlspXcEntry = NULL;

    tOutSegment        *pE2eLspOutSegment = NULL;
    tInSegment         *pE2eLspInSegment = NULL;
    tInSegment         *pSlspInSegment = NULL;

    tTeTrfcParams      *pTeTrfcParams = NULL;

    TE_DBG (TE_MAIN_ETEXT, "TeDeleteSLSPParams : ENTRY \n");

    /* Get the S-LSP and e2e LSP tunnel Info */
    pTeE2eLSPInfo =
        TeGetTunnelInfo (TE_LSP_MAP_SUB_TNL_INDEX (pteFsMplsLSPMapTunnelTable),
                         TE_LSP_MAP_SUB_TNL_INST (pteFsMplsLSPMapTunnelTable),
                         TE_LSP_MAP_SUB_TNL_INGRESS
                         (pteFsMplsLSPMapTunnelTable),
                         TE_LSP_MAP_SUB_TNL_EGRESS
                         (pteFsMplsLSPMapTunnelTable));
    if (pTeE2eLSPInfo == NULL)
    {
        TE_DBG (TE_MAIN_FAIL,
                "TeDeleteSLSPParams : Failure to retrieve e2e LSP Tnl info \n");
        return TE_FAILURE;
    }
    pTeSlspTnlInfo =
        TeGetTunnelInfo (TE_LSP_MAP_TNL_INDEX (pteFsMplsLSPMapTunnelTable),
                         TE_LSP_MAP_TNL_INST (pteFsMplsLSPMapTunnelTable),
                         TE_LSP_MAP_TNL_INGRESS (pteFsMplsLSPMapTunnelTable),
                         TE_LSP_MAP_TNL_EGRESS (pteFsMplsLSPMapTunnelTable));
    if (pTeSlspTnlInfo == NULL)
    {
        TE_DBG (TE_MAIN_FAIL,
                "TeDeleteSLSPParams : Failure to retrieve S-LSP Tnl info \n");
        return TE_FAILURE;
    }

    /* 1. Clear the Fwd out-segment information of e2e LSP when the stitching 
     *    of e2e LSP with S-LSP is removed at ingress/intermediate of e2e LSP 
     *    and S-LSP being the ingress.
     * 2. Clear the Fwd in-segment of e2e LSP when the stitching of e2e LSP 
     *    with S-LSP is removed at intermediate node of e2e LSP and S-LSP 
     *    being the egress node.
     * 3. Clear the Fwd In-segment of e2e LSP when the stitching is removed
     *    at egress node of e2e LSP and S-LSP is also being the egress.
     */

    pFwdE2eLspXcEntry = MplsGetXCEntryByDirection
        (TE_TNL_XCINDEX (pTeE2eLSPInfo), MPLS_DIRECTION_FORWARD);
    if (pFwdE2eLspXcEntry == NULL)
    {
        TE_DBG (TE_MAIN_FAIL, "TeDeleteSLSPParams : Forward XC doesn't "
                "exists for e2e LSP.\r\n");
        return TE_FAILURE;
    }

    pE2eLspOutSegment = XC_OUTINDEX (pFwdE2eLspXcEntry);

    if (((TE_TNL_ROLE (pTeE2eLSPInfo) == TE_INGRESS) ||
         (TE_TNL_ROLE (pTeE2eLSPInfo) == TE_INTERMEDIATE)) &&
        (TE_TNL_ROLE (pTeSlspTnlInfo) == TE_INGRESS))
    {
        if (pE2eLspOutSegment != NULL)
        {
            /* Clear E2e LSP out-segment. */
            OUTSEGMENT_PUSH_FLAG (pE2eLspOutSegment) = (BOOL1) MPLS_SNMP_FALSE;
            OUTSEGMENT_LABEL (pE2eLspOutSegment) = MPLS_INVALID_LABEL;
            OUTSEGMENT_NH_ADDR (pE2eLspOutSegment) = 0;
            OUTSEGMENT_NH_ADDRTYPE (pE2eLspOutSegment) = LSR_ADDR_UNKNOWN;
        }
    }

    if (((TE_TNL_ROLE (pTeE2eLSPInfo) == TE_EGRESS) ||
         (TE_TNL_ROLE (pTeE2eLSPInfo) == TE_INTERMEDIATE)) &&
        (TE_TNL_ROLE (pTeSlspTnlInfo) == TE_EGRESS))
    {
        /* Clear E2E LSP in-segment. */
        pE2eLspInSegment = XC_ININDEX (pFwdE2eLspXcEntry);

        if (pE2eLspInSegment != NULL)
        {
            /* Delete the ILM entry of E2E LSP in the hardware. */
            if (TeDeleteILMInHw (pTeE2eLSPInfo, pE2eLspInSegment) == TE_FAILURE)
            {
                TE_DBG (TE_MAIN_FAIL,
                        "TeDeleteSLSPParams : ILM entry deletion failed "
                        "for e2e LSP.\n");
                return TE_FAILURE;
            }

            INSEGMENT_LABEL (pE2eLspInSegment) = MPLS_INVALID_LABEL;
            INSEGMENT_NPOP (pE2eLspInSegment) = 0;
            INSEGMENT_ADDR_FAMILY (pE2eLspInSegment) = LSR_ADDR_UNKNOWN;

            /* Add the ILM entry of S-LSP in the hardware. */
            pFwdSlspXcEntry =
                MplsGetXCEntryByDirection (TE_TNL_XCINDEX (pTeSlspTnlInfo),
                                           MPLS_DIRECTION_FORWARD);
            if ((pFwdSlspXcEntry != NULL) &&
                ((pSlspInSegment = XC_ININDEX (pFwdSlspXcEntry)) != NULL))
            {
                if (TeAddILMInHw (pTeSlspTnlInfo, pSlspInSegment) == TE_FAILURE)
                {
                    TE_DBG (TE_MAIN_FAIL,
                            "TeDeleteSLSPParams : ILM entry addition failed "
                            "for S-LSP.\n");
                    return TE_FAILURE;
                }
            }
        }
    }

    /* This is applicable only when the tunnels are co-routed bi-directional.
     * 
     * 1. Clear the Rev out-segment information of e2e LSP when the stitching 
     *    of e2e LSP with S-LSP at egress/intermediate of e2e LSP 
     *    and S-LSP being the egress.
     * 2. Clear the Rev in-segment of S-LSP when the stitching of e2e LSP with  
     *    S-LSP is removed at intermediate node of e2e LSP and S-LSP being the 
     *    ingress node.
     * 3. Clear the Rev In-segment of e2e LSP when the stitching is removed
     *    at ingress node of e2e LSP and S-LSP is also being the 
     *    ingress at that node.
     */

    if (pTeE2eLSPInfo->u4TnlMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
    {
        pRevE2eLspXcEntry = MplsGetXCEntryByDirection
            (TE_TNL_XCINDEX (pTeE2eLSPInfo), MPLS_DIRECTION_REVERSE);
        if (pRevE2eLspXcEntry == NULL)
        {
            TE_DBG (TE_MAIN_FAIL, "TeDeleteSLSPParams : Reverse XC doesn't "
                    "exists for e2e LSP.\r\n");
            return TE_FAILURE;
        }

        pE2eLspOutSegment = XC_OUTINDEX (pRevE2eLspXcEntry);

        if (((TE_TNL_ROLE (pTeE2eLSPInfo) == TE_EGRESS) ||
             (TE_TNL_ROLE (pTeE2eLSPInfo) == TE_INTERMEDIATE)) &&
            (TE_TNL_ROLE (pTeSlspTnlInfo) == TE_EGRESS))
        {
            if (pE2eLspOutSegment != NULL)
            {
                /* Clear E2e LSP out-segment. */
                OUTSEGMENT_PUSH_FLAG (pE2eLspOutSegment) =
                    (BOOL1) MPLS_SNMP_FALSE;
                OUTSEGMENT_LABEL (pE2eLspOutSegment) = MPLS_INVALID_LABEL;
                OUTSEGMENT_NH_ADDR (pE2eLspOutSegment) = 0;
                OUTSEGMENT_NH_ADDRTYPE (pE2eLspOutSegment) = LSR_ADDR_UNKNOWN;
            }
        }

        if (((TE_TNL_ROLE (pTeE2eLSPInfo) == TE_INGRESS) ||
             (TE_TNL_ROLE (pTeE2eLSPInfo) == TE_INTERMEDIATE)) &&
            (TE_TNL_ROLE (pTeSlspTnlInfo) == TE_INGRESS))
        {
            /* Copy the S-LSP in-segment to E2E LSP in-segment. */
            pE2eLspInSegment = XC_ININDEX (pRevE2eLspXcEntry);
            if (pE2eLspInSegment != NULL)
            {
                /* Delete the ILM entry of E2E LSP in the hardware. */
                if (TeDeleteILMInHw (pTeE2eLSPInfo,
                                     pE2eLspInSegment) == TE_FAILURE)
                {
                    TE_DBG (TE_MAIN_FAIL,
                            "TeDeleteSLSPParams : ILM entry deletion failed "
                            "for e2e LSP.\n");
                    return TE_FAILURE;
                }

                INSEGMENT_LABEL (pE2eLspInSegment) = MPLS_INVALID_LABEL;
                INSEGMENT_NPOP (pE2eLspInSegment) = 0;
                INSEGMENT_ADDR_FAMILY (pE2eLspInSegment) = LSR_ADDR_UNKNOWN;

                /* Add the ILM entry of S-LSP in the hardware. */
                pRevSlspXcEntry =
                    MplsGetXCEntryByDirection (TE_TNL_XCINDEX (pTeSlspTnlInfo),
                                               MPLS_DIRECTION_REVERSE);
                if ((pRevSlspXcEntry != NULL) &&
                    ((pSlspInSegment = XC_ININDEX (pRevSlspXcEntry)) != NULL))
                {
                    if (TeAddILMInHw (pTeSlspTnlInfo,
                                      pSlspInSegment) == TE_FAILURE)
                    {
                        TE_DBG (TE_MAIN_FAIL,
                                "TeDeleteSLSPParams : ILM entry addition failed "
                                "for S-LSP.\n");
                        return TE_FAILURE;
                    }
                }
            }
        }
    }

    if (TeCheckTrfcParamInTrfcParamTable
        (TE_TNL_TRFC_PARAM_INDEX (pTeE2eLSPInfo), &pTeTrfcParams) != TE_SUCCESS)
    {
        TE_DBG (TE_MAIN_FAIL, "TeDeleteSLSPParams: unable to retieve "
                "traffic Params for e2e LSP tunnel \n");
        return TE_FAILURE;
    }

    /* Update the available Bandwidth of S-LSP */
    TE_HLSP_AVAILABLE_BW (pTeSlspTnlInfo) +=
        TE_TNLRSRC_MAX_RATE (pTeTrfcParams);

    TE_MAP_TNL_INFO (pTeE2eLSPInfo) = NULL;
    TE_MAP_TNL_INFO (pTeSlspTnlInfo) = NULL;

    TE_DBG (TE_MAIN_ETEXT, "TeDeleteSLSPParams : EXIT \n");

    return TE_SUCCESS;
}

/****************************************************************************
 Function    :  TeGetAllFsMplsLSPMapTunnelTable
 Input       :  The Indices
                pteFsMplsLSPMapTunnelTable
 Output      :  This Routine Take the Indices &
                Gets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
TeGetAllFsMplsLSPMapTunnelTable (tteFsMplsLSPMapTunnelTable *
                                 pteGetFsMplsLSPMapTunnelTable)
{
    tteFsMplsLSPMapTunnelTable *pteFsMplsLSPMapTunnelTable = NULL;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeGetAllFsMplsLSPMapTunnelTable : ENTRY \n");

    /* Check whether the node is already present */
    pteFsMplsLSPMapTunnelTable =
        RBTreeGet (gTeLSPMapTnlGblInfo.pLSPMapTnlRBTree,
                   (tRBElem *) pteGetFsMplsLSPMapTunnelTable);

    if (pteFsMplsLSPMapTunnelTable == NULL)
    {
        return SNMP_FAILURE;
    }

    /* Assign values from the existing node */
    pteGetFsMplsLSPMapTunnelTable->u4FsMplsLSPMaptunnelOperation =
        pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMaptunnelOperation;

    pteGetFsMplsLSPMapTunnelTable->i4FsMplsLSPMaptunnelRowStatus =
        pteFsMplsLSPMapTunnelTable->i4FsMplsLSPMaptunnelRowStatus;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeGetAllFsMplsLSPMapTunnelTable : EXIT \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TeSetAllFsMplsLSPMapTunnelTable
 Input       :  The Indices
                pteFsMplsLSPMapTunnelTable
                pteIsSetFsMplsLSPMapTunnelTable
                i4RowStatusLogic
                i4RowCreateOption
 Output      :  This Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
TeSetAllFsMplsLSPMapTunnelTable (tteFsMplsLSPMapTunnelTable *
                                 pteSetFsMplsLSPMapTunnelTable,
                                 tteIsSetFsMplsLSPMapTunnelTable *
                                 pteIsSetFsMplsLSPMapTunnelTable,
                                 INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tteFsMplsLSPMapTunnelTable *pteFsMplsLSPMapTunnelTable = NULL;
    tteFsMplsLSPMapTunnelTable teOldFsMplsLSPMapTunnelTable;
    tteFsMplsLSPMapTunnelTable teTrgFsMplsLSPMapTunnelTable;
    tteIsSetFsMplsLSPMapTunnelTable teTrgIsSetFsMplsLSPMapTunnelTable;
    INT4                i4RowMakeActive = TE_FALSE;

    MEMSET (&teOldFsMplsLSPMapTunnelTable, 0,
            sizeof (tteFsMplsLSPMapTunnelTable));
    MEMSET (&teTrgFsMplsLSPMapTunnelTable, 0,
            sizeof (tteFsMplsLSPMapTunnelTable));
    MEMSET (&teTrgIsSetFsMplsLSPMapTunnelTable, 0,
            sizeof (tteIsSetFsMplsLSPMapTunnelTable));

    MPLS_CMN_LOCK ();
    /* Check whether the node is already present */
    pteFsMplsLSPMapTunnelTable =
        RBTreeGet (gTeLSPMapTnlGblInfo.pLSPMapTnlRBTree,
                   (tRBElem *) pteSetFsMplsLSPMapTunnelTable);

    if (pteFsMplsLSPMapTunnelTable == NULL)
    {
        /* Create the node if the RowStatus given is CREATE_AND_WAIT or CREATE_AND_GO */
        if ((pteSetFsMplsLSPMapTunnelTable->i4FsMplsLSPMaptunnelRowStatus ==
             CREATE_AND_WAIT)
            || (pteSetFsMplsLSPMapTunnelTable->i4FsMplsLSPMaptunnelRowStatus ==
                CREATE_AND_GO)
            ||
            ((pteSetFsMplsLSPMapTunnelTable->i4FsMplsLSPMaptunnelRowStatus ==
              ACTIVE) && (i4RowCreateOption == 1)))
        {
            /* Allocate memory for the new node */
            pteFsMplsLSPMapTunnelTable = (tteFsMplsLSPMapTunnelTable *)
                MemAllocMemBlk (TE_LSP_MAP_POOL_ID);

            if (pteFsMplsLSPMapTunnelTable == NULL)
            {
                TE_DBG (TE_MAIN_FAIL,
                        "TeSetAllFsMplsLSPMapTunnelTable :Failure to allocate memory \n");
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            MEMSET (pteFsMplsLSPMapTunnelTable, 0,
                    sizeof (tteFsMplsLSPMapTunnelTable));
            /* Assign values for the new node */
            if (pteIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapTunnelIndex !=
                OSIX_FALSE)
            {
                pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelIndex =
                    pteSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelIndex;
            }

            if (pteIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapTunnelInstance !=
                OSIX_FALSE)
            {
                pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelInstance =
                    pteSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelInstance;
            }

            if (pteIsSetFsMplsLSPMapTunnelTable->
                bFsMplsLSPMapTunnelIngressLSRId != OSIX_FALSE)
            {
                pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelIngressLSRId =
                    pteSetFsMplsLSPMapTunnelTable->
                    u4FsMplsLSPMapTunnelIngressLSRId;
            }

            if (pteIsSetFsMplsLSPMapTunnelTable->
                bFsMplsLSPMapTunnelEgressLSRId != OSIX_FALSE)
            {
                pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelEgressLSRId =
                    pteSetFsMplsLSPMapTunnelTable->
                    u4FsMplsLSPMapTunnelEgressLSRId;
            }

            if (pteIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMapSubTunnelIndex !=
                OSIX_FALSE)
            {
                pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelIndex =
                    pteSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelIndex;
            }

            if (pteIsSetFsMplsLSPMapTunnelTable->
                bFsMplsLSPMapSubTunnelInstance != OSIX_FALSE)
            {
                pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelInstance =
                    pteSetFsMplsLSPMapTunnelTable->
                    u4FsMplsLSPMapSubTunnelInstance;
            }

            if (pteIsSetFsMplsLSPMapTunnelTable->
                bFsMplsLSPMapSubTunnelIngressLSRId != OSIX_FALSE)
            {
                pteFsMplsLSPMapTunnelTable->
                    u4FsMplsLSPMapSubTunnelIngressLSRId =
                    pteSetFsMplsLSPMapTunnelTable->
                    u4FsMplsLSPMapSubTunnelIngressLSRId;
            }

            if (pteIsSetFsMplsLSPMapTunnelTable->
                bFsMplsLSPMapSubTunnelEgressLSRId != OSIX_FALSE)
            {
                pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelEgressLSRId =
                    pteSetFsMplsLSPMapTunnelTable->
                    u4FsMplsLSPMapSubTunnelEgressLSRId;
            }

            if (pteIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMaptunnelOperation !=
                OSIX_FALSE)
            {
                pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMaptunnelOperation =
                    pteSetFsMplsLSPMapTunnelTable->
                    u4FsMplsLSPMaptunnelOperation;
            }

            if (pteIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMaptunnelRowStatus !=
                OSIX_FALSE)
            {
                pteFsMplsLSPMapTunnelTable->i4FsMplsLSPMaptunnelRowStatus =
                    pteSetFsMplsLSPMapTunnelTable->
                    i4FsMplsLSPMaptunnelRowStatus;
            }

            if ((pteSetFsMplsLSPMapTunnelTable->i4FsMplsLSPMaptunnelRowStatus ==
                 CREATE_AND_GO) ||
                ((i4RowCreateOption == 1) &&
                 (pteSetFsMplsLSPMapTunnelTable->i4FsMplsLSPMaptunnelRowStatus
                  == ACTIVE)))
            {
                pteSetFsMplsLSPMapTunnelTable->i4FsMplsLSPMaptunnelRowStatus =
                    ACTIVE;
                pteFsMplsLSPMapTunnelTable->i4FsMplsLSPMaptunnelRowStatus =
                    ACTIVE;

                /* For MSR and RM Trigger */
                teTrgFsMplsLSPMapTunnelTable.i4FsMplsLSPMaptunnelRowStatus =
                    CREATE_AND_WAIT;
                teTrgFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelIndex =
                    pteSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelIndex;
                teTrgFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelInstance =
                    pteSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelInstance;
                teTrgFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelIngressLSRId =
                    pteSetFsMplsLSPMapTunnelTable->
                    u4FsMplsLSPMapTunnelIngressLSRId;
                teTrgFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelEgressLSRId =
                    pteSetFsMplsLSPMapTunnelTable->
                    u4FsMplsLSPMapTunnelEgressLSRId;

                teTrgFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelIndex =
                    pteSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelIndex;
                teTrgFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelInstance =
                    pteSetFsMplsLSPMapTunnelTable->
                    u4FsMplsLSPMapSubTunnelInstance;
                teTrgFsMplsLSPMapTunnelTable.
                    u4FsMplsLSPMapSubTunnelIngressLSRId =
                    pteSetFsMplsLSPMapTunnelTable->
                    u4FsMplsLSPMapSubTunnelIngressLSRId;
                teTrgFsMplsLSPMapTunnelTable.
                    u4FsMplsLSPMapSubTunnelEgressLSRId =
                    pteSetFsMplsLSPMapTunnelTable->
                    u4FsMplsLSPMapSubTunnelEgressLSRId;

                teTrgIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMaptunnelRowStatus =
                    OSIX_TRUE;

                if (TeSetAllFsMplsLSPMapTunnelTableTrigger
                    (&teTrgFsMplsLSPMapTunnelTable,
                     &teTrgIsSetFsMplsLSPMapTunnelTable,
                     SNMP_SUCCESS) != SNMP_SUCCESS)
                {
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
            }
            else if (pteSetFsMplsLSPMapTunnelTable->
                     i4FsMplsLSPMaptunnelRowStatus == CREATE_AND_WAIT)
            {
                pteFsMplsLSPMapTunnelTable->i4FsMplsLSPMaptunnelRowStatus =
                    NOT_READY;
            }

            /* Add the new node to the database */
            if (RBTreeAdd (gTeLSPMapTnlGblInfo.pLSPMapTnlRBTree,
                           pteFsMplsLSPMapTunnelTable) != RB_SUCCESS)
            {
                TE_DBG (TE_MAIN_FAIL,
                        "TeSetAllFsMplsLSPMapTunnelTable: Failure to add to RB Tree \n");
                MPLS_CMN_UNLOCK ();
                return TE_FAILURE;
            }
            if (TeUtilUpdateFsMplsLSPMapTunnelTable
                (NULL, pteFsMplsLSPMapTunnelTable) != OSIX_TRUE)
            {
                RBTreeRem (gTeLSPMapTnlGblInfo.pLSPMapTnlRBTree,
                           pteFsMplsLSPMapTunnelTable);
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            if (TeSetAllFsMplsLSPMapTunnelTableTrigger
                (pteSetFsMplsLSPMapTunnelTable, pteIsSetFsMplsLSPMapTunnelTable,
                 SNMP_SUCCESS) != SNMP_SUCCESS)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
        else
        {
            TE_DBG (TE_MAIN_FAIL,
                    "TeSetAllFsMplsLSPMapTunnelTable :pteFsMplsLSPMapTunnelTable does not exist to make the Row Status active \n");
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    else if ((pteSetFsMplsLSPMapTunnelTable->i4FsMplsLSPMaptunnelRowStatus ==
              CREATE_AND_WAIT)
             || (pteSetFsMplsLSPMapTunnelTable->i4FsMplsLSPMaptunnelRowStatus ==
                 CREATE_AND_GO))
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    /* Copy the previous values before setting the new values */
    MEMCPY (&teOldFsMplsLSPMapTunnelTable, pteFsMplsLSPMapTunnelTable,
            sizeof (tteFsMplsLSPMapTunnelTable));

    /*Delete the node from the database if the RowStatus given is DESTROY */
    if (pteSetFsMplsLSPMapTunnelTable->i4FsMplsLSPMaptunnelRowStatus == DESTROY)
    {
        pteFsMplsLSPMapTunnelTable->i4FsMplsLSPMaptunnelRowStatus = DESTROY;

        if (TeUtilUpdateFsMplsLSPMapTunnelTable (&teOldFsMplsLSPMapTunnelTable,
                                                 pteFsMplsLSPMapTunnelTable) !=
            OSIX_TRUE)
        {
            TE_DBG (TE_MAIN_FAIL,
                    "TeSetAllFsMplsLSPMapTunnelTable:: Failure to delete HLSP Params \n");
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }

        RBTreeRem (gTeLSPMapTnlGblInfo.pLSPMapTnlRBTree,
                   pteFsMplsLSPMapTunnelTable);
        MemReleaseMemBlock (TE_LSP_MAP_POOL_ID,
                            (UINT1 *) pteFsMplsLSPMapTunnelTable);

        if (TeSetAllFsMplsLSPMapTunnelTableTrigger
            (pteSetFsMplsLSPMapTunnelTable, pteIsSetFsMplsLSPMapTunnelTable,
             SNMP_SUCCESS) != SNMP_SUCCESS)
        {
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }

    /*Function to check whether the given input is same as there in database */
    if (FsMplsLSPMapTunnelTableFilterInputs
        (pteFsMplsLSPMapTunnelTable, pteSetFsMplsLSPMapTunnelTable,
         pteIsSetFsMplsLSPMapTunnelTable) == OSIX_FALSE)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }

    /*This condtion is to make the row NOT_IN_SERVICE before modifying the values */
    if ((i4RowStatusLogic == TE_TRUE) &&
        (pteFsMplsLSPMapTunnelTable->i4FsMplsLSPMaptunnelRowStatus == ACTIVE))
    {
        pteFsMplsLSPMapTunnelTable->i4FsMplsLSPMaptunnelRowStatus =
            NOT_IN_SERVICE;
        i4RowMakeActive = TE_TRUE;

        /* For MSR and RM Trigger */
        teTrgFsMplsLSPMapTunnelTable.i4FsMplsLSPMaptunnelRowStatus =
            NOT_IN_SERVICE;
        teTrgIsSetFsMplsLSPMapTunnelTable.bFsMplsLSPMaptunnelRowStatus =
            OSIX_TRUE;

        teTrgFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelIndex =
            pteSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelIndex;
        teTrgFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelInstance =
            pteSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelInstance;
        teTrgFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelIngressLSRId =
            pteSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelIngressLSRId;
        teTrgFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelEgressLSRId =
            pteSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelEgressLSRId;

        teTrgFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelIndex =
            pteSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelIndex;
        teTrgFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelInstance =
            pteSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelInstance;
        teTrgFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelIngressLSRId =
            pteSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelIngressLSRId;
        teTrgFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelEgressLSRId =
            pteSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelEgressLSRId;

        if (TeUtilUpdateFsMplsLSPMapTunnelTable (&teOldFsMplsLSPMapTunnelTable,
                                                 pteFsMplsLSPMapTunnelTable) !=
            OSIX_TRUE)
        {
            /*Restore back with previous values */
            MEMCPY (pteFsMplsLSPMapTunnelTable, &teOldFsMplsLSPMapTunnelTable,
                    sizeof (tteFsMplsLSPMapTunnelTable));
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }

        if (TeSetAllFsMplsLSPMapTunnelTableTrigger
            (&teTrgFsMplsLSPMapTunnelTable, &teTrgIsSetFsMplsLSPMapTunnelTable,
             SNMP_SUCCESS) != SNMP_SUCCESS)
        {
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    if (pteSetFsMplsLSPMapTunnelTable->i4FsMplsLSPMaptunnelRowStatus == ACTIVE)
    {
        i4RowMakeActive = TE_TRUE;
    }

    /* Assign values for the existing node */
    if (pteIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMaptunnelOperation !=
        OSIX_FALSE)
    {
        pteFsMplsLSPMapTunnelTable->u4FsMplsLSPMaptunnelOperation =
            pteSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMaptunnelOperation;
    }
    if (pteIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMaptunnelRowStatus !=
        OSIX_FALSE)
    {
        pteFsMplsLSPMapTunnelTable->i4FsMplsLSPMaptunnelRowStatus =
            pteSetFsMplsLSPMapTunnelTable->i4FsMplsLSPMaptunnelRowStatus;
    }

    /* This condtion is to make the row back to ACTIVE after modifying 
     * the values */
    if (i4RowMakeActive == TE_TRUE)
    {
        pteFsMplsLSPMapTunnelTable->i4FsMplsLSPMaptunnelRowStatus = ACTIVE;
    }

    if (TeUtilUpdateFsMplsLSPMapTunnelTable (&teOldFsMplsLSPMapTunnelTable,
                                             pteFsMplsLSPMapTunnelTable) !=
        OSIX_TRUE)
    {
        /*Restore back with previous values */
        MEMCPY (pteFsMplsLSPMapTunnelTable, &teOldFsMplsLSPMapTunnelTable,
                sizeof (tteFsMplsLSPMapTunnelTable));
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TeSetAllFsMplsLSPMapTunnelTableTrigger (pteSetFsMplsLSPMapTunnelTable,
                                                pteIsSetFsMplsLSPMapTunnelTable,
                                                SNMP_SUCCESS) != SNMP_SUCCESS)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeSetAllFsMplsLSPMapTunnelTable : EXIT \n");
    return TE_SUCCESS;
}

/****************************************************************************
 Function    :  TeTestAllFsMplsLSPMapTunnelTable
 Input       :  The Indices
                pu4ErrorCode
                pteFsMplsLSPMapTunnelTable
                pteIsSetFsMplsLSPMapTunnelTable
 Output      :  This Routine Take the Indices &
                Test the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/

INT4
TeTestAllFsMplsLSPMapTunnelTable (UINT4 *pu4ErrorCode,
                                  tteFsMplsLSPMapTunnelTable *
                                  pteSetFsMplsLSPMapTunnelTable,
                                  tteIsSetFsMplsLSPMapTunnelTable *
                                  pteIsSetFsMplsLSPMapTunnelTable,
                                  INT4 i4RowStatusLogic, INT4 i4RowCreateOption)
{
    tteFsMplsLSPMapTunnelTable *pteFsMplsLSPMapTunnelTableEntry = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlInfo         *pInstance0Tnl = NULL;
    tTeTnlInfo         *pTeHlspOrSlspTnlInfo = NULL;
    UINT4               u4RetVal = TE_FAILURE;
    tTeTrfcParams      *pTeHlspOrSlspTrfcParams = NULL;
    tTeTrfcParams      *pTeTrfcParams = NULL;

    UNUSED_PARAM (i4RowStatusLogic);

    TE_DBG (TE_MAIN_ETEXT,
            "MAIN : TeTestAllFsMplsLSPMapTunnelTable : ENTRY \n");

    /* Check if Row status and operation are in valid range */
    if (pteIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMaptunnelRowStatus
        == TE_TRUE)
    {
        TE_VALIDATE_RS (u4RetVal, pteSetFsMplsLSPMapTunnelTable);
        if (TE_SUCCESS != u4RetVal)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return TE_FAILURE;
        }
    }
    if (pteIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMaptunnelOperation ==
        TE_TRUE)
    {
        TE_VALIDATE_OPERATION (u4RetVal, pteSetFsMplsLSPMapTunnelTable);
        if (TE_SUCCESS != u4RetVal)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            return TE_FAILURE;
        }
    }

    MPLS_CMN_LOCK ();
    /* Check if the HLSP/S-LSP and service/e2e LSP tunnel exists */
    pTeTnlInfo = TeGetTunnelInfo
        (pteSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelIndex,
         pteSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelInstance,
         pteSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelIngressLSRId,
         pteSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMapSubTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        TE_DBG (TE_MAIN_FAIL,
                "TeTestAllFsMplsLSPMapTunnelTable: Tunnel does not exist \n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }

    pTeHlspOrSlspTnlInfo = TeGetTunnelInfo
        (pteSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelIndex,
         pteSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelInstance,
         pteSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelIngressLSRId,
         pteSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMapTunnelEgressLSRId);
    if (pTeHlspOrSlspTnlInfo == NULL)
    {
        TE_DBG (TE_MAIN_FAIL,
                "TeTestAllFsMplsLSPMapTunnelTable    : H-LSP/S-LSP Tunnel "
                "does not exist \n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }

    /* Get the H-LSP/S-LSP and service/e2e LSP tunnel traffic parameters */
    if (TeCheckTrfcParamInTrfcParamTable
        (TE_TNL_TRFC_PARAM_INDEX (pTeHlspOrSlspTnlInfo),
         &pTeHlspOrSlspTrfcParams) != TE_SUCCESS)
    {
        TE_DBG (TE_MAIN_FAIL,
                "TeTestAllFsMplsLSPMapTunnelTable :unable to retieve traffic "
                "Params for H-LSP/S-LSP\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }

    /* Check if Zero Instance Tunnel Exists */
    pInstance0Tnl =
        TeGetTunnelInfo (pteSetFsMplsLSPMapTunnelTable->
                         u4FsMplsLSPMapSubTunnelIndex, TE_ZERO,
                         pteSetFsMplsLSPMapTunnelTable->
                         u4FsMplsLSPMapSubTunnelIngressLSRId,
                         pteSetFsMplsLSPMapTunnelTable->
                         u4FsMplsLSPMapSubTunnelEgressLSRId);

    if (pInstance0Tnl != NULL)
    {
        if (TeCheckTrfcParamInTrfcParamTable
            (TE_TNL_TRFC_PARAM_INDEX (pInstance0Tnl),
             &pTeTrfcParams) != TE_SUCCESS)
        {
            TE_DBG (TE_MAIN_FAIL,
                    "TeTestAllFsMplsLSPMapTunnelTable :unable to retieve "
                    "traffic Params for service/e2e LSP tunnel \n");
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            MPLS_CMN_UNLOCK ();
            return TE_FAILURE;
        }
    }
    else if (TeCheckTrfcParamInTrfcParamTable
             (TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo),
              &pTeTrfcParams) != TE_SUCCESS)
    {
        TE_DBG (TE_MAIN_FAIL,
                "TeTestAllFsMplsLSPMapTunnelTable :unable to retieve "
                "traffic Params for service/e2e LSP tunnel \n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return TE_FAILURE;
    }

    pteFsMplsLSPMapTunnelTableEntry =
        RBTreeGet (gTeLSPMapTnlGblInfo.pLSPMapTnlRBTree,
                   (tRBElem *) pteSetFsMplsLSPMapTunnelTable);

    /* Test - RowStatus */
    if (pteIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMaptunnelRowStatus ==
        TE_TRUE)
    {
        /* Node does not exist already */
        if (pteFsMplsLSPMapTunnelTableEntry == NULL)
        {
            /* Row status of the new node can only be Create and Wait or
             * Create and Go Through SNMP. Row status of the new node can 
             * be ACTIVE only thorugh CLI */
            if ((TE_LSP_MAP_TNL_RS (pteSetFsMplsLSPMapTunnelTable) !=
                 CREATE_AND_WAIT)
                && (TE_LSP_MAP_TNL_RS (pteSetFsMplsLSPMapTunnelTable) !=
                    CREATE_AND_GO)
                &&
                (!((TE_LSP_MAP_TNL_RS (pteSetFsMplsLSPMapTunnelTable) == ACTIVE)
                   && (i4RowCreateOption == TE_TRUE))))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                MPLS_CMN_UNLOCK ();
                return TE_FAILURE;
            }
        }
        else
        {
            /* Existing node Row status can be set only as DESTROY / ACTIVE */
            if ((TE_LSP_MAP_TNL_RS (pteSetFsMplsLSPMapTunnelTable)) != DESTROY
                && (TE_LSP_MAP_TNL_RS (pteSetFsMplsLSPMapTunnelTable) !=
                    ACTIVE))
            {
                if ((TE_LSP_MAP_TNL_RS (pteSetFsMplsLSPMapTunnelTable) ==
                     CREATE_AND_WAIT)
                    || (TE_LSP_MAP_TNL_RS (pteSetFsMplsLSPMapTunnelTable) ==
                        CREATE_AND_GO))

                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                    MPLS_CMN_UNLOCK ();
                    return TE_FAILURE;
                }
                /* NOT_IN_SERVICE cannot be set to modify the row 
                 *  in FsMplsLSPMapTunnelTable. the row needs to be
                 *  deleted and re-created in that case */
                if (TE_LSP_MAP_TNL_RS (pteSetFsMplsLSPMapTunnelTable) ==
                    NOT_IN_SERVICE)
                {
                    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                    MPLS_CMN_UNLOCK ();
                    return TE_FAILURE;
                }
            }
            else
            {
                if (TE_LSP_MAP_TNL_RS (pteSetFsMplsLSPMapTunnelTable) == ACTIVE)
                {
                    /* Verify whether the mandatory objects are set when 
                     * the row is made active.*/
                    TE_VALIDATE_OPERATION (u4RetVal,
                                           pteFsMplsLSPMapTunnelTableEntry);
                    if (TE_SUCCESS != u4RetVal)
                    {
                        TE_VALIDATE_OPERATION (u4RetVal,
                                               pteSetFsMplsLSPMapTunnelTable);
                        if (TE_SUCCESS != u4RetVal)
                        {
                            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                            MPLS_CMN_UNLOCK ();
                            return TE_FAILURE;
                        }
                    }
                }
            }
        }
    }

    /* Test - Operation */
    if (pteIsSetFsMplsLSPMapTunnelTable->bFsMplsLSPMaptunnelOperation ==
        TE_TRUE)
    {
        /* Check whether all the required XC, in and out-segments are created
         * and associated for the tunnels.*/
        u4RetVal = TeValidateAssociatedTnlsXC (pTeHlspOrSlspTnlInfo,
                                               pTeTnlInfo, pu4ErrorCode);
        if (u4RetVal == TE_FAILURE)
        {
            TE_DBG (TE_MAIN_FAIL,
                    "TeTestAllFsMplsLSPMapTunnelTable: XC validation for the "
                    "tunnels failed.  \n");
            MPLS_CMN_UNLOCK ();
            return TE_FAILURE;
        }

        /* Check the HLSP Specific params if operation is stack */
        if ((pteSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMaptunnelOperation ==
             TE_OPER_STACK) && (i4RowCreateOption == OSIX_TRUE))
        {
            /* Tunnel type must be HLSP for service tunnel to be stacked and 
             * a H-LSP tunnel canot be stacked over another H-LSP tunnel.*/
            if ((!(pTeHlspOrSlspTnlInfo->u4TnlType & TE_TNL_TYPE_HLSP)) ||
                (pTeTnlInfo->u4TnlType & TE_TNL_TYPE_HLSP))
            {
                TE_DBG (TE_MAIN_FAIL,
                        "TeTestAllFsMplsLSPMapTunnelTable : Tunnel type "
                        "must be H-LSP and the Sub-tunnel type should not"
                        "be of type H-LSP.\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return TE_FAILURE;
            }

            /* HLSP tunnel role must be ingress to be stacked or 
             * egress in case of co-routed bidirectional tunnels */
            if ((TE_TNL_ROLE (pTeHlspOrSlspTnlInfo) != TE_INGRESS)
                && ((TE_TNL_ROLE (pTeHlspOrSlspTnlInfo) == TE_EGRESS)
                    && (pTeHlspOrSlspTnlInfo->u4TnlMode !=
                        TE_TNL_MODE_COROUTED_BIDIRECTIONAL)))
            {
                TE_DBG (TE_MAIN_FAIL,
                        "TeTestAllFsMplsLSPMapTunnelTable : HLSP tunnel role "
                        "must be ingress or egress(co-routed bidriectional) "
                        "for service tunnel to be stacked\r\n");
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                MPLS_CMN_UNLOCK ();
                return TE_FAILURE;
            }

            /* If the available BW of HLSP is less than the BW reqd by 
             * the stacked tunnel, return failure */
            if (TE_HLSP_INFO(pTeHlspOrSlspTnlInfo) != NULL)
            {
                if (TE_HLSP_AVAILABLE_BW (pTeHlspOrSlspTnlInfo) <
                     TE_TNLRSRC_MAX_RATE (pTeTrfcParams))
                {
                    TE_DBG (TE_MAIN_FAIL,
                            "TeTestAllFsMplsLSPMapTunnelTable :HLSP does not "
                            "have sufficent BW to stack the tunnel  \n");
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    MPLS_CMN_UNLOCK ();
                    return TE_FAILURE;
                }
                if (TE_HLSP_NO_OF_STACKED_TNLS (pTeHlspOrSlspTnlInfo) >=
                    MAX_TE_DEF_STACKED_TNLS)
                {
                    TE_DBG (TE_MAIN_FAIL,
                            "TeTestAllFsMplsLSPMapTunnelTable: Cannot Stack, "
                            "exceeds the max number of stacked tunnels \n");
                    *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                    MPLS_CMN_UNLOCK ();
                    return TE_FAILURE;
                }
            }
        }

        if ((pteSetFsMplsLSPMapTunnelTable->u4FsMplsLSPMaptunnelOperation ==
             TE_OPER_STITCH) && (i4RowCreateOption == OSIX_TRUE))
        {
            /* Tunnel type must be S-LSP for e2e LSP tunnel to be stitched 
             * and a S-LSP should not be stitched to another S-LSP tunnel.*/
            if ((!(pTeHlspOrSlspTnlInfo->u4TnlType & TE_TNL_TYPE_SLSP)) ||
                (pTeTnlInfo->u4TnlType & TE_TNL_TYPE_SLSP))
            {
                TE_DBG (TE_MAIN_FAIL,
                        "TeTestAllFsMplsLSPMapTunnelTable : Tunnel type "
                        "must be S-LSP and Sub-tunnel should not be of "
                        "type S-LSP\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return TE_FAILURE;
            }

            /* A LSP can be stitched to atmost one LSP only. */
            if ((TE_MAP_TNL_INFO (pTeHlspOrSlspTnlInfo) != NULL) ||
                (TE_MAP_TNL_INFO (pTeTnlInfo) != NULL))
            {
                TE_DBG (TE_MAIN_FAIL,
                        "TeTestAllFsMplsLSPMapTunnelTable : S-LSP/e2e LSP"
                        "is already stitched to an LSP\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return TE_FAILURE;
            }

            /* No FTN should be associated with the S-LSP tunnel when it is 
             * stitched to an LSP.*/
            if (TMO_DLL_Count (&(pTeHlspOrSlspTnlInfo->FtnList)) > 0)
            {
                TE_DBG (TE_MAIN_FAIL, "teTestAllFsMplsLSPMapTunnelTable: "
                        "FTN associated with SLSP, remove the FTN and stitch"
                        " with the tunnel \n");
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                MPLS_CMN_UNLOCK ();
                return TE_FAILURE;
            }

            /* e2e LSP can be stitched to S-LSP tunnel only at ingress or  
             * egress node of S-LSP. */
            if (TE_TNL_ROLE (pTeHlspOrSlspTnlInfo) == TE_INTERMEDIATE)
            {
                TE_DBG (TE_MAIN_FAIL,
                        "TeTestAllFsMplsLSPMapTunnelTable : S-LSP tunnel role "
                        "must be ingress or egress for e2e LSP tunnel to be "
                        "stitched\r\n");
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                MPLS_CMN_UNLOCK ();
                return TE_FAILURE;
            }

            /* An LSP cannot be stitched to S-LSP when it is associated to a PW. */
            if (pTeHlspOrSlspTnlInfo->u1TnlInUseByVPN & TE_TNL_INUSE_BY_L2VPN)
            {
                TE_DBG (TE_MAIN_FAIL,
                        "TeTestAllFsMplsLSPMapTunnelTable : PW is associated to "
                        "S-LSP tunnel \r\n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return TE_FAILURE;
            }

            if (TE_TNLRSRC_MAX_RATE (pTeHlspOrSlspTrfcParams) <
                TE_TNLRSRC_MAX_RATE (pTeTrfcParams))
            {
                TE_DBG (TE_MAIN_FAIL, "teTestAllFsMplsLSPMapTunnelTable :"
                        "S-LSP does not have sufficent BW to stitch the "
                        "e2e LSP tunnel  \n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return TE_FAILURE;
            }
            /* Tunnel cannot be stacked and stitched together */
            if (TE_MAP_TNL_INFO (pTeTnlInfo) != NULL)
            {
                TE_DBG (TE_MAIN_FAIL,
                        "TeTestAllFsMplsLSPMapTunnelTable: Operation cannot be "
                        "changed \n");
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                MPLS_CMN_UNLOCK ();
                return TE_FAILURE;
            }

        }
    }

    MPLS_CMN_UNLOCK ();

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeTestAllFsMplsLSPMapTunnelTable : EXIT \n");
    return TE_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeValidateAssociatedTnlsXC
 * Description   : This routine validates the XC of H-LSP/S-LSP and 
 *                 service/e2e LSP tunnel before stacking/stitching. 
 *                 
 * Input(s)      : pTeHlspOrSlspTnlInfo - pointer to H-LSP/S-LSP tunnel
 *               : pTeTnlInfo   - pointer to service/e2e LSP tunnel
 * Output(s)     : pu4ErrorCode - pointer to the error code in which the 
 *               :                appropriate SNMP error codes will be updated 
 *               :                on validation failure.
 * Return(s)     : TE_SUCCESS/TE_FAILURE
 *****************************************************************************/

INT4
TeValidateAssociatedTnlsXC (tTeTnlInfo * pTeHlspOrSlspTnlInfo,
                            tTeTnlInfo * pTeTnlInfo, UINT4 *pu4ErrorCode)
{
    tXcEntry           *pFwdXcEntry = NULL;
    tXcEntry           *pFwdHlspOrSlspXcEntry = NULL;
    tXcEntry           *pRevXcEntry = NULL;
    tXcEntry           *pRevHlspOrSlspXcEntry = NULL;
    tOutSegment        *pOutSegment = NULL;
    tInSegment         *pInSegment = NULL;
    BOOL1               bFwdXCExists = FALSE;
    BOOL1               bRevXCExists = FALSE;

    TE_DBG (TE_MAIN_ETEXT, "TeValidateAssociatedTnlsXC : ENTRY \n");

    /* Check whether all the required XC, in and out-segments are created
     * and associated for the tunnels.*/
    pFwdHlspOrSlspXcEntry = MplsGetXCEntryByDirection
        (TE_TNL_XCINDEX (pTeHlspOrSlspTnlInfo), MPLS_DIRECTION_FORWARD);
    if (pFwdHlspOrSlspXcEntry != NULL)
    {
        pFwdXcEntry = MplsGetXCEntryByDirection
            (TE_TNL_XCINDEX (pTeTnlInfo), MPLS_DIRECTION_FORWARD);

        if (pFwdXcEntry != NULL)
        {
            bFwdXCExists = TRUE;
        }
    }
    else
    {
        TE_DBG (TE_MAIN_FAIL,
                "TeValidateAssociatedTnlsXC : H-LSP/S-LSP Tnl "
                "XC entry doesn't exists\t\n");
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return TE_FAILURE;
    }

    if (pTeHlspOrSlspTnlInfo->u4TnlMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
    {
        /* Check whether the reverse XC exists for H-LSP/S-LSP 
         * co-routed bidirectional tunnel. If reverse XC also doesn't 
         * exists, return failure.*/
        pRevHlspOrSlspXcEntry = MplsGetXCEntryByDirection
            (TE_TNL_XCINDEX (pTeHlspOrSlspTnlInfo), MPLS_DIRECTION_REVERSE);
        if (pRevHlspOrSlspXcEntry == NULL)
        {
            TE_DBG (TE_MAIN_FAIL,
                    "TeValidateAssociatedTnlsXC : H-LSP/S-LSP Tnl "
                    "Reverse XC entry doesn't exists\t\n");
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            return TE_FAILURE;
        }

        /* Check whether the reverse XC exists for service/e2e LSP 
         * (co-routed bidirectional tunnel. Stacking or stiching is not 
         * possible with only the XC information of H-LSP or S-LSP.*/
        pRevXcEntry = MplsGetXCEntryByDirection
            (TE_TNL_XCINDEX (pTeTnlInfo), MPLS_DIRECTION_REVERSE);
        if (pRevXcEntry != NULL)
        {
            bRevXCExists = TRUE;
        }
    }

    if (bFwdXCExists == TRUE)
    {
        if (((TE_TNL_ROLE (pTeTnlInfo) == TE_INGRESS) ||
             (TE_TNL_ROLE (pTeTnlInfo) == TE_INTERMEDIATE)) &&
            (TE_TNL_ROLE (pTeHlspOrSlspTnlInfo) == TE_INGRESS))
        {
            pOutSegment = XC_OUTINDEX (pFwdHlspOrSlspXcEntry);
            if (pOutSegment == NULL)
            {
                TE_DBG (TE_MAIN_FAIL,
                        "TeValidateAssociatedTnlsXC : The H-LSP/S-LSP  "
                        "tunnel Fwd out segment needs to be created  \n");
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return TE_FAILURE;
            }

            pOutSegment = XC_OUTINDEX (pFwdXcEntry);
            if (pOutSegment == NULL)
            {
                TE_DBG (TE_MAIN_FAIL,
                        "TeValidateAssociatedTnlsXC : The service/e2e LSP "
                        "tunnel Fwd out segment needs to be created  \n");
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return TE_FAILURE;
            }
        }

        if (((TE_TNL_ROLE (pTeTnlInfo) == TE_EGRESS) ||
             (TE_TNL_ROLE (pTeTnlInfo) == TE_INTERMEDIATE)) &&
            (TE_TNL_ROLE (pTeHlspOrSlspTnlInfo) == TE_EGRESS))
        {
            pInSegment = XC_ININDEX (pFwdHlspOrSlspXcEntry);
            if (pInSegment == NULL)
            {
                TE_DBG (TE_MAIN_FAIL,
                        "TeValidateAssociatedTnlsXC : The H-LSP/S-LSP  "
                        "tunnel Fwd in segment needs to be created  \n");
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return TE_FAILURE;
            }

            pInSegment = XC_ININDEX (pFwdXcEntry);
            if (pInSegment == NULL)
            {
                TE_DBG (TE_MAIN_FAIL,
                        "TeValidateAssociatedTnlsXC : The service/e2e "
                        "LSP tunnel Fwd in segment needs to be created\n");
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return TE_FAILURE;
            }
        }
    }

    if (bRevXCExists == TRUE)
    {
        if (((TE_TNL_ROLE (pTeTnlInfo) == TE_EGRESS) ||
             (TE_TNL_ROLE (pTeTnlInfo) == TE_INTERMEDIATE)) &&
            (TE_TNL_ROLE (pTeHlspOrSlspTnlInfo) == TE_EGRESS))
        {
            pOutSegment = XC_OUTINDEX (pRevHlspOrSlspXcEntry);
            if (pOutSegment == NULL)
            {
                TE_DBG (TE_MAIN_FAIL,
                        "TeValidateAssociatedTnlsXC : The H-LSP/S-LSP  "
                        "tunnel Rev out segment needs to be created  \n");
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return TE_FAILURE;
            }

            pOutSegment = XC_OUTINDEX (pRevXcEntry);
            if (pOutSegment == NULL)
            {
                TE_DBG (TE_MAIN_FAIL,
                        "TeValidateAssociatedTnlsXC : The service/e2e LSP "
                        "tunnel Rev out segment needs to be created  \n");
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return TE_FAILURE;
            }
        }

        if (((TE_TNL_ROLE (pTeTnlInfo) == TE_INGRESS) ||
             (TE_TNL_ROLE (pTeTnlInfo) == TE_INTERMEDIATE)) &&
            (TE_TNL_ROLE (pTeHlspOrSlspTnlInfo) == TE_INGRESS))

        {
            pInSegment = XC_ININDEX (pRevHlspOrSlspXcEntry);
            if (pInSegment == NULL)
            {
                TE_DBG (TE_MAIN_FAIL,
                        "TeValidateAssociatedTnlsXC : The H-LSP/S-LSP  "
                        "tunnel Rev in segment needs to be created  \n");
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return TE_FAILURE;
            }

            pInSegment = XC_ININDEX (pRevXcEntry);
            if (pInSegment == NULL)
            {
                TE_DBG (TE_MAIN_FAIL,
                        "TeValidateAssociatedTnlsXC : The service/e2e "
                        "LSP tunnel Rev in segment needs to be created \n");
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                return TE_FAILURE;
            }
        }
    }

    TE_DBG (TE_MAIN_ETEXT, "TeValidateAssociatedTnlsXC : EXIT \n");
    return (UINT4)TE_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeHLSPModifyILM
 * Description   : This routine updates the ILM in Hw with action as Pop and search
 *                 if tunnel role is egress
 * Input(s)      : pTeHlspTnlInfo - Pointer to 
 *                     Tunnel table
 * Output(s)     : None
 * Return(s)     : TE_SUCCESS/TE_FAILURE
 *****************************************************************************/

UINT4
TeHLSPModifyILM (tTeTnlInfo * pTeHlspTnlInfo)
{
    tXcEntry           *pHlspXcEntry = NULL;
    tInSegment         *pInSegment = NULL;
    UINT4               u4Direction = TE_ZERO;

    TE_DBG (TE_MAIN_ETEXT, "TeHLSPModifyILM : ENTRY \n");

    /* Get the insegment from XC entry */
    TE_GET_DIRECTION_FOR_HWACTION (pTeHlspTnlInfo, u4Direction);
    pHlspXcEntry =
        MplsGetXCEntryByDirection (TE_TNL_XCINDEX (pTeHlspTnlInfo),
                                   (eDirection) u4Direction);
    if (pHlspXcEntry == NULL)
    {
        TE_DBG (TE_MAIN_FAIL,
                "TeHLSPModifyILM : Failure in getting XC Entry \n");
        return TE_FAILURE;
    }

    pInSegment = pHlspXcEntry->pInIndex;
    if (pInSegment == NULL)
    {
        TE_DBG (TE_MAIN_FAIL, "TeHLSPModifyILM : Failure in modifying ILM  \n");
        return TE_FAILURE;
    }

    /* Delete the existing ILM and create a New ILM with action Pop and
       search */
    if (MplsILMDel (pInSegment) == TE_FAILURE)
    {
        TE_DBG (TE_MAIN_FAIL, "TeHLSPModifyILM : MplsILMDel Failed \t\n");
        return MPLS_FAILURE;
    }
    if (MplsILMAdd (pInSegment, pTeHlspTnlInfo) == TE_FAILURE)
    {
        TE_DBG (TE_MAIN_FAIL, "TeHLSPModifyILM : MplsILMAdd Failed \t\n");
        return MPLS_FAILURE;
    }

    TE_DBG (TE_MAIN_ETEXT, "TeHLSPModifyILM : EXIT \n");
    return TE_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeUpdateServiceTnlOperStatus
 * Description   : This routine updates the oper status of Service tunnels
 *                 stacked over H-LSP when H-LSP Oper status changes
 * Input(s)      : pTeHlspTnlInfo - Pointer to
 *                                  Tunnel table
 *                 u1TnlOperStatus - Oper Status of H-LSP
 * Output(s)     : None
 * Return(s)     : TE_SUCCESS/TE_FAILURE
 ******************************************************************************/

INT4
TeUpdateServiceTnlOperStatus (tTeTnlInfo * pTeHlspTnlInfo,
                              UINT1 u1TnlOperStatus)
{
    tTeTnlInfo         *pTeServiceTnlInfo = NULL;
    tTMO_DLL_NODE      *pNextServiceTnl = NULL;

    TE_DBG (TE_MAIN_ETEXT, "TeUpdateServiceTnlOperStatus : ENTRY \n");

    TMO_DLL_Scan (&TE_HLSP_STACK_LIST (pTeHlspTnlInfo), pNextServiceTnl,
                  tTMO_DLL_NODE *)
    {
        pTeServiceTnlInfo =
            (tTeTnlInfo *) (((FS_ULONG) pNextServiceTnl -
                             (TE_OFFSET (tTeTnlInfo, NextServiceTnl))));
        if (pTeServiceTnlInfo != NULL)
        {
            if (TeCmnExtSetOperStatusAndProgHw (pTeServiceTnlInfo, u1TnlOperStatus)
                == TE_FAILURE)
            {
                continue;
            }
        }
    }

    TE_DBG (TE_MAIN_ETEXT, "TeUpdateServiceTnlOperStatus : EXIT \n");
    return TE_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeFillFsMplsLspMapTnlTable
 * Description   : This routine Fills the FsMplsLSPMapTunnelTable inorder to
 *                 call nmhTestv2FsMplsLSPMapTunnelTable and 
 *                 nmhSetFsMplsLSPMapTunnelTable
 * Input(s)      : HlspTnlCliArgs - Pointer to HlspTnlCliArgs
 * Output(s)     : None
 * Return(s)     : NONE
 ******************************************************************************/

VOID
TeFillFsMplsLspMapTnlTable (tHlspTnlCliArgs * pHlspTnlCliArgs)
{
    TE_FILL_FSMPLSLSPMAPTUNNELTABLE_ARGS ((&
                                           (pHlspTnlCliArgs->
                                            teSetFsMplsLSPMapTunnelTable)),
                                          (&
                                           (pHlspTnlCliArgs->
                                            teIsSetFsMplsLSPMapTunnelTable)),
                                          &(pHlspTnlCliArgs->u4SetHlspTnlIndex),
                                          &(pHlspTnlCliArgs->
                                            u4SetHlspTnlInstance),
                                          &(pHlspTnlCliArgs->
                                            u4SetHlspTnlIngressId),
                                          &(pHlspTnlCliArgs->
                                            u4SetHlspTnlEgressId),
                                          &(pHlspTnlCliArgs->
                                            u4IsSetHlspTnlIndex),
                                          &(pHlspTnlCliArgs->
                                            u4IsSetHlspTnlInstance),
                                          &(pHlspTnlCliArgs->
                                            u4IsSetHlspTnlIngressId),
                                          &(pHlspTnlCliArgs->
                                            u4IsSetHlspTnlEgressId),
                                          &(pHlspTnlCliArgs->u4Operation),
                                          &(pHlspTnlCliArgs->u4RowStatus));
}
