#include "teincs.h"

/* $Id: teset.c,v 1.20 2014/11/17 12:00:09 siva Exp $ */

/****************************************************************************
 Function    :  TeSetAllGmplsTunnelTable
 Description :  This function sets GMPLS Tunnel MIB objects.
 Input       :  u4TnlIndex       - Tunnel Index
                u4TnlInstance    - Tunnel Instance
                u4IngressId      - Tunnel Ingress Id
                u4EgressId       - Tunnel Egress Id
                pSetGmplsTnlInfo - Pointer to GMPLS Tunnel Info
                u4ObjectId       - Object ID
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TeSetAllGmplsTunnelTable (UINT4 u4TnlIndex, UINT4 u4TnlInstance,
                          UINT4 u4IngressId, UINT4 u4EgressId,
                          tGmplsTnlInfo * pSetGmplsTnlInfo, UINT4 u4ObjectId)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT1               u1IsRpteAdminDown = TE_FALSE;

    pTeTnlInfo = TeGetTunnelInfo (u4TnlIndex, u4TnlInstance,
                                  u4IngressId, u4EgressId);
    if (pTeTnlInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case TE_GMPLS_TUNNEL_UNNUM_IF:
        {
            pTeTnlInfo->GmplsTnlInfo.bUnnumIf = pSetGmplsTnlInfo->bUnnumIf;
            break;
        }

        case TE_GMPLS_TUNNEL_ATTRIBUTES:
        {
            pTeTnlInfo->GmplsTnlInfo.u1Attributes
                = pSetGmplsTnlInfo->u1Attributes;
            pTeTnlInfo->GmplsTnlInfo.u1AttrLen = pSetGmplsTnlInfo->u1AttrLen;
            break;
        }

        case TE_GMPLS_TUNNEL_ENCODING_TYPE:
        {
            pTeTnlInfo->GmplsTnlInfo.u1EncodingType
                = pSetGmplsTnlInfo->u1EncodingType;
            break;
        }

        case TE_GMPLS_TUNNEL_SWITCHING_TYPE:
        {
            pTeTnlInfo->GmplsTnlInfo.u1SwitchingType
                = pSetGmplsTnlInfo->u1SwitchingType;
            break;
        }

        case TE_GMPLS_TUNNEL_LINK_PROTECTION_TYPE:
        {
            pTeTnlInfo->GmplsTnlInfo.u1LinkProtection
                = pSetGmplsTnlInfo->u1LinkProtection;
            break;
        }

        case TE_GMPLS_TUNNEL_GPID:
        {
            pTeTnlInfo->GmplsTnlInfo.u2Gpid = pSetGmplsTnlInfo->u2Gpid;
            break;
        }

        case TE_GMPLS_TUNNEL_SECONDARY:
        {
            pTeTnlInfo->GmplsTnlInfo.u1Secondary
                = pSetGmplsTnlInfo->u1Secondary;
            break;
        }

        case TE_GMPLS_TUNNEL_DIRECTION:
        {
            pTeTnlInfo->GmplsTnlInfo.u1Direction =
                pSetGmplsTnlInfo->u1Direction;
            pTeTnlInfo->u4TnlMode = (UINT4) pSetGmplsTnlInfo->u1Direction;
            break;
        }

        case TE_GMPLS_TUNNEL_PATH_COMP:
        {
            pTeTnlInfo->GmplsTnlInfo.PathComp = pSetGmplsTnlInfo->PathComp;
            break;
        }

        case TE_GMPLS_TUNNEL_ADMIN_STATUS_FLAGS:
        {
            /* After setting the admin status flag, an indication should be given
             * to RSVPTE module if the row status is active. */

            if (pTeTnlInfo->GmplsTnlInfo.u4AdminStatus !=
                pSetGmplsTnlInfo->u4AdminStatus)
            {
                pTeTnlInfo->GmplsTnlInfo.u4AdminStatus =
                    pSetGmplsTnlInfo->u4AdminStatus;

                pTeTnlInfo->GmplsTnlInfo.u1AdminStatusLen =
                    pSetGmplsTnlInfo->u1AdminStatusLen;

                if (TE_TNL_ADMIN_STATUS (pTeTnlInfo) == TE_ADMIN_UP)
                {
                    if (TeRpteTEEventHandler (TE_TNL_TX_PATH_MSG,
                                              pTeTnlInfo, &u1IsRpteAdminDown)
                        == TE_RPTE_FAILURE)
                    {
                        TE_DBG (TE_MAIN_SNMP, "Posting to RSVP-TE failed\n");
                    }
                }
            }

            break;
        }
        case TE_GMPLS_SEND_PATH_NOTIFY_RECEPIENT_TYPE:
        {
            pTeTnlInfo->GmplsTnlInfo.SendPathNotifyRecipientType =
                pSetGmplsTnlInfo->SendPathNotifyRecipientType;
            break;
        }
        case TE_GMPLS_SEND_PATH_NOTIFY_RECEPIENT:
        {
            pTeTnlInfo->GmplsTnlInfo.u4SendPathNotifyRecipient =
                pSetGmplsTnlInfo->u4SendPathNotifyRecipient;
            break;
        }
        case TE_GMPLS_SEND_RESV_NOTIFY_RECEPIENT_TYPE:
        {
            pTeTnlInfo->GmplsTnlInfo.SendResvNotifyRecipientType =
                pSetGmplsTnlInfo->SendResvNotifyRecipientType;
            break;
        }
        case TE_GMPLS_SEND_RESV_NOTIFY_RECEPIENT:
        {
            pTeTnlInfo->GmplsTnlInfo.u4SendResvNotifyRecipient =
                pSetGmplsTnlInfo->u4SendResvNotifyRecipient;
            break;
        }
        default:
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
  Function     : TeSetAllGmplsTunnelHopTable
  Description  : This function sets the GMPLS Tunnel Hop Table Objects. 
  Input        : u4HopListIndex      - Hop List Index
                 u4PathOptionIndex   - Path Option Index
                 u4HopIndex          - Hop Index
                 pSetGmplsTnlHopInfo - Pointer to GMPLS Tunnel Hop Info
                 u4ObjectId          - Object Id
  Output      :  None
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TeSetAllGmplsTunnelHopTable (UINT4 u4HopListIndex, UINT4 u4PathOptionIndex,
                             UINT4 u4HopIndex,
                             tGmplsTnlHopInfo * pSetGmplsTnlHopInfo,
                             UINT4 u4ObjectId)
{
    tTeHopInfo         *pTeHopInfo = NULL;

    if (TeCheckHopInfo (u4HopListIndex, u4PathOptionIndex, u4HopIndex,
                        &pTeHopInfo) == TE_FAILURE)
    {
        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case TE_GMPLS_TNL_HOP_FORWARD_LBL:
        {
            pTeHopInfo->GmplsTnlHopInfo.u4ForwardLbl
                = pSetGmplsTnlHopInfo->u4ForwardLbl;
            pTeHopInfo->GmplsTnlHopInfo.u1LblStatus
                |= TE_ERHOP_FORWARD_LBL_PRESENT;

            break;
        }
        case TE_GMPLS_TNL_HOP_REVERSE_LBL:
        {
            pTeHopInfo->GmplsTnlHopInfo.u4ReverseLbl
                = pSetGmplsTnlHopInfo->u4ReverseLbl;
            pTeHopInfo->GmplsTnlHopInfo.u1LblStatus
                |= TE_ERHOP_REVERSE_LBL_PRESENT;

            break;
        }

        default:
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TeSetAllFsMplsTunnelTable
 Description :  This function sets FS MPLS Tunnel Table Objects
 Input       :  u4TnlInstance     - Tunnel Index
                u4TnlInstance     - Tunnel Instance
                u4IngressId       - Ingress LSR Id
                u4EgressId        - Egress LSR Id
                pSetTeTnlInfo     - Pointer to MPLS Tunnel Info
                u4ObjectId        - Object ID
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TeSetAllFsMplsTunnelTable (UINT4 u4TnlIndex, UINT4 u4TnlInstance,
                           UINT4 u4IngressId, UINT4 u4EgressId,
                           tTeTnlInfo * pSetTeTnlInfo, UINT4 u4ObjectId)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlInfo         *pInstance0Tnl = NULL;
    tTeAttrListInfo    *pTeAttrListInfo = NULL;
    UINT1               u1IfType = TE_ZERO;
    UINT1               u1IsRpteAdminDown = TE_FALSE;

    pTeTnlInfo = TeGetTunnelInfo (u4TnlIndex, u4TnlInstance,
                                  u4IngressId, u4EgressId);
    if (pTeTnlInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pInstance0Tnl =
        TeGetTunnelInfo (u4TnlIndex, TE_ZERO, u4IngressId, u4EgressId);

    switch (u4ObjectId)
    {
        case TE_FS_MPLS_END_TO_END_PROTECTION:
        {
            if (pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType !=
                pSetTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType)
            {
                pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType =
                    pSetTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType;

                if (TE_TNL_ADMIN_STATUS (pTeTnlInfo) == TE_ADMIN_UP)
                {
                    if (TeRpteTEEventHandler (TE_TNL_TX_PATH_MSG,
                                              pTeTnlInfo, &u1IsRpteAdminDown)
                        == TE_RPTE_FAILURE)
                    {
                        TE_DBG (TE_MAIN_SNMP, "Posting to RSVP-TE failed\n");
                    }
                }
            }

            if (pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType !=
                MPLS_TE_UNPROTECTED)
            {
                pTeTnlInfo->GmplsTnlInfo.
                    u4SendPathNotifyRecipient = u4IngressId;
                pTeTnlInfo->GmplsTnlInfo.u4SendResvNotifyRecipient = u4EgressId;
            }
            if (pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
                MPLS_TE_FULL_REROUTE)
            {
                pTeTnlInfo->bIsSEStyleDesired = TRUE;
            }
            break;
        }

        case TE_FS_MPLS_PR_CONFIG_OPER_TYPE:
        {
            pTeTnlInfo->u1TnlProtOperType = pSetTeTnlInfo->u1TnlProtOperType;
            break;
        }

        case TE_FS_MPLS_SRLG_TYPE:
        {
            pTeTnlInfo->u1TnlSrlgType = pSetTeTnlInfo->u1TnlSrlgType;
            break;
        }
        case TE_FS_MPLS_MBB_STATUS:
        {
            pTeTnlInfo->u1TnlMBBStatus = pSetTeTnlInfo->u1TnlMBBStatus;

            if (pSetTeTnlInfo->u1TnlMBBStatus == MPLS_TE_MBB_ENABLED)
            {
                pTeTnlInfo->bIsSEStyleDesired = TRUE;
            }
            else
            {
                pTeTnlInfo->bIsSEStyleDesired = FALSE;
            }

            break;
        }
        case TE_FS_MPLS_ATTR_PTR:
        {
            if (pTeTnlInfo->u4AttrParamIndex == pSetTeTnlInfo->u4AttrParamIndex)
            {
                break;
            }

            if (pSetTeTnlInfo->u4AttrParamIndex == TE_ZERO)
            {
                pTeAttrListInfo
                    = TeGetAttrListInfo (pTeTnlInfo->u4AttrParamIndex);

                if (pTeAttrListInfo != NULL)
                {
                    pTeAttrListInfo->u2NumOfTunnels--;
                }

                pTeTnlInfo->u4AttrParamIndex = TE_ZERO;
                pTeTnlInfo->pTeAttrListInfo = NULL;
            }
            else
            {
                pTeTnlInfo->u4AttrParamIndex = pSetTeTnlInfo->u4AttrParamIndex;

                pTeAttrListInfo
                    = TeGetAttrListInfo (pTeTnlInfo->u4AttrParamIndex);

                if (pTeAttrListInfo != NULL)
                {
                    pTeAttrListInfo->u2NumOfTunnels++;
                    pTeTnlInfo->pTeAttrListInfo = pTeAttrListInfo;
                    if (TMO_SLL_Count (&pTeAttrListInfo->SrlgList) != TE_ZERO)
                    {
                        pTeTnlInfo->pAttrSrlgList = &pTeAttrListInfo->SrlgList;
                    }
                }
            }

            break;
        }

        case TE_FS_MPLS_TNL_PATH_TYPE:
        {
            pTeTnlInfo->u1TnlPathType = pSetTeTnlInfo->u1TnlPathType;

            if (pInstance0Tnl != NULL)
            {
                if (pTeTnlInfo->u1TnlPathType == TE_TNL_WORKING_PATH)
                {
                    pInstance0Tnl->u4TnlPrimaryInstance = u4TnlInstance;
                }
                else if (pTeTnlInfo->u1TnlPathType == TE_TNL_PROTECTION_PATH)
                {
                    pInstance0Tnl->u4BkpTnlInstance = u4TnlInstance;
                }
            }
            break;
        }
        case TE_FS_MPLS_TUNNEL_IFINDEX:
        {
            /* Passed If Index should be present in CFA, if it is not present
             * the value u4TnlIfIndex is set to ZERO. */
            if (CfaGetIfaceType (pSetTeTnlInfo->u4TnlIfIndex,
                                 &u1IfType) == CFA_FAILURE)
            {
                pTeTnlInfo->u4TnlIfIndex = TE_ZERO;
                break;
            }

            pTeTnlInfo->u4TnlIfIndex = pSetTeTnlInfo->u4TnlIfIndex;

            if ((pTeTnlInfo->u4TnlIfIndex != TE_ZERO) &&
                ((pTeTnlInfo->u4TnlInstance == TE_ZERO) ||
                 ((TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_HLSP) &&
                  (pTeTnlInfo->u1TnlRole == TE_EGRESS))))
            {
                if (RBTreeAdd
                    ((gTeGblInfo.TnlIfIndexBasedTbl),
                     (tRBElem *) pTeTnlInfo) == RB_FAILURE)
                {
                    pTeTnlInfo->u4TnlIfIndex = TE_ZERO;
                    return SNMP_FAILURE;
                }
            }
            break;
        }

        default:
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TeSetAllFsMplsAttributeTable
 Description :  This Routine sets the FS MPLS Attribute table objects
 Input       :  u4AttrListIndex       - Attribute List Index
                pSetTeAttrListInfo    - Pointer to Attribute List Info
                u4ObjectId            - Object Identifier
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TeSetAllFsMplsAttributeTable (UINT4 u4AttrListIndex,
                              tTeAttrListInfo * pSetTeAttrListInfo,
                              UINT4 u4ObjectId)
{
    tTeAttrListInfo    *pTeAttrListInfo = NULL;

    pTeAttrListInfo = TeGetAttrListInfo (u4AttrListIndex);

    if ((u4ObjectId != TE_FS_MPLS_ATTR_ROW_STATUS) ||
        (pSetTeAttrListInfo->u1RowStatus != CREATE_AND_WAIT))
    {
        if (pTeAttrListInfo == NULL)
        {
            return SNMP_FAILURE;
        }
    }
    else if (pTeAttrListInfo != NULL)
    {
        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case TE_FS_MPLS_ATTR_NAME:
        {
            pTeAttrListInfo->i4ListNameLen = pSetTeAttrListInfo->i4ListNameLen;
            MEMCPY (pTeAttrListInfo->au1ListName,
                    pSetTeAttrListInfo->au1ListName,
                    pTeAttrListInfo->i4ListNameLen);
            break;
        }
        case TE_FS_MPLS_ATTR_SETUP_PRIOR:
        {
            pTeAttrListInfo->u1SetupPriority
                = pSetTeAttrListInfo->u1SetupPriority;
            pTeAttrListInfo->u4ListBitmask |= TE_ATTR_SETUPPRI_BITMASK;
            break;
        }
        case TE_FS_MPLS_ATTR_HOLDING_PRIOR:
        {
            pTeAttrListInfo->u1HoldingPriority
                = pSetTeAttrListInfo->u1HoldingPriority;
            pTeAttrListInfo->u4ListBitmask |= TE_ATTR_HOLDPRI_BITMASK;
            break;
        }
        case TE_FS_MPLS_ATTR_INCLUDE_ANY:
        {
            pTeAttrListInfo->u4IncludeAnyAffinity
                = pSetTeAttrListInfo->u4IncludeAnyAffinity;
            pTeAttrListInfo->u4ListBitmask
                |= TE_ATTR_INCLUDE_ANY_AFFINITY_BITMASK;
            break;
        }
        case TE_FS_MPLS_ATTR_INCLUDE_ALL:
        {
            pTeAttrListInfo->u4IncludeAllAffinity
                = pSetTeAttrListInfo->u4IncludeAllAffinity;
            pTeAttrListInfo->u4ListBitmask
                |= TE_ATTR_INCLUDE_ALL_AFFINITY_BITMASK;
            break;
        }
        case TE_FS_MPLS_ATTR_EXCLUDE_ANY:
        {
            pTeAttrListInfo->u4ExcludeAnyAffinity
                = pSetTeAttrListInfo->u4ExcludeAnyAffinity;
            pTeAttrListInfo->u4ListBitmask
                |= TE_ATTR_EXCLUDE_ANY_AFFINITY_BITMASK;
            break;
        }
        case TE_FS_MPLS_ATTR_SSN_ATTR:
        {
            pTeAttrListInfo->u1SsnAttr = pSetTeAttrListInfo->u1SsnAttr;
            pTeAttrListInfo->u1SsnAttrLen = pSetTeAttrListInfo->u1SsnAttrLen;
            pTeAttrListInfo->u4ListBitmask |= TE_ATTR_LSP_SSN_ATTR_BITMASK;
            break;
        }
        case TE_FS_MPLS_ATTR_BANDWIDTH:
        {
            pTeAttrListInfo->u4Bandwidth = pSetTeAttrListInfo->u4Bandwidth;
            pTeAttrListInfo->u4ListBitmask |= TE_ATTR_BANDWIDTH_BITMASK;
            break;
        }
        case TE_FS_MPLS_ATTR_CLASS_TYPE:
        {
            pTeAttrListInfo->u4ClassType = pSetTeAttrListInfo->u4ClassType;
            pTeAttrListInfo->u4ListBitmask |= TE_ATTR_CLASS_TYPE_BITMASK;
            break;
        }
        case TE_FS_MPLS_ATTR_SRLG_TYPE:
        {
            pTeAttrListInfo->u1SrlgType = pSetTeAttrListInfo->u1SrlgType;
            pTeAttrListInfo->u4ListBitmask |= TE_ATTR_SRLG_TYPE_BITMASK;
            break;
        }
        case TE_FS_MPLS_ATTR_ROW_STATUS:
        {
            switch (pSetTeAttrListInfo->u1RowStatus)
            {
                case CREATE_AND_WAIT:
                {
                    if (TeCreateAttrListInfo (&pTeAttrListInfo, u4AttrListIndex)
                        == TE_FAILURE)
                    {
                        return SNMP_FAILURE;
                    }
                    pTeAttrListInfo->u1RowStatus = TE_NOTREADY;
                    break;
                }
                case ACTIVE:
                {
                    pTeAttrListInfo->u1RowStatus = ACTIVE;
                    break;
                }
                case TE_NOTINSERVICE:
                {
                    pTeAttrListInfo->u1RowStatus = NOT_IN_SERVICE;
                    break;
                }
                case TE_DESTROY:
                {
                    if (TeDeleteAttrListInfo (pTeAttrListInfo) == TE_FAILURE)
                    {
                        return SNMP_FAILURE;
                    }
                    break;
                }
                case CREATE_AND_GO:
                case NOT_READY:
                default:
                {
                    return SNMP_FAILURE;
                }
            }

            break;
        }

        default:
        {
            return SNMP_FAILURE;
        }
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TeSetAllFsMplsTunnelSrlgTable
 Description :  This function sets FS MPLS Tunnel SRLG Table Objects.
 Input       :  u4TnlIndex           - Tunnel Index
                u4TnlInstance        - Tunnel Instance
                u4IngressId          - Ingress LSR Id
                u4EgressId           - Egress LSR Id
                u4SrlgNo             - SRLG Value
                pSetTeTnlSrlg        - Pointer to Set Tunnel SRLG Info
                u4ObjectId           - Object Id
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TeSetAllFsMplsTunnelSrlgTable (UINT4 u4TnlIndex, UINT4 u4TnlInstance,
                               UINT4 u4IngressId, UINT4 u4EgressId,
                               UINT4 u4SrlgNo, tTeTnlSrlg * pSetTeTnlSrlg,
                               UINT4 u4ObjectId)
{
    tTeTnlSrlg         *pTeTnlSrlg = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;

    pTeTnlInfo = TeGetTunnelInfo (u4TnlIndex, u4TnlInstance,
                                  u4IngressId, u4EgressId);
    if (pTeTnlInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pTeTnlSrlg = TeGetSrlgFromTnlSrlgList (u4SrlgNo, pTeTnlInfo);

    if ((u4ObjectId != TE_FS_MPLS_SRLG_ROW_STATUS) ||
        (pSetTeTnlSrlg->u4RowStatus != CREATE_AND_WAIT))
    {
        if (pTeTnlSrlg == NULL)
        {
            return SNMP_FAILURE;
        }
    }
    else if (pTeTnlSrlg != NULL)
    {
        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case TE_FS_MPLS_SRLG_ROW_STATUS:
        {
            switch (pSetTeTnlSrlg->u4RowStatus)
            {
                case CREATE_AND_WAIT:
                {
                    pTeTnlSrlg = (tTeTnlSrlg *)
                        TE_ALLOC_MEM_BLOCK (TE_TNL_SRLG_POOL_ID);

                    if (pTeTnlSrlg == NULL)
                    {
                        return SNMP_FAILURE;
                    }

                    MEMSET (pTeTnlSrlg, TE_ZERO, sizeof (tTeTnlSrlg));
                    pTeTnlSrlg->u4SrlgNo = u4SrlgNo;
		    /*Set SRLG Type to INCLUDE_ANY if its NULL*/
		    if(pTeTnlInfo->u1TnlSrlgType == TE_ZERO)
		    {
			    pTeTnlInfo->u1TnlSrlgType = TNL_SRLG_INCLUDE_ANY;
		    }
		    pTeTnlSrlg->u4RowStatus = NOT_READY;

                    TeAddSrlgToSortTnlSrlgList (pTeTnlSrlg, pTeTnlInfo);

                    break;
                }
                case ACTIVE:
                {
                    if (pTeTnlSrlg->u4SrlgNo != u4SrlgNo)
                    {
                        pTeTnlInfo->bIsMbbRequired = TRUE;
                    }
                    pTeTnlSrlg->u4RowStatus = ACTIVE;
                    break;
                }
                case DESTROY:
                {
                    TeRemoveSrlgFromSortTnlSrlgList (pTeTnlSrlg, pTeTnlInfo);

                    MEMSET (pTeTnlSrlg, TE_ZERO, sizeof (tTeTnlSrlg));

                    TE_REL_MEM_BLOCK (TE_TNL_SRLG_POOL_ID,
                                      (UINT1 *) pTeTnlSrlg);
                    break;
                }
                case CREATE_AND_GO:
                case NOT_IN_SERVICE:
                case NOT_READY:
                default:
                {
                    return SNMP_FAILURE;
                }
            }

            break;
        }

        default:
        {
            return SNMP_FAILURE;
        }
    }
    KW_FALSEPOSITIVE_FIX (pTeTnlSrlg);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TeSetAllFsMplsAttributeSrlgTable
 Description :  This function sets FS MPLS Tunnel Attribute SRLG Objects
 Input       :  u4AttrListIndex       - Attribute List Index
                u4SrlgNo              - SRLG Value
                pSetTeAttrSrlg        - Pointer to Attribute SRLG Info
                u4ObjectId            - Object ID
 Output      :  None
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TeSetAllFsMplsAttributeSrlgTable (UINT4 u4AttrListIndex, UINT4 u4SrlgNo,
                                  tTeAttrSrlg * pSetTeAttrSrlg,
                                  UINT4 u4ObjectId)
{
    tTeAttrSrlg        *pTeAttrSrlg = NULL;
    tTeAttrListInfo    *pTeAttrListInfo = NULL;

    pTeAttrListInfo = TeGetAttrListInfo (u4AttrListIndex);

    if (pTeAttrListInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pTeAttrSrlg = TeGetSrlgFromAttrSrlgList (u4SrlgNo, pTeAttrListInfo);

    if ((u4ObjectId != TE_FS_MPLS_ATTR_SRLG_ROW_STATUS) ||
        (pSetTeAttrSrlg->u4RowStatus != CREATE_AND_WAIT))
    {
        if (pTeAttrSrlg == NULL)
        {
            return SNMP_FAILURE;
        }
    }
    else if (pTeAttrSrlg != NULL)
    {
        return SNMP_FAILURE;
    }

    switch (u4ObjectId)
    {
        case TE_FS_MPLS_ATTR_SRLG_ROW_STATUS:
        {
            switch (pSetTeAttrSrlg->u4RowStatus)
            {
                case CREATE_AND_WAIT:
                {
                    pTeAttrSrlg = (tTeAttrSrlg *)
                        TE_ALLOC_MEM_BLOCK (TE_ATTR_SRLG_POOL_ID);

                    if (pTeAttrSrlg == NULL)
                    {
                        return SNMP_FAILURE;
                    }

                    MEMSET (pTeAttrSrlg, TE_ZERO, sizeof (tTeAttrSrlg));
                    pTeAttrSrlg->u4SrlgNo = u4SrlgNo;
                    pTeAttrSrlg->u4RowStatus = NOT_READY;

                    TeAddSrlgToSortAttrSrlgList (pTeAttrSrlg, pTeAttrListInfo);

                    break;
                }
                case ACTIVE:
                {
                    pTeAttrSrlg->u4RowStatus = ACTIVE;
                    break;
                }
                case DESTROY:
                {
                    TeRemoveSrlgFromSortAttrSrlgList (pTeAttrSrlg,
                                                      pTeAttrListInfo);

                    MEMSET (pTeAttrSrlg, TE_ZERO, sizeof (tTeAttrSrlg));

                    TE_REL_MEM_BLOCK (TE_ATTR_SRLG_POOL_ID,
                                      (UINT1 *) pTeAttrSrlg);
                    break;
                }
                case CREATE_AND_GO:
                case NOT_IN_SERVICE:
                case NOT_READY:
                default:
                {
                    return SNMP_FAILURE;
                }
            }

            break;
        }

        default:
        {
            return SNMP_FAILURE;
        }
    }
    KW_FALSEPOSITIVE_FIX (pTeAttrSrlg);
    return SNMP_SUCCESS;
}
