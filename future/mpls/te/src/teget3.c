/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: teget3.c,v 1.8 2014/12/12 11:56:47 siva Exp $
 *
 * Description: This file contains the low level GET routines
 *              for the following TE MIB tables.
 *              - MplsTunnelResourceTable
 *              - MplsTunnelCRLDPResTable
 *              - FsMplsTunnelCRLDPResTable
 *              - FsMplsTunnelRSVPResTable
 *******************************************************************/

#include "teincs.h"
#include "fsmplslw.h"
#include "stdtelw.h"

/* LOW LEVEL Routines for Table : MplsTunnelResourceTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsTunnelResourceTable
 Input       :  The Indices
                MplsTunnelResourceIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhValidateIndexInstanceMplsTunnelResourceTable (UINT4
                                                 u4MplsTunnelResourceIndex)
{
    /* Curently the Tunnel Resource index value is checked against a range.
     */
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (!((u4MplsTunnelResourceIndex >= TE_TNL_RSRC_INDEX_MINVAL) &&
          (u4MplsTunnelResourceIndex <= TE_TNL_RSRC_INDEX_MAXVAL)))
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsTunnelResourceTable
 Input       :  The Indices
                MplsTunnelResourceIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexMplsTunnelResourceTable (UINT4 *pu4MplsTunnelResourceIndex)
{
    UINT4               u4TrfcParamArrayIndex;

    *pu4MplsTunnelResourceIndex = TE_ZERO;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    for (u4TrfcParamArrayIndex = TE_ONE;
         u4TrfcParamArrayIndex <= TE_MAX_TRFC_PARAMS (gTeGblInfo);
         u4TrfcParamArrayIndex++)
    {
        if (TE_TRFC_PARAMS_TNL_RESINDEX (u4TrfcParamArrayIndex) != TE_ZERO)
        {
            *pu4MplsTunnelResourceIndex = u4TrfcParamArrayIndex;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsTunnelResourceTable
 Input       :  The Indices
                MplsTunnelResourceIndex
                nextMplsTunnelResourceIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexMplsTunnelResourceTable (UINT4 u4MplsTunnelResourceIndex,
                                        UINT4 *pu4NextMplsTunnelResourceIndex)
{
    UINT4               u4TrfcParamArrayIndex;

    *pu4NextMplsTunnelResourceIndex = TE_ZERO;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* Checking if the Resource Index lies between the minimum and maximum values */

    if (!((u4MplsTunnelResourceIndex >= TE_TNL_RSRC_INDEX_MINVAL) &&
          (u4MplsTunnelResourceIndex <= TE_TNL_RSRC_INDEX_MAXVAL)))
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    for (u4TrfcParamArrayIndex = u4MplsTunnelResourceIndex + TE_ONE;
         u4TrfcParamArrayIndex <= TE_MAX_TRFC_PARAMS (gTeGblInfo);
         u4TrfcParamArrayIndex++)
    {
        if (TE_TRFC_PARAMS_TNL_RESINDEX (u4TrfcParamArrayIndex) != TE_ZERO)
        {
            *pu4NextMplsTunnelResourceIndex = u4TrfcParamArrayIndex;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelResourceIndexNext
 Input       :  The Indices

                The Object 
                retValMplsTunnelResourceIndexNext
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelResourceIndexNext (UINT4 *pu4RetValMplsTunnelResourceIndexNext)
{
    UINT4               u4TrfcParamArrayIndex;

    *pu4RetValMplsTunnelResourceIndexNext = 0;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }

    for (u4TrfcParamArrayIndex = TE_ONE;
         u4TrfcParamArrayIndex <= TE_MAX_TRFC_PARAMS (gTeGblInfo);
         u4TrfcParamArrayIndex++)
    {
        if (TE_TRFC_PARAMS_TNL_RESINDEX (u4TrfcParamArrayIndex) == TE_ZERO)
        {
            *pu4RetValMplsTunnelResourceIndexNext = u4TrfcParamArrayIndex;
            break;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelResourceMaxRate
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                retValMplsTunnelResourceMaxRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelResourceMaxRate (UINT4 u4MplsTunnelResourceIndex,
                                 UINT4 *pu4RetValMplsTunnelResourceMaxRate)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();
    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        *pu4RetValMplsTunnelResourceMaxRate =
            TE_TNLRSRC_MAX_RATE (pTeTrfcParams);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelResourceMeanRate
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                retValMplsTunnelResourceMeanRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelResourceMeanRate (UINT4 u4MplsTunnelResourceIndex,
                                  UINT4 *pu4RetValMplsTunnelResourceMeanRate)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();
    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        /* TODO Add Support for this Object */
        *pu4RetValMplsTunnelResourceMeanRate = 0;
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelResourceMaxBurstSize
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                retValMplsTunnelResourceMaxBurstSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelResourceMaxBurstSize (UINT4 u4MplsTunnelResourceIndex,
                                      UINT4
                                      *pu4RetValMplsTunnelResourceMaxBurstSize)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();
    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        /* TODO Add Support for this Object */
        *pu4RetValMplsTunnelResourceMaxBurstSize = 0;
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelResourceMeanBurstSize
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                retValMplsTunnelResourceMeanBurstSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelResourceMeanBurstSize (UINT4 u4MplsTunnelResourceIndex,
                                       UINT4
                                       *pu4RetValMplsTunnelResourceMeanBurstSize)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();
    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        /* TODO Add Support for this Object */
        *pu4RetValMplsTunnelResourceMeanBurstSize = 0;
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelResourceExBurstSize
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                retValMplsTunnelResourceExBurstSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelResourceExBurstSize (UINT4 u4MplsTunnelResourceIndex,
                                     UINT4
                                     *pu4RetValMplsTunnelResourceExBurstSize)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();
    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        /* TODO Add Support for this Object */
        *pu4RetValMplsTunnelResourceExBurstSize = 0;
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelResourceFrequency
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                retValMplsTunnelResourceFrequency
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelResourceFrequency (UINT4 u4MplsTunnelResourceIndex,
                                   INT4 *pi4RetValMplsTunnelResourceFrequency)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();
    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        /* TODO Add Support for this Object */
        *pi4RetValMplsTunnelResourceFrequency = TE_TNL_FREQ_UNSPECIFIED;
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelResourceWeight
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                retValMplsTunnelResourceWeight
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelResourceWeight (UINT4 u4MplsTunnelResourceIndex,
                                UINT4 *pu4RetValMplsTunnelResourceWeight)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();
    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        /* TODO Add Support for this Object */
        *pu4RetValMplsTunnelResourceWeight = 0;
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelResourceRowStatus
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                retValMplsTunnelResourceRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelResourceRowStatus (UINT4 u4MplsTunnelResourceIndex,
                                   INT4 *pi4RetValMplsTunnelResourceRowStatus)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();

    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        *pi4RetValMplsTunnelResourceRowStatus =
            TE_TNLRSRC_ROW_STATUS (pTeTrfcParams);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelResourceStorageType
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                retValMplsTunnelResourceStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelResourceStorageType (UINT4 u4MplsTunnelResourceIndex,
                                     INT4
                                     *pi4RetValMplsTunnelResourceStorageType)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        *pi4RetValMplsTunnelResourceStorageType =
            TE_TNLRSRC_STORAGE_TYPE (pTeTrfcParams);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : MplsTunnelCRLDPResTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsTunnelCRLDPResTable
 Input       :  The Indices
                MplsTunnelResourceIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsTunnelCRLDPResTable (UINT4
                                                 u4MplsTunnelResourceIndex)
{

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    if (nmhValidateIndexInstanceMplsTunnelResourceTable
        (u4MplsTunnelResourceIndex) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsTunnelCRLDPResTable
 Input       :  The Indices
                MplsTunnelResourceIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsTunnelCRLDPResTable (UINT4 *pu4MplsTunnelResourceIndex)
{
    UINT4               u4TrfcParamArrayIndex;

    *pu4MplsTunnelResourceIndex = TE_ZERO;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    for (u4TrfcParamArrayIndex = TE_ONE;
         u4TrfcParamArrayIndex <= TE_MAX_TRFC_PARAMS (gTeGblInfo);
         u4TrfcParamArrayIndex++)
    {
        if ((TE_TRFC_PARAMS_TNL_RESINDEX (u4TrfcParamArrayIndex) != TE_ZERO) &&
            (TE_CRLDP_TRFC_PARAMS_PTR (u4TrfcParamArrayIndex) != NULL))
        {
            *pu4MplsTunnelResourceIndex = u4TrfcParamArrayIndex;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsTunnelCRLDPResTable
 Input       :  The Indices
                MplsTunnelResourceIndex
                nextMplsTunnelResourceIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMplsTunnelCRLDPResTable (UINT4 u4MplsTunnelResourceIndex,
                                        UINT4 *pu4NextMplsTunnelResourceIndex)
{
    UINT4               u4TrfcParamArrayIndex;

    *pu4NextMplsTunnelResourceIndex = TE_ZERO;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    for (u4TrfcParamArrayIndex = u4MplsTunnelResourceIndex + TE_ONE;
         u4TrfcParamArrayIndex <= TE_MAX_TRFC_PARAMS (gTeGblInfo);
         u4TrfcParamArrayIndex++)
    {
        if ((TE_TRFC_PARAMS_TNL_RESINDEX (u4TrfcParamArrayIndex) != TE_ZERO) &&
            (TE_CRLDP_TRFC_PARAMS_PTR (u4TrfcParamArrayIndex) != NULL))
        {
            *pu4NextMplsTunnelResourceIndex = u4TrfcParamArrayIndex;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : MplsTunnelCRLDPResTable. */

/****************************************************************************
 Function    :  nmhGetMplsTunnelCRLDPResMeanBurstSize
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                retValMplsTunnelCRLDPResMeanBurstSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelCRLDPResMeanBurstSize (UINT4 u4MplsTunnelResourceIndex,
                                       UINT4
                                       *pu4RetValMplsTunnelCRLDPResMeanBurstSize)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) != NULL)
        {
            *pu4RetValMplsTunnelCRLDPResMeanBurstSize =
                TE_CRLDP_TPARAM_CBS (pTeTrfcParams);

            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelCRLDPResExBurstSize
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                retValMplsTunnelCRLDPResExBurstSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelCRLDPResExBurstSize (UINT4 u4MplsTunnelResourceIndex,
                                     UINT4
                                     *pu4RetValMplsTunnelCRLDPResExBurstSize)
{

    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) != NULL)
        {
            *pu4RetValMplsTunnelCRLDPResExBurstSize =
                TE_CRLDP_TPARAM_EBS (pTeTrfcParams);
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelCRLDPResFrequency
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                retValMplsTunnelCRLDPResFrequency
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelCRLDPResFrequency (UINT4 u4MplsTunnelResourceIndex,
                                   INT4 *pi4RetValMplsTunnelCRLDPResFrequency)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) != NULL)
        {
            *pi4RetValMplsTunnelCRLDPResFrequency =
                TE_CRLDP_TPARAM_FREQ (pTeTrfcParams);
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelCRLDPResWeight
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                retValMplsTunnelCRLDPResWeight
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelCRLDPResWeight (UINT4 u4MplsTunnelResourceIndex,
                                UINT4 *pu4RetValMplsTunnelCRLDPResWeight)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) != NULL)
        {
            *pu4RetValMplsTunnelCRLDPResWeight =
                TE_CRLDP_TPARAM_WEIGHT (pTeTrfcParams);
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelCRLDPResFlags
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                retValMplsTunnelCRLDPResFlags
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelCRLDPResFlags (UINT4 u4MplsTunnelResourceIndex,
                               UINT4 *pu4RetValMplsTunnelCRLDPResFlags)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();

    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) != NULL)
        {
            *pu4RetValMplsTunnelCRLDPResFlags =
                TE_CRLDP_TPARAM_FLAGS (pTeTrfcParams);
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelCRLDPResRowStatus
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                retValMplsTunnelCRLDPResRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelCRLDPResRowStatus (UINT4 u4MplsTunnelResourceIndex,
                                   INT4 *pi4RetValMplsTunnelCRLDPResRowStatus)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();

    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) != NULL)
        {
            *pi4RetValMplsTunnelCRLDPResRowStatus =
                TE_CRLDP_TPARAM_ROW_STATUS (pTeTrfcParams);
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelCRLDPResStorageType
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                retValMplsTunnelCRLDPResStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelCRLDPResStorageType (UINT4 u4MplsTunnelResourceIndex,
                                     INT4
                                     *pi4RetValMplsTunnelCRLDPResStorageType)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) != NULL)
        {
            *pi4RetValMplsTunnelCRLDPResStorageType =
                TE_CRLDP_TPARAM_STORAGE_TYPE (pTeTrfcParams);
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FsMplsTunnelCRLDPResTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsTunnelCRLDPResTable
 Input       :  The Indices
                MplsTunnelResourceIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMplsTunnelCRLDPResTable (UINT4
                                                   u4MplsTunnelResourceIndex)
{

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    if (nmhValidateIndexInstanceMplsTunnelResourceTable
        (u4MplsTunnelResourceIndex) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsTunnelCRLDPResTable
 Input       :  The Indices
                MplsTunnelResourceIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMplsTunnelCRLDPResTable (UINT4 *pu4MplsTunnelResourceIndex)
{

    if (nmhGetFirstIndexMplsTunnelCRLDPResTable (pu4MplsTunnelResourceIndex)
        != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMplsTunnelCRLDPResTable
 Input       :  The Indices
                MplsTunnelResourceIndex
                nextMplsTunnelResourceIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMplsTunnelCRLDPResTable (UINT4 u4MplsTunnelResourceIndex,
                                          UINT4 *pu4NextMplsTunnelResourceIndex)
{

    if (nmhGetNextIndexMplsTunnelCRLDPResTable (u4MplsTunnelResourceIndex,
                                                pu4NextMplsTunnelResourceIndex)
        != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelCRLDPResPeakDataRate
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                retValFsMplsTunnelCRLDPResPeakDataRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelCRLDPResPeakDataRate (UINT4 u4MplsTunnelResourceIndex,
                                        UINT4
                                        *pu4RetValFsMplsTunnelCRLDPResPeakDataRate)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) != NULL)
        {
            *pu4RetValFsMplsTunnelCRLDPResPeakDataRate =
                TE_CRLDP_TPARAM_PDR (pTeTrfcParams);
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelCRLDPResCommittedDataRate
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                retValFsMplsTunnelCRLDPResCommittedDataRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelCRLDPResCommittedDataRate (UINT4 u4MplsTunnelResourceIndex,
                                             UINT4
                                             *pu4RetValFsMplsTunnelCRLDPResCommittedDataRate)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) != NULL)
        {
            *pu4RetValFsMplsTunnelCRLDPResCommittedDataRate =
                TE_CRLDP_TPARAM_CDR (pTeTrfcParams);
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelCRLDPResPeakBurstSize
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                retValFsMplsTunnelCRLDPResPeakBurstSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelCRLDPResPeakBurstSize (UINT4 u4MplsTunnelResourceIndex,
                                         UINT4
                                         *pu4RetValFsMplsTunnelCRLDPResPeakBurstSize)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();

    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) != NULL)
        {
            *pu4RetValFsMplsTunnelCRLDPResPeakBurstSize =
                TE_CRLDP_TPARAM_PBS (pTeTrfcParams);
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelCRLDPResCommittedBurstSize
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                retValFsMplsTunnelCRLDPResCommittedBurstSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelCRLDPResCommittedBurstSize (UINT4 u4MplsTunnelResourceIndex,
                                              UINT4
                                              *pu4RetValFsMplsTunnelCRLDPResCommittedBurstSize)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();

    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) != NULL)
        {
            *pu4RetValFsMplsTunnelCRLDPResCommittedBurstSize =
                TE_CRLDP_TPARAM_CBS (pTeTrfcParams);
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelCRLDPResExcessBurstSize
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                retValFsMplsTunnelCRLDPResExcessBurstSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelCRLDPResExcessBurstSize (UINT4 u4MplsTunnelResourceIndex,
                                           UINT4
                                           *pu4RetValFsMplsTunnelCRLDPResExcessBurstSize)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();

    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) != NULL)
        {
            *pu4RetValFsMplsTunnelCRLDPResExcessBurstSize =
                TE_CRLDP_TPARAM_EBS (pTeTrfcParams);
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FsMplsTunnelRSVPResTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsTunnelRSVPResTable
 Input       :  The Indices
                MplsTunnelResourceIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMplsTunnelRSVPResTable (UINT4
                                                  u4MplsTunnelResourceIndex)
{

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    if (nmhValidateIndexInstanceMplsTunnelResourceTable
        (u4MplsTunnelResourceIndex) != SNMP_SUCCESS)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsTunnelRSVPResTable
 Input       :  The Indices
                MplsTunnelResourceIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMplsTunnelRSVPResTable (UINT4 *pu4MplsTunnelResourceIndex)
{
    UINT4               u4TrfcParamArrayIndex;

    *pu4MplsTunnelResourceIndex = TE_ZERO;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    for (u4TrfcParamArrayIndex = TE_ONE;
         u4TrfcParamArrayIndex <= TE_MAX_TRFC_PARAMS (gTeGblInfo);
         u4TrfcParamArrayIndex++)
    {
        if ((TE_TRFC_PARAMS_TNL_RESINDEX (u4TrfcParamArrayIndex) != TE_ZERO) &&
            (TE_RSVP_TRFC_PARAMS_PTR (u4TrfcParamArrayIndex) != NULL))
        {
            *pu4MplsTunnelResourceIndex = u4TrfcParamArrayIndex;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMplsTunnelRSVPResTable
 Input       :  The Indices
                MplsTunnelResourceIndex
                nextMplsTunnelResourceIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMplsTunnelRSVPResTable (UINT4 u4MplsTunnelResourceIndex,
                                         UINT4 *pu4NextMplsTunnelResourceIndex)
{
    UINT4               u4TrfcParamArrayIndex;

    *pu4NextMplsTunnelResourceIndex = TE_ZERO;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (!((u4MplsTunnelResourceIndex >= TE_TNL_RSRC_INDEX_MINVAL) &&
          (u4MplsTunnelResourceIndex <= TE_TNL_RSRC_INDEX_MAXVAL)))
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    for (u4TrfcParamArrayIndex = u4MplsTunnelResourceIndex + TE_ONE;
         u4TrfcParamArrayIndex <= TE_MAX_TRFC_PARAMS (gTeGblInfo);
         u4TrfcParamArrayIndex++)
    {
        if ((TE_TRFC_PARAMS_TNL_RESINDEX (u4TrfcParamArrayIndex) != TE_ZERO) &&
            (TE_RSVP_TRFC_PARAMS_PTR (u4TrfcParamArrayIndex) != NULL))
        {
            *pu4NextMplsTunnelResourceIndex = u4TrfcParamArrayIndex;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelRSVPResTokenBucketRate
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                retValFsMplsTunnelRSVPResTokenBucketRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelRSVPResTokenBucketRate (UINT4 u4MplsTunnelResourceIndex,
                                          UINT4
                                          *pu4RetValFsMplsTunnelRSVPResTokenBucketRate)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();

    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if (TE_RSVPTE_TRFC_PARAMS (pTeTrfcParams) != NULL)
        {
            *pu4RetValFsMplsTunnelRSVPResTokenBucketRate =
                TE_RSVPTE_TPARAM_TBR (pTeTrfcParams);
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelRSVPResTokenBucketSize
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                retValFsMplsTunnelRSVPResTokenBucketSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelRSVPResTokenBucketSize (UINT4 u4MplsTunnelResourceIndex,
                                          UINT4
                                          *pu4RetValFsMplsTunnelRSVPResTokenBucketSize)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();

    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if (TE_RSVPTE_TRFC_PARAMS (pTeTrfcParams) != NULL)
        {
            *pu4RetValFsMplsTunnelRSVPResTokenBucketSize =
                TE_RSVPTE_TPARAM_TBS (pTeTrfcParams);
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelRSVPResPeakDataRate
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                retValFsMplsTunnelRSVPResPeakDataRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelRSVPResPeakDataRate (UINT4 u4MplsTunnelResourceIndex,
                                       UINT4
                                       *pu4RetValFsMplsTunnelRSVPResPeakDataRate)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();

    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if (TE_RSVPTE_TRFC_PARAMS (pTeTrfcParams) != NULL)
        {
            *pu4RetValFsMplsTunnelRSVPResPeakDataRate =
                TE_RSVPTE_TPARAM_PDR (pTeTrfcParams);
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelRSVPResMinimumPolicedUnit
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                retValFsMplsTunnelRSVPResMinimumPolicedUnit
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelRSVPResMinimumPolicedUnit (UINT4 u4MplsTunnelResourceIndex,
                                             INT4
                                             *pi4RetValFsMplsTunnelRSVPResMinimumPolicedUnit)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();

    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if (TE_RSVPTE_TRFC_PARAMS (pTeTrfcParams) != NULL)
        {
            *pi4RetValFsMplsTunnelRSVPResMinimumPolicedUnit =
               (INT4) TE_RSVPTE_TPARAM_MPU (pTeTrfcParams);
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelRSVPResMaximumPacketSize
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                retValFsMplsTunnelRSVPResMaximumPacketSize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelRSVPResMaximumPacketSize (UINT4 u4MplsTunnelResourceIndex,
                                            INT4
                                            *pi4RetValFsMplsTunnelRSVPResMaximumPacketSize)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();

    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if (TE_RSVPTE_TRFC_PARAMS (pTeTrfcParams) != NULL)
        {
            *pi4RetValFsMplsTunnelRSVPResMaximumPacketSize =
                (INT4)TE_RSVPTE_TPARAM_MPS (pTeTrfcParams);
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/*---------------------------------------------------------------------------*/
/*                       End of file teget3.c                                */
/*---------------------------------------------------------------------------*/
