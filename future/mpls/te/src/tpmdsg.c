/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: tpmdsg.c,v 1.5 2013/06/26 11:42:04 siva Exp $
*
* Description: This file contains the routines for DataStructure access for the module Te 
*********************************************************************/

#include "teincs.h"
#include "mplslsr.h"
#include "stdtelw.h"
/****************************************************************************
 Function    :  TeGetMplsTeP2mpTunnelConfigured
 Input       :  The Indices
                pu4MplsTeP2mpTunnelConfigured
 Output      :  This Routine will Get 
                the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT4
TeGetMplsTeP2mpTunnelConfigured (UINT4 *pu4MplsTeP2mpTunnelConfigured)
{
    TE_DBG (TE_MAIN_ETEXT, "TeGetMplsTeP2mpTunnelConfigured : ENTRY \n");
    *pu4MplsTeP2mpTunnelConfigured = TE_P2MP_CONFIGURED_TNLS (gTeGblInfo);
    TE_DBG (TE_MAIN_ETEXT, "TeGetMplsTeP2mpTunnelConfigured : EXIT \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TeGetMplsTeP2mpTunnelActive
 Input       :  The Indices
                pu4MplsTeP2mpTunnelActive
 Output      :  This Routine will Get 
                the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT4
TeGetMplsTeP2mpTunnelActive (UINT4 *pu4MplsTeP2mpTunnelActive)
{
    TE_DBG (TE_MAIN_ETEXT, "TeGetMplsTeP2mpTunnelActive : ENTRY \n");
    *pu4MplsTeP2mpTunnelActive = TE_P2MP_ACTIVE_TNLS (gTeGblInfo);
    TE_DBG (TE_MAIN_ETEXT, "TeGetMplsTeP2mpTunnelActive : EXIT \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TeGetMplsTeP2mpTunnelTotalMaxHops
 Input       :  The Indices
                pu4MplsTeP2mpTunnelTotalMaxHops
 Output      :  This Routine will Get 
                the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT4
TeGetMplsTeP2mpTunnelTotalMaxHops (UINT4 *pu4MplsTeP2mpTunnelTotalMaxHops)
{
    TE_DBG (TE_MAIN_ETEXT, "TeGetMplsTeP2mpTunnelTotalMaxHops : ENTRY \n");
    *pu4MplsTeP2mpTunnelTotalMaxHops = TE_P2MP_TNL_MAX_HOP (gTeGblInfo);
    TE_DBG (TE_MAIN_ETEXT, "TeGetMplsTeP2mpTunnelTotalMaxHops : EXIT \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TeGetFirstMplsTeP2mpTunnelTable
 Input       :  The Indices
                NONE
 Output      :  First index in P2MP tunnel table 
                pTeMplsTeP2mpTunnelTable
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT4
TeGetFirstMplsTeP2mpTunnelTable (tTeMplsTeP2mpTunnelTable *
                                 pTeMplsTeP2mpTunnelTable)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4Index = TE_ZERO, u4Instance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO, u4EgressId = TE_ZERO;

    TE_DBG (TE_MAIN_ETEXT, "TeGetFirstMplsTeP2mpTunnelTable : ENTRY \n");

    if (SNMP_SUCCESS == nmhGetFirstIndexMplsTunnelTable (&u4Index,
                                                         &u4Instance,
                                                         &u4IngressId,
                                                         &u4EgressId))
    {
        do
        {
            MPLS_CMN_LOCK ();
            pTeTnlInfo = TeGetTunnelInfo (u4Index,
                                          u4Instance, u4IngressId, u4EgressId);
            if (pTeTnlInfo == NULL)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            /*Check whether its P2MP tunnel and has valid data in the table */
            if ((NULL != TE_P2MP_TNL_INFO (pTeTnlInfo)) &&
                (TE_TNL_TYPE_P2MP & TE_TNL_TYPE (pTeTnlInfo)))
            {
                MPLS_CMN_UNLOCK ();
                pTeMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelIndex = u4Index;
                pTeMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelInstance =
                    u4Instance;
                pTeMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelIngressLSRId =
                    u4IngressId;
                pTeMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelEgressLSRId =
                    u4EgressId;
                return SNMP_SUCCESS;
            }
            MPLS_CMN_UNLOCK ();
        }
        while (SNMP_SUCCESS == (nmhGetNextIndexMplsTunnelTable (u4Index,
                                                                &u4Index,
                                                                u4Instance,
                                                                &u4Instance,
                                                                u4IngressId,
                                                                &u4IngressId,
                                                                u4EgressId,
                                                                &u4EgressId)));
    }
    TE_DBG (TE_MAIN_ETEXT, "TeGetFirstMplsTeP2mpTunnelTable : EXIT \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  TeGetNextMplsTeP2mpTunnelTable
 Input       :  The Indices
                pCurrentTeMplsTeP2mpTunnelTable
 Output      :  Next index in P2MP tunnel table 
                pNextTeMplsTeP2mpTunnelTable
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT4
TeGetNextMplsTeP2mpTunnelTable (tTeMplsTeP2mpTunnelTable *
                                pCurrentTeMplsTeP2mpTunnelTable,
                                tTeMplsTeP2mpTunnelTable *
                                pNextTeMplsTeP2mpTunnelTable)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4Index = TE_ZERO, u4Instance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO, u4EgressId = TE_ZERO;

    TE_DBG (TE_MAIN_ETEXT, "TeGetNextMplsTeP2mpTunnelTable : ENTRY \n");

    MPLS_CMN_LOCK ();

    /*Check whether its a valid tunnel */
    pTeTnlInfo = TeGetTunnelInfo (pCurrentTeMplsTeP2mpTunnelTable->MibObject.
                                  u4MplsTunnelIndex,
                                  pCurrentTeMplsTeP2mpTunnelTable->MibObject.
                                  u4MplsTunnelInstance,
                                  pCurrentTeMplsTeP2mpTunnelTable->MibObject.
                                  u4MplsTunnelIngressLSRId,
                                  pCurrentTeMplsTeP2mpTunnelTable->MibObject.
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    else
    {
        if (!(TE_TNL_TYPE_P2MP & TE_TNL_TYPE (pTeTnlInfo)))
        {
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }
    MPLS_CMN_UNLOCK ();

    /*Get a next valid P2MP tunnel */
    if (SNMP_SUCCESS ==
        nmhGetNextIndexMplsTunnelTable (pCurrentTeMplsTeP2mpTunnelTable->
                                        MibObject.u4MplsTunnelIndex, &u4Index,
                                        pCurrentTeMplsTeP2mpTunnelTable->
                                        MibObject.u4MplsTunnelInstance,
                                        &u4Instance,
                                        pCurrentTeMplsTeP2mpTunnelTable->
                                        MibObject.u4MplsTunnelIngressLSRId,
                                        &u4IngressId,
                                        pCurrentTeMplsTeP2mpTunnelTable->
                                        MibObject.u4MplsTunnelEgressLSRId,
                                        &u4EgressId))
    {
        do
        {
            MPLS_CMN_LOCK ();

            /*Check whether its a valid tunnel */
            pTeTnlInfo = TeGetTunnelInfo (u4Index,
                                          u4Instance, u4IngressId, u4EgressId);
            if (pTeTnlInfo == NULL)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            /*Check whether its P2MP tunnel and has valid data in the table */
            if ((NULL != TE_P2MP_TNL_INFO (pTeTnlInfo)) &&
                (TE_TNL_TYPE_P2MP & TE_TNL_TYPE (pTeTnlInfo)))
            {
                MPLS_CMN_UNLOCK ();
                pNextTeMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelIndex =
                    u4Index;
                pNextTeMplsTeP2mpTunnelTable->MibObject.u4MplsTunnelInstance =
                    u4Instance;
                pNextTeMplsTeP2mpTunnelTable->MibObject.
                    u4MplsTunnelIngressLSRId = u4IngressId;
                pNextTeMplsTeP2mpTunnelTable->MibObject.
                    u4MplsTunnelEgressLSRId = u4EgressId;
                return SNMP_SUCCESS;
            }
            MPLS_CMN_UNLOCK ();
        }
        while (SNMP_SUCCESS == (nmhGetNextIndexMplsTunnelTable (u4Index,
                                                                &u4Index,
                                                                u4Instance,
                                                                &u4Instance,
                                                                u4IngressId,
                                                                &u4IngressId,
                                                                u4EgressId,
                                                                &u4EgressId)));
    }
    TE_DBG (TE_MAIN_ETEXT, "TeGetNextMplsTeP2mpTunnelTable : EXIT \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  TeGetMplsTeP2mpTunnelSubGroupIDNext
 Input       :  The Indices
                pu4MplsTeP2mpTunnelSubGroupIDNext
 Output      :  This Routine will Get 
                the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT4
TeGetMplsTeP2mpTunnelSubGroupIDNext (UINT4 *pu4MplsTeP2mpTunnelSubGroupIDNext)
{
    TE_DBG (TE_MAIN_ETEXT, "TeGetMplsTeP2mpTunnelSubGroupIDNext : ENTRY \n");
    *pu4MplsTeP2mpTunnelSubGroupIDNext = TE_ONE;
    TE_DBG (TE_MAIN_ETEXT, "TeGetMplsTeP2mpTunnelSubGroupIDNext : EXIT \n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TeGetFirstMplsTeP2mpTunnelDestTable
 Input       :  The Indices
                NONE
 Output      :  First index in P2MP destination table 
                pTeMplsTeP2mpTunnelDestTable
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT4
TeGetFirstMplsTeP2mpTunnelDestTable (tTeMplsTeP2mpTunnelDestTable *
                                     pTeMplsTeP2mpTunnelDestTable)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tP2mpDestEntry     *pTeP2mpDestEntry = NULL;
    UINT4               u4Index = TE_ZERO, u4Instance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO, u4EgressId = TE_ZERO;
    UINT4               u4DestId = TE_ZERO;

    TE_DBG (TE_MAIN_ETEXT, "TeGetFirstMplsTeP2mpTunnelDestTable : ENTRY \n");

    if (SNMP_SUCCESS == nmhGetFirstIndexMplsTunnelTable (&u4Index,
                                                         &u4Instance,
                                                         &u4IngressId,
                                                         &u4EgressId))
    {
        do
        {
            MPLS_CMN_LOCK ();

            /*Check whether its a valid tunnel */
            pTeTnlInfo = TeGetTunnelInfo (u4Index,
                                          u4Instance, u4IngressId, u4EgressId);
            if (pTeTnlInfo == NULL)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            /*Check whether its P2MP tunnel and has valid data in the table */
            if (NULL != TE_P2MP_TNL_INFO (pTeTnlInfo))
            {
                pTeP2mpDestEntry =
                    TMO_SLL_First (&(TE_P2MP_TNL_DEST_LIST_INFO (pTeTnlInfo)));
                if (pTeP2mpDestEntry != NULL)
                {
                    MPLS_CMN_UNLOCK ();
                    CONVERT_TO_INTEGER (TE_P2MP_DEST_ADDRESS (pTeP2mpDestEntry),
                                        u4DestId);
                    u4DestId = OSIX_NTOHL (u4DestId);
                    pTeMplsTeP2mpTunnelDestTable->MibObject.
                        i4MplsTeP2mpTunnelDestDestinationLen = ROUTER_ID_LENGTH;
                    MEMCPY (pTeMplsTeP2mpTunnelDestTable->MibObject.
                            au1MplsTeP2mpTunnelDestDestination,
                            &u4DestId, ROUTER_ID_LENGTH);

                    pTeMplsTeP2mpTunnelDestTable->MibObject.
                        i4MplsTeP2mpTunnelDestDestinationType = LSR_ADDR_IPV4;
                    pTeMplsTeP2mpTunnelDestTable->MibObject.
                        u4MplsTeP2mpTunnelDestSubGroupID = TE_ONE;
                    pTeMplsTeP2mpTunnelDestTable->MibObject.
                        i4MplsTeP2mpTunnelDestSubGroupOriginType =
                        LSR_ADDR_IPV4;
                    pTeMplsTeP2mpTunnelDestTable->MibObject.
                        u4MplsTeP2mpTunnelDestSrcSubGroupID = TE_ZERO;
                    pTeMplsTeP2mpTunnelDestTable->MibObject.
                        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
                        LSR_ADDR_IPV4;

                    /*SrcSubGrpOrigin and SubGrpOrigin are ZERO, 
                     * so no need to assign*/
                    pTeMplsTeP2mpTunnelDestTable->MibObject.
                        i4MplsTeP2mpTunnelDestSubGroupOriginLen = TE_ONE;
                    pTeMplsTeP2mpTunnelDestTable->MibObject.
                        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen = TE_ONE;

                    pTeMplsTeP2mpTunnelDestTable->MibObject.
                        u4MplsTunnelIndex = u4Index;
                    pTeMplsTeP2mpTunnelDestTable->MibObject.
                        u4MplsTunnelInstance = u4Instance;
                    pTeMplsTeP2mpTunnelDestTable->MibObject.
                        u4MplsTunnelIngressLSRId = u4IngressId;
                    pTeMplsTeP2mpTunnelDestTable->MibObject.
                        u4MplsTunnelEgressLSRId = u4EgressId;

                    return SNMP_SUCCESS;
                }
            }
            MPLS_CMN_UNLOCK ();
        }
        while (SNMP_SUCCESS == (nmhGetNextIndexMplsTunnelTable (u4Index,
                                                                &u4Index,
                                                                u4Instance,
                                                                &u4Instance,
                                                                u4IngressId,
                                                                &u4IngressId,
                                                                u4EgressId,
                                                                &u4EgressId)));
    }
    TE_DBG (TE_MAIN_ETEXT, "TeGetFirstMplsTeP2mpTunnelDestTable : EXIT \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  TeGetNextMplsTeP2mpTunnelDestTable
 Input       :  The Indices
                pCurrentTeMplsTeP2mpTunnelDestTable
 Output      :  Next index in P2MP destination table 
                pNextTeMplsTeP2mpTunnelDestTable
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT4
TeGetNextMplsTeP2mpTunnelDestTable (tTeMplsTeP2mpTunnelDestTable *
                                    pCurrentTeMplsTeP2mpTunnelDestTable,
                                    tTeMplsTeP2mpTunnelDestTable *
                                    pNextTeMplsTeP2mpTunnelDestTable)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tP2mpDestEntry     *pTeP2mpDestEntry = NULL;
    UINT4               u4Index = TE_ZERO;
    UINT4               u4Instance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    UINT4               u4DestId = TE_ZERO;
    INT1                i1Count = TE_ZERO;
    BOOL1               bFound = TE_FALSE;
    UINT4               u4DestNextId = TE_ZERO;

    TE_DBG (TE_MAIN_ETEXT, "TeGetNextMplsTeP2mpTunnelDestTable : ENTRY \n");

    MEMCPY (&u4DestId, pCurrentTeMplsTeP2mpTunnelDestTable->MibObject.
            au1MplsTeP2mpTunnelDestDestination, ROUTER_ID_LENGTH);

    u4DestId = OSIX_HTONL (u4DestId);

    u4Index = pCurrentTeMplsTeP2mpTunnelDestTable->MibObject.u4MplsTunnelIndex;
    u4Instance = pCurrentTeMplsTeP2mpTunnelDestTable->MibObject.
        u4MplsTunnelInstance;
    u4IngressId = pCurrentTeMplsTeP2mpTunnelDestTable->MibObject.
        u4MplsTunnelIngressLSRId;
    u4EgressId = pCurrentTeMplsTeP2mpTunnelDestTable->MibObject.
        u4MplsTunnelEgressLSRId;

    do
    {
        MPLS_CMN_LOCK ();

        /*Check whether its a valid tunnel */
        pTeTnlInfo = TeGetTunnelInfo (u4Index, u4Instance,
                                      u4IngressId, u4EgressId);
        if (pTeTnlInfo != NULL)
        {
            if ((i1Count == TE_ZERO) &&
                ((NULL == TE_P2MP_TNL_INFO (pTeTnlInfo)) ||
                 (TE_ZERO == TMO_SLL_Count
                  (&(TE_P2MP_TNL_DEST_LIST_INFO (pTeTnlInfo))))))
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
        }
        else
        {
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }

        /*Check whether its P2MP tunnel and has valid data in the table */
        if ((NULL != TE_P2MP_TNL_INFO (pTeTnlInfo)) &&
            (TE_ZERO != TMO_SLL_Count
             (&(TE_P2MP_TNL_DEST_LIST_INFO (pTeTnlInfo)))))
        {
            if (i1Count == TE_ZERO)
            {
                TMO_SLL_Scan (&(TE_P2MP_TNL_DEST_LIST_INFO (pTeTnlInfo)),
                              pTeP2mpDestEntry, tP2mpDestEntry *)
                {
                    CONVERT_TO_INTEGER
                        ((TE_P2MP_DEST_ADDRESS (pTeP2mpDestEntry)),
                         u4DestNextId);
                    if (u4DestId == u4DestNextId)
                    {
                        bFound = TE_TRUE;
                        pTeP2mpDestEntry = TMO_SLL_Next
                            (&(TE_P2MP_TNL_DEST_LIST_INFO (pTeTnlInfo)),
                             &(pTeP2mpDestEntry->NextP2mpDestEntry));

                        if (NULL == pTeP2mpDestEntry)
                        {
                            break;
                        }

                        MPLS_CMN_UNLOCK ();
                        CONVERT_TO_INTEGER (TE_P2MP_DEST_ADDRESS
                                            (pTeP2mpDestEntry), u4DestId);
                        u4DestId = OSIX_NTOHL (u4DestId);
                        pNextTeMplsTeP2mpTunnelDestTable->MibObject.
                            i4MplsTeP2mpTunnelDestDestinationLen =
                            ROUTER_ID_LENGTH;
                        MEMCPY (pNextTeMplsTeP2mpTunnelDestTable->MibObject.
                                au1MplsTeP2mpTunnelDestDestination,
                                &u4DestId, ROUTER_ID_LENGTH);

                        pNextTeMplsTeP2mpTunnelDestTable->MibObject.
                            i4MplsTeP2mpTunnelDestDestinationType =
                            LSR_ADDR_IPV4;
                        pNextTeMplsTeP2mpTunnelDestTable->MibObject.
                            u4MplsTeP2mpTunnelDestSubGroupID = TE_ONE;
                        pNextTeMplsTeP2mpTunnelDestTable->MibObject.
                            i4MplsTeP2mpTunnelDestSubGroupOriginType =
                            LSR_ADDR_IPV4;
                        pNextTeMplsTeP2mpTunnelDestTable->MibObject.
                            u4MplsTeP2mpTunnelDestSrcSubGroupID = TE_ZERO;
                        pNextTeMplsTeP2mpTunnelDestTable->MibObject.
                            i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
                            LSR_ADDR_IPV4;

                        /* SrcSubGrpOrigin and SubGrpOrigin are ZERO, 
                         * so no need to assign*/
                        pNextTeMplsTeP2mpTunnelDestTable->MibObject.
                            i4MplsTeP2mpTunnelDestSubGroupOriginLen = TE_ONE;
                        pNextTeMplsTeP2mpTunnelDestTable->MibObject.
                            i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen = TE_ONE;

                        pNextTeMplsTeP2mpTunnelDestTable->MibObject.
                            u4MplsTunnelIndex = u4Index;
                        pNextTeMplsTeP2mpTunnelDestTable->MibObject.
                            u4MplsTunnelInstance = u4Instance;
                        pNextTeMplsTeP2mpTunnelDestTable->MibObject.
                            u4MplsTunnelIngressLSRId = u4IngressId;
                        pNextTeMplsTeP2mpTunnelDestTable->MibObject.
                            u4MplsTunnelEgressLSRId = u4EgressId;

                        return SNMP_SUCCESS;
                    }
                }
                if (TE_FALSE == bFound)
                {
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
            }
            else
            {
                pTeP2mpDestEntry =
                    TMO_SLL_First (&(TE_P2MP_TNL_DEST_LIST_INFO (pTeTnlInfo)));
                if (pTeP2mpDestEntry != NULL)
                {
                    MPLS_CMN_UNLOCK ();
                    CONVERT_TO_INTEGER (TE_P2MP_DEST_ADDRESS (pTeP2mpDestEntry),
                                        u4DestId);
                    u4DestId = OSIX_NTOHL (u4DestId);
                    pNextTeMplsTeP2mpTunnelDestTable->MibObject.
                        i4MplsTeP2mpTunnelDestDestinationLen = ROUTER_ID_LENGTH;
                    MEMCPY (pNextTeMplsTeP2mpTunnelDestTable->MibObject.
                            au1MplsTeP2mpTunnelDestDestination,
                            &u4DestId, ROUTER_ID_LENGTH);

                    pNextTeMplsTeP2mpTunnelDestTable->MibObject.
                        i4MplsTeP2mpTunnelDestDestinationType = LSR_ADDR_IPV4;
                    pNextTeMplsTeP2mpTunnelDestTable->MibObject.
                        u4MplsTeP2mpTunnelDestSubGroupID = TE_ONE;
                    pNextTeMplsTeP2mpTunnelDestTable->MibObject.
                        i4MplsTeP2mpTunnelDestSubGroupOriginType =
                        LSR_ADDR_IPV4;
                    pNextTeMplsTeP2mpTunnelDestTable->MibObject.
                        u4MplsTeP2mpTunnelDestSrcSubGroupID = TE_ZERO;
                    pNextTeMplsTeP2mpTunnelDestTable->MibObject.
                        i4MplsTeP2mpTunnelDestSrcSubGroupOriginType =
                        LSR_ADDR_IPV4;

                    /*SrcSubGrpOrigin and SubGrpOrigin are ZERO, 
                     * so no need to assign*/
                    pNextTeMplsTeP2mpTunnelDestTable->MibObject.
                        i4MplsTeP2mpTunnelDestSubGroupOriginLen = TE_ONE;
                    pNextTeMplsTeP2mpTunnelDestTable->MibObject.
                        i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen = TE_ONE;

                    pNextTeMplsTeP2mpTunnelDestTable->MibObject.
                        u4MplsTunnelIndex = u4Index;
                    pNextTeMplsTeP2mpTunnelDestTable->MibObject.
                        u4MplsTunnelInstance = u4Instance;
                    pNextTeMplsTeP2mpTunnelDestTable->MibObject.
                        u4MplsTunnelIngressLSRId = u4IngressId;
                    pNextTeMplsTeP2mpTunnelDestTable->MibObject.
                        u4MplsTunnelEgressLSRId = u4EgressId;

                    return SNMP_SUCCESS;
                }
            }
            i1Count++;
        }
        MPLS_CMN_UNLOCK ();
    }
    while (SNMP_SUCCESS == (nmhGetNextIndexMplsTunnelTable (u4Index,
                                                            &u4Index,
                                                            u4Instance,
                                                            &u4Instance,
                                                            u4IngressId,
                                                            &u4IngressId,
                                                            u4EgressId,
                                                            &u4EgressId)));
    TE_DBG (TE_MAIN_ETEXT, "TeGetNextMplsTeP2mpTunnelDestTable : EXIT \n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  TeGetFirstMplsTeP2mpTunnelBranchPerfTable
 Input       :  The Indices
                NONE
 Output      :  First index in branch performance table 
                pTeMplsTeP2mpTunnelBranchPerfTable
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT4
TeGetFirstMplsTeP2mpTunnelBranchPerfTable (tTeMplsTeP2mpTunnelBranchPerfTable *
                                           pTeMplsTeP2mpTunnelBranchPerfTable)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tP2mpBranchEntry   *pTeP2mpBranchEntry = NULL;
    UINT4               u4Index = TE_ZERO;
    UINT4               u4Instance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    UINT4               u4OutIndex = TE_ZERO;

    TE_DBG (TE_MAIN_ETEXT, "TeGetFirstMplsTeP2mpTunnelBranchPerfTable : "
            "ENTRY\n");

    if (SNMP_SUCCESS == nmhGetFirstIndexMplsTunnelTable (&u4Index,
                                                         &u4Instance,
                                                         &u4IngressId,
                                                         &u4EgressId))
    {
        do
        {
            MPLS_CMN_LOCK ();

            /*Check whether its a valid tunnel */
            pTeTnlInfo = TeGetTunnelInfo (u4Index,
                                          u4Instance, u4IngressId, u4EgressId);
            if (pTeTnlInfo == NULL)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            /*Check whether its P2MP tunnel and has valid data in the table */
            if (NULL != TE_P2MP_TNL_INFO (pTeTnlInfo))
            {
                pTeP2mpBranchEntry =
                    TMO_SLL_First (&
                                   (TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo)));
                if (pTeP2mpBranchEntry != NULL)
                {
                    MPLS_CMN_UNLOCK ();
                    pTeMplsTeP2mpTunnelBranchPerfTable->MibObject.
                        u4MplsTunnelIndex = u4Index;
                    pTeMplsTeP2mpTunnelBranchPerfTable->MibObject.
                        u4MplsTunnelInstance = u4Instance;
                    pTeMplsTeP2mpTunnelBranchPerfTable->MibObject.
                        u4MplsTunnelIngressLSRId = u4IngressId;
                    pTeMplsTeP2mpTunnelBranchPerfTable->MibObject.
                        u4MplsTunnelEgressLSRId = u4EgressId;

                    pTeMplsTeP2mpTunnelBranchPerfTable->MibObject.
                        i4MplsTeP2mpTunnelBranchPerfBranchLen =
                        MPLS_INDEX_LENGTH;
                    u4OutIndex =
                        TE_HTONL (TE_P2MP_BRANCH_OUTSEG_INDEX
                                  (pTeP2mpBranchEntry));
                    MEMCPY (pTeMplsTeP2mpTunnelBranchPerfTable->MibObject.
                            au1MplsTeP2mpTunnelBranchPerfBranch, &u4OutIndex,
                            MPLS_INDEX_LENGTH);
                    return SNMP_SUCCESS;
                }
            }
            MPLS_CMN_UNLOCK ();
        }
        while (SNMP_SUCCESS == (nmhGetNextIndexMplsTunnelTable (u4Index,
                                                                &u4Index,
                                                                u4Instance,
                                                                &u4Instance,
                                                                u4IngressId,
                                                                &u4IngressId,
                                                                u4EgressId,
                                                                &u4EgressId)));
    }
    TE_DBG (TE_MAIN_ETEXT, "TeGetFirstMplsTeP2mpTunnelBranchPerfTable : "
            "EXIT\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  TeGetNextMplsTeP2mpTunnelBranchPerfTable
 Input       :  The Indices
                pCurrentTeMplsTeP2mpTunnelBranchPerfTable
 Output      :  Next index in branch performance table 
                pNextTeMplsTeP2mpTunnelBranchPerfTable
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT4
TeGetNextMplsTeP2mpTunnelBranchPerfTable (tTeMplsTeP2mpTunnelBranchPerfTable *
                                          pCurrentTeMplsTeP2mpTunnelBranchPerfTable,
                                          tTeMplsTeP2mpTunnelBranchPerfTable *
                                          pNextTeMplsTeP2mpTunnelBranchPerfTable)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tP2mpBranchEntry   *pTeP2mpBranchEntry = NULL;
    UINT4               u4Index = TE_ZERO;
    UINT4               u4Instance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    UINT4               u4BranchOutIndex = TE_ZERO;
    INT1                i1Count = TE_ZERO;
    BOOL1               bFound = TE_FALSE;
    UINT4               u4OutIndex = TE_ZERO;

    TE_DBG (TE_MAIN_ETEXT, "TeGetNextMplsTeP2mpTunnelBranchPerfTable : "
            "ENTRY\n");

    MEMCPY (&u4BranchOutIndex,
            pCurrentTeMplsTeP2mpTunnelBranchPerfTable->MibObject.
            au1MplsTeP2mpTunnelBranchPerfBranch, MPLS_INDEX_LENGTH);
    u4BranchOutIndex = TE_NTOHL (u4BranchOutIndex);

    u4Index = pCurrentTeMplsTeP2mpTunnelBranchPerfTable->MibObject.
        u4MplsTunnelIndex;
    u4Instance = pCurrentTeMplsTeP2mpTunnelBranchPerfTable->MibObject.
        u4MplsTunnelInstance;
    u4IngressId = pCurrentTeMplsTeP2mpTunnelBranchPerfTable->MibObject.
        u4MplsTunnelIngressLSRId;
    u4EgressId = pCurrentTeMplsTeP2mpTunnelBranchPerfTable->MibObject.
        u4MplsTunnelEgressLSRId;

    do
    {
        MPLS_CMN_LOCK ();

        /*Check whether its a valid tunnel */
        pTeTnlInfo = TeGetTunnelInfo (u4Index,
                                      u4Instance, u4IngressId, u4EgressId);
        if (pTeTnlInfo != NULL)
        {
            if ((i1Count == TE_ZERO) &&
                ((NULL == TE_P2MP_TNL_INFO (pTeTnlInfo)) ||
                 (TE_ZERO == TMO_SLL_Count
                  (&(TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo))))))
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
        }
        else
        {
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }

        /*Check whether its P2MP tunnel and has valid data in the table */
        if ((NULL != TE_P2MP_TNL_INFO (pTeTnlInfo)) &&
            (TE_ZERO != TMO_SLL_Count
             (&(TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo)))))
        {
            if (i1Count == TE_ZERO)
            {
                TMO_SLL_Scan (&(TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo)),
                              pTeP2mpBranchEntry, tP2mpBranchEntry *)
                {
                    if (u4BranchOutIndex == TE_P2MP_BRANCH_OUTSEG_INDEX
                        (pTeP2mpBranchEntry))
                    {
                        bFound = TE_TRUE;
                        pTeP2mpBranchEntry = TMO_SLL_Next
                            (&(TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo)),
                             &(pTeP2mpBranchEntry->NextP2mpBranchEntry));

                        if (pTeP2mpBranchEntry == NULL)
                        {
                            break;
                        }

                        MPLS_CMN_UNLOCK ();
                        pNextTeMplsTeP2mpTunnelBranchPerfTable->MibObject.
                            u4MplsTunnelIndex = u4Index;

                        pNextTeMplsTeP2mpTunnelBranchPerfTable->MibObject.
                            u4MplsTunnelInstance = u4Instance;

                        pNextTeMplsTeP2mpTunnelBranchPerfTable->MibObject.
                            u4MplsTunnelIngressLSRId = u4IngressId;

                        pNextTeMplsTeP2mpTunnelBranchPerfTable->MibObject.
                            u4MplsTunnelEgressLSRId = u4EgressId;

                        pNextTeMplsTeP2mpTunnelBranchPerfTable->MibObject.
                            i4MplsTeP2mpTunnelBranchPerfBranchLen
                            = MPLS_INDEX_LENGTH;
                        u4OutIndex = TE_HTONL (TE_P2MP_BRANCH_OUTSEG_INDEX
                                               (pTeP2mpBranchEntry));
                        MEMCPY (pNextTeMplsTeP2mpTunnelBranchPerfTable->
                                MibObject.au1MplsTeP2mpTunnelBranchPerfBranch,
                                &u4OutIndex, MPLS_INDEX_LENGTH);
                        return SNMP_SUCCESS;
                    }
                }
                if (TE_FALSE == bFound)
                {
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
            }
            else
            {
                pTeP2mpBranchEntry = TMO_SLL_First
                    (&(TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo)));
                if (pTeP2mpBranchEntry != NULL)
                {
                    MPLS_CMN_UNLOCK ();
                    pNextTeMplsTeP2mpTunnelBranchPerfTable->MibObject.
                        u4MplsTunnelIndex = u4Index;

                    pNextTeMplsTeP2mpTunnelBranchPerfTable->MibObject.
                        u4MplsTunnelInstance = u4Instance;

                    pNextTeMplsTeP2mpTunnelBranchPerfTable->MibObject.
                        u4MplsTunnelIngressLSRId = u4IngressId;

                    pNextTeMplsTeP2mpTunnelBranchPerfTable->MibObject.
                        u4MplsTunnelEgressLSRId = u4EgressId;

                    pNextTeMplsTeP2mpTunnelBranchPerfTable->MibObject.
                        i4MplsTeP2mpTunnelBranchPerfBranchLen
                        = MPLS_INDEX_LENGTH;
                    u4OutIndex = TE_HTONL (TE_P2MP_BRANCH_OUTSEG_INDEX
                                           (pTeP2mpBranchEntry));
                    MEMCPY (pNextTeMplsTeP2mpTunnelBranchPerfTable->MibObject.
                            au1MplsTeP2mpTunnelBranchPerfBranch, &u4OutIndex,
                            MPLS_INDEX_LENGTH);
                    return SNMP_SUCCESS;
                }
            }
            i1Count++;
        }
        MPLS_CMN_UNLOCK ();
    }
    while (SNMP_SUCCESS == (nmhGetNextIndexMplsTunnelTable (u4Index,
                                                            &u4Index,
                                                            u4Instance,
                                                            &u4Instance,
                                                            u4IngressId,
                                                            &u4IngressId,
                                                            u4EgressId,
                                                            &u4EgressId)));
    TE_DBG (TE_MAIN_ETEXT, "TeGetNextMplsTeP2mpTunnelBranchPerfTable : EXIT\n");
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  TeGetMplsTeP2mpTunnelNotificationEnable
 Input       :  The Indices
                pi4MplsTeP2mpTunnelNotificationEnable
 Output      :  This Routine will Get 
                the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT4
TeGetMplsTeP2mpTunnelNotificationEnable (INT4
                                         *pi4MplsTeP2mpTunnelNotificationEnable)
{
    TE_DBG (TE_MAIN_ETEXT, "TeGetMplsTeP2mpTunnelNotificationEnable : ENTRY\n");
    *pi4MplsTeP2mpTunnelNotificationEnable =
        TE_P2MP_TNL_NOTIFICATION (gTeGblInfo);
    TE_DBG (TE_MAIN_ETEXT, "TeGetMplsTeP2mpTunnelNotificationEnable : EXIT\n");
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TeSetMplsTeP2mpTunnelNotificationEnable
 Input       :  The Indices
                i4MplsTeP2mpTunnelNotificationEnable
 Output      :  This Routine will Set 
                the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT4
TeSetMplsTeP2mpTunnelNotificationEnable (INT4
                                         i4MplsTeP2mpTunnelNotificationEnable)
{
    TE_DBG (TE_MAIN_ETEXT, "TeSetMplsTeP2mpTunnelNotificationEnable : ENTRY\n");
    if (TE_SNMP_TRUE == i4MplsTeP2mpTunnelNotificationEnable)
    {
        TE_P2MP_TNL_NOTIFICATION (gTeGblInfo) = TE_SNMP_TRUE;
        return SNMP_SUCCESS;
    }
    else if (TE_SNMP_FALSE == i4MplsTeP2mpTunnelNotificationEnable)
    {
        TE_P2MP_TNL_NOTIFICATION (gTeGblInfo) = TE_SNMP_FALSE;
        return SNMP_SUCCESS;
    }
    TE_DBG (TE_MAIN_ETEXT, "TeSetMplsTeP2mpTunnelNotificationEnable : EXIT\n");
    return SNMP_FAILURE;
}
