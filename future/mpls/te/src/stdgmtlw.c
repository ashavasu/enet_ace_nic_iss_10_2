/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdgmtlw.c,v 1.9 2014/11/17 12:00:09 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include "teincs.h"
#include "stdtelw.h"
#include "stdgmtlw.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetGmplsTunnelsConfigured
 Input       :  The Indices

                The Object 
                retValGmplsTunnelsConfigured
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelsConfigured (UINT4 *pu4RetValGmplsTunnelsConfigured)
{
    tTeTnlInfo         *pTmpTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    if (gTeGblInfo.TeParams.u1TeAdminStatus == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pTmpTeTnlInfo = RBTreeGetFirst (gTeGblInfo.TnlInfoTbl);

    while (pTmpTeTnlInfo != NULL)
    {
        if ((pTmpTeTnlInfo->GmplsTnlInfo.u1EncodingType !=
             GMPLS_TUNNEL_LSP_NOT_GMPLS) &&
            (pTmpTeTnlInfo->u1TnlRowStatus == ACTIVE))
        {
            (*pu4RetValGmplsTunnelsConfigured)++;
        }
        pTmpTeTnlInfo = RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                       (tRBElem *) pTmpTeTnlInfo, NULL);
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelsActive
 Input       :  The Indices

                The Object 
                retValGmplsTunnelsActive
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelsActive (UINT4 *pu4RetValGmplsTunnelsActive)
{
    tTeTnlInfo         *pTmpTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    if (gTeGblInfo.TeParams.u1TeAdminStatus == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pTmpTeTnlInfo = RBTreeGetFirst (gTeGblInfo.TnlInfoTbl);

    while (pTmpTeTnlInfo != NULL)
    {
        if ((pTmpTeTnlInfo->GmplsTnlInfo.u1EncodingType !=
             GMPLS_TUNNEL_LSP_NOT_GMPLS) &&
            (pTmpTeTnlInfo->u1TnlOperStatus == TE_OPER_UP))
        {
            (*pu4RetValGmplsTunnelsActive)++;
        }
        pTmpTeTnlInfo = RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                       (tRBElem *) pTmpTeTnlInfo, NULL);
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : GmplsTunnelTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceGmplsTunnelTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */
INT1
nmhValidateIndexInstanceGmplsTunnelTable (UINT4 u4MplsTunnelIndex,
                                          UINT4 u4MplsTunnelInstance,
                                          UINT4 u4MplsTunnelIngressLSRId,
                                          UINT4 u4MplsTunnelEgressLSRId)
{
    return (nmhValidateIndexInstanceMplsTunnelTable (u4MplsTunnelIndex,
                                                     u4MplsTunnelInstance,
                                                     u4MplsTunnelIngressLSRId,
                                                     u4MplsTunnelEgressLSRId));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexGmplsTunnelTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */
INT1
nmhGetFirstIndexGmplsTunnelTable (UINT4 *pu4MplsTunnelIndex,
                                  UINT4 *pu4MplsTunnelInstance,
                                  UINT4 *pu4MplsTunnelIngressLSRId,
                                  UINT4 *pu4MplsTunnelEgressLSRId)
{
    return (nmhGetFirstIndexMplsTunnelTable (pu4MplsTunnelIndex,
                                             pu4MplsTunnelInstance,
                                             pu4MplsTunnelIngressLSRId,
                                             pu4MplsTunnelEgressLSRId));
}

/****************************************************************************
 Function    :  nmhGetNextIndexGmplsTunnelTable
 Input       :  The Indices
                MplsTunnelIndex
                nextMplsTunnelIndex
                MplsTunnelInstance
                nextMplsTunnelInstance
                MplsTunnelIngressLSRId
                nextMplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                nextMplsTunnelEgressLSRId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexGmplsTunnelTable (UINT4 u4MplsTunnelIndex,
                                 UINT4 *pu4NextMplsTunnelIndex,
                                 UINT4 u4MplsTunnelInstance,
                                 UINT4 *pu4NextMplsTunnelInstance,
                                 UINT4 u4MplsTunnelIngressLSRId,
                                 UINT4 *pu4NextMplsTunnelIngressLSRId,
                                 UINT4 u4MplsTunnelEgressLSRId,
                                 UINT4 *pu4NextMplsTunnelEgressLSRId)
{
    return (nmhGetNextIndexMplsTunnelTable (u4MplsTunnelIndex,
                                            pu4NextMplsTunnelIndex,
                                            u4MplsTunnelInstance,
                                            pu4NextMplsTunnelInstance,
                                            u4MplsTunnelIngressLSRId,
                                            pu4NextMplsTunnelIngressLSRId,
                                            u4MplsTunnelEgressLSRId,
                                            pu4NextMplsTunnelEgressLSRId));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetGmplsTunnelUnnumIf
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValGmplsTunnelUnnumIf
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelUnnumIf (UINT4 u4MplsTunnelIndex,
                          UINT4 u4MplsTunnelInstance,
                          UINT4 u4MplsTunnelIngressLSRId,
                          UINT4 u4MplsTunnelEgressLSRId,
                          INT4 *pi4RetValGmplsTunnelUnnumIf)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllGmplsTunnelTable (u4MplsTunnelIndex,
                                         u4MplsTunnelInstance,
                                         u4MplsTunnelIngressLSRId,
                                         u4MplsTunnelEgressLSRId,
                                         &GmplsTnlInfo);

    MPLS_CMN_UNLOCK ();

    *pi4RetValGmplsTunnelUnnumIf = (INT4) GmplsTnlInfo.bUnnumIf;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelAttributes
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValGmplsTunnelAttributes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelAttributes (UINT4 u4MplsTunnelIndex,
                             UINT4 u4MplsTunnelInstance,
                             UINT4 u4MplsTunnelIngressLSRId,
                             UINT4 u4MplsTunnelEgressLSRId,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValGmplsTunnelAttributes)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllGmplsTunnelTable (u4MplsTunnelIndex,
                                         u4MplsTunnelInstance,
                                         u4MplsTunnelIngressLSRId,
                                         u4MplsTunnelEgressLSRId,
                                         &GmplsTnlInfo);

    MPLS_CMN_UNLOCK ();

    pRetValGmplsTunnelAttributes->pu1_OctetList[TE_ZERO] =
        GmplsTnlInfo.u1Attributes;

    pRetValGmplsTunnelAttributes->i4_Length = GmplsTnlInfo.u1AttrLen;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelLSPEncoding
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValGmplsTunnelLSPEncoding
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelLSPEncoding (UINT4 u4MplsTunnelIndex,
                              UINT4 u4MplsTunnelInstance,
                              UINT4 u4MplsTunnelIngressLSRId,
                              UINT4 u4MplsTunnelEgressLSRId,
                              INT4 *pi4RetValGmplsTunnelLSPEncoding)
{

    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllGmplsTunnelTable (u4MplsTunnelIndex,
                                         u4MplsTunnelInstance,
                                         u4MplsTunnelIngressLSRId,
                                         u4MplsTunnelEgressLSRId,
                                         &GmplsTnlInfo);

    MPLS_CMN_UNLOCK ();

    *pi4RetValGmplsTunnelLSPEncoding = (INT4) GmplsTnlInfo.u1EncodingType;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelSwitchingType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValGmplsTunnelSwitchingType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelSwitchingType (UINT4 u4MplsTunnelIndex,
                                UINT4 u4MplsTunnelInstance,
                                UINT4 u4MplsTunnelIngressLSRId,
                                UINT4 u4MplsTunnelEgressLSRId,
                                INT4 *pi4RetValGmplsTunnelSwitchingType)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllGmplsTunnelTable (u4MplsTunnelIndex,
                                         u4MplsTunnelInstance,
                                         u4MplsTunnelIngressLSRId,
                                         u4MplsTunnelEgressLSRId,
                                         &GmplsTnlInfo);

    MPLS_CMN_UNLOCK ();

    *pi4RetValGmplsTunnelSwitchingType = (INT4) GmplsTnlInfo.u1SwitchingType;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelLinkProtection
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValGmplsTunnelLinkProtection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelLinkProtection (UINT4 u4MplsTunnelIndex,
                                 UINT4 u4MplsTunnelInstance,
                                 UINT4 u4MplsTunnelIngressLSRId,
                                 UINT4 u4MplsTunnelEgressLSRId,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pRetValGmplsTunnelLinkProtection)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllGmplsTunnelTable (u4MplsTunnelIndex,
                                         u4MplsTunnelInstance,
                                         u4MplsTunnelIngressLSRId,
                                         u4MplsTunnelEgressLSRId,
                                         &GmplsTnlInfo);

    MPLS_CMN_UNLOCK ();

    pRetValGmplsTunnelLinkProtection->pu1_OctetList[TE_ZERO] =
        GmplsTnlInfo.u1LinkProtection;

    pRetValGmplsTunnelLinkProtection->i4_Length
        = (INT4) GmplsTnlInfo.u1LinkProtLen;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelGPid
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValGmplsTunnelGPid
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelGPid (UINT4 u4MplsTunnelIndex, UINT4 u4MplsTunnelInstance,
                       UINT4 u4MplsTunnelIngressLSRId,
                       UINT4 u4MplsTunnelEgressLSRId,
                       INT4 *pi4RetValGmplsTunnelGPid)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllGmplsTunnelTable (u4MplsTunnelIndex,
                                         u4MplsTunnelInstance,
                                         u4MplsTunnelIngressLSRId,
                                         u4MplsTunnelEgressLSRId,
                                         &GmplsTnlInfo);

    MPLS_CMN_UNLOCK ();

    *pi4RetValGmplsTunnelGPid = (INT4) GmplsTnlInfo.u2Gpid;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelSecondary
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValGmplsTunnelSecondary
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelSecondary (UINT4 u4MplsTunnelIndex, UINT4 u4MplsTunnelInstance,
                            UINT4 u4MplsTunnelIngressLSRId,
                            UINT4 u4MplsTunnelEgressLSRId,
                            INT4 *pi4RetValGmplsTunnelSecondary)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllGmplsTunnelTable (u4MplsTunnelIndex,
                                         u4MplsTunnelInstance,
                                         u4MplsTunnelIngressLSRId,
                                         u4MplsTunnelEgressLSRId,
                                         &GmplsTnlInfo);

    MPLS_CMN_UNLOCK ();

    *pi4RetValGmplsTunnelSecondary = (INT4) GmplsTnlInfo.u1Secondary;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelDirection
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValGmplsTunnelDirection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelDirection (UINT4 u4MplsTunnelIndex, UINT4 u4MplsTunnelInstance,
                            UINT4 u4MplsTunnelIngressLSRId,
                            UINT4 u4MplsTunnelEgressLSRId,
                            INT4 *pi4RetValGmplsTunnelDirection)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllGmplsTunnelTable (u4MplsTunnelIndex,
                                         u4MplsTunnelInstance,
                                         u4MplsTunnelIngressLSRId,
                                         u4MplsTunnelEgressLSRId,
                                         &GmplsTnlInfo);

    MPLS_CMN_UNLOCK ();

    *pi4RetValGmplsTunnelDirection = (INT4) GmplsTnlInfo.u1Direction;

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelPathComp
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValGmplsTunnelPathComp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelPathComp (UINT4 u4MplsTunnelIndex, UINT4 u4MplsTunnelInstance,
                           UINT4 u4MplsTunnelIngressLSRId,
                           UINT4 u4MplsTunnelEgressLSRId,
                           INT4 *pi4RetValGmplsTunnelPathComp)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllGmplsTunnelTable (u4MplsTunnelIndex,
                                         u4MplsTunnelInstance,
                                         u4MplsTunnelIngressLSRId,
                                         u4MplsTunnelEgressLSRId,
                                         &GmplsTnlInfo);

    MPLS_CMN_UNLOCK ();

    *pi4RetValGmplsTunnelPathComp = (INT4) GmplsTnlInfo.PathComp;

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelUpstreamNotifyRecipientType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValGmplsTunnelUpstreamNotifyRecipientType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelUpstreamNotifyRecipientType (UINT4 u4MplsTunnelIndex,
                                              UINT4 u4MplsTunnelInstance,
                                              UINT4 u4MplsTunnelIngressLSRId,
                                              UINT4 u4MplsTunnelEgressLSRId,
                                              INT4
                                              *pi4RetValGmplsTunnelUpstreamNotifyRecipientType)
{
    UNUSED_PARAM (u4MplsTunnelIndex);
    UNUSED_PARAM (u4MplsTunnelInstance);
    UNUSED_PARAM (u4MplsTunnelIngressLSRId);
    UNUSED_PARAM (u4MplsTunnelEgressLSRId);
    UNUSED_PARAM (pi4RetValGmplsTunnelUpstreamNotifyRecipientType);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelUpstreamNotifyRecipient
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValGmplsTunnelUpstreamNotifyRecipient
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelUpstreamNotifyRecipient (UINT4 u4MplsTunnelIndex,
                                          UINT4 u4MplsTunnelInstance,
                                          UINT4 u4MplsTunnelIngressLSRId,
                                          UINT4 u4MplsTunnelEgressLSRId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pRetValGmplsTunnelUpstreamNotifyRecipient)
{
    UNUSED_PARAM (u4MplsTunnelIndex);
    UNUSED_PARAM (u4MplsTunnelInstance);
    UNUSED_PARAM (u4MplsTunnelIngressLSRId);
    UNUSED_PARAM (u4MplsTunnelEgressLSRId);
    UNUSED_PARAM (pRetValGmplsTunnelUpstreamNotifyRecipient);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelSendResvNotifyRecipientType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValGmplsTunnelSendResvNotifyRecipientType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelSendResvNotifyRecipientType (UINT4 u4MplsTunnelIndex,
                                              UINT4 u4MplsTunnelInstance,
                                              UINT4 u4MplsTunnelIngressLSRId,
                                              UINT4 u4MplsTunnelEgressLSRId,
                                              INT4
                                              *pi4RetValGmplsTunnelSendResvNotifyRecipientType)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllGmplsTunnelTable (u4MplsTunnelIndex,
                                         u4MplsTunnelInstance,
                                         u4MplsTunnelIngressLSRId,
                                         u4MplsTunnelEgressLSRId,
                                         &GmplsTnlInfo);

    MPLS_CMN_UNLOCK ();

    *pi4RetValGmplsTunnelSendResvNotifyRecipientType =
        GmplsTnlInfo.SendResvNotifyRecipientType;
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelSendResvNotifyRecipient
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValGmplsTunnelSendResvNotifyRecipient
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelSendResvNotifyRecipient (UINT4 u4MplsTunnelIndex,
                                          UINT4 u4MplsTunnelInstance,
                                          UINT4 u4MplsTunnelIngressLSRId,
                                          UINT4 u4MplsTunnelEgressLSRId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pRetValGmplsTunnelSendResvNotifyRecipient)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllGmplsTunnelTable (u4MplsTunnelIndex,
                                         u4MplsTunnelInstance,
                                         u4MplsTunnelIngressLSRId,
                                         u4MplsTunnelEgressLSRId,
                                         &GmplsTnlInfo);

    MPLS_CMN_UNLOCK ();

    MPLS_INTEGER_TO_OCTETSTRING (GmplsTnlInfo.u4SendResvNotifyRecipient,
                                 (pRetValGmplsTunnelSendResvNotifyRecipient));

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelDownstreamNotifyRecipientType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValGmplsTunnelDownstreamNotifyRecipientType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelDownstreamNotifyRecipientType (UINT4 u4MplsTunnelIndex,
                                                UINT4 u4MplsTunnelInstance,
                                                UINT4 u4MplsTunnelIngressLSRId,
                                                UINT4 u4MplsTunnelEgressLSRId,
                                                INT4
                                                *pi4RetValGmplsTunnelDownstreamNotifyRecipientType)
{
    UNUSED_PARAM (u4MplsTunnelIndex);
    UNUSED_PARAM (u4MplsTunnelInstance);
    UNUSED_PARAM (u4MplsTunnelIngressLSRId);
    UNUSED_PARAM (u4MplsTunnelEgressLSRId);
    UNUSED_PARAM (pi4RetValGmplsTunnelDownstreamNotifyRecipientType);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelDownstreamNotifyRecipient
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValGmplsTunnelDownstreamNotifyRecipient
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelDownstreamNotifyRecipient (UINT4 u4MplsTunnelIndex,
                                            UINT4 u4MplsTunnelInstance,
                                            UINT4 u4MplsTunnelIngressLSRId,
                                            UINT4 u4MplsTunnelEgressLSRId,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pRetValGmplsTunnelDownstreamNotifyRecipient)
{
    UNUSED_PARAM (u4MplsTunnelIndex);
    UNUSED_PARAM (u4MplsTunnelInstance);
    UNUSED_PARAM (u4MplsTunnelIngressLSRId);
    UNUSED_PARAM (u4MplsTunnelEgressLSRId);
    UNUSED_PARAM (pRetValGmplsTunnelDownstreamNotifyRecipient);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelSendPathNotifyRecipientType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValGmplsTunnelSendPathNotifyRecipientType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelSendPathNotifyRecipientType (UINT4 u4MplsTunnelIndex,
                                              UINT4 u4MplsTunnelInstance,
                                              UINT4 u4MplsTunnelIngressLSRId,
                                              UINT4 u4MplsTunnelEgressLSRId,
                                              INT4
                                              *pi4RetValGmplsTunnelSendPathNotifyRecipientType)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllGmplsTunnelTable (u4MplsTunnelIndex,
                                         u4MplsTunnelInstance,
                                         u4MplsTunnelIngressLSRId,
                                         u4MplsTunnelEgressLSRId,
                                         &GmplsTnlInfo);

    MPLS_CMN_UNLOCK ();

    *pi4RetValGmplsTunnelSendPathNotifyRecipientType =
        GmplsTnlInfo.SendPathNotifyRecipientType;
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelSendPathNotifyRecipient
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValGmplsTunnelSendPathNotifyRecipient
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelSendPathNotifyRecipient (UINT4 u4MplsTunnelIndex,
                                          UINT4 u4MplsTunnelInstance,
                                          UINT4 u4MplsTunnelIngressLSRId,
                                          UINT4 u4MplsTunnelEgressLSRId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pRetValGmplsTunnelSendPathNotifyRecipient)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllGmplsTunnelTable (u4MplsTunnelIndex,
                                         u4MplsTunnelInstance,
                                         u4MplsTunnelIngressLSRId,
                                         u4MplsTunnelEgressLSRId,
                                         &GmplsTnlInfo);

    MPLS_CMN_UNLOCK ();

    MPLS_INTEGER_TO_OCTETSTRING (GmplsTnlInfo.u4SendPathNotifyRecipient,
                                 (pRetValGmplsTunnelSendPathNotifyRecipient));

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelAdminStatusFlags
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValGmplsTunnelAdminStatusFlags
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelAdminStatusFlags (UINT4 u4MplsTunnelIndex,
                                   UINT4 u4MplsTunnelInstance,
                                   UINT4 u4MplsTunnelIngressLSRId,
                                   UINT4 u4MplsTunnelEgressLSRId,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValGmplsTunnelAdminStatusFlags)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    UINT4               u4LspAdminStatus = 0;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllGmplsTunnelTable (u4MplsTunnelIndex,
                                         u4MplsTunnelInstance,
                                         u4MplsTunnelIngressLSRId,
                                         u4MplsTunnelEgressLSRId,
                                         &GmplsTnlInfo);

    MPLS_CMN_UNLOCK ();

    u4LspAdminStatus = (UINT4) GmplsTnlInfo.u4AdminStatus;

    MPLS_INTEGER_TO_OCTETSTRING (u4LspAdminStatus,
                                 (pRetValGmplsTunnelAdminStatusFlags));
    pRetValGmplsTunnelAdminStatusFlags->i4_Length
        = (INT4) GmplsTnlInfo.u1AdminStatusLen;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelExtraParamsPtr
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValGmplsTunnelExtraParamsPtr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelExtraParamsPtr (UINT4 u4MplsTunnelIndex,
                                 UINT4 u4MplsTunnelInstance,
                                 UINT4 u4MplsTunnelIngressLSRId,
                                 UINT4 u4MplsTunnelEgressLSRId,
                                 tSNMP_OID_TYPE *
                                 pRetValGmplsTunnelExtraParamsPtr)
{
    UNUSED_PARAM (u4MplsTunnelIndex);
    UNUSED_PARAM (u4MplsTunnelInstance);
    UNUSED_PARAM (u4MplsTunnelIngressLSRId);
    UNUSED_PARAM (u4MplsTunnelEgressLSRId);
    UNUSED_PARAM (pRetValGmplsTunnelExtraParamsPtr);

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetGmplsTunnelUnnumIf
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValGmplsTunnelUnnumIf
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetGmplsTunnelUnnumIf (UINT4 u4MplsTunnelIndex, UINT4 u4MplsTunnelInstance,
                          UINT4 u4MplsTunnelIngressLSRId,
                          UINT4 u4MplsTunnelEgressLSRId,
                          INT4 i4SetValGmplsTunnelUnnumIf)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    GmplsTnlInfo.bUnnumIf = (BOOL1) i4SetValGmplsTunnelUnnumIf;

    MPLS_CMN_LOCK ();

    i1RetVal = TeSetAllGmplsTunnelTable (u4MplsTunnelIndex,
                                         u4MplsTunnelInstance,
                                         u4MplsTunnelIngressLSRId,
                                         u4MplsTunnelEgressLSRId,
                                         (&GmplsTnlInfo),
                                         TE_GMPLS_TUNNEL_UNNUM_IF);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetGmplsTunnelAttributes
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValGmplsTunnelAttributes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetGmplsTunnelAttributes (UINT4 u4MplsTunnelIndex,
                             UINT4 u4MplsTunnelInstance,
                             UINT4 u4MplsTunnelIngressLSRId,
                             UINT4 u4MplsTunnelEgressLSRId,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValGmplsTunnelAttributes)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    GmplsTnlInfo.u1Attributes =
        pSetValGmplsTunnelAttributes->pu1_OctetList[TE_ZERO];
    GmplsTnlInfo.u1AttrLen = (UINT1) pSetValGmplsTunnelAttributes->i4_Length;

    MPLS_CMN_LOCK ();

    i1RetVal =
        TeSetAllGmplsTunnelTable (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId, (&GmplsTnlInfo),
                                  TE_GMPLS_TUNNEL_ATTRIBUTES);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetGmplsTunnelLSPEncoding
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValGmplsTunnelLSPEncoding
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetGmplsTunnelLSPEncoding (UINT4 u4MplsTunnelIndex,
                              UINT4 u4MplsTunnelInstance,
                              UINT4 u4MplsTunnelIngressLSRId,
                              UINT4 u4MplsTunnelEgressLSRId,
                              INT4 i4SetValGmplsTunnelLSPEncoding)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    GmplsTnlInfo.u1EncodingType = (UINT1) i4SetValGmplsTunnelLSPEncoding;

    MPLS_CMN_LOCK ();

    i1RetVal =
        TeSetAllGmplsTunnelTable (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId, (&GmplsTnlInfo),
                                  TE_GMPLS_TUNNEL_ENCODING_TYPE);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetGmplsTunnelSwitchingType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValGmplsTunnelSwitchingType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetGmplsTunnelSwitchingType (UINT4 u4MplsTunnelIndex,
                                UINT4 u4MplsTunnelInstance,
                                UINT4 u4MplsTunnelIngressLSRId,
                                UINT4 u4MplsTunnelEgressLSRId,
                                INT4 i4SetValGmplsTunnelSwitchingType)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    GmplsTnlInfo.u1SwitchingType = (UINT1) i4SetValGmplsTunnelSwitchingType;

    MPLS_CMN_LOCK ();

    i1RetVal =
        TeSetAllGmplsTunnelTable (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId, (&GmplsTnlInfo),
                                  TE_GMPLS_TUNNEL_SWITCHING_TYPE);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetGmplsTunnelLinkProtection
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValGmplsTunnelLinkProtection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetGmplsTunnelLinkProtection (UINT4 u4MplsTunnelIndex,
                                 UINT4 u4MplsTunnelInstance,
                                 UINT4 u4MplsTunnelIngressLSRId,
                                 UINT4 u4MplsTunnelEgressLSRId,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pSetValGmplsTunnelLinkProtection)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    GmplsTnlInfo.u1LinkProtection =
        pSetValGmplsTunnelLinkProtection->pu1_OctetList[TE_ZERO];

    GmplsTnlInfo.u1LinkProtLen
        = (UINT1) pSetValGmplsTunnelLinkProtection->i4_Length;

    MPLS_CMN_LOCK ();

    i1RetVal =
        TeSetAllGmplsTunnelTable (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId, (&GmplsTnlInfo),
                                  TE_GMPLS_TUNNEL_LINK_PROTECTION_TYPE);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetGmplsTunnelGPid
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValGmplsTunnelGPid
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetGmplsTunnelGPid (UINT4 u4MplsTunnelIndex, UINT4 u4MplsTunnelInstance,
                       UINT4 u4MplsTunnelIngressLSRId,
                       UINT4 u4MplsTunnelEgressLSRId,
                       INT4 i4SetValGmplsTunnelGPid)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    GmplsTnlInfo.u2Gpid = (UINT2) i4SetValGmplsTunnelGPid;

    MPLS_CMN_LOCK ();

    i1RetVal =
        TeSetAllGmplsTunnelTable (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId, (&GmplsTnlInfo),
                                  TE_GMPLS_TUNNEL_GPID);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetGmplsTunnelSecondary
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValGmplsTunnelSecondary
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetGmplsTunnelSecondary (UINT4 u4MplsTunnelIndex, UINT4 u4MplsTunnelInstance,
                            UINT4 u4MplsTunnelIngressLSRId,
                            UINT4 u4MplsTunnelEgressLSRId,
                            INT4 i4SetValGmplsTunnelSecondary)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    GmplsTnlInfo.u1Secondary = (UINT1) i4SetValGmplsTunnelSecondary;

    MPLS_CMN_LOCK ();

    i1RetVal =
        TeSetAllGmplsTunnelTable (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId, (&GmplsTnlInfo),
                                  TE_GMPLS_TUNNEL_SECONDARY);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetGmplsTunnelDirection
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValGmplsTunnelDirection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetGmplsTunnelDirection (UINT4 u4MplsTunnelIndex, UINT4 u4MplsTunnelInstance,
                            UINT4 u4MplsTunnelIngressLSRId,
                            UINT4 u4MplsTunnelEgressLSRId,
                            INT4 i4SetValGmplsTunnelDirection)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    GmplsTnlInfo.u1Direction = (UINT1) i4SetValGmplsTunnelDirection;

    MPLS_CMN_LOCK ();

    i1RetVal =
        TeSetAllGmplsTunnelTable (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId, (&GmplsTnlInfo),
                                  TE_GMPLS_TUNNEL_DIRECTION);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetGmplsTunnelPathComp
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValGmplsTunnelPathComp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetGmplsTunnelPathComp (UINT4 u4MplsTunnelIndex, UINT4 u4MplsTunnelInstance,
                           UINT4 u4MplsTunnelIngressLSRId,
                           UINT4 u4MplsTunnelEgressLSRId,
                           INT4 i4SetValGmplsTunnelPathComp)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    GmplsTnlInfo.PathComp = (eGmplsTnlPathComp) i4SetValGmplsTunnelPathComp;

    MPLS_CMN_LOCK ();

    i1RetVal =
        TeSetAllGmplsTunnelTable (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId, (&GmplsTnlInfo),
                                  TE_GMPLS_TUNNEL_PATH_COMP);
    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetGmplsTunnelUpstreamNotifyRecipientType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValGmplsTunnelUpstreamNotifyRecipientType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetGmplsTunnelUpstreamNotifyRecipientType (UINT4 u4MplsTunnelIndex,
                                              UINT4 u4MplsTunnelInstance,
                                              UINT4 u4MplsTunnelIngressLSRId,
                                              UINT4 u4MplsTunnelEgressLSRId,
                                              INT4
                                              i4SetValGmplsTunnelUpstreamNotifyRecipientType)
{
    UNUSED_PARAM (u4MplsTunnelIndex);
    UNUSED_PARAM (u4MplsTunnelInstance);
    UNUSED_PARAM (u4MplsTunnelIngressLSRId);
    UNUSED_PARAM (u4MplsTunnelEgressLSRId);
    UNUSED_PARAM (i4SetValGmplsTunnelUpstreamNotifyRecipientType);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetGmplsTunnelUpstreamNotifyRecipient
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValGmplsTunnelUpstreamNotifyRecipient
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetGmplsTunnelUpstreamNotifyRecipient (UINT4 u4MplsTunnelIndex,
                                          UINT4 u4MplsTunnelInstance,
                                          UINT4 u4MplsTunnelIngressLSRId,
                                          UINT4 u4MplsTunnelEgressLSRId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pSetValGmplsTunnelUpstreamNotifyRecipient)
{
    UNUSED_PARAM (u4MplsTunnelIndex);
    UNUSED_PARAM (u4MplsTunnelInstance);
    UNUSED_PARAM (u4MplsTunnelIngressLSRId);
    UNUSED_PARAM (u4MplsTunnelEgressLSRId);
    UNUSED_PARAM (pSetValGmplsTunnelUpstreamNotifyRecipient);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetGmplsTunnelSendResvNotifyRecipientType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValGmplsTunnelSendResvNotifyRecipientType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetGmplsTunnelSendResvNotifyRecipientType (UINT4 u4MplsTunnelIndex,
                                              UINT4 u4MplsTunnelInstance,
                                              UINT4 u4MplsTunnelIngressLSRId,
                                              UINT4 u4MplsTunnelEgressLSRId,
                                              INT4
                                              i4SetValGmplsTunnelSendResvNotifyRecipientType)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    GmplsTnlInfo.SendResvNotifyRecipientType =
        (eInetAddrType) i4SetValGmplsTunnelSendResvNotifyRecipientType;

    MPLS_CMN_LOCK ();

    i1RetVal =
        TeSetAllGmplsTunnelTable (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId, (&GmplsTnlInfo),
                                  TE_GMPLS_SEND_RESV_NOTIFY_RECEPIENT_TYPE);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetGmplsTunnelSendResvNotifyRecipient
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValGmplsTunnelSendResvNotifyRecipient
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetGmplsTunnelSendResvNotifyRecipient (UINT4 u4MplsTunnelIndex,
                                          UINT4 u4MplsTunnelInstance,
                                          UINT4 u4MplsTunnelIngressLSRId,
                                          UINT4 u4MplsTunnelEgressLSRId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pSetValGmplsTunnelSendResvNotifyRecipient)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    MPLS_OCTETSTRING_TO_INTEGER (pSetValGmplsTunnelSendResvNotifyRecipient,
                                 GmplsTnlInfo.u4SendResvNotifyRecipient);
    MPLS_CMN_LOCK ();

    i1RetVal =
        TeSetAllGmplsTunnelTable (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId, (&GmplsTnlInfo),
                                  TE_GMPLS_SEND_RESV_NOTIFY_RECEPIENT);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetGmplsTunnelDownstreamNotifyRecipientType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValGmplsTunnelDownstreamNotifyRecipientType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetGmplsTunnelDownstreamNotifyRecipientType (UINT4 u4MplsTunnelIndex,
                                                UINT4 u4MplsTunnelInstance,
                                                UINT4 u4MplsTunnelIngressLSRId,
                                                UINT4 u4MplsTunnelEgressLSRId,
                                                INT4
                                                i4SetValGmplsTunnelDownstreamNotifyRecipientType)
{
    UNUSED_PARAM (u4MplsTunnelIndex);
    UNUSED_PARAM (u4MplsTunnelInstance);
    UNUSED_PARAM (u4MplsTunnelIngressLSRId);
    UNUSED_PARAM (u4MplsTunnelEgressLSRId);
    UNUSED_PARAM (i4SetValGmplsTunnelDownstreamNotifyRecipientType);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetGmplsTunnelDownstreamNotifyRecipient
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValGmplsTunnelDownstreamNotifyRecipient
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetGmplsTunnelDownstreamNotifyRecipient (UINT4 u4MplsTunnelIndex,
                                            UINT4 u4MplsTunnelInstance,
                                            UINT4 u4MplsTunnelIngressLSRId,
                                            UINT4 u4MplsTunnelEgressLSRId,
                                            tSNMP_OCTET_STRING_TYPE *
                                            pSetValGmplsTunnelDownstreamNotifyRecipient)
{
    UNUSED_PARAM (u4MplsTunnelIndex);
    UNUSED_PARAM (u4MplsTunnelInstance);
    UNUSED_PARAM (u4MplsTunnelIngressLSRId);
    UNUSED_PARAM (u4MplsTunnelEgressLSRId);
    UNUSED_PARAM (pSetValGmplsTunnelDownstreamNotifyRecipient);
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetGmplsTunnelSendPathNotifyRecipientType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValGmplsTunnelSendPathNotifyRecipientType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetGmplsTunnelSendPathNotifyRecipientType (UINT4 u4MplsTunnelIndex,
                                              UINT4 u4MplsTunnelInstance,
                                              UINT4 u4MplsTunnelIngressLSRId,
                                              UINT4 u4MplsTunnelEgressLSRId,
                                              INT4
                                              i4SetValGmplsTunnelSendPathNotifyRecipientType)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    GmplsTnlInfo.SendPathNotifyRecipientType =
        (eInetAddrType) i4SetValGmplsTunnelSendPathNotifyRecipientType;

    MPLS_CMN_LOCK ();

    i1RetVal =
        TeSetAllGmplsTunnelTable (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId, (&GmplsTnlInfo),
                                  TE_GMPLS_SEND_PATH_NOTIFY_RECEPIENT_TYPE);
    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetGmplsTunnelSendPathNotifyRecipient
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValGmplsTunnelSendPathNotifyRecipient
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetGmplsTunnelSendPathNotifyRecipient (UINT4 u4MplsTunnelIndex,
                                          UINT4 u4MplsTunnelInstance,
                                          UINT4 u4MplsTunnelIngressLSRId,
                                          UINT4 u4MplsTunnelEgressLSRId,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pSetValGmplsTunnelSendPathNotifyRecipient)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    MPLS_OCTETSTRING_TO_INTEGER (pSetValGmplsTunnelSendPathNotifyRecipient,
                                 GmplsTnlInfo.u4SendPathNotifyRecipient);

    MPLS_CMN_LOCK ();

    i1RetVal =
        TeSetAllGmplsTunnelTable (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId, (&GmplsTnlInfo),
                                  TE_GMPLS_SEND_PATH_NOTIFY_RECEPIENT);
    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetGmplsTunnelAdminStatusFlags
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValGmplsTunnelAdminStatusFlags
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetGmplsTunnelAdminStatusFlags (UINT4 u4MplsTunnelIndex,
                                   UINT4 u4MplsTunnelInstance,
                                   UINT4 u4MplsTunnelIngressLSRId,
                                   UINT4 u4MplsTunnelEgressLSRId,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pSetValGmplsTunnelAdminStatusFlags)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = TE_ZERO;
    UINT4               u4AdminStatus = TE_ZERO;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    GmplsTnlInfo.u1AdminStatusLen =
        (UINT1) pSetValGmplsTunnelAdminStatusFlags->i4_Length;

    MPLS_OCTETSTRING_TO_INTEGER (pSetValGmplsTunnelAdminStatusFlags,
                                 u4AdminStatus);

    GmplsTnlInfo.u4AdminStatus = u4AdminStatus;

    MPLS_CMN_LOCK ();

    i1RetVal =
        TeSetAllGmplsTunnelTable (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId, (&GmplsTnlInfo),
                                  TE_GMPLS_TUNNEL_ADMIN_STATUS_FLAGS);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetGmplsTunnelExtraParamsPtr
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValGmplsTunnelExtraParamsPtr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetGmplsTunnelExtraParamsPtr (UINT4 u4MplsTunnelIndex,
                                 UINT4 u4MplsTunnelInstance,
                                 UINT4 u4MplsTunnelIngressLSRId,
                                 UINT4 u4MplsTunnelEgressLSRId,
                                 tSNMP_OID_TYPE *
                                 pSetValGmplsTunnelExtraParamsPtr)
{
    UNUSED_PARAM (u4MplsTunnelIndex);
    UNUSED_PARAM (u4MplsTunnelInstance);
    UNUSED_PARAM (u4MplsTunnelIngressLSRId);
    UNUSED_PARAM (u4MplsTunnelEgressLSRId);
    UNUSED_PARAM (pSetValGmplsTunnelExtraParamsPtr);

    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2GmplsTunnelUnnumIf
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValGmplsTunnelUnnumIf
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2GmplsTunnelUnnumIf (UINT4 *pu4ErrorCode, UINT4 u4MplsTunnelIndex,
                             UINT4 u4MplsTunnelInstance,
                             UINT4 u4MplsTunnelIngressLSRId,
                             UINT4 u4MplsTunnelEgressLSRId,
                             INT4 i4TestValGmplsTunnelUnnumIf)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));
    GmplsTnlInfo.bUnnumIf = (BOOL1) i4TestValGmplsTunnelUnnumIf;

    MPLS_CMN_LOCK ();

    i1RetVal = TeTestAllGmplsTunnelTable (pu4ErrorCode,
                                          u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&GmplsTnlInfo),
                                          TE_GMPLS_TUNNEL_UNNUM_IF);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2GmplsTunnelAttributes
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValGmplsTunnelAttributes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2GmplsTunnelAttributes (UINT4 *pu4ErrorCode, UINT4 u4MplsTunnelIndex,
                                UINT4 u4MplsTunnelInstance,
                                UINT4 u4MplsTunnelIngressLSRId,
                                UINT4 u4MplsTunnelEgressLSRId,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValGmplsTunnelAttributes)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));
    GmplsTnlInfo.u1Attributes =
        (UINT1) pTestValGmplsTunnelAttributes->pu1_OctetList[TE_ZERO];

    GmplsTnlInfo.u1AttrLen = (UINT1) pTestValGmplsTunnelAttributes->i4_Length;

    MPLS_CMN_LOCK ();

    i1RetVal = TeTestAllGmplsTunnelTable (pu4ErrorCode,
                                          u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&GmplsTnlInfo),
                                          TE_GMPLS_TUNNEL_ATTRIBUTES);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2GmplsTunnelLSPEncoding
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValGmplsTunnelLSPEncoding
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2GmplsTunnelLSPEncoding (UINT4 *pu4ErrorCode, UINT4 u4MplsTunnelIndex,
                                 UINT4 u4MplsTunnelInstance,
                                 UINT4 u4MplsTunnelIngressLSRId,
                                 UINT4 u4MplsTunnelEgressLSRId,
                                 INT4 i4TestValGmplsTunnelLSPEncoding)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    GmplsTnlInfo.u1EncodingType = (UINT1) i4TestValGmplsTunnelLSPEncoding;

    MPLS_CMN_LOCK ();

    i1RetVal = TeTestAllGmplsTunnelTable (pu4ErrorCode,
                                          u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&GmplsTnlInfo),
                                          TE_GMPLS_TUNNEL_ENCODING_TYPE);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2GmplsTunnelSwitchingType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValGmplsTunnelSwitchingType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2GmplsTunnelSwitchingType (UINT4 *pu4ErrorCode, UINT4 u4MplsTunnelIndex,
                                   UINT4 u4MplsTunnelInstance,
                                   UINT4 u4MplsTunnelIngressLSRId,
                                   UINT4 u4MplsTunnelEgressLSRId,
                                   INT4 i4TestValGmplsTunnelSwitchingType)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));
    GmplsTnlInfo.u1SwitchingType = (UINT1) i4TestValGmplsTunnelSwitchingType;

    MPLS_CMN_LOCK ();

    i1RetVal = TeTestAllGmplsTunnelTable (pu4ErrorCode,
                                          u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&GmplsTnlInfo),
                                          TE_GMPLS_TUNNEL_SWITCHING_TYPE);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2GmplsTunnelLinkProtection
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValGmplsTunnelLinkProtection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2GmplsTunnelLinkProtection (UINT4 *pu4ErrorCode,
                                    UINT4 u4MplsTunnelIndex,
                                    UINT4 u4MplsTunnelInstance,
                                    UINT4 u4MplsTunnelIngressLSRId,
                                    UINT4 u4MplsTunnelEgressLSRId,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pTestValGmplsTunnelLinkProtection)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));
    GmplsTnlInfo.u1LinkProtection =
        pTestValGmplsTunnelLinkProtection->pu1_OctetList[TE_ZERO];
    GmplsTnlInfo.u1LinkProtLen
        = (UINT1) pTestValGmplsTunnelLinkProtection->i4_Length;

    MPLS_CMN_LOCK ();

    i1RetVal = TeTestAllGmplsTunnelTable (pu4ErrorCode,
                                          u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&GmplsTnlInfo),
                                          TE_GMPLS_TUNNEL_LINK_PROTECTION_TYPE);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2GmplsTunnelGPid
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValGmplsTunnelGPid
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2GmplsTunnelGPid (UINT4 *pu4ErrorCode, UINT4 u4MplsTunnelIndex,
                          UINT4 u4MplsTunnelInstance,
                          UINT4 u4MplsTunnelIngressLSRId,
                          UINT4 u4MplsTunnelEgressLSRId,
                          INT4 i4TestValGmplsTunnelGPid)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    GmplsTnlInfo.u2Gpid = (UINT2) i4TestValGmplsTunnelGPid;

    MPLS_CMN_LOCK ();

    i1RetVal = TeTestAllGmplsTunnelTable (pu4ErrorCode,
                                          u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&GmplsTnlInfo),
                                          TE_GMPLS_TUNNEL_GPID);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2GmplsTunnelSecondary
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValGmplsTunnelSecondary
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2GmplsTunnelSecondary (UINT4 *pu4ErrorCode, UINT4 u4MplsTunnelIndex,
                               UINT4 u4MplsTunnelInstance,
                               UINT4 u4MplsTunnelIngressLSRId,
                               UINT4 u4MplsTunnelEgressLSRId,
                               INT4 i4TestValGmplsTunnelSecondary)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));
    GmplsTnlInfo.u1Secondary = (UINT1) i4TestValGmplsTunnelSecondary;

    MPLS_CMN_LOCK ();

    i1RetVal = TeTestAllGmplsTunnelTable (pu4ErrorCode,
                                          u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&GmplsTnlInfo),
                                          TE_GMPLS_TUNNEL_SECONDARY);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2GmplsTunnelDirection
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValGmplsTunnelDirection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2GmplsTunnelDirection (UINT4 *pu4ErrorCode, UINT4 u4MplsTunnelIndex,
                               UINT4 u4MplsTunnelInstance,
                               UINT4 u4MplsTunnelIngressLSRId,
                               UINT4 u4MplsTunnelEgressLSRId,
                               INT4 i4TestValGmplsTunnelDirection)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));
    GmplsTnlInfo.u1Direction = (UINT1) i4TestValGmplsTunnelDirection;

    MPLS_CMN_LOCK ();

    i1RetVal = TeTestAllGmplsTunnelTable (pu4ErrorCode,
                                          u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&GmplsTnlInfo),
                                          TE_GMPLS_TUNNEL_DIRECTION);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2GmplsTunnelPathComp
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValGmplsTunnelPathComp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2GmplsTunnelPathComp (UINT4 *pu4ErrorCode, UINT4 u4MplsTunnelIndex,
                              UINT4 u4MplsTunnelInstance,
                              UINT4 u4MplsTunnelIngressLSRId,
                              UINT4 u4MplsTunnelEgressLSRId,
                              INT4 i4TestValGmplsTunnelPathComp)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    GmplsTnlInfo.PathComp = (eGmplsTnlPathComp) i4TestValGmplsTunnelPathComp;

    MPLS_CMN_LOCK ();

    i1RetVal = TeTestAllGmplsTunnelTable (pu4ErrorCode,
                                          u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&GmplsTnlInfo),
                                          TE_GMPLS_TUNNEL_PATH_COMP);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2GmplsTunnelUpstreamNotifyRecipientType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValGmplsTunnelUpstreamNotifyRecipientType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2GmplsTunnelUpstreamNotifyRecipientType (UINT4 *pu4ErrorCode,
                                                 UINT4 u4MplsTunnelIndex,
                                                 UINT4 u4MplsTunnelInstance,
                                                 UINT4 u4MplsTunnelIngressLSRId,
                                                 UINT4 u4MplsTunnelEgressLSRId,
                                                 INT4
                                                 i4TestValGmplsTunnelUpstreamNotifyRecipientType)
{
    UNUSED_PARAM (u4MplsTunnelIndex);
    UNUSED_PARAM (u4MplsTunnelInstance);
    UNUSED_PARAM (u4MplsTunnelIngressLSRId);
    UNUSED_PARAM (u4MplsTunnelEgressLSRId);
    UNUSED_PARAM (i4TestValGmplsTunnelUpstreamNotifyRecipientType);

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2GmplsTunnelUpstreamNotifyRecipient
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValGmplsTunnelUpstreamNotifyRecipient
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2GmplsTunnelUpstreamNotifyRecipient (UINT4 *pu4ErrorCode,
                                             UINT4 u4MplsTunnelIndex,
                                             UINT4 u4MplsTunnelInstance,
                                             UINT4 u4MplsTunnelIngressLSRId,
                                             UINT4 u4MplsTunnelEgressLSRId,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pTestValGmplsTunnelUpstreamNotifyRecipient)
{
    UNUSED_PARAM (u4MplsTunnelIndex);
    UNUSED_PARAM (u4MplsTunnelInstance);
    UNUSED_PARAM (u4MplsTunnelIngressLSRId);
    UNUSED_PARAM (u4MplsTunnelEgressLSRId);
    UNUSED_PARAM (pTestValGmplsTunnelUpstreamNotifyRecipient);

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2GmplsTunnelSendResvNotifyRecipientType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValGmplsTunnelSendResvNotifyRecipientType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2GmplsTunnelSendResvNotifyRecipientType (UINT4 *pu4ErrorCode,
                                                 UINT4 u4MplsTunnelIndex,
                                                 UINT4 u4MplsTunnelInstance,
                                                 UINT4 u4MplsTunnelIngressLSRId,
                                                 UINT4 u4MplsTunnelEgressLSRId,
                                                 INT4
                                                 i4TestValGmplsTunnelSendResvNotifyRecipientType)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    GmplsTnlInfo.SendResvNotifyRecipientType =
        (eInetAddrType) i4TestValGmplsTunnelSendResvNotifyRecipientType;

    MPLS_CMN_LOCK ();

    i1RetVal = TeTestAllGmplsTunnelTable (pu4ErrorCode,
                                          u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&GmplsTnlInfo),
                                          TE_GMPLS_SEND_RESV_NOTIFY_RECEPIENT_TYPE);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2GmplsTunnelSendResvNotifyRecipient
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValGmplsTunnelSendResvNotifyRecipient
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2GmplsTunnelSendResvNotifyRecipient (UINT4 *pu4ErrorCode,
                                             UINT4 u4MplsTunnelIndex,
                                             UINT4 u4MplsTunnelInstance,
                                             UINT4 u4MplsTunnelIngressLSRId,
                                             UINT4 u4MplsTunnelEgressLSRId,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pTestValGmplsTunnelSendResvNotifyRecipient)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    MPLS_OCTETSTRING_TO_INTEGER (pTestValGmplsTunnelSendResvNotifyRecipient,
                                 GmplsTnlInfo.u4SendResvNotifyRecipient);
    MPLS_CMN_LOCK ();

    i1RetVal = TeTestAllGmplsTunnelTable (pu4ErrorCode,
                                          u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&GmplsTnlInfo),
                                          TE_GMPLS_SEND_RESV_NOTIFY_RECEPIENT);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2GmplsTunnelDownstreamNotifyRecipientType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValGmplsTunnelDownstreamNotifyRecipientType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2GmplsTunnelDownstreamNotifyRecipientType (UINT4 *pu4ErrorCode,
                                                   UINT4 u4MplsTunnelIndex,
                                                   UINT4 u4MplsTunnelInstance,
                                                   UINT4
                                                   u4MplsTunnelIngressLSRId,
                                                   UINT4
                                                   u4MplsTunnelEgressLSRId,
                                                   INT4
                                                   i4TestValGmplsTunnelDownstreamNotifyRecipientType)
{
    UNUSED_PARAM (u4MplsTunnelIndex);
    UNUSED_PARAM (u4MplsTunnelInstance);
    UNUSED_PARAM (u4MplsTunnelIngressLSRId);
    UNUSED_PARAM (u4MplsTunnelEgressLSRId);
    UNUSED_PARAM (i4TestValGmplsTunnelDownstreamNotifyRecipientType);

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2GmplsTunnelDownstreamNotifyRecipient
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValGmplsTunnelDownstreamNotifyRecipient
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2GmplsTunnelDownstreamNotifyRecipient (UINT4 *pu4ErrorCode,
                                               UINT4 u4MplsTunnelIndex,
                                               UINT4 u4MplsTunnelInstance,
                                               UINT4 u4MplsTunnelIngressLSRId,
                                               UINT4 u4MplsTunnelEgressLSRId,
                                               tSNMP_OCTET_STRING_TYPE *
                                               pTestValGmplsTunnelDownstreamNotifyRecipient)
{
    UNUSED_PARAM (u4MplsTunnelIndex);
    UNUSED_PARAM (u4MplsTunnelInstance);
    UNUSED_PARAM (u4MplsTunnelIngressLSRId);
    UNUSED_PARAM (u4MplsTunnelEgressLSRId);
    UNUSED_PARAM (pTestValGmplsTunnelDownstreamNotifyRecipient);

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2GmplsTunnelSendPathNotifyRecipientType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValGmplsTunnelSendPathNotifyRecipientType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2GmplsTunnelSendPathNotifyRecipientType (UINT4 *pu4ErrorCode,
                                                 UINT4 u4MplsTunnelIndex,
                                                 UINT4 u4MplsTunnelInstance,
                                                 UINT4 u4MplsTunnelIngressLSRId,
                                                 UINT4 u4MplsTunnelEgressLSRId,
                                                 INT4
                                                 i4TestValGmplsTunnelSendPathNotifyRecipientType)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    GmplsTnlInfo.SendPathNotifyRecipientType =
        (eInetAddrType) i4TestValGmplsTunnelSendPathNotifyRecipientType;

    MPLS_CMN_LOCK ();

    i1RetVal = TeTestAllGmplsTunnelTable (pu4ErrorCode,
                                          u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&GmplsTnlInfo),
                                          TE_GMPLS_SEND_PATH_NOTIFY_RECEPIENT_TYPE);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2GmplsTunnelSendPathNotifyRecipient
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValGmplsTunnelSendPathNotifyRecipient
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2GmplsTunnelSendPathNotifyRecipient (UINT4 *pu4ErrorCode,
                                             UINT4 u4MplsTunnelIndex,
                                             UINT4 u4MplsTunnelInstance,
                                             UINT4 u4MplsTunnelIngressLSRId,
                                             UINT4 u4MplsTunnelEgressLSRId,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pTestValGmplsTunnelSendPathNotifyRecipient)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    MPLS_OCTETSTRING_TO_INTEGER (pTestValGmplsTunnelSendPathNotifyRecipient,
                                 GmplsTnlInfo.u4SendPathNotifyRecipient);
    MPLS_CMN_LOCK ();

    i1RetVal = TeTestAllGmplsTunnelTable (pu4ErrorCode,
                                          u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&GmplsTnlInfo),
                                          TE_GMPLS_SEND_PATH_NOTIFY_RECEPIENT);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2GmplsTunnelAdminStatusFlags
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValGmplsTunnelAdminStatusFlags
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2GmplsTunnelAdminStatusFlags (UINT4 *pu4ErrorCode,
                                      UINT4 u4MplsTunnelIndex,
                                      UINT4 u4MplsTunnelInstance,
                                      UINT4 u4MplsTunnelIngressLSRId,
                                      UINT4 u4MplsTunnelEgressLSRId,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pTestValGmplsTunnelAdminStatusFlags)
{
    tGmplsTnlInfo       GmplsTnlInfo;
    UINT4               u4AdminStatus = TE_ZERO;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlInfo, TE_ZERO, sizeof (tGmplsTnlInfo));

    MPLS_OCTETSTRING_TO_INTEGER (pTestValGmplsTunnelAdminStatusFlags,
                                 u4AdminStatus);

    GmplsTnlInfo.u4AdminStatus = u4AdminStatus;

    GmplsTnlInfo.u1AdminStatusLen =
        (UINT1) pTestValGmplsTunnelAdminStatusFlags->i4_Length;

    MPLS_CMN_LOCK ();

    i1RetVal = TeTestAllGmplsTunnelTable (pu4ErrorCode,
                                          u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&GmplsTnlInfo),
                                          TE_GMPLS_TUNNEL_ADMIN_STATUS_FLAGS);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2GmplsTunnelExtraParamsPtr
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValGmplsTunnelExtraParamsPtr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2GmplsTunnelExtraParamsPtr (UINT4 *pu4ErrorCode,
                                    UINT4 u4MplsTunnelIndex,
                                    UINT4 u4MplsTunnelInstance,
                                    UINT4 u4MplsTunnelIngressLSRId,
                                    UINT4 u4MplsTunnelEgressLSRId,
                                    tSNMP_OID_TYPE *
                                    pTestValGmplsTunnelExtraParamsPtr)
{
    UNUSED_PARAM (u4MplsTunnelIndex);
    UNUSED_PARAM (u4MplsTunnelInstance);
    UNUSED_PARAM (u4MplsTunnelIngressLSRId);
    UNUSED_PARAM (u4MplsTunnelEgressLSRId);
    UNUSED_PARAM (pTestValGmplsTunnelExtraParamsPtr);

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2GmplsTunnelTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2GmplsTunnelTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : GmplsTunnelHopTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceGmplsTunnelHopTable
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceGmplsTunnelHopTable (UINT4 u4MplsTunnelHopListIndex,
                                             UINT4
                                             u4MplsTunnelHopPathOptionIndex,
                                             UINT4 u4MplsTunnelHopIndex)
{
    return (nmhValidateIndexInstanceMplsTunnelHopTable
            (u4MplsTunnelHopListIndex, u4MplsTunnelHopPathOptionIndex,
             u4MplsTunnelHopIndex));

}

/****************************************************************************
 Function    :  nmhGetFirstIndexGmplsTunnelHopTable
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexGmplsTunnelHopTable (UINT4 *pu4MplsTunnelHopListIndex,
                                     UINT4 *pu4MplsTunnelHopPathOptionIndex,
                                     UINT4 *pu4MplsTunnelHopIndex)
{
    return (nmhGetFirstIndexMplsTunnelHopTable (pu4MplsTunnelHopListIndex,
                                                pu4MplsTunnelHopPathOptionIndex,
                                                pu4MplsTunnelHopIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexGmplsTunnelHopTable
 Input       :  The Indices
                MplsTunnelHopListIndex
                nextMplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                nextMplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex
                nextMplsTunnelHopIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexGmplsTunnelHopTable (UINT4 u4MplsTunnelHopListIndex,
                                    UINT4 *pu4NextMplsTunnelHopListIndex,
                                    UINT4 u4MplsTunnelHopPathOptionIndex,
                                    UINT4 *pu4NextMplsTunnelHopPathOptionIndex,
                                    UINT4 u4MplsTunnelHopIndex,
                                    UINT4 *pu4NextMplsTunnelHopIndex)
{
    return (nmhGetNextIndexMplsTunnelHopTable (u4MplsTunnelHopListIndex,
                                               pu4NextMplsTunnelHopListIndex,
                                               u4MplsTunnelHopPathOptionIndex,
                                               pu4NextMplsTunnelHopPathOptionIndex,
                                               u4MplsTunnelHopIndex,
                                               pu4NextMplsTunnelHopIndex));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetGmplsTunnelHopLabelStatuses
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                retValGmplsTunnelHopLabelStatuses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelHopLabelStatuses (UINT4 u4MplsTunnelHopListIndex,
                                   UINT4 u4MplsTunnelHopPathOptionIndex,
                                   UINT4 u4MplsTunnelHopIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValGmplsTunnelHopLabelStatuses)
{
    tGmplsTnlHopInfo    GmplsTnlHopInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlHopInfo, TE_ZERO, sizeof (tGmplsTnlHopInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllGmplsTunnelHopTable (u4MplsTunnelHopListIndex,
                                            u4MplsTunnelHopPathOptionIndex,
                                            u4MplsTunnelHopIndex,
                                            (&GmplsTnlHopInfo));

    MPLS_CMN_UNLOCK ();

    pRetValGmplsTunnelHopLabelStatuses->pu1_OctetList[TE_ZERO]
        = GmplsTnlHopInfo.u1LblStatus;
    pRetValGmplsTunnelHopLabelStatuses->i4_Length = (UINT1) sizeof (UINT1);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelHopExplicitForwardLabel
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                retValGmplsTunnelHopExplicitForwardLabel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelHopExplicitForwardLabel (UINT4 u4MplsTunnelHopListIndex,
                                          UINT4 u4MplsTunnelHopPathOptionIndex,
                                          UINT4 u4MplsTunnelHopIndex,
                                          UINT4
                                          *pu4RetValGmplsTunnelHopExplicitForwardLabel)
{
    tGmplsTnlHopInfo    GmplsTnlHopInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlHopInfo, TE_ZERO, sizeof (tGmplsTnlHopInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllGmplsTunnelHopTable (u4MplsTunnelHopListIndex,
                                            u4MplsTunnelHopPathOptionIndex,
                                            u4MplsTunnelHopIndex,
                                            (&GmplsTnlHopInfo));

    MPLS_CMN_UNLOCK ();

    *pu4RetValGmplsTunnelHopExplicitForwardLabel = GmplsTnlHopInfo.u4ForwardLbl;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelHopExplicitForwardLabelPtr
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                retValGmplsTunnelHopExplicitForwardLabelPtr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelHopExplicitForwardLabelPtr (UINT4 u4MplsTunnelHopListIndex,
                                             UINT4
                                             u4MplsTunnelHopPathOptionIndex,
                                             UINT4 u4MplsTunnelHopIndex,
                                             tSNMP_OID_TYPE *
                                             pRetValGmplsTunnelHopExplicitForwardLabelPtr)
{
    UNUSED_PARAM (u4MplsTunnelHopListIndex);
    UNUSED_PARAM (u4MplsTunnelHopPathOptionIndex);
    UNUSED_PARAM (u4MplsTunnelHopIndex);
    UNUSED_PARAM (pRetValGmplsTunnelHopExplicitForwardLabelPtr);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelHopExplicitReverseLabel
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                retValGmplsTunnelHopExplicitReverseLabel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelHopExplicitReverseLabel (UINT4 u4MplsTunnelHopListIndex,
                                          UINT4 u4MplsTunnelHopPathOptionIndex,
                                          UINT4 u4MplsTunnelHopIndex,
                                          UINT4
                                          *pu4RetValGmplsTunnelHopExplicitReverseLabel)
{
    tGmplsTnlHopInfo    GmplsTnlHopInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlHopInfo, TE_ZERO, sizeof (tGmplsTnlHopInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllGmplsTunnelHopTable (u4MplsTunnelHopListIndex,
                                            u4MplsTunnelHopPathOptionIndex,
                                            u4MplsTunnelHopIndex,
                                            (&GmplsTnlHopInfo));

    MPLS_CMN_UNLOCK ();

    *pu4RetValGmplsTunnelHopExplicitReverseLabel = GmplsTnlHopInfo.u4ReverseLbl;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelHopExplicitReverseLabelPtr
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                retValGmplsTunnelHopExplicitReverseLabelPtr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelHopExplicitReverseLabelPtr (UINT4 u4MplsTunnelHopListIndex,
                                             UINT4
                                             u4MplsTunnelHopPathOptionIndex,
                                             UINT4 u4MplsTunnelHopIndex,
                                             tSNMP_OID_TYPE *
                                             pRetValGmplsTunnelHopExplicitReverseLabelPtr)
{
    UNUSED_PARAM (u4MplsTunnelHopListIndex);
    UNUSED_PARAM (u4MplsTunnelHopPathOptionIndex);
    UNUSED_PARAM (u4MplsTunnelHopIndex);
    UNUSED_PARAM (pRetValGmplsTunnelHopExplicitReverseLabelPtr);

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetGmplsTunnelHopExplicitForwardLabel
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                setValGmplsTunnelHopExplicitForwardLabel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetGmplsTunnelHopExplicitForwardLabel (UINT4 u4MplsTunnelHopListIndex,
                                          UINT4 u4MplsTunnelHopPathOptionIndex,
                                          UINT4 u4MplsTunnelHopIndex,
                                          UINT4
                                          u4SetValGmplsTunnelHopExplicitForwardLabel)
{
    tGmplsTnlHopInfo    GmplsTnlHopInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlHopInfo, TE_ZERO, sizeof (tGmplsTnlHopInfo));
    GmplsTnlHopInfo.u4ForwardLbl = u4SetValGmplsTunnelHopExplicitForwardLabel;

    MPLS_CMN_LOCK ();

    i1RetVal = TeSetAllGmplsTunnelHopTable (u4MplsTunnelHopListIndex,
                                            u4MplsTunnelHopPathOptionIndex,
                                            u4MplsTunnelHopIndex,
                                            (&GmplsTnlHopInfo),
                                            TE_GMPLS_TNL_HOP_FORWARD_LBL);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetGmplsTunnelHopExplicitForwardLabelPtr
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                setValGmplsTunnelHopExplicitForwardLabelPtr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetGmplsTunnelHopExplicitForwardLabelPtr (UINT4 u4MplsTunnelHopListIndex,
                                             UINT4
                                             u4MplsTunnelHopPathOptionIndex,
                                             UINT4 u4MplsTunnelHopIndex,
                                             tSNMP_OID_TYPE *
                                             pSetValGmplsTunnelHopExplicitForwardLabelPtr)
{
    UNUSED_PARAM (u4MplsTunnelHopListIndex);
    UNUSED_PARAM (u4MplsTunnelHopPathOptionIndex);
    UNUSED_PARAM (u4MplsTunnelHopIndex);
    UNUSED_PARAM (pSetValGmplsTunnelHopExplicitForwardLabelPtr);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetGmplsTunnelHopExplicitReverseLabel
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                setValGmplsTunnelHopExplicitReverseLabel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetGmplsTunnelHopExplicitReverseLabel (UINT4 u4MplsTunnelHopListIndex,
                                          UINT4 u4MplsTunnelHopPathOptionIndex,
                                          UINT4 u4MplsTunnelHopIndex,
                                          UINT4
                                          u4SetValGmplsTunnelHopExplicitReverseLabel)
{
    tGmplsTnlHopInfo    GmplsTnlHopInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlHopInfo, TE_ZERO, sizeof (tGmplsTnlHopInfo));

    GmplsTnlHopInfo.u4ReverseLbl = u4SetValGmplsTunnelHopExplicitReverseLabel;

    MPLS_CMN_LOCK ();

    i1RetVal = TeSetAllGmplsTunnelHopTable (u4MplsTunnelHopListIndex,
                                            u4MplsTunnelHopPathOptionIndex,
                                            u4MplsTunnelHopIndex,
                                            (&GmplsTnlHopInfo),
                                            TE_GMPLS_TNL_HOP_REVERSE_LBL);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetGmplsTunnelHopExplicitReverseLabelPtr
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                setValGmplsTunnelHopExplicitReverseLabelPtr
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetGmplsTunnelHopExplicitReverseLabelPtr (UINT4 u4MplsTunnelHopListIndex,
                                             UINT4
                                             u4MplsTunnelHopPathOptionIndex,
                                             UINT4 u4MplsTunnelHopIndex,
                                             tSNMP_OID_TYPE *
                                             pSetValGmplsTunnelHopExplicitReverseLabelPtr)
{
    UNUSED_PARAM (u4MplsTunnelHopListIndex);
    UNUSED_PARAM (u4MplsTunnelHopPathOptionIndex);
    UNUSED_PARAM (u4MplsTunnelHopIndex);
    UNUSED_PARAM (pSetValGmplsTunnelHopExplicitReverseLabelPtr);

    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2GmplsTunnelHopExplicitForwardLabel
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                testValGmplsTunnelHopExplicitForwardLabel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2GmplsTunnelHopExplicitForwardLabel (UINT4 *pu4ErrorCode,
                                             UINT4 u4MplsTunnelHopListIndex,
                                             UINT4
                                             u4MplsTunnelHopPathOptionIndex,
                                             UINT4 u4MplsTunnelHopIndex,
                                             UINT4
                                             u4TestValGmplsTunnelHopExplicitForwardLabel)
{
    tGmplsTnlHopInfo    GmplsTnlHopInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlHopInfo, TE_ZERO, sizeof (tGmplsTnlHopInfo));

    GmplsTnlHopInfo.u4ForwardLbl = u4TestValGmplsTunnelHopExplicitForwardLabel;

    MPLS_CMN_LOCK ();

    i1RetVal = TeTestAllGmplsTunnelHopTable (pu4ErrorCode,
                                             u4MplsTunnelHopListIndex,
                                             u4MplsTunnelHopPathOptionIndex,
                                             u4MplsTunnelHopIndex,
                                             (&GmplsTnlHopInfo),
                                             TE_GMPLS_TNL_HOP_FORWARD_LBL);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2GmplsTunnelHopExplicitForwardLabelPtr
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                testValGmplsTunnelHopExplicitForwardLabelPtr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2GmplsTunnelHopExplicitForwardLabelPtr (UINT4 *pu4ErrorCode,
                                                UINT4 u4MplsTunnelHopListIndex,
                                                UINT4
                                                u4MplsTunnelHopPathOptionIndex,
                                                UINT4 u4MplsTunnelHopIndex,
                                                tSNMP_OID_TYPE *
                                                pTestValGmplsTunnelHopExplicitForwardLabelPtr)
{
    UNUSED_PARAM (u4MplsTunnelHopListIndex);
    UNUSED_PARAM (u4MplsTunnelHopPathOptionIndex);
    UNUSED_PARAM (u4MplsTunnelHopIndex);
    UNUSED_PARAM (pTestValGmplsTunnelHopExplicitForwardLabelPtr);

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2GmplsTunnelHopExplicitReverseLabel
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                testValGmplsTunnelHopExplicitReverseLabel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2GmplsTunnelHopExplicitReverseLabel (UINT4 *pu4ErrorCode,
                                             UINT4 u4MplsTunnelHopListIndex,
                                             UINT4
                                             u4MplsTunnelHopPathOptionIndex,
                                             UINT4 u4MplsTunnelHopIndex,
                                             UINT4
                                             u4TestValGmplsTunnelHopExplicitReverseLabel)
{
    tGmplsTnlHopInfo    GmplsTnlHopInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlHopInfo, TE_ZERO, sizeof (tGmplsTnlHopInfo));

    GmplsTnlHopInfo.u4ReverseLbl = u4TestValGmplsTunnelHopExplicitReverseLabel;

    MPLS_CMN_LOCK ();

    i1RetVal = TeTestAllGmplsTunnelHopTable (pu4ErrorCode,
                                             u4MplsTunnelHopListIndex,
                                             u4MplsTunnelHopPathOptionIndex,
                                             u4MplsTunnelHopIndex,
                                             (&GmplsTnlHopInfo),
                                             TE_GMPLS_TNL_HOP_REVERSE_LBL);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2GmplsTunnelHopExplicitReverseLabelPtr
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                testValGmplsTunnelHopExplicitReverseLabelPtr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2GmplsTunnelHopExplicitReverseLabelPtr (UINT4 *pu4ErrorCode,
                                                UINT4 u4MplsTunnelHopListIndex,
                                                UINT4
                                                u4MplsTunnelHopPathOptionIndex,
                                                UINT4 u4MplsTunnelHopIndex,
                                                tSNMP_OID_TYPE *
                                                pTestValGmplsTunnelHopExplicitReverseLabelPtr)
{
    UNUSED_PARAM (u4MplsTunnelHopListIndex);
    UNUSED_PARAM (u4MplsTunnelHopPathOptionIndex);
    UNUSED_PARAM (u4MplsTunnelHopIndex);
    UNUSED_PARAM (pTestValGmplsTunnelHopExplicitReverseLabelPtr);

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2GmplsTunnelHopTable
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2GmplsTunnelHopTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : GmplsTunnelARHopTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceGmplsTunnelARHopTable
 Input       :  The Indices
                MplsTunnelARHopListIndex
                MplsTunnelARHopIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceGmplsTunnelARHopTable (UINT4 u4MplsTunnelARHopListIndex,
                                               UINT4 u4MplsTunnelARHopIndex)
{
    return (nmhValidateIndexInstanceMplsTunnelARHopTable
            (u4MplsTunnelARHopListIndex, u4MplsTunnelARHopIndex));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexGmplsTunnelARHopTable
 Input       :  The Indices
                MplsTunnelARHopListIndex
                MplsTunnelARHopIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexGmplsTunnelARHopTable (UINT4 *pu4MplsTunnelARHopListIndex,
                                       UINT4 *pu4MplsTunnelARHopIndex)
{
    return (nmhGetFirstIndexMplsTunnelARHopTable (pu4MplsTunnelARHopListIndex,
                                                  pu4MplsTunnelARHopIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexGmplsTunnelARHopTable
 Input       :  The Indices
                MplsTunnelARHopListIndex
                nextMplsTunnelARHopListIndex
                MplsTunnelARHopIndex
                nextMplsTunnelARHopIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexGmplsTunnelARHopTable (UINT4 u4MplsTunnelARHopListIndex,
                                      UINT4 *pu4NextMplsTunnelARHopListIndex,
                                      UINT4 u4MplsTunnelARHopIndex,
                                      UINT4 *pu4NextMplsTunnelARHopIndex)
{
    return (nmhGetNextIndexMplsTunnelARHopTable (u4MplsTunnelARHopListIndex,
                                                 pu4NextMplsTunnelARHopListIndex,
                                                 u4MplsTunnelARHopIndex,
                                                 pu4NextMplsTunnelARHopIndex));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetGmplsTunnelARHopLabelStatuses
 Input       :  The Indices
                MplsTunnelARHopListIndex
                MplsTunnelARHopIndex

                The Object 
                retValGmplsTunnelARHopLabelStatuses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelARHopLabelStatuses (UINT4 u4MplsTunnelARHopListIndex,
                                     UINT4 u4MplsTunnelARHopIndex,
                                     tSNMP_OCTET_STRING_TYPE *
                                     pRetValGmplsTunnelARHopLabelStatuses)
{
    tGmplsTnlArHopInfo  GmplsTnlArHopInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlArHopInfo, TE_ZERO, sizeof (tGmplsTnlArHopInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllGmplsTunnelArHopTable (u4MplsTunnelARHopListIndex,
                                              u4MplsTunnelARHopIndex,
                                              (&GmplsTnlArHopInfo));

    MPLS_CMN_UNLOCK ();

    pRetValGmplsTunnelARHopLabelStatuses->pu1_OctetList[TE_ZERO] =
        GmplsTnlArHopInfo.u1LblStatus;
    pRetValGmplsTunnelARHopLabelStatuses->i4_Length = sizeof (UINT1);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelARHopExplicitForwardLabel
 Input       :  The Indices
                MplsTunnelARHopListIndex
                MplsTunnelARHopIndex

                The Object 
                retValGmplsTunnelARHopExplicitForwardLabel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelARHopExplicitForwardLabel (UINT4 u4MplsTunnelARHopListIndex,
                                            UINT4 u4MplsTunnelARHopIndex,
                                            UINT4
                                            *pu4RetValGmplsTunnelARHopExplicitForwardLabel)
{
    tGmplsTnlArHopInfo  GmplsTnlArHopInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlArHopInfo, TE_ZERO, sizeof (tGmplsTnlArHopInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllGmplsTunnelArHopTable (u4MplsTunnelARHopListIndex,
                                              u4MplsTunnelARHopIndex,
                                              (&GmplsTnlArHopInfo));

    MPLS_CMN_UNLOCK ();

    *pu4RetValGmplsTunnelARHopExplicitForwardLabel
        = GmplsTnlArHopInfo.u4ForwardLbl;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelARHopExplicitForwardLabelPtr
 Input       :  The Indices
                MplsTunnelARHopListIndex
                MplsTunnelARHopIndex

                The Object 
                retValGmplsTunnelARHopExplicitForwardLabelPtr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelARHopExplicitForwardLabelPtr (UINT4 u4MplsTunnelARHopListIndex,
                                               UINT4 u4MplsTunnelARHopIndex,
                                               tSNMP_OID_TYPE *
                                               pRetValGmplsTunnelARHopExplicitForwardLabelPtr)
{
    UNUSED_PARAM (u4MplsTunnelARHopListIndex);
    UNUSED_PARAM (u4MplsTunnelARHopIndex);
    UNUSED_PARAM (pRetValGmplsTunnelARHopExplicitForwardLabelPtr);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelARHopExplicitReverseLabel
 Input       :  The Indices
                MplsTunnelARHopListIndex
                MplsTunnelARHopIndex

                The Object 
                retValGmplsTunnelARHopExplicitReverseLabel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelARHopExplicitReverseLabel (UINT4 u4MplsTunnelARHopListIndex,
                                            UINT4 u4MplsTunnelARHopIndex,
                                            UINT4
                                            *pu4RetValGmplsTunnelARHopExplicitReverseLabel)
{
    tGmplsTnlArHopInfo  GmplsTnlArHopInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlArHopInfo, TE_ZERO, sizeof (tGmplsTnlArHopInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllGmplsTunnelArHopTable (u4MplsTunnelARHopListIndex,
                                              u4MplsTunnelARHopIndex,
                                              (&GmplsTnlArHopInfo));

    MPLS_CMN_UNLOCK ();

    *pu4RetValGmplsTunnelARHopExplicitReverseLabel
        = GmplsTnlArHopInfo.u4ReverseLbl;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelARHopExplicitReverseLabelPtr
 Input       :  The Indices
                MplsTunnelARHopListIndex
                MplsTunnelARHopIndex

                The Object 
                retValGmplsTunnelARHopExplicitReverseLabelPtr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelARHopExplicitReverseLabelPtr (UINT4 u4MplsTunnelARHopListIndex,
                                               UINT4 u4MplsTunnelARHopIndex,
                                               tSNMP_OID_TYPE *
                                               pRetValGmplsTunnelARHopExplicitReverseLabelPtr)
{
    UNUSED_PARAM (u4MplsTunnelARHopListIndex);
    UNUSED_PARAM (u4MplsTunnelARHopIndex);
    UNUSED_PARAM (pRetValGmplsTunnelARHopExplicitReverseLabelPtr);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelARHopProtection
 Input       :  The Indices
                MplsTunnelARHopListIndex
                MplsTunnelARHopIndex

                The Object 
                retValGmplsTunnelARHopProtection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelARHopProtection (UINT4 u4MplsTunnelARHopListIndex,
                                  UINT4 u4MplsTunnelARHopIndex,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValGmplsTunnelARHopProtection)
{
    tGmplsTnlArHopInfo  GmplsTnlArHopInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&GmplsTnlArHopInfo, 0, sizeof (tGmplsTnlArHopInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllGmplsTunnelArHopTable (u4MplsTunnelARHopListIndex,
                                              u4MplsTunnelARHopIndex,
                                              (&GmplsTnlArHopInfo));

    MPLS_CMN_UNLOCK ();

    pRetValGmplsTunnelARHopProtection->pu1_OctetList[TE_ZERO]
        = GmplsTnlArHopInfo.u1Protection;
    pRetValGmplsTunnelARHopProtection->i4_Length
        = (UINT1) GmplsTnlArHopInfo.u1ProtLen;

    return i1RetVal;
}

/* LOW LEVEL Routines for Table : GmplsTunnelCHopTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceGmplsTunnelCHopTable
 Input       :  The Indices
                MplsTunnelCHopListIndex
                MplsTunnelCHopIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceGmplsTunnelCHopTable (UINT4 u4MplsTunnelCHopListIndex,
                                              UINT4 u4MplsTunnelCHopIndex)
{
    return (nmhValidateIndexInstanceMplsTunnelCHopTable
            (u4MplsTunnelCHopListIndex, u4MplsTunnelCHopIndex));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexGmplsTunnelCHopTable
 Input       :  The Indices
                MplsTunnelCHopListIndex
                MplsTunnelCHopIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexGmplsTunnelCHopTable (UINT4 *pu4MplsTunnelCHopListIndex,
                                      UINT4 *pu4MplsTunnelCHopIndex)
{
    return (nmhGetFirstIndexMplsTunnelCHopTable (pu4MplsTunnelCHopListIndex,
                                                 pu4MplsTunnelCHopIndex));
}

/****************************************************************************
 Function    :  nmhGetNextIndexGmplsTunnelCHopTable
 Input       :  The Indices
                MplsTunnelCHopListIndex
                nextMplsTunnelCHopListIndex
                MplsTunnelCHopIndex
                nextMplsTunnelCHopIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexGmplsTunnelCHopTable (UINT4 u4MplsTunnelCHopListIndex,
                                     UINT4 *pu4NextMplsTunnelCHopListIndex,
                                     UINT4 u4MplsTunnelCHopIndex,
                                     UINT4 *pu4NextMplsTunnelCHopIndex)
{
    return (nmhGetNextIndexMplsTunnelCHopTable (u4MplsTunnelCHopListIndex,
                                                pu4NextMplsTunnelCHopListIndex,
                                                u4MplsTunnelCHopIndex,
                                                pu4NextMplsTunnelCHopIndex));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetGmplsTunnelCHopLabelStatuses
 Input       :  The Indices
                MplsTunnelCHopListIndex
                MplsTunnelCHopIndex

                The Object 
                retValGmplsTunnelCHopLabelStatuses
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelCHopLabelStatuses (UINT4 u4MplsTunnelCHopListIndex,
                                    UINT4 u4MplsTunnelCHopIndex,
                                    tSNMP_OCTET_STRING_TYPE *
                                    pRetValGmplsTunnelCHopLabelStatuses)
{
    tTeCHopInfo         TeCHop;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeCHop, TE_ZERO, sizeof (tTeCHopInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllMplsTunnelCHopTable (u4MplsTunnelCHopListIndex,
                                            u4MplsTunnelCHopIndex, (&TeCHop));

    MPLS_CMN_UNLOCK ();

    pRetValGmplsTunnelCHopLabelStatuses->pu1_OctetList[TE_ZERO] =
        TeCHop.GmplsTnlCHopInfo.u1LblStatus;
    pRetValGmplsTunnelCHopLabelStatuses->i4_Length = sizeof (UINT1);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelCHopExplicitForwardLabel
 Input       :  The Indices
                MplsTunnelCHopListIndex
                MplsTunnelCHopIndex

                The Object 
                retValGmplsTunnelCHopExplicitForwardLabel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelCHopExplicitForwardLabel (UINT4 u4MplsTunnelCHopListIndex,
                                           UINT4 u4MplsTunnelCHopIndex,
                                           UINT4
                                           *pu4RetValGmplsTunnelCHopExplicitForwardLabel)
{
    tTeCHopInfo         TeCHop;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeCHop, TE_ZERO, sizeof (tTeCHopInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllMplsTunnelCHopTable (u4MplsTunnelCHopListIndex,
                                            u4MplsTunnelCHopIndex, (&TeCHop));

    MPLS_CMN_UNLOCK ();

    *pu4RetValGmplsTunnelCHopExplicitForwardLabel
        = TeCHop.GmplsTnlCHopInfo.u4ForwardLbl;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelCHopExplicitForwardLabelPtr
 Input       :  The Indices
                MplsTunnelCHopListIndex
                MplsTunnelCHopIndex

                The Object 
                retValGmplsTunnelCHopExplicitForwardLabelPtr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelCHopExplicitForwardLabelPtr (UINT4 u4MplsTunnelCHopListIndex,
                                              UINT4 u4MplsTunnelCHopIndex,
                                              tSNMP_OID_TYPE *
                                              pRetValGmplsTunnelCHopExplicitForwardLabelPtr)
{
    UNUSED_PARAM (u4MplsTunnelCHopListIndex);
    UNUSED_PARAM (u4MplsTunnelCHopIndex);
    UNUSED_PARAM (pRetValGmplsTunnelCHopExplicitForwardLabelPtr);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelCHopExplicitReverseLabel
 Input       :  The Indices
                MplsTunnelCHopListIndex
                MplsTunnelCHopIndex

                The Object 
                retValGmplsTunnelCHopExplicitReverseLabel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelCHopExplicitReverseLabel (UINT4 u4MplsTunnelCHopListIndex,
                                           UINT4 u4MplsTunnelCHopIndex,
                                           UINT4
                                           *pu4RetValGmplsTunnelCHopExplicitReverseLabel)
{
    tTeCHopInfo         TeCHop;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeCHop, TE_ZERO, sizeof (tTeCHopInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllMplsTunnelCHopTable (u4MplsTunnelCHopListIndex,
                                            u4MplsTunnelCHopIndex, (&TeCHop));

    MPLS_CMN_UNLOCK ();

    *pu4RetValGmplsTunnelCHopExplicitReverseLabel
        = TeCHop.GmplsTnlCHopInfo.u4ReverseLbl;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelCHopExplicitReverseLabelPtr
 Input       :  The Indices
                MplsTunnelCHopListIndex
                MplsTunnelCHopIndex

                The Object 
                retValGmplsTunnelCHopExplicitReverseLabelPtr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelCHopExplicitReverseLabelPtr (UINT4 u4MplsTunnelCHopListIndex,
                                              UINT4 u4MplsTunnelCHopIndex,
                                              tSNMP_OID_TYPE *
                                              pRetValGmplsTunnelCHopExplicitReverseLabelPtr)
{
    UNUSED_PARAM (u4MplsTunnelCHopListIndex);
    UNUSED_PARAM (u4MplsTunnelCHopIndex);
    UNUSED_PARAM (pRetValGmplsTunnelCHopExplicitReverseLabelPtr);

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : GmplsTunnelReversePerfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceGmplsTunnelReversePerfTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceGmplsTunnelReversePerfTable (UINT4 u4MplsTunnelIndex,
                                                     UINT4 u4MplsTunnelInstance,
                                                     UINT4
                                                     u4MplsTunnelIngressLSRId,
                                                     UINT4
                                                     u4MplsTunnelEgressLSRId)
{
    return (nmhValidateIndexInstanceMplsTunnelTable (u4MplsTunnelIndex,
                                                     u4MplsTunnelInstance,
                                                     u4MplsTunnelIngressLSRId,
                                                     u4MplsTunnelEgressLSRId));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexGmplsTunnelReversePerfTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexGmplsTunnelReversePerfTable (UINT4 *pu4MplsTunnelIndex,
                                             UINT4 *pu4MplsTunnelInstance,
                                             UINT4 *pu4MplsTunnelIngressLSRId,
                                             UINT4 *pu4MplsTunnelEgressLSRId)
{
    return (nmhGetFirstIndexMplsTunnelTable (pu4MplsTunnelIndex,
                                             pu4MplsTunnelInstance,
                                             pu4MplsTunnelIngressLSRId,
                                             pu4MplsTunnelEgressLSRId));
}

/****************************************************************************
 Function    :  nmhGetNextIndexGmplsTunnelReversePerfTable
 Input       :  The Indices
                MplsTunnelIndex
                nextMplsTunnelIndex
                MplsTunnelInstance
                nextMplsTunnelInstance
                MplsTunnelIngressLSRId
                nextMplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                nextMplsTunnelEgressLSRId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexGmplsTunnelReversePerfTable (UINT4 u4MplsTunnelIndex,
                                            UINT4 *pu4NextMplsTunnelIndex,
                                            UINT4 u4MplsTunnelInstance,
                                            UINT4 *pu4NextMplsTunnelInstance,
                                            UINT4 u4MplsTunnelIngressLSRId,
                                            UINT4
                                            *pu4NextMplsTunnelIngressLSRId,
                                            UINT4 u4MplsTunnelEgressLSRId,
                                            UINT4 *pu4NextMplsTunnelEgressLSRId)
{
    return (nmhGetNextIndexMplsTunnelTable (u4MplsTunnelIndex,
                                            pu4NextMplsTunnelIndex,
                                            u4MplsTunnelInstance,
                                            pu4NextMplsTunnelInstance,
                                            u4MplsTunnelIngressLSRId,
                                            pu4NextMplsTunnelIngressLSRId,
                                            u4MplsTunnelEgressLSRId,
                                            pu4NextMplsTunnelEgressLSRId));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetGmplsTunnelReversePerfPackets
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValGmplsTunnelReversePerfPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelReversePerfPackets (UINT4 u4MplsTunnelIndex,
                                     UINT4 u4MplsTunnelInstance,
                                     UINT4 u4MplsTunnelIngressLSRId,
                                     UINT4 u4MplsTunnelEgressLSRId,
                                     UINT4
                                     *pu4RetValGmplsTunnelReversePerfPackets)
{
    UNUSED_PARAM (u4MplsTunnelIndex);
    UNUSED_PARAM (u4MplsTunnelInstance);
    UNUSED_PARAM (u4MplsTunnelIngressLSRId);
    UNUSED_PARAM (u4MplsTunnelEgressLSRId);
    UNUSED_PARAM (pu4RetValGmplsTunnelReversePerfPackets);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelReversePerfHCPackets
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValGmplsTunnelReversePerfHCPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelReversePerfHCPackets (UINT4 u4MplsTunnelIndex,
                                       UINT4 u4MplsTunnelInstance,
                                       UINT4 u4MplsTunnelIngressLSRId,
                                       UINT4 u4MplsTunnelEgressLSRId,
                                       tSNMP_COUNTER64_TYPE *
                                       pu8RetValGmplsTunnelReversePerfHCPackets)
{
    UNUSED_PARAM (u4MplsTunnelIndex);
    UNUSED_PARAM (u4MplsTunnelInstance);
    UNUSED_PARAM (u4MplsTunnelIngressLSRId);
    UNUSED_PARAM (u4MplsTunnelEgressLSRId);
    UNUSED_PARAM (pu8RetValGmplsTunnelReversePerfHCPackets);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelReversePerfErrors
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValGmplsTunnelReversePerfErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelReversePerfErrors (UINT4 u4MplsTunnelIndex,
                                    UINT4 u4MplsTunnelInstance,
                                    UINT4 u4MplsTunnelIngressLSRId,
                                    UINT4 u4MplsTunnelEgressLSRId,
                                    UINT4
                                    *pu4RetValGmplsTunnelReversePerfErrors)
{
    UNUSED_PARAM (u4MplsTunnelIndex);
    UNUSED_PARAM (u4MplsTunnelInstance);
    UNUSED_PARAM (u4MplsTunnelIngressLSRId);
    UNUSED_PARAM (u4MplsTunnelEgressLSRId);
    UNUSED_PARAM (pu4RetValGmplsTunnelReversePerfErrors);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelReversePerfBytes
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValGmplsTunnelReversePerfBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelReversePerfBytes (UINT4 u4MplsTunnelIndex,
                                   UINT4 u4MplsTunnelInstance,
                                   UINT4 u4MplsTunnelIngressLSRId,
                                   UINT4 u4MplsTunnelEgressLSRId,
                                   UINT4 *pu4RetValGmplsTunnelReversePerfBytes)
{
    UNUSED_PARAM (u4MplsTunnelIndex);
    UNUSED_PARAM (u4MplsTunnelInstance);
    UNUSED_PARAM (u4MplsTunnelIngressLSRId);
    UNUSED_PARAM (u4MplsTunnelEgressLSRId);
    UNUSED_PARAM (pu4RetValGmplsTunnelReversePerfBytes);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelReversePerfHCBytes
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValGmplsTunnelReversePerfHCBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelReversePerfHCBytes (UINT4 u4MplsTunnelIndex,
                                     UINT4 u4MplsTunnelInstance,
                                     UINT4 u4MplsTunnelIngressLSRId,
                                     UINT4 u4MplsTunnelEgressLSRId,
                                     tSNMP_COUNTER64_TYPE *
                                     pu8RetValGmplsTunnelReversePerfHCBytes)
{
    UNUSED_PARAM (u4MplsTunnelIndex);
    UNUSED_PARAM (u4MplsTunnelInstance);
    UNUSED_PARAM (u4MplsTunnelIngressLSRId);
    UNUSED_PARAM (u4MplsTunnelEgressLSRId);
    UNUSED_PARAM (pu8RetValGmplsTunnelReversePerfHCBytes);

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : GmplsTunnelErrorTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceGmplsTunnelErrorTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceGmplsTunnelErrorTable (UINT4 u4MplsTunnelIndex,
                                               UINT4 u4MplsTunnelInstance,
                                               UINT4 u4MplsTunnelIngressLSRId,
                                               UINT4 u4MplsTunnelEgressLSRId)
{
    return (nmhValidateIndexInstanceGmplsTunnelTable
            (u4MplsTunnelIndex, u4MplsTunnelInstance, u4MplsTunnelIngressLSRId,
             u4MplsTunnelEgressLSRId));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexGmplsTunnelErrorTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexGmplsTunnelErrorTable (UINT4 *pu4MplsTunnelIndex,
                                       UINT4 *pu4MplsTunnelInstance,
                                       UINT4 *pu4MplsTunnelIngressLSRId,
                                       UINT4 *pu4MplsTunnelEgressLSRId)
{
    return (nmhGetFirstIndexGmplsTunnelTable
            (pu4MplsTunnelIndex, pu4MplsTunnelInstance,
             pu4MplsTunnelIngressLSRId, pu4MplsTunnelEgressLSRId));
}

/****************************************************************************
 Function    :  nmhGetNextIndexGmplsTunnelErrorTable
 Input       :  The Indices
                MplsTunnelIndex
                nextMplsTunnelIndex
                MplsTunnelInstance
                nextMplsTunnelInstance
                MplsTunnelIngressLSRId
                nextMplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                nextMplsTunnelEgressLSRId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexGmplsTunnelErrorTable (UINT4 u4MplsTunnelIndex,
                                      UINT4 *pu4NextMplsTunnelIndex,
                                      UINT4 u4MplsTunnelInstance,
                                      UINT4 *pu4NextMplsTunnelInstance,
                                      UINT4 u4MplsTunnelIngressLSRId,
                                      UINT4 *pu4NextMplsTunnelIngressLSRId,
                                      UINT4 u4MplsTunnelEgressLSRId,
                                      UINT4 *pu4NextMplsTunnelEgressLSRId)
{
    return (nmhGetNextIndexGmplsTunnelTable (u4MplsTunnelIndex,
                                             pu4NextMplsTunnelIndex,
                                             u4MplsTunnelInstance,
                                             pu4NextMplsTunnelInstance,
                                             u4MplsTunnelIngressLSRId,
                                             pu4NextMplsTunnelIngressLSRId,
                                             u4MplsTunnelEgressLSRId,
                                             pu4NextMplsTunnelEgressLSRId));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetGmplsTunnelErrorLastErrorType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValGmplsTunnelErrorLastErrorType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelErrorLastErrorType (UINT4 u4MplsTunnelIndex,
                                     UINT4 u4MplsTunnelInstance,
                                     UINT4 u4MplsTunnelIngressLSRId,
                                     UINT4 u4MplsTunnelEgressLSRId,
                                     INT4
                                     *pi4RetValGmplsTunnelErrorLastErrorType)
{
    tTunnelErrInfo      TmpGmplsTnlErrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TmpGmplsTnlErrInfo, TE_ZERO, sizeof (tTunnelErrInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllGmplsTunnelErrorTable (u4MplsTunnelIndex,
                                              u4MplsTunnelInstance,
                                              u4MplsTunnelIngressLSRId,
                                              u4MplsTunnelEgressLSRId,
                                              (&TmpGmplsTnlErrInfo));

    MPLS_CMN_UNLOCK ();

    *pi4RetValGmplsTunnelErrorLastErrorType
        = (INT4) TmpGmplsTnlErrInfo.LastErrType;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelErrorLastTime
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValGmplsTunnelErrorLastTime
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelErrorLastTime (UINT4 u4MplsTunnelIndex,
                                UINT4 u4MplsTunnelInstance,
                                UINT4 u4MplsTunnelIngressLSRId,
                                UINT4 u4MplsTunnelEgressLSRId,
                                UINT4 *pu4RetValGmplsTunnelErrorLastTime)
{
    tTunnelErrInfo      TmpGmplsTnlErrInfo;

    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TmpGmplsTnlErrInfo, TE_ZERO, sizeof (tTunnelErrInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllGmplsTunnelErrorTable (u4MplsTunnelIndex,
                                              u4MplsTunnelInstance,
                                              u4MplsTunnelIngressLSRId,
                                              u4MplsTunnelEgressLSRId,
                                              (&TmpGmplsTnlErrInfo));

    MPLS_CMN_UNLOCK ();

    *pu4RetValGmplsTunnelErrorLastTime = TmpGmplsTnlErrInfo.u4LastErrTime;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelErrorReporterType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValGmplsTunnelErrorReporterType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelErrorReporterType (UINT4 u4MplsTunnelIndex,
                                    UINT4 u4MplsTunnelInstance,
                                    UINT4 u4MplsTunnelIngressLSRId,
                                    UINT4 u4MplsTunnelEgressLSRId,
                                    INT4 *pi4RetValGmplsTunnelErrorReporterType)
{
    tTunnelErrInfo      TmpGmplsTnlErrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TmpGmplsTnlErrInfo, TE_ZERO, sizeof (tTunnelErrInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllGmplsTunnelErrorTable (u4MplsTunnelIndex,
                                              u4MplsTunnelInstance,
                                              u4MplsTunnelIngressLSRId,
                                              u4MplsTunnelEgressLSRId,
                                              (&TmpGmplsTnlErrInfo));

    MPLS_CMN_UNLOCK ();

    *pi4RetValGmplsTunnelErrorReporterType
        = TmpGmplsTnlErrInfo.ErrorReporterType;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelErrorReporter
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValGmplsTunnelErrorReporter
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelErrorReporter (UINT4 u4MplsTunnelIndex,
                                UINT4 u4MplsTunnelInstance,
                                UINT4 u4MplsTunnelIngressLSRId,
                                UINT4 u4MplsTunnelEgressLSRId,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValGmplsTunnelErrorReporter)
{
    tTunnelErrInfo      TmpGmplsTnlErrInfo;
    INT1                i1RetVal = TE_ZERO;
    UINT4               u4Temp = TE_ZERO;

    MEMSET (&TmpGmplsTnlErrInfo, TE_ZERO, sizeof (tTunnelErrInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllGmplsTunnelErrorTable (u4MplsTunnelIndex,
                                              u4MplsTunnelInstance,
                                              u4MplsTunnelIngressLSRId,
                                              u4MplsTunnelEgressLSRId,
                                              (&TmpGmplsTnlErrInfo));

    MPLS_CMN_UNLOCK ();

    u4Temp = OSIX_HTONL (TmpGmplsTnlErrInfo.u4ErrorIpAddress);
    pRetValGmplsTunnelErrorReporter->i4_Length
        = (INT4) TmpGmplsTnlErrInfo.u1ErrorIpAddressLen;
    MEMCPY (pRetValGmplsTunnelErrorReporter->pu1_OctetList,
            (UINT1 *) &u4Temp, TmpGmplsTnlErrInfo.u1ErrorIpAddressLen);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelErrorCode
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValGmplsTunnelErrorCode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelErrorCode (UINT4 u4MplsTunnelIndex, UINT4 u4MplsTunnelInstance,
                            UINT4 u4MplsTunnelIngressLSRId,
                            UINT4 u4MplsTunnelEgressLSRId,
                            UINT4 *pu4RetValGmplsTunnelErrorCode)
{
    tTunnelErrInfo      TmpGmplsTnlErrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TmpGmplsTnlErrInfo, TE_ZERO, sizeof (tTunnelErrInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllGmplsTunnelErrorTable (u4MplsTunnelIndex,
                                              u4MplsTunnelInstance,
                                              u4MplsTunnelIngressLSRId,
                                              u4MplsTunnelEgressLSRId,
                                              (&TmpGmplsTnlErrInfo));

    MPLS_CMN_UNLOCK ();

    *pu4RetValGmplsTunnelErrorCode = TmpGmplsTnlErrInfo.u4ErrorCode;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelErrorSubcode
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValGmplsTunnelErrorSubcode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelErrorSubcode (UINT4 u4MplsTunnelIndex,
                               UINT4 u4MplsTunnelInstance,
                               UINT4 u4MplsTunnelIngressLSRId,
                               UINT4 u4MplsTunnelEgressLSRId,
                               UINT4 *pu4RetValGmplsTunnelErrorSubcode)
{
    tTunnelErrInfo      TmpGmplsTnlErrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TmpGmplsTnlErrInfo, TE_ZERO, sizeof (tTunnelErrInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllGmplsTunnelErrorTable (u4MplsTunnelIndex,
                                              u4MplsTunnelInstance,
                                              u4MplsTunnelIngressLSRId,
                                              u4MplsTunnelEgressLSRId,
                                              (&TmpGmplsTnlErrInfo));

    MPLS_CMN_UNLOCK ();

    *pu4RetValGmplsTunnelErrorSubcode = TmpGmplsTnlErrInfo.u4SubErrorCode;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelErrorTLVs
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValGmplsTunnelErrorTLVs
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelErrorTLVs (UINT4 u4MplsTunnelIndex, UINT4 u4MplsTunnelInstance,
                            UINT4 u4MplsTunnelIngressLSRId,
                            UINT4 u4MplsTunnelEgressLSRId,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValGmplsTunnelErrorTLVs)
{
    tTunnelErrInfo      TmpGmplsTnlErrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TmpGmplsTnlErrInfo, TE_ZERO, sizeof (tTunnelErrInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllGmplsTunnelErrorTable (u4MplsTunnelIndex,
                                              u4MplsTunnelInstance,
                                              u4MplsTunnelIngressLSRId,
                                              u4MplsTunnelEgressLSRId,
                                              (&TmpGmplsTnlErrInfo));

    MPLS_CMN_UNLOCK ();

    pRetValGmplsTunnelErrorTLVs->i4_Length =
        TmpGmplsTnlErrInfo.i4GmplsTnlErrTLVLength;
    if (pRetValGmplsTunnelErrorTLVs->i4_Length != TE_ZERO)
    {
        MEMCPY (pRetValGmplsTunnelErrorTLVs->pu1_OctetList,
                &TmpGmplsTnlErrInfo.au1GmplsTnlErrTLV,
                pRetValGmplsTunnelErrorTLVs->i4_Length);
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetGmplsTunnelErrorHelpString
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValGmplsTunnelErrorHelpString
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetGmplsTunnelErrorHelpString (UINT4 u4MplsTunnelIndex,
                                  UINT4 u4MplsTunnelInstance,
                                  UINT4 u4MplsTunnelIngressLSRId,
                                  UINT4 u4MplsTunnelEgressLSRId,
                                  tSNMP_OCTET_STRING_TYPE *
                                  pRetValGmplsTunnelErrorHelpString)
{
    tTunnelErrInfo      TmpGmplsTnlErrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TmpGmplsTnlErrInfo, TE_ZERO, sizeof (tTunnelErrInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllGmplsTunnelErrorTable (u4MplsTunnelIndex,
                                              u4MplsTunnelInstance,
                                              u4MplsTunnelIngressLSRId,
                                              u4MplsTunnelEgressLSRId,
                                              (&TmpGmplsTnlErrInfo));

    MPLS_CMN_UNLOCK ();

    pRetValGmplsTunnelErrorHelpString->i4_Length =
        TmpGmplsTnlErrInfo.i4GmplsErrHelpStringLength;

    if (pRetValGmplsTunnelErrorHelpString->i4_Length != TE_ZERO)
    {
        MEMCPY (pRetValGmplsTunnelErrorHelpString->pu1_OctetList,
                &(TmpGmplsTnlErrInfo.au1GmplsErrHelpString),
                pRetValGmplsTunnelErrorHelpString->i4_Length);
    }

    return i1RetVal;
}
