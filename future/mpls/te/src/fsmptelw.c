/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsmptelw.c,v 1.34 2016/06/21 09:45:40 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/

# include  "lr.h"
# include  "fssnmp.h"
# include  "teincs.h"
# include  "fsmptelw.h"
# include  "stdtelw.h"

/* LOW LEVEL Routines for Table : FsMplsTunnelTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsTunnelTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMplsTunnelTable (UINT4 u4MplsTunnelIndex,
                                           UINT4 u4MplsTunnelInstance,
                                           UINT4 u4MplsTunnelIngressLSRId,
                                           UINT4 u4MplsTunnelEgressLSRId)
{
    return (nmhValidateIndexInstanceMplsTunnelTable (u4MplsTunnelIndex,
                                                     u4MplsTunnelInstance,
                                                     u4MplsTunnelIngressLSRId,
                                                     u4MplsTunnelEgressLSRId));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsTunnelTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMplsTunnelTable (UINT4 *pu4MplsTunnelIndex,
                                   UINT4 *pu4MplsTunnelInstance,
                                   UINT4 *pu4MplsTunnelIngressLSRId,
                                   UINT4 *pu4MplsTunnelEgressLSRId)
{
    return (nmhGetFirstIndexMplsTunnelTable (pu4MplsTunnelIndex,
                                             pu4MplsTunnelInstance,
                                             pu4MplsTunnelIngressLSRId,
                                             pu4MplsTunnelEgressLSRId));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMplsTunnelTable
 Input       :  The Indices
                MplsTunnelIndex
                nextMplsTunnelIndex
                MplsTunnelInstance
                nextMplsTunnelInstance
                MplsTunnelIngressLSRId
                nextMplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                nextMplsTunnelEgressLSRId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMplsTunnelTable (UINT4 u4MplsTunnelIndex,
                                  UINT4 *pu4NextMplsTunnelIndex,
                                  UINT4 u4MplsTunnelInstance,
                                  UINT4 *pu4NextMplsTunnelInstance,
                                  UINT4 u4MplsTunnelIngressLSRId,
                                  UINT4 *pu4NextMplsTunnelIngressLSRId,
                                  UINT4 u4MplsTunnelEgressLSRId,
                                  UINT4 *pu4NextMplsTunnelEgressLSRId)
{
    return (nmhGetNextIndexMplsTunnelTable (u4MplsTunnelIndex,
                                            pu4NextMplsTunnelIndex,
                                            u4MplsTunnelInstance,
                                            pu4NextMplsTunnelInstance,
                                            u4MplsTunnelIngressLSRId,
                                            pu4NextMplsTunnelIngressLSRId,
                                            u4MplsTunnelEgressLSRId,
                                            pu4NextMplsTunnelEgressLSRId));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsTunnelType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelType (UINT4 u4MplsTunnelIndex,
                        UINT4 u4MplsTunnelInstance,
                        UINT4 u4MplsTunnelIngressLSRId,
                        UINT4 u4MplsTunnelEgressLSRId,
                        tSNMP_OCTET_STRING_TYPE * pRetValFsMplsTunnelType)
{
    tTeTnlInfo          TeTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeTnlInfo, TE_ZERO, sizeof (tTeTnlInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllFsMplsTunnelTable (u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&TeTnlInfo));

    MPLS_CMN_UNLOCK ();

    pRetValFsMplsTunnelType->i4_Length = (INT4) sizeof (UINT1);
    pRetValFsMplsTunnelType->pu1_OctetList[TE_ZERO]
        = (UINT1) TeTnlInfo.u4TnlType;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelLSRIdMapInfo
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsTunnelLSRIdMapInfo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelLSRIdMapInfo (UINT4 u4MplsTunnelIndex,
                                UINT4 u4MplsTunnelInstance,
                                UINT4 u4MplsTunnelIngressLSRId,
                                UINT4 u4MplsTunnelEgressLSRId,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValFsMplsTunnelLSRIdMapInfo)
{
    tTeTnlInfo          TeTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeTnlInfo, TE_ZERO, sizeof (tTeTnlInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllFsMplsTunnelTable (u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&TeTnlInfo));

    MPLS_CMN_UNLOCK ();

    pRetValFsMplsTunnelLSRIdMapInfo->i4_Length = sizeof (UINT1);
    pRetValFsMplsTunnelLSRIdMapInfo->pu1_OctetList[TE_ZERO]
        = (UINT1) TeTnlInfo.u4TnlLsrIdMapInfo;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelMode
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsTunnelMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelMode (UINT4 u4MplsTunnelIndex,
                        UINT4 u4MplsTunnelInstance,
                        UINT4 u4MplsTunnelIngressLSRId,
                        UINT4 u4MplsTunnelEgressLSRId,
                        INT4 *pi4RetValFsMplsTunnelMode)
{
    tTeTnlInfo          TeTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeTnlInfo, TE_ZERO, sizeof (tTeTnlInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllFsMplsTunnelTable (u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&TeTnlInfo));

    MPLS_CMN_UNLOCK ();

    *pi4RetValFsMplsTunnelMode = (INT4) TeTnlInfo.u4TnlMode;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelProactiveSessIndex
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsTunnelProactiveSessIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelProactiveSessIndex (UINT4 u4MplsTunnelIndex,
                                      UINT4 u4MplsTunnelInstance,
                                      UINT4 u4MplsTunnelIngressLSRId,
                                      UINT4 u4MplsTunnelEgressLSRId,
                                      UINT4
                                      *pu4RetValFsMplsTunnelProactiveSessIndex)
{
    tTeTnlInfo          TeTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeTnlInfo, TE_ZERO, sizeof (tTeTnlInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllFsMplsTunnelTable (u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&TeTnlInfo));

    MPLS_CMN_UNLOCK ();

    *pu4RetValFsMplsTunnelProactiveSessIndex
        = TeTnlInfo.u4ProactiveSessionIndex;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelMBBStatus
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsTunnelMBBStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelMBBStatus (UINT4 u4MplsTunnelIndex,
                             UINT4 u4MplsTunnelInstance,
                             UINT4 u4MplsTunnelIngressLSRId,
                             UINT4 u4MplsTunnelEgressLSRId,
                             INT4 *pi4RetValFsMplsTunnelMBBStatus)
{
    tTeTnlInfo          TeTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeTnlInfo, TE_ZERO, sizeof (tTeTnlInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllFsMplsTunnelTable (u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&TeTnlInfo));

    MPLS_CMN_UNLOCK ();

    *pi4RetValFsMplsTunnelMBBStatus = TeTnlInfo.u1TnlMBBStatus;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelDisJointType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsTunnelDisJointType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelDisJointType (UINT4 u4MplsTunnelIndex,
                                UINT4 u4MplsTunnelInstance,
                                UINT4 u4MplsTunnelIngressLSRId,
                                UINT4 u4MplsTunnelEgressLSRId,
                                INT4 *pi4RetValFsMplsTunnelDisJointType)
{
    tTeTnlInfo          TeTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeTnlInfo, TE_ZERO, sizeof (tTeTnlInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllFsMplsTunnelTable (u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&TeTnlInfo));

    MPLS_CMN_UNLOCK ();

    *pi4RetValFsMplsTunnelDisJointType = (INT4) TeTnlInfo.u4TnlDisJointType;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelAttPointer
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsTunnelAttPointer
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelAttPointer (UINT4 u4MplsTunnelIndex,
                              UINT4 u4MplsTunnelInstance,
                              UINT4 u4MplsTunnelIngressLSRId,
                              UINT4 u4MplsTunnelEgressLSRId,
                              tSNMP_OID_TYPE * pRetValFsMplsTunnelAttPointer)
{
    tTeTnlInfo          TeTnlInfo;

    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeTnlInfo, 0, sizeof (tTeTnlInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllFsMplsTunnelTable (u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&TeTnlInfo));

    MPLS_CMN_UNLOCK ();

    pRetValFsMplsTunnelAttPointer->u4_Length = TE_TNL_ATTR_TABLE_DEF_OFFSET;

    if (TeTnlInfo.u4AttrParamIndex == TE_ZERO)
    {
        MEMSET (pRetValFsMplsTunnelAttPointer->pu4_OidList, TE_ZERO,
                sizeof (UINT4) * TE_TWO);
        pRetValFsMplsTunnelAttPointer->u4_Length = TE_TWO;
    }
    else
    {
        pRetValFsMplsTunnelAttPointer->u4_Length = TE_TNL_ATTR_TABLE_DEF_OFFSET;

        MEMCPY (pRetValFsMplsTunnelAttPointer->pu4_OidList,
                au4TeTnlAttributeTableOid,
                TE_TNL_ATTR_TABLE_DEF_OFFSET * sizeof (UINT4));
        pRetValFsMplsTunnelAttPointer->pu4_OidList
            [TE_TNL_ATTR_TABLE_DEF_OFFSET - TE_ONE]
            = TeTnlInfo.u4AttrParamIndex;
    }

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelEndToEndProtection
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsTunnelEndToEndProtection
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelEndToEndProtection (UINT4 u4MplsTunnelIndex,
                                      UINT4 u4MplsTunnelInstance,
                                      UINT4 u4MplsTunnelIngressLSRId,
                                      UINT4 u4MplsTunnelEgressLSRId,
                                      tSNMP_OCTET_STRING_TYPE
                                      * pRetValFsMplsTunnelEndToEndProtection)
{
    tTeTnlInfo          TeTnlInfo;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TeTnlInfo, TE_ZERO, sizeof (tTeTnlInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllFsMplsTunnelTable (u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&TeTnlInfo));

    pRetValFsMplsTunnelEndToEndProtection->pu1_OctetList[0] =
        (UINT1)TeTnlInfo.GmplsTnlInfo.i4E2EProtectionType;

    pRetValFsMplsTunnelEndToEndProtection->i4_Length = sizeof (UINT1);

    MPLS_CMN_UNLOCK ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelPrConfigOperType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsTunnelPrConfigOperType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelPrConfigOperType (UINT4 u4MplsTunnelIndex,
                                    UINT4 u4MplsTunnelInstance,
                                    UINT4 u4MplsTunnelIngressLSRId,
                                    UINT4 u4MplsTunnelEgressLSRId,
                                    INT4 *pi4RetValFsMplsTunnelPrConfigOperType)
{
    tTeTnlInfo          TeTnlInfo;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TeTnlInfo, TE_ZERO, sizeof (tTeTnlInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllFsMplsTunnelTable (u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&TeTnlInfo));

    *pi4RetValFsMplsTunnelPrConfigOperType = TeTnlInfo.u1TnlProtOperType;

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelSrlgType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsTunnelSrlgType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelSrlgType (UINT4 u4MplsTunnelIndex,
                            UINT4 u4MplsTunnelInstance,
                            UINT4 u4MplsTunnelIngressLSRId,
                            UINT4 u4MplsTunnelEgressLSRId,
                            INT4 *pi4RetValFsMplsTunnelSrlgType)
{
    tTeTnlInfo          TeTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeTnlInfo, TE_ZERO, sizeof (tTeTnlInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllFsMplsTunnelTable (u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&TeTnlInfo));

    MPLS_CMN_UNLOCK ();

    *pi4RetValFsMplsTunnelSrlgType = (INT4) TeTnlInfo.u1TnlSrlgType;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelIfIndex
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsTunnelIfIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelIfIndex (UINT4 u4MplsTunnelIndex,
                           UINT4 u4MplsTunnelInstance,
                           UINT4 u4MplsTunnelIngressLSRId,
                           UINT4 u4MplsTunnelEgressLSRId,
                           INT4 *pi4RetValFsMplsTunnelIfIndex)
{
    tTeTnlInfo          TeTnlInfo;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TeTnlInfo, TE_ZERO, sizeof (tTeTnlInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllFsMplsTunnelTable (u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&TeTnlInfo));
    MPLS_CMN_UNLOCK ();

    *pi4RetValFsMplsTunnelIfIndex = (INT4) TeTnlInfo.u4TnlIfIndex;
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelInitReOptimize
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsTunnelInitReOptimize
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelInitReOptimize (UINT4 u4MplsTunnelIndex,
                                  UINT4 u4MplsTunnelInstance,
                                  UINT4 u4MplsTunnelIngressLSRId,
                                  UINT4 u4MplsTunnelEgressLSRId,
                                  INT4 *pi4RetValFsMplsTunnelInitReOptimize)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();
    
	if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

	*pi4RetValFsMplsTunnelInitReOptimize = TE_TNL_REOPTIMIZE_STATUS(pTeTnlInfo);

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelIsProtectingLsp
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsTunnelIsProtectingLsp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelIsProtectingLsp (UINT4 u4MplsTunnelIndex,
                                   UINT4 u4MplsTunnelInstance,
                                   UINT4 u4MplsTunnelIngressLSRId,
                                   UINT4 u4MplsTunnelEgressLSRId,
                                   INT4 *pi4RetValFsMplsTunnelIsProtectingLsp)
{
    tTeTnlInfo          TeTnlInfo;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TeTnlInfo, TE_ZERO, sizeof (tTeTnlInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllFsMplsTunnelTable (u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&TeTnlInfo));

    MPLS_CMN_UNLOCK ();

    *pi4RetValFsMplsTunnelIsProtectingLsp = (INT4) TeTnlInfo.u1TnlPathType;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLspTunnelMapIndex
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsLspTunnelMapIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLspTunnelMapIndex (UINT4 u4MplsTunnelIndex,
                               UINT4 u4MplsTunnelInstance,
                               UINT4 u4MplsTunnelIngressLSRId,
                               UINT4 u4MplsTunnelEgressLSRId,
                               UINT4 *pu4RetValFsMplsLspTunnelMapIndex)
{
    tTeTnlInfo          TeTnlInfo;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TeTnlInfo, TE_ZERO, sizeof (tTeTnlInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllFsMplsTunnelTable (u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&TeTnlInfo));

    MPLS_CMN_UNLOCK ();

    *pu4RetValFsMplsLspTunnelMapIndex = TeTnlInfo.u4TnlMapIndex;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLspTunnelMapInstance
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsLspTunnelMapInstance
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLspTunnelMapInstance (UINT4 u4MplsTunnelIndex,
                                  UINT4 u4MplsTunnelInstance,
                                  UINT4 u4MplsTunnelIngressLSRId,
                                  UINT4 u4MplsTunnelEgressLSRId,
                                  UINT4 *pu4RetValFsMplsLspTunnelMapInstance)
{
    tTeTnlInfo          TeTnlInfo;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TeTnlInfo, TE_ZERO, sizeof (tTeTnlInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllFsMplsTunnelTable (u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&TeTnlInfo));

    MPLS_CMN_UNLOCK ();

    *pu4RetValFsMplsLspTunnelMapInstance = TeTnlInfo.u4TnlMapInstance;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLspTunnelMapIngressLSRId
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsLspTunnelMapIngressLSRId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLspTunnelMapIngressLSRId (UINT4 u4MplsTunnelIndex,
                                      UINT4 u4MplsTunnelInstance,
                                      UINT4 u4MplsTunnelIngressLSRId,
                                      UINT4 u4MplsTunnelEgressLSRId,
                                      UINT4
                                      *pu4RetValFsMplsLspTunnelMapIngressLSRId)
{
    tTeTnlInfo          TeTnlInfo;
    UINT4               u4MapIngressLSRId = TE_ZERO;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TeTnlInfo, TE_ZERO, sizeof (tTeTnlInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllFsMplsTunnelTable (u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&TeTnlInfo));

    MPLS_CMN_UNLOCK ();

    CONVERT_TO_INTEGER (TeTnlInfo.TnlMapIngressLsrId, u4MapIngressLSRId);
    u4MapIngressLSRId = OSIX_NTOHL (u4MapIngressLSRId);

    *pu4RetValFsMplsLspTunnelMapIngressLSRId = u4MapIngressLSRId;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMplsLspTunnelMapEgressLSRId
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsLspTunnelMapEgressLSRId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsLspTunnelMapEgressLSRId (UINT4 u4MplsTunnelIndex,
                                     UINT4 u4MplsTunnelInstance,
                                     UINT4 u4MplsTunnelIngressLSRId,
                                     UINT4 u4MplsTunnelEgressLSRId,
                                     UINT4
                                     *pu4RetValFsMplsLspTunnelMapEgressLSRId)
{
    tTeTnlInfo          TeTnlInfo;
    UINT4               u4MapEgressLSRId = TE_ZERO;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TeTnlInfo, TE_ZERO, sizeof (tTeTnlInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllFsMplsTunnelTable (u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&TeTnlInfo));

    MPLS_CMN_UNLOCK ();

    CONVERT_TO_INTEGER (TeTnlInfo.TnlMapEgressLsrId, u4MapEgressLSRId);
    u4MapEgressLSRId = OSIX_NTOHL (u4MapEgressLSRId);

    *pu4RetValFsMplsLspTunnelMapEgressLSRId = u4MapEgressLSRId;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelSynchronizationStatus
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsTunnelSynchronizationStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelSynchronizationStatus (UINT4 u4MplsTunnelIndex,
                                         UINT4 u4MplsTunnelInstance,
                                         UINT4 u4MplsTunnelIngressLSRId,
                                         UINT4 u4MplsTunnelEgressLSRId,
                                         INT4
                                         *pi4RetValFsMplsTunnelSynchronizationStatus)
{
    tTeTnlInfo          TeTnlInfo;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TeTnlInfo, TE_ZERO, sizeof (tTeTnlInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllFsMplsTunnelTable (u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&TeTnlInfo));

    MPLS_CMN_UNLOCK ();

    *pi4RetValFsMplsTunnelSynchronizationStatus = TeTnlInfo.u1TnlSyncStatus;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelOutPathMsgId
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsTunnelOutPathMsgId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelOutPathMsgId (UINT4 u4MplsTunnelIndex,
                                UINT4 u4MplsTunnelInstance,
                                UINT4 u4MplsTunnelIngressLSRId,
                                UINT4 u4MplsTunnelEgressLSRId,
                                INT4 *pi4RetValFsMplsTunnelOutPathMsgId)
{
    tTeTnlInfo          TeTnlInfo;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TeTnlInfo, TE_ZERO, sizeof (tTeTnlInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllFsMplsTunnelTable (u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&TeTnlInfo));

    MPLS_CMN_UNLOCK ();

    *pi4RetValFsMplsTunnelOutPathMsgId = (INT4)TeTnlInfo.u4OutPathMsgId;

    return i1RetVal;
}
/****************************************************************************
 Function    :  nmhGetFsMplsTunnelBackupHopTableIndex
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object
                retValFsMplsTunnelBackupHopTableIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMplsTunnelBackupHopTableIndex(UINT4 u4MplsTunnelIndex , UINT4 u4MplsTunnelInstance , 
					   UINT4 u4MplsTunnelIngressLSRId , UINT4 u4MplsTunnelEgressLSRId , 
					   UINT4 *pu4RetValFsMplsTunnelBackupHopTableIndex)
{

	tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);

    MPLS_CMN_UNLOCK ();
    if (pTeTnlInfo != NULL)
    {
        *pu4RetValFsMplsTunnelBackupHopTableIndex = TE_TNL_BACKUPHOP_TABLE_INDEX (pTeTnlInfo);
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelBackupPathInUse
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object
                retValFsMplsTunnelBackupPathInUse
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMplsTunnelBackupPathInUse(UINT4 u4MplsTunnelIndex , UINT4 u4MplsTunnelInstance ,
				       UINT4 u4MplsTunnelIngressLSRId , UINT4 u4MplsTunnelEgressLSRId , 
				       UINT4 *pu4RetValFsMplsTunnelBackupPathInUse)
{

	tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
	
        MPLS_CMN_UNLOCK ();
    if (pTeTnlInfo != NULL)
    {
        *pu4RetValFsMplsTunnelBackupPathInUse
            = (UINT4) (TE_TNL_BACKUPPATH_IN_USE (pTeTnlInfo));
        return SNMP_SUCCESS;
    }
    return SNMP_FAILURE;

}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMplsTunnelType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValFsMplsTunnelType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsTunnelType (UINT4 u4MplsTunnelIndex, UINT4 u4MplsTunnelInstance,
                        UINT4 u4MplsTunnelIngressLSRId,
                        UINT4 u4MplsTunnelEgressLSRId,
                        tSNMP_OCTET_STRING_TYPE * pSetValFsMplsTunnelType)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4RetVal = 0;
    UINT1               u1IsRpteAdminDown = TE_FALSE;

    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo ((UINT4) u4MplsTunnelIndex,
                                  u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (!((pSetValFsMplsTunnelType->pu1_OctetList[0] & TE_TNL_TYPE_HLSP) ||
          ((UINT1) pTeTnlInfo->u4TnlType & TE_TNL_TYPE_HLSP)))
    {
        if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
        {
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    /* If the previous type and current type are same, return success */
    if (((pSetValFsMplsTunnelType->pu1_OctetList[0] &
          (TE_TNL_TYPE (pTeTnlInfo))) == TE_TNL_TYPE (pTeTnlInfo)) &&
        ((pSetValFsMplsTunnelType->pu1_OctetList[0] &
          (TE_TNL_TYPE (pTeTnlInfo))) ==
         pSetValFsMplsTunnelType->pu1_OctetList[0]))
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }

    /* H-LSP/S-LSP tunnel is reconfigured as normal mpls tunnel */
    if (((TE_TNL_TYPE_HLSP & TE_TNL_TYPE (pTeTnlInfo)) ||
         (TE_TNL_TYPE_SLSP & TE_TNL_TYPE (pTeTnlInfo))) &&
        (pSetValFsMplsTunnelType->pu1_OctetList[0] & TE_TNL_TYPE_MPLS))
    {
        TeDeInitHLSPParams (pTeTnlInfo);
    }

    /*Check for Tunnel Type change from MPLS to GMPLS and vice versa */
    if ((pTeTnlInfo->u4TnlType !=
         (UINT1) pSetValFsMplsTunnelType->pu1_OctetList[0]) &&
        (((pSetValFsMplsTunnelType->pu1_OctetList[0] & TE_TNL_TYPE_MPLS &&
           pTeTnlInfo->u4TnlType & TE_TNL_TYPE_GMPLS)) ||
         ((pSetValFsMplsTunnelType->pu1_OctetList[0] & TE_TNL_TYPE_GMPLS) &&
          (pTeTnlInfo->u4TnlType & TE_TNL_TYPE_MPLS))))
    {
        pTeTnlInfo->bIsMbbRequired = TRUE;
    }

    /*Check for Tunnel Type change frome (MPLS + HLSP) or (GMPLS + HLSP) to
       MPLS or GMPLS respectively and viceversa */

    if ((pTeTnlInfo->u4TnlType !=
         (UINT1) pSetValFsMplsTunnelType->pu1_OctetList[0]) &&
        ((pSetValFsMplsTunnelType->pu1_OctetList[0] & TE_TNL_TYPE_HLSP) ||
         (pTeTnlInfo->u4TnlType & TE_TNL_TYPE_HLSP)))
    {
        if (TE_TNL_ADMIN_STATUS (pTeTnlInfo) == TE_ADMIN_UP)
        {
            if (TeRpteTEEventHandler (TE_TNL_TX_PATH_MSG, pTeTnlInfo,
                                      &u1IsRpteAdminDown) == TE_RPTE_FAILURE)
            {
                TE_DBG (TE_MAIN_SNMP, "Posting to RSVP-TE failed\n");
            }
        }

    }

    /* Set the tunnel type */
    pTeTnlInfo->u4TnlType = pSetValFsMplsTunnelType->pu1_OctetList[0];

    /* STATIC_HLSP */
    if ((TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_HLSP) ||
        (TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_SLSP))
    {
        /* Initialise the H-LSP/S-LSP Parameters when the type is set 
         * as H-LSP/S-LSP.*/
        u4RetVal = TeInitHLSPParams (pTeTnlInfo);
        if (u4RetVal == TE_FAILURE)
        {
            TE_TNL_TYPE (pTeTnlInfo) = TE_TNL_TYPE_MPLS;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    if (TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_P2MP)
    {
        /* Since Egress LSR ID refers to P2MP identifier, set egress ID map 
         * info for the tunnel */
        pTeTnlInfo->u4TnlLsrIdMapInfo |= TE_TNL_EGRESSID_MAP_INFO;
    }

    MPLS_CMN_UNLOCK ();

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsTunnelLSRIdMapInfo
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValFsMplsTunnelLSRIdMapInfo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsTunnelLSRIdMapInfo (UINT4 u4MplsTunnelIndex,
                                UINT4 u4MplsTunnelInstance,
                                UINT4 u4MplsTunnelIngressLSRId,
                                UINT4 u4MplsTunnelEgressLSRId,
                                tSNMP_OCTET_STRING_TYPE *
                                pSetValFsMplsTunnelLSRIdMapInfo)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo ((UINT4) u4MplsTunnelIndex,
                                  u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        if (TE_TNL_ROW_STATUS (pTeTnlInfo) != TE_ACTIVE)
        {
            pTeTnlInfo->u4TnlLsrIdMapInfo =
                pSetValFsMplsTunnelLSRIdMapInfo->pu1_OctetList[0];
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsTunnelMode
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValFsMplsTunnelMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsTunnelMode (UINT4 u4MplsTunnelIndex, UINT4 u4MplsTunnelInstance,
                        UINT4 u4MplsTunnelIngressLSRId,
                        UINT4 u4MplsTunnelEgressLSRId,
                        INT4 i4SetValFsMplsTunnelMode)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo ((UINT4) u4MplsTunnelIndex,
                                  u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        if (TE_TNL_ROW_STATUS (pTeTnlInfo) != TE_ACTIVE)
        {
            if (pTeTnlInfo->u4TnlMode != (UINT4) i4SetValFsMplsTunnelMode)
            {
                pTeTnlInfo->bIsMbbRequired = TRUE;
            }

            pTeTnlInfo->u4TnlMode = (UINT4)i4SetValFsMplsTunnelMode;
            pTeTnlInfo->GmplsTnlInfo.u1Direction
                = (UINT1) i4SetValFsMplsTunnelMode;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsTunnelMBBStatus
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValFsMplsTunnelMBBStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsTunnelMBBStatus (UINT4 u4MplsTunnelIndex,
                             UINT4 u4MplsTunnelInstance,
                             UINT4 u4MplsTunnelIngressLSRId,
                             UINT4 u4MplsTunnelEgressLSRId,
                             INT4 i4SetValFsMplsTunnelMBBStatus)
{
    tTeTnlInfo          TeTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeTnlInfo, TE_ZERO, sizeof (tTeTnlInfo));

    TeTnlInfo.u1TnlMBBStatus = (UINT1) i4SetValFsMplsTunnelMBBStatus;

    MPLS_CMN_LOCK ();

    i1RetVal = TeSetAllFsMplsTunnelTable (u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&TeTnlInfo), TE_FS_MPLS_MBB_STATUS);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMplsTunnelDisJointType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValFsMplsTunnelDisJointType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsTunnelDisJointType (UINT4 u4MplsTunnelIndex,
                                UINT4 u4MplsTunnelInstance,
                                UINT4 u4MplsTunnelIngressLSRId,
                                UINT4 u4MplsTunnelEgressLSRId,
                                INT4 i4SetValFsMplsTunnelDisJointType)
{
    UNUSED_PARAM (u4MplsTunnelIndex);
    UNUSED_PARAM (u4MplsTunnelInstance);
    UNUSED_PARAM (u4MplsTunnelIngressLSRId);
    UNUSED_PARAM (u4MplsTunnelEgressLSRId);
    UNUSED_PARAM (i4SetValFsMplsTunnelDisJointType);

    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsTunnelAttPointer
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValFsMplsTunnelAttPointer
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsTunnelAttPointer (UINT4 u4MplsTunnelIndex,
                              UINT4 u4MplsTunnelInstance,
                              UINT4 u4MplsTunnelIngressLSRId,
                              UINT4 u4MplsTunnelEgressLSRId,
                              tSNMP_OID_TYPE * pSetValFsMplsTunnelAttPointer)
{
    tTeTnlInfo          TeTnlInfo;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TeTnlInfo, TE_ZERO, sizeof (tTeTnlInfo));

    TeTnlInfo.u4AttrParamIndex
        = pSetValFsMplsTunnelAttPointer->pu4_OidList
        [pSetValFsMplsTunnelAttPointer->u4_Length - TE_ONE];

    MPLS_CMN_LOCK ();

    i1RetVal = TeSetAllFsMplsTunnelTable (u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          &TeTnlInfo, TE_FS_MPLS_ATTR_PTR);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMplsTunnelEndToEndProtection
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValFsMplsTunnelEndToEndProtection
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsTunnelEndToEndProtection (UINT4 u4MplsTunnelIndex,
                                      UINT4 u4MplsTunnelInstance,
                                      UINT4 u4MplsTunnelIngressLSRId,
                                      UINT4 u4MplsTunnelEgressLSRId,
                                      tSNMP_OCTET_STRING_TYPE
                                      * pSetValFsMplsTunnelEndToEndProtection)
{
    tTeTnlInfo          TeTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeTnlInfo, TE_ZERO, sizeof (tTeTnlInfo));

    TeTnlInfo.GmplsTnlInfo.i4E2EProtectionType =
        pSetValFsMplsTunnelEndToEndProtection->pu1_OctetList[0];

    MPLS_CMN_LOCK ();

    i1RetVal = TeSetAllFsMplsTunnelTable (u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&TeTnlInfo),
                                          TE_FS_MPLS_END_TO_END_PROTECTION);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMplsTunnelPrConfigOperType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValFsMplsTunnelPrConfigOperType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsTunnelPrConfigOperType (UINT4 u4MplsTunnelIndex,
                                    UINT4 u4MplsTunnelInstance,
                                    UINT4 u4MplsTunnelIngressLSRId,
                                    UINT4 u4MplsTunnelEgressLSRId,
                                    INT4 i4SetValFsMplsTunnelPrConfigOperType)
{
    tTeTnlInfo          TeTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeTnlInfo, TE_ZERO, sizeof (tTeTnlInfo));

    TeTnlInfo.u1TnlProtOperType = (UINT1) i4SetValFsMplsTunnelPrConfigOperType;

    MPLS_CMN_LOCK ();

    i1RetVal = TeSetAllFsMplsTunnelTable (u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&TeTnlInfo),
                                          TE_FS_MPLS_PR_CONFIG_OPER_TYPE);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMplsTunnelSrlgType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValFsMplsTunnelSrlgType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsTunnelSrlgType (UINT4 u4MplsTunnelIndex, UINT4 u4MplsTunnelInstance,
                            UINT4 u4MplsTunnelIngressLSRId,
                            UINT4 u4MplsTunnelEgressLSRId,
                            INT4 i4SetValFsMplsTunnelSrlgType)
{
    tTeTnlInfo          TeTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeTnlInfo, TE_ZERO, sizeof (tTeTnlInfo));

    TeTnlInfo.u1TnlSrlgType = (UINT1) i4SetValFsMplsTunnelSrlgType;

    MPLS_CMN_LOCK ();

    i1RetVal = TeSetAllFsMplsTunnelTable (u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&TeTnlInfo), TE_FS_MPLS_SRLG_TYPE);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMplsTunnelIfIndex
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValFsMplsTunnelIfIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsTunnelIfIndex (UINT4 u4MplsTunnelIndex,
                           UINT4 u4MplsTunnelInstance,
                           UINT4 u4MplsTunnelIngressLSRId,
                           UINT4 u4MplsTunnelEgressLSRId,
                           INT4 i4SetValFsMplsTunnelIfIndex)
{
    tTeTnlInfo          TeTnlInfo;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TeTnlInfo, TE_ZERO, sizeof (tTeTnlInfo));

    TeTnlInfo.u4TnlIfIndex = (UINT4) i4SetValFsMplsTunnelIfIndex;

    MPLS_CMN_LOCK ();

    i1RetVal = TeSetAllFsMplsTunnelTable (u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          &TeTnlInfo,
                                          TE_FS_MPLS_TUNNEL_IFINDEX);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhSetFsMplsTunnelInitReOptimize
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValFsMplsTunnelInitReOptimize
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsTunnelInitReOptimize (UINT4 u4MplsTunnelIndex,
                                  UINT4 u4MplsTunnelInstance,
                                  UINT4 u4MplsTunnelIngressLSRId,
                                  UINT4 u4MplsTunnelEgressLSRId,
                                  INT4 i4SetValFsMplsTunnelInitReOptimize)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();
    
	if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

	TE_TNL_REOPTIMIZE_STATUS(pTeTnlInfo) = (UINT1)i4SetValFsMplsTunnelInitReOptimize;

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsMplsTunnelIsProtectingLsp
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValFsMplsTunnelIsProtectingLsp
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsTunnelIsProtectingLsp (UINT4 u4MplsTunnelIndex,
                                   UINT4 u4MplsTunnelInstance,
                                   UINT4 u4MplsTunnelIngressLSRId,
                                   UINT4 u4MplsTunnelEgressLSRId,
                                   INT4 i4SetValFsMplsTunnelIsProtectingLsp)
{
    tTeTnlInfo          TeTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeTnlInfo, TE_ZERO, sizeof (tTeTnlInfo));

    TeTnlInfo.u1TnlPathType = (UINT1) i4SetValFsMplsTunnelIsProtectingLsp;

    MPLS_CMN_LOCK ();

    i1RetVal = TeSetAllFsMplsTunnelTable (u4MplsTunnelIndex,
                                          u4MplsTunnelInstance,
                                          u4MplsTunnelIngressLSRId,
                                          u4MplsTunnelEgressLSRId,
                                          (&TeTnlInfo),
                                          TE_FS_MPLS_TNL_PATH_TYPE);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsMplsTunnelBackupHopTableIndex
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object
                setValFsMplsTunnelBackupHopTableIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMplsTunnelBackupHopTableIndex(UINT4 u4MplsTunnelIndex , UINT4 u4MplsTunnelInstance ,
					   UINT4 u4MplsTunnelIngressLSRId , UINT4 u4MplsTunnelEgressLSRId , 
					   UINT4 u4SetValFsMplsTunnelBackupHopTableIndex)
{
	

	tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex,
                                  u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
	
    MPLS_CMN_UNLOCK ();
    if (pTeTnlInfo != NULL)
    {
        if (TE_TNL_ROW_STATUS (pTeTnlInfo) != TE_ACTIVE)
        {
            if (TE_TNL_BACKUPHOP_TABLE_INDEX (pTeTnlInfo) !=
                u4SetValFsMplsTunnelBackupHopTableIndex)
            {
                pTeTnlInfo->bIsMbbRequired = TRUE;
            }

            TE_TNL_BACKUPHOP_TABLE_INDEX (pTeTnlInfo) =
                u4SetValFsMplsTunnelBackupHopTableIndex;
            return SNMP_SUCCESS;
        }
    }
    return SNMP_FAILURE;

}
/****************************************************************************
 Function    :  nmhSetFsMplsTunnelBackupPathInUse
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object
                setValFsMplsTunnelBackupPathInUse
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMplsTunnelBackupPathInUse(UINT4 u4MplsTunnelIndex , UINT4 u4MplsTunnelInstance ,
				       UINT4 u4MplsTunnelIngressLSRId , UINT4 u4MplsTunnelEgressLSRId , 
				       UINT4 u4SetValFsMplsTunnelBackupPathInUse)
{
	tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    MPLS_CMN_UNLOCK ();
    if (pTeTnlInfo != NULL)
    {
        /* Association of pTepathInfo with TetnlInfo is done here */
        if (TE_TNL_ROW_STATUS (pTeTnlInfo) != TE_ACTIVE)
        {
            TE_TNL_BACKUPPATH_IN_USE (pTeTnlInfo) =
                (UINT1) u4SetValFsMplsTunnelBackupPathInUse;
        }
    }
    return SNMP_SUCCESS;

}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMplsTunnelType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValFsMplsTunnelType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsTunnelType (UINT4 *pu4ErrorCode, UINT4 u4MplsTunnelIndex,
                           UINT4 u4MplsTunnelInstance,
                           UINT4 u4MplsTunnelIngressLSRId,
                           UINT4 u4MplsTunnelEgressLSRId,
                           tSNMP_OCTET_STRING_TYPE * pTestValFsMplsTunnelType)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* Allowed tunnel indices 1 to 65535 as per MPLS-TC-STD-MIB [RFC 3811] */
    if ((u4MplsTunnelIndex < TE_TNLINDEX_MINVAL) ||
        (u4MplsTunnelIndex > TE_TNLINDEX_MAXVAL))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /*Checking whether the Tunnel Instance lies between min and max values */
    if (u4MplsTunnelInstance > TE_TNLINST_MAXVAL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo ((UINT4) u4MplsTunnelIndex,
                                  u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (pTestValFsMplsTunnelType->i4_Length != sizeof (UINT1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    switch (pTestValFsMplsTunnelType->pu1_OctetList[0])
    {
        case TE_TNL_TYPE_GMPLS:
            /* Intentional fall-through */
        case TE_TNL_TYPE_MPLS:
            /* Intentional fall-through */
        case TE_TNL_TYPE_MPLSTP:
            if (TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_HLSP)
            {
                if (pTeTnlInfo->u4NoOfStackedTunnels != TE_ZERO)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }

                if ((TE_HLSP_INFO (pTeTnlInfo) != NULL) &&
                    (TE_HLSP_NO_OF_STACKED_TNLS (pTeTnlInfo) > TE_ZERO) &&
                    (u4MplsTunnelInstance != TE_ZERO))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
            }

            if (TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_P2MP)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            /* The tunnel is stitched with other tunnel. The stitching 
             * should be removed before changing the tunnel type.*/
            if ((TE_TNL_TYPE_SLSP & (TE_TNL_TYPE (pTeTnlInfo))) &&
                (u4MplsTunnelInstance != TE_ZERO))
            {
                if (TE_MAP_TNL_INFO (pTeTnlInfo) != NULL)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
            }

            break;

        case (TE_TNL_TYPE_MPLSTP + TE_TNL_TYPE_HLSP):
            /* Intentional fall-through */
        case (TE_TNL_TYPE_MPLS + TE_TNL_TYPE_HLSP):
            /* Intentional fall-through */
        case TE_TNL_TYPE_HLSP:
            /* Check if No of HLSP exceeds the limit */
            if (gu4TeNoOfHlsp >= MAX_TE_HLSP)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            /* If the current tunnel type is P2MP, it cannot be
               configured as HLSP */
            if (TE_TNL_TYPE_P2MP & (TE_TNL_TYPE (pTeTnlInfo)))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            /* A Tunnel cannot act as a HLSP if it is 
             * stitched with other tunnel */
            if (TE_MAP_TNL_INFO (pTeTnlInfo) != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            /* Traffic parameters needs to be updated before configuring tunnel as HLSP */
            if (TE_TRFC_PARAMS_INFO (gTeGblInfo) == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            break;

        case (TE_TNL_TYPE_MPLS + TE_TNL_TYPE_SLSP):
        case (TE_TNL_TYPE_MPLSTP + TE_TNL_TYPE_SLSP):
        case TE_TNL_TYPE_SLSP:
            /* Check if No of S-LSP exceeds the limit */
            if (gu4TeNoOfSlsp >= MAX_TE_SLSP)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            /* If the current tunnel type is P2MP, it cannot be
               configured as SLSP */
            if (TE_TNL_TYPE_P2MP & (TE_TNL_TYPE (pTeTnlInfo)))
            {
                *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            /* Signaling is not supported */
            if (TE_TNL_SIGPROTO (pTeTnlInfo) != TE_SIGPROTO_NONE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            /* Traffic parameters needs to be updated before 
             * configuring tunnel as S-LSP */
            if (TE_TRFC_PARAMS_INFO (gTeGblInfo) == NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            break;

        case (TE_TNL_TYPE_GMPLS + TE_TNL_TYPE_P2MP):
        case (TE_TNL_TYPE_GMPLS + TE_TNL_TYPE_MPLSTP):
            /* Intentional fall-through */
        case (TE_TNL_TYPE_GMPLS + TE_TNL_TYPE_MPLS):
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
            /*GMPLS tunnel */
        case (TE_TNL_TYPE_GMPLS + TE_TNL_TYPE_HLSP):
        case (TE_TNL_TYPE_GMPLS + TE_TNL_TYPE_SLSP):
            break;

            /* MPLS_P2MP_LSP_CHANGES - S */
        case (TE_TNL_TYPE_MPLS + TE_TNL_TYPE_P2MP):
            /* Intentional fall-through */
        case (TE_TNL_TYPE_MPLSTP + TE_TNL_TYPE_P2MP):
            /* Intentional fall-through */
        case TE_TNL_TYPE_P2MP:
            if (TE_P2MP_CONFIGURED_TNLS (gTeGblInfo) >= MAX_TE_P2MP_TUNNEL_INFO)
            {
                *pu4ErrorCode = SNMP_ERR_NO_CREATION;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            /* MPLS/MPLS-TP tunnel alone can be configured as P2MP */
            if ((TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_GMPLS) ||
                (TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_HLSP))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            /* Signaling is not supported */
            if (TE_TNL_SIGPROTO (pTeTnlInfo) != TE_SIGPROTO_NONE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            if ((TE_EGRESS != TE_TNL_ROLE (pTeTnlInfo))
                && (TE_ZERO != TE_TNL_XCINDEX (pTeTnlInfo)))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            if (TE_ZERO != TE_TNL_HOP_TABLE_INDEX (pTeTnlInfo))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            if (TE_ZERO != TE_TNL_PATH_IN_USE (pTeTnlInfo))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            /* MPLS_P2MP_LSP_CHANGES - E */
            break;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsTunnelLSRIdMapInfo
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValFsMplsTunnelLSRIdMapInfo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsTunnelLSRIdMapInfo (UINT4 *pu4ErrorCode, UINT4 u4MplsTunnelIndex,
                                   UINT4 u4MplsTunnelInstance,
                                   UINT4 u4MplsTunnelIngressLSRId,
                                   UINT4 u4MplsTunnelEgressLSRId,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pTestValFsMplsTunnelLSRIdMapInfo)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* Allowed tunnel indices 1 to 65535 as per MPLS-TC-STD-MIB [RFC 3811] */
    if ((u4MplsTunnelIndex < TE_TNLINDEX_MINVAL) ||
        (u4MplsTunnelIndex > TE_TNLINDEX_MAXVAL))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /*Checking whether the Tunnel Instance lies between min and max values */
    if (u4MplsTunnelInstance > TE_TNLINST_MAXVAL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex,
                                  u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (pTestValFsMplsTunnelLSRIdMapInfo->i4_Length != sizeof (UINT1))

    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    switch (pTestValFsMplsTunnelLSRIdMapInfo->pu1_OctetList[0])
    {
        case TE_TNL_INGRESSID_MAP_INFO:
        case TE_TNL_EGRESSID_MAP_INFO:
        case (TE_TNL_INGRESSID_MAP_INFO + TE_TNL_EGRESSID_MAP_INFO):
        case MPLS_ZERO:        /* For MPLS tunnels */
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
    }
    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsTunnelMode
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValFsMplsTunnelMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsTunnelMode (UINT4 *pu4ErrorCode, UINT4 u4MplsTunnelIndex,
                           UINT4 u4MplsTunnelInstance,
                           UINT4 u4MplsTunnelIngressLSRId,
                           UINT4 u4MplsTunnelEgressLSRId,
                           INT4 i4TestValFsMplsTunnelMode)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* Allowed tunnel indices 1 to 65535 as per MPLS-TC-STD-MIB [RFC 3811] */
    if ((u4MplsTunnelIndex < TE_TNLINDEX_MINVAL) ||
        (u4MplsTunnelIndex > TE_TNLINDEX_MAXVAL))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /*Checking whether the Tunnel Instance lies between min and max values */
    if (u4MplsTunnelInstance > TE_TNLINST_MAXVAL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo ((UINT4) u4MplsTunnelIndex,
                                  u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    switch (i4TestValFsMplsTunnelMode)
    {
        case TE_TNL_MODE_UNIDIRECTIONAL:
        case TE_TNL_MODE_COROUTED_BIDIRECTIONAL:
        case TE_TNL_MODE_ASSOCIATED_BIDIRECTIONAL:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
    }
    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsTunnelMBBStatus
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValFsMplsTunnelMBBStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsTunnelMBBStatus (UINT4 *pu4ErrorCode, UINT4 u4MplsTunnelIndex,
                                UINT4 u4MplsTunnelInstance,
                                UINT4 u4MplsTunnelIngressLSRId,
                                UINT4 u4MplsTunnelEgressLSRId,
                                INT4 i4TestValFsMplsTunnelMBBStatus)
{
    tTeTnlInfo          TeTnlInfo;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TeTnlInfo, TE_ZERO, sizeof (tTeTnlInfo));
    MPLS_CMN_LOCK ();
    TeTnlInfo.u1TnlMBBStatus = (UINT1) i4TestValFsMplsTunnelMBBStatus;

    i1RetVal = TeTestAllFsMplsTunnelTable (pu4ErrorCode,
                                           u4MplsTunnelIndex,
                                           u4MplsTunnelInstance,
                                           u4MplsTunnelIngressLSRId,
                                           u4MplsTunnelEgressLSRId,
                                           &TeTnlInfo, TE_FS_MPLS_MBB_STATUS);

    MPLS_CMN_UNLOCK ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsTunnelDisJointType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValFsMplsTunnelDisJointType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsTunnelDisJointType (UINT4 *pu4ErrorCode, UINT4 u4MplsTunnelIndex,
                                   UINT4 u4MplsTunnelInstance,
                                   UINT4 u4MplsTunnelIngressLSRId,
                                   UINT4 u4MplsTunnelEgressLSRId,
                                   INT4 i4TestValFsMplsTunnelDisJointType)
{
    UNUSED_PARAM (u4MplsTunnelIndex);
    UNUSED_PARAM (u4MplsTunnelInstance);
    UNUSED_PARAM (u4MplsTunnelIngressLSRId);
    UNUSED_PARAM (u4MplsTunnelEgressLSRId);
    UNUSED_PARAM (i4TestValFsMplsTunnelDisJointType);

    *pu4ErrorCode = SNMP_ERR_NOT_WRITABLE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsTunnelAttPointer
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValFsMplsTunnelAttPointer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsTunnelAttPointer (UINT4 *pu4ErrorCode, UINT4 u4MplsTunnelIndex,
                                 UINT4 u4MplsTunnelInstance,
                                 UINT4 u4MplsTunnelIngressLSRId,
                                 UINT4 u4MplsTunnelEgressLSRId,
                                 tSNMP_OID_TYPE *
                                 pTestValFsMplsTunnelAttPointer)
{
    INT1                i1RetVal = SNMP_FAILURE;
    tTeTnlInfo          TeTnlInfo;

    MEMSET (&TeTnlInfo, TE_ZERO, sizeof (tTeTnlInfo));

    if (pTestValFsMplsTunnelAttPointer->u4_Length !=
        TE_TNL_ATTR_TABLE_DEF_OFFSET)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        return SNMP_FAILURE;
    }

    TeTnlInfo.u4AttrParamIndex
        = pTestValFsMplsTunnelAttPointer->pu4_OidList
        [pTestValFsMplsTunnelAttPointer->u4_Length - TE_ONE];

    MPLS_CMN_LOCK ();

    i1RetVal = TeTestAllFsMplsTunnelTable (pu4ErrorCode,
                                           u4MplsTunnelIndex,
                                           u4MplsTunnelInstance,
                                           u4MplsTunnelIngressLSRId,
                                           u4MplsTunnelEgressLSRId,
                                           &TeTnlInfo, TE_FS_MPLS_ATTR_PTR);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsTunnelEndToEndProtection
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValFsMplsTunnelEndToEndProtection
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsTunnelEndToEndProtection (UINT4 *pu4ErrorCode,
                                         UINT4 u4MplsTunnelIndex,
                                         UINT4 u4MplsTunnelInstance,
                                         UINT4 u4MplsTunnelIngressLSRId,
                                         UINT4 u4MplsTunnelEgressLSRId,
                                         tSNMP_OCTET_STRING_TYPE
                                         *
                                         pTestValFsMplsTunnelEndToEndProtection)
{
    tTeTnlInfo          TeTnlInfo;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TeTnlInfo, TE_ZERO, sizeof (tTeTnlInfo));

    TeTnlInfo.GmplsTnlInfo.i4E2EProtectionType =
        pTestValFsMplsTunnelEndToEndProtection->pu1_OctetList[0];

    MPLS_CMN_LOCK ();

    i1RetVal = TeTestAllFsMplsTunnelTable (pu4ErrorCode,
                                           u4MplsTunnelIndex,
                                           u4MplsTunnelInstance,
                                           u4MplsTunnelIngressLSRId,
                                           u4MplsTunnelEgressLSRId,
                                           &TeTnlInfo,
                                           TE_FS_MPLS_END_TO_END_PROTECTION);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsTunnelPrConfigOperType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValFsMplsTunnelPrConfigOperType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsTunnelPrConfigOperType (UINT4 *pu4ErrorCode,
                                       UINT4 u4MplsTunnelIndex,
                                       UINT4 u4MplsTunnelInstance,
                                       UINT4 u4MplsTunnelIngressLSRId,
                                       UINT4 u4MplsTunnelEgressLSRId,
                                       INT4
                                       i4TestValFsMplsTunnelPrConfigOperType)
{
    tTeTnlInfo          TeTnlInfo;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TeTnlInfo, TE_ZERO, sizeof (tTeTnlInfo));
    MPLS_CMN_LOCK ();
    TeTnlInfo.u1TnlProtOperType = (UINT1) i4TestValFsMplsTunnelPrConfigOperType;

    i1RetVal = TeTestAllFsMplsTunnelTable (pu4ErrorCode,
                                           u4MplsTunnelIndex,
                                           u4MplsTunnelInstance,
                                           u4MplsTunnelIngressLSRId,
                                           u4MplsTunnelEgressLSRId,
                                           &TeTnlInfo,
                                           TE_FS_MPLS_PR_CONFIG_OPER_TYPE);

    MPLS_CMN_UNLOCK ();
    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsTunnelSrlgType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValFsMplsTunnelSrlgType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsTunnelSrlgType (UINT4 *pu4ErrorCode, UINT4 u4MplsTunnelIndex,
                               UINT4 u4MplsTunnelInstance,
                               UINT4 u4MplsTunnelIngressLSRId,
                               UINT4 u4MplsTunnelEgressLSRId,
                               INT4 i4TestValFsMplsTunnelSrlgType)
{
    tTeTnlInfo          TeTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeTnlInfo, TE_ZERO, sizeof (tTeTnlInfo));

    TeTnlInfo.u1TnlSrlgType = (UINT1) i4TestValFsMplsTunnelSrlgType;

    MPLS_CMN_LOCK ();

    i1RetVal = TeTestAllFsMplsTunnelTable (pu4ErrorCode,
                                           u4MplsTunnelIndex,
                                           u4MplsTunnelInstance,
                                           u4MplsTunnelIngressLSRId,
                                           u4MplsTunnelEgressLSRId,
                                           (&TeTnlInfo), TE_FS_MPLS_SRLG_TYPE);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsTunnelIfIndex
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValFsMplsTunnelIfIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsTunnelIfIndex (UINT4 *pu4ErrorCode,
                              UINT4 u4MplsTunnelIndex,
                              UINT4 u4MplsTunnelInstance,
                              UINT4 u4MplsTunnelIngressLSRId,
                              UINT4 u4MplsTunnelEgressLSRId,
                              INT4 i4TestValFsMplsTunnelIfIndex)
{

    tTeTnlInfo          TeTnlInfo;
    INT1                i1RetVal = SNMP_FAILURE;

    MEMSET (&TeTnlInfo, TE_ZERO, sizeof (tTeTnlInfo));

    TeTnlInfo.u4TnlIfIndex = (UINT4) i4TestValFsMplsTunnelIfIndex;

    MPLS_CMN_LOCK ();

    i1RetVal = TeTestAllFsMplsTunnelTable (pu4ErrorCode,
                                           u4MplsTunnelIndex,
                                           u4MplsTunnelInstance,
                                           u4MplsTunnelIngressLSRId,
                                           u4MplsTunnelEgressLSRId,
                                           (&TeTnlInfo),
                                           TE_FS_MPLS_TUNNEL_IFINDEX);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsTunnelInitReOptimize
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValFsMplsTunnelInitReOptimize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsTunnelInitReOptimize (UINT4 *pu4ErrorCode,
                                     UINT4 u4MplsTunnelIndex,
                                     UINT4 u4MplsTunnelInstance,
                                     UINT4 u4MplsTunnelIngressLSRId,
                                     UINT4 u4MplsTunnelEgressLSRId,
                                     INT4 i4TestValFsMplsTunnelInitReOptimize)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();
    
	if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
	TE_SUPPRESS_WARNING(i4TestValFsMplsTunnelInitReOptimize);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsTunnelIsProtectingLsp
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValFsMplsTunnelIsProtectingLsp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsTunnelIsProtectingLsp (UINT4 *pu4ErrorCode,
                                      UINT4 u4MplsTunnelIndex,
                                      UINT4 u4MplsTunnelInstance,
                                      UINT4 u4MplsTunnelIngressLSRId,
                                      UINT4 u4MplsTunnelEgressLSRId,
                                      INT4 i4TestValFsMplsTunnelIsProtectingLsp)
{
    tTeTnlInfo          TeTnlInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeTnlInfo, TE_ZERO, sizeof (tTeTnlInfo));

    TeTnlInfo.u1TnlPathType = (UINT1) i4TestValFsMplsTunnelIsProtectingLsp;

    MPLS_CMN_LOCK ();

    i1RetVal = TeTestAllFsMplsTunnelTable (pu4ErrorCode,
                                           u4MplsTunnelIndex,
                                           u4MplsTunnelInstance,
                                           u4MplsTunnelIngressLSRId,
                                           u4MplsTunnelEgressLSRId,
                                           (&TeTnlInfo),
                                           TE_FS_MPLS_TNL_PATH_TYPE);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsTunnelBackupHopTableIndex
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object
                testValFsMplsTunnelBackupHopTableIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMplsTunnelBackupHopTableIndex(UINT4 *pu4ErrorCode , UINT4 u4MplsTunnelIndex ,
					 UINT4 u4MplsTunnelInstance , UINT4 u4MplsTunnelIngressLSRId , 
					 UINT4 u4MplsTunnelEgressLSRId , UINT4 u4TestValFsMplsTunnelBackupHopTableIndex)
{

    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

 /* MPLS_P2MP_LSP_CHANGES - S */
    if (TE_TNL_TYPE_P2MP & TE_TNL_TYPE (pTeTnlInfo))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    /* MPLS_P2MP_LSP_CHANGES - E */

    if (u4TestValFsMplsTunnelBackupHopTableIndex > TE_TNL_HOPLSTINDEX_MAXVAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if ((u4TestValFsMplsTunnelBackupHopTableIndex > (TE_MAX_HOP_LIST (gTeGblInfo) / 2)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhTestv2FsMplsTunnelBackupPathInUse
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object
                testValFsMplsTunnelBackupPathInUse
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMplsTunnelBackupPathInUse(UINT4 *pu4ErrorCode , UINT4 u4MplsTunnelIndex ,
					  UINT4 u4MplsTunnelInstance , UINT4 u4MplsTunnelIngressLSRId , 
					  UINT4 u4MplsTunnelEgressLSRId , UINT4 u4TestValFsMplsTunnelBackupPathInUse)
{

	tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (u4TestValFsMplsTunnelBackupPathInUse > TE_UINT2_SIZE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* MPLS_P2MP_LSP_CHANGES - S */
    if (TE_TNL_TYPE_P2MP & TE_TNL_TYPE (pTeTnlInfo))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    /* MPLS_P2MP_LSP_CHANGES - E */
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;

}


/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsTunnelTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsTunnelTable (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMplsTpTunnelTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsTpTunnelTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMplsTpTunnelTable (UINT4 u4MplsTunnelIndex,
                                             UINT4 u4MplsTunnelInstance,
                                             UINT4 u4MplsTunnelIngressLSRId,
                                             UINT4 u4MplsTunnelEgressLSRId)
{
    return (nmhValidateIndexInstanceMplsTunnelTable (u4MplsTunnelIndex,
                                                     u4MplsTunnelInstance,
                                                     u4MplsTunnelIngressLSRId,
                                                     u4MplsTunnelEgressLSRId));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsTpTunnelTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMplsTpTunnelTable (UINT4 *pu4MplsTunnelIndex,
                                     UINT4 *pu4MplsTunnelInstance,
                                     UINT4 *pu4MplsTunnelIngressLSRId,
                                     UINT4 *pu4MplsTunnelEgressLSRId)
{
    return (nmhGetFirstIndexMplsTunnelTable (pu4MplsTunnelIndex,
                                             pu4MplsTunnelInstance,
                                             pu4MplsTunnelIngressLSRId,
                                             pu4MplsTunnelEgressLSRId));
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMplsTpTunnelTable
 Input       :  The Indices
                MplsTunnelIndex
                nextMplsTunnelIndex
                MplsTunnelInstance
                nextMplsTunnelInstance
                MplsTunnelIngressLSRId
                nextMplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                nextMplsTunnelEgressLSRId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMplsTpTunnelTable (UINT4 u4MplsTunnelIndex,
                                    UINT4 *pu4NextMplsTunnelIndex,
                                    UINT4 u4MplsTunnelInstance,
                                    UINT4 *pu4NextMplsTunnelInstance,
                                    UINT4 u4MplsTunnelIngressLSRId,
                                    UINT4 *pu4NextMplsTunnelIngressLSRId,
                                    UINT4 u4MplsTunnelEgressLSRId,
                                    UINT4 *pu4NextMplsTunnelEgressLSRId)
{
    return (nmhGetNextIndexMplsTunnelTable (u4MplsTunnelIndex,
                                            pu4NextMplsTunnelIndex,
                                            u4MplsTunnelInstance,
                                            pu4NextMplsTunnelInstance,
                                            u4MplsTunnelIngressLSRId,
                                            pu4NextMplsTunnelIngressLSRId,
                                            u4MplsTunnelEgressLSRId,
                                            pu4NextMplsTunnelEgressLSRId));

}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsTpTunnelDestTunnelIndex
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsTpTunnelDestTunnelIndex
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTpTunnelDestTunnelIndex (UINT4 u4MplsTunnelIndex,
                                     UINT4 u4MplsTunnelInstance,
                                     UINT4 u4MplsTunnelIngressLSRId,
                                     UINT4 u4MplsTunnelEgressLSRId,
                                     UINT4
                                     *pu4RetValFsMplsTpTunnelDestTunnelIndex)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo ((UINT4) u4MplsTunnelIndex,
                                  u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        /* Fill the Destination tunnel index information  */
        *pu4RetValFsMplsTpTunnelDestTunnelIndex = pTeTnlInfo->u4DestTnlIndex;
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetFsMplsTpTunnelDestTunnelLspNum
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValFsMplsTpTunnelDestTunnelLspNum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTpTunnelDestTunnelLspNum (UINT4 u4MplsTunnelIndex,
                                      UINT4 u4MplsTunnelInstance,
                                      UINT4 u4MplsTunnelIngressLSRId,
                                      UINT4 u4MplsTunnelEgressLSRId,
                                      UINT4
                                      *pu4RetValFsMplsTpTunnelDestTunnelLspNum)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo ((UINT4) u4MplsTunnelIndex,
                                  u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        /* Fill the destination tunnel LSP number information  */
        *pu4RetValFsMplsTpTunnelDestTunnelLspNum =
            pTeTnlInfo->u4DestTnlInstance;
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMplsTpTunnelDestTunnelIndex
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValFsMplsTpTunnelDestTunnelIndex
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsTpTunnelDestTunnelIndex (UINT4 u4MplsTunnelIndex,
                                     UINT4 u4MplsTunnelInstance,
                                     UINT4 u4MplsTunnelIngressLSRId,
                                     UINT4 u4MplsTunnelEgressLSRId,
                                     UINT4
                                     u4SetValFsMplsTpTunnelDestTunnelIndex)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo ((UINT4) u4MplsTunnelIndex,
                                  u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        if (TE_TNL_ROW_STATUS (pTeTnlInfo) != TE_ACTIVE)
        {
            /* Fill the Destination tunnel index information  */
            pTeTnlInfo->u4DestTnlIndex = u4SetValFsMplsTpTunnelDestTunnelIndex;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhSetFsMplsTpTunnelDestTunnelLspNum
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                setValFsMplsTpTunnelDestTunnelLspNum
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsTpTunnelDestTunnelLspNum (UINT4 u4MplsTunnelIndex,
                                      UINT4 u4MplsTunnelInstance,
                                      UINT4 u4MplsTunnelIngressLSRId,
                                      UINT4 u4MplsTunnelEgressLSRId,
                                      UINT4
                                      u4SetValFsMplsTpTunnelDestTunnelLspNum)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo ((UINT4) u4MplsTunnelIndex,
                                  u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        if (TE_TNL_ROW_STATUS (pTeTnlInfo) != TE_ACTIVE)
        {
            /* Fill the destination tunnel LSP number information  */
            pTeTnlInfo->u4DestTnlInstance =
                u4SetValFsMplsTpTunnelDestTunnelLspNum;
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMplsTpTunnelDestTunnelIndex
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValFsMplsTpTunnelDestTunnelIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsTpTunnelDestTunnelIndex (UINT4 *pu4ErrorCode,
                                        UINT4 u4MplsTunnelIndex,
                                        UINT4 u4MplsTunnelInstance,
                                        UINT4 u4MplsTunnelIngressLSRId,
                                        UINT4 u4MplsTunnelEgressLSRId,
                                        UINT4
                                        u4TestValFsMplsTpTunnelDestTunnelIndex)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* Allowed tunnel indices 1 to 65535 as per MPLS-TC-STD-MIB [RFC 3811] */
    if ((u4MplsTunnelIndex < TE_TNLINDEX_MINVAL) ||
        (u4MplsTunnelIndex > TE_TNLINDEX_MAXVAL))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* Allowed tunnel instance 0 to 65535 as per MPLS-TC-STD-MIB [RFC 3811] */
    if (u4MplsTunnelInstance > TE_TNLINST_MAXVAL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* Allowed tunnel indices 0 to 65535 
     * The value 0 is applicable to reset/dis-associate the reverse tunnel 
     * from the tunnel it is associated with. */
    if (u4TestValFsMplsTpTunnelDestTunnelIndex > TE_TNLINDEX_MAXVAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo ((UINT4) u4MplsTunnelIndex,
                                  u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsTpTunnelDestTunnelLspNum
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValFsMplsTpTunnelDestTunnelLspNum
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsTpTunnelDestTunnelLspNum (UINT4 *pu4ErrorCode,
                                         UINT4 u4MplsTunnelIndex,
                                         UINT4 u4MplsTunnelInstance,
                                         UINT4 u4MplsTunnelIngressLSRId,
                                         UINT4 u4MplsTunnelEgressLSRId,
                                         UINT4
                                         u4TestValFsMplsTpTunnelDestTunnelLspNum)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* Allowed tunnel indices 1 to 65535 as per MPLS-TC-STD-MIB [RFC 3811] */
    if ((u4MplsTunnelIndex < TE_TNLINDEX_MINVAL) ||
        (u4MplsTunnelIndex > TE_TNLINDEX_MAXVAL))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* Checking whether the Tunnel Instance lies between min and max values */
    if (u4MplsTunnelInstance > TE_TNLINST_MAXVAL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (u4TestValFsMplsTpTunnelDestTunnelLspNum > TE_TNLINST_MAXVAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    pTeTnlInfo = TeGetTunnelInfo ((UINT4) u4MplsTunnelIndex,
                                  u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsTpTunnelTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsTpTunnelTable (UINT4 *pu4ErrorCode,
                             tSnmpIndexList * pSnmpIndexList,
                             tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsTunnelAttributeIndexNext
 Input       :  The Indices

                The Object 
                retValFsTunnelAttributeIndexNext
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTunnelAttributeIndexNext (UINT4 *pu4RetValFsTunnelAttributeIndexNext)
{
    UINT4               u4AttrListIndex = TE_ZERO;

    MPLS_CMN_LOCK ();

    *pu4RetValFsTunnelAttributeIndexNext = TE_ZERO;

    u4AttrListIndex = TeGetFreeAttrListIndex ();

    if (u4AttrListIndex == TE_ZERO)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    *pu4RetValFsTunnelAttributeIndexNext = u4AttrListIndex;

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsTunnelAttributeTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsTunnelAttributeTable
 Input       :  The Indices
                FsTunnelAttributeIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsTunnelAttributeTable (UINT4 u4FsTunnelAttributeIndex)
{
    UNUSED_PARAM (u4FsTunnelAttributeIndex);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsTunnelAttributeTable
 Input       :  The Indices
                FsTunnelAttributeIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsTunnelAttributeTable (UINT4 *pu4FsTunnelAttributeIndex)
{
    UINT4               u4AttrListIndex = TE_ZERO;

    *pu4FsTunnelAttributeIndex = TE_ZERO;

    MPLS_CMN_LOCK ();

    u4AttrListIndex = TeGetNextAttrListIndex (u4AttrListIndex);

    if (u4AttrListIndex == TE_ZERO)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    *pu4FsTunnelAttributeIndex = u4AttrListIndex;

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsTunnelAttributeTable
 Input       :  The Indices
                FsTunnelAttributeIndex
                nextFsTunnelAttributeIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsTunnelAttributeTable (UINT4 u4FsTunnelAttributeIndex,
                                       UINT4 *pu4NextFsTunnelAttributeIndex)
{
    UINT4               u4AttrListIndex = u4FsTunnelAttributeIndex;

    *pu4NextFsTunnelAttributeIndex = TE_ZERO;

    MPLS_CMN_LOCK ();

    u4AttrListIndex = TeGetNextAttrListIndex (u4AttrListIndex);

    if (u4AttrListIndex == TE_ZERO)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    *pu4NextFsTunnelAttributeIndex = u4AttrListIndex;

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsTunnelAttributeName
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object 
                retValFsTunnelAttributeName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTunnelAttributeName (UINT4 u4FsTunnelAttributeIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValFsTunnelAttributeName)
{
    tTeAttrListInfo     TeAttrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    MPLS_CMN_LOCK ();

    i1RetVal =
        TeGetAllFsMplsAttributeTable (u4FsTunnelAttributeIndex, &TeAttrInfo);

    MPLS_CMN_UNLOCK ();

    pRetValFsTunnelAttributeName->i4_Length = TeAttrInfo.i4ListNameLen;
    MEMCPY (pRetValFsTunnelAttributeName->pu1_OctetList,
            TeAttrInfo.au1ListName, TeAttrInfo.i4ListNameLen);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsTunnelAttributeSetupPrio
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object 
                retValFsTunnelAttributeSetupPrio
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTunnelAttributeSetupPrio (UINT4 u4FsTunnelAttributeIndex,
                                  INT4 *pi4RetValFsTunnelAttributeSetupPrio)
{
    tTeAttrListInfo     TeAttrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    MPLS_CMN_LOCK ();

    i1RetVal =
        TeGetAllFsMplsAttributeTable (u4FsTunnelAttributeIndex, &TeAttrInfo);

    MPLS_CMN_UNLOCK ();

    *pi4RetValFsTunnelAttributeSetupPrio = (INT4) TeAttrInfo.u1SetupPriority;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsTunnelAttributeHoldingPrio
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object 
                retValFsTunnelAttributeHoldingPrio
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTunnelAttributeHoldingPrio (UINT4 u4FsTunnelAttributeIndex,
                                    INT4 *pi4RetValFsTunnelAttributeHoldingPrio)
{
    tTeAttrListInfo     TeAttrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    MPLS_CMN_LOCK ();

    i1RetVal =
        TeGetAllFsMplsAttributeTable (u4FsTunnelAttributeIndex, &TeAttrInfo);

    MPLS_CMN_UNLOCK ();

    *pi4RetValFsTunnelAttributeHoldingPrio
        = (INT4) TeAttrInfo.u1HoldingPriority;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsTunnelAttributeIncludeAnyAffinity
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object 
                retValFsTunnelAttributeIncludeAnyAffinity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTunnelAttributeIncludeAnyAffinity (UINT4 u4FsTunnelAttributeIndex,
                                           UINT4
                                           *pu4RetValFsTunnelAttributeIncludeAnyAffinity)
{
    tTeAttrListInfo     TeAttrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    MPLS_CMN_LOCK ();

    i1RetVal =
        TeGetAllFsMplsAttributeTable (u4FsTunnelAttributeIndex, (&TeAttrInfo));

    MPLS_CMN_UNLOCK ();

    *pu4RetValFsTunnelAttributeIncludeAnyAffinity =
        TeAttrInfo.u4IncludeAnyAffinity;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsTunnelAttributeIncludeAllAffinity
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object 
                retValFsTunnelAttributeIncludeAllAffinity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTunnelAttributeIncludeAllAffinity (UINT4 u4FsTunnelAttributeIndex,
                                           UINT4
                                           *pu4RetValFsTunnelAttributeIncludeAllAffinity)
{
    tTeAttrListInfo     TeAttrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    MPLS_CMN_LOCK ();

    i1RetVal =
        TeGetAllFsMplsAttributeTable (u4FsTunnelAttributeIndex, (&TeAttrInfo));

    MPLS_CMN_UNLOCK ();

    *pu4RetValFsTunnelAttributeIncludeAllAffinity =
        TeAttrInfo.u4IncludeAllAffinity;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsTunnelAttributeExcludeAnyAffinity
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object 
                retValFsTunnelAttributeExcludeAnyAffinity
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTunnelAttributeExcludeAnyAffinity (UINT4 u4FsTunnelAttributeIndex,
                                           UINT4
                                           *pu4RetValFsTunnelAttributeExcludeAnyAffinity)
{
    tTeAttrListInfo     TeAttrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    MPLS_CMN_LOCK ();

    i1RetVal =
        TeGetAllFsMplsAttributeTable (u4FsTunnelAttributeIndex, (&TeAttrInfo));

    MPLS_CMN_UNLOCK ();

    *pu4RetValFsTunnelAttributeExcludeAnyAffinity =
        TeAttrInfo.u4ExcludeAnyAffinity;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsTunnelAttributeSessionAttributes
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object 
                retValFsTunnelAttributeSessionAttributes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTunnelAttributeSessionAttributes (UINT4 u4FsTunnelAttributeIndex,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pRetValFsTunnelAttributeSessionAttributes)
{
    tTeAttrListInfo     TeAttrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    MPLS_CMN_LOCK ();

    i1RetVal =
        TeGetAllFsMplsAttributeTable (u4FsTunnelAttributeIndex, (&TeAttrInfo));

    MPLS_CMN_UNLOCK ();

    pRetValFsTunnelAttributeSessionAttributes->pu1_OctetList[TE_ZERO] =
        TeAttrInfo.u1SsnAttr;
    pRetValFsTunnelAttributeSessionAttributes->i4_Length =
        TeAttrInfo.u1SsnAttrLen;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsTunnelAttributeBandwidth
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object 
                retValFsTunnelAttributeBandwidth
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTunnelAttributeBandwidth (UINT4 u4FsTunnelAttributeIndex,
                                  UINT4 *pu4RetValFsTunnelAttributeBandwidth)
{
    tTeAttrListInfo     TeAttrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    MPLS_CMN_LOCK ();

    i1RetVal =
        TeGetAllFsMplsAttributeTable (u4FsTunnelAttributeIndex, (&TeAttrInfo));

    MPLS_CMN_UNLOCK ();

    *pu4RetValFsTunnelAttributeBandwidth = TeAttrInfo.u4Bandwidth;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsTunnelAttributeTeClassType
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object 
                retValFsTunnelAttributeTeClassType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTunnelAttributeTeClassType (UINT4 u4FsTunnelAttributeIndex,
                                    INT4 *pi4RetValFsTunnelAttributeTeClassType)
{
    tTeAttrListInfo     TeAttrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    MPLS_CMN_LOCK ();

    i1RetVal =
        TeGetAllFsMplsAttributeTable (u4FsTunnelAttributeIndex, (&TeAttrInfo));

    MPLS_CMN_UNLOCK ();

    *pi4RetValFsTunnelAttributeTeClassType = (INT4) TeAttrInfo.u4ClassType;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsTunnelAttributeSrlgType
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object
                retValFsTunnelAttributeSrlgType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTunnelAttributeSrlgType (UINT4 u4FsTunnelAttributeIndex,
                                 INT4 *pi4RetValFsTunnelAttributeSrlgType)
{
    tTeAttrListInfo     TeAttrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    MPLS_CMN_LOCK ();

    i1RetVal =
        TeGetAllFsMplsAttributeTable (u4FsTunnelAttributeIndex, (&TeAttrInfo));

    MPLS_CMN_UNLOCK ();

    *pi4RetValFsTunnelAttributeSrlgType = (INT4) TeAttrInfo.u1SrlgType;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsTunnelAttributeRowStatus
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object 
                retValFsTunnelAttributeRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTunnelAttributeRowStatus (UINT4 u4FsTunnelAttributeIndex,
                                  INT4 *pi4RetValFsTunnelAttributeRowStatus)
{
    tTeAttrListInfo     TeAttrInfo;

    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    MPLS_CMN_LOCK ();

    i1RetVal =
        TeGetAllFsMplsAttributeTable (u4FsTunnelAttributeIndex, (&TeAttrInfo));

    MPLS_CMN_UNLOCK ();

    *pi4RetValFsTunnelAttributeRowStatus = (INT4) TeAttrInfo.u1RowStatus;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetFsTunnelAttributeMask
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object 
                retValFsTunnelAttributeMask
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTunnelAttributeMask (UINT4 u4FsTunnelAttributeIndex,
                             tSNMP_OCTET_STRING_TYPE
                             * pRetValFsTunnelAttributeMask)
{
    UNUSED_PARAM (u4FsTunnelAttributeIndex);
    UNUSED_PARAM (pRetValFsTunnelAttributeMask);

    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsTunnelAttributeName
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object 
                setValFsTunnelAttributeName
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTunnelAttributeName (UINT4 u4FsTunnelAttributeIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pSetValFsTunnelAttributeName)
{
    tTeAttrListInfo     TeAttrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    MEMCPY (TeAttrInfo.au1ListName,
            pSetValFsTunnelAttributeName->pu1_OctetList,
            pSetValFsTunnelAttributeName->i4_Length);

    TeAttrInfo.i4ListNameLen = pSetValFsTunnelAttributeName->i4_Length;

    MPLS_CMN_LOCK ();

    i1RetVal = TeSetAllFsMplsAttributeTable (u4FsTunnelAttributeIndex,
                                             (&TeAttrInfo),
                                             TE_FS_MPLS_ATTR_NAME);
    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsTunnelAttributeSetupPrio
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object 
                setValFsTunnelAttributeSetupPrio
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTunnelAttributeSetupPrio (UINT4 u4FsTunnelAttributeIndex,
                                  INT4 i4SetValFsTunnelAttributeSetupPrio)
{
    tTeAttrListInfo     TeAttrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    TeAttrInfo.u1SetupPriority = (UINT1)i4SetValFsTunnelAttributeSetupPrio;

    MPLS_CMN_LOCK ();

    i1RetVal = TeSetAllFsMplsAttributeTable (u4FsTunnelAttributeIndex,
                                             (&TeAttrInfo),
                                             TE_FS_MPLS_ATTR_SETUP_PRIOR);
    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsTunnelAttributeHoldingPrio
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object 
                setValFsTunnelAttributeHoldingPrio
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTunnelAttributeHoldingPrio (UINT4 u4FsTunnelAttributeIndex,
                                    INT4 i4SetValFsTunnelAttributeHoldingPrio)
{
    tTeAttrListInfo     TeAttrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    TeAttrInfo.u1HoldingPriority = (UINT1)i4SetValFsTunnelAttributeHoldingPrio;

    MPLS_CMN_LOCK ();

    i1RetVal = TeSetAllFsMplsAttributeTable (u4FsTunnelAttributeIndex,
                                             (&TeAttrInfo),
                                             TE_FS_MPLS_ATTR_HOLDING_PRIOR);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsTunnelAttributeIncludeAnyAffinity
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object 
                setValFsTunnelAttributeIncludeAnyAffinity
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTunnelAttributeIncludeAnyAffinity (UINT4 u4FsTunnelAttributeIndex,
                                           UINT4
                                           u4SetValFsTunnelAttributeIncludeAnyAffinity)
{
    tTeAttrListInfo     TeAttrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    TeAttrInfo.u4IncludeAnyAffinity =
        u4SetValFsTunnelAttributeIncludeAnyAffinity;

    MPLS_CMN_LOCK ();

    i1RetVal = TeSetAllFsMplsAttributeTable (u4FsTunnelAttributeIndex,
                                             (&TeAttrInfo),
                                             TE_FS_MPLS_ATTR_INCLUDE_ANY);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsTunnelAttributeIncludeAllAffinity
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object 
                setValFsTunnelAttributeIncludeAllAffinity
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTunnelAttributeIncludeAllAffinity (UINT4 u4FsTunnelAttributeIndex,
                                           UINT4
                                           u4SetValFsTunnelAttributeIncludeAllAffinity)
{
    tTeAttrListInfo     TeAttrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    TeAttrInfo.u4IncludeAllAffinity =
        u4SetValFsTunnelAttributeIncludeAllAffinity;

    MPLS_CMN_LOCK ();

    i1RetVal = TeSetAllFsMplsAttributeTable (u4FsTunnelAttributeIndex,
                                             (&TeAttrInfo),
                                             TE_FS_MPLS_ATTR_INCLUDE_ALL);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsTunnelAttributeExcludeAnyAffinity
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object 
                setValFsTunnelAttributeExcludeAnyAffinity
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTunnelAttributeExcludeAnyAffinity (UINT4 u4FsTunnelAttributeIndex,
                                           UINT4
                                           u4SetValFsTunnelAttributeExcludeAnyAffinity)
{
    tTeAttrListInfo     TeAttrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    TeAttrInfo.u4ExcludeAnyAffinity =
        u4SetValFsTunnelAttributeExcludeAnyAffinity;

    MPLS_CMN_LOCK ();

    i1RetVal = TeSetAllFsMplsAttributeTable (u4FsTunnelAttributeIndex,
                                             (&TeAttrInfo),
                                             TE_FS_MPLS_ATTR_EXCLUDE_ANY);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsTunnelAttributeSessionAttributes
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object 
                setValFsTunnelAttributeSessionAttributes
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTunnelAttributeSessionAttributes (UINT4 u4FsTunnelAttributeIndex,
                                          tSNMP_OCTET_STRING_TYPE *
                                          pSetValFsTunnelAttributeSessionAttributes)
{
    tTeAttrListInfo     TeAttrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    TeAttrInfo.u1SsnAttr =
        pSetValFsTunnelAttributeSessionAttributes->pu1_OctetList[TE_ZERO];

    TeAttrInfo.u1SsnAttrLen =
        (UINT1)pSetValFsTunnelAttributeSessionAttributes->i4_Length;

    MPLS_CMN_LOCK ();

    i1RetVal = TeSetAllFsMplsAttributeTable (u4FsTunnelAttributeIndex,
                                             (&TeAttrInfo),
                                             TE_FS_MPLS_ATTR_SSN_ATTR);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsTunnelAttributeBandwidth
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object 
                setValFsTunnelAttributeBandwidth
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTunnelAttributeBandwidth (UINT4 u4FsTunnelAttributeIndex,
                                  UINT4 u4SetValFsTunnelAttributeBandwidth)
{
    tTeAttrListInfo     TeAttrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    TeAttrInfo.u4Bandwidth = u4SetValFsTunnelAttributeBandwidth;

    MPLS_CMN_LOCK ();

    i1RetVal = TeSetAllFsMplsAttributeTable (u4FsTunnelAttributeIndex,
                                             (&TeAttrInfo),
                                             TE_FS_MPLS_ATTR_BANDWIDTH);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsTunnelAttributeTeClassType
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object 
                setValFsTunnelAttributeTeClassType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTunnelAttributeTeClassType (UINT4 u4FsTunnelAttributeIndex,
                                    INT4 i4SetValFsTunnelAttributeTeClassType)
{
    tTeAttrListInfo     TeAttrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    TeAttrInfo.u4ClassType = (UINT4) i4SetValFsTunnelAttributeTeClassType;

    MPLS_CMN_LOCK ();

    i1RetVal = TeSetAllFsMplsAttributeTable (u4FsTunnelAttributeIndex,
                                             (&TeAttrInfo),
                                             TE_FS_MPLS_ATTR_CLASS_TYPE);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsTunnelAttributeSrlgType
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object
                setValFsTunnelAttributeSrlgType
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTunnelAttributeSrlgType (UINT4 u4FsTunnelAttributeIndex,
                                 INT4 i4SetValFsTunnelAttributeSrlgType)
{
    tTeAttrListInfo     TeAttrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    TeAttrInfo.u1SrlgType = (UINT1)i4SetValFsTunnelAttributeSrlgType;

    MPLS_CMN_LOCK ();

    i1RetVal = TeSetAllFsMplsAttributeTable (u4FsTunnelAttributeIndex,
                                             (&TeAttrInfo),
                                             TE_FS_MPLS_ATTR_SRLG_TYPE);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsTunnelAttributeRowStatus
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object 
                setValFsTunnelAttributeRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTunnelAttributeRowStatus (UINT4 u4FsTunnelAttributeIndex,
                                  INT4 i4SetValFsTunnelAttributeRowStatus)
{
    tTeAttrListInfo     TeAttrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    TeAttrInfo.u1RowStatus = (UINT1) i4SetValFsTunnelAttributeRowStatus;

    MPLS_CMN_LOCK ();

    i1RetVal = TeSetAllFsMplsAttributeTable (u4FsTunnelAttributeIndex,
                                             (&TeAttrInfo),
                                             TE_FS_MPLS_ATTR_ROW_STATUS);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhSetFsTunnelAttributeMask
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object 
                setValFsTunnelAttributeMask
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTunnelAttributeMask (UINT4 u4FsTunnelAttributeIndex,
                             tSNMP_OCTET_STRING_TYPE
                             * pSetValFsTunnelAttributeMask)
{
    UNUSED_PARAM (u4FsTunnelAttributeIndex);
    UNUSED_PARAM (pSetValFsTunnelAttributeMask);
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsTunnelAttributeName
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object 
                testValFsTunnelAttributeName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTunnelAttributeName (UINT4 *pu4ErrorCode,
                                UINT4 u4FsTunnelAttributeIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValFsTunnelAttributeName)
{
    tTeAttrListInfo     TeAttrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    MEMCPY (TeAttrInfo.au1ListName,
            pTestValFsTunnelAttributeName,
            pTestValFsTunnelAttributeName->i4_Length);

    TeAttrInfo.i4ListNameLen = pTestValFsTunnelAttributeName->i4_Length;

    MPLS_CMN_LOCK ();

    i1RetVal =
        TeTestAllFsMplsAttributeTable (pu4ErrorCode, u4FsTunnelAttributeIndex,
                                       (&TeAttrInfo), TE_FS_MPLS_ATTR_NAME);
    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsTunnelAttributeSetupPrio
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object 
                testValFsTunnelAttributeSetupPrio
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTunnelAttributeSetupPrio (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsTunnelAttributeIndex,
                                     INT4 i4TestValFsTunnelAttributeSetupPrio)
{
    tTeAttrListInfo     TeAttrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    TeAttrInfo.u1SetupPriority = (UINT1) i4TestValFsTunnelAttributeSetupPrio;

    MPLS_CMN_LOCK ();

    i1RetVal =
        TeTestAllFsMplsAttributeTable (pu4ErrorCode, u4FsTunnelAttributeIndex,
                                       (&TeAttrInfo),
                                       TE_FS_MPLS_ATTR_SETUP_PRIOR);
    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsTunnelAttributeHoldingPrio
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object 
                testValFsTunnelAttributeHoldingPrio
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTunnelAttributeHoldingPrio (UINT4 *pu4ErrorCode,
                                       UINT4 u4FsTunnelAttributeIndex,
                                       INT4
                                       i4TestValFsTunnelAttributeHoldingPrio)
{
    tTeAttrListInfo     TeAttrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    TeAttrInfo.u1HoldingPriority
        = (UINT1) i4TestValFsTunnelAttributeHoldingPrio;

    MPLS_CMN_LOCK ();

    i1RetVal =
        TeTestAllFsMplsAttributeTable (pu4ErrorCode, u4FsTunnelAttributeIndex,
                                       (&TeAttrInfo),
                                       TE_FS_MPLS_ATTR_HOLDING_PRIOR);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsTunnelAttributeIncludeAnyAffinity
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object 
                testValFsTunnelAttributeIncludeAnyAffinity
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTunnelAttributeIncludeAnyAffinity (UINT4 *pu4ErrorCode,
                                              UINT4 u4FsTunnelAttributeIndex,
                                              UINT4
                                              u4TestValFsTunnelAttributeIncludeAnyAffinity)
{
    tTeAttrListInfo     TeAttrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    TeAttrInfo.u4IncludeAnyAffinity =
        u4TestValFsTunnelAttributeIncludeAnyAffinity;

    MPLS_CMN_LOCK ();

    i1RetVal =
        TeTestAllFsMplsAttributeTable (pu4ErrorCode, u4FsTunnelAttributeIndex,
                                       (&TeAttrInfo),
                                       TE_FS_MPLS_ATTR_INCLUDE_ANY);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsTunnelAttributeIncludeAllAffinity
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object 
                testValFsTunnelAttributeIncludeAllAffinity
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTunnelAttributeIncludeAllAffinity (UINT4 *pu4ErrorCode,
                                              UINT4 u4FsTunnelAttributeIndex,
                                              UINT4
                                              u4TestValFsTunnelAttributeIncludeAllAffinity)
{
    tTeAttrListInfo     TeAttrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    TeAttrInfo.u4IncludeAllAffinity =
        u4TestValFsTunnelAttributeIncludeAllAffinity;

    MPLS_CMN_LOCK ();

    i1RetVal =
        TeTestAllFsMplsAttributeTable (pu4ErrorCode, u4FsTunnelAttributeIndex,
                                       (&TeAttrInfo),
                                       TE_FS_MPLS_ATTR_INCLUDE_ALL);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsTunnelAttributeExcludeAnyAffinity
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object 
                testValFsTunnelAttributeExcludeAnyAffinity
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTunnelAttributeExcludeAnyAffinity (UINT4 *pu4ErrorCode,
                                              UINT4 u4FsTunnelAttributeIndex,
                                              UINT4
                                              u4TestValFsTunnelAttributeExcludeAnyAffinity)
{
    tTeAttrListInfo     TeAttrInfo;

    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    TeAttrInfo.u4ExcludeAnyAffinity =
        u4TestValFsTunnelAttributeExcludeAnyAffinity;

    MPLS_CMN_LOCK ();

    i1RetVal =
        TeTestAllFsMplsAttributeTable (pu4ErrorCode, u4FsTunnelAttributeIndex,
                                       (&TeAttrInfo),
                                       TE_FS_MPLS_ATTR_EXCLUDE_ANY);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsTunnelAttributeSessionAttributes
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object 
                testValFsTunnelAttributeSessionAttributes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTunnelAttributeSessionAttributes (UINT4 *pu4ErrorCode,
                                             UINT4 u4FsTunnelAttributeIndex,
                                             tSNMP_OCTET_STRING_TYPE *
                                             pTestValFsTunnelAttributeSessionAttributes)
{
    tTeAttrListInfo     TeAttrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    TeAttrInfo.u1SsnAttr =
        pTestValFsTunnelAttributeSessionAttributes->pu1_OctetList[TE_ZERO];
    TeAttrInfo.u1SsnAttrLen =
        (UINT1)pTestValFsTunnelAttributeSessionAttributes->i4_Length;

    MPLS_CMN_LOCK ();

    i1RetVal = TeTestAllFsMplsAttributeTable (pu4ErrorCode,
                                              u4FsTunnelAttributeIndex,
                                              (&TeAttrInfo),
                                              TE_FS_MPLS_ATTR_SSN_ATTR);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsTunnelAttributeBandwidth
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object 
                testValFsTunnelAttributeBandwidth
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTunnelAttributeBandwidth (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsTunnelAttributeIndex,
                                     UINT4 u4TestValFsTunnelAttributeBandwidth)
{
    tTeAttrListInfo     TeAttrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    TeAttrInfo.u4Bandwidth = u4TestValFsTunnelAttributeBandwidth;

    MPLS_CMN_LOCK ();

    i1RetVal =
        TeTestAllFsMplsAttributeTable (pu4ErrorCode, u4FsTunnelAttributeIndex,
                                       (&TeAttrInfo),
                                       TE_FS_MPLS_ATTR_BANDWIDTH);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsTunnelAttributeTeClassType
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object 
                testValFsTunnelAttributeTeClassType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTunnelAttributeTeClassType (UINT4 *pu4ErrorCode,
                                       UINT4 u4FsTunnelAttributeIndex,
                                       INT4
                                       i4TestValFsTunnelAttributeTeClassType)
{
    tTeAttrListInfo     TeAttrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    TeAttrInfo.u4ClassType = (UINT4) i4TestValFsTunnelAttributeTeClassType;

    MPLS_CMN_LOCK ();

    i1RetVal = TeTestAllFsMplsAttributeTable (pu4ErrorCode,
                                              u4FsTunnelAttributeIndex,
                                              (&TeAttrInfo),
                                              TE_FS_MPLS_ATTR_CLASS_TYPE);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************                    
 * Function    :  nmhTestv2FsTunnelAttributeSrlgType
 Input       :  The Indices                                                                                      
                FsTunnelAttributeIndex  
                The Object
                testValFsTunnelAttributeSrlgType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned                                                     
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTunnelAttributeSrlgType (UINT4 *pu4ErrorCode,
                                    UINT4 u4FsTunnelAttributeIndex,
                                    INT4 i4TestValFsTunnelAttributeSrlgType)
{
    tTeAttrListInfo     TeAttrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    TeAttrInfo.u1SrlgType = (UINT1)i4TestValFsTunnelAttributeSrlgType;

    MPLS_CMN_LOCK ();

    i1RetVal = TeTestAllFsMplsAttributeTable (pu4ErrorCode,
                                              u4FsTunnelAttributeIndex,
                                              (&TeAttrInfo),
                                              TE_FS_MPLS_ATTR_SRLG_TYPE);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsTunnelAttributeRowStatus
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object 
                testValFsTunnelAttributeRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTunnelAttributeRowStatus (UINT4 *pu4ErrorCode,
                                     UINT4 u4FsTunnelAttributeIndex,
                                     INT4 i4TestValFsTunnelAttributeRowStatus)
{
    tTeAttrListInfo     TeAttrInfo;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrInfo, TE_ZERO, sizeof (tTeAttrListInfo));

    TeAttrInfo.u1RowStatus = (UINT1) i4TestValFsTunnelAttributeRowStatus;

    MPLS_CMN_LOCK ();

    i1RetVal =
        TeTestAllFsMplsAttributeTable (pu4ErrorCode, u4FsTunnelAttributeIndex,
                                       (&TeAttrInfo),
                                       TE_FS_MPLS_ATTR_ROW_STATUS);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhTestv2FsTunnelAttributeMask
 Input       :  The Indices
                FsTunnelAttributeIndex

                The Object 
                testValFsTunnelAttributeMask
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTunnelAttributeMask (UINT4 *pu4ErrorCode,
                                UINT4 u4FsTunnelAttributeIndex,
                                tSNMP_OCTET_STRING_TYPE
                                * pTestValFsTunnelAttributeMask)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4FsTunnelAttributeIndex);
    UNUSED_PARAM (pTestValFsTunnelAttributeMask);
    return SNMP_FAILURE;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsTunnelAttributeTable
 Input       :  The Indices
                FsTunnelAttributeIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsTunnelAttributeTable (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMplsTunnelSrlgTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsTunnelSrlgTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                FsMplsTunnelSrlgNo
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMplsTunnelSrlgTable (UINT4 u4MplsTunnelIndex,
                                               UINT4 u4MplsTunnelInstance,
                                               UINT4 u4MplsTunnelIngressLSRId,
                                               UINT4 u4MplsTunnelEgressLSRId,
                                               UINT4 u4FsMplsTunnelSrlgNo)
{
    UNUSED_PARAM (u4MplsTunnelIndex);
    UNUSED_PARAM (u4MplsTunnelInstance);
    UNUSED_PARAM (u4MplsTunnelIngressLSRId);
    UNUSED_PARAM (u4MplsTunnelEgressLSRId);
    UNUSED_PARAM (u4FsMplsTunnelSrlgNo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsTunnelSrlgTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                FsMplsTunnelSrlgNo
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMplsTunnelSrlgTable (UINT4 *pu4MplsTunnelIndex,
                                       UINT4 *pu4MplsTunnelInstance,
                                       UINT4 *pu4MplsTunnelIngressLSRId,
                                       UINT4 *pu4MplsTunnelEgressLSRId,
                                       UINT4 *pu4FsMplsTunnelSrlgNo)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlSrlg         *pTeTnlSrlg = NULL;
    UINT4               u4EgressId = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;

    *pu4MplsTunnelIndex = TE_ZERO;
    *pu4MplsTunnelInstance = TE_ZERO;
    *pu4MplsTunnelIngressLSRId = TE_ZERO;
    *pu4MplsTunnelEgressLSRId = TE_ZERO;
    *pu4FsMplsTunnelSrlgNo = TE_ZERO;

    MPLS_CMN_LOCK ();

    pTeTnlInfo = (tTeTnlInfo *) RBTreeGetFirst (gTeGblInfo.TnlInfoTbl);

    while (pTeTnlInfo != NULL)
    {
        pTeTnlSrlg = (tTeTnlSrlg *) TMO_SLL_First (&pTeTnlInfo->SrlgList);

        if (pTeTnlSrlg != NULL)
        {
            *pu4MplsTunnelIndex = pTeTnlInfo->u4TnlIndex;
            *pu4MplsTunnelInstance = pTeTnlInfo->u4TnlInstance;

            CONVERT_TO_INTEGER (pTeTnlInfo->TnlIngressLsrId, u4IngressId);
            CONVERT_TO_INTEGER (pTeTnlInfo->TnlEgressLsrId, u4EgressId);
            u4EgressId = OSIX_NTOHL (u4EgressId);
            u4IngressId = OSIX_NTOHL (u4IngressId);

            *pu4MplsTunnelIngressLSRId = u4IngressId;
            *pu4MplsTunnelEgressLSRId = u4EgressId;

            *pu4FsMplsTunnelSrlgNo = pTeTnlSrlg->u4SrlgNo;

            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
        else
        {
            pTeTnlInfo
                = (tTeTnlInfo *) RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                                (tRBElem *) pTeTnlInfo, NULL);
        }
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMplsTunnelSrlgTable
 Input       :  The Indices
                MplsTunnelIndex
                nextMplsTunnelIndex
                MplsTunnelInstance
                nextMplsTunnelInstance
                MplsTunnelIngressLSRId
                nextMplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                nextMplsTunnelEgressLSRId
                FsMplsTunnelSrlgNo
                nextFsMplsTunnelSrlgNo
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMplsTunnelSrlgTable (UINT4 u4MplsTunnelIndex,
                                      UINT4 *pu4NextMplsTunnelIndex,
                                      UINT4 u4MplsTunnelInstance,
                                      UINT4 *pu4NextMplsTunnelInstance,
                                      UINT4 u4MplsTunnelIngressLSRId,
                                      UINT4 *pu4NextMplsTunnelIngressLSRId,
                                      UINT4 u4MplsTunnelEgressLSRId,
                                      UINT4 *pu4NextMplsTunnelEgressLSRId,
                                      UINT4 u4FsMplsTunnelSrlgNo,
                                      UINT4 *pu4NextFsMplsTunnelSrlgNo)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlSrlg         *pTeTnlSrlg = NULL;
    UINT4               u4EgressId = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    BOOL1               bNextTnlEntryReqd = FALSE;

    *pu4NextMplsTunnelIndex = TE_ZERO;
    *pu4NextMplsTunnelInstance = TE_ZERO;
    *pu4NextMplsTunnelIngressLSRId = TE_ZERO;
    *pu4NextMplsTunnelEgressLSRId = TE_ZERO;
    *pu4NextFsMplsTunnelSrlgNo = TE_ZERO;

    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);

    while (pTeTnlInfo != NULL)
    {
        TMO_SLL_Scan (&pTeTnlInfo->SrlgList, pTeTnlSrlg, tTeTnlSrlg *)
        {
            if ((bNextTnlEntryReqd == TRUE) ||
                (pTeTnlSrlg->u4SrlgNo > u4FsMplsTunnelSrlgNo))
            {
                CONVERT_TO_INTEGER (pTeTnlInfo->TnlIngressLsrId, u4IngressId);
                CONVERT_TO_INTEGER (pTeTnlInfo->TnlEgressLsrId, u4EgressId);
                u4IngressId = OSIX_NTOHL (u4IngressId);
                u4EgressId = OSIX_NTOHL (u4EgressId);

                *pu4NextMplsTunnelIndex = pTeTnlInfo->u4TnlIndex;
                *pu4NextMplsTunnelInstance = pTeTnlInfo->u4TnlInstance;
                *pu4NextMplsTunnelIngressLSRId = u4IngressId;
                *pu4NextMplsTunnelEgressLSRId = u4EgressId;
                *pu4NextFsMplsTunnelSrlgNo = pTeTnlSrlg->u4SrlgNo;

                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
        }

        pTeTnlInfo
            = (tTeTnlInfo *) RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                            (tRBElem *) pTeTnlInfo, NULL);
        bNextTnlEntryReqd = TRUE;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelSrlgRowStatus
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                FsMplsTunnelSrlgNo

                The Object 
                retValFsMplsTunnelSrlgRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelSrlgRowStatus (UINT4 u4MplsTunnelIndex,
                                 UINT4 u4MplsTunnelInstance,
                                 UINT4 u4MplsTunnelIngressLSRId,
                                 UINT4 u4MplsTunnelEgressLSRId,
                                 UINT4 u4FsMplsTunnelSrlgNo,
                                 INT4 *pi4RetValFsMplsTunnelSrlgRowStatus)
{
    tTeTnlSrlg          TeTnlSrlg;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeTnlSrlg, TE_ZERO, sizeof (tTeTnlSrlg));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllFsMplsTunnelSrlgTable (u4MplsTunnelIndex,
                                              u4MplsTunnelInstance,
                                              u4MplsTunnelIngressLSRId,
                                              u4MplsTunnelEgressLSRId,
                                              u4FsMplsTunnelSrlgNo,
                                              (&TeTnlSrlg));

    MPLS_CMN_UNLOCK ();

    *pi4RetValFsMplsTunnelSrlgRowStatus = (INT4)TeTnlSrlg.u4RowStatus;

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMplsTunnelSrlgRowStatus
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                FsMplsTunnelSrlgNo

                The Object 
                setValFsMplsTunnelSrlgRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsTunnelSrlgRowStatus (UINT4 u4MplsTunnelIndex,
                                 UINT4 u4MplsTunnelInstance,
                                 UINT4 u4MplsTunnelIngressLSRId,
                                 UINT4 u4MplsTunnelEgressLSRId,
                                 UINT4 u4FsMplsTunnelSrlgNo,
                                 INT4 i4SetValFsMplsTunnelSrlgRowStatus)
{
    tTeTnlSrlg          TeTnlSrlg;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeTnlSrlg, TE_ZERO, sizeof (tTeTnlSrlg));

    MPLS_CMN_LOCK ();

    TeTnlSrlg.u4RowStatus = (UINT4) i4SetValFsMplsTunnelSrlgRowStatus;

    i1RetVal = TeSetAllFsMplsTunnelSrlgTable (u4MplsTunnelIndex,
                                              u4MplsTunnelInstance,
                                              u4MplsTunnelIngressLSRId,
                                              u4MplsTunnelEgressLSRId,
                                              u4FsMplsTunnelSrlgNo,
                                              (&TeTnlSrlg),
                                              TE_FS_MPLS_SRLG_ROW_STATUS);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMplsTunnelSrlgRowStatus
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                FsMplsTunnelSrlgNo

                The Object 
                testValFsMplsTunnelSrlgRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsTunnelSrlgRowStatus (UINT4 *pu4ErrorCode,
                                    UINT4 u4MplsTunnelIndex,
                                    UINT4 u4MplsTunnelInstance,
                                    UINT4 u4MplsTunnelIngressLSRId,
                                    UINT4 u4MplsTunnelEgressLSRId,
                                    UINT4 u4FsMplsTunnelSrlgNo,
                                    INT4 i4TestValFsMplsTunnelSrlgRowStatus)
{
    tTeTnlSrlg          TeTnlSrlg;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeTnlSrlg, TE_ZERO, sizeof (tTeTnlSrlg));

    MPLS_CMN_LOCK ();

    TeTnlSrlg.u4RowStatus = (UINT4) i4TestValFsMplsTunnelSrlgRowStatus;

    i1RetVal = TeTestAllFsMplsTunnelSrlgTable (pu4ErrorCode, u4MplsTunnelIndex,
                                               u4MplsTunnelInstance,
                                               u4MplsTunnelIngressLSRId,
                                               u4MplsTunnelEgressLSRId,
                                               u4FsMplsTunnelSrlgNo,
                                               (&TeTnlSrlg),
                                               TE_FS_MPLS_SRLG_ROW_STATUS);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsTunnelSrlgTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                FsMplsTunnelSrlgNo
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsTunnelSrlgTable (UINT4 *pu4ErrorCode,
                               tSnmpIndexList * pSnmpIndexList,
                               tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsTunnelAttributeSrlgTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsTunnelAttributeSrlgTable
 Input       :  The Indices
                FsTunnelAttributeIndex
                FsTunnelAttributeSrlgNo
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsTunnelAttributeSrlgTable (UINT4
                                                    u4FsTunnelAttributeIndex,
                                                    UINT4
                                                    u4FsTunnelAttributeSrlgNo)
{
    UNUSED_PARAM (u4FsTunnelAttributeIndex);
    UNUSED_PARAM (u4FsTunnelAttributeSrlgNo);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsTunnelAttributeSrlgTable
 Input       :  The Indices
                FsTunnelAttributeIndex
                FsTunnelAttributeSrlgNo
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsTunnelAttributeSrlgTable (UINT4 *pu4FsTunnelAttributeIndex,
                                            UINT4 *pu4FsTunnelAttributeSrlgNo)
{
    tTeAttrListInfo    *pTeAttrInfo = NULL;
    tTeAttrSrlg        *pTeAttrSrlg = NULL;
    UINT4               u4AttrListIndex = TE_ZERO;

    MPLS_CMN_LOCK ();

    *pu4FsTunnelAttributeIndex = TE_ZERO;
    *pu4FsTunnelAttributeSrlgNo = TE_ZERO;

    u4AttrListIndex = TeGetNextAttrListIndex (u4AttrListIndex);
    pTeAttrInfo = TeGetAttrListInfo (u4AttrListIndex);

    while (pTeAttrInfo != NULL)
    {
        pTeAttrSrlg = (tTeAttrSrlg *) TMO_SLL_First (&pTeAttrInfo->SrlgList);

        if (pTeAttrSrlg != NULL)
        {
            *pu4FsTunnelAttributeIndex = u4AttrListIndex;
            *pu4FsTunnelAttributeSrlgNo = pTeAttrSrlg->u4SrlgNo;

            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
        else
        {
            u4AttrListIndex = TeGetNextAttrListIndex (u4AttrListIndex);
            pTeAttrInfo = TeGetAttrListInfo (u4AttrListIndex);
        }
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsTunnelAttributeSrlgTable
 Input       :  The Indices
                FsTunnelAttributeIndex
                nextFsTunnelAttributeIndex
                FsTunnelAttributeSrlgNo
                nextFsTunnelAttributeSrlgNo
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsTunnelAttributeSrlgTable (UINT4 u4FsTunnelAttributeIndex,
                                           UINT4 *pu4NextFsTunnelAttributeIndex,
                                           UINT4 u4FsTunnelAttributeSrlgNo,
                                           UINT4
                                           *pu4NextFsTunnelAttributeSrlgNo)
{
    tTeAttrListInfo    *pTeAttrInfo = NULL;
    tTeAttrSrlg        *pTeAttrSrlg = NULL;
    UINT4               u4AttrListIndex = TE_ZERO;
    BOOL1               bNextAttrEntryReqd = FALSE;

    MPLS_CMN_LOCK ();

    *pu4NextFsTunnelAttributeIndex = TE_ZERO;
    *pu4NextFsTunnelAttributeSrlgNo = TE_ZERO;

    pTeAttrInfo = TeGetAttrListInfo (u4FsTunnelAttributeIndex);

    while (pTeAttrInfo != NULL)
    {
        TMO_SLL_Scan (&pTeAttrInfo->SrlgList, pTeAttrSrlg, tTeAttrSrlg *)
        {
            if ((bNextAttrEntryReqd == TRUE) ||
                (pTeAttrSrlg->u4SrlgNo > u4FsTunnelAttributeSrlgNo))
            {
                *pu4NextFsTunnelAttributeIndex = pTeAttrInfo->u4ListIndex;
                *pu4NextFsTunnelAttributeSrlgNo = pTeAttrSrlg->u4SrlgNo;

                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }
        }

        u4AttrListIndex = TeGetNextAttrListIndex (u4FsTunnelAttributeIndex);
        pTeAttrInfo = TeGetAttrListInfo (u4AttrListIndex);

        bNextAttrEntryReqd = TRUE;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsTunnelAttributeSrlgRowStatus
 Input       :  The Indices
                FsTunnelAttributeIndex
                FsTunnelAttributeSrlgNo

                The Object 
                retValFsTunnelAttributeSrlgRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsTunnelAttributeSrlgRowStatus (UINT4 u4FsTunnelAttributeIndex,
                                      UINT4 u4FsTunnelAttributeSrlgNo,
                                      INT4
                                      *pi4RetValFsTunnelAttributeSrlgRowStatus)
{
    tTeAttrSrlg         TeAttrSrlg;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrSrlg, TE_ZERO, sizeof (tTeAttrSrlg));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllFsMplsAttributeSrlgTable (u4FsTunnelAttributeIndex,
                                                 u4FsTunnelAttributeSrlgNo,
                                                 (&TeAttrSrlg));

    MPLS_CMN_UNLOCK ();

    *pi4RetValFsTunnelAttributeSrlgRowStatus = (INT4) TeAttrSrlg.u4RowStatus;

    return i1RetVal;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsTunnelAttributeSrlgRowStatus
 Input       :  The Indices
                FsTunnelAttributeIndex
                FsTunnelAttributeSrlgNo

                The Object 
                setValFsTunnelAttributeSrlgRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsTunnelAttributeSrlgRowStatus (UINT4 u4FsTunnelAttributeIndex,
                                      UINT4 u4FsTunnelAttributeSrlgNo,
                                      INT4
                                      i4SetValFsTunnelAttributeSrlgRowStatus)
{
    tTeAttrSrlg         TeAttrSrlg;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrSrlg, TE_ZERO, sizeof (tTeAttrSrlg));

    TeAttrSrlg.u4RowStatus = (UINT4) i4SetValFsTunnelAttributeSrlgRowStatus;

    MPLS_CMN_LOCK ();

    i1RetVal = TeSetAllFsMplsAttributeSrlgTable (u4FsTunnelAttributeIndex,
                                                 u4FsTunnelAttributeSrlgNo,
                                                 (&TeAttrSrlg),
                                                 TE_FS_MPLS_ATTR_SRLG_ROW_STATUS);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsTunnelAttributeSrlgRowStatus
 Input       :  The Indices
                FsTunnelAttributeIndex
                FsTunnelAttributeSrlgNo

                The Object 
                testValFsTunnelAttributeSrlgRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsTunnelAttributeSrlgRowStatus (UINT4 *pu4ErrorCode,
                                         UINT4 u4FsTunnelAttributeIndex,
                                         UINT4 u4FsTunnelAttributeSrlgNo,
                                         INT4
                                         i4TestValFsTunnelAttributeSrlgRowStatus)
{
    tTeAttrSrlg         TeAttrSrlg;
    INT1                i1RetVal = TE_ZERO;

    MEMSET (&TeAttrSrlg, TE_ZERO, sizeof (tTeAttrSrlg));

    TeAttrSrlg.u4RowStatus = (UINT4) i4TestValFsTunnelAttributeSrlgRowStatus;

    MPLS_CMN_LOCK ();

    i1RetVal = TeTestAllFsMplsAttributeSrlgTable (pu4ErrorCode,
                                                  u4FsTunnelAttributeIndex,
                                                  u4FsTunnelAttributeSrlgNo,
                                                  (&TeAttrSrlg),
                                                  TE_FS_MPLS_ATTR_SRLG_ROW_STATUS);

    MPLS_CMN_UNLOCK ();

    return i1RetVal;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsTunnelAttributeSrlgTable
 Input       :  The Indices
                FsTunnelAttributeIndex
                FsTunnelAttributeSrlgNo
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsTunnelAttributeSrlgTable (UINT4 *pu4ErrorCode,
                                    tSnmpIndexList * pSnmpIndexList,
                                    tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMplsTunnelHopTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsTunnelHopTable
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMplsTunnelHopTable (UINT4 u4MplsTunnelHopListIndex,
                                              UINT4
                                              u4MplsTunnelHopPathOptionIndex,
                                              UINT4 u4MplsTunnelHopIndex)
{

    return (nmhValidateIndexInstanceMplsTunnelHopTable
            (u4MplsTunnelHopListIndex, u4MplsTunnelHopPathOptionIndex,
             u4MplsTunnelHopIndex));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsTunnelHopTable
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMplsTunnelHopTable (UINT4 *pu4MplsTunnelHopListIndex,
                                      UINT4 *pu4MplsTunnelHopPathOptionIndex,
                                      UINT4 *pu4MplsTunnelHopIndex)
{

    return (nmhGetFirstIndexMplsTunnelHopTable (pu4MplsTunnelHopListIndex,
                                                pu4MplsTunnelHopPathOptionIndex,
                                                pu4MplsTunnelHopIndex));

}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMplsTunnelHopTable
 Input       :  The Indices
                MplsTunnelHopListIndex
                nextMplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                nextMplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex
                nextMplsTunnelHopIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMplsTunnelHopTable (UINT4 u4MplsTunnelHopListIndex,
                                     UINT4 *pu4NextMplsTunnelHopListIndex,
                                     UINT4 u4MplsTunnelHopPathOptionIndex,
                                     UINT4 *pu4NextMplsTunnelHopPathOptionIndex,
                                     UINT4 u4MplsTunnelHopIndex,
                                     UINT4 *pu4NextMplsTunnelHopIndex)
{

    return (nmhGetNextIndexMplsTunnelHopTable (u4MplsTunnelHopListIndex,
                                               pu4NextMplsTunnelHopListIndex,
                                               u4MplsTunnelHopPathOptionIndex,
                                               pu4NextMplsTunnelHopPathOptionIndex,
                                               u4MplsTunnelHopIndex,
                                               pu4NextMplsTunnelHopIndex));
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsTunnelHopIncludeAny
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                retValFsMplsTunnelHopIncludeAny
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsTunnelHopIncludeAny (UINT4 u4MplsTunnelHopListIndex,
                                 UINT4 u4MplsTunnelHopPathOptionIndex,
                                 UINT4 u4MplsTunnelHopIndex,
                                 INT4 *pi4RetValFsMplsTunnelHopIncludeAny)
{
    tTeHopInfo         *pTeHopInfo = NULL;

    MPLS_CMN_LOCK ();

    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) == TE_SUCCESS)
    {
        *pi4RetValFsMplsTunnelHopIncludeAny = pTeHopInfo->i4TnlHopIncludeAny;
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMplsTunnelHopIncludeAny
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                setValFsMplsTunnelHopIncludeAny
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsMplsTunnelHopIncludeAny (UINT4 u4MplsTunnelHopListIndex,
                                 UINT4 u4MplsTunnelHopPathOptionIndex,
                                 UINT4 u4MplsTunnelHopIndex,
                                 INT4 i4SetValFsMplsTunnelHopIncludeAny)
{
    tTeHopInfo         *pTeHopInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) == TE_SUCCESS)
    {
        pTeHopInfo->i4TnlHopIncludeAny = i4SetValFsMplsTunnelHopIncludeAny;
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMplsTunnelHopIncludeAny
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                testValFsMplsTunnelHopIncludeAny
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsTunnelHopIncludeAny (UINT4 *pu4ErrorCode,
                                    UINT4 u4MplsTunnelHopListIndex,
                                    UINT4 u4MplsTunnelHopPathOptionIndex,
                                    UINT4 u4MplsTunnelHopIndex,
                                    INT4 i4TestValFsMplsTunnelHopIncludeAny)
{
    tTeHopInfo         *pTeHopInfo = NULL;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) != TE_SUCCESS)
    {

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_ERHOP_ROW_STATUS (pTeHopInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMplsTunnelHopIncludeAny != TE_SNMP_TRUE) &&
        (i4TestValFsMplsTunnelHopIncludeAny != TE_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsTunnelHopTable
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsMplsTunnelHopTable (UINT4 *pu4ErrorCode,
                              tSnmpIndexList * pSnmpIndexList,
                              tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsGmplsTunnelNotifyErrorTrapEnable
 Input       :  The Indices

                The Object 
                retValFsGmplsTunnelNotifyErrorTrapEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsGmplsTunnelNotifyErrorTrapEnable (INT4
                                          *pi4RetValFsGmplsTunnelNotifyErrorTrapEnable)
{
    *pi4RetValFsGmplsTunnelNotifyErrorTrapEnable =
        gTeGblInfo.TeParams.i4NotificationEnable;
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsGmplsTunnelNotifyErrorTrapEnable
 Input       :  The Indices

                The Object 
                setValFsGmplsTunnelNotifyErrorTrapEnable
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsGmplsTunnelNotifyErrorTrapEnable (INT4
                                          i4SetValFsGmplsTunnelNotifyErrorTrapEnable)
{
    gTeGblInfo.TeParams.i4NotificationEnable =
        i4SetValFsGmplsTunnelNotifyErrorTrapEnable;
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsGmplsTunnelNotifyErrorTrapEnable
 Input       :  The Indices

                The Object 
                testValFsGmplsTunnelNotifyErrorTrapEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsGmplsTunnelNotifyErrorTrapEnable (UINT4 *pu4ErrorCode,
                                             INT4
                                             i4TestValFsGmplsTunnelNotifyErrorTrapEnable)
{
    if ((i4TestValFsGmplsTunnelNotifyErrorTrapEnable != MPLS_SNMP_TRUE) &&
        (i4TestValFsGmplsTunnelNotifyErrorTrapEnable != MPLS_SNMP_FALSE))
    {
        /* The variable cannot take this value */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsGmplsTunnelNotifyErrorTrapEnable
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsGmplsTunnelNotifyErrorTrapEnable (UINT4 *pu4ErrorCode,
                                            tSnmpIndexList * pSnmpIndexList,
                                            tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);

    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMplsReoptimizationTunnelTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsReoptimizationTunnelTable
 Input       :  The Indices
                FsMplsReoptimizationTunnelIndex
                FsMplsReoptimizationTunnelIngressLSRId
                FsMplsReoptimizationTunnelEgressLSRId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1 nmhValidateIndexInstanceFsMplsReoptimizationTunnelTable(
								UINT4 u4FsMplsReoptimizationTunnelIndex , 
								UINT4 u4FsMplsReoptimizationTunnelIngressLSRId , 
								UINT4 u4FsMplsReoptimizationTunnelEgressLSRId)
{
	UNUSED_PARAM(u4FsMplsReoptimizationTunnelIndex);
	UNUSED_PARAM(u4FsMplsReoptimizationTunnelIngressLSRId);
	UNUSED_PARAM(u4FsMplsReoptimizationTunnelEgressLSRId);
	return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsReoptimizationTunnelTable
 Input       :  The Indices
                FsMplsReoptimizationTunnelIndex
                FsMplsReoptimizationTunnelIngressLSRId
                FsMplsReoptimizationTunnelEgressLSRId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1 nmhGetFirstIndexFsMplsReoptimizationTunnelTable(
					UINT4 *pu4FsMplsReoptimizationTunnelIndex , 
					UINT4 *pu4FsMplsReoptimizationTunnelIngressLSRId , 
					UINT4 *pu4FsMplsReoptimizationTunnelEgressLSRId)
{
    UINT4               u4TmpTnlIngressLsrId = TE_ZERO;
    UINT4               u4TmpTnlEgressLsrId = TE_ZERO;
	tTeReoptTnlInfo 	*pFirstTeReoptTnlInfo = NULL;

    *pu4FsMplsReoptimizationTunnelIndex = TE_ZERO;
    *pu4FsMplsReoptimizationTunnelIngressLSRId = TE_ZERO;
    *pu4FsMplsReoptimizationTunnelEgressLSRId = TE_ZERO;

    MPLS_CMN_LOCK ();
   
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    
	pFirstTeReoptTnlInfo = (tTeReoptTnlInfo *)RBTreeGetFirst(gTeGblInfo.ReoptimizeTnlList);
	
	if(pFirstTeReoptTnlInfo != NULL)
	{
		*pu4FsMplsReoptimizationTunnelIndex = pFirstTeReoptTnlInfo->u4ReoptTnlIndex;
		CONVERT_TO_INTEGER ((pFirstTeReoptTnlInfo->ReoptTnlIngressLsrId),
							u4TmpTnlIngressLsrId);
		CONVERT_TO_INTEGER ((pFirstTeReoptTnlInfo->ReoptTnlEgressLsrId),
							u4TmpTnlEgressLsrId);
		
		u4TmpTnlEgressLsrId = OSIX_NTOHL (u4TmpTnlEgressLsrId);
		u4TmpTnlIngressLsrId = OSIX_NTOHL (u4TmpTnlIngressLsrId);		

		*pu4FsMplsReoptimizationTunnelIngressLSRId = u4TmpTnlIngressLsrId;
		*pu4FsMplsReoptimizationTunnelEgressLSRId = u4TmpTnlEgressLsrId;

		MPLS_CMN_UNLOCK ();
		return SNMP_SUCCESS;

	}

    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMplsReoptimizationTunnelTable
 Input       :  The Indices
                FsMplsReoptimizationTunnelIndex
                nextFsMplsReoptimizationTunnelIndex
                FsMplsReoptimizationTunnelIngressLSRId
                nextFsMplsReoptimizationTunnelIngressLSRId
                FsMplsReoptimizationTunnelEgressLSRId
                nextFsMplsReoptimizationTunnelEgressLSRId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1 nmhGetNextIndexFsMplsReoptimizationTunnelTable(
					UINT4 u4FsMplsReoptimizationTunnelIndex ,
					UINT4 *pu4NextFsMplsReoptimizationTunnelIndex  , 
					UINT4 u4FsMplsReoptimizationTunnelIngressLSRId ,
					UINT4 *pu4NextFsMplsReoptimizationTunnelIngressLSRId  , 
					UINT4 u4FsMplsReoptimizationTunnelEgressLSRId ,
					UINT4 *pu4NextFsMplsReoptimizationTunnelEgressLSRId )
{
    UINT4               	u4TmpNextTnlIngressLsrId = TE_ZERO;
    UINT4               	u4TmpNextTnlEgressLsrId = TE_ZERO;
    tTeReoptTnlInfo         InputTeReoptTnlInfo;
    tTeReoptTnlInfo         *pNextTeReoptTnlInfo = NULL;

    *pu4NextFsMplsReoptimizationTunnelIndex = TE_ZERO;
    *pu4NextFsMplsReoptimizationTunnelIngressLSRId = TE_ZERO;
    *pu4NextFsMplsReoptimizationTunnelEgressLSRId = TE_ZERO;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    InputTeReoptTnlInfo.u4ReoptTnlIndex = u4FsMplsReoptimizationTunnelIndex;
    u4FsMplsReoptimizationTunnelIngressLSRId = OSIX_HTONL (u4FsMplsReoptimizationTunnelIngressLSRId);
    MEMCPY (InputTeReoptTnlInfo.ReoptTnlIngressLsrId, &(u4FsMplsReoptimizationTunnelIngressLSRId),
            IPV4_ADDR_LENGTH);
    u4FsMplsReoptimizationTunnelEgressLSRId = OSIX_HTONL (u4FsMplsReoptimizationTunnelEgressLSRId);
    MEMCPY (InputTeReoptTnlInfo.ReoptTnlEgressLsrId, &(u4FsMplsReoptimizationTunnelEgressLSRId),
            IPV4_ADDR_LENGTH);

    pNextTeReoptTnlInfo = (tTeReoptTnlInfo *) RBTreeGetNext (gTeGblInfo.ReoptimizeTnlList,
                                                   (tRBElem *) &InputTeReoptTnlInfo,
                                                   NULL);
    if (pNextTeReoptTnlInfo != NULL)
    {
        *pu4NextFsMplsReoptimizationTunnelIndex = pNextTeReoptTnlInfo->u4ReoptTnlIndex;
        CONVERT_TO_INTEGER ((pNextTeReoptTnlInfo->ReoptTnlIngressLsrId),
                            u4TmpNextTnlIngressLsrId);
        CONVERT_TO_INTEGER ((pNextTeReoptTnlInfo->ReoptTnlEgressLsrId),
                            u4TmpNextTnlEgressLsrId);
        u4TmpNextTnlIngressLsrId = OSIX_NTOHL (u4TmpNextTnlIngressLsrId);
        u4TmpNextTnlEgressLsrId = OSIX_NTOHL (u4TmpNextTnlEgressLsrId);

        *pu4NextFsMplsReoptimizationTunnelIngressLSRId = u4TmpNextTnlIngressLsrId;
        *pu4NextFsMplsReoptimizationTunnelEgressLSRId = u4TmpNextTnlEgressLsrId;

        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;


}
/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsReoptimizationTunnelStatus
 Input       :  The Indices
                FsMplsReoptimizationTunnelIndex
                FsMplsReoptimizationTunnelIngressLSRId
                FsMplsReoptimizationTunnelEgressLSRId

                The Object 
                retValFsMplsReoptimizationTunnelStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMplsReoptimizationTunnelStatus(
					UINT4 u4FsMplsReoptimizationTunnelIndex , 
					UINT4 u4FsMplsReoptimizationTunnelIngressLSRId , 
					UINT4 u4FsMplsReoptimizationTunnelEgressLSRId , 
					INT4 *pi4RetValFsMplsReoptimizationTunnelStatus)
{
	tTeReoptTnlInfo		*pTeGetReoptTnlInfo = NULL;
	tTeReoptTnlInfo		TeReoptTnlInfo;

	MPLS_CMN_LOCK ();

	MEMSET(&TeReoptTnlInfo,TE_ZERO,sizeof(tTeReoptTnlInfo));

    u4FsMplsReoptimizationTunnelIngressLSRId = 
					OSIX_HTONL (u4FsMplsReoptimizationTunnelIngressLSRId);
    u4FsMplsReoptimizationTunnelEgressLSRId = 
					OSIX_HTONL (u4FsMplsReoptimizationTunnelEgressLSRId);

	TeReoptTnlInfo.u4ReoptTnlIndex = u4FsMplsReoptimizationTunnelIndex; 
    MEMCPY (TeReoptTnlInfo.ReoptTnlIngressLsrId, 
					&u4FsMplsReoptimizationTunnelIngressLSRId, sizeof (UINT4));
    MEMCPY (TeReoptTnlInfo.ReoptTnlEgressLsrId, 
					&u4FsMplsReoptimizationTunnelEgressLSRId, sizeof (UINT4));

	pTeGetReoptTnlInfo = (tTeReoptTnlInfo *)RBTreeGet(gTeGblInfo.ReoptimizeTnlList,
											(tRBElem *)(&TeReoptTnlInfo));

	if(pTeGetReoptTnlInfo != NULL)
	{
		*pi4RetValFsMplsReoptimizationTunnelStatus = 
						(INT4)pTeGetReoptTnlInfo->u1ReoptTnlStatus;
		MPLS_CMN_UNLOCK ();
		return SNMP_SUCCESS;
	}
	
	MPLS_CMN_UNLOCK ();
	return SNMP_FAILURE;



}
/****************************************************************************
 Function    :  nmhGetFsMplsReoptimizationTunnelManualTrigger
 Input       :  The Indices
                FsMplsReoptimizationTunnelIndex
                FsMplsReoptimizationTunnelIngressLSRId
                FsMplsReoptimizationTunnelEgressLSRId

                The Object
                retValFsMplsReoptimizationTunnelManualTrigger
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMplsReoptimizationTunnelManualTrigger(
					UINT4 u4FsMplsReoptimizationTunnelIndex , 
					UINT4 u4FsMplsReoptimizationTunnelIngressLSRId , 
					UINT4 u4FsMplsReoptimizationTunnelEgressLSRId , 
					INT4 *pi4RetValFsMplsReoptimizationTunnelManualTrigger)
{
	tTeReoptTnlInfo		*pTeGetReoptTnlInfo = NULL;
	tTeReoptTnlInfo		TeReoptTnlInfo;

	MPLS_CMN_LOCK ();

	MEMSET(&TeReoptTnlInfo,TE_ZERO,sizeof(tTeReoptTnlInfo));

    u4FsMplsReoptimizationTunnelIngressLSRId = 
					OSIX_HTONL (u4FsMplsReoptimizationTunnelIngressLSRId);
    u4FsMplsReoptimizationTunnelEgressLSRId = 
					OSIX_HTONL (u4FsMplsReoptimizationTunnelEgressLSRId);

	TeReoptTnlInfo.u4ReoptTnlIndex = u4FsMplsReoptimizationTunnelIndex; 
    MEMCPY (TeReoptTnlInfo.ReoptTnlIngressLsrId, 
					&u4FsMplsReoptimizationTunnelIngressLSRId, sizeof (UINT4));
    MEMCPY (TeReoptTnlInfo.ReoptTnlEgressLsrId, 
					&u4FsMplsReoptimizationTunnelEgressLSRId, sizeof (UINT4));

	pTeGetReoptTnlInfo = (tTeReoptTnlInfo *)RBTreeGet(gTeGblInfo.ReoptimizeTnlList,
											(tRBElem *)(&TeReoptTnlInfo));

	if(pTeGetReoptTnlInfo != NULL)
	{
		*pi4RetValFsMplsReoptimizationTunnelManualTrigger = 
							(INT4)pTeGetReoptTnlInfo->u1ReoptManualTrigger;
		MPLS_CMN_UNLOCK ();
		return SNMP_SUCCESS;
	}
	
	MPLS_CMN_UNLOCK ();
	return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhGetFsMplsReoptimizationTunnelRowStatus
 Input       :  The Indices
                FsMplsReoptimizationTunnelIndex
                FsMplsReoptimizationTunnelIngressLSRId
                FsMplsReoptimizationTunnelEgressLSRId

                The Object 
                retValFsMplsReoptimizationTunnelRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhGetFsMplsReoptimizationTunnelRowStatus(
					UINT4 u4FsMplsReoptimizationTunnelIndex , 
					UINT4 u4FsMplsReoptimizationTunnelIngressLSRId , 
					UINT4 u4FsMplsReoptimizationTunnelEgressLSRId , 
					INT4 *pi4RetValFsMplsReoptimizationTunnelRowStatus)
{
	tTeReoptTnlInfo		*pTeGetReoptTnlInfo = NULL;
	tTeReoptTnlInfo		TeReoptTnlInfo;

	MPLS_CMN_LOCK ();

	MEMSET(&TeReoptTnlInfo,TE_ZERO,sizeof(tTeReoptTnlInfo));

    u4FsMplsReoptimizationTunnelIngressLSRId = 
					OSIX_HTONL (u4FsMplsReoptimizationTunnelIngressLSRId);
    u4FsMplsReoptimizationTunnelEgressLSRId = 
					OSIX_HTONL (u4FsMplsReoptimizationTunnelEgressLSRId);

	TeReoptTnlInfo.u4ReoptTnlIndex = u4FsMplsReoptimizationTunnelIndex; 
    MEMCPY (TeReoptTnlInfo.ReoptTnlIngressLsrId, 
					&u4FsMplsReoptimizationTunnelIngressLSRId, sizeof (UINT4));
    MEMCPY (TeReoptTnlInfo.ReoptTnlEgressLsrId, 
					&u4FsMplsReoptimizationTunnelEgressLSRId, sizeof (UINT4));

	pTeGetReoptTnlInfo = (tTeReoptTnlInfo *)RBTreeGet(gTeGblInfo.ReoptimizeTnlList,
											(tRBElem *)(&TeReoptTnlInfo));

	if(pTeGetReoptTnlInfo != NULL)
	{
		*pi4RetValFsMplsReoptimizationTunnelRowStatus = TE_ACTIVE;
		MPLS_CMN_UNLOCK ();
		return SNMP_SUCCESS;
	}
	
	MPLS_CMN_UNLOCK ();
	return SNMP_FAILURE;
	
}
/* Low Level SET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhSetFsMplsReoptimizationTunnelStatus
 Input       :  The Indices
                FsMplsReoptimizationTunnelIndex
                FsMplsReoptimizationTunnelIngressLSRId
                FsMplsReoptimizationTunnelEgressLSRId

                The Object 
                setValFsMplsReoptimizationTunnelStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMplsReoptimizationTunnelStatus(
					UINT4 u4FsMplsReoptimizationTunnelIndex , 
					UINT4 u4FsMplsReoptimizationTunnelIngressLSRId , 
					UINT4 u4FsMplsReoptimizationTunnelEgressLSRId , 
					INT4 i4SetValFsMplsReoptimizationTunnelStatus)
{
	tTeReoptTnlInfo		*pTeGetReoptTnlInfo = NULL;
	tTeReoptTnlInfo		TeReoptTnlInfo;

	MPLS_CMN_LOCK ();

	MEMSET(&TeReoptTnlInfo,TE_ZERO,sizeof(tTeReoptTnlInfo));

    u4FsMplsReoptimizationTunnelIngressLSRId = 
					OSIX_HTONL (u4FsMplsReoptimizationTunnelIngressLSRId);
    u4FsMplsReoptimizationTunnelEgressLSRId = 
					OSIX_HTONL (u4FsMplsReoptimizationTunnelEgressLSRId);

	TeReoptTnlInfo.u4ReoptTnlIndex = u4FsMplsReoptimizationTunnelIndex; 
    MEMCPY (TeReoptTnlInfo.ReoptTnlIngressLsrId, 
					&u4FsMplsReoptimizationTunnelIngressLSRId, sizeof (UINT4));
    MEMCPY (TeReoptTnlInfo.ReoptTnlEgressLsrId, 
					&u4FsMplsReoptimizationTunnelEgressLSRId, sizeof (UINT4));

	pTeGetReoptTnlInfo = (tTeReoptTnlInfo *)RBTreeGet(gTeGblInfo.ReoptimizeTnlList,
											(tRBElem *)(&TeReoptTnlInfo));

	if(pTeGetReoptTnlInfo != NULL)
	{
		pTeGetReoptTnlInfo->u1ReoptTnlStatus = 
			(UINT1)i4SetValFsMplsReoptimizationTunnelStatus;	
		
		MPLS_CMN_UNLOCK ();
		return SNMP_SUCCESS;
	}

	MPLS_CMN_UNLOCK ();
	return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhSetFsMplsReoptimizationTunnelManualTrigger
 Input       :  The Indices
                FsMplsReoptimizationTunnelIndex
                FsMplsReoptimizationTunnelIngressLSRId
                FsMplsReoptimizationTunnelEgressLSRId

                The Object
                setValFsMplsReoptimizationTunnelManualTrigger
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMplsReoptimizationTunnelManualTrigger(
					UINT4 u4FsMplsReoptimizationTunnelIndex , 
					UINT4 u4FsMplsReoptimizationTunnelIngressLSRId , 
					UINT4 u4FsMplsReoptimizationTunnelEgressLSRId , 
					INT4 i4SetValFsMplsReoptimizationTunnelManualTrigger)
{
	tTeReoptTnlInfo		*pTeGetReoptTnlInfo = NULL;
	tTeReoptTnlInfo		TeReoptTnlInfo;
	UINT4				u4TmpTunnelIngressLSRId = TE_ZERO;
	UINT4				u4TmpTunnelEgressLSRId = TE_ZERO;
   tTeTnlInfo 			*pIns0TeTnlInfo = NULL;
   tTeTnlInfo 			*pTeTnlInfo = NULL;


	MPLS_CMN_LOCK ();

	MEMSET(&TeReoptTnlInfo,TE_ZERO,sizeof(tTeReoptTnlInfo));

    u4TmpTunnelIngressLSRId = 
					OSIX_HTONL (u4FsMplsReoptimizationTunnelIngressLSRId);
    u4TmpTunnelEgressLSRId = 
					OSIX_HTONL (u4FsMplsReoptimizationTunnelEgressLSRId);

	TeReoptTnlInfo.u4ReoptTnlIndex = u4FsMplsReoptimizationTunnelIndex; 
    MEMCPY (TeReoptTnlInfo.ReoptTnlIngressLsrId, 
					&u4TmpTunnelIngressLSRId, sizeof (UINT4));
    MEMCPY (TeReoptTnlInfo.ReoptTnlEgressLsrId, 
					&u4TmpTunnelEgressLSRId, sizeof (UINT4));

	pTeGetReoptTnlInfo = (tTeReoptTnlInfo *)RBTreeGet(gTeGblInfo.ReoptimizeTnlList,
											(tRBElem *)(&TeReoptTnlInfo));

	if(pTeGetReoptTnlInfo != NULL)
	{
		pTeGetReoptTnlInfo->u1ReoptManualTrigger = 
			(UINT1)i4SetValFsMplsReoptimizationTunnelManualTrigger;	
	

   		pIns0TeTnlInfo = TeGetTunnelInfo(u4FsMplsReoptimizationTunnelIndex,TE_ZERO,
                         u4FsMplsReoptimizationTunnelIngressLSRId,
						 u4FsMplsReoptimizationTunnelEgressLSRId);

   		if(pIns0TeTnlInfo != NULL)
		{

			if(TeSigSendReoptTnlManualTriggerEvt(pIns0TeTnlInfo) == TE_FAILURE)
			{
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;
			}	
			MPLS_CMN_UNLOCK ();
			return SNMP_SUCCESS;
		}
		else
		{
			pTeTnlInfo = (tTeTnlInfo *) TE_ALLOC_MEM_BLOCK (TE_TNL_POOL_ID);

 			if (pTeTnlInfo == NULL)
 			{
     			MPLS_CMN_UNLOCK ();
     			return SNMP_FAILURE;
 			}
			
			INIT_TE_TNL_INFO (pTeTnlInfo);

			TE_TNL_TNL_INDEX (pTeTnlInfo) = (UINT4) u4FsMplsReoptimizationTunnelIndex;
			TE_TNL_TNL_INSTANCE (pTeTnlInfo) = TE_ZERO;
			MEMCPY ((UINT1 *) &(TE_TNL_INGRESS_LSRID (pTeTnlInfo)),
        			(UINT1 *) &(u4TmpTunnelIngressLSRId), sizeof (UINT4));
			MEMCPY ((UINT1 *) &(TE_TNL_EGRESS_LSRID (pTeTnlInfo)),
        			(UINT1 *) &(u4TmpTunnelEgressLSRId), sizeof (UINT4));

			/*This variable with value=3 is used only to block manual reoptimization
			 * on Transit and Egress Node*/
			TE_TNL_REOPTIMIZE_STATUS(pTeTnlInfo) = TE_REOPTIMIZE_MANUAL_TRIG;

			MPLS_CMN_UNLOCK ();
			
			if(TeSigSendReoptTnlManualTriggerEvt(pTeTnlInfo) == TE_FAILURE)
			{
     			return SNMP_FAILURE;
 			}

 			return SNMP_SUCCESS;
		}
	}

	MPLS_CMN_UNLOCK ();
	return SNMP_FAILURE;
}
/****************************************************************************
 Function    :  nmhSetFsMplsReoptimizationTunnelRowStatus
 Input       :  The Indices
                FsMplsReoptimizationTunnelIndex
                FsMplsReoptimizationTunnelIngressLSRId
                FsMplsReoptimizationTunnelEgressLSRId

                The Object 
                setValFsMplsReoptimizationTunnelRowStatus
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhSetFsMplsReoptimizationTunnelRowStatus(
					UINT4 u4FsMplsReoptimizationTunnelIndex , 
					UINT4 u4FsMplsReoptimizationTunnelIngressLSRId , 
					UINT4 u4FsMplsReoptimizationTunnelEgressLSRId , 
					INT4 i4SetValFsMplsReoptimizationTunnelRowStatus)
{

	tTeReoptTnlInfo		*pTeReoptTnlInfo = NULL;
	tTeReoptTnlInfo		*pTeGetReoptTnlInfo = NULL;
	tTeReoptTnlInfo		TeReoptTnlInfo;
	BOOL1               bFound = FALSE;


	MPLS_CMN_LOCK ();

	MEMSET(&TeReoptTnlInfo,TE_ZERO,sizeof(tTeReoptTnlInfo));

    u4FsMplsReoptimizationTunnelIngressLSRId = 
					OSIX_HTONL (u4FsMplsReoptimizationTunnelIngressLSRId);
    u4FsMplsReoptimizationTunnelEgressLSRId = 
					OSIX_HTONL (u4FsMplsReoptimizationTunnelEgressLSRId);

	TeReoptTnlInfo.u4ReoptTnlIndex = u4FsMplsReoptimizationTunnelIndex; 
    MEMCPY (TeReoptTnlInfo.ReoptTnlIngressLsrId, 
					&u4FsMplsReoptimizationTunnelIngressLSRId, sizeof (UINT4));
    MEMCPY (TeReoptTnlInfo.ReoptTnlEgressLsrId, 
					&u4FsMplsReoptimizationTunnelEgressLSRId, sizeof (UINT4));

	pTeGetReoptTnlInfo = (tTeReoptTnlInfo *)RBTreeGet(gTeGblInfo.ReoptimizeTnlList,
											(tRBElem *)(&TeReoptTnlInfo));

	if(pTeGetReoptTnlInfo == NULL)
	{
		bFound = FALSE;
	}
	else
	{
		bFound = TRUE;
	}

	switch(i4SetValFsMplsReoptimizationTunnelRowStatus)
	{
		case TE_CREATEANDWAIT:
   		{
			if (bFound == TRUE)
    		{
        		/* Tunnel is already present in the reoptimize Tunnel List */
				MPLS_CMN_UNLOCK ();
        		return SNMP_SUCCESS;
    		}
		
			pTeReoptTnlInfo = (tTeReoptTnlInfo *)MemAllocMemBlk(TE_REOPT_TNL_POOL_ID);

			if(pTeReoptTnlInfo == NULL)
			{
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;			
			}

			pTeReoptTnlInfo->u4ReoptTnlIndex = u4FsMplsReoptimizationTunnelIndex;
			MEMCPY (pTeReoptTnlInfo->ReoptTnlIngressLsrId, &u4FsMplsReoptimizationTunnelIngressLSRId, sizeof (UINT4));
			MEMCPY (pTeReoptTnlInfo->ReoptTnlEgressLsrId, &u4FsMplsReoptimizationTunnelEgressLSRId, sizeof (UINT4));
			
			pTeReoptTnlInfo->u1ReoptTnlRowStatus = TE_CREATEANDWAIT;
 
			if(RBTreeAdd(gTeGblInfo.ReoptimizeTnlList,(tRBElem *)pTeReoptTnlInfo) == RB_FAILURE)
			{
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;
			}
			break;
		}
		case TE_ACTIVE:
		{
			if(bFound == TRUE)
			{
				pTeGetReoptTnlInfo->u1ReoptTnlRowStatus = TE_ACTIVE;				
				pTeGetReoptTnlInfo->u1ReoptTnlStatus = TE_REOPTIMIZE_ENABLE;
				pTeGetReoptTnlInfo->u1ReoptManualTrigger = TE_REOPT_TRIGGER_DISABLE;
			}
			else
			{
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;	
			}
			break;		
		} 
		case TE_DESTROY: 
    	{
			if(bFound == TRUE)
			{
				RBTreeRem(gTeGblInfo.ReoptimizeTnlList,pTeGetReoptTnlInfo);
				MemReleaseMemBlock (TE_REOPT_TNL_POOL_ID,(UINT1 *) pTeGetReoptTnlInfo);
			}
			else
			{
				MPLS_CMN_UNLOCK ();
				return SNMP_FAILURE;	
			}

			break;
		}
		default:
    		MPLS_CMN_UNLOCK ();
    		return SNMP_FAILURE;
	}
	
	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;
}
/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2FsMplsReoptimizationTunnelStatus
 Input       :  The Indices
                FsMplsReoptimizationTunnelIndex
                FsMplsReoptimizationTunnelIngressLSRId
                FsMplsReoptimizationTunnelEgressLSRId

                The Object 
                testValFsMplsReoptimizationTunnelStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMplsReoptimizationTunnelStatus(
					UINT4 *pu4ErrorCode , 
					UINT4 u4FsMplsReoptimizationTunnelIndex , 
					UINT4 u4FsMplsReoptimizationTunnelIngressLSRId , 
					UINT4 u4FsMplsReoptimizationTunnelEgressLSRId , 
					INT4 i4TestValFsMplsReoptimizationTunnelStatus)
{

	if((i4TestValFsMplsReoptimizationTunnelStatus != TE_ONE) &&
		(i4TestValFsMplsReoptimizationTunnelStatus != TE_TWO))
	{
		  *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
		  return SNMP_FAILURE;
	}

	UNUSED_PARAM(u4FsMplsReoptimizationTunnelIndex);
	UNUSED_PARAM(u4FsMplsReoptimizationTunnelIngressLSRId);
	UNUSED_PARAM(u4FsMplsReoptimizationTunnelEgressLSRId);
	return SNMP_SUCCESS;

}
/****************************************************************************
 Function    :  nmhTestv2FsMplsReoptimizationTunnelManualTrigger
 Input       :  The Indices
                FsMplsReoptimizationTunnelIndex
                FsMplsReoptimizationTunnelIngressLSRId
                FsMplsReoptimizationTunnelEgressLSRId

                The Object
                testValFsMplsReoptimizationTunnelManualTrigger
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMplsReoptimizationTunnelManualTrigger(
					UINT4 *pu4ErrorCode , 
					UINT4 u4FsMplsReoptimizationTunnelIndex , 
					UINT4 u4FsMplsReoptimizationTunnelIngressLSRId , 
					UINT4 u4FsMplsReoptimizationTunnelEgressLSRId , 
					INT4 i4TestValFsMplsReoptimizationTunnelManualTrigger)
{
	tTeReoptTnlInfo		*pTeGetReoptTnlInfo = NULL;
	tTeReoptTnlInfo		TeReoptTnlInfo;
		

	if(i4TestValFsMplsReoptimizationTunnelManualTrigger != TE_ONE)
	{
		  *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
		  return SNMP_FAILURE;
	}

	MPLS_CMN_LOCK ();

	MEMSET(&TeReoptTnlInfo,TE_ZERO,sizeof(tTeReoptTnlInfo));

    u4FsMplsReoptimizationTunnelIngressLSRId = 
					OSIX_HTONL (u4FsMplsReoptimizationTunnelIngressLSRId);
    u4FsMplsReoptimizationTunnelEgressLSRId = 
					OSIX_HTONL (u4FsMplsReoptimizationTunnelEgressLSRId);

	TeReoptTnlInfo.u4ReoptTnlIndex = u4FsMplsReoptimizationTunnelIndex; 
    MEMCPY (TeReoptTnlInfo.ReoptTnlIngressLsrId, 
					&u4FsMplsReoptimizationTunnelIngressLSRId, sizeof (UINT4));
    MEMCPY (TeReoptTnlInfo.ReoptTnlEgressLsrId, 
					&u4FsMplsReoptimizationTunnelEgressLSRId, sizeof (UINT4));

	pTeGetReoptTnlInfo = (tTeReoptTnlInfo *)RBTreeGet(gTeGblInfo.ReoptimizeTnlList,
											(tRBElem *)(&TeReoptTnlInfo));

	if(pTeGetReoptTnlInfo == NULL)
	{
		*pu4ErrorCode = SNMP_ERR_NO_CREATION;
		MPLS_CMN_UNLOCK ();
		return SNMP_FAILURE;
	}

	MPLS_CMN_UNLOCK ();
	return SNMP_SUCCESS;


}
/****************************************************************************
 Function    :  nmhTestv2FsMplsReoptimizationTunnelRowStatus
 Input       :  The Indices
                FsMplsReoptimizationTunnelIndex
                FsMplsReoptimizationTunnelIngressLSRId
                FsMplsReoptimizationTunnelEgressLSRId

                The Object 
                testValFsMplsReoptimizationTunnelRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhTestv2FsMplsReoptimizationTunnelRowStatus(
					UINT4 *pu4ErrorCode , 
					UINT4 u4FsMplsReoptimizationTunnelIndex , 
					UINT4 u4FsMplsReoptimizationTunnelIngressLSRId , 
					UINT4 u4FsMplsReoptimizationTunnelEgressLSRId , 
					INT4 i4TestValFsMplsReoptimizationTunnelRowStatus)
{

	tTeReoptTnlInfo     *pTeGetReoptTnlInfo = NULL;
	tTeReoptTnlInfo     TeReoptTnlInfo;
	
    /* Allowed tunnel indices 1 to 65535 as per MPLS-TC-STD-MIB [RFC 3811] */
    if ((u4FsMplsReoptimizationTunnelIndex < TE_TNLINDEX_MINVAL) ||
        (u4FsMplsReoptimizationTunnelIndex > TE_TNLINDEX_MAXVAL))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        return SNMP_FAILURE;
    }

	/* Operations other than CREATE AND GO and DESTROY is not permitted for
	 ** this object */
	if ((i4TestValFsMplsReoptimizationTunnelRowStatus != TE_CREATEANDWAIT) &&
    	(i4TestValFsMplsReoptimizationTunnelRowStatus != TE_ACTIVE) &&
    	(i4TestValFsMplsReoptimizationTunnelRowStatus != TE_DESTROY))
	{
    	*pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    	return SNMP_FAILURE;
	}
	

	if((i4TestValFsMplsReoptimizationTunnelRowStatus == TE_ACTIVE) ||
	   (i4TestValFsMplsReoptimizationTunnelRowStatus == TE_DESTROY))
	{
		TeReoptTnlInfo.u4ReoptTnlIndex = u4FsMplsReoptimizationTunnelIndex;
		u4FsMplsReoptimizationTunnelIngressLSRId =
			OSIX_HTONL (u4FsMplsReoptimizationTunnelIngressLSRId);
		u4FsMplsReoptimizationTunnelEgressLSRId =
			OSIX_HTONL (u4FsMplsReoptimizationTunnelEgressLSRId);
		MEMCPY (TeReoptTnlInfo.ReoptTnlIngressLsrId,
					&u4FsMplsReoptimizationTunnelIngressLSRId, sizeof (UINT4));
		MEMCPY (TeReoptTnlInfo.ReoptTnlEgressLsrId,
					&u4FsMplsReoptimizationTunnelEgressLSRId, sizeof (UINT4));

		MPLS_CMN_LOCK ();
		
		pTeGetReoptTnlInfo = (tTeReoptTnlInfo *)RBTreeGet(gTeGblInfo.ReoptimizeTnlList,
					(tRBElem *)(&TeReoptTnlInfo));

		if(pTeGetReoptTnlInfo == NULL)
		{
			*pu4ErrorCode = SNMP_ERR_NO_CREATION;
			MPLS_CMN_UNLOCK ();
			return SNMP_FAILURE;
		}
		else
		{
			MPLS_CMN_UNLOCK ();
			return SNMP_SUCCESS;
		}
	}

	return SNMP_SUCCESS;

}
/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsMplsReoptimizationTunnelTable
 Input       :  The Indices
                FsMplsReoptimizationTunnelIndex
                FsMplsReoptimizationTunnelIngressLSRId
                FsMplsReoptimizationTunnelEgressLSRId
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1 nmhDepv2FsMplsReoptimizationTunnelTable(
					UINT4 *pu4ErrorCode, 
					tSnmpIndexList *pSnmpIndexList, 
					tSNMP_VAR_BIND *pSnmpVarBind)
{
	UNUSED_PARAM(pu4ErrorCode);
	UNUSED_PARAM(pSnmpIndexList);
	UNUSED_PARAM(pSnmpVarBind);
	return SNMP_SUCCESS;
}

