 /********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: teget2.c,v 1.27 2016/03/02 11:40:06 siva Exp $
 *
 * Description: This file contains the low level GET routines
 *              for the following TE MIB tables.
 *              - MplsTunnelHopTable
 *              - MplsTunnelARHopTable
 *              - MplsTunnelCHopTable
 *              - MplsTunnelPerfTable
 *              and scalars
 *              - MplsTunnelConfigured
 *              - MplsTunnelActive
 *              - MplsTunnelTEDistProto
 *              - MplsTunnelMaxHops
 *              - FsMplsFrrTunARHopTable
 *******************************************************************/

#include "teincs.h"
#include "fsmplslw.h"
#include "stdtelw.h"
#include "mplsnp.h"
#include "mplslsr.h"
#include "fsmpfrlw.h"
#include  "mplsnpwr.h"

/* LOW LEVEL Routines for Table : MplsTunnelHopTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsTunnelHopTable
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhValidateIndexInstanceMplsTunnelHopTable (UINT4 u4MplsTunnelHopListIndex,
                                            UINT4
                                            u4MplsTunnelHopPathOptionIndex,
                                            UINT4 u4MplsTunnelHopIndex)
{
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (!((u4MplsTunnelHopListIndex >= TE_TNL_HOPLSTINDEX_MINVAL) &&
          (u4MplsTunnelHopListIndex <= TE_TNL_HOPLSTINDEX_MAXVAL)))
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (!((u4MplsTunnelHopPathOptionIndex >= TE_TNL_POINDEX_MINVAL) &&
          (u4MplsTunnelHopPathOptionIndex <= TE_MAX_HOP_PER_PO(gTeGblInfo))))
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (!((u4MplsTunnelHopIndex >= TE_TNL_ERHOPINDEX_MINVAL) &&
          (u4MplsTunnelHopIndex <= TE_TNL_ERHOPINDEX_MAXVAL)))
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsTunnelHopTable
 Input       :  The Indices
               MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexMplsTunnelHopTable (UINT4 *pu4MplsTunnelHopListIndex,
                                    UINT4 *pu4MplsTunnelHopPathOptionIndex,
                                    UINT4 *pu4MplsTunnelHopIndex)
{
    UINT4               u4HopListIndex = TE_ZERO;
    UINT1               u1PathOptionIndexFound = TE_FALSE;
    UINT1               u1HopIndexFound = TE_FALSE;
    tTeSll             *pPathOptionList = NULL;
    tTeSll             *pErHopList = NULL;
    tTePathInfo        *pTePathInfo = NULL;
    tTePathInfo        *pTmpTePathInfo = NULL;
    tTeHopInfo         *pTeHopInfo = NULL;

    *pu4MplsTunnelHopListIndex = TE_ZERO;
    *pu4MplsTunnelHopPathOptionIndex = TE_ZERO;
    *pu4MplsTunnelHopIndex = TE_ZERO;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    for (u4HopListIndex = TE_ONE;
         u4HopListIndex <= TE_MAX_HOP_LIST (gTeGblInfo); u4HopListIndex++)
    {
        if (TE_PATH_LIST_INDEX (u4HopListIndex) == TE_ZERO)
        {
            continue;
        }
        *pu4MplsTunnelHopListIndex = TE_PATH_LIST_INDEX (u4HopListIndex);

        /* Scan through PathOptionlist for least PathOption index */

        pPathOptionList = TE_PATH_OPTION_LIST (u4HopListIndex);
        TE_SLL_SCAN (pPathOptionList, pTePathInfo, tTePathInfo *)
        {
            if (u1PathOptionIndexFound == TE_FALSE)
            {
                *pu4MplsTunnelHopPathOptionIndex = TE_PO_INDEX (pTePathInfo);
                pTmpTePathInfo = pTePathInfo;
                u1PathOptionIndexFound = TE_TRUE;
            }
            else
            {
                if (*pu4MplsTunnelHopPathOptionIndex >
                    TE_PO_INDEX (pTePathInfo))
                {
                    *pu4MplsTunnelHopPathOptionIndex
                        = TE_PO_INDEX (pTePathInfo);
                    pTmpTePathInfo = pTePathInfo;
                }
            }
        }

        /* Scan through ErHoplist for  least Tunnel HopIndex */
        pErHopList = TE_PO_HOP_LIST (pTmpTePathInfo);
        if (pErHopList == NULL)
        {
            continue;
        }
        TE_SLL_SCAN (pErHopList, pTeHopInfo, tTeHopInfo *)
        {
            if (u1HopIndexFound == TE_FALSE)
            {
                *pu4MplsTunnelHopIndex = TE_ERHOP_INDEX (pTeHopInfo);
                u1HopIndexFound = TE_TRUE;
            }
            else
            {
                if (*pu4MplsTunnelHopIndex > TE_ERHOP_INDEX (pTeHopInfo))
                {
                    *pu4MplsTunnelHopIndex = TE_ERHOP_INDEX (pTeHopInfo);
                }
            }
        }
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsTunnelHopTable
 Input       :  The Indices
                MplsTunnelHopListIndex
                nextMplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                nextMplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex
                nextMplsTunnelHopIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexMplsTunnelHopTable (UINT4 u4MplsTunnelHopListIndex,
                                   UINT4 *pu4NextMplsTunnelHopListIndex,
                                   UINT4 u4MplsTunnelHopPathOptionIndex,
                                   UINT4 *pu4NextMplsTunnelHopPathOptionIndex,
                                   UINT4 u4MplsTunnelHopIndex,
                                   UINT4 *pu4NextMplsTunnelHopIndex)
{
    UINT4               u4HopListIndex = TE_ZERO;
    UINT1               u1PathOptionIndexFound = TE_FALSE;
    UINT1               u1HopIndexFound = TE_FALSE;
    tTeSll             *pPathOptionList = NULL;
    tTeSll             *pErHopList = NULL;
    tTePathInfo        *pTePathInfo = NULL;
    tTePathInfo        *pTmpTePathInfo = NULL;
    tTeHopInfo         *pTeHopInfo = NULL;

    *pu4NextMplsTunnelHopListIndex = TE_ZERO;
    *pu4NextMplsTunnelHopPathOptionIndex = TE_ZERO;
    *pu4NextMplsTunnelHopIndex = TE_ZERO;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pPathOptionList = TE_PATH_OPTION_LIST (u4MplsTunnelHopListIndex);
    TE_SLL_SCAN (pPathOptionList, pTePathInfo, tTePathInfo *)
    {
        if (u4MplsTunnelHopPathOptionIndex == TE_PO_INDEX (pTePathInfo))
        {
            pErHopList = TE_PO_HOP_LIST (pTePathInfo);
            TE_SLL_SCAN (pErHopList, pTeHopInfo, tTeHopInfo *)
            {
                if (u1HopIndexFound == TE_FALSE)
                {
                    if (u4MplsTunnelHopIndex < TE_ERHOP_INDEX (pTeHopInfo))
                    {
                        *pu4NextMplsTunnelHopIndex =
                            TE_ERHOP_INDEX (pTeHopInfo);
                        u1HopIndexFound = TE_TRUE;
                    }
                }
                else
                {
                    if ((u4MplsTunnelHopIndex < TE_ERHOP_INDEX (pTeHopInfo))
                        && (*pu4NextMplsTunnelHopIndex >
                            TE_ERHOP_INDEX (pTeHopInfo)))
                    {
                        *pu4NextMplsTunnelHopIndex =
                            TE_ERHOP_INDEX (pTeHopInfo);
                    }
                }
            }
        }
    }
    if (u1HopIndexFound == TE_TRUE)
    {
        *pu4NextMplsTunnelHopListIndex = u4MplsTunnelHopListIndex;
        *pu4NextMplsTunnelHopPathOptionIndex = u4MplsTunnelHopPathOptionIndex;
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }

    TE_SLL_SCAN (pPathOptionList, pTePathInfo, tTePathInfo *)
    {
        if (u1PathOptionIndexFound == TE_FALSE)
        {
            if (u4MplsTunnelHopPathOptionIndex < TE_PO_INDEX (pTePathInfo))
            {
                *pu4NextMplsTunnelHopPathOptionIndex =
                    TE_PO_INDEX (pTePathInfo);
                pTmpTePathInfo = pTePathInfo;
                u1PathOptionIndexFound = TE_TRUE;
            }
        }
        else
        {
            if (u4MplsTunnelHopPathOptionIndex <
                TE_PO_INDEX (pTePathInfo) &&
                (*pu4NextMplsTunnelHopPathOptionIndex >
                 TE_PO_INDEX (pTePathInfo)))
            {
                *pu4NextMplsTunnelHopPathOptionIndex
                    = TE_PO_INDEX (pTePathInfo);
                pTmpTePathInfo = pTePathInfo;
            }
        }
    }
    /* Scan through ErHoplist for  least Tunnel HopIndex */
    if (u1PathOptionIndexFound == TE_TRUE)
    {
        pErHopList = TE_PO_HOP_LIST (pTmpTePathInfo);
        TE_SLL_SCAN (pErHopList, pTeHopInfo, tTeHopInfo *)
        {
            if (u1HopIndexFound == TE_FALSE)
            {
                *pu4NextMplsTunnelHopIndex = TE_ERHOP_INDEX (pTeHopInfo);
                u1HopIndexFound = TE_TRUE;
            }
            else
            {
                if (*pu4NextMplsTunnelHopIndex > TE_ERHOP_INDEX (pTeHopInfo))
                {
                    *pu4NextMplsTunnelHopIndex = TE_ERHOP_INDEX (pTeHopInfo);
                }
            }
        }
    }
    if (u1HopIndexFound == TE_TRUE)
    {
        *pu4NextMplsTunnelHopListIndex = u4MplsTunnelHopListIndex;
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }

    if (!((u4MplsTunnelHopListIndex >= TE_TNL_HOPLSTINDEX_MINVAL) &&
          (u4MplsTunnelHopListIndex <= TE_TNL_HOPLSTINDEX_MAXVAL)))
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    for (u4HopListIndex = u4MplsTunnelHopListIndex + TE_ONE;
         u4HopListIndex <= TE_MAX_HOP_LIST (gTeGblInfo); u4HopListIndex++)
    {
        if (TE_PATH_LIST_INDEX (u4HopListIndex) == TE_ZERO)
        {
            continue;
        }
        *pu4NextMplsTunnelHopListIndex = TE_PATH_LIST_INDEX (u4HopListIndex);

        /* Scan through PathOptionlist for least PathOption index */

        pPathOptionList = TE_PATH_OPTION_LIST (u4HopListIndex);
        TE_SLL_SCAN (pPathOptionList, pTePathInfo, tTePathInfo *)
        {
            if (u1PathOptionIndexFound == TE_FALSE)
            {
                *pu4NextMplsTunnelHopPathOptionIndex =
                    TE_PO_INDEX (pTePathInfo);
                pTmpTePathInfo = pTePathInfo;
                u1PathOptionIndexFound = TE_TRUE;
            }
            else
            {
                if (*pu4NextMplsTunnelHopPathOptionIndex >
                    TE_PO_INDEX (pTePathInfo))
                {
                    *pu4NextMplsTunnelHopPathOptionIndex
                        = TE_PO_INDEX (pTePathInfo);
                    pTmpTePathInfo = pTePathInfo;
                    break;
                }
            }
        }
        /* Scan through ErHoplist for  least Tunnel HopIndex */

        if (pTmpTePathInfo == NULL)
        {
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }

        pErHopList = TE_PO_HOP_LIST (pTmpTePathInfo);
        TE_SLL_SCAN (pErHopList, pTeHopInfo, tTeHopInfo *)
        {
            if (u1HopIndexFound == TE_FALSE)
            {
                *pu4NextMplsTunnelHopIndex = TE_ERHOP_INDEX (pTeHopInfo);
                u1HopIndexFound = TE_TRUE;
            }
            else
            {
                if ((*pu4NextMplsTunnelHopIndex > TE_ERHOP_INDEX (pTeHopInfo)))
                {
                    *pu4NextMplsTunnelHopIndex = TE_ERHOP_INDEX (pTeHopInfo);
                }
            }
        }

        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;

    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelHopListIndexNext
 Input       :  The Indices

                The Object 
                retValMplsTunnelHopListIndexNext
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelHopListIndexNext (UINT4 *pu4RetValMplsTunnelHopListIndexNext)
{
    UINT4               u4HopListIndex;

    *pu4RetValMplsTunnelHopListIndexNext = 0;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    for (u4HopListIndex = TE_ONE;
         u4HopListIndex <= TE_MAX_HOP_LIST (gTeGblInfo); u4HopListIndex++)
    {
        if (TE_PATH_LIST_INDEX (u4HopListIndex) == TE_ZERO)
        {
            *pu4RetValMplsTunnelHopListIndexNext = u4HopListIndex;
            break;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelHopAddrType
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                retValMplsTunnelHopAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelHopAddrType (UINT4 u4MplsTunnelHopListIndex,
                             UINT4 u4MplsTunnelHopPathOptionIndex,
                             UINT4 u4MplsTunnelHopIndex,
                             INT4 *pi4RetValMplsTunnelHopAddrType)
{
    tTeHopInfo         *pTeHopInfo = NULL;

    MPLS_CMN_LOCK ();
    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) == TE_SUCCESS)
    {

        *pi4RetValMplsTunnelHopAddrType =
            (INT4) TE_ERHOP_ADDR_TYPE (pTeHopInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelHopIpAddr
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                retValMplsTunnelHopIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelHopIpAddr (UINT4 u4MplsTunnelHopListIndex,
                           UINT4 u4MplsTunnelHopPathOptionIndex,
                           UINT4 u4MplsTunnelHopIndex,
                           tSNMP_OCTET_STRING_TYPE * pRetValMplsTunnelHopIpAddr)
{
    tTeHopInfo         *pTeHopInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) == TE_SUCCESS)
    {
        MEMCPY (pRetValMplsTunnelHopIpAddr->pu1_OctetList,
                &TE_ERHOP_IP_ADDR (pTeHopInfo), sizeof (UINT4));
        pRetValMplsTunnelHopIpAddr->i4_Length = sizeof (UINT4);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelHopIpPrefixLen
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                retValMplsTunnelHopIpPrefixLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelHopIpPrefixLen (UINT4 u4MplsTunnelHopListIndex,
                                UINT4 u4MplsTunnelHopPathOptionIndex,
                                UINT4 u4MplsTunnelHopIndex,
                                UINT4 *pu4RetValMplsTunnelHopIpPrefixLen)
{
    tTeHopInfo         *pTeHopInfo = NULL;

    MPLS_CMN_LOCK ();
    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) == TE_SUCCESS)
    {
        *pu4RetValMplsTunnelHopIpPrefixLen =
            TE_ERHOP_ADDR_PRFX_LEN (pTeHopInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelHopAsNumber
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                retValMplsTunnelHopAsNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelHopAsNumber (UINT4 u4MplsTunnelHopListIndex,
                             UINT4 u4MplsTunnelHopPathOptionIndex,
                             UINT4 u4MplsTunnelHopIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValMplsTunnelHopAsNumber)
{
    tTeHopInfo         *pTeHopInfo = NULL;
    UINT4               u4Local = 0;
    MPLS_CMN_LOCK ();
    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) == TE_SUCCESS)
    {
        u4Local = (UINT4) TE_ERHOP_AS_NUM (pTeHopInfo);
        u4Local = OSIX_HTONL (u4Local);
        MEMCPY (pRetValMplsTunnelHopAsNumber->pu1_OctetList, &u4Local,
                sizeof (UINT4));
        pRetValMplsTunnelHopAsNumber->i4_Length = sizeof (UINT4);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelHopAddrUnnum
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object
                retValMplsTunnelHopAddrUnnum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelHopAddrUnnum (UINT4 u4MplsTunnelHopListIndex,
                              UINT4 u4MplsTunnelHopPathOptionIndex,
                              UINT4 u4MplsTunnelHopIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValMplsTunnelHopAddrUnnum)
{
    tTeHopInfo         *pTeHopInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) == TE_SUCCESS)
    {
        MPLS_INTEGER_TO_OCTETSTRING (pTeHopInfo->u4UnnumIf,
                                     pRetValMplsTunnelHopAddrUnnum);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelHopLspId
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                retValMplsTunnelHopLspId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelHopLspId (UINT4 u4MplsTunnelHopListIndex,
                          UINT4 u4MplsTunnelHopPathOptionIndex,
                          UINT4 u4MplsTunnelHopIndex,
                          tSNMP_OCTET_STRING_TYPE * pRetValMplsTunnelHopLspId)
{
    tTeHopInfo         *pTeHopInfo = NULL;
    UINT2               u2LspId = 0;
    MPLS_CMN_LOCK ();
    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) == TE_SUCCESS)
    {
        /* LSPID length can be either 2 or 6. 6 in case of LDP
         * and 2 in case of RSVP */
        u2LspId = (UINT2) TE_ERHOP_LSPID (pTeHopInfo);
        pRetValMplsTunnelHopLspId->i4_Length = sizeof (UINT2);
        MEMCPY (pRetValMplsTunnelHopLspId->pu1_OctetList,
                &(u2LspId), pRetValMplsTunnelHopLspId->i4_Length);
        if (TE_ERHOP_ADDR_PRFX_LEN (pTeHopInfo) != 0)
        {
            MEMCPY ((pRetValMplsTunnelHopLspId->pu1_OctetList +
                     pRetValMplsTunnelHopLspId->i4_Length),
                    &(TE_ERHOP_IP_ADDR (pTeHopInfo)), sizeof (UINT4));
            pRetValMplsTunnelHopLspId->i4_Length = (INT4)(pRetValMplsTunnelHopLspId->i4_Length + sizeof (UINT4));
        }
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelHopType
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                retValMplsTunnelHopType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelHopType (UINT4 u4MplsTunnelHopListIndex,
                         UINT4 u4MplsTunnelHopPathOptionIndex,
                         UINT4 u4MplsTunnelHopIndex,
                         INT4 *pi4RetValMplsTunnelHopType)
{
    tTeHopInfo         *pTeHopInfo = NULL;

    MPLS_CMN_LOCK ();
    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) == TE_SUCCESS)
    {
        *pi4RetValMplsTunnelHopType = (INT4) TE_ERHOP_TYPE (pTeHopInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelHopInclude
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                retValMplsTunnelHopInclude
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelHopInclude (UINT4 u4MplsTunnelHopListIndex,
                            UINT4 u4MplsTunnelHopPathOptionIndex,
                            UINT4 u4MplsTunnelHopIndex,
                            INT4 *pi4RetValMplsTunnelHopInclude)
{
    /* Currently Not Supported */

    tTeHopInfo         *pTeHopInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) == TE_SUCCESS)
    {
        *pi4RetValMplsTunnelHopInclude
            = (INT4) TE_ERHOP_INCLD_EXCLD (pTeHopInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelHopPathOptionName
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                retValMplsTunnelHopPathOptionName
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelHopPathOptionName (UINT4 u4MplsTunnelHopListIndex,
                                   UINT4 u4MplsTunnelHopPathOptionIndex,
                                   UINT4 u4MplsTunnelHopIndex,
                                   tSNMP_OCTET_STRING_TYPE *
                                   pRetValMplsTunnelHopPathOptionName)
{
    /* Currently Not Supported */

    tTeHopInfo         *pTeHopInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) == TE_SUCCESS)
    {
        pRetValMplsTunnelHopPathOptionName->i4_Length =
            (INT4)STRLEN (pTeHopInfo->au1PathOptionName);
        MEMCPY (pRetValMplsTunnelHopPathOptionName->pu1_OctetList,
                pTeHopInfo->au1PathOptionName,
                pRetValMplsTunnelHopPathOptionName->i4_Length);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelHopEntryPathComp
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                retValMplsTunnelHopEntryPathComp
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelHopEntryPathComp (UINT4 u4MplsTunnelHopListIndex,
                                  UINT4 u4MplsTunnelHopPathOptionIndex,
                                  UINT4 u4MplsTunnelHopIndex,
                                  INT4 *pi4RetValMplsTunnelHopEntryPathComp)
{
    /* Added to suppress warnings */
    tTeHopInfo         *pTeHopInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) == TE_SUCCESS)
    {
        *pi4RetValMplsTunnelHopEntryPathComp
            = (INT4) TE_ERHOP_PATH_COMP (pTeHopInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelHopRowStatus
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                retValMplsTunnelHopRowStatus
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelHopRowStatus (UINT4 u4MplsTunnelHopListIndex,
                              UINT4 u4MplsTunnelHopPathOptionIndex,
                              UINT4 u4MplsTunnelHopIndex,
                              INT4 *pi4RetValMplsTunnelHopRowStatus)
{
    tTeHopInfo         *pTeHopInfo = NULL;
    MPLS_CMN_LOCK ();

    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) == TE_SUCCESS)
    {
        *pi4RetValMplsTunnelHopRowStatus =
            (INT4) TE_ERHOP_ROW_STATUS (pTeHopInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelHopStorageType
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                retValMplsTunnelHopStorageType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelHopStorageType (UINT4 u4MplsTunnelHopListIndex,
                                UINT4 u4MplsTunnelHopPathOptionIndex,
                                UINT4 u4MplsTunnelHopIndex,
                                INT4 *pi4RetValMplsTunnelHopStorageType)
{
    tTeHopInfo         *pTeHopInfo = NULL;

    MPLS_CMN_LOCK ();
    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) == TE_SUCCESS)
    {
        *pi4RetValMplsTunnelHopStorageType =
            (INT4) TE_ERHOP_STORAGE_TYPE (pTeHopInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : MplsTunnelARHopTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsTunnelARHopTable
 Input       :  The Indices
                MplsTunnelARHopListIndex
                MplsTunnelARHopIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhValidateIndexInstanceMplsTunnelARHopTable (UINT4 u4MplsTunnelARHopListIndex,
                                              UINT4 u4MplsTunnelARHopIndex)
{
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (!((u4MplsTunnelARHopListIndex >= TE_TNL_ARHOPLSTINDEX_MINVAL) &&
          (u4MplsTunnelARHopListIndex <= TE_TNL_ARHOPLSTINDEX_MAXVAL)))
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (!((u4MplsTunnelARHopIndex >= TE_TNL_ARHOPINDEX_MINVAL) &&
          (u4MplsTunnelARHopIndex <= TE_TNL_ARHOPINDEX_MAXVAL)))
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsTunnelARHopTable
 Input       :  The Indices
                MplsTunnelARHopListIndex
                MplsTunnelARHopIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFirstIndexMplsTunnelARHopTable (UINT4 *pu4MplsTunnelARHopListIndex,
                                      UINT4 *pu4MplsTunnelARHopIndex)
{
    UINT4               u4ARHopListIndex = TE_ZERO;
    UINT1               u1HopIndexFound = TE_FALSE;
    tTeSll             *pArHopList = NULL;
    tTeArHopInfo       *pTeArHopInfo = NULL;

    *pu4MplsTunnelARHopListIndex = TE_ZERO;
    *pu4MplsTunnelARHopIndex = TE_ZERO;
    MPLS_CMN_LOCK ();

    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    for (u4ARHopListIndex = TE_ONE;
         u4ARHopListIndex <= TE_MAX_ARHOP_LIST (gTeGblInfo); u4ARHopListIndex++)
    {
        if (TE_ARHOP_LIST_INDEX (u4ARHopListIndex) == TE_ZERO)
        {
            continue;
        }
        *pu4MplsTunnelARHopListIndex = TE_ARHOP_LIST_INDEX (u4ARHopListIndex);

        /* Scan through ArHopList for least  ArHop index */

        pArHopList = TE_ARHOP_LIST (u4ARHopListIndex);
        TE_SLL_SCAN (pArHopList, pTeArHopInfo, tTeArHopInfo *)
        {
            if (u1HopIndexFound == TE_FALSE)
            {
                *pu4MplsTunnelARHopIndex = TE_ARHOP_INDEX (pTeArHopInfo);
                u1HopIndexFound = TE_TRUE;
            }
            else
            {
                if (*pu4MplsTunnelARHopIndex > TE_ARHOP_INDEX (pTeArHopInfo))
                {
                    *pu4MplsTunnelARHopIndex = TE_ARHOP_INDEX (pTeArHopInfo);
                }
            }
        }
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsTunnelARHopTable
 Input       :  The Indices
                MplsTunnelARHopListIndex
                nextMplsTunnelARHopListIndex
                MplsTunnelARHopIndex
                nextMplsTunnelARHopIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetNextIndexMplsTunnelARHopTable (UINT4 u4MplsTunnelARHopListIndex,
                                     UINT4 *pu4NextMplsTunnelARHopListIndex,
                                     UINT4 u4MplsTunnelARHopIndex,
                                     UINT4 *pu4NextMplsTunnelARHopIndex)
{
    UINT4               u4ARHopListIndex = TE_ZERO;
    UINT1               u1HopIndexFound = TE_FALSE;
    tTeSll             *pArHopList = NULL;
    tTeArHopInfo       *pTeArHopInfo = NULL;

    *pu4NextMplsTunnelARHopListIndex = TE_ZERO;
    *pu4NextMplsTunnelARHopIndex = TE_ZERO;

    MPLS_CMN_LOCK ();
    pArHopList = TE_ARHOP_LIST (u4MplsTunnelARHopListIndex);
    TE_SLL_SCAN (pArHopList, pTeArHopInfo, tTeArHopInfo *)
    {
        if (u1HopIndexFound == TE_FALSE)
        {
            if (u4MplsTunnelARHopIndex < TE_ARHOP_INDEX (pTeArHopInfo))
            {
                *pu4NextMplsTunnelARHopIndex = TE_ARHOP_INDEX (pTeArHopInfo);
                u1HopIndexFound = TE_TRUE;
            }
            if (u1HopIndexFound == TE_TRUE)
            {
                if ((u4MplsTunnelARHopIndex < TE_ARHOP_INDEX (pTeArHopInfo)) &&
                    (*pu4NextMplsTunnelARHopIndex >
                     TE_ARHOP_INDEX (pTeArHopInfo)))
                {
                    *pu4NextMplsTunnelARHopIndex =
                        TE_ARHOP_INDEX (pTeArHopInfo);
                }
            }
        }
    }
    if (u1HopIndexFound == TE_TRUE)
    {
        *pu4NextMplsTunnelARHopListIndex = u4MplsTunnelARHopListIndex;
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }

    for (u4ARHopListIndex = u4MplsTunnelARHopListIndex + 1;
         u4ARHopListIndex <= TE_MAX_ARHOP_LIST (gTeGblInfo); u4ARHopListIndex++)
    {
        if (TE_ARHOP_LIST_INDEX (u4ARHopListIndex) == TE_ZERO)
        {
            continue;
        }
        *pu4NextMplsTunnelARHopListIndex =
            TE_ARHOP_LIST_INDEX (u4ARHopListIndex);
        pArHopList = TE_ARHOP_LIST (u4ARHopListIndex);
        TE_SLL_SCAN (pArHopList, pTeArHopInfo, tTeArHopInfo *)
        {
            if (u1HopIndexFound == TE_FALSE)
            {
                *pu4NextMplsTunnelARHopIndex = TE_ARHOP_INDEX (pTeArHopInfo);
                u1HopIndexFound = TE_TRUE;
            }
            else
            {
                if (*pu4NextMplsTunnelARHopIndex >
                    TE_ARHOP_INDEX (pTeArHopInfo))
                {
                    *pu4NextMplsTunnelARHopIndex =
                        TE_ARHOP_INDEX (pTeArHopInfo);
                }
            }
        }
        if (u1HopIndexFound == TE_TRUE)
        {
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelARHopAddrType
 Input       :  The Indices
                MplsTunnelARHopListIndex
                MplsTunnelARHopIndex

                The Object 
                retValMplsTunnelARHopAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelARHopAddrType (UINT4 u4MplsTunnelARHopListIndex,
                               UINT4 u4MplsTunnelARHopIndex,
                               INT4 *pi4RetValMplsTunnelARHopAddrType)
{
    tTeArHopInfo       *pTeArHopInfo = NULL;
    MPLS_CMN_LOCK ();

    if (TeCheckArHopInfo (u4MplsTunnelARHopListIndex,
                          u4MplsTunnelARHopIndex, &pTeArHopInfo) == TE_SUCCESS)
    {
        *pi4RetValMplsTunnelARHopAddrType =
            (INT4) TE_ARHOP_ADDR_TYPE (pTeArHopInfo);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelARHopIpAddr
 Input       :  The Indices
                MplsTunnelARHopListIndex
                MplsTunnelARHopIndex

                The Object 
                retValMplsTunnelARHopIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelARHopIpAddr (UINT4 u4MplsTunnelARHopListIndex,
                             UINT4 u4MplsTunnelARHopIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pRetValMplsTunnelARHopIpAddr)
{
    tTeArHopInfo       *pTeArHopInfo = NULL;

    MPLS_CMN_LOCK ();

    if (TeCheckArHopInfo (u4MplsTunnelARHopListIndex,
                          u4MplsTunnelARHopIndex, &pTeArHopInfo) == TE_SUCCESS)
    {
        MEMCPY (pRetValMplsTunnelARHopIpAddr->pu1_OctetList,
                &TE_ARHOP_IP_ADDR (pTeArHopInfo), sizeof (UINT4));
        pRetValMplsTunnelARHopIpAddr->i4_Length = sizeof (UINT4);

        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelARHopAddrUnnum
 Input       :  The Indices
                MplsTunnelARHopListIndex
                MplsTunnelARHopIndex

                The Object
                retValMplsTunnelARHopAddrUnnum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelARHopAddrUnnum (UINT4 u4MplsTunnelARHopListIndex,
                                UINT4 u4MplsTunnelARHopIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pRetValMplsTunnelARHopAddrUnnum)
{
    tTeArHopInfo       *pTeArHopInfo = NULL;

    MPLS_CMN_LOCK ();
    if (TeCheckArHopInfo (u4MplsTunnelARHopListIndex,
                          u4MplsTunnelARHopIndex, &pTeArHopInfo) == TE_SUCCESS)
    {
        MPLS_INTEGER_TO_OCTETSTRING (pTeArHopInfo->u4UnnumIf,
                                     pRetValMplsTunnelARHopAddrUnnum);

        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelARHopLspId
 Input       :  The Indices
                MplsTunnelARHopListIndex
                MplsTunnelARHopIndex

                The Object 
                retValMplsTunnelARHopLspId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelARHopLspId (UINT4 u4MplsTunnelARHopListIndex,
                            UINT4 u4MplsTunnelARHopIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValMplsTunnelARHopLspId)
{
    tTeArHopInfo       *pTeArHopInfo = NULL;

    MPLS_CMN_LOCK ();
    if (TeCheckArHopInfo (u4MplsTunnelARHopListIndex,
                          u4MplsTunnelARHopIndex, &pTeArHopInfo) == TE_SUCCESS)
    {
        if (TE_ARHOP_LSPID (pTeArHopInfo)[2] == 0)
        {
            pRetValMplsTunnelARHopLspId->i4_Length = 2;
        }
        else
        {
            pRetValMplsTunnelARHopLspId->i4_Length = 6;
        }
        MEMCPY (pRetValMplsTunnelARHopLspId->pu1_OctetList,
                TE_ARHOP_LSPID (pTeArHopInfo),
                pRetValMplsTunnelARHopLspId->i4_Length);
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : MplsTunnelCHopTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsTunnelCHopTable
 Input       :  The Indices
                MplsTunnelCHopListIndex
                MplsTunnelCHopIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsTunnelCHopTable (UINT4 u4MplsTunnelCHopListIndex,
                                             UINT4 u4MplsTunnelCHopIndex)
{
    UNUSED_PARAM (u4MplsTunnelCHopListIndex);
    UNUSED_PARAM (u4MplsTunnelCHopIndex);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsTunnelCHopTable
 Input       :  The Indices
                MplsTunnelCHopListIndex
                MplsTunnelCHopIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsTunnelCHopTable (UINT4 *pu4MplsTunnelCHopListIndex,
                                     UINT4 *pu4MplsTunnelCHopIndex)
{
    UINT4               u4HopListIndex = TE_ZERO;
    tTMO_SLL           *pCHopList = NULL;
    tTeCHopInfo        *pTeCHopInfo = NULL;

    *pu4MplsTunnelCHopListIndex = TE_ZERO;
    *pu4MplsTunnelCHopListIndex = TE_ZERO;

    MPLS_CMN_LOCK ();

    if (gTeGblInfo.TeParams.u1TeAdminStatus == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    for (u4HopListIndex = TE_ONE;
         u4HopListIndex <= gTeGblInfo.TeParams.u4MaxCHopList; u4HopListIndex++)
    {
        if (gTeGblInfo.pCHopListEntry[u4HopListIndex - TE_ONE].u4CHopListIndex
            == TE_ZERO)
        {
            continue;
        }

        pCHopList
            = &(gTeGblInfo.pCHopListEntry[u4HopListIndex - TE_ONE].CHopList);

        pTeCHopInfo = (tTeCHopInfo *) TMO_SLL_First (pCHopList);

        if (pTeCHopInfo == NULL)
        {
            continue;
        }

        *pu4MplsTunnelCHopListIndex = u4HopListIndex;
        *pu4MplsTunnelCHopIndex = pTeCHopInfo->u4TnlCHopIndex;

        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsTunnelCHopTable
 Input       :  The Indices
                MplsTunnelCHopListIndex
                nextMplsTunnelCHopListIndex
                MplsTunnelCHopIndex
                nextMplsTunnelCHopIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMplsTunnelCHopTable (UINT4 u4MplsTunnelCHopListIndex,
                                    UINT4 *pu4NextMplsTunnelCHopListIndex,
                                    UINT4 u4MplsTunnelCHopIndex,
                                    UINT4 *pu4NextMplsTunnelCHopIndex)
{
    UINT4               u4HopListIndex = TE_ZERO;
    UINT4               u4HopIndex = TE_ZERO;
    tTMO_SLL           *pCHopList = NULL;
    tTeCHopInfo        *pTeCHopInfo = NULL;
    BOOL1               bEntryFound = FALSE;

    *pu4NextMplsTunnelCHopListIndex = TE_ZERO;
    *pu4NextMplsTunnelCHopIndex = TE_ZERO;

    MPLS_CMN_LOCK ();

    if (gTeGblInfo.TeParams.u1TeAdminStatus == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    u4HopListIndex = u4MplsTunnelCHopListIndex;

    if (gTeGblInfo.pCHopListEntry[u4HopListIndex - TE_ONE].u4CHopListIndex
        == TE_ZERO)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pCHopList = &(gTeGblInfo.pCHopListEntry[u4HopListIndex - TE_ONE].CHopList);

    TMO_SLL_Scan (pCHopList, pTeCHopInfo, tTeCHopInfo *)
    {
        if (pTeCHopInfo->u4TnlCHopIndex > u4MplsTunnelCHopIndex)
        {
            u4HopIndex = pTeCHopInfo->u4TnlCHopIndex;
            bEntryFound = TRUE;
            break;
        }
    }

    if (bEntryFound == TRUE)
    {
        *pu4NextMplsTunnelCHopListIndex = u4HopListIndex;
        *pu4NextMplsTunnelCHopIndex = u4HopIndex;

        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }

    for (u4HopListIndex = (u4MplsTunnelCHopListIndex + 1);
         u4HopListIndex <= gTeGblInfo.TeParams.u4MaxCHopList; u4HopListIndex++)
    {
        if (gTeGblInfo.pCHopListEntry[u4HopListIndex - TE_ONE].u4CHopListIndex
            == TE_ZERO)
        {
            continue;
        }

        pCHopList
            = &(gTeGblInfo.pCHopListEntry[u4HopListIndex - TE_ONE].CHopList);

        TMO_SLL_Scan (pCHopList, pTeCHopInfo, tTeCHopInfo *)
        {
            if (pTeCHopInfo->u4TnlCHopIndex > u4MplsTunnelCHopIndex)
            {
                u4HopIndex = pTeCHopInfo->u4TnlCHopIndex;
                bEntryFound = TRUE;
                break;
            }
        }

        if (bEntryFound == FALSE)
        {
            continue;
        }

        *pu4NextMplsTunnelCHopListIndex = u4HopListIndex;
        *pu4NextMplsTunnelCHopIndex = u4HopIndex;

        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsTunnelCHopAddrType
 Input       :  The Indices
                MplsTunnelCHopListIndex
                MplsTunnelCHopIndex

                The Object 
                retValMplsTunnelCHopAddrType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelCHopAddrType (UINT4 u4MplsTunnelCHopListIndex,
                              UINT4 u4MplsTunnelCHopIndex,
                              INT4 *pi4RetValMplsTunnelCHopAddrType)
{
    INT1                i1RetVal = SNMP_FAILURE;
    tTeCHopInfo         TeCHop;

    MEMSET (&TeCHop, TE_ZERO, sizeof (tTeCHopInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllMplsTunnelCHopTable (u4MplsTunnelCHopListIndex,
                                            u4MplsTunnelCHopIndex, &TeCHop);

    MPLS_CMN_UNLOCK ();

    *pi4RetValMplsTunnelCHopAddrType = (INT4) TeCHop.u1AddressType;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelCHopIpAddr
 Input       :  The Indices
                MplsTunnelCHopListIndex
                MplsTunnelCHopIndex

                The Object 
                retValMplsTunnelCHopIpAddr
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelCHopIpAddr (UINT4 u4MplsTunnelCHopListIndex,
                            UINT4 u4MplsTunnelCHopIndex,
                            tSNMP_OCTET_STRING_TYPE *
                            pRetValMplsTunnelCHopIpAddr)
{
    INT1                i1RetVal = SNMP_FAILURE;
    tTeCHopInfo         TeCHop;

    MEMSET (&TeCHop, TE_ZERO, sizeof (tTeCHopInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllMplsTunnelCHopTable (u4MplsTunnelCHopListIndex,
                                            u4MplsTunnelCHopIndex, &TeCHop);

    MPLS_CMN_UNLOCK ();

    pRetValMplsTunnelCHopIpAddr->i4_Length = sizeof (UINT4);
    MEMCPY (pRetValMplsTunnelCHopIpAddr->pu1_OctetList,
            TeCHop.IpAddr.au1Ipv4Addr, pRetValMplsTunnelCHopIpAddr->i4_Length);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelCHopIpPrefixLen
 Input       :  The Indices
                MplsTunnelCHopListIndex
                MplsTunnelCHopIndex

                The Object 
                retValMplsTunnelCHopIpPrefixLen
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelCHopIpPrefixLen (UINT4 u4MplsTunnelCHopListIndex,
                                 UINT4 u4MplsTunnelCHopIndex,
                                 UINT4 *pu4RetValMplsTunnelCHopIpPrefixLen)
{
    INT1                i1RetVal = SNMP_FAILURE;
    tTeCHopInfo         TeCHop;

    MEMSET (&TeCHop, TE_ZERO, sizeof (tTeCHopInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllMplsTunnelCHopTable (u4MplsTunnelCHopListIndex,
                                            u4MplsTunnelCHopIndex, &TeCHop);

    MPLS_CMN_UNLOCK ();

    *pu4RetValMplsTunnelCHopIpPrefixLen = TeCHop.u1AddrPrefixLen;

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelCHopAsNumber
 Input       :  The Indices
                MplsTunnelCHopListIndex
                MplsTunnelCHopIndex

                The Object 
                retValMplsTunnelCHopAsNumber
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelCHopAsNumber (UINT4 u4MplsTunnelCHopListIndex,
                              UINT4 u4MplsTunnelCHopIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pRetValMplsTunnelCHopAsNumber)
{
    INT1                i1RetVal = SNMP_FAILURE;
    UINT2               u2Temp = TE_ZERO;
    tTeCHopInfo         TeCHop;

    MEMSET (&TeCHop, TE_ZERO, sizeof (tTeCHopInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllMplsTunnelCHopTable (u4MplsTunnelCHopListIndex,
                                            u4MplsTunnelCHopIndex, &TeCHop);

    MPLS_CMN_UNLOCK ();

    u2Temp = OSIX_HTONS (TeCHop.u2AsNumber);

    pRetValMplsTunnelCHopAsNumber->i4_Length = sizeof (UINT2);
    MEMCPY (pRetValMplsTunnelCHopAsNumber->pu1_OctetList,
            (UINT1 *) &u2Temp, pRetValMplsTunnelCHopAsNumber->i4_Length);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelCHopAddrUnnum
 Input       :  The Indices
                MplsTunnelCHopListIndex
                MplsTunnelCHopIndex

                The Object 
                retValMplsTunnelCHopAddrUnnum
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelCHopAddrUnnum (UINT4 u4MplsTunnelCHopListIndex,
                               UINT4 u4MplsTunnelCHopIndex,
                               tSNMP_OCTET_STRING_TYPE *
                               pRetValMplsTunnelCHopAddrUnnum)
{
    INT1                i1RetVal = SNMP_FAILURE;
    UINT4               u4Temp = TE_ZERO;
    tTeCHopInfo         TeCHop;

    MEMSET (&TeCHop, TE_ZERO, sizeof (tTeCHopInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllMplsTunnelCHopTable (u4MplsTunnelCHopListIndex,
                                            u4MplsTunnelCHopIndex, &TeCHop);

    MPLS_CMN_UNLOCK ();

    u4Temp = OSIX_HTONL (TeCHop.u4UnnumIf);
    pRetValMplsTunnelCHopAddrUnnum->i4_Length = sizeof (UINT4);
    MEMCPY (pRetValMplsTunnelCHopAddrUnnum->pu1_OctetList,
            (UINT1 *) &u4Temp, pRetValMplsTunnelCHopAddrUnnum->i4_Length);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelCHopLspId
 Input       :  The Indices
                MplsTunnelCHopListIndex
                MplsTunnelCHopIndex

                The Object 
                retValMplsTunnelCHopLspId
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelCHopLspId (UINT4 u4MplsTunnelCHopListIndex,
                           UINT4 u4MplsTunnelCHopIndex,
                           tSNMP_OCTET_STRING_TYPE * pRetValMplsTunnelCHopLspId)
{
    INT1                i1RetVal = SNMP_FAILURE;
    UINT4               u4Temp = TE_ZERO;
    tTeCHopInfo         TeCHop;

    MEMSET (&TeCHop, TE_ZERO, sizeof (tTeCHopInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllMplsTunnelCHopTable (u4MplsTunnelCHopListIndex,
                                            u4MplsTunnelCHopIndex, &TeCHop);

    MPLS_CMN_UNLOCK ();

    u4Temp = OSIX_HTONL (TeCHop.u4LocalLspId);
    pRetValMplsTunnelCHopLspId->i4_Length = sizeof (UINT4);
    MEMCPY (pRetValMplsTunnelCHopLspId->pu1_OctetList,
            (UINT1 *) &u4Temp, pRetValMplsTunnelCHopLspId->i4_Length);

    return i1RetVal;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelCHopType
 Input       :  The Indices
                MplsTunnelCHopListIndex
                MplsTunnelCHopIndex

                The Object 
                retValMplsTunnelCHopType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelCHopType (UINT4 u4MplsTunnelCHopListIndex,
                          UINT4 u4MplsTunnelCHopIndex,
                          INT4 *pi4RetValMplsTunnelCHopType)
{
    INT1                i1RetVal = SNMP_FAILURE;
    tTeCHopInfo         TeCHop;

    MEMSET (&TeCHop, TE_ZERO, sizeof (tTeCHopInfo));

    MPLS_CMN_LOCK ();

    i1RetVal = TeGetAllMplsTunnelCHopTable (u4MplsTunnelCHopListIndex,
                                            u4MplsTunnelCHopIndex, &TeCHop);

    MPLS_CMN_UNLOCK ();

    *pi4RetValMplsTunnelCHopType = (INT4) TeCHop.u1CHopType;

    return i1RetVal;
}

/* LOW LEVEL Routines for Table : MplsTunnelPerfTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceMplsTunnelPerfTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceMplsTunnelPerfTable (UINT4 u4MplsTunnelIndex,
                                             UINT4 u4MplsTunnelInstance,
                                             UINT4 u4MplsTunnelIngressLSRId,
                                             UINT4 u4MplsTunnelEgressLSRId)
{
    return (nmhValidateIndexInstanceMplsTunnelTable (u4MplsTunnelIndex,
                                                     u4MplsTunnelInstance,
                                                     u4MplsTunnelIngressLSRId,
                                                     u4MplsTunnelEgressLSRId));
}

/****************************************************************************
 Function    :  nmhGetFirstIndexMplsTunnelPerfTable
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexMplsTunnelPerfTable (UINT4 *pu4MplsTunnelIndex,
                                     UINT4 *pu4MplsTunnelInstance,
                                     UINT4 *pu4MplsTunnelIngressLSRId,
                                     UINT4 *pu4MplsTunnelEgressLSRId)
{
    return (nmhGetFirstIndexMplsTunnelTable (pu4MplsTunnelIndex,
                                             pu4MplsTunnelInstance,
                                             pu4MplsTunnelIngressLSRId,
                                             pu4MplsTunnelEgressLSRId));
}

/****************************************************************************
 Function    :  nmhGetNextIndexMplsTunnelPerfTable
 Input       :  The Indices
                MplsTunnelIndex
                nextMplsTunnelIndex
                MplsTunnelInstance
                nextMplsTunnelInstance
                MplsTunnelIngressLSRId
                nextMplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                nextMplsTunnelEgressLSRId
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexMplsTunnelPerfTable (UINT4 u4MplsTunnelIndex,
                                    UINT4 *pu4NextMplsTunnelIndex,
                                    UINT4 u4MplsTunnelInstance,
                                    UINT4 *pu4NextMplsTunnelInstance,
                                    UINT4 u4MplsTunnelIngressLSRId,
                                    UINT4 *pu4NextMplsTunnelIngressLSRId,
                                    UINT4 u4MplsTunnelEgressLSRId,
                                    UINT4 *pu4NextMplsTunnelEgressLSRId)
{
    return (nmhGetNextIndexMplsTunnelTable (u4MplsTunnelIndex,
                                            pu4NextMplsTunnelIndex,
                                            u4MplsTunnelInstance,
                                            pu4NextMplsTunnelInstance,
                                            u4MplsTunnelIngressLSRId,
                                            pu4NextMplsTunnelIngressLSRId,
                                            u4MplsTunnelEgressLSRId,
                                            pu4NextMplsTunnelEgressLSRId));
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelPerfPackets
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelPerfPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelPerfPackets (UINT4 u4MplsTunnelIndex,
                             UINT4 u4MplsTunnelInstance,
                             UINT4 u4MplsTunnelIngressLSRId,
                             UINT4 u4MplsTunnelEgressLSRId,
                             UINT4 *pu4RetValMplsTunnelPerfPackets)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        *pu4RetValMplsTunnelPerfPackets = 0;
#ifdef NPAPI_WANTED
        {
            tMplsInputParams    MplsStatsInput;
            tStatsInfo          StatsInfo;
            tXcEntry           *pXcEntry = NULL;

            MEMSET (&MplsStatsInput, 0, sizeof (tMplsInputParams));
            StatsInfo.u8Value.u4Hi = 0;
            StatsInfo.u8Value.u4Lo = 0;

            pXcEntry = MplsGetXCEntryByDirection (pTeTnlInfo->u4TnlXcIndex,
                                                  MPLS_DIRECTION_FORWARD);
            if ((pXcEntry != NULL) && (XC_OUTINDEX (pXcEntry) != NULL))
            {
                MplsStatsInput.i1NpReset = FALSE;
                MplsStatsInput.InputType = MPLS_GET_TNL_LBL_STATS;
                MPLS_HW_STATS_INPUT_LABEL (MplsStatsInput) =
                    OUTSEGMENT_LABEL (XC_OUTINDEX (pXcEntry));
                
                MplsStatsInput.u4TnlIfIndex = OUTSEGMENT_IF_INDEX(XC_OUTINDEX (pXcEntry));
                 if (XC_ININDEX(pXcEntry) != NULL)
                 {
                    MplsStatsInput.MplsInLabel = INSEGMENT_LABEL(XC_ININDEX(pXcEntry));
                 }  
                MplsFsMplsHwGetMplsStats (&MplsStatsInput,
                                          MPLS_HW_STAT_TUNNEL_OUTPKTS,
                                          &StatsInfo);
                *pu4RetValMplsTunnelPerfPackets = StatsInfo.u8Value.u4Lo;
            }
        }
#endif
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelPerfHCPackets
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelPerfHCPackets
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelPerfHCPackets (UINT4 u4MplsTunnelIndex,
                               UINT4 u4MplsTunnelInstance,
                               UINT4 u4MplsTunnelIngressLSRId,
                               UINT4 u4MplsTunnelEgressLSRId,
                               tSNMP_COUNTER64_TYPE *
                               pu8RetValMplsTunnelPerfHCPackets)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlInfo         *pInstance0Tnl = NULL;
    UINT4              u4MplsPrimaryTunnelInstance;
    MPLS_CMN_LOCK ();

    if (u4MplsTunnelInstance == TE_ZERO)
    {
        pInstance0Tnl = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                         u4MplsTunnelIngressLSRId,
                                         u4MplsTunnelEgressLSRId);
        if (pInstance0Tnl == NULL)
        {
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }
        u4MplsPrimaryTunnelInstance = 
                TE_TNL_PRIMARY_TNL_INSTANCE(pInstance0Tnl);
    }
    else
    {
        u4MplsPrimaryTunnelInstance = u4MplsTunnelInstance;
    }
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsPrimaryTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        pu8RetValMplsTunnelPerfHCPackets->msn = 0;
        pu8RetValMplsTunnelPerfHCPackets->lsn = 0;
#ifdef NPAPI_WANTED
        {
            tMplsInputParams    MplsStatsInput;
            tStatsInfo          StatsInfo;
            tXcEntry           *pXcEntry = NULL;

            MEMSET (&MplsStatsInput, 0, sizeof (tMplsInputParams));
            StatsInfo.u8Value.u4Hi = 0;
            StatsInfo.u8Value.u4Lo = 0;

            pXcEntry = MplsGetXCEntryByDirection (pTeTnlInfo->u4TnlXcIndex,
                                                  MPLS_DIRECTION_FORWARD);
            if ((pXcEntry != NULL) && (XC_OUTINDEX (pXcEntry) != NULL))
            {
                MplsStatsInput.i1NpReset = FALSE;
                MplsStatsInput.InputType = MPLS_GET_TNL_LBL_STATS;
                MPLS_HW_STATS_INPUT_LABEL (MplsStatsInput) =
                    OUTSEGMENT_LABEL (XC_OUTINDEX (pXcEntry));
                MplsStatsInput.u4TnlIfIndex = OUTSEGMENT_IF_INDEX(XC_OUTINDEX (pXcEntry));
                if (XC_ININDEX(pXcEntry) != NULL)
                {
                   MplsStatsInput.MplsInLabel = INSEGMENT_LABEL(XC_ININDEX(pXcEntry));
                }
                MplsFsMplsHwGetMplsStats (&MplsStatsInput,
                                          MPLS_HW_STAT_TUNNEL_OUTPKTS,
                                          &StatsInfo);
                pu8RetValMplsTunnelPerfHCPackets->msn = StatsInfo.u8Value.u4Hi;
                pu8RetValMplsTunnelPerfHCPackets->lsn = StatsInfo.u8Value.u4Lo;
            }
        }
#endif
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelPerfErrors
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelPerfErrors
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelPerfErrors (UINT4 u4MplsTunnelIndex, UINT4 u4MplsTunnelInstance,
                            UINT4 u4MplsTunnelIngressLSRId,
                            UINT4 u4MplsTunnelEgressLSRId,
                            UINT4 *pu4RetValMplsTunnelPerfErrors)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlInfo         *pInstance0Tnl = NULL;
    UINT4              u4MplsPrimaryTunnelInstance;    
    MPLS_CMN_LOCK ();

    if (u4MplsTunnelInstance == TE_ZERO)
    {
        pInstance0Tnl = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                         u4MplsTunnelIngressLSRId,
                                         u4MplsTunnelEgressLSRId);
        if (pInstance0Tnl == NULL)
        {
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }
        u4MplsPrimaryTunnelInstance =
                TE_TNL_PRIMARY_TNL_INSTANCE(pInstance0Tnl);
    }
    else
    {
        u4MplsPrimaryTunnelInstance = u4MplsTunnelInstance;
    }

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsPrimaryTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        *pu4RetValMplsTunnelPerfErrors = 0;
#ifdef NPAPI_WANTED
        {
            tMplsInputParams    MplsStatsInput;
            tStatsInfo          StatsInfo;
            tXcEntry           *pXcEntry = NULL;

            MEMSET (&MplsStatsInput, 0, sizeof (tMplsInputParams));
            StatsInfo.u8Value.u4Hi = 0;
            StatsInfo.u8Value.u4Lo = 0;

            pXcEntry = MplsGetXCEntryByDirection (pTeTnlInfo->u4TnlXcIndex,
                                                  MPLS_DIRECTION_FORWARD);
            if ((pXcEntry != NULL) && (XC_OUTINDEX (pXcEntry) != NULL))
            {
                MplsStatsInput.i1NpReset = FALSE;
                MplsStatsInput.InputType = MPLS_GET_TNL_LBL_STATS;
                MPLS_HW_STATS_INPUT_LABEL (MplsStatsInput) =
                    OUTSEGMENT_LABEL (XC_OUTINDEX (pXcEntry));
                MplsStatsInput.u4TnlIfIndex = OUTSEGMENT_IF_INDEX(XC_OUTINDEX (pXcEntry));
                if (XC_ININDEX(pXcEntry) != NULL)
                {
                    MplsStatsInput.MplsInLabel = INSEGMENT_LABEL(XC_ININDEX(pXcEntry));
                }
                MplsFsMplsHwGetMplsStats (&MplsStatsInput,
                                          MPLS_HW_STAT_TUNNEL_OUTERR,
                                          &StatsInfo);
                *pu4RetValMplsTunnelPerfErrors = StatsInfo.u8Value.u4Lo;
            }
        }
#endif
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelPerfBytes
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelPerfBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelPerfBytes (UINT4 u4MplsTunnelIndex, UINT4 u4MplsTunnelInstance,
                           UINT4 u4MplsTunnelIngressLSRId,
                           UINT4 u4MplsTunnelEgressLSRId,
                           UINT4 *pu4RetValMplsTunnelPerfBytes)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        *pu4RetValMplsTunnelPerfBytes = 0;
#ifdef NPAPI_WANTED
        {
            tMplsInputParams    MplsStatsInput;
            tStatsInfo          StatsInfo;
            tXcEntry           *pXcEntry = NULL;

            MEMSET (&MplsStatsInput, 0, sizeof (tMplsInputParams));
            StatsInfo.u8Value.u4Hi = 0;
            StatsInfo.u8Value.u4Lo = 0;

            pXcEntry = MplsGetXCEntryByDirection (pTeTnlInfo->u4TnlXcIndex,
                                                  MPLS_DIRECTION_FORWARD);
            if ((pXcEntry != NULL) && (XC_OUTINDEX (pXcEntry) != NULL))
            {
                MplsStatsInput.i1NpReset = FALSE;
                MplsStatsInput.InputType = MPLS_GET_TNL_LBL_STATS;
                MPLS_HW_STATS_INPUT_LABEL (MplsStatsInput) =
                    OUTSEGMENT_LABEL (XC_OUTINDEX (pXcEntry));
                
		if (XC_ININDEX(pXcEntry) != NULL)
                {
                    MplsStatsInput.MplsInLabel = INSEGMENT_LABEL(XC_ININDEX(pXcEntry));
                }
                MplsFsMplsHwGetMplsStats (&MplsStatsInput,
                                          MPLS_HW_STAT_TUNNEL_OUTBYTES,
                                          &StatsInfo);
                *pu4RetValMplsTunnelPerfBytes = StatsInfo.u8Value.u4Lo;
            }
        }
#endif
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelPerfHCBytes
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                retValMplsTunnelPerfHCBytes
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelPerfHCBytes (UINT4 u4MplsTunnelIndex,
                             UINT4 u4MplsTunnelInstance,
                             UINT4 u4MplsTunnelIngressLSRId,
                             UINT4 u4MplsTunnelEgressLSRId,
                             tSNMP_COUNTER64_TYPE *
                             pu8RetValMplsTunnelPerfHCBytes)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlInfo         *pInstance0Tnl = NULL;
    UINT4              u4MplsPrimaryTunnelInstance;

    MPLS_CMN_LOCK ();

    if (u4MplsTunnelInstance == TE_ZERO)
    {
        pInstance0Tnl = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                         u4MplsTunnelIngressLSRId,
                                         u4MplsTunnelEgressLSRId);
        if (pInstance0Tnl == NULL)
        {
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }
        u4MplsPrimaryTunnelInstance =
                TE_TNL_PRIMARY_TNL_INSTANCE(pInstance0Tnl);
    }
    else
    {
        u4MplsPrimaryTunnelInstance = u4MplsTunnelInstance;
    }

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsPrimaryTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo != NULL)
    {
        pu8RetValMplsTunnelPerfHCBytes->msn = 0;
        pu8RetValMplsTunnelPerfHCBytes->lsn = 0;
#ifdef NPAPI_WANTED
        {
            tMplsInputParams    MplsStatsInput;
            tStatsInfo          StatsInfo;
            tXcEntry           *pXcEntry = NULL;

            MEMSET (&MplsStatsInput, 0, sizeof (tMplsInputParams));
            StatsInfo.u8Value.u4Hi = 0;
            StatsInfo.u8Value.u4Lo = 0;

            pXcEntry = MplsGetXCEntryByDirection (pTeTnlInfo->u4TnlXcIndex,
                                                  MPLS_DIRECTION_FORWARD);
            if ((pXcEntry != NULL) && (XC_OUTINDEX (pXcEntry) != NULL))
            {
                MplsStatsInput.i1NpReset = FALSE;
                MplsStatsInput.InputType = MPLS_GET_TNL_LBL_STATS;
                MPLS_HW_STATS_INPUT_LABEL (MplsStatsInput) =
                    OUTSEGMENT_LABEL (XC_OUTINDEX (pXcEntry));
                MplsStatsInput.InputType = MPLS_GET_TNL_LBL_STATS;
                MplsStatsInput.u4TnlIfIndex = OUTSEGMENT_IF_INDEX(XC_OUTINDEX (pXcEntry));
                if (XC_ININDEX(pXcEntry) != NULL)
                {
                    MplsStatsInput.MplsInLabel = INSEGMENT_LABEL(XC_ININDEX(pXcEntry));   
                }
                MplsFsMplsHwGetMplsStats (&MplsStatsInput,
                                          MPLS_HW_STAT_TUNNEL_OUTBYTES,
                                          &StatsInfo);
                pu8RetValMplsTunnelPerfHCBytes->msn = StatsInfo.u8Value.u4Hi;
                pu8RetValMplsTunnelPerfHCBytes->lsn = StatsInfo.u8Value.u4Lo;
            }
        }
#endif
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* Low Level GET Routine for All Scalar Objects  */

/****************************************************************************
 Function    :  nmhGetMplsTunnelConfigured
 Input       :  The Indices

                The Object 
                retValMplsTunnelConfigured
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelConfigured (UINT4 *pu4RetValMplsTunnelConfigured)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    *pu4RetValMplsTunnelConfigured = 0;

    MPLS_CMN_LOCK ();
    pTeTnlInfo = RBTreeGetFirst (gTeGblInfo.TnlInfoTbl);

    while (pTeTnlInfo != NULL)
    {
        if (pTeTnlInfo->u1TnlRowStatus == ACTIVE)
        {
            (*pu4RetValMplsTunnelConfigured)++;
        }
        pTeTnlInfo = RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                    (tRBElem *) pTeTnlInfo, NULL);
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelActive
 Input       :  The Indices

                The Object 
                retValMplsTunnelActive
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelActive (UINT4 *pu4RetValMplsTunnelActive)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    *pu4RetValMplsTunnelActive = 0;
    MPLS_CMN_LOCK ();

    pTeTnlInfo = RBTreeGetFirst (gTeGblInfo.TnlInfoTbl);

    while (pTeTnlInfo != NULL)
    {
        if (pTeTnlInfo->u1TnlOperStatus == TE_OPER_UP)
        {
            (*pu4RetValMplsTunnelActive)++;
        }
        pTeTnlInfo = RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                    (tRBElem *) pTeTnlInfo, NULL);
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelTEDistProto
 Input       :  The Indices

                The Object 
                retValMplsTunnelTEDistProto
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelTEDistProto (tSNMP_OCTET_STRING_TYPE *
                             pRetValMplsTunnelTEDistProto)
{
    /* BIT Position 
       --------------------------------
       | others(0)  | ospf(1) | isis(2) |
       --------------------------------
       |    1       |   0     |   0     |
       --------------------------------

       Check RFC 1906 Section 8 (3) for BITS Encoding 
     */
    MPLS_CMN_LOCK ();
    pRetValMplsTunnelTEDistProto->pu1_OctetList[0] = 0x8;    /* None */
    pRetValMplsTunnelTEDistProto->i4_Length = 1;
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelMaxHops
 Input       :  The Indices

                The Object 
                retValMplsTunnelMaxHops
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelMaxHops (UINT4 *pu4RetValMplsTunnelMaxHops)
{
    MPLS_CMN_LOCK ();
    *pu4RetValMplsTunnelMaxHops = TE_MAX_HOP_PER_PO (gTeGblInfo);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetMplsTunnelNotificationMaxRate
 Input       :  The Indices

                The Object
                retValMplsTunnelNotificationMaxRate
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelNotificationMaxRate (UINT4
                                     *pu4RetValMplsTunnelNotificationMaxRate)
{

    MPLS_CMN_LOCK ();
    *pu4RetValMplsTunnelNotificationMaxRate =
        TE_NOTIFICATION_MAX_RATE (gTeGblInfo);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/* LOW LEVEL Routines for Table : FsMplsFrrTunARHopTable. */

/****************************************************************************
 Function    :  nmhValidateIndexInstanceFsMplsFrrTunARHopTable
 Input       :  The Indices
                MplsTunnelARHopListIndex
                MplsTunnelARHopIndex
 Output      :  The Routines Validates the Given Indices.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_EXACT Validate Index Instance Routine. */

INT1
nmhValidateIndexInstanceFsMplsFrrTunARHopTable (UINT4
                                                u4MplsTunnelARHopListIndex,
                                                UINT4 u4MplsTunnelARHopIndex)
{
    if (nmhValidateIndexInstanceMplsTunnelARHopTable
        (u4MplsTunnelARHopListIndex, u4MplsTunnelARHopIndex) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFirstIndexFsMplsFrrTunARHopTable
 Input       :  The Indices
                MplsTunnelARHopListIndex
                MplsTunnelARHopIndex
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
nmhGetFirstIndexFsMplsFrrTunARHopTable (UINT4 *pu4MplsTunnelARHopListIndex,
                                        UINT4 *pu4MplsTunnelARHopIndex)
{
    if (nmhGetFirstIndexMplsTunnelARHopTable (pu4MplsTunnelARHopListIndex,
                                              pu4MplsTunnelARHopIndex)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetNextIndexFsMplsFrrTunARHopTable
 Input       :  The Indices
                MplsTunnelARHopListIndex
                nextMplsTunnelARHopListIndex
                MplsTunnelARHopIndex
                nextMplsTunnelARHopIndex
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMplsFrrTunARHopTable (UINT4 u4MplsTunnelARHopListIndex,
                                       UINT4
                                       *pu4NextMplsTunnelARHopListIndex,
                                       UINT4 u4MplsTunnelARHopIndex,
                                       UINT4 *pu4NextMplsTunnelARHopIndex)
{
    if (nmhGetNextIndexMplsTunnelARHopTable (u4MplsTunnelARHopListIndex,
                                             pu4NextMplsTunnelARHopListIndex,
                                             u4MplsTunnelARHopIndex,
                                             pu4NextMplsTunnelARHopIndex)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsMplsFrrTunARHopProtectType
 Input       :  The Indices
                MplsTunnelARHopListIndex
                MplsTunnelARHopIndex

                The Object 
                retValFsMplsFrrTunARHopProtectType
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsFrrTunARHopProtectType (UINT4 u4MplsTunnelARHopListIndex,
                                    UINT4 u4MplsTunnelARHopIndex,
                                    tSNMP_OCTET_STRING_TYPE
                                    * pRetValFsMplsFrrTunARHopProtectType)
{
    tTeArHopInfo       *pTeArHopInfo = NULL;
    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get This Object, pTeArHopInfo should not be NULL */
    if (TeCheckArHopInfo (u4MplsTunnelARHopListIndex,
                          u4MplsTunnelARHopIndex, &pTeArHopInfo) == TE_FAILURE)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* Copying the Values */
    MEMCPY (pRetValFsMplsFrrTunARHopProtectType->pu1_OctetList,
            (UINT1 *) &(TE_FRR_ARHOP_PROT_TYPE (pTeArHopInfo)), sizeof (UINT1));
    pRetValFsMplsFrrTunARHopProtectType->i4_Length = sizeof (UINT1);

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsFrrTunARHopProtectTypeInUse
 Input       :  The Indices
                MplsTunnelARHopListIndex
                MplsTunnelARHopIndex

                The Object 
                retValFsMplsFrrTunARHopProtectTypeInUse
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsFrrTunARHopProtectTypeInUse (UINT4 u4MplsTunnelARHopListIndex,
                                         UINT4 u4MplsTunnelARHopIndex,
                                         tSNMP_OCTET_STRING_TYPE
                                         *
                                         pRetValFsMplsFrrTunARHopProtectTypeInUse)
{
    tTeArHopInfo       *pTeArHopInfo = NULL;
    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get This Object, pTeArHopInfo should not be NULL */
    if (TeCheckArHopInfo (u4MplsTunnelARHopListIndex,
                          u4MplsTunnelARHopIndex, &pTeArHopInfo) == TE_FAILURE)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* Copying the Values */
    MEMCPY (pRetValFsMplsFrrTunARHopProtectTypeInUse->pu1_OctetList,
            (UINT1 *) &(TE_FRR_ARHOP_PROT_TYPE_IN_USE (pTeArHopInfo)),
            sizeof (UINT1));
    pRetValFsMplsFrrTunARHopProtectTypeInUse->i4_Length = sizeof (UINT1);

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsFrrTunARHopLabel
 Input       :  The Indices
                MplsTunnelARHopListIndex
                MplsTunnelARHopIndex

                The Object 
                retValFsMplsFrrTunARHopLabel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsFrrTunARHopLabel (UINT4 u4MplsTunnelARHopListIndex,
                              UINT4 u4MplsTunnelARHopIndex,
                              UINT4 *pu4RetValFsMplsFrrTunARHopLabel)
{
    tTeArHopInfo       *pTeArHopInfo = NULL;
    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get This Object, pTeArHopInfo should not be NULL */
    if (TeCheckArHopInfo (u4MplsTunnelARHopListIndex,
                          u4MplsTunnelARHopIndex, &pTeArHopInfo) == TE_FAILURE)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* Copying the Values */
    *pu4RetValFsMplsFrrTunARHopLabel = TE_FRR_ARHOP_LABEL (pTeArHopInfo);

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsMplsFrrTunARBwProtAvailable
 Input       :  The Indices
                MplsTunnelARHopListIndex
                MplsTunnelARHopIndex

                The Object
                retValFsMplsFrrTunARBwProtAvailable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsMplsFrrTunARBwProtAvailable (UINT4 u4MplsTunnelARHopListIndex,
                                     UINT4 u4MplsTunnelARHopIndex,
                                     INT4
                                     *pi4RetValFsMplsFrrTunARBwProtAvailable)
{
    tTeArHopInfo       *pTeArHopInfo = NULL;
    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Get This Object, pTeArHopInfo should not be NULL */
    if (TeCheckArHopInfo (u4MplsTunnelARHopListIndex,
                          u4MplsTunnelARHopIndex, &pTeArHopInfo) == TE_FAILURE)
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* Copying the Values */
    *pi4RetValFsMplsFrrTunARBwProtAvailable
        = TE_FRR_ARHOP_BW_PROT_AVAIL (pTeArHopInfo);

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetMplsTunnelNotificationEnable
 Input       :  The Indices

                The Object
                retValMplsTunnelNotificationEnable
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetMplsTunnelNotificationEnable (INT4 *pi4RetValMplsTunnelNotificationEnable)
{
    *pi4RetValMplsTunnelNotificationEnable =
        gTeGblInfo.TeParams.i4NotificationEnable;
    return SNMP_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/*                       End of file teget2.c                                */
/*---------------------------------------------------------------------------*/
