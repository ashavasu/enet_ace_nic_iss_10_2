#include "teincs.h"

/* $Id: teget.c,v 1.16 2014/12/12 11:56:48 siva Exp $ */

/****************************************************************************
 Function    :  TeGetAllGmplsTunnelTable
 Description :  This function gets GMPLS Tunnel MIB objects.
 Input       :  u4TnlIndex        - Tunnel Index
                u4TnlInstance     - Tunnel Instance
                u4IngressId       - Tunnel Ingress Id
                u4EgressId        - Tunnel Egress Id
 Output      :  pGmplsTnlInfo     - Pointer to GMPLS Tunnel Info
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TeGetAllGmplsTunnelTable (UINT4 u4TnlIndex, UINT4 u4TnlInstance,
                          UINT4 u4IngressId, UINT4 u4EgressId,
                          tGmplsTnlInfo * pGmplsTnlInfo)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    pTeTnlInfo = TeGetTunnelInfo (u4TnlIndex, u4TnlInstance,
                                  u4IngressId, u4EgressId);
    if (pTeTnlInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pGmplsTnlInfo->bUnnumIf = pTeTnlInfo->GmplsTnlInfo.bUnnumIf;

    pGmplsTnlInfo->u1AttrLen = pTeTnlInfo->GmplsTnlInfo.u1AttrLen;

    pGmplsTnlInfo->u1Attributes = pTeTnlInfo->GmplsTnlInfo.u1Attributes;

    pGmplsTnlInfo->u1EncodingType = pTeTnlInfo->GmplsTnlInfo.u1EncodingType;

    pGmplsTnlInfo->u1SwitchingType = pTeTnlInfo->GmplsTnlInfo.u1SwitchingType;

    pGmplsTnlInfo->u1LinkProtection = pTeTnlInfo->GmplsTnlInfo.u1LinkProtection;

    pGmplsTnlInfo->u1LinkProtLen = pTeTnlInfo->GmplsTnlInfo.u1LinkProtLen;

    pGmplsTnlInfo->u2Gpid = pTeTnlInfo->GmplsTnlInfo.u2Gpid;

    pGmplsTnlInfo->u1Secondary = pTeTnlInfo->GmplsTnlInfo.u1Secondary;

    pGmplsTnlInfo->u1Direction = pTeTnlInfo->GmplsTnlInfo.u1Direction;

    pGmplsTnlInfo->SendPathNotifyRecipientType =
        pTeTnlInfo->GmplsTnlInfo.SendPathNotifyRecipientType;

    pGmplsTnlInfo->u4SendPathNotifyRecipient =
        pTeTnlInfo->GmplsTnlInfo.u4SendPathNotifyRecipient;

    pGmplsTnlInfo->SendResvNotifyRecipientType =
        pTeTnlInfo->GmplsTnlInfo.SendResvNotifyRecipientType;

    pGmplsTnlInfo->u4SendResvNotifyRecipient =
        pTeTnlInfo->GmplsTnlInfo.u4SendResvNotifyRecipient;

    pGmplsTnlInfo->UpstreamNotifyRecipientType = (eInetAddrType) 0;

    pGmplsTnlInfo->u4UpstreamNotifyRecipient = 0;

    pGmplsTnlInfo->DownstreamNotifyRecipientType = (eInetAddrType) 0;

    pGmplsTnlInfo->u4DownstreamNotifyRecipient = 0;

    pGmplsTnlInfo->PathComp = pTeTnlInfo->GmplsTnlInfo.PathComp;

    pGmplsTnlInfo->u4AdminStatus = pTeTnlInfo->GmplsTnlInfo.u4AdminStatus;

    pGmplsTnlInfo->u1AdminStatusLen = pTeTnlInfo->GmplsTnlInfo.u1AdminStatusLen;

    return SNMP_SUCCESS;
}

/****************************************************************************
  Function     : TeGetAllGmplsTunnelHopTable
  Description  : This function gets the GMPLS Tunnel Hop Table Objects. 
  Input        : u4HopListIndex       - Hop List Index
                 u4PathOptionIndex    - Path Option Index
                 u4HopIndex           - Hop Index
  Output      :  pGmplsTnlHopInfo     - Pointer to GMPLS Tunnel Hop Info
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TeGetAllGmplsTunnelHopTable (UINT4 u4HopListIndex, UINT4 u4HopPathOptionIndex,
                             UINT4 u4HopIndex,
                             tGmplsTnlHopInfo * pGmplsTnlHopInfo)
{
    tTeHopInfo         *pTeHopInfo = NULL;

    if (TeCheckHopInfo (u4HopListIndex, u4HopPathOptionIndex,
                        u4HopIndex, &pTeHopInfo) == TE_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pGmplsTnlHopInfo->u1LblStatus = pTeHopInfo->GmplsTnlHopInfo.u1LblStatus;
    pGmplsTnlHopInfo->u4ForwardLbl = pTeHopInfo->GmplsTnlHopInfo.u4ForwardLbl;
    pGmplsTnlHopInfo->u4ReverseLbl = pTeHopInfo->GmplsTnlHopInfo.u4ReverseLbl;

    return SNMP_SUCCESS;
}

/****************************************************************************
  Function     : TeGetAllGmplsTunnelArHopTable
  Description  : This function gets the GMPLS Tunnel AR Hop Table Objects.
  Input        : u4HopListIndex       - Hop List Index
                 u4PathOptionIndex    - Path Option Index
                 u4HopIndex           - Hop Index
  Output      :  pGmplsTnlArHopInfo   - Pointer to GMPLS Tunnel AR Hop Info
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TeGetAllGmplsTunnelArHopTable (UINT4 u4HopListIndex, UINT4 u4HopIndex,
                               tGmplsTnlArHopInfo * pGmplsTnlArHopInfo)
{
    tTeArHopInfo       *pTeArHopInfo = NULL;

    if (TeCheckArHopInfo (u4HopListIndex, u4HopIndex, &pTeArHopInfo)
        == TE_FAILURE)
    {
        return SNMP_FAILURE;
    }

    pGmplsTnlArHopInfo->u1LblStatus
        = pTeArHopInfo->GmplsTnlArHopInfo.u1LblStatus;
    pGmplsTnlArHopInfo->u4ForwardLbl
        = pTeArHopInfo->GmplsTnlArHopInfo.u4ForwardLbl;
    pGmplsTnlArHopInfo->u4ReverseLbl
        = pTeArHopInfo->GmplsTnlArHopInfo.u4ReverseLbl;
    pGmplsTnlArHopInfo->u1Protection
        = pTeArHopInfo->GmplsTnlArHopInfo.u1Protection;
    pGmplsTnlArHopInfo->u1ProtLen = pTeArHopInfo->GmplsTnlArHopInfo.u1ProtLen;

    return SNMP_SUCCESS;
}

/****************************************************************************
  Function     : TeGetAllGmplsTunnelCHopTable
  Description  : This function gets the GMPLS Tunnel CHop Table Objects.
  Input        : u4HopListIndex       - Hop List Index
                 u4HopIndex           - Hop Index
  Output      :  pGetTeCHopInfo       - Pointer to CHop Info
  Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TeGetAllMplsTunnelCHopTable (UINT4 u4HopListIndex, UINT4 u4HopIndex,
                             tTeCHopInfo * pGetTeCHopInfo)
{
    tTeCHopInfo        *pTeCHopInfo = NULL;

    pTeCHopInfo = TeGetCHop (u4HopListIndex, u4HopIndex);

    if (pTeCHopInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pGetTeCHopInfo->u1AddressType = pTeCHopInfo->u1AddressType;

    MEMCPY (pGetTeCHopInfo->IpAddr.au1Ipv4Addr,
            pTeCHopInfo->IpAddr.au1Ipv4Addr, sizeof (UINT4));

    pGetTeCHopInfo->u1AddrPrefixLen = pTeCHopInfo->u1AddrPrefixLen;
    pGetTeCHopInfo->u4UnnumIf = pTeCHopInfo->u4UnnumIf;
    pGetTeCHopInfo->u2AsNumber = pTeCHopInfo->u2AsNumber;
    pGetTeCHopInfo->u4LocalLspId = pTeCHopInfo->u4LocalLspId;
    pGetTeCHopInfo->u1CHopType = pTeCHopInfo->u1CHopType;

    pGetTeCHopInfo->GmplsTnlCHopInfo.u1LblStatus
        = pTeCHopInfo->GmplsTnlCHopInfo.u1LblStatus;
    pGetTeCHopInfo->GmplsTnlCHopInfo.u4ForwardLbl
        = pTeCHopInfo->GmplsTnlCHopInfo.u4ForwardLbl;
    pGetTeCHopInfo->GmplsTnlCHopInfo.u4ReverseLbl
        = pTeCHopInfo->GmplsTnlCHopInfo.u4ReverseLbl;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TeGetAllGmplsTunnelErrorTable
 Description :  This function gets GMPLS Tunnel MIB objects.
 Input       :  u4TnlIndex        - Tunnel Index
                u4TnlInstance     - Tunnel Instance
                u4IngressId       - Tunnel Ingress Id
                u4EgressId        - Tunnel Egress Id
 Output      :  pGmplsTnlErrInfo  - Pointer to GMPLS Tunnel Error Info
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TeGetAllGmplsTunnelErrorTable (UINT4 u4TnlIndex, UINT4 u4TnlInstance,
                               UINT4 u4IngressId, UINT4 u4EgressId,
                               tTunnelErrInfo * pGmplsTnlErrInfo)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    pTeTnlInfo = TeGetTunnelInfo (u4TnlIndex, u4TnlInstance,
                                  u4IngressId, u4EgressId);
    if (pTeTnlInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    if (pTeTnlInfo->pTunnelErrInfo == NULL)
    {
        return SNMP_SUCCESS;
    }

    pGmplsTnlErrInfo->LastErrType = pTeTnlInfo->pTunnelErrInfo->LastErrType;

    pGmplsTnlErrInfo->u4LastErrTime = pTeTnlInfo->pTunnelErrInfo->u4LastErrTime;

    pGmplsTnlErrInfo->ErrorReporterType
        = pTeTnlInfo->pTunnelErrInfo->ErrorReporterType;

    pGmplsTnlErrInfo->u4ErrorIpAddress
        = pTeTnlInfo->pTunnelErrInfo->u4ErrorIpAddress;

    pGmplsTnlErrInfo->u1ErrorIpAddressLen
        = pTeTnlInfo->pTunnelErrInfo->u1ErrorIpAddressLen;

    pGmplsTnlErrInfo->u4ErrorCode = pTeTnlInfo->pTunnelErrInfo->u4ErrorCode;

    pGmplsTnlErrInfo->u4SubErrorCode =
        pTeTnlInfo->pTunnelErrInfo->u4SubErrorCode;

    pGmplsTnlErrInfo->i4GmplsTnlErrTLVLength =
        pTeTnlInfo->pTunnelErrInfo->i4GmplsTnlErrTLVLength;

    MEMCPY (&(pGmplsTnlErrInfo->au1GmplsTnlErrTLV),
            &(pTeTnlInfo->pTunnelErrInfo->au1GmplsTnlErrTLV),
            pGmplsTnlErrInfo->i4GmplsTnlErrTLVLength);

    pGmplsTnlErrInfo->i4GmplsErrHelpStringLength =
        pTeTnlInfo->pTunnelErrInfo->i4GmplsErrHelpStringLength;

    MEMCPY (&(pGmplsTnlErrInfo->au1GmplsErrHelpString),
            &(pTeTnlInfo->pTunnelErrInfo->au1GmplsErrHelpString),
            pGmplsTnlErrInfo->i4GmplsErrHelpStringLength);

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TeGetAllFsMplsTunnelTable
 Description :  This function gets FS MPLS Tunnel MIB objects.
 Input       :  u4TnlIndex        - Tunnel Index
                u4TnlInstance     - Tunnel Instance
                u4IngressId       - Tunnel Ingress Id
                u4EgressId        - Tunnel Egress Id
 Output      :  pGetTeTnlInfo     - Pointer to MPLS Tunnel Info
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TeGetAllFsMplsTunnelTable (UINT4 u4TnlIndex, UINT4 u4TnlInstance,
                           UINT4 u4IngressId, UINT4 u4EgressId,
                           tTeTnlInfo * pGetTeTnlInfo)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    pTeTnlInfo = TeGetTunnelInfo (u4TnlIndex, u4TnlInstance,
                                  u4IngressId, u4EgressId);
    if (pTeTnlInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pGetTeTnlInfo->u4TnlType = pTeTnlInfo->u4TnlType;

    pGetTeTnlInfo->u4TnlLsrIdMapInfo = pTeTnlInfo->u4TnlLsrIdMapInfo;

    pGetTeTnlInfo->u4TnlMode = pTeTnlInfo->u4TnlMode;

    pGetTeTnlInfo->u4ProactiveSessionIndex
        = pTeTnlInfo->u4ProactiveSessionIndex;

    pGetTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType
        = pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType;

    pGetTeTnlInfo->u1TnlProtMethod = pTeTnlInfo->u1TnlProtMethod;

    pGetTeTnlInfo->u1TnlSrlgType = pTeTnlInfo->u1TnlSrlgType;

    pGetTeTnlInfo->u4AttrParamIndex = pTeTnlInfo->u4AttrParamIndex;

    pGetTeTnlInfo->u1TnlProtOperType = pTeTnlInfo->u1TnlProtOperType;

    pGetTeTnlInfo->u4BkpTnlIndex = pTeTnlInfo->u4BkpTnlIndex;

    pGetTeTnlInfo->u4BkpTnlInstance = pTeTnlInfo->u4BkpTnlInstance;

    MEMCPY (pGetTeTnlInfo->BkpTnlIngressLsrId, pTeTnlInfo->BkpTnlIngressLsrId,
            ROUTER_ID_LENGTH);

    MEMCPY (pGetTeTnlInfo->BkpTnlEgressLsrId, pTeTnlInfo->BkpTnlEgressLsrId,
            ROUTER_ID_LENGTH);

    pGetTeTnlInfo->u1TnlPathType = pTeTnlInfo->u1TnlPathType;

    pGetTeTnlInfo->u4TnlMapIndex = pTeTnlInfo->u4TnlMapIndex;

    pGetTeTnlInfo->u4TnlMapInstance = pTeTnlInfo->u4TnlMapInstance;

    pGetTeTnlInfo->u4TnlIfIndex = pTeTnlInfo->u4TnlIfIndex;

    MEMCPY (pGetTeTnlInfo->TnlMapIngressLsrId, pTeTnlInfo->TnlMapIngressLsrId,
            ROUTER_ID_LENGTH);

    MEMCPY (pGetTeTnlInfo->TnlMapEgressLsrId, pTeTnlInfo->TnlMapEgressLsrId,
            ROUTER_ID_LENGTH);

    pGetTeTnlInfo->u1TnlMBBStatus = pTeTnlInfo->u1TnlMBBStatus;

    pGetTeTnlInfo->u4TnlDisJointType = pTeTnlInfo->u4TnlDisJointType;

    pGetTeTnlInfo->u1TnlSyncStatus = pTeTnlInfo->u1TnlSyncStatus;

    pGetTeTnlInfo->u4OutPathMsgId = pTeTnlInfo->u4OutPathMsgId;

    pGetTeTnlInfo->u4OutRecoveryPathMsgId = pTeTnlInfo->u4OutRecoveryPathMsgId;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TeGetAllFsMplsAttributeTable
 Description :  This function gets FS MPLS Attribute Table Objects
 Input       :  u4AttrListIndex     - Attribute List Index
 Output      :  pGetTeAttrListInfo  - Pointer to Attribute List Info
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TeGetAllFsMplsAttributeTable (UINT4 u4AttrListIndex,
                              tTeAttrListInfo * pGetTeAttrListInfo)
{
    tTeAttrListInfo    *pTeAttrListInfo = NULL;

    pTeAttrListInfo = TeGetAttrListInfo (u4AttrListIndex);

    if (pTeAttrListInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    MEMCPY (pGetTeAttrListInfo->au1ListName, pTeAttrListInfo->au1ListName,
            pTeAttrListInfo->i4ListNameLen);

    pGetTeAttrListInfo->i4ListNameLen = pTeAttrListInfo->i4ListNameLen;

    if (pTeAttrListInfo->u4ListBitmask & TE_ATTR_SETUPPRI_BITMASK)
    {
        pGetTeAttrListInfo->u1SetupPriority = pTeAttrListInfo->u1SetupPriority;
    }

    if (pTeAttrListInfo->u4ListBitmask & TE_ATTR_HOLDPRI_BITMASK)
    {
        pGetTeAttrListInfo->u1HoldingPriority =
            pTeAttrListInfo->u1HoldingPriority;
    }

    if (pTeAttrListInfo->u4ListBitmask & TE_ATTR_LSP_SSN_ATTR_BITMASK)
    {
        pGetTeAttrListInfo->u1SsnAttr = pTeAttrListInfo->u1SsnAttr;
        pGetTeAttrListInfo->u1SsnAttrLen = pTeAttrListInfo->u1SsnAttrLen;
    }

    if (pTeAttrListInfo->u4ListBitmask & TE_ATTR_INCLUDE_ANY_AFFINITY_BITMASK)
    {
        pGetTeAttrListInfo->u4IncludeAnyAffinity
            = pTeAttrListInfo->u4IncludeAnyAffinity;
    }

    if (pTeAttrListInfo->u4ListBitmask & TE_ATTR_INCLUDE_ALL_AFFINITY_BITMASK)
    {
        pGetTeAttrListInfo->u4IncludeAllAffinity
            = pTeAttrListInfo->u4IncludeAllAffinity;
    }

    if (pTeAttrListInfo->u4ListBitmask & TE_ATTR_EXCLUDE_ANY_AFFINITY_BITMASK)
    {
        pGetTeAttrListInfo->u4ExcludeAnyAffinity
            = pTeAttrListInfo->u4ExcludeAnyAffinity;
    }

    if (pTeAttrListInfo->u4ListBitmask & TE_ATTR_BANDWIDTH_BITMASK)
    {
        pGetTeAttrListInfo->u4Bandwidth = pTeAttrListInfo->u4Bandwidth;
    }

    if (pTeAttrListInfo->u4ListBitmask & TE_ATTR_CLASS_TYPE_BITMASK)
    {
        pGetTeAttrListInfo->u4ClassType = pTeAttrListInfo->u4ClassType;
    }

    if (pTeAttrListInfo->u4ListBitmask & TE_ATTR_SRLG_TYPE_BITMASK)
    {
        pGetTeAttrListInfo->u1SrlgType = pTeAttrListInfo->u1SrlgType;
    }

    pGetTeAttrListInfo->u1RowStatus = pTeAttrListInfo->u1RowStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TeGetAllFsMplsTunnelSrlgTable
 Description :  This function gets FS MPLS Tunnel SRLG Table Objects
 Input       :  u4TnlIndex        - Tunnel Index
                u4TnlInstance     - Tunnel Instance
                u4IngressId       - Tunnel Ingress Id
                u4EgressId        - Tunnel Egress Id
                u4SrlgNo          - SRLG Value
 Output      :  pGetTeTnlSrlg     - Pointer to Tunnel SRLG Info
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TeGetAllFsMplsTunnelSrlgTable (UINT4 u4TnlIndex, UINT4 u4TnlInstance,
                               UINT4 u4IngressId, UINT4 u4EgressId,
                               UINT4 u4SrlgNo, tTeTnlSrlg * pGetTeTnlSrlg)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlSrlg         *pTeTnlSrlg = NULL;

    pTeTnlInfo = TeGetTunnelInfo (u4TnlIndex, u4TnlInstance,
                                  u4IngressId, u4EgressId);
    if (pTeTnlInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pTeTnlSrlg = TeGetSrlgFromTnlSrlgList (u4SrlgNo, pTeTnlInfo);

    if (pTeTnlSrlg == NULL)
    {
        return SNMP_FAILURE;
    }

    pGetTeTnlSrlg->u4RowStatus = pTeTnlSrlg->u4RowStatus;

    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  TeGetAllFsMplsAttributeSrlgTable
 Description :  This function gets FS MPLS Tunnel SRLG Table Objects
 Input       :  u4AttrListIndex   - Attribute List Index
                u4SrlgNo          - SRLG Value
 Output      :  pGetTeAttrSrlg    - Pointer to Attribute SRLG Info
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
TeGetAllFsMplsAttributeSrlgTable (UINT4 u4AttrListIndex, UINT4 u4SrlgNo,
                                  tTeAttrSrlg * pGetTeAttrSrlg)
{
    tTeAttrListInfo    *pTeAttrListInfo = NULL;
    tTeAttrSrlg        *pTeAttrSrlg = NULL;

    pTeAttrListInfo = TeGetAttrListInfo (u4AttrListIndex);

    if (pTeAttrListInfo == NULL)
    {
        return SNMP_FAILURE;
    }

    pTeAttrSrlg = TeGetSrlgFromAttrSrlgList (u4SrlgNo, pTeAttrListInfo);

    if (pTeAttrSrlg == NULL)
    {
        return SNMP_FAILURE;
    }

    pGetTeAttrSrlg->u4RowStatus = pTeAttrSrlg->u4RowStatus;

    return SNMP_SUCCESS;
}

/* Addition of two get functions*/
/****************************************************************************
 Function    :  GetFirstIndexFsMplsTunnelSrlgNumber
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                FsMplsTunnelSrlgNo
 Output      :  The Get First Routines gets the Lexicographicaly
                First Entry from the Table.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_FIRST Routine. */

INT1
GetFirstIndexFsMplsTunnelSrlgNumber (UINT4 *pu4MplsTunnelIndex,
                                       UINT4 *pu4MplsTunnelInstance,
                                       UINT4 *pu4MplsTunnelIngressLSRId,
                                       UINT4 *pu4MplsTunnelEgressLSRId,
                                       UINT4 *pu4FsMplsTunnelSrlgNo)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlSrlg         *pTeTnlSrlg = NULL;
    UINT4               u4EgressId = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;

    *pu4MplsTunnelIndex = TE_ZERO;
    *pu4MplsTunnelInstance = TE_ZERO;
    *pu4MplsTunnelIngressLSRId = TE_ZERO;
    *pu4MplsTunnelEgressLSRId = TE_ZERO;
    *pu4FsMplsTunnelSrlgNo = TE_ZERO;
 pTeTnlInfo = (tTeTnlInfo *) RBTreeGetFirst (gTeGblInfo.TnlInfoTbl);

    while (pTeTnlInfo != NULL)
    {
        pTeTnlSrlg = (tTeTnlSrlg *) TMO_SLL_First (&pTeTnlInfo->SrlgList);

        if (pTeTnlSrlg != NULL)
        {
            *pu4MplsTunnelIndex = pTeTnlInfo->u4TnlIndex;
            *pu4MplsTunnelInstance = pTeTnlInfo->u4TnlInstance;

            CONVERT_TO_INTEGER (pTeTnlInfo->TnlIngressLsrId, u4IngressId);
            CONVERT_TO_INTEGER (pTeTnlInfo->TnlEgressLsrId, u4EgressId);
            u4EgressId = OSIX_NTOHL (u4EgressId);
            u4IngressId = OSIX_NTOHL (u4IngressId);

            *pu4MplsTunnelIngressLSRId = u4IngressId;
            *pu4MplsTunnelEgressLSRId = u4EgressId;

            *pu4FsMplsTunnelSrlgNo = pTeTnlSrlg->u4SrlgNo;

            return SNMP_SUCCESS;
        }
        else
        {
            pTeTnlInfo
                = (tTeTnlInfo *) RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                                (tRBElem *) pTeTnlInfo, NULL);
        }
    }
    return SNMP_FAILURE;
}


/****************************************************************************
 Function    :  GetNextIndexFsMplsTunnelSrlgNumber
 Input       :  The Indices
                MplsTunnelIndex
                nextMplsTunnelIndex
                MplsTunnelInstance
                nextMplsTunnelInstance
 		MplsTunnelIngressLSRId
                nextMplsTunnelIngressLSRId
                MplsTunnelEgressLSRId
                nextMplsTunnelEgressLSRId
                FsMplsTunnelSrlgNo
                nextFsMplsTunnelSrlgNo
 Output      :  The Get Next function gets the Next Index for
                the Index Value given in the Index Values. The
                Indices are stored in the next_varname variables.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
/* GET_NEXT Routine.  */
INT1
GetNextIndexFsMplsTunnelSrlgNumber (UINT4 u4MplsTunnelIndex,
                                      UINT4 *pu4NextMplsTunnelIndex,
                                      UINT4 u4MplsTunnelInstance,
                                      UINT4 *pu4NextMplsTunnelInstance,
                                      UINT4 u4MplsTunnelIngressLSRId,
                                      UINT4 *pu4NextMplsTunnelIngressLSRId,
                                      UINT4 u4MplsTunnelEgressLSRId,
                                      UINT4 *pu4NextMplsTunnelEgressLSRId,
                                      UINT4 *pu4NextFsMplsTunnelSrlgNo)
{

 tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlSrlg         *pTeTnlSrlg = NULL;
    UINT4               u4EgressId = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;

    *pu4NextMplsTunnelIndex = TE_ZERO;
    *pu4NextMplsTunnelInstance = TE_ZERO;
    *pu4NextMplsTunnelIngressLSRId = TE_ZERO;
    *pu4NextMplsTunnelEgressLSRId = TE_ZERO;
    *pu4NextFsMplsTunnelSrlgNo = TE_ZERO;


    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);

    while (pTeTnlInfo != NULL)
    {
        TMO_SLL_Scan (&pTeTnlInfo->SrlgList, pTeTnlSrlg, tTeTnlSrlg *)
        {
	
            {
                CONVERT_TO_INTEGER (pTeTnlInfo->TnlIngressLsrId, u4IngressId);
                CONVERT_TO_INTEGER (pTeTnlInfo->TnlEgressLsrId, u4EgressId);
                u4IngressId = OSIX_NTOHL (u4IngressId);
                u4EgressId = OSIX_NTOHL (u4EgressId);

                *pu4NextMplsTunnelIndex = pTeTnlInfo->u4TnlIndex;
                *pu4NextMplsTunnelInstance = pTeTnlInfo->u4TnlInstance;
                *pu4NextMplsTunnelIngressLSRId = u4IngressId;
                *pu4NextMplsTunnelEgressLSRId = u4EgressId;
                *pu4NextFsMplsTunnelSrlgNo = pTeTnlSrlg->u4SrlgNo;
   		return SNMP_SUCCESS;
            }
        }

        pTeTnlInfo
            = (tTeTnlInfo *) RBTreeGetNext (gTeGblInfo.TnlInfoTbl,
                                            (tRBElem *) pTeTnlInfo, NULL);
    }

  return SNMP_FAILURE;
}

