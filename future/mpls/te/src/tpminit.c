/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: tpminit.c,v 1.1 2010/11/16 07:29:25 prabuc Exp $
*
* Description: This file contains the routines to initialize the
*              protocol structure for the module Te 
*********************************************************************/

#include "teincs.h"

/****************************************************************************
* Function    : TeInitializeMibMplsTeP2mpTunnelTable
* Input       : pTeMplsTeP2mpTunnelTable
* Output      : It Intialize the given structure
* Returns     : SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT4
TeInitializeMibMplsTeP2mpTunnelTable (tTeP2mpTnlInfo * pTeMplsTeP2mpTunnelTable)
{
    INIT_TE_P2MP_TNL_INFO (pTeMplsTeP2mpTunnelTable);
    INIT_TE_P2MP_TNL_WITH_DEF_VALS (pTeMplsTeP2mpTunnelTable);
    return SNMP_SUCCESS;
}

/****************************************************************************
* Function    : TeInitializeMibMplsTeP2mpTunnelDestTable
* Input       : pTeMplsTeP2mpTunnelDestTable
* Output      : It Intialize the given structure
* Returns     : SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT4
TeInitializeMibMplsTeP2mpTunnelDestTable (tP2mpDestEntry *
                                          pTeMplsTeP2mpTunnelDestTable)
{
    INIT_TE_P2MP_DEST_ENTRY (pTeMplsTeP2mpTunnelDestTable);
    INIT_TE_P2MP_DEST_WITH_DEF_VALS (pTeMplsTeP2mpTunnelDestTable);
    return SNMP_SUCCESS;
}

/****************************************************************************
* Function    : TeInitializeMplsTeP2mpTunnelTable
* Input       : pTeMplsTeP2mpTunnelTable
* Output      : It Intialize the given structure
* Returns     : SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT4
TeInitializeMplsTeP2mpTunnelTable (tTeP2mpTnlInfo * pTeMplsTeP2mpTunnelTable)
{
    if (pTeMplsTeP2mpTunnelTable == NULL)
    {
        return SNMP_FAILURE;
    }
    if ((TeInitializeMibMplsTeP2mpTunnelTable (pTeMplsTeP2mpTunnelTable)) ==
        SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
* Function    : TeInitializeMplsTeP2mpTunnelDestTable
* Input       : pTeMplsTeP2mpTunnelDestTable
* Output      : It Intialize the given structure
* Returns     : SNMP_SUCCESS or SNMP_FAILURE
*****************************************************************************/
INT4
TeInitializeMplsTeP2mpTunnelDestTable (tP2mpDestEntry *
                                       pTeMplsTeP2mpTunnelDestTable)
{
    if (pTeMplsTeP2mpTunnelDestTable == NULL)
    {
        return SNMP_FAILURE;
    }
    if ((TeInitializeMibMplsTeP2mpTunnelDestTable
         (pTeMplsTeP2mpTunnelDestTable)) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
