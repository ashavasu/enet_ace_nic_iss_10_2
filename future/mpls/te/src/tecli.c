/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tecli.c,v 1.202 2017/12/22 09:42:23 siva Exp $
 *
 * Description: Action routines for CLI TE commands
 ********************************************************************/

#ifndef __TEECCLI_C__
#define __TEECCLI_C__

#include "teincs.h"
#include "mplscli.h"
#include "teclip.h"
#include "stdtelw.h"
#include "fsmplslw.h"
#include "mplslsr.h"
#include "stdlsrwr.h"
#include "stdlsrlw.h"
#include "teextrn.h"
#include "temacs.h"
#include "rtm.h"
#include "fsmpfrlw.h"
#include "stdtewr.h"
#include "fsmptelw.h"
#include "fsmpfrwr.h"
#include "fsmplswr.h"
#include "stdtecli.h"
#include "fsmplscli.h"
#include "mplsincs.h"
#include "stdmplcli.h"
#include "stdgmtcli.h"
#include "stdgmtlw.h"
#include "stdgmtwr.h"
#include "fsmptewr.h"
#include "fsmptecli.h"
#include "fslsrlw.h"
#include "oamlwg.h"
#include "oamext.h"
#include "fslsrwr.h"
#include "fsmlsrcli.h"
#include "tedbg.h"

/* STATIC_HLSP */
#include "inmgrex.h"
#include "indexmgr.h"
/* MPLS_P2MP_LSP_CHANGES */
#include "tpmlwg.h"
#include "fscfacli.h"

extern INT4         IfMainAdminStatusSet (tSnmpIndex *, tRetVal *);
extern INT4         IfMainRowStatusSet (tSnmpIndex *, tRetVal *);
extern INT4         IfMainStorageTypeSet (tSnmpIndex *, tRetVal *);
extern INT1         nmhGetIfMainRowStatus (INT4 i4IfMainIndex,
                                           INT4 *pi4RetValIfMainRowStatus);

INT4
cli_process_te_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             ap;
    UINT4              *pu4args[TE_CLI_MAX_ARGS];

    INT1                i1argno = TE_ZERO;
    INT4                i4RetStatus = TE_ZERO;
    INT4                i4HoldingPrio = TE_HOLD_PRIO_DEF_VAL;
    INT4                i4SetupPrio = TE_SETUP_PRIO_DEF_VAL;
    UINT4               u4Flag = TE_ZERO;
    UINT4               u4ErrCode = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4IngressLocalMapNum = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    UINT4               u4IncAllAffinity = TE_ZERO;
    UINT4               u4IncAnyAffinity = TE_ZERO;
    UINT4               u4ExclAnyAffinity = TE_ZERO;
    UINT4               u4TnlInstance = 0;
    UINT4               u4DestTnlNum = 0;
    UINT4               u4DestLspNum = 0;
    UINT4               u4OutIfIndex = 0;
    UINT4               u4MplsTnlDir = 0;
    UINT4               u4NextHop = 0;
    UINT4               u4AdminStatus = 0;
    tFrrCliArgs         FrrCliArgs;
    tTeCliArgs          TeCliArgs;
    tteCliStBindArgs    teCliStBindArgs;
    INT1                ai1CurPrompt[MAX_PROMPT_LEN] = { 0 };
    INT1               *pi1Pos = NULL;
    /* STATIC_HLSP */
    UINT4               u4TnlIndex = 0;
    /* MPLS_P2MP_LSP_CHANGES - S */
    UINT4               u4P2mpDestId = TE_ZERO;
    UINT4               u4P2mpDestLocalMapNum = TE_ZERO;
    /* MPLS_P2MP_LSP_CHANGES - E */

    INT4                i4PathComp = 0;
    UINT4               u4PathOptNum = 0;
    UINT4               u4PathNum = 0;
    BOOL1               bLabelRecordingReqd = FALSE;
    UINT4               u4TunnelIndex = TE_ZERO;
    UINT4               u4TnlPathType = TE_TNL_WORKING_PATH;
    UINT1               u1TnlPathType = TE_TNL_WORKING_PATH;
    INT4                i4Args = 0;

    MEMSET (&teCliStBindArgs, 0, sizeof (tteCliStBindArgs));

    MEMSET (&TeCliArgs, 0, sizeof (tTeCliArgs));

    /* Check the Cli handle value with the allowed max session */
    if (CliHandle >= CLI_MAX_SESSIONS)
    {
        CliPrintf (CliHandle, "\r %%CLI Max sessions reached \n");
        return CLI_FAILURE;
    }
    if (MplsIsMplsInitialised () != TRUE)
    {
        CliPrintf (CliHandle, "\r %%MPLS module is shutdown\n");
        return CLI_FAILURE;
    }
    va_start (ap, u4Command);

    /* third arguement is always instance number of the tunnel */
    pu4args[0] = va_arg (ap, UINT4 *);
    if (pu4args[0] != NULL)
    {
        u4TnlInstance = *(pu4args[0]);
        pu4args[0] = NULL;
    }

    while (1)
    {
        pu4args[i1argno++] = va_arg (ap, UINT4 *);

        if (i1argno == TE_CLI_MAX_ARGS)
            break;
    }
    va_end (ap);

    if (CliGetCurPrompt (ai1CurPrompt) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
    pi1Pos = (INT1 *) STRRCHR ((CHR1 *) ai1CurPrompt, '/');

    /* For iss(config-if)# mode, variable u4TnlIndex in the structure
     * gaTnlCliArgs is mandatory. u4TnlInstance in the structure gaTnlCliArgs
     * is optionally set.
     */
    if (STRNCMP (pi1Pos + 1, MPLS_TUNNEL_PROMPT,
                 STRLEN (MPLS_TUNNEL_PROMPT)) == 0)
    {
        if ((CLI_GET_MPLS_TUNNEL_ID ()) > 0)
        {
            gaTnlCliArgs[CliHandle].u4TnlIndex =
                (UINT4) CLI_GET_MPLS_TUNNEL_ID ();
        }
        gaTnlCliArgs[CliHandle].u4TnlInstance = u4TnlInstance;
    }

    CLI_SET_ERR (0);

    switch (u4Command)
    {
        case CLI_MPLS_TE_TNL_CREATE:

            TeCliArgs.u4TunnelIndex = gaTnlCliArgs[CliHandle].u4TnlIndex;
            TeCliArgs.u4TunnelInstance = u4TnlInstance;

            /* EgressId configuration is mandatatory */
            if (pu4args[0] != NULL)
            {
                TeCliArgs.u4EgressId = *(pu4args[0]);
            }
            else if (pu4args[1] != NULL)
            {
                TeCliArgs.u4EgressLocalMapNum = *(pu4args[1]);
            }

            /* IngressId configuration is optional */
            if (pu4args[2] != NULL)
            {
                TeCliArgs.u4IngressId = *(pu4args[2]);
            }
            else if (pu4args[3] != NULL)
            {
                TeCliArgs.u4IngressLocalMapNum = *(pu4args[3]);
            }
            i4RetStatus = TeCliTunnelCreate (CliHandle, &TeCliArgs, OSIX_TRUE);
            break;

        case CLI_MPLS_TE_TNL_DELETE:

            TeCliArgs.u4TunnelIndex = gaTnlCliArgs[CliHandle].u4TnlIndex;
            TeCliArgs.u4TunnelInstance = u4TnlInstance;

            if (pu4args[0] != NULL)
            {
                TeCliArgs.u4EgressId = *(pu4args[0]);
            }
            else if (pu4args[1] != NULL)
            {
                TeCliArgs.u4EgressId = *(pu4args[1]);
            }

            if (pu4args[2] != NULL)
            {
                TeCliArgs.u4IngressId = *(pu4args[2]);
            }
            else if (pu4args[3] != NULL)
            {
                TeCliArgs.u4IngressId = *(pu4args[3]);
            }
            if (pu4args[4] != NULL)
            {
                TeCliArgs.u4AdminStatus = GMPLS_ADMIN_DELETE_IN_PROGRESS;
                TeCliArgs.u4AdminStatus |= GMPLS_ADMIN_REFLECT;
            }

            TeCliArgs.i4TnlIfIndex
                = (INT4) gaTnlCliArgs[CliHandle].u4MplsTnlIfIndex;

            i4RetStatus = TeCliTunnelDelete (CliHandle, &TeCliArgs, OSIX_TRUE);

            gaTnlCliArgs[CliHandle].u4TnlIndex = 0;
            gaTnlCliArgs[CliHandle].u4TnlInstance = 0;
            gaTnlCliArgs[CliHandle].u4IngressId = 0;
            gaTnlCliArgs[CliHandle].u4EgressId = 0;
            break;

        case CLI_MPLS_TE_TNL_IF_CREATE:
            i4RetStatus =
                TeCliTunnelInterfaceCreate (CliHandle, *(pu4args[0]),
                                            CLI_PTR_TO_U4 (pu4args[1]));
            break;

        case CLI_MPLS_TE_TNL_NO_IF_CREATE:

            if (pu4args[1] != NULL)
            {
                u4AdminStatus = GMPLS_ADMIN_DELETE_IN_PROGRESS;
                u4AdminStatus |= GMPLS_ADMIN_REFLECT;
            }
            i4RetStatus =
                TeCliTunnelInterfaceDelete (CliHandle, *((pu4args[0])),
                                            CLI_PTR_TO_U4 (pu4args[2]),
                                            u4AdminStatus);
            gaTnlCliArgs[CliHandle].u4TnlIndex = 0;
            gaTnlCliArgs[CliHandle].u4TnlInstance = 0;
            gaTnlCliArgs[CliHandle].u4IngressId = 0;
            gaTnlCliArgs[CliHandle].u4EgressId = 0;
            break;

        case CLI_MPLS_TE_SIG_PROTO:
            i4RetStatus = TeCliTunnelSigProtocol (CliHandle,
                                                  CLI_PTR_TO_I4 (pu4args[0]));
            break;

        case CLI_MPLS_TE_TYPE:
            i4RetStatus =
                TeCliTunnelSetTnlType (CliHandle, CLI_PTR_TO_U4 (pu4args[0]));
            break;

        case CLI_MPLS_TE_MODE:
            i4RetStatus =
                TeCliTunnelSetTnlMode (CliHandle, CLI_PTR_TO_U4 (pu4args[0]));
            break;

        case CLI_MPLS_TE_DEST_TUNNEL:
            u4DestTnlNum = *((pu4args[0]));
            if (pu4args[1] != NULL)
            {
                u4DestLspNum = *((pu4args[1]));
            }
            i4RetStatus =
                TeCliTunnelSetDestTnlInfo (CliHandle, u4DestTnlNum,
                                           u4DestLspNum);
            break;

        case CLI_MPLS_TE_NO_DEST_TUNNEL:
            i4RetStatus =
                TeCliTunnelResetDestTnlInfo (CliHandle,
                                             CLI_PTR_TO_U4 (pu4args[0]));
            break;

        case CLI_MPLS_TE_TRAFFIC:
            i4RetStatus = TeCliTunnelTrafficEng (CliHandle);
            break;

        case CLI_MPLS_TE_RSRC:
            i4RetStatus =
                TeCliTunnelResourceCreate (CliHandle, *((pu4args[0])));
            break;

        case CLI_MPLS_TE_ST_BIND:
            u4Flag = CLI_PTR_TO_U4 (pu4args[0]);
            if (u4Flag == MPLS_TE_TNL_INGRESS)
            {
                u4OutIfIndex = CLI_PTR_TO_U4 (pu4args[5]);
                u4MplsTnlDir = CLI_PTR_TO_U4 (pu4args[6]);
                if (pu4args[4] != NULL)
                {
                    u4NextHop = *(pu4args[4]);
                }
                if (pu4args[7] != NULL)
                {
                    u4TnlPathType = TE_TNL_PROTECTION_PATH;
                }

                u1TnlPathType = (UINT1) u4TnlPathType;
                i4RetStatus = TeCliStaticBindIngress (CliHandle,
                                                      *((pu4args[3])),
                                                      u4NextHop,
                                                      u4OutIfIndex,
                                                      u4MplsTnlDir,
                                                      u1TnlPathType);
            }
            else if (u4Flag == MPLS_TE_TNL_EGRESS)
            {
                u4MplsTnlDir = CLI_PTR_TO_U4 (pu4args[6]);
                if (pu4args[7] != NULL)
                {
                    u4TnlPathType = TE_TNL_PROTECTION_PATH;
                }
                u1TnlPathType = (UINT1) u4TnlPathType;
                i4RetStatus = TeCliStaticBindEgress (CliHandle,
                                                     *((pu4args[1])),
                                                     CLI_PTR_TO_U4
                                                     (pu4args[2]),
                                                     u4MplsTnlDir,
                                                     u1TnlPathType);
            }
            else if (u4Flag == MPLS_TE_TNL_INTERMEDIATE)    /* STATIC_HLSP */
            {
                if (pu4args[4] != NULL)
                {
                    u4NextHop = *(pu4args[4]);
                }
                if (pu4args[7] != NULL)
                {
                    u4TnlPathType = TE_TNL_PROTECTION_PATH;
                }
                u1TnlPathType = (UINT1) u4TnlPathType;
                u4OutIfIndex = CLI_PTR_TO_U4 (pu4args[5]);
                u4MplsTnlDir = CLI_PTR_TO_U4 (pu4args[6]);
                i4RetStatus = TeCliStaticBind (CliHandle,
                                               *((pu4args[1])),
                                               CLI_PTR_TO_U4
                                               (pu4args[2]),
                                               *((pu4args[3])),
                                               u4NextHop,
                                               u4OutIfIndex, u4MplsTnlDir,
                                               u1TnlPathType);
            }                    /* STATIC_HLSP */
            else if (u4Flag == MPLS_TE_TNL_INGRESS_STACK)
            {
                u4MplsTnlDir = CLI_PTR_TO_U4 (pu4args[6]);
                if (pu4args[7] != NULL)
                {
                    u4TnlPathType = TE_TNL_PROTECTION_PATH;
                }
                u1TnlPathType = (UINT1) u4TnlPathType;
                i4RetStatus = TeCliStaticBindIngressStack (CliHandle,
                                                           *((pu4args[3])),
                                                           *((pu4args[4])),
                                                           u4MplsTnlDir,
                                                           u1TnlPathType);
            }
            else if (u4Flag == MPLS_TE_TNL_INTERMEDIATE_STACK)
            {
                teCliStBindArgs.u4InLabel = *((pu4args[1]));
                teCliStBindArgs.u4InIfIndex = CLI_PTR_TO_U4 (pu4args[2]);
                teCliStBindArgs.u4OutLabel = *((pu4args[3]));
                teCliStBindArgs.u4TnlId = *((pu4args[4]));
                teCliStBindArgs.u4Direction = CLI_PTR_TO_U4 (pu4args[6]);
                teCliStBindArgs.u4Operation = TE_OPER_STACK;

                if (pu4args[7] != NULL)
                {
                    u4TnlPathType = TE_TNL_PROTECTION_PATH;
                }
                teCliStBindArgs.u1TnlPathType = (UINT1) u4TnlPathType;

                i4RetStatus = TeCliStaticBindStack (CliHandle,
                                                    &teCliStBindArgs);
            }
            else if (u4Flag == MPLS_TE_TNL_INTERMEDIATE_STITCH)
            {
                teCliStBindArgs.u4Direction = CLI_PTR_TO_U4 (pu4args[8]);

                if (((CLI_PTR_TO_U4 (pu4args[1]) ==
                      MPLS_TE_TNL_SLSP_INGRESS) &&
                     (teCliStBindArgs.u4Direction ==
                      MPLS_CLI_DIRECTION_FORWARD)) ||
                    ((CLI_PTR_TO_U4 (pu4args[1]) ==
                      MPLS_TE_TNL_SLSP_EGRESS) &&
                     (teCliStBindArgs.u4Direction ==
                      MPLS_CLI_DIRECTION_REVERSE)))
                {
                    teCliStBindArgs.u4InLabel = *((pu4args[2]));
                    teCliStBindArgs.u4InIfIndex = CLI_PTR_TO_U4 (pu4args[3]);
                }
                else
                {
                    teCliStBindArgs.u4OutLabel = *((pu4args[4]));
                    if (pu4args[5] != NULL)
                    {
                        teCliStBindArgs.u4NextHopAddr = *((pu4args[5]));
                    }
                    teCliStBindArgs.u4OutIfIndex = CLI_PTR_TO_U4 (pu4args[6]);
                }

                teCliStBindArgs.u4TnlId = *((pu4args[7]));
                teCliStBindArgs.u4Operation = TE_OPER_STITCH;

                if (pu4args[9] != NULL)
                {
                    u4TnlPathType = TE_TNL_PROTECTION_PATH;
                }
                teCliStBindArgs.u1TnlPathType = (UINT1) u4TnlPathType;

                i4RetStatus = TeCliStaticBindStack (CliHandle,
                                                    &teCliStBindArgs);
            }
            else if (u4Flag == MPLS_TE_TNL_END_POINT_STITCH)
            {
                teCliStBindArgs.u4TnlId = *((pu4args[1]));
                teCliStBindArgs.u4Direction = CLI_PTR_TO_U4 (pu4args[2]);
                teCliStBindArgs.u4Operation = TE_OPER_STITCH;

                if (pu4args[3] != NULL)
                {
                    u4TnlPathType = TE_TNL_PROTECTION_PATH;
                }
                teCliStBindArgs.u1TnlPathType = (UINT1) u4TnlPathType;

                i4RetStatus = TeCliStaticBindStack (CliHandle,
                                                    &teCliStBindArgs);
            }
            break;

        case CLI_MPLS_TE_NO_SHUT:
            i4RetStatus = TeCliTunnelActive (CliHandle);
            break;

        case CLI_MPLS_TE_SHUT:
            i4RetStatus = TeCliTunnelInActive (CliHandle);
            break;

        case CLI_MPLS_TE_DEST_ADDR:
        case CLI_MPLS_TE_DEST_LOCAL_MAP_NUM:
        case CLI_MPLS_TE_SRC_ADDR:
        case CLI_MPLS_TE_SRC_LOCAL_MAP_NUM:
        case CLI_MPLS_TE_NAME:
        case CLI_MPLS_TE_ROLE_ALL:
        case CLI_MPLS_TE_ROLE_HEAD:
        case CLI_MPLS_TE_ROLE_MIDDLE:
        case CLI_MPLS_TE_ROLE_TAIL:
        case CLI_MPLS_TE_ROLE_REMOTE:
        case CLI_MPLS_TE_ALL:
        case CLI_MPLS_TE_UNIDIRECTIONAL:
        case CLI_MPLS_TE_COROUTED_BIDIR:
        case CLI_MPLS_TE_ASSOCIATED_BIDIR:
            i4RetStatus = TeCliShowTunnel (CliHandle, u4Command,
                                           CLI_PTR_TO_U4 (pu4args[0]),
                                           (UINT1 *) pu4args[1]);
            break;

            /*FRR */
        case CLI_MPLS_FRR_CONST_INFO:
            MEMSET (&FrrCliArgs, TE_ZERO, sizeof (tFrrCliArgs));
            FrrCliArgs.u4Flag = CLI_PTR_TO_U4 (pu4args[0]);
            if (pu4args[1] != NULL)
            {
                FrrCliArgs.u4Bandwidth = *((pu4args[1]));
            }
            if (pu4args[2] != NULL)
            {
                FrrCliArgs.u4BckSetupPrio = *((pu4args[2]));
            }
            if (pu4args[3] != NULL)
            {
                FrrCliArgs.u4BckHoldingPrior = *((pu4args[3]));
            }
            if (pu4args[4] != NULL)
            {
                FrrCliArgs.u4HopLimit = *((pu4args[4]));
            }
            if (pu4args[5] != NULL)
            {
                FrrCliArgs.u4IncAny = *((pu4args[5]));
            }
            if (pu4args[6] != NULL)
            {
                FrrCliArgs.u4ExcAny = *((pu4args[6]));
            }
            if (pu4args[7] != NULL)
            {
                FrrCliArgs.u4IncAll = *((pu4args[7]));
            }
#ifdef MPLS_SIG_WANTED
            i4RetStatus = TeCliFrrConstInfo (CliHandle, &FrrCliArgs);
#endif
            break;

        case CLI_MPLS_FRR_NO_CONST_INFO:
#ifdef MPLS_SIG_WANTED
            i4RetStatus = TeCliFrrNoConstInfo (CliHandle);
#endif
            break;

        case CLI_MPLS_TE_PRIORITY:
            i4SetupPrio = *((INT4 *) (pu4args[0]));
            if (pu4args[1] != NULL)
            {
                i4HoldingPrio = *((INT4 *) (pu4args[1]));
            }
            i4RetStatus = TeCliTunnelPriorities (CliHandle, i4SetupPrio,
                                                 i4HoldingPrio);
            break;

        case CLI_MPLS_TE_NO_PRIORITY:
            i4RetStatus = TeCliTunnelNoPriorities (CliHandle);
            break;

        case CLI_MPLS_TE_AFFINITY:
            if (pu4args[0] != NULL)
            {
                u4IncAnyAffinity = *((pu4args[0]));
            }
            if (pu4args[1] != NULL)
            {
                u4ExclAnyAffinity = *((pu4args[1]));
            }
            if (pu4args[2] != NULL)
            {
                u4IncAllAffinity = *((pu4args[2]));
            }

            i4RetStatus = TeCliTunnelAffinities (CliHandle, u4IncAnyAffinity,
                                                 u4ExclAnyAffinity,
                                                 u4IncAllAffinity);
            break;

        case CLI_MPLS_TE_NO_AFFINITY:
            i4RetStatus = TeCliTunnelNoAffinities (CliHandle);
            break;

        case CLI_MPLS_TE_RECORD_ROUTE:
            if (pu4args[TE_ZERO] != NULL)
            {
                bLabelRecordingReqd = TRUE;
            }
            i4RetStatus = TeCliRecordRoute (CliHandle, bLabelRecordingReqd);
            break;
        case CLI_MPLS_TE_NO_RECORD_ROUTE:
            i4RetStatus = TeCliNoRecordRoute (CliHandle);
            break;
        case CLI_MPLS_TE_SWITCHING_ENCODING_TYPE:
            TeCliArgs.u1SwitchingType
                = (UINT1) CLI_PTR_TO_U4 (pu4args[TE_ZERO]);
            TeCliArgs.u1EncodingType = (UINT1) CLI_PTR_TO_U4 (pu4args[TE_ONE]);

            i4RetStatus
                = TeCliMplsSetTnlProperties (CliHandle, u4Command, &TeCliArgs);
            break;
        case CLI_MPLS_TE_TNL_SRLG_TYPE:
            TeCliArgs.i4SrlgType = CLI_PTR_TO_I4 (pu4args[TE_ZERO]);

            i4RetStatus
                = TeCliMplsSetTnlProperties (CliHandle, u4Command, &TeCliArgs);
            break;
        case CLI_MPLS_TE_TNL_NO_SRLG_TYPE:
            TeCliArgs.i4SrlgType = TE_ZERO;
            i4RetStatus
                = TeCliMplsSetTnlProperties (CliHandle, u4Command, &TeCliArgs);
            break;
        case CLI_MPLS_TE_TNL_SRLG_VALUE:
            TeCliArgs.u4SrlgNo = *(pu4args[TE_ZERO]);

            i4RetStatus
                = TeCliMplsSetTnlProperties (CliHandle, u4Command, &TeCliArgs);
            break;
        case CLI_MPLS_TE_TNL_NO_SRLG_VALUE:
            TeCliArgs.u4SrlgNo = *(pu4args[TE_ZERO]);

            i4RetStatus
                = TeCliMplsSetTnlProperties (CliHandle, u4Command, &TeCliArgs);
            break;

        case CLI_MPLS_TE_ATTR_CLASSTYPE_VALUE:
            TeCliArgs.i4ClassType = *((INT4 *) pu4args[TE_ZERO]);
            i4RetStatus = TeCliSetTnlAttrProperties (CliHandle, u4Command,
                                                     &TeCliArgs);
            break;

        case CLI_MPLS_TE_ATTR_NO_CLASSTYPE_VALUE:
            i4RetStatus = TeCliSetTnlAttrProperties (CliHandle, u4Command,
                                                     &TeCliArgs);
            break;

        case CLI_MPLS_TE_HOP_ENTRY:
            i4PathComp = CLI_PTR_TO_I4 (pu4args[TE_TWO]);
            if (i4PathComp == GMPLS_PATH_COMP_EXPLICIT)
            {
                u4PathOptNum = *((pu4args[TE_ZERO]));
                u4PathNum = CLI_PTR_TO_U4 (pu4args[TE_ONE]);
            }
            else
            {
                u4PathOptNum = *((pu4args[TE_ZERO]));
                u4PathNum = TE_ZERO;
            }

            i4RetStatus
                = TeCliHopTablePathOptAndPathNum (CliHandle, u4PathOptNum,
                                                  u4PathNum, i4PathComp,
                                                  (UINT1
                                                   *) (pu4args[TE_THREE]));
            break;

        case CLI_MPLS_TE_NO_HOP_ENTRY:

            i4RetStatus = TeCliNoHopTablePathOptAndPathNum (CliHandle,
                                                            *((pu4args[0])),
                                                            (CLI_PTR_TO_U4
                                                             (pu4args[1])));
            break;

        case CLI_MPLS_TE_BACKUPHOP_ENTRY:
            i4PathComp = CLI_PTR_TO_I4 (pu4args[TE_TWO]);
            if (i4PathComp == GMPLS_PATH_COMP_EXPLICIT)
            {
                u4PathOptNum = *((pu4args[TE_ZERO]));
                u4PathNum = CLI_PTR_TO_U4 (pu4args[TE_ONE]);
            }
            else
            {
                u4PathOptNum = *((pu4args[TE_ZERO]));
                u4PathNum = TE_ZERO;
            }

            i4RetStatus
                = TeCliBackupHopTablePathOptAndPathNum (CliHandle, u4PathOptNum,
                                                        u4PathNum, i4PathComp,
                                                        (UINT1
                                                         *) (pu4args
                                                             [TE_THREE]));
            break;

        case CLI_MPLS_TE_NO_BACKUPHOP_ENTRY:

            i4RetStatus = TeCliNoBackupHopTablePathOptAndPathNum (CliHandle,
                                                                  *((pu4args
                                                                     [0])),
                                                                  (CLI_PTR_TO_U4
                                                                   (pu4args
                                                                    [1])));
            break;

        case CLI_MPLS_TE_HOP_TABLE:
            i4RetStatus = TeCliTunnelHopTableEntries (CliHandle,
                                                      *((pu4args[0])),
                                                      *((pu4args[1])),
                                                      (CLI_PTR_TO_I4
                                                       (pu4args[2])));
            break;

        case CLI_MPLS_TE_NO_HOP_TABLE:
            i4RetStatus = TeCliTunnelNoHopTableEntries (CliHandle,
                                                        *((pu4args[0])));
            break;
        case CLI_MPLS_TE_SNMP_TRAP_ENABLE:
            i4RetStatus =
                TeCliSetTunnelNotification (CliHandle, MPLS_SNMP_TRUE);
            break;
        case CLI_MPLS_TE_SNMP_TRAP_DISABLE:
            i4RetStatus =
                TeCliSetTunnelNotification (CliHandle, MPLS_SNMP_FALSE);
            break;

        case CLI_MPLS_TE_LSP_ATTR:
            i4RetStatus =
                TeCliTunnelAttrListCreate (CliHandle, (UINT1 *) pu4args[0]);
            break;

        case CLI_MPLS_TE_NO_LSP_ATTR:
            i4RetStatus =
                TeCliTunnelAttrListDelete (CliHandle, (UINT1 *) pu4args[0]);
            break;

        case CLI_MPLS_TE_IP_EXPL_PATH:
            u4PathNum = *(pu4args[TE_ZERO]);

            if (pu4args[TE_ONE] != NULL)
            {
                u4PathOptNum = *(pu4args[TE_ONE]);
            }
            else
            {
                u4PathOptNum = TE_ONE;
            }
            i4RetStatus
                = TeCliIpExplicitPathCreate (CliHandle, u4PathNum,
                                             u4PathOptNum);
            break;
        case CLI_MPLS_TE_NO_IP_EXPL_PATH:
            u4PathNum = *(pu4args[TE_ZERO]);

            if (pu4args[TE_ONE] != NULL)
            {
                u4PathOptNum = *(pu4args[TE_ONE]);
            }
            else
            {
                u4PathOptNum = TE_ONE;
            }
            i4RetStatus
                = TeCliIpExplicitPathDelete (CliHandle, u4PathNum,
                                             u4PathOptNum);
            break;
        case CLI_MPLS_TE_ATTR_PRIORITY:

            TeCliArgs.i4SetupPrio = *((INT4 *) (pu4args[TE_ZERO]));

            if (pu4args[TE_ONE] != NULL)
            {
                TeCliArgs.i4HoldingPrio = *((INT4 *) (pu4args[TE_ONE]));
            }
            else
            {
                TeCliArgs.i4HoldingPrio = TeCliArgs.i4SetupPrio;
            }

            i4RetStatus = TeCliSetTnlAttrProperties (CliHandle, u4Command,
                                                     &TeCliArgs);
            break;
        case CLI_MPLS_TE_ATTR_NO_PRIORITY:
            i4RetStatus = TeCliSetTnlAttrProperties (CliHandle, u4Command,
                                                     &TeCliArgs);
            break;

        case CLI_MPLS_TE_ATTR_AFFINITY:

            TeCliArgs.u4IncludeAnyAffinity = *(pu4args[TE_ZERO]);
            TeCliArgs.u4ExcludeAnyAffinity = *(pu4args[TE_ONE]);
            TeCliArgs.u4IncludeAllAffinity = *(pu4args[TE_TWO]);
            i4RetStatus = TeCliSetTnlAttrProperties (CliHandle, u4Command,
                                                     &TeCliArgs);
            break;

        case CLI_MPLS_TE_ATTR_NO_AFFINITY:
            i4RetStatus = TeCliSetTnlAttrProperties (CliHandle, u4Command,
                                                     &TeCliArgs);
            break;

        case CLI_MPLS_TE_ATTR_BW:
            TeCliArgs.u4Bandwidth = *(pu4args[0]);
            i4RetStatus = TeCliSetTnlAttrProperties (CliHandle, u4Command,
                                                     &TeCliArgs);
            break;

        case CLI_MPLS_TE_ATTR_NO_BW:
            i4RetStatus = TeCliSetTnlAttrProperties (CliHandle, u4Command,
                                                     &TeCliArgs);
            break;

        case CLI_MPLS_TE_ATTR_RECORD_ROUTE:
            i4RetStatus = TeCliSetTnlAttrProperties (CliHandle, u4Command,
                                                     &TeCliArgs);
            break;

        case CLI_MPLS_TE_ATTR_NO_RECORD_ROUTE:
            i4RetStatus = TeCliSetTnlAttrProperties (CliHandle, u4Command,
                                                     &TeCliArgs);
            break;

        case CLI_MPLS_TE_ATTR_SRLG_TYPE:
            TeCliArgs.i4SrlgType = CLI_PTR_TO_I4 (pu4args[TE_ZERO]);
            i4RetStatus = TeCliSetTnlAttrProperties (CliHandle, u4Command,
                                                     &TeCliArgs);
            break;

        case CLI_MPLS_TE_ATTR_NO_SRLG_TYPE:
            i4RetStatus = TeCliSetTnlAttrProperties (CliHandle, u4Command,
                                                     &TeCliArgs);
            break;

        case CLI_MPLS_TE_ATTR_SRLG_VALUE:
            TeCliArgs.u4SrlgNo = *(pu4args[TE_ZERO]);
            i4RetStatus = TeCliSetTnlAttrProperties (CliHandle, u4Command,
                                                     &TeCliArgs);
            break;

        case CLI_MPLS_TE_ATTR_NO_SRLG_VALUE:
            TeCliArgs.u4SrlgNo = *(pu4args[TE_ZERO]);
            i4RetStatus = TeCliSetTnlAttrProperties (CliHandle, u4Command,
                                                     &TeCliArgs);
            break;

        case CLI_MPLS_TE_PATH_HOP:
            TeCliArgs.u4PathListId = gaTeCliArgs[CliHandle].u4PathListId;
            TeCliArgs.u4PathOptionId = gaTeCliArgs[CliHandle].u4PathOptionId;
            TeCliArgs.u4HopIndex = CLI_PTR_TO_U4 (pu4args[0]);
            TeCliArgs.u4HopIpAddr = CLI_PTR_TO_U4 (pu4args[1]);
            TeCliArgs.u4HopUnnumIf = CLI_PTR_TO_U4 (pu4args[2]);
            TeCliArgs.i4HopIncludeExclude = CLI_PTR_TO_I4 (pu4args[3]);
            TeCliArgs.i4HopType = CLI_PTR_TO_I4 (pu4args[4]);
            TeCliArgs.i4PathComp = TE_DYNAMIC;
            TeCliArgs.u4ForwardLbl = CLI_PTR_TO_U4 (pu4args[5]);
            TeCliArgs.u4ReverseLbl = CLI_PTR_TO_U4 (pu4args[6]);

            i4RetStatus = TeCliAddHopTableEntries (CliHandle, &TeCliArgs);
            break;

        case CLI_MPLS_TE_NO_PATH_HOP:
            TeCliArgs.u4PathListId = gaTeCliArgs[CliHandle].u4PathListId;
            TeCliArgs.u4PathOptionId = gaTeCliArgs[CliHandle].u4PathOptionId;
            TeCliArgs.u4HopIndex = *(pu4args[0]);

            i4RetStatus = TeCliDelHopTableEntries (CliHandle, &TeCliArgs);
            break;
#ifdef MPLS_RSVPTE_WANTED
        case CLI_MPLS_FRR_SHOW:
            i4RetStatus = TeCliFrrShow (CliHandle, CLI_PTR_TO_I4 (pu4args[0]));
            break;

        case CLI_MPLS_TE_GLOBAL_REVERTIVE_TIME:
            i4RetStatus = TeCliFrrGlobalRevertiveTime (CliHandle,
                                                       *((INT4
                                                          *) (pu4args[0])));
            break;
#endif
            /*endof FRR */
        case CLI_MPLS_TE_DBG:
            if (pu4args[1] != NULL)
            {
                i4Args = CLI_PTR_TO_I4 (pu4args[1]);
                TeCliSetDebugLevel (CliHandle, i4Args);
            }

            if (CLI_PTR_TO_U4 (pu4args[0]) == CLI_ENABLE)
            {
                TE_DBG_FLAG = TE_ALL;
            }
            else
            {
                TE_DBG_FLAG = TE_DEF_DBG_FLAG;
            }
            i4RetStatus = CLI_SUCCESS;
            break;
            /* STATIC_HLSP */
        case CLI_MPLS_HLSP_SHOW:
        case CLI_MPLS_SLSP_SHOW:
            u4Flag = CLI_PTR_TO_U4 (pu4args[0]);

            if ((u4Flag == CLI_MPLS_HLSP) || (u4Flag == CLI_MPLS_SLSP_TNLS))
            {
                i4RetStatus = TeCliShowHLSP (CliHandle, u4Flag);
            }
            else
            {
                if (pu4args[1] != NULL)
                {
                    u4TnlIndex = *((pu4args[1]));
                }
                if (pu4args[2] != NULL)
                {
                    u4TnlInstance = *((pu4args[2]));
                }
                if (pu4args[3] != NULL)
                {
                    u4IngressId = *((pu4args[3]));
                }
                else if (pu4args[4] != NULL)
                {
                    u4IngressId = *(pu4args[4]);
                }
                if (pu4args[5] != NULL)
                {
                    u4EgressId = *((pu4args[5]));
                }
                else if (pu4args[6] != NULL)
                {
                    u4EgressId = *(pu4args[6]);
                }
                if (u4Flag == CLI_MPLS_HLSP_STACKED_TNL)
                {
                    i4RetStatus =
                        TeCliShowHLSPStackedTnl (CliHandle, u4TnlIndex,
                                                 u4TnlInstance,
                                                 u4IngressId, u4EgressId);
                }
                else
                {
                    i4RetStatus =
                        TeCliShowSLSPStitchedTnl (CliHandle, u4TnlIndex,
                                                  u4TnlInstance,
                                                  u4IngressId, u4EgressId);
                }
            }
            break;
            /* MPLS_P2MP_LSP_CHANGES - S */
        case CLI_MPLS_P2MP_TE_TNL_CREATE:
            /* EgressId configuration is mandatatory. u4EgressLocalMapNum 
             * will be zero for P2MP tunnel */
            u4TnlInstance = TE_ONE;

            if (pu4args[TE_ZERO] != NULL)
            {
                u4EgressId = *(pu4args[TE_ZERO]);
            }

            /* IngressId configuration is optional */
            if (pu4args[TE_ONE] != NULL)
            {
                u4IngressId = *(pu4args[TE_ONE]);
            }
            else if (pu4args[TE_TWO] != NULL)
            {
                u4IngressLocalMapNum = *(pu4args[TE_TWO]);
            }

            u4TunnelIndex = (UINT4) CLI_GET_MPLS_TUNNEL_ID ();

            i4RetStatus = TeCliP2mpTunnelCreate (CliHandle, u4TunnelIndex,
                                                 u4EgressId, u4IngressId,
                                                 u4IngressLocalMapNum,
                                                 u4TnlInstance,
                                                 MPLS_TE_TNL_TYPE_P2MP);
            break;

        case CLI_MPLS_P2MP_TE_TNL_ING_DST_ADD:
            if (pu4args[TE_ZERO] != NULL)
            {
                u4P2mpDestId = *(pu4args[TE_ZERO]);
            }
            else if (pu4args[TE_ONE] != NULL)
            {
                u4P2mpDestLocalMapNum = *(pu4args[TE_ONE]);
            }
            if (pu4args[TE_THREE] != NULL)
            {
                u4NextHop = *(pu4args[TE_THREE]);
            }
            u4OutIfIndex = CLI_PTR_TO_U4 (pu4args[4]);

            i4RetStatus = TeCliP2mpAddDestinationAtIngress (CliHandle,
                                                            u4P2mpDestId,
                                                            u4P2mpDestLocalMapNum,
                                                            *((pu4args
                                                               [TE_TWO])),
                                                            u4NextHop,
                                                            u4OutIfIndex);
            break;

        case CLI_MPLS_P2MP_TE_TNL_BR_ST_BIND:
            if (pu4args[TE_ZERO] != NULL)
            {
                u4P2mpDestId = *(pu4args[TE_ZERO]);
            }
            else if (pu4args[TE_ONE] != NULL)
            {
                u4P2mpDestLocalMapNum = *(pu4args[TE_ONE]);
            }
            if (pu4args[5] != NULL)
            {
                u4NextHop = *(pu4args[5]);
            }
            u4OutIfIndex = CLI_PTR_TO_U4 (pu4args[6]);

            i4RetStatus = TeCliP2mpStaticBind
                (CliHandle,
                 u4P2mpDestId,
                 u4P2mpDestLocalMapNum,
                 *((pu4args[TE_TWO])),
                 (CLI_PTR_TO_U4 (pu4args[TE_THREE])),
                 *((pu4args[4])), u4NextHop, u4OutIfIndex);
            break;

        case CLI_MPLS_P2MP_TE_ST_BIND_EGRESS:
            i4RetStatus = TeCliP2mpStaticBindEgress
                (CliHandle,
                 *((pu4args[TE_ZERO])), (CLI_PTR_TO_U4 (pu4args[TE_ONE])));
            break;

        case CLI_MPLS_P2MP_TE_BUD_NODE_SET:
            if (pu4args[TE_ZERO] != NULL)
            {
                u4P2mpDestId = *(pu4args[TE_ZERO]);
            }
            else if (pu4args[TE_ONE] != NULL)
            {
                u4P2mpDestLocalMapNum = *(pu4args[TE_ONE]);
            }
            i4RetStatus = TeCliP2mpConfigureBudNode
                (CliHandle, u4P2mpDestId, u4P2mpDestLocalMapNum);
            break;

        case CLI_MPLS_P2MP_TE_NO_ST_BIND:
            if (pu4args[TE_ZERO] != NULL)
            {
                u4P2mpDestId = *(pu4args[TE_ZERO]);
            }
            else if (pu4args[TE_ONE] != NULL)
            {
                u4P2mpDestLocalMapNum = *(pu4args[TE_ONE]);
            }
            i4RetStatus = TeCliP2mpDeleteDestination (CliHandle, u4P2mpDestId,
                                                      u4P2mpDestLocalMapNum);
            break;

        case CLI_MPLS_TE_P2MP_BRANCH_STATS:
            if (pu4args[TE_ONE] != NULL)
            {
                u4EgressId = *(pu4args[TE_ONE]);
            }
            if (pu4args[TE_TWO] != NULL)
            {
                u4IngressId = *(pu4args[TE_TWO]);
            }
            else if (pu4args[TE_THREE] != NULL)
            {
                u4IngressLocalMapNum = *(pu4args[TE_THREE]);
            }
            i4RetStatus = TeCliShowP2mpBranchStatistics (CliHandle,
                                                         *((pu4args[TE_ZERO])),
                                                         u4TnlInstance,
                                                         u4EgressId,
                                                         u4IngressId,
                                                         u4IngressLocalMapNum);
            break;

        case CLI_MPLS_P2MP_TE_TNL_DELETE:
            if (pu4args[TE_ZERO] != NULL)
            {
                u4EgressId = *(pu4args[TE_ZERO]);
            }
            if (pu4args[TE_ONE] != NULL)
            {
                u4IngressId = *(pu4args[TE_ONE]);
            }
            else if (pu4args[TE_TWO] != NULL)
            {
                u4IngressLocalMapNum = *(pu4args[TE_TWO]);
            }

            u4TnlInstance = TE_ONE;

            i4RetStatus = TeCliP2mpTunnelDelete (CliHandle,
                                                 gaTnlCliArgs[CliHandle].
                                                 u4TnlIndex, u4TnlInstance,
                                                 u4EgressId, u4IngressId,
                                                 u4IngressLocalMapNum);
            gaTnlCliArgs[CliHandle].u4TnlIndex = TE_ZERO;
            gaTnlCliArgs[CliHandle].u4TnlInstance = TE_ZERO;
            gaTnlCliArgs[CliHandle].u4IngressId = TE_ZERO;
            gaTnlCliArgs[CliHandle].u4EgressId = TE_ZERO;
            break;

            /* MPLS_P2MP_LSP_CHANGES - E */

        case CLI_MPLS_TE_ADMIN_STATUS_FLAGS:
            TeCliArgs.u4AdminStatus = CLI_PTR_TO_U4 (pu4args[TE_ZERO]);

            i4RetStatus =
                TeCliMplsSetTnlProperties (CliHandle, u4Command, &TeCliArgs);
            break;

        case CLI_MPLS_TE_GPID:
            TeCliArgs.i4Gpid = CLI_PTR_TO_I4 (pu4args[TE_ZERO]);
            i4RetStatus =
                TeCliMplsSetTnlProperties (CliHandle, u4Command, &TeCliArgs);
            break;
        case CLI_MPLS_TUNNEL_CLASS_TYPE:
            TeCliArgs.i4ClassType = *((INT4 *) pu4args[TE_ZERO]);
            TeCliArgs.u4TnlInstance = u4TnlInstance;
            i4RetStatus = TeCliMplsSetTnlProperties (CliHandle, u4Command,
                                                     &TeCliArgs);
            break;
        case CLI_MPLS_TE_END_TO_END_PROTECTION:
            TeCliArgs.i4EndToEndProtection = CLI_PTR_TO_I4 (pu4args[TE_ZERO]);
            TeCliArgs.u4TnlInstance = u4TnlInstance;
            i4RetStatus = TeCliMplsSetTnlProperties (CliHandle, u4Command,
                                                     &TeCliArgs);
            break;
        case CLI_MPLS_TE_PROTECTION_OPER_TYPE:
            TeCliArgs.i4ProtnOperType = CLI_PTR_TO_I4 (pu4args[TE_ZERO]);
            TeCliArgs.u4TnlInstance = u4TnlInstance;
            i4RetStatus = TeCliMplsSetTnlProperties (CliHandle, u4Command,
                                                     &TeCliArgs);
            break;
        case CLI_MPLS_TE_MBB_STATUS:
            TeCliArgs.i4MbbStatus = CLI_PTR_TO_I4 (pu4args[TE_ZERO]);
            TeCliArgs.u4TnlInstance = u4TnlInstance;
            i4RetStatus = TeCliMplsSetTnlProperties (CliHandle, u4Command,
                                                     &TeCliArgs);
            break;
        case CLI_MPLS_QOS_POLICY:
            TeCliArgs.i4QosPolicy = CLI_PTR_TO_I4 (pu4args[TE_ZERO]);
            i4RetStatus = TeCliMplsQosPolicy (CliHandle, &TeCliArgs);
            break;
        case CLI_MPLS_DELETE_QOS_POLICY:
            TeCliArgs.i4QosPolicy = MPLS_QOS_STD_IP;
            i4RetStatus = TeCliMplsQosPolicy (CliHandle, &TeCliArgs);
            break;
        case CLI_MPLS_DIFFSERV_LSP_TYPE:
            TeCliArgs.i4LspType = CLI_PTR_TO_I4 (pu4args[TE_ZERO]);
            TeCliArgs.u4TnlInstance = u4TnlInstance;
            i4RetStatus = TeCliMplsSetTnlProperties (CliHandle, u4Command,
                                                     &TeCliArgs);
            break;

        case CLI_MPLS_CREATE_EXP_PHB_MAP:
            i4RetStatus =
                TeCliMplsCreateExpPhbMap (CliHandle, *(pu4args[TE_ZERO]));
            break;
        case CLI_MPLS_DELETE_EXP_PHB_MAP:
            i4RetStatus =
                TeCliMplsDeleteExpPhbMap (CliHandle,
                                          *((INT4 *) pu4args[TE_ZERO]));
            break;
        case CLI_MPLS_CREATE_ELSP_INFO_LIST_INDEX:
            i4RetStatus =
                TeCliMplsCreateElspInfoPhbMap (CliHandle, *(pu4args[TE_ZERO]));
            break;
        case CLI_MPLS_DELETE_ELSP_INFO_LIST_INDEX:
            i4RetStatus =
                TeCliMplsDeleteElspInfoPhbMap (CliHandle,
                                               *((INT4 *) pu4args[TE_ZERO]));
            break;
        case CLI_MPLS_DSTE_STATUS:
            i4RetStatus = TeCliMplsSetDsteStatus (CliHandle,
                                                  CLI_PTR_TO_U4 (pu4args
                                                                 [TE_ZERO]));
            break;
        case CLI_MPLS_DSTE_CLASS_TYPE:
            i4RetStatus =
                TeCliMplsSetDsTeClassType (CliHandle, *(pu4args[ZERO]),
                                           (UINT1 *) (pu4args[TE_ONE]),
                                           *(pu4args[TE_TWO]));
            break;
        case CLI_MPLS_DSTE_NO_CLASS_TYPE:
            i4RetStatus =
                TeCliMplsDeleteDsTeClassType (CliHandle, *(pu4args[ZERO]));
            break;
        case CLI_MPLS_PRECONFIG_EXP_PHB_INDEX:
            i4RetStatus =
                TeCliSetPreConfExpPhbIndex (CliHandle,
                                            *((INT4 *) pu4args[ZERO]));
            break;
        case CLI_MPLS_UNMAP_PRECONFIG_EXP_PHB_INDEX:
            i4RetStatus =
                TeCliUnMapPreConfExpPhbIndex (CliHandle,
                                              *((INT4 *) pu4args[ZERO]));
            break;
        case CLI_MPLS_CONFIGURE_EXP_PHB_MAP:
            i4RetStatus =
                TeCliMplsSetExpToPhbMapping (CliHandle, *(pu4args[TE_ZERO]),
                                             *(pu4args[TE_ONE]));
            break;
        case CLI_MPLS_REMOVE_EXP_PHB_MAP:
            i4RetStatus =
                TeCliMplsDestroyExpToPhbMapping (CliHandle,
                                                 *(pu4args[TE_ZERO]));
            break;
        case CLI_MPLS_CONFIGURE_ELSP_INFO_PHB_MAP:
            i4RetStatus =
                TeCliMplsSetElspInfoPhbMapping (CliHandle, *(pu4args[TE_ZERO]),
                                                *(pu4args[TE_ONE]));
            break;
        case CLI_MPLS_REMOVE_ELSP_INFO_PHB_MAP:
            i4RetStatus =
                TeCliMplsDestroyElspInfoPhbMapping (CliHandle,
                                                    *(pu4args[TE_ZERO]));
            break;
        case CLI_MPLS_DSTE_TRAFFIC_CLASS:
            i4RetStatus = TeCliMplsSetDsTeClassTcMap (CliHandle,
                                                      CLI_PTR_TO_U4 (pu4args
                                                                     [TE_ZERO]),
                                                      (UINT1
                                                       *) (pu4args[TE_ONE]));
            break;
        case CLI_MPLS_DSTE_NO_TRAFFIC_CLASS:
            i4RetStatus = TeCliMplsDeleteDsTeClassTcMap (CliHandle,
                                                         CLI_PTR_TO_U4 (pu4args
                                                                        [TE_ZERO]));
            break;
        case CLI_MPLS_DSTE_TE_CLASS:
            i4RetStatus =
                TeCliMplsSetDsteTeClass (CliHandle, *(pu4args[TE_ZERO]),
                                         *(pu4args[TE_ONE]),
                                         (UINT1 *) (pu4args[TE_TWO]));
            break;
        case CLI_MPLS_DSTE_NO_TE_CLASS:
            i4RetStatus =
                TeCliMplsDeleteDsteTeClass (CliHandle, *(pu4args[TE_ZERO]));
            break;
        case CLI_MPLS_DIFFSERV_ELSP_TYPE:
            i4RetStatus = TeCliMplsSetDiffServElspType (CliHandle,
                                                        CLI_PTR_TO_U4 (pu4args
                                                                       [TE_ZERO]));
            break;
        case CLI_MPLS_ASSOCIATE_EXP_PHB_MAP:
            i4RetStatus =
                TeCliMplsAssociateExpPhbMap (CliHandle, *(pu4args[TE_ZERO]));
            break;
        case CLI_MPLS_DIFFSERV_LLSP_PSC:
            i4RetStatus = TeCliMplsDiffServLlspPsc (CliHandle,
                                                    CLI_PTR_TO_U4 (pu4args
                                                                   [TE_ZERO]));
            break;
        case CLI_MPLS_DSTE_SHOW_STATUS:
            i4RetStatus = TeCliMplsDsTeShowStatus (CliHandle);
            break;
        case CLI_MPLS_DSTE_SHOW_CLASS:
            i4RetStatus =
                TeCliMplsDiffServShowClass (CliHandle, *(pu4args[TE_ZERO]));
            break;
        case CLI_MPLS_REOPTIMIZE_TNL_LIST:
            TeCliArgs.u4TunnelIndex = *(pu4args[0]);
            TeCliArgs.u4EgressId = *(pu4args[1]);
            TeCliArgs.u4IngressId = *(pu4args[2]);
            i4RetStatus =
                TeCliAddTunnelInReoptimizeList (CliHandle, &TeCliArgs);
            break;
        case CLI_MPLS_REOPTIMIZE_NO_TNL_LIST:
            TeCliArgs.u4TunnelIndex = *(pu4args[0]);
            TeCliArgs.u4EgressId = *(pu4args[1]);
            TeCliArgs.u4IngressId = *(pu4args[2]);
            i4RetStatus =
                TeCliDelTunnelFromReoptimizeList (CliHandle, &TeCliArgs);
            break;
        case CLI_MPLS_REOPTIMIZE_TUNNEL_SHOW:
            i4RetStatus = TeCliShowReoptimizeTunnelList (CliHandle);
            break;
        case CLI_MPLS_REOPTIMIZE_MANUAL_TRIGGER:
            TeCliArgs.u4TunnelIndex = *(pu4args[0]);
            TeCliArgs.u4EgressId = *(pu4args[1]);
            TeCliArgs.u4IngressId = *(pu4args[2]);
            i4RetStatus =
                TeCliTriggerManualLspReoptimization (CliHandle, &TeCliArgs);
            break;
        default:
            CliPrintf (CliHandle, "\r%% Unknown command \r\n");
            return (CLI_FAILURE);
    }

    if ((i4RetStatus == CLI_FAILURE)
        && (CLI_GET_ERR (&u4ErrCode) == CLI_SUCCESS))
    {
        if ((u4ErrCode >= CLI_ERR_START_ID_MPLS_TE) &&
            (u4ErrCode < MPLS_CLI_TE_MAX_ERROR_MSGS))
        {
            CliPrintf (CliHandle, "%s",
                       TE_CLI_ERROR_MSGS[CLI_ERR_OFFSET_MPLS_TE (u4ErrCode)]);
        }
        CLI_SET_ERR (0);
    }

    return i4RetStatus;
}

/****************************************************************************
* Function    :  TeCliSetDebugLevel
* Description :
* Input       :  CliHandle, i4CliDebugLevel
* Output      :  None
* Returns     :  CLI_SUCCESS/CLI_FAILURE
****************************************************************************/
INT4
TeCliSetDebugLevel (tCliHandle CliHandle, INT4 i4CliDebugLevel)
{
    TE_DBG_LVL = 0;
    UNUSED_PARAM (CliHandle);
    if (i4CliDebugLevel == DEBUG_DEBUG_LEVEL)
    {
        TE_DBG_LVL =
            ENTRY_AND_EXIT | TE_MAIN_PRCS | TE_MAIN_FAIL | TE_MAIN_MEM |
            TE_MAIN_SNMP | TE_LLVL_PRCS | TE_LLVL_FAIL | TE_LLVL_SET_SCSS |
            TE_LLVL_SET_FAIL | TE_LLVL_GET_SCSS | TE_LLVL_GET_FAIL |
            TE_LLVL_TEST_SCSS | TE_LLVL_TEST_FAIL | TE_EXTN_PRCS | TE_EXTN_FAIL
            | TE_EXTN_ALL | TE_DIFF_PRCS | TE_DIFF_FAIL;

    }
    else if (i4CliDebugLevel == DEBUG_INFO_LEVEL)
    {
        TE_DBG_LVL =
            TE_MAIN_ETEXT | TE_LLVL_ETEXT | TE_EXTN_ETEXT | TE_DIFF_ETEXT;

    }
    else if (i4CliDebugLevel == DEBUG_NOTICE_LEVEL)
    {
        TE_DBG_LVL =
            ENTRY_AND_EXIT | TE_MAIN_PRCS | TE_MAIN_FAIL | TE_MAIN_MEM |
            TE_MAIN_SNMP | TE_LLVL_PRCS | TE_LLVL_FAIL | TE_LLVL_SET_SCSS |
            TE_LLVL_SET_FAIL | TE_LLVL_GET_SCSS | TE_LLVL_GET_FAIL |
            TE_LLVL_TEST_SCSS | TE_LLVL_TEST_FAIL | TE_EXTN_PRCS | TE_EXTN_FAIL
            | TE_EXTN_ALL | TE_DIFF_PRCS | TE_DIFF_FAIL;
    }
    else if (i4CliDebugLevel == DEBUG_WARN_LEVEL)
    {
        TE_DBG_LVL =
            ENTRY_AND_EXIT | TE_MAIN_PRCS | TE_MAIN_MEM | TE_MAIN_SNMP |
            TE_LLVL_PRCS | TE_LLVL_SET_SCSS | TE_LLVL_GET_SCSS;
    }
    else if (i4CliDebugLevel == DEBUG_ERROR_LEVEL)
    {
        TE_DBG_LVL =
            TE_MAIN_FAIL | TE_LLVL_SET_FAIL | TE_LLVL_FAIL | TE_LLVL_GET_FAIL |
            TE_LLVL_TEST_FAIL | TE_EXTN_FAIL;
    }
    else if ((i4CliDebugLevel == DEBUG_CRITICAL_LEVEL)
             || (i4CliDebugLevel == DEBUG_ALERT_LEVEL)
             || (i4CliDebugLevel == DEBUG_EMERG_LEVEL))
    {
        TE_DBG_LVL =
            ENTRY_AND_EXIT | TE_MAIN_PRCS | TE_MAIN_FAIL | TE_MAIN_MEM |
            TE_MAIN_SNMP | TE_LLVL_PRCS | TE_LLVL_FAIL | TE_LLVL_SET_SCSS |
            TE_LLVL_SET_FAIL | TE_LLVL_GET_SCSS | TE_LLVL_GET_FAIL |
            TE_LLVL_TEST_SCSS | TE_LLVL_TEST_FAIL | TE_EXTN_PRCS | TE_EXTN_FAIL
            | TE_EXTN_ALL | TE_DIFF_PRCS | TE_DIFF_FAIL;
    }
    else if (i4CliDebugLevel == DEBUG_DEF_LVL_FLAG)
    {
        return CLI_SUCCESS;
    }
    else
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
* Function Name : TeCliShowTunnel 
* Description   : This routine is used to display tunnel information
* Input(s)      : CliHandle        - Cli Context Handle
                  u4Command        - Specifies the command given as the input
                  u4ShowMode           - Mode in which the SHOW output should be
                  pu1Arg           - can be EgressLsrId or TunnelName
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT1
TeCliShowTunnel (tCliHandle CliHandle, UINT4 u4Command,
                 UINT4 u4ShowMode, UINT1 *pu1Arg)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4TnlId = 0;
    UINT4               u4TnlInstance = 0;
    UINT4               u4TnlIngressLSrId = 0;
    UINT4               u4TnlEgressLsrId = 0;
    UINT4               u4TotalHeadTnls = 0;
    UINT4               u4SigTnls = 0;
    UINT4               u4OperUpTnls = 0;
    UINT4               u4AdmnUpTnls = 0;
    UINT4               u4AdmnDnTnls = 0;
    UINT4               u4TransitTnls = 0;
    UINT4               u4TailTnls = 0;
    BOOL1               bFound = FALSE;
    BOOL1               bFlag = FALSE;
    BOOL1               bTitleFlag = TRUE;
    UINT4               u4NumActiveTnl = 0;

    if ((nmhGetFirstIndexMplsTunnelTable (&u4TnlId, &u4TnlInstance,
                                          &u4TnlIngressLSrId,
                                          &u4TnlEgressLsrId)) == SNMP_SUCCESS)
    {
        do
        {
            bFound = FALSE;
            CliRegisterLock (CliHandle, CmnLock, CmnUnLock);
            MPLS_CMN_LOCK ();
            pTeTnlInfo = TeGetTunnelInfo (u4TnlId, u4TnlInstance,
                                          u4TnlIngressLSrId, u4TnlEgressLsrId);
            if (pTeTnlInfo != NULL)
            {
                if (u4Command == CLI_MPLS_TE_NAME)
                {
                    if (STRCMP (TE_TNL_NAME (pTeTnlInfo), pu1Arg) == TE_ZERO)
                    {
                        bFound = TRUE;
                    }
                }
                if ((u4Command == CLI_MPLS_TE_DEST_ADDR) ||
                    (u4Command == CLI_MPLS_TE_DEST_LOCAL_MAP_NUM))
                {
                    if (u4TnlEgressLsrId == *(UINT4 *) (VOID *) pu1Arg)
                    {
                        bFound = TRUE;
                    }
                }
                else if ((u4Command == CLI_MPLS_TE_SRC_ADDR) ||
                         (u4Command == CLI_MPLS_TE_SRC_LOCAL_MAP_NUM))
                {
                    if (u4TnlIngressLSrId == *(UINT4 *) (VOID *) pu1Arg)
                    {
                        bFound = TRUE;
                    }
                }
                else if ((u4Command == CLI_MPLS_TE_ROLE_ALL)
                         || (u4Command == CLI_MPLS_TE_ALL))
                {
                    bFound = TRUE;
                }
                else if (u4Command == CLI_MPLS_TE_ROLE_HEAD)
                {
                    if (TE_TNL_ROLE (pTeTnlInfo) == TE_INGRESS)
                    {
                        bFound = TRUE;
                    }
                }
                else if (u4Command == CLI_MPLS_TE_ROLE_MIDDLE)
                {
                    if (TE_TNL_ROLE (pTeTnlInfo) == TE_INTERMEDIATE)
                    {
                        bFound = TRUE;
                    }
                }
                else if (u4Command == CLI_MPLS_TE_ROLE_TAIL)
                {
                    if (TE_TNL_ROLE (pTeTnlInfo) == TE_EGRESS)
                    {
                        bFound = TRUE;
                    }
                }
                else if (u4Command == CLI_MPLS_TE_ROLE_REMOTE)
                {
                    if (TE_TNL_ROLE (pTeTnlInfo) != TE_INGRESS)
                    {
                        bFound = TRUE;
                    }
                }
                else if (u4Command == CLI_MPLS_TE_UNIDIRECTIONAL)
                {
                    if (pTeTnlInfo->u4TnlMode == TE_TNL_MODE_UNIDIRECTIONAL)
                    {
                        bFound = TRUE;
                    }
                }
                else if (u4Command == CLI_MPLS_TE_COROUTED_BIDIR)
                {
                    if (pTeTnlInfo->u4TnlMode ==
                        TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
                    {
                        bFound = TRUE;
                    }
                }
                else if (u4Command == CLI_MPLS_TE_ASSOCIATED_BIDIR)
                {
                    if (pTeTnlInfo->u4TnlMode ==
                        TE_TNL_MODE_ASSOCIATED_BIDIRECTIONAL)
                    {
                        bFound = TRUE;
                    }
                }

                if (bFound == TRUE)
                {
                    if (u4ShowMode == CLI_MPLS_TE_BRIEF)
                    {
                        if (bTitleFlag)
                        {
                            CliPrintf (CliHandle,
                                       "\rTUNNEL NAME     "
                                       "SOURCE             DESTINATION    "
                                       " UP IF   " "DOWN IF   " "STATE/PROT\n");
                            bTitleFlag = FALSE;
                        }
                        TeCliShowTnlBrief (CliHandle, pTeTnlInfo);
                        if (pTeTnlInfo->u4TnlInstance != TE_ZERO)
                        {
                            u4NumActiveTnl++;
                        }
                    }
                    else if (u4ShowMode == CLI_MPLS_TE_SUMMARY)
                    {
                        TeCliShowTnlSummary (CliHandle, pTeTnlInfo,
                                             &u4TotalHeadTnls, &u4SigTnls,
                                             &u4OperUpTnls, &u4AdmnUpTnls,
                                             &u4AdmnDnTnls, &u4TransitTnls,
                                             &u4TailTnls);
                    }
                    else
                    {
                        TeCliShowTnlDetail (CliHandle, pTeTnlInfo);
                    }
                }
            }
            MPLS_CMN_UNLOCK ();
            CliUnRegisterLock (CliHandle);
            /*bFlag is set to TRUE when any matchin tunnels found */
            if (bFound == TRUE)
            {
                bFlag = TRUE;
            }

        }
        while ((nmhGetNextIndexMplsTunnelTable (u4TnlId, &u4TnlId,
                                                u4TnlInstance,
                                                &u4TnlInstance,
                                                u4TnlIngressLSrId,
                                                &u4TnlIngressLSrId,
                                                u4TnlEgressLsrId,
                                                &u4TnlEgressLsrId))
               == SNMP_SUCCESS);
    }
    if ((u4ShowMode == CLI_MPLS_TE_SUMMARY) && (bFlag == TRUE))
    {
        CliPrintf (CliHandle, "\r\n Signalling Summary:\n");
        CliPrintf (CliHandle, "\r  Head: %d interfaces,%d signalling attemps,"
                   "%d established\n", u4TotalHeadTnls, u4SigTnls,
                   u4OperUpTnls);
        CliPrintf (CliHandle, "\r \t %d activations,%d deactivations\n",
                   u4AdmnUpTnls, u4AdmnDnTnls);
        CliPrintf (CliHandle, "\r  Midpoint:%d, Tails:%d\n", u4TransitTnls,
                   u4TailTnls);
    }

    if (u4ShowMode == CLI_MPLS_TE_BRIEF)
    {
        CliPrintf (CliHandle, "\r\nTotal number of Active Tunnels: %u\n",
                   u4NumActiveTnl);
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
* Function Name : TeCliShowTnlBrief 
* Description   : This routine is used to display tunnel information in brief
* Input(s)      : CliHandle  - Cli Context Handle
                  pTeTnlInfo - Tunnel information
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT1
TeCliShowTnlBrief (tCliHandle CliHandle, tTeTnlInfo * pTeTnlInfo)
{
    tXcEntry           *pXcEntry = NULL;
    tXcEntry           *pXcEntry1 = NULL;
    tCfaIfInfo          IfInfo;
    UINT4               u4EgrTnlIp = 0;
    UINT4               u4IgrTnlIp = 0;
    UINT4               u4InIntf = 0;
    UINT1               au1Status[2][5] = { {"UP"}, {"DOWN"} };
    UINT4               u4L3Intf = 0;
    CHR1               *pCli = NULL;
    CHR1                acEgrTnlIp[TE_CLI_ADDR];
    CHR1                acIgrTnlIp[TE_CLI_ADDR];

    MEMCPY ((UINT1 *) &u4IgrTnlIp, &TE_TNL_INGRESS_LSRID (pTeTnlInfo), 4);
    u4IgrTnlIp = OSIX_HTONL (u4IgrTnlIp);
    CLI_CONVERT_IPADDR_TO_STR (pCli, u4IgrTnlIp);
    STRNCPY (acIgrTnlIp, pCli, (TE_CLI_ADDR - 1));
    acIgrTnlIp[TE_CLI_ADDR - 1] = '\0';

    MEMCPY ((UINT1 *) &u4EgrTnlIp, &TE_TNL_EGRESS_LSRID (pTeTnlInfo), 4);
    u4EgrTnlIp = OSIX_HTONL (u4EgrTnlIp);
    CLI_CONVERT_IPADDR_TO_STR (pCli, u4EgrTnlIp);
    STRNCPY (acEgrTnlIp, pCli, (TE_CLI_ADDR - 1));
    acEgrTnlIp[TE_CLI_ADDR - 1] = '\0';

    if (pTeTnlInfo->u4TnlInstance == TE_ZERO)
    {
        return CLI_SUCCESS;
    }

    if (TE_TNL_TNL_INSTANCE (pTeTnlInfo) >= TE_DETOUR_TNL_INSTANCE)
    {
        if (pTeTnlInfo->u4TnlLsrIdMapInfo & TE_TNL_INGRESSID_MAP_INFO)
        {
            CliPrintf (CliHandle, "\r%s [Detour LSP] \t%d ",
                       TE_TNL_NAME (pTeTnlInfo), u4IgrTnlIp);
        }
        else
        {
            CliPrintf (CliHandle, "\r%s [Detour LSP] \t%s ",
                       TE_TNL_NAME (pTeTnlInfo), acIgrTnlIp);
        }

        if (pTeTnlInfo->u4TnlLsrIdMapInfo & TE_TNL_EGRESSID_MAP_INFO)
        {
            CliPrintf (CliHandle, "\r [Detour LSP] \t \t%d ", u4EgrTnlIp);
        }
        else
        {
            CliPrintf (CliHandle, "\r [Detour LSP] \t \t%s ", acEgrTnlIp);
        }
    }
    else
    {
        if (pTeTnlInfo->u1DetourActive == TE_TRUE)
        {
            if (pTeTnlInfo->u4TnlLsrIdMapInfo & TE_TNL_INGRESSID_MAP_INFO)
            {
                CliPrintf (CliHandle, "\r%s [Detour Active] \t%d ",
                           TE_TNL_NAME (pTeTnlInfo), u4IgrTnlIp);
            }
            else
            {
                CliPrintf (CliHandle, "\r%s [Detour Active] \t%s ",
                           TE_TNL_NAME (pTeTnlInfo), acIgrTnlIp);
            }

            if (pTeTnlInfo->u4TnlLsrIdMapInfo & TE_TNL_EGRESSID_MAP_INFO)
            {
                CliPrintf (CliHandle, "\r [Detour Active] \t %d ", u4EgrTnlIp);
            }
            else
            {
                CliPrintf (CliHandle, "\r [Detour Active] \t %s ", acEgrTnlIp);
            }
        }
        else
        {
            if (pTeTnlInfo->u4TnlLsrIdMapInfo & TE_TNL_INGRESSID_MAP_INFO)
            {
                CliPrintf (CliHandle, "\r%s  \t%d ",
                           TE_TNL_NAME (pTeTnlInfo), u4IgrTnlIp);
            }
            else
            {
                CliPrintf (CliHandle, "\r%s  \t%s ",
                           TE_TNL_NAME (pTeTnlInfo), acIgrTnlIp);
            }
            if (pTeTnlInfo->u4TnlLsrIdMapInfo & TE_TNL_EGRESSID_MAP_INFO)
            {
                CliPrintf (CliHandle, "\t \t   %-12d ", u4EgrTnlIp);
            }
            else
            {
                CliPrintf (CliHandle, "\t   %-12s ", acEgrTnlIp);
            }
        }
    }

    pXcEntry = MplsGetXCEntryByDirection (pTeTnlInfo->u4TnlXcIndex,
                                          MPLS_DIRECTION_FORWARD);
    pXcEntry1 = MplsGetXCEntryByDirection (pTeTnlInfo->u4TnlXcIndex,
                                           MPLS_DIRECTION_REVERSE);

    if ((pXcEntry != NULL) && (pXcEntry1 != NULL))
    {
        if ((XC_ININDEX (pXcEntry) != NULL)
            && ((XC_ININDEX (pXcEntry1) != NULL)))
        {
            u4InIntf = INSEGMENT_IF_INDEX (XC_ININDEX (pXcEntry));
            if (MplsGetL3Intf (u4InIntf, &u4L3Intf) == MPLS_SUCCESS)
            {
                u4InIntf = u4L3Intf;
            }
            if (CfaGetIfInfo (u4InIntf, &IfInfo) == CFA_SUCCESS)
            {
                CliPrintf (CliHandle, "   %-8s", IfInfo.au1IfName);
            }
        }

        else if ((XC_ININDEX (pXcEntry) != NULL)
                 && (XC_OUTINDEX (pXcEntry) == NULL))
        {
            u4InIntf = INSEGMENT_IF_INDEX (XC_ININDEX (pXcEntry));
            if (MplsGetL3Intf (u4InIntf, &u4L3Intf) == MPLS_SUCCESS)
            {
                u4InIntf = u4L3Intf;
            }
            if (CfaGetIfInfo (u4InIntf, &IfInfo) == CFA_SUCCESS)
            {
                CliPrintf (CliHandle, "   %-8s", IfInfo.au1IfName);
            }
        }
        else
        {
            CliPrintf (CliHandle, "     -     ");
        }
        if ((XC_OUTINDEX (pXcEntry1) != NULL)
            && (XC_OUTINDEX (pXcEntry) != NULL))
        {
            u4InIntf = OUTSEGMENT_IF_INDEX (XC_OUTINDEX (pXcEntry1));
            if (MplsGetL3Intf (OUTSEGMENT_IF_INDEX
                               (XC_OUTINDEX (pXcEntry1)),
                               &u4L3Intf) == MPLS_SUCCESS)
            {
                if (CfaGetIfInfo (u4L3Intf, &IfInfo) == CFA_SUCCESS)
                {
                    CliPrintf (CliHandle, "%-8s", IfInfo.au1IfName);
                }

            }
        }

        else if ((XC_ININDEX (pXcEntry1) != NULL)
                 && (XC_OUTINDEX (pXcEntry1) == NULL))
        {
            u4InIntf = OUTSEGMENT_IF_INDEX (XC_OUTINDEX (pXcEntry));
            if (MplsGetL3Intf (OUTSEGMENT_IF_INDEX
                               (XC_OUTINDEX (pXcEntry)),
                               &u4L3Intf) == MPLS_SUCCESS)
            {
                if (CfaGetIfInfo (u4L3Intf, &IfInfo) == CFA_SUCCESS)
                {
                    CliPrintf (CliHandle, "%-8s", IfInfo.au1IfName);
                }

            }
        }
        else
        {
            CliPrintf (CliHandle, "   -   ");
        }
    }
    else if ((pXcEntry != NULL) && (pXcEntry1 == NULL))
    {
        if ((XC_ININDEX (pXcEntry) != NULL))
        {
            u4InIntf = INSEGMENT_IF_INDEX (XC_ININDEX (pXcEntry));
            if (MplsGetL3Intf (u4InIntf, &u4L3Intf) == MPLS_SUCCESS)
            {
                u4InIntf = u4L3Intf;
            }
            if (CfaGetIfInfo (u4InIntf, &IfInfo) == CFA_SUCCESS)
            {
                CliPrintf (CliHandle, "   %-8s", IfInfo.au1IfName);
            }
        }

        else if ((XC_ININDEX (pXcEntry) != NULL)
                 && (XC_OUTINDEX (pXcEntry) == NULL))
        {
            u4InIntf = INSEGMENT_IF_INDEX (XC_ININDEX (pXcEntry));
            if (MplsGetL3Intf (u4InIntf, &u4L3Intf) == MPLS_SUCCESS)
            {
                u4InIntf = u4L3Intf;
            }
            if (CfaGetIfInfo (u4InIntf, &IfInfo) == CFA_SUCCESS)
            {
                CliPrintf (CliHandle, "   %-8s", IfInfo.au1IfName);
            }
        }
        else
        {
            CliPrintf (CliHandle, "     -     ");
        }
        if ((XC_OUTINDEX (pXcEntry) != NULL))
        {
            u4InIntf = OUTSEGMENT_IF_INDEX (XC_OUTINDEX (pXcEntry));
            if (MplsGetL3Intf (OUTSEGMENT_IF_INDEX
                               (XC_OUTINDEX (pXcEntry)),
                               &u4L3Intf) == MPLS_SUCCESS)
            {
                if (CfaGetIfInfo (u4L3Intf, &IfInfo) == CFA_SUCCESS)
                {
                    CliPrintf (CliHandle, "%-8s", IfInfo.au1IfName);
                }

            }
        }
        else
        {
            CliPrintf (CliHandle, "   -    ");
        }
    }
    else
    {
        CliPrintf (CliHandle, "    -       -      ");
    }
    CliPrintf (CliHandle, "\t%s/%s\n",
               au1Status[TE_TNL_ADMIN_STATUS (pTeTnlInfo) - 1],
               au1Status[TE_TNL_OPER_STATUS (pTeTnlInfo) - 1]);
    return CLI_SUCCESS;
}

/*******************************************************************************
* Function Name : TeCliShowTnlSummary 
* Description   : This routine is used to display tunnel info. as a summary
* Input(s)      : CliHandle        - Cli Context Handle
                  pTeTnlInfo       - Tunnel Info.
                  pTotalHeadTnls   - Count for Head Tnls(Starts from this node)
                  pu4SigTnls       - Count for Signalled 
                  pu4OperUpTnls    - Count for Tunnels in Oper UP
                  pu4AdmnUpTnls    - Count for Tunnels in Oper Down
                  pu4AdmnDnTnls    - Count for Tunnels in Admin UP
                  pu4TransitTnls   - Count for Tunnels in Admin Down
                  pu4TailTnls      - Count for Tail Tunnels (Ends in this node)
* Output(s)     : None
* Return(s)     : None
*******************************************************************************/
VOID
TeCliShowTnlSummary (tCliHandle CliHandle, tTeTnlInfo * pTeTnlInfo,
                     UINT4 *pTotalHeadTnls, UINT4 *pu4SigTnls,
                     UINT4 *pu4OperUpTnls, UINT4 *pu4AdmnUpTnls,
                     UINT4 *pu4AdmnDnTnls, UINT4 *pu4TransitTnls,
                     UINT4 *pu4TailTnls)
{
    UNUSED_PARAM (CliHandle);

    if (TE_TNL_ROLE (pTeTnlInfo) == TE_INGRESS)
    {
        (*pTotalHeadTnls)++;
    }
    if (TE_TNL_ROLE (pTeTnlInfo) == TE_EGRESS)
    {
        (*pu4TailTnls)++;
    }
    if (TE_TNL_ROLE (pTeTnlInfo) == TE_INTERMEDIATE)
    {
        (*pu4TransitTnls)++;
    }
    if ((TE_TNL_ROLE (pTeTnlInfo) == TE_INGRESS) &&
        ((TE_TNL_OWNER (pTeTnlInfo) == TE_TNL_OWNER_RSVP)
         || (TE_TNL_OWNER (pTeTnlInfo) == TE_TNL_OWNER_LDP)))
    {
        (*pu4SigTnls)++;
    }
    if ((TE_TNL_ROLE (pTeTnlInfo) == TE_INGRESS) &&
        (TE_TNL_OPER_STATUS (pTeTnlInfo) == TE_OPER_UP))
    {
        (*pu4OperUpTnls)++;
    }
    if ((TE_TNL_ROLE (pTeTnlInfo) == TE_INGRESS) &&
        (TE_TNL_ADMIN_STATUS (pTeTnlInfo) == TE_ADMIN_UP))
    {
        (*pu4AdmnUpTnls)++;
    }
    if ((TE_TNL_ROLE (pTeTnlInfo) == TE_INGRESS) &&
        (TE_TNL_ADMIN_STATUS (pTeTnlInfo) == TE_ADMIN_DOWN))
    {
        (*pu4AdmnDnTnls)++;
    }
}

/*******************************************************************************
* Function Name : TeCliShowTnlDetail 
* Description   : This routine is used to display tunnel information in Detail
* Input(s)      : CliHandle        - Cli Context Handle
                  pTeTnlInfo       - Tunnel Info.
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT1
TeCliShowTnlDetail (tCliHandle CliHandle, tTeTnlInfo * pTeTnlInfo)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    tTeGetAttributes    TeGetAttributes;
    tTeTnlSrlg         *pTnlSrlg = NULL;
    INT1                ai1TnlCreateTime[MAX_CLI_TIME_STRING];
    INT1                ai1TnlReoptimizeTime[MAX_CLI_TIME_STRING];
    INT1                ai1TnlLastChange[MAX_CLI_TIME_STRING];
    INT1                ai1TnlUpTime[MAX_CLI_TIME_STRING];
    UINT4               u4CurrentTime = 0;
    UINT1               au1Status[6][10] = { {"up"}, {"down"},
    {"Invalid"}, {"Valid"},
    {"connected"}, {"None"}
    };
    CHR1               *pEgrTnlIp = NULL;
    CHR1               *pIngTnlIp = NULL;
    INT4                i4ValidFlag = 0;
    INT4                i4SigFlag = 0;
    BOOL1               b1ConfigFlag = FALSE;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    UINT4               u4TnlIndex = TE_ZERO;
    UINT4               u4TnlInstance = TE_ZERO;
    tTeTnlInfo         *pInstance0Tnl = NULL;
    tTeTnlInfo         *pActiveTnlInfo = NULL;
    UINT4               u4UsedInstance = TE_ZERO;

    MEMSET (&TeGetAttributes, TE_ZERO, sizeof (tTeGetAttributes));

    CONVERT_TO_INTEGER (TE_TNL_EGRESS_LSRID (pTeTnlInfo), u4EgressId);
    u4EgressId = OSIX_NTOHL (u4EgressId);
    CLI_CONVERT_IPADDR_TO_STR (pEgrTnlIp, u4EgressId);

    CONVERT_TO_INTEGER (TE_TNL_INGRESS_LSRID (pTeTnlInfo), u4IngressId);
    u4IngressId = OSIX_NTOHL (u4IngressId);

    u4TnlIndex = TE_TNL_TNL_INDEX (pTeTnlInfo);
    u4TnlInstance = TE_TNL_TNL_INSTANCE (pTeTnlInfo);

    if (pTeTnlInfo->u4TnlInstance != TE_ZERO)
    {
        pInstance0Tnl = TeGetTunnelInfo (pTeTnlInfo->u4TnlIndex, TE_ZERO,
                                         u4IngressId, u4EgressId);

        if (pInstance0Tnl != NULL)
        {
            /* Instance 0 tunnel is present for this Non-Zero Instance.
             * So, no need to display show output for this. */
            return TE_SUCCESS;
        }
    }

    if (TE_TNL_TNL_INSTANCE (pTeTnlInfo) >= TE_DETOUR_TNL_INSTANCE)
    {
        if (pTeTnlInfo->u4TnlLsrIdMapInfo & TE_TNL_EGRESSID_MAP_INFO)
        {
            CliPrintf (CliHandle,
                       "\r\n Name: %s [Detour LSP]           "
                       "     Destination : %d\n",
                       TE_TNL_NAME (pTeTnlInfo), u4EgressId);
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\n Name: %s [Detour LSP]           "
                       "     Destination : %s\n",
                       TE_TNL_NAME (pTeTnlInfo), pEgrTnlIp);
        }
    }
    else
    {
        if (pTeTnlInfo->u1DetourActive == TE_TRUE)
        {
            if (pTeTnlInfo->u4TnlLsrIdMapInfo & TE_TNL_EGRESSID_MAP_INFO)
            {
                CliPrintf (CliHandle, "\r\n Name: %s [Detour LSP Active]    "
                           "     Destination : %d\n",
                           TE_TNL_NAME (pTeTnlInfo), u4EgressId);
            }
            else
            {
                CliPrintf (CliHandle, "\r\n Name: %s [Detour LSP Active]    "
                           "     Destination : %s\n",
                           TE_TNL_NAME (pTeTnlInfo), pEgrTnlIp);
            }
        }
        else
        {
            if (pTeTnlInfo->u4TnlLsrIdMapInfo & TE_TNL_EGRESSID_MAP_INFO)
            {
                CliPrintf (CliHandle, "\r\n Name: %s                        "
                           "     Destination : %d\n",
                           TE_TNL_NAME (pTeTnlInfo), u4EgressId);
            }
            /* MPLS_P2MP_LSP_CHANGES - S */
            else if (TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_P2MP)
            {
                CliPrintf (CliHandle, "\r\n Name: %s                        "
                           "     P2MP ID : %d\n",
                           TE_TNL_NAME (pTeTnlInfo), u4EgressId);
            }
            /* MPLS_P2MP_LSP_CHANGES - S */
            else
            {
                CliPrintf (CliHandle, "\r\n Name: %s                        "
                           "     Destination : %s\n",
                           TE_TNL_NAME (pTeTnlInfo), pEgrTnlIp);

            }
        }
    }

    if ((pTeTnlInfo->pTunnelErrInfo == NULL) ||
        (TE_TNL_OPER_STATUS (pTeTnlInfo) == TE_OPER_UP))
    {
        i4ValidFlag = TE_THREE;
    }
    else
    {
        i4ValidFlag = TE_TWO;
    }

    if ((pTeTnlInfo->u1TnlSgnlPrtcl == TE_SIGPROTO_NONE) ||
        (i4ValidFlag == TE_TWO))
    {
        i4SigFlag = TE_FIVE;
    }
    else
    {
        i4SigFlag = TE_FOUR;
    }

    CliPrintf (CliHandle, "\r   Status:\n\r    Admin: %-4s Oper: %-4s ",
               au1Status[TE_TNL_ADMIN_STATUS (pTeTnlInfo) - 1],
               au1Status[TE_TNL_OPER_STATUS (pTeTnlInfo) - 1]);
    CliPrintf (CliHandle, "Path: %-7s Signaling: %-10s\n",
               au1Status[i4ValidFlag], au1Status[i4SigFlag]);

    CliPrintf (CliHandle, "\r   Tunnel Type:");
    if (pTeTnlInfo->u4TnlType & TE_TNL_TYPE_MPLS)
    {
        CliPrintf (CliHandle, " mpls-te");
    }
    if (pTeTnlInfo->u4TnlType & TE_TNL_TYPE_MPLSTP)
    {
        CliPrintf (CliHandle, " mpls-tp");
    }
    if (pTeTnlInfo->u4TnlType & TE_TNL_TYPE_HLSP)
    {
        CliPrintf (CliHandle, " h-lsp");
    }
    if (pTeTnlInfo->u4TnlType & TE_TNL_TYPE_GMPLS)
    {
        CliPrintf (CliHandle, " gmpls");
    }
    /* MPLS_P2MP_LSP_CHANGES - S */
    if (pTeTnlInfo->u4TnlType & TE_TNL_TYPE_P2MP)
    {
        CliPrintf (CliHandle, " p2mp");
    }
    /* MPLS_P2MP_LSP_CHANGES - E */
    if (pTeTnlInfo->u4TnlType & TE_TNL_TYPE_SLSP)
    {
        CliPrintf (CliHandle, " s-lsp");
    }

    switch (pTeTnlInfo->u4TnlMode)
    {
        case TE_TNL_MODE_UNIDIRECTIONAL:
            CliPrintf (CliHandle, "\n\r   Tunnel Mode: unidirectional\n");
            break;
        case TE_TNL_MODE_COROUTED_BIDIRECTIONAL:
            CliPrintf (CliHandle,
                       "\n\r   Tunnel Mode: Corouted bi-directional\n");
            break;
        case TE_TNL_MODE_ASSOCIATED_BIDIRECTIONAL:
            CliPrintf (CliHandle,
                       "\n\r   Tunnel Mode: associated bi-directional\n");
            break;
        default:
            CliPrintf (CliHandle, "\n\r   Tunnel Mode: unknown\n");
    }
    if (pTeTnlInfo->u4TnlMode == TE_TNL_MODE_ASSOCIATED_BIDIRECTIONAL)
    {
        CliPrintf (CliHandle, "\r   Reverse tunnel index: %d\n",
                   pTeTnlInfo->u4DestTnlIndex);
        CliPrintf (CliHandle, "\r   Reverse tunnel lsp-num: %d\n",
                   pTeTnlInfo->u4DestTnlInstance);
    }

    CLI_CONVERT_IPADDR_TO_STR (pIngTnlIp, u4IngressId);
    if (pTeTnlInfo->u4TnlLsrIdMapInfo & TE_TNL_INGRESSID_MAP_INFO)
    {
        CliPrintf (CliHandle, "\r   Tunnel source : %d\n", u4IngressId);
    }
    else
    {
        CliPrintf (CliHandle, "\r   Tunnel source : %s\n", pIngTnlIp);
    }

    /*Path computation from gmplsTunnelTable is displayed */
    if ((pTeTnlInfo->u1TnlRole == TE_INGRESS) &&
        (pTeTnlInfo->u1TnlOwner == TE_TNL_OWNER_SNMP))
    {
        b1ConfigFlag = TRUE;
        if (pTeTnlInfo->GmplsTnlInfo.PathComp == GMPLS_PATH_COMP_EXPLICIT)
        {
            CliPrintf (CliHandle, "\r   Path Computation : Explicit\n");
        }
        else if (pTeTnlInfo->GmplsTnlInfo.PathComp ==
                 GMPLS_PATH_COMP_DYNAMIC_PARTIAL)
        {
            CliPrintf (CliHandle, "\r   Path Computation : Dynamic Partial\n");
        }
        else
        {
            CliPrintf (CliHandle, "\r   Path Computation : Dynamic\n");
        }
    }
    CliPrintf (CliHandle, "\r   Reoptimization Status:");
    if (TE_TNL_REOPTIMIZE_STATUS (pTeTnlInfo) == TE_REOPTIMIZE_ENABLE)
    {
        CliPrintf (CliHandle, " enabled\n");
    }
    else
    {
        CliPrintf (CliHandle, " disabled\n");
    }

    /* Admin Status Flags information is displayed */

    if (pTeTnlInfo->GmplsTnlInfo.u4AdminStatus != GMPLS_ADMIN_UNKNOWN)
    {
        CliPrintf (CliHandle, "\r   Admin Status Flags : ");

        if ((pTeTnlInfo->GmplsTnlInfo.u4AdminStatus &
             GMPLS_ADMIN_DELETE_IN_PROGRESS) == GMPLS_ADMIN_DELETE_IN_PROGRESS)
        {
            CliPrintf (CliHandle, " Delete In Progress ");
        }
        if ((pTeTnlInfo->GmplsTnlInfo.u4AdminStatus &
             GMPLS_ADMIN_ADMIN_DOWN) == GMPLS_ADMIN_ADMIN_DOWN)
        {
            CliPrintf (CliHandle, " Admin Down ");
        }
        if ((pTeTnlInfo->GmplsTnlInfo.u4AdminStatus &
             GMPLS_ADMIN_REFLECT) == GMPLS_ADMIN_REFLECT)
        {
            CliPrintf (CliHandle, " Reflect ");
        }
        if ((pTeTnlInfo->GmplsTnlInfo.u4AdminStatus &
             GMPLS_ADMIN_TESTING) == GMPLS_ADMIN_TESTING)
        {
            CliPrintf (CliHandle, " Testing ");
        }

        CliPrintf (CliHandle, "\n");
    }

    if ((TeCheckTrfcParamInTrfcParamTable (TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo),
                                           &pTeTrfcParams) == TE_SUCCESS) &&
        (b1ConfigFlag == TRUE))
    {
        /* Get the required attribute values for Te-tunnel information
         * or Attribute list information if exists */
        TeGetAttrFromTnlOrAttrList (&TeGetAttributes, pTeTnlInfo);
        if (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_RSVP)
        {
            CliPrintf (CliHandle,
                       "\r   Config Parameters:\n\r     Bandwidth: %u "
                       "kbps (Global) Priority: %d %d Affinity: %x %x %x\n",
                       TeGetAttributes.u4Bandwidth,
                       TeGetAttributes.u1SetupPriority,
                       TeGetAttributes.u1HoldingPriority,
                       TeGetAttributes.u4IncludeAnyAffinity,
                       TeGetAttributes.u4ExcludeAnyAffinity,
                       TeGetAttributes.u4IncludeAllAffinity);
            if (pTeTnlInfo->pMplsDiffServTnlInfo != NULL)
            {
                switch (pTeTnlInfo->pMplsDiffServTnlInfo->u1ServiceType)
                {
                    case MPLS_TE_GEN_LSP:
                        CliPrintf (CliHandle, "\r   Lsp-type: non-diff\r\n");
                        break;
                    case MPLS_TE_DIFF_SERV_LLSP:
                        CliPrintf (CliHandle, "\r   Lsp-type: l-lsp\r\n");
                        CliPrintf (CliHandle, "\r   Class-type : %d \r\n",
                                   TeGetAttributes.u4ClassType);
                        switch (pTeTnlInfo->pMplsDiffServTnlInfo->u1LlspPscDscp)
                        {
                            case MPLS_DIFFSERV_DF_DSCP:
                                CliPrintf (CliHandle, "\r   psc : df \r\n");
                                break;
                            case MPLS_DIFFSERV_CS1_DSCP:
                                CliPrintf (CliHandle, "\r   psc : cs1 \r\n");
                                break;
                            case MPLS_DIFFSERV_AF1_PSC_DSCP:
                                CliPrintf (CliHandle, "\r   psc : af1 \r\n");
                                break;
                            case MPLS_DIFFSERV_CS2_DSCP:
                                CliPrintf (CliHandle, "\r   psc : cs2 \r\n");
                                break;
                            case MPLS_DIFFSERV_AF2_PSC_DSCP:
                                CliPrintf (CliHandle, "\r   psc : af2 \r\n");
                                break;
                            case MPLS_DIFFSERV_CS3_DSCP:
                                CliPrintf (CliHandle, "\r   psc : cs3 \r\n");
                                break;
                            case MPLS_DIFFSERV_AF3_PSC_DSCP:
                                CliPrintf (CliHandle, "\r   psc : af3 \r\n");
                                break;
                            case MPLS_DIFFSERV_CS4_DSCP:
                                CliPrintf (CliHandle, "\r   psc : cs4 \r\n");
                                break;
                            case MPLS_DIFFSERV_AF4_PSC_DSCP:
                                CliPrintf (CliHandle, "\r   psc : af4 \r\n");
                                break;
                            case MPLS_DIFFSERV_CS5_DSCP:
                                CliPrintf (CliHandle, "\r   psc : cs5 \r\n");
                                break;
                            case MPLS_DIFFSERV_EF_DSCP:
                                CliPrintf (CliHandle, "\r   psc : ef \r\n");
                                break;
                            case MPLS_DIFFSERV_CS6_DSCP:
                                CliPrintf (CliHandle, "\r   psc : cs6 \r\n");
                                break;
                            case MPLS_DIFFSERV_CS7_DSCP:
                                CliPrintf (CliHandle, "\r   psc : cs7 \r\n");
                                break;
                            default:
                                TE_DBG (TE_MAIN_ETEXT,
                                        "MAIN : Wrong Value of LlspPscDscp \n");
                                break;
                        }
                        break;
                    case MPLS_TE_DIFF_SERV_ELSP:
                        CliPrintf (CliHandle, "\r   Lsp-type: e-lsp\r\n");
                        CliPrintf (CliHandle, "\r   Class-type : %d \r\n",
                                   TeGetAttributes.u4ClassType);
                        CliPrintf (CliHandle, "\r   Exp-map index : %d \r\n",
                                   pTeTnlInfo->pMplsDiffServTnlInfo->
                                   u4ElspListIndex);
                        switch (pTeTnlInfo->pMplsDiffServTnlInfo->u1ElspType)
                        {
                            case MPLS_DIFFSERV_PRECONF_ELSP:
                                CliPrintf (CliHandle,
                                           "\r   Map use : pre-conf \r\n");
                                break;
                            case MPLS_DIFFSERV_SIG_ELSP:
                                CliPrintf (CliHandle,
                                           "\r   Map use : signalled \r\n");
                                break;
                            default:
                                TE_DBG (TE_MAIN_ETEXT,
                                        "MAIN : Wrong Value of ElspType \n");
                                break;
                        }
                        break;
                    default:
                        TE_DBG (TE_MAIN_ETEXT,
                                "MAIN : Wrong Value of ServiceType \n");
                        break;
                }
            }
        }
        else if (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_LDP)
        {
            if (TE_TNL_CTPARAM_PTR (pTeTnlInfo) != NULL)
            {
                CliPrintf (CliHandle,
                           "\r   Config Parameters:\n\r     Bandwidth: %u "
                           "kbps (Global) Priority: %d %d Affinity: %x %x %x\n",
                           TeGetAttributes.u4Bandwidth,
                           TeGetAttributes.u1SetupPriority,
                           TeGetAttributes.u1HoldingPriority,
                           TeGetAttributes.u4IncludeAnyAffinity,
                           TeGetAttributes.u4ExcludeAnyAffinity,
                           TeGetAttributes.u4IncludeAllAffinity);
            }
        }
        else if (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_NONE)
        {
            CliPrintf (CliHandle,
                       "\r   Config Parameters:\n\r     Bandwidth: %u "
                       "kbps (Global) Priority: %d %d Affinity: %x %x %x\n",
                       TeGetAttributes.u4Bandwidth,
                       TeGetAttributes.u1SetupPriority,
                       TeGetAttributes.u1HoldingPriority,
                       TeGetAttributes.u4IncludeAnyAffinity,
                       TeGetAttributes.u4ExcludeAnyAffinity,
                       TeGetAttributes.u4IncludeAllAffinity);
            if (pTeTnlInfo->pMplsDiffServTnlInfo != NULL)
            {
                switch (pTeTnlInfo->pMplsDiffServTnlInfo->u1ServiceType)
                {
                    case MPLS_TE_GEN_LSP:
                        CliPrintf (CliHandle, "\r   Lsp-type: non-diff\r\n");
                        break;
                    case MPLS_TE_DIFF_SERV_LLSP:
                        CliPrintf (CliHandle, "\r   Lsp-type: l-lsp\r\n");
                        CliPrintf (CliHandle, "\r   Class-type : %d \r\n",
                                   TeGetAttributes.u4ClassType);
                        switch (pTeTnlInfo->pMplsDiffServTnlInfo->u1LlspPscDscp)
                        {
                            case MPLS_DIFFSERV_DF_DSCP:
                                CliPrintf (CliHandle, "\r   psc : df \r\n");
                                break;
                            case MPLS_DIFFSERV_CS1_DSCP:
                                CliPrintf (CliHandle, "\r   psc : cs1 \r\n");
                                break;
                            case MPLS_DIFFSERV_AF1_PSC_DSCP:
                                CliPrintf (CliHandle, "\r   psc : af1 \r\n");
                                break;
                            case MPLS_DIFFSERV_CS2_DSCP:
                                CliPrintf (CliHandle, "\r   psc : cs2 \r\n");
                                break;
                            case MPLS_DIFFSERV_AF2_PSC_DSCP:
                                CliPrintf (CliHandle, "\r   psc : af2 \r\n");
                                break;
                            case MPLS_DIFFSERV_CS3_DSCP:
                                CliPrintf (CliHandle, "\r   psc : cs3 \r\n");
                                break;
                            case MPLS_DIFFSERV_AF3_PSC_DSCP:
                                CliPrintf (CliHandle, "\r   psc : af3 \r\n");
                                break;
                            case MPLS_DIFFSERV_CS4_DSCP:
                                CliPrintf (CliHandle, "\r   psc : cs4 \r\n");
                                break;
                            case MPLS_DIFFSERV_AF4_PSC_DSCP:
                                CliPrintf (CliHandle, "\r   psc : af4 \r\n");
                                break;
                            case MPLS_DIFFSERV_CS5_DSCP:
                                CliPrintf (CliHandle, "\r   psc : cs5 \r\n");
                                break;
                            case MPLS_DIFFSERV_EF_DSCP:
                                CliPrintf (CliHandle, "\r   psc : ef \r\n");
                                break;
                            case MPLS_DIFFSERV_CS6_DSCP:
                                CliPrintf (CliHandle, "\r   psc : cs6 \r\n");
                                break;
                            case MPLS_DIFFSERV_CS7_DSCP:
                                CliPrintf (CliHandle, "\r   psc : cs7 \r\n");
                                break;
                            default:
                                TE_DBG (TE_MAIN_ETEXT,
                                        "MAIN : Wrong Value of LlspPscDscp \n");
                                break;
                        }
                        break;
                    case MPLS_TE_DIFF_SERV_ELSP:
                        CliPrintf (CliHandle, "\r   Lsp-type: e-lsp\r\n");
                        CliPrintf (CliHandle, "\r   Class-type : %d \r\n",
                                   TeGetAttributes.u4ClassType);
                        CliPrintf (CliHandle, "\r   Exp-map index : %d \r\n",
                                   pTeTnlInfo->pMplsDiffServTnlInfo->
                                   u4ElspListIndex);
                        switch (pTeTnlInfo->pMplsDiffServTnlInfo->u1ElspType)
                        {
                            case MPLS_DIFFSERV_PRECONF_ELSP:
                                CliPrintf (CliHandle,
                                           "\r   Map use : pre-conf \r\n");
                                break;
                            case MPLS_DIFFSERV_SIG_ELSP:
                                CliPrintf (CliHandle,
                                           "\r   Map use : signalled \r\n");
                                break;
                            default:
                                TE_DBG (TE_MAIN_ETEXT,
                                        "MAIN : Wrong Value of ElspType \n");
                                break;
                        }
                        break;
                    default:
                        TE_DBG (TE_MAIN_ETEXT,
                                "MAIN : Wrong Value of ServiceType \n");
                        break;
                }
            }
        }

        /*SRLG Parameters */
        if (pTeTnlInfo->u1TnlSrlgType == TNL_SRLG_INCLUDE_ANY)
        {

            CliPrintf (CliHandle,
                       "\r   SRLG Parameters:\n\r     SRLG TYPE:  INCLUDE_ANY\n");
        }
        else if (pTeTnlInfo->u1TnlSrlgType == TNL_SRLG_INCLUDE_ALL)
        {

            CliPrintf (CliHandle,
                       "\r   SRLG Parameters:\n\r     SRLG TYPE:  INCLUDE_ALL\n");
        }
        else

            CliPrintf (CliHandle,
                       "\r   SRLG Parameters:\n\r     SRLG TYPE:  EXCLUDE\n");

        /*Print All SRLG associated with tunnel */
        if (pTeTnlInfo->u4TnlInstance == TE_ZERO)
        {
            TMO_SLL_Scan (&pTeTnlInfo->SrlgList, pTnlSrlg, tTeTnlSrlg *)
            {
                CliPrintf (CliHandle,
                           "\r     SRLG Value: %d\n", pTnlSrlg->u4SrlgNo);
            }
        }

        /*End */

        MPLS_CMN_UNLOCK ();
        TeCliErHopShow (CliHandle, pTeTnlInfo, b1ConfigFlag);
        MPLS_CMN_LOCK ();
    }

    /*Check whetehr gmpls tunnel.If yes,then print gmpls parameters in show output */
    if ((pTeTnlInfo->GmplsTnlInfo).u1EncodingType != GMPLS_TUNNEL_LSP_NOT_GMPLS)
    {
        TePrintGmplsTunnelInfo (CliHandle, pTeTnlInfo);
    }

    if (pTeTnlInfo->u4TnlInstance == TE_ZERO)
    {
        TeCliShowDynInfoForNonZeroInst (CliHandle, pTeTnlInfo);
    }
    else
    {
        TeCliShowTnlDynamicInfo (CliHandle, pTeTnlInfo);
    }

    OsixGetSysTime (&u4CurrentTime);
    CliMplsConvertTimetoString (u4CurrentTime -
                                TE_TNL_CREATE_TIME (pTeTnlInfo),
                                ai1TnlCreateTime);
    OsixGetSysTime (&u4CurrentTime);
    CliMplsConvertTimetoString (u4CurrentTime -
                                TE_TNL_PRIMARY_INSTANCE_UP_TIME (pTeTnlInfo),
                                ai1TnlUpTime);
    CliMplsConvertTimetoString (TE_TNL_LAST_PATH_CHG_TIME (pTeTnlInfo),
                                ai1TnlLastChange);
    if (TE_TNL_REOPTIMIZE_STATUS (pTeTnlInfo) == TE_REOPTIMIZE_ENABLE)
    {
        TeRpteGetReoptTime (pTeTnlInfo, &u4CurrentTime);

        CliMplsConvertTimetoString (u4CurrentTime, ai1TnlReoptimizeTime);
    }

    if (!((TE_TNL_ROLE (pTeTnlInfo) == TE_EGRESS) &&
          (pTeTnlInfo->u4TnlMode == TE_TNL_MODE_UNIDIRECTIONAL)))
    {
        MPLS_CMN_UNLOCK ();
        TeCliShowTnlCounters (CliHandle, u4IngressId, u4EgressId,
                              u4TnlIndex, u4TnlInstance);
        MPLS_CMN_LOCK ();
    }

    CliPrintf (CliHandle, "\r   History:\n     Tunnel:\n");
    CliPrintf (CliHandle, "\r       Time since created     : %-10s\n",
               ai1TnlCreateTime);
    CliPrintf (CliHandle, "\r       Time since up          : %-10s\n",
               ai1TnlUpTime);
    CliPrintf (CliHandle, "\r       Time since path change : %-10s\n",
               ai1TnlLastChange);

    if (TE_TNL_REOPTIMIZE_STATUS (pTeTnlInfo) == TE_REOPTIMIZE_ENABLE)
    {
        CliPrintf (CliHandle, "\r       Reoptimize Time        : %-10s\n",
                   ai1TnlReoptimizeTime);
    }

    if (TE_TNL_ROLE (pTeTnlInfo) == TE_INGRESS)
    {
        pActiveTnlInfo = TeGetActiveTnlInfo (u4TnlIndex, u4TnlInstance,
                                             u4IngressId, u4EgressId);

        if (pActiveTnlInfo != NULL)
        {
            u4UsedInstance = pActiveTnlInfo->u4TnlInstance;
        }
    }
    else if (TE_TNL_ROLE (pTeTnlInfo) == TE_INTERMEDIATE)
    {
        u4UsedInstance = pTeTnlInfo->u4TnlPrimaryInstance;
    }
    else
    {
        if ((pTeTnlInfo->GmplsTnlInfo.i4E2EProtectionType ==
             MPLS_TE_DEDICATED_ONE2ONE) &&
            (pTeTnlInfo->u1TnlLocalProtectInUse == LOCAL_PROT_IN_USE))
        {
            u4UsedInstance = pTeTnlInfo->u4BkpTnlInstance;
        }
        else
        {
            u4UsedInstance = pTeTnlInfo->u4TnlPrimaryInstance;
        }
    }

    CliPrintf (CliHandle, "  Active Tunnel Instance : "
               "%-10d\n", u4UsedInstance);

    return CLI_SUCCESS;

}

/******************************************************************************
 * Function Name : TeCliTunnelCreate 
 * Description   : This routine is used to create a tunnel 
 * Input(s)      : CliHandle        - Cli Context Handle
                   pTeCliArgs       - Pointer to CLI Args containing required 
                                      parameters of tunnel                                      
                   bValidateLsrId   - LSR ID validation. To be skipped for 
                                      P2MP tunnel
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ******************************************************************************/
INT1
TeCliTunnelCreate (tCliHandle CliHandle, tTeCliArgs * pTeCliArgs,
                   BOOL1 bValidateLsrId)
{
    INT4                i4RowStatus = 0;
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    BOOL1               bModeChangeReqd = FALSE;

    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);

    if (OSIX_TRUE == bValidateLsrId)
    {
        if (TeValidateAndGetIngressId (CliHandle, pTeCliArgs->u4EgressId,
                                       pTeCliArgs->u4EgressLocalMapNum,
                                       &pTeCliArgs->u4IngressId,
                                       &pTeCliArgs->u4IngressLocalMapNum))
        {
            return CLI_FAILURE;
        }
    }

    if (pTeCliArgs->u4EgressLocalMapNum != 0)
    {
        pTeCliArgs->u4EgressId = pTeCliArgs->u4EgressLocalMapNum;
    }
    if (pTeCliArgs->u4IngressLocalMapNum != 0)
    {
        pTeCliArgs->u4IngressId = pTeCliArgs->u4IngressLocalMapNum;
    }

    /*   Get the TunnelId,TunnelInstance,Ingress and Egress using the TunnelName */

    if (nmhGetMplsTunnelRowStatus (pTeCliArgs->u4TunnelIndex,
                                   pTeCliArgs->u4TunnelInstance,
                                   pTeCliArgs->u4IngressId,
                                   pTeCliArgs->u4EgressId,
                                   &i4RowStatus) == SNMP_FAILURE)
    {
        TE_DBG (TE_EXTN_FAIL, "\r\n%%Failed to get tunnel information\r\n");
    }

    if (i4RowStatus != TE_ZERO)
    {
        if (pTeCliArgs->u4TunnelInstance != TE_ZERO)
        {
            bModeChangeReqd = TRUE;
        }
        else
        {
            /* If Tunnel is already present , return failure */
            CliPrintf (CliHandle, "\r\n%%Tunnel is already present\r\n");
            return CLI_FAILURE;
        }
    }
    else if (TeCliCreateTunnel (CliHandle, pTeCliArgs) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
    else if (pTeCliArgs->u4TunnelInstance != TE_ZERO)
    {
        bModeChangeReqd = TRUE;
    }

    if (bModeChangeReqd == TRUE)
    {
        /* ENTER MPLS TUNNEL LSP Mode */
        SNPRINTF ((CHR1 *) au1Cmd, sizeof (au1Cmd), "%s%u",
                  MPLS_TUNNEL_LSP_MODE, pTeCliArgs->u4TunnelIndex);

        if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Unable to enter into Mpls Tunnel LSP mode\r\n");
            return CLI_FAILURE;
        }
    }

    gaTnlCliArgs[CliHandle].u4TnlIndex = pTeCliArgs->u4TunnelIndex;
    gaTnlCliArgs[CliHandle].u4TnlInstance = pTeCliArgs->u4TunnelInstance;
    gaTnlCliArgs[CliHandle].u4IngressId = pTeCliArgs->u4IngressId;
    gaTnlCliArgs[CliHandle].u4EgressId = pTeCliArgs->u4EgressId;

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : TeValidateAndGetIngressId 
 * Description   : This routine is used to validate the egress and ingress 
 *                 LSR Ids.
 * Input(s)      : CliHandle        - Cli Context Handle
 *                 u4EgressId       - Identity of the egress LSR associated
                                      with the tunnel instance.
                   u4EgressLocalMapNum - Egress local map number for global-id 
                                          and node-id combination                   
                   u4IngressId       - Identity of the ingress LSR associated
                                      with the tunnel instance.
                   u4IngressLocalMapNum - Ingress local map number for global-id 
                                          and node-id combination                   
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ******************************************************************************/
INT1
TeValidateAndGetIngressId (tCliHandle CliHandle, UINT4 u4EgressId,
                           UINT4 u4EgressLocalMapNum,
                           UINT4 *pu4IngressId, UINT4 *pu4IngressLocalMapNum)
{
    UINT4               u4ContextId = MPLS_DEF_CONTEXT_ID;
    UINT4               u4GlobalId = 0;
    UINT4               u4NodeId = 0;

    if (((*pu4IngressId != 0) && (u4EgressLocalMapNum != 0)) ||
        ((u4EgressId != 0) && (*pu4IngressLocalMapNum != 0)))
    {
        CliPrintf (CliHandle, "\r%%Both Egress and Ingress identifiers "
                   "should be in the same format\n");
        return CLI_FAILURE;
    }

    if ((*pu4IngressId == 0) && (*pu4IngressLocalMapNum == 0) &&
        (u4EgressId != 0))
    {
        /* Ingress address of the Tunnel is not given, 
         * So, fetch from Egress address */
        if ((NetIpv4GetSrcAddrToUseForDest (u4EgressId, pu4IngressId)) ==
            NETIPV4_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Egress Address is not reachable\n");
            return CLI_FAILURE;
        }
    }
    else if ((*pu4IngressId == 0) && (*pu4IngressLocalMapNum == 0) &&
             (u4EgressLocalMapNum != 0))
    {
        /* Fetch the source global-id and node-id 
         * local map number from node map table */
        if (TeCliGetLocalMapNum (u4ContextId, pu4IngressLocalMapNum)
            == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Global Or Node Identifier is not "
                       "configured\n");
            return CLI_FAILURE;
        }
    }
    if (((u4EgressId != 0) && (u4EgressId == *pu4IngressId)) ||
        ((u4EgressLocalMapNum != 0) &&
         (u4EgressLocalMapNum == *pu4IngressLocalMapNum)))
    {
        CliPrintf (CliHandle, "\r%%Both Egress and Ingress are same\n");
        return CLI_FAILURE;
    }
    if ((*pu4IngressLocalMapNum != 0) || (u4EgressLocalMapNum != 0))
    {
        if (OamUtilGetGlobalIdNodeId (u4ContextId, *pu4IngressLocalMapNum,
                                      &u4GlobalId, &u4NodeId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%%Configure the local map number "
                       "in the node map table\n");
            return CLI_FAILURE;
        }
        if (OamUtilGetGlobalIdNodeId (u4ContextId, u4EgressLocalMapNum,
                                      &u4GlobalId, &u4NodeId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%%Configure the local map number "
                       "in the node map table\n");
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliTunnelInterfaceCreate 
* Description   : This routine is used to change the prompt. 
* Input(s)      : CliHandle        - Cli Context Handle
                  u4TunnelIndex    - Uniquely identifies a set of tunnel instances. 
                  u4MplsTnlIfIndex - Tunnel Interface Index
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT1
TeCliTunnelInterfaceCreate (tCliHandle CliHandle, UINT4 u4MplsTunnelId,
                            UINT4 u4MplsTnlIfIndex)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT1               au1Cmd[MAX_PROMPT_LEN];
    UINT4               u4TunnelInstance = 0;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;

    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);

    if (u4MplsTnlIfIndex == MPLS_ZERO)
    {
        if (CfaGetFreeInterfaceIndex (&u4MplsTnlIfIndex, CFA_MPLS_TUNNEL) ==
            OSIX_FAILURE)
        {
            CliPrintf (CliHandle,
                       "%% Free Interface index is not available\r\n");
            return CLI_FAILURE;
        }
        if (MplsCreateMplsTnlIf (u4MplsTunnelId,
                                 u4MplsTnlIfIndex) == MPLS_FAILURE)
        {
            CliPrintf (CliHandle,
                       "%% Unable to create MPLS tunnel interface\r\n");
            return CLI_FAILURE;
        }
    }

    /* ENTER MPLS TUNNEL  Mode */
    SNPRINTF ((CHR1 *) au1Cmd, sizeof (au1Cmd), "%s%u", MPLS_TUNNEL_MODE,
              u4MplsTunnelId);
    if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to enter into Mpls Tunnel configuration mode\r\n");
        return CLI_FAILURE;
    }
    gaTnlCliArgs[CliHandle].u4TnlIndex = 0;
    gaTnlCliArgs[CliHandle].u4TnlInstance = 0;
    gaTnlCliArgs[CliHandle].u4IngressId = 0;
    gaTnlCliArgs[CliHandle].u4EgressId = 0;
    /* Store the MplsTnlIfIndex in the gaTnlCliArgs for later use */
    gaTnlCliArgs[CliHandle].u4MplsTnlIfIndex = u4MplsTnlIfIndex;

    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTnlInfoByOwnerAndRole (u4MplsTunnelId, u4TunnelInstance,
                                             u4IngressId, u4EgressId,
                                             TE_TNL_OWNER_SNMP, TE_ZERO);

    if (pTeTnlInfo != NULL)
    {

        gaTnlCliArgs[CliHandle].u4TnlIndex = pTeTnlInfo->u4TnlIndex;
        gaTnlCliArgs[CliHandle].u4TnlInstance = pTeTnlInfo->u4TnlInstance;

        CONVERT_TO_INTEGER (pTeTnlInfo->TnlIngressLsrId, u4IngressId);
        gaTnlCliArgs[CliHandle].u4IngressId = OSIX_NTOHL (u4IngressId);

        CONVERT_TO_INTEGER (pTeTnlInfo->TnlEgressLsrId, u4EgressId);
        gaTnlCliArgs[CliHandle].u4EgressId = OSIX_NTOHL (u4EgressId);
    }
    MPLS_CMN_UNLOCK ();

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : TeCliTunnelInterfaceDelete 
 * Description   : This routine is used to delete the tunnel corresponding to
 *                 the MplsTunnelId. 
 * Input(s)      : CliHandle      - Cli Context Handle
                   u4TunnelIndex  - Uniquely identifies a set of tunnel
                                    instances. 
                   u4MplsTnlIfIndex - Tunnel Interface Index
                   u4AdminStatus  - AdminStatus Flag
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 *****************************************************************************/
INT1
TeCliTunnelInterfaceDelete (tCliHandle CliHandle, UINT4 u4MplsTunnelId,
                            UINT4 u4MplsTnlIfIndex, UINT4 u4AdminStatus)
{
    UINT4               u4TunnelIndex = u4MplsTunnelId;
    UINT4               u4TunnelInstance = 0;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;
    INT4                i4TnlOwner = 0;
    INT4                i4RetVal = CLI_SUCCESS;
    UINT4               u4OrgTnlIndex = TE_ZERO;
    UINT4               u4OrgIngressId = TE_ZERO;
    UINT4               u4OrgEgressId = TE_ZERO;
    UINT4               u4ErrorCode = TE_ZERO;
    BOOL1               bInstance0Present = FALSE;
    BOOL1               bTnlEntryNotFound = TRUE;
    tTeCliArgs          TeCliArgs;

    MEMSET (&TeCliArgs, TE_ZERO, sizeof (tTeCliArgs));

    TeCliArgs.bTnlIfIndexFound = TRUE;

    TeCliSetIfMainStorageType ((INT4) u4MplsTnlIfIndex,
                               (INT4) TE_STORAGE_VOLATILE);

    /* Scan and delete all instances of the tunnel that matches the same
     * tunnel index. */
    while (nmhGetNextIndexMplsTunnelTable (u4TunnelIndex, &u4TunnelIndex,
                                           u4TunnelInstance, &u4TunnelInstance,
                                           u4IngressId, &u4IngressId,
                                           u4EgressId, &u4EgressId)
           == SNMP_SUCCESS)
    {
        bTnlEntryNotFound = FALSE;
        if (u4MplsTunnelId != u4TunnelIndex)
        {
            break;
        }

        if (u4TunnelInstance == TE_ZERO)
        {
            u4OrgTnlIndex = u4TunnelIndex;
            u4OrgIngressId = u4IngressId;
            u4OrgEgressId = u4EgressId;

            /* Delete Instance 0 tunnel at the end */
            bInstance0Present = TRUE;
            continue;
        }

        i4TnlOwner = TE_ZERO;
        nmhGetMplsTunnelOwner (u4TunnelIndex, u4TunnelInstance,
                               u4IngressId, u4EgressId, &i4TnlOwner);

        if (i4TnlOwner != TE_TNL_OWNER_SNMP)
        {
            continue;
        }

        TeCliArgs.u4TunnelIndex = u4TunnelIndex;
        TeCliArgs.u4TunnelInstance = u4TunnelInstance;
        TeCliArgs.u4IngressId = u4IngressId;
        TeCliArgs.u4EgressId = u4EgressId;
        TeCliArgs.u4AdminStatus = u4AdminStatus;
        TeCliArgs.i4TnlIfIndex = TE_ZERO;

        i4RetVal =
            TeCliDeleteP2mpOrNormalTunnel (CliHandle, &TeCliArgs, OSIX_TRUE);
    }

    if ((bInstance0Present == TRUE) && (i4RetVal != CLI_FAILURE))
    {
        TeCliArgs.u4TunnelIndex = u4OrgTnlIndex;
        TeCliArgs.u4TunnelInstance = TE_ZERO;
        TeCliArgs.u4IngressId = u4OrgIngressId;
        TeCliArgs.u4EgressId = u4OrgEgressId;
        TeCliArgs.u4AdminStatus = u4AdminStatus;
        TeCliArgs.i4TnlIfIndex = TE_ZERO;

        i4RetVal = TeCliDeleteP2mpOrNormalTunnel (CliHandle, &TeCliArgs,
                                                  OSIX_TRUE);
        if (i4RetVal == CLI_FAILURE)
        {
            TeCliSetIfMainStorageType ((INT4) u4MplsTnlIfIndex,
                                       (INT4) TE_STORAGE_NONVOLATILE);
        }
    }

    if ((bTnlEntryNotFound == TRUE) || (TeCliArgs.bTnlIfIndexFound == FALSE) ||
        (bInstance0Present == FALSE))
    {
        if (i4RetVal != CLI_FAILURE)
        {
            if ((nmhTestv2IfMainRowStatus (&u4ErrorCode,
                                           (INT4) u4MplsTnlIfIndex,
                                           MPLS_STATUS_DESTROY)) ==
                SNMP_FAILURE)
            {
                if (u4ErrorCode == SNMP_ERR_NO_CREATION)
                {
                    CliPrintf (CliHandle, "\r\n%% Tunnel Entry not found\n");
                }
                else
                {
                    TE_DBG1 (TE_EXTN_FAIL, "\r\n%%Unable to Destroy Tunnel "
                             "u4MplsTnlIfIndex: %u\r\n", u4MplsTnlIfIndex);
                    CliPrintf (CliHandle, "\r\n%% Unable to Destroy Tunnel\n");
                }
                TeCliSetIfMainStorageType ((INT4) u4MplsTnlIfIndex,
                                           (INT4) TE_STORAGE_NONVOLATILE);
                return CLI_FAILURE;

            }

            TeCliSetIfMainRowStatus ((INT4) u4MplsTnlIfIndex,
                                     (INT4) MPLS_STATUS_DESTROY);
        }
    }

    return (INT1) i4RetVal;
}

/******************************************************************************
 * Function Name : TeCliTunnelDelete
 * Description   : This routine is used to delete the tunnel corresponding to
 *                 the MplsTunnelId. 
 * Input(s)      : CliHandle      - Cli Context Handle
 *                 pTeCliArgs       - Pointer to TE Cli Args containing
 *                                    required parameters for tunnel
 *                 bValidateLsrId   - LSR ID validation. To be skipped for 
 *                                    P2MP tunnel
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 *****************************************************************************/
INT4
TeCliTunnelDelete (tCliHandle CliHandle, tTeCliArgs * pTeCliArgs,
                   BOOL1 bValidateLsrId)
{
    INT4                i4TnlOwner = 0;
    INT4                i4RetVal = CLI_SUCCESS;
    UINT4               u4TnlIndex = pTeCliArgs->u4TunnelIndex;
    UINT4               u4IngressId = pTeCliArgs->u4IngressId;
    UINT4               u4EgressId = pTeCliArgs->u4EgressId;
    UINT4               u4TnlInstance = pTeCliArgs->u4TunnelInstance;
    BOOL1               bInstance0Present = FALSE;
    INT4                i4RowStatus = TE_ZERO;

    if (pTeCliArgs->u4TunnelInstance != TE_ZERO)
    {
        return (TeCliDeleteP2mpOrNormalTunnel (CliHandle, pTeCliArgs,
                                               bValidateLsrId));
    }
    else
    {
        if (nmhGetMplsTunnelRowStatus (u4TnlIndex, u4TnlInstance, u4IngressId,
                                       u4EgressId,
                                       &i4RowStatus) == SNMP_FAILURE)
        {
            TE_DBG (TE_EXTN_FAIL, "\r\n%%Failed to get tunnel information\r\n");
        }

        if (i4RowStatus != TE_ZERO)
        {
            bInstance0Present = TRUE;
        }
        else
        {
            CliPrintf (CliHandle, "\r\n%% Tunnel not found\n");
            return CLI_FAILURE;
        }
    }

    /* Scan and delete all instances of the tunnel that matches the same
     * tunnel index. */
    while (nmhGetNextIndexMplsTunnelTable (u4TnlIndex, &u4TnlIndex,
                                           u4TnlInstance, &u4TnlInstance,
                                           u4IngressId, &u4IngressId,
                                           u4EgressId, &u4EgressId)
           == SNMP_SUCCESS)
    {
        if ((pTeCliArgs->u4TunnelIndex != u4TnlIndex) ||
            (pTeCliArgs->u4IngressId != u4IngressId) ||
            (pTeCliArgs->u4EgressId != u4EgressId))
        {
            break;
        }

        i4TnlOwner = TE_ZERO;
        nmhGetMplsTunnelOwner (u4TnlIndex, u4TnlInstance, u4IngressId,
                               u4EgressId, &i4TnlOwner);

        if (i4TnlOwner != TE_TNL_OWNER_SNMP)
        {
            continue;
        }

        pTeCliArgs->u4TunnelInstance = u4TnlInstance;
        TeCliDeleteP2mpOrNormalTunnel (CliHandle, pTeCliArgs, bValidateLsrId);
    }

    if (bInstance0Present == TRUE)
    {
        pTeCliArgs->u4TunnelInstance = TE_ZERO;
        i4RetVal = TeCliDeleteP2mpOrNormalTunnel (CliHandle, pTeCliArgs,
                                                  bValidateLsrId);
    }

    return i4RetVal;
}

/******************************************************************************
* Function Name : TeCliTunnelTrafficEng 
* Description   : This routine is not used  at present.
* Input(s)      : CliHandle     - Cli Context Handle
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT1
TeCliTunnelTrafficEng (tCliHandle CliHandle)
{
    UNUSED_PARAM (CliHandle);
    return (CLI_SUCCESS);
}

/******************************************************************************
* Function Name : TeCliTunnelResourceCreate 
* Description   : This routine is used to create a Tunnel Resource Table. 
* Input(s)      : CliHandle     - Cli Context Handle
                  u4BandWidth   - The maximum rate in bits/second. 
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT1
TeCliTunnelResourceCreate (tCliHandle CliHandle, UINT4 u4BandWidth)
{
    UINT4               u4TunnelResourceIndex = 0;
    UINT4               u4ErrorCode = 0;
    tTeTrfcParams      *pTeTrfcParams = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4TunnelIndex = 0;
    UINT4               u4TunnelInstance = 0;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;
    UINT4               u4GetTunnelResourceIndex = 0;
    UINT4               u4NoOfStackedTnls = 0;
    UINT4               u4TnlRsvpPeakDataRate = 0;
    tSNMP_OID_TYPE      ResourcePointer;
    tSNMP_OID_TYPE      GetResourcePointer;
    tSNMP_OCTET_STRING_TYPE MplsTeTnlType;
    INT4                i4RetValTunnelRowStatus;
    INT4                i4TunnelSignallingProto = 0;
    INT4                i4TnlMBBStatus = MPLS_TE_MBB_DISABLED;
    static UINT4        au4GetRsrcPointer[MPLS_TE_RSRC_TABLE_MAX_RATE_OFFSET];
    static UINT4        au4ResourceTableOid[MPLS_TE_RSRC_TABLE_MAX_RATE_OFFSET]
        = { 1, 3, 6, 1, 2, 1, 10, 166, 3, 2, 6, 1, 2 };
    UINT1               au1TnlType[CLI_MPLS_TE_TNL_TYPE];
    BOOL1               b1IsHlspTnl = MPLS_FALSE;
    UINT4               u4OldBandwidth = 0;

    GetResourcePointer.pu4_OidList = au4GetRsrcPointer;
    MEMSET (GetResourcePointer.pu4_OidList, 0, sizeof (UINT4) * 2);
    GetResourcePointer.u4_Length = 0;

    MEMSET (au1TnlType, TE_ZERO, CLI_MPLS_TE_TNL_TYPE);
    MplsTeTnlType.pu1_OctetList = au1TnlType;
    MplsTeTnlType.i4_Length = CLI_MPLS_TE_TNL_TYPE;

    if (nmhGetMplsTunnelResourceIndexNext (&u4TunnelResourceIndex) !=
        SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%%Failed to Get a Valid Resource Index\r\n");
        return CLI_FAILURE;
    }

    au4ResourceTableOid[MPLS_TE_RSRC_TABLE_MAX_RATE_OFFSET - 1] =
        u4TunnelResourceIndex;
    ResourcePointer.pu4_OidList = au4ResourceTableOid;
    ResourcePointer.u4_Length = MPLS_TE_RSRC_TABLE_MAX_RATE_OFFSET;

    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    if ((nmhGetMplsTunnelRowStatus
         (u4TunnelIndex, u4TunnelInstance, u4IngressId,
          u4EgressId, &i4RetValTunnelRowStatus)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Tunnel Information is not available\r\n");
        return CLI_FAILURE;
    }

    /* Get the signalling protocol. Reqd. to Get/Set Resource Table */
    nmhGetMplsTunnelSignallingProto (u4TunnelIndex, u4TunnelInstance,
                                     u4IngressId, u4EgressId,
                                     &i4TunnelSignallingProto);

    nmhGetFsMplsTunnelMBBStatus (u4TunnelIndex, u4TunnelInstance,
                                 u4IngressId, u4EgressId, &i4TnlMBBStatus);

    if (nmhGetFsMplsTunnelType (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                u4EgressId, &MplsTeTnlType) == SNMP_FAILURE)
    {
        /*Failure condition is not handled here */
    }

    if (MplsTeTnlType.pu1_OctetList[TE_ZERO] & TE_TNL_TYPE_HLSP)
    {
        b1IsHlspTnl = MPLS_TRUE;
    }

    TeSigGetNoOfStackedTnls (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                             u4EgressId, &u4NoOfStackedTnls);

    if ((nmhGetMplsTunnelResourcePointer
         (u4TunnelIndex, u4TunnelInstance, u4IngressId, u4EgressId,
          &GetResourcePointer)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%TunnelResourcePointer is not available\r\n");
        return CLI_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (u4TunnelIndex, u4TunnelInstance,
                                  u4IngressId, u4EgressId);
    if (pTeTnlInfo != NULL)
    {
        if ((i4TnlMBBStatus == MPLS_TE_MBB_DISABLED)
            && (TE_TNL_ADMIN_STATUS (pTeTnlInfo) == TE_ADMIN_UP))
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_CANNOT_SET_ATTRIBUTE);
            return CLI_FAILURE;
        }
    }

    u4GetTunnelResourceIndex =
        GetResourcePointer.pu4_OidList[TE_TNL_RSRC_TABLE_DEF_OFFSET - 1];

    nmhGetFsMplsTunnelRSVPResPeakDataRate (u4GetTunnelResourceIndex,
                                           &u4TnlRsvpPeakDataRate);

    /* Storing Old Bandwidth */
    u4OldBandwidth = u4TnlRsvpPeakDataRate;
    /* For HLSP (Signalled) case, Bandwidth is not allowed to change
     * if MBB is not enabled and the tunnel is used by other tunnels
     * for stacking.
     */
    if ((i4TunnelSignallingProto == TE_SIGPROTO_RSVP) &&
        (b1IsHlspTnl == MPLS_TRUE) &&
        (u4NoOfStackedTnls != TE_ZERO) &&
        (i4TnlMBBStatus != MPLS_TE_MBB_ENABLED))
    {
        CliPrintf (CliHandle, "\r\n%% Bandwidth cannot be Modified.\r\n");
        return CLI_FAILURE;
    }

    /* For HLSP (Signalled) case, Bandwidth cannot be reduced, if the tunnel 
     * is used by other tunnels for stacking, though MBB is enabled.
     */
    if ((i4TunnelSignallingProto == TE_SIGPROTO_RSVP) &&
        (b1IsHlspTnl == MPLS_TRUE) &&
        (u4NoOfStackedTnls != TE_ZERO) && (u4TnlRsvpPeakDataRate > u4BandWidth))

    {
        CliPrintf (CliHandle, "\r\n%% Bandwidth cannot be decreased\r\n");
        return CLI_FAILURE;
    }

    /* If Resource Pointer is not set earlier,check if row status is active.
     * If active,make row status to Not In Service and set it 
     * else, create a new entry
     * */

    if (i4RetValTunnelRowStatus == MPLS_STATUS_ACTIVE)
    {
        if (TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                         u4IngressId, u4EgressId,
                                         MPLS_STATUS_NOT_INSERVICE) ==
            CLI_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set Tunnel RowStatus Active\r\n");
            return CLI_FAILURE;
        }
    }

    /* If Resource is not assigned earlier, create a new resource entry.
     * Else destroy it, add the same entry and assign the new bandwidth
     * to that */

    if ((GetResourcePointer.u4_Length == 2)
        || (u4GetTunnelResourceIndex == TE_DFLT_TRFC_PRAM_INDEX))
    {
        if (nmhGetMplsTunnelResourceIndexNext (&u4TunnelResourceIndex) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Failed to Get a Valid Resource Index\r\n");
            return CLI_FAILURE;
        }
    }
    else
    {
        /* Check Whether the Tunnel Resource Index created is the Default One,
         * If Yes, Don't destroy it.
         * Else, Get the Index, Destroy the Resource Entry with this Index,
         * Create the Resource Entry with the same Index */
        /* Since, the Tunnel Resource Index is created from administrator,
         * it is destroyed */
        if ((nmhTestv2MplsTunnelResourceRowStatus
             (&u4ErrorCode, u4GetTunnelResourceIndex,
              MPLS_STATUS_DESTROY)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "%%Unable to set MplsTunnelResourceRowStatus - destroy\r\n");
            return CLI_FAILURE;
        }
        if ((nmhSetMplsTunnelResourceRowStatus (u4GetTunnelResourceIndex,
                                                MPLS_STATUS_DESTROY))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "%%Unable to set MplsTunnelResourceRowStatus - destroy\r\n");
            return CLI_FAILURE;
        }
        u4TunnelResourceIndex = u4GetTunnelResourceIndex;
    }
    /* Default Tunnel Resource Index can't be deleted,
     * So, creating a new one. */
    au4ResourceTableOid[MPLS_TE_RSRC_TABLE_MAX_RATE_OFFSET - 1] =
        u4TunnelResourceIndex;
    ResourcePointer.pu4_OidList = au4ResourceTableOid;
    ResourcePointer.u4_Length = MPLS_TE_RSRC_TABLE_MAX_RATE_OFFSET;

    /*Set the Bandwidth */
    if ((TeCliTunnelSigBandwidth (CliHandle, u4TunnelResourceIndex,
                                  i4TunnelSignallingProto, u4BandWidth))
        == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set Tunnel Bandwidth\r\n");
        return CLI_FAILURE;
    }

    /*Storing Old bandwidth */

    if (i4TnlMBBStatus == MPLS_TE_MBB_ENABLED)
    {
        /*IF MBB is enabled and Admin Status is UP then Old
           bandwidth is saved in traffic parameters. */

        if ((TeCheckTrfcParamInTrfcParamTable
             (u4TunnelResourceIndex, &pTeTrfcParams) == TE_SUCCESS)
            && (TE_RSVPTE_TRFC_PARAMS (pTeTrfcParams) != NULL))
        {
            TE_RSVPTE_TOLDPARAM_PDR (pTeTrfcParams) = u4OldBandwidth;
        }
    }

    /* Set Resource Row Status to active */
    if ((nmhTestv2MplsTunnelResourceRowStatus (&u4ErrorCode,
                                               u4TunnelResourceIndex,
                                               MPLS_STATUS_ACTIVE)) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to set MplsTunnelResourceRowStatus\r\n");
        return CLI_FAILURE;
    }

    if ((nmhSetMplsTunnelResourceRowStatus (u4TunnelResourceIndex,
                                            MPLS_STATUS_ACTIVE)) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to set MplsTunnelResourceRowStatus\r\n");
        return CLI_FAILURE;
    }
    /* Set Tunnel Resource Pointer */

    if ((nmhTestv2MplsTunnelResourcePointer
         (&u4ErrorCode, u4TunnelIndex, u4TunnelInstance, u4IngressId,
          u4EgressId, &ResourcePointer)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to set MplsTunnelResourcePointer\r\n");
        return CLI_FAILURE;
    }

    if ((nmhSetMplsTunnelResourcePointer
         (u4TunnelIndex, u4TunnelInstance, u4IngressId, u4EgressId,
          &ResourcePointer)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to set MplsTunnelResourcePointer\r\n");
        return CLI_FAILURE;
    }

    if (i4RetValTunnelRowStatus == MPLS_STATUS_ACTIVE)
    {
        if (TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                         u4IngressId, u4EgressId,
                                         MPLS_STATUS_ACTIVE) == CLI_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set Tunnel RowStatus Active\r\n");
            return CLI_FAILURE;
        }

    }

    return (CLI_SUCCESS);
}

/******************************************************************************
 * Function Name : TeCliStaticBindEgress 
 * Description   : This routine is used to associate XC Entries with that of
 *                 tunnel. 
 * Input(s)      : CliHandle       - Cli Context Handle
 *               : u4InLabel       - Incoming Label associated with in-segment.
 *               : u4InIfIndex     - Interface Index for the incoming MPLS
 *                                   interface.  
 *                 u4MplsTnlDir - MPLS tunnel direction (forward/reverse)                  
 *                 u1TnlPathType   - Tunnel Path Type - Protection or Working
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 *****************************************************************************/
INT1
TeCliStaticBindEgress (tCliHandle CliHandle, UINT4 u4InLabel,
                       UINT4 u4InIfIndex, UINT4 u4MplsTnlDir,
                       UINT1 u1TnlPathType)
{
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCSegmentIndex;

    tSNMP_OID_TYPE      TunnelXCPointer;

    tSNMP_OID_TYPE      XCSegIndex;
    static UINT4        au4XCIndexx[MPLS_TE_XC_TABLE_OFFSET];

    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];

    static UINT4        au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 2, 1, 10, 1, 4, 4, 0, 0, 0, 0, 4, 0, 0, 0,
        0, 4, 0, 0, 0, 0
    };

    UINT4               u4TunnelIndex = 0;

    UINT4               u4TunnelInstance = 0;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;

    UINT4               u4XCTabIndex = 0;
    UINT4               u4InIndex = 0;
    UINT4               u4OutIndex = 0;

    UINT4               u4XCSegInd = 0;
    UINT4               u4InSegInd = 0;
    UINT4               u4OutSegInd = 0;

    INT4                i4InSegmntNPop;
    UINT4               u4ErrorCode = 0;
    UINT4               u4MplsTnlIfIndex = 0;

    INT4                i4TunnelMode = 0;
    INT4                i4Direction = 0;

    INT4                i4IfIndex = 0;
    INT4                i4TnlRole = 0;
    INT4                i4PrevRowStatus = 0;

    BOOL1               bXcDeleteFlag = TRUE;

    UINT4               u4CreatedInstance = TE_ZERO;
    BOOL1               bEntryCreated = FALSE;
    BOOL1               bTempFlag = FALSE;
    BOOL1               bIntfCreationReqd = TRUE;

    XCSegIndex.pu4_OidList = au4XCIndexx;
    XCSegIndex.u4_Length = 0;

    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    XCIndex.pu1_OctetList = au1XCIndex;
    XCIndex.i4_Length = MPLS_INDEX_LENGTH;

    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetFsMplsTunnelMode (u4TunnelIndex, u4TunnelInstance,
                                u4IngressId, u4EgressId, &i4TunnelMode)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Tunnel Mode is not available.\r\n");
        return CLI_FAILURE;
    }

    if (nmhGetMplsTunnelRole (u4TunnelIndex, u4TunnelInstance,
                              u4IngressId, u4EgressId,
                              &i4TnlRole) == SNMP_SUCCESS)
    {
        if ((i4TnlRole == TE_INGRESS) &&
            (i4TunnelMode != TE_TNL_MODE_COROUTED_BIDIRECTIONAL))
        {
            CliPrintf (CliHandle,
                       "\r\n%%Wrong label configuration for Ingress tunnel\r\n");
            return CLI_FAILURE;
        }
        else if (i4TnlRole == TE_INTERMEDIATE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Wrong label configuration for Intermediate tunnel\r\n");
            return CLI_FAILURE;
        }
    }
    MEMSET (au1InSegIndex, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1XCIndex, 0, MPLS_INDEX_LENGTH);

    if (nmhGetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                   u4IngressId, u4EgressId,
                                   &i4PrevRowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to get tunnel row status\r\n");
        return CLI_FAILURE;
    }

    if ((i4TunnelMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
        && (u4MplsTnlDir == MPLS_CLI_DIRECTION_REVERSE))
    {
        u4MplsTnlDir = MPLS_DIRECTION_REVERSE;
    }
    else
    {
        u4MplsTnlDir = MPLS_DIRECTION_FORWARD;
    }

    if (u4TunnelInstance == TE_ZERO)
    {
        if (TeCliNormalCreateNonZeroInstTunnel (CliHandle, u4TunnelIndex,
                                                u4TunnelInstance, u4IngressId,
                                                u4EgressId, u1TnlPathType,
                                                &u4CreatedInstance,
                                                &bEntryCreated) == CLI_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to create non-zero instance tunnel\n");
            return CLI_FAILURE;
        }

        u4TunnelInstance = u4CreatedInstance;

        if (((i4TunnelMode == TE_TNL_MODE_UNIDIRECTIONAL) ||
             (i4TunnelMode == TE_TNL_MODE_ASSOCIATED_BIDIRECTIONAL) ||
             ((i4TnlRole == TE_INGRESS) &&
              (u4MplsTnlDir == MPLS_DIRECTION_FORWARD)) ||
             ((i4TnlRole == TE_EGRESS) &&
              (u4MplsTnlDir == MPLS_DIRECTION_REVERSE))) &&
            (u1TnlPathType == TE_TNL_WORKING_PATH))
        {
            u4MplsTnlIfIndex = gaTnlCliArgs[CliHandle].u4MplsTnlIfIndex;
            bIntfCreationReqd = FALSE;
        }
    }
    if (nmhGetMplsTunnelXCPointer (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                   u4EgressId, &XCSegIndex) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to get tunnel XC Pointer\r\n");

        TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                        u4TunnelInstance, u4IngressId,
                                        u4EgressId, bEntryCreated);
        return CLI_FAILURE;
    }

    MplsGetSegFromTunnelXC (&XCSegIndex, &XCSegmentIndex, &InSegmentIndex,
                            &OutSegmentIndex, &u4XCSegInd, &u4InSegInd,
                            &u4OutSegInd);

    /* This function is called for making static binding for incoming tunnels 
     * in egress node. For corouted bidirectional tunnels, we need to
     * check whether outgoing tunnel exist.
     *
     * If it exist, do not delete XC table.
     * Instead just create another insegment and associate it with XC table.
     *
     * If it does not exist create a new XC table entry.*/

    if ((i4TunnelMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL) &&
        (u4OutSegInd != 0))
    {
        if ((nmhGetFsMplsOutSegmentDirection (&OutSegmentIndex,
                                              &i4Direction)
             == SNMP_SUCCESS) && (i4Direction != (INT4) u4MplsTnlDir))
        {
            bXcDeleteFlag = FALSE;
        }
    }

    if ((u4XCSegInd != 0) && (bXcDeleteFlag == TRUE))
    {
        if (nmhTestv2MplsXCRowStatus (&u4ErrorCode, &XCSegmentIndex,
                                      &InSegmentIndex,
                                      &OutSegmentIndex,
                                      MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_XC_ENTRY_DELETION);

            TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                            u4TunnelInstance, u4IngressId,
                                            u4EgressId, bEntryCreated);
            return CLI_FAILURE;
        }

        if (nmhSetMplsXCRowStatus
            (&XCSegmentIndex, &InSegmentIndex, &OutSegmentIndex,
             MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_XC_ENTRY_DELETION);

            TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                            u4TunnelInstance, u4IngressId,
                                            u4EgressId, bEntryCreated);
            return CLI_FAILURE;
        }
    }
    /* Destroy the InSegment entry */

    if ((u4InSegInd != 0) && (bXcDeleteFlag == TRUE))
    {
        if (nmhGetMplsInSegmentInterface (&InSegmentIndex, &i4IfIndex) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to fetch the Insegment "
                       "table entry\r\n");
            return CLI_FAILURE;
        }

        MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, (UINT4) i4IfIndex,
                                     CFA_MPLS_TUNNEL, MPLS_TRUE);

        if (nmhTestv2MplsInSegmentRowStatus (&u4ErrorCode,
                                             &InSegmentIndex,
                                             MPLS_STATUS_DESTROY) ==
            SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_INSEG_ENTRY_CREATION);

            TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                            u4TunnelInstance, u4IngressId,
                                            u4EgressId, bEntryCreated);
            return CLI_FAILURE;
        }

        if (nmhSetMplsInSegmentRowStatus
            (&InSegmentIndex, MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_INSEG_ENTRY_DELETION);

            TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                            u4TunnelInstance, u4IngressId,
                                            u4EgressId, bEntryCreated);
            return CLI_FAILURE;
        }
    }
    /* create a new in seg entry */
    if ((nmhGetMplsInSegmentIndexNext (&InSegmentIndex)) == SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_NO_FREE_INDEX);

        TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                        u4TunnelInstance, u4IngressId,
                                        u4EgressId, bEntryCreated);
        return CLI_FAILURE;
    }
    if ((nmhTestv2MplsInSegmentRowStatus (&u4ErrorCode, &InSegmentIndex,
                                          MPLS_STATUS_CREATE_AND_WAIT)) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_INSEG_ENTRY_CREATION);

        TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                        u4TunnelInstance, u4IngressId,
                                        u4EgressId, bEntryCreated);
        return CLI_FAILURE;
    }
    if ((nmhSetMplsInSegmentRowStatus
         (&InSegmentIndex, MPLS_STATUS_CREATE_AND_WAIT)) == SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_INSEG_ENTRY_CREATION);

        TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                        u4TunnelInstance, u4IngressId,
                                        u4EgressId, bEntryCreated);
        return CLI_FAILURE;
    }

    if (bXcDeleteFlag == TRUE)
    {
        /* create an XC entry */
        if ((nmhGetMplsXCIndexNext (&XCIndex)) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_NO_FREE_INDEX);

            /* delete the created InSeg  entry */
            if (nmhSetMplsInSegmentRowStatus
                (&InSegmentIndex, MPLS_STATUS_DESTROY) == SNMP_FAILURE)
            {
                /* Failure check not handled */
            }

            TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                            u4TunnelInstance, u4IngressId,
                                            u4EgressId, bEntryCreated);
            return CLI_FAILURE;
        }
    }
    else
    {
        MPLS_INTEGER_TO_OCTETSTRING (u4XCSegInd, (&XCIndex));
        MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
        OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
        OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    }

    if ((nmhTestv2MplsXCRowStatus (&u4ErrorCode, &XCIndex, &InSegmentIndex,
                                   &OutSegmentIndex,
                                   MPLS_STATUS_CREATE_AND_WAIT))
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_XC_ENTRY_CREATION);
        /* delete the created InSeg  entry */
        if (nmhSetMplsInSegmentRowStatus (&InSegmentIndex, MPLS_STATUS_DESTROY)
            == SNMP_FAILURE)
        {
            /* Failure check not handled */
        }

        TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                        u4TunnelInstance, u4IngressId,
                                        u4EgressId, bEntryCreated);
        return CLI_FAILURE;
    }
    if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex,
                                &OutSegmentIndex,
                                MPLS_STATUS_CREATE_AND_WAIT)) == SNMP_FAILURE)
    {
        /* delete the created InSeg  entry */
        if (nmhSetMplsInSegmentRowStatus (&InSegmentIndex, MPLS_STATUS_DESTROY)
            == SNMP_FAILURE)
        {
            /* Failure check not handled */
        }

        CLI_SET_ERR (MPLS_CLI_XC_ENTRY_CREATION);

        TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                        u4TunnelInstance, u4IngressId,
                                        u4EgressId, bEntryCreated);
        return CLI_FAILURE;
    }

    do
    {
        /*  Set inlabel value in seg table */
        if ((nmhTestv2MplsInSegmentLabel (&u4ErrorCode, &InSegmentIndex,
                                          u4InLabel)) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_LABEL);
            break;
        }
        if ((nmhSetMplsInSegmentLabel (&InSegmentIndex, u4InLabel))
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_LABEL);
            break;
        }
        /* Set InSegment NPop */
        i4InSegmntNPop = 1;
        if ((nmhTestv2MplsInSegmentNPop (&u4ErrorCode, &InSegmentIndex,
                                         i4InSegmntNPop)) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_NPOP);
            break;
        }
        if ((nmhSetMplsInSegmentNPop (&InSegmentIndex, i4InSegmntNPop))
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_NPOP);
            break;
        }
        /* Setting the InSegment ip addr family to ipv4 */
        if (nmhTestv2MplsInSegmentAddrFamily (&u4ErrorCode, &InSegmentIndex,
                                              LSR_ADDR_IPV4) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_ADDR_FAMILY);
            break;
        }
        if (nmhSetMplsInSegmentAddrFamily (&InSegmentIndex, LSR_ADDR_IPV4)
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_ADDR_FAMILY);
            break;
        }

        if (u4MplsTnlIfIndex == 0)
        {
            if (CfaGetFreeInterfaceIndex (&u4MplsTnlIfIndex, CFA_MPLS_TUNNEL) ==
                OSIX_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_NO_FREE_TUNNEL_INTERFACE);
                break;
            }
        }

        if ((nmhTestv2MplsInSegmentInterface (&u4ErrorCode,
                                              &InSegmentIndex,
                                              (INT4) u4MplsTnlIfIndex))
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_IF);
            break;
        }
        if ((nmhSetMplsInSegmentInterface (&InSegmentIndex,
                                           (INT4) u4MplsTnlIfIndex))
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_IF);
            break;
        }

        if (bIntfCreationReqd == FALSE)
        {
            if (MplsStackMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlIfIndex,
                                            CFA_MPLS_TUNNEL) == MPLS_FAILURE)
            {
                CliPrintf (CliHandle, "\r %%Unable to stack mplstunnel "
                           "interface\r\n");
                break;
            }
        }
        else
        {
            if (MplsCreateMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlIfIndex,
                                             CFA_MPLS_TUNNEL) == MPLS_FAILURE)
            {
                CliPrintf (CliHandle,
                           "%% Unable to create MPLS tunnel interface\r\n");
                break;
            }
        }

        if ((nmhTestv2FsMplsInSegmentDirection (&u4ErrorCode,
                                                &InSegmentIndex,
                                                (INT4) u4MplsTnlDir))
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_DIR);
            break;
        }
        if ((nmhSetFsMplsInSegmentDirection (&InSegmentIndex,
                                             (INT4) u4MplsTnlDir))
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_DIR);
            break;
        }

        if (nmhTestv2MplsInSegmentStorageType (&u4ErrorCode, &InSegmentIndex,
                                               MPLS_STORAGE_NONVOLATILE)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set the In-segment "
                       "storage type");
            bTempFlag = TRUE;
            break;
        }

        if (nmhSetMplsInSegmentStorageType (&InSegmentIndex,
                                            MPLS_STORAGE_NONVOLATILE)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set the In-segment "
                       "storage type");
            bTempFlag = TRUE;
            break;
        }

        /* set the row status of the InSegment and XC tables as active */
        if ((nmhTestv2MplsInSegmentRowStatus (&u4ErrorCode, &InSegmentIndex,
                                              MPLS_STATUS_ACTIVE)) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set the In-segment "
                       "row status active");
            bTempFlag = TRUE;
            break;
        }
        if ((nmhSetMplsInSegmentRowStatus
             (&InSegmentIndex, MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set the In-segment "
                       "row status active");
            bTempFlag = TRUE;
            break;
        }

        if (bXcDeleteFlag == TRUE)
        {
            MPLS_OCTETSTRING_TO_INTEGER ((&XCIndex), u4XCTabIndex);
            MPLS_OCTETSTRING_TO_INTEGER ((&OutSegmentIndex), u4OutIndex);
            MPLS_OCTETSTRING_TO_INTEGER ((&InSegmentIndex), u4InIndex);
            MPLS_INTEGER_TO_OID (au4XCTableOid, u4XCTabIndex,
                                 TE_TNL_XC_TABLE_DEF_OFFSET);
            MPLS_INTEGER_TO_OID (au4XCTableOid, u4InIndex,
                                 TE_TNL_XC_TABLE_DEF_OFFSET + 5);
            MPLS_INTEGER_TO_OID (au4XCTableOid, u4OutIndex,
                                 TE_TNL_XC_TABLE_DEF_OFFSET + 10);
            TunnelXCPointer.pu4_OidList = au4XCTableOid;
            TunnelXCPointer.u4_Length = MPLS_TE_XC_TABLE_OFFSET;

            if (TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                             u4IngressId, u4EgressId,
                                             TE_NOTINSERVICE) == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n%%Unable to set MPLS tunnel row "
                           "status as Not in Service");
                bTempFlag = TRUE;
                break;
            }

            if ((nmhTestv2MplsTunnelXCPointer (&u4ErrorCode, u4TunnelIndex,
                                               u4TunnelInstance, u4IngressId,
                                               u4EgressId,
                                               &TunnelXCPointer))
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%%Unable to set MplsTunnelXCPointer\r\n");
                bTempFlag = TRUE;
                if (i4PrevRowStatus != TE_NOTREADY)
                {
                    TeCliSetMplsTunnelRowStatus (u4TunnelIndex,
                                                 u4TunnelInstance,
                                                 u4IngressId, u4EgressId,
                                                 i4PrevRowStatus);
                }
                break;
            }

            if ((nmhSetMplsTunnelXCPointer (u4TunnelIndex,
                                            u4TunnelInstance, u4IngressId,
                                            u4EgressId,
                                            &TunnelXCPointer)) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%%Unable to set MplsTunnelXCPointer\r\n");
                bTempFlag = TRUE;
                if (i4PrevRowStatus != TE_NOTREADY)
                {
                    TeCliSetMplsTunnelRowStatus (u4TunnelIndex,
                                                 u4TunnelInstance,
                                                 u4IngressId, u4EgressId,
                                                 i4PrevRowStatus);
                }
                break;
            }
        }

        if (nmhTestv2MplsXCStorageType (&u4ErrorCode, &XCIndex, &InSegmentIndex,
                                        &OutSegmentIndex,
                                        MPLS_STORAGE_NONVOLATILE)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%% Unable to set storage type\r\n");
            bTempFlag = TRUE;
            break;
        }

        if (nmhSetMplsXCStorageType
            (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
             MPLS_STORAGE_NONVOLATILE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%% Unable to set storage type\r\n");
            bTempFlag = TRUE;
            break;
        }

        if ((nmhTestv2MplsXCRowStatus
             (&u4ErrorCode, &XCIndex, &InSegmentIndex, &OutSegmentIndex,
              MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set the XC row status active");
            bTempFlag = TRUE;
            break;
        }
        if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex,
                                    &OutSegmentIndex,
                                    MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set the XC row status active");
            bTempFlag = TRUE;
            break;
        }

        if ((i4PrevRowStatus != TE_NOTREADY) && (bXcDeleteFlag == TRUE))
        {
            TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                         u4IngressId, u4EgressId,
                                         i4PrevRowStatus);
        }

        /* all set operations have been successful */
        return CLI_SUCCESS;
    }
    while (0);
    if (bTempFlag == TRUE)        /* Error occured */
    {
        if (((i4TunnelMode == TE_TNL_MODE_UNIDIRECTIONAL) ||
             (u4MplsTnlDir == MPLS_DIRECTION_REVERSE)) &&
            (u1TnlPathType == TE_TNL_WORKING_PATH))
        {
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_FALSE);
        }
        else
        {
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
        }
    }
    /* delete the created InSeg and XC entry */
    if (nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               MPLS_STATUS_DESTROY) == SNMP_FAILURE)
    {
        /* Failure check not handled */
    }

    if (nmhSetMplsInSegmentRowStatus (&InSegmentIndex, MPLS_STATUS_DESTROY)
        == SNMP_FAILURE)
    {
        /* Failure check not handled */
    }

    TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                    u4TunnelInstance, u4IngressId,
                                    u4EgressId, bEntryCreated);
    return CLI_FAILURE;
}

/******************************************************************************
 * Function Name : TeCliTunnelActive
 * Description   : This routine is used to make tunnel row status active. 
 * Input(s)      : CliHandle     - Cli Context Handle
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ******************************************************************************/
INT1
TeCliTunnelActive (tCliHandle CliHandle)
{
    UINT4               u4TunnelIndex = 0;
    UINT4               u4MplsTnlIfIndex = 0;
    UINT4               u4TunnelInstance = 0;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4OrgTnlIndex = TE_ZERO;
    UINT4               u4OrgIngressId = TE_ZERO;
    UINT4               u4OrgEgressId = TE_ZERO;
    INT4                i4TnlOwner = TE_ZERO;
    tCfaIfInfo          CfaIfInfo;
    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    u4OrgTnlIndex = u4TunnelIndex;
    u4OrgIngressId = u4IngressId;
    u4OrgEgressId = u4EgressId;

    u4MplsTnlIfIndex = gaTnlCliArgs[CliHandle].u4MplsTnlIfIndex;
    if (CfaGetIfInfo (u4MplsTnlIfIndex, &CfaIfInfo) == CFA_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to get the CFA interface info\r\n");
        return CLI_FAILURE;
    }
    if (u4MplsTnlIfIndex != TE_ZERO)
    {
        CFA_LOCK ();

        if (nmhSetIfMainAdminStatus ((INT4) u4MplsTnlIfIndex, CFA_IF_UP)
            == SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            CliPrintf (CliHandle, "\r\n%%Unable to set IfMain AdminStatus"
                       "to %u.\n", u4MplsTnlIfIndex);
            return CLI_FAILURE;
        }

        CFA_UNLOCK ();
    }
    do
    {
        if ((u4OrgTnlIndex != u4TunnelIndex) ||
            (u4OrgIngressId != u4IngressId) || (u4OrgEgressId != u4EgressId))
        {
            break;
        }

        i4TnlOwner = TE_ZERO;
        nmhGetMplsTunnelOwner (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                               u4EgressId, &i4TnlOwner);

        if (i4TnlOwner != TE_TNL_OWNER_SNMP)
        {
            continue;
        }

        /* Set Tunnel Row Status to active */
        if (TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                         u4IngressId, u4EgressId,
                                         TE_ACTIVE) == CLI_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set Tunnel RowStatus Active\r\n");
            CFA_LOCK ();

            if (nmhSetIfMainAdminStatus ((INT4) u4MplsTnlIfIndex,
                                         (INT4) CfaIfInfo.u1IfAdminStatus)
                == SNMP_FAILURE)
            {
                CFA_UNLOCK ();
                CliPrintf (CliHandle, "\r\n%%Unable to set IfMain AdminStatus"
                           "to %u.\n", u4MplsTnlIfIndex);
                return CLI_FAILURE;
            }

            CFA_UNLOCK ();
            return CLI_FAILURE;
        }

        /* Set Admin Status to up */

        if (nmhTestv2MplsTunnelAdminStatus (&u4ErrorCode,
                                            u4TunnelIndex, u4TunnelInstance,
                                            u4IngressId, u4EgressId,
                                            TE_ADMIN_UP) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set Tunnel AdminStatus\r\n");
            CFA_LOCK ();

            if (nmhSetIfMainAdminStatus ((INT4) u4MplsTnlIfIndex,
                                         (INT4) CfaIfInfo.u1IfAdminStatus)
                == SNMP_FAILURE)
            {
                CFA_UNLOCK ();
                CliPrintf (CliHandle, "\r\n%%Unable to set IfMain AdminStatus"
                           "to %u.\n", u4MplsTnlIfIndex);
                return CLI_FAILURE;
            }

            CFA_UNLOCK ();
            return CLI_FAILURE;
        }

        if (nmhSetMplsTunnelAdminStatus
            (u4TunnelIndex, u4TunnelInstance, u4IngressId, u4EgressId,
             TE_ADMIN_UP) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set Tunnel AdminStatus\r\n");
            CFA_LOCK ();

            if (nmhSetIfMainAdminStatus ((INT4) u4MplsTnlIfIndex,
                                         (INT4) CfaIfInfo.u1IfAdminStatus)
                == SNMP_FAILURE)
            {
                CFA_UNLOCK ();
                CliPrintf (CliHandle, "\r\n%%Unable to set IfMain AdminStatus"
                           "to %u.\n", u4MplsTnlIfIndex);
                return CLI_FAILURE;
            }

            CFA_UNLOCK ();
            return CLI_FAILURE;
        }

        if (u4TunnelInstance != TE_ZERO)
        {
            break;
        }
    }
    while (nmhGetNextIndexMplsTunnelTable (u4TunnelIndex, &u4TunnelIndex,
                                           u4TunnelInstance, &u4TunnelInstance,
                                           u4IngressId, &u4IngressId,
                                           u4EgressId, &u4EgressId)
           == SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : TeCliTunnelInActive
 * Description   : This routine is used to make the tunnel Admin Status down. 
* Input(s)      : CliHandle     - Cli Context Handle
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT1
TeCliTunnelInActive (tCliHandle CliHandle)
{
    UINT4               u4TunnelIndex = 0;
    UINT4               u4MplsTnlIfIndex = 0;
    UINT4               u4TunnelInstance = 0;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4OrgTnlIndex = TE_ZERO;
    UINT4               u4OrgIngressId = TE_ZERO;
    UINT4               u4OrgEgressId = TE_ZERO;
    INT4                i4TnlOwner = TE_ZERO;

    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    u4OrgTnlIndex = u4TunnelIndex;
    u4OrgIngressId = u4IngressId;
    u4OrgEgressId = u4EgressId;

    do
    {
        if ((u4OrgTnlIndex != u4TunnelIndex) ||
            (u4OrgIngressId != u4IngressId) || (u4OrgEgressId != u4EgressId))
        {
            break;
        }

        i4TnlOwner = TE_ZERO;
        nmhGetMplsTunnelOwner (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                               u4EgressId, &i4TnlOwner);

        if (i4TnlOwner != TE_TNL_OWNER_SNMP)
        {
            continue;
        }

        /* Make AdminStatus Down */
        if (nmhTestv2MplsTunnelAdminStatus (&u4ErrorCode,
                                            u4TunnelIndex, u4TunnelInstance,
                                            u4IngressId, u4EgressId,
                                            TE_ADMIN_DOWN) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set Tunnel AdminStatus to Down\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetMplsTunnelAdminStatus
            (u4TunnelIndex, u4TunnelInstance, u4IngressId, u4EgressId,
             TE_ADMIN_DOWN) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set Tunnel AdminStatus to Down\r\n");
            return CLI_FAILURE;
        }

        if (u4TunnelInstance != TE_ZERO)
        {
            break;
        }
    }
    while (nmhGetNextIndexMplsTunnelTable (u4TunnelIndex, &u4TunnelIndex,
                                           u4TunnelInstance, &u4TunnelInstance,
                                           u4IngressId, &u4IngressId,
                                           u4EgressId, &u4EgressId)
           == SNMP_SUCCESS);

    u4MplsTnlIfIndex = gaTnlCliArgs[CliHandle].u4MplsTnlIfIndex;
    if (u4MplsTnlIfIndex != TE_ZERO)
    {
        CFA_LOCK ();

        if (nmhSetIfMainAdminStatus ((INT4) u4MplsTnlIfIndex, CFA_IF_DOWN)
            == SNMP_FAILURE)
        {
            CFA_UNLOCK ();
            return CLI_FAILURE;
        }

        CFA_UNLOCK ();
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : MplsGetMplsTunnelCfgPrompt
 * Description   : This routine is returns the prompt to be displayed. 
 * Input(s)      : pi1ModeName - Mode to be configured
 * Output(s)     : pi1DispStr  - Prompt to be displayed.
 * Return(s)     : TRUE or FALSE.
 *****************************************************************************/
INT1
MplsGetMplsTunnelCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    INT4                i4Index;
    UINT4               u4Len = STRLEN (MPLS_TUNNEL_MODE);
    CHR1                ac1PromptDisplayName[MAX_PROMPT_LEN];

    if (!pi1DispStr || !pi1ModeName)
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, MPLS_TUNNEL_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    i4Index = CLI_ATOI (pi1ModeName);

    CLI_SET_MPLS_TUNNEL_ID (i4Index);

    SNPRINTF (ac1PromptDisplayName, MAX_PROMPT_LEN, "%s", "(config-if)#");

    STRNCPY (pi1DispStr, ac1PromptDisplayName, (MAX_PROMPT_LEN - 1));
    *(pi1DispStr + (MAX_PROMPT_LEN - 1)) = '\0';

    return TRUE;
}

/******************************************************************************
 * Function Name : MplsGetMplsTunnelLspCfgPrompt
 * Description   : This routine is returns the prompt to be displayed. 
 * Input(s)      : pi1ModeName - Mode to be configured
 * Output(s)     : pi1DispStr  - Prompt to be displayed.
 * Return(s)     : TRUE or FALSE.
 *****************************************************************************/
INT1
MplsGetMplsTunnelLspCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    INT4                i4Index;
    UINT4               u4Len = STRLEN (MPLS_TUNNEL_LSP_MODE);
    CHR1                ac1PromptDisplayName[MAX_PROMPT_LEN];

    if (!pi1DispStr || !pi1ModeName)
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, MPLS_TUNNEL_LSP_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    i4Index = CLI_ATOI (pi1ModeName);

    CLI_SET_MPLS_TUNNEL_ID (i4Index);

    SNPRINTF (ac1PromptDisplayName, MAX_PROMPT_LEN, "%s", "(config-if-lsp)#");

    STRNCPY (pi1DispStr, ac1PromptDisplayName, (MAX_PROMPT_LEN - 1));
    *(pi1DispStr + (MAX_PROMPT_LEN - 1)) = '\0';

    return TRUE;
}

/******************************************************************************
 * Function Name : MplsGetMplsTunnelElspCfgPrompt
 * Description   : This routine is returns the prompt to be displayed for ELSP 
 * Input(s)      : pi1ModeName - Mode to be configured
 * Output(s)     : pi1DispStr  - Prompt to be displayed.
 * Return(s)     : TRUE or FALSE.
 *****************************************************************************/
INT1
MplsGetMplsTunnelElspCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN (MPLS_TUNNEL_ELSP_MODE);
    CHR1                ac1PromptDisplayName[MAX_PROMPT_LEN];

    if (!pi1DispStr || !pi1ModeName)
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, MPLS_TUNNEL_ELSP_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    SNPRINTF (ac1PromptDisplayName, MAX_PROMPT_LEN, "%s", "(config-if-elsp)#");

    STRNCPY (pi1DispStr, ac1PromptDisplayName, (MAX_PROMPT_LEN - 1));
    *(pi1DispStr + (MAX_PROMPT_LEN - 1)) = '\0';

    return TRUE;
}

/******************************************************************************
 * Function Name : MplsGetMplsTunnelLlspCfgPrompt
 * Description   : This routine is returns the prompt to be displayed for LLSP 
 * Input(s)      : pi1ModeName - Mode to be configured
 * Output(s)     : pi1DispStr  - Prompt to be displayed.
 * Return(s)     : TRUE or FALSE.
 *****************************************************************************/
INT1
MplsGetMplsTunnelLlspCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN (MPLS_TUNNEL_LLSP_MODE);
    CHR1                ac1PromptDisplayName[MAX_PROMPT_LEN];

    if (!pi1DispStr || !pi1ModeName)
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, MPLS_TUNNEL_LLSP_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    SNPRINTF (ac1PromptDisplayName, MAX_PROMPT_LEN, "%s", "(config-if-llsp)#");

    STRNCPY (pi1DispStr, ac1PromptDisplayName, (MAX_PROMPT_LEN - 1));
    *(pi1DispStr + (MAX_PROMPT_LEN - 1)) = '\0';

    return TRUE;
}

/******************************************************************************
 * Function Name : MplsGetMplsTunnelClassCfgPrompt
 * Description   : This routine is returns the prompt to be displayed for Class 
 * Input(s)      : pi1ModeName - Mode to be configured
 * Output(s)     : pi1DispStr  - Prompt to be displayed.
 * Return(s)     : TRUE or FALSE.
 *****************************************************************************/
INT1
MplsGetMplsTunnelClassCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN (MPLS_TUNNEL_CLASS_MODE);
    CHR1                ac1PromptDisplayName[MAX_PROMPT_LEN];

    if (!pi1DispStr || !pi1ModeName)
    {
        return FALSE;
    }

    u4Len = STRLEN (MPLS_TUNNEL_CLASS_MODE);

    if (STRNCMP (pi1ModeName, MPLS_TUNNEL_CLASS_MODE, u4Len) != MPLS_ZERO)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    SNPRINTF (ac1PromptDisplayName, MAX_PROMPT_LEN, "%s", "(config-class)#");

    STRNCPY (pi1DispStr, ac1PromptDisplayName, (MAX_PROMPT_LEN - 1));
    *(pi1DispStr + (MAX_PROMPT_LEN - 1)) = '\0';

    return TRUE;
}

/******************************************************************************
 * Function Name : MplsGetMplsTunnelExpToPhbMapCfgPrompt
 * Description   : This routine is returns the prompt to be displayed for EXP to
 *                 PHB mapping configuration
 * Input(s)      : pi1ModeName - Mode to be configured
 * Output(s)     : pi1DispStr  - Prompt to be displayed.
 * Return(s)     : TRUE or FALSE.
 *****************************************************************************/
INT1
MplsGetMplsTunnelExpToPhbMapCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN (MPLS_TUNNEL_EXP_PHB_MAP_MODE);
    CHR1                ac1PromptDisplayName[MAX_PROMPT_LEN];

    if (!pi1DispStr || !pi1ModeName)
    {
        return FALSE;
    }
    u4Len = STRLEN (MPLS_TUNNEL_EXP_PHB_MAP_MODE);

    if (STRNCMP (pi1ModeName, MPLS_TUNNEL_EXP_PHB_MAP_MODE, u4Len) != MPLS_ZERO)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    SNPRINTF (ac1PromptDisplayName, MAX_PROMPT_LEN,
              "%s", "(config-exp-phb-map)#");

    STRNCPY (pi1DispStr, ac1PromptDisplayName, (MAX_PROMPT_LEN - 1));
    *(pi1DispStr + (MAX_PROMPT_LEN - 1)) = '\0';

    return TRUE;
}

/******************************************************************************
 * Function Name : TeGetAttrCfgPrompt
 * Description   : This routine is returns the prompt to be displayed. 
 * Input(s)      : pi1ModeName - Mode to be configured
 * Output(s)     : pi1DispStr  - Prompt to be displayed.
 * Return(s)     : TRUE or FALSE.
 *****************************************************************************/
INT1
TeGetAttrCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN (MPLS_TE_ATTR_MODE);
    CHR1                ac1PromptDisplayName[MAX_PROMPT_LEN];

    if (!pi1DispStr || !pi1ModeName)
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, MPLS_TE_ATTR_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    SNPRINTF (ac1PromptDisplayName, MAX_PROMPT_LEN, "%s", "(config-lsp-attr)#");

    STRNCPY (pi1DispStr, ac1PromptDisplayName, (MAX_PROMPT_LEN - 1));
    *(pi1DispStr + (MAX_PROMPT_LEN - 1)) = '\0';

    return TRUE;
}

/******************************************************************************
 * Function Name : TeGetExplicitPathCfgPrompt
 * Description   : This routine is returns the prompt to be displayed. 
 * Input(s)      : pi1ModeName - Mode to be configured
 * Output(s)     : pi1DispStr  - Prompt to be displayed.
 * Return(s)     : TRUE or FALSE.
 *****************************************************************************/
INT1
TeGetExplicitPathCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN (MPLS_TE_EXPLICIT_PATH_MODE);
    CHR1                ac1PromptDisplayName[MAX_PROMPT_LEN];

    if (!pi1DispStr || !pi1ModeName)
    {
        return FALSE;
    }

    if (STRNCMP (pi1ModeName, MPLS_TE_EXPLICIT_PATH_MODE, u4Len) != 0)
    {
        return FALSE;
    }
    pi1ModeName = pi1ModeName + u4Len;

    SNPRINTF (ac1PromptDisplayName, MAX_PROMPT_LEN,
              "%s", "(cfg-ip-expl-path)#");

    STRNCPY (pi1DispStr, ac1PromptDisplayName, (MAX_PROMPT_LEN - 1));
    *(pi1DispStr + (MAX_PROMPT_LEN - 1)) = '\0';

    return TRUE;
}

/******************************************************************************
 * Function Name : MplsGetMplsTunnelElspInfoPhbMapCfgPrompt
 * Description   : This routine is returns the prompt to be displayed for EXP to
 *                 PHB mapping configuration
 * Input(s)      : pi1ModeName - Mode to be configured
 * Output(s)     : pi1DispStr  - Prompt to be displayed.
 * Return(s)     : TRUE or FALSE.
 *****************************************************************************/
INT1
MplsGetMplsTunnelElspInfoPhbMapCfgPrompt (INT1 *pi1ModeName, INT1 *pi1DispStr)
{
    UINT4               u4Len = STRLEN (MPLS_TUNNEL_ELSP_INFO_PHB_MAP_MODE);
    CHR1                ac1PromptDisplayName[MAX_PROMPT_LEN];

    if (!pi1DispStr || !pi1ModeName)
    {
        return FALSE;
    }
    u4Len = STRLEN (MPLS_TUNNEL_ELSP_INFO_PHB_MAP_MODE);

    if (STRNCMP (pi1ModeName, MPLS_TUNNEL_ELSP_INFO_PHB_MAP_MODE, u4Len) !=
        MPLS_ZERO)
    {
        return FALSE;
    }

    pi1ModeName = pi1ModeName + u4Len;

    SNPRINTF (ac1PromptDisplayName, MAX_PROMPT_LEN,
              "%s", "(config-elsp-info-phb)#");

    STRNCPY (pi1DispStr, ac1PromptDisplayName, (MAX_PROMPT_LEN - 1));
    *(pi1DispStr + (MAX_PROMPT_LEN - 1)) = '\0';

    return TRUE;
}

/*****************************************************************************
 * Function Name : TeCliMplsCreateExpPhbMap
 * Description   : This routine is used to Create Exp Phb Map.
 * * Input(s)      : CliHandle  - Cli Context Handle
 *                 u4ExpMapIndex - Index of the Map to be created
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE 
 ******************************************************************************/
INT4
TeCliMplsCreateExpPhbMap (tCliHandle CliHandle, UINT4 u4ExpMapIndex)
{
    gaTeCliArgs[CliHandle].u4ElspMapIndex = u4ExpMapIndex;

    if (CliChangePath (MPLS_TUNNEL_EXP_PHB_MAP_MODE) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to enter into config-pw-red-class"
                   "configuration mode\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeCliMplsDeleteExpPhbMap
 * Description   : This routine is used to Delete Exp Phb Map.
 * Input(s)      : CliHandle  - Cli Context Handle
 *                 u4ExpMapIndex - Index of the Map to be created
 * Output(s)     : None
 *
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ******************************************************************************/
INT4
TeCliMplsDeleteExpPhbMap (tCliHandle CliHandle, INT4 i4ExpMapIndex)
{
    INT4                i4ElspMapExpIndex = TE_ZERO;
    INT4                i4NextExpMapIndex = TE_ZERO;
    UINT4               u4ErrorCode = TE_ZERO;

    i4NextExpMapIndex = i4ExpMapIndex;

    while (nmhGetNextIndexFsMplsDiffServElspMapTable (i4NextExpMapIndex,
                                                      &i4NextExpMapIndex,
                                                      i4ElspMapExpIndex,
                                                      &i4ElspMapExpIndex)
           == SNMP_SUCCESS)
    {
        if (i4NextExpMapIndex != i4ExpMapIndex)
        {
            break;
        }

        if (nmhTestv2FsMplsDiffServElspMapStatus (&u4ErrorCode, i4ExpMapIndex,
                                                  i4ElspMapExpIndex,
                                                  TE_DESTROY) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to Delete EXP to PHB mapping\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetFsMplsDiffServElspMapStatus (i4ExpMapIndex, i4ElspMapExpIndex,
                                               TE_DESTROY) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to Delete EXP to PHB mapping\r\n");
            return CLI_FAILURE;
        }
    }

    gaTeCliArgs[CliHandle].u4ElspMapIndex = TE_ZERO;

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeCliMplsCreateElspInfoPhbMap
 * Description   : This routine is used to Create Elsp Info Phb Map Table.
 * * Input(s)      : CliHandle  - Cli Context Handle
 *                 u4ElspInfoIndex - Index of the Map to be created
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ******************************************************************************/
INT1
TeCliMplsCreateElspInfoPhbMap (tCliHandle CliHandle, UINT4 u4ElspInfoListIndex)
{

    if ((u4ElspInfoListIndex > (UINT4) TE_MAX_DS_ELSPS (gTeGblInfo))
        || (u4ElspInfoListIndex < TE_ONE))
    {
        CliPrintf (CliHandle, "\r%% Unable to create ElspInfo index \r\n");
        return CLI_FAILURE;
    }
    gaTeCliArgs[CliHandle].u4ElspInfoIndex = u4ElspInfoListIndex;

    TE_DS_GBL_ELSP_INFO_LIST_INDEX (u4ElspInfoListIndex) = u4ElspInfoListIndex;

    if (CliChangePath (MPLS_TUNNEL_ELSP_INFO_PHB_MAP_MODE) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to enter into config-pw-red-class"
                   "configuration mode\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeCliMplsDeleteElspInfoPhbMap
 * Description   : This routine is used to Delete Elsp Info Phb Map Table.
 * * Input(s)      : CliHandle  - Cli Context Handle
   *                 u4ElspInfoIndex - Index of the Map to be created
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ******************************************************************************/
INT1
TeCliMplsDeleteElspInfoPhbMap (tCliHandle CliHandle, INT4 i4ElspInfoListIndex)
{
    INT4                i4NextElspInfoListIndex = TE_ZERO;
    INT4                i4ElspInfoIndex = TE_ZERO;
    tMplsDiffServElspInfo *pElspInfoNode = NULL;
    tTeSll             *pList = NULL;

    i4NextElspInfoListIndex = i4ElspInfoListIndex;

    if ((i4ElspInfoListIndex > (INT4) TE_MAX_DS_ELSPS (gTeGblInfo))
        || (i4ElspInfoListIndex < TE_ONE))
    {
        CliPrintf (CliHandle, "\r%% Unable to delete ElspInfo index \r\n");
        return CLI_FAILURE;
    }

    pList = (TE_DS_GBL_ELSP_INFO_LIST (i4ElspInfoListIndex));
    TE_SLL_SCAN (pList, pElspInfoNode, tMplsDiffServElspInfo *)
    {
        if (TE_DS_ELSP_INFO_INDEX (pElspInfoNode) == TE_ZERO)
        {
            i4ElspInfoIndex = TE_DS_ELSP_INFO_INDEX (pElspInfoNode);
            if (TeCliDeleteMplsDiffServElspInfoRowStatus (i4ElspInfoListIndex,
                                                          i4ElspInfoIndex) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%%Unable to Delete ElspInfo PHB mapping\r\n");
                return CLI_FAILURE;
            }
            break;
        }
    }

    while (nmhGetNextIndexFsMplsDiffServElspInfoTable
           (i4NextElspInfoListIndex, &i4NextElspInfoListIndex,
            i4ElspInfoIndex, &i4ElspInfoIndex) == SNMP_SUCCESS)
    {
        if (i4ElspInfoListIndex != i4NextElspInfoListIndex)
        {
            break;
        }

        if (TeCliDeleteMplsDiffServElspInfoRowStatus (i4ElspInfoListIndex,
                                                      i4ElspInfoIndex) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to Delete ElspInfo PHB mapping\r\n");
            return CLI_FAILURE;
        }
    }

    gaTeCliArgs[CliHandle].u4ElspInfoIndex = TE_ZERO;

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeCliMplsSetDsteStatus
 * Description   : This routine is used to enable/disable DiffServ Status
 * Input(s)      : CliHandle  - Cli Context Handle
 *                 u4DsTeStatus - DiffServ Status
 * Output(s)     : None
 *
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ******************************************************************************/
INT4
TeCliMplsSetDsteStatus (tCliHandle CliHandle, UINT4 u4DsTeStatus)
{
    UINT4               u4ErrorCode = TE_ZERO;

    if (nmhTestv2FsMplsDsTeStatus (&u4ErrorCode, (INT4) u4DsTeStatus) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to Test DiffServ Status\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsDsTeStatus ((INT4) u4DsTeStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to Set DiffServ Status\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeCliMplsSetDsTeClassType
 * Description   : This routine is used to configure class type and percentage
 * Input(s)      : CliHandle  - Cli Context Handle
 *                 u4ClassType - Diff Serv Class Type
 *                 pu1Description - Class Description
 *                 u4BwPercent - Bandwidth Percentage
 * Output(s)     : None
 *
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ******************************************************************************/
INT1
TeCliMplsSetDsTeClassType (tCliHandle CliHandle, UINT4 u4ClassType,
                           UINT1 *pu1Description, UINT4 u4BwPercent)
{
    UINT4               u4ErrorCode = TE_ZERO;
    tSNMP_OCTET_STRING_TYPE ClassDesc;
    INT4                i4DsTeClassTypeRowStatus = TE_ZERO;

    MEMSET (&ClassDesc, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    MplsDsTeCliGetOctetString (pu1Description, &ClassDesc);

    nmhGetFsMplsDsTeClassTypeRowStatus (u4ClassType, &i4DsTeClassTypeRowStatus);

    if (i4DsTeClassTypeRowStatus == TE_ACTIVE)
    {
        if (nmhTestv2FsMplsDsTeClassTypeRowStatus (&u4ErrorCode, u4ClassType,
                                                   TE_NOTINSERVICE) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Unable to make Class Type Row Status NOT IN SERVICE\n");
            return CLI_FAILURE;
        }

        if (nmhSetFsMplsDsTeClassTypeRowStatus (u4ClassType, TE_NOTINSERVICE)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Unable to make Class Type Row Status NOT IN SERVICE\n");
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsMplsDsTeClassTypeRowStatus (&u4ErrorCode, u4ClassType,
                                                   CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to Create Te Class Type\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetFsMplsDsTeClassTypeRowStatus (u4ClassType, CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to Create Te Class Type\r\n");
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsMplsDsTeClassTypeDescription (&u4ErrorCode, u4ClassType,
                                                 &ClassDesc) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to Set Class Type Description\r\n");

        if (i4DsTeClassTypeRowStatus == TE_ACTIVE)
        {
            if (nmhSetFsMplsDsTeClassTypeRowStatus (u4ClassType, ACTIVE) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%%Unable to Set Class Type Row Status\r\n");
            }
        }
        else
        {
            if (nmhSetFsMplsDsTeClassTypeRowStatus (u4ClassType, DESTROY) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%%Unable to Set Class Type Row Status\r\n");
            }
        }

        return CLI_FAILURE;
    }

    if (nmhSetFsMplsDsTeClassTypeDescription (u4ClassType, &ClassDesc)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to Set Class Type Description\r\n");

        if (i4DsTeClassTypeRowStatus == TE_ACTIVE)
        {
            nmhSetFsMplsDsTeClassTypeRowStatus (u4ClassType, ACTIVE);
        }
        else
        {
            nmhSetFsMplsDsTeClassTypeRowStatus (u4ClassType, DESTROY);
        }

        return CLI_FAILURE;
    }

    if (nmhTestv2FsMplsDsTeClassTypeBwPercentage (&u4ErrorCode, u4ClassType,
                                                  (INT4) u4BwPercent) ==
        SNMP_FAILURE)
    {

        if (i4DsTeClassTypeRowStatus == TE_ACTIVE)
        {
            nmhSetFsMplsDsTeClassTypeRowStatus (u4ClassType, ACTIVE);
        }
        else
        {
            nmhSetFsMplsDsTeClassTypeRowStatus (u4ClassType, DESTROY);
        }
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsDsTeClassTypeBwPercentage (u4ClassType, (INT4) u4BwPercent)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%% Unable to Set Class Type Bandwidth Percentage\r\n");

        if (i4DsTeClassTypeRowStatus == TE_ACTIVE)
        {
            nmhSetFsMplsDsTeClassTypeRowStatus (u4ClassType, ACTIVE);
        }
        else
        {
            nmhSetFsMplsDsTeClassTypeRowStatus (u4ClassType, DESTROY);
        }

        return CLI_FAILURE;
    }

    if (nmhTestv2FsMplsDsTeClassTypeRowStatus (&u4ErrorCode, u4ClassType,
                                               ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to activate Te Class Type\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsDsTeClassTypeRowStatus (u4ClassType, ACTIVE)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to activate Te Class Type\r\n");
        return CLI_FAILURE;
    }

    gaTeCliArgs[CliHandle].i4ClassType = (INT4) u4ClassType;
    if (CliChangePath (MPLS_TUNNEL_CLASS_MODE) == CLI_FAILURE)

    {
        CliPrintf (CliHandle, "\r%% Unable to enter into config-pw-red-class"
                   "configuration mode\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeCliMplsDeleteDsTeClassType
 * Description   : This routine is used to delete class type entry
 * Input(s)      : CliHandle  - Cli Context Handle
 *                 u4ClassType - Diff Serv Class Type
 * Output(s)     : None
 *
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ******************************************************************************/
INT1
TeCliMplsDeleteDsTeClassType (tCliHandle CliHandle, UINT4 u4ClassType)
{
    UINT4               u4ErrorCode = TE_ZERO;

    UINT4               u4CType = u4ClassType;
    UINT4               u4CTypeTcMap = TE_ZERO;
    UINT4               u4TeClass = TE_ZERO;

    while (nmhGetNextIndexFsMplsDsTeClassTypeToTcMapTable (u4CType, &u4CType,
                                                           u4CTypeTcMap,
                                                           &u4CTypeTcMap)
           == SNMP_SUCCESS)
    {
        if (u4CType != u4ClassType)
        {
            break;
        }

        if (nmhSetFsMplsDsTeTcMapRowStatus (u4CType, u4CTypeTcMap, TE_DESTROY)
            == SNMP_FAILURE)
        {
            continue;
        }
    }

    u4CType = u4ClassType;

    while (nmhGetNextIndexFsMplsDsTeTeClassTable (u4CType, &u4CType,
                                                  u4TeClass, &u4TeClass)
           == SNMP_SUCCESS)
    {
        if (u4CType != u4ClassType)
        {
            break;
        }

        if (nmhSetFsMplsDsTeTeClassRowStatus (u4CType, u4TeClass, TE_DESTROY)
            == SNMP_FAILURE)
        {
            continue;
        }
    }

    if (nmhTestv2FsMplsDsTeClassTypeRowStatus (&u4ErrorCode, u4ClassType,
                                               TE_DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to Destroy Class Type Entry\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsDsTeClassTypeRowStatus (u4ClassType, TE_DESTROY)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to Destroy Class Type Entry\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeCliSetPreConfExpPhbIndex
 * Description   : This routine is used to configure the ElspMapindex to be used
 *                 in the case of preconfigured services.
 * Input(s)      : CliHandle  - Cli Context Handle
 *                 u4ExpBits - Exp bits value
 *                 u4Phb - PHB value
 * Output(s)     : None
 *
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ******************************************************************************/
INT1
TeCliSetPreConfExpPhbIndex (tCliHandle CliHandle, INT4 i4PreConfIndex)
{
    UINT4               u4ErrorCode = TE_ZERO;

    if (nmhTestv2FsMplsDiffServElspPreConfExpPhbMapIndex (&u4ErrorCode,
                                                          i4PreConfIndex) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to set the pre-configured index\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsDiffServElspPreConfExpPhbMapIndex (i4PreConfIndex)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to set the pre-configured index\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeCliUnMapPreConfExpPhbIndex
 * Description   : This routine is used to unmap the ElspMapIndex 
 *                 in the case of preconfigured services.
 * Input(s)      : CliHandle  - Cli Context Handle
 *                 u4ExpBits - Exp bits value
 *                 u4Phb - PHB value
 * Output(s)     : None
 *
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ******************************************************************************/
INT1
TeCliUnMapPreConfExpPhbIndex (tCliHandle CliHandle, INT4 i4PreConfIndex)
{
    UINT4               u4ErrorCode = TE_ZERO;

    if (nmhTestv2FsMplsDiffServElspPreConfExpPhbMapIndex (&u4ErrorCode,
                                                          MPLS_INVALID_EXP_PHB_MAP_INDEX)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to set the pre-configured index\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsDiffServElspPreConfExpPhbMapIndex
        (MPLS_INVALID_EXP_PHB_MAP_INDEX) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to set the pre-configured index\r\n");
        return CLI_FAILURE;
    }

    UNUSED_PARAM (i4PreConfIndex);

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeCliMplsSetExpToPhbMapping
 * Description   : This routine is used to configure Exp To PHB mapping
 * Input(s)      : CliHandle  - Cli Context Handle
 *                 u4ExpBits - Exp bits value
 *                 u4Phb - PHB value
 * Output(s)     : None
 *
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ******************************************************************************/
INT1
TeCliMplsSetExpToPhbMapping (tCliHandle CliHandle, UINT4 u4ExpBits, UINT4 u4Phb)
{
    UINT4               u4ErrorCode = TE_ZERO;
    INT4                i4NextElspMapId = TE_ZERO;

    i4NextElspMapId = (INT4) gaTeCliArgs[CliHandle].u4ElspMapIndex;

    if (nmhTestv2FsMplsDiffServElspMapStatus (&u4ErrorCode, i4NextElspMapId,
                                              (INT4) u4ExpBits,
                                              (INT4) CREATE_AND_WAIT) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to Create EXP to PHB mapping\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsDiffServElspMapStatus (i4NextElspMapId, u4ExpBits,
                                           CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to Create EXP to PHB mapping\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMplsDiffServElspMapPhbDscp (&u4ErrorCode, i4NextElspMapId,
                                               (INT4) u4ExpBits,
                                               (INT4) u4Phb) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to Configure EXP to PHB mapping\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsDiffServElspMapPhbDscp (i4NextElspMapId, u4ExpBits, u4Phb)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to Configure EXP to PHB mapping\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMplsDiffServElspMapStatus (&u4ErrorCode, i4NextElspMapId,
                                              (INT4) u4ExpBits,
                                              (INT4) ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to Activate EXP to PHB mapping\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsDiffServElspMapStatus
        (i4NextElspMapId, (INT4) u4ExpBits, ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to Activate EXP to PHB mapping\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeCliMplsDestroyExpToPhbMapping
 * Description   : This routine is used to destroy the entry of 
 *                 Exp To PHB mapping
 * Input(s)      : CliHandle  - Cli Context Handle
 *                 u4ExpBits - Exp bits value
 *                 u4Phb - PHB value
 * Output(s)     : None
 *
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ******************************************************************************/
INT1
TeCliMplsDestroyExpToPhbMapping (tCliHandle CliHandle, UINT4 u4ExpBits)
{
    UINT4               u4ErrorCode = TE_ZERO;
    INT4                i4NextElspMapId = TE_ZERO;

    i4NextElspMapId = (INT4) gaTeCliArgs[CliHandle].u4ElspMapIndex;

    if (nmhTestv2FsMplsDiffServElspMapStatus (&u4ErrorCode, i4NextElspMapId,
                                              (INT4) u4ExpBits,
                                              (INT4) TE_DESTROY) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to Delete EXP to PHB mapping\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsDiffServElspMapStatus
        (i4NextElspMapId, u4ExpBits, TE_DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to Delete EXP to PHB mapping\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeCliMplsSetElspInfoPhbMapping
 * Description   : This routine is used to configure ElspInfoPHB mapping 
 *                 for Signalled services
 * Input(s)      : CliHandle  - Cli Context Handle
 *                 u4ExpBits - Exp bits value
 *                 u4Phb - PHB value
 * Output(s)     : None
 *
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ******************************************************************************/
INT1
TeCliMplsSetElspInfoPhbMapping (tCliHandle CliHandle, UINT4 u4ExpBits,
                                UINT4 u4Phb)
{

    UINT4               u4ErrorCode = TE_ZERO;
    INT4                i4NextElspInfoListId = TE_ZERO;

    i4NextElspInfoListId = (INT4) gaTeCliArgs[CliHandle].u4ElspInfoIndex;

    if (SNMP_FAILURE == nmhTestv2FsMplsDiffServElspInfoRowStatus (&u4ErrorCode,
                                                                  i4NextElspInfoListId,
                                                                  (INT4)
                                                                  u4ExpBits,
                                                                  (INT4)
                                                                  CREATE_AND_WAIT))
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to Create ElspInfo PHB mapping\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsDiffServElspInfoRowStatus (i4NextElspInfoListId, u4ExpBits,
                                               CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to Create ElspInfo PHB mapping\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMplsDiffServElspInfoPHB (&u4ErrorCode, i4NextElspInfoListId,
                                            (INT4) u4ExpBits,
                                            (INT4) u4Phb) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to Configure ElspInfo PHB mapping\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsDiffServElspInfoPHB (i4NextElspInfoListId, u4ExpBits, u4Phb)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to Configure ElspInfo PHB mapping\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMplsDiffServElspInfoRowStatus
        (&u4ErrorCode, i4NextElspInfoListId, (INT4) u4ExpBits,
         (INT4) ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to Activate ElspInfo PHB mapping\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsDiffServElspInfoRowStatus (i4NextElspInfoListId, u4ExpBits,
                                               ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to Activate ElspInfo PHB mapping\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeCliMplsDestroyElspInfoPhbMapping
 * Description   : This routine is used to Destroy the entry for 
                   ElspInfoPHB mapping for Signalled services.
 * Input(s)      : CliHandle  - Cli Context Handle
 *                 u4ExpBits - Exp bits value
 *                 u4Phb - PHB value
 * Output(s)     : None
 *
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ******************************************************************************/
INT1
TeCliMplsDestroyElspInfoPhbMapping (tCliHandle CliHandle, UINT4 u4ExpBits)
{
    UINT4               u4ErrorCode = TE_ZERO;
    UINT4               u4NextElspInfoListId = TE_ZERO;

    u4NextElspInfoListId = gaTeCliArgs[CliHandle].u4ElspInfoIndex;

    if (SNMP_FAILURE == nmhTestv2FsMplsDiffServElspInfoRowStatus (&u4ErrorCode,
                                                                  (INT4)
                                                                  u4NextElspInfoListId,
                                                                  (INT4)
                                                                  u4ExpBits,
                                                                  TE_DESTROY))
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to Delete ElspInfo PHB mapping\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsDiffServElspInfoRowStatus (u4NextElspInfoListId, u4ExpBits,
                                               TE_DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to Delete ElspInfo PHB mapping\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeCliMplsSetDsTeClassTcMap
 * Description   : This routine is used to configure Class Type to Traffic class
 *                 Mapping
 * Input(s)      : CliHandle  - Cli Context Handle
 *                 u4ClassMapType - Traffic Class Map Type
 *                 pu1Description - Class Description
 * Output(s)     : None
 *
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ******************************************************************************/
INT1
TeCliMplsSetDsTeClassTcMap (tCliHandle CliHandle, UINT4 u4ClassMapType,
                            UINT1 *pu1Description)
{
    tSNMP_OCTET_STRING_TYPE TcMapDesc;
    tMplsDsTeClassTypeTcMap TrafficClassEntry;
    UINT4               u4TcMapIndex = TE_ZERO;
    UINT4               u4ClassType = TE_ZERO;
    UINT4               u4ErrorCode = TE_ZERO;
    UINT4               u4TempTcIndex = TE_ZERO;
    BOOL1               bEntryDeletionReqd = FALSE;

    MEMSET (&TcMapDesc, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    MplsDsTeCliGetOctetString (pu1Description, &TcMapDesc);

    u4ClassType = (UINT4) gaTeCliArgs[CliHandle].i4ClassType;

    /*Get next Traffic Class index for traffic class table */
    for (u4TempTcIndex = TE_ZERO;
         u4TempTcIndex < MPLS_MAX_TRAFFIC_CLASS_ENTRIES; u4TempTcIndex++)
    {
        TrafficClassEntry = MPLS_DIFFSERV_TRAFFIC_CLASS_MAP (MPLS_DEF_INCARN,
                                                             u4ClassType,
                                                             u4TempTcIndex);
        if ((TrafficClassEntry.i4DsTeClassTypeToTcRowStatus !=
             MPLS_STATUS_ACTIVE)
            && (TrafficClassEntry.i4DsTeClassTypeToTcRowStatus !=
                MPLS_STATUS_NOT_READY))
        {
            u4TcMapIndex = u4TempTcIndex;
            break;
        }
    }

    if (u4TempTcIndex == MPLS_MAX_TRAFFIC_CLASS_ENTRIES)
    {
        CliPrintf (CliHandle, "\r\n%%Index cant be obtained : "
                   "No free index exists\r\n");
        return CLI_FAILURE;
    }

    do
    {
        if (nmhTestv2FsMplsDsTeTcMapRowStatus (&u4ErrorCode, u4ClassType,
                                               u4TcMapIndex, CREATE_AND_WAIT)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to Create Class type to Traffic Class"
                       "mapping\r\n");
            break;
        }

        if (nmhSetFsMplsDsTeTcMapRowStatus
            (u4ClassType, u4TcMapIndex, CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to Create Class type to Traffic Class"
                       "mapping\r\n");
            break;
        }

        if (nmhTestv2FsMplsDsTeTcType (&u4ErrorCode, u4ClassType, u4TcMapIndex,
                                       (INT4) u4ClassMapType) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to Configure Class type to Traffic Class"
                       "mapping\r\n");

            bEntryDeletionReqd = TRUE;
            break;
        }
        if (nmhSetFsMplsDsTeTcType
            (u4ClassType, u4TcMapIndex, (INT4) u4ClassMapType) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to Configure Class type to Traffic Class"
                       "mapping\r\n");

            bEntryDeletionReqd = TRUE;
            break;
        }

        if (nmhTestv2FsMplsDsTeTcDescription
            (&u4ErrorCode, u4ClassType, u4TcMapIndex,
             &TcMapDesc) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to Configure Class type to Traffic Class"
                       "mapping Description\r\n");

            bEntryDeletionReqd = TRUE;
            break;
        }

        if (nmhSetFsMplsDsTeTcDescription
            (u4ClassType, u4TcMapIndex, &TcMapDesc) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to Configure Class type to Traffic Class"
                       "mapping Description\r\n");

            bEntryDeletionReqd = TRUE;
            break;
        }

        if (nmhTestv2FsMplsDsTeTcMapRowStatus (&u4ErrorCode, u4ClassType,
                                               u4TcMapIndex, ACTIVE)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to Activate Class Type to Traffic Class"
                       "mapping\r\n");

            bEntryDeletionReqd = TRUE;
            break;
        }

        if (nmhSetFsMplsDsTeTcMapRowStatus (u4ClassType, u4TcMapIndex, ACTIVE)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to Activate Class Type to Traffic Class"
                       "mapping\r\n");

            bEntryDeletionReqd = TRUE;
            break;
        }
    }
    while (TE_ZERO);

    if (bEntryDeletionReqd == TRUE)
    {
        if (nmhSetFsMplsDsTeTcMapRowStatus (u4ClassType, u4TcMapIndex, DESTROY)
            == SNMP_FAILURE)
        {
            /* Failure Check Not handled */
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeCliMplsDeleteDsTeClassTcMap
 * Description   : This routine is used to Delete Class Type to Traffic class
 *                 Mapping
 * Input(s)      : CliHandle  - Cli Context Handle
 *                 u4ClassMapType - Diff Serv Class Type
 * Output(s)     : None
 *
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ******************************************************************************/
INT1
TeCliMplsDeleteDsTeClassTcMap (tCliHandle CliHandle, UINT4 u4ClassMapType)
{
    UINT4               u4ErrorCode = TE_ZERO;
    UINT4               u4ClassType = TE_ZERO;
    UINT4               u4TcMapIndex = TE_ZERO;
    tMplsDsTeClassTypeTcMap TrafficClassEntry;

    /*get class index */
    u4ClassType = (UINT4) gaTeCliArgs[CliHandle].i4ClassType;

    for (u4TcMapIndex = TE_ZERO;
         u4TcMapIndex < MPLS_MAX_TRAFFIC_CLASS_ENTRIES; u4TcMapIndex++)
    {
        TrafficClassEntry = MPLS_DIFFSERV_TRAFFIC_CLASS_MAP (MPLS_DEF_INCARN,
                                                             u4ClassType,
                                                             u4TcMapIndex);

        /* found TcIndex for TC Type entered */
        if (TrafficClassEntry.u4DsTeTcType == u4ClassMapType)
        {
            break;
        }
    }

    if (u4TcMapIndex == MPLS_MAX_TRAFFIC_CLASS_ENTRIES)
    {
        CliPrintf (CliHandle, "\r\n%%Invalid Traffic Class Value\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMplsDsTeTcMapRowStatus (&u4ErrorCode, u4ClassType,
                                           u4TcMapIndex, DESTROY)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to destroy Class type to Traffic"
                   "Class mapping\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsDsTeTcMapRowStatus (u4ClassType, u4TcMapIndex, DESTROY)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to destroy Class type to Traffic"
                   "Class mapping\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeCliMplsSetDsteTeClass
 * Description   : This routine is used to Create class type to TE class
 *                 Mapping
 * Input(s)      : CliHandle  - Cli Context Handle
 *                 u4TePriority - TE priority
 *                 u4TeClassNum - TE class number
 *                 pu1Description - Description for TE class mapping
 * Output(s)     : None
 *
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ******************************************************************************/
INT1
TeCliMplsSetDsteTeClass (tCliHandle CliHandle, UINT4 u4TePriority,
                         UINT4 u4TeClassNum, UINT1 *pu1Description)
{
    UINT4               u4ErrorCode = TE_ZERO;
    UINT4               u4ClassType = TE_ZERO;
    tSNMP_OCTET_STRING_TYPE TeClassMapDesc;

    MEMSET (&TeClassMapDesc, 0, sizeof (tSNMP_OCTET_STRING_TYPE));

    u4ClassType = (UINT4) gaTeCliArgs[CliHandle].i4ClassType;

    MplsDsTeCliGetOctetString (pu1Description, &TeClassMapDesc);

    if (nmhTestv2FsMplsDsTeTeClassRowStatus (&u4ErrorCode, u4ClassType,
                                             u4TePriority, CREATE_AND_WAIT)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to create Class type to TE"
                   "Class Mapping\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsDsTeTeClassRowStatus (u4ClassType, u4TePriority,
                                          CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to create Class type to TE"
                   "Class Mapping\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMplsDsTeTeClassDesc (&u4ErrorCode, u4ClassType,
                                        u4TePriority,
                                        &TeClassMapDesc) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to Configure Class type to TE"
                   "Class Mapping Description\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsDsTeTeClassDesc (u4ClassType, u4TePriority, &TeClassMapDesc)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to Configure Class type to TE"
                   "Class Mapping Description\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMplsDsTeTeClassNumber (&u4ErrorCode, u4ClassType,
                                          u4TePriority, u4TeClassNum) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to configure Class number for "
                   "Class type to TE Class Mapping\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsDsTeTeClassNumber (u4ClassType, u4TePriority, u4TeClassNum)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to configure Class number for "
                   "Class type to TE Class Mapping\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMplsDsTeTeClassRowStatus (&u4ErrorCode, u4ClassType,
                                             u4TePriority, ACTIVE)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to activate Class type to TE"
                   "Class Mapping\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsDsTeTeClassRowStatus (u4ClassType, u4TePriority, ACTIVE)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to activate Class type to TE"
                   "Class Mapping\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeCliMplsDeleteDsteTeClass
 * Description   : This routine is used to delete class type to TE class
 *                 Mapping
 * Input(s)      : CliHandle  - Cli Context Handle
 *                 u4TePriority - TE priority
 * Output(s)     : None
 *
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ******************************************************************************/
INT1
TeCliMplsDeleteDsteTeClass (tCliHandle CliHandle, UINT4 u4TePriority)
{
    UINT4               u4ClassType = TE_ZERO;
    UINT4               u4ErrorCode = TE_ZERO;

    u4ClassType = (UINT4) gaTeCliArgs[CliHandle].i4ClassType;

    if (nmhTestv2FsMplsDsTeTeClassRowStatus (&u4ErrorCode, u4ClassType,
                                             u4TePriority, DESTROY)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to activate Class type to TE"
                   "Class Mapping\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsDsTeTeClassRowStatus (u4ClassType, u4TePriority, DESTROY)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to activate Class type to TE"
                   "Class Mapping\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeCliMplsSetDiffServElspType
 * Description   : This routine is used to configure LSP type
 * Input(s)      : CliHandle  - Cli Context Handle
 *                 u4LspType - LSP type
 * Output(s)     : None
 *
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ******************************************************************************/
INT1
TeCliMplsSetDiffServElspType (tCliHandle CliHandle, UINT4 u4LspType)
{
    UINT4               u4ErrorCode = TE_ZERO;
    UINT4               u4TunnelIndex = TE_ZERO;
    UINT4               u4TunnelInstance = TE_ZERO;
    INT4                i4RetValTunnelRowStatus = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;

    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                   u4EgressId,
                                   &i4RetValTunnelRowStatus) == SNMP_FAILURE)
    {
        TE_DBG (TE_EXTN_FAIL, "\r\n%%Failed to get tunnel information\r\n");
    }

    if (i4RetValTunnelRowStatus == ACTIVE)
    {
        if (nmhTestv2MplsTunnelRowStatus (&u4ErrorCode, u4TunnelIndex,
                                          u4TunnelInstance, u4IngressId,
                                          u4EgressId, NOT_IN_SERVICE) ==
            SNMP_FAILURE)
        {

            CliPrintf (CliHandle, "\r\n%%Unable to make Tunnel RowStatus"
                       "as Not In Service\r\n");
            return CLI_FAILURE;
        }
    }

    if (nmhSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                   u4EgressId, NOT_IN_SERVICE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to make Tunnel RowStatus"
                   "as Not In Service\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMplsDiffServRowStatus (&u4ErrorCode, u4TunnelIndex,
                                          u4TunnelInstance, u4IngressId,
                                          u4EgressId, NOT_IN_SERVICE) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to make DiffServ RowStatus as Not in Service\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsDiffServRowStatus (u4TunnelIndex, u4TunnelInstance,
                                       u4IngressId, u4EgressId, NOT_IN_SERVICE)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to make DiffServ RowStatus as Not in Service\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMplsDiffServElspType (&u4ErrorCode, u4TunnelIndex,
                                         u4TunnelInstance, u4IngressId,
                                         u4EgressId,
                                         (INT4) u4LspType) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to Set Tunnel LSP type\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsDiffServElspType
        (u4TunnelIndex, u4TunnelInstance, u4IngressId, u4EgressId,
         u4LspType) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to Set Tunnel LSP type\r\n");
        return CLI_FAILURE;
    }

    TeCliSetMplsTunnelOldBandwidth (u4TunnelIndex, u4TunnelInstance,
                                    u4IngressId, u4EgressId);
    if (i4RetValTunnelRowStatus != TE_NOTREADY)
    {
        if (TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                         u4IngressId, u4EgressId,
                                         i4RetValTunnelRowStatus) ==
            CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Cannot Set Mpls tunnel RowStatus\r\n");
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeCliMplsAssociateExpPhbMap
 * Description   : This routine is used to configure Exp to Phb mapping
 * Input(s)      : CliHandle  - Cli Context Handle
 *                 u4MapIndex - EXP to PHB map index
 * Output(s)     : None
 *
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ******************************************************************************/
INT1
TeCliMplsAssociateExpPhbMap (tCliHandle CliHandle, UINT4 u4MapIndex)
{
    UINT4               u4ErrorCode = TE_ZERO;
    UINT4               u4TunnelIndex = TE_ZERO;
    UINT4               u4TunnelInstance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    INT4                i4DiffservRowStatus = TE_ZERO;

    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    nmhGetFsMplsDiffServRowStatus (u4TunnelIndex, u4TunnelInstance,
                                   u4IngressId, u4EgressId,
                                   &i4DiffservRowStatus);

    if (i4DiffservRowStatus == TE_ACTIVE)
    {
        if (nmhTestv2FsMplsDiffServRowStatus (&u4ErrorCode, u4TunnelIndex,
                                              u4TunnelInstance,
                                              u4IngressId, u4EgressId,
                                              TE_NOTINSERVICE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Unable to make Diffserv Entry as NOT IN SERVICE\n");
            return CLI_FAILURE;
        }

        if (nmhSetFsMplsDiffServRowStatus (u4TunnelIndex, u4TunnelInstance,
                                           u4IngressId, u4EgressId,
                                           TE_NOTINSERVICE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Unable to make Diffserv Entry as NOT IN SERVICE\n");
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsMplsDiffServElspListIndex (&u4ErrorCode, u4TunnelIndex,
                                              u4TunnelInstance, u4IngressId,
                                              u4EgressId, (INT4) u4MapIndex) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to Set ElspInfoPhb Mapping index\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsDiffServElspListIndex (u4TunnelIndex, u4TunnelInstance,
                                           u4IngressId, u4EgressId, u4MapIndex)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to Set ElspInfoPhb Mapping index\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMplsDiffServRowStatus (&u4ErrorCode, u4TunnelIndex,
                                          u4TunnelInstance, u4IngressId,
                                          u4EgressId, ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to Activate DiffServ RowStatus\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsDiffServRowStatus
        (u4TunnelIndex, u4TunnelInstance, u4IngressId, u4EgressId,
         ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to Activate DiffServ RowStatus\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeCliMplsDiffServLlspPsc
 * Description   : This routine is used to configure Exp to Phb mapping
 * Input(s)      : CliHandle  - Cli Context Handle
 *                 u4PscValue - PSC value for DiffServ Tunnel
 * Output(s)     : None
 *
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ******************************************************************************/
INT1
TeCliMplsDiffServLlspPsc (tCliHandle CliHandle, UINT4 u4PscValue)
{
    UINT4               u4ErrorCode = TE_ZERO;
    UINT4               u4TunnelIndex = TE_ZERO;
    UINT4               u4TunnelInstance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    INT4                i4DiffServRowStatus = TE_ZERO;
    INT4                i4RetValTunnelRowStatus = TE_ZERO;

    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                   u4EgressId,
                                   &i4RetValTunnelRowStatus) == SNMP_FAILURE)
    {
        TE_DBG (TE_EXTN_FAIL, "\r\n%%Failed to get tunnel information\r\n");
    }

    if (i4RetValTunnelRowStatus == ACTIVE)
    {
        if (nmhTestv2MplsTunnelRowStatus (&u4ErrorCode, u4TunnelIndex,
                                          u4TunnelInstance, u4IngressId,
                                          u4EgressId, NOT_IN_SERVICE) ==
            SNMP_FAILURE)
        {

            CliPrintf (CliHandle, "\r\n%%Unable to make Tunnel RowStatus"
                       "as Not In Service\r\n");
            return CLI_FAILURE;
        }
    }

    if (nmhSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                   u4IngressId, u4EgressId, NOT_IN_SERVICE)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to make Tunnel RowStatus"
                   "as Not In Service\r\n");
        return CLI_FAILURE;
    }

    nmhGetFsMplsDiffServRowStatus (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                   u4EgressId, &i4DiffServRowStatus);

    if (i4DiffServRowStatus == ACTIVE)
    {
        if (nmhTestv2FsMplsDiffServRowStatus (&u4ErrorCode, u4TunnelIndex,
                                              u4TunnelInstance, u4IngressId,
                                              u4EgressId, TE_NOTINSERVICE) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to Activate DiffServ RowStatus\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetFsMplsDiffServRowStatus (u4TunnelIndex, u4TunnelInstance,
                                           u4IngressId, u4EgressId,
                                           TE_NOTINSERVICE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to Activate DiffServ RowStatus\r\n");
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhTestv2FsMplsDiffServRowStatus (&u4ErrorCode, u4TunnelIndex,
                                              u4TunnelInstance, u4IngressId,
                                              u4EgressId, CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to Activate DiffServ RowStatus\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetFsMplsDiffServRowStatus (u4TunnelIndex, u4TunnelInstance,
                                           u4IngressId, u4EgressId,
                                           CREATE_AND_WAIT) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to Activate DiffServ RowStatus\r\n");
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2FsMplsDiffServLlspPsc (&u4ErrorCode, u4TunnelIndex,
                                        u4TunnelInstance, u4IngressId,
                                        u4EgressId,
                                        (INT4) u4PscValue) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to Set PSC for LLSP tunnel\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsDiffServLlspPsc
        (u4TunnelIndex, u4TunnelInstance, u4IngressId, u4EgressId,
         u4PscValue) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to Set PSC for LLSP tunnel\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMplsDiffServRowStatus (&u4ErrorCode, u4TunnelIndex,
                                          u4TunnelInstance, u4IngressId,
                                          u4EgressId, ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to Activate DiffServ RowStatus\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsDiffServRowStatus (u4TunnelIndex, u4TunnelInstance,
                                       u4IngressId, u4EgressId, ACTIVE)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to Activate DiffServ RowStatus\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeCliMplsDsTeShowStatus
 * Description   : This routine is used to show Dste Status.
 * Input(s)      : CliHandle  - Cli Context Handle
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ******************************************************************************/
INT4
TeCliMplsDsTeShowStatus (tCliHandle CliHandle)
{
    INT1                i1Result;
    INT4                i4DsTeStatus;

    i1Result = nmhGetFsMplsDsTeStatus (&i4DsTeStatus);
    if (i1Result == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%FAIL TO GET DIFFSERV TE STATUS\r\n");
        return CLI_FAILURE;

    }

    if (i4DsTeStatus == MPLS_DSTE_STATUS_ENABLE)
    {
        CliPrintf (CliHandle, "\r\n DiffServ-TE STATUS : ENABLE\r\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r\n DiffServ-TE STATUS : DISABLE\r\n");
    }
    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeCliMplsDiffServShowClass
 * Description   : This function is used to display DiffServ Class Information
 * Input(s)      : CliHandle  - Cli Context Handle
 *                 u4ClassType - Class Type
 * Output(s)     : None
 *
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ******************************************************************************/
INT1
TeCliMplsDiffServShowClass (tCliHandle CliHandle, UINT4 u4ClassType)
{
    UINT4               u4TrafficClassIndex = TE_ZERO;
    UINT4               u4TeClassPrio = TE_ZERO;
    UINT4               u4TeClassNo = TE_ZERO;
    INT1                i1Result = SNMP_FAILURE;
    INT4                i4TrafficClassType = TE_ZERO;
    INT4                i4RowStatus = TE_ZERO;

    tSNMP_OCTET_STRING_TYPE ClassDescription;
    tSNMP_OCTET_STRING_TYPE TrafficClassDescription;
    tSNMP_OCTET_STRING_TYPE TeClassDescription;

    static UINT1        au1Class[MPLS_MAX_DSTE_MAX_STRING_LEN];
    static UINT1        au1TrafficClass[MPLS_MAX_DSTE_MAX_STRING_LEN];
    static UINT1        au1TeClass[MPLS_MAX_DSTE_MAX_STRING_LEN];

    /*Allocate memory for string */
    MEMSET (au1Class, TE_ZERO, MPLS_MAX_DSTE_MAX_STRING_LEN);
    ClassDescription.pu1_OctetList = au1Class;

    MEMSET (au1TrafficClass, TE_ZERO, MPLS_MAX_DSTE_MAX_STRING_LEN);
    TrafficClassDescription.pu1_OctetList = au1TrafficClass;

    MEMSET (au1TeClass, TE_ZERO, MPLS_MAX_DSTE_MAX_STRING_LEN);
    TeClassDescription.pu1_OctetList = au1TeClass;

    /*get Class Table entry */
    /*get row status of Class Table */
    i1Result = nmhGetFsMplsDsTeClassTypeRowStatus (u4ClassType, &i4RowStatus);
    if (i1Result == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n DiffServ-TE CLASS entry doesnt exist \r\n");
        return CLI_FAILURE;
    }

    i1Result =
        nmhGetFsMplsDsTeClassTypeDescription (u4ClassType, &ClassDescription);
    if (i1Result == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\n\nDiffServ-TE CLASS %d : %s ", u4ClassType,
                   " ");
    }
    else
    {
        CliPrintf (CliHandle, "\n\nDiffServ-TE CLASS %d : %s ", u4ClassType,
                   ClassDescription.pu1_OctetList);
    }
    MEMSET (au1Class, TE_ZERO, MPLS_MAX_DSTE_MAX_STRING_LEN);

    /*get all Traffic-Class Table entries */
    /*get row status of Traffic-Class Table */
    for (u4TrafficClassIndex = TE_ZERO;
         u4TrafficClassIndex < MPLS_MAX_TRAFFIC_CLASS_ENTRIES;
         u4TrafficClassIndex++)
    {

        i1Result =
            nmhGetFsMplsDsTeTcMapRowStatus (u4ClassType, u4TrafficClassIndex,
                                            &i4RowStatus);
        if (i1Result == SNMP_FAILURE)
        {
            continue;
        }

        else
        {
            CliPrintf (CliHandle, "\nTRAFFIC CLASS ID %d ",
                       u4TrafficClassIndex);
            /*get traffic class type */
            i1Result =
                nmhGetFsMplsDsTeTcType (u4ClassType, u4TrafficClassIndex,
                                        &i4TrafficClassType);
            if (i1Result == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\n\tTRAFFIC CLASS TYPE :   ");
            }
            else
            {
                switch (i4TrafficClassType)
                {

                    case MPLS_DIFFSERV_DF_DSCP:
                        CliPrintf (CliHandle, "\n\tTRAFFIC CLASS TYPE : DF ");
                        break;

                    case MPLS_DIFFSERV_CS1_DSCP:
                        CliPrintf (CliHandle, "\n\tTRAFFIC CLASS TYPE : CS1 ");
                        break;

                    case MPLS_DIFFSERV_CS2_DSCP:
                        CliPrintf (CliHandle, "\n\tTRAFFIC CLASS TYPE : CS2 ");
                        break;

                    case MPLS_DIFFSERV_CS3_DSCP:
                        CliPrintf (CliHandle, "\n\tTRAFFIC CLASS TYPE : CS3  ");
                        break;
                    case MPLS_DIFFSERV_CS4_DSCP:
                        CliPrintf (CliHandle, "\n\tTRAFFIC CLASS TYPE : CS4  ");
                        break;

                    case MPLS_DIFFSERV_CS5_DSCP:
                        CliPrintf (CliHandle, "\n\tTRAFFIC CLASS TYPE : CS5  ");
                        break;

                    case MPLS_DIFFSERV_CS6_DSCP:
                        CliPrintf (CliHandle, "\n\tTRAFFIC CLASS TYPE : CS6  ");
                        break;

                    case MPLS_DIFFSERV_CS7_DSCP:
                        CliPrintf (CliHandle, "\n\tTRAFFIC CLASS TYPE : CS7  ");
                        break;

                    case MPLS_DIFFSERV_EF1_DSCP:
                        CliPrintf (CliHandle, "\n\tTRAFFIC CLASS TYPE : EF1  ");
                        break;

                    case MPLS_DIFFSERV_EF2_DSCP:
                        CliPrintf (CliHandle, "\n\tTRAFFIC CLASS TYPE : EF  ");
                        break;

                    case MPLS_DIFFSERV_AF11_DSCP:
                        CliPrintf (CliHandle, "\n\tTRAFFIC CLASS TYPE : AF1 ");
                        break;

                    case MPLS_DIFFSERV_AF21_DSCP:
                        CliPrintf (CliHandle, "\n\t TRAFFIC CLASS TYPE : AF2 ");
                        break;

                    case MPLS_DIFFSERV_AF31_DSCP:
                        CliPrintf (CliHandle,
                                   "\n\t TRAFFIC CLASS TYPE : AF3  ");
                        break;
                    case MPLS_DIFFSERV_AF41_DSCP:
                        CliPrintf (CliHandle,
                                   "\n\t TRAFFIC CLASS TYPE : AF4  ");
                        break;

                    default:
                        CliPrintf (CliHandle, "\n\t TRAFFIC CLASS TYPE :   ");

                }
            }
            /*get traffic class description */
            i1Result =
                nmhGetFsMplsDsTeTcDescription (u4ClassType, u4TrafficClassIndex,
                                               &TrafficClassDescription);
            if (i1Result == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\n\tDESCRIPTION : ");
            }
            else
            {
                CliPrintf (CliHandle, "\n\t DESCRIPTION : %s",
                           TrafficClassDescription.pu1_OctetList);
            }
            MEMSET (au1TrafficClass, TE_ZERO, MPLS_MAX_DSTE_MAX_STRING_LEN);
            TrafficClassDescription.pu1_OctetList = au1TrafficClass;

        }

    }

    /*get TE Class Table entry */
    /*get row status of TE Class Table */
    for (u4TeClassPrio = TE_ZERO; u4TeClassPrio < MPLS_MAX_TE_CLASS_ENTRIES;
         u4TeClassPrio++)
    {
        i1Result =
            nmhGetFsMplsDsTeTeClassRowStatus (u4ClassType, u4TeClassPrio,
                                              &i4RowStatus);
        if ((i1Result == SNMP_FAILURE) || (i4RowStatus == MPLS_ZERO))
        {
            continue;
        }

        else
        {
            /*get TE class */
            i1Result =
                nmhGetFsMplsDsTeTeClassNumber (u4ClassType, u4TeClassPrio,
                                               &u4TeClassNo);
            if (i1Result == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\nTE CLASS NUMBER ");
            }
            else
            {
                CliPrintf (CliHandle, "\nTE CLASS NUMBER %d", u4TeClassNo);
            }

            /*get TE class description */
            i1Result =
                nmhGetFsMplsDsTeTeClassDesc (u4ClassType, u4TeClassPrio,
                                             &TeClassDescription);
            if (i1Result == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\n\tDESCRIPTION : ");
            }
            else
            {
                CliPrintf (CliHandle, " \n\t DESCRIPTION : %s ",
                           TeClassDescription.pu1_OctetList);
            }
            MEMSET (au1TeClass, TE_ZERO, MPLS_MAX_DSTE_MAX_STRING_LEN);
            CliPrintf (CliHandle, "\n\t CLASS :  %d ", u4ClassType);
            CliPrintf (CliHandle, "\n\t PRIORITY :  %d \n", u4TeClassPrio);
        }

    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliStaticBindIngress
* Description   : This routine is used to associate XC Entries with that of tunnel. 
* Input(s)      : CliHandle       - Cli Context Handle
*               : u4OutLabel       - Label associated with the outgoing packet
*               : u4NextHop        - Interface Index for the outgoing MPLS interface.  
*                 u4OutIfIndex    - Outgoing interface
*                 u4MplsTnlDir - MPLS tunnel direction (forward/reverse)
*                 u1TnlPathType   - Tnl Path Type - Working or Protection
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT1
TeCliStaticBindIngress (tCliHandle CliHandle, UINT4 u4OutLabel, UINT4 u4NextHop,
                        UINT4 u4OutIfIndex, UINT4 u4MplsTnlDir,
                        UINT1 u1TnlPathType)
{

    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OCTET_STRING_TYPE NextHopAddr;
    tSNMP_OCTET_STRING_TYPE XCSegmentIndex;
    tSNMP_OCTET_STRING_TYPE MplsTeTnlType;    /* MPLS_P2MP_LSP_CHANGES */

    tSNMP_OID_TYPE      TunnelXCPointer;

    tSNMP_OID_TYPE      XCSegIndex;

    static UINT4        au4XCIndexx[MPLS_TE_XC_TABLE_OFFSET];

    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1NextHopAddr[MPLS_INDEX_LENGTH];

    /* MPLS_P2MP_LSP_CHANGES - S */
    static UINT1        au1TnlType[CLI_MPLS_TE_TNL_TYPE];
    /* MPLS_P2MP_LSP_CHANGES - E */

    static UINT4        au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 2, 1, 10, 1, 4, 4, 0, 0, 0, 0, 4, 0, 0, 0,
        0, 4, 0, 0, 0, 0
    };

    UINT4               u4TunnelIndex = 0;
    UINT4               u4TunnelInstance = 0;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;

    UINT4               u4XCTabIndex = 0;
    UINT4               u4InIndex = 0;
    UINT4               u4OutIndex = 0;

    UINT4               u4XCSegInd = 0;
    UINT4               u4InSegInd = 0;
    UINT4               u4OutSegInd = 0;

    UINT4               u4ErrorCode = 0;
    UINT4               u4MplsTnlIfIndex = 0;
    UINT4               u4AddressType = LSR_ADDR_UNKNOWN;
#ifdef CFA_WANTED
    UINT4               u4MplsIfIndex = 0;
#endif
    INT4                i4Direction = 0;
    INT4                i4TunnelMode = 0;
    INT4                i4IfIndex = 0;
    INT4                i4TnlRole = 0;
    INT4                i4PrevRowStatus = 0;

    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;
    UINT4               u4Mask = 0xffffffff;

    BOOL1               bXcDeleteFlag = TRUE;
    BOOL1               bNextHopCfg = FALSE;

    UINT4               u4CreatedInstance = TE_ZERO;
    BOOL1               bEntryCreated = FALSE;
    BOOL1               bTempFlag = FALSE;
    BOOL1               bIntfCreationReqd = TRUE;

    XCSegIndex.pu4_OidList = au4XCIndexx;
    XCSegIndex.u4_Length = 0;

    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    XCIndex.pu1_OctetList = au1XCIndex;
    XCIndex.i4_Length = MPLS_INDEX_LENGTH;
    NextHopAddr.pu1_OctetList = au1NextHopAddr;
    /* MPLS_P2MP_LSP_CHANGES - S */
    MplsTeTnlType.pu1_OctetList = au1TnlType;
    MplsTeTnlType.i4_Length = CLI_MPLS_TE_TNL_TYPE;
    /* MPLS_P2MP_LSP_CHANGES - E */

    if (u4OutIfIndex == 0)
    {
        bNextHopCfg = TRUE;
        u4AddressType = LSR_ADDR_IPV4;
        if (NetIpv4IfIsOurAddress (u4NextHop) == NETIPV4_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%%Next hop is same as self-ip addr\n");
            return CLI_FAILURE;
        }

        MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
        MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));
        RtQuery.u4DestinationIpAddress = u4NextHop;
        RtQuery.u4DestinationSubnetMask = u4Mask;
        RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;

        if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_SUCCESS)
        {
            if (NetIpv4GetCfaIfIndexFromPort (NetIpRtInfo.u4RtIfIndx,
                                              &u4OutIfIndex) == NETIPV4_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Unable to fetch Next hop information\n");
                return CLI_FAILURE;
            }
        }
#ifdef CFA_WANTED
        else
        {
            if (CfaIpIfGetIfIndexFromHostIpAddressInCxt (MPLS_DEF_CONTEXT_ID,
                                                         u4NextHop,
                                                         &u4OutIfIndex)
                == CFA_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Unable to fetch Next hop information\n");
                return CLI_FAILURE;
            }
        }
    }
    if (CfaUtilGetMplsIfFromIfIndex (u4OutIfIndex, &u4MplsIfIndex, TRUE) ==
        CFA_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% Enable MPLS over outgoing interface\r\n");
        return CLI_FAILURE;
    }
#else
    }
#endif

    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* MPLS_P2MP_LSP_CHANGES - S */
    /* Destination address is mandatory for P2MP tunnels. Reject configuration
     * if tunnel type is P2MP. */
    MEMSET (au1TnlType, TE_ZERO, CLI_MPLS_TE_TNL_TYPE);
    if (nmhGetFsMplsTunnelType (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                u4EgressId, &MplsTeTnlType) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%%Failed to retrieve tunnel type\r\n");
        return CLI_FAILURE;
    }
    if (MplsTeTnlType.pu1_OctetList[TE_ZERO] & TE_TNL_TYPE_P2MP)
    {
        CliPrintf (CliHandle, "\r\n%%Insufficient input for P2MP tunnel. "
                   "Configuration not allowed.\r\n");
        return CLI_FAILURE;
    }
    /* MPLS_P2MP_LSP_CHANGES - E */

    if (nmhGetFsMplsTunnelMode (u4TunnelIndex, u4TunnelInstance,
                                u4IngressId, u4EgressId, &i4TunnelMode)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Tunnel Mode is not available.\r\n");
        return CLI_FAILURE;
    }

    if (nmhGetMplsTunnelRole (u4TunnelIndex, u4TunnelInstance,
                              u4IngressId, u4EgressId,
                              &i4TnlRole) == SNMP_SUCCESS)
    {
        if ((i4TnlRole == TE_EGRESS) &&
            (i4TunnelMode != TE_TNL_MODE_COROUTED_BIDIRECTIONAL))
        {
            CliPrintf (CliHandle,
                       "\r\n%%Wrong label configuration for Egress tunnel\r\n");
            return CLI_FAILURE;
        }
        else if (i4TnlRole == TE_INTERMEDIATE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Wrong label configuration for Intermediate "
                       "tunnel\r\n");
            return CLI_FAILURE;
        }
    }

    if (nmhGetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                   u4IngressId, u4EgressId,
                                   &i4PrevRowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to get tunnel row status\r\n");
        return CLI_FAILURE;
    }

    if ((i4TunnelMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
        && (u4MplsTnlDir == MPLS_CLI_DIRECTION_REVERSE))
    {
        u4MplsTnlDir = MPLS_DIRECTION_REVERSE;
    }
    else
    {
        u4MplsTnlDir = MPLS_DIRECTION_FORWARD;
    }

    if (u4TunnelInstance == TE_ZERO)
    {
        if (TeCliNormalCreateNonZeroInstTunnel (CliHandle, u4TunnelIndex,
                                                u4TunnelInstance, u4IngressId,
                                                u4EgressId, u1TnlPathType,
                                                &u4CreatedInstance,
                                                &bEntryCreated) == CLI_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to create non-zero instance tunnel\n");
            return CLI_FAILURE;
        }

        bEntryCreated = TRUE;
        u4TunnelInstance = u4CreatedInstance;

        if (((i4TunnelMode == TE_TNL_MODE_UNIDIRECTIONAL) ||
             (i4TunnelMode == TE_TNL_MODE_ASSOCIATED_BIDIRECTIONAL) ||
             ((i4TnlRole == TE_INGRESS) &&
              (u4MplsTnlDir == MPLS_DIRECTION_FORWARD)) ||
             ((i4TnlRole == TE_EGRESS) &&
              (u4MplsTnlDir == MPLS_DIRECTION_REVERSE))) &&
            (u1TnlPathType == TE_TNL_WORKING_PATH))
        {
            u4MplsTnlIfIndex = gaTnlCliArgs[CliHandle].u4MplsTnlIfIndex;
            bIntfCreationReqd = FALSE;
        }
    }

    if (nmhGetMplsTunnelXCPointer (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                   u4EgressId, &XCSegIndex) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%TunnelXCPointer is not available .\r\n");

        TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                        u4TunnelInstance, u4IngressId,
                                        u4EgressId, bEntryCreated);
        return CLI_FAILURE;
    }

    MplsGetSegFromTunnelXC (&XCSegIndex, &XCSegmentIndex, &InSegmentIndex,
                            &OutSegmentIndex, &u4XCSegInd, &u4InSegInd,
                            &u4OutSegInd);

    /* This function is called for making static binding for outgoing tunnels 
     * in ingress node. For corouted bidirectional tunnels, we need to
     * check whether incoming tunnel exist.
     *
     * If it exist, do not delete XC table.
     * Instead just create another outsegment and associate it with XC table.
     *
     * If it does not exist create a new XC table entry.*/

    if ((i4TunnelMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL) &&
        (u4InSegInd != 0))
    {
        if ((nmhGetFsMplsInSegmentDirection (&InSegmentIndex,
                                             &i4Direction)
             == SNMP_SUCCESS) && (i4Direction != (INT4) u4MplsTnlDir))
        {
            bXcDeleteFlag = FALSE;
        }
    }

    if ((u4XCSegInd != 0) && (bXcDeleteFlag == TRUE))
    {
        /* Destroy the XC entry */
        if (nmhTestv2MplsXCRowStatus (&u4ErrorCode, &XCSegmentIndex,
                                      &InSegmentIndex,
                                      &OutSegmentIndex,
                                      MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_XC_ENTRY_DELETION);

            TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                            u4TunnelInstance, u4IngressId,
                                            u4EgressId, bEntryCreated);
            return CLI_FAILURE;
        }

        if (nmhSetMplsXCRowStatus
            (&XCSegmentIndex, &InSegmentIndex, &OutSegmentIndex,
             MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_XC_ENTRY_DELETION);

            TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                            u4TunnelInstance, u4IngressId,
                                            u4EgressId, bEntryCreated);
            return CLI_FAILURE;
        }
    }
    /* Destroy the OutSegment entry */

    if ((u4OutSegInd != 0) && (bXcDeleteFlag == TRUE))
    {
        nmhGetMplsOutSegmentInterface (&OutSegmentIndex, &i4IfIndex);
        MplsDeleteMplsIfOrMplsTnlIf (u4OutIfIndex, (UINT4) i4IfIndex,
                                     CFA_MPLS_TUNNEL, MPLS_TRUE);
        if (nmhTestv2MplsOutSegmentRowStatus (&u4ErrorCode,
                                              &OutSegmentIndex,
                                              MPLS_STATUS_DESTROY) ==
            SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_OUTSEG_ENTRY_DELETION);

            TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                            u4TunnelInstance, u4IngressId,
                                            u4EgressId, bEntryCreated);
            return CLI_FAILURE;
        }

        if (nmhSetMplsOutSegmentRowStatus
            (&OutSegmentIndex, MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_OUTSEG_ENTRY_DELETION);

            TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                            u4TunnelInstance, u4IngressId,
                                            u4EgressId, bEntryCreated);
            return CLI_FAILURE;
        }
    }

    MEMSET (au1InSegIndex, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1XCIndex, 0, MPLS_INDEX_LENGTH);
    /* create an OutSeg entry */
    if ((nmhGetMplsOutSegmentIndexNext (&OutSegmentIndex)) == SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_NO_FREE_INDEX);

        TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                        u4TunnelInstance, u4IngressId,
                                        u4EgressId, bEntryCreated);
        return CLI_FAILURE;
    }
    if ((nmhTestv2MplsOutSegmentRowStatus (&u4ErrorCode, &OutSegmentIndex,
                                           MPLS_STATUS_CREATE_AND_WAIT)) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_OUTSEG_ENTRY_CREATION);

        TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                        u4TunnelInstance, u4IngressId,
                                        u4EgressId, bEntryCreated);
        return CLI_FAILURE;
    }
    if ((nmhSetMplsOutSegmentRowStatus
         (&OutSegmentIndex, MPLS_STATUS_CREATE_AND_WAIT)) == SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_OUTSEG_ENTRY_CREATION);

        TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                        u4TunnelInstance, u4IngressId,
                                        u4EgressId, bEntryCreated);
        return CLI_FAILURE;
    }

    if (bXcDeleteFlag == TRUE)
    {
        /* create an XC entry */
        if ((nmhGetMplsXCIndexNext (&XCIndex)) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_NO_FREE_INDEX);
            /* delete the created OutSeg entry */
            if (nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                               MPLS_STATUS_DESTROY)
                == SNMP_FAILURE)
            {
                /* Failure check not handled */
            }

            TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                            u4TunnelInstance, u4IngressId,
                                            u4EgressId, bEntryCreated);
            return CLI_FAILURE;
        }
    }
    else
    {
        MPLS_INTEGER_TO_OCTETSTRING (u4XCSegInd, (&XCIndex));
        InSegmentIndex.pu1_OctetList = au1InSegIndex;
        InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
        MEMSET (au1InSegIndex, 0, MPLS_INDEX_LENGTH);
    }
    if ((nmhTestv2MplsXCRowStatus (&u4ErrorCode, &XCIndex, &InSegmentIndex,
                                   &OutSegmentIndex,
                                   MPLS_STATUS_CREATE_AND_WAIT))
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_XC_ENTRY_CREATION);
        /* delete the created InSeg OutSeg entry */
        if (nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                           MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            /* Failure check not handled */
        }

        TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                        u4TunnelInstance, u4IngressId,
                                        u4EgressId, bEntryCreated);
        return CLI_FAILURE;
    }
    if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex,
                                &OutSegmentIndex,
                                MPLS_STATUS_CREATE_AND_WAIT)) == SNMP_FAILURE)
    {
        /* delete the created OutSeg entry */
        if (nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                           MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            /* Failure check not handled */
        }
        CLI_SET_ERR (MPLS_CLI_XC_ENTRY_CREATION);

        TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                        u4TunnelInstance, u4IngressId,
                                        u4EgressId, bEntryCreated);
        return CLI_FAILURE;
    }

    do
    {
        /* Set Out-segment top label */
        if (nmhTestv2MplsOutSegmentTopLabel (&u4ErrorCode, &OutSegmentIndex,
                                             u4OutLabel) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_LABEL);
            break;
        }
        if (nmhSetMplsOutSegmentTopLabel (&OutSegmentIndex, u4OutLabel)
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_LABEL);
            break;
        }
        /* Set Out-segment push top label to true */
        if (nmhTestv2MplsOutSegmentPushTopLabel
            (&u4ErrorCode, &OutSegmentIndex, MPLS_TRUE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_PUSH_TOP_LABEL);
            break;
        }
        if (nmhSetMplsOutSegmentPushTopLabel (&OutSegmentIndex,
                                              MPLS_TRUE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_PUSH_TOP_LABEL);
            break;
        }
        /* Set the Next hop addr type to ipv4 */
        if (nmhTestv2MplsOutSegmentNextHopAddrType (&u4ErrorCode,
                                                    &OutSegmentIndex,
                                                    (INT4) u4AddressType)
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_NEXT_HOP_ADDR_TYPE);
            break;
        }
        if (nmhSetMplsOutSegmentNextHopAddrType (&OutSegmentIndex,
                                                 u4AddressType) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_NEXT_HOP_ADDR_TYPE);
            break;
        }

        if (bNextHopCfg == TRUE)
        {
            /* Set nexthop address */
            MPLS_INTEGER_TO_OCTETSTRING (u4NextHop, (&NextHopAddr));
            if (nmhTestv2MplsOutSegmentNextHopAddr
                (&u4ErrorCode, &OutSegmentIndex, &NextHopAddr) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_NEXT_HOP_ADDR);
                break;
            }
            if (nmhSetMplsOutSegmentNextHopAddr (&OutSegmentIndex,
                                                 &NextHopAddr) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_NEXT_HOP_ADDR);
                break;
            }
        }

        if (u4MplsTnlIfIndex == 0)
        {
            if (CfaGetFreeInterfaceIndex (&u4MplsTnlIfIndex, CFA_MPLS_TUNNEL) ==
                OSIX_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_NO_FREE_INDEX);
                break;
            }
        }

        if ((nmhTestv2MplsOutSegmentInterface (&u4ErrorCode,
                                               &OutSegmentIndex,
                                               (INT4) u4MplsTnlIfIndex))
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_IF);
            break;
        }
        if ((nmhSetMplsOutSegmentInterface (&OutSegmentIndex,
                                            (INT4) u4MplsTnlIfIndex))
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_IF);
            break;
        }
        if ((nmhTestv2FsMplsOutSegmentDirection (&u4ErrorCode,
                                                 &OutSegmentIndex,
                                                 (INT4) u4MplsTnlDir))
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_DIR);
            break;
        }
        if ((nmhSetFsMplsOutSegmentDirection (&OutSegmentIndex,
                                              (INT4) u4MplsTnlDir))
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_DIR);
            break;
        }

        if (bIntfCreationReqd == FALSE)
        {
            if (MplsStackMplsIfOrMplsTnlIf (u4OutIfIndex, u4MplsTnlIfIndex,
                                            CFA_MPLS_TUNNEL) == MPLS_FAILURE)
            {
                CliPrintf (CliHandle, "\r %%Unable to stack mplstunnel "
                           "interface\r\n");
                break;
            }
        }
        else
        {
            if (MplsCreateMplsIfOrMplsTnlIf (u4OutIfIndex, u4MplsTnlIfIndex,
                                             CFA_MPLS_TUNNEL) == MPLS_FAILURE)
            {
                CliPrintf (CliHandle,
                           "%% Unable to create MPLS tunnel interface\r\n");
                break;
            }
        }

        if (nmhTestv2MplsOutSegmentStorageType (&u4ErrorCode, &OutSegmentIndex,
                                                MPLS_STORAGE_NONVOLATILE)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set the Out-Segment "
                       "storage type");
            bTempFlag = TRUE;
            break;
        }

        if (nmhSetMplsOutSegmentStorageType (&OutSegmentIndex,
                                             MPLS_STORAGE_NONVOLATILE)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set the Out-Segment "
                       "storage type");
            bTempFlag = TRUE;
            break;
        }

        if ((nmhTestv2MplsOutSegmentRowStatus
             (&u4ErrorCode, &OutSegmentIndex,
              MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set the Out-Segment "
                       "row status active");
            bTempFlag = TRUE;
            break;
        }
        if ((nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                            MPLS_STATUS_ACTIVE)) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set the Out-Segment "
                       "row status active");
            bTempFlag = TRUE;
            break;
        }

        if (bXcDeleteFlag == TRUE)
        {
            MPLS_OCTETSTRING_TO_INTEGER ((&XCIndex), u4XCTabIndex);
            MPLS_OCTETSTRING_TO_INTEGER ((&OutSegmentIndex), u4OutIndex);
            MPLS_OCTETSTRING_TO_INTEGER ((&InSegmentIndex), u4InIndex);
            MPLS_INTEGER_TO_OID (au4XCTableOid, u4XCTabIndex,
                                 TE_TNL_XC_TABLE_DEF_OFFSET);
            MPLS_INTEGER_TO_OID (au4XCTableOid, u4InIndex,
                                 TE_TNL_XC_TABLE_DEF_OFFSET + 5);
            MPLS_INTEGER_TO_OID (au4XCTableOid, u4OutIndex,
                                 TE_TNL_XC_TABLE_DEF_OFFSET + 10);
            TunnelXCPointer.pu4_OidList = au4XCTableOid;
            TunnelXCPointer.u4_Length = MPLS_TE_XC_TABLE_OFFSET;

            if (TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                             u4IngressId, u4EgressId,
                                             TE_NOTINSERVICE) == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n%%Unable to set MPLS tunnel row "
                           "status as Not in Service");
                bTempFlag = TRUE;
                break;
            }

            if ((nmhTestv2MplsTunnelXCPointer (&u4ErrorCode, u4TunnelIndex,
                                               u4TunnelInstance, u4IngressId,
                                               u4EgressId,
                                               &TunnelXCPointer)) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%%Unable to set Tunnel XCPointer\r\n");
                bTempFlag = TRUE;
                if (i4PrevRowStatus != TE_NOTREADY)
                {
                    TeCliSetMplsTunnelRowStatus (u4TunnelIndex,
                                                 u4TunnelInstance, u4IngressId,
                                                 u4EgressId, i4PrevRowStatus);
                }
                break;
            }

            if ((nmhSetMplsTunnelXCPointer (u4TunnelIndex,
                                            u4TunnelInstance, u4IngressId,
                                            u4EgressId,
                                            &TunnelXCPointer)) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%%Unable to set Tunnel XCPointer\r\n");
                bTempFlag = TRUE;
                if (i4PrevRowStatus != TE_NOTREADY)
                {
                    TeCliSetMplsTunnelRowStatus (u4TunnelIndex,
                                                 u4TunnelInstance, u4IngressId,
                                                 u4EgressId, i4PrevRowStatus);
                }
                break;
            }
        }

        if (nmhTestv2MplsXCStorageType (&u4ErrorCode, &XCIndex, &InSegmentIndex,
                                        &OutSegmentIndex,
                                        MPLS_STORAGE_NONVOLATILE)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%% Unable to set storage type\r\n");
            bTempFlag = TRUE;
            break;
        }

        if (nmhSetMplsXCStorageType
            (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
             MPLS_STORAGE_NONVOLATILE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%% Unable to set storage type\r\n");
            bTempFlag = TRUE;
            break;
        }

        /*  Make XC RowStatus Active */
        if ((nmhTestv2MplsXCRowStatus
             (&u4ErrorCode, &XCIndex, &InSegmentIndex, &OutSegmentIndex,
              MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set the XC row status active");
            bTempFlag = TRUE;
            break;
        }
        if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex,
                                    &OutSegmentIndex,
                                    MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set the XC row status active");
            bTempFlag = TRUE;
            break;
        }

        if ((i4PrevRowStatus != TE_NOTREADY) && (bXcDeleteFlag == TRUE))
        {
            TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                         u4IngressId, u4EgressId,
                                         i4PrevRowStatus);
        }

        /* all set operations have been successful */
        return CLI_SUCCESS;
    }
    while (0);
    if (bTempFlag == TRUE)        /* Error occured */
    {
        if (((i4TunnelMode == TE_TNL_MODE_UNIDIRECTIONAL) ||
             (u4MplsTnlDir == MPLS_DIRECTION_REVERSE)) &&
            (u1TnlPathType == TE_TNL_WORKING_PATH))
        {
            MplsDeleteMplsIfOrMplsTnlIf (u4OutIfIndex, u4MplsTnlIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_FALSE);
        }
        else
        {
            MplsDeleteMplsIfOrMplsTnlIf (u4OutIfIndex, u4MplsTnlIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
        }
    }

    /* delete the created OutSeg and XC entry */
    if (nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               MPLS_STATUS_DESTROY) == SNMP_FAILURE)
    {
        /*Failure condition not handled here */
    }
    if (nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex, MPLS_STATUS_DESTROY)
        == SNMP_FAILURE)
    {
        /*Failure condition not handled here */
    }

    TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                    u4TunnelInstance, u4IngressId,
                                    u4EgressId, bEntryCreated);
    return CLI_FAILURE;
}

/******************************************************************************
* Function Name : TeCliStaticBind 
* Description   : This routine is used to associate XC Entries with that of
*                 tunnel. 
* Input(s)      : CliHandle       - Cli Context Handle
*               : u4InLabel       - Incoming Label associated with in-segment.
*               : u4InIfIndex     - Interface Index for the incoming MPLS
*                                   interface.  
*               : u4OutLabel      - Label associated with the outgoing packet
*               : u4NextHop       - Interface Index for the outgoing MPLS
*                                   interface.  
*                 u4OutIfIndex    - Outgoing interface
*                 u4MplsTnlDir - MPLS tunnel direction (forward/reverse)
*                 u1TnlPathType   - Tunnel Path Type - Protection or Working
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT1
TeCliStaticBind (tCliHandle CliHandle, UINT4 u4InLabel, UINT4 u4InIfIndex,
                 UINT4 u4OutLabel, UINT4 u4NextHop, UINT4 u4OutIfIndex,
                 UINT4 u4MplsTnlDir, UINT1 u1TnlPathType)
{

    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OCTET_STRING_TYPE NextHopAddr;
    tSNMP_OCTET_STRING_TYPE XCSegmentIndex;
    tSNMP_OCTET_STRING_TYPE MplsTeTnlType;    /* MPLS_P2MP_LSP_CHANGES */

    tSNMP_OID_TYPE      TunnelXCPointer;
    tSNMP_OID_TYPE      XCSegIndex;
    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;

    static UINT4        au4XCIndexx[MPLS_TE_XC_TABLE_OFFSET];

    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1NextHopAddr[MPLS_INDEX_LENGTH];

    /* MPLS_P2MP_LSP_CHANGES - S */
    static UINT1        au1TnlType[CLI_MPLS_TE_TNL_TYPE];
    /* MPLS_P2MP_LSP_CHANGES - E */

    static UINT4        au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 2, 1, 10, 1, 4, 4, 0, 0, 0, 0, 4, 0, 0, 0,
        0, 4, 0, 0, 0, 0
    };

    UINT4               u4TunnelIndex = 0;
    UINT4               u4TunnelInstance = 0;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;

    UINT4               u4XCTabIndex = 0;
    UINT4               u4InIndex = 0;
    UINT4               u4OutIndex = 0;

    UINT4               u4XCSegInd = 0;
    UINT4               u4InSegInd = 0;
    UINT4               u4OutSegInd = 0;

    INT4                i4InSegmntNPop;
    INT4                i4IfIndex = 0;
    INT4                i4TnlRole = 0;
    UINT4               u4ErrorCode = 0;
    UINT4               u4MplsTnlInIfIndex = 0;
    UINT4               u4MplsTnlOutIfIndex = 0;
#ifdef CFA_WANTED
    UINT4               u4MplsIfIndex = 0;
#endif
    UINT4               u4Mask = 0xffffffff;
    INT4                i4PrevRowStatus = 0;
    INT4                i4Direction = 0;
    INT4                i4TunnelMode = 0;

    BOOL1               bXcDeleteFlag = TRUE;
    BOOL1               bNextHopCfg = FALSE;

    UINT4               u4CreatedInstance = TE_ZERO;
    BOOL1               bEntryCreated = FALSE;
    BOOL1               bTempFlag = FALSE;
    BOOL1               bIntfCreationReqd = TRUE;

    XCSegIndex.pu4_OidList = au4XCIndexx;
    XCSegIndex.u4_Length = 0;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    XCIndex.pu1_OctetList = au1XCIndex;
    XCIndex.i4_Length = MPLS_INDEX_LENGTH;
    NextHopAddr.pu1_OctetList = au1NextHopAddr;
    /* MPLS_P2MP_LSP_CHANGES - S */
    MplsTeTnlType.pu1_OctetList = au1TnlType;
    MplsTeTnlType.i4_Length = CLI_MPLS_TE_TNL_TYPE;
    /* MPLS_P2MP_LSP_CHANGES - E */

    if (u4OutIfIndex == 0)
    {
        bNextHopCfg = TRUE;
        if (NetIpv4IfIsOurAddress (u4NextHop) == NETIPV4_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%%Next hop is same as self-ip addr\n");
            return CLI_FAILURE;
        }

        MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
        MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));
        RtQuery.u4DestinationIpAddress = u4NextHop;
        RtQuery.u4DestinationSubnetMask = u4Mask;
        RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;

        if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_SUCCESS)
        {
            if (NetIpv4GetCfaIfIndexFromPort (NetIpRtInfo.u4RtIfIndx,
                                              &u4OutIfIndex) == NETIPV4_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Unable to fetch Next hop information\n");
                return CLI_FAILURE;
            }
        }
#ifdef CFA_WANTED
        else
        {
            if (CfaIpIfGetIfIndexFromHostIpAddressInCxt (MPLS_DEF_CONTEXT_ID,
                                                         u4NextHop,
                                                         &u4OutIfIndex)
                == CFA_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Unable to fetch Next hop information\n");
                return CLI_FAILURE;
            }
        }
    }
    if (CfaUtilGetMplsIfFromIfIndex (u4OutIfIndex, &u4MplsIfIndex, TRUE) ==
        CFA_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% Enable MPLS over outgoing interface\r\n");
        return CLI_FAILURE;
    }
#else
    }
#endif
    /*
     * Step 1: 
     * Get the TunnelId,TunnelInstance,Ingress and Egress using the TunnelName (mplsTunnel+TunnelIndex)
     * TunnelIndex is obtained from the prompt.
     * If Tunnel Exists, get the instance , Ingress and Egress Ids.
     * If Tunnel does not exist , return Failure.
     */

    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* MPLS_P2MP_LSP_CHANGES - S */
    /* Destination address is mandatory for P2MP tunnels. Reject configuration
     * if tunnel type is P2MP. */
    MEMSET (au1TnlType, TE_ZERO, CLI_MPLS_TE_TNL_TYPE);
    if (nmhGetFsMplsTunnelType (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                u4EgressId, &MplsTeTnlType) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%%Failed to retrieve tunnel type\r\n");
        return CLI_FAILURE;
    }
    if (MplsTeTnlType.pu1_OctetList[TE_ZERO] & TE_TNL_TYPE_P2MP)
    {
        CliPrintf (CliHandle, "\r\n%%Insufficient input for P2MP tunnel. "
                   "Configuration not allowed.\r\n");
        return CLI_FAILURE;
    }
    /* MPLS_P2MP_LSP_CHANGES - E */

    if (nmhGetMplsTunnelRole (u4TunnelIndex, u4TunnelInstance,
                              u4IngressId, u4EgressId,
                              &i4TnlRole) == SNMP_SUCCESS)
    {
        if (i4TnlRole == TE_INGRESS)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Wrong label configuration for Ingress tunnel\r\n");
            return CLI_FAILURE;
        }
        else if (i4TnlRole == TE_EGRESS)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Wrong label configuration for Egress tunnel\r\n");
            return CLI_FAILURE;
        }
    }

    if (nmhGetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                   u4IngressId, u4EgressId,
                                   &i4PrevRowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to get tunnel row status\r\n");
        return CLI_FAILURE;
    }

    /* This function is called for making static binding  
     * in intermediate node. For corouted bidirectional tunnels, we need to
     * check whether a tunnel with opposite XC direction exist.
     *
     * If it exist, do not delete the XC table, InSegmentTable and 
     * OutSegment Table.
     * Instead just create another insegment and outsegment and associate it
     * with XC table.
     *
     * If it does not exist create a new XC table, InSegmentTable and 
     * OutSegmentTable. */

    if (nmhGetFsMplsTunnelMode (u4TunnelIndex, u4TunnelInstance,
                                u4IngressId, u4EgressId, &i4TunnelMode)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Tunnel Mode is not available.\r\n");
        return CLI_FAILURE;
    }

    if ((i4TunnelMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
        && (u4MplsTnlDir == MPLS_CLI_DIRECTION_REVERSE))
    {
        u4MplsTnlDir = MPLS_DIRECTION_REVERSE;
    }
    else
    {
        u4MplsTnlDir = MPLS_DIRECTION_FORWARD;
    }

    if (u4TunnelInstance == TE_ZERO)
    {
        if (TeCliNormalCreateNonZeroInstTunnel (CliHandle, u4TunnelIndex,
                                                u4TunnelInstance, u4IngressId,
                                                u4EgressId, u1TnlPathType,
                                                &u4CreatedInstance,
                                                &bEntryCreated) == CLI_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to create non-zero instance tunnel\n");
            return CLI_FAILURE;
        }

        u4TunnelInstance = u4CreatedInstance;

        if ((u4MplsTnlDir == MPLS_DIRECTION_FORWARD) &&
            (u1TnlPathType == TE_TNL_WORKING_PATH))
        {
            u4MplsTnlOutIfIndex = gaTnlCliArgs[CliHandle].u4MplsTnlIfIndex;
            bIntfCreationReqd = FALSE;
        }
    }

    /* 
     * Step 2: 
     * Using the obtained tunnel indices, get the tunnel XC pointer.
     * From the XCPointer value obtain the XCIndex,InSegment and OutSegment Index.
     */
    if (nmhGetMplsTunnelXCPointer (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                   u4EgressId, &XCSegIndex) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to get tunnel XC Pointer\r\n");

        TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                        u4TunnelInstance, u4IngressId,
                                        u4EgressId, bEntryCreated);
        return CLI_FAILURE;
    }

    MplsGetSegFromTunnelXC (&XCSegIndex, &XCSegmentIndex, &InSegmentIndex,
                            &OutSegmentIndex, &u4XCSegInd, &u4InSegInd,
                            &u4OutSegInd);

    if ((i4TunnelMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL) &&
        (u4InSegInd != 0))
    {
        if ((nmhGetFsMplsInSegmentDirection (&InSegmentIndex,
                                             &i4Direction)
             == SNMP_SUCCESS) && (i4Direction != (INT4) u4MplsTnlDir))
        {
            bXcDeleteFlag = FALSE;
        }
    }

    /*
     * Step 3:
     * If In Segment, Out Segment and XC Indices are already present for the corresponding 
     * Tunnel, then destroy the corresponding XC ,InSeg and OutSeg entries , and update with 
     * the new inputs.
     */
    if ((u4XCSegInd != 0) && (bXcDeleteFlag == TRUE))
    {
        /* Destroy the XC entry */
        if (nmhTestv2MplsXCRowStatus (&u4ErrorCode, &XCSegmentIndex,
                                      &InSegmentIndex,
                                      &OutSegmentIndex,
                                      MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_XC_ENTRY_DELETION);

            TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                            u4TunnelInstance, u4IngressId,
                                            u4EgressId, bEntryCreated);
            return CLI_FAILURE;
        }

        if (nmhSetMplsXCRowStatus
            (&XCSegmentIndex, &InSegmentIndex, &OutSegmentIndex,
             MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_XC_ENTRY_DELETION);

            TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                            u4TunnelInstance, u4IngressId,
                                            u4EgressId, bEntryCreated);
            return CLI_FAILURE;
        }
    }

    /* Destroy the InSegment entry */
    if ((u4InSegInd != 0) && (bXcDeleteFlag == TRUE))
    {
        if (nmhGetMplsInSegmentInterface (&InSegmentIndex, &i4IfIndex) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to fetch the Insegment "
                       "table entry\r\n");
            return CLI_FAILURE;
        }
        MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, (UINT4) i4IfIndex,
                                     CFA_MPLS_TUNNEL, MPLS_TRUE);
        if (nmhTestv2MplsInSegmentRowStatus (&u4ErrorCode,
                                             &InSegmentIndex,
                                             MPLS_STATUS_DESTROY) ==
            SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_INSEG_ENTRY_DELETION);

            TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                            u4TunnelInstance, u4IngressId,
                                            u4EgressId, bEntryCreated);
            return CLI_FAILURE;
        }

        if (nmhSetMplsInSegmentRowStatus
            (&InSegmentIndex, MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_INSEG_ENTRY_DELETION);

            TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                            u4TunnelInstance, u4IngressId,
                                            u4EgressId, bEntryCreated);
            return CLI_FAILURE;
        }
    }
    /* Destroy the OutSegment entry */

    if ((u4OutSegInd != 0) && (bXcDeleteFlag == TRUE))
    {
        nmhGetMplsOutSegmentInterface (&OutSegmentIndex, &i4IfIndex);
        MplsDeleteMplsIfOrMplsTnlIf (u4OutIfIndex, (UINT4) i4IfIndex,
                                     CFA_MPLS_TUNNEL, MPLS_TRUE);
        if (nmhTestv2MplsOutSegmentRowStatus
            (&u4ErrorCode, &OutSegmentIndex,
             MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_OUTSEG_ENTRY_DELETION);

            TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                            u4TunnelInstance, u4IngressId,
                                            u4EgressId, bEntryCreated);
            return CLI_FAILURE;
        }

        if (nmhSetMplsOutSegmentRowStatus
            (&OutSegmentIndex, MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_OUTSEG_ENTRY_DELETION);

            TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                            u4TunnelInstance, u4IngressId,
                                            u4EgressId, bEntryCreated);
            return CLI_FAILURE;
        }
    }

    /*
     * Step 4:
     * If InSeg , Out Seg and XC Entries are not already present for the same tunnel,
     *  get a free InSeg, OutSeg and XC Index,
     * and  populate the XC, InSeg and OutSeg Tables.
     */

    MEMSET (au1InSegIndex, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1XCIndex, 0, MPLS_INDEX_LENGTH);

    /*
     * Step 4a :Create an Inseg entry 
     * */
    if ((nmhGetMplsInSegmentIndexNext (&InSegmentIndex)) == SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_NO_FREE_INDEX);

        TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                        u4TunnelInstance, u4IngressId,
                                        u4EgressId, bEntryCreated);
        return CLI_FAILURE;
    }
    if ((nmhTestv2MplsInSegmentRowStatus (&u4ErrorCode, &InSegmentIndex,
                                          MPLS_STATUS_CREATE_AND_WAIT)) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_INSEG_ENTRY_CREATION);

        TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                        u4TunnelInstance, u4IngressId,
                                        u4EgressId, bEntryCreated);
        return CLI_FAILURE;
    }
    if ((nmhSetMplsInSegmentRowStatus
         (&InSegmentIndex, MPLS_STATUS_CREATE_AND_WAIT)) == SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_INSEG_ENTRY_CREATION);

        TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                        u4TunnelInstance, u4IngressId,
                                        u4EgressId, bEntryCreated);
        return CLI_FAILURE;
    }

    /* 
     * Step 4b:
     * Create an OutSeg entry 
     Delete the created InSeg  entry in case of any failure.
     */

    if ((nmhGetMplsOutSegmentIndexNext (&OutSegmentIndex)) == SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_NO_FREE_INDEX);
        if (nmhSetMplsInSegmentRowStatus (&InSegmentIndex, MPLS_STATUS_DESTROY)
            == SNMP_FAILURE)
        {
            /*Failure condition not handled here */
        }

        TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                        u4TunnelInstance, u4IngressId,
                                        u4EgressId, bEntryCreated);
        return CLI_FAILURE;
    }
    if ((nmhTestv2MplsOutSegmentRowStatus (&u4ErrorCode, &OutSegmentIndex,
                                           MPLS_STATUS_CREATE_AND_WAIT)) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_OUTSEG_ENTRY_CREATION);
        if (nmhSetMplsInSegmentRowStatus (&InSegmentIndex, MPLS_STATUS_DESTROY)
            == SNMP_FAILURE)
        {
            /*Failure condition not handled here */
        }

        TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                        u4TunnelInstance, u4IngressId,
                                        u4EgressId, bEntryCreated);
        return CLI_FAILURE;
    }
    if ((nmhSetMplsOutSegmentRowStatus
         (&OutSegmentIndex, MPLS_STATUS_CREATE_AND_WAIT)) == SNMP_FAILURE)
    {
        if (nmhSetMplsInSegmentRowStatus (&InSegmentIndex, MPLS_STATUS_DESTROY)
            == SNMP_FAILURE)
        {
            /*Failure condition not handled here */
        }
        CLI_SET_ERR (MPLS_CLI_OUTSEG_ENTRY_CREATION);

        TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                        u4TunnelInstance, u4IngressId,
                                        u4EgressId, bEntryCreated);
        return CLI_FAILURE;
    }

    /* 
     * Step 4c:
     * Create an XC entry 
     * Delete the created InSeg and OutSeg entry in case of any failures.
     * */
    if (bXcDeleteFlag == TRUE)
    {
        if ((nmhGetMplsXCIndexNext (&XCIndex)) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_NO_FREE_INDEX);
            if (nmhSetMplsInSegmentRowStatus
                (&InSegmentIndex, MPLS_STATUS_DESTROY) == SNMP_FAILURE)
            {
                /*Failure condition not handled here */
            }
            if (nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                               MPLS_STATUS_DESTROY) ==
                SNMP_FAILURE)
            {
                /*Failure condition not handled here */
            }

            TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                            u4TunnelInstance, u4IngressId,
                                            u4EgressId, bEntryCreated);
            return CLI_FAILURE;
        }
    }
    else
    {
        MPLS_INTEGER_TO_OCTETSTRING (u4XCSegInd, (&XCIndex));
    }

    if ((nmhTestv2MplsXCRowStatus (&u4ErrorCode, &XCIndex, &InSegmentIndex,
                                   &OutSegmentIndex,
                                   MPLS_STATUS_CREATE_AND_WAIT))
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_XC_ENTRY_CREATION);
        if (nmhSetMplsInSegmentRowStatus (&InSegmentIndex, MPLS_STATUS_DESTROY)
            == SNMP_FAILURE)
        {
            /*Failure condition not handled here */
        }
        if (nmhSetMplsOutSegmentRowStatus
            (&OutSegmentIndex, MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            /*Failure condition not handled here */
        }

        TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                        u4TunnelInstance, u4IngressId,
                                        u4EgressId, bEntryCreated);
        return CLI_FAILURE;
    }
    if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex,
                                &OutSegmentIndex,
                                MPLS_STATUS_CREATE_AND_WAIT)) == SNMP_FAILURE)
    {
        if (nmhSetMplsInSegmentRowStatus (&InSegmentIndex, MPLS_STATUS_DESTROY)
            == SNMP_FAILURE)
        {
            /*Failure condition not handled here */
        }
        if (nmhSetMplsOutSegmentRowStatus
            (&OutSegmentIndex, MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            /*Failure condition not handled here */
        }
        CLI_SET_ERR (MPLS_CLI_XC_ENTRY_CREATION);

        TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                        u4TunnelInstance, u4IngressId,
                                        u4EgressId, bEntryCreated);
        return CLI_FAILURE;
    }

    /*
     * Step 5:
     * Populate the InSeg, OutSeg and XC Tables
     * Make the InSeg, OutSeg and XC Tables active
     * */

    do
    {
        /* Set inlabel value in seg table */
        if ((nmhTestv2MplsInSegmentLabel (&u4ErrorCode, &InSegmentIndex,
                                          u4InLabel)) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_LABEL);
            break;
        }
        if ((nmhSetMplsInSegmentLabel (&InSegmentIndex, u4InLabel))
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_LABEL);
            break;
        }
        /* Set InSegment NPop */
        i4InSegmntNPop = 1;
        if ((nmhTestv2MplsInSegmentNPop (&u4ErrorCode, &InSegmentIndex,
                                         i4InSegmntNPop)) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_NPOP);
            break;
        }
        if ((nmhSetMplsInSegmentNPop (&InSegmentIndex, i4InSegmntNPop))
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_NPOP);
            break;
        }
        /* Setting the InSegment ip addr family to ipv4 */
        if (nmhTestv2MplsInSegmentAddrFamily (&u4ErrorCode, &InSegmentIndex,
                                              LSR_ADDR_IPV4) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_ADDR_FAMILY);
            break;
        }
        if (nmhSetMplsInSegmentAddrFamily (&InSegmentIndex, LSR_ADDR_IPV4)
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_ADDR_FAMILY);
            break;
        }

        /* Set outsegment top label */
        if (nmhTestv2MplsOutSegmentTopLabel (&u4ErrorCode, &OutSegmentIndex,
                                             u4OutLabel) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_LABEL);
            break;
        }
        if (nmhSetMplsOutSegmentTopLabel (&OutSegmentIndex, u4OutLabel)
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_ERR_OUTSEG_LABEL);
            break;
        }
        /* set out segment push top label to true */
        if (nmhTestv2MplsOutSegmentPushTopLabel
            (&u4ErrorCode, &OutSegmentIndex, MPLS_TRUE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_PUSH_TOP_LABEL);
            break;
        }
        if (nmhSetMplsOutSegmentPushTopLabel (&OutSegmentIndex,
                                              MPLS_TRUE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_PUSH_TOP_LABEL);
            break;
        }

        if (bNextHopCfg == TRUE)
        {
            /* set the next hop addr type to ipv4 */
            if (nmhTestv2MplsOutSegmentNextHopAddrType (&u4ErrorCode,
                                                        &OutSegmentIndex,
                                                        LSR_ADDR_IPV4)
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_NEXT_HOP_ADDR_TYPE);
                break;
            }
            if (nmhSetMplsOutSegmentNextHopAddrType (&OutSegmentIndex,
                                                     LSR_ADDR_IPV4) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_NEXT_HOP_ADDR_TYPE);
                break;
            }
            /* set nexthop address */
            MPLS_INTEGER_TO_OCTETSTRING (u4NextHop, (&NextHopAddr));
            if (nmhTestv2MplsOutSegmentNextHopAddr
                (&u4ErrorCode, &OutSegmentIndex, &NextHopAddr) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_NEXT_HOP_ADDR);
                break;
            }
            if (nmhSetMplsOutSegmentNextHopAddr (&OutSegmentIndex,
                                                 &NextHopAddr) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_NEXT_HOP_ADDR);
                break;
            }
        }
        if (CfaGetFreeInterfaceIndex (&u4MplsTnlInIfIndex,
                                      CFA_MPLS_TUNNEL) == OSIX_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_NO_FREE_TUNNEL_INTERFACE);
            break;
        }
        if ((nmhTestv2MplsInSegmentInterface (&u4ErrorCode,
                                              &InSegmentIndex,
                                              (INT4) u4MplsTnlInIfIndex))
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_IF);
            break;
        }
        if ((nmhSetMplsInSegmentInterface (&InSegmentIndex,
                                           (INT4) u4MplsTnlInIfIndex))
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_IF);
            break;
        }
        if (MplsCreateMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL) == MPLS_FAILURE)
        {
            CliPrintf (CliHandle,
                       "%% Unable to create MPLS tunnel interface\r\n");
            break;
        }
        if ((nmhTestv2FsMplsInSegmentDirection (&u4ErrorCode,
                                                &InSegmentIndex,
                                                (INT4) u4MplsTnlDir))
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_DIR);
            break;
        }
        if ((nmhSetFsMplsInSegmentDirection (&InSegmentIndex,
                                             (INT4) u4MplsTnlDir))
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_DIR);
            break;
        }

        if (nmhTestv2MplsInSegmentStorageType (&u4ErrorCode, &InSegmentIndex,
                                               MPLS_STORAGE_NONVOLATILE)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set In segment "
                       "storage type");
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            break;
        }

        if (nmhSetMplsInSegmentStorageType (&InSegmentIndex,
                                            MPLS_STORAGE_NONVOLATILE) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set In segment "
                       "storage type");
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            break;
        }

        /* Set the row status of the InSegment,OutSegment and XC tables as active */
        if ((nmhTestv2MplsInSegmentRowStatus (&u4ErrorCode, &InSegmentIndex,
                                              MPLS_STATUS_ACTIVE)) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set In segment "
                       "row status active");
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            break;
        }
        if ((nmhSetMplsInSegmentRowStatus
             (&InSegmentIndex, MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set In segment "
                       "row status active");
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            break;
        }
        /* Tunnel Interface created when giving the command 'interface
         * mplsTunnel <integer> should be associated only with the
         * forward direction outsegment only.
         */
        if (u4MplsTnlOutIfIndex == 0)
        {
            if (CfaGetFreeInterfaceIndex (&u4MplsTnlOutIfIndex,
                                          CFA_MPLS_TUNNEL) == OSIX_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_NO_FREE_TUNNEL_INTERFACE);
                MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                             CFA_MPLS_TUNNEL, MPLS_TRUE);
                break;
            }
        }

        if ((nmhTestv2MplsOutSegmentInterface (&u4ErrorCode,
                                               &OutSegmentIndex,
                                               (INT4) u4MplsTnlOutIfIndex))
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_IF);
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            break;
        }
        if ((nmhSetMplsOutSegmentInterface (&OutSegmentIndex,
                                            (INT4) u4MplsTnlOutIfIndex))
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_IF);
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            break;
        }

        if (bIntfCreationReqd == FALSE)
        {
            /* Create a stack MPLS Tunnel Interface and MPLS If */
            if (MplsStackMplsIfOrMplsTnlIf (u4OutIfIndex, u4MplsTnlOutIfIndex,
                                            CFA_MPLS_TUNNEL) == MPLS_FAILURE)
            {
                CliPrintf (CliHandle, "\r %%Unable to stack mplstunnel "
                           "interface\r\n");
                MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                             CFA_MPLS_TUNNEL, MPLS_TRUE);
                break;
            }
        }
        else
        {
            /* Create a new MPLS Tunnel Interface and stack over MPLS If */
            if (MplsCreateMplsIfOrMplsTnlIf (u4OutIfIndex, u4MplsTnlOutIfIndex,
                                             CFA_MPLS_TUNNEL) == MPLS_FAILURE)
            {
                CliPrintf (CliHandle,
                           "%% Unable to create MPLS tunnel interface\r\n");
                MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                             CFA_MPLS_TUNNEL, MPLS_TRUE);
                break;
            }
        }
        if ((nmhTestv2FsMplsOutSegmentDirection (&u4ErrorCode,
                                                 &OutSegmentIndex,
                                                 (INT4) u4MplsTnlDir))
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_DIR);
            break;
        }
        if ((nmhSetFsMplsOutSegmentDirection (&OutSegmentIndex,
                                              (INT4) u4MplsTnlDir))
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_DIR);
            break;
        }

        if (nmhTestv2MplsOutSegmentStorageType (&u4ErrorCode, &OutSegmentIndex,
                                                MPLS_STORAGE_NONVOLATILE)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set Out segment "
                       "storage type");
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            break;
        }

        if (nmhSetMplsOutSegmentStorageType (&OutSegmentIndex,
                                             MPLS_STORAGE_NONVOLATILE) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set Out segment "
                       "storage type");
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            break;
        }

        if ((nmhTestv2MplsOutSegmentRowStatus
             (&u4ErrorCode, &OutSegmentIndex,
              MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set Out segment "
                       "row status active");
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            bTempFlag = TRUE;
            break;
        }
        if ((nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                            MPLS_STATUS_ACTIVE)) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set Out segment "
                       "row status active");
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            bTempFlag = TRUE;
            break;
        }

        /*  Set the value of mplsTunnelXCPointer. */

        if (bXcDeleteFlag == TRUE)
        {
            MPLS_OCTETSTRING_TO_INTEGER ((&XCIndex), u4XCTabIndex);
            MPLS_OCTETSTRING_TO_INTEGER ((&OutSegmentIndex), u4OutIndex);
            MPLS_OCTETSTRING_TO_INTEGER ((&InSegmentIndex), u4InIndex);
            MPLS_INTEGER_TO_OID (au4XCTableOid, u4XCTabIndex,
                                 TE_TNL_XC_TABLE_DEF_OFFSET);
            MPLS_INTEGER_TO_OID (au4XCTableOid, u4InIndex,
                                 TE_TNL_XC_TABLE_DEF_OFFSET + 5);
            MPLS_INTEGER_TO_OID (au4XCTableOid, u4OutIndex,
                                 TE_TNL_XC_TABLE_DEF_OFFSET + 10);
            TunnelXCPointer.pu4_OidList = au4XCTableOid;
            TunnelXCPointer.u4_Length = MPLS_TE_XC_TABLE_OFFSET;

            if (TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                             u4IngressId, u4EgressId,
                                             TE_NOTINSERVICE) == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n%%Unable to set MPLS tunnel row "
                           "status as Not in Service");
                MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                             CFA_MPLS_TUNNEL, MPLS_TRUE);
                bTempFlag = TRUE;
                break;
            }

            if ((nmhTestv2MplsTunnelXCPointer (&u4ErrorCode, u4TunnelIndex,
                                               u4TunnelInstance, u4IngressId,
                                               u4EgressId,
                                               &TunnelXCPointer)) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%%Unable to set Tunnel XCPointer\r\n");
                MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                             CFA_MPLS_TUNNEL, MPLS_TRUE);
                bTempFlag = TRUE;
                if (i4PrevRowStatus != TE_NOTREADY)
                {
                    TeCliSetMplsTunnelRowStatus (u4TunnelIndex,
                                                 u4TunnelInstance, u4IngressId,
                                                 u4EgressId, i4PrevRowStatus);
                }

                break;
            }

            if ((nmhSetMplsTunnelXCPointer (u4TunnelIndex,
                                            u4TunnelInstance, u4IngressId,
                                            u4EgressId,
                                            &TunnelXCPointer)) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%%Unable to set Tunnel XCPointer\r\n");
                MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                             CFA_MPLS_TUNNEL, MPLS_TRUE);
                bTempFlag = TRUE;
                if (i4PrevRowStatus != TE_NOTREADY)
                {
                    TeCliSetMplsTunnelRowStatus (u4TunnelIndex,
                                                 u4TunnelInstance, u4IngressId,
                                                 u4EgressId, i4PrevRowStatus);
                }

                break;
            }
        }

        if (nmhTestv2MplsXCStorageType (&u4ErrorCode, &XCIndex, &InSegmentIndex,
                                        &OutSegmentIndex,
                                        MPLS_STORAGE_NONVOLATILE)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%% Unable to set storage type\r\n");
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            bTempFlag = TRUE;
            break;
        }

        if (nmhSetMplsXCStorageType
            (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
             MPLS_STORAGE_NONVOLATILE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%% Unable to set storage type\r\n");
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            bTempFlag = TRUE;
            break;
        }

        if ((nmhTestv2MplsXCRowStatus
             (&u4ErrorCode, &XCIndex, &InSegmentIndex, &OutSegmentIndex,
              MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set the XC row status active");
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            bTempFlag = TRUE;
            break;
        }
        if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex,
                                    &OutSegmentIndex,
                                    MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set the XC row status active");
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            bTempFlag = TRUE;
            break;
        }

        if ((i4PrevRowStatus != TE_NOTREADY) && (bXcDeleteFlag == TRUE))
        {
            TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                         u4IngressId, u4EgressId,
                                         i4PrevRowStatus);
        }

        /* 
         * Step 6:
         * If all set operations have been successful , return success.
         * */

        return CLI_SUCCESS;
    }
    while (0);
    if (bTempFlag == TRUE)        /* Error occured */
    {
        if ((u4MplsTnlDir == MPLS_DIRECTION_FORWARD) &&
            (u1TnlPathType == TE_TNL_WORKING_PATH))
        {
            MplsDeleteMplsIfOrMplsTnlIf (u4OutIfIndex, u4MplsTnlOutIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_FALSE);
        }
        else
        {
            MplsDeleteMplsIfOrMplsTnlIf (u4OutIfIndex, u4MplsTnlOutIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
        }
    }

    /* 
     * Step 7:
     * If all set operations are NOT  successful , Delete the created InSegment , OutSegment and XC entry 
     * Then return failure.
     * */

    if (nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               MPLS_STATUS_DESTROY) == SNMP_FAILURE)
    {
        /*Failure condition not handled here */
    }
    if (nmhSetMplsInSegmentRowStatus (&InSegmentIndex, MPLS_STATUS_DESTROY)
        == SNMP_FAILURE)
    {
        /*Failure condition not handled here */
    }

    if (nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex, MPLS_STATUS_DESTROY)
        == SNMP_FAILURE)
    {
        /*Failure condition not handled here */
    }

    TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                    u4TunnelInstance, u4IngressId,
                                    u4EgressId, bEntryCreated);
    return CLI_FAILURE;
}

/******************************************************************************
* Function Name : TeCliTunnelSigProtocol 
* Description   : This routine is used to associate XC Entries with that of
*                 tunnel. 
* Input(s)      : CliHandle     - Cli Context Handle
*               : i4SigProtocol - Tunnel signalling protocol (none/rsvp/crldp/
*                                 other)
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliTunnelSigProtocol (tCliHandle CliHandle, INT4 i4SigProtocol)
{
    UINT4               u4TunnelIndex = 0;
    UINT4               u4TunnelInstance = 0;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4PrevRowStatus = 0;
    INT4                i4TunnelSignallingProto = 0;
    INT4                i4RetValMplsTunnelRole = 0;
    UINT4               u4TunnelResourceIndex = 0;
    UINT4               u4Bandwidth = 0;
    INT4                i4TunnelAdminStatus = 0;
    tTeTnlInfo         *pInstance0Tnl = NULL;
    tSNMP_OID_TYPE      GetResourcePointer;
    tSNMP_OID_TYPE      ResourcePointer;
    static UINT4        au4ResourceTableOid[MPLS_TE_RSRC_TABLE_MAX_RATE_OFFSET]
        = { 1, 3, 6, 1, 2, 1, 10, 166, 3, 2, 6, 1, 2 };

    static UINT4        au4GetRsrcPointer[MPLS_TE_RSRC_TABLE_MAX_RATE_OFFSET];

    GetResourcePointer.pu4_OidList = au4GetRsrcPointer;
    MEMSET (GetResourcePointer.pu4_OidList, 0, sizeof (UINT4) * 2);
    GetResourcePointer.u4_Length = 0;

    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    /*Retriving the tunnel rowstatus */
    if ((nmhGetMplsTunnelRowStatus
         (u4TunnelIndex, u4TunnelInstance, u4IngressId,
          u4EgressId, &i4PrevRowStatus)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%TunnelRowStatus is not available\r\n");
        return CLI_FAILURE;
    }

    /*Retriving the tunnel adminstatus */
    if ((nmhGetMplsTunnelAdminStatus
         (u4TunnelIndex, u4TunnelInstance, u4IngressId,
          u4EgressId, &i4TunnelAdminStatus)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%TunnelAdminStatus is not available\r\n");
        return CLI_FAILURE;
    }

    if (i4TunnelAdminStatus == TE_ADMIN_UP)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Signalling can't be changed as the tunnel is up\r\n");
        return CLI_FAILURE;
    }
    if (i4PrevRowStatus == MPLS_STATUS_ACTIVE)
    {
        TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                     u4IngressId, u4EgressId, TE_NOTINSERVICE);
    }

    if (nmhGetMplsTunnelSignallingProto (u4TunnelIndex, u4TunnelInstance,
                                         u4IngressId, u4EgressId,
                                         &i4TunnelSignallingProto)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set Tunnel Bandwidth\r\n");
        return CLI_FAILURE;

    }

    if ((nmhGetMplsTunnelResourcePointer
         (u4TunnelIndex, u4TunnelInstance, u4IngressId, u4EgressId,
          &GetResourcePointer)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%TunnelResourcePointer is not available\r\n");
        return CLI_FAILURE;
    }

    u4TunnelResourceIndex =
        GetResourcePointer.pu4_OidList[TE_TNL_RSRC_TABLE_DEF_OFFSET - 1];
    if ((GetResourcePointer.u4_Length != 2) &&
        (u4TunnelResourceIndex != TE_DFLT_TRFC_PRAM_INDEX))
    {

        if (i4TunnelSignallingProto == i4SigProtocol)
        {
            return CLI_SUCCESS;
        }
        /*Get the bandwidth from standard table */
        if (nmhGetMplsTunnelResourceMaxRate (u4TunnelResourceIndex,
                                             &u4Bandwidth) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Failed to Get the bandwidth\r\n");
            return CLI_FAILURE;
        }
        if ((nmhTestv2MplsTunnelResourceRowStatus (&u4ErrorCode,
                                                   u4TunnelResourceIndex,
                                                   MPLS_STATUS_DESTROY))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "%%Unable to set MplsTunnelResourceRowStatus - destroy\r\n");
            return CLI_FAILURE;
        }
        if ((nmhSetMplsTunnelResourceRowStatus (u4TunnelResourceIndex,
                                                MPLS_STATUS_DESTROY))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "%%Unable to set MplsTunnelResourceRowStatus - destroy\r\n");
            return CLI_FAILURE;
        }

        if (nmhGetMplsTunnelResourceIndexNext (&u4TunnelResourceIndex) !=
            SNMP_SUCCESS)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Failed to Get a Valid Resource Index\r\n");
            return CLI_FAILURE;
        }

        if (i4SigProtocol != TE_SIGPROTO_LDP)
        {
            /*Set the Bandwidth */
            if ((TeCliTunnelSigBandwidth (CliHandle, u4TunnelResourceIndex,
                                          i4SigProtocol, u4Bandwidth))
                == CLI_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%%Unable to set Tunnel Bandwidth\r\n");
                return CLI_FAILURE;
            }

        }

        if (i4SigProtocol == TE_SIGPROTO_LDP)
        {
#ifdef MPLS_LDP_WANTED
            if ((TeCliTunnelCRLDPResourceTable (CliHandle,
                                                u4Bandwidth,
                                                u4TunnelResourceIndex))
                == CLI_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%%Unable to set CRLDP resource table\n");
                return CLI_FAILURE;

            }
#endif
        }
        if ((nmhTestv2MplsTunnelResourceRowStatus (&u4ErrorCode,
                                                   u4TunnelResourceIndex,
                                                   MPLS_STATUS_ACTIVE))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "%%Unable to set MplsTunnelResourceRowStatus - Active\r\n");
            return CLI_FAILURE;
        }
        if ((nmhSetMplsTunnelResourceRowStatus (u4TunnelResourceIndex,
                                                MPLS_STATUS_ACTIVE))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "%%Unable to set MplsTunnelResourceRowStatus - Active\r\n");
            return CLI_FAILURE;
        }
        /* Default Tunnel Resource Index can't be deleted,
         * So, creating a new one. */
        au4ResourceTableOid[MPLS_TE_RSRC_TABLE_MAX_RATE_OFFSET - 1] =
            u4TunnelResourceIndex;
        ResourcePointer.pu4_OidList = au4ResourceTableOid;
        ResourcePointer.u4_Length = MPLS_TE_RSRC_TABLE_MAX_RATE_OFFSET;

        if ((nmhTestv2MplsTunnelResourcePointer
             (&u4ErrorCode, u4TunnelIndex, u4TunnelInstance, u4IngressId,
              u4EgressId, &ResourcePointer)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set MplsTunnelResourcePointer\r\n");
            return CLI_FAILURE;
        }

        if ((nmhSetMplsTunnelResourcePointer
             (u4TunnelIndex, u4TunnelInstance, u4IngressId, u4EgressId,
              &ResourcePointer)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set MplsTunnelResourcePointer\r\n");
            return CLI_FAILURE;
        }

    }
    if ((nmhGetMplsTunnelRole (u4TunnelIndex,
                               u4TunnelInstance,
                               u4IngressId,
                               u4EgressId,
                               &i4RetValMplsTunnelRole)) != SNMP_FAILURE)
    {
        if ((i4SigProtocol == TE_SIGPROTO_LDP) ||
            (i4SigProtocol == TE_SIGPROTO_RSVP))
        {
            if (i4RetValMplsTunnelRole != TE_INGRESS)
            {
                CliPrintf (CliHandle,
                           "\r%%ERROR : Signalling protocol can't be set as RSVP/LDP since Tunnel Role is not Ingress.\r\n\n");
                return CLI_FAILURE;
            }
        }
    }

    if (nmhTestv2MplsTunnelSignallingProto
        (&u4ErrorCode, u4TunnelIndex, u4TunnelInstance, u4IngressId, u4EgressId,
         i4SigProtocol) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\n%% Invalid Tunnel signalling protocol type\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetMplsTunnelSignallingProto (u4TunnelIndex, u4TunnelInstance,
                                         u4IngressId, u4EgressId,
                                         i4SigProtocol) != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle,
                   "\r\n%% Tunnel must be made down to configure this\r\n");
        return CLI_FAILURE;
    }

    if (i4PrevRowStatus != TE_NOTREADY)
    {
        TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                     u4IngressId, u4EgressId, i4PrevRowStatus);
    }

    if ((i4SigProtocol == TE_SIGPROTO_NONE) && (u4TunnelInstance == TE_ZERO))
    {
        pInstance0Tnl = TeGetTunnelInfo (u4TunnelIndex, TE_ZERO, u4IngressId,
                                         u4EgressId);
        if ((pInstance0Tnl != NULL)
            && (pInstance0Tnl->u4OrgTnlInstance != TE_ZERO))
        {
            pInstance0Tnl->u4OrgTnlInstance = TE_ZERO;
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliTunnelSetTnlType 
* Description   : This routine is used to set the tunnel type
* Input(s)      : CliHandle     - Cli Context Handle
*               : u4TnlType - Tunnel type (mpls/mpls-tp/gmpls/h-lsp/s-lsp)
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliTunnelSetTnlType (tCliHandle CliHandle, UINT4 u4TnlType)
{
    tSNMP_OCTET_STRING_TYPE MplsTeTnlType;
    UINT4               u4ErrorCode = 0;
    UINT4               u4TunnelIndex = 0;
    UINT4               u4TunnelInstance = 0;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;
    INT4                i4RowStatus = 0;
    UINT1               au1TnlType[CLI_MPLS_TE_TNL_TYPE] = { 0 };

    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
    if ((nmhGetMplsTunnelRowStatus
         (u4TunnelIndex, u4TunnelInstance, u4IngressId,
          u4EgressId, &i4RowStatus)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Tunnel doesnot exist\r\n");
        return CLI_FAILURE;
    }

    if (i4RowStatus == ACTIVE)
    {
        if ((nmhTestv2MplsTunnelRowStatus
             (&u4ErrorCode, u4TunnelIndex, u4TunnelInstance, u4IngressId,
              u4EgressId, TE_NOTINSERVICE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Not able to make tunnel down\r\n");
            return CLI_FAILURE;
        }

        if ((nmhSetMplsTunnelRowStatus
             (u4TunnelIndex, u4TunnelInstance, u4IngressId,
              u4EgressId, TE_NOTINSERVICE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Not able to make tunnel down\r\n");
            return CLI_FAILURE;
        }
    }
    MplsTeTnlType.pu1_OctetList = au1TnlType;
    MplsTeTnlType.i4_Length = CLI_MPLS_TE_TNL_TYPE;

    MplsTeTnlType.pu1_OctetList[TE_ZERO] = (UINT1) u4TnlType;

    if ((nmhTestv2FsMplsTunnelType
         (&u4ErrorCode, u4TunnelIndex, u4TunnelInstance, u4IngressId,
          u4EgressId, &MplsTeTnlType)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Not able to set tunnel type\r\n");
        return CLI_FAILURE;
    }
    if ((nmhSetFsMplsTunnelType
         (u4TunnelIndex, u4TunnelInstance, u4IngressId,
          u4EgressId, &MplsTeTnlType)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Not able to set tunnel type\r\n");
        return CLI_FAILURE;
    }

    if (i4RowStatus == ACTIVE)
    {
        if ((nmhTestv2MplsTunnelRowStatus
             (&u4ErrorCode, u4TunnelIndex, u4TunnelInstance, u4IngressId,
              u4EgressId, TE_ACTIVE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Not able to make tunnel up\r\n");
            return CLI_FAILURE;
        }

        if ((nmhSetMplsTunnelRowStatus
             (u4TunnelIndex, u4TunnelInstance, u4IngressId,
              u4EgressId, TE_ACTIVE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Not able to make tunnel up\r\n");
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliTunnelSetTnlMode 
* Description   : This routine is used to set the tunnel mode
* Input(s)      : CliHandle     - Cli Context Handle
*               : u4TnlMode - Tunnel mode (unidirectional/corouted-bidirectional/
*                                          associated-bidirectional)
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliTunnelSetTnlMode (tCliHandle CliHandle, UINT4 u4TnlMode)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4TunnelIndex = 0;
    UINT4               u4TunnelInstance = 0;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;
    INT4                i4RowStatus = 0;

    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
    if ((nmhGetMplsTunnelRowStatus
         (u4TunnelIndex, u4TunnelInstance, u4IngressId,
          u4EgressId, &i4RowStatus)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Tunnel doesnot exist\r\n");
        return CLI_FAILURE;
    }

    if (i4RowStatus == ACTIVE)
    {
        if ((nmhTestv2MplsTunnelRowStatus
             (&u4ErrorCode, u4TunnelIndex, u4TunnelInstance, u4IngressId,
              u4EgressId, TE_NOTINSERVICE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Not able to make tunnel down\r\n");
            return CLI_FAILURE;
        }

        if ((nmhSetMplsTunnelRowStatus
             (u4TunnelIndex, u4TunnelInstance, u4IngressId,
              u4EgressId, TE_NOTINSERVICE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Not able to make tunnel down\r\n");
            return CLI_FAILURE;
        }
    }
    if (u4TnlMode == MPLS_CLI_TE_COROUTED_BIDIRECTIONAL)
    {
        u4TnlMode = TE_TNL_MODE_COROUTED_BIDIRECTIONAL;
    }
    else if (u4TnlMode == MPLS_CLI_TE_ASSOCIATED_BIDIRECTIONAL)
    {
        u4TnlMode = TE_TNL_MODE_ASSOCIATED_BIDIRECTIONAL;
    }
    else
    {
        u4TnlMode = TE_TNL_MODE_UNIDIRECTIONAL;
    }

    if ((nmhTestv2FsMplsTunnelMode
         (&u4ErrorCode, u4TunnelIndex, u4TunnelInstance, u4IngressId,
          u4EgressId, (INT4) u4TnlMode)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Not able to set tunnel mode\r\n");
        return CLI_FAILURE;
    }
    if ((nmhSetFsMplsTunnelMode
         (u4TunnelIndex, u4TunnelInstance, u4IngressId,
          u4EgressId, u4TnlMode)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Not able to set tunnel mode\r\n");
        return CLI_FAILURE;
    }

    /*Set Bandwidth in case of MBB enabled */
    TeCliSetMplsTunnelOldBandwidth (u4TunnelIndex, u4TunnelInstance,
                                    u4IngressId, u4EgressId);

    if (i4RowStatus == ACTIVE)
    {
        if ((nmhTestv2MplsTunnelRowStatus
             (&u4ErrorCode, u4TunnelIndex, u4TunnelInstance, u4IngressId,
              u4EgressId, TE_ACTIVE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Not able to make tunnel up\r\n");
            return CLI_FAILURE;
        }

        if ((nmhSetMplsTunnelRowStatus
             (u4TunnelIndex, u4TunnelInstance, u4IngressId,
              u4EgressId, TE_ACTIVE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Not able to make tunnel up\r\n");
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliTunnelSetDestTnlInfo
* Description   : This routine is used to configure the destination tunnel
*                 information.
* Input(s)      : CliHandle     - Cli Context Handle
*               : u4DestTnlIndex - Reverse Tunnel index
*                 u4DestLspNum - Reverse LSP number
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliTunnelSetDestTnlInfo (tCliHandle CliHandle, UINT4 u4DestTnlIndex,
                           UINT4 u4DestLspNum)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4TunnelIndex = 0;
    UINT4               u4TunnelInstance = 0;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;
    INT4                i4RowStatus = 0;

    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
    if ((nmhGetMplsTunnelRowStatus
         (u4TunnelIndex, u4TunnelInstance, u4IngressId,
          u4EgressId, &i4RowStatus)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Tunnel doesnot exist\r\n");
        return CLI_FAILURE;
    }

    /*Set Bandwidth in case of MBB enabled */
    TeCliSetMplsTunnelOldBandwidth (u4TunnelIndex, u4TunnelInstance,
                                    u4IngressId, u4EgressId);

    if (i4RowStatus == ACTIVE)
    {
        if ((nmhTestv2MplsTunnelRowStatus
             (&u4ErrorCode, u4TunnelIndex, u4TunnelInstance, u4IngressId,
              u4EgressId, TE_NOTINSERVICE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Not able to make tunnel down\r\n");
            return CLI_FAILURE;
        }

        if ((nmhSetMplsTunnelRowStatus
             (u4TunnelIndex, u4TunnelInstance, u4IngressId,
              u4EgressId, TE_NOTINSERVICE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Not able to make tunnel down\r\n");
            return CLI_FAILURE;
        }
    }
    if ((nmhTestv2FsMplsTpTunnelDestTunnelIndex
         (&u4ErrorCode, u4TunnelIndex, u4TunnelInstance, u4IngressId,
          u4EgressId, u4DestTnlIndex)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Not able to set reverse "
                   "tunnel number\r\n");
        return CLI_FAILURE;
    }
    if ((nmhSetFsMplsTpTunnelDestTunnelIndex
         (u4TunnelIndex, u4TunnelInstance, u4IngressId,
          u4EgressId, u4DestTnlIndex)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Not able to set reverse "
                   "tunnel number\r\n");
        return CLI_FAILURE;
    }
    if (u4DestLspNum != 0)
    {
        if ((nmhTestv2FsMplsTpTunnelDestTunnelLspNum
             (&u4ErrorCode, u4TunnelIndex, u4TunnelInstance, u4IngressId,
              u4EgressId, u4DestLspNum)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Not able to set reverse tunnel "
                       "lsp number\r\n");
            return CLI_FAILURE;
        }
        if ((nmhSetFsMplsTpTunnelDestTunnelLspNum
             (u4TunnelIndex, u4TunnelInstance, u4IngressId,
              u4EgressId, u4DestLspNum)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Not able to set reverse tunnel "
                       "lsp number\r\n");
            return CLI_FAILURE;
        }
    }
    if (i4RowStatus == ACTIVE)
    {
        if ((nmhTestv2MplsTunnelRowStatus
             (&u4ErrorCode, u4TunnelIndex, u4TunnelInstance, u4IngressId,
              u4EgressId, TE_ACTIVE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Not able to make tunnel up\r\n");
            return CLI_FAILURE;
        }

        if ((nmhSetMplsTunnelRowStatus
             (u4TunnelIndex, u4TunnelInstance, u4IngressId,
              u4EgressId, TE_ACTIVE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Not able to make tunnel up\r\n");
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliTunnelResetDestTnlInfo
* Description   : This routine is used to deconfigure the destination tunnel
*                 information.
* Input(s)      : CliHandle     - Cli Context Handle
*               : u4Mask - MPLS_TE_DESTINATION_TUNNEL and/or MPLS_TE_DESTINATION_LSP
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliTunnelResetDestTnlInfo (tCliHandle CliHandle, UINT4 u4Mask)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4TunnelIndex = 0;
    UINT4               u4TunnelInstance = 0;
    UINT4               u4IngressId = 0;
    UINT4               u4EgressId = 0;
    INT4                i4RowStatus = 0;

    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
    if ((nmhGetMplsTunnelRowStatus
         (u4TunnelIndex, u4TunnelInstance, u4IngressId,
          u4EgressId, &i4RowStatus)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Tunnel doesnot exist\r\n");
        return CLI_FAILURE;
    }

    if (i4RowStatus == ACTIVE)
    {
        if ((nmhTestv2MplsTunnelRowStatus
             (&u4ErrorCode, u4TunnelIndex, u4TunnelInstance, u4IngressId,
              u4EgressId, TE_NOTINSERVICE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Not able to make tunnel down\r\n");
            return CLI_FAILURE;
        }

        if ((nmhSetMplsTunnelRowStatus
             (u4TunnelIndex, u4TunnelInstance, u4IngressId,
              u4EgressId, TE_NOTINSERVICE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Not able to make tunnel down\r\n");
            return CLI_FAILURE;
        }
    }
    if (u4Mask == MPLS_CLI_TE_DESTINATION_TUNNEL)
    {
        if ((nmhTestv2FsMplsTpTunnelDestTunnelIndex
             (&u4ErrorCode, u4TunnelIndex, u4TunnelInstance, u4IngressId,
              u4EgressId, TE_ZERO)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Not able to set reverse "
                       "tunnel number\r\n");
            return CLI_FAILURE;
        }
        if ((nmhSetFsMplsTpTunnelDestTunnelIndex
             (u4TunnelIndex, u4TunnelInstance, u4IngressId,
              u4EgressId, TE_ZERO)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Not able to set reverse "
                       "tunnel number\r\n");
            return CLI_FAILURE;
        }
    }
    if (u4Mask == MPLS_CLI_TE_DESTINATION_LSP)
    {
        if ((nmhTestv2FsMplsTpTunnelDestTunnelLspNum
             (&u4ErrorCode, u4TunnelIndex, u4TunnelInstance, u4IngressId,
              u4EgressId, TE_ZERO)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Not able to set reverse tunnel "
                       "lsp number\r\n");
            return CLI_FAILURE;
        }
        if ((nmhSetFsMplsTpTunnelDestTunnelLspNum
             (u4TunnelIndex, u4TunnelInstance, u4IngressId,
              u4EgressId, TE_ZERO)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Not able to set reverse tunnel "
                       "lsp number\r\n");
            return CLI_FAILURE;
        }
    }
    if (i4RowStatus == ACTIVE)
    {
        if ((nmhTestv2MplsTunnelRowStatus
             (&u4ErrorCode, u4TunnelIndex, u4TunnelInstance, u4IngressId,
              u4EgressId, TE_ACTIVE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Not able to make tunnel up\r\n");
            return CLI_FAILURE;
        }

        if ((nmhSetMplsTunnelRowStatus
             (u4TunnelIndex, u4TunnelInstance, u4IngressId,
              u4EgressId, TE_ACTIVE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Not able to make tunnel up\r\n");
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
 *  Function Name : TeCliTunnelSigBandwidth  
 *  Description   : Depeding upon the Signalling protocol,
 *                  this function sets the bandwidth in the appropriate table
 *  Input(s)      :  CliHandle     - Cli Context Handle
 *                   UINT4 u4TunnelIndex - Index of the tunnel
 *                   INT4 i4SigProtocol - Protocol configured in tunnel
 *  Output(s)     : None.
 *  Return Values : CLI_SUCCESS/CLI_FAILURE
 ********************************************************************************/
INT4
TeCliTunnelSigBandwidth (tCliHandle CliHandle,
                         UINT4 u4TunnelResourceIndex, INT4 i4SigProtocol,
                         UINT4 u4BandWidth)
{
    UINT4               u4ErrorCode;
    /* If tunnel signalling protocol is none, Bandwidth information should be 
     * stored in standard resource table otherwise Bandwidth information should 
     * be stored in proprietary table. Store the bandwidth accordingly*/

    if (i4SigProtocol == TE_SIGPROTO_RSVP)
    {
        if ((nmhTestv2MplsTunnelResourceRowStatus (&u4ErrorCode,
                                                   u4TunnelResourceIndex,
                                                   MPLS_STATUS_CREATE_AND_WAIT))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "%%Unable to create MplsTunnelResourceTable\r\n");
            return CLI_FAILURE;
        }

        if ((nmhSetMplsTunnelResourceRowStatus (u4TunnelResourceIndex,
                                                MPLS_STATUS_CREATE_AND_WAIT))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "%%Unable to create MplsTunnelResourceTable\r\n");
            return CLI_FAILURE;
        }

        if ((nmhTestv2FsMplsTunnelRSVPResTokenBucketRate (&u4ErrorCode,
                                                          u4TunnelResourceIndex,
                                                          u4BandWidth)) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set MplsTunnelResourceMaxRate\r\n");
            return CLI_FAILURE;
        }

        if ((nmhSetFsMplsTunnelRSVPResTokenBucketRate (u4TunnelResourceIndex,
                                                       u4BandWidth)) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set MplsTunnelResourceMaxRate\r\n");
            return CLI_FAILURE;
        }

        if ((nmhTestv2FsMplsTunnelRSVPResPeakDataRate (&u4ErrorCode,
                                                       u4TunnelResourceIndex,
                                                       u4BandWidth)) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set MplsTunnelResourceMaxRate\r\n");

            return CLI_FAILURE;
        }
        if ((nmhSetFsMplsTunnelRSVPResPeakDataRate (u4TunnelResourceIndex,
                                                    u4BandWidth)) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set MplsTunnelResourceMaxRate\r\n");

            return CLI_FAILURE;
        }
    }
    else if (i4SigProtocol == TE_SIGPROTO_LDP)
    {
#ifdef MPLS_LDP_WANTED
        if ((TeCliTunnelCRLDPResourceTable (CliHandle,
                                            u4BandWidth,
                                            u4TunnelResourceIndex)) ==
            CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set CRLDP resource table\n");
            return CLI_FAILURE;
        }
#endif
    }
    else if (i4SigProtocol == TE_SIGPROTO_NONE)
    {
        if ((nmhTestv2MplsTunnelResourceRowStatus (&u4ErrorCode,
                                                   u4TunnelResourceIndex,
                                                   MPLS_STATUS_CREATE_AND_WAIT))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "%%Unable to create MplsTunnelResourceTable\r\n");
            return CLI_FAILURE;
        }

        if ((nmhSetMplsTunnelResourceRowStatus (u4TunnelResourceIndex,
                                                MPLS_STATUS_CREATE_AND_WAIT))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "%%Unable to create MplsTunnelResourceTable\r\n");
            return CLI_FAILURE;
        }

        /* Set Tunnel Resource Max Rate */
        if ((nmhTestv2MplsTunnelResourceMaxRate (&u4ErrorCode,
                                                 u4TunnelResourceIndex,
                                                 u4BandWidth)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set MplsTunnelResourceMaxRate\r\n");
            return CLI_FAILURE;
        }

        if ((nmhSetMplsTunnelResourceMaxRate (u4TunnelResourceIndex,
                                              u4BandWidth)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set MplsTunnelResourceMaxRate\r\n");
            return CLI_FAILURE;
        }
    }
    else
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to set MplsTunnelResourceMaxRate\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
 *  Function Name : MplsIpExplicitShowRunningConfig 
 *  Description   : This function shows the Ip Explicit TunnelHop Entries
 *  Input(s)      : tCliHandle CliHandle - CLI Handle.
 *  Output(s)     : None.
 *  Return Values : CLI_SUCCESS/CLI_FAILURE
 ********************************************************************************/
INT4
MplsIpExplicitShowRunningConfig (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE HopUnnumIf;
    tSNMP_OCTET_STRING_TYPE HopIpAddr;

    UINT4               u4HopListIdPrv = TE_ZERO;
    UINT4               u4PathNumPrv = TE_ZERO;
    UINT4               u4TnlHopIdPrv = TE_ZERO;
    UINT4               u4HopListId = TE_ZERO;
    UINT4               u4PathNum = TE_ZERO;
    UINT4               u4TnlHopId = TE_ZERO;
    BOOL1               bLoop = FALSE;
    UINT4               u4UnnumIf = TE_ZERO;

    INT4                i4HopAddrType = TE_ZERO;
    INT4                i4IncludeExclude = TE_ZERO;
    INT4                i4HopType = TE_ZERO;
    INT4                i4HopPathComp = TE_ZERO;
    INT4                i4IncludeAny = TE_SNMP_FALSE;
    UINT4               u4FrwdLbl = TE_ZERO;
    UINT4               u4RevLbl = TE_ZERO;

    static UINT1        au1HopUnnumIf[TE_FOUR] = { TE_ZERO };
    static UINT1        au1HopIpAddr[TE_FOUR] = { TE_ZERO };

    HopUnnumIf.pu1_OctetList = au1HopUnnumIf;
    HopUnnumIf.i4_Length = TE_FOUR;
    HopIpAddr.pu1_OctetList = au1HopIpAddr;
    HopIpAddr.i4_Length = TE_FOUR;

    /*Fetch each entry of tunnel Hop Table */
    while (TRUE)
    {
        /* get next entry */
        if ((TRUE == bLoop) &&
            (SNMP_FAILURE ==
             nmhGetNextIndexGmplsTunnelHopTable (u4HopListIdPrv, &u4HopListId,
                                                 u4PathNumPrv, &u4PathNum,
                                                 u4TnlHopIdPrv, &u4TnlHopId)))

        {
            break;
        }
        /* get first entry */
        if ((FALSE == bLoop) &&
            (SNMP_FAILURE ==
             nmhGetFirstIndexGmplsTunnelHopTable (&u4HopListId, &u4PathNum,
                                                  &u4TnlHopId)))
        {
            break;
        }

        bLoop = TRUE;
        nmhGetMplsTunnelHopEntryPathComp (u4HopListId, u4PathNum, u4TnlHopId,
                                          &i4HopPathComp);

        if (i4HopPathComp != TE_DYNAMIC)
        {
            u4HopListIdPrv = u4HopListId;
            u4PathNumPrv = u4PathNum;
            u4TnlHopIdPrv = u4TnlHopId;
            continue;
        }

        if ((u4HopListId != 0) && (u4PathNum != 0) &&
            ((u4HopListIdPrv != u4HopListId) || (u4PathNumPrv != u4PathNum)))
        {
            CliPrintf (CliHandle, "\n!\r\n");
            FilePrintf (CliHandle, "\n!\r\n");
            CliPrintf (CliHandle, "ip explicit-path identifier %d ",
                       u4HopListId);
            FilePrintf (CliHandle, "ip explicit-path identifier %d ",
                        u4HopListId);

            if (u4PathNum == 1)
            {
                CliPrintf (CliHandle, "\n");
                FilePrintf (CliHandle, "\n");
            }

            if (u4PathNum != 1)
            {
                CliPrintf (CliHandle, "path-option number %d\n", u4PathNum);
                FilePrintf (CliHandle, "path-option number %d\n", u4PathNum);
            }

        }

        u4HopListIdPrv = u4HopListId;
        u4PathNumPrv = u4PathNum;
        u4TnlHopIdPrv = u4TnlHopId;

        nmhGetMplsTunnelHopAddrType (u4HopListId, u4PathNum, u4TnlHopId,
                                     &i4HopAddrType);

        nmhGetMplsTunnelHopIpAddr (u4HopListId, u4PathNum, u4TnlHopId,
                                   &HopIpAddr);

        nmhGetMplsTunnelHopInclude (u4HopListId, u4PathNum,
                                    u4TnlHopId, &i4IncludeExclude);

        nmhGetFsMplsTunnelHopIncludeAny (u4HopListId, u4PathNum,
                                         u4TnlHopId, &i4IncludeAny);

        nmhGetMplsTunnelHopAddrUnnum (u4HopListId, u4PathNum,
                                      u4TnlHopId, &HopUnnumIf);
        nmhGetMplsTunnelHopType (u4HopListId, u4PathNum,
                                 u4TnlHopId, &i4HopType);
        nmhGetGmplsTunnelHopExplicitForwardLabel (u4HopListId, u4PathNum,
                                                  u4TnlHopId, &u4FrwdLbl);
        nmhGetGmplsTunnelHopExplicitReverseLabel (u4HopListId, u4PathNum,
                                                  u4TnlHopId, &u4RevLbl);

        MPLS_OCTETSTRING_TO_INTEGER ((&HopUnnumIf), u4UnnumIf);

        CliPrintf (CliHandle, "\r index %d", u4TnlHopId);
        FilePrintf (CliHandle, "\r index %d", u4TnlHopId);

        if (i4IncludeAny == TE_SNMP_TRUE)
        {
            CliPrintf (CliHandle, " include-any ");
            FilePrintf (CliHandle, " include-any ");
        }
        else if (i4IncludeExclude == TE_SNMP_FALSE)
        {
            CliPrintf (CliHandle, " exclude-address ");
            FilePrintf (CliHandle, " exclude-address ");
        }
        else if (i4IncludeExclude == TE_SNMP_TRUE)
        {
            CliPrintf (CliHandle, " next-address ");
            FilePrintf (CliHandle, " next-address ");

            if (i4HopType == TE_STRICT_ER)
            {
                CliPrintf (CliHandle, "strict ");
                FilePrintf (CliHandle, "strict ");
            }
            else
            {
                CliPrintf (CliHandle, "loose ");
                FilePrintf (CliHandle, "loose ");
            }
        }

        CliPrintf (CliHandle, " %d.%d.%d.%d",
                   HopIpAddr.pu1_OctetList[0],
                   HopIpAddr.pu1_OctetList[1],
                   HopIpAddr.pu1_OctetList[2], HopIpAddr.pu1_OctetList[3]);
        FilePrintf (CliHandle, " %d.%d.%d.%d",
                    HopIpAddr.pu1_OctetList[0],
                    HopIpAddr.pu1_OctetList[1],
                    HopIpAddr.pu1_OctetList[2], HopIpAddr.pu1_OctetList[3]);

        if (u4UnnumIf != TE_ZERO)
        {
            CliPrintf (CliHandle, "%d ", u4UnnumIf);
            FilePrintf (CliHandle, "%d ", u4UnnumIf);
        }

        if (u4FrwdLbl != MPLS_INVALID_LABEL)
        {
            CliPrintf (CliHandle, " forward-label %d ", u4FrwdLbl);
            FilePrintf (CliHandle, " forward-label %d ", u4FrwdLbl);
        }

        if (u4RevLbl != MPLS_INVALID_LABEL)
        {
            CliPrintf (CliHandle, " reverse-label %d ", u4RevLbl);
            FilePrintf (CliHandle, " reverse-label %d ", u4RevLbl);
        }
        CliPrintf (CliHandle, "\r\n");
        FilePrintf (CliHandle, "\r\n");
    }
    return CLI_SUCCESS;
}

/*******************************************************************************
 *  Function Name : MplsLspAttributesShowRunningConfig 
 *  Description   : This function shows the Mpls Attribute Table Entries
 *  Input(s)      : tCliHandle CliHandle - CLI Handle.
 *  Output(s)     : None.
 *  Return Values : CLI_SUCCESS/CLI_FAILURE
 ********************************************************************************/
INT4
MplsLspAttributesShowRunningConfig (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE AttributeName;
    tSNMP_OCTET_STRING_TYPE SsnAttr;
    UINT4               u4AttrIndexPrv = TE_ZERO;
    UINT4               u4AttrIndex = TE_ZERO;
    INT4                i4AttrSetupPrio = TE_ZERO;
    INT4                i4AttrHoldPrio = TE_ZERO;
    UINT4               u4IncludeAnyAffinity = TE_ZERO;
    UINT4               u4IncludeAllAffinity = TE_ZERO;
    UINT4               u4ExcludeAnyAffinity = TE_ZERO;
    UINT4               u4Bandwidth = TE_ZERO;
    INT4                i4SrlgType = TE_ZERO;
    UINT4               u4ClassType = TE_ZERO;
    UINT4               u4AttrIndexSrlgPrv = TE_ZERO;
    UINT4               u4SrlgNoPrv = TE_ZERO;
    UINT4               u4AttrIndexSrlg = TE_ZERO;
    UINT4               u4SrlgNo = TE_ZERO;
    UINT4               u4ListBitmask = TE_ZERO;
    BOOL1               bLoop = FALSE;
    UINT1               au1AttrName[TE_FOUR] = { TE_ZERO };
    UINT1               au1SsnAttr[TE_FOUR] = { TE_ZERO };

    AttributeName.pu1_OctetList = au1AttrName;
    AttributeName.i4_Length = TE_FOUR;
    SsnAttr.pu1_OctetList = au1SsnAttr;
    SsnAttr.i4_Length = TE_FOUR;

    while (TRUE)
    {
        u4AttrIndexSrlgPrv = TE_ZERO;
        u4AttrIndexSrlg = TE_ZERO;
        u4SrlgNoPrv = TE_ZERO;
        u4SrlgNo = TE_ZERO;

        /* get next entry */
        if ((TRUE == bLoop) &&
            (SNMP_FAILURE ==
             nmhGetNextIndexFsTunnelAttributeTable (u4AttrIndexPrv,
                                                    &u4AttrIndex)))
        {
            break;
        }

        /* get first entry */
        if ((FALSE == bLoop) &&
            (SNMP_FAILURE ==
             nmhGetFirstIndexFsTunnelAttributeTable (&u4AttrIndex)))
        {
            break;
        }

        u4AttrIndexPrv = u4AttrIndex;
        bLoop = TRUE;

        nmhGetFsTunnelAttributeName (u4AttrIndex, &AttributeName);
        nmhGetFsTunnelAttributeSetupPrio (u4AttrIndex, &i4AttrSetupPrio);
        nmhGetFsTunnelAttributeHoldingPrio (u4AttrIndex, &i4AttrHoldPrio);
        nmhGetFsTunnelAttributeIncludeAnyAffinity (u4AttrIndex,
                                                   &u4IncludeAnyAffinity);
        nmhGetFsTunnelAttributeIncludeAllAffinity (u4AttrIndex,
                                                   &u4IncludeAllAffinity);
        nmhGetFsTunnelAttributeExcludeAnyAffinity (u4AttrIndex,
                                                   &u4ExcludeAnyAffinity);
        nmhGetFsTunnelAttributeSessionAttributes (u4AttrIndex, &SsnAttr);
        nmhGetFsTunnelAttributeSrlgType (u4AttrIndex, &i4SrlgType);
        nmhGetFsTunnelAttributeTeClassType (u4AttrIndex, (INT4 *) &u4ClassType);
        nmhGetFsTunnelAttributeBandwidth (u4AttrIndex, &u4Bandwidth);

        u4ListBitmask = TeSigExtGetAttributeListMask (u4AttrIndex);

        CliPrintf (CliHandle, "\n!\r\n");
        FilePrintf (CliHandle, "\n!\r\n");
        CliPrintf (CliHandle,
                   " mpls traffic-eng lsp attributes %s",
                   AttributeName.pu1_OctetList);
        FilePrintf (CliHandle,
                    " mpls traffic-eng lsp attributes %s",
                    AttributeName.pu1_OctetList);

        if (u4ListBitmask & TE_ATTR_SETUPPRI_BITMASK)
        {
            CliPrintf (CliHandle, "\r\n  priority %d", i4AttrSetupPrio);
            FilePrintf (CliHandle, "\r\n  priority %d", i4AttrSetupPrio);

            if (u4ListBitmask & TE_ATTR_HOLDPRI_BITMASK)
            {
                CliPrintf (CliHandle, " %d", i4AttrHoldPrio);
                FilePrintf (CliHandle, " %d", i4AttrHoldPrio);
            }
        }

        if ((u4ListBitmask & TE_ATTR_INCLUDE_ANY_AFFINITY_BITMASK) ||
            (u4ListBitmask & TE_ATTR_INCLUDE_ALL_AFFINITY_BITMASK) ||
            (u4ListBitmask & TE_ATTR_EXCLUDE_ANY_AFFINITY_BITMASK))
        {
            CliPrintf (CliHandle,
                       "\r\n  affinity %d %d %d",
                       u4IncludeAnyAffinity, u4IncludeAllAffinity,
                       u4ExcludeAnyAffinity);
            FilePrintf (CliHandle,
                        "\r\n  affinity %d %d %d",
                        u4IncludeAnyAffinity, u4IncludeAllAffinity,
                        u4ExcludeAnyAffinity);

        }

        if (u4ListBitmask & TE_ATTR_BANDWIDTH_BITMASK)
        {
            CliPrintf (CliHandle, "\r\n  bandwidth %d", u4Bandwidth);
            FilePrintf (CliHandle, "\r\n  bandwidth %d", u4Bandwidth);
        }

        if (u4ListBitmask & TE_ATTR_CLASS_TYPE_BITMASK)
        {
            CliPrintf (CliHandle, "\r\n  classtype %d", u4ClassType);
            FilePrintf (CliHandle, "\r\n  classtype %d", u4ClassType);
        }

        if (u4ListBitmask & TE_ATTR_LSP_SSN_ATTR_BITMASK)
        {
            if (SsnAttr.pu1_OctetList[TE_ZERO] & TE_SSN_REC_ROUTE_BIT)
            {
                CliPrintf (CliHandle, "\r\n  record-route");
                FilePrintf (CliHandle, "\r\n  record-route");
            }
        }

        if (u4ListBitmask & TE_ATTR_SRLG_TYPE_BITMASK)
        {
            CliPrintf (CliHandle, "\r\n  srlg type");
            FilePrintf (CliHandle, "\r\n  srlg type");

            if (i4SrlgType == TE_TNL_SRLG_EXCLUDE_ALL_AFFINITY)
            {
                CliPrintf (CliHandle, " exclude");
                FilePrintf (CliHandle, " exclude");
            }
            else if (i4SrlgType == TE_TNL_SRLG_INCLUDE_ALL_AFFINITY)
            {
                CliPrintf (CliHandle, " include all");
                FilePrintf (CliHandle, " include all");
            }
            else
            {
                CliPrintf (CliHandle, " include any");
                FilePrintf (CliHandle, " include any");
            }
        }

        /*This while is for Attribute srlg parameters */
        while (nmhGetNextIndexFsTunnelAttributeSrlgTable (u4AttrIndexSrlgPrv,
                                                          &u4AttrIndexSrlg,
                                                          u4SrlgNoPrv,
                                                          &u4SrlgNo) ==
               SNMP_SUCCESS)
        {
            if (u4AttrIndexSrlg != u4AttrIndex)
            {
                break;
            }

            CliPrintf (CliHandle, "\r\n  srlg %d", u4SrlgNo);
            FilePrintf (CliHandle, "\r\n  srlg %d", u4SrlgNo);

            u4AttrIndexSrlgPrv = u4AttrIndexSrlg;
            u4SrlgNoPrv = u4SrlgNo;
        }
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
 *  Function Name : MplsTunnelShowRunningConfig
 *  Description   : This function shows the Mpls Tunnel Entries
 *  Input(s)      : tCliHandle CliHandle - CLI Handle.
 *  Output(s)     : None.
 *  Return Values : CLI_SUCCESS/CLI_FAILURE
 ********************************************************************************/
INT4
MplsTunnelShowRunningConfig (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE LSRIdMapInfo;
    tSNMP_OCTET_STRING_TYPE MplsTeTnlType;
    tSNMP_OCTET_STRING_TYPE AdminStatusFlag;
    tSNMP_OCTET_STRING_TYPE ProtectionType;
    tSNMP_OCTET_STRING_TYPE TeSessionAttribute;
    tSNMP_OID_TYPE      GetAttrPointer;
    tSNMP_OID_TYPE      ResourcePointer;
    UINT4               u4TunnelInstancePrev = 0;
    UINT4               u4TunnelInstance = 0;
    UINT4               u4ResourceIndex = 0;
    UINT4               u4IngressIdPrev = 0;
    UINT4               u4TunnelIdPrev = 0;
    UINT4               u4EgressIdPrev = 0;
    UINT4               u4BandWidth = 0;
    UINT4               u4IngressId = 0;
    UINT4               u4TunnelId = 0;
    UINT4               u4EgressId = 0;
    UINT4               u4AttrIndex = 0;
    UINT4               u4AdminStatus = 0;
    UINT1               au1Space[3];
    INT4                i4TunnelAdminStatus = 0;
    CHR1               *pc1Ingress = NULL;
    CHR1               *pc1Egress = NULL;
    BOOL1               bLoop = FALSE;
    INT4                i4RetValMplsTunnelRole = 0;
    INT4                i4SnmpTrapStatus = 0;
    INT4                i4SigProtocol = 0;
    INT1                i1RetVal = 0;
    INT4                i4TnlMode = 0;
    INT4                i4GmplsEnctype = 0;
    INT4                i4GmplsSwtype = 0;
    INT4                i4GmplsGpid = 0;
    INT4                i4MplsDiffservserviceType = 0;
    INT4                i4DiffServLlspPsc = 0;
    INT4                i4ElspListIndex = 0;
    INT4                i4ElspType = 0;
    INT4                i4MbbStatus = 0;
    INT4                i4SetupPriority = 0;
    INT4                i4HoldPriority = 0;
    UINT4               u4ProtType = 0;
    UINT4               u4DestTnlIndex = 0;
    UINT4               u4DestLspNum = 0;
    UINT4               u4Flag = TE_ZERO;
    UINT4               u4HopListIndex = 0;
    UINT4               u4HopPathOptionIndex = 0;
    INT4                i4RowStatus = 0;
    INT4                i4TnlOwner = TE_ZERO;
    INT4                i4ClassType = 0;
    UINT4               u4TempListBitMask = TE_ZERO;
    INT4                i4TunnelFrrConstProtectionMethod = 0;
    UINT4               u4TunnelFrrConstSetupPrio = 0;
    UINT4               u4TunnelFrrConstHoldPrio = TE_ZERO;
    INT4                i4TunnelFrrConstProtType = TE_ZERO;
    UINT4               u4TunnelFrrConstBandwidth = TE_ZERO;
    UINT4               u4TunnelFrrConstHopLimit = TE_ZERO;
    UINT4               u4TnlIncludeAnyAffinity = 0;
    UINT4               u4TnlIncludeAllAffinity = 0;
    UINT4               u4TnlExcludeAnyAffinity = 0;
    static UINT4        au4ResourcePtr[MPLS_TE_RSRC_TABLE_MAX_RATE_OFFSET] =
        { 0 };
    static UINT1        au1TnlType[CLI_MPLS_TE_TNL_TYPE] = { 0 };
    static UINT1        au1LSRIdMapInfo[MPLS_ONE] = { 0 };
    static UINT4        au4GetAttrPointer[TE_TNL_ATTR_TABLE_DEF_OFFSET];
    static UINT1        au1AdminStatusFlag[TE_FOUR] = { 0 };
    static UINT1        au1ProtectType[TE_FOUR] = { 0 };
    static UINT1        au1Length[CLI_MPLS_TE_SESS_ATTR] = { 0 };
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlSrlg         *pTnlSrlg = NULL;

    MEMSET (au1Space, 0, sizeof (au1Space));
    ResourcePointer.pu4_OidList = au4ResourcePtr;
    MplsTeTnlType.pu1_OctetList = au1TnlType;
    MplsTeTnlType.i4_Length = CLI_MPLS_TE_TNL_TYPE;
    LSRIdMapInfo.pu1_OctetList = au1LSRIdMapInfo;
    LSRIdMapInfo.i4_Length = sizeof (UINT1);
    GetAttrPointer.pu4_OidList = au4GetAttrPointer;
    AdminStatusFlag.pu1_OctetList = au1AdminStatusFlag;
    AdminStatusFlag.i4_Length = TE_FOUR;
    ProtectionType.pu1_OctetList = au1ProtectType;
    ProtectionType.i4_Length = TE_FOUR;
    TeSessionAttribute.pu1_OctetList = au1Length;
    TeSessionAttribute.i4_Length = CLI_MPLS_TE_SESS_ATTR;

    MEMSET (GetAttrPointer.pu4_OidList, TE_ZERO, sizeof (UINT4) * TE_TWO);
    GetAttrPointer.u4_Length = TE_ZERO;
    nmhGetMplsTunnelNotificationEnable (&i4SnmpTrapStatus);
    if (i4SnmpTrapStatus == TE_ONE)
    {
        CliPrintf (CliHandle, "\r\nsnmp-server enable traps mpls traffic-eng");
        FilePrintf (CliHandle, "\r\nsnmp-server enable traps mpls traffic-eng");
    }

    /*Show - ip explicit hop table parameters */
    if (CLI_SUCCESS != MplsIpExplicitShowRunningConfig (CliHandle))
    {
        return CLI_FAILURE;
    }

    /*Show - Attribute table parameters */
    if (CLI_SUCCESS != MplsLspAttributesShowRunningConfig (CliHandle))
    {
        return CLI_FAILURE;
    }

    /* Fetch every entry in the Tunnel Table */
    while (1)
    {
        /* get next entry */
        if ((TRUE == bLoop) &&
            (SNMP_FAILURE ==
             nmhGetNextIndexMplsTunnelTable (u4TunnelIdPrev, &u4TunnelId,
                                             u4TunnelInstancePrev,
                                             &u4TunnelInstance, u4IngressIdPrev,
                                             &u4IngressId, u4EgressIdPrev,
                                             &u4EgressId)))
        {
            break;
        }
        /* get first entry */
        if ((FALSE == bLoop) &&
            (SNMP_FAILURE ==
             nmhGetFirstIndexMplsTunnelTable (&u4TunnelId, &u4TunnelInstance,
                                              &u4IngressId, &u4EgressId)))
        {
            break;
        }

        u4TunnelIdPrev = u4TunnelId;
        u4TunnelInstancePrev = u4TunnelInstance;
        u4IngressIdPrev = u4IngressId;
        u4EgressIdPrev = u4EgressId;
        bLoop = TRUE;
        i4TnlOwner = TE_ZERO;
        i4RowStatus = TE_ZERO;

        nmhGetMplsTunnelOwner (u4TunnelId, u4TunnelInstance, u4IngressId,
                               u4EgressId, &i4TnlOwner);

        if (i4TnlOwner != TE_TNL_OWNER_SNMP)
        {
            continue;
        }

        if (u4TunnelInstance != TE_ZERO)
        {
            if (nmhGetMplsTunnelRowStatus (u4TunnelId, TE_ZERO, u4IngressId,
                                           u4EgressId,
                                           &i4RowStatus) == SNMP_FAILURE)
            {
                TE_DBG (TE_EXTN_FAIL,
                        "\r\n%%Failed to get tunnel information\r\n");
            }

            if (i4RowStatus != TE_ZERO)
            {
                continue;
            }
        }

        nmhGetMplsTunnelSignallingProto (u4TunnelId, u4TunnelInstance,
                                         u4IngressId, u4EgressId,
                                         &i4SigProtocol);

        /*To retrive the tunnel role */
        if (nmhGetMplsTunnelRole (u4TunnelId, u4TunnelInstance,
                                  u4IngressId, u4EgressId,
                                  &i4RetValMplsTunnelRole) == SNMP_FAILURE)
        {
            continue;
        }

        if ((i4SigProtocol == TE_SIGPROTO_LDP) ||
            (i4SigProtocol == TE_SIGPROTO_RSVP))
        {
            if (i4RetValMplsTunnelRole != TE_INGRESS)
            {
                continue;
            }
        }

        CliPrintf (CliHandle, "\n!");
        FilePrintf (CliHandle, "\n!");
        CliPrintf (CliHandle, "\r\ninterface mplstunnel %u", u4TunnelId);
        FilePrintf (CliHandle, "\r\ninterface mplstunnel %u", u4TunnelId);
        /* To get session attribute */
        if ((nmhGetMplsTunnelSessionAttributes (u4TunnelId, u4TunnelInstance,
                                                u4IngressId, u4EgressId,
                                                &TeSessionAttribute))
            != SNMP_FAILURE)
        {
            /*Check for record-Route by default record-Route is disable */
            if (TeSessionAttribute.
                pu1_OctetList[TE_ZERO] & TE_SSN_REC_ROUTE_BIT)
            {
                CliPrintf (CliHandle,
                           "\r\ntunnel mpls traffic-eng record-route");
                FilePrintf (CliHandle,
                            "\r\ntunnel mpls traffic-eng record-route");
            }
        }
        CliPrintf (CliHandle, "\r\n tunnel mpls destination ");
        FilePrintf (CliHandle, "\r\n tunnel mpls destination ");

        if (nmhGetFsMplsTunnelLSRIdMapInfo (u4TunnelId, u4TunnelInstance,
                                            u4IngressId, u4EgressId,
                                            &LSRIdMapInfo) == SNMP_SUCCESS)
        {
            if (!(au1LSRIdMapInfo[0] & TE_TNL_EGRESSID_MAP_INFO))
            {
                CLI_CONVERT_IPADDR_TO_STR (pc1Egress, u4EgressId);
                CliPrintf (CliHandle, "%s ", pc1Egress);
                FilePrintf (CliHandle, "%s ", pc1Egress);
            }
            else
            {
                CliPrintf (CliHandle, "%d ", u4EgressId);
                FilePrintf (CliHandle, "%d ", u4EgressId);
            }
            if (!(au1LSRIdMapInfo[0] & TE_TNL_INGRESSID_MAP_INFO))
            {
                CLI_CONVERT_IPADDR_TO_STR (pc1Ingress, u4IngressId);
                CliPrintf (CliHandle, "source %s", pc1Ingress);
                FilePrintf (CliHandle, "source %s", pc1Ingress);
            }
            else
            {
                CliPrintf (CliHandle, "source %d", u4IngressId);
                FilePrintf (CliHandle, "source %d", u4IngressId);
            }
            if (u4TunnelInstance != MPLS_ZERO)
            {
                CliPrintf (CliHandle, " lsp-num %d", u4TunnelInstance);
                FilePrintf (CliHandle, " lsp-num %d", u4TunnelInstance);
                STRNCPY (au1Space, "  ", (sizeof (au1Space) - 1));
            }
            else
            {
                STRNCPY (au1Space, "  ", (sizeof (au1Space) - 1));
            }
        }

        if (SNMP_FAILURE !=
            nmhGetFsMplsTunnelType (u4TunnelId, u4TunnelInstance,
                                    u4IngressId, u4EgressId, &MplsTeTnlType))
        {
            if (MplsTeTnlType.pu1_OctetList[TE_ZERO] != TE_TNL_TYPE_MPLS)
            {
                if (MplsTeTnlType.pu1_OctetList[TE_ZERO] != MPLS_ZERO)
                {
                    CliPrintf (CliHandle, "\r\n%stunnel type", au1Space);
                    FilePrintf (CliHandle, "\r\n%stunnel type", au1Space);
                }
                if (MplsTeTnlType.pu1_OctetList[TE_ZERO] & TE_TNL_TYPE_MPLS)
                {
                    CliPrintf (CliHandle, " mpls");
                    FilePrintf (CliHandle, " mpls");
                }
                if (MplsTeTnlType.pu1_OctetList[TE_ZERO] & TE_TNL_TYPE_MPLSTP)
                {
                    CliPrintf (CliHandle, " mpls-tp");
                    FilePrintf (CliHandle, " mpls-tp");
                }
                if (MplsTeTnlType.pu1_OctetList[TE_ZERO] & TE_TNL_TYPE_GMPLS)
                {
                    CliPrintf (CliHandle, " gmpls");
                    FilePrintf (CliHandle, " gmpls");
                }
                if (MplsTeTnlType.pu1_OctetList[TE_ZERO] & TE_TNL_TYPE_HLSP)
                {
                    CliPrintf (CliHandle, " h-lsp");
                    FilePrintf (CliHandle, " h-lsp");
                }
                if (MplsTeTnlType.pu1_OctetList[TE_ZERO] & TE_TNL_TYPE_SLSP)
                {
                    CliPrintf (CliHandle, " s-lsp");
                    FilePrintf (CliHandle, " s-lsp");
                }
                /* MPLS_P2MP_LSP_CHANGES - S */
                if (MplsTeTnlType.pu1_OctetList[TE_ZERO] & TE_TNL_TYPE_P2MP)
                {
                    CliPrintf (CliHandle, " p2mp-lsp");
                    FilePrintf (CliHandle, " p2mp-lsp");
                }
                /* MPLS_P2MP_LSP_CHANGES - E */
            }
        }

        if (SNMP_FAILURE !=
            nmhGetFsMplsTunnelMode (u4TunnelId, u4TunnelInstance,
                                    u4IngressId, u4EgressId, &i4TnlMode))
        {
            if (i4TnlMode != TE_TNL_MODE_UNIDIRECTIONAL)
            {
                CliPrintf (CliHandle, "\r\n%stunnel mode", au1Space);
                FilePrintf (CliHandle, "\r\n%stunnel mode", au1Space);
                if (i4TnlMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
                {
                    CliPrintf (CliHandle, " corouted-bidirectional");
                    FilePrintf (CliHandle, " corouted-bidirectional");
                }
                else if (i4TnlMode == TE_TNL_MODE_ASSOCIATED_BIDIRECTIONAL)
                {
                    CliPrintf (CliHandle, " associated-bidirectional");
                    FilePrintf (CliHandle, " associated-bidirectional");
                }
            }
        }
        if (SNMP_FAILURE !=
            nmhGetFsMplsFrrConstProtectionMethod (u4TunnelId, u4TunnelInstance,
                                                  u4IngressId, u4EgressId,
                                                  &i4TunnelFrrConstProtectionMethod))
        {

            /* Check for the Method of Protection */
            if (i4TunnelFrrConstProtectionMethod != TE_TNL_FRR_ONE2ONE_METHOD)
            {
                CliPrintf (CliHandle,
                           "\r\ntunnel mpls traffic-eng fast-reroute many-to-one");
                FilePrintf (CliHandle,
                            "\r\ntunnel mpls traffic-eng fast-reroute many-to-one");

                /* Check for the Backup Bandwidth */
                if ((SNMP_FAILURE !=
                     nmhGetFsMplsFrrConstBandwidth (u4TunnelId,
                                                    u4TunnelInstance,
                                                    u4IngressId, u4EgressId,
                                                    &u4TunnelFrrConstBandwidth))
                    && (TE_ZERO != u4TunnelFrrConstBandwidth))
                {
                    CliPrintf (CliHandle, " bw-prot %u",
                               u4TunnelFrrConstBandwidth);
                    FilePrintf (CliHandle, " bw-prot %u",
                                u4TunnelFrrConstBandwidth);
                }

                /* Check for protection type, by default Link protection is enabled in the system */
                if ((SNMP_FAILURE !=
                     nmhGetFsMplsFrrConstProtectionType (u4TunnelId,
                                                         u4TunnelInstance,
                                                         u4IngressId,
                                                         u4EgressId,
                                                         &i4TunnelFrrConstProtType))
                    && (TE_TNL_FRR_PROT_NODE == i4TunnelFrrConstProtType))
                {
                    CliPrintf (CliHandle, " node-prot");
                    FilePrintf (CliHandle, " node-prot");
                }

                /* Check for Setup and Holding Priority , By default setup prot is 7 and holding prot is 0 */
                if ((SNMP_FAILURE !=
                     nmhGetFsMplsFrrConstSetupPrio (u4TunnelId,
                                                    u4TunnelInstance,
                                                    u4IngressId, u4EgressId,
                                                    &u4TunnelFrrConstSetupPrio))
                    && (SNMP_FAILURE !=
                        nmhGetFsMplsFrrConstHoldingPrio (u4TunnelId,
                                                         u4TunnelInstance,
                                                         u4IngressId,
                                                         u4EgressId,
                                                         &u4TunnelFrrConstHoldPrio))
                    &&
                    ((TE_TNL_FRR_SETUP_PRIO_DEF_VAL !=
                      u4TunnelFrrConstSetupPrio)
                     || (TE_HOLD_PRIO_DEF_VAL != u4TunnelFrrConstHoldPrio)))
                {
                    CliPrintf (CliHandle, " priority %u %u",
                               u4TunnelFrrConstSetupPrio,
                               u4TunnelFrrConstHoldPrio);
                    FilePrintf (CliHandle, " priority %u %u",
                                u4TunnelFrrConstSetupPrio,
                                u4TunnelFrrConstHoldPrio);
                }

                /* Check for hop-limit value, By default hop limit is set to 32 */
                if ((SNMP_FAILURE !=
                     nmhGetFsMplsFrrConstHopLimit (u4TunnelId, u4TunnelInstance,
                                                   u4IngressId, u4EgressId,
                                                   &u4TunnelFrrConstHopLimit))
                    && (TE_TNL_FRR_HOP_LIMIT_DEF_VAL !=
                        u4TunnelFrrConstHopLimit))
                {
                    CliPrintf (CliHandle, " hop-limit %u",
                               u4TunnelFrrConstHopLimit);
                    FilePrintf (CliHandle, " hop-limit %u",
                                u4TunnelFrrConstHopLimit);
                }

                /* Check for Affinity values */
                if ((SNMP_FAILURE !=
                     nmhGetFsMplsFrrConstInclAnyAffinity (u4TunnelId,
                                                          u4TunnelInstance,
                                                          u4IngressId,
                                                          u4EgressId,
                                                          &u4TnlIncludeAnyAffinity))
                    && (SNMP_FAILURE !=
                        nmhGetFsMplsFrrConstExclAnyAffinity (u4TunnelId,
                                                             u4TunnelInstance,
                                                             u4IngressId,
                                                             u4EgressId,
                                                             &u4TnlExcludeAnyAffinity))
                    && (SNMP_FAILURE !=
                        nmhGetFsMplsFrrConstInclAllAffinity (u4TunnelId,
                                                             u4TunnelInstance,
                                                             u4IngressId,
                                                             u4EgressId,
                                                             &u4TnlIncludeAllAffinity))
                    && ((u4TnlIncludeAnyAffinity != TE_ZERO)
                        || (u4TnlExcludeAnyAffinity != TE_ZERO)
                        || (u4TnlIncludeAllAffinity != TE_ZERO)))
                {
                    CliPrintf (CliHandle, " affinity %u %u %u",
                               u4TnlIncludeAnyAffinity, u4TnlExcludeAnyAffinity,
                               u4TnlIncludeAllAffinity);
                    FilePrintf (CliHandle, " affinity %u %u %u",
                                u4TnlIncludeAnyAffinity,
                                u4TnlExcludeAnyAffinity,
                                u4TnlIncludeAllAffinity);

                    /* Reset the Affinity variables because variables are again using in below function */
                    u4TnlIncludeAnyAffinity = u4TnlExcludeAnyAffinity =
                        u4TnlIncludeAllAffinity = TE_ZERO;
                }
            }

        }

        if (SNMP_FAILURE !=
            nmhGetFsMplsTpTunnelDestTunnelIndex (u4TunnelId, u4TunnelInstance,
                                                 u4IngressId, u4EgressId,
                                                 &u4DestTnlIndex))
        {
            if (u4DestTnlIndex != 0)
            {
                CliPrintf (CliHandle,
                           "\r\n%stunnel mpls destination tnl-num %d ",
                           au1Space, u4DestTnlIndex);
                FilePrintf (CliHandle,
                            "\r\n%stunnel mpls destination tnl-num %d ",
                            au1Space, u4DestTnlIndex);

                if (SNMP_FAILURE !=
                    nmhGetFsMplsTpTunnelDestTunnelLspNum (u4TunnelId,
                                                          u4TunnelInstance,
                                                          u4IngressId,
                                                          u4EgressId,
                                                          &u4DestLspNum))
                {
                    if (u4DestLspNum != 0)
                    {
                        CliPrintf (CliHandle, "lsp-num %d", u4DestLspNum);
                        FilePrintf (CliHandle, "lsp-num %d", u4DestLspNum);
                    }
                }
            }
        }

        if ((i4SigProtocol == TE_SIGPROTO_LDP) ||
            (i4SigProtocol == TE_SIGPROTO_RSVP))
        {
            CliPrintf (CliHandle, "\r\n%stunnel signalling protocol", au1Space);
            FilePrintf (CliHandle, "\r\n%stunnel signalling protocol",
                        au1Space);

            if (i4SigProtocol == TE_SIGPROTO_LDP)
            {
                CliPrintf (CliHandle, " crldp");
                FilePrintf (CliHandle, " crldp");
            }
            else if (i4SigProtocol == TE_SIGPROTO_RSVP)
            {
                CliPrintf (CliHandle, " rsvp");
                FilePrintf (CliHandle, " rsvp");
            }

            if (u4TunnelInstance != TE_ZERO)
            {
                CliPrintf (CliHandle, " %d", u4TunnelInstance);
                FilePrintf (CliHandle, " %d", u4TunnelInstance);
            }
        }

        nmhGetGmplsTunnelLSPEncoding (u4TunnelId, u4TunnelInstance, u4IngressId,
                                      u4EgressId, &i4GmplsEnctype);

        if (i4GmplsEnctype != GMPLS_TUNNEL_LSP_NOT_GMPLS)
        {
            if (i4GmplsEnctype == GMPLS_TUNNEL_LSP_PACKET)
            {
                CliPrintf (CliHandle,
                           "\r\n%stunnel endpoint capability encoding lsp-packet",
                           au1Space);
                FilePrintf (CliHandle,
                            "\r\n%stunnel endpoint capability encoding lsp-packet",
                            au1Space);

            }
        }
        nmhGetGmplsTunnelSwitchingType (u4TunnelId, u4TunnelInstance,
                                        u4IngressId, u4EgressId,
                                        &i4GmplsSwtype);

        if (i4GmplsSwtype != GMPLS_UNKNOWN_SWITCHING)
        {
            if (i4GmplsSwtype == GMPLS_PSC1)
            {
                CliPrintf (CliHandle, " switching psc1");
                FilePrintf (CliHandle, " switching psc1");
            }
            else if (i4GmplsSwtype == GMPLS_PSC2)
            {
                CliPrintf (CliHandle, " switching psc2");
                FilePrintf (CliHandle, " switching psc2");
            }
        }
        /*Gpid */
        nmhGetGmplsTunnelGPid (u4TunnelId, u4TunnelInstance, u4IngressId,
                               u4EgressId, &i4GmplsGpid);

        if (i4GmplsGpid != GMPLS_GPID_UNKNOWN)
        {
            if (i4GmplsGpid == GMPLS_GPID_FIBERCHANNEL)
            {
                CliPrintf (CliHandle,
                           "\r\n%stunnel capability gpid fiberchannel",
                           au1Space);
                FilePrintf (CliHandle,
                            "\r\n%stunnel capability gpid fiberchannel",
                            au1Space);

            }
            else if (i4GmplsGpid == GMPLS_GPID_ETHERNET)
            {
                CliPrintf (CliHandle,
                           "\r\n%stunnel capability gpid ethernet", au1Space);
                FilePrintf (CliHandle,
                            "\r\n%stunnel capability gpid ethernet", au1Space);
            }
            else if (i4GmplsGpid == GMPLS_GPID_SDHSONET)
            {
                CliPrintf (CliHandle,
                           "\r\n%stunnel capability gpid sdhSonet", au1Space);
                FilePrintf (CliHandle,
                            "\r\n%stunnel capability gpid sdhSonet", au1Space);
            }
        }

        /* Fetch the tunnel's Affinity values  */
        nmhGetMplsTunnelIncludeAnyAffinity (u4TunnelId, u4TunnelInstance,
                                            u4IngressId, u4EgressId,
                                            &u4TnlIncludeAnyAffinity);
        nmhGetMplsTunnelExcludeAnyAffinity (u4TunnelId, u4TunnelInstance,
                                            u4IngressId, u4EgressId,
                                            &u4TnlExcludeAnyAffinity);
        nmhGetMplsTunnelIncludeAllAffinity (u4TunnelId, u4TunnelInstance,
                                            u4IngressId, u4EgressId,
                                            &u4TnlIncludeAllAffinity);
        if ((u4TnlIncludeAnyAffinity != TE_ZERO)
            || (u4TnlExcludeAnyAffinity != TE_ZERO)
            || (u4TnlIncludeAllAffinity != TE_ZERO))
        {
            CliPrintf (CliHandle,
                       "\r\n%stunnel mpls traffic-eng affinity "
                       "%u %u %u", au1Space, u4TnlIncludeAnyAffinity,
                       u4TnlExcludeAnyAffinity, u4TnlIncludeAllAffinity);
            FilePrintf (CliHandle,
                        "\r\n%stunnel mpls traffic-eng affinity "
                        "%u %u %u", au1Space, u4TnlIncludeAnyAffinity,
                        u4TnlExcludeAnyAffinity, u4TnlIncludeAllAffinity);
        }
        nmhGetMplsTunnelSetupPrio (u4TunnelId, u4TunnelInstance,
                                   u4IngressId, u4EgressId, &i4SetupPriority);

        nmhGetMplsTunnelHoldingPrio (u4TunnelId, u4TunnelInstance,
                                     u4IngressId, u4EgressId, &i4HoldPriority);

        if ((i4SetupPriority != TE_ZERO) || (i4HoldPriority != TE_ZERO))
        {
            CliPrintf (CliHandle,
                       "\r\n%stunnel mpls traffic-eng priority "
                       "%d %d", au1Space, i4SetupPriority, i4HoldPriority);
            FilePrintf (CliHandle,
                        "\r\n%stunnel mpls traffic-eng priority "
                        "%d %d", au1Space, i4SetupPriority, i4HoldPriority);
            if (u4TunnelInstance != TE_ZERO)
            {
                CliPrintf (CliHandle, " %d", u4TunnelInstance);
                FilePrintf (CliHandle, " %d", u4TunnelInstance);
            }

        }
        /*Print All SRLG associated with tunnel */

        pTeTnlInfo = TeGetTunnelInfo (u4TunnelId, u4TunnelInstance,
                                      u4IngressId, u4EgressId);

        if (pTeTnlInfo != NULL)
        {
            TMO_SLL_Scan (&pTeTnlInfo->SrlgList, pTnlSrlg, tTeTnlSrlg *)
            {
                CliPrintf (CliHandle,
                           "\r\n%stunnel mpls traffic-eng srlg "
                           "%u", au1Space, pTnlSrlg->u4SrlgNo);
                FilePrintf (CliHandle,
                            "\r\n%stunnel mpls traffic-eng srlg "
                            "%u", au1Space, pTnlSrlg->u4SrlgNo);
            }

            /* Fetch the tunnel's SRLG values  */
            if (pTeTnlInfo->u1TnlSrlgType == TNL_SRLG_INCLUDE_ANY)
            {
                CliPrintf (CliHandle,
                           "\r\n%stunnel mpls traffic-eng srlg type "
                           "%s", au1Space, "include any");
                FilePrintf (CliHandle,
                            "\r\n%stunnel mpls traffic-eng srlg type "
                            "%s", au1Space, "include any");

            }
            else if (pTeTnlInfo->u1TnlSrlgType == TNL_SRLG_INCLUDE_ALL)
            {
                CliPrintf (CliHandle,
                           "\r\n%stunnel mpls traffic-eng srlg type "
                           "%s", au1Space, "include all");
                FilePrintf (CliHandle,
                            "\r\n%stunnel mpls traffic-eng srlg type "
                            "%s", au1Space, "include all");

            }
            else if (pTeTnlInfo->u1TnlSrlgType == TNL_SRLG_EXCLUDE_ANY)

                CliPrintf (CliHandle,
                           "\r\n%stunnel mpls traffic-eng srlg type "
                           "%s", au1Space, "exclude");
            FilePrintf (CliHandle,
                        "\r\n%stunnel mpls traffic-eng srlg type "
                        "%s", au1Space, "exclude");

        }
        /* Fetch the resource pointer to get the bandwidth allotted */
        if (SNMP_FAILURE ==
            nmhGetMplsTunnelResourcePointer (u4TunnelId, u4TunnelInstance,
                                             u4IngressId, u4EgressId,
                                             &ResourcePointer))
        {
            continue;
        }
        u4ResourceIndex =
            ResourcePointer.pu4_OidList[MPLS_TE_RSRC_TABLE_MAX_RATE_OFFSET - 1];

        if (i4SigProtocol == TE_SIGPROTO_NONE)
        {
            if (u4TunnelInstance == TE_ZERO)
            {
                TeSRCDisplayStaticXCForNonZeroInst (CliHandle,
                                                    u4TunnelId,
                                                    u4TunnelInstance,
                                                    u4IngressId, u4EgressId,
                                                    au1Space);
            }
            else
            {
                TeSRCDisplayStaticXC (CliHandle, u4TunnelId, u4TunnelInstance,
                                      u4IngressId, u4EgressId, au1Space);
            }
        }
        else
        {
            /*Path -option */
            /*To retrive Hop Table Index */
            nmhGetMplsTunnelHopTableIndex (u4TunnelId, u4TunnelInstance,
                                           u4IngressId, u4EgressId,
                                           &u4HopListIndex);
            nmhGetMplsTunnelPathInUse (u4TunnelId, u4TunnelInstance,
                                       u4IngressId, u4EgressId,
                                       &u4HopPathOptionIndex);
            nmhGetFsMplsTunnelAttPointer (u4TunnelId, u4TunnelInstance,
                                          u4IngressId, u4EgressId,
                                          &GetAttrPointer);
            u4AttrIndex =
                GetAttrPointer.pu4_OidList[TE_TNL_ATTR_TABLE_DEF_OFFSET - 1];

            if (u4HopPathOptionIndex != TE_ZERO)
            {
                u4Flag = TE_ZERO;
                MplsTunnelShowEROConfig (CliHandle, u4HopPathOptionIndex,
                                         u4HopListIndex, u4AttrIndex,
                                         u4TunnelInstance, u4Flag);
            }
        }

        /*Backup Path-Option */
        /*To retrive Hop Table Index */

        nmhGetFsMplsTunnelBackupHopTableIndex (u4TunnelId, u4TunnelInstance,
                                               u4IngressId, u4EgressId,
                                               &u4HopListIndex);
        nmhGetFsMplsTunnelBackupPathInUse (u4TunnelId, u4TunnelInstance,
                                           u4IngressId, u4EgressId,
                                           &u4HopPathOptionIndex);
        nmhGetFsMplsTunnelAttPointer (u4TunnelId, u4TunnelInstance,
                                      u4IngressId, u4EgressId, &GetAttrPointer);

        u4AttrIndex =
            GetAttrPointer.pu4_OidList[TE_TNL_ATTR_TABLE_DEF_OFFSET - 1];

        if (u4HopPathOptionIndex != TE_ZERO)
        {

            u4Flag = TE_ONE;

            MplsTunnelShowEROConfig (CliHandle, u4HopPathOptionIndex,
                                     u4HopListIndex, u4AttrIndex,
                                     u4TunnelInstance, u4Flag);
        }

        /* get the tunnel max bandwidth rate */
        if (i4SigProtocol == TE_SIGPROTO_RSVP)
        {
            i1RetVal =
                nmhGetFsMplsTunnelRSVPResPeakDataRate (u4ResourceIndex,
                                                       &u4BandWidth);
        }
        else if (i4SigProtocol == TE_SIGPROTO_LDP)
        {
            i1RetVal =
                nmhGetFsMplsTunnelCRLDPResPeakDataRate (u4ResourceIndex,
                                                        &u4BandWidth);
        }
        else if (i4SigProtocol == TE_SIGPROTO_NONE)
        {
            i1RetVal =
                nmhGetMplsTunnelResourceMaxRate (u4ResourceIndex, &u4BandWidth);
        }

        if (i1RetVal != SNMP_FAILURE)
        {
            if (u4BandWidth != 0)
            {
                CliPrintf (CliHandle,
                           "\r\n%stunnel mpls traffic-eng bandwidth %u",
                           au1Space, u4BandWidth);
                FilePrintf (CliHandle,
                            "\r\n%stunnel mpls traffic-eng bandwidth %u",
                            au1Space, u4BandWidth);
            }
        }
        nmhGetGmplsTunnelAdminStatusFlags (u4TunnelId, u4TunnelInstance,
                                           u4IngressId, u4EgressId,
                                           &AdminStatusFlag);
        MPLS_OCTETSTRING_TO_INTEGER ((&AdminStatusFlag), u4AdminStatus);

        if (u4AdminStatus != TE_ZERO)
        {
            CliPrintf (CliHandle, "\r\ntunnel mpls admin-status");
            FilePrintf (CliHandle, "\r\ntunnel mpls admin-status");

            if (u4AdminStatus & GMPLS_ADMIN_ADMIN_DOWN)
            {
                CliPrintf (CliHandle, "%sadmin-down", au1Space);
                FilePrintf (CliHandle, "%sadmin-down", au1Space);
            }
            if (u4AdminStatus & GMPLS_ADMIN_REFLECT)
            {
                CliPrintf (CliHandle, "%sreflect", au1Space);
                FilePrintf (CliHandle, "%sreflect", au1Space);
            }
            if (u4AdminStatus & GMPLS_ADMIN_TESTING)
            {
                CliPrintf (CliHandle, "%stesting", au1Space);
                FilePrintf (CliHandle, "%stesting", au1Space);
            }
        }

        nmhGetFsMplsTunnelEndToEndProtection (u4TunnelId, u4TunnelInstance,
                                              u4IngressId, u4EgressId,
                                              &ProtectionType);

        u4ProtType = ProtectionType.pu1_OctetList[0];

        if (u4ProtType == MPLS_TE_FULL_REROUTE)
        {
            CliPrintf (CliHandle,
                       "\r\n%stunnel mpls end-to-end-protection-type "
                       "full-reroute", au1Space);
            FilePrintf (CliHandle,
                        "\r\n%stunnel mpls end-to-end-protection-type "
                        "full-reroute", au1Space);

        }
        else if (u4ProtType == MPLS_TE_DEDICATED_ONE2ONE)
        {
            CliPrintf (CliHandle,
                       "\r\n%stunnel mpls end-to-end-protection-type "
                       "one-to-one", au1Space);
            FilePrintf (CliHandle,
                        "\r\n%stunnel mpls end-to-end-protection-type "
                        "one-to-one", au1Space);
        }

        nmhGetFsMplsTunnelMBBStatus (u4TunnelId, u4TunnelInstance,
                                     u4IngressId, u4EgressId, &i4MbbStatus);

        if (i4MbbStatus == MPLS_TE_MBB_ENABLED)
        {
            CliPrintf (CliHandle,
                       "\r\n%stunnel mpls capable mbb enable", au1Space);
            FilePrintf (CliHandle,
                        "\r\n%stunnel mpls capable mbb enable", au1Space);
        }
        if ((pTeTnlInfo != NULL) && (pTeTnlInfo->pMplsDiffServTnlInfo != NULL))
        {
            nmhGetFsMplsDiffServServiceType (u4TunnelId, u4TunnelInstance,
                                             u4IngressId, u4EgressId,
                                             &i4MplsDiffservserviceType);
            if (pTeTnlInfo->pTeAttrListInfo != NULL)
            {
                u4TempListBitMask = pTeTnlInfo->pTeAttrListInfo->u4ListBitmask;
            }
            if ((u4TempListBitMask & TE_ATTR_CLASS_TYPE_BITMASK)
                && (pTeTnlInfo->pTeAttrListInfo != NULL))
            {
                i4ClassType = (INT4) pTeTnlInfo->pTeAttrListInfo->u4ClassType;
            }
            else
            {

                nmhGetFsMplsDiffServClassType (u4TunnelId, u4TunnelInstance,
                                               u4IngressId, u4EgressId,
                                               &i4ClassType);
            }
            if (i4ClassType >= 0)
            {
                CliPrintf (CliHandle,
                           "\r\n%stunnel mpls traffic-eng class-type %d",
                           au1Space, i4ClassType);
                FilePrintf (CliHandle,
                            "\r\n%stunnel mpls traffic-eng class-type %d",
                            au1Space, i4ClassType);

            }

            if (i4MplsDiffservserviceType == MPLS_DIFFSERV_ELSP)
            {
                CliPrintf (CliHandle, "\r\n%slsp-type e-lsp", au1Space);
                FilePrintf (CliHandle, "\r\n%slsp-type e-lsp", au1Space);

                if (i4ElspType == TE_DS_SIG_ELSP)
                {
                    CliPrintf (CliHandle, "\r\n%s map use signalled", au1Space);
                    FilePrintf (CliHandle, "\r\n%s map use signalled",
                                au1Space);
                }
                nmhGetFsMplsDiffServElspListIndex (u4TunnelId, u4TunnelInstance,
                                                   u4IngressId, u4EgressId,
                                                   &i4ElspListIndex);

                CliPrintf (CliHandle,
                           "\r\n%s use exp-map index %d",
                           au1Space, i4ElspListIndex);
                FilePrintf (CliHandle,
                            "\r\n%s use exp-map index %d",
                            au1Space, i4ElspListIndex);

            }
            else if (i4MplsDiffservserviceType == MPLS_DIFFSERV_LLSP)
            {
                CliPrintf (CliHandle, "\r\n%slsp-type l-lsp", au1Space);
                FilePrintf (CliHandle, "\r\n%slsp-type l-lsp", au1Space);

                nmhGetFsMplsDiffServLlspPsc (u4TunnelId, u4TunnelInstance,
                                             u4IngressId, u4EgressId,
                                             &i4DiffServLlspPsc);

                switch (i4DiffServLlspPsc)
                {
                    case TE_DS_DF_DSCP:
                        CliPrintf (CliHandle, "\r\n%s psc df", au1Space);
                        FilePrintf (CliHandle, "\r\n%s psc df", au1Space);
                        break;
                    case TE_DS_CS1_DSCP:
                        CliPrintf (CliHandle, "\r\n%s psc cs1", au1Space);
                        FilePrintf (CliHandle, "\r\n%s psc cs1", au1Space);
                        break;
                    case TE_DS_CS2_DSCP:
                        CliPrintf (CliHandle, "\r\n%s psc cs2", au1Space);
                        FilePrintf (CliHandle, "\r\n%s psc cs2", au1Space);
                        break;
                    case TE_DS_CS3_DSCP:
                        CliPrintf (CliHandle, "\r\n%s psc cs3", au1Space);
                        FilePrintf (CliHandle, "\r\n%s psc cs3", au1Space);
                        break;
                    case TE_DS_CS4_DSCP:
                        CliPrintf (CliHandle, "\r\n%s psc cs4", au1Space);
                        FilePrintf (CliHandle, "\r\n%s psc cs4", au1Space);
                        break;
                    case TE_DS_CS5_DSCP:
                        CliPrintf (CliHandle, "\r\n%s psc cs5", au1Space);
                        FilePrintf (CliHandle, "\r\n%s psc cs5", au1Space);
                        break;
                    case TE_DS_CS6_DSCP:
                        CliPrintf (CliHandle, "\r\n%s psc cs6", au1Space);
                        FilePrintf (CliHandle, "\r\n%s psc cs6", au1Space);
                        break;
                    case TE_DS_CS7_DSCP:
                        CliPrintf (CliHandle, "\r\n%s psc cs7", au1Space);
                        FilePrintf (CliHandle, "\r\n%s psc cs7", au1Space);
                        break;
                    case TE_DS_EF_DSCP:
                        CliPrintf (CliHandle, "\r\n%s psc ef", au1Space);
                        FilePrintf (CliHandle, "\r\n%s psc ef", au1Space);
                        break;
                    case TE_DS_AF11_DSCP:
                        CliPrintf (CliHandle, "\r\n%s psc af1", au1Space);
                        FilePrintf (CliHandle, "\r\n%s psc af1", au1Space);
                        break;
                    case TE_DS_AF21_DSCP:
                        CliPrintf (CliHandle, "\r\n%s psc af2", au1Space);
                        FilePrintf (CliHandle, "\r\n%s psc af2", au1Space);
                        break;
                    case TE_DS_AF31_DSCP:
                        CliPrintf (CliHandle, "\r\n%s psc af3", au1Space);
                        FilePrintf (CliHandle, "\r\n%s psc af3", au1Space);
                        break;
                    case TE_DS_AF41_DSCP:
                        CliPrintf (CliHandle, "\r\n%s psc af4", au1Space);
                        FilePrintf (CliHandle, "\r\n%s psc af4", au1Space);
                        break;
                    default:
                        break;
                }
            }
        }
        nmhGetMplsTunnelAdminStatus (u4TunnelId, u4TunnelInstance,
                                     u4IngressId, u4EgressId,
                                     &i4TunnelAdminStatus);

        if (TE_ADMIN_UP == i4TunnelAdminStatus)
        {
            CliPrintf (CliHandle, "\r\n%sno shutdown", au1Space);
            FilePrintf (CliHandle, "\r\n%sno shutdown", au1Space);
        }
        if (u4TunnelInstance != TE_ZERO)
        {
            CliPrintf (CliHandle, "\n!");
            FilePrintf (CliHandle, "\n!");
        }
    }
    CliPrintf (CliHandle, "\r\n");
    FilePrintf (CliHandle, "\r\n");

    return CLI_SUCCESS;
}

/*******************************************************************************
 *  Function Name : MplsLspReoptimizationRunningConfig
 *  Description   : This function shows the Mpls LSP Reoptimization tunnels
 *  Input(s)      : tCliHandle CliHandle - CLI Handle.
 *  Output(s)     : None.
 *  Return Values : CLI_SUCCESS/CLI_FAILURE
 ********************************************************************************/
INT4
MplsLspReoptimizationRunningConfig (tCliHandle CliHandle)
{
    tTeReoptTnlInfo    *pTeReoptTnlInfo = NULL;

    pTeReoptTnlInfo =
        (tTeReoptTnlInfo *) RBTreeGetFirst (gTeGblInfo.ReoptimizeTnlList);

    if (pTeReoptTnlInfo == NULL)
    {
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\n!");
    FilePrintf (CliHandle, "\n!");
    while (pTeReoptTnlInfo != NULL)
    {
        CliPrintf (CliHandle,
                   "\r\nmpls traffic-eng reoptimize tunnel %u destination %d.%d.%d.%d source %d.%d.%d.%d",
                   pTeReoptTnlInfo->u4ReoptTnlIndex,
                   pTeReoptTnlInfo->ReoptTnlEgressLsrId[0],
                   pTeReoptTnlInfo->ReoptTnlEgressLsrId[1],
                   pTeReoptTnlInfo->ReoptTnlEgressLsrId[2],
                   pTeReoptTnlInfo->ReoptTnlEgressLsrId[3],
                   pTeReoptTnlInfo->ReoptTnlIngressLsrId[0],
                   pTeReoptTnlInfo->ReoptTnlIngressLsrId[1],
                   pTeReoptTnlInfo->ReoptTnlIngressLsrId[2],
                   pTeReoptTnlInfo->ReoptTnlIngressLsrId[3]);

        FilePrintf (CliHandle,
                    "\r\nmpls traffic-eng reoptimize tunnel %u destination %d.%d.%d.%d source %d.%d.%d.%d",
                    pTeReoptTnlInfo->u4ReoptTnlIndex,
                    pTeReoptTnlInfo->ReoptTnlEgressLsrId[0],
                    pTeReoptTnlInfo->ReoptTnlEgressLsrId[1],
                    pTeReoptTnlInfo->ReoptTnlEgressLsrId[2],
                    pTeReoptTnlInfo->ReoptTnlEgressLsrId[3],
                    pTeReoptTnlInfo->ReoptTnlIngressLsrId[0],
                    pTeReoptTnlInfo->ReoptTnlIngressLsrId[1],
                    pTeReoptTnlInfo->ReoptTnlIngressLsrId[2],
                    pTeReoptTnlInfo->ReoptTnlIngressLsrId[3]);

        pTeReoptTnlInfo =
            (tTeReoptTnlInfo *) RBTreeGetNext (gTeGblInfo.ReoptimizeTnlList,
                                               (tRBElem *) pTeReoptTnlInfo,
                                               NULL);

    }

    CliPrintf (CliHandle, "\n!");
    FilePrintf (CliHandle, "\n!");
    CliPrintf (CliHandle, "\n");
    FilePrintf (CliHandle, "\n");

    return CLI_SUCCESS;
}

#ifdef MPLS_SIG_WANTED
/******************************************************************************
* Function Name : TeCliFrrConstInfo 
* Description   : This routine is used to configure the constraint information.
* Input(s)      : CliHandle   - Cli Context Handle
*                 pFrrCliArgs - Pointer to the constraint table.
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliFrrConstInfo (tCliHandle CliHandle, tFrrCliArgs * pFrrCliArgs)
{
    UINT4               u4TunnelIndex = TE_ZERO;
    UINT4               u4TunnelInstance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    UINT4               u4ErrorCode = TE_ZERO;
    UINT4               u4Flag = TE_ZERO;
    INT4                i4RetValFsMplsFrrConstRowStatus = TE_ZERO;
    INT4                i4BackupId = TE_ZERO;
    INT4                i4PrevRowStatus = TE_ZERO;
    static UINT1        au1Length[CLI_MPLS_TE_SESS_ATTR] = { 0 };
    tSNMP_OCTET_STRING_TYPE MplsTeSessionAttribute;

    u4Flag = pFrrCliArgs->u4Flag;

    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    MplsTeSessionAttribute.pu1_OctetList = au1Length;
    MplsTeSessionAttribute.i4_Length = CLI_MPLS_TE_SESS_ATTR;

    if (nmhGetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                   u4IngressId, u4EgressId,
                                   &i4PrevRowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Cannot create Tunnel FRR Constraint information\r\n");
        return CLI_FAILURE;
    }

    if (TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                     u4IngressId, u4EgressId, TE_NOTINSERVICE)
        == CLI_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Cannot create Tunnel FRR Constraint information\r\n");
        return CLI_FAILURE;
    }

    if (nmhGetMplsTunnelSessionAttributes (u4TunnelIndex, u4TunnelInstance,
                                           u4IngressId, u4EgressId,
                                           &MplsTeSessionAttribute)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Cannot create Tunnel FRR Constraint information\r\n");
        return CLI_FAILURE;
    }

    MplsTeSessionAttribute.pu1_OctetList[TE_ZERO] |= TE_SSN_FAST_REROUTE_BIT;

    /*Check for the Session Attribute Objects */
    if ((nmhTestv2MplsTunnelSessionAttributes (&u4ErrorCode,
                                               u4TunnelIndex, u4TunnelInstance,
                                               u4IngressId, u4EgressId,
                                               &MplsTeSessionAttribute))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to Set Fast-Reoute \r\n");
        return CLI_FAILURE;
    }
    /*To Set the Fast-Reroute */
    if ((nmhSetMplsTunnelSessionAttributes (u4TunnelIndex, u4TunnelInstance,
                                            u4IngressId, u4EgressId,
                                            &MplsTeSessionAttribute))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to Set Fast-Reroute\r\n");
        return CLI_FAILURE;
    }

    nmhGetFsMplsFrrConstRowStatus (u4TunnelIndex,
                                   u4TunnelInstance,
                                   u4IngressId, u4EgressId,
                                   &i4RetValFsMplsFrrConstRowStatus);
    /*Retriving RowStatus of Constraint table */
    if (i4RetValFsMplsFrrConstRowStatus == TE_ZERO)
    {
        if (nmhTestv2FsMplsFrrConstRowStatus (&u4ErrorCode,
                                              u4TunnelIndex,
                                              u4TunnelInstance,
                                              u4IngressId, u4EgressId,
                                              TE_CREATEANDWAIT) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to configure constraint information\r\n");
            return CLI_FAILURE;
        }
        /*To set RowStatus as Create and Wait */
        if (nmhSetFsMplsFrrConstRowStatus (u4TunnelIndex,
                                           u4TunnelInstance,
                                           u4IngressId, u4EgressId,
                                           TE_CREATEANDWAIT) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to configure constraint information\r\n");
            return CLI_FAILURE;
        }

    }
    else if (i4RetValFsMplsFrrConstRowStatus == TE_ACTIVE)
    {
        /*Checking the RowStatus */
        if (nmhTestv2FsMplsFrrConstRowStatus (&u4ErrorCode,
                                              u4TunnelIndex,
                                              u4TunnelInstance,
                                              u4IngressId, u4EgressId,
                                              TE_NOTINSERVICE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to configure constraint information\r\n");
            return CLI_FAILURE;
        }
        /*To set RowStatus as Not-In-Service */
        if (nmhSetFsMplsFrrConstRowStatus (u4TunnelIndex,
                                           u4TunnelInstance,
                                           u4IngressId, u4EgressId,
                                           TE_NOTINSERVICE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to configure constraint information\r\n");
            return CLI_FAILURE;
        }
    }

    /*Checking for the type of Backup Identification Method */
    if ((u4Flag & CLI_MPLS_FRR_BACKUP_SENDER_TEMPLATE) ==
        CLI_MPLS_FRR_BACKUP_SENDER_TEMPLATE)
    {
        i4BackupId = TE_SNMP_TRUE;
    }
    else if ((u4Flag & CLI_MPLS_FRR_BACKUP_PATH_SPECIFIC) ==
             CLI_MPLS_FRR_BACKUP_PATH_SPECIFIC)
    {
        i4BackupId = TE_SNMP_FALSE;
    }
    /*Check for the SE sytle value */
    if ((nmhTestv2FsMplsFrrConstSEStyle (&u4ErrorCode,
                                         u4TunnelIndex, u4TunnelInstance,
                                         u4IngressId, u4EgressId,
                                         i4BackupId)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Wrong Backup Identification Method\r\n");
        return CLI_FAILURE;
    }
    /*To Set the Backup identification Method */
    if ((nmhSetFsMplsFrrConstSEStyle (u4TunnelIndex, u4TunnelInstance,
                                      u4IngressId, u4EgressId,
                                      i4BackupId)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to configure Backup"
                   "Identification Method\r\n");
        return CLI_FAILURE;
    }

    /*Checking for one-to-one flag */
    if ((u4Flag & CLI_MPLS_FRR_ONE_TO_ONE) == CLI_MPLS_FRR_ONE_TO_ONE)
    {
        /*Check for the type of Protection */
        if ((nmhTestv2FsMplsFrrConstProtectionMethod (&u4ErrorCode,
                                                      u4TunnelIndex,
                                                      u4TunnelInstance,
                                                      u4IngressId, u4EgressId,
                                                      TE_TNL_FRR_ONE2ONE_METHOD))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Wrong Protection Method\r\n");
            return CLI_FAILURE;
        }
        /*To Set Protection Method */
        if ((nmhSetFsMplsFrrConstProtectionMethod (u4TunnelIndex,
                                                   u4TunnelInstance,
                                                   u4IngressId, u4EgressId,
                                                   TE_TNL_FRR_ONE2ONE_METHOD))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to configure Protection Method\r\n");
            return CLI_FAILURE;
        }
    }

    /*Checking for many-to-one flag */
    if ((u4Flag & CLI_MPLS_FRR_MANY_TO_ONE) == CLI_MPLS_FRR_MANY_TO_ONE)
    {
        /*Check for the type of Protection */
        if ((nmhTestv2FsMplsFrrConstProtectionMethod (&u4ErrorCode,
                                                      u4TunnelIndex,
                                                      u4TunnelInstance,
                                                      u4IngressId, u4EgressId,
                                                      TE_TNL_FRR_FACILITY_METHOD))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Wrong Protection Method\r\n");
            return CLI_FAILURE;
        }
        /*To Set Protection Method */
        if ((nmhSetFsMplsFrrConstProtectionMethod (u4TunnelIndex,
                                                   u4TunnelInstance,
                                                   u4IngressId, u4EgressId,
                                                   TE_TNL_FRR_FACILITY_METHOD))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to configure Protection Method\r\n");
            return CLI_FAILURE;
        }
    }
    /*Checking for link protection flag */
    if ((u4Flag & CLI_MPLS_FRR_LINK_PROT) == CLI_MPLS_FRR_LINK_PROT)
    {
        /*Check for Link or Node protection */
        if ((nmhTestv2FsMplsFrrConstProtectionType (&u4ErrorCode,
                                                    u4TunnelIndex,
                                                    u4TunnelInstance,
                                                    u4IngressId, u4EgressId,
                                                    TE_TNL_FRR_PROT_LINK))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Wrong Protection Type\r\n");
            return CLI_FAILURE;
        }
        /*To set the Protection Type */
        if ((nmhSetFsMplsFrrConstProtectionType (u4TunnelIndex,
                                                 u4TunnelInstance,
                                                 u4IngressId, u4EgressId,
                                                 TE_TNL_FRR_PROT_LINK))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to configure Protection Type\r\n");
            return CLI_FAILURE;
        }
    }

    /*Checking for node protection flag */
    if ((u4Flag & CLI_MPLS_FRR_NODE_PROT) == CLI_MPLS_FRR_NODE_PROT)
    {
        /*Check for Link or Node protection */
        if ((nmhTestv2FsMplsFrrConstProtectionType (&u4ErrorCode,
                                                    u4TunnelIndex,
                                                    u4TunnelInstance,
                                                    u4IngressId, u4EgressId,
                                                    TE_TNL_FRR_PROT_NODE))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Wrong Protection Type\r\n");
            return CLI_FAILURE;
        }
        /*To set the Protection Type */
        if ((nmhSetFsMplsFrrConstProtectionType (u4TunnelIndex,
                                                 u4TunnelInstance,
                                                 u4IngressId, u4EgressId,
                                                 TE_TNL_FRR_PROT_NODE))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to configure Protection Type\r\n");
            return CLI_FAILURE;
        }
    }
    /*Checking for the baandwidth protection flag */
    if ((u4Flag & CLI_MPLS_FRR_BW_PROT) == CLI_MPLS_FRR_BW_PROT)
    {
        /*Check for the Backup Bandwidth */
        if ((nmhTestv2FsMplsFrrConstBandwidth (&u4ErrorCode,
                                               u4TunnelIndex,
                                               u4TunnelInstance,
                                               u4IngressId, u4EgressId,
                                               pFrrCliArgs->u4Bandwidth))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Wrong bandwidth value\r\n");
            return CLI_FAILURE;
        }
        /*To set the bandwidth value */
        if ((nmhSetFsMplsFrrConstBandwidth (u4TunnelIndex,
                                            u4TunnelInstance,
                                            u4IngressId, u4EgressId,
                                            pFrrCliArgs->u4Bandwidth))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to configure Bandwidth Value\r\n");
            return CLI_FAILURE;
        }
    }
    /*Checking for the priority flag */
    if ((u4Flag & CLI_MPLS_FRR_PRIOR) == CLI_MPLS_FRR_PRIOR)
    {
        /*Check whether Backup Setup Priority is within the specified range */
        if ((nmhTestv2FsMplsFrrConstSetupPrio (&u4ErrorCode,
                                               u4TunnelIndex,
                                               u4TunnelInstance,
                                               u4IngressId, u4EgressId,
                                               pFrrCliArgs->u4BckSetupPrio))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Invalid Setup Priority\r\n");
            return CLI_FAILURE;
        }
        /*To set the Setup Priority */
        if ((nmhSetFsMplsFrrConstSetupPrio (u4TunnelIndex,
                                            u4TunnelInstance,
                                            u4IngressId, u4EgressId,
                                            pFrrCliArgs->u4BckSetupPrio))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set backup Setup Priority\r\n");
            return CLI_FAILURE;
        }
        /*Check whether Backup Holding Priority is within the specified range */
        if ((nmhTestv2FsMplsFrrConstHoldingPrio (&u4ErrorCode,
                                                 u4TunnelIndex,
                                                 u4TunnelInstance,
                                                 u4IngressId, u4EgressId,
                                                 pFrrCliArgs->
                                                 u4BckHoldingPrior)) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Invalid Holding Priority\r\n");
            return CLI_FAILURE;
        }
        /*To set the holding priority */
        if ((nmhSetFsMplsFrrConstHoldingPrio (u4TunnelIndex,
                                              u4TunnelInstance,
                                              u4IngressId, u4EgressId,
                                              pFrrCliArgs->u4BckHoldingPrior))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set backup Holding Priority\r\n");
            return CLI_FAILURE;
        }
    }
    /*Checking for the hop limit flag */
    if ((u4Flag & CLI_MPLS_FRR_HOP_LIMIT) == CLI_MPLS_FRR_HOP_LIMIT)
    {
        /*Check whether Hop Limit is within the specified range */
        if ((nmhTestv2FsMplsFrrConstHopLimit (&u4ErrorCode,
                                              u4TunnelIndex,
                                              u4TunnelInstance,
                                              u4IngressId, u4EgressId,
                                              pFrrCliArgs->u4HopLimit))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Invalid Hop Limit\r\n");
            return CLI_FAILURE;
        }
        /*To set the hop limit */
        if ((nmhSetFsMplsFrrConstHopLimit (u4TunnelIndex,
                                           u4TunnelInstance,
                                           u4IngressId, u4EgressId,
                                           pFrrCliArgs->u4HopLimit))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set Hop Limit of backup tunnel\r\n");
            return CLI_FAILURE;
        }
    }
    /*Checking for the affinity flag */
    if ((u4Flag & CLI_MPLS_FRR_AFFINITY) == CLI_MPLS_FRR_AFFINITY)
    {
        /*Check for the Include-any affinity */
        if ((nmhTestv2FsMplsFrrConstInclAnyAffinity (&u4ErrorCode,
                                                     u4TunnelIndex,
                                                     u4TunnelInstance,
                                                     u4IngressId, u4EgressId,
                                                     pFrrCliArgs->u4IncAny))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Invalid Include-Any Affinity Value\r\n");
            return CLI_FAILURE;
        }
        /*To set include-any affinity */
        if ((nmhSetFsMplsFrrConstInclAnyAffinity (u4TunnelIndex,
                                                  u4TunnelInstance,
                                                  u4IngressId, u4EgressId,
                                                  pFrrCliArgs->u4IncAny))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set backup Include-Any Affinity\r\n");
            return CLI_FAILURE;
        }
        /*Check for the exclude-any affinity */
        if ((nmhTestv2FsMplsFrrConstExclAnyAffinity (&u4ErrorCode,
                                                     u4TunnelIndex,
                                                     u4TunnelInstance,
                                                     u4IngressId, u4EgressId,
                                                     pFrrCliArgs->u4ExcAny))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Invalid Exclude-Any Affinity Value\r\n");
            return CLI_FAILURE;
        }
        /*To set the exclude-any affinity */
        if ((nmhSetFsMplsFrrConstExclAnyAffinity (u4TunnelIndex,
                                                  u4TunnelInstance,
                                                  u4IngressId, u4EgressId,
                                                  pFrrCliArgs->u4ExcAny))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set backup Exclude-Any Affinity\r\n");
            return CLI_FAILURE;
        }
        /*Check for the Include-all affinity */
        if ((nmhTestv2FsMplsFrrConstInclAllAffinity (&u4ErrorCode,
                                                     u4TunnelIndex,
                                                     u4TunnelInstance,
                                                     u4IngressId, u4EgressId,
                                                     pFrrCliArgs->u4IncAll))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Invalid Include-All Affinity Value\r\n");
            return CLI_FAILURE;
        }
        /*To set the include-all affinity */
        if ((nmhSetFsMplsFrrConstInclAllAffinity (u4TunnelIndex,
                                                  u4TunnelInstance,
                                                  u4IngressId, u4EgressId,
                                                  pFrrCliArgs->u4IncAll))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set backup Include-All Affinity\r\n");
            return CLI_FAILURE;
        }
    }
    /*Check for the SE sytle value */
    if ((nmhTestv2FsMplsFrrConstSEStyle (&u4ErrorCode,
                                         u4TunnelIndex, u4TunnelInstance,
                                         u4IngressId, u4EgressId,
                                         i4BackupId)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Wrong Backup Identification" "Method\r\n");
        return CLI_FAILURE;
    }
    /*To Set the Backup identification Method */
    if ((nmhSetFsMplsFrrConstSEStyle (u4TunnelIndex, u4TunnelInstance,
                                      u4IngressId, u4EgressId,
                                      i4BackupId)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to configure Backup"
                   "Identification Method\r\n");
        return CLI_FAILURE;
    }

    /*Check for the Rowstatus */
    if (nmhTestv2FsMplsFrrConstRowStatus (&u4ErrorCode,
                                          u4TunnelIndex,
                                          u4TunnelInstance,
                                          u4IngressId, u4EgressId,
                                          TE_ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to make constraint table active\r\n");
        return CLI_FAILURE;
    }
    /*To set the RowStatus as active */
    if (nmhSetFsMplsFrrConstRowStatus (u4TunnelIndex,
                                       u4TunnelInstance,
                                       u4IngressId, u4EgressId,
                                       TE_ACTIVE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to make constraint table active\r\n");
        return CLI_FAILURE;
    }

    if (i4PrevRowStatus != TE_NOTREADY)
    {
        if (TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                         u4IngressId, u4EgressId,
                                         i4PrevRowStatus) == CLI_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Cannot create Tunnel FRR Constraint "
                       "information\r\n");
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliFrrNoConstInfo 
* Description   : This routine is remove constraint information
* Input(s)      : CliHandle - Cli Context Handle
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliFrrNoConstInfo (tCliHandle CliHandle)
{
    UINT4               u4TunnelIndex = TE_ZERO;
    UINT4               u4TunnelInstance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    UINT4               u4ErrorCode = TE_ZERO;
    INT4                i4PrevRowStatus = TE_ZERO;
    static UINT1        au1Length[CLI_MPLS_TE_SESS_ATTR] = { 0 };
    tSNMP_OCTET_STRING_TYPE MplsTeSessionAttribute;

    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                   u4IngressId, u4EgressId,
                                   &i4PrevRowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Cannot destroy Tunnel FRR Constraint information\r\n");
        return CLI_FAILURE;
    }

    if (TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                     u4IngressId, u4EgressId, TE_NOTINSERVICE)
        == CLI_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Cannot destroy Tunnel FRR Constraint information\r\n");
        return CLI_FAILURE;
    }

    /*Check for the RowStatus */
    if (nmhTestv2FsMplsFrrConstRowStatus (&u4ErrorCode,
                                          u4TunnelIndex,
                                          u4TunnelInstance,
                                          u4IngressId, u4EgressId,
                                          TE_DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to destroy constraint information\r\n");
        return CLI_FAILURE;
    }
    /*To set RowStatus as destroy */
    if (nmhSetFsMplsFrrConstRowStatus (u4TunnelIndex,
                                       u4TunnelInstance,
                                       u4IngressId, u4EgressId,
                                       TE_DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to destroy constraint information\r\n");
        return CLI_FAILURE;
    }

    MplsTeSessionAttribute.pu1_OctetList = au1Length;
    MplsTeSessionAttribute.i4_Length = CLI_MPLS_TE_SESS_ATTR;

    if (nmhGetMplsTunnelSessionAttributes (u4TunnelIndex, u4TunnelInstance,
                                           u4IngressId, u4EgressId,
                                           &MplsTeSessionAttribute)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to destroy constraint information\r\n");
        return CLI_FAILURE;
    }

    MplsTeSessionAttribute.pu1_OctetList[TE_ZERO] &=
        (UINT1) (~(TE_SSN_FAST_REROUTE_BIT));

    if (nmhTestv2MplsTunnelSessionAttributes (&u4ErrorCode,
                                              u4TunnelIndex, u4TunnelInstance,
                                              u4IngressId, u4EgressId,
                                              &MplsTeSessionAttribute)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to destroy constraint information\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetMplsTunnelSessionAttributes (u4TunnelIndex, u4TunnelInstance,
                                           u4IngressId, u4EgressId,
                                           &MplsTeSessionAttribute)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to destroy constraint information\r\n");
        return CLI_FAILURE;
    }

    if (i4PrevRowStatus != TE_NOTREADY)
    {
        if (TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                         u4IngressId, u4EgressId,
                                         i4PrevRowStatus) == CLI_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Cannot destroy Tunnel FRR Constraint "
                       "information\r\n");
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}
#endif

/******************************************************************************
* Function Name : TeCliTunnelPriorities 
* Description   : This routine is used to configure setup and holding 
*                 priorities of the protected tunnel.
* Input(s)      : CliHandle       - Cli Context Handle
*                 i4SetupPriority - Setup priority of protected tunnel.
*                 i4HoldPriority  - Holding priority of protected tunnel.
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliTunnelPriorities (tCliHandle CliHandle, INT4 i4SetupPriority,
                       INT4 i4HoldPriority)
{
    UINT4               u4TunnelIndex = TE_ZERO;
    UINT4               u4TunnelInstance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    UINT4               u4ErrorCode = TE_ZERO;
    INT4                i4PrevRowStatus = TE_ZERO;

    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (i4HoldPriority > i4SetupPriority)
    {
        CliPrintf (CliHandle,
                   "\r%% The Setup Priority should be less than Holding Priority \r\n");
        return CLI_FAILURE;
    }
    if (nmhGetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                   u4IngressId, u4EgressId,
                                   &i4PrevRowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Cannot get the tunnel row status value\r\n");
        return CLI_FAILURE;
    }
    if (TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                     u4IngressId, u4EgressId, TE_NOTINSERVICE)
        == CLI_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Cannot make tunnel row status as NOT IN SERVICE\r\n");
        return CLI_FAILURE;
    }

    /*Check whether Setup Priority is within the specified range */
    if ((nmhTestv2MplsTunnelSetupPrio (&u4ErrorCode,
                                       u4TunnelIndex, u4TunnelInstance,
                                       u4IngressId, u4EgressId,
                                       i4SetupPriority)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Setup Priority \r\n");
        return CLI_FAILURE;
    }
    /*To Set the Setup Priority of Tunnel */
    if ((nmhSetMplsTunnelSetupPrio (u4TunnelIndex, u4TunnelInstance,
                                    u4IngressId, u4EgressId,
                                    i4SetupPriority)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to configure Setup Priority\r\n");
        return CLI_FAILURE;
    }
    /*Check whether Holding Priority is within the specified range */
    if ((nmhTestv2MplsTunnelHoldingPrio (&u4ErrorCode,
                                         u4TunnelIndex, u4TunnelInstance,
                                         u4IngressId, u4EgressId,
                                         i4HoldPriority)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Invalid Holding Priority \r\n");
        return CLI_FAILURE;
    }
    /*To Set the Holding Priority of Tunnel */
    if ((nmhSetMplsTunnelHoldingPrio (u4TunnelIndex, u4TunnelInstance,
                                      u4IngressId, u4EgressId,
                                      i4HoldPriority)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to configure Holding Priority \r\n");
        return CLI_FAILURE;
    }

    /*In case of MBB set Old Bandwidth */
    TeCliSetMplsTunnelOldBandwidth (u4TunnelIndex, u4TunnelInstance,
                                    u4IngressId, u4EgressId);

    if (i4PrevRowStatus != TE_NOTREADY)
    {
        if (TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                         u4IngressId, u4EgressId,
                                         i4PrevRowStatus) == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Cannot Set Mpls tunnel RowStatus\r\n");
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliTunnelNoPriorities 
* Description   : This routine is delete the setup and holding priorities
*                 of the protected tunnel.  
* Input(s)      : CliHandle - Cli Context Handle
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliTunnelNoPriorities (tCliHandle CliHandle)
{
    UINT4               u4TunnelIndex = TE_ZERO;
    UINT4               u4TunnelInstance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    UINT4               u4ErrorCode = TE_ZERO;
    INT4                i4PrevRowStatus = TE_ZERO;
    UINT4               u4SnmpFailure = TE_ZERO;

    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                   u4IngressId, u4EgressId,
                                   &i4PrevRowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to fetch tunnel row-status\r\n");
        return CLI_FAILURE;
    }

    if (TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                     u4IngressId, u4EgressId, TE_NOTINSERVICE)
        == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set tunnel row-status\r\n");
        return CLI_FAILURE;
    }

    /*Checking whether Setup Priority is within the specified range */
    if ((nmhTestv2MplsTunnelSetupPrio (&u4ErrorCode,
                                       u4TunnelIndex, u4TunnelInstance,
                                       u4IngressId, u4EgressId,
                                       TE_SETUP_PRIO_DEF_VAL)) == SNMP_FAILURE)
    {
        u4SnmpFailure = TE_ONE;
    }

    /*Checking whether Holding Priority is within the specified range */
    if ((nmhTestv2MplsTunnelHoldingPrio (&u4ErrorCode,
                                         u4TunnelIndex, u4TunnelInstance,
                                         u4IngressId, u4EgressId,
                                         TE_HOLD_PRIO_DEF_VAL)) == SNMP_FAILURE)
    {
        u4SnmpFailure = TE_ONE;
    }

    if (u4SnmpFailure == TE_ONE)
    {
        if (TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                         u4IngressId, u4EgressId,
                                         i4PrevRowStatus) == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set tunnel row-status\r\n");
        }
        return CLI_FAILURE;
    }

    /*To Set the default Setup Priority of the Tunnel */
    if ((nmhSetMplsTunnelSetupPrio (u4TunnelIndex, u4TunnelInstance,
                                    u4IngressId, u4EgressId,
                                    TE_SETUP_PRIO_DEF_VAL)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to destroy Setup Priority\r\n");
        u4SnmpFailure = TE_ONE;
    }

    /*To Set the default Holding Priority of the Tunnel */
    if ((nmhSetMplsTunnelHoldingPrio (u4TunnelIndex, u4TunnelInstance,
                                      u4IngressId, u4EgressId,
                                      TE_HOLD_PRIO_DEF_VAL)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to destroy Holding Priority\r\n");
        u4SnmpFailure = TE_ONE;
    }

    if (u4SnmpFailure == TE_ONE)
    {
        if (TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                         u4IngressId, u4EgressId,
                                         i4PrevRowStatus) == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set tunnel row-status\r\n");
        }
        return CLI_FAILURE;
    }

    if (i4PrevRowStatus != TE_NOTREADY)
    {
        if (TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                         u4IngressId, u4EgressId,
                                         i4PrevRowStatus) == CLI_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set previous tunnel row-status "
                       "\r\n");
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

 /******************************************************************************
* Function Name : TeCliTunnelAffinities
* Description   : This routine is used to configure the affinities of the   
*                 of the protected tunnel.
* Input(s)      : CliHandle - Cli Context Handle.
*                 u4IncAny  - Include-any affinity of protected tunnel.
*                 u4ExcAny  - Exclude-any affinity of protected tunnel.
*                 u4IncAll  - Include-all affinity of protected tunnel.
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliTunnelAffinities (tCliHandle CliHandle, UINT4 u4IncAny, UINT4 u4ExcAny,
                       UINT4 u4IncAll)
{
    UINT4               u4TunnelIndex = TE_ZERO;
    UINT4               u4TunnelInstance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    UINT4               u4ErrorCode = TE_ZERO;
    INT4                i4PrevRowStatus = TE_ZERO;

    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhGetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                   u4IngressId, u4EgressId,
                                   &i4PrevRowStatus) == SNMP_FAILURE)
    {
        TE_DBG (TE_EXTN_FAIL, "\r\n%%Failed to get tunnel information\r\n");
    }

    if (TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                     u4IngressId, u4EgressId,
                                     TE_NOTINSERVICE) == CLI_FAILURE)
    {
        TE_DBG (TE_EXTN_FAIL, "\r\n%%Failed to get tunnel information\r\n");
    }

    /*Checking whether Include-Any Affinity is valid or not */
    if ((nmhTestv2MplsTunnelIncludeAnyAffinity (&u4ErrorCode,
                                                u4TunnelIndex, u4TunnelInstance,
                                                u4IngressId, u4EgressId,
                                                u4IncAny)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Invalid Include-any affinity \r\n");
        return CLI_FAILURE;
    }
    /*To Set the Include-Any Affinity of the Tunnel */
    if ((nmhSetMplsTunnelIncludeAnyAffinity (u4TunnelIndex, u4TunnelInstance,
                                             u4IngressId, u4EgressId,
                                             u4IncAny)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to Configure Include-Any"
                   "Affinity of the Tunnel\r\n");
        return CLI_FAILURE;
    }
    /*Checking whether Exclude-Any Affinity is valid or not */
    if ((nmhTestv2MplsTunnelExcludeAnyAffinity (&u4ErrorCode,
                                                u4TunnelIndex, u4TunnelInstance,
                                                u4IngressId, u4EgressId,
                                                u4ExcAny)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Invalid Exclude-any affinity \r\n");
        return CLI_FAILURE;
    }
    /*To Set the Exclude-Any Affinity of the Tunnel */
    if ((nmhSetMplsTunnelExcludeAnyAffinity (u4TunnelIndex, u4TunnelInstance,
                                             u4IngressId, u4EgressId,
                                             u4ExcAny)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to Configure Exclude-Any"
                   "Affinity of the Tunnel\r\n");
        return CLI_FAILURE;
    }
    /*Checking whether Include-All Affinity is valid or not */
    if ((nmhTestv2MplsTunnelIncludeAllAffinity (&u4ErrorCode,
                                                u4TunnelIndex, u4TunnelInstance,
                                                u4IngressId, u4EgressId,
                                                u4IncAll)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Invalid Include-all affinity \r\n");
        return CLI_FAILURE;
    }
    /*To Set the Include-All Affinity of the Tunnel */
    if ((nmhSetMplsTunnelIncludeAllAffinity (u4TunnelIndex, u4TunnelInstance,
                                             u4IngressId, u4EgressId,
                                             u4IncAll)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%%Unable to Configure Include-all affinity \r\n");
        return CLI_FAILURE;
    }

    /*In case of MBB set Old Bandwidth */
    TeCliSetMplsTunnelOldBandwidth (u4TunnelIndex, u4TunnelInstance,
                                    u4IngressId, u4EgressId);

    if (i4PrevRowStatus != TE_NOTREADY)
    {
        if (TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                         u4IngressId, u4EgressId,
                                         i4PrevRowStatus) == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Cannot Set Mpls tunnel RowStatus\r\n");
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliTunnelNoAffinities 
* Description   : This routine is delete the affinities of the 
*                 protected tunnel.  
* Input(s)      : CliHandle - Cli Context Handle
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliTunnelNoAffinities (tCliHandle CliHandle)
{
    UINT4               u4TunnelIndex = TE_ZERO;
    UINT4               u4TunnelInstance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    UINT4               u4ErrorCode = TE_ZERO;
    INT4                i4PrevRowStatus = TE_ZERO;

    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                   u4IngressId, u4EgressId,
                                   &i4PrevRowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to fetch tunnel row-status\r\n");
        return CLI_FAILURE;
    }

    if (TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                     u4IngressId, u4EgressId, TE_NOTINSERVICE)
        == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set tunnel row-status\r\n");
        return CLI_FAILURE;
    }

    /*Checking whether Include-any Affinity is valid or not */
    if ((nmhTestv2MplsTunnelIncludeAnyAffinity (&u4ErrorCode,
                                                u4TunnelIndex, u4TunnelInstance,
                                                u4IngressId, u4EgressId,
                                                TE_TUNNEL_SSN_ATTR_DEF_VAL))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%%Unable to destroy Include-Any Affinity of the tunnel\r\n");
        return CLI_FAILURE;
    }
    /*To Set the default Include-Any Afiinity */
    if ((nmhSetMplsTunnelIncludeAnyAffinity (u4TunnelIndex, u4TunnelInstance,
                                             u4IngressId, u4EgressId,
                                             TE_TUNNEL_SSN_ATTR_DEF_VAL))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%%Unable to destroy Include-Any Affinity of the tunnel\r\n");
        return CLI_FAILURE;
    }
    /*Checking whether Exclude-any Affinity is valid or not */
    if ((nmhTestv2MplsTunnelExcludeAnyAffinity (&u4ErrorCode,
                                                u4TunnelIndex, u4TunnelInstance,
                                                u4IngressId, u4EgressId,
                                                TE_TUNNEL_SSN_ATTR_DEF_VAL))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%%Unable to destroy Exclude-Any Affinity of the tunnel \r\n");
        return CLI_FAILURE;
    }
    /*To Set the default Exclude-Any Affinity */
    if ((nmhSetMplsTunnelExcludeAnyAffinity (u4TunnelIndex, u4TunnelInstance,
                                             u4IngressId, u4EgressId,
                                             TE_TUNNEL_SSN_ATTR_DEF_VAL))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%%Unable to destroy Exclude-Any Affinity of the Tunnel\r\n");
        return CLI_FAILURE;
    }
    /*Checking whether Include-all Affinity is valid or not */
    if ((nmhTestv2MplsTunnelIncludeAllAffinity (&u4ErrorCode,
                                                u4TunnelIndex, u4TunnelInstance,
                                                u4IngressId, u4EgressId,
                                                TE_TUNNEL_SSN_ATTR_DEF_VAL))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%%Unable to destroy Include-All Affinity of the Tunnel\r\n");
        return CLI_FAILURE;
    }
    /*To Set the default Include-All Affinity */
    if ((nmhSetMplsTunnelIncludeAllAffinity (u4TunnelIndex, u4TunnelInstance,
                                             u4IngressId, u4EgressId,
                                             TE_TUNNEL_SSN_ATTR_DEF_VAL))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%%Unable to destroy Include-All Affinity of the Tunnel\r\n");
        return CLI_FAILURE;
    }

    if (i4PrevRowStatus != TE_NOTREADY)
    {
        if (TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                         u4IngressId, u4EgressId,
                                         i4PrevRowStatus) == CLI_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set previous tunnel row-status "
                       "\r\n");
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliRecordRoute 
* Description   : This routine is used to enable record-route in
*                 SESSION-ATTRIBUTE object.
* Input(s)      : CliHandle           - Cli Context Handle
*                 bLabelRecordingReqd - Label Recording Required Flag
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliRecordRoute (tCliHandle CliHandle, BOOL1 bLabelRecordingReqd)
{
    UINT4               u4TunnelIndex = TE_ZERO;
    UINT4               u4TunnelInstance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    UINT4               u4ErrorCode = TE_ZERO;
    static UINT1        au1Length[CLI_MPLS_TE_SESS_ATTR] = { 0 };
    tSNMP_OCTET_STRING_TYPE MplsTeSessionAttribute;

    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    /*Setting the fourth flag in MplsTeSessionAttribute for enabling RRO */
    MplsTeSessionAttribute.pu1_OctetList = au1Length;
    MplsTeSessionAttribute.i4_Length = CLI_MPLS_TE_SESS_ATTR;

    if (nmhGetMplsTunnelSessionAttributes (u4TunnelIndex, u4TunnelInstance,
                                           u4IngressId, u4EgressId,
                                           &MplsTeSessionAttribute)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Cannot set Session Attributes\r\n");
        return CLI_FAILURE;
    }

    MplsTeSessionAttribute.pu1_OctetList[TE_ZERO] |= TE_SSN_REC_ROUTE_BIT;

    /*Check for the Session Attribute Objects */
    if ((nmhTestv2MplsTunnelSessionAttributes (&u4ErrorCode,
                                               u4TunnelIndex, u4TunnelInstance,
                                               u4IngressId, u4EgressId,
                                               &MplsTeSessionAttribute))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to Set Record Route \r\n");
        return CLI_FAILURE;
    }
    /*To Set the Record-Route Object */
    if ((nmhSetMplsTunnelSessionAttributes (u4TunnelIndex, u4TunnelInstance,
                                            u4IngressId, u4EgressId,
                                            &MplsTeSessionAttribute))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to Set Record Route\r\n");
        return CLI_FAILURE;
    }

    if (bLabelRecordingReqd == TRUE)
    {
        nmhGetGmplsTunnelAttributes (u4TunnelIndex, u4TunnelInstance,
                                     u4IngressId, u4EgressId,
                                     &MplsTeSessionAttribute);

        MplsTeSessionAttribute.pu1_OctetList[TE_ZERO] |= TE_SSN_LBL_RECORD_BIT;
        MplsTeSessionAttribute.i4_Length = sizeof (UINT1);

        if (nmhTestv2GmplsTunnelAttributes (&u4ErrorCode,
                                            u4TunnelIndex, u4TunnelInstance,
                                            u4IngressId, u4EgressId,
                                            &MplsTeSessionAttribute)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to Set Label Recording\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetGmplsTunnelAttributes (u4TunnelIndex, u4TunnelInstance,
                                         u4IngressId, u4EgressId,
                                         &MplsTeSessionAttribute)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to Set Label Recording\r\n");
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliNoRecordRoute 
* Description   : This routine is used to disbale record-route in
*                 SESSION-ATTRIBUTE object.
* Input(s)      : CliHandle - Cli Context Handle
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliNoRecordRoute (tCliHandle CliHandle)
{
    UINT4               u4TunnelIndex = TE_ZERO;
    UINT4               u4TunnelInstance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    UINT4               u4ErrorCode = TE_ZERO;
    static UINT1        au1Length[TE_ONE] = { 0 };
    tSNMP_OCTET_STRING_TYPE MplsTeSessionAttribute;

    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    MplsTeSessionAttribute.pu1_OctetList = &au1Length[0];
    MplsTeSessionAttribute.i4_Length = TE_ONE;
    /*To retrive Session Attribute value */
    if ((nmhGetMplsTunnelSessionAttributes (u4TunnelIndex, u4TunnelInstance,
                                            u4IngressId, u4EgressId,
                                            &MplsTeSessionAttribute))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to Retrive Session Attribute Object\r\n");
        return CLI_FAILURE;
    }
    /*Disabling the Record Route Bit */

    MplsTeSessionAttribute.pu1_OctetList[TE_ZERO] &=
        (UINT1) (~(TE_SSN_REC_ROUTE_BIT));

    /*Checking for Session attribute Value */
    if ((nmhTestv2MplsTunnelSessionAttributes (&u4ErrorCode,
                                               u4TunnelIndex, u4TunnelInstance,
                                               u4IngressId, u4EgressId,
                                               &MplsTeSessionAttribute))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to disable Record Route \r\n");
        return CLI_FAILURE;
    }
    /*To set the default Record route value */
    if ((nmhSetMplsTunnelSessionAttributes (u4TunnelIndex, u4TunnelInstance,
                                            u4IngressId, u4EgressId,
                                            &MplsTeSessionAttribute))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to disable Record Route \r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliHopTablePathOptAndPathNum 
* Description   : This routine is used to configure path option number 
*                 and hop list index
* Input(s)      : CliHandle    - Cli Context Handle
*                 i4PathOptNum - Hop path option index 
*                 i4PathNum    - HopListIndex
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliHopTablePathOptAndPathNum (tCliHandle CliHandle, UINT4 u4PathOptNum,
                                UINT4 u4PathNum, INT4 i4PathComp,
                                UINT1 *pAttrName)
{
    UINT4               u4TunnelIndex = TE_ZERO;
    UINT4               u4TunnelInstance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    UINT4               u4ErrorCode = TE_ZERO;
    UINT4               u4RetValMplsTunnelPathInUse = TE_ZERO;
    INT4                i4PrevRowStatus = 0;
    INT4                i4TunnelAdminStatus = 0;
    INT4                i4MbbStatus = 0;

    tSNMP_OID_TYPE      GetAttrPointer;
    tSNMP_OID_TYPE      AttrPointer;
    INT4                i4LSPEncodingType = 0;
    UINT4               u4HopListIndex = 0;
    static UINT4        au4AttrTableOid[TE_TNL_ATTR_TABLE_DEF_OFFSET] =
        { 1, 3, 6, 1, 4, 1, 2076, 13, 15, 1, 5, 1, 2 };
    static UINT4        au4GetAttrPointer[TE_TNL_ATTR_TABLE_DEF_OFFSET];

    tTeAttrListInfo     TeAttrListInfo;
    tTePathInfo        *pTePathInfo = NULL;

    MEMSET (&TeAttrListInfo, 0, sizeof (tTeAttrListInfo));

    GetAttrPointer.pu4_OidList = au4GetAttrPointer;

    MEMSET (GetAttrPointer.pu4_OidList, TE_ZERO, sizeof (UINT4) * TE_TWO);
    GetAttrPointer.u4_Length = TE_ZERO;

    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    nmhGetMplsTunnelAdminStatus (u4TunnelIndex, u4TunnelInstance,
                                 u4IngressId, u4EgressId, &i4TunnelAdminStatus);

    nmhGetFsMplsTunnelMBBStatus (u4TunnelIndex, u4TunnelInstance,
                                 u4IngressId, u4EgressId, &i4MbbStatus);

    if ((i4TunnelAdminStatus == TE_ADMIN_UP) &&
        (i4MbbStatus == MPLS_TE_MBB_DISABLED))
    {
        CliPrintf (CliHandle, "\r\n%% Tunnel is  ADMIN-UP,"
                   "Unable to configure path-option and path number \r\n");
        return CLI_FAILURE;

    }

    if (nmhGetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                   u4IngressId, u4EgressId,
                                   &i4PrevRowStatus) == SNMP_FAILURE)
    {
        TE_DBG (TE_EXTN_FAIL, "\r\n%%Failed to get tunnel information\r\n");
    }
    TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                 u4IngressId, u4EgressId, TE_NOTINSERVICE);

    nmhGetGmplsTunnelLSPEncoding (u4TunnelIndex, u4TunnelInstance,
                                  u4IngressId, u4EgressId, &i4LSPEncodingType);

    nmhGetFsMplsTunnelAttPointer (u4TunnelIndex, u4TunnelInstance,
                                  u4IngressId, u4EgressId, &GetAttrPointer);
    nmhGetMplsTunnelHopTableIndex (u4TunnelIndex, u4TunnelInstance,
                                   u4IngressId, u4EgressId, &u4HopListIndex);
    if ((u4PathNum != TE_ZERO) || (u4HopListIndex != TE_ZERO))
    {
        if (u4PathNum != TE_ZERO)
        {
            if (TeCheckPathOptionInfo (u4PathNum, u4PathOptNum, &pTePathInfo)
                == TE_FAILURE)
            {
                /* Path Option Info with the specified index is not found */
                CliPrintf (CliHandle, "\r\n Warning :NO ERO is configured,"
                           "Unable to associate path-option and path number \r\n");
            }

        }
        if (nmhTestv2MplsTunnelHopTableIndex (&u4ErrorCode,
                                              u4TunnelIndex, u4TunnelInstance,
                                              u4IngressId, u4EgressId,
                                              u4PathNum) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Invalid Path Number \r\n");
            return CLI_FAILURE;
        }
    }

    if ((nmhGetMplsTunnelPathInUse (u4TunnelIndex, u4TunnelInstance,
                                    u4IngressId, u4EgressId,
                                    &u4RetValMplsTunnelPathInUse))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to retrive Path in use\r\n");
        return CLI_FAILURE;
    }

    /*Checking whether Path Option Number is valid or not */
    if ((u4RetValMplsTunnelPathInUse != 0) &&
        (u4RetValMplsTunnelPathInUse != u4PathOptNum))
    {
        CliPrintf (CliHandle, "\r\n%%Cannot set more than one path option\r\n");
        return CLI_FAILURE;
    }
    else
    {
        if (nmhTestv2MplsTunnelPathInUse (&u4ErrorCode,
                                          u4TunnelIndex, u4TunnelInstance,
                                          u4IngressId, u4EgressId,
                                          u4PathOptNum) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Invalid Path-Option \r\n");
            return CLI_FAILURE;
        }
    }

    if (i4LSPEncodingType != GMPLS_TUNNEL_LSP_NOT_GMPLS)
    {
        if (nmhTestv2GmplsTunnelPathComp (&u4ErrorCode, u4TunnelIndex,
                                          u4TunnelInstance, u4IngressId,
                                          u4EgressId, i4PathComp)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Unable to configure Path Computation\n");
            return CLI_FAILURE;
        }
    }

    if (pAttrName != NULL)
    {
        TeAttrListInfo.i4ListNameLen = (INT4) STRLEN (pAttrName);
        MEMCPY (TeAttrListInfo.au1ListName, pAttrName,
                TeAttrListInfo.i4ListNameLen);

        if (TeSigGetAttrListIndexFromName (&TeAttrListInfo) == TE_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Attribute List do not exist\n");
            return CLI_FAILURE;
        }

        au4AttrTableOid[TE_TNL_ATTR_TABLE_DEF_OFFSET - TE_ONE]
            = TeAttrListInfo.u4ListIndex;
        AttrPointer.pu4_OidList = au4AttrTableOid;
        AttrPointer.u4_Length = TE_TNL_ATTR_TABLE_DEF_OFFSET;

        if (nmhTestv2FsMplsTunnelAttPointer (&u4ErrorCode, u4TunnelIndex,
                                             u4TunnelInstance, u4IngressId,
                                             u4EgressId, &AttrPointer)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Invalid Attribute Table Pointer \r\n");
            return CLI_FAILURE;
        }
    }

    if (i4PrevRowStatus == TE_ACTIVE)
    {
        if (nmhSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                       u4IngressId, u4EgressId, TE_NOTINSERVICE)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Invalid Tunnel Row Status \r\n");
            return CLI_FAILURE;
        }
    }

    if ((u4PathNum != TE_ZERO) || (u4HopListIndex != TE_ZERO))
    {
        if (nmhSetMplsTunnelHopTableIndex (u4TunnelIndex, u4TunnelInstance,
                                           u4IngressId, u4EgressId, u4PathNum)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%% nmhSetMplsTunnelHopTableIndex failed\r\n");
            return CLI_FAILURE;
        }

    }

    if (nmhSetMplsTunnelPathInUse (u4TunnelIndex, u4TunnelInstance,
                                   u4IngressId, u4EgressId, u4PathOptNum)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% nmhSetMplsTunnelPathInUse failed\r\n");
        return CLI_FAILURE;
    }

    if (i4LSPEncodingType != GMPLS_TUNNEL_LSP_NOT_GMPLS)
    {
        if (nmhSetGmplsTunnelPathComp (u4TunnelIndex, u4TunnelInstance,
                                       u4IngressId, u4EgressId, i4PathComp)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%% nmhSetGmplsTunnelPathComp failed\r\n");
            return CLI_FAILURE;
        }

    }

    if (pAttrName != NULL)
    {
        if (nmhSetFsMplsTunnelAttPointer (u4TunnelIndex, u4TunnelInstance,
                                          u4IngressId, u4EgressId, &AttrPointer)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Invalid Attribute Table Pointer \r\n");
            return CLI_FAILURE;
        }
    }

    if (i4PrevRowStatus != TE_NOTREADY)
    {
        TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                     u4IngressId, u4EgressId, i4PrevRowStatus);
    }

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliNoHopTablePathOptAndPathNum 
* Description   : This routine is used to configure path option number 
*                 and hop list index
* Input(s)      : CliHandle    - Cli Context Handle
*                 i4PathOptNum - Hop path option index 
*                 i4PathNum    - HopListIndex
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliNoHopTablePathOptAndPathNum (tCliHandle CliHandle, UINT4 u4PathOptNum,
                                  UINT4 u4PathNum)
{
    UINT4               u4TunnelIndex = TE_ZERO;
    UINT4               u4TunnelInstance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    UINT4               u4ErrorCode = TE_ZERO;
    UINT4               u4RetValMplsTunnelHopTableIndex = TE_ZERO;
    UINT4               u4RetValMplsTunnelPathInUse = TE_ZERO;
    INT4                i4PrevRowStatus = 0;
    INT4                i4TunnelAdminStatus = 0;
    INT4                i4MbbStatus = 0;

    tSNMP_OID_TYPE      SetAttrPointer;
    static UINT4        au4SetAttrPointer[TE_TNL_ATTR_TABLE_DEF_OFFSET] =
        { 1, 3, 6, 1, 4, 1, 2076, 13, 15, 1, 5, 1, 2 };

    au4SetAttrPointer[TE_TNL_ATTR_TABLE_DEF_OFFSET - TE_ONE] = 0;
    SetAttrPointer.pu4_OidList = au4SetAttrPointer;
    SetAttrPointer.u4_Length = TE_TNL_ATTR_TABLE_DEF_OFFSET;

    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    nmhGetMplsTunnelAdminStatus (u4TunnelIndex, u4TunnelInstance,
                                 u4IngressId, u4EgressId, &i4TunnelAdminStatus);

    nmhGetFsMplsTunnelMBBStatus (u4TunnelIndex, u4TunnelInstance,
                                 u4IngressId, u4EgressId, &i4MbbStatus);

    if ((i4TunnelAdminStatus == TE_ADMIN_UP) &&
        (i4MbbStatus == MPLS_TE_MBB_DISABLED))
    {
        CliPrintf (CliHandle, "\r\n%% Tunnel is  ADMIN-UP,"
                   "Unable to configure path-option and path number \r\n");
        return CLI_FAILURE;

    }

    if (nmhGetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                   u4IngressId, u4EgressId,
                                   &i4PrevRowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to get tunnel row status\r\n");
        return CLI_FAILURE;
    }

    if ((nmhSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                    u4IngressId, u4EgressId,
                                    TE_NOTINSERVICE)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to reset MplsTunnelRowStatus\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMplsTunnelAttPointer (&u4ErrorCode, u4TunnelIndex,
                                         u4TunnelInstance, u4IngressId,
                                         u4EgressId, &SetAttrPointer)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Invalid Attribute  Table Pointer \r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsTunnelAttPointer (u4TunnelIndex, u4TunnelInstance,
                                      u4IngressId, u4EgressId, &SetAttrPointer)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Invalid Attribute  Table Pointer \r\n");
        return CLI_FAILURE;
    }

    /*To retrive Hop table index */
    if ((nmhGetMplsTunnelHopTableIndex (u4TunnelIndex, u4TunnelInstance,
                                        u4IngressId, u4EgressId,
                                        &u4RetValMplsTunnelHopTableIndex))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to retrive Hop table index\r\n");
        return CLI_FAILURE;
    }
    /*To retrive path in use */
    if ((nmhGetMplsTunnelPathInUse (u4TunnelIndex, u4TunnelInstance,
                                    u4IngressId, u4EgressId,
                                    &u4RetValMplsTunnelPathInUse))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to retrive Path in use\r\n");
        return CLI_FAILURE;
    }
    /*Checking with the configured values */
    if ((u4RetValMplsTunnelHopTableIndex != u4PathNum) ||
        (u4RetValMplsTunnelPathInUse != u4PathOptNum))
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to configure path-option and path number\r\n");
        TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                     u4IngressId, u4EgressId, i4PrevRowStatus);

        return CLI_FAILURE;
    }
    else
    {
        u4PathNum = TE_ZERO;
        u4PathOptNum = TE_ZERO;
    }

    /*Checking whether Path Number is valid or not */
    if ((nmhTestv2MplsTunnelHopTableIndex (&u4ErrorCode,
                                           u4TunnelIndex, u4TunnelInstance,
                                           u4IngressId, u4EgressId,
                                           u4PathNum)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Invalid Path Number \r\n");
        return CLI_FAILURE;
    }
    /*To Set Hop Table Index */
    if ((nmhSetMplsTunnelHopTableIndex (u4TunnelIndex, u4TunnelInstance,
                                        u4IngressId, u4EgressId,
                                        u4PathNum)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to configure Hop table"
                   "Index\r\n");
        return CLI_FAILURE;
    }
    /*Checking whether Path Option Number is valid or not */
    if ((nmhTestv2MplsTunnelPathInUse (&u4ErrorCode,
                                       u4TunnelIndex, u4TunnelInstance,
                                       u4IngressId, u4EgressId,
                                       u4PathOptNum)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Invalid Path-Option \r\n");
        return CLI_FAILURE;
    }
    /*To Set Path In Use */
    if ((nmhSetMplsTunnelPathInUse (u4TunnelIndex, u4TunnelInstance,
                                    u4IngressId, u4EgressId,
                                    u4PathOptNum)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to configure Path In Use \r\n");
        return CLI_FAILURE;
    }

    if (i4PrevRowStatus != TE_NOTREADY)
    {
        TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                     u4IngressId, u4EgressId, i4PrevRowStatus);
    }

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliTunnelHopTableEntries
* Description   : This routine is used to configure hop table.
* Input(s)      : CliHandle - Cli Context Handle
*                 u4HopNum  - Hop number
*                 u4HopAdd  - Hop address
*                 i4HopType - Hop type
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliTunnelHopTableEntries (tCliHandle CliHandle, UINT4 u4HopNum,
                            UINT4 u4HopAddr, INT4 i4HopType)
{
    UINT4               u4TunnelIndex = TE_ZERO;
    UINT4               u4TunnelInstance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    UINT4               u4HopListIndex = TE_ZERO;
    UINT4               u4HopPathOptionIndex = TE_ZERO;
    INT4                i4PrevRowStatus = 0;
    INT4                i4RetVal = TE_ZERO;
    tTeCliArgs          TeCliArgs;

    MEMSET (&TeCliArgs, TE_ZERO, sizeof (tTeCliArgs));

    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    /*To retrive Hop Table Index */
    nmhGetMplsTunnelHopTableIndex (u4TunnelIndex, u4TunnelInstance,
                                   u4IngressId, u4EgressId, &u4HopListIndex);

    if (u4HopListIndex == TE_ZERO)
    {
        CliPrintf (CliHandle, "\r%%Hop List Index not configured\n");
        return CLI_FAILURE;
    }

    /*To retrive Hop Path Option Index */
    nmhGetMplsTunnelPathInUse (u4TunnelIndex, u4TunnelInstance,
                               u4IngressId, u4EgressId, &u4HopPathOptionIndex);

    if (u4HopPathOptionIndex == TE_ZERO)
    {
        CliPrintf (CliHandle, "\r%%Path Option Index not configured\n");
        return CLI_FAILURE;
    }

    if (nmhGetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                   u4IngressId, u4EgressId,
                                   &i4PrevRowStatus) == SNMP_FAILURE)
    {
        TE_DBG (TE_EXTN_FAIL, "\r\n%%Failed to get tunnel information\r\n");
    }
    if (TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                     u4IngressId, u4EgressId, TE_NOTINSERVICE)
        == CLI_FAILURE)
    {
        TE_DBG (TE_EXTN_FAIL, "\r\n%%Failed to get tunnel information\r\n");
        return CLI_FAILURE;
    }

    TeCliArgs.u4PathListId = u4HopListIndex;
    TeCliArgs.u4PathOptionId = u4HopPathOptionIndex;
    TeCliArgs.u4HopIndex = u4HopNum;
    TeCliArgs.u4HopIpAddr = u4HopAddr;
    TeCliArgs.u4HopUnnumIf = TE_ZERO;
    TeCliArgs.i4HopIncludeExclude = TE_SNMP_TRUE;
    TeCliArgs.i4HopType = i4HopType;
    TeCliArgs.i4PathComp = TE_EXPLICIT;
    TeCliArgs.u4ForwardLbl = MPLS_INVALID_LABEL;
    TeCliArgs.u4ReverseLbl = MPLS_INVALID_LABEL;

    i4RetVal = TeCliAddHopTableEntries (CliHandle, &TeCliArgs);

    if (i4PrevRowStatus != TE_NOTREADY)
    {
        TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                     u4IngressId, u4EgressId, i4PrevRowStatus);
    }

    return i4RetVal;
}

/******************************************************************************
* Function Name : TeCliTunnelNoHopTableEntries
* Description   : This routine is used to destroy hop table.
* Input(s)      : CliHandle - Cli Context Handle
*                 u4HopNum  - Hop number
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliTunnelNoHopTableEntries (tCliHandle CliHandle, UINT4 u4HopNum)
{
    UINT4               u4TunnelIndex = TE_ZERO;
    UINT4               u4TunnelInstance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    UINT4               u4HopListIndex = TE_ZERO;
    UINT4               u4HopPathOptionIndex = TE_ZERO;
    INT4                i4PrevRowStatus = TE_ZERO;
    INT4                i4RetVal = TE_ZERO;
    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    /*To retrive Hop table Index */
    nmhGetMplsTunnelHopTableIndex (u4TunnelIndex, u4TunnelInstance,
                                   u4IngressId, u4EgressId, &u4HopListIndex);
    /*To retrive Path Option Index */
    nmhGetMplsTunnelPathInUse (u4TunnelIndex, u4TunnelInstance,
                               u4IngressId, u4EgressId, &u4HopPathOptionIndex);
    if (nmhGetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                   u4IngressId, u4EgressId,
                                   &i4PrevRowStatus) == SNMP_FAILURE)
    {
        TE_DBG (TE_EXTN_FAIL, "\r\n%%Failed to get tunnel information\r\n");
    }

    if (TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                     u4IngressId, u4EgressId,
                                     TE_NOTINSERVICE) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\n%% Unable to set tunnel row status\n");
        return CLI_FAILURE;
    }

    i4RetVal
        = TeCliSetMplsTunnelHopRowStatus (u4HopListIndex, u4HopPathOptionIndex,
                                          u4HopNum, TE_DESTROY);

    if (i4PrevRowStatus != TE_NOTREADY)
    {
        TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                     u4IngressId, u4EgressId, i4PrevRowStatus);
    }

    if (i4RetVal == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\n%% Unable to destroy hop table\n");
    }

    return i4RetVal;
}

#ifdef MPLS_RSVPTE_WANTED
/******************************************************************************
* Function Name : TeCliFrrShow  
* Description   : This routine is used show FRR database.
* Input(s)      : CliHandle     - Cli Context Handle.
*                 i4FrrDatabase - one-to-one/many-to-one/both.
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliFrrShow (tCliHandle CliHandle, INT4 i4FrrDatabase)
{
    UINT4               u4MplsTunnelIndex = TE_ZERO;
    UINT4               u4MplsTunnelInstance = TE_ZERO;
    UINT4               u4MplsTunnelIngressLSRId = TE_ZERO;
    UINT4               u4MplsTunnelEgressLSRId = TE_ZERO;

    /*To Get TE Table Indicies */
    if ((nmhGetFirstIndexMplsTunnelTable (&u4MplsTunnelIndex,
                                          &u4MplsTunnelInstance,
                                          &u4MplsTunnelIngressLSRId,
                                          &u4MplsTunnelEgressLSRId)) ==
        SNMP_FAILURE)
    {
        return CLI_SUCCESS;
    }
    /*Checking for the type of Protection Method */
    if (i4FrrDatabase == CLI_MPLS_FRR_SHOW_ONETOONE)
    {
        TeCliFrrShowOne2One (CliHandle,
                             u4MplsTunnelIndex,
                             u4MplsTunnelInstance,
                             u4MplsTunnelIngressLSRId, u4MplsTunnelEgressLSRId);
    }
    /*Checking for the type of Protection Method */
    if (i4FrrDatabase == CLI_MPLS_FRR_SHOW_MANYTOONE)
    {
        TeCliFrrShowMany2One (CliHandle,
                              u4MplsTunnelIndex,
                              u4MplsTunnelInstance,
                              u4MplsTunnelIngressLSRId,
                              u4MplsTunnelEgressLSRId);
    }
    /*Checking for the type of Protection Method */
    if (i4FrrDatabase == CLI_MPLS_FRR_SHOW_ALL)
    {
        TeCliFrrShowAllFrrDatabase (CliHandle,
                                    u4MplsTunnelIndex,
                                    u4MplsTunnelInstance,
                                    u4MplsTunnelIngressLSRId,
                                    u4MplsTunnelEgressLSRId);
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliFrrShowOne2One  
* Description   : This routine is used show One2One database.
* Input(s)      : CliHandle                - Cli Context Handle.
*                 u4MplsTunnelIndex        - Tunnel index
*                 u4MplsTunnelInstance     - Tunnel instance
*                 u4MplsTunnelIngressLSRId - Ingresss Id
*                 u4MplsTunnelEgressLSRId  - Egress Id
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliFrrShowOne2One (tCliHandle CliHandle,
                     UINT4 u4MplsTunnelIndex,
                     UINT4 u4MplsTunnelInstance,
                     UINT4 u4MplsTunnelIngressLSRId,
                     UINT4 u4MplsTunnelEgressLSRId)
{
    UINT4               u4CspfRetryCount = TE_ZERO;
    UINT4               u4ARHopListIndex = TE_ZERO;
    UINT4               u4ARHopIndex = TE_ONE;
    UINT4               u4ExtBkpTunIdx = TE_ONE;
    UINT4               u4ExtBkpTunInst = TE_ONE;
    UINT4               u4BkpTunIngrLSRId = TE_ONE;
    UINT4               u4BkpTunEgrLSRId = TE_ONE;
    INT4                i4CspfRetryInterval = TE_ZERO;
    INT4                i4PlrSenderAddrType = TE_ZERO;
    INT4                i4PlrAvoidNAddrType = TE_ZERO;
    INT4                i4DetourActive = TE_ZERO;
    INT4                i4DetourMerging = TE_ZERO;
    INT4                i4SEStyle = TE_ZERO;
    INT4                i4Prot = TE_ZERO;
    INT4                i4RetValMplsTunnelRole = TE_ZERO;
    static UINT1        au1PlrSenderAddr[ROUTER_ID_LENGTH] = { 0 };
    static UINT1        au1PlrAvoidNAddr[ROUTER_ID_LENGTH] = { 0 };
    static UINT1        au1One2OnePlrId[ROUTER_ID_LENGTH] = { 0 };
    static UINT1        au1FrrRevertiveMode[ROUTER_ID_LENGTH] = { 0 };
    static UINT1        au1ARHopIpAddr[MPLS_INDEX_LENGTH] = { 0 };
    static UINT1        au1ARHopProtectType[TE_ONE] = { 0 };
    static UINT1        au1ARHopProtectTypeInUse[TE_ONE] = { 0 };
    static UINT1        au1SessionAttributes[TE_ONE] = { 0 };
    tSNMP_OCTET_STRING_TYPE PlrSenderAddr;
    tSNMP_OCTET_STRING_TYPE PlrAvoidNAddr;
    tSNMP_OCTET_STRING_TYPE One2OnePlrId;
    tSNMP_OCTET_STRING_TYPE FrrRevertiveMode;
    tSNMP_OCTET_STRING_TYPE ARHopIpAddr;
    tSNMP_OCTET_STRING_TYPE ARHopProtectType;
    tSNMP_OCTET_STRING_TYPE ARHopProtectTypeInUse;
    tSNMP_OCTET_STRING_TYPE SessionAttributes;

    PlrSenderAddr.i4_Length = ROUTER_ID_LENGTH;
    PlrAvoidNAddr.i4_Length = ROUTER_ID_LENGTH;
    One2OnePlrId.i4_Length = ROUTER_ID_LENGTH;
    FrrRevertiveMode.i4_Length = ROUTER_ID_LENGTH;
    SessionAttributes.i4_Length = CLI_MPLS_TE_SESS_ATTR;
    PlrSenderAddr.pu1_OctetList = &au1PlrSenderAddr[0];
    PlrAvoidNAddr.pu1_OctetList = &au1PlrAvoidNAddr[0];
    One2OnePlrId.pu1_OctetList = &au1One2OnePlrId[0];
    FrrRevertiveMode.pu1_OctetList = &au1FrrRevertiveMode[0];
    ARHopIpAddr.pu1_OctetList = au1ARHopIpAddr;
    ARHopProtectType.pu1_OctetList = au1ARHopProtectType;
    ARHopProtectTypeInUse.pu1_OctetList = au1ARHopProtectTypeInUse;
    SessionAttributes.pu1_OctetList = au1SessionAttributes;

    do
    {
        /* For FRR all required information for display is available in non-zero
         * instance only. So, skip all instance zero tunnel instances. */
        if (u4MplsTunnelInstance == TE_ZERO)
        {
            continue;
        }

        /*To retrive session attribuite object */
        if (nmhGetMplsTunnelSessionAttributes (u4MplsTunnelIndex,
                                               u4MplsTunnelInstance,
                                               u4MplsTunnelIngressLSRId,
                                               u4MplsTunnelEgressLSRId,
                                               &SessionAttributes) ==
            SNMP_FAILURE)
        {
            continue;
        }

        /*To retrive the tunnel role */
        if ((nmhGetMplsTunnelRole (u4MplsTunnelIndex,
                                   u4MplsTunnelInstance,
                                   u4MplsTunnelIngressLSRId,
                                   u4MplsTunnelEgressLSRId,
                                   &i4RetValMplsTunnelRole)) == SNMP_FAILURE)
        {
            continue;
        }
        /*Checking whether the node is head-end of the tunnel */
        if (i4RetValMplsTunnelRole == TE_INGRESS)
        {
            if ((SessionAttributes.
                 pu1_OctetList[TE_ZERO] & TE_SSN_FAST_REROUTE_BIT) !=
                TE_SSN_FAST_REROUTE_BIT)
            {
                continue;
            }
            /*To retrive Protection method */
            if (nmhGetFsMplsTunnelExtProtectionMethod (u4MplsTunnelIndex,
                                                       u4MplsTunnelInstance,
                                                       u4MplsTunnelIngressLSRId,
                                                       u4MplsTunnelEgressLSRId,
                                                       &i4Prot) == SNMP_FAILURE)
            {
                continue;
            }
            else if (i4Prot != TE_TNL_FRR_ONE2ONE_METHOD)
            {
                continue;
            }
            /*To retrive SE style flag */
            if ((nmhGetFsMplsFrrConstSEStyle (u4MplsTunnelIndex,
                                              u4MplsTunnelInstance,
                                              u4MplsTunnelIngressLSRId,
                                              u4MplsTunnelEgressLSRId,
                                              &i4SEStyle)) == SNMP_FAILURE)
            {
                i4SEStyle = TE_SNMP_TRUE;
            }

        }
        else
        {
            /*To retrive Protection method */
            if (nmhGetFsMplsTunnelExtProtectionMethod (u4MplsTunnelIndex,
                                                       u4MplsTunnelInstance,
                                                       u4MplsTunnelIngressLSRId,
                                                       u4MplsTunnelEgressLSRId,
                                                       &i4Prot) == SNMP_FAILURE)
            {
                continue;
            }
            else if (i4Prot != TE_TNL_FRR_ONE2ONE_METHOD)
            {
                continue;
            }
            /*Checking whether fast-reroute bit is set in the session-attribute obj */
            if ((SessionAttributes.pu1_OctetList[TE_ZERO] &
                 TE_FRR_SSN_ATTR_LOCAL_PROT_FLAG) !=
                TE_FRR_SSN_ATTR_LOCAL_PROT_FLAG)
            {
                continue;
            }
            /*To check whether SE style flag is set or not */
            i4SEStyle = (SessionAttributes.pu1_OctetList[TE_ZERO] &
                         TE_FRR_SSN_ATTR_SE_STYLE_FLAG);
            if (i4SEStyle == TE_FRR_SSN_ATTR_SE_STYLE_FLAG)
            {
                i4SEStyle = TE_SNMP_TRUE;
            }
            else
            {
                i4SEStyle = TE_SNMP_FALSE;
            }
        }
        /*Retriving the backup indices */
        if ((nmhGetFsMplsTunnelExtBkpTunIdx (u4MplsTunnelIndex,
                                             u4MplsTunnelInstance,
                                             u4MplsTunnelIngressLSRId,
                                             u4MplsTunnelEgressLSRId,
                                             &u4ExtBkpTunIdx) == SNMP_SUCCESS)
            &&
            (nmhGetFsMplsTunnelExtBkpInst
             (u4MplsTunnelIndex, u4MplsTunnelInstance, u4MplsTunnelIngressLSRId,
              u4MplsTunnelEgressLSRId, &u4ExtBkpTunInst) == SNMP_SUCCESS)
            &&
            (nmhGetFsMplsTunnelExtBkpIngrLSRId
             (u4MplsTunnelIndex, u4MplsTunnelInstance, u4MplsTunnelIngressLSRId,
              u4MplsTunnelEgressLSRId, &u4BkpTunIngrLSRId) == SNMP_SUCCESS)
            &&
            (nmhGetFsMplsTunnelExtBkpEgrLSRId
             (u4MplsTunnelIndex, u4MplsTunnelInstance, u4MplsTunnelIngressLSRId,
              u4MplsTunnelEgressLSRId, &u4BkpTunEgrLSRId) == SNMP_SUCCESS))
        {
            if (u4ExtBkpTunIdx != TE_ZERO && u4ExtBkpTunInst != TE_ZERO)
            {
                /*To retrive PLR ID */
                if ((nmhGetFsMplsTunnelExtOne2OnePlrId (u4ExtBkpTunIdx,
                                                        u4ExtBkpTunInst,
                                                        u4BkpTunIngrLSRId,
                                                        u4BkpTunEgrLSRId,
                                                        &One2OnePlrId)) ==
                    SNMP_FAILURE)
                {
                    continue;
                }
                /*To retrive Sender Address type */
                if ((nmhGetFsMplsTunnelExtOne2OnePlrSenderAddrType
                     (u4ExtBkpTunIdx, u4ExtBkpTunInst, u4BkpTunIngrLSRId,
                      u4BkpTunEgrLSRId, &i4PlrSenderAddrType)) == SNMP_FAILURE)
                {
                    continue;
                }
                /*To retrive Sender Address */
                if ((nmhGetFsMplsTunnelExtOne2OnePlrSenderAddr (u4ExtBkpTunIdx,
                                                                u4ExtBkpTunInst,
                                                                u4BkpTunIngrLSRId,
                                                                u4BkpTunEgrLSRId,
                                                                &PlrSenderAddr))
                    == SNMP_FAILURE)
                {
                    continue;
                }
                /*To retrive Avoid Node Address type */
                if ((nmhGetFsMplsTunnelExtOne2OnePlrAvoidNAddrType
                     (u4ExtBkpTunIdx, u4ExtBkpTunInst, u4BkpTunIngrLSRId,
                      u4BkpTunEgrLSRId, &i4PlrAvoidNAddrType)) == SNMP_FAILURE)
                {
                    continue;
                }
                /*To retrive Avoid Node Address */
                if ((nmhGetFsMplsTunnelExtOne2OnePlrAvoidNAddr (u4ExtBkpTunIdx,
                                                                u4ExtBkpTunInst,
                                                                u4BkpTunIngrLSRId,
                                                                u4BkpTunEgrLSRId,
                                                                &PlrAvoidNAddr))
                    == SNMP_FAILURE)
                {
                    continue;
                }
            }
            else
            {
                continue;
            }
        }
        /*To retrive Detour Object Support */
        if ((nmhGetFsMplsFrrDetourEnabled (&i4DetourActive)) == SNMP_FAILURE)
        {
            continue;
        }
        /*To retrive Detour-Merging Support */
        if ((nmhGetFsMplsFrrDetourMergingEnabled (&i4DetourMerging)) ==
            SNMP_FAILURE)
        {
            continue;
        }
        /*To retrive CSPF retry interval */
        if ((nmhGetFsMplsFrrCspfRetryInterval (&i4CspfRetryInterval)) ==
            SNMP_FAILURE)
        {
            continue;
        }
        /*To retrive CSPF retry count */
        if ((nmhGetFsMplsFrrCspfRetryCount (&u4CspfRetryCount)) == SNMP_FAILURE)
        {
            continue;
        }
        /*To retrive revertive mode */
        if ((nmhGetFsMplsFrrRevertiveMode (&FrrRevertiveMode)) == SNMP_FAILURE)
        {
            continue;
        }

        if (i4SEStyle == TE_SNMP_FALSE)
        {
            CliPrintf (CliHandle, "\r\nOne-to-one Backup : Path Specific\n");
            if (One2OnePlrId.pu1_OctetList[0] != TE_ZERO)
            {
                CliPrintf (CliHandle, "\rPLR Address :  %d.%d.%d.%d\n",
                           One2OnePlrId.pu1_OctetList[0],
                           One2OnePlrId.pu1_OctetList[1],
                           One2OnePlrId.pu1_OctetList[2],
                           One2OnePlrId.pu1_OctetList[3]);
            }
            if (i4PlrSenderAddrType == LSR_ADDR_IPV4)
            {
                CliPrintf (CliHandle, "\rPLR Sender Address Type : IPv4\n");
            }
            else if (i4PlrSenderAddrType == LSR_ADDR_IPV6)
            {
                CliPrintf (CliHandle, "\rPLR Sender Address Type : IPv6\n");
            }
            if (PlrSenderAddr.pu1_OctetList[0] != TE_ZERO)
            {
                CliPrintf (CliHandle,
                           "\rPLR Sender Address : %d.%d.%d.%d\n",
                           PlrSenderAddr.pu1_OctetList[0],
                           PlrSenderAddr.pu1_OctetList[1],
                           PlrSenderAddr.pu1_OctetList[2],
                           PlrSenderAddr.pu1_OctetList[3]);
            }
            if (i4PlrAvoidNAddrType == LSR_ADDR_IPV4)
            {
                CliPrintf (CliHandle, "Avoid Node Address Type :  IPv4\n");
            }
            else if (i4PlrAvoidNAddrType == LSR_ADDR_IPV6)
            {
                CliPrintf (CliHandle, "\rAvoid Node Address Type :IPv6\n");
            }
            if (PlrAvoidNAddr.pu1_OctetList[0] != TE_ZERO)
            {
                CliPrintf (CliHandle,
                           "\rAvoid Node Address : %d.%d.%d.%d\n",
                           PlrAvoidNAddr.pu1_OctetList[0],
                           PlrAvoidNAddr.pu1_OctetList[1],
                           PlrAvoidNAddr.pu1_OctetList[2],
                           PlrAvoidNAddr.pu1_OctetList[3]);
            }
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\nOne-to-one Backup : Sender-Template Specific\n");
            if (One2OnePlrId.pu1_OctetList[0] != 0)
            {
                CliPrintf (CliHandle, "\rPLR Address :  %d.%d.%d.%d\n",
                           One2OnePlrId.pu1_OctetList[0],
                           One2OnePlrId.pu1_OctetList[1],
                           One2OnePlrId.pu1_OctetList[2],
                           One2OnePlrId.pu1_OctetList[3]);
            }
            if (i4PlrSenderAddrType == LSR_ADDR_IPV4)
            {
                CliPrintf (CliHandle, "\rPLR Sender Address Type : IPv4\n");
            }
            else if (i4PlrSenderAddrType == LSR_ADDR_IPV6)
            {
                CliPrintf (CliHandle, "\rPLR Sender Address Type : IPv6\n");
            }

            if (PlrSenderAddr.pu1_OctetList[0] != 0)
            {
                CliPrintf (CliHandle,
                           "\rPLR Sender Address : %d.%d.%d.%d\n",
                           PlrSenderAddr.pu1_OctetList[0],
                           PlrSenderAddr.pu1_OctetList[1],
                           PlrSenderAddr.pu1_OctetList[2],
                           PlrSenderAddr.pu1_OctetList[3]);
            }
            if (PlrAvoidNAddr.pu1_OctetList[0] != 0)
            {
                CliPrintf (CliHandle,
                           "\rAvoid Node Address : %d.%d.%d.%d\n",
                           PlrAvoidNAddr.pu1_OctetList[0],
                           PlrAvoidNAddr.pu1_OctetList[1],
                           PlrAvoidNAddr.pu1_OctetList[2],
                           PlrAvoidNAddr.pu1_OctetList[3]);
            }

        }
        if (i4DetourActive == TE_SNMP_TRUE)
        {
            CliPrintf (CliHandle, "\rDetour Support : Active\n");
        }
        else
        {
            CliPrintf (CliHandle, "\rDetour Support : Inactive\n");
        }
        if (i4DetourMerging == TE_SNMP_TRUE)
        {
            CliPrintf (CliHandle, "\rDetour-Merging : Active\n");
        }
        else
        {
            CliPrintf (CliHandle, "\rDetour-Merging : Inactive\n");
        }
        if (FrrRevertiveMode.pu1_OctetList[0] == TE_SNMP_TRUE)
        {
            CliPrintf (CliHandle, "\rRevertive Mode : Global\n");
        }
        else
        {
            CliPrintf (CliHandle, "\rRevertive Mode : Local\n");
        }
        CliPrintf (CliHandle, "\rCSPF Time Interval : %dms\n",
                   i4CspfRetryInterval);
        CliPrintf (CliHandle, "\rCSPF Retry Count : %d\n", u4CspfRetryCount);
        CliPrintf (CliHandle,
                   "\rReceived RRO (ProtectionFlag 1=Available 2=InUse 4=B/W 8=Node "
                   "10=Softpreempt)\n");
        CliPrintf (CliHandle, "\rRecord Route : ");

        /*To retrive ARHop Table Index */
        if (nmhGetMplsTunnelARHopTableIndex (u4MplsTunnelIndex,
                                             u4MplsTunnelInstance,
                                             u4MplsTunnelIngressLSRId,
                                             u4MplsTunnelEgressLSRId,
                                             &u4ARHopListIndex) == SNMP_SUCCESS)
        {
            u4ARHopIndex = TE_ONE;
        }
        do
        {
            if (u4ARHopListIndex == 0 || u4ARHopIndex == 0)
            {
                break;
            }
            /*To retrive ARHop Ip address */
            if (nmhGetMplsTunnelARHopIpAddr (u4ARHopListIndex,
                                             u4ARHopIndex,
                                             &ARHopIpAddr) == SNMP_FAILURE)
            {
                u4ARHopIndex++;
                continue;
            }

            /*To retrive ARHop protection type */
            if ((nmhGetFsMplsFrrTunARHopProtectType (u4ARHopListIndex,
                                                     u4ARHopIndex,
                                                     &ARHopProtectType) ==
                 SNMP_FAILURE))
            {
                u4ARHopIndex++;
                continue;
            }
            /*To retrive ARHop protection Inuse */
            if ((nmhGetFsMplsFrrTunARHopProtectTypeInUse (u4ARHopListIndex,
                                                          u4ARHopIndex,
                                                          &ARHopProtectTypeInUse)
                 == SNMP_FAILURE))
            {
                u4ARHopIndex++;
                continue;
            }

            /*To retrive ARHop Bandwidth protection */
            if ((nmhGetFsMplsFrrTunARBwProtAvailable (u4ARHopListIndex,
                                                      u4ARHopIndex,
                                                      &i4Prot) == SNMP_FAILURE))
            {
                u4ARHopIndex++;
                continue;
            }

            CliPrintf (CliHandle, " %d.%d.%d.%d",
                       ARHopIpAddr.pu1_OctetList[0],
                       ARHopIpAddr.pu1_OctetList[1],
                       ARHopIpAddr.pu1_OctetList[2],
                       ARHopIpAddr.pu1_OctetList[3]);

            if (ARHopProtectType.pu1_OctetList[0] == TE_TNL_FRR_PROTECTION_LINK)
            {
                if (ARHopProtectTypeInUse.pu1_OctetList[0] ==
                    TE_TNL_FRR_PROTECTION_LINK)
                {
                    if (i4Prot == TE_SNMP_TRUE)
                    {
                        CliPrintf (CliHandle, "(flag=7) ");
                    }
                    else
                    {
                        CliPrintf (CliHandle, "(flag=3) ");
                    }
                }
                else
                {
                    if (i4Prot == TE_SNMP_TRUE)
                    {
                        CliPrintf (CliHandle, "(flag=5) ");
                    }
                    else
                    {
                        CliPrintf (CliHandle, "(flag=1) ");
                    }
                }
            }
            else if (ARHopProtectType.pu1_OctetList[0] ==
                     TE_TNL_FRR_PROTECTION_NODE)
            {
                if (ARHopProtectTypeInUse.pu1_OctetList[0] ==
                    TE_TNL_FRR_PROTECTION_LINK)
                {
                    if (i4Prot == TE_SNMP_TRUE)
                    {
                        CliPrintf (CliHandle, "(flag=14) ");
                    }
                    else
                    {
                        CliPrintf (CliHandle, "(flag=10) ");
                    }
                }
                else
                {
                    if (i4Prot == TE_SNMP_TRUE)
                    {
                        CliPrintf (CliHandle, "(flag=12) ");
                    }
                    else
                    {
                        CliPrintf (CliHandle, "(flag=8) ");
                    }
                }
            }
            else
            {
                CliPrintf (CliHandle, "(flag=0) ");
            }
            u4ARHopIndex++;
        }                        /*To retrive ARHop table Next indices */
        while (u4ARHopIndex <= MAX_TE_ARHOP_PER_LIST);
        CliPrintf (CliHandle, "\n");
    }                            /*Retriving the next tunnel */
    while ((nmhGetNextIndexMplsTunnelTable (u4MplsTunnelIndex,
                                            &u4MplsTunnelIndex,
                                            u4MplsTunnelInstance,
                                            &u4MplsTunnelInstance,
                                            u4MplsTunnelIngressLSRId,
                                            &u4MplsTunnelIngressLSRId,
                                            u4MplsTunnelEgressLSRId,
                                            &u4MplsTunnelEgressLSRId)) ==
           SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliFrrShowMany2One 
* Description   : This routine is used show Many2One database.
* Input(s)      : CliHandle                - Cli Context Handle.
*                 u4MplsTunnelIndex        - Tunnel index.
*                 u4MplsTunnelInstance     - Tunnel instance.
*                 u4MplsTunnelIngressLSRId - Ingresss Id.
*                 u4MplsTunnelEgressLSRId  - Egress Id.
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliFrrShowMany2One (tCliHandle CliHandle,
                      UINT4 u4MplsTunnelIndex,
                      UINT4 u4MplsTunnelInstance,
                      UINT4 u4MplsTunnelIngressLSRId,
                      UINT4 u4MplsTunnelEgressLSRId)
{
    BOOL1               bTitleFound = FALSE;

    TeCliFrrShowIngressMany2One (CliHandle,
                                 u4MplsTunnelIndex,
                                 u4MplsTunnelInstance,
                                 u4MplsTunnelIngressLSRId,
                                 u4MplsTunnelEgressLSRId, &bTitleFound);
    TeCliFrrShowIntermediateMany2One (CliHandle,
                                      u4MplsTunnelIndex,
                                      u4MplsTunnelInstance,
                                      u4MplsTunnelIngressLSRId,
                                      u4MplsTunnelEgressLSRId, bTitleFound);
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliFrrShowIngressMany2One 
* Description   : This routine is used show Many2One database at Ingress.
* Input(s)      : CliHandle                - Cli Context Handle.
*                 u4MplsTunnelIndex        - Tunnel index.
*                 u4MplsTunnelInstance     - Tunnel instance.
*                 u4MplsTunnelIngressLSRId - Ingresss Id.
*                 u4MplsTunnelEgressLSRId  - Egress Id.
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliFrrShowIngressMany2One (tCliHandle CliHandle,
                             UINT4 u4MplsTunnelIndex,
                             UINT4 u4MplsTunnelInstance,
                             UINT4 u4MplsTunnelIngressLSRId,
                             UINT4 u4MplsTunnelEgressLSRId, BOOL1 * bTitleFound)
{
    UINT4               u4XCIndex = TE_ZERO;
    UINT4               u4InIndex = TE_ZERO;
    UINT4               u4OutIndex = TE_ZERO;
    UINT4               u4InLabel = TE_ZERO;
    UINT4               u4OutLabel = TE_ZERO;
#ifdef CFA_WANTED
    UINT4               u4OutL3Intf = TE_ZERO;
    UINT4               u4BkpOutL3Intf = TE_ZERO;
#endif
    UINT4               u4BkpXCIndex = TE_ZERO;
    UINT4               u4BkpInIndex = TE_ZERO;
    UINT4               u4BkpOutIndex = TE_ZERO;
    UINT4               u4BkpOutLabel = TE_ZERO;
    UINT4               u4ExtBkpTunIdx = TE_ZERO;
    UINT4               u4ExtBkpTunInst = TE_ZERO;
    UINT4               u4BkpTunIngrLSRId = TE_ZERO;
    UINT4               u4BkpTunEgrLSRId = TE_ZERO;
    UINT4               u4ARHopListIndex = TE_ZERO;
    UINT4               u4ARHopIndex = TE_ONE;
    UINT4               u4TunARHopLabel = TE_ZERO;
    INT4                i4MplsTnlIfIndex = TE_ZERO;
    INT4                i4RetValMplsTunnelRole = TE_ZERO;
    INT4                i4Prot = TE_ZERO;
    INT4                i4ProtectionMethod = TE_ZERO;
    BOOL1               bFoundIngress = FALSE;
    UINT1               u1ProtType = TE_ZERO;
    UINT1               u1Flag = TE_ZERO;
    static UINT1        au1XcIndex[MPLS_INDEX_LENGTH] = { 0 };
    static UINT1        au1InIndex[MPLS_INDEX_LENGTH] = { 0 };
    static UINT1        au1OutIndex[MPLS_INDEX_LENGTH] = { 0 };
    static UINT4        au4XCSegIndex[MPLS_TE_XC_TABLE_OFFSET] = { 0 };
    static UINT1        au1BkpXcIndex[MPLS_INDEX_LENGTH] = { 0 };
    static UINT1        au1BkpInIndex[MPLS_INDEX_LENGTH] = { 0 };
    static UINT1        au1BkpOutIndex[MPLS_INDEX_LENGTH] = { 0 };
    static UINT4        au4BkpXCSegIndex[MPLS_TE_XC_TABLE_OFFSET] = { 0 };
    static UINT1        au1SessionAttributes[CLI_MPLS_TE_SESS_ATTR] = { 0 };
    static UINT1        au1ARHopIpAddr[MPLS_INDEX_LENGTH] = { 0 };

    tSNMP_OID_TYPE      XCSegIndex;
    tSNMP_OCTET_STRING_TYPE InIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutIndex;
    tSNMP_OID_TYPE      BkpXCSegIndex;
    tSNMP_OCTET_STRING_TYPE BkpInIndex;
    tSNMP_OCTET_STRING_TYPE BkpXCIndex;
    tSNMP_OCTET_STRING_TYPE BkpOutIndex;
    tSNMP_OCTET_STRING_TYPE SessionAttributes;
    tSNMP_OCTET_STRING_TYPE ARHopIpAddr;

    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlInfo         *pTeBkpTnlInfo = NULL;
#ifdef CFA_WANTED
    tCfaIfInfo          CfaIfInfo;
#endif
    UINT1               au1Status[2][5] = { {"up"}, {"down"} };

    XCSegIndex.pu4_OidList = au4XCSegIndex;
    XCIndex.pu1_OctetList = au1XcIndex;
    InIndex.pu1_OctetList = au1InIndex;
    OutIndex.pu1_OctetList = au1OutIndex;
    BkpXCSegIndex.pu4_OidList = au4BkpXCSegIndex;
    BkpXCIndex.pu1_OctetList = au1BkpXcIndex;
    BkpInIndex.pu1_OctetList = au1BkpInIndex;
    BkpOutIndex.pu1_OctetList = au1BkpOutIndex;
    SessionAttributes.pu1_OctetList = au1SessionAttributes;
    SessionAttributes.i4_Length = CLI_MPLS_TE_SESS_ATTR;
    ARHopIpAddr.pu1_OctetList = au1ARHopIpAddr;

    do
    {
        /* For FRR all required information for display is available in non-zero
         * instance only. So, skip all instance zero tunnel instances. */
        if (u4MplsTunnelInstance == TE_ZERO)
        {
            continue;
        }

        /*Retriving the tunnel pointer of the specified tunnel indices */
        pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex,
                                      u4MplsTunnelInstance,
                                      u4MplsTunnelIngressLSRId,
                                      u4MplsTunnelEgressLSRId);
        if (pTeTnlInfo == NULL)
        {
            continue;
        }
        /*To retrive session attribuite object */
        if (nmhGetMplsTunnelSessionAttributes (u4MplsTunnelIndex,
                                               u4MplsTunnelInstance,
                                               u4MplsTunnelIngressLSRId,
                                               u4MplsTunnelEgressLSRId,
                                               &SessionAttributes) ==
            SNMP_FAILURE)
        {
            continue;
        }

        /*To retrive Protection method */
        if (nmhGetFsMplsTunnelExtProtectionMethod (u4MplsTunnelIndex,
                                                   u4MplsTunnelInstance,
                                                   u4MplsTunnelIngressLSRId,
                                                   u4MplsTunnelEgressLSRId,
                                                   &i4Prot) == SNMP_FAILURE)
        {
            continue;
        }
        else if (i4Prot != TE_TNL_FRR_FACILITY_METHOD)
        {
            continue;
        }

        /*To retrive the tunnel role */
        if ((nmhGetMplsTunnelRole (u4MplsTunnelIndex,
                                   u4MplsTunnelInstance,
                                   u4MplsTunnelIngressLSRId,
                                   u4MplsTunnelEgressLSRId,
                                   &i4RetValMplsTunnelRole)) == SNMP_FAILURE)
        {
            continue;
        }
        /*Checking whether the node is head-end of the tunnel */
        if (i4RetValMplsTunnelRole == TE_INGRESS)
        {
            if ((SessionAttributes.
                 pu1_OctetList[TE_ZERO] & TE_SSN_FAST_REROUTE_BIT) !=
                TE_SSN_FAST_REROUTE_BIT)
            {
                continue;
            }

            /* TO Get the protection method enabled on the tunel */
            if ((nmhGetFsMplsFrrConstProtectionMethod (u4MplsTunnelIndex,
                                                       u4MplsTunnelInstance,
                                                       u4MplsTunnelIngressLSRId,
                                                       u4MplsTunnelEgressLSRId,
                                                       &i4ProtectionMethod)) ==
                SNMP_FAILURE)
            {
                continue;
            }
            /*Checking whether the protection method is many-to-one */
            else if (i4ProtectionMethod != TE_TNL_FRR_FACILITY_METHOD)
            {
                continue;

            }
            if (bFoundIngress == FALSE)
            {
                CliPrintf (CliHandle,
                           "\r\nMany to One fast reroute tunnels information \n");
                CliPrintf (CliHandle,
                           "\rReceived RRO (ProtectionFlag 0x01=Available "
                           "0x02=InUse 0x04=B/W 0x08=Node 10=Softpreempt)\n");
                CliPrintf (CliHandle,
                           "\rTunnel head end item frr information: \n");
                CliPrintf (CliHandle,
                           "\rProtected tunnel \t "
                           "In-label \t Out intf/label\t "
                           "   FRR\t        intf/label\tstate/oper\n");
                bFoundIngress = TRUE;
                *bTitleFound = TRUE;
            }
        }
        else
        {
            continue;
        }
        CliPrintf (CliHandle, "\r%s ", TE_TNL_NAME (pTeTnlInfo));
        CliPrintf (CliHandle, "Tun hd\t");
        /*To retrive XCSegIndex for the corresponding tunnel indices */
        if ((nmhGetMplsTunnelXCPointer (u4MplsTunnelIndex,
                                        u4MplsTunnelInstance,
                                        u4MplsTunnelIngressLSRId,
                                        u4MplsTunnelEgressLSRId,
                                        &XCSegIndex)) == SNMP_SUCCESS)
        {
            MplsGetSegFromTunnelXC (&XCSegIndex, &XCIndex, &InIndex,
                                    &OutIndex, &u4XCIndex, &u4InIndex,
                                    &u4OutIndex);
        }
        /* From InIndex and OutIndex, Fetch the InLabel, 
         * InInterface (MplsTnlIf) from InSegmentTable and OutLabel, NextHop
         * from OutSegment Table */
        if (u4InIndex != 0)
        {
            /*To retrive Inlabel */
            if (nmhGetMplsInSegmentLabel (&InIndex, &u4InLabel) == SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, " %d\t\t", u4InLabel);
            }
            else
            {
                CliPrintf (CliHandle, "    -\t\t");
            }
        }
        else
        {
            CliPrintf (CliHandle, "    -\t\t");
        }
        if (u4OutIndex == 0)
        {
            continue;
        }
        /*To retrive Outlabel */
        if (nmhGetMplsOutSegmentTopLabel (&OutIndex, &u4OutLabel) ==
            SNMP_FAILURE)
        {
            continue;
        }
        if (nmhGetMplsOutSegmentInterface (&OutIndex, &i4MplsTnlIfIndex)
            == SNMP_FAILURE)
        {
            continue;
        }
#ifdef CFA_WANTED
        if (MplsGetL3Intf ((UINT4) i4MplsTnlIfIndex, &u4OutL3Intf) ==
            MPLS_FAILURE)
        {
            continue;
        }
        if (CfaGetIfInfo (u4OutL3Intf, &CfaIfInfo) != CFA_FAILURE)
        {
            CliPrintf (CliHandle, "  %s/%d\t", CfaIfInfo.au1IfName, u4OutLabel);
        }
        else
        {
            CliPrintf (CliHandle, "     -/-\t");
        }
#endif /*Retriving the backup indices */
        if ((nmhGetFsMplsTunnelExtBkpTunIdx (u4MplsTunnelIndex,
                                             u4MplsTunnelInstance,
                                             u4MplsTunnelIngressLSRId,
                                             u4MplsTunnelEgressLSRId,
                                             &u4ExtBkpTunIdx) == SNMP_FAILURE)
            ||
            (nmhGetFsMplsTunnelExtBkpInst
             (u4MplsTunnelIndex, u4MplsTunnelInstance, u4MplsTunnelIngressLSRId,
              u4MplsTunnelEgressLSRId, &u4ExtBkpTunInst) == SNMP_FAILURE)
            ||
            (nmhGetFsMplsTunnelExtBkpIngrLSRId
             (u4MplsTunnelIndex, u4MplsTunnelInstance, u4MplsTunnelIngressLSRId,
              u4MplsTunnelEgressLSRId, &u4BkpTunIngrLSRId) == SNMP_FAILURE)
            ||
            (nmhGetFsMplsTunnelExtBkpEgrLSRId
             (u4MplsTunnelIndex, u4MplsTunnelInstance, u4MplsTunnelIngressLSRId,
              u4MplsTunnelEgressLSRId, &u4BkpTunEgrLSRId) == SNMP_FAILURE))
        {
            CliPrintf (CliHandle, "       --   \t");
        }
        /*Retriving the backup tunnel name */
        /*if (nmhGetMplsTunnelName (u4ExtBkpTunIdx,
           u4ExtBkpTunInst,
           u4BkpTunIngrLSRId,
           u4BkpTunEgrLSRId,
           &MplsBkpTunnelName) == SNMP_SUCCESS)
           {
           CliPrintf (CliHandle, "%s",MplsBkpTunnelName.pu1_OctetList);
           } */
        pTeBkpTnlInfo = TeGetTunnelInfo (u4ExtBkpTunIdx,
                                         u4ExtBkpTunInst,
                                         u4BkpTunIngrLSRId, u4BkpTunEgrLSRId);
        if (pTeBkpTnlInfo != NULL)
        {
            if (pTeBkpTnlInfo != NULL)
            {
                CliPrintf (CliHandle, "%s", TE_TNL_NAME (pTeBkpTnlInfo));
            }
        }

        else
        {
            CliPrintf (CliHandle, "     --    ");
        }
        /*To retrive XCSegIndex for the corresponding bachup tunnel indices */
        if ((nmhGetMplsTunnelXCPointer (u4ExtBkpTunIdx,
                                        u4ExtBkpTunInst,
                                        u4BkpTunIngrLSRId,
                                        u4BkpTunEgrLSRId,
                                        &BkpXCSegIndex)) == SNMP_SUCCESS)
        {
            MplsGetSegFromTunnelXC (&BkpXCSegIndex, &BkpXCIndex, &BkpInIndex,
                                    &BkpOutIndex, &u4BkpXCIndex, &u4BkpInIndex,
                                    &u4BkpOutIndex);
            /* From InIndex and OutIndex, Fetch the InLabel, 
             * InInterface (MplsTnlIf) from InSegmentTable and OutLabel, NextHop
             * from OutSegment Table */
            if (u4BkpOutIndex == 0)
            {
                continue;
            }
            /*To retrive Outlabel */
            if (nmhGetMplsOutSegmentTopLabel (&BkpOutIndex, &u4BkpOutLabel) ==
                SNMP_FAILURE)
            {
                continue;
            }
            if (nmhGetMplsOutSegmentInterface (&BkpOutIndex, &i4MplsTnlIfIndex)
                == SNMP_FAILURE)
            {
                continue;
            }
#ifdef CFA_WANTED
            if (MplsGetL3Intf ((UINT4) i4MplsTnlIfIndex, &u4BkpOutL3Intf)
                == MPLS_FAILURE)
            {
                continue;
            }
            if (CfaGetIfInfo (u4BkpOutL3Intf, &CfaIfInfo) == CFA_SUCCESS)
            {
                CliPrintf (CliHandle, "   %s/%d ",
                           CfaIfInfo.au1IfName, u4BkpOutLabel);
            }

#endif
        }
        else
        {
            CliPrintf (CliHandle, "\t\t");
        }
        CliPrintf (CliHandle, "\t    %s/%s\n",
                   au1Status[TE_TNL_ADMIN_STATUS (pTeTnlInfo) - 1],
                   au1Status[TE_TNL_OPER_STATUS (pTeTnlInfo) - 1]);

        /*To retrive ARHop Table Index */
        if (nmhGetMplsTunnelARHopTableIndex (u4MplsTunnelIndex,
                                             u4MplsTunnelInstance,
                                             u4MplsTunnelIngressLSRId,
                                             u4MplsTunnelEgressLSRId,
                                             &u4ARHopListIndex) == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\rRecord Route : ");
            u4ARHopIndex = TE_ONE;
        }
        do
        {
            if (u4ARHopListIndex == 0 || u4ARHopIndex == 0)
            {
                break;
            }
            /*To retrive ARHop Ip address */
            if (nmhGetMplsTunnelARHopIpAddr (u4ARHopListIndex,
                                             u4ARHopIndex,
                                             &ARHopIpAddr) == SNMP_FAILURE)
            {
                u4ARHopIndex++;
                continue;
            }
            /*To retrive ARHop Label */
            if ((nmhGetGmplsTunnelARHopExplicitForwardLabel (u4ARHopListIndex,
                                                             u4ARHopIndex,
                                                             &u4TunARHopLabel)
                 == SNMP_FAILURE))
            {
                u4ARHopIndex++;
                continue;
            }

            if (TE_TNL_FRR_CONST_INFO (pTeTnlInfo) != NULL)
            {
                u1ProtType =
                    TE_TNL_FRR_CONST_PROT_TYPE (TE_TNL_FRR_CONST_INFO
                                                (pTeTnlInfo));
            }

            CliPrintf (CliHandle, " %d.%d.%d.%d",
                       ARHopIpAddr.pu1_OctetList[0],
                       ARHopIpAddr.pu1_OctetList[1],
                       ARHopIpAddr.pu1_OctetList[2],
                       ARHopIpAddr.pu1_OctetList[3]);

            CliPrintf (CliHandle, " (label=%d)", u4TunARHopLabel);

            if (pTeTnlInfo->u1TnlLocalProtectInUse == LOCAL_PROT_AVAIL)
            {
                u1Flag = 0x01;

                if (u1ProtType == TE_TNL_FRR_PROTECTION_NODE)
                {
                    u1Flag |= 0x08;
                }
            }
            else if (pTeTnlInfo->u1TnlLocalProtectInUse == LOCAL_PROT_IN_USE)
            {
                u1Flag = 0x02;

                if (u1ProtType == TE_TNL_FRR_PROTECTION_NODE)
                {
                    u1Flag |= 0x08;
                }
            }

            CliPrintf (CliHandle, "(flag=0x%x)", u1Flag);
            u4ARHopIndex++;
        }                        /*To retrive ARHop table Next indices */
        while (u4ARHopIndex <= MAX_TE_ARHOP_PER_LIST);
        CliPrintf (CliHandle, "\r\n");
    }
    /*Retriving the next tunnel */
    while ((nmhGetNextIndexMplsTunnelTable (u4MplsTunnelIndex,
                                            &u4MplsTunnelIndex,
                                            u4MplsTunnelInstance,
                                            &u4MplsTunnelInstance,
                                            u4MplsTunnelIngressLSRId,
                                            &u4MplsTunnelIngressLSRId,
                                            u4MplsTunnelEgressLSRId,
                                            &u4MplsTunnelEgressLSRId)) ==
           SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliFrrShowIntermediateMany2One 
* Description   : This routine is used show Many2One database at intermediate 
*                 LSR's.
* Input(s)      : CliHandle                - Cli Context Handle.
*                 u4MplsTunnelIndex        - Tunnel index.
*                 u4MplsTunnelInstance     - Tunnel instance.
*                 u4MplsTunnelIngressLSRId - Ingresss Id.
*                 u4MplsTunnelEgressLSRId  - Egress Id.
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliFrrShowIntermediateMany2One (tCliHandle CliHandle,
                                  UINT4 u4MplsTunnelIndex,
                                  UINT4 u4MplsTunnelInstance,
                                  UINT4 u4MplsTunnelIngressLSRId,
                                  UINT4 u4MplsTunnelEgressLSRId,
                                  BOOL1 bTitleFound)
{
    UINT4               u4XCIndex = TE_ZERO;
    UINT4               u4InIndex = TE_ZERO;
    UINT4               u4OutIndex = TE_ZERO;
    UINT4               u4InLabel = TE_ZERO;
    UINT4               u4OutLabel = TE_ZERO;
#ifdef CFA_WANTED
    UINT4               u4OutL3Intf = TE_ZERO;
    UINT4               u4BkpOutL3Intf = TE_ZERO;
#endif
    UINT4               u4BkpXCIndex = TE_ZERO;
    UINT4               u4BkpInIndex = TE_ZERO;
    UINT4               u4BkpOutIndex = TE_ZERO;
    UINT4               u4BkpOutLabel = TE_ZERO;
    UINT4               u4ExtBkpTunIdx = TE_ZERO;
    UINT4               u4ExtBkpTunInst = TE_ZERO;
    UINT4               u4BkpTunIngrLSRId = TE_ZERO;
    UINT4               u4BkpTunEgrLSRId = TE_ZERO;
    UINT4               u4ARHopListIndex = TE_ZERO;
    UINT4               u4ARHopIndex = TE_ONE;
    UINT4               u4TunARHopLabel = TE_ZERO;
    INT4                i4MplsTnlIfIndex = TE_ZERO;
    INT4                i4RetValMplsTunnelRole = TE_ZERO;
    INT4                i4Prot = TE_ZERO;
    /*INT4                i4InterDBProtTunStatus = TE_ZERO; */
    BOOL1               bFoundIntermediate = FALSE;
    static UINT1        au1InIndex[MPLS_INDEX_LENGTH] = { 0 };
    static UINT1        au1OutIndex[MPLS_INDEX_LENGTH] = { 0 };
    static UINT4        au4XCSegIndex[MPLS_TE_XC_TABLE_OFFSET] = { 0 };
    static UINT1        au1BkpXcIndex[MPLS_INDEX_LENGTH] = { 0 };
    static UINT1        au1BkpInIndex[MPLS_INDEX_LENGTH] = { 0 };
    static UINT1        au1BkpOutIndex[MPLS_INDEX_LENGTH] = { 0 };
    static UINT4        au4BkpXCSegIndex[MPLS_TE_XC_TABLE_OFFSET] = { 0 };
    static UINT1        au1SessionAttributes[CLI_MPLS_TE_SESS_ATTR] = { 0 };
    static UINT1        au1ARHopIpAddr[MPLS_INDEX_LENGTH] = { 0 };
    static UINT1        au1ARHopProtectType[TE_ONE] = { 0 };
    static UINT1        au1ARHopProtectTypeInUse[TE_ONE] = { 0 };

    tSNMP_OID_TYPE      XCSegIndex;
    tSNMP_OCTET_STRING_TYPE InIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutIndex;
    tSNMP_OID_TYPE      BkpXCSegIndex;
    tSNMP_OCTET_STRING_TYPE BkpInIndex;
    tSNMP_OCTET_STRING_TYPE BkpXCIndex;
    tSNMP_OCTET_STRING_TYPE BkpOutIndex;
    tSNMP_OCTET_STRING_TYPE SessionAttributes;
    tSNMP_OCTET_STRING_TYPE ARHopIpAddr;
    tSNMP_OCTET_STRING_TYPE ARHopProtectType;
    tSNMP_OCTET_STRING_TYPE ARHopProtectTypeInUse;

    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlInfo         *pTeBkpTnlInfo = NULL;
#ifdef CFA_WANTED
    tCfaIfInfo          CfaIfInfo;
#endif
    UINT1               au1Status[2][5] = { {"up"}, {"down"} };

    XCSegIndex.pu4_OidList = au4XCSegIndex;
    InIndex.pu1_OctetList = au1InIndex;
    OutIndex.pu1_OctetList = au1OutIndex;
    BkpXCSegIndex.pu4_OidList = au4BkpXCSegIndex;
    BkpXCIndex.pu1_OctetList = au1BkpXcIndex;
    BkpInIndex.pu1_OctetList = au1BkpInIndex;
    BkpOutIndex.pu1_OctetList = au1BkpOutIndex;
    SessionAttributes.pu1_OctetList = au1SessionAttributes;
    SessionAttributes.i4_Length = CLI_MPLS_TE_SESS_ATTR;
    ARHopIpAddr.pu1_OctetList = au1ARHopIpAddr;
    ARHopProtectType.pu1_OctetList = au1ARHopProtectType;
    ARHopProtectTypeInUse.pu1_OctetList = au1ARHopProtectTypeInUse;

    do
    {
        /* For FRR all required information for display is available in non-zero
         * instance only. So, skip all instance zero tunnel instances. */
        if (u4MplsTunnelInstance == TE_ZERO)
        {
            continue;
        }

        pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex,
                                      u4MplsTunnelInstance,
                                      u4MplsTunnelIngressLSRId,
                                      u4MplsTunnelEgressLSRId);
        if (pTeTnlInfo == NULL)
        {
            continue;
        }

        /*To retrive session attribuite object */
        if (nmhGetMplsTunnelSessionAttributes (u4MplsTunnelIndex,
                                               u4MplsTunnelInstance,
                                               u4MplsTunnelIngressLSRId,
                                               u4MplsTunnelEgressLSRId,
                                               &SessionAttributes) ==
            SNMP_FAILURE)
        {
            continue;
        }
        /*To retrive Protection method */
        if (nmhGetFsMplsTunnelExtProtectionMethod (u4MplsTunnelIndex,
                                                   u4MplsTunnelInstance,
                                                   u4MplsTunnelIngressLSRId,
                                                   u4MplsTunnelEgressLSRId,
                                                   &i4Prot) == SNMP_FAILURE)
        {
            continue;
        }
        else if (i4Prot != TE_TNL_FRR_FACILITY_METHOD)
        {
            continue;
        }

        /*To retrive the tunnel role */
        if ((nmhGetMplsTunnelRole (u4MplsTunnelIndex,
                                   u4MplsTunnelInstance,
                                   u4MplsTunnelIngressLSRId,
                                   u4MplsTunnelEgressLSRId,
                                   &i4RetValMplsTunnelRole)) == SNMP_FAILURE)
        {
            continue;
        }
        /*Checking whether the node is head-end of the tunnel */
        if (i4RetValMplsTunnelRole == TE_INTERMEDIATE)
        {
            /*Checking whether fast-reroute bit is set in the session-attribute obj */
            if ((SessionAttributes.pu1_OctetList[TE_ZERO] &
                 TE_FRR_SSN_ATTR_LOCAL_PROT_FLAG) !=
                TE_FRR_SSN_ATTR_LOCAL_PROT_FLAG)
            {
                continue;
            }
            if (bFoundIntermediate == FALSE)
            {
                if (bTitleFound == FALSE)
                {
                    CliPrintf (CliHandle,
                               "\r\nMany to One fast reroute tunnels information \n");
                    CliPrintf (CliHandle,
                               "\rReceived RRO (ProtectionFlag 0x01=Available "
                               "0x02=InUse 0x04=B/W 0x08=Node 10=Softpreempt)\n");
                }
                CliPrintf (CliHandle,
                           "\rLSP midpoint item frr information: \n");
                CliPrintf (CliHandle,
                           "\rLSP midpoint \t " "In-label \t Out intf/label\t "
                           "\tFRR\t\tintf/label\tstate/oper\n");
                bFoundIntermediate = TRUE;
            }
        }
        else
        {
            continue;
        }
        CliPrintf (CliHandle, "\r%s ", TE_TNL_NAME (pTeTnlInfo));
        /*To retrive XCSegIndex for the corresponding tunnel indices */
        if ((nmhGetMplsTunnelXCPointer (u4MplsTunnelIndex,
                                        u4MplsTunnelInstance,
                                        u4MplsTunnelIngressLSRId,
                                        u4MplsTunnelEgressLSRId,
                                        &XCSegIndex)) == SNMP_SUCCESS)
        {
            MplsGetSegFromTunnelXC (&XCSegIndex, &XCIndex, &InIndex,
                                    &OutIndex, &u4XCIndex, &u4InIndex,
                                    &u4OutIndex);
        }
        /* From InIndex and OutIndex, Fetch the InLabel, 
         * InInterface (MplsTnlIf) from InSegmentTable and OutLabel, NextHop
         * from OutSegment Table */
        if (u4InIndex != 0)
        {
            /*To retrive Inlabel */
            if (nmhGetMplsInSegmentLabel (&InIndex, &u4InLabel) == SNMP_SUCCESS)
            {
                CliPrintf (CliHandle, "\t  %d\t    ", u4InLabel);
            }
            else
            {
                CliPrintf (CliHandle, "   -\t\t");
            }
        }
        else
        {
            CliPrintf (CliHandle, "   -\t\t");
        }
        if (u4OutIndex == 0)
        {
            continue;
        }
        /*To retrive Outlabel */
        if (nmhGetMplsOutSegmentTopLabel (&OutIndex, &u4OutLabel) ==
            SNMP_FAILURE)
        {
            continue;
        }
        if (nmhGetMplsOutSegmentInterface (&OutIndex, &i4MplsTnlIfIndex)
            == SNMP_FAILURE)
        {
            continue;
        }
#ifdef CFA_WANTED
        if (MplsGetL3Intf ((UINT4) i4MplsTnlIfIndex, &u4OutL3Intf) ==
            MPLS_FAILURE)
        {
            continue;
        }
        if (CfaGetIfInfo (u4OutL3Intf, &CfaIfInfo) != CFA_FAILURE)
        {
            CliPrintf (CliHandle, "%s/%d     ", CfaIfInfo.au1IfName,
                       u4OutLabel);
        }
        else
        {
            CliPrintf (CliHandle, "-/- \t");
        }
#endif /*Retriving the backup indices */
        if ((nmhGetFsMplsTunnelExtBkpTunIdx (u4MplsTunnelIndex,
                                             u4MplsTunnelInstance,
                                             u4MplsTunnelIngressLSRId,
                                             u4MplsTunnelEgressLSRId,
                                             &u4ExtBkpTunIdx) == SNMP_FAILURE)
            ||
            (nmhGetFsMplsTunnelExtBkpInst
             (u4MplsTunnelIndex, u4MplsTunnelInstance, u4MplsTunnelIngressLSRId,
              u4MplsTunnelEgressLSRId, &u4ExtBkpTunInst) == SNMP_FAILURE)
            ||
            (nmhGetFsMplsTunnelExtBkpIngrLSRId
             (u4MplsTunnelIndex, u4MplsTunnelInstance, u4MplsTunnelIngressLSRId,
              u4MplsTunnelEgressLSRId, &u4BkpTunIngrLSRId) == SNMP_FAILURE)
            ||
            (nmhGetFsMplsTunnelExtBkpEgrLSRId
             (u4MplsTunnelIndex, u4MplsTunnelInstance, u4MplsTunnelIngressLSRId,
              u4MplsTunnelEgressLSRId, &u4BkpTunEgrLSRId) == SNMP_FAILURE))
        {
            CliPrintf (CliHandle, "       --   \t");
        }
        /*Retriving the backup tunnel name */
        /*if (nmhGetMplsTunnelName (u4ExtBkpTunIdx,
           u4ExtBkpTunInst,
           u4BkpTunIngrLSRId,
           u4BkpTunEgrLSRId,
           &MplsBkpTunnelName) == SNMP_SUCCESS)
           {
           CliPrintf (CliHandle, "%s", MplsBkpTunnelName.pu1_OctetList);
           } */
        pTeBkpTnlInfo = TeGetTunnelInfo (u4ExtBkpTunIdx,
                                         u4ExtBkpTunInst,
                                         u4BkpTunIngrLSRId, u4BkpTunEgrLSRId);
        if (pTeBkpTnlInfo != NULL)
        {
            if (pTeBkpTnlInfo != NULL)
            {
                CliPrintf (CliHandle, "%s", TE_TNL_NAME (pTeBkpTnlInfo));
            }
        }
        else
        {
            CliPrintf (CliHandle, "     --    ");
        }
        /*To retrive XCSegIndex for the corresponding bachup tunnel indices */
        if ((nmhGetMplsTunnelXCPointer (u4ExtBkpTunIdx,
                                        u4ExtBkpTunInst,
                                        u4BkpTunIngrLSRId,
                                        u4BkpTunEgrLSRId,
                                        &BkpXCSegIndex)) == SNMP_SUCCESS)
        {
            MplsGetSegFromTunnelXC (&BkpXCSegIndex, &BkpXCIndex, &BkpInIndex,
                                    &BkpOutIndex, &u4BkpXCIndex, &u4BkpInIndex,
                                    &u4BkpOutIndex);
            /* From InIndex and OutIndex, Fetch the InLabel, 
             * InInterface (MplsTnlIf) from InSegmentTable and OutLabel, NextHop
             * from OutSegment Table */
            if (u4BkpOutIndex == 0)
            {
                continue;
            }
            /*To retrive Outlabel */
            if (nmhGetMplsOutSegmentTopLabel (&BkpOutIndex, &u4BkpOutLabel) ==
                SNMP_FAILURE)
            {
                continue;
            }
            if (nmhGetMplsOutSegmentInterface (&BkpOutIndex, &i4MplsTnlIfIndex)
                == SNMP_FAILURE)
            {
                continue;
            }
#ifdef CFA_WANTED
            if (MplsGetL3Intf
                ((UINT4) i4MplsTnlIfIndex, &u4BkpOutL3Intf) == MPLS_FAILURE)
            {
                continue;
            }
            if (CfaGetIfInfo (u4BkpOutL3Intf, &CfaIfInfo) == CFA_SUCCESS)
            {
                CliPrintf (CliHandle, "   %s/%d ",
                           CfaIfInfo.au1IfName, u4BkpOutLabel);
            }

#endif
        }
        else
        {
            CliPrintf (CliHandle, "\t\t\t");
        }
        CliPrintf (CliHandle, "     %s/%s\n",
                   au1Status[TE_TNL_ADMIN_STATUS (pTeTnlInfo) - 1],
                   au1Status[TE_TNL_OPER_STATUS (pTeTnlInfo) - 1]);

        /*To retrive ARHop Table Index */
        if (nmhGetMplsTunnelARHopTableIndex (u4MplsTunnelIndex,
                                             u4MplsTunnelInstance,
                                             u4MplsTunnelIngressLSRId,
                                             u4MplsTunnelEgressLSRId,
                                             &u4ARHopListIndex) == SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\rRecord Route : ");
            u4ARHopIndex = TE_ONE;
        }
        do
        {
            if (u4ARHopListIndex == 0 || u4ARHopIndex == 0)
            {
                break;
            }
            /*To retrive ARHop Ip address */
            if (nmhGetMplsTunnelARHopIpAddr (u4ARHopListIndex,
                                             u4ARHopIndex,
                                             &ARHopIpAddr) == SNMP_FAILURE)
            {
                u4ARHopIndex++;
                continue;
            }

            /*To retrive ARHop protection type */
            if ((nmhGetFsMplsFrrTunARHopProtectType (u4ARHopListIndex,
                                                     u4ARHopIndex,
                                                     &ARHopProtectType) ==
                 SNMP_FAILURE))
            {
                u4ARHopIndex++;
                continue;
            }
            /*To retrive ARHop protection Inuse */
            if ((nmhGetFsMplsFrrTunARHopProtectTypeInUse (u4ARHopListIndex,
                                                          u4ARHopIndex,
                                                          &ARHopProtectTypeInUse)
                 == SNMP_FAILURE))
            {
                u4ARHopIndex++;
                continue;
            }
            /*To retrive ARHop Label */
            if ((nmhGetFsMplsFrrTunARHopLabel (u4ARHopListIndex,
                                               u4ARHopIndex,
                                               &u4TunARHopLabel)
                 == SNMP_FAILURE))
            {
                u4ARHopIndex++;
                continue;
            }

            CliPrintf (CliHandle, " %d.%d.%d.%d",
                       ARHopIpAddr.pu1_OctetList[0],
                       ARHopIpAddr.pu1_OctetList[1],
                       ARHopIpAddr.pu1_OctetList[2],
                       ARHopIpAddr.pu1_OctetList[3]);

            if ((ARHopProtectType.pu1_OctetList[0] ==
                 TE_TNL_FRR_PROTECTION_LINK)
                && (ARHopProtectTypeInUse.pu1_OctetList[0] !=
                    TE_TNL_FRR_PROTECTION_LINK))

            {
                CliPrintf (CliHandle, "(flag=0x1,Label=%d) ", u4TunARHopLabel);
            }
            else if ((ARHopProtectType.pu1_OctetList[0] ==
                      TE_TNL_FRR_PROTECTION_LINK)
                     && (ARHopProtectTypeInUse.pu1_OctetList[0] ==
                         TE_TNL_FRR_PROTECTION_LINK))

            {
                CliPrintf (CliHandle, "(flag=0x3,Label=%d) ", u4TunARHopLabel);
            }
            else if ((ARHopProtectType.pu1_OctetList[0] ==
                      TE_TNL_FRR_PROTECTION_NODE)
                     && (ARHopProtectTypeInUse.pu1_OctetList[0] !=
                         TE_TNL_FRR_PROTECTION_NODE))

            {
                CliPrintf (CliHandle, "(flag=0x9,Label=%d) ", u4TunARHopLabel);
            }
            else if ((ARHopProtectType.pu1_OctetList[0] ==
                      TE_TNL_FRR_PROTECTION_NODE)
                     && (ARHopProtectTypeInUse.pu1_OctetList[0] ==
                         TE_TNL_FRR_PROTECTION_NODE))

            {
                CliPrintf (CliHandle, "(flag=0x10,Label=%d) ", u4TunARHopLabel);
            }
            else
            {
                CliPrintf (CliHandle, "(flag=0,Label=%d) ", u4TunARHopLabel);
            }
            u4ARHopIndex++;
        }                        /*To retrive ARHop table Next indices */
        while (u4ARHopIndex <= MAX_TE_ARHOP_PER_LIST);
        CliPrintf (CliHandle, "\r\n");
    }
    /*Retriving the next tunnel */
    while ((nmhGetNextIndexMplsTunnelTable (u4MplsTunnelIndex,
                                            &u4MplsTunnelIndex,
                                            u4MplsTunnelInstance,
                                            &u4MplsTunnelInstance,
                                            u4MplsTunnelIngressLSRId,
                                            &u4MplsTunnelIngressLSRId,
                                            u4MplsTunnelEgressLSRId,
                                            &u4MplsTunnelEgressLSRId)) ==
           SNMP_SUCCESS);

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliFrrShowAllFrrDatabase  
* Description   : This routine is used show FRR database.
* Input(s)      : CliHandle     - Cli Context Handle.
*                 i4FrrDatabase - one-to-one/many-to-one/both.
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliFrrShowAllFrrDatabase (tCliHandle CliHandle,
                            UINT4 u4TunnelIndex,
                            UINT4 u4TunnelInstance,
                            UINT4 u4TunnelIngressLSRId,
                            UINT4 u4TunnelEgressLSRId)
{
    TeCliFrrShowOne2One (CliHandle,
                         u4TunnelIndex,
                         u4TunnelInstance,
                         u4TunnelIngressLSRId, u4TunnelEgressLSRId);

    TeCliFrrShowMany2One (CliHandle,
                          u4TunnelIndex,
                          u4TunnelInstance,
                          u4TunnelIngressLSRId, u4TunnelEgressLSRId);
    return CLI_SUCCESS;
}
#endif

/******************************************************************************
* Function Name : TeCliErHopShow  
* Description   : This routine is used show ER-Hop information.
* Input(s)      : CliHandle     - Cli Context Handle.
*                 pTeTnlInfo    - Pointer to the Tunnel Info
*                 b1ConfigFlag  - Config Flag
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
VOID
TeCliErHopShow (tCliHandle CliHandle, tTeTnlInfo * pTeTnlInfo,
                BOOL1 b1ConfigFlag)
{
    UINT4               u4HopListIndex = TE_ZERO;
    UINT4               u4PathIndex = TE_ZERO;
    UINT4               u4HopIndex = TE_ZERO;
    UINT4               u4CHopListIndex = TE_ZERO;
    UINT4               u4TunnelIndex = TE_ZERO;
    UINT4               u4TunnelInstance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    INT4                i4HopType = TE_ZERO;
    INT4                i4IncludeExclude = TE_ZERO;
    INT4                i4IncludeAny = TE_SNMP_FALSE;
    UINT4               u4FwdLbl = MPLS_INVALID_LABEL;
    UINT4               u4RevLbl = MPLS_INVALID_LABEL;
    BOOL1               bFlag = TE_FALSE;
    UINT4               u4Addr = TE_ZERO;
    UINT4               u4HopTblIndex = TE_ZERO;
    UINT4               u4TempPathIndex = TE_ZERO;
    CHR1               *pIp = NULL;

    UINT1               au1HopIpAddr[MPLS_INDEX_LENGTH];
    tSNMP_OCTET_STRING_TYPE HopIpAddr;

    MEMSET (au1HopIpAddr, TE_ZERO, MPLS_INDEX_LENGTH);
    HopIpAddr.pu1_OctetList = au1HopIpAddr;

    if (pTeTnlInfo != NULL)
    {
        u4TunnelIndex = TE_TNL_TNL_INDEX (pTeTnlInfo);
        u4TunnelInstance = TE_TNL_TNL_INSTANCE (pTeTnlInfo);

        CONVERT_TO_INTEGER (pTeTnlInfo->TnlIngressLsrId, u4IngressId);
        u4IngressId = OSIX_NTOHL (u4IngressId);

        CONVERT_TO_INTEGER (pTeTnlInfo->TnlEgressLsrId, u4EgressId);
        u4EgressId = OSIX_NTOHL (u4EgressId);
    }

    /*To retrive Hop table index */
    nmhGetMplsTunnelHopTableIndex (u4TunnelIndex, u4TunnelInstance,
                                   u4IngressId, u4EgressId, &u4HopListIndex);
    nmhGetMplsTunnelPathInUse (u4TunnelIndex, u4TunnelInstance,
                               u4IngressId, u4EgressId, &u4PathIndex);
    nmhGetMplsTunnelCHopTableIndex (u4TunnelIndex, u4TunnelInstance,
                                    u4IngressId, u4EgressId, &u4CHopListIndex);

    if ((u4CHopListIndex != TE_ZERO) && (b1ConfigFlag == FALSE))
    {
        u4HopTblIndex = u4CHopListIndex;
        CliPrintf (CliHandle, "\r   RSVP Path Info: \n");

        while (nmhGetNextIndexMplsTunnelCHopTable (u4CHopListIndex,
                                                   &u4CHopListIndex,
                                                   u4HopIndex, &u4HopIndex)
               == SNMP_SUCCESS)
        {
            if (u4HopTblIndex != u4CHopListIndex)
            {
                break;
            }

            if (bFlag == TE_FALSE)
            {
                CliPrintf (CliHandle,
                           "\r     Explicit Route     Forward Label "
                           "Reverse Label\n");
                CliPrintf (CliHandle,
                           "\r     --------------     ------------- "
                           "-------------\n");

                bFlag = TE_TRUE;
            }

            nmhGetMplsTunnelCHopIpAddr (u4CHopListIndex, u4HopIndex,
                                        &HopIpAddr);
            nmhGetMplsTunnelCHopType (u4CHopListIndex, u4HopIndex, &i4HopType);
            nmhGetGmplsTunnelCHopExplicitForwardLabel (u4CHopListIndex,
                                                       u4HopIndex, &u4FwdLbl);
            nmhGetGmplsTunnelCHopExplicitReverseLabel (u4CHopListIndex,
                                                       u4HopIndex, &u4RevLbl);

            MPLS_OCTETSTRING_TO_INTEGER ((&HopIpAddr), u4Addr);
            CLI_CONVERT_IPADDR_TO_STR (pIp, u4Addr);
            CliPrintf (CliHandle, "\r     %-15s", pIp);

            if (i4HopType == TE_STRICT_ER)
            {
                CliPrintf (CliHandle, "(S) ");
            }
            else
            {
                CliPrintf (CliHandle, "(L) ");
            }

            if (u4FwdLbl != MPLS_INVALID_LABEL)
            {
                CliPrintf (CliHandle, "%-6d        ", u4FwdLbl);
            }
            else
            {
                CliPrintf (CliHandle, "      -     ");
            }

            if (u4RevLbl != MPLS_INVALID_LABEL)
            {
                CliPrintf (CliHandle, "%-6d        ", u4RevLbl);
            }
            else
            {
                CliPrintf (CliHandle, "      -     ");
            }

            CliPrintf (CliHandle, "\n");
        }
    }
    else if (u4HopListIndex != TE_ZERO)
    {
        u4HopTblIndex = u4HopListIndex;
        u4TempPathIndex = u4PathIndex;

        if (b1ConfigFlag == FALSE)
        {
            CliPrintf (CliHandle, "\r   RSVP Path Info: \n");
        }

        while (nmhGetNextIndexMplsTunnelHopTable (u4HopListIndex,
                                                  &u4HopListIndex,
                                                  u4PathIndex, &u4PathIndex,
                                                  u4HopIndex, &u4HopIndex)
               == SNMP_SUCCESS)
        {
            if ((u4HopListIndex != u4HopTblIndex) ||
                (u4PathIndex != u4TempPathIndex))
            {
                break;
            }

            if (bFlag == TE_FALSE)
            {
                CliPrintf (CliHandle,
                           "\r     Explicit Route     Forward Label "
                           "Reverse Label\n");
                CliPrintf (CliHandle,
                           "\r     --------------     ------------- "
                           "-------------\n");

                bFlag = TE_TRUE;
            }

            nmhGetMplsTunnelHopIpAddr (u4HopListIndex, u4PathIndex, u4HopIndex,
                                       &HopIpAddr);
            nmhGetMplsTunnelHopType (u4HopListIndex, u4PathIndex, u4HopIndex,
                                     &i4HopType);
            nmhGetFsMplsTunnelHopIncludeAny (u4HopListIndex, u4PathIndex,
                                             u4HopIndex, &i4IncludeAny);
            nmhGetMplsTunnelHopInclude (u4HopListIndex, u4PathIndex, u4HopIndex,
                                        &i4IncludeExclude);
            nmhGetGmplsTunnelHopExplicitForwardLabel (u4HopListIndex,
                                                      u4PathIndex, u4HopIndex,
                                                      &u4FwdLbl);
            nmhGetGmplsTunnelHopExplicitReverseLabel (u4HopListIndex,
                                                      u4PathIndex, u4HopIndex,
                                                      &u4RevLbl);

            MPLS_OCTETSTRING_TO_INTEGER ((&HopIpAddr), u4Addr);
            CLI_CONVERT_IPADDR_TO_STR (pIp, u4Addr);
            CliPrintf (CliHandle, "\r     %-15s", pIp);

            if (i4IncludeAny == TE_SNMP_TRUE)
            {
                CliPrintf (CliHandle, "(I) ");
            }
            else if (i4IncludeExclude == TE_SNMP_FALSE)
            {
                CliPrintf (CliHandle, "(E) ");
            }
            else if (i4HopType == TE_STRICT_ER)
            {
                CliPrintf (CliHandle, "(S) ");
            }
            else
            {
                CliPrintf (CliHandle, "(L) ");
            }

            if (u4FwdLbl != MPLS_INVALID_LABEL)
            {
                CliPrintf (CliHandle, "%6d        ", u4FwdLbl);
            }
            else
            {
                CliPrintf (CliHandle, "      -     ");
            }

            if (u4RevLbl != MPLS_INVALID_LABEL)
            {
                CliPrintf (CliHandle, "%6d        ", u4RevLbl);
            }
            else
            {
                CliPrintf (CliHandle, "      -     ");
            }

            CliPrintf (CliHandle, "\n");
        }
    }

    return;
}

/******************************************************************************
* Function Name : TeCliRrHopShow
* Description   : This routine is used show AR-Hop information.
* Input(s)      : CliHandle     - Cli Context Handle.
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
VOID
TeCliRrHopShow (tCliHandle CliHandle, tTeTnlInfo * pTeTnlInfo)
{
    UINT4               u4HopListIndex = TE_ZERO;
    UINT4               u4HopIndex = TE_ZERO;
    UINT4               u4TunnelIndex = TE_ZERO;
    UINT4               u4TunnelInstance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    UINT4               u4FwdLbl = MPLS_INVALID_LABEL;
    UINT4               u4RevLbl = MPLS_INVALID_LABEL;
    UINT4               u4HopTblIndex = TE_ZERO;
    BOOL1               bFlag = TE_FALSE;
    UINT4               u4Addr = TE_ZERO;
    CHR1               *pIp = NULL;

    UINT1               au1HopIpAddr[MPLS_INDEX_LENGTH];
    tSNMP_OCTET_STRING_TYPE HopIpAddr;

    MEMSET (au1HopIpAddr, TE_ZERO, MPLS_INDEX_LENGTH);
    HopIpAddr.pu1_OctetList = au1HopIpAddr;

    if (pTeTnlInfo != NULL)
    {
        u4TunnelIndex = TE_TNL_TNL_INDEX (pTeTnlInfo);
        u4TunnelInstance = TE_TNL_TNL_INSTANCE (pTeTnlInfo);

        CONVERT_TO_INTEGER (pTeTnlInfo->TnlIngressLsrId, u4IngressId);
        u4IngressId = OSIX_NTOHL (u4IngressId);

        CONVERT_TO_INTEGER (pTeTnlInfo->TnlEgressLsrId, u4EgressId);
        u4EgressId = OSIX_NTOHL (u4EgressId);
    }

    /*To retrive Hop table index */
    if (nmhGetMplsTunnelARHopTableIndex (u4TunnelIndex, u4TunnelInstance,
                                         u4IngressId, u4EgressId,
                                         &u4HopTblIndex) == SNMP_FAILURE)
    {
        return;
    }

    if (u4HopTblIndex == TE_ZERO)
    {
        return;
    }

    CliPrintf (CliHandle, "\r   RSVP Resv Info: \n");
    if (nmhGetFirstIndexMplsTunnelARHopTable (&u4HopListIndex,
                                              &u4HopIndex) == SNMP_FAILURE)
    {
        return;
    }
    do
    {
        if (u4HopTblIndex == u4HopListIndex)
        {

            if (bFlag == TE_FALSE)
            {
                CliPrintf (CliHandle,
                           "\r     Record Route    Forward Label "
                           "Reverse Label\n");
                CliPrintf (CliHandle,
                           "\r     ------------    ------------- "
                           "-------------\n");

                bFlag = TE_TRUE;
            }

            nmhGetGmplsTunnelARHopExplicitForwardLabel (u4HopListIndex,
                                                        u4HopIndex, &u4FwdLbl);
            nmhGetGmplsTunnelARHopExplicitReverseLabel (u4HopListIndex,
                                                        u4HopIndex, &u4RevLbl);
            if (nmhGetMplsTunnelARHopIpAddr
                (u4HopListIndex, u4HopIndex, &HopIpAddr) == SNMP_FAILURE)
            {
                return;
            }

            MPLS_OCTETSTRING_TO_INTEGER ((&HopIpAddr), u4Addr);
            CLI_CONVERT_IPADDR_TO_STR (pIp, u4Addr);
            CliPrintf (CliHandle, "\r     %-15s ", pIp);

            if (u4FwdLbl != MPLS_INVALID_LABEL)
            {
                CliPrintf (CliHandle, "%6d        ", u4FwdLbl);
            }
            else
            {
                CliPrintf (CliHandle, "      -     ");
            }

            if (u4RevLbl != MPLS_INVALID_LABEL)
            {
                CliPrintf (CliHandle, "%6d        ", u4RevLbl);
            }
            else
            {
                CliPrintf (CliHandle, "      -     ");
            }

            CliPrintf (CliHandle, "\n");
        }
    }
    while (nmhGetNextIndexMplsTunnelARHopTable (u4HopListIndex,
                                                &u4HopListIndex,
                                                u4HopIndex, &u4HopIndex)
           == SNMP_SUCCESS);

    return;
}

/******************************************************************************
* Function Name : TeCliSetMplsTunnelRowStatus
* Description   : This routine is used to set the tunnel passed with the 
*                 indices in NOT_IN_SERVICE state.
* Input(s)      : u4TunnelIndex    - Tunnel Index
*                 u4TunnelInstance - Tunnel Instance
*                 u4IngressId      - Tunnel Source
*                 u4EgressId       - Tunnel Destination
*                 i4RowStatusValue - Tunnel Row status Value
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliSetMplsTunnelRowStatus (UINT4 u4TunnelIndex, UINT4 u4TunnelInstance,
                             UINT4 u4IngressId, UINT4 u4EgressId,
                             INT4 i4RowStatusValue)
{
    UINT4               u4ErrorCode = TE_ZERO;
    /* MPLS_P2MP_LSP_CHANGES - S */
    tSNMP_OCTET_STRING_TYPE MplsTeTnlType;
    static UINT1        au1TnlType[CLI_MPLS_TE_TNL_TYPE];
    INT4                i4TnlRole = TE_ZERO;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCliSetMplsTunnelRowStatus : ENTRY \n");

    MEMSET (au1TnlType, TE_ZERO, CLI_MPLS_TE_TNL_TYPE);
    MplsTeTnlType.pu1_OctetList = au1TnlType;
    MplsTeTnlType.i4_Length = CLI_MPLS_TE_TNL_TYPE;

    if (nmhGetFsMplsTunnelType (u4TunnelIndex, u4TunnelInstance,
                                u4IngressId, u4EgressId, &MplsTeTnlType)
        == SNMP_FAILURE)
    {
        TE_DBG (TE_MAIN_FAIL, "MAIN : Failed to retrieve tunnel type \n");
        return CLI_FAILURE;
    }

    if (nmhGetMplsTunnelRole (u4TunnelIndex, u4TunnelInstance,
                              u4IngressId, u4EgressId,
                              &i4TnlRole) == SNMP_FAILURE)
    {
        TE_DBG (TE_MAIN_FAIL, "MAIN : Failed to retrieve tunnel role \n");
        return CLI_FAILURE;
    }

    if ((TE_TNL_TYPE_P2MP & MplsTeTnlType.pu1_OctetList[TE_ZERO]) &&
        (TE_EGRESS != i4TnlRole))
    {
        if (nmhTestv2MplsTeP2mpTunnelRowStatus (&u4ErrorCode, u4TunnelIndex,
                                                u4TunnelInstance, u4IngressId,
                                                u4EgressId, i4RowStatusValue)
            == SNMP_FAILURE)
        {
            TE_DBG (TE_MAIN_FAIL, "MAIN : Test routine for P2MP tunnel row "
                    "status failed \n");
            return CLI_FAILURE;
        }

        if (nmhSetMplsTeP2mpTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                             u4IngressId, u4EgressId,
                                             i4RowStatusValue) == SNMP_FAILURE)
        {
            TE_DBG (TE_MAIN_FAIL, "MAIN : Failed to set P2MP tunnel row "
                    "status\n");
            return CLI_FAILURE;
        }
        return CLI_SUCCESS;
    }
    /* MPLS_P2MP_LSP_CHANGES - E */

    if (nmhTestv2MplsTunnelRowStatus (&u4ErrorCode, u4TunnelIndex,
                                      u4TunnelInstance, u4IngressId, u4EgressId,
                                      i4RowStatusValue) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                   u4IngressId, u4EgressId,
                                   i4RowStatusValue) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCliSetMplsTunnelRowStatus : EXIT \n");
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliGetLocalMapNum
* Description   : This routine is used to get the local map number from
*                 the global identifier and node identifier value.
* Input(s)      : u4ContextId      - Context Identifier
* Output(s)     : pu4LocalMapNum   - Local Map Number
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliGetLocalMapNum (UINT4 u4ContextId, UINT4 *pu4LocalMapNum)
{
    UINT4               u4GlobalId = 0;
    UINT4               u4NodeId = 0;

    if (nmhGetFsMplsTpGlobalId (u4ContextId, &u4GlobalId) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetFsMplsTpNodeIdentifier (u4ContextId, &u4NodeId) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (OamUtilGetLocalMapNum (u4ContextId, u4GlobalId, u4NodeId,
                               pu4LocalMapNum) == OSIX_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliGetTnlIndices
* Description   : This routine is used to get the correct tunnel indices based
*                 on the CLI mode.
* Input(s)      : CliHandle           - Cli Handle                 
* Output(s)     : pu4TunnelIndex      - Tunnel Index
*                 pu4TunnelInstance   - Tunnel Instance
*                 pu4IngressId        - Tunnel Ingress Id
*                 pu4EgressId         - Tunnel Egress Id
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliGetTnlIndices (tCliHandle CliHandle,
                    UINT4 *pu4TunnelIndex, UINT4 *pu4TunnelInstance,
                    UINT4 *pu4IngressId, UINT4 *pu4EgressId)
{
    UINT4               u4TnlIndex = gaTnlCliArgs[CliHandle].u4TnlIndex;
    UINT4               u4TnlInstance = gaTnlCliArgs[CliHandle].u4TnlInstance;
    UINT4               u4IngressId = gaTnlCliArgs[CliHandle].u4IngressId;
    UINT4               u4EgressId = gaTnlCliArgs[CliHandle].u4EgressId;

    tTeTnlInfo         *pTeTnlInfo = NULL;

    if ((u4IngressId == TE_ZERO) && (u4EgressId == TE_ZERO))
    {
        MPLS_CMN_LOCK ();
        pTeTnlInfo = TeGetTnlInfoByOwnerAndRole (u4TnlIndex, u4TnlInstance,
                                                 u4IngressId, u4EgressId,
                                                 TE_TNL_OWNER_SNMP, TE_ZERO);

        if (pTeTnlInfo == NULL)
        {
            MPLS_CMN_UNLOCK ();

            CliPrintf (CliHandle, "\r\n%%Failed to Get a Tunnel Info\r\n");
            return CLI_FAILURE;
        }

        *pu4TunnelIndex = pTeTnlInfo->u4TnlIndex;
        *pu4TunnelInstance = pTeTnlInfo->u4TnlInstance;

        CONVERT_TO_INTEGER (pTeTnlInfo->TnlIngressLsrId, u4IngressId);
        *pu4IngressId = OSIX_NTOHL (u4IngressId);

        CONVERT_TO_INTEGER (pTeTnlInfo->TnlEgressLsrId, u4EgressId);
        *pu4EgressId = OSIX_NTOHL (u4EgressId);

        MPLS_CMN_UNLOCK ();
    }
    else
    {
        *pu4TunnelIndex = u4TnlIndex;
        *pu4TunnelInstance = u4TnlInstance;
        *pu4IngressId = u4IngressId;
        *pu4EgressId = u4EgressId;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliDeleteXcInAndOutSegment
* Description   : This function is used to delete the XC, InSegment and 
*                 OutSegment Entries.
* Input(s)      : pXCSegmentIndex     - XC Index Octet string
*                 pInSegmentIndex     - InSegment Index Octet string
*                 pOutSegmentIndex    - OutSegment Index Octet string
*                 u4XCInd             - XC Index
*                 u4InIndex           - InSegment Index
*                 u4OutIndex          - OutSegment Index 
*                 pTeCliArgs          - Pointer to TE CLI Args
* Output(s)     : pu4L3Intf           - L3 Interface Index
*                 pi4MplsTnlIfIndex   - MPLS Tunnel Interface Index
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliDeleteXcInAndOutSegment (tSNMP_OCTET_STRING_TYPE * pXCSegmentIndex,
                              tSNMP_OCTET_STRING_TYPE * pInSegmentIndex,
                              tSNMP_OCTET_STRING_TYPE * pOutSegmentIndex,
                              UINT4 u4XCInd, UINT4 u4InIndex, UINT4 u4OutIndex,
                              UINT4 *pu4L3Intf, INT4 *pi4MplsTnlIfIndex,
                              tTeCliArgs * pTeCliArgs)
{
    UINT4               u4ErrorCode = 0;
    UINT4               u4L3Intf = 0;
    INT4                i4Owner = 0;
    INT4                i4IfIndex = 0;
    /* STATIC_HLSP */
    UINT4               u4MplsIntf = TE_ZERO;
    UINT1               u1IfType = TE_ZERO;
    BOOL1               bFlag = MPLS_FALSE;

    /* Destroy the XCTable entry */
    if (u4XCInd != 0)
    {
        if (nmhGetMplsXCOwner (pXCSegmentIndex,
                               pInSegmentIndex, pOutSegmentIndex,
                               &i4Owner) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (i4Owner == MPLS_OWNER_SNMP)
        {
            if (nmhTestv2MplsXCRowStatus (&u4ErrorCode, pXCSegmentIndex,
                                          pInSegmentIndex,
                                          pOutSegmentIndex,
                                          MPLS_STATUS_DESTROY) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetMplsXCRowStatus
                (pXCSegmentIndex, pInSegmentIndex, pOutSegmentIndex,
                 MPLS_STATUS_DESTROY) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
        }
    }

    /* Destroy the InSegment entry */
    if (u4InIndex != 0)
    {
        if (nmhGetMplsInSegmentOwner (pInSegmentIndex, &i4Owner)
            == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }
        if (nmhGetMplsInSegmentInterface (pInSegmentIndex,
                                          &i4IfIndex) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (i4Owner == MPLS_OWNER_SNMP)
        {
            if (MplsGetL3Intf ((UINT4) i4IfIndex, &u4L3Intf) == MPLS_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (i4IfIndex == pTeCliArgs->i4TnlIfIndex)
            {
                bFlag = MPLS_FALSE;
            }
            else
            {
                bFlag = MPLS_TRUE;
            }

            if (MplsDeleteMplsIfOrMplsTnlIf (u4L3Intf, (UINT4) i4IfIndex,
                                             CFA_MPLS_TUNNEL,
                                             bFlag) == MPLS_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhTestv2MplsInSegmentRowStatus (&u4ErrorCode,
                                                 pInSegmentIndex,
                                                 MPLS_STATUS_DESTROY) ==
                SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetMplsInSegmentRowStatus (pInSegmentIndex,
                                              MPLS_STATUS_DESTROY)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
        }
    }

    /* Destroy the OutSegment entry */
    if (u4OutIndex != 0)
    {
        /* get the interface index b4 deleting the out seg entry */
        if (nmhGetMplsOutSegmentInterface (pOutSegmentIndex,
                                           &i4IfIndex) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (nmhGetMplsOutSegmentOwner (pOutSegmentIndex,
                                       &i4Owner) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (i4Owner == MPLS_OWNER_SNMP)
        {
            if (MplsGetL3Intf ((UINT4) i4IfIndex, &u4L3Intf) == MPLS_FAILURE)
            {
                return CLI_FAILURE;
            }

            /* STATIC_HLSP */
            if (CfaUtilGetMplsIfFromMplsTnlIf ((UINT4) i4IfIndex, &u4MplsIntf,
                                               TRUE) == CFA_FAILURE)
            {
                return CLI_FAILURE;
            }
            CfaGetIfType (u4MplsIntf, &u1IfType);
            if (u1IfType == CFA_MPLS_TUNNEL)
            {
                u4L3Intf = u4MplsIntf;
            }

            *pu4L3Intf = u4L3Intf;
            *pi4MplsTnlIfIndex = i4IfIndex;

            if (nmhTestv2MplsOutSegmentRowStatus (&u4ErrorCode,
                                                  pOutSegmentIndex,
                                                  MPLS_STATUS_DESTROY)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }

            if (nmhSetMplsOutSegmentRowStatus (pOutSegmentIndex,
                                               MPLS_STATUS_DESTROY)
                == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
        }
    }

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliDisplayLabelInfo
* Description   : This function is used to display label information for the
*                 tunnel.*
* Input(s)      : CliHandle           - Cli Handle
*                 u4XcIndex           - XC Index
*                 Direction           - Direction on which label information
*                                       to be displayed.
*                 u1TnlSgnlPrtcl      - Tunnel Signalling protocol.
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
VOID
TeCliDisplayLabelInfo (tCliHandle CliHandle, UINT4 u4XcIndex,
                       eDirection Direction, UINT1 u1TnlSgnlPrtcl)
{
    tXcEntry           *pXcEntry = NULL;
    UINT4               u4InIntf = 0;
    UINT4               u4L3Intf = 0;
    tCfaIfInfo          IfInfo;

    MEMSET (&IfInfo, 0, sizeof (tCfaIfInfo));

    pXcEntry = MplsGetXCEntryByDirection (u4XcIndex, Direction);
    if (pXcEntry == NULL)
    {
        return;
    }

    if (XC_ININDEX (pXcEntry) != NULL)
    {
        u4InIntf = INSEGMENT_IF_INDEX (XC_ININDEX (pXcEntry));
        if (u1TnlSgnlPrtcl == TE_SIGPROTO_NONE)
        {
            if (MplsGetL3Intf (u4InIntf, &u4L3Intf) == MPLS_SUCCESS)
            {
                u4InIntf = u4L3Intf;
            }
        }
        if (CfaGetIfInfo (u4InIntf, &IfInfo) == CFA_SUCCESS)
        {
            CliPrintf (CliHandle, "\r   InLabel  : %s, %d\n",
                       IfInfo.au1IfName,
                       INSEGMENT_LABEL (XC_ININDEX (pXcEntry)));
        }
    }
    else
    {
        CliPrintf (CliHandle, "\r   InLabel  : - \n");
    }

    if (XC_OUTINDEX (pXcEntry) != NULL)
    {
        if (MplsGetL3Intf (OUTSEGMENT_IF_INDEX
                           (XC_OUTINDEX (pXcEntry)), &u4L3Intf) == MPLS_SUCCESS)
        {
            if (CfaGetIfInfo (u4L3Intf, &IfInfo) == CFA_SUCCESS)
            {
                CliPrintf (CliHandle, "\r   OutLabel : %s, %d\n",
                           IfInfo.au1IfName,
                           OUTSEGMENT_LABEL (XC_OUTINDEX (pXcEntry)));
            }
        }
    }
    else
    {
        CliPrintf (CliHandle, "\r   OutLabel : - \n");
    }

    return;
}

#ifdef MPLS_RSVPTE_WANTED
/******************************************************************************
* Function Name : TeCliFrrGlobalRevertiveTime 
* Description   : This routine is used to configure global revertive time.
* Input(s)      : CliHandle  - Cli Context Handle
*                 i4GlobalRevertiveTime - Global revertive time interval. 
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliFrrGlobalRevertiveTime (tCliHandle CliHandle, INT4 i4GlobalRevertiveTime)
{
    UINT4               u4TunnelIndex = TE_ZERO;
    UINT4               u4TunnelInstance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    UINT4               u4ErrorCode = TE_ZERO;

    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    /*To configure global revertive time interval */
    if (nmhTestv2FsMplsTunnelExtMaxGblRevertTime (&u4ErrorCode,
                                                  u4TunnelIndex,
                                                  u4TunnelInstance,
                                                  u4IngressId,
                                                  u4EgressId,
                                                  i4GlobalRevertiveTime) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to configure global revertive"
                   "time interval\r\n");
        return CLI_FAILURE;
    }
    if (nmhSetFsMplsTunnelExtMaxGblRevertTime (u4TunnelIndex,
                                               u4TunnelInstance,
                                               u4IngressId,
                                               u4EgressId,
                                               i4GlobalRevertiveTime) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to configure global revertive"
                   "time interval\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliTunnelCRLDPResourceTable 
* Description   : This routine is used to configure CRLDP resource table.
* Input(s)      : CliHandle  - Cli Context Handle
*                 u4Bandwidth - Bandwidth.
                  u4TunnelResourceIndex - Resource table index.
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliTunnelCRLDPResourceTable (tCliHandle CliHandle, UINT4 u4Bandwidth,
                               UINT4 u4TunnelResourceIndex)
{
    UINT4               u4ErrorCode;

    if ((nmhTestv2MplsTunnelCRLDPResRowStatus (&u4ErrorCode,
                                               u4TunnelResourceIndex,
                                               MPLS_STATUS_CREATE_AND_WAIT))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to make resource rowstatus as"
                   "create-and-wait\r\n");
        return CLI_FAILURE;
    }

    if ((nmhSetMplsTunnelCRLDPResRowStatus (u4TunnelResourceIndex,
                                            MPLS_STATUS_CREATE_AND_WAIT)) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to make  resource rowstatus as "
                   "create-and-wait\r\n");
        return CLI_FAILURE;
    }

    if ((nmhTestv2FsMplsTunnelCRLDPResPeakDataRate (&u4ErrorCode,
                                                    u4TunnelResourceIndex,
                                                    u4Bandwidth)) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set Peak Datarate\r\n");
        return CLI_FAILURE;
    }
    if ((nmhSetFsMplsTunnelCRLDPResPeakDataRate (u4TunnelResourceIndex,
                                                 u4Bandwidth)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set Peak Datarate\r\n");
        return CLI_FAILURE;
    }

    if ((nmhTestv2MplsTunnelCRLDPResFrequency (&u4ErrorCode,
                                               u4TunnelResourceIndex,
                                               TE_TNL_FREQ_UNSPECIFIED))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set the frequency\r\n");
        return CLI_FAILURE;
    }
    if ((nmhSetMplsTunnelCRLDPResFrequency (u4TunnelResourceIndex,
                                            TE_TNL_FREQ_UNSPECIFIED))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set the frequency\r\n");
        return CLI_FAILURE;
    }
    if ((nmhTestv2MplsTunnelCRLDPResWeight (&u4ErrorCode,
                                            u4TunnelResourceIndex,
                                            TE_CRLDP_WGS)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set the weight\r\n");
        return CLI_FAILURE;
    }
    if ((nmhSetMplsTunnelCRLDPResWeight (u4TunnelResourceIndex,
                                         TE_CRLDP_WGS)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set the weight\r\n");
        return CLI_FAILURE;
    }
    if ((nmhTestv2MplsTunnelCRLDPResFlags (&u4ErrorCode,
                                           u4TunnelResourceIndex,
                                           TE_CRLDP_FLAGS)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set the CRLDP flags\r\n");
        return CLI_FAILURE;
    }
    if ((nmhSetMplsTunnelCRLDPResFlags (u4TunnelResourceIndex,
                                        TE_CRLDP_FLAGS)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set the CRLDP flags\r\n");
        return CLI_FAILURE;
    }
    if ((nmhTestv2FsMplsTunnelCRLDPResCommittedDataRate (&u4ErrorCode,
                                                         u4TunnelResourceIndex,
                                                         TE_CRLDP_DR))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set the comitted data rate\r\n");
        return CLI_FAILURE;
    }
    if ((nmhSetFsMplsTunnelCRLDPResCommittedDataRate (u4TunnelResourceIndex,
                                                      TE_CRLDP_DR))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set the comitted data rate\r\n");
        return CLI_FAILURE;
    }
    if ((nmhTestv2FsMplsTunnelCRLDPResPeakBurstSize (&u4ErrorCode,
                                                     u4TunnelResourceIndex,
                                                     TE_CRLDP_PBS))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set the peak burst size\r\n");
        return CLI_FAILURE;
    }
    if ((nmhSetFsMplsTunnelCRLDPResPeakBurstSize (u4TunnelResourceIndex,
                                                  TE_CRLDP_PBS))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set the peak burst size\r\n");
        return CLI_FAILURE;
    }
    if ((nmhTestv2FsMplsTunnelCRLDPResCommittedBurstSize (&u4ErrorCode,
                                                          u4TunnelResourceIndex,
                                                          TE_CRLDP_CBS))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to set the comitted burst size\r\n");
        return CLI_FAILURE;
    }
    if ((nmhSetFsMplsTunnelCRLDPResCommittedBurstSize (u4TunnelResourceIndex,
                                                       TE_CRLDP_CBS))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to set the comitted burst size\r\n");
        return CLI_FAILURE;
    }
    if ((nmhTestv2FsMplsTunnelCRLDPResExcessBurstSize (&u4ErrorCode,
                                                       u4TunnelResourceIndex,
                                                       TE_CRLDP_EBS))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set the excess burst size\r\n");
        return CLI_FAILURE;
    }
    if ((nmhSetFsMplsTunnelCRLDPResExcessBurstSize (u4TunnelResourceIndex,
                                                    TE_CRLDP_EBS))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set the excess burst size\r\n");
        return CLI_FAILURE;
    }

    if ((nmhTestv2MplsTunnelCRLDPResRowStatus (&u4ErrorCode,
                                               u4TunnelResourceIndex,
                                               MPLS_STATUS_ACTIVE)) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set the Row status active\r\n");
        return CLI_FAILURE;
    }

    if ((nmhSetMplsTunnelCRLDPResRowStatus (u4TunnelResourceIndex,
                                            MPLS_STATUS_ACTIVE)) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set the Row status active\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeShowRunningConfigLabelInfoDisplay
* Description   : This routine displays SRC for tunnel.
* Input(s)      : CliHandle  - Cli Context Handle
*                 u4XCIndex  - XC Index
                  Direction  - Segment Direction
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeShowRunningConfigLabelInfoDisplay (tCliHandle CliHandle, UINT4 u4XCIndex,
                                     eDirection Direction)
{
    tInTlmTeLink        InTlmTeLinkInfo;
    tOutTlmTeLink       OutTlmTeLinkInfo;
    UINT4               u4NextHop = 0;
    UINT4               u4OutIfIndex = 0;
#ifdef CFA_WANTED
    UINT4               u4InL3Intf = 0;
#endif
    UINT1               au1IfName[CFA_MAX_PORT_NAME_LENGTH] = { 0 };
    INT1               *pi1IfName = NULL;
    tXcEntry           *pXcEntry = NULL;
    UINT4               u4MplsTnlIfIndex = 0;
    CHR1               *pc1NextHop = NULL;

    MEMSET (&InTlmTeLinkInfo, TE_ZERO, sizeof (tInTlmTeLink));
    MEMSET (&OutTlmTeLinkInfo, TE_ZERO, sizeof (tOutTlmTeLink));

    pi1IfName = (INT1 *) &au1IfName[0];

    pXcEntry = MplsGetXCEntryByDirection (u4XCIndex, Direction);
    if (pXcEntry == NULL)
    {
        return CLI_FAILURE;
    }

    if (XC_ININDEX (pXcEntry) != NULL)
    {
        u4MplsTnlIfIndex = INSEGMENT_IF_INDEX (XC_ININDEX (pXcEntry));
#ifdef CFA_WANTED
        /* get interface index from tunnel interface */
        if (MPLS_FAILURE == MplsGetL3Intf (u4MplsTnlIfIndex, &u4InL3Intf))
        {
            return CLI_FAILURE;
        }
        /* get interface name */
        if (CFA_SUCCESS != CfaCliConfGetIfName (u4InL3Intf, pi1IfName))
        {
            return CLI_FAILURE;
        }
#endif
        /* From InIndex and OutIndex, Fetch the InLabel,
         * InInterface (MplsTnlIf) from InSegmentTable and OutLabel, NextHop
         * from OutSegment Table */
        CliPrintf (CliHandle, " in-label %u",
                   INSEGMENT_LABEL (XC_ININDEX (pXcEntry)));
        FilePrintf (CliHandle, " in-label %u",
                    INSEGMENT_LABEL (XC_ININDEX (pXcEntry)));

        CliPrintf (CliHandle, " %s", pi1IfName);
        FilePrintf (CliHandle, " %s", pi1IfName);
    }
    if (XC_OUTINDEX (pXcEntry) != NULL)
    {
        CliPrintf (CliHandle, " out-label %u",
                   OUTSEGMENT_LABEL (XC_OUTINDEX (pXcEntry)));
        FilePrintf (CliHandle, " out-label %u",
                    OUTSEGMENT_LABEL (XC_OUTINDEX (pXcEntry)));

        u4NextHop = OUTSEGMENT_NH_ADDR (XC_OUTINDEX (pXcEntry));

        u4OutIfIndex = pXcEntry->pOutIndex->u4IfIndex;

        if (u4NextHop != TE_ZERO)
        {
            CLI_CONVERT_IPADDR_TO_STR (pc1NextHop, u4NextHop);
            CliPrintf (CliHandle, " %s", pc1NextHop);
        }
        else
        {
            if (MplsGetL3Intf (u4OutIfIndex, &u4OutIfIndex) == MPLS_FAILURE)
            {
                return CLI_FAILURE;
            }

#ifdef CFA_WANTED
#ifdef TLM_WANTED
            CfaApiGetTeLinkIfFromL3If (u4OutIfIndex, &u4OutIfIndex, TE_ONE,
                                       TRUE);
#endif
#endif
            InTlmTeLinkInfo.u4IfIndex = u4OutIfIndex;

#ifdef TLM_WANTED
            TlmApiGetTeLinkParams (TLM_TE_LINK_IF_INDEX, &InTlmTeLinkInfo,
                                   &OutTlmTeLinkInfo);
#endif
            CliPrintf (CliHandle, " te-link %s",
                       OutTlmTeLinkInfo.au1TeLinkName);
            FilePrintf (CliHandle, " te-link %s",
                        OutTlmTeLinkInfo.au1TeLinkName);
        }
    }

    if (Direction == MPLS_DIRECTION_REVERSE)
    {
        CliPrintf (CliHandle, " direction reverse");
        FilePrintf (CliHandle, " direction reverse");
    }
    return CLI_SUCCESS;
}

#endif
/* STATIC_HLSP */

/******************************************************************************
* Function Name : TeCliGetOutTnlIfIndex 
* Description   : This routine is used to get the tunnel ifIndex from OutSegment
*               : associated to the tunnel for the given direction. 
* Input(s)      : CliHandle       - Cli context identifier
*               : u4TnlIndex      - Tunnel index
*               : u4TnlInstance   - Tunnel Instance number
*               : u4IngresId      - Tunnel Ingress LSR Id
*               : u4EgressId      - Tunnel Egress LSR Id
*               : u4Direction     - Specifies the direction of the XC from 
*               :                   which the tunnel ifIndex should be 
*               :                   retrieved.
* Output(s)     : pi4TnlIfIndex   - the tunnel ifIndex fetched from the 
*               :                   corresponding OutSegment.
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliGetOutTnlIfIndex (tCliHandle CliHandle, UINT4 u4TnlIndex,
                       UINT4 u4TnlInstance, UINT4 u4IngresId,
                       UINT4 u4EgressId, UINT4 u4Direction, INT4 *pi4TnlIfIndex)
{
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    UINT1               au1OutSegIndex[MPLS_INDEX_LENGTH];

    tTeTnlInfo         *pTeTnlInfo = NULL;
    tXcEntry           *pXcEntry = NULL;
    tOutSegment        *pOutSegment = NULL;

    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;

    MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);

    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4TnlIndex, u4TnlInstance, u4IngresId,
                                  u4EgressId);
    if (pTeTnlInfo == NULL)
    {
        CliPrintf (CliHandle, "\r\n%%Fetching tunnel entry failed.\r\n");
        MPLS_CMN_UNLOCK ();
        return CLI_FAILURE;
    }

    pXcEntry =
        MplsGetXCEntryByDirection (TE_TNL_XCINDEX (pTeTnlInfo),
                                   (eDirection) u4Direction);
    if (pXcEntry == NULL)
    {
        CliPrintf (CliHandle, "\r\n%%XC doesn't exists for the tunnel"
                   "specidfied direction.\r\n");
        MPLS_CMN_UNLOCK ();
        return CLI_FAILURE;
    }

    pOutSegment = XC_OUTINDEX (pXcEntry);
    if (pOutSegment == NULL)
    {
        CliPrintf (CliHandle, "\r\n%%OutSegment doesn't available.\r\n");
        MPLS_CMN_UNLOCK ();
        return CLI_FAILURE;
    }
    MPLS_INTEGER_TO_OCTETSTRING (pOutSegment->u4Index, (&OutSegmentIndex));
    MPLS_CMN_UNLOCK ();

    if ((nmhGetMplsOutSegmentInterface
         (&OutSegmentIndex, pi4TnlIfIndex)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to get the Tnl ifIndex "
                   "from OutSegment.\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliGetInTnlIfIndex 
* Description   : This routine is used to get the tunnel ifIndex from InSegment
*               : associated to the tunnel for the given direction. 
* Input(s)      : CliHandle       - Cli context identifier
*               : u4TnlIndex      - Tunnel index
*               : u4TnlInstance   - Tunnel Instance number
*               : u4IngresId      - Tunnel Ingress LSR Id
*               : u4EgressId      - Tunnel Egress LSR Id
*               : u4Direction     - Specifies the direction of the XC from 
*               :                   which the tunnel ifIndex should be 
*               :                   retrieved.
* Output(s)     : pi4TnlIfIndex   - the tunnel ifIndex fetched from the 
*               :                   corresponding InSegment.
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliGetInTnlIfIndex (tCliHandle CliHandle, UINT4 u4TnlIndex,
                      UINT4 u4TnlInstance, UINT4 u4IngresId,
                      UINT4 u4EgressId, UINT4 u4Direction, INT4 *pi4TnlIfIndex)
{
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    UINT1               au1InSegIndex[MPLS_INDEX_LENGTH];

    tTeTnlInfo         *pTeTnlInfo = NULL;
    tXcEntry           *pXcEntry = NULL;
    tInSegment         *pInSegment = NULL;

    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;

    MEMSET (au1InSegIndex, 0, MPLS_INDEX_LENGTH);

    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4TnlIndex, u4TnlInstance, u4IngresId,
                                  u4EgressId);
    if (pTeTnlInfo == NULL)
    {
        CliPrintf (CliHandle, "\r\n%%Fetching tunnel entry failed.\r\n");
        MPLS_CMN_UNLOCK ();
        return CLI_FAILURE;
    }

    pXcEntry =
        MplsGetXCEntryByDirection (TE_TNL_XCINDEX (pTeTnlInfo),
                                   (eDirection) u4Direction);
    if (pXcEntry == NULL)
    {
        CliPrintf (CliHandle, "\r\n%%XC doesn't exists for the tunnel"
                   "specidfied direction.\r\n");
        MPLS_CMN_UNLOCK ();
        return CLI_FAILURE;
    }

    pInSegment = XC_ININDEX (pXcEntry);
    if (pInSegment == NULL)
    {
        CliPrintf (CliHandle, "\r\n%%InSegment doesn't available.\r\n");
        MPLS_CMN_UNLOCK ();
        return CLI_FAILURE;
    }
    MPLS_INTEGER_TO_OCTETSTRING (pInSegment->u4Index, (&InSegmentIndex));
    MPLS_CMN_UNLOCK ();

    if ((nmhGetMplsInSegmentInterface
         (&InSegmentIndex, pi4TnlIfIndex)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to get the Tnl ifIndex "
                   "from InSegment.\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliActivateXc 
* Description   : This routine is used to activate the XC, In and Out segments
*               : associated to the tunnel for the given direction. 
* Input(s)      : CliHandle       - Cli context identifier
*               : u4TnlIndex      - Tunnel index
*               : u4TnlInstance   - Tunnel Instance number
*               : u4IngresId      - Tunnel Ingress LSR Id
*               : u4EgressId      - Tunnel Egress LSR Id
*               : u4Direction     - Specifies the direction of the XC from 
*               :                   which the tunnel ifIndex should be 
*               :                   retrieved.
* Output(s)     : None            
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliActivateXc (tCliHandle CliHandle, UINT4 u4TnlIndex,
                 UINT4 u4TnlInstance, UINT4 u4IngresId,
                 UINT4 u4EgressId, UINT4 u4Direction)
{
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    UINT1               au1InSegIndex[MPLS_INDEX_LENGTH];
    UINT1               au1OutSegIndex[MPLS_INDEX_LENGTH];
    UINT1               au1XCIndex[MPLS_INDEX_LENGTH];
    UINT4               u4ErrorCode = TE_ZERO;
    UINT1               u1TnlRole = TE_ZERO;

    tTeTnlInfo         *pTeTnlInfo = NULL;
    tXcEntry           *pXcEntry = NULL;
    tInSegment         *pInSegment = NULL;
    tOutSegment        *pOutSegment = NULL;

    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = 0;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = 0;
    XCIndex.pu1_OctetList = au1XCIndex;
    XCIndex.i4_Length = 0;

    MEMSET (au1InSegIndex, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1OutSegIndex, 0, MPLS_INDEX_LENGTH);
    MEMSET (au1XCIndex, 0, MPLS_INDEX_LENGTH);

    MPLS_CMN_LOCK ();

    pTeTnlInfo = TeGetTunnelInfo (u4TnlIndex, u4TnlInstance, u4IngresId,
                                  u4EgressId);
    if (pTeTnlInfo == NULL)
    {
        CliPrintf (CliHandle, "\r\n%%Fetching tunnel entry failed.\r\n");
        MPLS_CMN_UNLOCK ();
        return CLI_FAILURE;
    }

    u1TnlRole = pTeTnlInfo->u1TnlRole;

    pXcEntry =
        MplsGetXCEntryByDirection (TE_TNL_XCINDEX (pTeTnlInfo),
                                   (eDirection) u4Direction);
    if (pXcEntry == NULL)
    {
        CliPrintf (CliHandle, "\r\n%%XC doesn't exists for the tunnel"
                   "specified direction.\r\n");
        MPLS_CMN_UNLOCK ();
        return CLI_FAILURE;
    }

    MPLS_INTEGER_TO_OCTETSTRING (XC_INDEX (pXcEntry), (&XCIndex));

    pInSegment = XC_ININDEX (pXcEntry);
    if (pInSegment != NULL)
    {
        MPLS_INTEGER_TO_OCTETSTRING (pInSegment->u4Index, (&InSegmentIndex));
    }

    pOutSegment = XC_OUTINDEX (pXcEntry);
    if (pOutSegment != NULL)
    {
        MPLS_INTEGER_TO_OCTETSTRING (pOutSegment->u4Index, (&OutSegmentIndex));
    }

    MPLS_CMN_UNLOCK ();

    /* Set the row status of the InSegment as active */
    if (((u4Direction == MPLS_DIRECTION_FORWARD) &&
         ((u1TnlRole == TE_INTERMEDIATE) || (u1TnlRole == TE_EGRESS))) ||
        ((u4Direction == MPLS_DIRECTION_REVERSE) &&
         ((u1TnlRole == TE_INTERMEDIATE) || (u1TnlRole == TE_INGRESS))))
    {
        if (nmhTestv2MplsInSegmentStorageType (&u4ErrorCode, &InSegmentIndex,
                                               MPLS_STORAGE_NONVOLATILE)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set In segment "
                       "storage type");
            return CLI_FAILURE;
        }

        if (nmhSetMplsInSegmentStorageType (&InSegmentIndex,
                                            MPLS_STORAGE_NONVOLATILE) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set In segment "
                       "storage type");
            return CLI_FAILURE;
        }

        if ((nmhTestv2MplsInSegmentRowStatus (&u4ErrorCode, &InSegmentIndex,
                                              MPLS_STATUS_ACTIVE)) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set In segment "
                       "row status active");
            return CLI_FAILURE;
        }

        if ((nmhSetMplsInSegmentRowStatus
             (&InSegmentIndex, MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set In segment "
                       "row status active");
            return CLI_FAILURE;
        }
    }

    /* Set the RowStatus  of outsegment as active */
    if (((u4Direction == MPLS_DIRECTION_FORWARD) &&
         ((u1TnlRole == TE_INTERMEDIATE) || (u1TnlRole == TE_INGRESS))) ||
        ((u4Direction == MPLS_DIRECTION_REVERSE) &&
         ((u1TnlRole == TE_INTERMEDIATE) || (u1TnlRole == TE_EGRESS))))
    {
        if (nmhTestv2MplsOutSegmentStorageType (&u4ErrorCode, &OutSegmentIndex,
                                                MPLS_STORAGE_NONVOLATILE)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set Out segment "
                       "storage type");
            return CLI_FAILURE;
        }

        if (nmhSetMplsOutSegmentStorageType (&OutSegmentIndex,
                                             MPLS_STORAGE_NONVOLATILE) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set Out segment "
                       "storage type");
            return CLI_FAILURE;
        }

        if ((nmhTestv2MplsOutSegmentRowStatus
             (&u4ErrorCode, &OutSegmentIndex,
              MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set Out segment "
                       "row status active");
            return CLI_FAILURE;
        }
        if ((nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                            MPLS_STATUS_ACTIVE)) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set Out segment "
                       "row status active");
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2MplsXCStorageType (&u4ErrorCode, &XCIndex, &InSegmentIndex,
                                    &OutSegmentIndex,
                                    MPLS_STORAGE_NONVOLATILE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% Unable to set storage type\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetMplsXCStorageType (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                                 MPLS_STORAGE_NONVOLATILE) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% Unable to set storage type\r\n");
        return CLI_FAILURE;
    }

    /* Make the Cross connect row status as active */
    if ((nmhTestv2MplsXCRowStatus
         (&u4ErrorCode, &XCIndex, &InSegmentIndex, &OutSegmentIndex,
          MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set XC row status active");
        return CLI_FAILURE;
    }
    if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex,
                                &OutSegmentIndex,
                                MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set XC row status active");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliStaticBindStack
* Description   : This routine is used to associate XC Entries with that of
*                 tunne and stack the tunnel to HLSP tunnel. 
* Input(s)      : CliHandle        - Cli Context Handle
*               : pTeCliStBindArgs - pointer to the Static Binding arguments 
*               :                    which contains in-label, out-label, 
*               :                    interfaces, direction and operation.
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/

INT4
TeCliStaticBindStack (tCliHandle CliHandle, tteCliStBindArgs * pTeCliStBindArgs)
{

    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCSegmentIndex;
    tSNMP_OCTET_STRING_TYPE MplsLabelStackIndexNext;
    tSNMP_OID_TYPE      TunnelXCPointer;
    tSNMP_OID_TYPE      XCSegIndex;
    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;
    tSNMP_OCTET_STRING_TYPE NextHopAddr;
    tteFsMplsLSPMapTunnelTable teSetFsMplsLSPMapTunnelTable;
    tteIsSetFsMplsLSPMapTunnelTable teIsSetFsMplsLSPMapTunnelTable;

    static UINT4        au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 2, 1, 10, 1, 4, 4, 0, 0, 0, 0, 4, 0, 0, 0,
        0, 4, 0, 0, 0, 0
    };
    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1LblStackIndex[MPLS_INDEX_LENGTH];
    static UINT4        au4XCIndexx[MPLS_TE_XC_TABLE_OFFSET];
    static UINT1        au1NextHopAddr[MPLS_INDEX_LENGTH];

    tTeTnlInfo         *pTeHlspOrSlspTnlInfo = NULL;
    tHlspTnlCliArgs     HlspTnlCliArgs;
    INT4                i4TunnelMode = TE_ZERO;
    UINT4               u4TunnelIndex = TE_ZERO;
    UINT4               u4TunnelInstance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    UINT4               u4HlspOrSlspTnlIndex = TE_ZERO;
    UINT4               u4HlspOrSlspTnlInstance = TE_ZERO;
    UINT4               u4HlspOrSlspIngressId = TE_ZERO;
    UINT4               u4HlspOrSlspEgressId = TE_ZERO;
    UINT4               u4XCTabIndex = TE_ZERO;
    UINT4               u4InIndex = TE_ZERO;
    UINT4               u4OutIndex = TE_ZERO;
    UINT4               u4XCSegInd = TE_ZERO;
    UINT4               u4InSegInd = TE_ZERO;
    UINT4               u4OutSegInd = TE_ZERO;
    UINT4               u4ErrorCode = TE_ZERO;
    UINT4               u4MplsTnlInIfIndex = TE_ZERO;
    UINT4               u4MplsTnlOutIfIndex = TE_ZERO;
    UINT4               u4LblStkIndex = TE_ZERO;
    UINT4              *pu4Operation = &(pTeCliStBindArgs->u4Operation);
    UINT4               u4RowStatus = ACTIVE;
    UINT4               u4MplsIfIndex = 0;
    INT4                i4TnlRole = TE_ZERO;
    INT4                i4Direction = MPLS_DIRECTION_FORWARD;
    UINT1               u1HlspOrSlspTnlRole = 0;
    BOOL1               bXcDeleteFlag = TE_TRUE;
    BOOL1               bIntfCreationReqd = TRUE;
    BOOL1               bOutSegCreated = MPLS_SNMP_FALSE;
    BOOL1               bXcCreated = MPLS_SNMP_FALSE;
    BOOL1               bInTnlCreated = MPLS_SNMP_FALSE;
    BOOL1               bOutTnlCreated = MPLS_SNMP_FALSE;

    UINT4               u4CreatedInstance = TE_ZERO;
    UINT4               u4Direction = TE_ZERO;
    BOOL1               bEntryCreated = FALSE;

    XCSegIndex.pu4_OidList = au4XCIndexx;
    XCSegIndex.u4_Length = 0;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    XCIndex.pu1_OctetList = au1XCIndex;
    XCIndex.i4_Length = MPLS_INDEX_LENGTH;
    MplsLabelStackIndexNext.pu1_OctetList = au1LblStackIndex;
    MplsLabelStackIndexNext.i4_Length = MPLS_INDEX_LENGTH;
    NextHopAddr.pu1_OctetList = au1NextHopAddr;

    MEMSET (&teSetFsMplsLSPMapTunnelTable, 0,
            sizeof (tteFsMplsLSPMapTunnelTable));
    MEMSET (&teIsSetFsMplsLSPMapTunnelTable, 0,
            sizeof (tteIsSetFsMplsLSPMapTunnelTable));

    MEMSET (&HlspTnlCliArgs, 0, sizeof (tHlspTnlCliArgs));
    /* Get the outgoing ifIndex when the NextHop is given. */
    if (pTeCliStBindArgs->u4Operation == TE_OPER_STITCH)
    {
        if ((pTeCliStBindArgs->u4OutIfIndex == 0) &&
            (pTeCliStBindArgs->u4NextHopAddr != 0))
        {
            if (NetIpv4IfIsOurAddress
                (pTeCliStBindArgs->u4NextHopAddr) == NETIPV4_SUCCESS)
            {
                CliPrintf (CliHandle, "\r%%Next hop is same as self-ip addr\n");
                return CLI_FAILURE;
            }

            MEMSET (&NetIpRtInfo, 0, sizeof (tNetIpv4RtInfo));
            MEMSET (&RtQuery, 0, sizeof (tRtInfoQueryMsg));
            RtQuery.u4DestinationIpAddress = pTeCliStBindArgs->u4NextHopAddr;
            RtQuery.u4DestinationSubnetMask = 0xffffffff;
            RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;

            if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_SUCCESS)
            {
                if (NetIpv4GetCfaIfIndexFromPort (NetIpRtInfo.u4RtIfIndx,
                                                  &(pTeCliStBindArgs->
                                                    u4OutIfIndex)) ==
                    NETIPV4_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%% Unable to fetch Next hop information\n");
                    return CLI_FAILURE;
                }
            }
            else
            {
                if (CfaIpIfGetIfIndexFromHostIpAddressInCxt
                    (MPLS_DEF_CONTEXT_ID, pTeCliStBindArgs->u4NextHopAddr,
                     &(pTeCliStBindArgs->u4OutIfIndex)) == CFA_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%% Unable to fetch Next hop information\n");
                    return CLI_FAILURE;
                }
            }
        }
    }

    if (pTeCliStBindArgs->u4OutIfIndex != 0)
    {
        if (CfaUtilGetMplsIfFromIfIndex (pTeCliStBindArgs->u4OutIfIndex,
                                         &u4MplsIfIndex, TRUE) == CFA_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%% Enable MPLS over outgoing interface\r\n");
            return CLI_FAILURE;
        }
    }

    if (pTeCliStBindArgs->u4InIfIndex != 0)
    {
        if (CfaUtilGetMplsIfFromIfIndex (pTeCliStBindArgs->u4InIfIndex,
                                         &u4MplsIfIndex, TRUE) == CFA_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%% Enable MPLS over incoming interface\r\n");
            return CLI_FAILURE;
        }
    }

    /*
     * Step 1: 
     * Get the TunnelId,TunnelInstance,Ingress and Egress identifiers 
     * from the prompt.
     */
    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetMplsTunnelRole (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                              u4EgressId, &i4TnlRole) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (pTeCliStBindArgs->u4Operation == TE_OPER_STACK)
    {
        if (i4TnlRole != TE_INTERMEDIATE)
        {
            CliPrintf (CliHandle, "\r\n%%Wrong label configuration "
                       "for LSR\r\n");
            return CLI_FAILURE;
        }
    }

    if (nmhGetFsMplsTunnelMode (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                u4EgressId, &i4TunnelMode) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* Set the direction of the tunnel */
    if ((i4TunnelMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
        && (pTeCliStBindArgs->u4Direction == MPLS_CLI_DIRECTION_REVERSE))
    {
        pTeCliStBindArgs->u4Direction = MPLS_DIRECTION_REVERSE;
    }
    else
    {
        pTeCliStBindArgs->u4Direction = MPLS_DIRECTION_FORWARD;
    }

    /* Step 2 : Get the H-LSP/S-LSP Tunnel info */
    /* If Tunnel does not exist , return Failure */

    MPLS_CMN_LOCK ();

    pTeHlspOrSlspTnlInfo =
        TeGetTnlInfoByOwnerAndRole (pTeCliStBindArgs->u4TnlId,
                                    u4HlspOrSlspTnlInstance,
                                    u4HlspOrSlspIngressId, u4HlspOrSlspEgressId,
                                    TE_TNL_OWNER_SNMP, TE_ZERO);
    if (pTeHlspOrSlspTnlInfo == NULL)
    {
        if (pTeCliStBindArgs->u4Operation == TE_OPER_STACK)
        {
            CliPrintf (CliHandle, "\r\n%%H-LSP Tunnel Does Not Exist\r\n");
        }
        else
        {
            CliPrintf (CliHandle, "\r\n%%S-LSP Tunnel Does Not Exist\r\n");
        }
        CliPrintf (CliHandle,
                   "\r%%The Tunnel must be created before attempting "
                   "to do this operation.\r\n");
        MPLS_CMN_UNLOCK ();
        return CLI_FAILURE;
    }

    /* Get the H-LSP/S-LSP tunnel indices */
    u4HlspOrSlspTnlIndex = TE_TNL_TNL_INDEX (pTeHlspOrSlspTnlInfo);
    u4HlspOrSlspTnlInstance =
        TE_TNL_PRIMARY_TNL_INSTANCE (pTeHlspOrSlspTnlInfo);

    MEMCPY ((UINT1 *) (&u4HlspOrSlspIngressId),
            TE_TNL_INGRESS_LSRID (pTeHlspOrSlspTnlInfo), ROUTER_ID_LENGTH);
    MEMCPY ((UINT1 *) (&u4HlspOrSlspEgressId),
            TE_TNL_EGRESS_LSRID (pTeHlspOrSlspTnlInfo), ROUTER_ID_LENGTH);

    u4HlspOrSlspIngressId = OSIX_HTONL (u4HlspOrSlspIngressId);
    u4HlspOrSlspEgressId = OSIX_HTONL (u4HlspOrSlspEgressId);

    u1HlspOrSlspTnlRole = TE_TNL_ROLE (pTeHlspOrSlspTnlInfo);

    MPLS_CMN_UNLOCK ();

    if (u1HlspOrSlspTnlRole == TE_INTERMEDIATE)
    {
        CliPrintf (CliHandle, "\r%%The HLSP tunnel Role is intermediate.\r\n");
        MPLS_CMN_UNLOCK ();
        return CLI_FAILURE;
    }

    if ((u1HlspOrSlspTnlRole == TE_EGRESS) &&
        (pTeHlspOrSlspTnlInfo->u4TnlMode ==
         TE_TNL_MODE_COROUTED_BIDIRECTIONAL) &&
        (pTeCliStBindArgs->u4Direction == MPLS_DIRECTION_FORWARD))
    {
        u4Direction = MPLS_DIRECTION_REVERSE;
    }
    else
    {
        u4Direction = pTeCliStBindArgs->u4Direction;
    }
    if (TeCliGetOutTnlIfIndex (CliHandle, u4HlspOrSlspTnlIndex,
                               u4HlspOrSlspTnlInstance,
                               u4HlspOrSlspIngressId, u4HlspOrSlspEgressId,
                               u4Direction,
                               (INT4 *) &u4OutSegInd) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Fetching the Tunnel Out ifIndex "
                   "failed.\r\n");
        return CLI_FAILURE;
    }

    /* Fetch the L3 interface from the out tunnel interface so that 
     * the e2e LSP tunnel interface will be stacked over mpls interface 
     * which is stacked over the fetched L3 interface when the tunnles 
     * are to be stitched.*/
    if (pTeCliStBindArgs->u4Operation == TE_OPER_STITCH)
    {
        if (MplsGetL3Intf
            (u4OutSegInd, &(pTeCliStBindArgs->u4OutIfIndex)) == MPLS_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Fetching the L3 ifIndex "
                       "from tunnel ifIndex failed.\r\n");
            return CLI_FAILURE;
        }
    }
    else
    {
        /* Fetched out tunnel interface is sufficient to stack the service 
         * tunnel over H-LSP tunnel.*/
        pTeCliStBindArgs->u4OutIfIndex = u4OutSegInd;
    }
    if (u4TunnelInstance == TE_ZERO)
    {
        if (TeCliNormalCreateNonZeroInstTunnel (CliHandle, u4TunnelIndex,
                                                u4TunnelInstance, u4IngressId,
                                                u4EgressId,
                                                pTeCliStBindArgs->u1TnlPathType,
                                                &u4CreatedInstance,
                                                &bEntryCreated) == CLI_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to create non-zero instance tunnel\n");
            return CLI_FAILURE;
        }

        u4TunnelInstance = u4CreatedInstance;

        if ((pTeCliStBindArgs->u4Direction == MPLS_DIRECTION_FORWARD) &&
            (pTeCliStBindArgs->u1TnlPathType == TE_TNL_WORKING_PATH))
        {
            u4MplsTnlOutIfIndex = gaTnlCliArgs[CliHandle].u4MplsTnlIfIndex;
            bIntfCreationReqd = FALSE;
        }
    }

    /* Using the service/e2e LSP tunnel indices, get the tunnel XC pointer.
     * From the XCPointer value obtain the XCIndex,InSegment and OutSegment 
     * Index.
     */
    if (nmhGetMplsTunnelXCPointer (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                   u4EgressId, &XCSegIndex) == SNMP_FAILURE)
    {

        TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                        u4TunnelInstance, u4IngressId,
                                        u4EgressId, bEntryCreated);
        CliPrintf (CliHandle, "\r\n%%Unable to get tunnel XC Pointer\r\n");
        return CLI_FAILURE;
    }

    MplsGetSegFromTunnelXC (&XCSegIndex, &XCSegmentIndex, &InSegmentIndex,
                            &OutSegmentIndex, &u4XCSegInd, &u4InSegInd,
                            &u4OutSegInd);

    /* Check whether the XC is already exists for the opposite direction 
     * for Co-routed bi-directional tunnel to use the same XC index to 
     * create the XC entry for the given direction.*/
    if (i4TunnelMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
    {
        if (u4InSegInd != 0)
        {
            if ((nmhGetFsMplsInSegmentDirection
                 (&InSegmentIndex, &i4Direction) == SNMP_SUCCESS) &&
                (i4Direction != (INT4) (pTeCliStBindArgs->u4Direction)))
            {
                bXcDeleteFlag = FALSE;
            }
        }

        if (u4OutSegInd != 0)
        {
            if ((nmhGetFsMplsOutSegmentDirection
                 (&OutSegmentIndex, &i4Direction) == SNMP_SUCCESS) &&
                (i4Direction != (INT4) (pTeCliStBindArgs->u4Direction)))
            {
                bXcDeleteFlag = FALSE;
            }
        }
    }

    /* Step 3:  Check if Xc, Insegment and Outsegment already exist. 
     * Return Error message if the ILM is already configured */
    if ((u4XCSegInd != 0) && (bXcDeleteFlag == TE_TRUE))
    {
        if (u4InSegInd != 0)
        {
            CliPrintf (CliHandle,
                       "\n\t InSegment already exist. Delete the tunnel "
                       "and re create to modify the Insegment\n");
            return CLI_SUCCESS;
        }
        if (u4OutSegInd != 0)
        {
            CliPrintf (CliHandle,
                       "\n\t OutSegment already exist. Delete the tunnel "
                       "and re create to modify the Outsegment\n");
            return CLI_SUCCESS;
        }
    }

    /* The function MplsGetSegFromTunnelXC updates the pointer in the 
     * Octet string data struture passed. Hence reverting it.*/
    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = 0;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = 0;

    /*
     * Step 4:
     * If InSeg , Out Seg and XC Entries are not already present for the 
     * same tunnel, get a free InSeg, OutSeg and XC Index,
     * and  populate the XC, InSeg and OutSeg Tables.
     */

    /* Step 4a :Create an Inseg entry and set the Row status as Create 
     * and Wait */

    if (((pTeCliStBindArgs->u4Direction == MPLS_DIRECTION_FORWARD) &&
         ((i4TnlRole == TE_INTERMEDIATE) || (i4TnlRole == TE_EGRESS))) ||
        ((pTeCliStBindArgs->u4Direction == MPLS_DIRECTION_REVERSE) &&
         ((i4TnlRole == TE_INTERMEDIATE) || (i4TnlRole == TE_INGRESS))))
    {
        if ((nmhGetMplsInSegmentIndexNext (&InSegmentIndex)) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_NO_FREE_INDEX);

            TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                            u4TunnelInstance, u4IngressId,
                                            u4EgressId, bEntryCreated);
            return CLI_FAILURE;
        }
        if ((nmhTestv2MplsInSegmentRowStatus (&u4ErrorCode, &InSegmentIndex,
                                              MPLS_STATUS_CREATE_AND_WAIT)) ==
            SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_INSEG_ENTRY_CREATION);

            TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                            u4TunnelInstance, u4IngressId,
                                            u4EgressId, bEntryCreated);
            return CLI_FAILURE;
        }
        if ((nmhSetMplsInSegmentRowStatus
             (&InSegmentIndex, MPLS_STATUS_CREATE_AND_WAIT)) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_INSEG_ENTRY_CREATION);

            TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                            u4TunnelInstance, u4IngressId,
                                            u4EgressId, bEntryCreated);
            return CLI_FAILURE;
        }

        /* Set the direction of the in-segment created */
        if ((nmhTestv2FsMplsInSegmentDirection
             (&u4ErrorCode, &InSegmentIndex,
              (INT4) (pTeCliStBindArgs->u4Direction))) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_DIR);

            TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                            u4TunnelInstance, u4IngressId,
                                            u4EgressId, bEntryCreated);
            return CLI_FAILURE;
        }
        if (nmhSetFsMplsInSegmentDirection
            (&InSegmentIndex,
             (INT4) (pTeCliStBindArgs->u4Direction)) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_DIR);

            TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                            u4TunnelInstance, u4IngressId,
                                            u4EgressId, bEntryCreated);
            return CLI_FAILURE;
        }
    }

    do
    {
        /* Step 4b:
         * Create an OutSeg entry. Delete the created InSeg entry in case 
         * of any failure.*/
        if (((pTeCliStBindArgs->u4Direction == MPLS_DIRECTION_FORWARD) &&
             ((i4TnlRole == TE_INTERMEDIATE) || (i4TnlRole == TE_INGRESS))) ||
            ((pTeCliStBindArgs->u4Direction == MPLS_DIRECTION_REVERSE) &&
             ((i4TnlRole == TE_INTERMEDIATE) || (i4TnlRole == TE_EGRESS))))
        {
            if ((nmhGetMplsOutSegmentIndexNext (&OutSegmentIndex)) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_NO_FREE_INDEX);
                break;
            }
            if ((nmhTestv2MplsOutSegmentRowStatus
                 (&u4ErrorCode, &OutSegmentIndex,
                  MPLS_STATUS_CREATE_AND_WAIT)) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_OUTSEG_ENTRY_CREATION);
                break;
            }
            if ((nmhSetMplsOutSegmentRowStatus
                 (&OutSegmentIndex,
                  MPLS_STATUS_CREATE_AND_WAIT)) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_OUTSEG_ENTRY_CREATION);
                break;
            }
            bOutSegCreated = MPLS_SNMP_TRUE;

            if ((nmhTestv2FsMplsOutSegmentDirection
                 (&u4ErrorCode, &OutSegmentIndex,
                  (INT4) (pTeCliStBindArgs->u4Direction))) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_DIR);
                break;
            }
            if (nmhSetFsMplsOutSegmentDirection (&OutSegmentIndex,
                                                 (INT4) (pTeCliStBindArgs->
                                                         u4Direction)) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_DIR);
                break;
            }
        }

        /* Step 4c:
         * Create an XC entry. Delete the created InSeg and OutSeg entries 
         * in case of any failures.
         * */
        if (bXcDeleteFlag == TE_TRUE)
        {
            if ((nmhGetMplsXCIndexNext (&XCIndex)) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_NO_FREE_INDEX);
                break;
            }
        }
        else
        {
            MPLS_INTEGER_TO_OCTETSTRING (u4XCSegInd, (&XCIndex));
        }

        if ((nmhTestv2MplsXCRowStatus (&u4ErrorCode, &XCIndex, &InSegmentIndex,
                                       &OutSegmentIndex,
                                       MPLS_STATUS_CREATE_AND_WAIT))
            == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_XC_ENTRY_CREATION);
            break;
        }
        if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex,
                                    &OutSegmentIndex,
                                    MPLS_STATUS_CREATE_AND_WAIT)) ==
            SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_XC_ENTRY_CREATION);
            break;
        }
        bXcCreated = MPLS_SNMP_TRUE;

    }
    while (0);

    /* Delete the in-segment and/or out-segments before returning faiure 
     * when the creation of out-segment or XC fails.*/
    if (bXcCreated == MPLS_SNMP_FALSE)
    {
        if (nmhSetMplsInSegmentRowStatus (&InSegmentIndex, MPLS_STATUS_DESTROY)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set the In segment "
                       "row status active");
            return CLI_FAILURE;
        }

        if (bOutSegCreated == MPLS_SNMP_TRUE)
        {
            if (nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                               MPLS_STATUS_DESTROY)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n%%Unable to set the out segment "
                           "row status active");
                return CLI_FAILURE;
            }
        }

        TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                        u4TunnelInstance, u4IngressId,
                                        u4EgressId, bEntryCreated);
        return CLI_FAILURE;
    }

    /*
     * Step 5:
     * Populate the InSeg, OutSeg, Label stack and XC Tables
     * Make the InSeg, OutSeg and XC Tables active
     * */
    do
    {
        if (bXcDeleteFlag == TRUE)
        {
            /*  Update the XC Entry OID */
            CLI_MPLS_OCTETSTRING_TO_INTEGER ((&XCIndex), u4XCTabIndex);
            CLI_MPLS_OCTETSTRING_TO_INTEGER ((&OutSegmentIndex), u4OutIndex);
            CLI_MPLS_OCTETSTRING_TO_INTEGER ((&InSegmentIndex), u4InIndex);
            MPLS_INTEGER_TO_OID (au4XCTableOid, u4XCTabIndex,
                                 TE_TNL_XC_TABLE_DEF_OFFSET);
            MPLS_INTEGER_TO_OID (au4XCTableOid, u4InIndex,
                                 TE_TNL_XC_TABLE_DEF_OFFSET + 5);
            MPLS_INTEGER_TO_OID (au4XCTableOid, u4OutIndex,
                                 TE_TNL_XC_TABLE_DEF_OFFSET + 10);
            TunnelXCPointer.pu4_OidList = au4XCTableOid;
            TunnelXCPointer.u4_Length = MPLS_TE_XC_TABLE_OFFSET;

            /* Set the tunnel row status as not in service */
            if (TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                             u4IngressId, u4EgressId,
                                             TE_NOTINSERVICE) == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n%%Unable to set MPLS tunnel row "
                           "status as Not in Service");
                break;
            }

            /* Set the Tunnel XC Pointer */
            if ((nmhTestv2MplsTunnelXCPointer (&u4ErrorCode, u4TunnelIndex,
                                               u4TunnelInstance, u4IngressId,
                                               u4EgressId,
                                               &TunnelXCPointer)) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%%Unable to set Tunnel XCPointer\r\n");
                break;
            }

            if ((nmhSetMplsTunnelXCPointer (u4TunnelIndex, u4TunnelInstance,
                                            u4IngressId, u4EgressId,
                                            &TunnelXCPointer)) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%%Unable to set Tunnel XCPointer\r\n");
                break;
            }
        }

        /* Create the Label Stack and update the index in XC entry of 
         * service tunnel when it is stacked over H-LSP.*/
        if (pTeCliStBindArgs->u4Operation == TE_OPER_STACK)
        {
            /* Update the Label Stack Table Entry */
            u4LblStkIndex = MplsLblStackGetIndex ();

            MPLS_INTEGER_TO_OCTETSTRING (u4LblStkIndex,
                                         (&MplsLabelStackIndexNext));

            /* Set the Label Stack Row status as Create and Wait */
            if ((nmhTestv2MplsLabelStackRowStatus
                 (&u4ErrorCode, &MplsLabelStackIndexNext, MPLS_ONE,
                  TE_CREATEANDWAIT)) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_LBLSTK_RS);
                break;
            }
            if ((nmhSetMplsLabelStackRowStatus
                 (&MplsLabelStackIndexNext, MPLS_ONE,
                  TE_CREATEANDWAIT)) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_LBLSTK_RS);
                break;
            }

            if (nmhSetMplsLabelStackStorageType (&MplsLabelStackIndexNext,
                                                 MPLS_ONE,
                                                 MPLS_STORAGE_NONVOLATILE) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_LBLSTK_RS);
                break;
            }

            /* Update the Label stack Label with Service tunnel Label */
            if ((nmhTestv2MplsLabelStackLabel
                 (&u4ErrorCode, &MplsLabelStackIndexNext, MPLS_ONE,
                  pTeCliStBindArgs->u4OutLabel)) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_LBLSTK_LBL);
                break;
            }
            if ((nmhSetMplsLabelStackLabel
                 (&MplsLabelStackIndexNext, MPLS_ONE,
                  pTeCliStBindArgs->u4OutLabel)) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_LBLSTK_LBL);
                break;
            }

            /* Make the Label stack RS as active */
            if ((nmhTestv2MplsLabelStackRowStatus
                 (&u4ErrorCode, &MplsLabelStackIndexNext, MPLS_ONE,
                  TE_ACTIVE)) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_LBLSTK_RS);
                break;
            }
            if ((nmhSetMplsLabelStackRowStatus
                 (&MplsLabelStackIndexNext,
                  MPLS_ONE, TE_ACTIVE)) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_LBLSTK_RS);
                break;
            }
            if ((nmhTestv2MplsXCLabelStackIndex
                 (&u4ErrorCode, &XCIndex, &InSegmentIndex, &OutSegmentIndex,
                  &MplsLabelStackIndexNext)) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r %%Unable to create Label stacking\r\n");
                break;
            }
            if ((nmhSetMplsXCLabelStackIndex
                 (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                  &MplsLabelStackIndexNext)) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r %%Unable to create Label stacking\r\n");
                break;
            }
        }

        if ((((u1HlspOrSlspTnlRole == TE_INGRESS) &&
              (pTeCliStBindArgs->u4Direction == MPLS_DIRECTION_FORWARD)) ||
             ((u1HlspOrSlspTnlRole == TE_EGRESS) &&
              (pTeCliStBindArgs->u4Direction == MPLS_DIRECTION_REVERSE)) ||
             ((u1HlspOrSlspTnlRole == TE_EGRESS) &&
              (pTeHlspOrSlspTnlInfo->u4TnlMode ==
               TE_TNL_MODE_COROUTED_BIDIRECTIONAL) &&
              (pTeCliStBindArgs->u4Direction == MPLS_DIRECTION_FORWARD))) &&
            (i4TnlRole == TE_INTERMEDIATE))
        {
            /* Set inlabel value */
            if ((nmhTestv2MplsInSegmentLabel
                 (&u4ErrorCode, &InSegmentIndex,
                  pTeCliStBindArgs->u4InLabel)) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_LABEL);
                break;
            }
            if ((nmhSetMplsInSegmentLabel
                 (&InSegmentIndex,
                  pTeCliStBindArgs->u4InLabel)) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_LABEL);
                break;
            }

            if (nmhSetMplsInSegmentNPop (&InSegmentIndex, TE_ONE)
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_LABEL);
                break;
            }

            /* Setting the InSegment ip addr family to ipv4 */
            if (nmhSetMplsInSegmentAddrFamily (&InSegmentIndex, LSR_ADDR_IPV4)
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_LABEL);
                break;
            }
        }

        if ((((u1HlspOrSlspTnlRole == TE_EGRESS) &&
              (pTeCliStBindArgs->u4Direction == MPLS_DIRECTION_FORWARD)) ||
             ((u1HlspOrSlspTnlRole == TE_INGRESS) &&
              (pTeCliStBindArgs->u4Direction == MPLS_DIRECTION_REVERSE))) &&
            (i4TnlRole == TE_INTERMEDIATE))
        {
            /* Set outsegment top label */
            if (nmhTestv2MplsOutSegmentTopLabel
                (&u4ErrorCode, &OutSegmentIndex,
                 pTeCliStBindArgs->u4OutLabel) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_LABEL);
                break;
            }
            if (nmhSetMplsOutSegmentTopLabel
                (&OutSegmentIndex,
                 pTeCliStBindArgs->u4OutLabel) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_ERR_OUTSEG_LABEL);
                break;
            }
            /* set out segment push top label to true */
            if (nmhTestv2MplsOutSegmentPushTopLabel
                (&u4ErrorCode, &OutSegmentIndex, MPLS_TRUE) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_PUSH_TOP_LABEL);
                break;
            }
            if (nmhSetMplsOutSegmentPushTopLabel (&OutSegmentIndex,
                                                  MPLS_TRUE) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_PUSH_TOP_LABEL);
                break;
            }

            if (pTeCliStBindArgs->u4NextHopAddr != 0)
            {
                /* set the next hop addr type to ipv4 */
                if (nmhSetMplsOutSegmentNextHopAddrType (&OutSegmentIndex,
                                                         LSR_ADDR_IPV4) ==
                    SNMP_FAILURE)
                {
                    CLI_SET_ERR (MPLS_CLI_TE_ERR_NEXT_HOP_ADDR);
                    break;
                }
                /* set nexthop address */
                MPLS_INTEGER_TO_OCTETSTRING (pTeCliStBindArgs->u4NextHopAddr,
                                             (&NextHopAddr));
                if (nmhTestv2MplsOutSegmentNextHopAddr
                    (&u4ErrorCode, &OutSegmentIndex,
                     &NextHopAddr) == SNMP_FAILURE)
                {
                    CLI_SET_ERR (MPLS_CLI_TE_ERR_NEXT_HOP_ADDR);
                    break;
                }
                if (nmhSetMplsOutSegmentNextHopAddr (&OutSegmentIndex,
                                                     &NextHopAddr) ==
                    SNMP_FAILURE)
                {
                    CLI_SET_ERR (MPLS_CLI_TE_ERR_NEXT_HOP_ADDR);
                    break;
                }
            }
        }

        /* Check whether the entry exits in Tunnel Map table. The stitching 
         * of co-routed bidirectional tunnels can be done only when XCs
         * are created for both the directions.*/
        if ((i4TunnelMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL) &&
            (pTeCliStBindArgs->u4Operation == TE_OPER_STITCH))
        {
            teSetFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelIndex =
                u4HlspOrSlspTnlIndex;
            teSetFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelInstance =
                u4HlspOrSlspTnlInstance;
            teSetFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelIngressLSRId =
                u4HlspOrSlspIngressId;
            teSetFsMplsLSPMapTunnelTable.u4FsMplsLSPMapTunnelEgressLSRId =
                u4HlspOrSlspEgressId;
            teSetFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelIndex =
                u4TunnelIndex;
            teSetFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelInstance =
                u4TunnelInstance;
            teSetFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelIngressLSRId =
                u4IngressId;
            teSetFsMplsLSPMapTunnelTable.u4FsMplsLSPMapSubTunnelEgressLSRId =
                u4EgressId;

            if (TeGetAllFsMplsLSPMapTunnelTable
                (&teSetFsMplsLSPMapTunnelTable) != SNMP_SUCCESS)
            {
                u4RowStatus = TE_CREATEANDWAIT;
                *pu4Operation = TE_ZERO;
            }
        }

        /* Updating fsMplsLSPMapTunnelTable - this updates the 
         * service tunnel OutSegment from H-LSP OutSegment when the 
         * operation is stack and e2e LSP OutSegment/InSegment from S-LSP 
         * OutSegment/InSegment when the operation is stitch.*/
        MEMCPY (&HlspTnlCliArgs.teSetFsMplsLSPMapTunnelTable,
                &teSetFsMplsLSPMapTunnelTable,
                sizeof (tteFsMplsLSPMapTunnelTable));
        MEMCPY (&HlspTnlCliArgs.teIsSetFsMplsLSPMapTunnelTable,
                &teIsSetFsMplsLSPMapTunnelTable,
                sizeof (teIsSetFsMplsLSPMapTunnelTable));
        HlspTnlCliArgs.u4SetHlspTnlIndex = u4HlspOrSlspTnlIndex;
        HlspTnlCliArgs.u4SetHlspTnlInstance = u4HlspOrSlspTnlInstance;
        HlspTnlCliArgs.u4SetHlspTnlIngressId = u4HlspOrSlspIngressId;
        HlspTnlCliArgs.u4SetHlspTnlEgressId = u4HlspOrSlspEgressId;
        HlspTnlCliArgs.u4IsSetHlspTnlIndex = u4TunnelIndex;
        HlspTnlCliArgs.u4IsSetHlspTnlInstance = u4TunnelInstance;
        HlspTnlCliArgs.u4IsSetHlspTnlIngressId = u4IngressId;
        HlspTnlCliArgs.u4IsSetHlspTnlEgressId = u4EgressId;
        HlspTnlCliArgs.u4Operation = *pu4Operation;
        HlspTnlCliArgs.u4RowStatus = u4RowStatus;

        TeFillFsMplsLspMapTnlTable (&HlspTnlCliArgs);

        if (TeTestAllFsMplsLSPMapTunnelTable
            (&u4ErrorCode, &HlspTnlCliArgs.teSetFsMplsLSPMapTunnelTable,
             &HlspTnlCliArgs.teIsSetFsMplsLSPMapTunnelTable, OSIX_TRUE,
             OSIX_TRUE) != SNMP_SUCCESS)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_LSP_MAP);
            break;
        }

        if (TeSetAllFsMplsLSPMapTunnelTable
            (&HlspTnlCliArgs.teSetFsMplsLSPMapTunnelTable,
             &HlspTnlCliArgs.teIsSetFsMplsLSPMapTunnelTable,
             OSIX_TRUE, OSIX_TRUE) != SNMP_SUCCESS)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_LSP_MAP);
            break;
        }

        if (((pTeCliStBindArgs->u4Direction == MPLS_DIRECTION_FORWARD) &&
             ((i4TnlRole == TE_INTERMEDIATE) || (i4TnlRole == TE_EGRESS))) ||
            ((pTeCliStBindArgs->u4Direction == MPLS_DIRECTION_REVERSE) &&
             ((i4TnlRole == TE_INTERMEDIATE) || (i4TnlRole == TE_INGRESS))))
        {
            /* Interface creation */
            /* Get free insegment interface and stack over mpls interface */
            if (CfaGetFreeInterfaceIndex (&u4MplsTnlInIfIndex,
                                          CFA_MPLS_TUNNEL) == OSIX_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_NO_FREE_TUNNEL_INTERFACE);
                break;
            }
            if ((nmhTestv2MplsInSegmentInterface (&u4ErrorCode,
                                                  &InSegmentIndex,
                                                  (INT4) u4MplsTnlInIfIndex)) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_IF);
                break;
            }
            if (nmhSetMplsInSegmentInterface (&InSegmentIndex,
                                              (INT4) u4MplsTnlInIfIndex) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_IF);
                break;
            }

            if (MplsCreateMplsIfOrMplsTnlIf (pTeCliStBindArgs->u4InIfIndex,
                                             u4MplsTnlInIfIndex,
                                             CFA_MPLS_TUNNEL) == MPLS_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_ERR_INSTACK_ENTRY_CREATION);
                break;
            }
            bInTnlCreated = MPLS_SNMP_TRUE;

            if (u4RowStatus == TE_ACTIVE)
            {
                /* Setting the InSegment ip addr family to ipv4 */
                if (nmhSetMplsInSegmentAddrFamily
                    (&InSegmentIndex, LSR_ADDR_IPV4) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "\r\n%%Unable to set the In segment "
                               "Addr Family");
                    break;
                }

                if (nmhSetMplsInSegmentStorageType (&InSegmentIndex,
                                                    MPLS_STORAGE_NONVOLATILE) ==
                    SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "\r\n%%Unable to set the In segment "
                               "Storage Type");
                    break;
                }

                /* Set the row status of the InSegment as active */
                if ((nmhTestv2MplsInSegmentRowStatus
                     (&u4ErrorCode, &InSegmentIndex,
                      MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "\r\n%%Unable to set the In segment "
                               "row status active");
                    break;
                }
                if ((nmhSetMplsInSegmentRowStatus
                     (&InSegmentIndex, MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "\r\n%%Unable to set the In segment "
                               "row status active");
                    break;
                }
            }
        }

        if (((pTeCliStBindArgs->u4Direction == MPLS_DIRECTION_FORWARD) &&
             ((i4TnlRole == TE_INTERMEDIATE) || (i4TnlRole == TE_INGRESS))) ||
            ((pTeCliStBindArgs->u4Direction == MPLS_DIRECTION_REVERSE) &&
             ((i4TnlRole == TE_INTERMEDIATE) || (i4TnlRole == TE_EGRESS))))
        {

            if ((bIntfCreationReqd == TRUE) &&
                (CfaGetFreeInterfaceIndex (&u4MplsTnlOutIfIndex,
                                           CFA_MPLS_TUNNEL) == OSIX_FAILURE))
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_NO_FREE_TUNNEL_INTERFACE);
                break;
            }

            if ((nmhTestv2MplsOutSegmentInterface
                 (&u4ErrorCode, &OutSegmentIndex,
                  (INT4) u4MplsTnlOutIfIndex)) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_IF);
                break;
            }
            if ((nmhSetMplsOutSegmentInterface (&OutSegmentIndex,
                                                (INT4) u4MplsTnlOutIfIndex)) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_IF);
                break;
            }

            if (pTeCliStBindArgs->u4Operation == TE_OPER_STACK)
            {
                /* Stack the mplsTunnel Interface over the already created
                 * Tunnel Interface */
                if (bIntfCreationReqd == FALSE)
                {
                    if (MplsCreateTnlIntfOverHLSP
                        (pTeCliStBindArgs->u4OutIfIndex,
                         u4MplsTnlOutIfIndex) == MPLS_FAILURE)
                    {
                        CliPrintf (CliHandle, "\r %%Unable to stack tunnel "
                                   "interface\r\n");
                        break;
                    }
                }
                /* Create a new MPLS Tunnel Interface and stack over MPLS If */
                else if (MplsCreateMplsIfOrMplsTnlIf
                         (pTeCliStBindArgs->u4OutIfIndex, u4MplsTnlOutIfIndex,
                          CFA_MPLS_TUNNEL) == MPLS_FAILURE)
                {
                    CLI_SET_ERR (MPLS_CLI_ERR_OUTSTACK_ENTRY_CREATION);
                    break;
                }
            }
            else
            {
                if (MplsCreateMplsIfOrMplsTnlIf
                    (pTeCliStBindArgs->u4OutIfIndex,
                     u4MplsTnlOutIfIndex, CFA_MPLS_TUNNEL) == MPLS_FAILURE)
                {
                    CLI_SET_ERR (MPLS_CLI_ERR_OUTSTACK_ENTRY_CREATION);
                    break;
                }
            }

            bOutTnlCreated = MPLS_SNMP_TRUE;

            if (u4RowStatus == TE_ACTIVE)
            {
                if (nmhSetMplsOutSegmentStorageType (&OutSegmentIndex,
                                                     MPLS_STORAGE_NONVOLATILE)
                    == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "\r\n%%Unable to set the out segment "
                               "Storage Type ");
                    break;
                }

                /* updating the rowstatus of outsegment */
                if ((nmhTestv2MplsOutSegmentRowStatus
                     (&u4ErrorCode, &OutSegmentIndex,
                      MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "\r\n%%Unable to set the out segment "
                               "row status active");
                    break;
                }
                if ((nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                                    MPLS_STATUS_ACTIVE)) ==
                    SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "\r\n%%Unable to set the out segment "
                               "row status active");
                    break;
                }
            }
        }

        if (u4RowStatus == TE_ACTIVE)
        {

            if (nmhSetMplsXCStorageType (&XCIndex, &InSegmentIndex,
                                         &OutSegmentIndex,
                                         MPLS_STORAGE_NONVOLATILE) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n%%Unable to set the out segment "
                           "storage type");
                break;
            }

            /* Make the Cross connect row status as active */
            if ((nmhTestv2MplsXCRowStatus
                 (&u4ErrorCode, &XCIndex, &InSegmentIndex, &OutSegmentIndex,
                  MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%%Unable to set the XC row status active");
                break;
            }
            if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex,
                                        &OutSegmentIndex,
                                        MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%%Unable to set the XC row status active");
                break;
            }
        }

        /* Make the reverse Xc, In and Out segments of Co-routed bidirectioanl 
         * tunnel also active. All the four segments(Fwd In, Fwd Out, Rev In 
         * and Rev out) are stitched only after the four segments are configured. 
         * */
        if ((u4RowStatus == ACTIVE) &&
            (i4TunnelMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL) &&
            (pTeCliStBindArgs->u4Operation == TE_OPER_STITCH))
        {
            if (pTeCliStBindArgs->u4Direction == MPLS_DIRECTION_REVERSE)
            {
                i4Direction = MPLS_DIRECTION_FORWARD;
            }
            else
            {
                i4Direction = MPLS_DIRECTION_REVERSE;
            }

            if (TeCliActivateXc (CliHandle, u4TunnelIndex,
                                 u4TunnelInstance, u4IngressId,
                                 u4EgressId,
                                 (UINT4) i4Direction) == CLI_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\nAcitvating the XC, In and Out segments "
                           "failed\n");
                break;
            }
        }

        /* 
         * Step 6:
         * If all set operations have been successful , return success.
         * */
        return CLI_SUCCESS;
    }
    while (0);

    /* 
     * Step 7:
     * If all set operations are NOT  successful , Delete the created 
     * InSegment , OutSegment and XC entry 
     * Then return failure.
     * */
    u4RowStatus = TE_DESTROY;

    MEMCPY (&HlspTnlCliArgs.teSetFsMplsLSPMapTunnelTable,
            &teSetFsMplsLSPMapTunnelTable, sizeof (tteFsMplsLSPMapTunnelTable));
    MEMCPY (&HlspTnlCliArgs.teIsSetFsMplsLSPMapTunnelTable,
            &teIsSetFsMplsLSPMapTunnelTable,
            sizeof (teIsSetFsMplsLSPMapTunnelTable));
    HlspTnlCliArgs.u4SetHlspTnlIndex = u4HlspOrSlspTnlIndex;
    HlspTnlCliArgs.u4SetHlspTnlInstance = u4HlspOrSlspTnlInstance;
    HlspTnlCliArgs.u4SetHlspTnlIngressId = u4HlspOrSlspIngressId;
    HlspTnlCliArgs.u4SetHlspTnlEgressId = u4HlspOrSlspEgressId;
    HlspTnlCliArgs.u4IsSetHlspTnlIndex = u4TunnelIndex;
    HlspTnlCliArgs.u4IsSetHlspTnlInstance = u4TunnelInstance;
    HlspTnlCliArgs.u4IsSetHlspTnlIngressId = u4IngressId;
    HlspTnlCliArgs.u4IsSetHlspTnlEgressId = u4EgressId;
    HlspTnlCliArgs.u4Operation = pTeCliStBindArgs->u4Operation;
    HlspTnlCliArgs.u4RowStatus = u4RowStatus;

    TeFillFsMplsLspMapTnlTable (&HlspTnlCliArgs);

    TeSetAllFsMplsLSPMapTunnelTable (&teSetFsMplsLSPMapTunnelTable,
                                     &teIsSetFsMplsLSPMapTunnelTable, OSIX_TRUE,
                                     OSIX_TRUE);

    if (bInTnlCreated == MPLS_SNMP_TRUE)
    {
        MplsDeleteMplsIfOrMplsTnlIf (pTeCliStBindArgs->u4InIfIndex,
                                     u4MplsTnlInIfIndex, CFA_MPLS_TUNNEL,
                                     MPLS_TRUE);
    }

    if (bOutTnlCreated == MPLS_SNMP_TRUE)
    {
        if (pTeCliStBindArgs->u4Operation == TE_OPER_STACK)
        {
            MplsDeleteTnlIntfOverHLSP (pTeCliStBindArgs->u4OutIfIndex,
                                       u4MplsTnlOutIfIndex, MPLS_FALSE);
        }
        else
        {
            MplsDeleteMplsIfOrMplsTnlIf (pTeCliStBindArgs->u4OutIfIndex,
                                         u4MplsTnlOutIfIndex, CFA_MPLS_TUNNEL,
                                         MPLS_FALSE);
        }
    }

    if (nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               MPLS_STATUS_DESTROY) == SNMP_FAILURE)
    {
        /*Failure Condition not handled here */
    }

    /* Reset the Tunnel XC pointer to 0.0 */
    u4XCTabIndex = 0;
    u4OutIndex = 0;
    u4InIndex = 0;
    MPLS_INTEGER_TO_OID (au4XCTableOid, u4XCTabIndex,
                         TE_TNL_XC_TABLE_DEF_OFFSET);
    MPLS_INTEGER_TO_OID (au4XCTableOid, u4OutIndex,
                         TE_TNL_XC_TABLE_DEF_OFFSET + 5);
    MPLS_INTEGER_TO_OID (au4XCTableOid, u4InIndex,
                         TE_TNL_XC_TABLE_DEF_OFFSET + 10);
    TunnelXCPointer.pu4_OidList = au4XCTableOid;
    TunnelXCPointer.u4_Length = MPLS_TE_XC_TABLE_OFFSET;

    if (nmhSetMplsTunnelXCPointer (u4TunnelIndex, u4TunnelInstance,
                                   u4IngressId, u4EgressId,
                                   &TunnelXCPointer) == SNMP_FAILURE)
    {
        /*Failure Condition not handled here */
    }

    if (nmhSetMplsInSegmentRowStatus (&InSegmentIndex, MPLS_STATUS_DESTROY)
        == SNMP_FAILURE)
    {
        /*Failure Condition not handled here */
    }
    if (nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex, MPLS_STATUS_DESTROY)
        == SNMP_FAILURE)
    {
        /*Failure Condition not handled here */
    }

    TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                    u4TunnelInstance, u4IngressId,
                                    u4EgressId, bEntryCreated);
    return CLI_FAILURE;
}

/******************************************************************************
* Function Name : TeCliStaticBindIngressStack
* Description   : This routine is used to associate XC Entries with that of tunnel. 
* Input(s)      : CliHandle       - Cli Context Handle
*               : u4OutLabel       - Label associated with the outgoing packet
*               : u4HlspTnlId     - HLSP Tunnel Id on which the tunnel has to 
*                                   be stacked on to
*               : u4MplsTnlDir    - Direction of the Service tunnel
*               : u1TnlPathType   - Tunnel Path Type - Protection or Working
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/

INT4
TeCliStaticBindIngressStack (tCliHandle CliHandle, UINT4 u4OutLabel,
                             UINT4 u4HlspTnlId, UINT4 u4MplsTnlDir,
                             UINT1 u1TnlPathType)
{
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCSegmentIndex;
    tSNMP_OCTET_STRING_TYPE MplsLabelStackIndexNext;
    tSNMP_OID_TYPE      TunnelXCPointer;
    tSNMP_OID_TYPE      XCSegIndex;
    UINT4               u4HlspTunnelIndex = TE_ZERO;
    UINT4               u4HlspTunnelInstance = TE_ZERO;
    UINT4               u4HlspIngressId = TE_ZERO;
    UINT4               u4HlspEgressId = TE_ZERO;
    UINT4               u4Operation = TE_ZERO;
    UINT4               u4RowStatus = TE_ZERO;
    static UINT4        au4XCTableOid[MPLS_TE_XC_TABLE_OFFSET] =
        { 1, 3, 6, 1, 2, 1, 10, 166, 2, 1, 10, 1, 4, 4, 0, 0, 0, 0, 4, 0, 0, 0,
        0, 4, 0, 0, 0, 0
    };

    UINT4               u4TunnelIndex = TE_ZERO;
    UINT4               u4TunnelInstance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    UINT4               u4XCTabIndex = TE_ZERO;
    UINT4               u4InIndex = TE_ZERO;
    UINT4               u4OutIndex = TE_ZERO;
    UINT4               u4XCSegInd = TE_ZERO;
    UINT4               u4InSegInd = TE_ZERO;
    UINT4               u4OutSegInd = TE_ZERO;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlInfo         *pTeHlspTnlInfo = NULL;
    UINT4               u4ErrorCode = TE_ZERO;
    UINT4               u4OutIfIndex = TE_ZERO;
    UINT4               u4MplsTnlIfIndex = TE_ZERO;
    INT4                i4TnlRole = TE_ZERO;
    INT4                i4Direction = MPLS_DIRECTION_FORWARD;
    INT4                i4HlspOutSegIntf = TE_ZERO;
    UINT4               u4LblStkIndex = TE_ZERO;
    INT4                i4TunnelMode = TE_ZERO;
    BOOL1               bXcDeleteFlag = TRUE;
    BOOL1               b1Flag = FALSE;
    tHlspTnlCliArgs     HlspTnlCliArgs;
    tteFsMplsLSPMapTunnelTable teSetFsMplsLSPMapTunnelTable;
    tteIsSetFsMplsLSPMapTunnelTable teIsSetFsMplsLSPMapTunnelTable;
    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1LblStackIndex[MPLS_INDEX_LENGTH];
    static UINT4        au4XCIndexx[MPLS_TE_XC_TABLE_OFFSET];

    UINT4               u4CreatedInstance = TE_ZERO;
    BOOL1               bEntryCreated = FALSE;

    XCSegIndex.pu4_OidList = au4XCIndexx;
    XCSegIndex.u4_Length = 0;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    XCIndex.pu1_OctetList = au1XCIndex;
    XCIndex.i4_Length = MPLS_INDEX_LENGTH;
    MplsLabelStackIndexNext.pu1_OctetList = au1LblStackIndex;
    MplsLabelStackIndexNext.i4_Length = MPLS_INDEX_LENGTH;

    MEMSET (&HlspTnlCliArgs, 0, sizeof (tHlspTnlCliArgs));
    MEMSET (&teSetFsMplsLSPMapTunnelTable, 0,
            sizeof (tteFsMplsLSPMapTunnelTable));
    MEMSET (&teIsSetFsMplsLSPMapTunnelTable, 0,
            sizeof (tteIsSetFsMplsLSPMapTunnelTable));
    /*
     * >  Get the TunnelId,TunnelInstance,Ingress and Egress using the TunnelName.
     */
    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
    pTeTnlInfo = TeGetTunnelInfo (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                  u4EgressId);
    if (pTeTnlInfo == NULL)
    {
        return CLI_FAILURE;
    }
    if (nmhGetFsMplsTunnelMode (u4TunnelIndex, u4TunnelInstance,
                                u4IngressId, u4EgressId, &i4TunnelMode)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Tunnel Mode is not available.\r\n");
        return CLI_FAILURE;
    }

    /* If tunnel Role is not ingress, return error */
    if (nmhGetMplsTunnelRole (u4TunnelIndex, u4TunnelInstance,
                              u4IngressId, u4EgressId,
                              &i4TnlRole) == SNMP_SUCCESS)
    {
        if ((i4TnlRole == TE_EGRESS) &&
            (i4TunnelMode != TE_TNL_MODE_COROUTED_BIDIRECTIONAL))
        {
            CliPrintf (CliHandle,
                       "\r\n%%Wrong label configuration for Egress tunnel\r\n");
            return CLI_FAILURE;
        }
        else if (i4TnlRole == TE_INTERMEDIATE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Wrong label configuration for Intermediate "
                       "tunnel\r\n");
            return CLI_FAILURE;
        }
    }

    /*  : Get the HLSP Tunnel info */
    MPLS_CMN_LOCK ();

    pTeHlspTnlInfo =
        TeGetTnlInfoByOwnerAndRole (u4HlspTnlId, u4HlspTunnelInstance,
                                    u4HlspIngressId, u4HlspEgressId,
                                    TE_TNL_OWNER_SNMP, TE_ZERO);
    if (pTeHlspTnlInfo == NULL)
    {
        CliPrintf (CliHandle, "\r\n%%HLSP Tunnel Does Not Exist\r\n");
        CliPrintf (CliHandle,
                   "\r%%HLSP Tunnel must be created before attempting to do this operation.\r\n");
        MPLS_CMN_UNLOCK ();
        return CLI_FAILURE;
    }

    /* Get the HLSP tunnel indices */
    u4HlspTunnelIndex = TE_TNL_TNL_INDEX (pTeHlspTnlInfo);
    u4HlspTunnelInstance = TE_TNL_TNL_INSTANCE (pTeHlspTnlInfo);

    MEMCPY ((UINT1 *) (&u4HlspIngressId),
            TE_TNL_INGRESS_LSRID (pTeHlspTnlInfo), ROUTER_ID_LENGTH);
    MEMCPY ((UINT1 *) (&u4HlspEgressId),
            TE_TNL_EGRESS_LSRID (pTeHlspTnlInfo), ROUTER_ID_LENGTH);

    u4HlspIngressId = OSIX_NTOHL (u4HlspIngressId);
    u4HlspEgressId = OSIX_NTOHL (u4HlspEgressId);

    MPLS_CMN_UNLOCK ();

    nmhGetMplsTunnelPrimaryInstance (u4HlspTunnelIndex, u4HlspTunnelInstance,
                                     u4HlspIngressId, u4HlspEgressId,
                                     &u4HlspTunnelInstance);

    if (u4TunnelInstance == TE_ZERO)
    {
        if (TeCliNormalCreateNonZeroInstTunnel (CliHandle, u4TunnelIndex,
                                                u4TunnelInstance, u4IngressId,
                                                u4EgressId, u1TnlPathType,
                                                &u4CreatedInstance,
                                                &bEntryCreated) == CLI_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to create non-zero instance tunnel\n");
            return CLI_FAILURE;
        }

        u4TunnelInstance = u4CreatedInstance;
    }

    /* Using the service/e2e LSP tunnel indices, get the tunnel XC pointer.
     * From the XCPointer value obtain the XCIndex,InSegment and OutSegment 
     * Index.
     */
    if (nmhGetMplsTunnelXCPointer (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                   u4EgressId, &XCSegIndex) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to get tunnel XC Pointer\r\n");

        TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                        u4TunnelInstance, u4IngressId,
                                        u4EgressId, bEntryCreated);
        return CLI_FAILURE;
    }

    MplsGetSegFromTunnelXC (&XCSegIndex, &XCSegmentIndex, &InSegmentIndex,
                            &OutSegmentIndex, &u4XCSegInd, &u4InSegInd,
                            &u4OutSegInd);

    if ((i4TunnelMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
        && (u4MplsTnlDir == MPLS_CLI_DIRECTION_REVERSE))
    {
        u4MplsTnlDir = MPLS_DIRECTION_REVERSE;
        b1Flag = TRUE;
    }
    else
    {
        u4MplsTnlDir = MPLS_DIRECTION_FORWARD;
    }
    /* Check whether the XC is already exists for the opposite direction 
     * for Co-routed bi-directional tunnel to use the same XC index to 
     * create the XC entry for the given direction.*/
    if (i4TunnelMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
    {
        if (u4InSegInd != 0)
        {
            if ((nmhGetFsMplsInSegmentDirection
                 (&InSegmentIndex, &i4Direction) == SNMP_SUCCESS) &&
                (i4Direction != (INT4) u4MplsTnlDir))
            {
                bXcDeleteFlag = FALSE;
            }
        }

        if (u4OutSegInd != 0)
        {
            if ((nmhGetFsMplsOutSegmentDirection
                 (&OutSegmentIndex, &i4Direction) == SNMP_SUCCESS) &&
                (i4Direction != (INT4) u4MplsTnlDir))
            {
                bXcDeleteFlag = FALSE;
            }
        }
    }

    if (b1Flag == TRUE)
    {
        MEMSET (&InSegmentIndex, 0, sizeof (tSNMP_OCTET_STRING_TYPE));
    }

    /* If ILM already exist, return error message */
    if ((u4XCSegInd != 0) && (bXcDeleteFlag == TE_TRUE))
    {
        if (u4OutSegInd != 0)
        {
            CliPrintf (CliHandle,
                       "\n\t OutSegment already exist. Delete the tunnel "
                       "and re create to modify the Outsegment\n");
            return CLI_SUCCESS;
        }
    }

    /* create an OutSeg entry */
    if ((nmhGetMplsOutSegmentIndexNext (&OutSegmentIndex)) == SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_NO_FREE_INDEX);

        TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                        u4TunnelInstance, u4IngressId,
                                        u4EgressId, bEntryCreated);
        return CLI_FAILURE;
    }
    if ((nmhTestv2MplsOutSegmentRowStatus (&u4ErrorCode, &OutSegmentIndex,
                                           MPLS_STATUS_CREATE_AND_WAIT)) ==
        SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_OUTSEG_ENTRY_CREATION);

        TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                        u4TunnelInstance, u4IngressId,
                                        u4EgressId, bEntryCreated);
        return CLI_FAILURE;
    }
    if ((nmhSetMplsOutSegmentRowStatus
         (&OutSegmentIndex, MPLS_STATUS_CREATE_AND_WAIT)) == SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_OUTSEG_ENTRY_CREATION);
        return CLI_FAILURE;
    }

    /* create an XC entry */
    if (bXcDeleteFlag == TE_TRUE)
    {
        if ((nmhGetMplsXCIndexNext (&XCIndex)) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_NO_FREE_INDEX);
            if (nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                               MPLS_STATUS_DESTROY) ==
                SNMP_FAILURE)
            {
                /*Failure Condition not handled here */
            }

            TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                            u4TunnelInstance, u4IngressId,
                                            u4EgressId, bEntryCreated);
            return CLI_FAILURE;
        }
    }
    else
    {
        MPLS_INTEGER_TO_OCTETSTRING (u4XCSegInd, (&XCIndex));
    }
    if ((nmhTestv2MplsXCRowStatus (&u4ErrorCode, &XCIndex, &InSegmentIndex,
                                   &OutSegmentIndex,
                                   MPLS_STATUS_CREATE_AND_WAIT))
        == SNMP_FAILURE)
    {
        CLI_SET_ERR (MPLS_CLI_XC_ENTRY_CREATION);

        /* delete the created OutSeg entry in case of failure */
        if (nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                           MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            /*Failure Condition not handled here */
        }

        TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                        u4TunnelInstance, u4IngressId,
                                        u4EgressId, bEntryCreated);
        return CLI_FAILURE;
    }
    if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex,
                                &OutSegmentIndex,
                                MPLS_STATUS_CREATE_AND_WAIT)) == SNMP_FAILURE)
    {
        /* delete the created OutSeg entry */
        if (nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                           MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            /*Failure Condition not handled here */
        }
        CLI_SET_ERR (MPLS_CLI_XC_ENTRY_CREATION);

        TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                        u4TunnelInstance, u4IngressId,
                                        u4EgressId, bEntryCreated);
        return CLI_FAILURE;
    }

    /* Populate XC, outsegment and label stack entry */
    do
    {
        if (bXcDeleteFlag == TRUE)
        {
            /* update the XC Table entry OID */
            CLI_MPLS_OCTETSTRING_TO_INTEGER ((&XCIndex), u4XCTabIndex);
            CLI_MPLS_OCTETSTRING_TO_INTEGER ((&OutSegmentIndex), u4OutIndex);
            CLI_MPLS_OCTETSTRING_TO_INTEGER ((&InSegmentIndex), u4InIndex);
            MPLS_INTEGER_TO_OID (au4XCTableOid, u4XCTabIndex,
                                 TE_TNL_XC_TABLE_DEF_OFFSET);
            MPLS_INTEGER_TO_OID (au4XCTableOid, u4InIndex,
                                 TE_TNL_XC_TABLE_DEF_OFFSET + 5);
            MPLS_INTEGER_TO_OID (au4XCTableOid, u4OutIndex,
                                 TE_TNL_XC_TABLE_DEF_OFFSET + 10);
            TunnelXCPointer.pu4_OidList = au4XCTableOid;
            TunnelXCPointer.u4_Length = MPLS_TE_XC_TABLE_OFFSET;

            /* Set the tunnel Row status as NOT_IN_SERVICE */
            if (TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                             u4IngressId, u4EgressId,
                                             TE_NOTINSERVICE) == CLI_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n%%Unable to set MPLS tunnel row "
                           "status as Not in Service");
                break;
            }

            /* Update the XC pointer */
            if ((nmhTestv2MplsTunnelXCPointer (&u4ErrorCode, u4TunnelIndex,
                                               u4TunnelInstance, u4IngressId,
                                               u4EgressId,
                                               &TunnelXCPointer)) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%%Unable to set Tunnel XCPointer\r\n");
                break;
            }
            if ((nmhSetMplsTunnelXCPointer (u4TunnelIndex,
                                            u4TunnelInstance, u4IngressId,
                                            u4EgressId,
                                            &TunnelXCPointer)) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%%Unable to set Tunnel XCPointer\r\n");
                break;
            }
        }
        /* Create Label stack table entry */
        u4LblStkIndex = MplsLblStackGetIndex ();

        MPLS_INTEGER_TO_OCTETSTRING (u4LblStkIndex, (&MplsLabelStackIndexNext));

        /* Set the label stack Row status as Create and Wait */
        if ((nmhTestv2MplsLabelStackRowStatus
             (&u4ErrorCode, &MplsLabelStackIndexNext, MPLS_ONE,
              TE_CREATEANDWAIT)) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_LBLSTK_RS);
            break;
        }
        if ((nmhSetMplsLabelStackRowStatus (&MplsLabelStackIndexNext, MPLS_ONE,
                                            TE_CREATEANDWAIT)) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_LBLSTK_RS);
            break;
        }

        if (nmhSetMplsLabelStackStorageType (&MplsLabelStackIndexNext,
                                             MPLS_ONE,
                                             MPLS_STORAGE_NONVOLATILE) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set Label Stack Storage Type\r\n");
            break;
        }

        /* Update the service tunnel label to label stack table */
        if ((nmhTestv2MplsLabelStackLabel
             (&u4ErrorCode, &MplsLabelStackIndexNext, MPLS_ONE,
              u4OutLabel)) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_LBLSTK_LBL);
            break;
        }
        if ((nmhSetMplsLabelStackLabel (&MplsLabelStackIndexNext, MPLS_ONE,
                                        u4OutLabel)) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_LBLSTK_LBL);
            break;
        }

        /* Make the label stack row status as active */
        if ((nmhTestv2MplsLabelStackRowStatus
             (&u4ErrorCode, &MplsLabelStackIndexNext, MPLS_ONE,
              TE_ACTIVE)) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_LBLSTK_RS);
            break;
        }
        if ((nmhSetMplsLabelStackRowStatus
             (&MplsLabelStackIndexNext, MPLS_ONE, TE_ACTIVE)) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_LBLSTK_RS);
            break;
        }
        if ((nmhTestv2MplsXCLabelStackIndex
             (&u4ErrorCode, &XCIndex, &InSegmentIndex, &OutSegmentIndex,
              &MplsLabelStackIndexNext)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r %%Unable to create Label stacking\r\n");
            break;
        }
        if ((nmhSetMplsXCLabelStackIndex (&XCIndex, &InSegmentIndex,
                                          &OutSegmentIndex,
                                          &MplsLabelStackIndexNext)) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r %%Unable to create Label stacking\r\n");
            break;
        }

        if (nmhSetFsMplsOutSegmentDirection
            (&OutSegmentIndex, (INT4) u4MplsTnlDir) == SNMP_FAILURE)
        {
            /*Failure Condition not handled here */
        }

        /* Updating fsMplsLSPMapTunnelTable - This updates the outsegment of service tunnel
           same as HLSP */
        u4Operation = TE_OPER_STACK;
        u4RowStatus = TE_ACTIVE;

        MEMCPY (&HlspTnlCliArgs.teSetFsMplsLSPMapTunnelTable,
                &teSetFsMplsLSPMapTunnelTable,
                sizeof (tteFsMplsLSPMapTunnelTable));
        MEMCPY (&HlspTnlCliArgs.teIsSetFsMplsLSPMapTunnelTable,
                &teIsSetFsMplsLSPMapTunnelTable,
                sizeof (teIsSetFsMplsLSPMapTunnelTable));
        HlspTnlCliArgs.u4SetHlspTnlIndex = u4HlspTunnelIndex;
        HlspTnlCliArgs.u4SetHlspTnlInstance = u4HlspTunnelInstance;
        HlspTnlCliArgs.u4SetHlspTnlIngressId = u4HlspIngressId;
        HlspTnlCliArgs.u4SetHlspTnlEgressId = u4HlspEgressId;
        HlspTnlCliArgs.u4IsSetHlspTnlIndex = u4TunnelIndex;
        HlspTnlCliArgs.u4IsSetHlspTnlInstance = u4TunnelInstance;
        HlspTnlCliArgs.u4IsSetHlspTnlIngressId = u4IngressId;
        HlspTnlCliArgs.u4IsSetHlspTnlEgressId = u4EgressId;
        HlspTnlCliArgs.u4Operation = u4Operation;
        HlspTnlCliArgs.u4RowStatus = u4RowStatus;

        TeFillFsMplsLspMapTnlTable (&HlspTnlCliArgs);

        if (TeTestAllFsMplsLSPMapTunnelTable
            (&u4ErrorCode, &HlspTnlCliArgs.teSetFsMplsLSPMapTunnelTable,
             &HlspTnlCliArgs.teIsSetFsMplsLSPMapTunnelTable, OSIX_TRUE,
             OSIX_TRUE) != SNMP_SUCCESS)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_LSP_MAP);
            break;
        }
        if (TeSetAllFsMplsLSPMapTunnelTable
            (&HlspTnlCliArgs.teSetFsMplsLSPMapTunnelTable,
             &HlspTnlCliArgs.teIsSetFsMplsLSPMapTunnelTable,
             OSIX_TRUE, OSIX_TRUE) != SNMP_SUCCESS)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_LSP_MAP);
            break;
        }
        /* get HLSP tunnel interface index */
        if ((nmhGetMplsTunnelIfIndex (u4HlspTunnelIndex, u4HlspTunnelInstance,
                                      u4HlspIngressId, u4HlspEgressId,
                                      &i4HlspOutSegIntf)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to fetch the mpls"
                       "tunnel index");
            break;
        }
        if (u1TnlPathType == TE_TNL_WORKING_PATH)
        {
            u4MplsTnlIfIndex = gaTnlCliArgs[CliHandle].u4MplsTnlIfIndex;
        }

        if ((nmhTestv2MplsOutSegmentInterface (&u4ErrorCode,
                                               &OutSegmentIndex,
                                               (INT4) u4MplsTnlIfIndex)) ==
            SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_IF);
            break;
        }
        if ((nmhSetMplsOutSegmentInterface (&OutSegmentIndex,
                                            (INT4) u4MplsTnlIfIndex)) ==
            SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_IF);
            break;
        }
        /* Create a new MPLS Tunnel Interface and stack over MPLS If */
        if (MplsCreateTnlIntfOverHLSP
            ((UINT4) i4HlspOutSegIntf, u4MplsTnlIfIndex) == MPLS_FAILURE)
        {
            CliPrintf (CliHandle, "\r %%Unable to stack tunnel "
                       "interface\r\n");
            break;
        }

        if (nmhTestv2MplsOutSegmentStorageType (&u4ErrorCode, &OutSegmentIndex,
                                                MPLS_STORAGE_NONVOLATILE)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set the Out segment "
                       "storage type");
            MplsDeleteTnlIntfOverHLSP (u4OutIfIndex, u4MplsTnlIfIndex,
                                       MPLS_FALSE);
            break;
        }

        if (nmhSetMplsOutSegmentStorageType (&OutSegmentIndex,
                                             MPLS_STORAGE_NONVOLATILE) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set the Out segment "
                       "storage type");
            MplsDeleteTnlIntfOverHLSP (u4OutIfIndex, u4MplsTnlIfIndex,
                                       MPLS_FALSE);
            break;
            /*Failure Condition not handled here */
        }

        if ((nmhTestv2MplsOutSegmentRowStatus
             (&u4ErrorCode, &OutSegmentIndex,
              MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set the Out segment "
                       "row status active");
            MplsDeleteTnlIntfOverHLSP (u4OutIfIndex, u4MplsTnlIfIndex,
                                       MPLS_FALSE);
            break;
        }
        if ((nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                            MPLS_STATUS_ACTIVE)) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set the Out segment "
                       "row status active");
            MplsDeleteTnlIntfOverHLSP (u4OutIfIndex, u4MplsTnlIfIndex,
                                       MPLS_FALSE);
            break;
        }

        if (nmhTestv2MplsXCStorageType (&u4ErrorCode, &XCIndex, &InSegmentIndex,
                                        &OutSegmentIndex,
                                        MPLS_STORAGE_NONVOLATILE)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%% Unable to set storage type\r\n");
            MplsDeleteTnlIntfOverHLSP (u4OutIfIndex, u4MplsTnlIfIndex,
                                       MPLS_FALSE);
            break;
        }

        if (nmhSetMplsXCStorageType (&XCIndex, &InSegmentIndex,
                                     &OutSegmentIndex,
                                     MPLS_STORAGE_NONVOLATILE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%% Unable to set storage type\r\n");
            MplsDeleteTnlIntfOverHLSP (u4OutIfIndex, u4MplsTnlIfIndex,
                                       MPLS_FALSE);
            break;
        }

        /*  Make XC RowStatus Active */
        if ((nmhTestv2MplsXCRowStatus
             (&u4ErrorCode, &XCIndex, &InSegmentIndex, &OutSegmentIndex,
              MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set the XC row status active");
            MplsDeleteTnlIntfOverHLSP (u4OutIfIndex, u4MplsTnlIfIndex,
                                       MPLS_FALSE);
            break;
        }
        if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex,
                                    &OutSegmentIndex,
                                    MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set the XC row status active");
            MplsDeleteTnlIntfOverHLSP (u4OutIfIndex, u4MplsTnlIfIndex,
                                       MPLS_FALSE);
            break;
        }

        /* all set operations have been successful */
        return CLI_SUCCESS;
    }
    while (0);

    u4RowStatus = TE_DESTROY;
    MEMCPY (&HlspTnlCliArgs.teSetFsMplsLSPMapTunnelTable,
            &teSetFsMplsLSPMapTunnelTable, sizeof (tteFsMplsLSPMapTunnelTable));
    MEMCPY (&HlspTnlCliArgs.teIsSetFsMplsLSPMapTunnelTable,
            &teIsSetFsMplsLSPMapTunnelTable,
            sizeof (teIsSetFsMplsLSPMapTunnelTable));
    HlspTnlCliArgs.u4SetHlspTnlIndex = u4HlspTunnelIndex;
    HlspTnlCliArgs.u4SetHlspTnlInstance = u4HlspTunnelInstance;
    HlspTnlCliArgs.u4SetHlspTnlIngressId = u4HlspIngressId;
    HlspTnlCliArgs.u4SetHlspTnlEgressId = u4HlspEgressId;
    HlspTnlCliArgs.u4IsSetHlspTnlIndex = u4TunnelIndex;
    HlspTnlCliArgs.u4IsSetHlspTnlInstance = u4TunnelInstance;
    HlspTnlCliArgs.u4IsSetHlspTnlIngressId = u4IngressId;
    HlspTnlCliArgs.u4IsSetHlspTnlEgressId = u4EgressId;
    HlspTnlCliArgs.u4Operation = u4Operation;
    HlspTnlCliArgs.u4RowStatus = u4RowStatus;

    TeFillFsMplsLspMapTnlTable (&HlspTnlCliArgs);

    TeSetAllFsMplsLSPMapTunnelTable (&HlspTnlCliArgs.
                                     teSetFsMplsLSPMapTunnelTable,
                                     &HlspTnlCliArgs.
                                     teIsSetFsMplsLSPMapTunnelTable, OSIX_TRUE,
                                     OSIX_TRUE);

    /* delete the created OutSeg and XC entry */
    if (nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                               MPLS_STATUS_DESTROY) == SNMP_FAILURE)
    {
        /*Failure Condition not handled here */
    }
    if (nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex, MPLS_STATUS_DESTROY)
        == SNMP_FAILURE)
    {
        /*Failure Condition not handled here */
    }

    TeCliCheckAndDelNonZeroInstTnl (CliHandle, u4TunnelIndex,
                                    u4TunnelInstance, u4IngressId,
                                    u4EgressId, bEntryCreated);
    return CLI_FAILURE;
}

/******************************************************************************
* Function Name : TeCliShowHLSP
* Description   : This routine displays H-LSPs/S-LSPs configured in the system
* Input(s)      : CliHandle             - Cli Context Handle
*               : u4Flag                - Type of the tunnel to be displayed
*                
* Output(s)     : None
* Return(s)     : CLI_SUCCESS/CLI_FAILURE
******************************************************************************/
INT4
TeCliShowHLSP (tCliHandle CliHandle, UINT4 u4Flag)
{
    UINT4               u4TnlId = TE_ZERO;
    UINT4               u4TnlInstance = TE_ZERO;
    UINT4               u4TnlIngressLSrId = TE_ZERO;
    UINT4               u4TnlEgressLsrId = TE_ZERO;
    UINT4               u4TnlType = TE_ZERO;
    tTeTnlInfo         *pTeTnlInfo = NULL;

    if (u4Flag == CLI_MPLS_HLSP)
    {
        if (TE_ZERO == gu4TeNoOfHlsp)
        {
            CliPrintf (CliHandle, "\r No H-LSPs configured in the system \n");
            return CLI_SUCCESS;
        }
        CliPrintf (CliHandle,
                   "\r \n Total HLSPs configured in the system : %d \n",
                   gu4TeNoOfHlsp);
        u4TnlType = TE_TNL_TYPE_HLSP;
    }
    if (u4Flag == CLI_MPLS_SLSP_TNLS)
    {
        if (TE_ZERO == gu4TeNoOfSlsp)
        {
            CliPrintf (CliHandle, "\r No S-LSPs configured in the system \n");
            return CLI_SUCCESS;
        }
        CliPrintf (CliHandle,
                   "\r \n Total S-LSPs configured in the system : %d \n",
                   gu4TeNoOfSlsp);
        u4TnlType = TE_TNL_TYPE_SLSP;
    }

    CliPrintf (CliHandle,
               "\rTUNNEL NAME     "
               "SOURCE             DESTINATION    "
               " UP IF   " "DOWN IF   " "STATE/PROT\n");

    if ((nmhGetFirstIndexMplsTunnelTable (&u4TnlId, &u4TnlInstance,
                                          &u4TnlIngressLSrId,
                                          &u4TnlEgressLsrId)) == SNMP_SUCCESS)
    {
        CliRegisterLock (CliHandle, CmnLock, CmnUnLock);
        do
        {
            if (u4TnlInstance == TE_ZERO)
            {
                continue;
            }

            MPLS_CMN_LOCK ();
            pTeTnlInfo = TeGetTunnelInfo (u4TnlId, u4TnlInstance,
                                          u4TnlIngressLSrId, u4TnlEgressLsrId);
            if (pTeTnlInfo == NULL)
            {
                CliPrintf (CliHandle, "\r%% Failed to retrive tunnel info \n");
                MPLS_CMN_UNLOCK ();
                CliUnRegisterLock (CliHandle);
                return CLI_FAILURE;
            }
            if (TE_TNL_TYPE (pTeTnlInfo) & u4TnlType)
            {
                TeCliShowTnlBrief (CliHandle, pTeTnlInfo);
            }
            MPLS_CMN_UNLOCK ();
        }
        while ((nmhGetNextIndexMplsTunnelTable (u4TnlId, &u4TnlId,
                                                u4TnlInstance,
                                                &u4TnlInstance,
                                                u4TnlIngressLSrId,
                                                &u4TnlIngressLSrId,
                                                u4TnlEgressLsrId,
                                                &u4TnlEgressLsrId))
               == SNMP_SUCCESS);

        CliUnRegisterLock (CliHandle);
    }
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliShowHLSPStackedTnl
* Description   : This routine displays the tunnels that are stacked on to the 
*                     given HLSP
* Input(s)      : CliHandle             - Cli Context Handle
*                    u4TnlIndex          -  Mpls Tunnel Index
* Output(s)     : None
* Return(s)     : CLI_SUCCESS/CLI_FAILURE
******************************************************************************/

INT4
TeCliShowHLSPStackedTnl (tCliHandle CliHandle, UINT4 u4TnlIndex,
                         UINT4 u4TunnelInstance, UINT4 u4IngressId,
                         UINT4 u4EgressId)
{
    tTeTnlInfo         *pTeHlspTnlInfo = NULL;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTMO_DLL_NODE      *pNextServiceTnl = NULL;

    MPLS_CMN_LOCK ();

    if ((u4TunnelInstance == 0) || (u4IngressId == 0) || (u4EgressId == 0))
    {
        pTeHlspTnlInfo =
            TeGetTnlInfoByOwnerAndRole (u4TnlIndex, u4TunnelInstance,
                                        u4IngressId, u4EgressId, TE_ZERO,
                                        TE_ZERO);
    }
    else
    {
        pTeHlspTnlInfo = TeGetTunnelInfo (u4TnlIndex,
                                          u4TunnelInstance,
                                          u4IngressId, u4EgressId);
    }
    if (pTeHlspTnlInfo == NULL)
    {
        CliPrintf (CliHandle, "\r\n%%Tunnel Does Not Exist\r\n");

        MPLS_CMN_UNLOCK ();
        return CLI_FAILURE;
    }

    if (pTeHlspTnlInfo->u4TnlInstance == TE_ZERO)
    {
        pTeHlspTnlInfo = TeGetNextTunnelInfo (pTeHlspTnlInfo);
    }

    if (pTeHlspTnlInfo == NULL)
    {
        CliPrintf (CliHandle, "\r\n%%Tunnel Does Not Exist\r\n");

        MPLS_CMN_UNLOCK ();
        return CLI_FAILURE;
    }

    if (!(TE_TNL_TYPE (pTeHlspTnlInfo) & TE_TNL_TYPE_HLSP))
    {
        CliPrintf (CliHandle, "\r%% Tunnel type must be HLSP\r\n");
        MPLS_CMN_UNLOCK ();
        return CLI_FAILURE;
    }

    if (TE_HLSP_NO_OF_STACKED_TNLS (pTeHlspTnlInfo) == MPLS_ZERO)
    {
        CliPrintf (CliHandle, "No Tunnels stacked on to HLSP \r\n");
        MPLS_CMN_UNLOCK ();
        return CLI_SUCCESS;
    }

    CliPrintf (CliHandle, "\r  Available Band width of HLSP : %d \n",
               TE_HLSP_AVAILABLE_BW (pTeHlspTnlInfo));
    CliPrintf (CliHandle, "\r  No of stacked tunnels in HLSP : %d \n",
               TE_HLSP_NO_OF_STACKED_TNLS (pTeHlspTnlInfo));

    CliPrintf (CliHandle,
               "\rTUNNEL NAME     "
               "SOURCE             DESTINATION    "
               " UP IF   " "DOWN IF   " "STATE/PROT\n");

    TMO_DLL_Scan (&TE_HLSP_STACK_LIST (pTeHlspTnlInfo), pNextServiceTnl,
                  tTMO_DLL_NODE *)
    {
        pTeTnlInfo =
            (tTeTnlInfo
             *) (((FS_ULONG) pNextServiceTnl -
                  (TE_OFFSET (tTeTnlInfo, NextServiceTnl))));
        if (pTeTnlInfo != NULL)
        {
            TeCliShowTnlBrief (CliHandle, pTeTnlInfo);
        }
    }

    MPLS_CMN_UNLOCK ();
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliShowSLSPStitchedTnl
* Description   : This routine displays the tunnel information  that is 
*               : stitched to the given S-LSP.
* Input(s)      : CliHandle           - Cli Context Handle
*               : u4TnlIndex          - Mpls Tunnel Index
*               : u4TunnelInstance    - Tunnel instance
*               : u4IngressId         - Ingress LSR identifierof teh tunnel
*               : u4EgressId          - Egress LSR identifier of the tunnel
* Output(s)     : None
* Return(s)     : CLI_SUCCESS/CLI_FAILURE
******************************************************************************/

INT4
TeCliShowSLSPStitchedTnl (tCliHandle CliHandle, UINT4 u4TnlIndex,
                          UINT4 u4TunnelInstance, UINT4 u4IngressId,
                          UINT4 u4EgressId)
{
    tTeTnlInfo         *pTeSlspTnlInfo = NULL;

    MPLS_CMN_LOCK ();

    if ((u4TunnelInstance == 0) || (u4IngressId == 0) || (u4EgressId == 0))
    {
        pTeSlspTnlInfo =
            TeGetTnlInfoByOwnerAndRole (u4TnlIndex, u4TunnelInstance,
                                        u4IngressId, u4EgressId, TE_ZERO,
                                        TE_ZERO);
    }
    else
    {
        pTeSlspTnlInfo = TeGetTunnelInfo (u4TnlIndex,
                                          u4TunnelInstance,
                                          u4IngressId, u4EgressId);
    }
    if (pTeSlspTnlInfo == NULL)
    {
        CliPrintf (CliHandle, "\r\n%%Tunnel Does Not Exist\r\n");
        MPLS_CMN_UNLOCK ();
        return CLI_FAILURE;
    }

    if (pTeSlspTnlInfo->u4TnlInstance == TE_ZERO)
    {
        pTeSlspTnlInfo = TeGetNextTunnelInfo (pTeSlspTnlInfo);
    }

    if (pTeSlspTnlInfo == NULL)
    {
        CliPrintf (CliHandle, "\r\n%%Tunnel Does Not Exist\r\n");

        MPLS_CMN_UNLOCK ();
        return CLI_FAILURE;
    }

    if (!(TE_TNL_TYPE (pTeSlspTnlInfo) & TE_TNL_TYPE_SLSP))
    {
        CliPrintf (CliHandle, "\r%% Tunnel type must be S-LSP\r\n");
        MPLS_CMN_UNLOCK ();
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\r  Available Band width of SLSP : %d \n",
               TE_HLSP_AVAILABLE_BW (pTeSlspTnlInfo));

    if (TE_MAP_TNL_INFO (pTeSlspTnlInfo) != NULL)
    {
        CliPrintf (CliHandle,
                   "\rTUNNEL NAME     "
                   "SOURCE             DESTINATION    "
                   " UP IF   " "DOWN IF   " "STATE/PROT\n");

        TeCliShowTnlBrief (CliHandle, TE_MAP_TNL_INFO (pTeSlspTnlInfo));
    }

    MPLS_CMN_UNLOCK ();
    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliSetTunnelNotification
* Description   : This routine enables or disables the tunnels notification 
*                 snmp trap
* Input(s)      : CliHandle             - Cli Context Handle
*                 i4SnmpTrapStatus      - Mpls Tunnel notification trap status
* Output(s)     : None
* Return(s)     : CLI_SUCCESS/CLI_FAILURE
******************************************************************************/

INT4
TeCliSetTunnelNotification (tCliHandle CliHandle, INT4 i4SnmpTrapStatus)
{
    UINT4               u4ErrorCode = TE_ZERO;
    if (nmhTestv2MplsTunnelNotificationEnable (&u4ErrorCode,
                                               i4SnmpTrapStatus) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set SNMP Trap\r\n");
        return CLI_FAILURE;
    }
    if (nmhSetMplsTunnelNotificationEnable (i4SnmpTrapStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set SNMP Trap\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/* MPLS_P2MP_LSP_CHANGES - S */
/******************************************************************************
 * Function Name : TeCliP2mpTunnelCreate
 * Description   : This routine is used to create a P2MP tunnel
 * Input(s)      : CliHandle            - Cli Context Handle
 *                 u4TunnelIndex        - Tunnel Index
                   u4P2mpId             - P2MP identifier
                   u4IngressId          - Identity of the ingress LSR associated
                                          with the tunnel instance.
                   u4IngressLocalMapNum - Ingress local map number for global-id
                                          and node-id combination
                   u4LspNumber          - Identity of the tunnel instance.
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ******************************************************************************/
INT1
TeCliP2mpTunnelCreate (tCliHandle CliHandle, UINT4 u4TunnelIndex,
                       UINT4 u4P2mpId, UINT4 u4IngressId,
                       UINT4 u4IngressLocalMapNum, UINT4 u4LspNumber,
                       UINT1 u1TnlType)
{
    tSNMP_OCTET_STRING_TYPE MplsDefRouterId;

    UINT1               au1IpAddr[ROUTER_ID_LENGTH];

    UINT4               u4ContextId = MPLS_DEF_CONTEXT_ID;
    UINT4               u4GlobalId = TE_ZERO;
    UINT4               u4NodeId = TE_ZERO;
    UINT4               u4MplsDefRouterId = TE_ZERO;

    UINT4               u4ErrorCode = TE_ZERO;
    UINT4               u4TunnelInstance = u4LspNumber;
    UINT4               u4EgressId = u4P2mpId;
    UINT4               u4EgressLocalMapNum = TE_ZERO;
    tTeCliArgs          TeCliArgs;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCliP2mpTunnelCreate : ENTRY \n");

    MEMSET (au1IpAddr, TE_ZERO, ROUTER_ID_LENGTH);
    MEMSET (&TeCliArgs, TE_ZERO, sizeof (tTeCliArgs));

    MplsDefRouterId.pu1_OctetList = au1IpAddr;
    MplsDefRouterId.i4_Length = ROUTER_ID_LENGTH;

    if ((TE_ZERO == u4IngressId) && (TE_ZERO == u4IngressLocalMapNum))
    {
        /* P2MP ID cannot identify whether the tunnel is MPLS-TP or MPLS.
         * Retrieve default MPLS-TP or MPLS router ID for ingress LSR. */

        /* Fetch the source global-id and node-id local map number from node 
         * map table */
        if (TeCliGetLocalMapNum (u4ContextId, &u4IngressLocalMapNum)
            == CLI_SUCCESS)
        {
            u4IngressId = u4IngressLocalMapNum;
        }
        else
        {
            /* Local map number does not exist. Fetch MPLS_ROUTER_ID */
            nmhGetFsMplsRouterID (&MplsDefRouterId);
            MPLS_OCTETSTRING_TO_INTEGER ((&MplsDefRouterId), u4MplsDefRouterId);
            u4IngressId = u4MplsDefRouterId;
        }
    }
    else if (TE_ZERO != u4IngressLocalMapNum)
    {
        if (OamUtilGetGlobalIdNodeId (u4ContextId, u4IngressLocalMapNum,
                                      &u4GlobalId, &u4NodeId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%%Configure the local map number "
                       "in the node map table\n");
            return CLI_FAILURE;
        }
        u4IngressId = u4IngressLocalMapNum;
    }

    TeCliArgs.u4TunnelIndex = u4TunnelIndex;
    TeCliArgs.u4TunnelInstance = u4LspNumber;
    TeCliArgs.u4IngressId = u4IngressId;
    TeCliArgs.u4EgressId = u4EgressId;
    TeCliArgs.u4IngressLocalMapNum = u4IngressLocalMapNum;
    TeCliArgs.u4EgressLocalMapNum = u4EgressLocalMapNum;
    TeCliArgs.u1TnlType = u1TnlType;
    TeCliArgs.u1TnlPathType = TE_ZERO;
    TeCliArgs.i4TnlMode = TE_ZERO;
    TeCliArgs.i4TnlIfIndex = (INT4) gaTnlCliArgs[CliHandle].u4MplsTnlIfIndex;

    /* Invoke MPLS tunnel creation routine */
    if (TeCliTunnelCreate (CliHandle, &TeCliArgs, OSIX_FALSE) == CLI_FAILURE)
    {
        TE_DBG (TE_MAIN_FAIL, "MAIN : MPLS tunnel row creation failed \n");
        return CLI_FAILURE;
    }

    do
    {
        /* Create a P2MP tunnel row */
        if (nmhTestv2MplsTeP2mpTunnelRowStatus (&u4ErrorCode, u4TunnelIndex,
                                                u4TunnelInstance, u4IngressId,
                                                u4EgressId,
                                                MPLS_STATUS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            TE_DBG (TE_MAIN_ETEXT, "\r\n%%Test routine for P2MP tunnel row "
                    "creation failed \r\n");
            return CLI_FAILURE;
        }
        if (nmhSetMplsTeP2mpTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                             u4IngressId, u4EgressId,
                                             MPLS_STATUS_CREATE_AND_WAIT) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Failed to set P2MP tunnel row "
                       "status \r\n");
            break;
        }
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCliP2mpTunnelCreate : EXIT \n");
        return CLI_SUCCESS;
    }
    while (TE_ZERO);

    /* Clean-up in case of failure */
    TeCliDeleteTunnel (CliHandle, &TeCliArgs, OSIX_FALSE);

    TE_DBG (TE_MAIN_FAIL, "MAIN : P2MP tunnel creation failed \n");

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCliP2mpTunnelCreate : EXIT \n");
    return CLI_FAILURE;
}

/******************************************************************************
 * Function Name : TeCliP2mpTunnelDelete 
 * Description   : This routine is used to delete a P2MP tunnel
 * Input(s)      : CliHandle            - Cli Context Handle
                   u4TunnelIndex        - Uniquely identifies a set of tunnel
                                          instances
                   u4TunnelInstance     - Uniquely identifies a particular
                                          instance of a tunnel
                   u4P2mpId             - P2MP identifier
                   u4IngressId          - Identity of the ingress LSR associated
                                          with the tunnel instance
                   u4IngressLocalMapNum - Ingress local map number for global-id
                                          and node-id combination
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ******************************************************************************/
INT1
TeCliP2mpTunnelDelete (tCliHandle CliHandle, UINT4 u4TunnelIndex,
                       UINT4 u4TunnelInstance, UINT4 u4P2mpId,
                       UINT4 u4IngressId, UINT4 u4IngressLocalMapNum)
{
    tSNMP_OCTET_STRING_TYPE MplsDefRouterId;
    UINT1               au1IpAddr[ROUTER_ID_LENGTH];

    UINT4               u4MplsDefRouterId = TE_ZERO;
    UINT4               u4ContextId = MPLS_DEF_CONTEXT_ID;
    UINT4               u4GlobalId = TE_ZERO;
    UINT4               u4NodeId = TE_ZERO;

    UINT4               u4ErrorCode = TE_ZERO;
    UINT4               u4EgressId = u4P2mpId;
    UINT4               u4EgressLocalMapNum = TE_ZERO;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeCliArgs          TeCliArgs;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCliP2mpTunnelDelete : ENTRY \n");

    MEMSET (au1IpAddr, TE_ZERO, ROUTER_ID_LENGTH);
    MEMSET (&TeCliArgs, TE_ZERO, sizeof (TeCliArgs));

    MplsDefRouterId.pu1_OctetList = au1IpAddr;
    MplsDefRouterId.i4_Length = ROUTER_ID_LENGTH;

    if ((TE_ZERO == u4IngressId) && (TE_ZERO == u4IngressLocalMapNum))
    {
        /* Fetch the source global-id and node-id local map number from node 
         * map table */
        if (TeCliGetLocalMapNum (u4ContextId, &u4IngressLocalMapNum)
            == CLI_SUCCESS)
        {
            u4IngressId = u4IngressLocalMapNum;
        }
        else
        {
            /* Local map number does not exist. Fetch MPLS_ROUTER_ID */
            nmhGetFsMplsRouterID (&MplsDefRouterId);
            MPLS_OCTETSTRING_TO_INTEGER ((&MplsDefRouterId), u4MplsDefRouterId);
            u4IngressId = u4MplsDefRouterId;
        }
    }
    else if (TE_ZERO != u4IngressLocalMapNum)
    {
        if (OamUtilGetGlobalIdNodeId (u4ContextId, u4IngressLocalMapNum,
                                      &u4GlobalId, &u4NodeId) != OSIX_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%%Local map number is not present in the "
                       "node map table\n");
            return CLI_FAILURE;
        }
        u4IngressId = u4IngressLocalMapNum;
    }

    /* Check in Tunnel RBTree. If entry is not present, return failure */
    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                  u4EgressId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        CliPrintf (CliHandle, "\r\n%%Tunnel entry not present. Deletion "
                   "failed \r\n");
        return CLI_FAILURE;
    }
    /* Check if tunnel type is P2MP */
    if (NULL == TE_P2MP_TNL_INFO (pTeTnlInfo))
    {
        MPLS_CMN_UNLOCK ();
        CliPrintf (CliHandle, "\r\n%%Not a P2MP tunnel. Cannot delete \r\n");
        return CLI_FAILURE;
    }
    MPLS_CMN_UNLOCK ();

    /* Delete P2MP tunnel row */
    if (nmhTestv2MplsTeP2mpTunnelRowStatus (&u4ErrorCode, u4TunnelIndex,
                                            u4TunnelInstance, u4IngressId,
                                            u4EgressId, MPLS_STATUS_DESTROY)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Test routine for tunnel row deletion "
                   "failed \r\n");
        return CLI_FAILURE;
    }

    if (nmhSetMplsTeP2mpTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                         u4IngressId, u4EgressId,
                                         MPLS_STATUS_DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Failed to delete P2MP tunnel row "
                   "status \r\n");
        return CLI_FAILURE;
    }

    TeCliArgs.u4TunnelIndex = u4TunnelIndex;
    TeCliArgs.u4TunnelInstance = u4TunnelInstance;
    TeCliArgs.u4IngressId = u4IngressId;
    TeCliArgs.u4EgressId = u4EgressId;
    TeCliArgs.u4IngressLocalMapNum = u4IngressLocalMapNum;
    TeCliArgs.u4EgressLocalMapNum = u4EgressLocalMapNum;
    TeCliArgs.i4TnlIfIndex = (INT4) gaTnlCliArgs[CliHandle].u4MplsTnlIfIndex;

    /* Delete MPLS tunnel row */
    if (TeCliDeleteTunnel (CliHandle, &TeCliArgs, OSIX_FALSE) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%MPLS tunnel row deletion failed \r\n");
        return CLI_FAILURE;
    }

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCliP2mpTunnelDelete : EXIT \n");
    return CLI_SUCCESS;
}

/*******************************************************************************
* Function Name : TeCliP2mpAddDestinationAtIngress
* Description   : This routine is used to add destinations to a P2MP 
*                 tunnel at ingress LSR and associates labels with it.
* Input(s)      : CliHandle              - Cli Context Handle
                  u4P2mpDestId           - Identity of the P2MP destination LSR
                  u4P2mpDestLocalMapNum  - P2MP destination local map number 
                                           for global-id and node-id 
                                           combination
                  u4OutLabel             - Label associated with the outgoing
                                           packet
                  u4NextHop              - Index for the outgoing MPLS interface
                  u4OutIfIndex           - Outgoing interface
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT1
TeCliP2mpAddDestinationAtIngress (tCliHandle CliHandle, UINT4 u4P2mpDestId,
                                  UINT4 u4P2mpDestLocalMapNum,
                                  UINT4 u4OutLabel, UINT4 u4NextHop,
                                  UINT4 u4OutIfIndex)
{
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OCTET_STRING_TYPE NextHopAddr;

    tSNMP_OCTET_STRING_TYPE P2mpTnlSrcSubGrpOrigin;
    tSNMP_OCTET_STRING_TYPE P2mpTnlDestSubGrpOrigin;
    tSNMP_OCTET_STRING_TYPE P2mpTnlDestination;

    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1NextHopAddr[MPLS_INDEX_LENGTH];

    static UINT1        au1P2mpTnlSrcSubGrpOrigin[ROUTER_ID_LENGTH];
    static UINT1        au1P2mpTnlDestSubGrpOrigin[ROUTER_ID_LENGTH];
    static UINT1        au1P2mpTnlDestination[ROUTER_ID_LENGTH];

    UINT4               u4TunnelIndex = TE_ZERO;
    UINT4               u4TunnelInstance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    INT4                i4TnlRole = TE_ZERO;

    INT4                i4P2mpTnlSrcSubGrpOriginType = LSR_ADDR_IPV4;
    INT4                i4P2mpTnlDestSubGrpOriginType = LSR_ADDR_IPV4;
    INT4                i4P2mpTnlDestinationType = LSR_ADDR_IPV4;
    UINT4               u4P2mpTnlSrcSubGrpId = TE_ZERO;
    UINT4               u4P2mpTnlDestSubGrpId = TE_ONE;

    UINT4               u4ErrorCode = TE_ZERO;
    UINT4               u4FirstOutSegIndex = TE_ZERO;
    UINT4               u4OutSegIndex = TE_ZERO;
#ifdef CFA_WANTED
    UINT4               u4MplsIfIndex = TE_ZERO;
#endif
    UINT4               u4MplsTnlIfIndex = TE_ZERO;
    UINT4               u4Mask = 0xffffffff;

    tTeTnlInfo         *pTeTnlInfo = NULL;
    tP2mpBranchEntry   *pTeP2mpBranchEntry = NULL;
    tXcEntry           *pXcEntry = NULL;
    tOutSegment        *pOutSegment = NULL;
    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;

    BOOL1               bFoundOutSegment = TE_FALSE;
    BOOL1               bNextHopCfg = TE_FALSE;

    /* Flags for clean-up in case of failure */
    BOOL1               bOutSegmentDeleteFlag = TE_FALSE;
    BOOL1               bOutInterfaceDeleteFlag = TE_FALSE;
    BOOL1               bP2mpDestDeleteFlag = TE_FALSE;
    BOOL1               bXcDeleteFlag = TE_FALSE;

    TE_DBG (TE_MAIN_ETEXT, "MAIN: TeCliP2mpAddDestinationAtIngress : ENTRY \n");

    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;

    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;

    XCIndex.pu1_OctetList = au1XCIndex;
    XCIndex.i4_Length = MPLS_INDEX_LENGTH;

    NextHopAddr.pu1_OctetList = au1NextHopAddr;
    NextHopAddr.i4_Length = MPLS_INDEX_LENGTH;

    P2mpTnlSrcSubGrpOrigin.pu1_OctetList = au1P2mpTnlSrcSubGrpOrigin;
    P2mpTnlSrcSubGrpOrigin.i4_Length = ROUTER_ID_LENGTH;

    P2mpTnlDestSubGrpOrigin.pu1_OctetList = au1P2mpTnlDestSubGrpOrigin;
    P2mpTnlDestSubGrpOrigin.i4_Length = ROUTER_ID_LENGTH;

    P2mpTnlDestination.pu1_OctetList = au1P2mpTnlDestination;
    P2mpTnlDestination.i4_Length = ROUTER_ID_LENGTH;

    MEMSET (au1InSegIndex, TE_ZERO, MPLS_INDEX_LENGTH);
    MEMSET (au1OutSegIndex, TE_ZERO, MPLS_INDEX_LENGTH);
    MEMSET (au1XCIndex, TE_ZERO, MPLS_INDEX_LENGTH);
    MEMSET (au1NextHopAddr, TE_ZERO, MPLS_INDEX_LENGTH);
    MEMSET (au1P2mpTnlSrcSubGrpOrigin, TE_ZERO, ROUTER_ID_LENGTH);
    MEMSET (au1P2mpTnlDestSubGrpOrigin, TE_ZERO, ROUTER_ID_LENGTH);
    MEMSET (au1P2mpTnlDestination, TE_ZERO, ROUTER_ID_LENGTH);

    /* Check if input P2MP destination is valid */
    if (TE_ZERO != u4P2mpDestId)
    {
        MPLS_INTEGER_TO_OCTETSTRING (u4P2mpDestId, (&P2mpTnlDestination));
    }
    else if (TE_ZERO != u4P2mpDestLocalMapNum)
    {
        MPLS_INTEGER_TO_OCTETSTRING (u4P2mpDestLocalMapNum,
                                     (&P2mpTnlDestination));
    }
    else
    {
        CliPrintf (CliHandle, "\r\n%%Wrong P2MP Destination ID \r\n");
        return CLI_FAILURE;
    }

    /* Get the TunnelId, TunnelInstance, Ingress and Egress LSR ID */
    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }
    /* If tunnel does not exist, return failure */
    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                  u4EgressId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        CliPrintf (CliHandle, "\r\n%%Tunnel does not exist\r\n");
        return CLI_FAILURE;
    }
    /* Check the tunnel type */
    if (NULL == TE_P2MP_TNL_INFO (pTeTnlInfo))
    {
        MPLS_CMN_UNLOCK ();
        CliPrintf (CliHandle, "\r\n%%Not a P2MP tunnel. Cannot associate "
                   "labels to destinations\r\n");
        return CLI_FAILURE;
    }
    MPLS_CMN_UNLOCK ();

    /* Check if the tunnel role is ingress */
    if (nmhGetMplsTunnelRole (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                              u4EgressId, &i4TnlRole) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to get the tunnel role\r\n");
        return CLI_FAILURE;
    }
    if (i4TnlRole == TE_EGRESS)
    {
        CliPrintf (CliHandle, "\r\n%%Wrong label configuration for Egress "
                   "tunnel\r\n");
        return CLI_FAILURE;
    }
    else if (i4TnlRole == TE_INTERMEDIATE)
    {
        CliPrintf (CliHandle, "\r\n%%Wrong label configuration for "
                   "Intermediate tunnel\r\n");
        return CLI_FAILURE;
    }

    if (TE_ZERO == u4OutIfIndex)
    {
        bNextHopCfg = TE_TRUE;
        if (NetIpv4IfIsOurAddress (u4NextHop) == NETIPV4_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%%Next hop is same as self-ip addr\n");
            return CLI_FAILURE;
        }

        MEMSET (&NetIpRtInfo, TE_ZERO, sizeof (tNetIpv4RtInfo));
        MEMSET (&RtQuery, TE_ZERO, sizeof (tRtInfoQueryMsg));
        RtQuery.u4DestinationIpAddress = u4NextHop;
        RtQuery.u4DestinationSubnetMask = u4Mask;
        RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;

        if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_SUCCESS)
        {
            if (NetIpv4GetCfaIfIndexFromPort (NetIpRtInfo.u4RtIfIndx,
                                              &u4OutIfIndex) == NETIPV4_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Unable to fetch Next hop information\n");
                return CLI_FAILURE;
            }
        }
#ifdef CFA_WANTED
        else
        {
            if (CfaIpIfGetIfIndexFromHostIpAddressInCxt (MPLS_DEF_CONTEXT_ID,
                                                         u4NextHop,
                                                         &u4OutIfIndex)
                == CFA_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Unable to fetch Next hop information\n");
                return CLI_FAILURE;
            }
        }
    }
    if (CfaUtilGetMplsIfFromIfIndex (u4OutIfIndex, &u4MplsIfIndex,
                                     TRUE) == CFA_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% Enable MPLS over outgoing interface\r\n");
        return CLI_FAILURE;
    }
#else
    }
#endif

    /* Check if destination is already present in P2MP destination table,
     * same type as ingress LSR. If destination is a local map number, check
     * if it is present in the node map table */
    if ((nmhTestv2MplsTeP2mpTunnelDestRowStatus (&u4ErrorCode, u4TunnelIndex,
                                                 u4TunnelInstance, u4IngressId,
                                                 u4EgressId,
                                                 i4P2mpTnlSrcSubGrpOriginType,
                                                 &P2mpTnlSrcSubGrpOrigin,
                                                 u4P2mpTnlSrcSubGrpId,
                                                 i4P2mpTnlDestSubGrpOriginType,
                                                 &P2mpTnlDestSubGrpOrigin,
                                                 u4P2mpTnlDestSubGrpId,
                                                 i4P2mpTnlDestinationType,
                                                 &P2mpTnlDestination,
                                                 MPLS_STATUS_CREATE_AND_WAIT))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%P2MP destination entry creation "
                   "failed \r\n");
        return CLI_FAILURE;
    }

    /* Scan the branch table to check if same out label has been configured 
     * for another destination of the P2MP tunnel */
    MPLS_CMN_LOCK ();
    TMO_SLL_Scan (&(TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo)),
                  pTeP2mpBranchEntry, tP2mpBranchEntry *)
    {
        u4OutSegIndex = TE_P2MP_BRANCH_OUTSEG_INDEX (pTeP2mpBranchEntry);
        pOutSegment = MplsGetOutSegmentTableEntry (u4OutSegIndex);
        if ((NULL != pOutSegment) &&
            (u4OutLabel == OUTSEGMENT_LABEL (pOutSegment)))
        {
            /* Check if next-hop in out-segment and input next-hop address 
             * are same */
            if (u4NextHop != OUTSEGMENT_NH_ADDR (pOutSegment))
            {
                MPLS_CMN_UNLOCK ();
                CliPrintf (CliHandle, "\r\n%%Next hop addr configured "
                           "for the same out label does not match with "
                           "input next hop addr \r\n");
                return CLI_FAILURE;
            }
            MPLS_INTEGER_TO_OCTETSTRING (u4OutSegIndex, (&OutSegmentIndex));
            bFoundOutSegment = TE_TRUE;
            break;
        }
    }
    MPLS_CMN_UNLOCK ();

    /* Create out-segment entry if it is not present */
    if (TE_FALSE == bFoundOutSegment)
    {
        do
        {
            if ((nmhGetMplsOutSegmentIndexNext (&OutSegmentIndex))
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_NO_FREE_INDEX);
                return CLI_FAILURE;
            }
            if ((nmhTestv2MplsOutSegmentRowStatus (&u4ErrorCode,
                                                   &OutSegmentIndex,
                                                   MPLS_STATUS_CREATE_AND_WAIT))
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_OUTSEG_ENTRY_CREATION);
                return CLI_FAILURE;
            }
            if ((nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                                MPLS_STATUS_CREATE_AND_WAIT)) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_OUTSEG_ENTRY_CREATION);
                return CLI_FAILURE;
            }

            /* If out-segment is previously associated to P2MP destination, 
             * fetch XC index from out-segment */
            MPLS_CMN_LOCK ();
            if (TMO_SLL_Count (&(TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo)))
                != TE_ZERO)
            {
                pTeP2mpBranchEntry =
                    (tP2mpBranchEntry *)
                    TMO_SLL_First (&
                                   (TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo)));
                u4FirstOutSegIndex =
                    TE_P2MP_BRANCH_OUTSEG_INDEX (pTeP2mpBranchEntry);
                pOutSegment = MplsGetOutSegmentTableEntry (u4FirstOutSegIndex);
                if ((NULL != pOutSegment) &&
                    (NULL != OUTSEGMENT_XC_INDEX (pOutSegment)))
                {
                    pXcEntry = (tXcEntry *) (OUTSEGMENT_XC_INDEX (pOutSegment));
                    MPLS_INTEGER_TO_OCTETSTRING (XC_INDEX (pXcEntry),
                                                 (&XCIndex));
                }
            }
            else if ((nmhGetMplsXCIndexNext (&XCIndex)) == SNMP_FAILURE)
            {
                MPLS_CMN_UNLOCK ();
                CLI_SET_ERR (MPLS_CLI_NO_FREE_INDEX);
                bOutSegmentDeleteFlag = TE_TRUE;
                break;
            }
            MPLS_CMN_UNLOCK ();

            if ((nmhTestv2MplsXCRowStatus (&u4ErrorCode, &XCIndex,
                                           &InSegmentIndex, &OutSegmentIndex,
                                           MPLS_STATUS_CREATE_AND_WAIT)) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_XC_ENTRY_CREATION);
                bOutSegmentDeleteFlag = TE_TRUE;
                break;
            }
            if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex,
                                        &OutSegmentIndex,
                                        MPLS_STATUS_CREATE_AND_WAIT)) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_XC_ENTRY_CREATION);
                bOutSegmentDeleteFlag = TE_TRUE;
                break;
            }

            /* Set Out-segment top label */
            if (nmhTestv2MplsOutSegmentTopLabel (&u4ErrorCode, &OutSegmentIndex,
                                                 u4OutLabel) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_LABEL);
                bXcDeleteFlag = TE_TRUE;
                break;
            }
            if (nmhSetMplsOutSegmentTopLabel (&OutSegmentIndex, u4OutLabel)
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_ERR_OUTSEG_LABEL);
                bXcDeleteFlag = TE_TRUE;
                break;
            }

            /* Set Out-segment push top label to true */
            if (nmhTestv2MplsOutSegmentPushTopLabel (&u4ErrorCode,
                                                     &OutSegmentIndex,
                                                     MPLS_TRUE) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_PUSH_TOP_LABEL);
                bXcDeleteFlag = TE_TRUE;
                break;
            }
            if (nmhSetMplsOutSegmentPushTopLabel (&OutSegmentIndex, MPLS_TRUE)
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_PUSH_TOP_LABEL);
                bXcDeleteFlag = TE_TRUE;
                break;
            }

            if (TE_TRUE == bNextHopCfg)
            {
                /* Set the Next hop addr type to ipv4 */
                if (nmhTestv2MplsOutSegmentNextHopAddrType (&u4ErrorCode,
                                                            &OutSegmentIndex,
                                                            LSR_ADDR_IPV4) ==
                    SNMP_FAILURE)
                {
                    CLI_SET_ERR (MPLS_CLI_TE_ERR_NEXT_HOP_ADDR_TYPE);
                    bXcDeleteFlag = TE_TRUE;
                    break;
                }
                if (nmhSetMplsOutSegmentNextHopAddrType (&OutSegmentIndex,
                                                         LSR_ADDR_IPV4) ==
                    SNMP_FAILURE)
                {
                    CLI_SET_ERR (MPLS_CLI_TE_ERR_NEXT_HOP_ADDR_TYPE);
                    bXcDeleteFlag = TE_TRUE;
                    break;
                }

                /* Set nexthop address */
                MPLS_INTEGER_TO_OCTETSTRING (u4NextHop, (&NextHopAddr));
                if (nmhTestv2MplsOutSegmentNextHopAddr (&u4ErrorCode,
                                                        &OutSegmentIndex,
                                                        &NextHopAddr) ==
                    SNMP_FAILURE)
                {
                    CLI_SET_ERR (MPLS_CLI_TE_ERR_NEXT_HOP_ADDR);
                    bXcDeleteFlag = TE_TRUE;
                    break;
                }
                if (nmhSetMplsOutSegmentNextHopAddr (&OutSegmentIndex,
                                                     &NextHopAddr) ==
                    SNMP_FAILURE)
                {
                    CLI_SET_ERR (MPLS_CLI_TE_ERR_NEXT_HOP_ADDR);
                    bXcDeleteFlag = TE_TRUE;
                    break;
                }
            }
            if (CfaGetFreeInterfaceIndex (&u4MplsTnlIfIndex, CFA_MPLS_TUNNEL)
                == OSIX_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_NO_FREE_TUNNEL_INTERFACE);
                bXcDeleteFlag = TE_TRUE;
                break;
            }

            /* Create outgoing interface */
            if ((nmhTestv2MplsOutSegmentInterface (&u4ErrorCode,
                                                   &OutSegmentIndex,
                                                   (INT4) u4MplsTnlIfIndex)) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_IF);
                bXcDeleteFlag = TE_TRUE;
                break;
            }
            if ((nmhSetMplsOutSegmentInterface (&OutSegmentIndex,
                                                (INT4) u4MplsTnlIfIndex)) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_IF);
                bXcDeleteFlag = TE_TRUE;
                break;
            }
            if (MplsCreateMplsIfOrMplsTnlIf (u4OutIfIndex, u4MplsTnlIfIndex,
                                             CFA_MPLS_TUNNEL) == MPLS_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_ERR_OUTSTACK_ENTRY_CREATION);
                bXcDeleteFlag = TE_TRUE;
                break;
            }

            if (nmhTestv2MplsOutSegmentStorageType
                (&u4ErrorCode, &OutSegmentIndex,
                 MPLS_STORAGE_NONVOLATILE) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n%%Unable to set the Out segment "
                           "storage type");
                bOutInterfaceDeleteFlag = TE_TRUE;
                break;
            }

            if (nmhSetMplsOutSegmentStorageType (&OutSegmentIndex,
                                                 MPLS_STORAGE_NONVOLATILE) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n%%Unable to set the Out segment "
                           "storage type");
                bOutInterfaceDeleteFlag = TE_TRUE;
                break;
            }

            if ((nmhTestv2MplsOutSegmentRowStatus (&u4ErrorCode,
                                                   &OutSegmentIndex,
                                                   MPLS_STATUS_ACTIVE)) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n%%Unable to set the Out segment "
                           "row status active");
                bOutInterfaceDeleteFlag = TE_TRUE;
                break;
            }
            if ((nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                                MPLS_STATUS_ACTIVE)) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n%%Unable to set the Out segment "
                           "row status active");
                bOutInterfaceDeleteFlag = TE_TRUE;
                break;
            }
            MPLS_OCTETSTRING_TO_INTEGER ((&OutSegmentIndex), u4OutSegIndex);
        }
        while (TE_ZERO);        /* End - do */
        /* Clean-up in case of failure */
        if (TE_TRUE == bOutInterfaceDeleteFlag)
        {
            MplsDeleteMplsIfOrMplsTnlIf (u4OutIfIndex, u4MplsTnlIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            if (nmhSetMplsXCRowStatus
                (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                 MPLS_STATUS_DESTROY) == SNMP_FAILURE)
            {
                /*Failure Condition not handled here */
            }
            if (nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                               MPLS_STATUS_DESTROY) ==
                SNMP_FAILURE)
            {
                /*Failure Condition not handled here */
            }
            return CLI_FAILURE;
        }
        else if (TE_TRUE == bXcDeleteFlag)
        {
            if (nmhSetMplsXCRowStatus
                (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                 MPLS_STATUS_DESTROY) == SNMP_FAILURE)
            {
                /*Failure Condition not handled here */
            }
            if (nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                               MPLS_STATUS_DESTROY) ==
                SNMP_FAILURE)
            {
                /*Failure Condition not handled here */
            }
            return CLI_FAILURE;
        }
        else if (TE_TRUE == bOutSegmentDeleteFlag)
        {
            if (nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                               MPLS_STATUS_DESTROY) ==
                SNMP_FAILURE)
            {
                /*Failure Condition not handled here */
            }
            return CLI_FAILURE;
        }
    }                            /* End - if (TE_FALSE == bFoundOutSegment) */

    do
    {
        /* Create a destination entry */
        if ((nmhSetMplsTeP2mpTunnelDestRowStatus (u4TunnelIndex,
                                                  u4TunnelInstance, u4IngressId,
                                                  u4EgressId,
                                                  i4P2mpTnlSrcSubGrpOriginType,
                                                  &P2mpTnlSrcSubGrpOrigin,
                                                  u4P2mpTnlSrcSubGrpId,
                                                  i4P2mpTnlDestSubGrpOriginType,
                                                  &P2mpTnlDestSubGrpOrigin,
                                                  u4P2mpTnlDestSubGrpId,
                                                  i4P2mpTnlDestinationType,
                                                  &P2mpTnlDestination,
                                                  MPLS_STATUS_CREATE_AND_WAIT))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Failed to create P2MP destination "
                       "entry \r\n");
            bOutInterfaceDeleteFlag = TE_TRUE;
            break;
        }

        /* Add out-segment to destination entry */
        if ((nmhTestv2MplsTeP2mpTunnelDestBranchOutSegment
             (&u4ErrorCode, u4TunnelIndex, u4TunnelInstance, u4IngressId,
              u4EgressId, i4P2mpTnlSrcSubGrpOriginType, &P2mpTnlSrcSubGrpOrigin,
              u4P2mpTnlSrcSubGrpId, i4P2mpTnlDestSubGrpOriginType,
              &P2mpTnlDestSubGrpOrigin, u4P2mpTnlDestSubGrpId,
              i4P2mpTnlDestinationType, &P2mpTnlDestination,
              &OutSegmentIndex)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Failed to associate out-segment to "
                       "P2MP destination entry \r\n");
            bP2mpDestDeleteFlag = TE_TRUE;
            break;
        }
        if ((nmhSetMplsTeP2mpTunnelDestBranchOutSegment
             (u4TunnelIndex, u4TunnelInstance, u4IngressId, u4EgressId,
              i4P2mpTnlSrcSubGrpOriginType, &P2mpTnlSrcSubGrpOrigin,
              u4P2mpTnlSrcSubGrpId, i4P2mpTnlDestSubGrpOriginType,
              &P2mpTnlDestSubGrpOrigin, u4P2mpTnlDestSubGrpId,
              i4P2mpTnlDestinationType, &P2mpTnlDestination,
              &OutSegmentIndex)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Failed to associate out-segment to "
                       "P2MP destination entry \r\n");
            bP2mpDestDeleteFlag = TE_TRUE;
            break;
        }

        /* Make the destination row status as ACTIVE */
        if ((nmhTestv2MplsTeP2mpTunnelDestRowStatus
             (&u4ErrorCode, u4TunnelIndex, u4TunnelInstance, u4IngressId,
              u4EgressId, i4P2mpTnlSrcSubGrpOriginType, &P2mpTnlSrcSubGrpOrigin,
              u4P2mpTnlSrcSubGrpId, i4P2mpTnlDestSubGrpOriginType,
              &P2mpTnlDestSubGrpOrigin, u4P2mpTnlDestSubGrpId,
              i4P2mpTnlDestinationType, &P2mpTnlDestination,
              MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Failed to create P2MP destination "
                       "entry \r\n");
            bP2mpDestDeleteFlag = TE_TRUE;
            break;
        }
        if ((nmhSetMplsTeP2mpTunnelDestRowStatus
             (u4TunnelIndex, u4TunnelInstance, u4IngressId, u4EgressId,
              i4P2mpTnlSrcSubGrpOriginType, &P2mpTnlSrcSubGrpOrigin,
              u4P2mpTnlSrcSubGrpId, i4P2mpTnlDestSubGrpOriginType,
              &P2mpTnlDestSubGrpOrigin, u4P2mpTnlDestSubGrpId,
              i4P2mpTnlDestinationType, &P2mpTnlDestination,
              MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Failed to create P2MP destination "
                       "entry \r\n");
            bP2mpDestDeleteFlag = TE_TRUE;
            break;
        }
    }
    while (TE_ZERO);            /* End - do */
    /* Clean-up in case of failure */
    if ((TE_TRUE == bOutInterfaceDeleteFlag) ||
        (TE_TRUE == bP2mpDestDeleteFlag))
    {
        if (TE_TRUE == bP2mpDestDeleteFlag)
        {
            if (nmhSetMplsTeP2mpTunnelDestRowStatus (u4TunnelIndex,
                                                     u4TunnelInstance,
                                                     u4IngressId, u4EgressId,
                                                     i4P2mpTnlSrcSubGrpOriginType,
                                                     &P2mpTnlSrcSubGrpOrigin,
                                                     u4P2mpTnlSrcSubGrpId,
                                                     i4P2mpTnlDestSubGrpOriginType,
                                                     &P2mpTnlDestSubGrpOrigin,
                                                     u4P2mpTnlDestSubGrpId,
                                                     i4P2mpTnlDestinationType,
                                                     &P2mpTnlDestination,
                                                     MPLS_STATUS_DESTROY) ==
                SNMP_FAILURE)
            {
                /*Failure Condition not handled here */
            }
        }
        if (TE_FALSE == bFoundOutSegment)
        {
            MplsDeleteMplsIfOrMplsTnlIf (u4OutIfIndex, u4MplsTnlIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            if (nmhSetMplsXCRowStatus
                (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                 MPLS_STATUS_DESTROY) == SNMP_FAILURE)
            {
                /*Failure Condition not handled here */
            }
            if (nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                               MPLS_STATUS_DESTROY) ==
                SNMP_FAILURE)
            {
                /*Failure Condition not handled here */
            }
        }
        return CLI_FAILURE;
    }

    do
    {
        if (TE_TRUE == bFoundOutSegment)
        {
            break;
        }

        if (nmhTestv2MplsXCStorageType (&u4ErrorCode, &XCIndex, &InSegmentIndex,
                                        &OutSegmentIndex,
                                        MPLS_STORAGE_NONVOLATILE)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%% Unable to set storage type\r\n");
            bP2mpDestDeleteFlag = TE_TRUE;
            break;
        }

        if (nmhSetMplsXCStorageType
            (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
             MPLS_STORAGE_NONVOLATILE) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%% Unable to set storage type\r\n");
            bP2mpDestDeleteFlag = TE_TRUE;
            break;
        }

        /*  Make XC RowStatus Active */
        if ((nmhTestv2MplsXCRowStatus (&u4ErrorCode, &XCIndex, &InSegmentIndex,
                                       &OutSegmentIndex,
                                       MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set the XC row status active");
            bP2mpDestDeleteFlag = TE_TRUE;
            break;
        }
        if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                                    MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Unable to set the XC row status active");
            if (nmhSetMplsTeP2mpTunnelDestRowStatus (u4TunnelIndex,
                                                     u4TunnelInstance,
                                                     u4IngressId, u4EgressId,
                                                     i4P2mpTnlSrcSubGrpOriginType,
                                                     &P2mpTnlSrcSubGrpOrigin,
                                                     u4P2mpTnlSrcSubGrpId,
                                                     i4P2mpTnlDestSubGrpOriginType,
                                                     &P2mpTnlDestSubGrpOrigin,
                                                     u4P2mpTnlDestSubGrpId,
                                                     i4P2mpTnlDestinationType,
                                                     &P2mpTnlDestination,
                                                     MPLS_STATUS_DESTROY) ==
                SNMP_FAILURE)
            {
                /*Failure Condition not handled here */
            }
            MplsDeleteMplsIfOrMplsTnlIf (u4OutIfIndex, u4MplsTnlIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            if (nmhSetMplsXCRowStatus
                (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                 MPLS_STATUS_DESTROY) == SNMP_FAILURE)
            {
                /*Failure Condition not handled here */
            }
            if (nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                               MPLS_STATUS_DESTROY) ==
                SNMP_FAILURE)
            {
                /*Failure Condition not handled here */
            }
            return CLI_FAILURE;
        }
    }
    while (TE_ZERO);
    /* Clean-up in case of failure */
    if (TE_TRUE == bP2mpDestDeleteFlag)
    {
        if (nmhSetMplsTeP2mpTunnelDestRowStatus
            (u4TunnelIndex, u4TunnelInstance, u4IngressId, u4EgressId,
             i4P2mpTnlSrcSubGrpOriginType, &P2mpTnlSrcSubGrpOrigin,
             u4P2mpTnlSrcSubGrpId, i4P2mpTnlDestSubGrpOriginType,
             &P2mpTnlDestSubGrpOrigin, u4P2mpTnlDestSubGrpId,
             i4P2mpTnlDestinationType, &P2mpTnlDestination,
             MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            /*Failure Condition not handled here */
        }
        MplsDeleteMplsIfOrMplsTnlIf (u4OutIfIndex, u4MplsTnlIfIndex,
                                     CFA_MPLS_TUNNEL, MPLS_TRUE);
        if (nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                                   MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            /*Failure Condition not handled here */
        }
        if (nmhSetMplsOutSegmentRowStatus
            (&OutSegmentIndex, MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            /*Failure Condition not handled here */
        }
        return CLI_FAILURE;
    }

    /* All set operations have been successful */
    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCliP2mpAddDestinationAtIngress : EXIT \n");
    return CLI_SUCCESS;
}

/*******************************************************************************
* Function Name : TeCliP2mpStaticBind
* Description   : This routine is used to add destination to a P2MP 
*                 tunnel at intermediate LSR and associates labels with it
* Input(s)      : CliHandle              - Cli Context Handle
                  u4P2mpDestId           - Identity of the P2MP destination LSR
                  u4P2mpDestLocalMapNum  - P2MP destination local map number 
                                           for global-id and node-id 
                                           combination
                  u4InLabel              - Incoming Label associated with
                                           in-segment
                  u4InIfIndex            - Index for the incoming MPLS interface
                  u4OutLabel             - Label associated with the outgoing
                                           packet
                  u4NextHop              - Index for the outgoing MPLS interface
                  u4OutIfIndex           - Outgoing interface
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT1
TeCliP2mpStaticBind (tCliHandle CliHandle, UINT4 u4P2mpDestId,
                     UINT4 u4P2mpDestLocalMapNum, UINT4 u4InLabel,
                     UINT4 u4InIfIndex, UINT4 u4OutLabel, UINT4 u4NextHop,
                     UINT4 u4OutIfIndex)
{
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OCTET_STRING_TYPE NextHopAddr;

    tSNMP_OCTET_STRING_TYPE P2mpTnlSrcSubGrpOrigin;
    tSNMP_OCTET_STRING_TYPE P2mpTnlDestSubGrpOrigin;
    tSNMP_OCTET_STRING_TYPE P2mpTnlDestination;

    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1NextHopAddr[MPLS_INDEX_LENGTH];

    static UINT1        au1P2mpTnlSrcSubGrpOrigin[ROUTER_ID_LENGTH];
    static UINT1        au1P2mpTnlDestSubGrpOrigin[ROUTER_ID_LENGTH];
    static UINT1        au1P2mpTnlDestination[ROUTER_ID_LENGTH];

    UINT4               u4TunnelIndex = TE_ZERO;
    UINT4               u4TunnelInstance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    UINT4               u4ErrorCode = TE_ZERO;
    UINT4               u4Mask = 0xffffffff;
    INT4                i4TnlRole = TE_ZERO;

    INT4                i4P2mpTnlSrcSubGrpOriginType = LSR_ADDR_IPV4;
    INT4                i4P2mpTnlDestSubGrpOriginType = LSR_ADDR_IPV4;
    INT4                i4P2mpTnlDestinationType = LSR_ADDR_IPV4;
    UINT4               u4P2mpTnlSrcSubGrpId = TE_ZERO;
    UINT4               u4P2mpTnlDestSubGrpId = TE_ONE;

    UINT4               u4OutSegIndex = TE_ZERO;
    UINT4               u4MplsTnlOutIfIndex = TE_ZERO;

    UINT4               u4InSegIndex = TE_ZERO;
    INT4                i4ConfigInIfIndex = TE_ZERO;
    UINT4               u4MplsTnlInIfIndex = TE_ZERO;

    UINT4               u4XCIndex = TE_ZERO;
#ifdef CFA_WANTED
    UINT4               u4MplsIfIndex = TE_ZERO;
#endif
    UINT4               u4ConfigInLabel = TE_ZERO;
    UINT4               u4L3Intf = TE_ZERO;

    tTeTnlInfo         *pTeTnlInfo = NULL;
    tP2mpBranchEntry   *pTeP2mpBranchEntry = NULL;
    tOutSegment        *pOutSegment = NULL;
    tXcEntry           *pXCEntry = NULL;
    tNetIpv4RtInfo      NetIpRtInfo;
    tRtInfoQueryMsg     RtQuery;

    BOOL1               bFoundInSegment = TE_FALSE;
    BOOL1               bFoundOutSegment = TE_FALSE;
    BOOL1               bNextHopCfg = TE_FALSE;

    /* Flags for clean-up in case of failure */
    BOOL1               bInSegmentDeleteFlag = TE_FALSE;
    BOOL1               bInInterfaceDeleteFlag = TE_FALSE;
    BOOL1               bOutSegmentDeleteFlag = TE_FALSE;
    BOOL1               bOutInterfaceDeleteFlag = TE_FALSE;
    BOOL1               bP2mpDestDeleteFlag = TE_FALSE;
    BOOL1               bXcDeleteFlag = TE_FALSE;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCliP2mpStaticBind : ENTRY \n");

    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;

    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;

    XCIndex.pu1_OctetList = au1XCIndex;
    XCIndex.i4_Length = MPLS_INDEX_LENGTH;

    NextHopAddr.pu1_OctetList = au1NextHopAddr;
    NextHopAddr.i4_Length = MPLS_INDEX_LENGTH;

    P2mpTnlSrcSubGrpOrigin.pu1_OctetList = au1P2mpTnlSrcSubGrpOrigin;
    P2mpTnlSrcSubGrpOrigin.i4_Length = ROUTER_ID_LENGTH;

    P2mpTnlDestSubGrpOrigin.pu1_OctetList = au1P2mpTnlDestSubGrpOrigin;
    P2mpTnlDestSubGrpOrigin.i4_Length = ROUTER_ID_LENGTH;

    P2mpTnlDestination.pu1_OctetList = au1P2mpTnlDestination;
    P2mpTnlDestination.i4_Length = ROUTER_ID_LENGTH;

    MEMSET (au1InSegIndex, TE_ZERO, MPLS_INDEX_LENGTH);
    MEMSET (au1OutSegIndex, TE_ZERO, MPLS_INDEX_LENGTH);
    MEMSET (au1XCIndex, TE_ZERO, MPLS_INDEX_LENGTH);
    MEMSET (au1NextHopAddr, TE_ZERO, MPLS_INDEX_LENGTH);
    MEMSET (au1P2mpTnlSrcSubGrpOrigin, TE_ZERO, ROUTER_ID_LENGTH);
    MEMSET (au1P2mpTnlDestSubGrpOrigin, TE_ZERO, ROUTER_ID_LENGTH);
    MEMSET (au1P2mpTnlDestination, TE_ZERO, ROUTER_ID_LENGTH);

    /* Check if input P2MP destination is valid */
    if (TE_ZERO != u4P2mpDestId)
    {
        MPLS_INTEGER_TO_OCTETSTRING (u4P2mpDestId, (&P2mpTnlDestination));
    }
    else if (TE_ZERO != u4P2mpDestLocalMapNum)
    {
        MPLS_INTEGER_TO_OCTETSTRING (u4P2mpDestLocalMapNum,
                                     (&P2mpTnlDestination));
    }
    else
    {
        CliPrintf (CliHandle, "\r\n%%Wrong P2MP Destination ID \r\n");
        return CLI_FAILURE;
    }

    /* Get the TunnelId, TunnelInstance, Ingress and Egress LSR ID */
    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    /* If tunnel does not exist, return failure */
    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                  u4EgressId);
    if (pTeTnlInfo == NULL)
    {
        MPLS_CMN_UNLOCK ();
        CliPrintf (CliHandle, "\r\n%%Tunnel does not exist\r\n");
        return CLI_FAILURE;
    }
    /* Check the tunnel type */
    if (NULL == TE_P2MP_TNL_INFO (pTeTnlInfo))
    {
        MPLS_CMN_UNLOCK ();
        CliPrintf (CliHandle, "\r\n%%Not a P2MP tunnel. Cannot associate "
                   "labels to destinations\r\n");
        return CLI_FAILURE;
    }
    MPLS_CMN_UNLOCK ();

    /* Check if the tunnel role is intermediate */
    if (nmhGetMplsTunnelRole (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                              u4EgressId, &i4TnlRole) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to get the tunnel role\r\n");
        return CLI_FAILURE;
    }
    if (i4TnlRole == TE_INGRESS)
    {
        CliPrintf (CliHandle, "\r\n%%Wrong label configuration for Ingress "
                   "tunnel\r\n");
        return CLI_FAILURE;
    }
    else if (i4TnlRole == TE_EGRESS)
    {
        CliPrintf (CliHandle, "\r\n%%Wrong label configuration for Egress "
                   "tunnel\r\n");
        return CLI_FAILURE;
    }

    if (TE_ZERO == u4OutIfIndex)
    {
        bNextHopCfg = TE_TRUE;
        if (NetIpv4IfIsOurAddress (u4NextHop) == NETIPV4_SUCCESS)
        {
            CliPrintf (CliHandle, "\r%%Next hop is same as self-ip addr\n");
            return CLI_FAILURE;
        }

        MEMSET (&NetIpRtInfo, TE_ZERO, sizeof (tNetIpv4RtInfo));
        MEMSET (&RtQuery, TE_ZERO, sizeof (tRtInfoQueryMsg));
        RtQuery.u4DestinationIpAddress = u4NextHop;
        RtQuery.u4DestinationSubnetMask = u4Mask;
        RtQuery.u1QueryFlag = RTM_QUERIED_FOR_NEXT_HOP;

        if (NetIpv4GetRoute (&RtQuery, &NetIpRtInfo) == NETIPV4_SUCCESS)
        {
            if (NetIpv4GetCfaIfIndexFromPort (NetIpRtInfo.u4RtIfIndx,
                                              &u4OutIfIndex) == NETIPV4_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Unable to fetch Next hop information\n");
                return CLI_FAILURE;
            }
        }
#ifdef CFA_WANTED
        else
        {
            if (CfaIpIfGetIfIndexFromHostIpAddressInCxt (MPLS_DEF_CONTEXT_ID,
                                                         u4NextHop,
                                                         &u4OutIfIndex)
                == CFA_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Unable to fetch Next hop information\n");
                return CLI_FAILURE;
            }
        }
    }
    if (CfaUtilGetMplsIfFromIfIndex (u4OutIfIndex, &u4MplsIfIndex, TRUE)
        == CFA_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% Enable MPLS over outgoing interface\r\n");
        return CLI_FAILURE;
    }
#else
    }
#endif

    /* Check if destination is already present in P2MP destination table,
     * same type as ingress LSR. If destination is a local map number, check
     * if it is present in the node map table */
    if ((nmhTestv2MplsTeP2mpTunnelDestRowStatus (&u4ErrorCode, u4TunnelIndex,
                                                 u4TunnelInstance, u4IngressId,
                                                 u4EgressId,
                                                 i4P2mpTnlSrcSubGrpOriginType,
                                                 &P2mpTnlSrcSubGrpOrigin,
                                                 u4P2mpTnlSrcSubGrpId,
                                                 i4P2mpTnlDestSubGrpOriginType,
                                                 &P2mpTnlDestSubGrpOrigin,
                                                 u4P2mpTnlDestSubGrpId,
                                                 i4P2mpTnlDestinationType,
                                                 &P2mpTnlDestination,
                                                 MPLS_STATUS_CREATE_AND_WAIT))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%P2MP destination entry creation "
                   "failed \r\n");
        return CLI_FAILURE;
    }

    /* Scan the branch table to check if same out label has been configured 
     * for another destination of the P2MP tunnel */
    MPLS_CMN_LOCK ();
    TMO_SLL_Scan (&(TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo)),
                  pTeP2mpBranchEntry, tP2mpBranchEntry *)
    {
        u4OutSegIndex = TE_P2MP_BRANCH_OUTSEG_INDEX (pTeP2mpBranchEntry);
        pOutSegment = MplsGetOutSegmentTableEntry (u4OutSegIndex);
        if ((NULL != pOutSegment) &&
            (u4OutLabel == OUTSEGMENT_LABEL (pOutSegment)))
        {
            /* Check if next-hop in out-segment and input next-hop address 
             * are same */
            if (u4NextHop != OUTSEGMENT_NH_ADDR (pOutSegment))
            {
                MPLS_CMN_UNLOCK ();
                CliPrintf (CliHandle, "\r\n%%Next hop addr configured "
                           "for the same out label does not match with "
                           "input next hop addr \r\n");
                return CLI_FAILURE;
            }
            MPLS_INTEGER_TO_OCTETSTRING (u4OutSegIndex, (&OutSegmentIndex));
            bFoundOutSegment = TE_TRUE;
            break;
        }
    }

    if (TE_FALSE == bFoundOutSegment)
    {
        /* Retrieve the first P2MP branch entry */
        if (TMO_SLL_Count (&(TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo)))
            != TE_ZERO)
        {
            pTeP2mpBranchEntry =
                (tP2mpBranchEntry *)
                TMO_SLL_First (&(TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo)));
        }
        else
        {
            /* Out-segment is not configured for this P2MP tunnel */
            pTeP2mpBranchEntry = NULL;
        }
    }
    MPLS_CMN_UNLOCK ();

    /* Check if the input label is same as previously configured in-label */
    if (NULL != pTeP2mpBranchEntry)
    {
        if (TE_FALSE == bFoundOutSegment)
        {
            /* Matching out-segment is not present. Retrieve out-segment index 
             * from the first branch entry */
            u4OutSegIndex = TE_P2MP_BRANCH_OUTSEG_INDEX (pTeP2mpBranchEntry);
            MPLS_CMN_LOCK ();
            pOutSegment = MplsGetOutSegmentTableEntry (u4OutSegIndex);
            if (NULL != pOutSegment)
            {
                MPLS_INTEGER_TO_OCTETSTRING (u4OutSegIndex, (&OutSegmentIndex));
            }
            MPLS_CMN_UNLOCK ();
        }

        if (NULL == pOutSegment)
        {
            CliPrintf (CliHandle, "\r\n%%Out-segment entry does not exist\r\n");
            return CLI_FAILURE;
        }
        pXCEntry = OUTSEGMENT_XC_INDEX (pOutSegment);
        if ((NULL != pXCEntry) && (NULL != XC_ININDEX (pXCEntry)))
        {
            u4XCIndex = XC_INDEX (pXCEntry);
            MPLS_INTEGER_TO_OCTETSTRING (u4XCIndex, (&XCIndex));
            u4InSegIndex = (XC_ININDEX (pXCEntry))->u4Index;
            MPLS_INTEGER_TO_OCTETSTRING (u4InSegIndex, (&InSegmentIndex));
            if (TE_ZERO != u4InSegIndex)
            {
                if (nmhGetMplsInSegmentLabel (&InSegmentIndex, &u4ConfigInLabel)
                    == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "\r\n%%Unable to fetch the insegment "
                               "Label entry\r\n");
                    return CLI_FAILURE;
                }
                if (u4ConfigInLabel != u4InLabel)
                {
                    CliPrintf (CliHandle, "\r\n%%Input in-label does not match "
                               "with previously configured " "in-label \r\n");
                    return CLI_FAILURE;
                }
                if ((nmhGetMplsInSegmentInterface (&InSegmentIndex,
                                                   &i4ConfigInIfIndex)) ==
                    SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "\r\n%%Unable to fetch the insegment"
                               "Interface entry\r\n");
                    return CLI_FAILURE;
                }

                if (MplsGetL3Intf ((UINT4) i4ConfigInIfIndex, &u4L3Intf) ==
                    MPLS_FAILURE)
                {
                    return CLI_FAILURE;
                }

                if (u4L3Intf != u4InIfIndex)
                {
                    CliPrintf (CliHandle, "\r\n%%Input interface does not match"
                               " with previously configured incoming"
                               " interface \r\n");
                    return CLI_FAILURE;
                }
                bFoundInSegment = TE_TRUE;
            }
        }
    }                            /* End - if (NULL != pTeP2mpBranchEntry) */

    /* Create in-segment if it is not present */
    if (TE_FALSE == bFoundInSegment)
    {
        do
        {
            if ((nmhGetMplsInSegmentIndexNext (&InSegmentIndex))
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_NO_FREE_INDEX);
                return CLI_FAILURE;
            }
            if ((nmhTestv2MplsInSegmentRowStatus (&u4ErrorCode, &InSegmentIndex,
                                                  MPLS_STATUS_CREATE_AND_WAIT))
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_INSEG_ENTRY_CREATION);
                return CLI_FAILURE;
            }
            if ((nmhSetMplsInSegmentRowStatus (&InSegmentIndex,
                                               MPLS_STATUS_CREATE_AND_WAIT)) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_INSEG_ENTRY_CREATION);
                return CLI_FAILURE;
            }

            /* Set inlabel value in seg table */
            if ((nmhTestv2MplsInSegmentLabel (&u4ErrorCode, &InSegmentIndex,
                                              u4InLabel)) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_LABEL);
                bInSegmentDeleteFlag = TE_TRUE;
                break;
            }
            if ((nmhSetMplsInSegmentLabel (&InSegmentIndex, u4InLabel))
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_LABEL);
                bInSegmentDeleteFlag = TE_TRUE;
                break;
            }

            /* Set InSegmentNPop to TRUE */
            if ((nmhTestv2MplsInSegmentNPop (&u4ErrorCode, &InSegmentIndex,
                                             MPLS_TRUE)) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_NPOP);
                bInSegmentDeleteFlag = TE_TRUE;
                break;
            }
            if ((nmhSetMplsInSegmentNPop (&InSegmentIndex, MPLS_TRUE))
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_NPOP);
                bInSegmentDeleteFlag = TE_TRUE;
                break;
            }

            /* Set the InSegment IP addr family to IPv4 */
            if (nmhTestv2MplsInSegmentAddrFamily (&u4ErrorCode, &InSegmentIndex,
                                                  LSR_ADDR_IPV4) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_ADDR_FAMILY);
                bInSegmentDeleteFlag = TE_TRUE;
                break;
            }
            if (nmhSetMplsInSegmentAddrFamily (&InSegmentIndex, LSR_ADDR_IPV4)
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_ADDR_FAMILY);
                bInSegmentDeleteFlag = TE_TRUE;
                break;
            }

            /* Create incoming interface */
            if (CfaGetFreeInterfaceIndex (&u4MplsTnlInIfIndex, CFA_MPLS_TUNNEL)
                == OSIX_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_NO_FREE_TUNNEL_INTERFACE);
                bInSegmentDeleteFlag = TE_TRUE;
                break;
            }
            if ((nmhTestv2MplsInSegmentInterface (&u4ErrorCode, &InSegmentIndex,
                                                  (INT4) u4MplsTnlInIfIndex)) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_IF);
                bInSegmentDeleteFlag = TE_TRUE;
                break;
            }
            if ((nmhSetMplsInSegmentInterface (&InSegmentIndex,
                                               (INT4) u4MplsTnlInIfIndex)) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_INSEG_IF);
                bInSegmentDeleteFlag = TE_TRUE;
                break;
            }
            if (MplsCreateMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                             CFA_MPLS_TUNNEL) == MPLS_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_ERR_INSTACK_ENTRY_CREATION);
                bInSegmentDeleteFlag = TE_TRUE;
                break;
            }
        }
        while (TE_ZERO);        /* End - do */
        /* Clean-up in case of failure */
        if (TE_TRUE == bInSegmentDeleteFlag)
        {
            if (nmhSetMplsInSegmentRowStatus
                (&InSegmentIndex, MPLS_STATUS_DESTROY) == SNMP_FAILURE)
            {
                /*Failure Condition not handled here */
            }
            return CLI_FAILURE;
        }
    }                            /* End - if (TE_FALSE == bFoundInSegment) */

    /* If out-segment entry is not present, create out-segment and XC entry */
    if (TE_FALSE == bFoundOutSegment)
    {
        do
        {
            if ((nmhGetMplsOutSegmentIndexNext (&OutSegmentIndex))
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_NO_FREE_INDEX);
                bInInterfaceDeleteFlag = TE_TRUE;
                break;
            }
            if ((nmhTestv2MplsOutSegmentRowStatus (&u4ErrorCode,
                                                   &OutSegmentIndex,
                                                   MPLS_STATUS_CREATE_AND_WAIT))
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_OUTSEG_ENTRY_CREATION);
                bInInterfaceDeleteFlag = TE_TRUE;
                break;
            }
            if ((nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                                MPLS_STATUS_CREATE_AND_WAIT)) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_OUTSEG_ENTRY_CREATION);
                bInInterfaceDeleteFlag = TE_TRUE;
                break;
            }

            MPLS_OCTETSTRING_TO_INTEGER ((&OutSegmentIndex), u4OutSegIndex);

            /* Create a row in XC table with newly created out-segment index. 
             * If branch entry is already present, use XC index retrieved from 
             * out-segment index of first branch entry */
            if (TE_FALSE == bFoundInSegment)
            {
                if ((nmhGetMplsXCIndexNext (&XCIndex)) == SNMP_FAILURE)
                {
                    CLI_SET_ERR (MPLS_CLI_NO_FREE_INDEX);
                    bOutSegmentDeleteFlag = TE_TRUE;
                    break;
                }
            }
            if ((nmhTestv2MplsXCRowStatus (&u4ErrorCode, &XCIndex,
                                           &InSegmentIndex, &OutSegmentIndex,
                                           MPLS_STATUS_CREATE_AND_WAIT)) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_XC_ENTRY_CREATION);
                bOutSegmentDeleteFlag = TE_TRUE;
                break;
            }
            if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex,
                                        &OutSegmentIndex,
                                        MPLS_STATUS_CREATE_AND_WAIT)) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_XC_ENTRY_CREATION);
                bOutSegmentDeleteFlag = TE_TRUE;
                break;
            }

            /* Set outsegment top label */
            if (nmhTestv2MplsOutSegmentTopLabel (&u4ErrorCode, &OutSegmentIndex,
                                                 u4OutLabel) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_LABEL);
                bXcDeleteFlag = TE_TRUE;
                break;
            }
            if (nmhSetMplsOutSegmentTopLabel (&OutSegmentIndex, u4OutLabel)
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_ERR_OUTSEG_LABEL);
                bXcDeleteFlag = TE_TRUE;
                break;
            }

            /* set out segment push top label to true */
            if (nmhTestv2MplsOutSegmentPushTopLabel (&u4ErrorCode,
                                                     &OutSegmentIndex,
                                                     MPLS_TRUE) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_PUSH_TOP_LABEL);
                bXcDeleteFlag = TE_TRUE;
                break;
            }
            if (nmhSetMplsOutSegmentPushTopLabel (&OutSegmentIndex,
                                                  MPLS_TRUE) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_PUSH_TOP_LABEL);
                bXcDeleteFlag = TE_TRUE;
                break;
            }

            if (TE_TRUE == bNextHopCfg)
            {
                /* Set the Next hop addr type to ipv4 */
                if (nmhTestv2MplsOutSegmentNextHopAddrType (&u4ErrorCode,
                                                            &OutSegmentIndex,
                                                            LSR_ADDR_IPV4) ==
                    SNMP_FAILURE)
                {
                    CLI_SET_ERR (MPLS_CLI_TE_ERR_NEXT_HOP_ADDR_TYPE);
                    bXcDeleteFlag = TE_TRUE;
                    break;
                }
                if (nmhSetMplsOutSegmentNextHopAddrType (&OutSegmentIndex,
                                                         LSR_ADDR_IPV4) ==
                    SNMP_FAILURE)
                {
                    CLI_SET_ERR (MPLS_CLI_TE_ERR_NEXT_HOP_ADDR_TYPE);
                    bXcDeleteFlag = TE_TRUE;
                    break;
                }

                /* Set nexthop address */
                MPLS_INTEGER_TO_OCTETSTRING (u4NextHop, (&NextHopAddr));
                if (nmhTestv2MplsOutSegmentNextHopAddr (&u4ErrorCode,
                                                        &OutSegmentIndex,
                                                        &NextHopAddr) ==
                    SNMP_FAILURE)
                {
                    CLI_SET_ERR (MPLS_CLI_TE_ERR_NEXT_HOP_ADDR);
                    bXcDeleteFlag = TE_TRUE;
                    break;
                }
                if (nmhSetMplsOutSegmentNextHopAddr (&OutSegmentIndex,
                                                     &NextHopAddr) ==
                    SNMP_FAILURE)
                {
                    CLI_SET_ERR (MPLS_CLI_TE_ERR_NEXT_HOP_ADDR);
                    bXcDeleteFlag = TE_TRUE;
                    break;
                }
            }

            /* Create outgoing interface */
            if (CfaGetFreeInterfaceIndex (&u4MplsTnlOutIfIndex, CFA_MPLS_TUNNEL)
                == OSIX_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_NO_FREE_TUNNEL_INTERFACE);
                bXcDeleteFlag = TE_TRUE;
                break;
            }
            if ((nmhTestv2MplsOutSegmentInterface (&u4ErrorCode,
                                                   &OutSegmentIndex,
                                                   (INT4) u4MplsTnlOutIfIndex))
                == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_IF);
                bXcDeleteFlag = TE_TRUE;
                break;
            }
            if ((nmhSetMplsOutSegmentInterface (&OutSegmentIndex,
                                                (INT4) u4MplsTnlOutIfIndex)) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_TE_ERR_OUTSEG_IF);
                bXcDeleteFlag = TE_TRUE;
                break;
            }
            /* Create a new MPLS Tunnel Interface and stack over MPLS If */
            if (MplsCreateMplsIfOrMplsTnlIf (u4OutIfIndex, u4MplsTnlOutIfIndex,
                                             CFA_MPLS_TUNNEL) == MPLS_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_ERR_OUTSTACK_ENTRY_CREATION);
                bXcDeleteFlag = TE_TRUE;
                break;
            }

            if (nmhTestv2MplsOutSegmentStorageType
                (&u4ErrorCode, &OutSegmentIndex,
                 MPLS_STORAGE_NONVOLATILE) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n%%Unable to set the Out segment "
                           "storage type");
                bOutInterfaceDeleteFlag = TE_TRUE;
                break;
            }

            if (nmhSetMplsOutSegmentStorageType (&OutSegmentIndex,
                                                 MPLS_STORAGE_NONVOLATILE) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n%%Unable to set the Out segment "
                           "storage type");
                bOutInterfaceDeleteFlag = TE_TRUE;
                break;
            }

            if ((nmhTestv2MplsOutSegmentRowStatus (&u4ErrorCode,
                                                   &OutSegmentIndex,
                                                   MPLS_STATUS_ACTIVE)) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n%%Unable to set the Out segment "
                           "row status active");
                bOutInterfaceDeleteFlag = TE_TRUE;
                break;
            }
            if ((nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                                MPLS_STATUS_ACTIVE)) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n%%Unable to set the Out segment "
                           "row status active");
                bOutInterfaceDeleteFlag = TE_TRUE;
                break;
            }

            /* Set the row status of the InSegment as active */
            if (TE_FALSE == bFoundInSegment)
            {
                if (nmhTestv2MplsInSegmentStorageType (&u4ErrorCode,
                                                       &InSegmentIndex,
                                                       MPLS_STORAGE_NONVOLATILE)
                    == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "\r\n%%Unable to set the In segment "
                               "storage type");
                    bOutInterfaceDeleteFlag = TE_TRUE;
                    break;
                }

                if (nmhSetMplsInSegmentStorageType (&InSegmentIndex,
                                                    MPLS_STORAGE_NONVOLATILE) ==
                    SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "\r\n%%Unable to set the In segment "
                               "storage type");
                    bOutInterfaceDeleteFlag = TE_TRUE;
                    break;
                }

                if ((nmhTestv2MplsInSegmentRowStatus (&u4ErrorCode,
                                                      &InSegmentIndex,
                                                      MPLS_STATUS_ACTIVE))
                    == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "\r\n%%Unable to set the In segment "
                               "row status active");
                    bOutInterfaceDeleteFlag = TE_TRUE;
                    break;
                }
                if ((nmhSetMplsInSegmentRowStatus (&InSegmentIndex,
                                                   MPLS_STATUS_ACTIVE))
                    == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle, "\r\n%%Unable to set the In segment "
                               "row status active");
                    bOutInterfaceDeleteFlag = TE_TRUE;
                    break;
                }
            }
        }
        while (TE_ZERO);        /* End - do */
        /* Clean-up in case of failure */
        if ((TE_TRUE == bXcDeleteFlag) || (TE_TRUE == bOutInterfaceDeleteFlag))
        {
            if (TE_TRUE == bOutInterfaceDeleteFlag)
            {
                MplsDeleteMplsIfOrMplsTnlIf (u4OutIfIndex, u4MplsTnlOutIfIndex,
                                             CFA_MPLS_TUNNEL, MPLS_TRUE);
            }
            if (nmhSetMplsXCRowStatus
                (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                 MPLS_STATUS_DESTROY) == SNMP_FAILURE)
            {
                /*Failure Condition not handled here */
            }
            if (nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                               MPLS_STATUS_DESTROY) ==
                SNMP_FAILURE)
            {
                /*Failure Condition not handled here */
            }
            if (TE_FALSE == bFoundInSegment)
            {
                MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                             CFA_MPLS_TUNNEL, MPLS_TRUE);
                if (nmhSetMplsInSegmentRowStatus (&InSegmentIndex,
                                                  MPLS_STATUS_DESTROY) ==
                    SNMP_FAILURE)
                {
                    /*Failure Condition not handled here */
                }
            }
            return CLI_FAILURE;
        }
        else if ((TE_TRUE == bInInterfaceDeleteFlag) ||
                 (TE_TRUE == bOutSegmentDeleteFlag))
        {
            if (TE_TRUE == bOutSegmentDeleteFlag)
            {
                if (nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                                   MPLS_STATUS_DESTROY) ==
                    SNMP_FAILURE)
                {
                    /*Failure Condition not handled here */
                }
            }
            if (TE_FALSE == bFoundInSegment)
            {
                MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                             CFA_MPLS_TUNNEL, MPLS_TRUE);
                if (nmhSetMplsInSegmentRowStatus (&InSegmentIndex,
                                                  MPLS_STATUS_DESTROY) ==
                    SNMP_FAILURE)
                {
                    /*Failure Condition not handled here */
                }

            }
            return CLI_FAILURE;
        }
    }                            /* End - if (TE_FALSE == bFoundOutSegment) */

    do
    {
        /* Create a destination entry */
        if ((nmhSetMplsTeP2mpTunnelDestRowStatus
             (u4TunnelIndex, u4TunnelInstance, u4IngressId, u4EgressId,
              i4P2mpTnlSrcSubGrpOriginType, &P2mpTnlSrcSubGrpOrigin,
              u4P2mpTnlSrcSubGrpId, i4P2mpTnlDestSubGrpOriginType,
              &P2mpTnlDestSubGrpOrigin, u4P2mpTnlDestSubGrpId,
              i4P2mpTnlDestinationType, &P2mpTnlDestination,
              MPLS_STATUS_CREATE_AND_WAIT)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to create the p2mp "
                       "destination tunnel");
            bOutInterfaceDeleteFlag = TE_TRUE;
            break;
        }

        /* Add out-segment to destination entry */
        if ((nmhTestv2MplsTeP2mpTunnelDestBranchOutSegment
             (&u4ErrorCode, u4TunnelIndex, u4TunnelInstance, u4IngressId,
              u4EgressId, i4P2mpTnlSrcSubGrpOriginType, &P2mpTnlSrcSubGrpOrigin,
              u4P2mpTnlSrcSubGrpId, i4P2mpTnlDestSubGrpOriginType,
              &P2mpTnlDestSubGrpOrigin, u4P2mpTnlDestSubGrpId,
              i4P2mpTnlDestinationType, &P2mpTnlDestination,
              &OutSegmentIndex)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set P2MP tunnel destination"
                       "branch out segment");
            bP2mpDestDeleteFlag = TE_TRUE;
            break;
        }
        if ((nmhSetMplsTeP2mpTunnelDestBranchOutSegment
             (u4TunnelIndex, u4TunnelInstance, u4IngressId, u4EgressId,
              i4P2mpTnlSrcSubGrpOriginType, &P2mpTnlSrcSubGrpOrigin,
              u4P2mpTnlSrcSubGrpId, i4P2mpTnlDestSubGrpOriginType,
              &P2mpTnlDestSubGrpOrigin, u4P2mpTnlDestSubGrpId,
              i4P2mpTnlDestinationType, &P2mpTnlDestination,
              &OutSegmentIndex)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set P2MP tunnel destination"
                       "branch out segment");
            bP2mpDestDeleteFlag = TE_TRUE;
            break;
        }

        /* Make the destination row status as ACTIVE */
        if ((nmhTestv2MplsTeP2mpTunnelDestRowStatus
             (&u4ErrorCode, u4TunnelIndex, u4TunnelInstance, u4IngressId,
              u4EgressId, i4P2mpTnlSrcSubGrpOriginType, &P2mpTnlSrcSubGrpOrigin,
              u4P2mpTnlSrcSubGrpId, i4P2mpTnlDestSubGrpOriginType,
              &P2mpTnlDestSubGrpOrigin, u4P2mpTnlDestSubGrpId,
              i4P2mpTnlDestinationType, &P2mpTnlDestination,
              MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set P2MP tunnel destination"
                       "row status active");
            bP2mpDestDeleteFlag = TE_TRUE;
            break;
        }
        if ((nmhSetMplsTeP2mpTunnelDestRowStatus
             (u4TunnelIndex, u4TunnelInstance, u4IngressId, u4EgressId,
              i4P2mpTnlSrcSubGrpOriginType, &P2mpTnlSrcSubGrpOrigin,
              u4P2mpTnlSrcSubGrpId, i4P2mpTnlDestSubGrpOriginType,
              &P2mpTnlDestSubGrpOrigin, u4P2mpTnlDestSubGrpId,
              i4P2mpTnlDestinationType, &P2mpTnlDestination,
              MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set P2MP tunnel destination"
                       "row status active");
            bP2mpDestDeleteFlag = TE_TRUE;
            break;
        }

        if (TE_FALSE == bFoundOutSegment)
        {
            if (nmhTestv2MplsXCStorageType
                (&u4ErrorCode, &XCIndex, &InSegmentIndex, &OutSegmentIndex,
                 MPLS_STORAGE_NONVOLATILE) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n%% Unable to set storage type\r\n");
                bP2mpDestDeleteFlag = TE_TRUE;
                break;
            }

            if (nmhSetMplsXCStorageType (&XCIndex, &InSegmentIndex,
                                         &OutSegmentIndex,
                                         MPLS_STORAGE_NONVOLATILE) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n%% Unable to set storage type\r\n");
                bP2mpDestDeleteFlag = TE_TRUE;
                break;
            }

            /*  Make XC RowStatus Active */
            if ((nmhTestv2MplsXCRowStatus (&u4ErrorCode, &XCIndex,
                                           &InSegmentIndex, &OutSegmentIndex,
                                           MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%%Unable to set the XC row status active");
                bP2mpDestDeleteFlag = TE_TRUE;
                break;
            }
            if ((nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex,
                                        &OutSegmentIndex,
                                        MPLS_STATUS_ACTIVE)) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%%Unable to set the XC row status active");
                bP2mpDestDeleteFlag = TE_TRUE;
                break;
            }
        }
    }
    while (TE_ZERO);            /* End - do */
    /* Clean-up in case of failure */
    if ((TE_TRUE == bOutInterfaceDeleteFlag) ||
        (TE_TRUE == bP2mpDestDeleteFlag))
    {
        if (TE_TRUE == bP2mpDestDeleteFlag)
        {
            if (nmhSetMplsTeP2mpTunnelDestRowStatus (u4TunnelIndex,
                                                     u4TunnelInstance,
                                                     u4IngressId, u4EgressId,
                                                     i4P2mpTnlSrcSubGrpOriginType,
                                                     &P2mpTnlSrcSubGrpOrigin,
                                                     u4P2mpTnlSrcSubGrpId,
                                                     i4P2mpTnlDestSubGrpOriginType,
                                                     &P2mpTnlDestSubGrpOrigin,
                                                     u4P2mpTnlDestSubGrpId,
                                                     i4P2mpTnlDestinationType,
                                                     &P2mpTnlDestination,
                                                     MPLS_STATUS_DESTROY) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n%%Failed to remove P2MP destination "
                           "entry \r\n");
                return CLI_FAILURE;
            }

        }
        if (TE_FALSE == bFoundOutSegment)
        {
            MplsDeleteMplsIfOrMplsTnlIf (u4OutIfIndex, u4MplsTnlOutIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            if (nmhSetMplsXCRowStatus
                (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                 MPLS_STATUS_DESTROY) == SNMP_FAILURE)
            {
                /*Failure Condition not handled here */
            }
            if (nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                               MPLS_STATUS_DESTROY) ==
                SNMP_FAILURE)
            {
                /*Failure Condition not handled here */
            }
        }
        if (TE_FALSE == bFoundInSegment)
        {
            MplsDeleteMplsIfOrMplsTnlIf (u4InIfIndex, u4MplsTnlInIfIndex,
                                         CFA_MPLS_TUNNEL, MPLS_TRUE);
            if (nmhSetMplsInSegmentRowStatus (&InSegmentIndex,
                                              MPLS_STATUS_DESTROY) ==
                SNMP_FAILURE)
            {
                /*Failure Condition not handled here */
            }
        }
        return CLI_FAILURE;
    }

    /* All set operations have been successful */
    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCliP2mpStaticBind : EXIT \n");
    return CLI_SUCCESS;
}

/*******************************************************************************
* Function Name : TeCliP2mpStaticBindEgress
* Description   : This routine is used to associate static label at egress LSR
* Input(s)      : CliHandle        - Cli Context Handle
                  u4InLabel        - Incoming Label associated with in-segment.
                  u4InIfIndex      - Index for the incoming MPLS interface.
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT1
TeCliP2mpStaticBindEgress (tCliHandle CliHandle, UINT4 u4InLabel,
                           UINT4 u4InIfIndex)
{
    UINT4               u4ErrorCode = TE_ZERO;
    UINT4               u4TunnelIndex = TE_ZERO;
    UINT4               u4TunnelInstance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    tTeTnlInfo         *pTeTnlInfo = NULL;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCliP2mpStaticBindEgress : ENTRY \n");

    /* Get the TunnelId, TunnelInstance, Ingress and Egress LSR ID */
    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                  u4EgressId);
    if (pTeTnlInfo == NULL)
    {
        /* If tunnel does not exist, return failure */
        MPLS_CMN_UNLOCK ();
        CliPrintf (CliHandle, "\r\n%%Tunnel does not exist\r\n");
        return CLI_FAILURE;
    }
    if (NULL == TE_P2MP_TNL_INFO (pTeTnlInfo))
    {
        MPLS_CMN_UNLOCK ();
        CliPrintf (CliHandle, "\r\n%%Not a P2MP tunnel. Cannot set "
                   "label on egress\r\n");
        return CLI_FAILURE;
    }
    MPLS_CMN_UNLOCK ();

    if (nmhTestv2MplsTunnelRole (&u4ErrorCode, u4TunnelIndex, u4TunnelInstance,
                                 u4IngressId, u4EgressId,
                                 TE_EGRESS) == SNMP_SUCCESS)
    {
        if (nmhSetMplsTunnelRole (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                  u4EgressId, TE_EGRESS) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to set Tunnel Role\r\n");
            return CLI_FAILURE;
        }
    }
    else
    {
        CliPrintf (CliHandle, "\r\n%%Unable to set Tunnel Role\r\n");
        return CLI_FAILURE;
    }

    if (TeCliStaticBindEgress (CliHandle, u4InLabel, u4InIfIndex,
                               MPLS_DIRECTION_FORWARD,
                               TE_TNL_WORKING_PATH) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Failed to associate static label at "
                   "egress LSR \r\n");
        return CLI_FAILURE;
    }

    /* All set operations have been successful */
    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCliP2mpStaticBindEgress : EXIT \n");
    return CLI_SUCCESS;
}

/*******************************************************************************
* Function Name : TeCliP2mpConfigureBudNode
* Description   : This routine is used to associate static label at egress LSR
* Input(s)      : CliHandle              - Cli Context Handle
                  u4P2mpDestId           - Identity of the P2MP destination LSR
                  u4P2mpDestLocalMapNum  - P2MP destination local map number 
                                           for global-id and node-id 
                                           combination
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT1
TeCliP2mpConfigureBudNode (tCliHandle CliHandle, UINT4 u4P2mpDestId,
                           UINT4 u4P2mpDestLocalMapNum)
{
    BOOL1               bFoundBudNode = OSIX_FALSE;
    UINT4               u4ErrorCode = TE_ZERO;
    UINT4               u4LocalMapNum = TE_ZERO;
    UINT4               u4ContextId = MPLS_DEF_CONTEXT_ID;

    INT4                i4TnlRole = TE_ZERO;
    UINT4               u4TunnelIndex = TE_ZERO;
    UINT4               u4TunnelInstance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    INT4                i4P2mpPrevBranchRole = TE_ZERO;
    tTeTnlInfo         *pTeTnlInfo = NULL;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCliP2mpConfigureBudNode : ENTRY \n");

    /* Get the TunnelId, TunnelInstance, Ingress and Egress LSR ID */
    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                  u4EgressId);
    if (pTeTnlInfo == NULL)
    {
        /* If tunnel does not exist, return failure */
        MPLS_CMN_UNLOCK ();
        CliPrintf (CliHandle, "\r\n%%Tunnel does not exist\r\n");
        return CLI_FAILURE;
    }
    if (NULL == TE_P2MP_TNL_INFO (pTeTnlInfo))
    {
        MPLS_CMN_UNLOCK ();
        CliPrintf (CliHandle, "\r\n%%Not a P2MP tunnel. Cannot set "
                   "label on egress\r\n");
        return CLI_FAILURE;
    }
    MPLS_CMN_UNLOCK ();

    /* Check if the bud node address is valid */
    if (TE_ZERO != u4P2mpDestId)
    {
        if (!
            ((IP_IS_ADDR_CLASS_A (u4P2mpDestId)
              ? (IP_IS_ZERO_NETWORK (u4P2mpDestId) ? TE_ZERO : TE_ONE) :
              TE_ZERO) || (IP_IS_ADDR_CLASS_B (u4P2mpDestId))
             || (IP_IS_ADDR_CLASS_C (u4P2mpDestId))))
        {
            CliPrintf (CliHandle, "\r\n%%Invalid P2MP bud node IP "
                       "address\r\n");
            return SNMP_FAILURE;
        }
        if (NetIpv4IfIsOurAddress (u4P2mpDestId) == NETIPV4_SUCCESS)
        {
            bFoundBudNode = OSIX_TRUE;
        }
    }
    else if (TE_ZERO != u4P2mpDestLocalMapNum)
    {
        if (TeCliGetLocalMapNum (u4ContextId, &u4LocalMapNum) == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Global Or Node Identifier is not "
                       "configured\n");
            return CLI_FAILURE;
        }
        if (u4LocalMapNum == u4P2mpDestLocalMapNum)
        {
            bFoundBudNode = OSIX_TRUE;
        }
    }
    else
    {
        CliPrintf (CliHandle, "\r\n%%Invalid input for P2MP bud node "
                   "configuration \r\n");
        return CLI_FAILURE;
    }

    if (OSIX_TRUE == bFoundBudNode)
    {
        /* Only ingress and transit LSR can act as bud LSR */
        if (nmhGetMplsTunnelRole (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                  u4EgressId, &i4TnlRole) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Failed to get tunnel role\n");
            return CLI_FAILURE;
        }
        if (i4TnlRole == TE_EGRESS)
        {
            CliPrintf (CliHandle, "\r\n%%Wrong configuration for Egress "
                       "tunnel\n");
            return CLI_FAILURE;
        }

        /* Configure LSR as bud */
        if ((nmhGetMplsTeP2mpTunnelBranchRole (u4TunnelIndex, u4TunnelInstance,
                                               u4IngressId, u4EgressId,
                                               &i4P2mpPrevBranchRole)) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Failed to get P2MP branch role \n");
            return CLI_FAILURE;
        }
        if (TE_P2MP_BUD == i4P2mpPrevBranchRole)
        {
            return CLI_SUCCESS;
        }
        if ((nmhTestv2MplsTeP2mpTunnelBranchRole (&u4ErrorCode, u4TunnelIndex,
                                                  u4TunnelInstance, u4IngressId,
                                                  u4EgressId,
                                                  TE_P2MP_BUD)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Failed to test tunnel branch role as "
                       "bud LSR \n");
            return CLI_FAILURE;
        }
        if ((nmhSetMplsTeP2mpTunnelBranchRole (u4TunnelIndex, u4TunnelInstance,
                                               u4IngressId, u4EgressId,
                                               TE_P2MP_BUD)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Failed to set tunnel branch role as "
                       "bud LSR \n");
            return CLI_FAILURE;
        }
    }
    else
    {
        CliPrintf (CliHandle, "\r%%Destination should be local IP address \n");
        return CLI_FAILURE;
    }

    /* All set operations required for Bud LSR configuration succeeded */
    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCliP2mpConfigureBudNode : EXIT \n");
    return CLI_SUCCESS;
}

/*******************************************************************************
* Function Name : TeCliP2mpDeleteDestination
* Description   : This routine is used to delete destinations in a P2MP 
*                 tunnel and labels associated with them.
* Input(s)      : CliHandle              - Cli Context Handle
                  u4P2mpDestId           - Identity of the P2MP destination LSR
                  u4P2mpDestLocalMapNum  - P2MP destination local map number 
                                           for global-id and node-id 
                                           combination
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT1
TeCliP2mpDeleteDestination (tCliHandle CliHandle, UINT4 u4P2mpDestId,
                            UINT4 u4P2mpDestLocalMapNum)
{
    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;

    tSNMP_OCTET_STRING_TYPE P2mpTnlSrcSubGrpOrigin;
    tSNMP_OCTET_STRING_TYPE P2mpTnlDestSubGrpOrigin;
    tSNMP_OCTET_STRING_TYPE P2mpTnlDestination;

    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1XCIndex[MPLS_INDEX_LENGTH];

    static UINT1        au1P2mpTnlSrcSubGrpOrigin[ROUTER_ID_LENGTH];
    static UINT1        au1P2mpTnlDestSubGrpOrigin[ROUTER_ID_LENGTH];
    static UINT1        au1P2mpTnlDestination[ROUTER_ID_LENGTH];

    UINT4               u4LocalMapNum = TE_ZERO;
    UINT4               u4ContextId = MPLS_DEF_CONTEXT_ID;

    UINT4               u4TunnelIndex = TE_ZERO;
    UINT4               u4TunnelInstance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    INT4                i4P2mpPrevBranchRole = TE_ZERO;
    INT4                i4BranchRole = TE_P2MP_NOT_BRANCH;
    BOOL1               bFoundBudNode = OSIX_FALSE;

    INT4                i4P2mpTnlSrcSubGrpOriginType = LSR_ADDR_IPV4;
    INT4                i4P2mpTnlDestSubGrpOriginType = LSR_ADDR_IPV4;
    INT4                i4P2mpTnlDestinationType = LSR_ADDR_IPV4;
    UINT4               u4P2mpTnlSrcSubGrpId = TE_ZERO;
    UINT4               u4P2mpTnlDestSubGrpId = TE_ONE;

    BOOL1               bFoundBranchEntry = TE_FALSE;
    UINT4               u4P2mpBranchCount = TE_ZERO;
    UINT4               u4P2mpDestCount = TE_ZERO;
    UINT4               u4ErrorCode = TE_ZERO;
    UINT4               u4OutSegIndex = TE_ZERO;
    UINT4               u4XCIndex = TE_ZERO;
    UINT4               u4InSegIndex = TE_ZERO;
    UINT4               u4L3Intf = TE_ZERO;
    INT4                i4OutIfIndex = TE_ZERO;
    INT4                i4InIfIndex = TE_ZERO;
    INT4                i4XCOwner = TE_ZERO;

    tTeTnlInfo         *pTeTnlInfo = NULL;
    tP2mpBranchEntry   *pTeP2mpBranchEntry = NULL;
    tXcEntry           *pXCEntry = NULL;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCliP2mpDeleteDestination : ENTRY \n");

    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;

    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;

    XCIndex.pu1_OctetList = au1XCIndex;
    XCIndex.i4_Length = MPLS_INDEX_LENGTH;

    P2mpTnlSrcSubGrpOrigin.pu1_OctetList = au1P2mpTnlSrcSubGrpOrigin;
    P2mpTnlSrcSubGrpOrigin.i4_Length = ROUTER_ID_LENGTH;

    P2mpTnlDestSubGrpOrigin.pu1_OctetList = au1P2mpTnlDestSubGrpOrigin;
    P2mpTnlDestSubGrpOrigin.i4_Length = ROUTER_ID_LENGTH;

    P2mpTnlDestination.pu1_OctetList = au1P2mpTnlDestination;
    P2mpTnlDestination.i4_Length = ROUTER_ID_LENGTH;

    MEMSET (au1InSegIndex, TE_ZERO, MPLS_INDEX_LENGTH);
    MEMSET (au1OutSegIndex, TE_ZERO, MPLS_INDEX_LENGTH);
    MEMSET (au1XCIndex, TE_ZERO, MPLS_INDEX_LENGTH);
    MEMSET (au1P2mpTnlSrcSubGrpOrigin, TE_ZERO, ROUTER_ID_LENGTH);
    MEMSET (au1P2mpTnlDestSubGrpOrigin, TE_ZERO, ROUTER_ID_LENGTH);
    MEMSET (au1P2mpTnlDestination, TE_ZERO, ROUTER_ID_LENGTH);

    /* Check if input P2MP destination is valid */
    if (TE_ZERO != u4P2mpDestId)
    {
        MPLS_INTEGER_TO_OCTETSTRING (u4P2mpDestId, (&P2mpTnlDestination));
        if (NetIpv4IfIsOurAddress (u4P2mpDestId) == NETIPV4_SUCCESS)
        {
            bFoundBudNode = OSIX_TRUE;
        }
    }
    else if (TE_ZERO != u4P2mpDestLocalMapNum)
    {
        MPLS_INTEGER_TO_OCTETSTRING (u4P2mpDestLocalMapNum,
                                     (&P2mpTnlDestination));
        if (TeCliGetLocalMapNum (u4ContextId, &u4LocalMapNum) == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Global Or Node Identifier is not "
                       "configured\n");
            return CLI_FAILURE;
        }
        if (u4LocalMapNum == u4P2mpDestLocalMapNum)
        {
            bFoundBudNode = OSIX_TRUE;
        }
    }
    else
    {
        CliPrintf (CliHandle, "\r\n%%Wrong P2MP Destination ID \r\n");
        return CLI_FAILURE;
    }

    /* Get the TunnelId, TunnelInstance, Ingress and Egress LSR ID */
    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                  u4EgressId);
    if (pTeTnlInfo == NULL)
    {
        /* If tunnel does not exist, return failure */
        MPLS_CMN_UNLOCK ();
        CliPrintf (CliHandle, "\r\n%%Tunnel does not exist\r\n");
        return CLI_FAILURE;
    }
    if (NULL == TE_P2MP_TNL_INFO (pTeTnlInfo))
    {
        MPLS_CMN_UNLOCK ();
        CliPrintf (CliHandle, "\r\n%%Not a P2MP tunnel. Cannot delete "
                   "destination and associated label\r\n");
        return CLI_FAILURE;
    }
    if (TE_EGRESS == TE_TNL_ROLE (pTeTnlInfo))
    {
        MPLS_CMN_UNLOCK ();
        CliPrintf (CliHandle, "\r\n%%Command not applicable at P2MP egress "
                   "LSR\r\n");
        return CLI_FAILURE;
    }
    u4P2mpBranchCount =
        TMO_SLL_Count (&(TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo)));
    MPLS_CMN_UNLOCK ();

    if ((nmhGetMplsTeP2mpTunnelBranchRole (u4TunnelIndex, u4TunnelInstance,
                                           u4IngressId, u4EgressId,
                                           &i4P2mpPrevBranchRole)) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Failed to get tunnel branch role \n");
        return CLI_FAILURE;
    }

    if (OSIX_TRUE == bFoundBudNode)
    {
        if (TE_P2MP_BUD != i4P2mpPrevBranchRole)
        {
            CliPrintf (CliHandle, "\r\n%%Cannot delete destination. "
                       "Branch role is not configured as bud\r\n");
            return CLI_FAILURE;
        }
        if (u4P2mpBranchCount > TE_ONE)
        {
            i4BranchRole = TE_P2MP_BRANCH;
        }

        /* Configure LSR branch role */
        if ((nmhTestv2MplsTeP2mpTunnelBranchRole (&u4ErrorCode, u4TunnelIndex,
                                                  u4TunnelInstance, u4IngressId,
                                                  u4EgressId,
                                                  i4BranchRole)) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Failed to test tunnel branch role as "
                       "bud LSR \n");
            return CLI_FAILURE;
        }
        if ((nmhSetMplsTeP2mpTunnelBranchRole (u4TunnelIndex, u4TunnelInstance,
                                               u4IngressId, u4EgressId,
                                               i4BranchRole)) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r%%Failed to set tunnel branch role as "
                       "bud LSR \n");
            return CLI_FAILURE;
        }
        /* Set operations for bud node removal are successful */
        return CLI_SUCCESS;
    }

    /* Check if destination is already present in P2MP destination table */
    if ((nmhTestv2MplsTeP2mpTunnelDestRowStatus (&u4ErrorCode, u4TunnelIndex,
                                                 u4TunnelInstance, u4IngressId,
                                                 u4EgressId,
                                                 i4P2mpTnlSrcSubGrpOriginType,
                                                 &P2mpTnlSrcSubGrpOrigin,
                                                 u4P2mpTnlSrcSubGrpId,
                                                 i4P2mpTnlDestSubGrpOriginType,
                                                 &P2mpTnlDestSubGrpOrigin,
                                                 u4P2mpTnlDestSubGrpId,
                                                 i4P2mpTnlDestinationType,
                                                 &P2mpTnlDestination,
                                                 MPLS_STATUS_DESTROY)) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Entry not present in P2MP Destination "
                   "Table \r\n");
        return CLI_FAILURE;
    }

    /* Retrieve the out-segment index from P2MP destination row */
    if ((nmhGetMplsTeP2mpTunnelDestBranchOutSegment (u4TunnelIndex,
                                                     u4TunnelInstance,
                                                     u4IngressId, u4EgressId,
                                                     i4P2mpTnlSrcSubGrpOriginType,
                                                     &P2mpTnlSrcSubGrpOrigin,
                                                     u4P2mpTnlSrcSubGrpId,
                                                     i4P2mpTnlDestSubGrpOriginType,
                                                     &P2mpTnlDestSubGrpOrigin,
                                                     u4P2mpTnlDestSubGrpId,
                                                     i4P2mpTnlDestinationType,
                                                     &P2mpTnlDestination,
                                                     &OutSegmentIndex)) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Failed to retrieve P2MP branch "
                   "out-segment \r\n");
        return CLI_FAILURE;
    }
    MPLS_OCTETSTRING_TO_INTEGER ((&OutSegmentIndex), u4OutSegIndex);

    /* Remove the destination entry */
    if ((nmhSetMplsTeP2mpTunnelDestRowStatus (u4TunnelIndex, u4TunnelInstance,
                                              u4IngressId, u4EgressId,
                                              i4P2mpTnlSrcSubGrpOriginType,
                                              &P2mpTnlSrcSubGrpOrigin,
                                              u4P2mpTnlSrcSubGrpId,
                                              i4P2mpTnlDestSubGrpOriginType,
                                              &P2mpTnlDestSubGrpOrigin,
                                              u4P2mpTnlDestSubGrpId,
                                              i4P2mpTnlDestinationType,
                                              &P2mpTnlDestination,
                                              MPLS_STATUS_DESTROY)) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Failed to remove P2MP destination "
                   "entry \r\n");
        return CLI_FAILURE;
    }

    /* After deletion, update the count of P2MP destinations */
    MPLS_CMN_LOCK ();
    u4P2mpDestCount =
        TMO_SLL_Count (&(TE_P2MP_TNL_DEST_LIST_INFO (pTeTnlInfo)));

    /* Scan the branch table to check if entry is deleted */
    TMO_SLL_Scan (&(TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo)),
                  pTeP2mpBranchEntry, tP2mpBranchEntry *)
    {
        if (u4OutSegIndex == TE_P2MP_BRANCH_OUTSEG_INDEX (pTeP2mpBranchEntry))
        {
            bFoundBranchEntry = TE_TRUE;
            break;
        }
    }
    MPLS_CMN_UNLOCK ();

    if ((TE_FALSE == bFoundBranchEntry) && (TE_ZERO != u4OutSegIndex))
    {
        /* Since no destinations are associated to this out-segment, 
         * delete the out-segment, XC and branch entry */
        if ((nmhGetMplsOutSegmentInterface (&OutSegmentIndex, &i4OutIfIndex))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Unable to fetch MPLS out-segment "
                       "interface index");
            return CLI_FAILURE;
        }
        if (TE_ZERO != i4OutIfIndex)
        {
            if ((MplsGetL3Intf ((UINT4) i4OutIfIndex, &u4L3Intf)) ==
                MPLS_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n%%Unable to Fetch L3Interface "
                           "from the mpls Interface\r\n");
                return CLI_FAILURE;
            }

            if (MplsDeleteMplsIfOrMplsTnlIf (u4L3Intf, (UINT4) i4OutIfIndex,
                                             CFA_MPLS_TUNNEL, MPLS_TRUE)
                == MPLS_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n%%Unable to delete the MPLS "
                           "tunnel interface\r\n");
                return CLI_FAILURE;
            }
        }

        /* Delete XC entry corresponding to the out-segment to be deleted */
        if ((nmhGetMplsOutSegmentXCIndex (&OutSegmentIndex, &XCIndex))
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Failed in fetching the "
                       "outsegment XC index\r\n");
            return CLI_FAILURE;
        }
        MPLS_OCTETSTRING_TO_INTEGER ((&XCIndex), u4XCIndex);
        if (TE_ZERO != u4XCIndex)
        {
            /* Retrieve XC Entry */
            pXCEntry = MplsGetXCEntryByDirection (u4XCIndex,
                                                  MPLS_DIRECTION_FORWARD);
            if ((NULL != pXCEntry) && (NULL != XC_ININDEX (pXCEntry)))
            {
                u4InSegIndex = (XC_ININDEX (pXCEntry))->u4Index;
                MPLS_INTEGER_TO_OCTETSTRING (u4InSegIndex, (&InSegmentIndex));
            }
            if (nmhGetMplsXCOwner (&XCIndex, &InSegmentIndex, &OutSegmentIndex,
                                   &i4XCOwner) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\n\r%%Indexed MPLS XC "
                           "entry unavailable\r\n");
                return CLI_FAILURE;
            }
            if (MPLS_OWNER_SNMP != i4XCOwner)
            {
                CliPrintf (CliHandle, "\r\n%%Failed to delete XC entry since "
                           "it was not created through SNMP/CLI \r\n");
                return CLI_FAILURE;
            }
            if (nmhTestv2MplsXCRowStatus (&u4ErrorCode, &XCIndex,
                                          &InSegmentIndex, &OutSegmentIndex,
                                          MPLS_STATUS_DESTROY) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_XC_ENTRY_DELETION);
                return CLI_FAILURE;
            }
            if (nmhSetMplsXCRowStatus (&XCIndex, &InSegmentIndex,
                                       &OutSegmentIndex,
                                       MPLS_STATUS_DESTROY) == SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_XC_ENTRY_DELETION);
                return CLI_FAILURE;
            }
        }

        /* Delete the out-segment entry */
        if (nmhTestv2MplsOutSegmentRowStatus (&u4ErrorCode, &OutSegmentIndex,
                                              MPLS_STATUS_DESTROY) ==
            SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_OUTSEG_ENTRY_DELETION);
            return CLI_FAILURE;
        }
        if (nmhSetMplsOutSegmentRowStatus (&OutSegmentIndex,
                                           MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_OUTSEG_ENTRY_DELETION);
            return CLI_FAILURE;
        }

        /* Delete in-segment entry when there are no P2MP destination entries */
        if ((TE_ZERO == u4P2mpDestCount) && (TE_ZERO != u4InSegIndex))
        {
            if ((nmhGetMplsInSegmentInterface (&InSegmentIndex, &i4InIfIndex))
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n%%Unable to fetch the Insegment "
                           "table entry\r\n");
                return CLI_FAILURE;
            }
            if (TE_ZERO != i4InIfIndex)
            {
                if ((MplsGetL3Intf ((UINT4) i4InIfIndex, &u4L3Intf)) ==
                    MPLS_FAILURE)
                {
                    CliPrintf (CliHandle, "\r\n%%Unable to fetch the L3 "
                               "interface index\r\n");
                    return CLI_FAILURE;
                }
                if (MplsDeleteMplsIfOrMplsTnlIf (u4L3Intf, (UINT4) i4InIfIndex,
                                                 CFA_MPLS_TUNNEL, MPLS_TRUE) ==
                    MPLS_FAILURE)
                {
                    CliPrintf (CliHandle, "\r\n%%Unable to delete the MPLS "
                               "tunnel interface\r\n");
                    return CLI_FAILURE;
                }
            }
            if (nmhTestv2MplsInSegmentRowStatus (&u4ErrorCode, &InSegmentIndex,
                                                 MPLS_STATUS_DESTROY) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_INSEG_ENTRY_DELETION);
                return CLI_FAILURE;
            }
            if (nmhSetMplsInSegmentRowStatus (&InSegmentIndex,
                                              MPLS_STATUS_DESTROY) ==
                SNMP_FAILURE)
            {
                CLI_SET_ERR (MPLS_CLI_INSEG_ENTRY_DELETION);
                return CLI_FAILURE;
            }
        }
    }                            /* End - if ((TE_ZERO == TE_P2MP_BRANCH_DEST_COUNT(...)) && (...)) */

    /* All set operations have been successful */
    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCliP2mpDeleteDestination : EXIT \n");
    return CLI_SUCCESS;
}

/*******************************************************************************
* Function Name : TeCliShowP2mpDestinations 
* Description   : This routine is used to display list of destinations
*                 associated to P2MP tunnel
* Input(s)      : CliHandle     - Cli Context Handle
                  pTeTnlInfo    - Pointer of the tunnel info
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT1
TeCliShowP2mpDestinations (tCliHandle CliHandle, tTeTnlInfo * pTeTnlInfo)
{
    UINT4               u4DestId = TE_ZERO;
    UINT4               u4OutIndex = TE_ZERO;
    UINT4               u4InIntf = TE_ZERO;
    UINT4               u4L3Intf = TE_ZERO;

    INT4                i4RetValGetIfIndex = MPLS_FAILURE;
    INT4                i4RetValGetIfInfo = CFA_FAILURE;

    tP2mpBranchEntry   *pTeP2mpFirstBranchEntry = NULL;
    tP2mpDestEntry     *pTeP2mpDestEntry = NULL;
    CHR1               *pP2mpDestIp = NULL;

    tXcEntry           *pXcEntry = NULL;
    tOutSegment        *pOutSegment = NULL;
    tCfaIfInfo          IfInfo;

    MEMSET (&IfInfo, TE_ZERO, sizeof (tCfaIfInfo));

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCliShowP2mpDestinations : ENTRY \n");

    if (NULL == TE_P2MP_TNL_INFO (pTeTnlInfo))
    {
        TE_DBG (TE_MAIN_FAIL, "P2MP Tnl info not present \n");
        return CLI_FAILURE;
    }
    if (TMO_SLL_Count (&(TE_P2MP_TNL_DEST_LIST_INFO (pTeTnlInfo))) == TE_ZERO)
    {
        TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCliShowP2mpDestinations : EXIT \n");
        return CLI_SUCCESS;
    }

    /* Display incoming label information */
    pTeP2mpFirstBranchEntry =
        (tP2mpBranchEntry *)
        TMO_SLL_First (&(TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo)));
    if (NULL != pTeP2mpFirstBranchEntry)
    {
        u4OutIndex = TE_P2MP_BRANCH_OUTSEG_INDEX (pTeP2mpFirstBranchEntry);
        if (TE_ZERO != u4OutIndex)
        {
            pOutSegment = MplsGetOutSegmentTableEntry (u4OutIndex);
        }
        if (NULL != pOutSegment)
        {
            pXcEntry = (tXcEntry *) OUTSEGMENT_XC_INDEX (pOutSegment);
            if ((NULL != pXcEntry) && (NULL != XC_ININDEX (pXcEntry)))
            {
                u4InIntf = INSEGMENT_IF_INDEX (XC_ININDEX (pXcEntry));
            }
            if (TE_SIGPROTO_NONE == TE_TNL_SIGPROTO (pTeTnlInfo))
            {
                i4RetValGetIfIndex = MplsGetL3Intf (u4InIntf, &u4L3Intf);
            }
            i4RetValGetIfInfo = CfaGetIfInfo (u4L3Intf, &IfInfo);
        }
    }
    if ((MPLS_SUCCESS != i4RetValGetIfIndex) ||
        (CFA_SUCCESS != i4RetValGetIfInfo))
    {
        CliPrintf (CliHandle, "\r   InLabel  : - \n");
    }
    else if ((pXcEntry != NULL) && (XC_ININDEX (pXcEntry) != NULL))
    {
        CliPrintf (CliHandle, "\r   InLabel  : %s, %d\n", IfInfo.au1IfName,
                   INSEGMENT_LABEL (XC_ININDEX (pXcEntry)));
    }

    CliPrintf (CliHandle, "\r    P2MP destination        OutLabel\n");
    /* Check if destination is already present in P2MP destination table */
    TMO_SLL_Scan (&(TE_P2MP_TNL_DEST_LIST_INFO (pTeTnlInfo)),
                  pTeP2mpDestEntry, tP2mpDestEntry *)
    {
        MEMCPY ((UINT1 *) &u4DestId, &(TE_P2MP_DEST_ADDRESS (pTeP2mpDestEntry)),
                sizeof (UINT4));
        CLI_CONVERT_IPADDR_TO_STR (pP2mpDestIp, u4DestId);

        u4OutIndex = TE_P2MP_DEST_BRANCH_OUTSEG_INDEX (pTeP2mpDestEntry);
        if (TE_ZERO != u4OutIndex)
        {
            pOutSegment = MplsGetOutSegmentTableEntry (u4OutIndex);
        }
        if ((TE_ZERO == u4OutIndex) || (NULL == pOutSegment))
        {
            if (pTeTnlInfo->u4TnlLsrIdMapInfo & TE_TNL_INGRESSID_MAP_INFO)
            {
                CliPrintf (CliHandle, "\r    %d                       - \n",
                           u4DestId);
            }
            else
            {
                CliPrintf (CliHandle, "\r    %s               - \n",
                           pP2mpDestIp);
            }
            continue;
        }

        i4RetValGetIfIndex = MplsGetL3Intf
            (OUTSEGMENT_IF_INDEX (pOutSegment), &u4L3Intf);
        i4RetValGetIfInfo = CfaGetIfInfo (u4L3Intf, &IfInfo);
        if ((MPLS_SUCCESS != i4RetValGetIfIndex) ||
            (CFA_SUCCESS != i4RetValGetIfInfo))
        {
            if (pTeTnlInfo->u4TnlLsrIdMapInfo & TE_TNL_INGRESSID_MAP_INFO)
            {
                CliPrintf (CliHandle, "\r    %d                       - \n",
                           u4DestId);
            }
            else
            {
                CliPrintf (CliHandle, "\r    %s               - \n",
                           pP2mpDestIp);
            }
            continue;
        }
        if (pTeTnlInfo->u4TnlLsrIdMapInfo & TE_TNL_INGRESSID_MAP_INFO)
        {
            CliPrintf (CliHandle, "\r    %d                       %s, %d\n",
                       u4DestId, IfInfo.au1IfName,
                       OUTSEGMENT_LABEL (pOutSegment));
        }
        else
        {
            CliPrintf (CliHandle, "\r    %s               %s, %d\n",
                       pP2mpDestIp, IfInfo.au1IfName,
                       OUTSEGMENT_LABEL (pOutSegment));
        }
    }
    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCliShowP2mpDestinations : EXIT \n");
    return CLI_SUCCESS;
}

/*******************************************************************************
* Function Name : TeCliShowP2mpBranchStatistics 
* Description   : This routine is used to display P2MP traffic statistics
*                 at branch point 
* Input(s)      : CliHandle        - Cli Context Handle
                  u4TunnelIndex    - Uniquely identifies a set of tunnel
                                     instances. 
                  u4TunnelInstance - Uniquely identifies a particular
                                     instance of a tunnel.
                  u4P2mpId             - P2MP identifier
                  u4IngressId          - Identity of the ingress LSR associated
                                         with the tunnel instance.
                  u4IngressLocalMapNum - Ingress local map number for global-id
                                         and node-id combination
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT1
TeCliShowP2mpBranchStatistics (tCliHandle CliHandle, UINT4 u4TunnelIndex,
                               UINT4 u4TunnelInstance, UINT4 u4P2mpId,
                               UINT4 u4IngressId, UINT4 u4IngressLocalMapNum)
{
    UINT1               au1RetValP2mpBranchPerfHCPackets[MAX_U8_STRING_SIZE];
    UINT1               au1RetValP2mpBranchPerfHCBytes[MAX_U8_STRING_SIZE];

    UINT4               u4EgressId = u4P2mpId;
    UINT4               u4OutIndex = TE_ZERO;
    UINT4               u4L3Intf = TE_ZERO;

    UINT4               u4TnlId = TE_ZERO;
    UINT4               u4TnlInstance = TE_ZERO;
    UINT4               u4TnlIngressLsrId = TE_ZERO;
    UINT4               u4TnlEgressLsrId = TE_ZERO;
    BOOL1               bFoundP2mpTnlEntry = FALSE;

    tTeTnlInfo         *pTeTnlInfo = NULL;
    tP2mpBranchEntry   *pP2mpBranchEntry = NULL;
    tOutSegment        *pOutSegment = NULL;
    tCfaIfInfo          IfInfo;

    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCliShowP2mpBranchStatistics : ENTRY \n");

    /* Fetch the indices required to access tunnel table */
    if ((nmhGetFirstIndexMplsTunnelTable (&u4TnlId, &u4TnlInstance,
                                          &u4TnlIngressLsrId,
                                          &u4TnlEgressLsrId)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Failed to retrieve first row in tunnel "
                   "table\r\n");
        return CLI_FAILURE;
    }
    do
    {
        if (u4TunnelIndex == u4TnlId)
        {
            if (TE_ZERO == u4EgressId)
            {
                bFoundP2mpTnlEntry = TRUE;
                break;
            }
            if ((u4EgressId == u4TnlEgressLsrId) &&
                (u4TunnelInstance == u4TnlInstance) &&
                ((u4IngressId == u4TnlIngressLsrId) ||
                 (u4IngressLocalMapNum == u4TnlIngressLsrId)))
            {
                bFoundP2mpTnlEntry = TRUE;
                break;
            }
        }
    }
    while ((nmhGetNextIndexMplsTunnelTable (u4TnlId, &u4TnlId, u4TnlInstance,
                                            &u4TnlInstance, u4TnlIngressLsrId,
                                            &u4TnlIngressLsrId,
                                            u4TnlEgressLsrId,
                                            &u4TnlEgressLsrId)) ==
           SNMP_SUCCESS);
    if (FALSE == bFoundP2mpTnlEntry)
    {
        CliPrintf (CliHandle, "\r\n%%P2MP tunnel entry not found\r\n");
        return CLI_FAILURE;
    }

    /* When tunnel index is the only input, update remaining tunnel indices */
    if (TE_ZERO == u4EgressId)
    {
        u4TunnelInstance = u4TnlInstance;
        u4IngressId = u4TnlIngressLsrId;
        u4EgressId = u4TnlEgressLsrId;
    }

    /* Check in Tunnel RBTree. If entry is present, return failure */
    CliRegisterLock (CliHandle, CmnLock, CmnUnLock);
    MPLS_CMN_LOCK ();
    pTeTnlInfo = TeGetTunnelInfo (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                  u4EgressId);
    if (pTeTnlInfo == NULL)
    {
        CliPrintf (CliHandle, "\r\n%%Tunnel does not exist \r\n");
        MPLS_CMN_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }

    if (NULL == TE_P2MP_TNL_INFO (pTeTnlInfo))
    {
        CliPrintf (CliHandle, "\r\n%%Not a P2MP tunnel. Cannot retrieve "
                   "traffic statistics\r\n");
        MPLS_CMN_UNLOCK ();
        CliUnRegisterLock (CliHandle);
        return CLI_FAILURE;
    }
    CliPrintf (CliHandle, "\r\n Name: %s                        "
               "     P2MP ID: %u\r\n", TE_TNL_NAME (pTeTnlInfo), u4EgressId);
    CliPrintf (CliHandle, " MPLS Traffic statistics at P2MP branch LSR\r\n");

    TMO_SLL_Scan (&(TE_P2MP_TNL_BRANCH_LIST_INFO (pTeTnlInfo)),
                  pP2mpBranchEntry, tP2mpBranchEntry *)
    {
        u4OutIndex = TE_P2MP_BRANCH_OUTSEG_INDEX (pP2mpBranchEntry);
        if (TE_ZERO != u4OutIndex)
        {
            pOutSegment = MplsGetOutSegmentTableEntry (u4OutIndex);
            if (NULL == pOutSegment)
            {
                CliPrintf (CliHandle, "   OutLabel : - \r\n");
                continue;
            }

            if (MplsGetL3Intf
                (OUTSEGMENT_IF_INDEX (pOutSegment), &u4L3Intf) == MPLS_SUCCESS)
            {
                if (CfaGetIfInfo (u4L3Intf, &IfInfo) == CFA_SUCCESS)
                {
                    CliPrintf (CliHandle, "   OutLabel : %s, %d\r\n",
                               IfInfo.au1IfName,
                               OUTSEGMENT_LABEL (pOutSegment));
                }
            }

            MPLS_CMN_UNLOCK ();
            if ((MplsGetOutSegmentPerfStats (u4OutIndex)) == MPLS_FAILURE)
            {
                MPLS_CMN_LOCK ();
                CliPrintf (CliHandle, "    Branch performance statistics "
                           "unavailable - \r\n");
                continue;
            }
            MPLS_CMN_LOCK ();

            FSAP_U8_2STR (&(OUTSEGMENT_PERF_HC_PKTS (pOutSegment)),
                          (CHR1 *) au1RetValP2mpBranchPerfHCPackets);
            CliPrintf (CliHandle, "   Packets forwarded: %s\r\n",
                       au1RetValP2mpBranchPerfHCPackets);
            CliPrintf (CliHandle, "   Packets dropped: %u\r\n",
                       OUTSEGMENT_PERF_ERRORS (pOutSegment));
            FSAP_U8_2STR (&(OUTSEGMENT_PERF_HC_OCTETS (pOutSegment)),
                          (CHR1 *) au1RetValP2mpBranchPerfHCBytes);
            CliPrintf (CliHandle, "   Bytes forwarded: %s\r\n\n",
                       au1RetValP2mpBranchPerfHCBytes);
        }
    }
    MPLS_CMN_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    TE_DBG (TE_MAIN_ETEXT, "MAIN : TeCliShowP2mpBranchStatistics : EXIT \n");
    return CLI_SUCCESS;
}

/* MPLS_P2MP_LSP_CHANGES - E */

/*******************************************************************************
 * Function Name : TeCliMplslsSetTnlProperties
 * Description   : This routine is used to set Properties of GMPLS tunnel.
 * Input(s)      : CliHandle  - Cli Context Handle
 *                 u4Command  - Command Requested
 *                 pTeCliArgs - CLI Args
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ********************************************************************************/
INT4
TeCliMplsSetTnlProperties (tCliHandle CliHandle, UINT4 u4Command,
                           tTeCliArgs * pTeCliArgs)
{
    UINT4               u4TnlId = TE_ZERO;
    UINT4               u4TnlInstance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    UINT4               u4ErrorCode = TE_ZERO;
    INT4                i4TunnelRowStatus = TE_ZERO;
    INT4                i4DiffRowStatus = TE_ZERO;
    INT4                i4MbbStatus = TE_ZERO;
    INT4                i4TunnelAdminStatus = TE_ZERO;

    static UINT1        au1RetValAdminStatus[4];
    static UINT1        u1EndToEndProtn;

    tSNMP_OCTET_STRING_TYPE RetValAdminStatusStr;
    tSNMP_OCTET_STRING_TYPE TeEndToEndProtn;

    RetValAdminStatusStr.pu1_OctetList = au1RetValAdminStatus;

    u1EndToEndProtn = (UINT1) pTeCliArgs->i4EndToEndProtection;

    TeEndToEndProtn.pu1_OctetList = &u1EndToEndProtn;
    TeEndToEndProtn.i4_Length = sizeof (UINT1);

    if (TeCliGetTnlIndices (CliHandle, &u4TnlId, &u4TnlInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhGetMplsTunnelRowStatus (u4TnlId, u4TnlInstance, u4IngressId,
                                   u4EgressId,
                                   &i4TunnelRowStatus) == SNMP_FAILURE)
    {
        TE_DBG (TE_EXTN_FAIL, "\r\n%%Failed to get tunnel information\r\n");
    }

    nmhGetFsMplsTunnelMBBStatus (u4TnlId, u4TnlInstance,
                                 u4IngressId, u4EgressId, &i4MbbStatus);

    nmhGetMplsTunnelAdminStatus (u4TnlId, u4TnlInstance, u4IngressId,
                                 u4EgressId, &i4TunnelAdminStatus);

    if ((i4TunnelAdminStatus == TE_ADMIN_UP) &&
        (i4MbbStatus == MPLS_TE_MBB_DISABLED) &&
        (u4Command != CLI_MPLS_TE_ADMIN_STATUS_FLAGS))
    {

        CliPrintf (CliHandle,
                   "\r\n%%Cannot set Tunnel attributes,tunnel is active\r\n");
        return CLI_FAILURE;
    }

    if ((i4TunnelRowStatus == TE_ACTIVE) &&
        (u4Command != CLI_MPLS_TE_ADMIN_STATUS_FLAGS) &&
        (u4Command != CLI_MPLS_TE_END_TO_END_PROTECTION))
    {
        TeCliSetMplsTunnelRowStatus (u4TnlId, u4TnlInstance, u4IngressId,
                                     u4EgressId, TE_NOTINSERVICE);
    }

    switch (u4Command)
    {
        case CLI_MPLS_TE_GPID:
        {
            if (nmhTestv2GmplsTunnelGPid (&u4ErrorCode, u4TnlId,
                                          u4TnlInstance, u4IngressId,
                                          u4EgressId, pTeCliArgs->i4Gpid)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%%Cannot set Tunnel GPID attribute\r\n");
                return CLI_FAILURE;
            }

            if (nmhSetGmplsTunnelGPid (u4TnlId, u4TnlInstance, u4IngressId,
                                       u4EgressId, pTeCliArgs->i4Gpid)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%%Cannot set Tunnel GPID attribute\r\n");
                return CLI_FAILURE;
            }

            break;
        }

        case CLI_MPLS_TE_ADMIN_STATUS_FLAGS:
        {
            MPLS_INTEGER_TO_OCTETSTRING (pTeCliArgs->u4AdminStatus,
                                         (&RetValAdminStatusStr));

            if (nmhTestv2GmplsTunnelAdminStatusFlags (&u4ErrorCode, u4TnlId,
                                                      u4TnlInstance,
                                                      u4IngressId, u4EgressId,
                                                      &RetValAdminStatusStr) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r\n%%Unable to set Admin Status flag \r\n");
                return CLI_FAILURE;
            }

            if (nmhSetGmplsTunnelAdminStatusFlags (u4TnlId, u4TnlInstance,
                                                   u4IngressId, u4EgressId,
                                                   &RetValAdminStatusStr)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r\n%%no  value is supported \r\n");
                return CLI_FAILURE;
            }
            break;
        }

        case CLI_MPLS_TE_SWITCHING_ENCODING_TYPE:
        {
            if (nmhTestv2GmplsTunnelLSPEncoding (&u4ErrorCode, u4TnlId,
                                                 u4TnlInstance, u4IngressId,
                                                 u4EgressId,
                                                 pTeCliArgs->u1EncodingType)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to Set Encoding Type\r\n");
                return CLI_FAILURE;
            }

            if (nmhTestv2GmplsTunnelSwitchingType (&u4ErrorCode, u4TnlId,
                                                   u4TnlInstance, u4IngressId,
                                                   u4EgressId,
                                                   pTeCliArgs->u1SwitchingType)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to Set Switching Type\n");
                return CLI_FAILURE;
            }

            if (nmhSetGmplsTunnelLSPEncoding
                (u4TnlId, u4TnlInstance, u4IngressId, u4EgressId,
                 pTeCliArgs->u1EncodingType) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to Set Encoding Type\r\n");
                return CLI_FAILURE;
            }

            if (nmhSetGmplsTunnelSwitchingType
                (u4TnlId, u4TnlInstance, u4IngressId, u4EgressId,
                 pTeCliArgs->u1SwitchingType) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to Set Switching Type\n");
                return CLI_FAILURE;
            }

            break;
        }

        case CLI_MPLS_TE_TNL_SRLG_TYPE:
        {
            if (nmhTestv2FsMplsTunnelSrlgType (&u4ErrorCode, u4TnlId,
                                               u4TnlInstance, u4IngressId,
                                               u4EgressId,
                                               pTeCliArgs->i4SrlgType)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to Set Tunnel Srlg Type\n");
                return CLI_FAILURE;
            }

            if (nmhSetFsMplsTunnelSrlgType (u4TnlId, u4TnlInstance, u4IngressId,
                                            u4EgressId, pTeCliArgs->i4SrlgType)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to Set Tunnel Srlg Type\n");
                return CLI_FAILURE;
            }

            break;
        }

        case CLI_MPLS_TE_TNL_NO_SRLG_TYPE:
        {
            if (nmhSetFsMplsTunnelSrlgType (u4TnlId, u4TnlInstance, u4IngressId,
                                            u4EgressId, pTeCliArgs->i4SrlgType)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to Set Tunnel Srlg Type\n");
                return CLI_FAILURE;
            }

            break;
        }

        case CLI_MPLS_TE_TNL_SRLG_VALUE:
        {
            if (nmhTestv2FsMplsTunnelSrlgRowStatus (&u4ErrorCode, u4TnlId,
                                                    u4TnlInstance, u4IngressId,
                                                    u4EgressId,
                                                    pTeCliArgs->u4SrlgNo,
                                                    TE_CREATEANDWAIT)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to create Tunnel SRLG\n");
                return CLI_FAILURE;
            }

            if (nmhSetFsMplsTunnelSrlgRowStatus (u4TnlId, u4TnlInstance,
                                                 u4IngressId, u4EgressId,
                                                 pTeCliArgs->u4SrlgNo,
                                                 TE_CREATEANDWAIT)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to create Tunnel SRLG\n");
                return CLI_FAILURE;
            }
            TeCliSetMplsTunnelOldBandwidth (u4TnlId, u4TnlInstance, u4IngressId,
                                            u4EgressId);
            if (nmhSetFsMplsTunnelSrlgRowStatus
                (u4TnlId, u4TnlInstance, u4IngressId, u4EgressId,
                 pTeCliArgs->u4SrlgNo, TE_ACTIVE) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to create Tunnel SRLG\n");
                return CLI_FAILURE;
            }

            break;
        }
        case CLI_MPLS_TE_TNL_NO_SRLG_VALUE:
        {
            if (nmhTestv2FsMplsTunnelSrlgRowStatus (&u4ErrorCode, u4TnlId,
                                                    u4TnlInstance, u4IngressId,
                                                    u4EgressId,
                                                    pTeCliArgs->u4SrlgNo,
                                                    TE_DESTROY) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to delete Tunnel SRLG\n");
                return CLI_FAILURE;
            }

            if (nmhSetFsMplsTunnelSrlgRowStatus (u4TnlId, u4TnlInstance,
                                                 u4IngressId, u4EgressId,
                                                 pTeCliArgs->u4SrlgNo,
                                                 TE_DESTROY) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to delete Tunnel SRLG\n");
                return CLI_FAILURE;
            }

            break;
        }
        case CLI_MPLS_TUNNEL_CLASS_TYPE:
        {
            if (nmhTestv2FsMplsDiffServRowStatus (&u4ErrorCode, u4TnlId,
                                                  u4TnlInstance,
                                                  u4IngressId, u4EgressId,
                                                  TE_NOTINSERVICE)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to Set Tunnel Class type\n");
                return CLI_FAILURE;
            }

            if (nmhSetFsMplsDiffServRowStatus (u4TnlId,
                                               u4TnlInstance,
                                               u4IngressId, u4EgressId,
                                               TE_NOTINSERVICE) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to Set Tunnel Class type\n");
                return CLI_FAILURE;
            }

            if (nmhTestv2FsMplsDiffServClassType (&u4ErrorCode, u4TnlId,
                                                  u4TnlInstance,
                                                  u4IngressId, u4EgressId,
                                                  pTeCliArgs->i4ClassType)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to Set Tunnel Class type\n");
                return CLI_FAILURE;
            }

            if (nmhSetFsMplsDiffServClassType (u4TnlId, u4TnlInstance,
                                               u4IngressId, u4EgressId,
                                               pTeCliArgs->i4ClassType)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to Set Tunnel Class type\n");
                return CLI_FAILURE;
            }

            if (nmhTestv2FsMplsDiffServRowStatus (&u4ErrorCode, u4TnlId,
                                                  u4TnlInstance, u4IngressId,
                                                  u4EgressId,
                                                  TE_ACTIVE) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Unable to make Diffserv entry Active\n");
                return CLI_FAILURE;

            }
            if (nmhSetFsMplsDiffServRowStatus (u4TnlId,
                                               u4TnlInstance,
                                               u4IngressId, u4EgressId,
                                               TE_ACTIVE) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Unable to make Diffserv entry Active\n");
                return CLI_FAILURE;
            }

            break;
        }
        case CLI_MPLS_TE_END_TO_END_PROTECTION:
        {
            if (nmhTestv2FsMplsTunnelEndToEndProtection (&u4ErrorCode, u4TnlId,
                                                         u4TnlInstance,
                                                         u4IngressId,
                                                         u4EgressId,
                                                         &TeEndToEndProtn) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to Set Tunnel End to End "
                           "Protection type\n");
                return CLI_FAILURE;
            }

            if (nmhSetFsMplsTunnelEndToEndProtection (u4TnlId, u4TnlInstance,
                                                      u4IngressId, u4EgressId,
                                                      &TeEndToEndProtn)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle, "\r%% Unable to Set Tunnel End to End "
                           "Protection type\n");
                return CLI_FAILURE;
            }

            break;
        }
        case CLI_MPLS_TE_PROTECTION_OPER_TYPE:
        {
            if (nmhTestv2FsMplsTunnelPrConfigOperType (&u4ErrorCode, u4TnlId,
                                                       u4TnlInstance,
                                                       u4IngressId, u4EgressId,
                                                       pTeCliArgs->
                                                       i4ProtnOperType) ==
                SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Unable to Set Protection Operation type\n");
                return CLI_FAILURE;
            }

            if (nmhSetFsMplsTunnelPrConfigOperType (u4TnlId, u4TnlInstance,
                                                    u4IngressId, u4EgressId,
                                                    pTeCliArgs->i4ProtnOperType)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Unable to Set Protection Operation type\n");
                return CLI_FAILURE;
            }

            break;
        }
        case CLI_MPLS_TE_MBB_STATUS:
        {
            if (nmhTestv2FsMplsTunnelMBBStatus (&u4ErrorCode, u4TnlId,
                                                u4TnlInstance,
                                                u4IngressId, u4EgressId,
                                                pTeCliArgs->i4MbbStatus)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Unable to Set Make before break Status\n");
                return CLI_FAILURE;
            }
            if (nmhSetFsMplsTunnelMBBStatus
                (u4TnlId, u4TnlInstance, u4IngressId, u4EgressId,
                 pTeCliArgs->i4MbbStatus) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Unable to Set Make before break Status\n");
                return CLI_FAILURE;
            }

            break;
        }
        case CLI_MPLS_DIFFSERV_LSP_TYPE:
        {
            nmhGetFsMplsDiffServRowStatus (u4TnlId, u4TnlInstance, u4IngressId,
                                           u4EgressId, &i4DiffRowStatus);

            if ((i4DiffRowStatus != TE_ZERO) &&
                (pTeCliArgs->i4LspType == MPLS_TE_GEN_LSP))
            {
                if (nmhTestv2FsMplsDiffServRowStatus (&u4ErrorCode, u4TnlId,
                                                      u4TnlInstance,
                                                      u4IngressId, u4EgressId,
                                                      TE_DESTROY) ==
                    SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%% Unable to destroy Diffserv Entry\n");
                    return CLI_FAILURE;
                }

                if (nmhSetFsMplsDiffServRowStatus (u4TnlId, u4TnlInstance,
                                                   u4IngressId, u4EgressId,
                                                   TE_DESTROY) == SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%% Unable to destroy Diffserv Entry\n");
                    return CLI_FAILURE;
                }
                break;
            }

            if (i4DiffRowStatus == TE_ACTIVE)
            {
                if (nmhTestv2FsMplsDiffServRowStatus (&u4ErrorCode, u4TnlId,
                                                      u4TnlInstance,
                                                      u4IngressId, u4EgressId,
                                                      TE_NOTINSERVICE) ==
                    SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%% Unable to make Diffserv Entry as NOT IN SERVICE\n");
                    return CLI_FAILURE;
                }

                if (nmhSetFsMplsDiffServRowStatus (u4TnlId, u4TnlInstance,
                                                   u4IngressId, u4EgressId,
                                                   TE_NOTINSERVICE) ==
                    SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%% Unable to make Diffserv Entry as NOT IN SERVICE\n");
                    return CLI_FAILURE;
                }
            }
            else
            {

                if (nmhTestv2FsMplsDiffServRowStatus (&u4ErrorCode, u4TnlId,
                                                      u4TnlInstance,
                                                      u4IngressId, u4EgressId,
                                                      TE_CREATEANDWAIT) ==
                    SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%% Unable to create Diffserv Entry\n");
                    return CLI_FAILURE;
                }

                if (nmhSetFsMplsDiffServRowStatus (u4TnlId, u4TnlInstance,
                                                   u4IngressId, u4EgressId,
                                                   TE_CREATEANDWAIT) ==
                    SNMP_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%% Unable to create Diffserv Entry\n");
                    return CLI_FAILURE;
                }

            }
            if (nmhTestv2FsMplsDiffServServiceType (&u4ErrorCode, u4TnlId,
                                                    u4TnlInstance,
                                                    u4IngressId, u4EgressId,
                                                    pTeCliArgs->i4LspType)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Unable to Set LSP type of tunnel\n");
                return CLI_FAILURE;
            }

            if (nmhSetFsMplsDiffServServiceType (u4TnlId, u4TnlInstance,
                                                 u4IngressId, u4EgressId,
                                                 pTeCliArgs->i4LspType)
                == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Unable to Set LSP type of tunnel\n");
                return CLI_FAILURE;
            }

            if (pTeCliArgs->i4LspType == MPLS_DIFFSERV_ELSP)
            {
                if (CliChangePath (MPLS_TUNNEL_ELSP_MODE) == CLI_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%%Unable to change to LSP Attribute Mode\r\n");
                    return CLI_FAILURE;
                }
            }
            else if (pTeCliArgs->i4LspType == MPLS_DIFFSERV_LLSP)
            {
                if (CliChangePath (MPLS_TUNNEL_LLSP_MODE) == CLI_FAILURE)
                {
                    CliPrintf (CliHandle,
                               "\r%%Unable to change to LSP Attribute Mode\r\n");
                    return CLI_FAILURE;
                }
            }

            if (nmhTestv2FsMplsDiffServRowStatus (&u4ErrorCode, u4TnlId,
                                                  u4TnlInstance,
                                                  u4IngressId, u4EgressId,
                                                  TE_ACTIVE) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Unable to make Diffserv Entry as Active\n");
                return CLI_FAILURE;
            }

            if (nmhSetFsMplsDiffServRowStatus (u4TnlId, u4TnlInstance,
                                               u4IngressId, u4EgressId,
                                               TE_ACTIVE) == SNMP_FAILURE)
            {
                CliPrintf (CliHandle,
                           "\r%% Unable to make Diffserv Entry as Active\n");
                return CLI_FAILURE;
            }

            break;
        }

        default:
        {
            break;
        }
    }

    TeCliSetMplsTunnelOldBandwidth (u4TnlId, u4TnlInstance, u4IngressId,
                                    u4EgressId);
    if ((i4TunnelRowStatus != TE_NOTREADY) &&
        (u4Command != CLI_MPLS_TE_ADMIN_STATUS_FLAGS) &&
        (u4Command != CLI_MPLS_TE_END_TO_END_PROTECTION))
    {
        TeCliSetMplsTunnelRowStatus (u4TnlId, u4TnlInstance, u4IngressId,
                                     u4EgressId, TE_ACTIVE);
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name : TeCliMplsQosPolicy
 * Description   : This routine is used to set the Mpls Qos Policy
 * Input(s)      : CliHandle        - Cli Context Handle
 *                 pTeCliArgs       - Pointer to Cli Args
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 * *******************************************************************************/
INT4
TeCliMplsQosPolicy (tCliHandle CliHandle, tTeCliArgs * pTeCliArgs)
{
    UINT4               u4ErrorCode = TE_ZERO;
    INT4                i4AdminStatus = TE_ZERO;

    nmhGetFsMplsAdminStatus (&i4AdminStatus);

    if (nmhSetFsMplsAdminStatus (MPLS_ADMIN_STATUS_DOWN) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to Set MplsAdminStatus as Down\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMplsQosPolicy (&u4ErrorCode,
                                  pTeCliArgs->i4QosPolicy) == SNMP_FAILURE)
    {
        if (nmhSetFsMplsAdminStatus (i4AdminStatus) == SNMP_FAILURE)
        {
            /*Failure condition not handled here */
        }
        CliPrintf (CliHandle, "\r%% Unable to Set Mpls Qos Policy\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsQosPolicy (pTeCliArgs->i4QosPolicy) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to Set Mpls Qos Policy\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsAdminStatus (i4AdminStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to Set MplsAdminStatus\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name : TeCliTunnelAttrListCreate 
 * Description   : This routine is used to create the LSP Attribute Mode 
 * Input(s)      : CliHandle        - Cli Context Handle
 *                 u4Command        - Specifies the command given as the input
 *                 pu1Arg           - can be EgressLsrId or TunnelName
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 * *******************************************************************************/
INT4
TeCliTunnelAttrListCreate (tCliHandle CliHandle, UINT1 *pu1Arg)
{
    tTeAttrListInfo     TeAttrListInfo;
    UINT4               u4AttrListIndex = 0;
    UINT4               u4ErrorCode = 0;
    INT4                i4RetVal = TE_FAILURE;
    tSNMP_OCTET_STRING_TYPE AttrListName;

    MEMSET (&TeAttrListInfo, 0, sizeof (tTeAttrListInfo));

    TeAttrListInfo.i4ListNameLen = (INT4) STRLEN (pu1Arg);
    MEMCPY (TeAttrListInfo.au1ListName, pu1Arg, TeAttrListInfo.i4ListNameLen);

    i4RetVal = TeSigGetAttrListIndexFromName (&TeAttrListInfo);

    if (i4RetVal == TE_SUCCESS)
    {
        /* set  cli context  */
        gaTeCliArgs[CliHandle].u4AttrListIndex = TeAttrListInfo.u4ListIndex;
    }
    else
    {
        /* if corresponding attribute entry not exist creating the new entry */
        if (nmhGetFsTunnelAttributeIndexNext (&u4AttrListIndex) != SNMP_SUCCESS)
        {
            CliPrintf (CliHandle, "\r\n%%ERROR  TO GET NEXT INDEX \r\n");
            return CLI_FAILURE;
        }

        AttrListName.pu1_OctetList = pu1Arg;
        AttrListName.i4_Length = TeAttrListInfo.i4ListNameLen;

        if (TeCliAttrParamRowStatus (u4AttrListIndex, TE_CREATEANDWAIT)
            == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%% Unable to create Attribute List\n");
            return CLI_FAILURE;
        }

        if (nmhTestv2FsTunnelAttributeName (&u4ErrorCode, u4AttrListIndex,
                                            &AttrListName) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%% Invalid Attribute Name\n");
            return CLI_FAILURE;
        }

        if (nmhSetFsTunnelAttributeName (u4AttrListIndex, &AttrListName)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%% Invalid Attribute Name\n");
            return CLI_FAILURE;
        }

        if (TeCliAttrParamRowStatus (u4AttrListIndex, TE_ACTIVE) == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%% Unable to activate Attribute List\n");
            return CLI_FAILURE;
        }

        /* set  cli context  */
        gaTeCliArgs[CliHandle].u4AttrListIndex = u4AttrListIndex;
    }

    i4RetVal = TeCliTunnelAttrModeInterface (CliHandle);

    return i4RetVal;
}

/******************************************************************************
 * Function Name : TeCliTunnelAttrModeInterface 
 * Description   : This routine is used to change the prompt. 
 * Input(s)      : CliHandle        - Cli Context Handle
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 * *******************************************************************************/
INT4
TeCliTunnelAttrModeInterface (tCliHandle CliHandle)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];

    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);

    /* ENTER LSP ATTRIBUTE   Mode */
    SNPRINTF ((CHR1 *) au1Cmd, sizeof (au1Cmd), "%s", MPLS_TE_ATTR_MODE);
    if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to change to LSP Attribute Mode\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : TeCliIpExplcitPathModeChange 
 * Description   : This routine is used to change the prompt.
 * Input(s)      : CliHandle        - Cli Context Handle
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 * *******************************************************************************/
INT4
TeCliIpExplcitPathModeChange (tCliHandle CliHandle)
{
    UINT1               au1Cmd[MAX_PROMPT_LEN];

    MEMSET (au1Cmd, 0, MAX_PROMPT_LEN);

    /* ENTER LSP ATTRIBUTE   Mode */
    SNPRINTF ((CHR1 *) au1Cmd, sizeof (au1Cmd), "%s",
              MPLS_TE_EXPLICIT_PATH_MODE);
    if (CliChangePath ((CHR1 *) au1Cmd) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to change to explicit path mode\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name : TeCliTunnelAttrListDelete
 * Description   : This routine is used to delete the LSP Attribute Mode 
 * Input(s)      : CliHandle        - Cli Context Handle
 *                 u4Command        - Specifies the command given as the input
 *                  pu1Arg           -  Attribute list Name
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 * *******************************************************************************/
INT4
TeCliTunnelAttrListDelete (tCliHandle CliHandle, UINT1 *pu1Arg)
{
    tTeAttrListInfo     TeAttrListInfo;
    INT4                i4RetVal = TE_FAILURE;

    MEMSET (&TeAttrListInfo, 0, sizeof (tTeAttrListInfo));

    TeAttrListInfo.i4ListNameLen = (INT4) STRLEN (pu1Arg);
    MEMCPY (TeAttrListInfo.au1ListName, pu1Arg, TeAttrListInfo.i4ListNameLen);

    i4RetVal = TeSigGetAttrListIndexFromName (&TeAttrListInfo);

    if (i4RetVal == TE_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% Attribute List not found\n");
        return CLI_FAILURE;
    }

    if (TeCliAttrParamRowStatus (TeAttrListInfo.u4ListIndex,
                                 TE_DESTROY) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%% Unable to delete attribute list\n");
        return CLI_FAILURE;
    }

    gaTeCliArgs[CliHandle].u4AttrListIndex = TE_ZERO;

    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name : TeCliIpExplicitPathCreate
 * Description   : This routine is used to create IP Explicit Path List
 * Input(s)      : CliHandle        - Cli Context Handle
 *                 u4PathListId     - Path List Identifier
 *                 u4PathOptNum     - Path Option Number
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 * *******************************************************************************/
INT4
TeCliIpExplicitPathCreate (tCliHandle CliHandle, UINT4 u4PathListId,
                           UINT4 u4PathOptNum)
{

    if (u4PathListId > (TE_MAX_HOP_LIST (gTeGblInfo) / TE_TWO))
    {
        CliPrintf (CliHandle,
                   "\r%% Invalid path identifier. Maximum Value is %d\n",
                   (TE_MAX_HOP_LIST (gTeGblInfo) / TE_TWO));
        return CLI_FAILURE;
    }

    if (u4PathOptNum > MAX_TE_TUNNEL_INFO)
    {
        CliPrintf (CliHandle,
                   "\r%% Invalid path-option number. Maximum Value is %d\n",
                   MAX_TE_TUNNEL_INFO);
        return CLI_FAILURE;
    }

    gaTeCliArgs[CliHandle].u4PathListId = u4PathListId;
    gaTeCliArgs[CliHandle].u4PathOptionId = u4PathOptNum;

    return (TeCliIpExplcitPathModeChange (CliHandle));
}

/*******************************************************************************
 * Function Name : TeCliIpExplicitPathDelete
 * Description   : This routine is used to delete IP Explicit Path List
 * Input(s)      : CliHandle        - Cli Context Handle
 *                 u4InPathListId     - Path List Identifier
 *                 u4InPathOptNum     - Path Option Number
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 * *******************************************************************************/
INT4
TeCliIpExplicitPathDelete (tCliHandle CliHandle, UINT4 u4InPathListId,
                           UINT4 u4InPathOptNum)
{
    UINT4               u4HopIndex = TE_ZERO;
    UINT4               u4PathListId = u4InPathListId;
    UINT4               u4PathOptNum = u4InPathOptNum;
    BOOL1               bflag = FALSE;

    if (u4PathListId > (TE_MAX_HOP_LIST (gTeGblInfo)))
    {
        CliPrintf (CliHandle,
                   "\r%% Invalid path identifier. Maximum Value is %d\n",
                   TE_MAX_HOP_LIST (gTeGblInfo));
        return CLI_FAILURE;
    }

    if (u4PathOptNum > TE_MAX_PO_PER_HOP_LIST (gTeGblInfo))
    {
        CliPrintf (CliHandle,
                   "\r%% Invalid path-option number. Maximum Value is %d\n",
                   TE_MAX_PO_PER_HOP_LIST (gTeGblInfo));
        return CLI_FAILURE;
    }

    do
    {
        if (nmhGetNextIndexMplsTunnelHopTable (u4PathListId, &u4PathListId,
                                               u4PathOptNum, &u4PathOptNum,
                                               u4HopIndex, &u4HopIndex)
            == SNMP_FAILURE)
        {
            if (bflag == FALSE)
            {
                CliPrintf (CliHandle,
                           "\r%% Unable to delete Path identifier \n");
            }
            break;
        }

        if (u4PathListId != u4InPathListId)
        {
            break;
        }

        if (TeCliSetMplsTunnelHopRowStatus (u4PathListId, u4PathOptNum,
                                            u4HopIndex, TE_DESTROY)
            == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Unable to delete Hop Table \n");
            break;
        }
        bflag = TRUE;

    }
    while (TE_ONE);

    gaTeCliArgs[CliHandle].u4PathListId = TE_ZERO;
    gaTeCliArgs[CliHandle].u4PathOptionId = TE_ZERO;

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : TeCliAttrParamPriorities 
 * Description   : This routine is used to configure setup and holding 
 *                 priorities of the LSP.
 * Input(s)      : CliHandle       - Cli Context Handle
 *                 u4AttrListIndex - Attribute List Index
 *                 pTeCliArgs      - CLI Args
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 * ******************************************************************************/
INT4
TeCliAttrParamPriorities (tCliHandle CliHandle, UINT4 u4AttrListIndex,
                          tTeCliArgs * pTeCliArgs)
{
    UINT4               u4ErrorCode = TE_ZERO;

    if (pTeCliArgs->i4HoldingPrio > pTeCliArgs->i4SetupPrio)
    {
        CliPrintf (CliHandle,
                   "\r%% The Setup Priority should be less than Holding "
                   "Priority \r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsTunnelAttributeSetupPrio (&u4ErrorCode, u4AttrListIndex,
                                             pTeCliArgs->i4SetupPrio)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to configure setup priority\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsTunnelAttributeHoldingPrio (&u4ErrorCode, u4AttrListIndex,
                                               pTeCliArgs->i4HoldingPrio)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to configure holding priority\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsTunnelAttributeSetupPrio
        (u4AttrListIndex, pTeCliArgs->i4SetupPrio) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to configure setup priority\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsTunnelAttributeHoldingPrio (u4AttrListIndex,
                                            pTeCliArgs->i4HoldingPrio)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to configure holding priority\n");
        return CLI_FAILURE;
    }

    TeSigExtSetAttributeListMask (u4AttrListIndex, TE_ATTR_SETUPPRI_BITMASK);

    TeSigExtSetAttributeListMask (u4AttrListIndex, TE_ATTR_HOLDPRI_BITMASK);

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : TeCliAttrParamNoPriorities 
 * Description   : This routine is used to deconfigure setup and holding 
 *                 priorities of the LSP.
 * Input(s)      : CliHandle       - Cli Context Handle
 *                 u4AttrListIndex - Attribute List Index
 *                 pTeCliArgs      - CLI Args
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 * ******************************************************************************/
INT4
TeCliAttrParamNoPriorities (tCliHandle CliHandle, UINT4 u4AttrListIndex,
                            tTeCliArgs * pTeCliArgs)
{
    UNUSED_PARAM (pTeCliArgs);
    UNUSED_PARAM (CliHandle);

    TeSigExtResetAttributeListMask (u4AttrListIndex, TE_ATTR_SETUPPRI_BITMASK);

    TeSigExtResetAttributeListMask (u4AttrListIndex, TE_ATTR_HOLDPRI_BITMASK);

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliAttrParamAffinities
* Description   : This routine is used to configure the affinities of the   
*                 of the LSP.
* Input(s)      : CliHandle        - Cli Context Handle.
*                 u4AttrListIndex  - Attribute List Index
*                 pTeCliArgs       - CLI Args
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliAttrParamAffinities (tCliHandle CliHandle, UINT4 u4AttrListIndex,
                          tTeCliArgs * pTeCliArgs)
{
    UINT4               u4ErrorCode = TE_ZERO;

    if (nmhTestv2FsTunnelAttributeIncludeAnyAffinity
        (&u4ErrorCode, u4AttrListIndex, pTeCliArgs->u4IncludeAnyAffinity)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Invalid Include-any affinity \r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsTunnelAttributeIncludeAllAffinity
        (&u4ErrorCode, u4AttrListIndex, pTeCliArgs->u4IncludeAllAffinity)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Invalid Include-all affinity \r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsTunnelAttributeExcludeAnyAffinity
        (&u4ErrorCode, u4AttrListIndex, pTeCliArgs->u4ExcludeAnyAffinity)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Invalid Exclude-any affinity \r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsTunnelAttributeIncludeAnyAffinity
        (u4AttrListIndex, pTeCliArgs->u4IncludeAnyAffinity) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Invalid Include-all affinity \r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsTunnelAttributeIncludeAllAffinity
        (u4AttrListIndex, pTeCliArgs->u4IncludeAllAffinity) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Invalid Include-all affinity \r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsTunnelAttributeExcludeAnyAffinity
        (u4AttrListIndex, pTeCliArgs->u4ExcludeAnyAffinity) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Invalid Exclude-any affinity \r\n");
        return CLI_FAILURE;
    }

    TeSigExtSetAttributeListMask (u4AttrListIndex,
                                  TE_ATTR_INCLUDE_ANY_AFFINITY_BITMASK);

    TeSigExtSetAttributeListMask (u4AttrListIndex,
                                  TE_ATTR_INCLUDE_ALL_AFFINITY_BITMASK);

    TeSigExtSetAttributeListMask (u4AttrListIndex,
                                  TE_ATTR_EXCLUDE_ANY_AFFINITY_BITMASK);

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliAttrParamNoAffinities
* Description   : This routine is used to deconfigure the affinities of the   
*                 of the LSP.
* Input(s)      : CliHandle        - Cli Context Handle.
*                 u4AttrListIndex  - Attribute List Index
*                 pTeCliArgs       - CLI Args
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliAttrParamNoAffinities (tCliHandle CliHandle, UINT4 u4AttrListIndex,
                            tTeCliArgs * pTeCliArgs)
{
    UNUSED_PARAM (pTeCliArgs);
    UNUSED_PARAM (CliHandle);

    TeSigExtResetAttributeListMask (u4AttrListIndex,
                                    TE_ATTR_INCLUDE_ANY_AFFINITY_BITMASK);

    TeSigExtResetAttributeListMask (u4AttrListIndex,
                                    TE_ATTR_INCLUDE_ALL_AFFINITY_BITMASK);

    TeSigExtResetAttributeListMask (u4AttrListIndex,
                                    TE_ATTR_EXCLUDE_ANY_AFFINITY_BITMASK);

    return CLI_SUCCESS;
}

/*******************************************************************************
* Function Name : TeCliAttrParamBandwidth 
* Description   : This routine is used to configure bandwidth information
* Input(s)      : CliHandle        - Cli Context Handle
*                 u4AttrListIndex  - Attribute List Index
*                 pTeCliArgs       - CLI Args
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
TeCliAttrParamBandwidth (tCliHandle CliHandle, UINT4 u4AttrListIndex,
                         tTeCliArgs * pTeCliArgs)
{
    UINT4               u4ErrorCode = TE_ZERO;

    if (nmhTestv2FsTunnelAttributeBandwidth (&u4ErrorCode, u4AttrListIndex,
                                             pTeCliArgs->u4Bandwidth) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Invalid bandwidth \r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsTunnelAttributeBandwidth
        (u4AttrListIndex, pTeCliArgs->u4Bandwidth) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Invalid bandwidth \r\n");
        return CLI_FAILURE;
    }

    TeSigExtSetAttributeListMask (u4AttrListIndex, TE_ATTR_BANDWIDTH_BITMASK);

    return CLI_SUCCESS;
}

/*******************************************************************************
* Function Name : TeCliAttrParamNoBandwidth 
* Description   : This routine is used to deconfigure bandwidth information
* Input(s)      : CliHandle        - Cli Context Handle
*                 u4AttrListIndex  - Attribute List Index
*                 pTeCliArgs       - CLI Args
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
TeCliAttrParamNoBandwidth (tCliHandle CliHandle, UINT4 u4AttrListIndex,
                           tTeCliArgs * pTeCliArgs)
{
    UNUSED_PARAM (pTeCliArgs);
    UNUSED_PARAM (CliHandle);

    TeSigExtResetAttributeListMask (u4AttrListIndex, TE_ATTR_BANDWIDTH_BITMASK);
    return CLI_SUCCESS;
}

/*******************************************************************************
* Function Name : TeCliAttrParamRecordRoute
* Description   : This routine is used to configure record route
* Input(s)      : CliHandle        - Cli Context Handle
*                 u4AttrListIndex  - Attribute List Index
*                 pTeCliArgs       - CLI Args
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
TeCliAttrParamRecordRoute (tCliHandle CliHandle, UINT4 u4AttrListIndex,
                           tTeCliArgs * pTeCliArgs)
{
    UINT4               u4ErrorCode = TE_ZERO;
    UINT1               au1SsnAttr[TE_ONE] = { 0 };
    tSNMP_OCTET_STRING_TYPE SsnAttr;

    SsnAttr.pu1_OctetList = au1SsnAttr;
    SsnAttr.i4_Length = TE_ONE;

    nmhGetFsTunnelAttributeSessionAttributes (u4AttrListIndex, &SsnAttr);

    SsnAttr.pu1_OctetList[TE_ZERO] |= TE_SSN_REC_ROUTE_BIT;

    if (nmhTestv2FsTunnelAttributeSessionAttributes (&u4ErrorCode,
                                                     u4AttrListIndex,
                                                     &SsnAttr) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Invalid record-route \r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsTunnelAttributeSessionAttributes (u4AttrListIndex, &SsnAttr)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Invalid record-route \r\n");
        return CLI_FAILURE;
    }

    TeSigExtSetAttributeListMask (u4AttrListIndex,
                                  TE_ATTR_LSP_SSN_ATTR_BITMASK);

    UNUSED_PARAM (pTeCliArgs);

    return CLI_SUCCESS;
}

/*******************************************************************************
* Function Name : TeCliAttrParamNoRecordRoute
* Description   : This routine is used to deconfigure record route
* Input(s)      : CliHandle        - Cli Context Handle
*                 u4AttrListIndex  - Attribute List Index
*                 pTeCliArgs       - CLI Args
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT4
TeCliAttrParamNoRecordRoute (tCliHandle CliHandle, UINT4 u4AttrListIndex,
                             tTeCliArgs * pTeCliArgs)
{
    UINT4               u4ErrorCode = TE_ZERO;
    UINT1               au1SsnAttr[TE_ONE] = { 0 };
    tSNMP_OCTET_STRING_TYPE SsnAttr;

    SsnAttr.pu1_OctetList = au1SsnAttr;
    SsnAttr.i4_Length = TE_ONE;

    nmhGetFsTunnelAttributeSessionAttributes (u4AttrListIndex, &SsnAttr);

    SsnAttr.pu1_OctetList[TE_ZERO] &= (UINT1) (~(TE_SSN_REC_ROUTE_BIT));

    if (nmhTestv2FsTunnelAttributeSessionAttributes (&u4ErrorCode,
                                                     u4AttrListIndex,
                                                     &SsnAttr) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Invalid record-route \r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsTunnelAttributeSessionAttributes (u4AttrListIndex, &SsnAttr)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Invalid record-route \r\n");
        return CLI_FAILURE;
    }

    TeSigExtResetAttributeListMask (u4AttrListIndex,
                                    TE_ATTR_LSP_SSN_ATTR_BITMASK);

    UNUSED_PARAM (pTeCliArgs);
    return CLI_SUCCESS;
}

/*******************************************************************************
 *  Function Name : TeCliAttrParamSrlgType
 *  Description   : This routine is used to configure srlg type
 *  Input(s)      : CliHandle        - Cli Context Handle
 *                  u4AttrListIndex  - Attribute List Index
 *                  pTeCliArgs       - CLI Args
 *  Output(s)     : None
 *  Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ********************************************************************************/
INT4
TeCliAttrParamSrlgType (tCliHandle CliHandle, UINT4 u4AttrListIndex,
                        tTeCliArgs * pTeCliArgs)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (pTeCliArgs);

    TeSigExtSetAttributeListMask (u4AttrListIndex, TE_ATTR_SRLG_TYPE_BITMASK);

    return CLI_SUCCESS;
}

/*******************************************************************************
 *  Function Name : TeCliAttrParamNoSrlgType
 *  Description   : This routine is used to deconfigure srlg type
 *  Input(s)      : CliHandle        - Cli Context Handle
 *                  u4AttrListIndex  - Attribute List Index
 *                  pTeCliArgs       - CLI Args
 *  Output(s)     : None
 *  Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ********************************************************************************/
INT4
TeCliAttrParamNoSrlgType (tCliHandle CliHandle, UINT4 u4AttrListIndex,
                          tTeCliArgs * pTeCliArgs)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (pTeCliArgs);

    TeSigExtResetAttributeListMask (u4AttrListIndex, TE_ATTR_SRLG_TYPE_BITMASK);

    return CLI_SUCCESS;
}

/*******************************************************************************
 *  Function Name : TeCliAttrParamSrlgValue
 *  Description   : This routine is used to configure srlg value
 *  Input(s)      : CliHandle        - Cli Context Handle
 *                  u4AttrListIndex  - Attribute List Index
 *                  pTeCliArgs       - CLI Args
 *  Output(s)     : None
 *  Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ********************************************************************************/
INT4
TeCliAttrParamSrlgValue (tCliHandle CliHandle, UINT4 u4AttrListIndex,
                         tTeCliArgs * pTeCliArgs)
{
    if (TeCliSetTnlAttrSrlgRowStatus (u4AttrListIndex, pTeCliArgs->u4SrlgNo,
                                      TE_CREATEANDWAIT) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to create Attribute SRLG\n");
        return CLI_FAILURE;
    }

    TeCliSetTnlAttrSrlgRowStatus (u4AttrListIndex, pTeCliArgs->u4SrlgNo,
                                  TE_ACTIVE);

    return CLI_SUCCESS;
}

/*******************************************************************************
 *  Function Name : TeCliAttrParamNoSrlgValue
 *  Description   : This routine is used to configure srlg value
 *  Input(s)      : CliHandle        - Cli Context Handle
 *                  u4AttrListIndex  - Attribute List Index
 *                  pTeCliArgs       - CLI Args
 *  Output(s)     : None
 *  Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ********************************************************************************/
INT4
TeCliAttrParamNoSrlgValue (tCliHandle CliHandle, UINT4 u4AttrListIndex,
                           tTeCliArgs * pTeCliArgs)
{
    if (TeCliSetTnlAttrSrlgRowStatus (u4AttrListIndex, pTeCliArgs->u4SrlgNo,
                                      TE_DESTROY) != CLI_SUCCESS)
    {
        CliPrintf (CliHandle, "\r%%Unable to delete Attribute SRLG\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
 *  Function Name : TeCliAttrParamClassTypeValue
 *  Description   : This routine is used to configure class type value
 *  Input(s)      : CliHandle        - Cli Context Handle
 *                  u4AttrListIndex  - Attribute List Index
 *                  pTeCliArgs       - CLI Args
 *  Output(s)     : None
 *  Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ********************************************************************************/
INT4
TeCliAttrParamClassTypeValue (tCliHandle CliHandle, UINT4 u4AttrListIndex,
                              tTeCliArgs * pTeCliArgs)
{
    UINT4               u4ErrorCode = TE_ZERO;

    if (nmhTestv2FsTunnelAttributeTeClassType (&u4ErrorCode, u4AttrListIndex,
                                               pTeCliArgs->i4ClassType)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Invalid class type \r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsTunnelAttributeTeClassType (u4AttrListIndex,
                                            pTeCliArgs->i4ClassType)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Invalid class type \r\n");
        return CLI_FAILURE;
    }

    TeSigExtSetAttributeListMask (u4AttrListIndex, TE_ATTR_CLASS_TYPE_BITMASK);

    return CLI_SUCCESS;
}

/*******************************************************************************
 *  Function Name : TeCliAttrParamNoClassTypeValue
 *  Description   : This routine is used to deconfigure classtype
 *  Input(s)      : CliHandle        - Cli Context Handle
 *                  u4AttrListIndex  - Attribute List Index
 *                  pTeCliArgs       - CLI Args
 *  Output(s)     : None
 *  Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ********************************************************************************/
INT4
TeCliAttrParamNoClassTypeValue (tCliHandle CliHandle, UINT4 u4AttrListIndex,
                                tTeCliArgs * pTeCliArgs)
{
    UNUSED_PARAM (CliHandle);
    UNUSED_PARAM (pTeCliArgs);

    TeSigExtResetAttributeListMask (u4AttrListIndex,
                                    TE_ATTR_CLASS_TYPE_BITMASK);

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : TeCliAddHopTableEntries 
 * Description   : This routine add a new hop table entry
 * Input(s)      : CliHandle              - Cli Context Handle
 *                 pTeCliArgs             - Pointer to CLI Args
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS / CLI_FAILURE
 * ******************************************************************************/

INT4
TeCliAddHopTableEntries (tCliHandle CliHandle, tTeCliArgs * pTeCliArgs)
{
    INT4                i4RowStatus = TE_ZERO;
    INT4                i4HopAddrType = TE_ZERO;
    BOOL1               bNewEntry = FALSE;

    if (nmhGetMplsTunnelHopRowStatus (pTeCliArgs->u4PathListId,
                                      pTeCliArgs->u4PathOptionId,
                                      pTeCliArgs->u4HopIndex, &i4RowStatus)
        == SNMP_SUCCESS)
    {
        if (TeCliSetMplsTunnelHopRowStatus (pTeCliArgs->u4PathListId,
                                            pTeCliArgs->u4PathOptionId,
                                            pTeCliArgs->u4HopIndex,
                                            TE_NOTINSERVICE) == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Unable to set hoprow status \n");
            return CLI_FAILURE;
        }

    }
    else
    {
        bNewEntry = TRUE;
        if (TeCliSetMplsTunnelHopRowStatus (pTeCliArgs->u4PathListId,
                                            pTeCliArgs->u4PathOptionId,
                                            pTeCliArgs->u4HopIndex,
                                            TE_CREATEANDWAIT) == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Unable to create new hop\n");
            return CLI_FAILURE;
        }
    }

    if (pTeCliArgs->u4HopUnnumIf != TE_ZERO)
    {
        i4HopAddrType = TE_ERHOP_UNNUM_TYPE;
    }
    else
    {
        i4HopAddrType = TE_ERHOP_IPV4_TYPE;
    }

    if (TeCliSetMplsTunnelHopAddrType (pTeCliArgs->u4PathListId,
                                       pTeCliArgs->u4PathOptionId,
                                       pTeCliArgs->u4HopIndex,
                                       i4HopAddrType) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set hop address type\n");

        TeCliDelOrUpdateTunnelHopTable (pTeCliArgs->u4PathListId,
                                        pTeCliArgs->u4PathOptionId,
                                        pTeCliArgs->u4HopIndex, bNewEntry);
        return CLI_FAILURE;
    }

    if (TeCliSetMplsTunnelHopIpAddr (pTeCliArgs->u4PathListId,
                                     pTeCliArgs->u4PathOptionId,
                                     pTeCliArgs->u4HopIndex,
                                     pTeCliArgs->u4HopIpAddr) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set hop address\n");

        TeCliDelOrUpdateTunnelHopTable (pTeCliArgs->u4PathListId,
                                        pTeCliArgs->u4PathOptionId,
                                        pTeCliArgs->u4HopIndex, bNewEntry);
        return CLI_FAILURE;
    }

    if (TeCliSetMplsTunnelHopIpPrefixLen (pTeCliArgs->u4PathListId,
                                          pTeCliArgs->u4PathOptionId,
                                          pTeCliArgs->u4HopIndex,
                                          TE_ERHOP_IPV4_PREFIX_LEN)
        == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to set hop address prefix length\n");

        TeCliDelOrUpdateTunnelHopTable (pTeCliArgs->u4PathListId,
                                        pTeCliArgs->u4PathOptionId,
                                        pTeCliArgs->u4HopIndex, bNewEntry);
        return CLI_FAILURE;
    }

    if (pTeCliArgs->u4HopUnnumIf != TE_ZERO)
    {
        if (TeCliSetMplsTunnelHopUnnumIf (pTeCliArgs->u4PathListId,
                                          pTeCliArgs->u4PathOptionId,
                                          pTeCliArgs->u4HopIndex,
                                          pTeCliArgs->u4HopUnnumIf)
            == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Unable to set hop Unnum identifier\n");

            TeCliDelOrUpdateTunnelHopTable (pTeCliArgs->u4PathListId,
                                            pTeCliArgs->u4PathOptionId,
                                            pTeCliArgs->u4HopIndex, bNewEntry);
            return CLI_FAILURE;
        }
    }

    if (pTeCliArgs->i4HopIncludeExclude != TE_INCLUDE_ANY_HOP)
    {

        if (TeCliSetMplsTunnelHopIncludeExclude (pTeCliArgs->u4PathListId,
                                                 pTeCliArgs->u4PathOptionId,
                                                 pTeCliArgs->u4HopIndex,
                                                 pTeCliArgs->
                                                 i4HopIncludeExclude) ==
            CLI_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%% Unable to set hop include or exclude type\n");

            TeCliDelOrUpdateTunnelHopTable (pTeCliArgs->u4PathListId,
                                            pTeCliArgs->u4PathOptionId,
                                            pTeCliArgs->u4HopIndex, bNewEntry);
            return CLI_FAILURE;
        }
    }
    else
    {
        if (TeCliSetFsMplsTunnelHopIncludeAny (pTeCliArgs->u4PathListId,
                                               pTeCliArgs->u4PathOptionId,
                                               pTeCliArgs->u4HopIndex)
            == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Unable to set hop include any type\n");
            return CLI_FAILURE;
        }
    }

    if (pTeCliArgs->i4HopType != 0)
    {
        if (TeCliSetMplsTunnelHopType (pTeCliArgs->u4PathListId,
                                       pTeCliArgs->u4PathOptionId,
                                       pTeCliArgs->u4HopIndex,
                                       pTeCliArgs->i4HopType) == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Unable to set hop type\n");

            TeCliDelOrUpdateTunnelHopTable (pTeCliArgs->u4PathListId,
                                            pTeCliArgs->u4PathOptionId,
                                            pTeCliArgs->u4HopIndex, bNewEntry);
            return CLI_FAILURE;
        }
    }

    if (TeCliSetMplsTunnelHopPathComp (pTeCliArgs->u4PathListId,
                                       pTeCliArgs->u4PathOptionId,
                                       pTeCliArgs->u4HopIndex,
                                       pTeCliArgs->i4PathComp) == CLI_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r%% Unable to set hop path computation method\n");

        TeCliDelOrUpdateTunnelHopTable (pTeCliArgs->u4PathListId,
                                        pTeCliArgs->u4PathOptionId,
                                        pTeCliArgs->u4HopIndex, bNewEntry);
        return CLI_FAILURE;
    }

    if (pTeCliArgs->u4ForwardLbl != MPLS_INVALID_LABEL)
    {
        if (TeCliSetMplsTunnelHopForwardLbl (pTeCliArgs->u4PathListId,
                                             pTeCliArgs->u4PathOptionId,
                                             pTeCliArgs->u4HopIndex,
                                             pTeCliArgs->u4ForwardLbl)
            == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Unable to set forward label\n");

            TeCliDelOrUpdateTunnelHopTable (pTeCliArgs->u4PathListId,
                                            pTeCliArgs->u4PathOptionId,
                                            pTeCliArgs->u4HopIndex, bNewEntry);
            return CLI_FAILURE;
        }
    }

    if (pTeCliArgs->u4ReverseLbl != MPLS_INVALID_LABEL)
    {
        if (TeCliSetMplsTunnelHopReverseLbl (pTeCliArgs->u4PathListId,
                                             pTeCliArgs->u4PathOptionId,
                                             pTeCliArgs->u4HopIndex,
                                             pTeCliArgs->u4ReverseLbl)
            == CLI_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Unable to set reverse label\n");

            TeCliDelOrUpdateTunnelHopTable (pTeCliArgs->u4PathListId,
                                            pTeCliArgs->u4PathOptionId,
                                            pTeCliArgs->u4HopIndex, bNewEntry);
            return CLI_FAILURE;
        }
    }

    if (TeCliSetMplsTunnelHopRowStatus (pTeCliArgs->u4PathListId,
                                        pTeCliArgs->u4PathOptionId,
                                        pTeCliArgs->u4HopIndex, TE_ACTIVE)
        == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Unable to activate the Hop\n");

        TeCliDelOrUpdateTunnelHopTable (pTeCliArgs->u4PathListId,
                                        pTeCliArgs->u4PathOptionId,
                                        pTeCliArgs->u4HopIndex, bNewEntry);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : TeCliDelHopTableEntries 
 * Description   : This routine deletes a hop table entry
 * Input(s)      : CliHandle              - Cli Context Handle
 *                 pTeCliArgs             - CLI Args
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS\CLI_FAILURE
 * ******************************************************************************/
INT4
TeCliDelHopTableEntries (tCliHandle CliHandle, tTeCliArgs * pTeCliArgs)
{
    if (TeCliSetMplsTunnelHopRowStatus (pTeCliArgs->u4PathListId,
                                        pTeCliArgs->u4PathOptionId,
                                        pTeCliArgs->u4HopIndex,
                                        TE_DESTROY) == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "\r%%Unable to destroy Hop table entry\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : TeCliAttrParamRowStatus
 * Description   : This routine is used to set the row status value for
 *                 Tunnel Attribute table
 * Input         :  u4AttrListIndex - Attribute List Index
 *                  i4RowStatus           - Row Status Value
 * 
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 * ******************************************************************************/
INT4
TeCliAttrParamRowStatus (UINT4 u4AttrListIndex, INT4 i4RowStatus)
{
    UINT4               u4ErrorCode = TE_ZERO;

    if (nmhTestv2FsTunnelAttributeRowStatus (&u4ErrorCode, u4AttrListIndex,
                                             i4RowStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsTunnelAttributeRowStatus (u4AttrListIndex, i4RowStatus)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : TeCliSetMplsTunnelHopRowStatus
 * Description   : This routine is used to set the row status value for
 *                 MPLS Tunnel Hop Table
 * Input         :  u4PathListIndex       - Path List Index
 *                  u4PathOptionId        - Path Option Index
 *                  u4HopIndex            - Hop Index
 *                  i4RowStatus           - Row Status Value
 * 
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 * ******************************************************************************/
INT4
TeCliSetMplsTunnelHopRowStatus (UINT4 u4PathListIndex, UINT4 u4PathOptionId,
                                UINT4 u4HopIndex, INT4 i4RowStatus)
{
    UINT4               u4ErrorCode = TE_ZERO;

    if (nmhTestv2MplsTunnelHopRowStatus (&u4ErrorCode, u4PathListIndex,
                                         u4PathOptionId, u4HopIndex,
                                         i4RowStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetMplsTunnelHopRowStatus (u4PathListIndex, u4PathOptionId,
                                      u4HopIndex, i4RowStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : TeCliSetTnlAttrSrlgRowStatus
 * Description   : This routine is used to set the row status of Srlg Table
 * Input         :  u4AttrListIndex       - Attribute List Index
 *                  u4SrlgNo              - Srlg value
 *                  i4RowStatus           - Row Status Value
 *
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 * ******************************************************************************/
INT4
TeCliSetTnlAttrSrlgRowStatus (UINT4 u4AttrListIndex, UINT4 u4SrlgNo,
                              INT4 i4RowStatus)
{
    UINT4               u4ErrorCode = TE_ZERO;

    if (nmhTestv2FsTunnelAttributeSrlgRowStatus (&u4ErrorCode, u4AttrListIndex,
                                                 u4SrlgNo,
                                                 i4RowStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsTunnelAttributeSrlgRowStatus (u4AttrListIndex, u4SrlgNo,
                                              i4RowStatus) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : TeCliDelOrUpdateTunnelHopTable
 * Description   : This routine is used to delete or update tunnel hop table
 *                 MPLS Tunnel Hop Table
 * Input         :  u4PathListIndex       - Path List Index
 *                  u4PathOptionId        - Path Option Index
 *                  u4HopIndex            - Hop Index
 *                  bNewEntry             - Flag to indicate the new entry
 * 
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 * ******************************************************************************/
VOID
TeCliDelOrUpdateTunnelHopTable (UINT4 u4PathListIndex, UINT4 u4PathOptionId,
                                UINT4 u4HopIndex, BOOL1 bNewEntry)
{
    INT4                i4RowStatus = TE_ZERO;

    if (bNewEntry == TRUE)
    {
        i4RowStatus = TE_DESTROY;
    }
    else
    {
        i4RowStatus = TE_ACTIVE;
    }

    if (TeCliSetMplsTunnelHopRowStatus (u4PathListIndex, u4PathOptionId,
                                        u4HopIndex, i4RowStatus) == CLI_FAILURE)
    {
        return;
    }

    return;
}

/******************************************************************************
 * Function Name : TeCliSetMplsTunnelHopAddrType
 * Description   : This routine is used to set the Addr Type for
 *                 MPLS Tunnel Hop Table
 * Input         :  u4PathListIndex       - Path List Index
 *                  u4PathOptionId        - Path Option Index
 *                  u4HopIndex            - Hop Index
 *                  i4HopAddrType         - Hop Address Type
 * 
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 * ******************************************************************************/
INT4
TeCliSetMplsTunnelHopAddrType (UINT4 u4PathListIndex, UINT4 u4PathOptionId,
                               UINT4 u4HopIndex, INT4 i4HopAddrType)
{
    UINT4               u4ErrorCode = TE_ZERO;

    if (nmhTestv2MplsTunnelHopAddrType (&u4ErrorCode, u4PathListIndex,
                                        u4PathOptionId, u4HopIndex,
                                        i4HopAddrType) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetMplsTunnelHopAddrType
        (u4PathListIndex, u4PathOptionId, u4HopIndex,
         i4HopAddrType) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : TeCliSetMplsTunnelHopIpAddr
 * Description   : This routine is used to set the IP Addr for
 *                 MPLS Tunnel Hop Table
 * Input         :  u4PathListIndex       - Path List Index
 *                  u4PathOptionId        - Path Option Index
 *                  u4HopIndex            - Hop Index
 *                  u4HopIpAddr           - IP Addr
 * 
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 * ******************************************************************************/
INT4
TeCliSetMplsTunnelHopIpAddr (UINT4 u4PathListIndex, UINT4 u4PathOptionId,
                             UINT4 u4HopIndex, UINT4 u4HopIpAddr)
{
    tSNMP_OCTET_STRING_TYPE HopIpAddr;
    UINT1               au1HopIpAddr[TE_FOUR] = { 0 };
    UINT4               u4ErrorCode = TE_ZERO;

    HopIpAddr.pu1_OctetList = au1HopIpAddr;

    MPLS_INTEGER_TO_OCTETSTRING (u4HopIpAddr, (&HopIpAddr));

    if (nmhTestv2MplsTunnelHopIpAddr (&u4ErrorCode, u4PathListIndex,
                                      u4PathOptionId, u4HopIndex,
                                      &HopIpAddr) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetMplsTunnelHopIpAddr (u4PathListIndex, u4PathOptionId, u4HopIndex,
                                   &HopIpAddr) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : TeCliSetMplsTunnelHopIpPrefixLen
 * Description   : This routine is used to set the IP Prefix len  for
 *                 MPLS Tunnel Hop Table
 * Input         :  u4PathListIndex       - Path List Index
 *                  u4PathOptionId        - Path Option Index
 *                  u4HopIndex            - Hop Index
 *                  u4HopIpPrefixLen      - Prefix Len                  
 * 
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 * ******************************************************************************/
INT4
TeCliSetMplsTunnelHopIpPrefixLen (UINT4 u4PathListIndex, UINT4 u4PathOptionId,
                                  UINT4 u4HopIndex, UINT4 u4HopIpPrefixLen)
{
    UINT4               u4ErrorCode = TE_ZERO;

    if (nmhTestv2MplsTunnelHopIpPrefixLen (&u4ErrorCode, u4PathListIndex,
                                           u4PathOptionId, u4HopIndex,
                                           u4HopIpPrefixLen) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetMplsTunnelHopIpPrefixLen
        (u4PathListIndex, u4PathOptionId, u4HopIndex,
         u4HopIpPrefixLen) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : TeCliSetMplsTunnelHopUnnumIf
 * Description   : This routine is used to set the unnumbered identifier for
 *                 MPLS Tunnel Hop Table
 * Input         :  u4PathListIndex       - Path List Index
 *                  u4PathOptionId        - Path Option Index
 *                  u4HopIndex            - Hop Index
 *                  u4HopUnnumIf          - Unnumbered Identifier Value
 * 
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 * ******************************************************************************/
INT4
TeCliSetMplsTunnelHopUnnumIf (UINT4 u4PathListIndex, UINT4 u4PathOptionId,
                              UINT4 u4HopIndex, UINT4 u4HopUnnumIf)
{
    tSNMP_OCTET_STRING_TYPE HopUnnumIf;

    UINT1               au1HopUnnumIf[TE_FOUR] = { 0 };

    UINT4               u4ErrorCode = TE_ZERO;

    HopUnnumIf.pu1_OctetList = au1HopUnnumIf;

    MPLS_INTEGER_TO_OCTETSTRING (u4HopUnnumIf, (&HopUnnumIf));

    if (nmhTestv2MplsTunnelHopAddrUnnum (&u4ErrorCode, u4PathListIndex,
                                         u4PathOptionId, u4HopIndex,
                                         &HopUnnumIf) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetMplsTunnelHopAddrUnnum (u4PathListIndex, u4PathOptionId,
                                      u4HopIndex, &HopUnnumIf) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : TeCliSetMplsTunnelHopIncludeExclude
 * Description   : This routine is used to set the row status value for
 *                 MPLS Tunnel Hop Table
 * Input         :  u4PathListIndex       - Path List Index
 *                  u4PathOptionId        - Path Option Index
 *                  u4HopIndex            - Hop Index
 *                  i4IncludeExclude      - Include or Exclude
 * 
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 * ******************************************************************************/
INT4
TeCliSetMplsTunnelHopIncludeExclude (UINT4 u4PathListIndex,
                                     UINT4 u4PathOptionId,
                                     UINT4 u4HopIndex, INT4 i4IncludeExclude)
{
    UINT4               u4ErrorCode = TE_ZERO;

    if (nmhTestv2MplsTunnelHopInclude (&u4ErrorCode, u4PathListIndex,
                                       u4PathOptionId, u4HopIndex,
                                       i4IncludeExclude) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetMplsTunnelHopInclude (u4PathListIndex, u4PathOptionId, u4HopIndex,
                                    i4IncludeExclude) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : TeCliSetMplsTunnelHopType
 * Description   : This routine is used to set the Hop Type for
 *                 MPLS Tunnel Hop Table
 * Input         :  u4PathListIndex       - Path List Index
 *                  u4PathOptionId        - Path Option Index
 *                  u4HopIndex            - Hop Index
 *                  i4HopType             - Hop Type
 * 
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 * ******************************************************************************/
INT4
TeCliSetMplsTunnelHopType (UINT4 u4PathListIndex, UINT4 u4PathOptionId,
                           UINT4 u4HopIndex, INT4 i4HopType)
{
    UINT4               u4ErrorCode = TE_ZERO;

    if (nmhTestv2MplsTunnelHopType (&u4ErrorCode, u4PathListIndex,
                                    u4PathOptionId, u4HopIndex,
                                    i4HopType) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetMplsTunnelHopType (u4PathListIndex, u4PathOptionId,
                                 u4HopIndex, i4HopType) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : TeCliSetMplsTunnelHopPathComp
 * Description   : This routine is used to set the PATH Computation method for
 *                 MPLS Tunnel Hop Table
 * Input         :  u4PathListIndex       - Path List Index
 *                  u4PathOptionId        - Path Option Index
 *                  u4HopIndex            - Hop Index
 *                  i4PathComp            - Path Computation method
 * 
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 * ******************************************************************************/
INT4
TeCliSetMplsTunnelHopPathComp (UINT4 u4PathListIndex, UINT4 u4PathOptionId,
                               UINT4 u4HopIndex, INT4 i4PathComp)
{
    UINT4               u4ErrorCode = TE_ZERO;

    if (nmhTestv2MplsTunnelHopEntryPathComp (&u4ErrorCode, u4PathListIndex,
                                             u4PathOptionId, u4HopIndex,
                                             i4PathComp) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetMplsTunnelHopEntryPathComp (u4PathListIndex, u4PathOptionId,
                                          u4HopIndex,
                                          i4PathComp) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : TeCliSetMplsTunnelHopForwardLbl
 * Description   : This routine is used to set the Forward label value in
 *                 MPLS Tunnel Hop Table
 * Input         :  u4PathListIndex       - Path List Index
 *                  u4PathOptionId        - Path Option Index
 *                  u4HopIndex            - Hop Index
 *                  u4ForwardLbl          - Forward Label                  
 * 
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 * ******************************************************************************/
INT4
TeCliSetMplsTunnelHopForwardLbl (UINT4 u4PathListIndex, UINT4 u4PathOptionId,
                                 UINT4 u4HopIndex, UINT4 u4ForwardLbl)
{
    UINT4               u4ErrorCode = TE_ZERO;

    if (nmhTestv2GmplsTunnelHopExplicitForwardLabel (&u4ErrorCode,
                                                     u4PathListIndex,
                                                     u4PathOptionId,
                                                     u4HopIndex,
                                                     u4ForwardLbl)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetGmplsTunnelHopExplicitForwardLabel
        (u4PathListIndex, u4PathOptionId, u4HopIndex,
         u4ForwardLbl) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : TeCliSetMplsTunnelHopReverseLbl
 * Description   : This routine is used to set the reverse label value in
 *                 MPLS Tunnel Hop Table
 * Input         :  u4PathListIndex       - Path List Index
 *                  u4PathOptionId        - Path Option Index
 *                  u4HopIndex            - Hop Index
 *                  u4ReverseLbl          - Reverse Label                  
 * 
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 * ******************************************************************************/
INT4
TeCliSetMplsTunnelHopReverseLbl (UINT4 u4PathListIndex, UINT4 u4PathOptionId,
                                 UINT4 u4HopIndex, UINT4 u4ReverseLbl)
{
    UINT4               u4ErrorCode = TE_ZERO;

    if (nmhTestv2GmplsTunnelHopExplicitReverseLabel (&u4ErrorCode,
                                                     u4PathListIndex,
                                                     u4PathOptionId,
                                                     u4HopIndex,
                                                     u4ReverseLbl)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetGmplsTunnelHopExplicitReverseLabel
        (u4PathListIndex, u4PathOptionId, u4HopIndex,
         u4ReverseLbl) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
 * Function Name : TeCliSetTnlAttrProperties
 * Description   : This routine is used to set Properties of Attribute table.
 * Input(s)      : CliHandle  - Cli Context Handle
 *                 u4Command  - Command Requested
 *                 pTeCliArgs - CLI Args
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ********************************************************************************/
INT4
TeCliSetTnlAttrProperties (tCliHandle CliHandle, UINT4 u4Command,
                           tTeCliArgs * pTeCliArgs)
{
    INT4                i4RowStatus = TE_ZERO;
    INT4                i4RetStatus = TE_ZERO;
    UINT4               u4AttrListIndex = TE_ZERO;

    u4AttrListIndex = gaTeCliArgs[CliHandle].u4AttrListIndex;

    if (nmhGetFsTunnelAttributeRowStatus (u4AttrListIndex,
                                          &i4RowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% Attribute List not present\n");
        return CLI_FAILURE;
    }

    if (i4RowStatus == TE_ACTIVE)
    {
        TeCliAttrParamRowStatus (u4AttrListIndex, TE_NOTINSERVICE);
    }

    switch (u4Command)
    {
        case CLI_MPLS_TE_ATTR_PRIORITY:
            i4RetStatus
                = TeCliAttrParamPriorities (CliHandle, u4AttrListIndex,
                                            pTeCliArgs);
            break;
        case CLI_MPLS_TE_ATTR_NO_PRIORITY:
            i4RetStatus
                = TeCliAttrParamNoPriorities (CliHandle, u4AttrListIndex,
                                              pTeCliArgs);
            break;

        case CLI_MPLS_TE_ATTR_AFFINITY:
            i4RetStatus
                = TeCliAttrParamAffinities (CliHandle, u4AttrListIndex,
                                            pTeCliArgs);
            break;
        case CLI_MPLS_TE_ATTR_NO_AFFINITY:
            i4RetStatus
                = TeCliAttrParamNoAffinities (CliHandle, u4AttrListIndex,
                                              pTeCliArgs);
            break;
        case CLI_MPLS_TE_ATTR_BW:
            i4RetStatus
                = TeCliAttrParamBandwidth (CliHandle, u4AttrListIndex,
                                           pTeCliArgs);
            break;
        case CLI_MPLS_TE_ATTR_NO_BW:
            i4RetStatus
                = TeCliAttrParamNoBandwidth (CliHandle, u4AttrListIndex,
                                             pTeCliArgs);
            break;
        case CLI_MPLS_TE_ATTR_RECORD_ROUTE:
            i4RetStatus
                = TeCliAttrParamRecordRoute (CliHandle, u4AttrListIndex,
                                             pTeCliArgs);
            break;
        case CLI_MPLS_TE_ATTR_NO_RECORD_ROUTE:
            i4RetStatus
                = TeCliAttrParamNoRecordRoute (CliHandle, u4AttrListIndex,
                                               pTeCliArgs);
            break;
        case CLI_MPLS_TE_ATTR_SRLG_TYPE:
            i4RetStatus
                = TeCliAttrParamSrlgType (CliHandle, u4AttrListIndex,
                                          pTeCliArgs);
            break;
        case CLI_MPLS_TE_ATTR_NO_SRLG_TYPE:
            i4RetStatus
                = TeCliAttrParamNoSrlgType (CliHandle, u4AttrListIndex,
                                            pTeCliArgs);
            break;
        case CLI_MPLS_TE_ATTR_SRLG_VALUE:
            i4RetStatus
                = TeCliAttrParamSrlgValue (CliHandle, u4AttrListIndex,
                                           pTeCliArgs);
            break;
        case CLI_MPLS_TE_ATTR_NO_SRLG_VALUE:
            i4RetStatus
                = TeCliAttrParamNoSrlgValue (CliHandle, u4AttrListIndex,
                                             pTeCliArgs);
            break;

        case CLI_MPLS_TE_ATTR_CLASSTYPE_VALUE:
            i4RetStatus
                = TeCliAttrParamClassTypeValue (CliHandle, u4AttrListIndex,
                                                pTeCliArgs);
            break;
        case CLI_MPLS_TE_ATTR_NO_CLASSTYPE_VALUE:
            i4RetStatus
                = TeCliAttrParamNoClassTypeValue (CliHandle, u4AttrListIndex,
                                                  pTeCliArgs);
            break;

        default:
            break;
    }

    if (i4RowStatus == TE_ACTIVE)
    {
        TeCliAttrParamRowStatus (u4AttrListIndex, TE_ACTIVE);
    }

    return i4RetStatus;
}

/******************************************************************************
 * Function Name : TePrintGmplsTunnelInfo
 * Description   : This routine is used to Print the  GMPLS Parameter.
 * Input(s)      : CliHandle - Cli Context Handle
 *                 tTeTnlInfo *pTeTnlInfo
 * Output(s)     : None
 * Return(s)     : None
 *******************************************************************************/
VOID
TePrintGmplsTunnelInfo (tCliHandle CliHandle, tTeTnlInfo * pTeTnlInfo)
{
    CliPrintf (CliHandle, "\r   Gmpls Parameters are\n");

    if (pTeTnlInfo->GmplsTnlInfo.u1EncodingType != GMPLS_TUNNEL_LSP_PACKET)
    {
        CliPrintf (CliHandle, "\r      Encoding Type : Unknown\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r      Encoding Type : Lsp Packet\n");
    }

    if (pTeTnlInfo->GmplsTnlInfo.u1SwitchingType == GMPLS_PSC1)
    {
        CliPrintf (CliHandle, "\r      Switching Type : psc1\n");
    }
    else if (pTeTnlInfo->GmplsTnlInfo.u1SwitchingType == GMPLS_PSC2)
    {
        CliPrintf (CliHandle, "\r      Switching Type : psc2\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r      Switching Type : unknown\n");
    }

    if (pTeTnlInfo->GmplsTnlInfo.u2Gpid != GMPLS_GPID_ETHERNET)
    {
        CliPrintf (CliHandle, "\r      Gpid : Unknown\n");
    }
    else
    {
        CliPrintf (CliHandle, "\r      Gpid : Ethernet\n");
    }

    return;
}

/******************************************************************************
 * Function Name : TeCliShowDynInfoForNonZeroInst
 * Description   : This routine scans and shows dynamic / learnt tunnel info
 * Input(s)      : CliHandle - Cli Context Handle
 *                 tTeTnlInfo *pTeTnlInfo
 * Output(s)     : None
 * Return(s)     : None
 *******************************************************************************/
VOID
TeCliShowDynInfoForNonZeroInst (tCliHandle CliHandle, tTeTnlInfo * pTeTnlInfo)
{
    tTeTnlInfo         *pNonZeroTnlInfo = pTeTnlInfo;
    INT1                ai1TnlCreateTime[MAX_CLI_TIME_STRING];
    INT1                ai1TnlUpTime[MAX_CLI_TIME_STRING];
    UINT4               u4CurrentTime = TE_ZERO;

    pNonZeroTnlInfo = TeGetNextTunnelInfo (pNonZeroTnlInfo);

    while (pNonZeroTnlInfo != NULL)
    {
        if ((pNonZeroTnlInfo->u4TnlIndex == pTeTnlInfo->u4TnlIndex) &&
            (MEMCMP (pNonZeroTnlInfo->TnlIngressLsrId,
                     pTeTnlInfo->TnlIngressLsrId, sizeof (UINT4)) == TE_ZERO) &&
            (MEMCMP (pNonZeroTnlInfo->TnlEgressLsrId,
                     pTeTnlInfo->TnlEgressLsrId, sizeof (UINT4)) == TE_ZERO))
        {
            TeCliShowTnlDynamicInfo (CliHandle, pNonZeroTnlInfo);

            OsixGetSysTime (&u4CurrentTime);
            CliMplsConvertTimetoString (u4CurrentTime -
                                        TE_TNL_CREATE_TIME (pNonZeroTnlInfo),
                                        ai1TnlCreateTime);
            OsixGetSysTime (&u4CurrentTime);
            CliMplsConvertTimetoString (u4CurrentTime -
                                        TE_TNL_PRIMARY_INSTANCE_UP_TIME
                                        (pNonZeroTnlInfo), ai1TnlUpTime);

            CliPrintf (CliHandle,
                       "\r   Time since created     : %-10s\n",
                       ai1TnlCreateTime);
            CliPrintf (CliHandle,
                       "\r   Time since up          : %-10s\n", ai1TnlUpTime);
        }

        pNonZeroTnlInfo = TeGetNextTunnelInfo (pNonZeroTnlInfo);
    }

    return;
}

/******************************************************************************
 * Function Name : TeCliShowTnlDynamicInfo
 * Description   : This routine shows dynamic / learnt tunnel info
 * Input(s)      : CliHandle - Cli Context Handle
 *                 tTeTnlInfo *pTeTnlInfo
 * Output(s)     : None
 * Return(s)     : None
 *******************************************************************************/
VOID
TeCliShowTnlDynamicInfo (tCliHandle CliHandle, tTeTnlInfo * pTeTnlInfo)
{
    CHR1               *pEgrTnlIp = NULL;
    CHR1               *pIngTnlIp = NULL;
    UINT4               u4EgrTnlIp = 0;
    UINT4               u4IngTnlIp = 0;

    /* MPLS_P2MP_LSP_CHANGES - S */
    if (pTeTnlInfo->u4TnlType & TE_TNL_TYPE_P2MP)
    {
        TeCliShowP2mpDestinations (CliHandle, pTeTnlInfo);
        return;
    }
    /* MPLS_P2MP_LSP_CHANGES - E */

    if (pTeTnlInfo->u1TnlPathType == TE_TNL_WORKING_PATH)
    {
        CliPrintf (CliHandle, "\r Working Path Info: \n");

        if (pTeTnlInfo->u1TnlLocalProtectInUse != LOCAL_PROT_NOT_APPLICABLE)
        {
            CliPrintf (CliHandle,
                       "\r Protection Tunnel Id: %d lsp-num: %d\n",
                       pTeTnlInfo->u4BkpTnlIndex, pTeTnlInfo->u4BkpTnlInstance);
        }
    }
    else if (pTeTnlInfo->u1TnlPathType == TE_TNL_PROTECTION_PATH)
    {
        CliPrintf (CliHandle, "\r Protection Path Info: \n");
    }
    else
    {
        if (pTeTnlInfo->u1TnlLocalProtectInUse != LOCAL_PROT_NOT_APPLICABLE)
        {
            CliPrintf (CliHandle,
                       "\r Tunnel id: %d lsp-num: %d\n",
                       pTeTnlInfo->u4BkpTnlIndex, pTeTnlInfo->u4BkpTnlInstance);
        }
    }

    if (pTeTnlInfo->bTnlIntOamEnable)
    {
        switch (pTeTnlInfo->u1OamOperStatus)
        {
            case TE_OPER_UP:
                CliPrintf (CliHandle, "\r   OAM status: Enabled, Up\n");
                break;

            case TE_OPER_DOWN:
                CliPrintf (CliHandle, "\r   OAM status: Enabled, Down\n");
                break;
            case TE_OPER_UNKNOWN:
                CliPrintf (CliHandle, "\r   OAM status: Enabled, Unknown\n");
                break;
            default:
                break;
        }
    }
    else
    {
        CliPrintf (CliHandle, "\r   OAM status: Disabled\n");
    }
    CliPrintf (CliHandle, "\r   Protection status:");
    switch (pTeTnlInfo->u1TnlLocalProtectInUse)
    {
        case LOCAL_PROT_NOT_APPLICABLE:
            CliPrintf (CliHandle, " Not applicable\n");
            break;
        case LOCAL_PROT_IN_USE:
            CliPrintf (CliHandle, " In Use\n");
            break;
        case LOCAL_PROT_AVAIL:
            CliPrintf (CliHandle, " Available\n");
            break;
        case LOCAL_PROT_NOT_AVAIL:
            CliPrintf (CliHandle, " Not available\n");
            break;
        default:
            CliPrintf (CliHandle, " Unknown\n");
            break;
    }

    if (pTeTnlInfo->u4TnlMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
    {
        CliPrintf (CliHandle, "\r   Forward label information \n");
    }

    TeCliDisplayLabelInfo (CliHandle, pTeTnlInfo->u4TnlXcIndex,
                           MPLS_DIRECTION_FORWARD, pTeTnlInfo->u1TnlSgnlPrtcl);

    if (pTeTnlInfo->u4TnlMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
    {
        CliPrintf (CliHandle, "\r   Reverse label information \n");

        TeCliDisplayLabelInfo (CliHandle, pTeTnlInfo->u4TnlXcIndex,
                               MPLS_DIRECTION_REVERSE,
                               pTeTnlInfo->u1TnlSgnlPrtcl);
    }

    /*Displaying the Rsvp signalling information with explicit route table */
    if (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_RSVP)
    {
        CONVERT_TO_INTEGER (TE_TNL_INGRESS_LSRID (pTeTnlInfo), u4IngTnlIp);
        u4IngTnlIp = OSIX_NTOHL (u4IngTnlIp);

        CONVERT_TO_INTEGER (TE_TNL_EGRESS_LSRID (pTeTnlInfo), u4EgrTnlIp);
        u4EgrTnlIp = OSIX_NTOHL (u4EgrTnlIp);

        CliPrintf (CliHandle, "\r   RSVP Signaling Info: ");

        CLI_CONVERT_IPADDR_TO_STR (pIngTnlIp, u4IngTnlIp);
        CliPrintf (CliHandle, "\r       Src: %s, ", pIngTnlIp);

        CLI_CONVERT_IPADDR_TO_STR (pEgrTnlIp, u4EgrTnlIp);
        CliPrintf (CliHandle, "Dest: %s, Tun_Id: %d, Tun_Instance: %d\n",
                   pEgrTnlIp, pTeTnlInfo->u4TnlIndex,
                   pTeTnlInfo->u4TnlInstance);

        MPLS_CMN_UNLOCK ();
        TeCliErHopShow (CliHandle, pTeTnlInfo, FALSE);
        TeCliRrHopShow (CliHandle, pTeTnlInfo);
        MPLS_CMN_LOCK ();
    }

    return;
}

/******************************************************************************
 * Function Name : TeSRCDisplayStaticXCForNonZeroInst
 * Description   : This routine shows SRC for static cross-connects for all
 *                 non-zero instances
 * Input(s)      : CliHandle      - Cli Context Handle
 *                 u4TnlIndex     - Tunnel Index
 *                 u4TnlInstance  - Tunnel Instance
 *                 u4IngressId    - Ingress ID
 *                 u4EgressId     - Egress ID
 *                 pu1Space       - Space
 * Output(s)     : None
 * Return(s)     : None
 *******************************************************************************/
VOID
TeSRCDisplayStaticXCForNonZeroInst (tCliHandle CliHandle, UINT4 u4TnlIndex,
                                    UINT4 u4TnlInstance, UINT4 u4IngressId,
                                    UINT4 u4EgressId, UINT1 *pu1Space)
{
    UINT4               u4OrgTnlIndex = u4TnlIndex;
    UINT4               u4OrgIngressId = u4IngressId;
    UINT4               u4OrgEgressId = u4EgressId;

    while (nmhGetNextIndexMplsTunnelTable (u4TnlIndex, &u4TnlIndex,
                                           u4TnlInstance, &u4TnlInstance,
                                           u4IngressId, &u4IngressId,
                                           u4EgressId, &u4EgressId)
           == SNMP_SUCCESS)
    {
        if ((u4TnlIndex != u4OrgTnlIndex) ||
            (u4IngressId != u4OrgIngressId) || (u4EgressId != u4OrgEgressId))
        {
            break;
        }

        TeSRCDisplayStaticXC (CliHandle, u4TnlIndex, u4TnlInstance,
                              u4IngressId, u4EgressId, pu1Space);
    }

    return;
}

/******************************************************************************
 * Function Name : TeSRCDisplayStaticXC
 * Description   : This routine shows SRC for static cross-connects for a
 *                 tunnel instance
 * Input(s)      : CliHandle      - Cli Context Handle
 *                 u4TnlIndex     - Tunnel Index
 *                 u4TnlInstance  - Tunnel Instance
 *                 u4IngressId    - Ingress ID
 *                 u4EgressId     - Egress ID
 *                 pu1Space       - Space
 * Output(s)     : None
 * Return(s)     : None
 *******************************************************************************/
VOID
TeSRCDisplayStaticXC (tCliHandle CliHandle, UINT4 u4TnlIndex,
                      UINT4 u4TnlInstance, UINT4 u4IngressId,
                      UINT4 u4EgressId, UINT1 *pu1Space)
{
    tSNMP_OCTET_STRING_TYPE XCIndex;
    tSNMP_OID_TYPE      XCSegIndex;
    UINT4               u4XCIndex = 0;
    UINT4               u4Offset = 0;
    INT4                i4TnlMode = 0;

    static UINT4        au4XCSegIndex[MPLS_TE_XC_TABLE_OFFSET] = { 0 };
    static UINT1        au1XcIndex[MPLS_INDEX_LENGTH] = { 0 };

    XCSegIndex.pu4_OidList = au4XCSegIndex;
    XCIndex.pu1_OctetList = au1XcIndex;

    /* get cross connect pointer(cross connect segment index) */
    if (nmhGetMplsTunnelXCPointer (u4TnlIndex, u4TnlInstance, u4IngressId,
                                   u4EgressId, &XCSegIndex) == SNMP_FAILURE)
    {
        return;
    }

    if (nmhGetFsMplsTunnelMode (u4TnlIndex, u4TnlInstance, u4IngressId,
                                u4EgressId, &i4TnlMode) == SNMP_FAILURE)
    {
        return;
    }

    /* The length of XCIndex Oid List is 28. The XCSegment starts from
     * offset 13, InSegment from 18 and OutSegment from 23rd offset.
     * The macro GET_SEG_FROM_TUNNEL_XC fills the XCSegmentIndex,
     * InSegmentIndex and OutSegmentIndex entries by copying from the oid
     * list of XCIndex using the offsets  specified.*/

    u4Offset = TE_TNL_XC_TABLE_DEF_OFFSET - 1;
    GET_SEG_FROM_TUNNEL_XC (au1XcIndex, (&XCIndex), (&XCSegIndex), u4Offset);
    MPLS_OCTETSTRING_TO_INTEGER ((&XCIndex), u4XCIndex);

    CliPrintf (CliHandle, "\r\n%stunnel mpls static", pu1Space);
    FilePrintf (CliHandle, "\r\n%stunnel mpls static", pu1Space);
    if (TeShowRunningConfigLabelInfoDisplay (CliHandle, u4XCIndex,
                                             MPLS_DIRECTION_FORWARD) ==
        CLI_FAILURE)
    {
        return;
    }

    if (i4TnlMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
    {
        CliPrintf (CliHandle, "\r\n%stunnel mpls static", pu1Space);
        FilePrintf (CliHandle, "\r\n%stunnel mpls static", pu1Space);
        if (TeShowRunningConfigLabelInfoDisplay (CliHandle, u4XCIndex,
                                                 MPLS_DIRECTION_REVERSE)
            == CLI_FAILURE)
        {
            return;
        }
    }

    return;
}

/******************************************************************************
 * Function Name : TeCliCreateTunnel
 * Description   : This routine creates a tunnel
 * Input(s)      : CliHandle             - Cli Context Handle
 *                 pTeCliArgs            - Pointer to TE Cli Args containing
 *                                         required parameters for tunnel
 * Output(s)     : None
 * Return(s)     : CLI_FAILURE / CLI_SUCCESS
 *******************************************************************************/
INT4
TeCliCreateTunnel (tCliHandle CliHandle, tTeCliArgs * pTeCliArgs)
{
    UINT4               u4ErrorCode = TE_ZERO;
    UINT4               u4ContextId = MPLS_DEF_CONTEXT_ID;
    UINT4               u4LocalMapNum = TE_ZERO;
    INT4                i4MplsTnlIfIndex = TE_ZERO;
    INT4                i4TnlRole = TE_INGRESS;
    CHR1                ac1TunnelName[SNMP_MAX_OCTETSTRING_SIZE];
    tSNMP_OCTET_STRING_TYPE TunnelName;
    tSNMP_OCTET_STRING_TYPE LSRIdMapInfo;
    tSNMP_OCTET_STRING_TYPE MplsTeTnlType;
    UINT1               au1LSRIdMapInfo[MPLS_ONE] = { 0 };
    UINT1               au1TnlType[CLI_MPLS_TE_TNL_TYPE] = { 0 };

    LSRIdMapInfo.pu1_OctetList = au1LSRIdMapInfo;
    LSRIdMapInfo.i4_Length = sizeof (UINT1);

    MplsTeTnlType.pu1_OctetList = au1TnlType;
    MplsTeTnlType.i4_Length = CLI_MPLS_TE_TNL_TYPE;

    SNPRINTF (ac1TunnelName, SNMP_MAX_OCTETSTRING_SIZE, "%s%d",
              "mplstunnel", pTeCliArgs->u4TunnelIndex);
    TunnelName.pu1_OctetList = (UINT1 *) ac1TunnelName;
    TunnelName.i4_Length = STRLEN (ac1TunnelName);

    if (nmhTestv2MplsTunnelRowStatus (&u4ErrorCode,
                                      pTeCliArgs->u4TunnelIndex,
                                      pTeCliArgs->u4TunnelInstance,
                                      pTeCliArgs->u4IngressId,
                                      pTeCliArgs->u4EgressId,
                                      MPLS_STATUS_CREATE_AND_WAIT) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%% Unable to create tunnel. Maximum Value reached.\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetMplsTunnelRowStatus (pTeCliArgs->u4TunnelIndex,
                                   pTeCliArgs->u4TunnelInstance,
                                   pTeCliArgs->u4IngressId,
                                   pTeCliArgs->u4EgressId,
                                   MPLS_STATUS_CREATE_AND_WAIT) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%% Unable to create tunnel. Maximum Value reached.\r\n");
        return CLI_FAILURE;
    }

    /* Set Tunnel IsIf to true */
    if (nmhTestv2MplsTunnelIsIf (&u4ErrorCode,
                                 pTeCliArgs->u4TunnelIndex,
                                 pTeCliArgs->u4TunnelInstance,
                                 pTeCliArgs->u4IngressId,
                                 pTeCliArgs->u4EgressId,
                                 TE_SNMP_TRUE) == SNMP_FAILURE)
    {
        if (nmhSetMplsTunnelRowStatus (pTeCliArgs->u4TunnelIndex,
                                       pTeCliArgs->u4TunnelInstance,
                                       pTeCliArgs->u4IngressId,
                                       pTeCliArgs->u4EgressId,
                                       MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            /* Failure check not handled */
        }

        CliPrintf (CliHandle, "\r\n%%Unable to create MPLS Tnl Interface\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetMplsTunnelIsIf (pTeCliArgs->u4TunnelIndex,
                              pTeCliArgs->u4TunnelInstance,
                              pTeCliArgs->u4IngressId,
                              pTeCliArgs->u4EgressId,
                              TE_SNMP_TRUE) == SNMP_FAILURE)
    {
        if (nmhSetMplsTunnelRowStatus (pTeCliArgs->u4TunnelIndex,
                                       pTeCliArgs->u4TunnelInstance,
                                       pTeCliArgs->u4IngressId,
                                       pTeCliArgs->u4EgressId,
                                       MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            /* Failure check not handled */
        }

        CliPrintf (CliHandle, "\r\n%%Unable to create MPLS Tnl Interface\r\n");
        return CLI_FAILURE;
    }

    /* Set Tunnel Name */
    if (nmhTestv2MplsTunnelName (&u4ErrorCode,
                                 pTeCliArgs->u4TunnelIndex,
                                 pTeCliArgs->u4TunnelInstance,
                                 pTeCliArgs->u4IngressId,
                                 pTeCliArgs->u4EgressId,
                                 &TunnelName) == SNMP_FAILURE)
    {
        if (nmhSetMplsTunnelRowStatus (pTeCliArgs->u4TunnelIndex,
                                       pTeCliArgs->u4TunnelInstance,
                                       pTeCliArgs->u4IngressId,
                                       pTeCliArgs->u4EgressId,
                                       MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            /* Failure check not handled */
        }

        CliPrintf (CliHandle, "\r\n%%Unable to set Tunnel Name\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetMplsTunnelName (pTeCliArgs->u4TunnelIndex,
                              pTeCliArgs->u4TunnelInstance,
                              pTeCliArgs->u4IngressId,
                              pTeCliArgs->u4EgressId,
                              &TunnelName) == SNMP_FAILURE)
    {
        if (nmhSetMplsTunnelRowStatus (pTeCliArgs->u4TunnelIndex,
                                       pTeCliArgs->u4TunnelInstance,
                                       pTeCliArgs->u4IngressId,
                                       pTeCliArgs->u4EgressId,
                                       MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            /* Failure check not handled */
        }

        CliPrintf (CliHandle, "\r\n%%Unable to set Tunnel Name\r\n");
        return CLI_FAILURE;
    }

    if (pTeCliArgs->u1TnlType & MPLS_TE_TNL_TYPE_MPLSTP)
    {
        pTeCliArgs->u4IngressLocalMapNum = pTeCliArgs->u4IngressId;
        pTeCliArgs->u4EgressLocalMapNum = pTeCliArgs->u4EgressId;
    }

    if (pTeCliArgs->u4IngressLocalMapNum == 0)    /* Ingress IP address */
    {
        /* Setting the Role for the Tunnel */
        i4TnlRole =
            (NetIpv4IfIsOurAddress (pTeCliArgs->u4IngressId) == NETIPV4_SUCCESS)
            ? TE_INGRESS : ((NetIpv4IfIsOurAddress (pTeCliArgs->u4EgressId)
                             == NETIPV4_SUCCESS) ? TE_EGRESS : TE_INTERMEDIATE);
    }
    else
    {
        /* Determine the tunnel role based on the node map table */
        if (TeCliGetLocalMapNum (u4ContextId, &u4LocalMapNum) == CLI_FAILURE)
        {
            if (nmhSetMplsTunnelRowStatus (pTeCliArgs->u4TunnelIndex,
                                           pTeCliArgs->u4TunnelInstance,
                                           pTeCliArgs->u4IngressId,
                                           pTeCliArgs->u4EgressId,
                                           MPLS_STATUS_DESTROY) == SNMP_FAILURE)
            {
                /* Failure check not handled */
            }

            CliPrintf (CliHandle, "\r%% Global Or Node Identifier is not "
                       "configured\n");
            return CLI_FAILURE;
        }

        if (u4LocalMapNum == pTeCliArgs->u4IngressLocalMapNum)
        {
            i4TnlRole = TE_INGRESS;
        }
        else if (u4LocalMapNum == pTeCliArgs->u4EgressLocalMapNum)
        {
            i4TnlRole = TE_EGRESS;
        }
        else
        {
            i4TnlRole = TE_INTERMEDIATE;
        }
    }
    if (nmhTestv2MplsTunnelRole (&u4ErrorCode,
                                 pTeCliArgs->u4TunnelIndex,
                                 pTeCliArgs->u4TunnelInstance,
                                 pTeCliArgs->u4IngressId,
                                 pTeCliArgs->u4EgressId,
                                 i4TnlRole) == SNMP_SUCCESS)
    {
        if (nmhSetMplsTunnelRole (pTeCliArgs->u4TunnelIndex,
                                  pTeCliArgs->u4TunnelInstance,
                                  pTeCliArgs->u4IngressId,
                                  pTeCliArgs->u4EgressId,
                                  i4TnlRole) != SNMP_SUCCESS)
        {
            if (nmhSetMplsTunnelRowStatus (pTeCliArgs->u4TunnelIndex,
                                           pTeCliArgs->u4TunnelInstance,
                                           pTeCliArgs->u4IngressId,
                                           pTeCliArgs->u4EgressId,
                                           MPLS_STATUS_DESTROY) == SNMP_FAILURE)
            {
                /* Failure check not handled */
            }

            CliPrintf (CliHandle, "\r\n%%Unable to set Tunnel role\r\n");
            return CLI_FAILURE;
        }
    }
    else
    {
        if (nmhSetMplsTunnelRowStatus (pTeCliArgs->u4TunnelIndex,
                                       pTeCliArgs->u4TunnelInstance,
                                       pTeCliArgs->u4IngressId,
                                       pTeCliArgs->u4EgressId,
                                       MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            /* Failure check not handled */
        }

        CliPrintf (CliHandle, "\r\n%%Unable to set Tunnel role\r\n");
        return CLI_FAILURE;
    }

    if (pTeCliArgs->u4IngressLocalMapNum != 0)
    {
        au1LSRIdMapInfo[0] |= TE_TNL_INGRESSID_MAP_INFO;
    }
    if (pTeCliArgs->u4EgressLocalMapNum != 0)
    {
        au1LSRIdMapInfo[0] |= TE_TNL_EGRESSID_MAP_INFO;
    }

    if (nmhTestv2FsMplsTunnelLSRIdMapInfo (&u4ErrorCode,
                                           pTeCliArgs->u4TunnelIndex,
                                           pTeCliArgs->u4TunnelInstance,
                                           pTeCliArgs->u4IngressId,
                                           pTeCliArgs->u4EgressId,
                                           &LSRIdMapInfo) != SNMP_SUCCESS)
    {
        if (nmhSetMplsTunnelRowStatus (pTeCliArgs->u4TunnelIndex,
                                       pTeCliArgs->u4TunnelInstance,
                                       pTeCliArgs->u4IngressId,
                                       pTeCliArgs->u4EgressId,
                                       MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            /* Failure check not handled */
        }

        CliPrintf (CliHandle,
                   "\r\n%%Unable to set LSR ID mapping information \r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsTunnelLSRIdMapInfo (pTeCliArgs->u4TunnelIndex,
                                        pTeCliArgs->u4TunnelInstance,
                                        pTeCliArgs->u4IngressId,
                                        pTeCliArgs->u4EgressId,
                                        &LSRIdMapInfo) != SNMP_SUCCESS)
    {
        if (nmhSetMplsTunnelRowStatus (pTeCliArgs->u4TunnelIndex,
                                       pTeCliArgs->u4TunnelInstance,
                                       pTeCliArgs->u4IngressId,
                                       pTeCliArgs->u4EgressId,
                                       MPLS_STATUS_DESTROY) == SNMP_FAILURE)
        {
            /* Failure check not handled */
        }

        CliPrintf (CliHandle,
                   "\r\n%%Unable to set LSR ID mapping information \r\n");
        return CLI_FAILURE;
    }

    /* For Protection LSP, this i4TnlIfIndex values is Filled by the caller
     * with the Working LSP's Tunnel Interface Index.
     * For the Protection LSP, the MIB object should not be populated.
     * Only for Working LSP this MIB object should be populated.
     */
    if ((pTeCliArgs->i4TnlIfIndex == TE_ZERO) &&
        (pTeCliArgs->u4TunnelInstance == TE_ZERO))
    {
        i4MplsTnlIfIndex = (INT4) gaTnlCliArgs[CliHandle].u4MplsTnlIfIndex;
        if (nmhTestv2FsMplsTunnelIfIndex (&u4ErrorCode,
                                          pTeCliArgs->u4TunnelIndex,
                                          pTeCliArgs->u4TunnelInstance,
                                          pTeCliArgs->u4IngressId,
                                          pTeCliArgs->u4EgressId,
                                          i4MplsTnlIfIndex) != SNMP_SUCCESS)
        {
            if (nmhSetMplsTunnelRowStatus (pTeCliArgs->u4TunnelIndex,
                                           pTeCliArgs->u4TunnelInstance,
                                           pTeCliArgs->u4IngressId,
                                           pTeCliArgs->u4EgressId,
                                           MPLS_STATUS_DESTROY) == SNMP_FAILURE)
            {
                /* Failure check not handled */
            }

            CliPrintf (CliHandle,
                       "\r\n%%Tunnel If Index is already associated with another "
                       "tunnel \r\n");
            return CLI_FAILURE;
        }

        if (nmhSetFsMplsTunnelIfIndex (pTeCliArgs->u4TunnelIndex,
                                       pTeCliArgs->u4TunnelInstance,
                                       pTeCliArgs->u4IngressId,
                                       pTeCliArgs->u4EgressId,
                                       i4MplsTnlIfIndex) != SNMP_SUCCESS)
        {
            if (nmhSetMplsTunnelRowStatus (pTeCliArgs->u4TunnelIndex,
                                           pTeCliArgs->u4TunnelInstance,
                                           pTeCliArgs->u4IngressId,
                                           pTeCliArgs->u4EgressId,
                                           MPLS_STATUS_DESTROY) == SNMP_FAILURE)
            {
                /* Failure check not handled */
            }

            CliPrintf (CliHandle,
                       "\r\n%%Unable to set MplsTunnel Interface Index \r\n");
            return CLI_FAILURE;
        }
    }

    MplsTeTnlType.pu1_OctetList[TE_ZERO] = pTeCliArgs->u1TnlType;

    if (pTeCliArgs->u1TnlType != TE_ZERO)
    {
        if (nmhTestv2FsMplsTunnelType (&u4ErrorCode,
                                       pTeCliArgs->u4TunnelIndex,
                                       pTeCliArgs->u4TunnelInstance,
                                       pTeCliArgs->u4IngressId,
                                       pTeCliArgs->u4EgressId,
                                       &MplsTeTnlType) == SNMP_FAILURE)
        {
            if (nmhSetMplsTunnelRowStatus (pTeCliArgs->u4TunnelIndex,
                                           pTeCliArgs->u4TunnelInstance,
                                           pTeCliArgs->u4IngressId,
                                           pTeCliArgs->u4EgressId,
                                           MPLS_STATUS_DESTROY) == SNMP_FAILURE)
            {
                /* Failure check not handled */
            }

            CliPrintf (CliHandle, "\r\n%%Not able to set tunnel type\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetFsMplsTunnelType (pTeCliArgs->u4TunnelIndex,
                                    pTeCliArgs->u4TunnelInstance,
                                    pTeCliArgs->u4IngressId,
                                    pTeCliArgs->u4EgressId,
                                    &MplsTeTnlType) == SNMP_FAILURE)
        {
            if (nmhSetMplsTunnelRowStatus (pTeCliArgs->u4TunnelIndex,
                                           pTeCliArgs->u4TunnelInstance,
                                           pTeCliArgs->u4IngressId,
                                           pTeCliArgs->u4EgressId,
                                           MPLS_STATUS_DESTROY) == SNMP_FAILURE)
            {
                /* Failure check not handled */
            }

            CliPrintf (CliHandle, "\r\n%%Not able to set tunnel type\r\n");
            return CLI_FAILURE;
        }
    }

    if (pTeCliArgs->u1TnlPathType != TE_ZERO)
    {
        if (nmhTestv2FsMplsTunnelIsProtectingLsp (&u4ErrorCode,
                                                  pTeCliArgs->u4TunnelIndex,
                                                  pTeCliArgs->u4TunnelInstance,
                                                  pTeCliArgs->u4IngressId,
                                                  pTeCliArgs->u4EgressId,
                                                  (INT4) pTeCliArgs->
                                                  u1TnlPathType) ==
            SNMP_FAILURE)
        {
            if (nmhSetMplsTunnelRowStatus (pTeCliArgs->u4TunnelIndex,
                                           pTeCliArgs->u4TunnelInstance,
                                           pTeCliArgs->u4IngressId,
                                           pTeCliArgs->u4EgressId,
                                           MPLS_STATUS_DESTROY) == SNMP_FAILURE)
            {
                /* Failure check not handled */
            }

            CliPrintf (CliHandle, "\r\n%%Not able to set tunnel path type\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetFsMplsTunnelIsProtectingLsp (pTeCliArgs->u4TunnelIndex,
                                               pTeCliArgs->u4TunnelInstance,
                                               pTeCliArgs->u4IngressId,
                                               pTeCliArgs->u4EgressId,
                                               (INT4) pTeCliArgs->u1TnlPathType)
            == SNMP_FAILURE)
        {
            if (nmhSetMplsTunnelRowStatus (pTeCliArgs->u4TunnelIndex,
                                           pTeCliArgs->u4TunnelInstance,
                                           pTeCliArgs->u4IngressId,
                                           pTeCliArgs->u4EgressId,
                                           MPLS_STATUS_DESTROY) == SNMP_FAILURE)
            {
                /* Failure check not handled */
            }

            CliPrintf (CliHandle, "\r\n%%Not able to set tunnel path type\r\n");
            return CLI_FAILURE;
        }
    }

    if (pTeCliArgs->i4TnlMode != TE_ZERO)
    {
        if (nmhTestv2FsMplsTunnelMode (&u4ErrorCode,
                                       pTeCliArgs->u4TunnelIndex,
                                       pTeCliArgs->u4TunnelInstance,
                                       pTeCliArgs->u4IngressId,
                                       pTeCliArgs->u4EgressId,
                                       pTeCliArgs->i4TnlMode) == SNMP_FAILURE)
        {
            if (nmhSetMplsTunnelRowStatus (pTeCliArgs->u4TunnelIndex,
                                           pTeCliArgs->u4TunnelInstance,
                                           pTeCliArgs->u4IngressId,
                                           pTeCliArgs->u4EgressId,
                                           MPLS_STATUS_DESTROY) == SNMP_FAILURE)
            {
                /* Failure check not handled */
            }

            CliPrintf (CliHandle, "\r\n%%Not able to set tunnel mode\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetFsMplsTunnelMode (pTeCliArgs->u4TunnelIndex,
                                    pTeCliArgs->u4TunnelInstance,
                                    pTeCliArgs->u4IngressId,
                                    pTeCliArgs->u4EgressId,
                                    pTeCliArgs->i4TnlMode) == SNMP_FAILURE)
        {
            if (nmhSetMplsTunnelRowStatus (pTeCliArgs->u4TunnelIndex,
                                           pTeCliArgs->u4TunnelInstance,
                                           pTeCliArgs->u4IngressId,
                                           pTeCliArgs->u4EgressId,
                                           MPLS_STATUS_DESTROY) == SNMP_FAILURE)
            {
                /* Failure check Not handled */
            }

            CliPrintf (CliHandle, "\r\n%%Not able to set tunnel mode\r\n");
            return CLI_FAILURE;
        }
    }

    if (pTeCliArgs->u1SwitchingType != TE_ZERO)
    {
        if (nmhTestv2GmplsTunnelSwitchingType (&u4ErrorCode,
                                               pTeCliArgs->u4TunnelIndex,
                                               pTeCliArgs->u4TunnelInstance,
                                               pTeCliArgs->u4IngressId,
                                               pTeCliArgs->u4EgressId,
                                               (INT4) pTeCliArgs->
                                               u1SwitchingType) == SNMP_FAILURE)
        {
            if (nmhSetMplsTunnelRowStatus (pTeCliArgs->u4TunnelIndex,
                                           pTeCliArgs->u4TunnelInstance,
                                           pTeCliArgs->u4IngressId,
                                           pTeCliArgs->u4EgressId,
                                           MPLS_STATUS_DESTROY) == SNMP_FAILURE)
            {
                /* Failure check not handled */
            }

            CliPrintf (CliHandle, "\r\n%%Not able to set switching type\r\n");
            return CLI_FAILURE;

        }

        if (nmhSetGmplsTunnelSwitchingType (pTeCliArgs->u4TunnelIndex,
                                            pTeCliArgs->u4TunnelInstance,
                                            pTeCliArgs->u4IngressId,
                                            pTeCliArgs->u4EgressId,
                                            (INT4) pTeCliArgs->
                                            u1SwitchingType) == SNMP_FAILURE)
        {
            if (nmhSetMplsTunnelRowStatus (pTeCliArgs->u4TunnelIndex,
                                           pTeCliArgs->u4TunnelInstance,
                                           pTeCliArgs->u4IngressId,
                                           pTeCliArgs->u4EgressId,
                                           MPLS_STATUS_DESTROY) == SNMP_FAILURE)
            {
                /* Failure check not handled */
            }

            CliPrintf (CliHandle, "\r\n%%Not able to set switching type\r\n");
            return CLI_FAILURE;
        }

    }

    if (pTeCliArgs->u1EncodingType != TE_ZERO)
    {
        if (nmhTestv2GmplsTunnelLSPEncoding (&u4ErrorCode,
                                             pTeCliArgs->u4TunnelIndex,
                                             pTeCliArgs->u4TunnelInstance,
                                             pTeCliArgs->u4IngressId,
                                             pTeCliArgs->u4EgressId,
                                             (INT4) pTeCliArgs->u1EncodingType)
            == SNMP_FAILURE)
        {
            if (nmhSetMplsTunnelRowStatus (pTeCliArgs->u4TunnelIndex,
                                           pTeCliArgs->u4TunnelInstance,
                                           pTeCliArgs->u4IngressId,
                                           pTeCliArgs->u4EgressId,
                                           MPLS_STATUS_DESTROY) == SNMP_FAILURE)
            {
                /* Failure check not handled */
            }

            CliPrintf (CliHandle, "\r\n%%Not able to set encoding type\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetGmplsTunnelLSPEncoding (pTeCliArgs->u4TunnelIndex,
                                          pTeCliArgs->u4TunnelInstance,
                                          pTeCliArgs->u4IngressId,
                                          pTeCliArgs->u4EgressId,
                                          (INT4) pTeCliArgs->u1EncodingType)
            == SNMP_FAILURE)
        {
            if (nmhSetMplsTunnelRowStatus (pTeCliArgs->u4TunnelIndex,
                                           pTeCliArgs->u4TunnelInstance,
                                           pTeCliArgs->u4IngressId,
                                           pTeCliArgs->u4EgressId,
                                           MPLS_STATUS_DESTROY) == SNMP_FAILURE)
            {
                /* Failure check not handled */
            }

            CliPrintf (CliHandle, "\r\n%%Not able to set encoding type\r\n");
            return CLI_FAILURE;
        }
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : TeCliDeleteTunnel
 * Description   : This routine is used to delete the tunnel corresponding to
 *                 the MplsTunnelId. 
 * Input(s)      : CliHandle      - Cli Context Handle
 *                 pTeCliArgs       - Pointer to TE Cli Args containing
 *                                    required parameters for tunnel
 *                 bValidateLsrId   - LSR ID validation. To be skipped for 
 *                                    P2MP tunnel
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 *****************************************************************************/
INT4
TeCliDeleteTunnel (tCliHandle CliHandle, tTeCliArgs * pTeCliArgs,
                   BOOL1 bValidateLsrId)
{
    tSNMP_OID_TYPE      GetResourcePointer;
    static UINT4        au4GetRsrcPointer[MPLS_TE_RSRC_TABLE_MAX_RATE_OFFSET];

    tSNMP_OCTET_STRING_TYPE InSegmentIndex;
    tSNMP_OID_TYPE      XCIndex;
    tSNMP_OCTET_STRING_TYPE OutSegmentIndex;
    tSNMP_OCTET_STRING_TYPE XCSegmentIndex;
    tSNMP_OCTET_STRING_TYPE RetValAdminStatusStr;
    static UINT1        au1InSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1OutSegIndex[MPLS_INDEX_LENGTH];
    static UINT4        au4XCIndex[MPLS_TE_XC_TABLE_OFFSET];
    static UINT1        au1XCSegIndex[MPLS_INDEX_LENGTH];
    static UINT1        au1RetValAdminStatus[4];

    BOOL1               bLoop = FALSE;
    UINT4               u4TunnelResourceIndex = 0;
    UINT4               u4ErrorCode = 0;

    UINT4               u4InIndex = 0;
    UINT4               u4OutIndex = 0;
    UINT4               u4XCInd = 0;
    UINT4               u4FwdL3Intf = 0;
    UINT4               u4RevL3Intf = 0;
    UINT4               u4HopNum = TE_ZERO;
    UINT4               u4HopListIndex = TE_ZERO;
    UINT4               u4HopPathOptionIndex = TE_ZERO;
    INT4                i4TunnelMode = 0;
    INT4                i4FwdMplsTnlIf = 0;
    INT4                i4RevMplsTnlIf = 0;
    INT4                i4PathComp = 0;
    /* STATIC_HLSP */
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTnlInfo         *pTeHlspTnlInfo = NULL;
    UINT4               u4HlspTunnelIndex = 0;
    UINT4               u4HlspTunnelInstance = 0;
    UINT4               u4HlspIngressId = 0;
    UINT4               u4HlspEgressId = 0;
    UINT4               u4Operation = 0;
    UINT4               u4RowStatus = 0;
    BOOL1               b1Flag = MPLS_TRUE;
    tHlspTnlCliArgs     HlspTnlCliArgs;
    tteFsMplsLSPMapTunnelTable teSetFsMplsLSPMapTunnelTable;
    tteIsSetFsMplsLSPMapTunnelTable teIsSetFsMplsLSPMapTunnelTable;

    GetResourcePointer.pu4_OidList = au4GetRsrcPointer;
    MEMSET (GetResourcePointer.pu4_OidList, 0, sizeof (UINT4) * 2);
    GetResourcePointer.u4_Length = 0;
    InSegmentIndex.pu1_OctetList = au1InSegIndex;
    InSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    OutSegmentIndex.pu1_OctetList = au1OutSegIndex;
    OutSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    XCSegmentIndex.pu1_OctetList = au1XCSegIndex;
    XCSegmentIndex.i4_Length = MPLS_INDEX_LENGTH;
    XCIndex.pu4_OidList = au4XCIndex;
    XCIndex.u4_Length = 0;
    MEMSET (&HlspTnlCliArgs, 0, sizeof (tHlspTnlCliArgs));
    MEMSET (&teSetFsMplsLSPMapTunnelTable, 0,
            sizeof (tteFsMplsLSPMapTunnelTable));
    MEMSET (&teIsSetFsMplsLSPMapTunnelTable, 0,
            sizeof (tteIsSetFsMplsLSPMapTunnelTable));

    RetValAdminStatusStr.pu1_OctetList = au1RetValAdminStatus;
    if (OSIX_TRUE == bValidateLsrId)
    {
        if (TeValidateAndGetIngressId (CliHandle, pTeCliArgs->u4EgressId,
                                       pTeCliArgs->u4EgressLocalMapNum,
                                       &pTeCliArgs->u4IngressId,
                                       &pTeCliArgs->u4IngressLocalMapNum))
        {
            return CLI_FAILURE;
        }
    }

    if (pTeCliArgs->u4AdminStatus != TE_ZERO)
    {
        MPLS_INTEGER_TO_OCTETSTRING (pTeCliArgs->u4AdminStatus,
                                     (&RetValAdminStatusStr));

        if (nmhTestv2GmplsTunnelAdminStatusFlags (&u4ErrorCode,
                                                  pTeCliArgs->u4TunnelIndex,
                                                  pTeCliArgs->u4TunnelInstance,
                                                  pTeCliArgs->u4IngressId,
                                                  pTeCliArgs->u4EgressId,
                                                  &RetValAdminStatusStr) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%no  value is supported \r\n");
            return CLI_FAILURE;
        }

        if (nmhSetGmplsTunnelAdminStatusFlags (pTeCliArgs->u4TunnelIndex,
                                               pTeCliArgs->u4TunnelInstance,
                                               pTeCliArgs->u4IngressId,
                                               pTeCliArgs->u4EgressId,
                                               &RetValAdminStatusStr) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%no  value is supported \r\n");
            return CLI_FAILURE;
        }
    }

    if (nmhTestv2MplsTunnelRowStatus (&u4ErrorCode,
                                      pTeCliArgs->u4TunnelIndex,
                                      pTeCliArgs->u4TunnelInstance,
                                      pTeCliArgs->u4IngressId,
                                      pTeCliArgs->u4EgressId,
                                      TE_DESTROY) == SNMP_FAILURE)
    {
        TE_DBG2 (TE_MAIN_ETEXT,
                 "\nIn Func: TeCliDeleteTunnel Tunnel Index:%d Tunnel Instance:%d\n",
                 pTeCliArgs->u4TunnelIndex, pTeCliArgs->u4TunnelInstance);

        CliPrintf (CliHandle, "\r%% Tunnel is associated with FTN/LDP/PW/BFD."
                   " Remove the association and then delete the tunnel.\n");
        return CLI_FAILURE;
    }

    if (nmhGetMplsTunnelXCPointer (pTeCliArgs->u4TunnelIndex,
                                   pTeCliArgs->u4TunnelInstance,
                                   pTeCliArgs->u4IngressId,
                                   pTeCliArgs->u4EgressId,
                                   &XCIndex) == SNMP_SUCCESS)
    {
        MplsGetSegFromTunnelXC (&XCIndex, &XCSegmentIndex, &InSegmentIndex,
                                &OutSegmentIndex, &u4XCInd, &u4InIndex,
                                &u4OutIndex);
    }
    else
    {
        CliPrintf (CliHandle, "\r\n%%TunnelXCPointer is not available.\r\n");
        CliPrintf (CliHandle,
                   "\r%% InSegment, OutSegment and XC Entries can't be deleted. \r\n");
        return CLI_FAILURE;
    }
    /* STATIC_HLSP */
    /* If the Map Tunnel Info is not NULL and the tunnel is not stitched, the 
       tunnel is stacked to HLSP */
    /* HLSP Params needs to be deleted if the service tunnel is deleted */
    pTeTnlInfo = TeGetTunnelInfo (pTeCliArgs->u4TunnelIndex,
                                  pTeCliArgs->u4TunnelInstance,
                                  pTeCliArgs->u4IngressId,
                                  pTeCliArgs->u4EgressId);
    if (pTeTnlInfo == NULL)
    {
        return CLI_FAILURE;
    }

    /* Check whether the tunnel is service LSP or e2e LSP and remove the 
     * association (stacking/stitching) before deleting it.*/
    if ((TE_MAP_TNL_INFO (pTeTnlInfo) != NULL) &&
        (TE_TNL_TYPE (pTeTnlInfo) != TE_TNL_TYPE_SLSP) &&
        (TE_TNL_TYPE (pTeTnlInfo) != TE_TNL_TYPE_HLSP) &&
        (pTeCliArgs->u4TunnelInstance != TE_ZERO))
    {
        /* Get the H-LSP/S-LSP Tunnel Indices */
        pTeHlspTnlInfo = TE_MAP_TNL_INFO (pTeTnlInfo);

        u4HlspTunnelIndex = TE_TNL_TNL_INDEX (pTeHlspTnlInfo);
        u4HlspTunnelInstance = TE_TNL_TNL_INSTANCE (pTeHlspTnlInfo);

        MEMCPY ((UINT1 *) (&u4HlspIngressId),
                TE_TNL_INGRESS_LSRID (pTeHlspTnlInfo), ROUTER_ID_LENGTH);
        MEMCPY ((UINT1 *) (&u4HlspEgressId),
                TE_TNL_EGRESS_LSRID (pTeHlspTnlInfo), ROUTER_ID_LENGTH);

        u4HlspIngressId = OSIX_NTOHL (u4HlspIngressId);
        u4HlspEgressId = OSIX_NTOHL (u4HlspEgressId);

        /* Update the fsMplsLSPMapTunnelTable with RS as Destroy  and the 
         * corresponding operation.*/
        if (TE_TNL_TYPE (pTeHlspTnlInfo) & TE_TNL_TYPE_HLSP)
        {
            u4Operation = TE_OPER_STACK;
        }
        if (TE_TNL_TYPE (pTeHlspTnlInfo) & TE_TNL_TYPE_SLSP)
        {
            u4Operation = TE_OPER_STITCH;
        }

        u4RowStatus = TE_DESTROY;
        MEMCPY (&HlspTnlCliArgs.teSetFsMplsLSPMapTunnelTable,
                &teSetFsMplsLSPMapTunnelTable,
                sizeof (tteFsMplsLSPMapTunnelTable));
        MEMCPY (&HlspTnlCliArgs.teIsSetFsMplsLSPMapTunnelTable,
                &teIsSetFsMplsLSPMapTunnelTable,
                sizeof (teIsSetFsMplsLSPMapTunnelTable));
        HlspTnlCliArgs.teSetFsMplsLSPMapTunnelTable =
            teSetFsMplsLSPMapTunnelTable;
        HlspTnlCliArgs.teIsSetFsMplsLSPMapTunnelTable =
            teIsSetFsMplsLSPMapTunnelTable;
        HlspTnlCliArgs.u4SetHlspTnlIndex = u4HlspTunnelIndex;
        HlspTnlCliArgs.u4SetHlspTnlInstance = u4HlspTunnelInstance;
        HlspTnlCliArgs.u4SetHlspTnlIngressId = u4HlspIngressId;
        HlspTnlCliArgs.u4SetHlspTnlEgressId = u4HlspEgressId;
        HlspTnlCliArgs.u4IsSetHlspTnlIndex = pTeCliArgs->u4TunnelIndex;
        HlspTnlCliArgs.u4IsSetHlspTnlInstance = pTeCliArgs->u4TunnelInstance;
        HlspTnlCliArgs.u4IsSetHlspTnlIngressId = pTeCliArgs->u4IngressId;
        HlspTnlCliArgs.u4IsSetHlspTnlEgressId = pTeCliArgs->u4EgressId;
        HlspTnlCliArgs.u4Operation = u4Operation;
        HlspTnlCliArgs.u4RowStatus = u4RowStatus;

        TeFillFsMplsLspMapTnlTable (&HlspTnlCliArgs);

        /* Delete the HLSP Params */
        if (TeTestAllFsMplsLSPMapTunnelTable
            (&u4ErrorCode, &HlspTnlCliArgs.teSetFsMplsLSPMapTunnelTable,
             &HlspTnlCliArgs.teIsSetFsMplsLSPMapTunnelTable, OSIX_TRUE,
             OSIX_FALSE) == SNMP_FAILURE)
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_LSP_MAP);
            return CLI_FAILURE;
        }

        TeSetAllFsMplsLSPMapTunnelTable
            (&HlspTnlCliArgs.teSetFsMplsLSPMapTunnelTable,
             &HlspTnlCliArgs.teIsSetFsMplsLSPMapTunnelTable, OSIX_TRUE,
             OSIX_TRUE);
    }

    if ((pTeCliArgs->u4TunnelInstance != TE_ZERO) &&
        (TeCliDeleteXcInAndOutSegment (&XCSegmentIndex, &InSegmentIndex,
                                       &OutSegmentIndex, u4XCInd, u4InIndex,
                                       u4OutIndex, &u4FwdL3Intf,
                                       &i4FwdMplsTnlIf, pTeCliArgs)
         == CLI_FAILURE))
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to delete the Forward XC Information\r\n");
        return CLI_FAILURE;
    }

    if (nmhGetFsMplsTunnelMode (pTeCliArgs->u4TunnelIndex,
                                pTeCliArgs->u4TunnelInstance,
                                pTeCliArgs->u4IngressId,
                                pTeCliArgs->u4EgressId, &i4TunnelMode)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to get Tunnel Mode\r\n");
        return CLI_FAILURE;
    }

    if (i4TunnelMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL)
    {
        if (nmhGetNextIndexMplsXCTable (&XCSegmentIndex, &XCSegmentIndex,
                                        &InSegmentIndex, &InSegmentIndex,
                                        &OutSegmentIndex, &OutSegmentIndex)
            == SNMP_SUCCESS)
        {
            MPLS_OCTETSTRING_TO_INTEGER ((&XCSegmentIndex), u4XCInd);
            MPLS_OCTETSTRING_TO_INTEGER ((&InSegmentIndex), u4InIndex);
            MPLS_OCTETSTRING_TO_INTEGER ((&OutSegmentIndex), u4OutIndex);

            if ((pTeCliArgs->u4TunnelInstance != TE_ZERO) &&
                (TeCliDeleteXcInAndOutSegment (&XCSegmentIndex, &InSegmentIndex,
                                               &OutSegmentIndex, u4XCInd,
                                               u4InIndex, u4OutIndex,
                                               &u4RevL3Intf, &i4RevMplsTnlIf,
                                               pTeCliArgs) == CLI_FAILURE))
            {
                CliPrintf (CliHandle,
                           "\r\n%%Unable to delete the reverse XC "
                           "information\r\n");
                return CLI_FAILURE;
            }
        }
    }

    /*To retrive Hop table Index */
    if (((nmhGetMplsTunnelHopTableIndex (pTeCliArgs->u4TunnelIndex,
                                         pTeCliArgs->u4TunnelInstance,
                                         pTeCliArgs->u4IngressId,
                                         pTeCliArgs->u4EgressId,
                                         &u4HopListIndex)) == SNMP_SUCCESS) &&
        ((nmhGetMplsTunnelPathInUse (pTeCliArgs->u4TunnelIndex,
                                     pTeCliArgs->u4TunnelInstance,
                                     pTeCliArgs->u4IngressId,
                                     pTeCliArgs->u4EgressId,
                                     &u4HopPathOptionIndex)) == SNMP_SUCCESS))
    {
        if ((u4HopListIndex != TE_ZERO) && (u4HopPathOptionIndex != TE_ZERO))
        {
            bLoop = TRUE;
        }
    }

    /* Destroy the Tunnel Resource  entry */
    if ((nmhGetMplsTunnelResourcePointer (pTeCliArgs->u4TunnelIndex,
                                          pTeCliArgs->u4TunnelInstance,
                                          pTeCliArgs->u4IngressId,
                                          pTeCliArgs->u4EgressId,
                                          &GetResourcePointer)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%TunnelResourcePointer is not available\r\n");
        return CLI_FAILURE;
    }
    if (nmhTestv2MplsTunnelRowStatus (&u4ErrorCode,
                                      pTeCliArgs->u4TunnelIndex,
                                      pTeCliArgs->u4TunnelInstance,
                                      pTeCliArgs->u4IngressId,
                                      pTeCliArgs->u4EgressId,
                                      MPLS_STATUS_DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Tunnel entry deletion failed.\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetMplsTunnelRowStatus (pTeCliArgs->u4TunnelIndex,
                                   pTeCliArgs->u4TunnelInstance,
                                   pTeCliArgs->u4IngressId,
                                   pTeCliArgs->u4EgressId,
                                   MPLS_STATUS_DESTROY) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Tunnel entry deletion failed.\r\n");
        return CLI_FAILURE;
    }

    if (i4FwdMplsTnlIf != 0)
    {
        if (i4FwdMplsTnlIf == pTeCliArgs->i4TnlIfIndex)
        {
            b1Flag = MPLS_FALSE;
        }
        else
        {
            b1Flag = MPLS_TRUE;
        }

        if (MplsDeleteMplsIfOrMplsTnlIf (u4FwdL3Intf, (UINT4) i4FwdMplsTnlIf,
                                         CFA_MPLS_TUNNEL, b1Flag)
            == MPLS_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Forward Mpls Tunnel Interface deletion "
                       "failed.\r\n");
            return CLI_FAILURE;
        }

    }

    if (i4RevMplsTnlIf != 0)
    {
        if (i4RevMplsTnlIf == pTeCliArgs->i4TnlIfIndex)
        {
            b1Flag = MPLS_FALSE;
        }
        else
        {
            b1Flag = MPLS_TRUE;
        }
        if (MplsDeleteMplsIfOrMplsTnlIf (u4RevL3Intf, (UINT4) i4RevMplsTnlIf,
                                         CFA_MPLS_TUNNEL, b1Flag)
            == MPLS_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%%Reverse Mpls Tunnel Interface deletion "
                       "failed.\r\n");
            return CLI_FAILURE;
        }
    }

    if (((i4FwdMplsTnlIf == 0) && (i4RevMplsTnlIf == 0)) &&
        (pTeTnlInfo->u1TnlSgnlPrtcl == TE_SIGPROTO_NONE))
    {
        pTeCliArgs->bTnlIfIndexFound = FALSE;
    }

    u4TunnelResourceIndex =
        GetResourcePointer.pu4_OidList[MPLS_TE_RSRC_TABLE_MAX_RATE_OFFSET - 1];

    /* Default Resource Index is one, so don't allow to delete it. */
    if ((u4TunnelResourceIndex != TE_ONE) &&
        (GetResourcePointer.u4_Length == TE_TNL_RSRC_TABLE_DEF_OFFSET) &&
        (pTeCliArgs->u4TunnelInstance == TE_ZERO))
    {
        if (nmhTestv2MplsTunnelResourceRowStatus (&u4ErrorCode,
                                                  u4TunnelResourceIndex,
                                                  MPLS_STATUS_DESTROY) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "%%Unable to set MplsTunnelResourceRowStatus - destroy\r\n");
            return CLI_FAILURE;
        }

        if (nmhSetMplsTunnelResourceRowStatus (u4TunnelResourceIndex,
                                               MPLS_STATUS_DESTROY)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "%%Unable to set MplsTunnelResourceRowStatus - destroy\r\n");
            return CLI_FAILURE;
        }
    }
    if (bLoop == TRUE)
    {
        while (u4HopNum++ <= MAX_TE_HOP_PER_PO)
        {

            nmhGetMplsTunnelHopEntryPathComp (u4HopListIndex,
                                              u4HopPathOptionIndex,
                                              u4HopNum, &i4PathComp);
            if (i4PathComp == TE_DYNAMIC)
            {
                break;
            }

            /*Checking whether Rowstatus is valid or not */
            if ((nmhTestv2MplsTunnelHopRowStatus (&u4ErrorCode, u4HopListIndex,
                                                  u4HopPathOptionIndex,
                                                  u4HopNum,
                                                  MPLS_STATUS_DESTROY)) ==
                SNMP_FAILURE)
            {
                continue;
            }
            /*To Set the Rowstatus as Destroy */
            if ((nmhSetMplsTunnelHopRowStatus (u4HopListIndex,
                                               u4HopPathOptionIndex, u4HopNum,
                                               MPLS_STATUS_DESTROY)) ==
                SNMP_FAILURE)
            {
                continue;
            }
        }
    }

    return (CLI_SUCCESS);
}

/******************************************************************************
 * Function Name : TeCliDeleteP2mpOrNormalTunnel
 * Description   : This routine is used to deletes P2MP tunnel or Normal tunnel
 *                 corresponding to MplsTunnelId. 
 * Input(s)      : CliHandle      - Cli Context Handle
 *                 pTeCliArgs       - Pointer to TE Cli Args containing
 *                                    required parameters for tunnel
 *                 bValidateLsrId   - LSR ID validation. To be skipped for 
 *                                    P2MP tunnel
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 *****************************************************************************/
INT4
TeCliDeleteP2mpOrNormalTunnel (tCliHandle CliHandle, tTeCliArgs * pTeCliArgs,
                               BOOL1 bValidateLsrId)
{
    tSNMP_OCTET_STRING_TYPE MplsTeTnlType;
    INT4                i4RetVal = CLI_FAILURE;

    static UINT1        au1TnlType[CLI_MPLS_TE_TNL_TYPE];

    MplsTeTnlType.pu1_OctetList = au1TnlType;
    MplsTeTnlType.i4_Length = CLI_MPLS_TE_TNL_TYPE;

    MEMSET (au1TnlType, TE_ZERO, CLI_MPLS_TE_TNL_TYPE);

    if (nmhGetFsMplsTunnelType (pTeCliArgs->u4TunnelIndex,
                                pTeCliArgs->u4TunnelInstance,
                                pTeCliArgs->u4IngressId,
                                pTeCliArgs->u4EgressId, &MplsTeTnlType)
        != SNMP_SUCCESS)
    {
        CliPrintf (CliHandle, "\r\n%%Failed to retrieve tunnel type\r\n");
        return CLI_FAILURE;
    }

    if (MplsTeTnlType.pu1_OctetList[TE_ZERO] & TE_TNL_TYPE_P2MP)
    {
        i4RetVal = TeCliP2mpTunnelDelete (CliHandle, pTeCliArgs->u4TunnelIndex,
                                          pTeCliArgs->u4TunnelInstance,
                                          pTeCliArgs->u4EgressId,
                                          pTeCliArgs->u4IngressId,
                                          pTeCliArgs->u4IngressLocalMapNum);
    }
    else
    {
        i4RetVal = TeCliDeleteTunnel (CliHandle, pTeCliArgs, bValidateLsrId);
    }

    return i4RetVal;
}

/******************************************************************************
 * Function Name : TeCliNormalCreateNonZeroInstTunnel
 * Description   : This routine is used to create Non-Zero Instance Tunnel
 * Input(s)      : CliHandle          - Cli Context Handle
 *                 u4TunnelIndex      - Uniquely identifies a set of tunnel
 *                                      instances. 
 *                 u4TnlInstance      - Tunnel Instance
 *                 u4IngressId        - Tunnel Ingress Id
 *                 u4EgressId         - Tunnel Egress Id
 *                 u1TnlPathType      - Tnl Path Type - Working or Protection
 * Output(s)     : pu4CreatedInstance - Created Tunnel Instance
 *                 pbEntryCreated     - If Tunnel Entry created now, this 
 *                                      variable returns TRUE, Else, returns
 *                                      FALSE
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 *****************************************************************************/
INT4
TeCliNormalCreateNonZeroInstTunnel (tCliHandle CliHandle, UINT4 u4TunnelIndex,
                                    UINT4 u4TunnelInstance, UINT4 u4IngressId,
                                    UINT4 u4EgressId, UINT1 u1TnlPathType,
                                    UINT4 *pu4CreatedInstance,
                                    BOOL1 * pbEntryCreated)
{
    tSNMP_OCTET_STRING_TYPE MplsTeTnlType;
    UINT1               au1TnlType[CLI_MPLS_TE_TNL_TYPE] = { 0 };

    UINT4               u4CurInstance = TE_ZERO;
    INT4                i4RetVal = TE_ZERO;
    INT4                i4RowStatus = TE_ZERO;
    INT4                i4TnlIfIndex = TE_ZERO;
    INT4                i4TnlMode = TE_ZERO;
    INT4                i4SwitchingCap = TE_ZERO;
    INT4                i4EncodingType = TE_ZERO;
    UINT1               u1TnlType = TE_ZERO;
    tTeCliArgs          TeCliArgs;

    MplsTeTnlType.pu1_OctetList = au1TnlType;
    MplsTeTnlType.i4_Length = CLI_MPLS_TE_TNL_TYPE;

    MEMSET (au1TnlType, TE_ZERO, CLI_MPLS_TE_TNL_TYPE);
    MEMSET (&TeCliArgs, TE_ZERO, sizeof (tTeCliArgs));

    *pbEntryCreated = TRUE;

    if (u1TnlPathType == TE_TNL_WORKING_PATH)
    {
        nmhGetMplsTunnelPrimaryInstance (u4TunnelIndex, u4TunnelInstance,
                                         u4IngressId, u4EgressId,
                                         &u4CurInstance);

        if (u4CurInstance == 0)
        {
            if (nmhGetFsMplsTunnelExtBkpInst (u4TunnelIndex, u4TunnelInstance,
                                              u4IngressId, u4EgressId,
                                              &u4CurInstance) == SNMP_FAILURE)
            {
                return CLI_FAILURE;
            }
        }
        else
        {
            *pbEntryCreated = FALSE;
        }
    }
    else if (u1TnlPathType == TE_TNL_PROTECTION_PATH)
    {
        if (nmhGetFsMplsTunnelExtBkpInst (u4TunnelIndex, u4TunnelInstance,
                                          u4IngressId, u4EgressId,
                                          &u4CurInstance) == SNMP_FAILURE)
        {
            return CLI_FAILURE;
        }

        if (u4CurInstance == 0)
        {
            nmhGetMplsTunnelPrimaryInstance (u4TunnelIndex, u4TunnelInstance,
                                             u4IngressId, u4EgressId,
                                             &u4CurInstance);
        }
        else
        {
            *pbEntryCreated = FALSE;
        }
    }

    nmhGetFsMplsTunnelIfIndex (u4TunnelIndex, u4TunnelInstance,
                               u4IngressId, u4EgressId, &i4TnlIfIndex);

    if (nmhGetMplsTunnelRowStatus (u4TunnelIndex, u4CurInstance, u4IngressId,
                                   u4EgressId, &i4RowStatus) == SNMP_FAILURE)
    {
        TE_DBG (TE_EXTN_FAIL, "\r\n%%Failed to get tunnel information\r\n");
    }

    if ((*pbEntryCreated == FALSE) && (i4RowStatus != TE_ZERO))
    {
        *pu4CreatedInstance = u4CurInstance;
        return CLI_SUCCESS;
    }

    if (nmhGetFsMplsTunnelType (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                u4EgressId, &MplsTeTnlType) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    u1TnlType = MplsTeTnlType.pu1_OctetList[TE_ZERO];

    if (nmhGetFsMplsTunnelMode (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                u4EgressId, &i4TnlMode) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    nmhGetGmplsTunnelSwitchingType (u4TunnelIndex, u4TunnelInstance,
                                    u4IngressId, u4EgressId, &i4SwitchingCap);

    nmhGetGmplsTunnelLSPEncoding (u4TunnelIndex, u4TunnelInstance, u4IngressId,
                                  u4EgressId, &i4EncodingType);

    u4TunnelInstance = u4CurInstance + 1;

    TeCliArgs.u4TunnelIndex = u4TunnelIndex;
    TeCliArgs.u4TunnelInstance = u4TunnelInstance;
    TeCliArgs.u4IngressId = u4IngressId;
    TeCliArgs.u4EgressId = u4EgressId;
    TeCliArgs.u4IngressLocalMapNum = TE_ZERO;
    TeCliArgs.u4EgressLocalMapNum = TE_ZERO;
    TeCliArgs.u1TnlType = u1TnlType;
    TeCliArgs.u1TnlPathType = u1TnlPathType;
    TeCliArgs.i4TnlMode = i4TnlMode;
    TeCliArgs.i4TnlIfIndex = i4TnlIfIndex;
    TeCliArgs.u1SwitchingType = (UINT1) i4SwitchingCap;
    TeCliArgs.u1EncodingType = (UINT1) i4EncodingType;

    i4RetVal = TeCliCreateTunnel (CliHandle, &TeCliArgs);

    *pu4CreatedInstance = u4TunnelInstance;

    if (i4RetVal == CLI_SUCCESS)
    {
        *pbEntryCreated = TRUE;
    }

    return i4RetVal;
}

/******************************************************************************
 * Function Name : TeCliCheckAndDelNonZeroInstTnl
 * Description   : This routine checks and deletes non-zero instance tunnel
 * Input(s)      : CliHandle          - Cli Context Handle
 *                 u4TunnelIndex      - Uniquely identifies a set of tunnel
 *                                      instances. 
 *                 u4TnlInstance      - Tunnel Instance
 *                 u4IngressId        - Tunnel Ingress Id
 *                 u4EgressId         - Tunnel Egress Id
 *                 bInstance0Present  - Instance 0 Present or Not
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 *****************************************************************************/
INT4
TeCliCheckAndDelNonZeroInstTnl (tCliHandle CliHandle, UINT4 u4TunnelIndex,
                                UINT4 u4TunnelInstance, UINT4 u4IngressId,
                                UINT4 u4EgressId, BOOL1 bInstance0Present)
{
    UINT4               u4PrimaryInstance = TE_ZERO;
    tTeCliArgs          TeCliArgs;

    MEMSET (&TeCliArgs, TE_ZERO, sizeof (tTeCliArgs));

    TeCliArgs.u4TunnelIndex = u4TunnelIndex;
    TeCliArgs.u4TunnelInstance = u4TunnelInstance;
    TeCliArgs.u4IngressId = u4IngressId;
    TeCliArgs.u4EgressId = u4EgressId;
    TeCliArgs.i4TnlIfIndex = (INT4) gaTnlCliArgs[CliHandle].u4MplsTnlIfIndex;

    if (bInstance0Present == TRUE)
    {
        return (TeCliDeleteP2mpOrNormalTunnel (CliHandle, &TeCliArgs,
                                               OSIX_FALSE));
    }
    else if (u4TunnelInstance == TE_ZERO)
    {
        nmhGetMplsTunnelPrimaryInstance (u4TunnelIndex, u4TunnelInstance,
                                         u4IngressId, u4EgressId,
                                         &u4PrimaryInstance);

        return (TeCliDeleteP2mpOrNormalTunnel (CliHandle, &TeCliArgs,
                                               OSIX_FALSE));
    }

    return CLI_SUCCESS;
}

/*****************************************************************************
 *
 *     FUNCTION NAME    : MplsDsTeCliGetOctetString
 *
 *     DESCRIPTION      : This function takes in a string from CLI as
 *                        input  and returns the OctetString for the string
 *
 *                        pu1Input         - Pointer to Input String
 *
 *
 *     OUTPUT           : pDescription - Pointer to the SNMP String
 *
 *     RETURNS          : CLI_SUCCESS or CLI_FAILURE
 *
 ******************************************************************************/
INT4
MplsDsTeCliGetOctetString (UINT1 *pu1Input,
                           tSNMP_OCTET_STRING_TYPE * pDescription)
{
    static UINT1        au1Input[MPLS_MAX_DSTE_MAX_STRING_LEN];

    if (pu1Input == NULL)
    {
        pDescription->pu1_OctetList = NULL;
        pDescription->i4_Length = MPLS_ZERO;
        return CLI_SUCCESS;

    }
    if (STRLEN (pu1Input) > MPLS_MAX_DSTE_MAX_STRING_LEN)
    {
        return CLI_FAILURE;
    }

    MEMSET (au1Input, TE_ZERO, MPLS_MAX_DSTE_MAX_STRING_LEN);

    pDescription->pu1_OctetList = au1Input;
    pDescription->i4_Length = STRLEN (pu1Input);

    MEMCPY (pDescription->pu1_OctetList, pu1Input, pDescription->i4_Length);
    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : MplsDsTeShowRunningConfig
 * Description   : This routine displays running configuration information of
 *                 MPLS Diffserv.
 * Input(s)      : CliHandle          - Cli Context Handle
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 *****************************************************************************/
INT4
MplsDsTeShowRunningConfig (tCliHandle CliHandle)
{
    tSNMP_OCTET_STRING_TYPE CTDescription;
    tSNMP_OCTET_STRING_TYPE TCDescription;
    tSNMP_OCTET_STRING_TYPE TeClassDesc;
    static UINT1        au1CTDesc[SNMP_MAX_OID_LENGTH] = { 0 };
    static UINT1        au1TrafficDesc[SNMP_MAX_OID_LENGTH] = { 0 };
    static UINT1        au1TeClassDsec[SNMP_MAX_OID_LENGTH] = { 0 };
    BOOL1               bLoop = FALSE;
    BOOL1               bEntryValid = TRUE;
    INT4                i4MplsQosPolicy = 0;
    INT4                i4MplsDsTeStatus = 0;
    UINT4               u4CTIndex = 0;
    UINT4               u4CTPrevIndex = 0;
    UINT4               u4CTTcMapIndex = 0xFFFFFFFF;
    UINT4               u4TEClassIndex = 0xFFFFFFFF;
    INT4                i4DsTeTcType = 0;
    UINT4               u4DsTeTeClassNumber = 0;
    INT4                i4CTBwPercent = 0;
    INT4                i4ElspMapPrevIndex = 0;
    INT4                i4ElspMapIndex = 0;
    INT4                i4ElspMapExpIndex = 0;
    INT4                i4ExpMapDscp = 0;
    INT4                i4PreConfExpPhbMapIndex = 0;
    INT4                i4ElspInfoListIndex = 0;
    INT4                i4ElspInfoListPrevIndex = 0;
    INT4                i4ElspInfoIndex = 0;
    INT4                i4ElspInfoPHB = 0;

    MEMSET (au1CTDesc, 0, SNMP_MAX_OID_LENGTH);
    MEMSET (au1TrafficDesc, 0, SNMP_MAX_OID_LENGTH);
    MEMSET (au1TeClassDsec, 0, SNMP_MAX_OID_LENGTH);

    CTDescription.pu1_OctetList = au1CTDesc;
    CTDescription.i4_Length = 0;

    TCDescription.pu1_OctetList = au1TrafficDesc;
    TCDescription.i4_Length = 0;

    TeClassDesc.pu1_OctetList = au1TeClassDsec;
    TeClassDesc.i4_Length = 0;

    nmhGetFsMplsQosPolicy (&i4MplsQosPolicy);

    CliPrintf (CliHandle, "\n!\r\n");

    if (i4MplsQosPolicy == MPLS_QOS_RFC_1349)
    {
        CliPrintf (CliHandle, "\r\nmpls qos-policy rfc1349");
        FilePrintf (CliHandle, "\r\nmpls qos-policy rfc1349");
    }
    else if (i4MplsQosPolicy == MPLS_QOS_DIFFSERV)
    {
        CliPrintf (CliHandle, "\r\nmpls qos-policy diffserv");
        FilePrintf (CliHandle, "\r\nmpls qos-policy diffserv");
    }

    while (1)
    {
        if ((bLoop == TRUE) &&
            (nmhGetNextIndexFsMplsDsTeClassTypeTable (u4CTPrevIndex, &u4CTIndex)
             == SNMP_FAILURE))
        {
            break;
        }

        if ((bLoop == FALSE) &&
            (nmhGetFirstIndexFsMplsDsTeClassTypeTable (&u4CTIndex)
             == SNMP_FAILURE))
        {
            break;
        }

        u4CTPrevIndex = u4CTIndex;
        bLoop = TRUE;
        u4CTTcMapIndex = 0xFFFFFFFF;
        u4TEClassIndex = 0xFFFFFFFF;

        nmhGetFsMplsDsTeClassTypeDescription (u4CTIndex, &CTDescription);
        nmhGetFsMplsDsTeClassTypeBwPercentage (u4CTIndex, &i4CTBwPercent);

        CliPrintf (CliHandle, "\n!\r\n");
        FilePrintf (CliHandle, "\n!\r\n");
        CliPrintf (CliHandle,
                   "\r\nmpls class-type %d description %s bw-percentage %d",
                   u4CTIndex, CTDescription.pu1_OctetList, i4CTBwPercent);
        FilePrintf (CliHandle,
                    "\r\nmpls class-type %d description %s bw-percentage %d",
                    u4CTIndex, CTDescription.pu1_OctetList, i4CTBwPercent);

        while (nmhGetNextIndexFsMplsDsTeClassTypeToTcMapTable (u4CTIndex,
                                                               &u4CTIndex,
                                                               u4CTTcMapIndex,
                                                               &u4CTTcMapIndex)
               == SNMP_SUCCESS)
        {
            if (u4CTIndex != u4CTPrevIndex)
            {
                break;
            }

            bEntryValid = TRUE;

            nmhGetFsMplsDsTeTcType (u4CTIndex, u4CTTcMapIndex, &i4DsTeTcType);
            nmhGetFsMplsDsTeTcDescription (u4CTIndex, u4CTTcMapIndex,
                                           &TCDescription);

            CliPrintf (CliHandle, "\r\n traffic-class ");
            FilePrintf (CliHandle, "\r\n traffic-class ");

            switch (i4DsTeTcType)
            {
                case MPLS_DIFFSERV_DF_DSCP:
                    CliPrintf (CliHandle, "df ");
                    FilePrintf (CliHandle, "df ");
                    break;
                case MPLS_DIFFSERV_CS1_DSCP:
                    CliPrintf (CliHandle, "cs1 ");
                    FilePrintf (CliHandle, "cs1 ");
                    break;
                case MPLS_DIFFSERV_AF11_DSCP:
                    CliPrintf (CliHandle, "af1 ");
                    FilePrintf (CliHandle, "af1 ");
                    break;
                case MPLS_DIFFSERV_CS2_DSCP:
                    CliPrintf (CliHandle, "cs2 ");
                    FilePrintf (CliHandle, "cs2 ");
                    break;
                case MPLS_DIFFSERV_AF21_DSCP:
                    CliPrintf (CliHandle, "af2 ");
                    FilePrintf (CliHandle, "af2 ");
                    break;
                case MPLS_DIFFSERV_CS3_DSCP:
                    CliPrintf (CliHandle, "cs3 ");
                    FilePrintf (CliHandle, "cs3 ");
                    break;
                case MPLS_DIFFSERV_AF31_DSCP:
                    CliPrintf (CliHandle, "af3 ");
                    FilePrintf (CliHandle, "af3 ");
                    break;
                case MPLS_DIFFSERV_CS4_DSCP:
                    CliPrintf (CliHandle, "cs4 ");
                    FilePrintf (CliHandle, "cs4 ");
                    break;
                case MPLS_DIFFSERV_AF41_DSCP:
                    CliPrintf (CliHandle, "af4 ");
                    FilePrintf (CliHandle, "af4 ");
                    break;
                case MPLS_DIFFSERV_CS5_DSCP:
                    CliPrintf (CliHandle, "cs5 ");
                    FilePrintf (CliHandle, "cs5 ");
                    break;
                case MPLS_DIFFSERV_EF_DSCP:
                    CliPrintf (CliHandle, "ef ");
                    FilePrintf (CliHandle, "ef ");
                    break;
                case MPLS_DIFFSERV_CS6_DSCP:
                    CliPrintf (CliHandle, "cs6 ");
                    FilePrintf (CliHandle, "cs6 ");
                    break;
                case MPLS_DIFFSERV_CS7_DSCP:
                    CliPrintf (CliHandle, "cs7 ");
                    FilePrintf (CliHandle, "cs7 ");
                    break;
                case MPLS_DIFFSERV_EF1_DSCP:
                    CliPrintf (CliHandle, "ef1 ");
                    FilePrintf (CliHandle, "ef1 ");
                    break;
                default:
                    bEntryValid = FALSE;
                    break;
            }

            if ((TCDescription.i4_Length != 0) && (bEntryValid == TRUE))
            {
                CliPrintf (CliHandle,
                           "description %s", TCDescription.pu1_OctetList);
                FilePrintf (CliHandle,
                            "description %s", TCDescription.pu1_OctetList);
            }
        }

        u4CTIndex = u4CTPrevIndex;

        while (nmhGetNextIndexFsMplsDsTeTeClassTable (u4CTIndex, &u4CTIndex,
                                                      u4TEClassIndex,
                                                      &u4TEClassIndex)
               == SNMP_SUCCESS)
        {
            if (u4CTIndex != u4CTPrevIndex)
            {
                break;
            }

            nmhGetFsMplsDsTeTeClassDesc (u4CTIndex, u4TEClassIndex,
                                         &TeClassDesc);
            nmhGetFsMplsDsTeTeClassNumber (u4CTIndex, u4TEClassIndex,
                                           &u4DsTeTeClassNumber);

            CliPrintf (CliHandle,
                       "\r\ntraffic-class te-priority %d te-class-number %d ",
                       u4TEClassIndex, u4DsTeTeClassNumber);
            FilePrintf (CliHandle,
                        "\r\ntraffic-class te-priority %d te-class-number %d ",
                        u4TEClassIndex, u4DsTeTeClassNumber);

            if (TeClassDesc.i4_Length != 0)
            {
                CliPrintf (CliHandle,
                           "description %s", TeClassDesc.pu1_OctetList);
                FilePrintf (CliHandle,
                            "description %s", TeClassDesc.pu1_OctetList);
            }
        }
    }

    nmhGetFsMplsDsTeStatus (&i4MplsDsTeStatus);
    if (i4MplsDsTeStatus == MPLS_DSTE_STATUS_ENABLE)
    {
        CliPrintf (CliHandle, "\n!\r\n");
        FilePrintf (CliHandle, "\n!\r\n");
        CliPrintf (CliHandle, "\r\nmpls diffserv enable");
        FilePrintf (CliHandle, "\r\nmpls diffserv enable");
    }

    bLoop = FALSE;

    /*Loop for ExpPhbMapping for preconfigured scenario */
    while (1)
    {
        if ((bLoop == TRUE) &&
            (nmhGetNextIndexFsMplsDiffServElspMapTable (i4ElspMapIndex,
                                                        &i4ElspMapIndex,
                                                        i4ElspMapExpIndex,
                                                        &i4ElspMapExpIndex)
             == SNMP_FAILURE))
        {
            break;
        }

        if ((bLoop == FALSE) &&
            (nmhGetFirstIndexFsMplsDiffServElspMapTable (&i4ElspMapIndex,
                                                         &i4ElspMapExpIndex)
             == SNMP_FAILURE))
        {
            break;
        }

        bLoop = TRUE;

        if (i4ElspMapPrevIndex != i4ElspMapIndex)
        {
            i4ElspMapPrevIndex = i4ElspMapIndex;

            CliPrintf (CliHandle, "\n!\r\n");
            FilePrintf (CliHandle, "\n!\r\n");
            CliPrintf (CliHandle, "\r\nexp-phb-map index %d", i4ElspMapIndex);
            FilePrintf (CliHandle, "\r\nexp-phb-map index %d", i4ElspMapIndex);

        }

        nmhGetFsMplsDiffServElspMapPhbDscp (i4ElspMapIndex,
                                            i4ElspMapExpIndex, &i4ExpMapDscp);
        CliPrintf (CliHandle,
                   "\r\n exp-bits %d phb %d", i4ElspMapExpIndex, i4ExpMapDscp);
        FilePrintf (CliHandle,
                    "\r\n exp-bits %d phb %d", i4ElspMapExpIndex, i4ExpMapDscp);
    }

    nmhGetFsMplsDiffServElspPreConfExpPhbMapIndex (&i4PreConfExpPhbMapIndex);

    if (i4PreConfExpPhbMapIndex != MPLS_INVALID_EXP_PHB_MAP_INDEX)
    {
        CliPrintf (CliHandle, "\n!\r\n");
        FilePrintf (CliHandle, "\n!\r\n");
        CliPrintf (CliHandle,
                   "\r\nuse preconfig-exp-phb-map index %d",
                   i4PreConfExpPhbMapIndex);
        FilePrintf (CliHandle,
                    "\r\nuse preconfig-exp-phb-map index %d",
                    i4PreConfExpPhbMapIndex);

    }

    bLoop = FALSE;

    /* Loop for ElspInfoTable For signalled configuration */
    while (1)
    {
        if ((bLoop == TRUE) &&
            (nmhGetNextIndexFsMplsDiffServElspInfoTable (i4ElspInfoListIndex,
                                                         &i4ElspInfoListIndex,
                                                         i4ElspInfoIndex,
                                                         &i4ElspInfoIndex)
             == SNMP_FAILURE))
        {
            break;
        }

        if ((bLoop == FALSE) &&
            (nmhGetFirstIndexFsMplsDiffServElspInfoTable (&i4ElspInfoListIndex,
                                                          &i4ElspInfoIndex)
             == SNMP_FAILURE))
        {
            break;
        }

        bLoop = TRUE;

        if (i4ElspInfoListPrevIndex != i4ElspInfoListIndex)
        {
            i4ElspInfoListPrevIndex = i4ElspInfoListIndex;

            CliPrintf (CliHandle, "\n!\r\n");
            FilePrintf (CliHandle, "\n!\r\n");
            CliPrintf (CliHandle, "\r\nexp-info index %d", i4ElspInfoListIndex);
            FilePrintf (CliHandle,
                        "\r\nexp-info index %d", i4ElspInfoListIndex);
        }

        nmhGetFsMplsDiffServElspInfoPHB (i4ElspInfoListIndex,
                                         i4ElspInfoIndex, &i4ElspInfoPHB);
        CliPrintf (CliHandle,
                   "\r\n elsp-info exp-bits %d phb %d",
                   i4ElspInfoIndex, i4ElspInfoPHB);
        FilePrintf (CliHandle,
                    "\r\n elsp-info exp-bits %d phb %d",
                    i4ElspInfoIndex, i4ElspInfoPHB);
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : TeCliSetFsMplsTunnelHopIncludeAny  
 * Description   : This routine is used to set the Include Any object in 
 *                 FSMPLS Tunnel Hop Table
 * Input         :  u4PathListIndex       - Path List Index
 *                  u4PathOptionId        - Path Option Index
 *                  u4HopIndex            - Hop Index
 *                  i4IncludeExclude      - IncludeAny
 * 
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 * ******************************************************************************/
INT4
TeCliSetFsMplsTunnelHopIncludeAny (UINT4 u4PathListIndex,
                                   UINT4 u4PathOptionId, UINT4 u4HopIndex)
{
    UINT4               u4ErrorCode = TE_ZERO;

    if (nmhTestv2FsMplsTunnelHopIncludeAny (&u4ErrorCode, u4PathListIndex,
                                            u4PathOptionId, u4HopIndex,
                                            MPLS_SNMP_TRUE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsTunnelHopIncludeAny (u4PathListIndex, u4PathOptionId,
                                         u4HopIndex,
                                         MPLS_SNMP_TRUE) == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/******************************************************************************
 * Function Name : TeCliSetIfMainStorageType  
 * Description   : This routine sets If Main Storage Type
 * Input         : i4IfIndex     - If Index
 *                 i4Value       - Value to be set
 * Output(s)     : None
 * Return(s)     : None
 * ******************************************************************************/
VOID
TeCliSetIfMainStorageType (INT4 i4IfIndex, INT4 i4Value)
{
    CFA_LOCK ();

    if (nmhSetIfMainStorageType (i4IfIndex, i4Value) == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return;
    }

    CFA_UNLOCK ();
}

/******************************************************************************
 * Function Name : TeCliSetIfMainRowStatus 
 * Description   : This routine sets If Main RowStatus
 * Input         : i4IfIndex     - If Index
 *                 i4Value       - Value to be set
 * Output(s)     : None
 * Return(s)     : None
 * ******************************************************************************/
VOID
TeCliSetIfMainRowStatus (INT4 i4IfIndex, INT4 i4Value)
{
    CFA_LOCK ();

    if (nmhSetIfMainRowStatus (i4IfIndex, i4Value) == SNMP_FAILURE)
    {
        CFA_UNLOCK ();
        return;
    }

    CFA_UNLOCK ();
}

/***************************************************************************/
/*                                                                         */
/*     Function Name : IssMplsTeShowDebugging                              */
/*                                                                         */
/*     Description   : This function is used to display debug level for    */
/*                     MPLS TE module                                      */
/*                                                                         */
/*     INPUT         : CliHandle                                           */
/*                                                                         */
/*     OUTPUT        : None                                                */
/*                                                                         */
/*     RETURNS       : None                                                */
/*                                                                         */
/***************************************************************************/

VOID
IssMplsTeShowDebugging (tCliHandle CliHandle)
{
    CliRegisterLock (CliHandle, CmnLock, CmnUnLock);
    MPLS_CMN_LOCK ();

    if (TE_DBG_FLAG == TE_ALL)
    {
        CliPrintf (CliHandle, "\rMPLS TE :");
        CliPrintf (CliHandle, "\r\n  MPLS te all debugging is on");
        CliPrintf (CliHandle, "\r\n");
    }

    MPLS_CMN_UNLOCK ();
    CliUnRegisterLock (CliHandle);
    return;
}

/*******************************************************************************
* Function Name : TeCliShowTnlCounters 
* Description   : This routine is used to display tunnel counter 
* Input(s)      : CliHandle        - Cli Context Handle
*               : u4IngressId      - Ingress Id
*               : u4EgressId       - Egress Id
*               : u4TnlIndex       - Tunnel Index
*               : u4TnlInstance    - Tunnel Instance 
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
*******************************************************************************/
INT1
TeCliShowTnlCounters (tCliHandle CliHandle, UINT4 u4IngressId,
                      UINT4 u4EgressId, UINT4 u4TnlIndex, UINT4 u4TnlInstance)
{
    tSNMP_COUNTER64_TYPE RetValTnlPerfHCPackets;
    tSNMP_COUNTER64_TYPE RetValTnlPerfHCBytes;

    FS_UINT8            u8RetValTnlPerfCurrentHCPackets;
    FS_UINT8            u8RetValTnlPerfCurrentHCBytes;
    FS_UINT8            u8Zero;
    UINT4               u4ErrorCode = TE_ZERO;
    UINT1               au1RetValTnlPerfCurrentHCPackets[MAX_U8_STRING_SIZE];
    UINT1               au1RetValTnlPerfCurrentHCBytes[MAX_U8_STRING_SIZE];

    RetValTnlPerfHCPackets.msn = TE_ZERO;
    RetValTnlPerfHCPackets.lsn = TE_ZERO;
    RetValTnlPerfHCBytes.msn = TE_ZERO;
    RetValTnlPerfHCBytes.lsn = TE_ZERO;

    FSAP_U8_CLR (&u8RetValTnlPerfCurrentHCPackets);
    FSAP_U8_CLR (&u8RetValTnlPerfCurrentHCBytes);
    FSAP_U8_CLR (&u8Zero);

    nmhGetMplsTunnelPerfHCPackets (u4TnlIndex,
                                   u4TnlInstance,
                                   u4IngressId,
                                   u4EgressId, &RetValTnlPerfHCPackets);

    nmhGetMplsTunnelPerfHCBytes (u4TnlIndex,
                                 u4TnlInstance,
                                 u4IngressId,
                                 u4EgressId, &RetValTnlPerfHCBytes);

    nmhGetMplsTunnelPerfErrors (u4TnlIndex,
                                u4TnlInstance,
                                u4IngressId, u4EgressId, &u4ErrorCode);
    /* For HC Packets */
    FSAP_U8_ASSIGN_HI (&u8RetValTnlPerfCurrentHCPackets,
                       RetValTnlPerfHCPackets.msn);
    FSAP_U8_ASSIGN_LO (&u8RetValTnlPerfCurrentHCPackets,
                       RetValTnlPerfHCPackets.lsn);
    /* For HC Bytes */
    FSAP_U8_ASSIGN_HI (&u8RetValTnlPerfCurrentHCBytes,
                       RetValTnlPerfHCBytes.msn);
    FSAP_U8_ASSIGN_LO (&u8RetValTnlPerfCurrentHCBytes,
                       RetValTnlPerfHCBytes.lsn);

    FSAP_U8_2STR (&u8RetValTnlPerfCurrentHCPackets,
                  (CHR1 *) au1RetValTnlPerfCurrentHCPackets);
    FSAP_U8_2STR (&u8RetValTnlPerfCurrentHCBytes,
                  (CHR1 *) au1RetValTnlPerfCurrentHCBytes);

    CliPrintf (CliHandle, "Tunnel statistics:\n");
    if (FSAP_U8_CMP (&u8RetValTnlPerfCurrentHCPackets, &u8Zero) == 0)
    {
        CliPrintf (CliHandle, "    packet totals      : 0 \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "    packet totals      : %s \r\n",
                   au1RetValTnlPerfCurrentHCPackets);
    }
    if (FSAP_U8_CMP (&u8RetValTnlPerfCurrentHCBytes, &u8Zero) == 0)
    {
        CliPrintf (CliHandle, "    Bytes totals       : 0 \r\n");
    }
    else
    {
        CliPrintf (CliHandle, "    Bytes totals       : %s \r\n",
                   au1RetValTnlPerfCurrentHCBytes);
    }
    CliPrintf (CliHandle, "    Errors             : %d \r\n", u4ErrorCode);

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliSetMplsTunnelOldBandwidth
* Description   : This routine is used to set the old bandwidth 
*          of tunnel passed with the indices .
* Input(s)      : u4TunnelIndex    - Tunnel Index
*                 u4TunnelInstance - Tunnel Instance
*                 u4IngressId      - Tunnel Source
*                 u4EgressId       - Tunnel Destination
*                
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
VOID
TeCliSetMplsTunnelOldBandwidth (UINT4 u4TunnelIndex, UINT4 u4TunnelInstance,
                                UINT4 u4IngressId, UINT4 u4EgressId)
{
    INT4                i4TnlMBBStatus = MPLS_TE_MBB_DISABLED;
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeTrfcParams      *pTeTrfcParams = NULL;

    nmhGetFsMplsTunnelMBBStatus (u4TunnelIndex, u4TunnelInstance,
                                 u4IngressId, u4EgressId, &i4TnlMBBStatus);

    if (i4TnlMBBStatus == MPLS_TE_MBB_ENABLED)
    {

        pTeTnlInfo = TeGetTunnelInfo (u4TunnelIndex, u4TunnelInstance,
                                      u4IngressId, u4EgressId);

        if (pTeTnlInfo != NULL)
        {
            if ((TeCheckTrfcParamInTrfcParamTable
                 (TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo),
                  &pTeTrfcParams) == TE_SUCCESS)
                && (TE_RSVPTE_TRFC_PARAMS (pTeTrfcParams) != NULL))
            {
                /*Set Old Bandwidth into Traffic OldPeakDataRate */
                TE_RSVPTE_TOLDPARAM_PDR (pTeTrfcParams) =
                    TE_RSVPTE_TPARAM_PDR (pTeTrfcParams);
            }
        }
    }

}

/******************************************************************************
* Function Name : TeCliBackupHopTablePathOptAndPathNum 
* Description   : This routine is used to configure path option number 
*                 and hop list index
* Input(s)      : CliHandle    - Cli Context Handle
*                 i4PathOptNum - Hop path option index 
*                 i4PathNum    - HopListIndex
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliBackupHopTablePathOptAndPathNum (tCliHandle CliHandle, UINT4 u4PathOptNum,
                                      UINT4 u4PathNum, INT4 i4PathComp,
                                      UINT1 *pAttrName)
{
    UINT4               u4TunnelIndex = TE_ZERO;
    UINT4               u4TunnelInstance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    UINT4               u4ErrorCode = TE_ZERO;
    UINT4               u4RetValMplsTunnelBackupPathInUse = TE_ZERO;
    INT4                i4PrevRowStatus = 0;
    INT4                i4TunnelAdminStatus = 0;
    INT4                i4MbbStatus = 0;

    tSNMP_OID_TYPE      GetAttrPointer;
    tSNMP_OID_TYPE      AttrPointer;
    INT4                i4LSPEncodingType = 0;
    static UINT4        au4AttrTableOid[TE_TNL_ATTR_TABLE_DEF_OFFSET] =
        { 1, 3, 6, 1, 4, 1, 2076, 13, 15, 1, 5, 1, 2 };
    static UINT4        au4GetAttrPointer[TE_TNL_ATTR_TABLE_DEF_OFFSET];

    tTeAttrListInfo     TeAttrListInfo;

    MEMSET (&TeAttrListInfo, 0, sizeof (tTeAttrListInfo));

    GetAttrPointer.pu4_OidList = au4GetAttrPointer;

    MEMSET (GetAttrPointer.pu4_OidList, TE_ZERO, sizeof (UINT4) * TE_TWO);
    GetAttrPointer.u4_Length = TE_ZERO;

    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    nmhGetMplsTunnelAdminStatus (u4TunnelIndex, u4TunnelInstance,
                                 u4IngressId, u4EgressId, &i4TunnelAdminStatus);

    nmhGetFsMplsTunnelMBBStatus (u4TunnelIndex, u4TunnelInstance,
                                 u4IngressId, u4EgressId, &i4MbbStatus);

    if ((i4TunnelAdminStatus == TE_ADMIN_UP) &&
        (i4MbbStatus == MPLS_TE_MBB_DISABLED))
    {
        CliPrintf (CliHandle, "\r\n%% Tunnel is  ADMIN-UP,"
                   "Unable to configure path-option and path number \r\n");
        return CLI_FAILURE;

    }

    if (nmhGetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                   u4IngressId, u4EgressId,
                                   &i4PrevRowStatus) == SNMP_FAILURE)
    {
        TE_DBG (TE_EXTN_FAIL, "\r\n%%Failed to get tunnel information\r\n");
    }
    TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                 u4IngressId, u4EgressId, TE_NOTINSERVICE);

    nmhGetGmplsTunnelLSPEncoding (u4TunnelIndex, u4TunnelInstance,
                                  u4IngressId, u4EgressId, &i4LSPEncodingType);

    nmhGetFsMplsTunnelAttPointer (u4TunnelIndex, u4TunnelInstance,
                                  u4IngressId, u4EgressId, &GetAttrPointer);
    if (u4PathNum != TE_ZERO)
    {
        /*Checking whether Path Number is valid or not */
        if (nmhTestv2FsMplsTunnelBackupHopTableIndex (&u4ErrorCode,
                                                      u4TunnelIndex,
                                                      u4TunnelInstance,
                                                      u4IngressId, u4EgressId,
                                                      u4PathNum) ==
            SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Invalid Backup Path Number \r\n");
            return CLI_FAILURE;
        }
    }

    if ((nmhGetFsMplsTunnelBackupPathInUse (u4TunnelIndex, u4TunnelInstance,
                                            u4IngressId, u4EgressId,
                                            &u4RetValMplsTunnelBackupPathInUse))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to retrive Backup Path in use\r\n");
        return CLI_FAILURE;
    }

    /*Checking whether Path Option Number is valid or not */
    if ((u4RetValMplsTunnelBackupPathInUse != 0) &&
        (u4RetValMplsTunnelBackupPathInUse != u4PathOptNum))
    {
        CliPrintf (CliHandle, "\r\n%%Cannot set more than one path option\r\n");
        return CLI_FAILURE;
    }
    else
    {
        if (nmhTestv2FsMplsTunnelBackupPathInUse (&u4ErrorCode,
                                                  u4TunnelIndex,
                                                  u4TunnelInstance, u4IngressId,
                                                  u4EgressId,
                                                  u4PathOptNum) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Invalid Backup Path-Option \r\n");
            return CLI_FAILURE;
        }
    }

    if (i4LSPEncodingType != GMPLS_TUNNEL_LSP_NOT_GMPLS)
    {
        if (nmhTestv2GmplsTunnelPathComp (&u4ErrorCode, u4TunnelIndex,
                                          u4TunnelInstance, u4IngressId,
                                          u4EgressId, i4PathComp)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r%%Unable to configure Backup Path Computation\n");
            return CLI_FAILURE;
        }
    }

    if (pAttrName != NULL)
    {
        TeAttrListInfo.i4ListNameLen = (INT4) STRLEN (pAttrName);
        MEMCPY (TeAttrListInfo.au1ListName, pAttrName,
                TeAttrListInfo.i4ListNameLen);

        if (TeSigGetAttrListIndexFromName (&TeAttrListInfo) == TE_FAILURE)
        {
            CliPrintf (CliHandle, "\r%% Attribute List do not exist\n");
            return CLI_FAILURE;
        }

        au4AttrTableOid[TE_TNL_ATTR_TABLE_DEF_OFFSET - TE_ONE]
            = TeAttrListInfo.u4ListIndex;
        AttrPointer.pu4_OidList = au4AttrTableOid;
        AttrPointer.u4_Length = TE_TNL_ATTR_TABLE_DEF_OFFSET;

        if (nmhTestv2FsMplsTunnelAttPointer (&u4ErrorCode, u4TunnelIndex,
                                             u4TunnelInstance, u4IngressId,
                                             u4EgressId, &AttrPointer)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Invalid Attribute Table Pointer \r\n");
            return CLI_FAILURE;
        }
    }

    if (i4PrevRowStatus == TE_ACTIVE)
    {
        if (nmhSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                       u4IngressId, u4EgressId, TE_NOTINSERVICE)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Invalid Tunnel Row Status \r\n");
            return CLI_FAILURE;
        }
    }

    if (u4PathNum != TE_ZERO)
    {
        if (nmhSetFsMplsTunnelBackupHopTableIndex
            (u4TunnelIndex, u4TunnelInstance, u4IngressId, u4EgressId,
             u4PathNum) == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%% nmhSetMplsTunnelBackupHopTableIndex failed\r\n");
            return CLI_FAILURE;
        }

    }

    if (nmhSetFsMplsTunnelBackupPathInUse (u4TunnelIndex, u4TunnelInstance,
                                           u4IngressId, u4EgressId,
                                           u4PathOptNum) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%% nmhSetMplsTunnelBackupPathInUse failed\r\n");
        return CLI_FAILURE;
    }

    if (i4LSPEncodingType != GMPLS_TUNNEL_LSP_NOT_GMPLS)
    {
        if (nmhSetGmplsTunnelPathComp (u4TunnelIndex, u4TunnelInstance,
                                       u4IngressId, u4EgressId, i4PathComp)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle,
                       "\r\n%% nmhSetGmplsTunnelPathComp failed\r\n");
            return CLI_FAILURE;
        }

    }

    if (pAttrName != NULL)
    {
        if (nmhSetFsMplsTunnelAttPointer (u4TunnelIndex, u4TunnelInstance,
                                          u4IngressId, u4EgressId, &AttrPointer)
            == SNMP_FAILURE)
        {
            CliPrintf (CliHandle, "\r\n%%Invalid Attribute Table Pointer \r\n");
            return CLI_FAILURE;
        }
    }

    if (i4PrevRowStatus != TE_NOTREADY)
    {
        TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                     u4IngressId, u4EgressId, i4PrevRowStatus);
    }

    return CLI_SUCCESS;
}

/******************************************************************************
* Function Name : TeCliNoBackupHopTablePathOptAndPathNum
* Description   : This routine is used to configure path option number 
*                 and hop list index
* Input(s)      : CliHandle    - Cli Context Handle
*                 i4PathOptNum - Hop path option index 
*                 i4PathNum    - HopListIndex
* Output(s)     : None
* Return(s)     : CLI_SUCCESS or CLI_FAILURE
******************************************************************************/
INT4
TeCliNoBackupHopTablePathOptAndPathNum (tCliHandle CliHandle,
                                        UINT4 u4PathOptNum, UINT4 u4PathNum)
{
    UINT4               u4TunnelIndex = TE_ZERO;
    UINT4               u4TunnelInstance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    UINT4               u4ErrorCode = TE_ZERO;
    UINT4               u4RetValMplsTunnelBackupHopTableIndex = TE_ZERO;
    UINT4               u4RetValMplsTunnelBackupPathInUse = TE_ZERO;
    INT4                i4PrevRowStatus = 0;
    INT4                i4TunnelAdminStatus = 0;
    INT4                i4MbbStatus = 0;

    tSNMP_OID_TYPE      SetAttrPointer;
    static UINT4        au4SetAttrPointer[TE_TNL_ATTR_TABLE_DEF_OFFSET] =
        { 1, 3, 6, 1, 4, 1, 2076, 13, 15, 1, 5, 1, 2 };

    au4SetAttrPointer[TE_TNL_ATTR_TABLE_DEF_OFFSET - TE_ONE] = 0;
    SetAttrPointer.pu4_OidList = au4SetAttrPointer;
    SetAttrPointer.u4_Length = TE_TNL_ATTR_TABLE_DEF_OFFSET;

    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    nmhGetMplsTunnelAdminStatus (u4TunnelIndex, u4TunnelInstance,
                                 u4IngressId, u4EgressId, &i4TunnelAdminStatus);

    nmhGetFsMplsTunnelMBBStatus (u4TunnelIndex, u4TunnelInstance,
                                 u4IngressId, u4EgressId, &i4MbbStatus);

    if ((i4TunnelAdminStatus == TE_ADMIN_UP) &&
        (i4MbbStatus == MPLS_TE_MBB_DISABLED))
    {
        CliPrintf (CliHandle, "\r\n%% Tunnel is  ADMIN-UP,"
                   "Unable to configure path-option and path number \r\n");
        return CLI_FAILURE;

    }

    if (nmhGetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                   u4IngressId, u4EgressId,
                                   &i4PrevRowStatus) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to get tunnel row status\r\n");
        return CLI_FAILURE;
    }

    if ((nmhSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                    u4IngressId, u4EgressId,
                                    TE_NOTINSERVICE)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to reset MplsTunnelRowStatus\r\n");
        return CLI_FAILURE;
    }

    if (nmhTestv2FsMplsTunnelAttPointer (&u4ErrorCode, u4TunnelIndex,
                                         u4TunnelInstance, u4IngressId,
                                         u4EgressId, &SetAttrPointer)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Invalid Attribute  Table Pointer \r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsTunnelAttPointer (u4TunnelIndex, u4TunnelInstance,
                                      u4IngressId, u4EgressId, &SetAttrPointer)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Invalid Attribute  Table Pointer \r\n");
        return CLI_FAILURE;
    }

    /*To retrive Hop table index */
    if ((nmhGetFsMplsTunnelBackupHopTableIndex (u4TunnelIndex, u4TunnelInstance,
                                                u4IngressId, u4EgressId,
                                                &u4RetValMplsTunnelBackupHopTableIndex))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to retrive Backup Hop table index\r\n");
        return CLI_FAILURE;
    }
    /*To retrive path in use */
    if ((nmhGetFsMplsTunnelBackupPathInUse (u4TunnelIndex, u4TunnelInstance,
                                            u4IngressId, u4EgressId,
                                            &u4RetValMplsTunnelBackupPathInUse))
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to retrive Backup Path in use\r\n");
        return CLI_FAILURE;
    }
    /*Checking with the configured values */
    if ((u4RetValMplsTunnelBackupHopTableIndex != u4PathNum) ||
        (u4RetValMplsTunnelBackupPathInUse != u4PathOptNum))
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to configure path-option and path number\r\n");
        return CLI_FAILURE;
    }
    else
    {
        u4PathNum = TE_ZERO;
        u4PathOptNum = TE_ZERO;
    }

    /*Checking whether Path Number is valid or not */
    if ((nmhTestv2FsMplsTunnelBackupHopTableIndex (&u4ErrorCode,
                                                   u4TunnelIndex,
                                                   u4TunnelInstance,
                                                   u4IngressId, u4EgressId,
                                                   u4PathNum)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Invalid Backup Path Number \r\n");
        return CLI_FAILURE;
    }
    /*To Set Hop Table Index */
    if ((nmhSetFsMplsTunnelBackupHopTableIndex (u4TunnelIndex, u4TunnelInstance,
                                                u4IngressId, u4EgressId,
                                                u4PathNum)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to configure Backup Hop table"
                   "Index\r\n");
        return CLI_FAILURE;
    }
    /*Checking whether Path Option Number is valid or not */
    if ((nmhTestv2FsMplsTunnelBackupPathInUse (&u4ErrorCode,
                                               u4TunnelIndex, u4TunnelInstance,
                                               u4IngressId, u4EgressId,
                                               u4PathOptNum)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Invalid Path-Option \r\n");
        return CLI_FAILURE;
    }
    /*To Set Path In Use */
    if ((nmhSetFsMplsTunnelBackupPathInUse (u4TunnelIndex, u4TunnelInstance,
                                            u4IngressId, u4EgressId,
                                            u4PathOptNum)) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to configure Backup Path In Use \r\n");
        return CLI_FAILURE;
    }

    if (i4PrevRowStatus != TE_NOTREADY)
    {
        TeCliSetMplsTunnelRowStatus (u4TunnelIndex, u4TunnelInstance,
                                     u4IngressId, u4EgressId, i4PrevRowStatus);
    }

    return CLI_SUCCESS;
}

/*******************************************************************************
 *  Function Name : MplsTunnelShowEROConfig
 *  Description   : This function shows the Mpls Tunnel ERO Config 
 *  Input(s)      : 
 *  Output(s)     : None.
 *  Return Values : CLI_SUCCESS/CLI_FAILURE
 *********************************************************************************/
void
MplsTunnelShowEROConfig (tCliHandle CliHandle, UINT4 u4HopPathOptionIndex,
                         UINT4 u4HopListIndex, UINT4 u4AttrIndex,
                         UINT4 u4TunnelInstance, UINT4 u4Flag)
{

    INT4                i4RowStatus = 0;
    INT4                i4PathComp = 0;
    UINT4               u4TnlHopId = TE_ONE;
    INT4                i4HopType = 0;
    tSNMP_OCTET_STRING_TYPE HopIpAddr;
    tSNMP_OCTET_STRING_TYPE AttributeName;
    static UINT1        au1HopIpAddr[TE_FOUR] = { 0 };
    static UINT1        au1AttributeName[TE_FOUR] = { 0 };
    UINT1               au1Space[3];
    AttributeName.pu1_OctetList = au1AttributeName;
    AttributeName.i4_Length = TE_FOUR;
    HopIpAddr.pu1_OctetList = au1HopIpAddr;
    HopIpAddr.i4_Length = TE_FOUR;

    MEMSET (au1Space, 0, sizeof (au1Space));

    if (u4HopPathOptionIndex != TE_ZERO)
    {
        if (u4Flag == TE_ZERO)
        {
            CliPrintf (CliHandle,
                       "\r\n%stunnel mpls traffic-eng path-option number "
                       "%d", au1Space, u4HopPathOptionIndex);
            FilePrintf (CliHandle,
                        "\r\n%stunnel mpls traffic-eng path-option number "
                        "%d", au1Space, u4HopPathOptionIndex);
        }
        else
        {
            CliPrintf (CliHandle,
                       "\r\n%stunnel mpls traffic-eng backup path-option number "
                       "%d", au1Space, u4HopPathOptionIndex);
            FilePrintf (CliHandle,
                        "\r\n%stunnel mpls traffic-eng backup path-option number "
                        "%d", au1Space, u4HopPathOptionIndex);

        }
        if (u4HopListIndex != TE_ZERO)
        {
            CliPrintf (CliHandle, " explicit identifier %d", u4HopListIndex);
            FilePrintf (CliHandle, " explicit identifier %d", u4HopListIndex);
        }
        else
        {
            CliPrintf (CliHandle, " dynamic");
            FilePrintf (CliHandle, " dynamic");
        }

        if (u4AttrIndex != TE_ZERO)
        {
            nmhGetFsTunnelAttributeName (u4AttrIndex, &AttributeName);
            CliPrintf (CliHandle, " attribute %s", AttributeName.pu1_OctetList);
            FilePrintf (CliHandle, " attribute %s",
                        AttributeName.pu1_OctetList);
        }
        if (u4TunnelInstance != TE_ZERO)
        {
            CliPrintf (CliHandle, " %d", u4TunnelInstance);
            FilePrintf (CliHandle, " %d", u4TunnelInstance);
        }

        while (u4TnlHopId <= MAX_TE_HOP_PER_PO)
        {
            if (nmhGetMplsTunnelHopRowStatus (u4HopListIndex,
                                              u4HopPathOptionIndex,
                                              u4TnlHopId, &i4RowStatus)
                == SNMP_FAILURE)
            {
                u4TnlHopId++;
                continue;
            }

            nmhGetMplsTunnelHopEntryPathComp (u4HopListIndex,
                                              u4HopPathOptionIndex,
                                              u4TnlHopId, &i4PathComp);

            if (i4PathComp != TE_EXPLICIT)
            {
                u4TnlHopId++;
                continue;
            }

            CliPrintf (CliHandle, "\r\n tunnel tsp-hop %d", u4TnlHopId);
            FilePrintf (CliHandle, "\r\n tunnel tsp-hop %d", u4TnlHopId);

            nmhGetMplsTunnelHopIpAddr (u4HopListIndex, u4HopPathOptionIndex,
                                       u4TnlHopId, &HopIpAddr);

            CliPrintf (CliHandle, " %d.%d.%d.%d",
                       HopIpAddr.pu1_OctetList[0],
                       HopIpAddr.pu1_OctetList[1],
                       HopIpAddr.pu1_OctetList[2], HopIpAddr.pu1_OctetList[3]);
            FilePrintf (CliHandle, " %d.%d.%d.%d",
                        HopIpAddr.pu1_OctetList[0],
                        HopIpAddr.pu1_OctetList[1],
                        HopIpAddr.pu1_OctetList[2], HopIpAddr.pu1_OctetList[3]);

            nmhGetMplsTunnelHopType (u4HopListIndex, u4HopPathOptionIndex,
                                     u4TnlHopId, &i4HopType);

            if (i4HopType == TE_STRICT_ER)
            {
                CliPrintf (CliHandle, " strict");
                FilePrintf (CliHandle, " strict");
            }
            else
            {
                CliPrintf (CliHandle, " loose");
                FilePrintf (CliHandle, " loose");
            }

            if (u4TunnelInstance != TE_ZERO)
            {
                CliPrintf (CliHandle, " %d", u4TunnelInstance);
                FilePrintf (CliHandle, " %d", u4TunnelInstance);
            }

            u4TnlHopId++;
        }
    }
}

/*******************************************************************************
 * Function Name : TeCliAddTunnelInReoptimizeList
 * Description   : This routine is used to add tunnel in Reoptimize tunnel list
 * Input(s)      : CliHandle  - Cli Context Handle
 *                 pTeCliArgs - CLI Args
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ********************************************************************************/
INT4
TeCliAddTunnelInReoptimizeList (tCliHandle CliHandle, tTeCliArgs * pTeCliArgs)
{
    UINT4               u4ErrorCode = TE_ZERO;
    INT4                i4RowStatus = TE_ZERO;

    nmhGetFsMplsReoptimizationTunnelRowStatus (pTeCliArgs->u4TunnelIndex,
                                               pTeCliArgs->u4IngressId,
                                               pTeCliArgs->u4EgressId,
                                               &i4RowStatus);

    if (i4RowStatus == ACTIVE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Reoptimization is already enabled on this tunnel\r\n");
        return CLI_SUCCESS;
    }

    if ((nmhTestv2FsMplsReoptimizationTunnelRowStatus (&u4ErrorCode,
                                                       pTeCliArgs->
                                                       u4TunnelIndex,
                                                       pTeCliArgs->u4IngressId,
                                                       pTeCliArgs->u4EgressId,
                                                       CREATE_AND_WAIT)) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to create Row for Reoptimization "
                   "tunnel list\r\n");
        return CLI_FAILURE;
    }

    if ((nmhSetFsMplsReoptimizationTunnelRowStatus (pTeCliArgs->u4TunnelIndex,
                                                    pTeCliArgs->u4IngressId,
                                                    pTeCliArgs->u4EgressId,
                                                    CREATE_AND_WAIT)) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r\n%%Unable to create Row for Reoptimization "
                   "tunnel list\r\n");
        return CLI_FAILURE;
    }

    nmhSetFsMplsReoptimizationTunnelStatus (pTeCliArgs->u4TunnelIndex,
                                            pTeCliArgs->u4IngressId,
                                            pTeCliArgs->u4EgressId,
                                            TE_REOPTIMIZE_ENABLE);

    nmhTestv2FsMplsReoptimizationTunnelRowStatus (&u4ErrorCode,
                                                  pTeCliArgs->u4TunnelIndex,
                                                  pTeCliArgs->u4IngressId,
                                                  pTeCliArgs->u4EgressId,
                                                  ACTIVE);

    nmhSetFsMplsReoptimizationTunnelRowStatus (pTeCliArgs->u4TunnelIndex,
                                               pTeCliArgs->u4IngressId,
                                               pTeCliArgs->u4EgressId, ACTIVE);

    return CLI_SUCCESS;
}

 /*******************************************************************************
 * Function Name : TeCliDelTunnelFromReoptimizeList
 * Description   : This routine is used to del tunnel from Reoptimize tunnel list
 * Input(s)      : CliHandle  - Cli Context Handle
 *                 pTeCliArgs - CLI Args
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ********************************************************************************/
INT4
TeCliDelTunnelFromReoptimizeList (tCliHandle CliHandle, tTeCliArgs * pTeCliArgs)
{
    UINT4               u4ErrorCode;

    if ((nmhTestv2FsMplsReoptimizationTunnelRowStatus (&u4ErrorCode,
                                                       pTeCliArgs->
                                                       u4TunnelIndex,
                                                       pTeCliArgs->u4IngressId,
                                                       pTeCliArgs->u4EgressId,
                                                       DESTROY)) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to Delete tunnel from Reoptimization "
                   "tunnel list\r\n");
        return CLI_FAILURE;
    }

    nmhSetFsMplsReoptimizationTunnelRowStatus (pTeCliArgs->u4TunnelIndex,
                                               pTeCliArgs->u4IngressId,
                                               pTeCliArgs->u4EgressId, DESTROY);

    return CLI_SUCCESS;

}

/*******************************************************************************
 * Function Name : TeCliShowReoptimizeTunnelList
 * Description   : This routine is used to show all  Reoptimize tunnels
 * Input(s)      : CliHandle  - Cli Context Handle
 *                 pTeCliArgs - CLI Args
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 ********************************************************************************/
INT4
TeCliShowReoptimizeTunnelList (tCliHandle CliHandle)
{
    tTeReoptTnlInfo    *pTeReoptTnlInfo = NULL;

    pTeReoptTnlInfo =
        (tTeReoptTnlInfo *) RBTreeGetFirst (gTeGblInfo.ReoptimizeTnlList);

    if (pTeReoptTnlInfo == NULL)
    {
        CliPrintf (CliHandle, "\r\n%%Reoptimization Tunnel List is empty\r\n");
        return CLI_FAILURE;
    }

    CliPrintf (CliHandle, "\nTunnel ID\t Source    \tDestination\r\n");
    while (pTeReoptTnlInfo != NULL)
    {
        CliPrintf (CliHandle, " %d\t\t", pTeReoptTnlInfo->u4ReoptTnlIndex);
        CliPrintf (CliHandle, " %d.%d.%d.%d\t",
                   pTeReoptTnlInfo->ReoptTnlIngressLsrId[0],
                   pTeReoptTnlInfo->ReoptTnlIngressLsrId[1],
                   pTeReoptTnlInfo->ReoptTnlIngressLsrId[2],
                   pTeReoptTnlInfo->ReoptTnlIngressLsrId[3]);
        CliPrintf (CliHandle, " %d.%d.%d.%d\t",
                   pTeReoptTnlInfo->ReoptTnlEgressLsrId[0],
                   pTeReoptTnlInfo->ReoptTnlEgressLsrId[1],
                   pTeReoptTnlInfo->ReoptTnlEgressLsrId[2],
                   pTeReoptTnlInfo->ReoptTnlEgressLsrId[3]);
        CliPrintf (CliHandle, "\r\n");

        pTeReoptTnlInfo =
            (tTeReoptTnlInfo *) RBTreeGetNext (gTeGblInfo.ReoptimizeTnlList,
                                               (tRBElem *) pTeReoptTnlInfo,
                                               NULL);
    }
    return CLI_SUCCESS;
}

/**********************************************************************************
 * Function Name : TeCliTriggerLspReoptimization
 * Description   : This routine is used for the manual trigger of LSP Reoptmization
 * Input(s)      : CliHandle  - Cli Context Handle
 *                 pTeCliArgs - CLI Args
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 **********************************************************************************/
INT4
TeCliTriggerManualLspReoptimization (tCliHandle CliHandle,
                                     tTeCliArgs * pTeCliArgs)
{

    UINT4               u4ErrorCode = TE_ZERO;

    if (nmhTestv2FsMplsReoptimizationTunnelManualTrigger (&u4ErrorCode,
                                                          pTeCliArgs->
                                                          u4TunnelIndex,
                                                          pTeCliArgs->
                                                          u4IngressId,
                                                          pTeCliArgs->
                                                          u4EgressId,
                                                          TE_REOPT_MANUAL_TRIGGER)
        == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to Trigger LSP Reoptimization, Check Reoptimization is"
                   " enabled or not\r\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsMplsReoptimizationTunnelManualTrigger
        (pTeCliArgs->u4TunnelIndex, pTeCliArgs->u4IngressId,
         pTeCliArgs->u4EgressId, TE_REOPT_MANUAL_TRIGGER) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to Trigger LSP Reoptimization, Check Reoptimization is"
                   " enabled or not\r");
        CliPrintf (CliHandle,
                   "\r\n%%Manual Trigger is not applicable on Egress/"
                   "Transit(except Mid-Node) Node\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/**********************************************************************************
 * Function Name : TeCliEnableLspReoptimization
 * Description   : This routine is used for the manual trigger of LSP Reoptmization
 * Input(s)      : CliHandle  - Cli Context Handle
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 **********************************************************************************/
INT4
TeCliEnableLspReoptimization (tCliHandle CliHandle)
{
    UINT4               u4TunnelIndex = TE_ZERO;
    UINT4               u4TunnelInstance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    UINT4               u4ErrorCode = TE_ZERO;

    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    if ((nmhTestv2FsMplsTunnelInitReOptimize (&u4ErrorCode,
                                              u4TunnelIndex, u4TunnelInstance,
                                              u4IngressId, u4EgressId,
                                              TE_REOPTIMIZE_ENABLE)) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to enable LSP Reoptimization, test failed\r\n");
        return CLI_FAILURE;
    }

    if ((nmhSetFsMplsTunnelInitReOptimize (u4TunnelIndex, u4TunnelInstance,
                                           u4IngressId, u4EgressId,
                                           TE_REOPTIMIZE_ENABLE)) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to enable LSP Reoptimization, set failed\r\n");
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/**********************************************************************************
 * Function Name : TeCliDisableLspReoptimization
 * Description   : This routine is used for the manual trigger of LSP Reoptmization
 * Input(s)      : CliHandle  - Cli Context Handle
 * Output(s)     : None
 * Return(s)     : CLI_SUCCESS or CLI_FAILURE
 **********************************************************************************/
INT4
TeCliDisableLspReoptimization (tCliHandle CliHandle)
{
    UINT4               u4TunnelIndex = TE_ZERO;
    UINT4               u4TunnelInstance = TE_ZERO;
    UINT4               u4IngressId = TE_ZERO;
    UINT4               u4EgressId = TE_ZERO;
    UINT4               u4ErrorCode = TE_ZERO;

    if (TeCliGetTnlIndices (CliHandle, &u4TunnelIndex, &u4TunnelInstance,
                            &u4IngressId, &u4EgressId) == CLI_FAILURE)
    {
        return CLI_FAILURE;
    }

    if ((nmhTestv2FsMplsTunnelInitReOptimize (&u4ErrorCode,
                                              u4TunnelIndex, u4TunnelInstance,
                                              u4IngressId, u4EgressId,
                                              TE_REOPTIMIZE_DISABLE)) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to enable LSP Reoptimization, test failed\r\n");
        return CLI_FAILURE;
    }

    if ((nmhSetFsMplsTunnelInitReOptimize (u4TunnelIndex, u4TunnelInstance,
                                           u4IngressId, u4EgressId,
                                           TE_REOPTIMIZE_DISABLE)) ==
        SNMP_FAILURE)
    {
        CliPrintf (CliHandle,
                   "\r\n%%Unable to enable LSP Reoptimization, set failed\r\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************
 * Function Name : TeCliDeleteMplsDiffServElspInfoRowStatus
 * Description   : This routine is used to Delete Elsp Info Phb Map Table.
 * * Input(s)      : i4ElspInfoListIndex - Index of the Map to be created
   *                 i4ElspInfoIndex - Index of Exp-phb mapping
 * Output(s)     : None
 * Return(s)     : SNMP_SUCCESS or SNMP_FAILURE
 *******************************************************************************/
INT1
TeCliDeleteMplsDiffServElspInfoRowStatus (INT4 i4ElspInfoListIndex,
                                          INT4 i4ElspInfoIndex)
{
    UINT4               u4ErrorCode = TE_ZERO;

    if (nmhTestv2FsMplsDiffServElspInfoRowStatus (&u4ErrorCode,
                                                  i4ElspInfoListIndex,
                                                  i4ElspInfoIndex,
                                                  TE_DESTROY) == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }

    if (nmhSetFsMplsDiffServElspInfoRowStatus (i4ElspInfoListIndex,
                                               i4ElspInfoIndex, TE_DESTROY)
        == SNMP_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

#endif
