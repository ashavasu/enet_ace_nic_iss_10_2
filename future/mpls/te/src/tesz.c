/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tesz.c,v 1.5 2014/12/12 11:56:47 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _TESZ_C
#include "teincs.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
TeSizingMemCreateMemPools ()
{
    UINT4                u4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < TE_MAX_SIZING_ID; i4SizingId++)
    {
        u4RetVal = MemCreateMemPool (FsTESizingParams[i4SizingId].u4StructSize,
                                     FsTESizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(TEMemPoolIds[i4SizingId]));
        if (u4RetVal ==  MEM_FAILURE)
        {
            TeSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
TeSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsTESizingParams);
    IssSzRegisterModulePoolId (pu1ModName, TEMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
TeSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < TE_MAX_SIZING_ID; i4SizingId++)
    {
        if (TEMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (TEMemPoolIds[i4SizingId]);
            TEMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
