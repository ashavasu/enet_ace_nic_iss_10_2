/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tetest2.c,v 1.18 2015/06/26 05:14:56 siva Exp $
 *
 * Description: This file contains the low level TEST routines
 *              for the following TE MIB tables.
 *              - MplsTunnelHopTable
 *              - MplsTunnelResourceTable
 *              - MplsTunnelCRLDPResTable
 *              - FsMplsTunnelCRLDPResTable
 *              - FsMplsTunnelRSVPResTable
 *----------------------------------------------------------------------------*/

#include "teincs.h"
#include "fsmplslw.h"
#include "stdtelw.h"
#include "fsmptelw.h"

/* LOW LEVEL Routines for Table : MplsTunnelHopTable. */

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelHopAddrType
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                testValMplsTunnelHopAddrType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelHopAddrType (UINT4 *pu4ErrorCode,
                                UINT4 u4MplsTunnelHopListIndex,
                                UINT4 u4MplsTunnelHopPathOptionIndex,
                                UINT4 u4MplsTunnelHopIndex,
                                INT4 i4TestValMplsTunnelHopAddrType)
{
    tTeHopInfo         *pTeHopInfo = NULL;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) != TE_SUCCESS)
    {

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    switch (i4TestValMplsTunnelHopAddrType)
    {
        case TE_ERHOP_IPV4_TYPE:
        case TE_ERHOP_UNNUM_TYPE:
        case TE_ERHOP_LSPID_TYPE:
            break;

        case TE_ERHOP_IPV6_TYPE:
        case TE_ERHOP_ASNUM_TYPE:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelHopIpAddr
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                testValMplsTunnelHopIpAddr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelHopIpAddr (UINT4 *pu4ErrorCode,
                              UINT4 u4MplsTunnelHopListIndex,
                              UINT4 u4MplsTunnelHopPathOptionIndex,
                              UINT4 u4MplsTunnelHopIndex,
                              tSNMP_OCTET_STRING_TYPE *
                              pTestValMplsTunnelHopIpAddr)
{
    UINT4               u4TestValMplsTunnelHopIpAddr = 0;
    tTeHopInfo         *pTeHopInfo = NULL;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if ((pTestValMplsTunnelHopIpAddr->i4_Length < MIN_TE_HOP_ADDRESS_SIZE) ||
        (pTestValMplsTunnelHopIpAddr->i4_Length > MAX_TE_HOP_ADDRESS_SIZE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) != TE_SUCCESS)
    {

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MEMCPY (&u4TestValMplsTunnelHopIpAddr,
            pTestValMplsTunnelHopIpAddr->pu1_OctetList, sizeof (UINT4));
    u4TestValMplsTunnelHopIpAddr = OSIX_NTOHL (u4TestValMplsTunnelHopIpAddr);
    /* Ipv4 Addr should not be
     *      - NULL (0x00000000)
     *      - Broadcast Address
     *      - Multi-cast Address
     */
    if ((u4TestValMplsTunnelHopIpAddr == TE_MIN_UINT4_VALUE) ||
        (u4TestValMplsTunnelHopIpAddr == TE_MAX_UINT4_VALUE) ||
        ((u4TestValMplsTunnelHopIpAddr >= TE_MIN_MCAST_ADDR_VALUE) &&
         (u4TestValMplsTunnelHopIpAddr <= TE_MAX_MCAST_ADDR_VALUE)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_ERHOP_ROW_STATUS (pTeHopInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelHopIpPrefixLen
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                testValMplsTunnelHopIpPrefixLen
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelHopIpPrefixLen (UINT4 *pu4ErrorCode,
                                   UINT4 u4MplsTunnelHopListIndex,
                                   UINT4 u4MplsTunnelHopPathOptionIndex,
                                   UINT4 u4MplsTunnelHopIndex,
                                   UINT4 u4TestValMplsTunnelHopIpPrefixLen)
{
    tTeHopInfo         *pTeHopInfo = NULL;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) != TE_SUCCESS)
    {

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if ((u4TestValMplsTunnelHopIpPrefixLen < IPV4_MIN_PRFX_LEN) ||
        (u4TestValMplsTunnelHopIpPrefixLen > IPV4_MAX_PRFX_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_ERHOP_ROW_STATUS (pTeHopInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelHopAsNumber
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                testValMplsTunnelHopAsNumber
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelHopAsNumber (UINT4 *pu4ErrorCode,
                                UINT4 u4MplsTunnelHopListIndex,
                                UINT4 u4MplsTunnelHopPathOptionIndex,
                                UINT4 u4MplsTunnelHopIndex,
                                tSNMP_OCTET_STRING_TYPE *
                                pTestValMplsTunnelHopAsNumber)
{
    tTeHopInfo         *pTeHopInfo = NULL;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (pTestValMplsTunnelHopAsNumber->i4_Length != TE_HOP_ADDR_AS_SIZE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) != TE_SUCCESS)
    {

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* Currently Not supported */
    UNUSED_PARAM (pTestValMplsTunnelHopAsNumber);
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelHopAddrUnnum
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object
                testValMplsTunnelHopAddrUnnum
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelHopAddrUnnum (UINT4 *pu4ErrorCode,
                                 UINT4 u4MplsTunnelHopListIndex,
                                 UINT4 u4MplsTunnelHopPathOptionIndex,
                                 UINT4 u4MplsTunnelHopIndex,
                                 tSNMP_OCTET_STRING_TYPE *
                                 pTestValMplsTunnelHopAddrUnnum)
{
    tTeHopInfo         *pTeHopInfo = NULL;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (pTestValMplsTunnelHopAddrUnnum->i4_Length != sizeof (UINT4))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) != TE_SUCCESS)
    {

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelHopLspId
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                testValMplsTunnelHopLspId
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelHopLspId (UINT4 *pu4ErrorCode,
                             UINT4 u4MplsTunnelHopListIndex,
                             UINT4 u4MplsTunnelHopPathOptionIndex,
                             UINT4 u4MplsTunnelHopIndex,
                             tSNMP_OCTET_STRING_TYPE *
                             pTestValMplsTunnelHopLspId)
{
    tTeHopInfo         *pTeHopInfo = NULL;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if ((pTestValMplsTunnelHopLspId->i4_Length != RSVP_TE_LSP_ID_LEN) &&
        (pTestValMplsTunnelHopLspId->i4_Length != CRLDP_LSP_ID_LEN))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) != TE_SUCCESS)
    {

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_ERHOP_ADDR_TYPE (pTeHopInfo) != TE_ERHOP_LSPID_TYPE)
    {
        /* Allow to set this only if Address type is LSPID
         * as per MIB */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_ERHOP_ROW_STATUS (pTeHopInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelHopType
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                testValMplsTunnelHopType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelHopType (UINT4 *pu4ErrorCode, UINT4 u4MplsTunnelHopListIndex,
                            UINT4 u4MplsTunnelHopPathOptionIndex,
                            UINT4 u4MplsTunnelHopIndex,
                            INT4 i4TestValMplsTunnelHopType)
{
    tTeHopInfo         *pTeHopInfo = NULL;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) != TE_SUCCESS)
    {

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_ERHOP_ROW_STATUS (pTeHopInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    switch (i4TestValMplsTunnelHopType)
    {
        case TE_LOOSE_ER:
        case TE_STRICT_ER:
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            break;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelHopInclude
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                testValMplsTunnelHopInclude
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelHopInclude (UINT4 *pu4ErrorCode,
                               UINT4 u4MplsTunnelHopListIndex,
                               UINT4 u4MplsTunnelHopPathOptionIndex,
                               UINT4 u4MplsTunnelHopIndex,
                               INT4 i4TestValMplsTunnelHopInclude)
{
    tTeHopInfo         *pTeHopInfo = NULL;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) != TE_SUCCESS)
    {

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if ((i4TestValMplsTunnelHopInclude != TE_SNMP_TRUE) &&
        (i4TestValMplsTunnelHopInclude != TE_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelHopPathOptionName
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                testValMplsTunnelHopPathOptionName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelHopPathOptionName (UINT4 *pu4ErrorCode,
                                      UINT4 u4MplsTunnelHopListIndex,
                                      UINT4 u4MplsTunnelHopPathOptionIndex,
                                      UINT4 u4MplsTunnelHopIndex,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pTestValMplsTunnelHopPathOptionName)
{
    tTeHopInfo         *pTeHopInfo = NULL;
    UINT4               u4PathListIndex = TE_ONE;
    tTePathListInfo    *pTePathListInfo = NULL;
    tTePathInfo        *pTePathInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (pTestValMplsTunnelHopPathOptionName->i4_Length < 0 ||
        pTestValMplsTunnelHopPathOptionName->i4_Length > 255)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) != TE_SUCCESS)
    {

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_ERHOP_ROW_STATUS (pTeHopInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* Scan through all the hopinfo and if PathOptionIndex are matching the
     * compare OptionName. For the same Option Index name should be same */
    for (u4PathListIndex = TE_ONE;
         u4PathListIndex <= TE_MAX_HOP_LIST (gTeGblInfo); u4PathListIndex++)
    {
        if ((pTePathListInfo = TE_PATH_LIST_ENTRY (u4PathListIndex)) != TE_ZERO)
        {
            TE_SLL_SCAN ((TE_HOP_PO_LIST (pTePathListInfo)), pTePathInfo,
                         tTePathInfo *)
            {
                if (TE_PO_INDEX (pTePathInfo) == u4MplsTunnelHopPathOptionIndex)
                {
                    TE_SLL_SCAN ((TE_PO_HOP_LIST (pTePathInfo)), pTeHopInfo,
                                 tTeHopInfo *)
                    {
                        if (STRLEN (pTeHopInfo->au1PathOptionName) != 0)
                        {
                            if (MEMCMP (pTeHopInfo->au1PathOptionName,
                                        pTestValMplsTunnelHopPathOptionName->
                                        pu1_OctetList,
                                        pTestValMplsTunnelHopPathOptionName->
                                        i4_Length) != 0)
                            {
                                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                                MPLS_CMN_UNLOCK ();
                                return SNMP_FAILURE;
                            }
                            MPLS_CMN_UNLOCK ();
                            return SNMP_SUCCESS;
                        }
                    }
                }
            }
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelHopEntryPathComp
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                testValMplsTunnelHopEntryPathComp
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelHopEntryPathComp (UINT4 *pu4ErrorCode,
                                     UINT4 u4MplsTunnelHopListIndex,
                                     UINT4 u4MplsTunnelHopPathOptionIndex,
                                     UINT4 u4MplsTunnelHopIndex,
                                     INT4 i4TestValMplsTunnelHopEntryPathComp)
{
    tTeHopInfo         *pTeHopInfo = NULL;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) != TE_SUCCESS)
    {

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if ((i4TestValMplsTunnelHopEntryPathComp != TE_DYNAMIC) &&
        (i4TestValMplsTunnelHopEntryPathComp != TE_EXPLICIT))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelHopRowStatus
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                testValMplsTunnelHopRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelHopRowStatus (UINT4 *pu4ErrorCode,
                                 UINT4 u4MplsTunnelHopListIndex,
                                 UINT4 u4MplsTunnelHopPathOptionIndex,
                                 UINT4 u4MplsTunnelHopIndex,
                                 INT4 i4TestValMplsTunnelHopRowStatus)
{
    INT1                i1RetVal;
    tTeHopInfo         *pTeHopInfo = NULL;

    MPLS_CMN_LOCK ();

    if (u4MplsTunnelHopListIndex > (TE_MAX_HOP_LIST (gTeGblInfo) / 2))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if ((u4MplsTunnelHopPathOptionIndex > TE_MAX_PO_PER_HOP_LIST (gTeGblInfo)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    i1RetVal = TeCheckHopInfo (u4MplsTunnelHopListIndex,
                               u4MplsTunnelHopPathOptionIndex,
                               u4MplsTunnelHopIndex, &pTeHopInfo);

    switch (i4TestValMplsTunnelHopRowStatus)
    {
        case TE_CREATEANDWAIT:

            if (i1RetVal == TE_SUCCESS)
            {
                TE_DBG (TE_LLVL_TEST_FAIL,
                        "Manager is trying to create an existing Row");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            break;
        case TE_ACTIVE:

            if (i1RetVal == TE_FAILURE)
            {
                TE_DBG (TE_LLVL_TEST_FAIL,
                        "Manager is trying to access non existing Row");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            if ((TE_ERHOP_ROW_STATUS (pTeHopInfo) != TE_NOTREADY) &&
                (TE_ERHOP_ROW_STATUS (pTeHopInfo) != TE_NOTINSERVICE))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            break;
        case TE_NOTINSERVICE:

            if (i1RetVal == TE_FAILURE)
            {
                TE_DBG (TE_LLVL_TEST_FAIL,
                        "Manager is trying to access non existing Row");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            break;
        case TE_DESTROY:

            if (i1RetVal == TE_FAILURE)
            {
                TE_DBG (TE_LLVL_TEST_FAIL,
                        "Manager is trying to access non existing Row");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            break;

        case TE_CREATEANDGO:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            TE_DBG (TE_LLVL_TEST_FAIL, "Create and GO Not Supported \n");

            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelHopStorageType
 Input       :  The Indices
                MplsTunnelHopListIndex
                MplsTunnelHopPathOptionIndex
                MplsTunnelHopIndex

                The Object 
                testValMplsTunnelHopStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelHopStorageType (UINT4 *pu4ErrorCode,
                                   UINT4 u4MplsTunnelHopListIndex,
                                   UINT4 u4MplsTunnelHopPathOptionIndex,
                                   UINT4 u4MplsTunnelHopIndex,
                                   INT4 i4TestValMplsTunnelHopStorageType)
{
    tTeHopInfo         *pTeHopInfo = NULL;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TeCheckHopInfo (u4MplsTunnelHopListIndex,
                        u4MplsTunnelHopPathOptionIndex,
                        u4MplsTunnelHopIndex, &pTeHopInfo) != TE_SUCCESS)
    {

        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    switch (i4TestValMplsTunnelHopStorageType)
    {
        case TE_STORAGE_VOLATILE:
        case TE_STORAGE_NONVOLATILE:
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            break;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : MplsTunnelResourceTable. */

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelResourceMaxRate
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                testValMplsTunnelResourceMaxRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelResourceMaxRate (UINT4 *pu4ErrorCode,
                                    UINT4 u4MplsTunnelResourceIndex,
                                    UINT4 u4TestValMplsTunnelResourceMaxRate)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    UNUSED_PARAM (u4TestValMplsTunnelResourceMaxRate);

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_TNLRSRC_ROW_STATUS (pTeTrfcParams) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelResourceMeanRate
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                testValMplsTunnelResourceMeanRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelResourceMeanRate (UINT4 *pu4ErrorCode,
                                     UINT4 u4MplsTunnelResourceIndex,
                                     UINT4 u4TestValMplsTunnelResourceMeanRate)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_TNLRSRC_ROW_STATUS (pTeTrfcParams) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (u4TestValMplsTunnelResourceMeanRate);
    /* TODO Not Supported */
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelResourceMaxBurstSize
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                testValMplsTunnelResourceMaxBurstSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelResourceMaxBurstSize (UINT4 *pu4ErrorCode,
                                         UINT4 u4MplsTunnelResourceIndex,
                                         UINT4
                                         u4TestValMplsTunnelResourceMaxBurstSize)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_TNLRSRC_ROW_STATUS (pTeTrfcParams) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (u4TestValMplsTunnelResourceMaxBurstSize);
    /* TODO Not Supported */
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelResourceMeanBurstSize
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                testValMplsTunnelResourceMeanBurstSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelResourceMeanBurstSize (UINT4 *pu4ErrorCode,
                                          UINT4 u4MplsTunnelResourceIndex,
                                          UINT4
                                          u4TestValMplsTunnelResourceMeanBurstSize)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_TNLRSRC_ROW_STATUS (pTeTrfcParams) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (u4TestValMplsTunnelResourceMeanBurstSize);
    /* TODO Not Supported */
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelResourceExBurstSize
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                testValMplsTunnelResourceExBurstSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelResourceExBurstSize (UINT4 *pu4ErrorCode,
                                        UINT4 u4MplsTunnelResourceIndex,
                                        UINT4
                                        u4TestValMplsTunnelResourceExBurstSize)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_TNLRSRC_ROW_STATUS (pTeTrfcParams) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    UNUSED_PARAM (u4TestValMplsTunnelResourceExBurstSize);
    /* TODO Not Supported */
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelResourceFrequency
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                testValMplsTunnelResourceFrequency
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelResourceFrequency (UINT4 *pu4ErrorCode,
                                      UINT4 u4MplsTunnelResourceIndex,
                                      INT4 i4TestValMplsTunnelResourceFrequency)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    switch (i4TestValMplsTunnelResourceFrequency)
    {
        case TE_TNL_FREQ_UNSPECIFIED:
        case TE_TNL_FREQ_FREQUENT:
        case TE_TNL_FREQ_VERYFREQ:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
    }

    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_TNLRSRC_ROW_STATUS (pTeTrfcParams) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* TODO Not Supported */
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelResourceWeight
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                testValMplsTunnelResourceWeight
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelResourceWeight (UINT4 *pu4ErrorCode,
                                   UINT4 u4MplsTunnelResourceIndex,
                                   UINT4 u4TestValMplsTunnelResourceWeight)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_TNLRSRC_ROW_STATUS (pTeTrfcParams) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (u4TestValMplsTunnelResourceWeight > TE_MAX_RESOURCE_WEIGHT)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    /* TODO Not Supported */
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelResourceRowStatus
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                testValMplsTunnelResourceRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelResourceRowStatus (UINT4 *pu4ErrorCode,
                                      UINT4 u4MplsTunnelResourceIndex,
                                      INT4 i4TestValMplsTunnelResourceRowStatus)
{
    INT4                i4Return = 0;
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    i4Return = TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                                 &pTeTrfcParams);

    switch (i4TestValMplsTunnelResourceRowStatus)
    {
        case TE_CREATEANDWAIT:
            if (i4Return == TE_SUCCESS)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            break;
        case TE_ACTIVE:
        case TE_NOTINSERVICE:
            if (i4Return == TE_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            if (TE_TNLRSRC_ROW_STATUS (pTeTrfcParams) ==
                i4TestValMplsTunnelResourceRowStatus)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            break;
        case TE_DESTROY:
            if (i4Return == TE_FAILURE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelResourceStorageType
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                testValMplsTunnelResourceStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelResourceStorageType (UINT4 *pu4ErrorCode,
                                        UINT4 u4MplsTunnelResourceIndex,
                                        INT4
                                        i4TestValMplsTunnelResourceStorageType)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_FAILURE)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    switch (i4TestValMplsTunnelResourceStorageType)
    {
        case TE_STORAGE_VOLATILE:
        case TE_STORAGE_NONVOLATILE:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : MplsTunnelCRLDPResTable. */

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelCRLDPResMeanBurstSize
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                testValMplsTunnelCRLDPResMeanBurstSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelCRLDPResMeanBurstSize (UINT4 *pu4ErrorCode,
                                          UINT4 u4MplsTunnelResourceIndex,
                                          UINT4
                                          u4TestValMplsTunnelCRLDPResMeanBurstSize)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    if (TE_CRLDP_TPARAM_ROW_STATUS (pTeTrfcParams) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (!((u4TestValMplsTunnelCRLDPResMeanBurstSize >= TE_CRLDP_MIN_CB) &&
          (u4TestValMplsTunnelCRLDPResMeanBurstSize <= TE_CRLDP_MAX_CB)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelCRLDPResExBurstSize
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                testValMplsTunnelCRLDPResExBurstSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelCRLDPResExBurstSize (UINT4 *pu4ErrorCode,
                                        UINT4 u4MplsTunnelResourceIndex,
                                        UINT4
                                        u4TestValMplsTunnelCRLDPResExBurstSize)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    if (TE_CRLDP_TPARAM_ROW_STATUS (pTeTrfcParams) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (!((u4TestValMplsTunnelCRLDPResExBurstSize >= TE_CRLDP_MIN_EB) &&
          (u4TestValMplsTunnelCRLDPResExBurstSize <= TE_CRLDP_MAX_EB)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelCRLDPResFrequency
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                testValMplsTunnelCRLDPResFrequency
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelCRLDPResFrequency (UINT4 *pu4ErrorCode,
                                      UINT4 u4MplsTunnelResourceIndex,
                                      INT4 i4TestValMplsTunnelCRLDPResFrequency)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    switch (i4TestValMplsTunnelCRLDPResFrequency)
    {
        case TE_TNL_FREQ_UNSPECIFIED:
        case TE_TNL_FREQ_FREQUENT:
        case TE_TNL_FREQ_VERYFREQ:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
    }

    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    if (TE_CRLDP_TPARAM_ROW_STATUS (pTeTrfcParams) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelCRLDPResWeight
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                testValMplsTunnelCRLDPResWeight
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelCRLDPResWeight (UINT4 *pu4ErrorCode,
                                   UINT4 u4MplsTunnelResourceIndex,
                                   UINT4 u4TestValMplsTunnelCRLDPResWeight)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    if (TE_CRLDP_TPARAM_ROW_STATUS (pTeTrfcParams) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (!(u4TestValMplsTunnelCRLDPResWeight <= TE_MAX_WGT_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelCRLDPResFlags
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                testValMplsTunnelCRLDPResFlags
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelCRLDPResFlags (UINT4 *pu4ErrorCode,
                                  UINT4 u4MplsTunnelResourceIndex,
                                  UINT4 u4TestValMplsTunnelCRLDPResFlags)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    if (TE_CRLDP_TPARAM_ROW_STATUS (pTeTrfcParams) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (!(u4TestValMplsTunnelCRLDPResFlags <= TE_MAX_RES_FLAG_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelCRLDPResRowStatus
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                testValMplsTunnelCRLDPResRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelCRLDPResRowStatus (UINT4 *pu4ErrorCode,
                                      UINT4 u4MplsTunnelResourceIndex,
                                      INT4 i4TestValMplsTunnelCRLDPResRowStatus)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;
    UINT1               u1Flag = TE_FALSE;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) != NULL)
        {
            u1Flag = TE_TRUE;
        }
    }

    switch (i4TestValMplsTunnelCRLDPResRowStatus)
    {
        case TE_CREATEANDWAIT:
            if (u1Flag == TE_TRUE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            break;
        case TE_ACTIVE:
            if (u1Flag == TE_FALSE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            if ((TE_CRLDP_TPARAM_ROW_STATUS (pTeTrfcParams) !=
                 TE_NOTREADY) ||
                (TE_CRLDP_TPARAM_CFG_FLAGS (pTeTrfcParams) !=
                 TE_CRLDP_DESIRED_CFG_FLAG))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            break;
        case TE_DESTROY:
        case TE_NOTINSERVICE:
            if (u1Flag == TE_FALSE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelCRLDPResStorageType
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                testValMplsTunnelCRLDPResStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelCRLDPResStorageType (UINT4 *pu4ErrorCode,
                                        UINT4 u4MplsTunnelResourceIndex,
                                        INT4
                                        i4TestValMplsTunnelCRLDPResStorageType)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) == NULL)
        {
            *pu4ErrorCode = SNMP_ERR_NO_CREATION;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    switch (i4TestValMplsTunnelCRLDPResStorageType)
    {
        case TE_STORAGE_VOLATILE:
        case TE_STORAGE_NONVOLATILE:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;

}

/* LOW LEVEL Routines for Table : FsMplsTunnelCRLDPResTable. */

/****************************************************************************
 Function    :  nmhTestv2FsMplsTunnelCRLDPResPeakDataRate
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                testValFsMplsTunnelCRLDPResPeakDataRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsTunnelCRLDPResPeakDataRate (UINT4 *pu4ErrorCode,
                                           UINT4 u4MplsTunnelResourceIndex,
                                           UINT4
                                           u4TestValFsMplsTunnelCRLDPResPeakDataRate)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if ((TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                           &pTeTrfcParams) == TE_FAILURE) ||
        (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) == NULL))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_CRLDP_TPARAM_ROW_STATUS (pTeTrfcParams) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if ((u4TestValFsMplsTunnelCRLDPResPeakDataRate >= TE_CRLDP_MIN_PD) &&
        (u4TestValFsMplsTunnelCRLDPResPeakDataRate <= TE_CRLDP_MAX_PD))
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsTunnelCRLDPResCommittedDataRate
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                testValFsMplsTunnelCRLDPResCommittedDataRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsTunnelCRLDPResCommittedDataRate (UINT4 *pu4ErrorCode,
                                                UINT4 u4MplsTunnelResourceIndex,
                                                UINT4
                                                u4TestValFsMplsTunnelCRLDPResCommittedDataRate)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_AUTHORIZATION_ERROR;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if ((TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                           &pTeTrfcParams) == TE_FAILURE) ||
        (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) == NULL))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_CRLDP_TPARAM_ROW_STATUS (pTeTrfcParams) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if ((u4TestValFsMplsTunnelCRLDPResCommittedDataRate >= TE_CRLDP_MIN_CD) &&
        (u4TestValFsMplsTunnelCRLDPResCommittedDataRate <= TE_CRLDP_MAX_CD))
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsTunnelCRLDPResPeakBurstSize
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                testValFsMplsTunnelCRLDPResPeakBurstSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsTunnelCRLDPResPeakBurstSize (UINT4 *pu4ErrorCode,
                                            UINT4 u4MplsTunnelResourceIndex,
                                            UINT4
                                            u4TestValFsMplsTunnelCRLDPResPeakBurstSize)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_AUTHORIZATION_ERROR;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if ((TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                           &pTeTrfcParams) == TE_FAILURE) ||
        (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) == NULL))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_CRLDP_TPARAM_ROW_STATUS (pTeTrfcParams) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if ((u4TestValFsMplsTunnelCRLDPResPeakBurstSize >= TE_CRLDP_MIN_PB) &&
        (u4TestValFsMplsTunnelCRLDPResPeakBurstSize <= TE_CRLDP_MAX_PB))
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsTunnelCRLDPResCommittedBurstSize
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                testValFsMplsTunnelCRLDPResCommittedBurstSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsTunnelCRLDPResCommittedBurstSize (UINT4 *pu4ErrorCode,
                                                 UINT4
                                                 u4MplsTunnelResourceIndex,
                                                 UINT4
                                                 u4TestValFsMplsTunnelCRLDPResCommittedBurstSize)
{
    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_AUTHORIZATION_ERROR;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if ((TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                           &pTeTrfcParams) == TE_FAILURE) ||
        (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) == NULL))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_CRLDP_TPARAM_ROW_STATUS (pTeTrfcParams) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if ((u4TestValFsMplsTunnelCRLDPResCommittedBurstSize
         >= TE_CRLDP_MIN_CB) &&
        (u4TestValFsMplsTunnelCRLDPResCommittedBurstSize <= TE_CRLDP_MAX_CB))
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    MPLS_CMN_UNLOCK ();
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsTunnelCRLDPResExcessBurstSize
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                testValFsMplsTunnelCRLDPResExcessBurstSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsTunnelCRLDPResExcessBurstSize (UINT4 *pu4ErrorCode,
                                              UINT4 u4MplsTunnelResourceIndex,
                                              UINT4
                                              u4TestValFsMplsTunnelCRLDPResExcessBurstSize)
{

    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_AUTHORIZATION_ERROR;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if ((TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                           &pTeTrfcParams) == TE_FAILURE) ||
        (TE_CRLDP_TRFC_PARAMS (pTeTrfcParams) == NULL))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_CRLDP_TPARAM_ROW_STATUS (pTeTrfcParams) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if ((u4TestValFsMplsTunnelCRLDPResExcessBurstSize >= TE_CRLDP_MIN_EB) &&
        (u4TestValFsMplsTunnelCRLDPResExcessBurstSize <= TE_CRLDP_MAX_EB))
    {
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/* LOW LEVEL Routines for Table : FsMplsTunnelRSVPResTable. */

/****************************************************************************
 Function    :  nmhTestv2FsMplsTunnelRSVPResTokenBucketRate
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                testValFsMplsTunnelRSVPResTokenBucketRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsTunnelRSVPResTokenBucketRate (UINT4 *pu4ErrorCode,
                                             UINT4 u4MplsTunnelResourceIndex,
                                             UINT4
                                             u4TestValFsMplsTunnelRSVPResTokenBucketRate)
{

    tTeTrfcParams      *pTeTrfcParams = NULL;
    UINT4               u4TnlId = 0;
    UINT4               u4TnlInstance = 0;
    UINT4               u4TnlIngressLSrId = 0;
    UINT4               u4TnlEgressLsrId = 0;
    tSNMP_OID_TYPE      GetResourcePointer;
    static UINT4        au4GetRsrcPointer[MPLS_TE_RSRC_TABLE_MAX_RATE_OFFSET];
    UINT4               u4GetTunnelResourceIndex = 0;
    INT4                i4TnlMBBStatus = MPLS_TE_MBB_DISABLED;
    INT4                i4TunnelAdminStatus = 0;

    GetResourcePointer.pu4_OidList = au4GetRsrcPointer;
    MEMSET (GetResourcePointer.pu4_OidList, 0, sizeof (UINT4) * 2);
    GetResourcePointer.u4_Length = 0;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_AUTHORIZATION_ERROR;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (u4TestValFsMplsTunnelRSVPResTokenBucketRate > TE_RPTE_TBR_MAXVAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if ((TE_RSVPTE_TRFC_PARAMS (pTeTrfcParams) != NULL) &&
            (TE_TRFC_PARAMS_TNL_RESROWSTATUS (u4MplsTunnelResourceIndex) !=
             TE_ACTIVE))
        {
            MPLS_CMN_UNLOCK ();
	    if ((nmhGetFirstIndexMplsTunnelTable (&u4TnlId, &u4TnlInstance,
                                          &u4TnlIngressLSrId,
                                          &u4TnlEgressLsrId)) == SNMP_SUCCESS)
            {
                do
                {
                      if ((nmhGetMplsTunnelResourcePointer
                         (u4TnlId, u4TnlInstance, u4TnlIngressLSrId, u4TnlEgressLsrId,
                         &GetResourcePointer)) == SNMP_SUCCESS)
                      {
                            u4GetTunnelResourceIndex =
                                  GetResourcePointer.pu4_OidList[TE_TNL_RSRC_TABLE_DEF_OFFSET - 1];

                            if (u4MplsTunnelResourceIndex == u4GetTunnelResourceIndex)
                            {
                                  nmhGetFsMplsTunnelMBBStatus (u4TnlId, u4TnlInstance,
                                                               u4TnlIngressLSrId, u4TnlEgressLsrId,
                                                               &i4TnlMBBStatus);

                                  nmhGetMplsTunnelAdminStatus (u4TnlId, u4TnlInstance,
                                                               u4TnlIngressLSrId, u4TnlEgressLsrId,
                                                               &i4TunnelAdminStatus);
                                  if ((i4TnlMBBStatus == MPLS_TE_MBB_DISABLED)&&
                                      (i4TunnelAdminStatus == TE_ADMIN_UP))
                                  {
                                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                                        return SNMP_FAILURE;
                                  }
                            }

                      }

                }
                while ((nmhGetNextIndexMplsTunnelTable (u4TnlId, &u4TnlId,
                                                        u4TnlInstance,
                                                        &u4TnlInstance,
                                                        u4TnlIngressLSrId,
                                                        &u4TnlIngressLSrId,
                                                        u4TnlEgressLsrId,
 							&u4TnlEgressLsrId))
                  == SNMP_SUCCESS);
           }
            return SNMP_SUCCESS;
        }
    }

    MPLS_CMN_UNLOCK ();
    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsTunnelRSVPResTokenBucketSize
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                testValFsMplsTunnelRSVPResTokenBucketSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsTunnelRSVPResTokenBucketSize (UINT4 *pu4ErrorCode,
                                             UINT4 u4MplsTunnelResourceIndex,
                                             UINT4
                                             u4TestValFsMplsTunnelRSVPResTokenBucketSize)
{

    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_AUTHORIZATION_ERROR;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (u4TestValFsMplsTunnelRSVPResTokenBucketSize > TE_RPTE_TBS_MAXVAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if ((TE_RSVPTE_TRFC_PARAMS (pTeTrfcParams) != NULL) &&
            (TE_TRFC_PARAMS_TNL_RESROWSTATUS (u4MplsTunnelResourceIndex) !=
             TE_ACTIVE))
        {
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }

    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsTunnelRSVPResPeakDataRate
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                testValFsMplsTunnelRSVPResPeakDataRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsTunnelRSVPResPeakDataRate (UINT4 *pu4ErrorCode,
                                          UINT4 u4MplsTunnelResourceIndex,
                                          UINT4
                                          u4TestValFsMplsTunnelRSVPResPeakDataRate)
{

    tTeTrfcParams      *pTeTrfcParams = NULL;
    UINT4               u4TnlId = 0;
    UINT4               u4TnlInstance = 0;
    UINT4               u4TnlIngressLSrId = 0;
    UINT4               u4TnlEgressLsrId = 0;
    tSNMP_OID_TYPE      GetResourcePointer;
    static UINT4        au4GetRsrcPointer[MPLS_TE_RSRC_TABLE_MAX_RATE_OFFSET];
    UINT4               u4GetTunnelResourceIndex = 0;
    INT4                i4TnlMBBStatus = MPLS_TE_MBB_DISABLED;
    INT4                i4TunnelAdminStatus = 0;
    
    GetResourcePointer.pu4_OidList = au4GetRsrcPointer;
    MEMSET (GetResourcePointer.pu4_OidList, 0, sizeof (UINT4) * 2);
    GetResourcePointer.u4_Length = 0;
    

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_AUTHORIZATION_ERROR;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (u4TestValFsMplsTunnelRSVPResPeakDataRate > TE_RPTE_PDR_MAXVAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if ((TE_RSVPTE_TRFC_PARAMS (pTeTrfcParams) != NULL) &&
            (TE_TRFC_PARAMS_TNL_RESROWSTATUS (u4MplsTunnelResourceIndex) !=
             TE_ACTIVE))
        {
            MPLS_CMN_UNLOCK ();
            if ((nmhGetFirstIndexMplsTunnelTable (&u4TnlId, &u4TnlInstance,
                                          &u4TnlIngressLSrId,
                                          &u4TnlEgressLsrId)) == SNMP_SUCCESS)
    	    {
        	do
        	{
                      if ((nmhGetMplsTunnelResourcePointer
         		 (u4TnlId, u4TnlInstance, u4TnlIngressLSrId, u4TnlEgressLsrId,
          		 &GetResourcePointer)) == SNMP_SUCCESS)
                      {
			    u4GetTunnelResourceIndex =
        			  GetResourcePointer.pu4_OidList[TE_TNL_RSRC_TABLE_DEF_OFFSET - 1]; 
                            
                            if (u4MplsTunnelResourceIndex == u4GetTunnelResourceIndex)
                            {
				  nmhGetFsMplsTunnelMBBStatus (u4TnlId, u4TnlInstance,
                                 			       u4TnlIngressLSrId, u4TnlEgressLsrId,
                                                               &i4TnlMBBStatus);

                                  nmhGetMplsTunnelAdminStatus (u4TnlId, u4TnlInstance,
                                 			       u4TnlIngressLSrId, u4TnlEgressLsrId, 
                                                               &i4TunnelAdminStatus);
                                  if ((i4TnlMBBStatus == MPLS_TE_MBB_DISABLED)&&
                                      (i4TunnelAdminStatus == TE_ADMIN_UP))
            			  {
					*pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                 			return SNMP_FAILURE;
            			  }
                            }
                            
                      }

        	}	
        	while ((nmhGetNextIndexMplsTunnelTable (u4TnlId, &u4TnlId,
                	                                u4TnlInstance,
                        	                        &u4TnlInstance,
                                	                u4TnlIngressLSrId,
                                        	        &u4TnlIngressLSrId,
                                                	u4TnlEgressLsrId,
                                              	        &u4TnlEgressLsrId))
              	 == SNMP_SUCCESS);
    	   }
            return SNMP_SUCCESS;
        }
    }

    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsTunnelRSVPResMinimumPolicedUnit
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                testValFsMplsTunnelRSVPResMinimumPolicedUnit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsTunnelRSVPResMinimumPolicedUnit (UINT4 *pu4ErrorCode,
                                                UINT4 u4MplsTunnelResourceIndex,
                                                INT4
                                                i4TestValFsMplsTunnelRSVPResMinimumPolicedUnit)
{

    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_AUTHORIZATION_ERROR;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMplsTunnelRSVPResMinimumPolicedUnit < TE_RPTE_MPU_MINVAL)
        || (i4TestValFsMplsTunnelRSVPResMinimumPolicedUnit
            > TE_RPTE_MPU_MAXVAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if ((TE_RSVPTE_TRFC_PARAMS (pTeTrfcParams) != NULL) &&
            (TE_TRFC_PARAMS_TNL_RESROWSTATUS (u4MplsTunnelResourceIndex) !=
             TE_ACTIVE))
        {
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }

    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsTunnelRSVPResMaximumPacketSize
 Input       :  The Indices
                MplsTunnelResourceIndex

                The Object 
                testValFsMplsTunnelRSVPResMaximumPacketSize
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsTunnelRSVPResMaximumPacketSize (UINT4 *pu4ErrorCode,
                                               UINT4 u4MplsTunnelResourceIndex,
                                               INT4
                                               i4TestValFsMplsTunnelRSVPResMaximumPacketSize)
{

    tTeTrfcParams      *pTeTrfcParams = NULL;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_AUTHORIZATION_ERROR;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if ((i4TestValFsMplsTunnelRSVPResMaximumPacketSize < TE_RPTE_MPS_MINVAL) ||
        (i4TestValFsMplsTunnelRSVPResMaximumPacketSize > TE_RPTE_MPS_MAXVAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TeCheckTrfcParamInTrfcParamTable (u4MplsTunnelResourceIndex,
                                          &pTeTrfcParams) == TE_SUCCESS)
    {
        if ((TE_RSVPTE_TRFC_PARAMS (pTeTrfcParams) != NULL) &&
            (TE_TRFC_PARAMS_TNL_RESROWSTATUS (u4MplsTunnelResourceIndex) !=
             TE_ACTIVE))
        {
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
    }

    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;

}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelNotificationMaxRate
 Input       :  The Indices

                The Object
                testValMplsTunnelNotificationMaxRate
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelNotificationMaxRate (UINT4 *pu4ErrorCode,
                                        UINT4
                                        u4TestValMplsTunnelNotificationMaxRate)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (u4TestValMplsTunnelNotificationMaxRate);
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelNotificationEnable
 Input       :  The Indices

                The Object
                testValMplsTunnelNotificationEnable
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelNotificationEnable (UINT4 *pu4ErrorCode,
                                       INT4
                                       i4TestValMplsTunnelNotificationEnable)
{
    if ((i4TestValMplsTunnelNotificationEnable != MPLS_SNMP_TRUE) &&
        (i4TestValMplsTunnelNotificationEnable != MPLS_SNMP_FALSE))
    {
        /* The variable cannot take this value */
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}
