/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: teds.c,v 1.6 2012/05/08 15:29:39 siva Exp $
 *
 * Description: This file contains all the functions related to
 *              memory allocation of all structures and their
 *              initialisation in the TE diffServ -module.  
 *----------------------------------------------------------------------------*/

#include "teincs.h"
#include "mplsdefs.h"

/******************************************************************************
* Function Name : TeDiffServElspListRowStatusActive
* Description   : The routine is called to check that all the nodes within
*                 the Elsp list are in active state.
*
* Input(s)       i4ElspListIndex - Contains the value of the list index
*                 
* Output(s)      : None
*                                                           
* Return(s)      : TE_SUCCESS or TE_FAILURE
******************************************************************************/
UINT1
TeDiffServElspListRowStatusActive (INT4 i4ElspInfoListIndex)
{
    tMplsDiffServElspInfo *pElspInfoNode = NULL;
    TE_DBG (TE_DIFF_ETEXT,
            "MAIN : TeDiffServElspListRowStatusActive : ENTRY \n");
    TE_SLL_SCAN (TE_DS_GBL_ELSP_INFO_LIST (i4ElspInfoListIndex),
                 pElspInfoNode, tMplsDiffServElspInfo *)
    {
        if (TE_DS_ELSP_INFO_ROW_STATUS (pElspInfoNode) != ACTIVE)
        {
            TE_DBG1 (TE_DIFF_FAIL,
                     "MAIN: The ElspInfo with Index : %d is not Active \n",
                     pElspInfoNode->u1ElspInfoIndex);
            TE_DBG (TE_DIFF_ETEXT,
                    "MAIN: TeDiffServElspListRowStatusActive : INTMD_EXIT \n");
            return TE_FAILURE;
        }
    }
    /*All the nodes are in active state */
    TE_DBG (TE_DIFF_PRCS, "MAIN: All the ElspInfo's are Active \n");
    TE_DBG (TE_DIFF_ETEXT, "MAIN: TeDiffServElspListRowStatusActive : EXIT \n");
    return TE_SUCCESS;
}

/*****************************************************************************
* Function Name : teSigDiffServChkPerOAResrcConf                                 
* Description   : The follwing routine checks if the Per OA basis resources
*                 are configured by manager or not. It returns success if 
*                 the resources are configured .If not it returns the failure.
*                 This is called from Signal Module
* Input(s)      : i4ElspInfoListIndex - ElspListIndex
*
* Output(s)     : None                                                      
* Return(s)     : TE_SUCCESS / TE_FAILURE     
******************************************************************************/
UINT1
TeSigDiffServChkIsPerOAResrcConf (INT4 i4ElspInfoListIndex)
{
    UINT1               u1Return = TE_FAILURE;
    MPLS_CMN_LOCK ();
    u1Return = TeDiffServChkIsPerOAResrcConf (i4ElspInfoListIndex);
    MPLS_CMN_UNLOCK ();
    return u1Return;
}

/*****************************************************************************
* Function Name : TeDiffServChkIsPerOAResrcConf                                 
* Description   : The follwing routine checks if the Per OA basis resources
*                 are configured by manager or not. It returns success if 
*                 the resources are configured .If not it returns the failure
*
* Input(s)      : i4ElspInfoListIndex - ElspListIndex
*
* Output(s)     : None                                                      
* Return(s)     : TE_SUCCESS / TE_FAILURE     
******************************************************************************/
UINT1
TeDiffServChkIsPerOAResrcConf (INT4 i4ElspInfoListIndex)
{
    tMplsDiffServElspInfo *pElspInfoNode = NULL;
    TE_DBG (TE_DIFF_ETEXT, "MAIN : TeDiffServChkIsPerOAResrcConf : ENTRY \n");

    TE_SLL_SCAN (TE_DS_GBL_ELSP_INFO_LIST (i4ElspInfoListIndex),
                 pElspInfoNode, tMplsDiffServElspInfo *)
    {
        if (TE_DS_ELSP_INFO_PSC_TRFC_PARAMS (pElspInfoNode) != NULL)
        {
            TE_DBG1 (TE_DIFF_PRCS,
                     "MAIN: The ElspInfo with Index : "
                     "%d has TrfcParam configured \n",
                     pElspInfoNode->u1ElspInfoIndex);
            TE_DBG (TE_DIFF_ETEXT,
                    "MAIN : TeDiffServChkIsPerOAResrcConf : EXIT \n");
            return TE_SUCCESS;
        }
    }
    TE_DBG (TE_DIFF_FAIL,
            "MAIN: No ElspInfo has associated TrfcParams PerOA \n");
    TE_DBG (TE_DIFF_ETEXT,
            "MAIN : TeDiffServChkIsPerOAResrcConf : INTMD-EXIT \n");
    return TE_FAILURE;
}

/******************************************************************************
* Function Name : TeDiffServGetElspInfoNode                                 
* Description   : The function gets the ElspInfoNode of the input
*                 i4ElspInfoIndex. It is called from low level routines
*                 hence the validation of indexes are performed there
*                 itself.
* Input(s)      : i4ElspInfoListIndex - ElspInfo List Index 
*                 i4ElspInfoIndex - Index of the node in the search
*                 ppElspInfoNode - pointer of the ElspinfoNode
* Output(s)     : NONE                                                   
* Return(s)     : TE_SUCCESS / TE_FAILURE     
******************************************************************************/
UINT1
TeDiffServGetElspInfoNode (INT4 i4ElspInfoListIndex,
                           INT4 i4ElspInfoIndex,
                           tMplsDiffServElspInfo ** ppElspInfoNode)
{
    tMplsDiffServElspInfo *pElspInfoNode = NULL;
    TE_DBG (TE_LLVL_ETEXT, "EXTN : TeDiffServGetElspInfoNode : ENTRY \n");
    TE_SLL_SCAN (TE_DS_GBL_ELSP_INFO_LIST (i4ElspInfoListIndex),
                 pElspInfoNode, tMplsDiffServElspInfo *)
    {
        if (TE_DS_ELSP_INFO_INDEX (pElspInfoNode) == i4ElspInfoIndex)
        {
            *ppElspInfoNode = pElspInfoNode;
            TE_DBG (TE_LLVL_PRCS, "EXTN : Valid ElspInfoNode found \n");
            TE_DBG (TE_LLVL_ETEXT,
                    "EXTN : TeDiffServGetElspInfoNode : EXIT \n");
            return TE_SUCCESS;
        }
    }
    TE_DBG (TE_LLVL_ETEXT, "EXTN : TeDiffServGetElspInfoNode : INTMD-EXIT \n");
    return TE_FAILURE;
}

/*****************************************************************************/
/* Function Name : TeDiffServGetNextAvailableIndex                           
 * Description   : This routine gets the free available Index for the ElspList.
 * Input(s)      : NONE                 
 * Output(s)     : pu4Index - Pointer containing next available index.
 * Return(s)     : TE_SUCCESS/TE_FAILURE                            
 *****************************************************************************/
UINT1
TeDiffServGetNextAvailableIndex (UINT4 *pu4Index)
{
    UINT4               u4Count;
    TE_DBG (TE_LLVL_ETEXT, "EXTN : TeDiffServGetNextAvailableIndex : ENTRY \n");
    for (u4Count = TE_ONE; u4Count < TE_MAX_DS_ELSPS (gTeGblInfo); u4Count++)
    {
        if (TE_DS_GBL_ELSP_INFO_LIST_INDEX (u4Count) == TE_ZERO)
        {
            *pu4Index = u4Count;
            TE_DBG1 (TE_LLVL_PRCS,
                     "EXTN : Valid DSGetNextAvailableIndex : %d \n", u4Count);
            TE_DBG (TE_LLVL_ETEXT,
                    "EXTN : TeDiffServGetNextAvailableIndex : EXIT \n");
            return TE_SUCCESS;
        }
    }
    TE_DBG (TE_LLVL_ETEXT,
            "EXTN : TeDiffServGetNextAvailableIndex : INTMD-EXIT \n");
    return TE_FAILURE;
}

/*****************************************************************************
* Function Name : TeDeleteMplsDiffServTnlInfo
* Description   : This routine is called to delete the TunnelInfo and the 
*                 associated ElspList and Traffic Params.
* Input(s)        pTeTnlInfo - Pointer to the tunnelInfo which contains 
*                 diffserv Tunnel related params.
*                 pInstance0Tnl - Instance 0 Tunnel Entry
* Output(s)     : NONE
* Return(s)     : TE_SUCCESS or TE_FAILURE
*****************************************************************************/
UINT1
TeDeleteMplsDiffServTnlInfo (tTeTnlInfo * pTeTnlInfo,
                             tTeTnlInfo * pInstance0Tnl)
{
    tMplsDiffServTnlInfo *pMplsDiffServTnlInfo = NULL;
    tMplsDiffServElspList *pMplsDiffServElspList = NULL;

    TE_DBG (TE_DIFF_ETEXT, "MAIN : TeDeleteMplsDiffServTnlInfo : ENTRY \n");

    if (pInstance0Tnl != NULL)
    {
        return TE_SUCCESS;
    }

    pMplsDiffServTnlInfo = TE_DS_TNL_INFO (pTeTnlInfo);
    if (pMplsDiffServTnlInfo == NULL)
    {
        TE_DBG (TE_DIFF_FAIL,
                "MAIN : Null Pointer provided for MplsDiffServTnlInfo\n");
        TE_DBG (TE_DIFF_ETEXT,
                "MAIN : TeDeleteMplsDiffServTnlInfo : INTMD-EXIT \n");
        return TE_FAILURE;
    }

    if (TE_DS_TNL_ELSP_LIST_INDEX (pMplsDiffServTnlInfo) != TE_ZERO)
    {
        pMplsDiffServElspList = TE_DS_GBL_ELSP_INFO_LIST_ENTRY
            (TE_DS_TNL_ELSP_LIST_INDEX (pMplsDiffServTnlInfo));
        --TE_DS_GBL_ELSP_INFO_LIST_TNL_COUNT
            (TE_DS_TNL_ELSP_LIST_INDEX (pMplsDiffServTnlInfo));
    }
    else if (TE_DS_TNL_ELSP_INFO_LIST_INFO (pMplsDiffServTnlInfo) != NULL)
    {
        pMplsDiffServElspList = TE_DS_TNL_ELSP_INFO_LIST_INFO
            (pMplsDiffServTnlInfo);
    }
    if (pMplsDiffServElspList != NULL)
    {
        if ((TE_DS_ELSP_INFO_LIST_TNL_COUNT (pMplsDiffServElspList) == TE_ZERO)
            && (TE_TNL_ROLE (pTeTnlInfo) != TE_INGRESS))
        {
            TE_DBG (TE_DIFF_PRCS, "MAIN : ELspList Deletion called \n");
            TeDeleteDiffServElspList (pMplsDiffServElspList);
            TE_DS_TNL_ELSP_INFO_LIST_INFO (pMplsDiffServTnlInfo) = NULL;
        }
        else
        {
            TE_DBG (TE_DIFF_PRCS,
                    "MAIN : ELspList Present and TnlCount "
                    "alone decremented \n");
        }
    }
    if (TE_REL_MEM_BLOCK (TE_DS_TNL_INFO_POOL_ID,
                          (UINT1 *) (pMplsDiffServTnlInfo)) == TE_MEM_FAILURE)
    {
        TE_DBG (TE_DIFF_FAIL, "MAIN : MplsDiffServTnlInfo Deletion failed \n");
        TE_DBG (TE_DIFF_ETEXT,
                "MAIN : TeDeleteMplsDiffServTnlInfo : INTMD-EXIT \n");
        return TE_FAILURE;
    }
    TE_DS_TNL_INFO (pTeTnlInfo) = NULL;

    TE_DBG (TE_DIFF_ETEXT, "MAIN : teDeleteDiffServTnl : EXIT \n");
    return TE_SUCCESS;
}
