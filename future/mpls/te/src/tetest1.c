/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tetest1.c,v 1.56 2017/06/08 11:40:33 siva Exp $
 *
 * Description: This file contains the low level TEST routines
 *              for the following TE MIB tables.
 *              - MplsTunnelTable
 *              - FsMplsFrrConstTable
 *              - FsMplsTunnelExtTable
 ********************************************************************/

#include "teincs.h"
#include "fsmplslw.h"
#include "stdtelw.h"
#include "mplcmndb.h"
#include "mplslsr.h"
#include "fsmpfrlw.h"
#include "mplscli.h"

/* LOW LEVEL Routines for Table : MplsTunnelTable. */

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelName
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValMplsTunnelName
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelName (UINT4 *pu4ErrorCode, UINT4 u4MplsTunnelIndex,
                         UINT4 u4MplsTunnelInstance,
                         UINT4 u4MplsTunnelIngressLSRId,
                         UINT4 u4MplsTunnelEgressLSRId,
                         tSNMP_OCTET_STRING_TYPE * pTestValMplsTunnelName)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if ((pTestValMplsTunnelName->i4_Length < MIN_SNMP_STRING_LENGTH) ||
        (pTestValMplsTunnelName->i4_Length > MAX_SNMP_STRING_LENGTH))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (LSP_TNLNAME_LEN < pTestValMplsTunnelName->i4_Length)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelDescr
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValMplsTunnelDescr
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelDescr (UINT4 *pu4ErrorCode, UINT4 u4MplsTunnelIndex,
                          UINT4 u4MplsTunnelInstance,
                          UINT4 u4MplsTunnelIngressLSRId,
                          UINT4 u4MplsTunnelEgressLSRId,
                          tSNMP_OCTET_STRING_TYPE * pTestValMplsTunnelDescr)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if ((pTestValMplsTunnelDescr->i4_Length < MIN_SNMP_STRING_LENGTH) ||
        (pTestValMplsTunnelDescr->i4_Length > MAX_SNMP_STRING_LENGTH))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (LSP_TNLDESCR_LEN < pTestValMplsTunnelDescr->i4_Length)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelIsIf
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValMplsTunnelIsIf
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelIsIf (UINT4 *pu4ErrorCode, UINT4 u4MplsTunnelIndex,
                         UINT4 u4MplsTunnelInstance,
                         UINT4 u4MplsTunnelIngressLSRId,
                         UINT4 u4MplsTunnelEgressLSRId,
                         INT4 i4TestValMplsTunnelIsIf)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    switch (i4TestValMplsTunnelIsIf)
    {
        case TE_SNMP_TRUE:
        case TE_SNMP_FALSE:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelXCPointer
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValMplsTunnelXCPointer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelXCPointer (UINT4 *pu4ErrorCode, UINT4 u4MplsTunnelIndex,
                              UINT4 u4MplsTunnelInstance,
                              UINT4 u4MplsTunnelIngressLSRId,
                              UINT4 u4MplsTunnelEgressLSRId,
                              tSNMP_OID_TYPE * pTestValMplsTunnelXCPointer)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4XCIndex = 0;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_TNL_SIGPROTO (pTeTnlInfo) != TE_SIGPROTO_NONE)
    {
        /* Allow to set XC only for Tunnel with no signalling
         * protocol. If signal is their they they will associate
         * Tunnel and XC after getting labels from the Peer */
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* MPLS_P2MP_LSP_CHANGES - S */
    if (TE_TNL_TYPE_P2MP & TE_TNL_TYPE (pTeTnlInfo)
        && (TE_EGRESS != TE_TNL_ROLE (pTeTnlInfo)))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    /* MPLS_P2MP_LSP_CHANGES - E */

    if (pTestValMplsTunnelXCPointer->u4_Length <= TE_TNL_XC_TABLE_DEF_OFFSET)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (MEMCMP (pTestValMplsTunnelXCPointer->pu4_OidList,
                au4TeTnlXCTableOid,
                ((TE_TNL_XC_TABLE_DEF_OFFSET) * sizeof (UINT4))) != 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    u4XCIndex = u4XCIndex |
        ((pTestValMplsTunnelXCPointer->
          pu4_OidList[TE_TNL_XC_TABLE_DEF_OFFSET]) << 24);
    u4XCIndex = u4XCIndex |
        ((pTestValMplsTunnelXCPointer->
          pu4_OidList[TE_TNL_XC_TABLE_DEF_OFFSET + 1]) << 16);
    u4XCIndex = u4XCIndex |
        ((pTestValMplsTunnelXCPointer->
          pu4_OidList[TE_TNL_XC_TABLE_DEF_OFFSET + 2]) << 8);
    u4XCIndex = u4XCIndex |
        (pTestValMplsTunnelXCPointer->
         pu4_OidList[TE_TNL_XC_TABLE_DEF_OFFSET + 3]);

    if (MplsGetXCEntryByDirection (u4XCIndex,
                                   (eDirection) MPLS_DIRECTION_ANY) == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelSignallingProto
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValMplsTunnelSignallingProto
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelSignallingProto (UINT4 *pu4ErrorCode,
                                    UINT4 u4MplsTunnelIndex,
                                    UINT4 u4MplsTunnelInstance,
                                    UINT4 u4MplsTunnelIngressLSRId,
                                    UINT4 u4MplsTunnelEgressLSRId,
                                    INT4 i4TestValMplsTunnelSignallingProto)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
#ifdef MPLS_SIG_WANTED
    INT4                i4Count = 0;
#endif

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    switch (i4TestValMplsTunnelSignallingProto)
    {
#ifdef MPLS_SIG_WANTED
        case TE_SIGPROTO_LDP:
            nmhGetFsMplsLsrMaxCrlspTnls (&i4Count);
            if (TeGetTnlCount (TE_SIGPROTO_LDP) >= i4Count)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            break;
        case TE_SIGPROTO_RSVP:
            nmhGetFsMplsRsvpTeMaxTnls (&i4Count);
            if (TeGetTnlCount (TE_SIGPROTO_RSVP) >= i4Count)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            break;
#else
        case TE_SIGPROTO_LDP:
        case TE_SIGPROTO_RSVP:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
#endif
        case TE_SIGPROTO_NONE:
            break;
        case TE_SIGPROTO_OTHER:
            *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* MPLS_P2MP_LSP_CHANGES - S */
    if ((TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_P2MP)
        && (TE_SIGPROTO_NONE != i4TestValMplsTunnelSignallingProto))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    /* MPLS_P2MP_LSP_CHANGES - E */

    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelSetupPrio
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValMplsTunnelSetupPrio
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelSetupPrio (UINT4 *pu4ErrorCode, UINT4 u4MplsTunnelIndex,
                              UINT4 u4MplsTunnelInstance,
                              UINT4 u4MplsTunnelIngressLSRId,
                              UINT4 u4MplsTunnelEgressLSRId,
                              INT4 i4TestValMplsTunnelSetupPrio)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if ((TE_TNL_ADMIN_STATUS (pTeTnlInfo) == TE_ADMIN_UP) &&
        pTeTnlInfo->u1TnlMBBStatus == MPLS_TE_MBB_DISABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (MPLS_CLI_TE_ERR_CANNOT_SET_ATTRIBUTE);
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if ((i4TestValMplsTunnelSetupPrio < TE_SETPRIO_MINVAL) ||
        (i4TestValMplsTunnelSetupPrio > TE_SETPRIO_MAXVAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelHoldingPrio
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValMplsTunnelHoldingPrio
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelHoldingPrio (UINT4 *pu4ErrorCode, UINT4 u4MplsTunnelIndex,
                                UINT4 u4MplsTunnelInstance,
                                UINT4 u4MplsTunnelIngressLSRId,
                                UINT4 u4MplsTunnelEgressLSRId,
                                INT4 i4TestValMplsTunnelHoldingPrio)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_TNL_ADMIN_STATUS (pTeTnlInfo) == TE_ADMIN_UP)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        if ((pTeTnlInfo->u1TnlMBBStatus == MPLS_TE_MBB_DISABLED) &&
            (TE_TNL_HLDNG_PRIO (pTeTnlInfo) != i4TestValMplsTunnelHoldingPrio))
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_CANNOT_SET_ATTRIBUTE);
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }
        else if ((pTeTnlInfo->u1TnlMBBStatus == MPLS_TE_MBB_ENABLED) &&
                 (TE_TNL_HLDNG_PRIO (pTeTnlInfo) !=
                  i4TestValMplsTunnelHoldingPrio))
        {
            CLI_SET_ERR (MPLS_CLI_TE_ERR_HOLD_PRO);
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }

    }

    if ((i4TestValMplsTunnelHoldingPrio < TE_HLDPRIO_MINVAL) ||
        (i4TestValMplsTunnelHoldingPrio > TE_HLDPRIO_MAXVAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelSessionAttributes
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValMplsTunnelSessionAttributes
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelSessionAttributes (UINT4 *pu4ErrorCode,
                                      UINT4 u4MplsTunnelIndex,
                                      UINT4 u4MplsTunnelInstance,
                                      UINT4 u4MplsTunnelIngressLSRId,
                                      UINT4 u4MplsTunnelEgressLSRId,
                                      tSNMP_OCTET_STRING_TYPE *
                                      pTestValMplsTunnelSessionAttributes)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();

    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (pTestValMplsTunnelSessionAttributes->i4_Length != sizeof (UINT1))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (((TE_SSN_ALL_BIT &
          (pTestValMplsTunnelSessionAttributes->pu1_OctetList[0])) == 0)
        &&
        (((~TE_SSN_ALL_BIT) &
          (pTestValMplsTunnelSessionAttributes->pu1_OctetList[0])) != 0))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (((!(pTeTnlInfo->u1TnlSsnAttr & TE_SSN_REC_ROUTE_BIT)) &&
         (pTestValMplsTunnelSessionAttributes->pu1_OctetList[0] &
          TE_SSN_REC_ROUTE_BIT)) ||
        ((pTeTnlInfo->u1TnlSsnAttr & TE_SSN_REC_ROUTE_BIT) &&
         (!(pTestValMplsTunnelSessionAttributes->pu1_OctetList[0] &
            TE_SSN_REC_ROUTE_BIT))))
    {
        /* For enabling or disabling Record Route, The tunnel need
         * not be not in service */
        MPLS_CMN_UNLOCK ();
        return SNMP_SUCCESS;
    }
    else
    {
        if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
        {
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelLocalProtectInUse
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValMplsTunnelLocalProtectInUse
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelLocalProtectInUse (UINT4 *pu4ErrorCode,
                                      UINT4 u4MplsTunnelIndex,
                                      UINT4 u4MplsTunnelInstance,
                                      UINT4 u4MplsTunnelIngressLSRId,
                                      UINT4 u4MplsTunnelEgressLSRId,
                                      INT4 i4TestValMplsTunnelLocalProtectInUse)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    switch (i4TestValMplsTunnelLocalProtectInUse)
    {
        case TE_SNMP_TRUE:
        case TE_SNMP_FALSE:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
Function    :  nmhTestv2MplsTunnelResourcePointer
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValMplsTunnelResourcePointer
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelResourcePointer (UINT4 *pu4ErrorCode,
                                    UINT4 u4MplsTunnelIndex,
                                    UINT4 u4MplsTunnelInstance,
                                    UINT4 u4MplsTunnelIngressLSRId,
                                    UINT4 u4MplsTunnelEgressLSRId,
                                    tSNMP_OID_TYPE *
                                    pTestValMplsTunnelResourcePointer)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    UINT4               u4ResourceIndex = 0;
    tTeTrfcParams      *pTeTrfcParams = NULL;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if ((TE_TNL_ADMIN_STATUS (pTeTnlInfo) == TE_ADMIN_UP) &&
        pTeTnlInfo->u1TnlMBBStatus == MPLS_TE_MBB_DISABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (MPLS_CLI_TE_ERR_CANNOT_SET_ATTRIBUTE);
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    if (pTestValMplsTunnelResourcePointer->u4_Length !=
        TE_TNL_RSRC_TABLE_DEF_OFFSET)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_LENGTH;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (MEMCMP (pTestValMplsTunnelResourcePointer->pu4_OidList,
                au4TeTnlResourceTableOid,
                ((TE_TNL_RSRC_TABLE_DEF_OFFSET - 1) * sizeof (UINT4))) != 0)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* Check the u4ResourceIndex in Resource Table for Availability */
    u4ResourceIndex =
        pTestValMplsTunnelResourcePointer->
        pu4_OidList[pTestValMplsTunnelResourcePointer->u4_Length - 1];

    if ((TeCheckTrfcParamInTrfcParamTable (u4ResourceIndex,
                                           &pTeTrfcParams) != TE_SUCCESS) ||
        TE_TNLRSRC_ROW_STATUS (pTeTrfcParams) != ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelInstancePriority
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValMplsTunnelInstancePriority
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelInstancePriority (UINT4 *pu4ErrorCode,
                                     UINT4 u4MplsTunnelIndex,
                                     UINT4 u4MplsTunnelInstance,
                                     UINT4 u4MplsTunnelIngressLSRId,
                                     UINT4 u4MplsTunnelEgressLSRId,
                                     UINT4 u4TestValMplsTunnelInstancePriority)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* RFC 3812 u4TestValMplsTunnelInstancePriority is unsigned32 so,
     * It can have any value in unsigned32 range so, no check for value */
    TE_SUPPRESS_WARNING (u4TestValMplsTunnelInstancePriority);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelHopTableIndex
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValMplsTunnelHopTableIndex
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelHopTableIndex (UINT4 *pu4ErrorCode, UINT4 u4MplsTunnelIndex,
                                  UINT4 u4MplsTunnelInstance,
                                  UINT4 u4MplsTunnelIngressLSRId,
                                  UINT4 u4MplsTunnelEgressLSRId,
                                  UINT4 u4TestValMplsTunnelHopTableIndex)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* MPLS_P2MP_LSP_CHANGES - S */
    if (TE_TNL_TYPE_P2MP & TE_TNL_TYPE (pTeTnlInfo))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    /* MPLS_P2MP_LSP_CHANGES - E */

    if (u4TestValMplsTunnelHopTableIndex > TE_TNL_HOPLSTINDEX_MAXVAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if ((u4TestValMplsTunnelHopTableIndex > (TE_MAX_HOP_LIST (gTeGblInfo) / 2)))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelIncludeAnyAffinity
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValMplsTunnelIncludeAnyAffinity
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelIncludeAnyAffinity (UINT4 *pu4ErrorCode,
                                       UINT4 u4MplsTunnelIndex,
                                       UINT4 u4MplsTunnelInstance,
                                       UINT4 u4MplsTunnelIngressLSRId,
                                       UINT4 u4MplsTunnelEgressLSRId,
                                       UINT4
                                       u4TestValMplsTunnelIncludeAnyAffinity)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if ((TE_TNL_ADMIN_STATUS (pTeTnlInfo) == TE_ADMIN_UP) &&
        pTeTnlInfo->u1TnlMBBStatus == MPLS_TE_MBB_DISABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (MPLS_CLI_TE_ERR_CANNOT_SET_AFFINITY);
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    TE_SUPPRESS_WARNING (u4TestValMplsTunnelIncludeAnyAffinity);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelIncludeAllAffinity
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValMplsTunnelIncludeAllAffinity
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelIncludeAllAffinity (UINT4 *pu4ErrorCode,
                                       UINT4 u4MplsTunnelIndex,
                                       UINT4 u4MplsTunnelInstance,
                                       UINT4 u4MplsTunnelIngressLSRId,
                                       UINT4 u4MplsTunnelEgressLSRId,
                                       UINT4
                                       u4TestValMplsTunnelIncludeAllAffinity)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    TE_SUPPRESS_WARNING (u4TestValMplsTunnelIncludeAllAffinity);

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if ((TE_TNL_ADMIN_STATUS (pTeTnlInfo) == TE_ADMIN_UP) &&
        pTeTnlInfo->u1TnlMBBStatus == MPLS_TE_MBB_DISABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (MPLS_CLI_TE_ERR_CANNOT_SET_AFFINITY);
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    TE_SUPPRESS_WARNING (u4TestValMplsTunnelIncludeAllAffinity);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelExcludeAnyAffinity
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValMplsTunnelExcludeAnyAffinity
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelExcludeAnyAffinity (UINT4 *pu4ErrorCode,
                                       UINT4 u4MplsTunnelIndex,
                                       UINT4 u4MplsTunnelInstance,
                                       UINT4 u4MplsTunnelIngressLSRId,
                                       UINT4 u4MplsTunnelEgressLSRId,
                                       UINT4
                                       u4TestValMplsTunnelExcludeAnyAffinity)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if ((TE_TNL_ADMIN_STATUS (pTeTnlInfo) == TE_ADMIN_UP) &&
        pTeTnlInfo->u1TnlMBBStatus == MPLS_TE_MBB_DISABLED)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        CLI_SET_ERR (MPLS_CLI_TE_ERR_CANNOT_SET_AFFINITY);
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    TE_SUPPRESS_WARNING (u4TestValMplsTunnelExcludeAnyAffinity);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelPathInUse
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValMplsTunnelPathInUse
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelPathInUse (UINT4 *pu4ErrorCode, UINT4 u4MplsTunnelIndex,
                              UINT4 u4MplsTunnelInstance,
                              UINT4 u4MplsTunnelIngressLSRId,
                              UINT4 u4MplsTunnelEgressLSRId,
                              UINT4 u4TestValMplsTunnelPathInUse)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (u4TestValMplsTunnelPathInUse > TE_UINT2_SIZE)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* MPLS_P2MP_LSP_CHANGES - S */
    if (TE_TNL_TYPE_P2MP & TE_TNL_TYPE (pTeTnlInfo))
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    /* MPLS_P2MP_LSP_CHANGES - E */
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelRole
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValMplsTunnelRole
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelRole (UINT4 *pu4ErrorCode, UINT4 u4MplsTunnelIndex,
                         UINT4 u4MplsTunnelInstance,
                         UINT4 u4MplsTunnelIngressLSRId,
                         UINT4 u4MplsTunnelEgressLSRId,
                         INT4 i4TestValMplsTunnelRole)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    switch (i4TestValMplsTunnelRole)
    {
        case TE_INGRESS:
        case TE_INTERMEDIATE:
        case TE_EGRESS:
        case TE_INGRESS_EGRESS:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
    }
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelAdminStatus
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValMplsTunnelAdminStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelAdminStatus (UINT4 *pu4ErrorCode, UINT4 u4MplsTunnelIndex,
                                UINT4 u4MplsTunnelInstance,
                                UINT4 u4MplsTunnelIngressLSRId,
                                UINT4 u4MplsTunnelEgressLSRId,
                                INT4 i4TestValMplsTunnelAdminStatus)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* For P2MP tunnels, do not allow admin status change SET operation when
     * P2MP tunnel information is not available */
    if ((TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_P2MP)
        && (NULL == TE_P2MP_TNL_INFO (pTeTnlInfo)))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    switch (i4TestValMplsTunnelAdminStatus)
    {
        case TE_ADMIN_DOWN:
            if (TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_HLSP)
            {
                if (pTeTnlInfo->u4NoOfStackedTunnels != TE_ZERO)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    CLI_SET_ERR (MPLS_CLI_TE_HLSP_STK_TNL);
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
            }
            /* if BFD session is associated to the Tunnel by MEG or
             * Is directly associated with the tunnel without MEG*/
            if ((MplsIsOamMonitoringTnl (pTeTnlInfo) == TRUE) ||
                (pTeTnlInfo->u4ProactiveSessionIndex != 0))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (MPLS_CLI_TE_OAM_BFD_EXISTS);
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            /* If ELPS is associated with the tunnel dont allow it */
            if (TeCmnExtIsTnlProtected (pTeTnlInfo) == TRUE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                CLI_SET_ERR (MPLS_CLI_TE_ELPS_EXISTS);
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
        case TE_ADMIN_UP:
        case TE_ADMIN_TESTING:
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            break;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelRowStatus
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValMplsTunnelRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelRowStatus (UINT4 *pu4ErrorCode, UINT4 u4MplsTunnelIndex,
                              UINT4 u4MplsTunnelInstance,
                              UINT4 u4MplsTunnelIngressLSRId,
                              UINT4 u4MplsTunnelEgressLSRId,
                              INT4 i4TestValMplsTunnelRowStatus)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTePathInfo        *pTePathInfo = NULL;
    tTePathListInfo    *pTePathListInfo = NULL;
    tTeTnlInfo         *pTeInstance0Tnl = NULL;
    UINT4               u4TnlHopTableIndex = 0;

    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /*Checking whether the Tunnel Index exceeds Maxm No of Tunnels */
    if (i4TestValMplsTunnelRowStatus == TE_CREATEANDWAIT)
    {
        if ((u4MplsTunnelIndex < TE_TNLINDEX_MINVAL) ||
            (u4MplsTunnelIndex > TE_TNLINDEX_MAXVAL))
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }

        /*Checking whether the Tunnel Instance lies between min and max values */
        if (u4MplsTunnelInstance > TE_TNLINST_MAXVAL)
        {
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
        }
    }

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    pTeInstance0Tnl = TeGetTunnelInfo (u4MplsTunnelIndex, TE_ZERO,
                                       u4MplsTunnelIngressLSRId,
                                       u4MplsTunnelEgressLSRId);

    switch (i4TestValMplsTunnelRowStatus)
    {
        case TE_CREATEANDWAIT:

            if (pTeTnlInfo != NULL)
            {
                TE_DBG (TE_LLVL_TEST_FAIL,
                        "Manager is trying to create an existing Row \n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            /* Allow only the maximum number of tunnels supported */
            if ((u4MplsTunnelInstance == TE_ZERO) || (pTeInstance0Tnl == NULL))
            {
                if (gu4TeRpteTnlCount == TE_MAX_TERPTETNL)
                {
                    *pu4ErrorCode = SNMP_ERR_RESOURCE_UNAVAILABLE;
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
            }
            break;
        case TE_ACTIVE:

            if (pTeTnlInfo == NULL)
            {
                TE_DBG (TE_LLVL_TEST_FAIL,
                        "Manager is trying to access non existing Row \n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            if (TE_TNL_HLDNG_PRIO (pTeTnlInfo) > TE_TNL_SETUP_PRIO (pTeTnlInfo))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            if (TE_TNL_OWNER (pTeTnlInfo) != TE_TNL_OWNER_SNMP)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
            {
                MPLS_CMN_UNLOCK ();
                return SNMP_SUCCESS;
            }

            if ((pTeTnlInfo->u4TnlType & MPLS_TE_TNL_TYPE_GMPLS) &&
                (pTeTnlInfo->GmplsTnlInfo.u1Direction == GMPLS_BIDIRECTION) &&
                (pTeTnlInfo->GmplsTnlInfo.u1EncodingType ==
                 GMPLS_TUNNEL_LSP_NOT_GMPLS))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            /* MPLS_P2MP_LSP_CHANGES - S */
            if ((MplsCheckP2mpTnlRoleNotEgress (pTeTnlInfo)) == MPLS_SUCCESS)
            {
                /* Set mplsTeP2mpTunnelRowStatus to ACTIVE */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            /* MPLS_P2MP_LSP_CHANGES - E */

            if ((TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_NONE) &&
                (pTeTnlInfo->u4TnlXcIndex == 0) &&
                (u4MplsTunnelInstance != TE_ZERO))
            {
                TE_DBG (TE_LLVL_TEST_FAIL,
                        "Manager is trying to access non existing Row \n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            u4TnlHopTableIndex = TE_TNL_HOP_TABLE_INDEX (pTeTnlInfo);
            if (TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_LDP)
            {
                if (u4TnlHopTableIndex == TE_ZERO)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
            }

            if (((TE_TNL_SSN_ATTR (pTeTnlInfo) & TE_SSN_FAST_REROUTE_BIT)
                 == TE_ZERO) && (TE_TNL_FRR_CONST_INFO (pTeTnlInfo) != NULL))
            {
                /* Fast ReRoute Bit is Set in Tunnel Session Attributes Object
                 * but FRR Const Info is not filled or 
                 * Fast ReRoute Bit is Not Set in Tunnel Session Attributes 
                 * Object but FRR Const Info is filled */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            if ((TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_NONE) &&
                (pTeTnlInfo->u4TnlMode == TE_TNL_MODE_COROUTED_BIDIRECTIONAL) &&
                (u4MplsTunnelInstance != TE_ZERO))
            {
                if ((MplsGetXCEntryByDirection (pTeTnlInfo->u4TnlXcIndex,
                                                MPLS_DIRECTION_FORWARD)
                     == NULL) ||
                    (MplsGetXCEntryByDirection (pTeTnlInfo->u4TnlXcIndex,
                                                MPLS_DIRECTION_REVERSE) ==
                     NULL))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
            }
            if ((TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo)) != TE_ZERO)
            {
                if ((TE_TRFC_PARAMS_TNL_RESINDEX
                     ((TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo))) ==
                     (TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo))) &&
                    (TE_TRFC_PARAMS_TNL_RESROWSTATUS
                     ((TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo))) == TE_ACTIVE))
                {
                    if ((TE_TNL_SIGPROTO (pTeTnlInfo)
                         == TE_SIGPROTO_RSVP)
                        && (TE_RSVPTE_TRFC_PARAMS (TE_TRFC_PARAMS_PTR
                                                   (TE_TNL_TRFC_PARAM_INDEX
                                                    (pTeTnlInfo))) != NULL))
                    {
                        if (TE_TRFC_PARAMS_PTR
                            (TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo)) != NULL)
                        {
                            /* If the Tunnel Type is HLSP and MBB is enabled
                             * And it has E2E LSPs, then it can be allowed only
                             * to increase the bandwidth. it is not allowed to
                             * decrease 
                             */
                            if ((TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_HLSP) &&
                                (pTeTnlInfo->u1TnlMBBStatus ==
                                 MPLS_TE_MBB_ENABLED)
                                && (pTeTnlInfo->u1TnlRowStatus ==
                                    TE_NOTINSERVICE)
                                && (pTeTnlInfo->u4NoOfStackedTunnels !=
                                    TE_ZERO))
                            {
                                if (TE_RSVPTE_TPARAM_PDR
                                    (TE_TRFC_PARAMS_PTR
                                     ((TE_TNL_TRFC_PARAM_INDEX (pTeTnlInfo)))) <
                                    pTeTnlInfo->u4CurrentResvBw)
                                {
                                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                                    MPLS_CMN_UNLOCK ();
                                    return SNMP_FAILURE;
                                }
                            }
                        }
                    }
                }
            }
            /* To check whether the RSVP erhop count for the RSVP tunnels 
             * to be supported is less than MAX_TE_HOP_PER_PO or not */
            if ((TE_TNL_SIGPROTO (pTeTnlInfo) == TE_SIGPROTO_RSVP) &&
                (u4TnlHopTableIndex != TE_ZERO))
            {
                pTePathListInfo = TE_PATH_LIST_ENTRY (u4TnlHopTableIndex);

                if (pTePathListInfo != NULL)
                {
                    TE_SLL_SCAN (TE_HOP_PO_LIST (pTePathListInfo),
                                 pTePathInfo, tTePathInfo *)
                    {
                        if (pTePathInfo->u4PathOptionIndex ==
                            TE_TNL_PATH_IN_USE (pTeTnlInfo))
                        {
                            if (TE_PO_HOP_COUNT (pTePathInfo) >
                                TeGetMaxErhopsPerTnl ())
                            {
                                TE_DBG2 (TE_LLVL_TEST_FAIL,
                                         "Tunnel's Erhop count (%d) is greater than miximum RSVP supported,"
                                         "Erhops (%d) \n",
                                         TE_PO_HOP_COUNT (pTePathInfo),
                                         TeGetMaxErhopsPerTnl ());
                                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                                MPLS_CMN_UNLOCK ();
                                return SNMP_FAILURE;
                            }
                            break;
                        }
                    }
                }
            }
            break;
        case TE_NOTINSERVICE:

            if (pTeTnlInfo == NULL)
            {
                TE_DBG (TE_LLVL_TEST_FAIL,
                        "Manager is trying to access non existing Row \n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            if (TE_TNL_OWNER (pTeTnlInfo) != TE_TNL_OWNER_SNMP)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

#if 0
            /* if BFD session is associated to the Tunnel by MEG or
             * Is directly associated with the tunnel without MEG*/
            if ((MplsIsOamMonitoringTnl (pTeTnlInfo) == TRUE) ||
                (pTeTnlInfo->u4ProactiveSessionIndex != 0))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
#endif
            /* If ELPS is associated with the tunnel dont allow it */
            if (TeCmnExtIsTnlProtected (pTeTnlInfo) == TRUE)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            /* MPLS_P2MP_LSP_CHANGES - S */
            if ((MplsCheckP2mpTnlRoleNotEgress (pTeTnlInfo)) == MPLS_SUCCESS)
            {
                /* Set mplsTeP2mpTunnelRowStatus to NOT IN SERVICE */
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            /* In MBB Case, User can increase the bandwidth of the HLSP
             * even in the presence of E2E LSP
             */
            if (pTeTnlInfo->u1TnlMBBStatus != MPLS_TE_MBB_ENABLED)
            {
                if (TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_HLSP)
                {
                    if (pTeTnlInfo->u4NoOfStackedTunnels != TE_ZERO)
                    {
                        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                        MPLS_CMN_UNLOCK ();
                        return SNMP_FAILURE;
                    }
                }
            }
            /* MPLS_P2MP_LSP_CHANGES - E */
            break;
        case TE_DESTROY:

            if (pTeTnlInfo == NULL)
            {
                TE_DBG (TE_LLVL_TEST_FAIL,
                        "Manager is trying to access non existing Row \n");
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            if (TE_TNL_OWNER (pTeTnlInfo) != TE_TNL_OWNER_SNMP)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            if ((TE_TNL_IN_USE_BY_VPN (pTeTnlInfo) &
                 TE_TNL_INUSE_BY_L2VPN) == TE_TNL_INUSE_BY_L2VPN)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            if (TE_LDP_OVER_RSVP_ENT_INDEX (pTeTnlInfo) != TE_ZERO)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            if (TMO_DLL_Count (&TE_TNL_FTN_LIST (pTeTnlInfo)) != TE_ZERO)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            if ((pTeTnlInfo->u4MegIndex != 0) &&
                (pTeTnlInfo->u4MeIndex != 0) && (pTeTnlInfo->u4MpIndex != 0))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            /* MPLS_P2MP_LSP_CHANGES - S */
            if ((TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_P2MP)
                && (TE_P2MP_TNL_INFO (pTeTnlInfo) != NULL))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            /* MPLS_P2MP_LSP_CHANGES - E */

            if (TE_TNL_TYPE (pTeTnlInfo) & TE_TNL_TYPE_HLSP)
            {
                if (pTeTnlInfo->u4NoOfStackedTunnels != TE_ZERO)
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }

                if ((TE_HLSP_INFO (pTeTnlInfo) != NULL) &&
                    (TE_HLSP_NO_OF_STACKED_TNLS (pTeTnlInfo) > TE_ZERO) &&
                    (u4MplsTunnelInstance != TE_ZERO))
                {
                    *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                    MPLS_CMN_UNLOCK ();
                    return SNMP_FAILURE;
                }
            }

            /* Stitching should be removed before deleting the S-LSP tunnel. */
            if ((TE_TNL_TYPE_SLSP & (TE_TNL_TYPE (pTeTnlInfo))) &&
                (TE_MAP_TNL_INFO (pTeTnlInfo) != NULL) &&
                (u4MplsTunnelInstance != TE_ZERO))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            /* if BFD session is associated to the Tunnel */
            if (pTeTnlInfo->u4ProactiveSessionIndex != 0)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }

            break;

        case TE_CREATEANDGO:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            TE_DBG (TE_LLVL_TEST_FAIL, "Create and GO Not Supported \n");

            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;

        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2MplsTunnelStorageType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValMplsTunnelStorageType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2MplsTunnelStorageType (UINT4 *pu4ErrorCode, UINT4 u4MplsTunnelIndex,
                                UINT4 u4MplsTunnelInstance,
                                UINT4 u4MplsTunnelIngressLSRId,
                                UINT4 u4MplsTunnelEgressLSRId,
                                INT4 i4TestValMplsTunnelStorageType)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    MPLS_CMN_LOCK ();
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    switch (i4TestValMplsTunnelStorageType)
    {
        case TE_STORAGE_VOLATILE:
        case TE_STORAGE_NONVOLATILE:
            break;
        default:
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            MPLS_CMN_UNLOCK ();
            return SNMP_FAILURE;
    }

    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;

}

/****************************************************************************
 Function    :  nmhTestv2FsMplsFrrConstProtectionMethod
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValFsMplsFrrConstProtectionMethod
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsFrrConstProtectionMethod (UINT4 *pu4ErrorCode,
                                         UINT4 u4MplsTunnelIndex,
                                         UINT4 u4MplsTunnelInstance,
                                         UINT4 u4MplsTunnelIngressLSRId,
                                         UINT4 u4MplsTunnelEgressLSRId,
                                         INT4
                                         i4TestValFsMplsFrrConstProtectionMethod)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    /* Verifying for the Extreme Values */
    if ((i4TestValFsMplsFrrConstProtectionMethod !=
         TE_TNL_FRR_ONE2ONE_METHOD) &&
        (i4TestValFsMplsFrrConstProtectionMethod != TE_TNL_FRR_FACILITY_METHOD))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeTnlInfo should not be active */
    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be NULL */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);
    if (pTeFrrConstInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be active */
    if (TE_TNL_FRR_CONST_ROW_STATUS (pTeFrrConstInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsFrrConstProtectionType
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValFsMplsFrrConstProtectionType
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsFrrConstProtectionType (UINT4 *pu4ErrorCode,
                                       UINT4 u4MplsTunnelIndex,
                                       UINT4 u4MplsTunnelInstance,
                                       UINT4 u4MplsTunnelIngressLSRId,
                                       UINT4 u4MplsTunnelEgressLSRId,
                                       INT4
                                       i4TestValFsMplsFrrConstProtectionType)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    /* Verifying for the Extreme Values */
    if ((i4TestValFsMplsFrrConstProtectionType != TE_TNL_FRR_PROT_LINK) &&
        (i4TestValFsMplsFrrConstProtectionType != TE_TNL_FRR_PROT_NODE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeTnlInfo should not be active */
    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be NULL */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);
    if (pTeFrrConstInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be active */
    if (TE_TNL_FRR_CONST_ROW_STATUS (pTeFrrConstInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsFrrConstSetupPrio
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValFsMplsFrrConstSetupPrio
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsFrrConstSetupPrio (UINT4 *pu4ErrorCode,
                                  UINT4 u4MplsTunnelIndex,
                                  UINT4 u4MplsTunnelInstance,
                                  UINT4 u4MplsTunnelIngressLSRId,
                                  UINT4 u4MplsTunnelEgressLSRId,
                                  UINT4 u4TestValFsMplsFrrConstSetupPrio)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    /* Verifying for the Extreme Values */
    if (u4TestValFsMplsFrrConstSetupPrio > TE_SETPRIO_MAXVAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeTnlInfo should not be active */
    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be NULL */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);
    if (pTeFrrConstInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be active */
    if (TE_TNL_FRR_CONST_ROW_STATUS (pTeFrrConstInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsFrrConstHoldingPrio
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValFsMplsFrrConstHoldingPrio
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsFrrConstHoldingPrio (UINT4 *pu4ErrorCode,
                                    UINT4 u4MplsTunnelIndex,
                                    UINT4 u4MplsTunnelInstance,
                                    UINT4 u4MplsTunnelIngressLSRId,
                                    UINT4 u4MplsTunnelEgressLSRId,
                                    UINT4 u4TestValFsMplsFrrConstHoldingPrio)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    /* Verifying for the Extreme Values */
    if (u4TestValFsMplsFrrConstHoldingPrio > TE_HLDPRIO_MAXVAL)
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeTnlInfo should not be active */
    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be NULL */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);
    if (pTeFrrConstInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be active */
    if (TE_TNL_FRR_CONST_ROW_STATUS (pTeFrrConstInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsFrrConstSEStyle
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValFsMplsFrrConstSEStyle
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsFrrConstSEStyle (UINT4 *pu4ErrorCode,
                                UINT4 u4MplsTunnelIndex,
                                UINT4 u4MplsTunnelInstance,
                                UINT4 u4MplsTunnelIngressLSRId,
                                UINT4 u4MplsTunnelEgressLSRId,
                                INT4 i4TestValFsMplsFrrConstSEStyle)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    /* Verifying for the Extreme Values */
    if ((i4TestValFsMplsFrrConstSEStyle != TE_SNMP_TRUE) &&
        (i4TestValFsMplsFrrConstSEStyle != TE_SNMP_FALSE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeTnlInfo should not be active */
    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be NULL */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);
    if (pTeFrrConstInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be active */
    if (TE_TNL_FRR_CONST_ROW_STATUS (pTeFrrConstInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsFrrConstInclAnyAffinity
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValFsMplsFrrConstInclAnyAffinity
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsFrrConstInclAnyAffinity (UINT4 *pu4ErrorCode,
                                        UINT4 u4MplsTunnelIndex,
                                        UINT4 u4MplsTunnelInstance,
                                        UINT4 u4MplsTunnelIngressLSRId,
                                        UINT4 u4MplsTunnelEgressLSRId,
                                        UINT4
                                        u4TestValFsMplsFrrConstInclAnyAffinity)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeTnlInfo should not be active */
    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be NULL */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);
    if (pTeFrrConstInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be active */
    if (TE_TNL_FRR_CONST_ROW_STATUS (pTeFrrConstInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    TE_SUPPRESS_WARNING (u4TestValFsMplsFrrConstInclAnyAffinity);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsFrrConstInclAllAffinity
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValFsMplsFrrConstInclAllAffinity
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsFrrConstInclAllAffinity (UINT4 *pu4ErrorCode,
                                        UINT4 u4MplsTunnelIndex,
                                        UINT4 u4MplsTunnelInstance,
                                        UINT4 u4MplsTunnelIngressLSRId,
                                        UINT4 u4MplsTunnelEgressLSRId,
                                        UINT4
                                        u4TestValFsMplsFrrConstInclAllAffinity)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeTnlInfo should not be active */
    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be NULL */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);
    if (pTeFrrConstInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be active */
    if (TE_TNL_FRR_CONST_ROW_STATUS (pTeFrrConstInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    TE_SUPPRESS_WARNING (u4TestValFsMplsFrrConstInclAllAffinity);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsFrrConstExclAnyAffinity
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValFsMplsFrrConstExclAnyAffinity
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsFrrConstExclAnyAffinity (UINT4 *pu4ErrorCode,
                                        UINT4 u4MplsTunnelIndex,
                                        UINT4 u4MplsTunnelInstance,
                                        UINT4 u4MplsTunnelIngressLSRId,
                                        UINT4 u4MplsTunnelEgressLSRId,
                                        UINT4
                                        u4TestValFsMplsFrrConstExclAnyAffinity)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeTnlInfo should not be active */
    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be NULL */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);
    if (pTeFrrConstInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be active */
    if (TE_TNL_FRR_CONST_ROW_STATUS (pTeFrrConstInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    TE_SUPPRESS_WARNING (u4TestValFsMplsFrrConstExclAnyAffinity);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsFrrConstHopLimit
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValFsMplsFrrConstHopLimit
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsFrrConstHopLimit (UINT4 *pu4ErrorCode,
                                 UINT4 u4MplsTunnelIndex,
                                 UINT4 u4MplsTunnelInstance,
                                 UINT4 u4MplsTunnelIngressLSRId,
                                 UINT4 u4MplsTunnelEgressLSRId,
                                 UINT4 u4TestValFsMplsFrrConstHopLimit)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    /* Verifying for the Extreme Values */
    if ((u4TestValFsMplsFrrConstHopLimit < TE_TNL_FRR_HOP_LIM_MIN_VAL) ||
        (u4TestValFsMplsFrrConstHopLimit > TE_TNL_FRR_HOP_LIM_MAX_VAL))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeTnlInfo should not be active */
    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be NULL */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);
    if (pTeFrrConstInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be active */
    if (TE_TNL_FRR_CONST_ROW_STATUS (pTeFrrConstInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsFrrConstBandwidth
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValFsMplsFrrConstBandwidth
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsFrrConstBandwidth (UINT4 *pu4ErrorCode,
                                  UINT4 u4MplsTunnelIndex,
                                  UINT4 u4MplsTunnelInstance,
                                  UINT4 u4MplsTunnelIngressLSRId,
                                  UINT4 u4MplsTunnelEgressLSRId,
                                  UINT4 u4TestValFsMplsFrrConstBandwidth)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeTnlInfo should not be active */
    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be NULL */
    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);
    if (pTeFrrConstInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeFrrConstInfo should not be active */
    if (TE_TNL_FRR_CONST_ROW_STATUS (pTeFrrConstInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    TE_SUPPRESS_WARNING (u4TestValFsMplsFrrConstBandwidth);
    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsFrrConstRowStatus
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object 
                testValFsMplsFrrConstRowStatus
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsFrrConstRowStatus (UINT4 *pu4ErrorCode,
                                  UINT4 u4MplsTunnelIndex,
                                  UINT4 u4MplsTunnelInstance,
                                  UINT4 u4MplsTunnelIngressLSRId,
                                  UINT4 u4MplsTunnelEgressLSRId,
                                  INT4 i4TestValFsMplsFrrConstRowStatus)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;
    tTeFrrConstInfo    *pTeFrrConstInfo = NULL;

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Perform any Action on this Table, An Entry in Tunnel Table 
     * is required */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeTnlInfo should not be active */
    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_NAME;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    pTeFrrConstInfo = TE_TNL_FRR_CONST_INFO (pTeTnlInfo);

    /* For Actions other than CREATE_AND_WAIT on this Table, 
     * FrrConstInfo Pointer should not be NULL */
    if ((i4TestValFsMplsFrrConstRowStatus != TE_CREATEANDWAIT) &&
        (pTeFrrConstInfo == NULL))
    {
        *pu4ErrorCode = SNMP_ERR_NO_CREATION;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    switch (i4TestValFsMplsFrrConstRowStatus)
    {
        case TE_ACTIVE:
        {
            if ((TE_TNL_FRR_CONST_PROT_METHOD (pTeFrrConstInfo) ==
                 TE_TNL_FRR_FACILITY_METHOD) &&
                (TE_TNL_FRR_CONST_SE_STYLE (pTeFrrConstInfo) != TE_SNMP_TRUE))
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                break;
            }
        }
        case TE_NOTINSERVICE:
        case TE_NOTREADY:
        {
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
        case TE_CREATEANDGO:
        {
            /* UnSupported Action */
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            break;
        }
        case TE_CREATEANDWAIT:
        {
            /* For this Action, pTeFrrConstInfo should be NULL */
            if (pTeFrrConstInfo != NULL)
            {
                *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
                MPLS_CMN_UNLOCK ();
                return SNMP_FAILURE;
            }
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }
        case TE_DESTROY:
        {
            MPLS_CMN_UNLOCK ();
            return SNMP_SUCCESS;
        }

        default:
        {
            /* UnSupported Action */
            *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
            break;
        }
    }
    MPLS_CMN_UNLOCK ();
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsMplsTunnelExtMaxGblRevertTime
 Input       :  The Indices
                MplsTunnelIndex
                MplsTunnelInstance
                MplsTunnelIngressLSRId
                MplsTunnelEgressLSRId

                The Object
                testValFsMplsTunnelExtMaxGblRevertTime
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsMplsTunnelExtMaxGblRevertTime (UINT4 *pu4ErrorCode,
                                          UINT4 u4MplsTunnelIndex,
                                          UINT4 u4MplsTunnelInstance,
                                          UINT4 u4MplsTunnelIngressLSRId,
                                          UINT4 u4MplsTunnelEgressLSRId,
                                          INT4
                                          i4TestValFsMplsTunnelExtMaxGblRevertTime)
{
    tTeTnlInfo         *pTeTnlInfo = NULL;

    /* Verifying for Extreme Values */
    if ((i4TestValFsMplsTunnelExtMaxGblRevertTime <
         TE_TNL_FRR_GBL_REVERT_MIN_TIME) ||
        (i4TestValFsMplsTunnelExtMaxGblRevertTime >
         TE_TNL_FRR_GBL_REVERT_MAX_TIME))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    MPLS_CMN_LOCK ();

    /* Verifying whether TE_ADMIN is up or not */
    if (TE_ADMIN_STATUS (gTeGblInfo) == TE_ADMIN_DOWN)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, An entry in Tunnel Table should exist */
    pTeTnlInfo = TeGetTunnelInfo (u4MplsTunnelIndex, u4MplsTunnelInstance,
                                  u4MplsTunnelIngressLSRId,
                                  u4MplsTunnelEgressLSRId);
    if (pTeTnlInfo == NULL)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    /* To Set this Object, pTeTnlInfo should not be active */
    if (TE_TNL_ROW_STATUS (pTeTnlInfo) == TE_ACTIVE)
    {
        *pu4ErrorCode = SNMP_ERR_INCONSISTENT_VALUE;
        MPLS_CMN_UNLOCK ();
        return SNMP_FAILURE;
    }

    MPLS_CMN_UNLOCK ();
    return SNMP_SUCCESS;
}

/******************************** end of tetest1.c ***************************/
