/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tegblex.h,v 1.7 2014/06/21 13:09:12 siva Exp $
 *
 * Description: This file contains extern definition of global
 *              variables.
 *******************************************************************/

#ifndef _TE_GBLEX_H
#define _TE_GBLEX_H

extern tTeGblInfo gTeGblInfo;

extern UINT4 au4TeTnlResourceTableOid[];
extern UINT4 au4TeTnlXCTableOid[];

extern UINT4 au4DiffServElspInfoTableOid[];
extern UINT4 au4TeTnlAttributeTableOid[];

extern UINT4 gu4TeDbgFlag;
extern UINT4 gu4TeDbgLvl;
extern UINT4 gu4TeRpteTnlCount; 
                        
/* STATIC_HLSP */
extern tTeLSPMapTnlGblInfo gTeLSPMapTnlGblInfo; 
extern UINT4  gu4TeNoOfHlsp;
extern UINT4  gu4TeNoOfSlsp;
                        
#endif
