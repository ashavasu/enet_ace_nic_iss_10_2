/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdgmtlw.h,v 1.1 2011/11/30 09:58:14 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetGmplsTunnelsConfigured ARG_LIST((UINT4 *));

INT1
nmhGetGmplsTunnelsActive ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for GmplsTunnelTable. */
INT1
nmhValidateIndexInstanceGmplsTunnelTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for GmplsTunnelTable  */

INT1
nmhGetFirstIndexGmplsTunnelTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexGmplsTunnelTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetGmplsTunnelUnnumIf ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetGmplsTunnelAttributes ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetGmplsTunnelLSPEncoding ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetGmplsTunnelSwitchingType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetGmplsTunnelLinkProtection ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetGmplsTunnelGPid ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetGmplsTunnelSecondary ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetGmplsTunnelDirection ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetGmplsTunnelPathComp ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetGmplsTunnelUpstreamNotifyRecipientType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetGmplsTunnelUpstreamNotifyRecipient ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetGmplsTunnelSendResvNotifyRecipientType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetGmplsTunnelSendResvNotifyRecipient ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetGmplsTunnelDownstreamNotifyRecipientType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetGmplsTunnelDownstreamNotifyRecipient ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetGmplsTunnelSendPathNotifyRecipientType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetGmplsTunnelSendPathNotifyRecipient ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetGmplsTunnelAdminStatusFlags ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetGmplsTunnelExtraParamsPtr ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OID_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetGmplsTunnelUnnumIf ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetGmplsTunnelAttributes ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetGmplsTunnelLSPEncoding ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetGmplsTunnelSwitchingType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetGmplsTunnelLinkProtection ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetGmplsTunnelGPid ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetGmplsTunnelSecondary ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetGmplsTunnelDirection ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetGmplsTunnelPathComp ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetGmplsTunnelUpstreamNotifyRecipientType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetGmplsTunnelUpstreamNotifyRecipient ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetGmplsTunnelSendResvNotifyRecipientType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetGmplsTunnelSendResvNotifyRecipient ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetGmplsTunnelDownstreamNotifyRecipientType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetGmplsTunnelDownstreamNotifyRecipient ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetGmplsTunnelSendPathNotifyRecipientType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetGmplsTunnelSendPathNotifyRecipient ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetGmplsTunnelAdminStatusFlags ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetGmplsTunnelExtraParamsPtr ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OID_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2GmplsTunnelUnnumIf ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2GmplsTunnelAttributes ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2GmplsTunnelLSPEncoding ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2GmplsTunnelSwitchingType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2GmplsTunnelLinkProtection ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2GmplsTunnelGPid ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2GmplsTunnelSecondary ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2GmplsTunnelDirection ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2GmplsTunnelPathComp ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2GmplsTunnelUpstreamNotifyRecipientType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2GmplsTunnelUpstreamNotifyRecipient ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2GmplsTunnelSendResvNotifyRecipientType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2GmplsTunnelSendResvNotifyRecipient ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2GmplsTunnelDownstreamNotifyRecipientType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2GmplsTunnelDownstreamNotifyRecipient ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2GmplsTunnelSendPathNotifyRecipientType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2GmplsTunnelSendPathNotifyRecipient ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2GmplsTunnelAdminStatusFlags ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2GmplsTunnelExtraParamsPtr ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OID_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2GmplsTunnelTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for GmplsTunnelHopTable. */
INT1
nmhValidateIndexInstanceGmplsTunnelHopTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for GmplsTunnelHopTable  */

INT1
nmhGetFirstIndexGmplsTunnelHopTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexGmplsTunnelHopTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetGmplsTunnelHopLabelStatuses ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetGmplsTunnelHopExplicitForwardLabel ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetGmplsTunnelHopExplicitForwardLabelPtr ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetGmplsTunnelHopExplicitReverseLabel ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetGmplsTunnelHopExplicitReverseLabelPtr ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OID_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetGmplsTunnelHopExplicitForwardLabel ARG_LIST((UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetGmplsTunnelHopExplicitForwardLabelPtr ARG_LIST((UINT4  , UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetGmplsTunnelHopExplicitReverseLabel ARG_LIST((UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetGmplsTunnelHopExplicitReverseLabelPtr ARG_LIST((UINT4  , UINT4  , UINT4  ,tSNMP_OID_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2GmplsTunnelHopExplicitForwardLabel ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2GmplsTunnelHopExplicitForwardLabelPtr ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2GmplsTunnelHopExplicitReverseLabel ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2GmplsTunnelHopExplicitReverseLabelPtr ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,tSNMP_OID_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2GmplsTunnelHopTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for GmplsTunnelARHopTable. */
INT1
nmhValidateIndexInstanceGmplsTunnelARHopTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for GmplsTunnelARHopTable  */

INT1
nmhGetFirstIndexGmplsTunnelARHopTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexGmplsTunnelARHopTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetGmplsTunnelARHopLabelStatuses ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetGmplsTunnelARHopExplicitForwardLabel ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetGmplsTunnelARHopExplicitForwardLabelPtr ARG_LIST((UINT4  , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetGmplsTunnelARHopExplicitReverseLabel ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetGmplsTunnelARHopExplicitReverseLabelPtr ARG_LIST((UINT4  , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetGmplsTunnelARHopProtection ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for GmplsTunnelCHopTable. */
INT1
nmhValidateIndexInstanceGmplsTunnelCHopTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for GmplsTunnelCHopTable  */

INT1
nmhGetFirstIndexGmplsTunnelCHopTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexGmplsTunnelCHopTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetGmplsTunnelCHopLabelStatuses ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetGmplsTunnelCHopExplicitForwardLabel ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetGmplsTunnelCHopExplicitForwardLabelPtr ARG_LIST((UINT4  , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetGmplsTunnelCHopExplicitReverseLabel ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetGmplsTunnelCHopExplicitReverseLabelPtr ARG_LIST((UINT4  , UINT4 ,tSNMP_OID_TYPE * ));

/* Proto Validate Index Instance for GmplsTunnelReversePerfTable. */
INT1
nmhValidateIndexInstanceGmplsTunnelReversePerfTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for GmplsTunnelReversePerfTable  */

INT1
nmhGetFirstIndexGmplsTunnelReversePerfTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexGmplsTunnelReversePerfTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetGmplsTunnelReversePerfPackets ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetGmplsTunnelReversePerfHCPackets ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetGmplsTunnelReversePerfErrors ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetGmplsTunnelReversePerfBytes ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetGmplsTunnelReversePerfHCBytes ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

/* Proto Validate Index Instance for GmplsTunnelErrorTable. */
INT1
nmhValidateIndexInstanceGmplsTunnelErrorTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for GmplsTunnelErrorTable  */

INT1
nmhGetFirstIndexGmplsTunnelErrorTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexGmplsTunnelErrorTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetGmplsTunnelErrorLastErrorType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetGmplsTunnelErrorLastTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetGmplsTunnelErrorReporterType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetGmplsTunnelErrorReporter ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetGmplsTunnelErrorCode ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetGmplsTunnelErrorSubcode ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetGmplsTunnelErrorTLVs ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetGmplsTunnelErrorHelpString ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));
