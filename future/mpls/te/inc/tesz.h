/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: tesz.h,v 1.9 2016/02/03 10:37:59 siva Exp $
*
* Description: This file contains sizing related MACROS
*
*******************************************************************/

enum {
    MAX_TE_AR_HOP_INFO_SIZING_ID,
    MAX_TE_AR_HOP_LIST_INFO_SIZING_ID,
    MAX_TE_ATTR_LIST_INFO_SIZING_ID,
    MAX_TE_ATTR_SRLG_SIZING_ID,
    MAX_TE_CHOP_INFO_SIZING_ID,
    MAX_TE_CHOP_LIST_INFO_SIZING_ID,
    MAX_TE_CRLD_TRFC_PARAMS_SIZING_ID,
    MAX_TE_DEF_HLSP_SIZING_ID,
    MAX_TE_DIFFSERV_ELSP_INFO_SIZING_ID,
    MAX_TE_DIFFSERV_ELSP_LIST_SIZING_ID,
    MAX_TE_DIFFSERV_TNL_INFO_SIZING_ID,
    MAX_TE_FRR_CONST_INFO_SIZING_ID,
    MAX_TE_HOP_INFO_SIZING_ID,
    MAX_TE_LSP_MAP_SIZING_ID,
    MAX_TE_MPLS_API_INPUT_INFO_ENTRIES_SIZING_ID,
    MAX_TE_MPLS_API_OUTPUT_INFO_ENTRIES_SIZING_ID,
    MAX_TE_MPLS_TE_TNL_INDEX_SIZING_ID,
    MAX_TE_P2MP_BRANCH_INFO_SIZING_ID,
    MAX_TE_P2MP_DEST_INFO_SIZING_ID,
    MAX_TE_P2MP_TUNNEL_DEST_TABLE_SIZING_ID,
    MAX_TE_P2MP_TUNNEL_INFO_SIZING_ID,
    MAX_TE_PATH_INFO_SIZING_ID,
    MAX_TE_PATH_LIST_SIZING_ID,
    MAX_TE_RSVP_TRFC_PARAMS_SIZING_ID,
    MAX_TE_TNL_ERROR_TABLE_INFO_SIZING_ID,
    MAX_TE_TNL_INDEX_LIST_SIZING_ID,
    MAX_TE_TNL_SRLG_SIZING_ID,
    MAX_TE_TRFC_PARAMS_SIZING_ID,
    MAX_TE_TUNNEL_INFO_SIZING_ID,
 MAX_TE_REOPT_TUNNEL_INFO_SIZING_ID,
    TE_MAX_SIZING_ID
};


#ifdef  _TESZ_C
tMemPoolId TEMemPoolIds[ TE_MAX_SIZING_ID];
INT4  TeSizingMemCreateMemPools(VOID);
VOID  TeSizingMemDeleteMemPools(VOID);
INT4  TeSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _TESZ_C  */
extern tMemPoolId TEMemPoolIds[ ];
extern INT4  TeSizingMemCreateMemPools(VOID);
extern VOID  TeSizingMemDeleteMemPools(VOID);
extern INT4  TeSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _TESZ_C  */


#ifdef  _TESZ_C
tFsModSizingParams FsTESizingParams [] = {
{ "tTeArHopInfo", "MAX_TE_AR_HOP_INFO", sizeof(tTeArHopInfo),MAX_TE_AR_HOP_INFO, MAX_TE_AR_HOP_INFO,0 },
{ "tTeArHopListInfoSize", "MAX_TE_AR_HOP_LIST_INFO", sizeof(tTeArHopListInfoSize),MAX_TE_AR_HOP_LIST_INFO, MAX_TE_AR_HOP_LIST_INFO,0 },
{ "tTeAttrListInfoSize", "MAX_TE_ATTR_LIST_INFO", sizeof(tTeAttrListInfoSize),MAX_TE_ATTR_LIST_INFO, MAX_TE_ATTR_LIST_INFO,0 },
{ "tTeAttrSrlg", "MAX_TE_ATTR_SRLG", sizeof(tTeAttrSrlg),MAX_TE_ATTR_SRLG, MAX_TE_ATTR_SRLG,0 },
{ "tTeCHopInfo", "MAX_TE_CHOP_INFO", sizeof(tTeCHopInfo),MAX_TE_CHOP_INFO, MAX_TE_CHOP_INFO,0 },
{ "tTeCHopListInfoSize", "MAX_TE_CHOP_LIST_INFO", sizeof(tTeCHopListInfoSize),MAX_TE_CHOP_LIST_INFO, MAX_TE_CHOP_LIST_INFO,0 },
{ "tCRLDPTrfcParams", "MAX_TE_CRLD_TRFC_PARAMS", sizeof(tCRLDPTrfcParams),MAX_TE_CRLD_TRFC_PARAMS, MAX_TE_CRLD_TRFC_PARAMS,0 },
{ "tHlspParams", "MAX_TE_DEF_HLSP", sizeof(tHlspParams),MAX_TE_DEF_HLSP, MAX_TE_DEF_HLSP,0 },
{ "tMplsDiffServElspInfo", "MAX_TE_DIFFSERV_ELSP_INFO", sizeof(tMplsDiffServElspInfo),MAX_TE_DIFFSERV_ELSP_INFO, MAX_TE_DIFFSERV_ELSP_INFO,0 },
{ "tMplsDiffServElspListSize", "MAX_TE_DIFFSERV_ELSP_LIST", sizeof(tMplsDiffServElspListSize),MAX_TE_DIFFSERV_ELSP_LIST, MAX_TE_DIFFSERV_ELSP_LIST,0 },
{ "tMplsDiffServTnlInfo", "MAX_TE_DIFFSERV_TNL_INFO", sizeof(tMplsDiffServTnlInfo),MAX_TE_DIFFSERV_TNL_INFO, MAX_TE_DIFFSERV_TNL_INFO,0 },
{ "tTeFrrConstInfo", "MAX_TE_FRR_CONST_INFO", sizeof(tTeFrrConstInfo),MAX_TE_FRR_CONST_INFO, MAX_TE_FRR_CONST_INFO,0 },
{ "tTeHopInfo", "MAX_TE_HOP_INFO", sizeof(tTeHopInfo),MAX_TE_HOP_INFO, MAX_TE_HOP_INFO,0 },
{ "tteFsMplsLSPMapTunnelTable", "MAX_TE_LSP_MAP", sizeof(tteFsMplsLSPMapTunnelTable),MAX_TE_LSP_MAP, MAX_TE_LSP_MAP,0 },
{ "tMplsApiInInfo", "MAX_TE_MPLS_API_INPUT_INFO_ENTRIES", sizeof(tMplsApiInInfo),MAX_TE_MPLS_API_INPUT_INFO_ENTRIES, MAX_TE_MPLS_API_INPUT_INFO_ENTRIES,0 },
{ "tMplsApiOutInfo", "MAX_TE_MPLS_API_OUTPUT_INFO_ENTRIES", sizeof(tMplsApiOutInfo),MAX_TE_MPLS_API_OUTPUT_INFO_ENTRIES, MAX_TE_MPLS_API_OUTPUT_INFO_ENTRIES,0 },
{ "tPwVcMplsTeTnlIndex", "MAX_TE_MPLS_TE_TNL_INDEX", sizeof(tPwVcMplsTeTnlIndex),MAX_TE_MPLS_TE_TNL_INDEX, MAX_TE_MPLS_TE_TNL_INDEX,0 },
{ "tP2mpBranchEntry", "MAX_TE_P2MP_BRANCH_INFO", sizeof(tP2mpBranchEntry),MAX_TE_P2MP_BRANCH_INFO, MAX_TE_P2MP_BRANCH_INFO,0 },
{ "tP2mpDestEntry", "MAX_TE_P2MP_DEST_INFO", sizeof(tP2mpDestEntry),MAX_TE_P2MP_DEST_INFO, MAX_TE_P2MP_DEST_INFO,0 },
{ "tTeMplsTeP2mpTunnelDestTable", "MAX_TE_P2MP_TUNNEL_DEST_TABLE", sizeof(tTeMplsTeP2mpTunnelDestTable),MAX_TE_P2MP_TUNNEL_DEST_TABLE, MAX_TE_P2MP_TUNNEL_DEST_TABLE,0 },
{ "tTeP2mpTnlInfo", "MAX_TE_P2MP_TUNNEL_INFO", sizeof(tTeP2mpTnlInfo),MAX_TE_P2MP_TUNNEL_INFO, MAX_TE_P2MP_TUNNEL_INFO,0 },
{ "tTePathInfo", "MAX_TE_PATH_INFO", sizeof(tTePathInfo),MAX_TE_PATH_INFO, MAX_TE_PATH_INFO,0 },
{ "tTePathListInfoSize", "MAX_TE_PATH_LIST", sizeof(tTePathListInfoSize),MAX_TE_PATH_LIST, MAX_TE_PATH_LIST,0 },
{ "tRSVPTrfcParams", "MAX_TE_RSVP_TRFC_PARAMS", sizeof(tRSVPTrfcParams),MAX_TE_RSVP_TRFC_PARAMS, MAX_TE_RSVP_TRFC_PARAMS,0 },
{ "tTunnelErrInfo", "MAX_TE_TNL_ERROR_TABLE_INFO", sizeof(tTunnelErrInfo),MAX_TE_TNL_ERROR_TABLE_INFO, MAX_TE_TNL_ERROR_TABLE_INFO,0 },
{ "tTeTunnelIndexListSize", "MAX_TE_TNL_INDEX_LIST", sizeof(tTeTunnelIndexListSize),MAX_TE_TNL_INDEX_LIST, MAX_TE_TNL_INDEX_LIST,0 },
{ "tTeTnlSrlg", "MAX_TE_TNL_SRLG", sizeof(tTeTnlSrlg),MAX_TE_TNL_SRLG, MAX_TE_TNL_SRLG,0 },
{ "tTeTrfcParamsSize", "MAX_TE_TRFC_PARAMS", sizeof(tTeTrfcParamsSize),MAX_TE_TRFC_PARAMS, MAX_TE_TRFC_PARAMS,0 },
{ "tTeTnlInfo", "MAX_TE_TUNNEL_INFO", sizeof(tTeTnlInfo),MAX_TE_TUNNEL_INFO, MAX_TE_TUNNEL_INFO,0 },
{ "tTeReoptTnlInfo", "MAX_TE_TUNNEL_INFO", sizeof(tTeReoptTnlInfo),MAX_TE_TUNNEL_INFO,MAX_TE_TUNNEL_INFO,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _TESZ_C  */
extern tFsModSizingParams FsTESizingParams [];
#endif /*  _TESZ_C  */


