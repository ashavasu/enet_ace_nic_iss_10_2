/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdtelw.h,v 1.4 2008/08/20 15:16:09 iss Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsTunnelConfigured ARG_LIST((UINT4 *));

INT1
nmhGetMplsTunnelActive ARG_LIST((UINT4 *));

INT1
nmhGetMplsTunnelTEDistProto ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMplsTunnelMaxHops ARG_LIST((UINT4 *));

INT1
nmhGetMplsTunnelNotificationMaxRate ARG_LIST((UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsTunnelNotificationMaxRate ARG_LIST((UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsTunnelNotificationMaxRate ARG_LIST((UINT4 *  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MplsTunnelNotificationMaxRate ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsTunnelIndexNext ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for MplsTunnelTable. */
INT1
nmhValidateIndexInstanceMplsTunnelTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for MplsTunnelTable  */

INT1
nmhGetFirstIndexMplsTunnelTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsTunnelTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsTunnelName ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMplsTunnelDescr ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMplsTunnelIsIf ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetMplsTunnelIfIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetMplsTunnelOwner ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetMplsTunnelRole ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetMplsTunnelXCPointer ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetMplsTunnelSignallingProto ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetMplsTunnelSetupPrio ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetMplsTunnelHoldingPrio ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetMplsTunnelSessionAttributes ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMplsTunnelLocalProtectInUse ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetMplsTunnelResourcePointer ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetMplsTunnelPrimaryInstance ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetMplsTunnelInstancePriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetMplsTunnelHopTableIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetMplsTunnelPathInUse ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetMplsTunnelARHopTableIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetMplsTunnelCHopTableIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetMplsTunnelIncludeAnyAffinity ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetMplsTunnelIncludeAllAffinity ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetMplsTunnelExcludeAnyAffinity ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetMplsTunnelTotalUpTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetMplsTunnelInstanceUpTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetMplsTunnelPrimaryUpTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetMplsTunnelPathChanges ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetMplsTunnelLastPathChange ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetMplsTunnelCreationTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetMplsTunnelStateTransitions ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetMplsTunnelAdminStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetMplsTunnelOperStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetMplsTunnelRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetMplsTunnelStorageType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsTunnelName ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetMplsTunnelDescr ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetMplsTunnelIsIf ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetMplsTunnelRole ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetMplsTunnelXCPointer ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetMplsTunnelSignallingProto ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetMplsTunnelSetupPrio ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetMplsTunnelHoldingPrio ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetMplsTunnelSessionAttributes ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetMplsTunnelLocalProtectInUse ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetMplsTunnelResourcePointer ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetMplsTunnelInstancePriority ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetMplsTunnelHopTableIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetMplsTunnelPathInUse ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetMplsTunnelIncludeAnyAffinity ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetMplsTunnelIncludeAllAffinity ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetMplsTunnelExcludeAnyAffinity ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetMplsTunnelAdminStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetMplsTunnelRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetMplsTunnelStorageType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsTunnelName ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2MplsTunnelDescr ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2MplsTunnelIsIf ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2MplsTunnelRole ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2MplsTunnelXCPointer ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2MplsTunnelSignallingProto ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2MplsTunnelSetupPrio ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2MplsTunnelHoldingPrio ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2MplsTunnelSessionAttributes ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2MplsTunnelLocalProtectInUse ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2MplsTunnelResourcePointer ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2MplsTunnelInstancePriority ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2MplsTunnelHopTableIndex ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2MplsTunnelPathInUse ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2MplsTunnelIncludeAnyAffinity ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2MplsTunnelIncludeAllAffinity ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2MplsTunnelExcludeAnyAffinity ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2MplsTunnelAdminStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2MplsTunnelRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2MplsTunnelStorageType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MplsTunnelTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsTunnelHopListIndexNext ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for MplsTunnelHopTable. */
INT1
nmhValidateIndexInstanceMplsTunnelHopTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for MplsTunnelHopTable  */

INT1
nmhGetFirstIndexMplsTunnelHopTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsTunnelHopTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsTunnelHopAddrType ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetMplsTunnelHopIpAddr ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMplsTunnelHopIpPrefixLen ARG_LIST((UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetMplsTunnelHopAsNumber ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMplsTunnelHopAddrUnnum ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMplsTunnelHopLspId ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMplsTunnelHopType ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetMplsTunnelHopInclude ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetMplsTunnelHopPathOptionName ARG_LIST((UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMplsTunnelHopEntryPathComp ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetMplsTunnelHopRowStatus ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetMplsTunnelHopStorageType ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsTunnelHopAddrType ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetMplsTunnelHopIpAddr ARG_LIST((UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetMplsTunnelHopIpPrefixLen ARG_LIST((UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetMplsTunnelHopAsNumber ARG_LIST((UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetMplsTunnelHopAddrUnnum ARG_LIST((UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetMplsTunnelHopLspId ARG_LIST((UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetMplsTunnelHopType ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetMplsTunnelHopInclude ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetMplsTunnelHopPathOptionName ARG_LIST((UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetMplsTunnelHopEntryPathComp ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetMplsTunnelHopRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetMplsTunnelHopStorageType ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsTunnelHopAddrType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2MplsTunnelHopIpAddr ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2MplsTunnelHopIpPrefixLen ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2MplsTunnelHopAsNumber ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2MplsTunnelHopAddrUnnum ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2MplsTunnelHopLspId ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2MplsTunnelHopType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2MplsTunnelHopInclude ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2MplsTunnelHopPathOptionName ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2MplsTunnelHopEntryPathComp ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2MplsTunnelHopRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2MplsTunnelHopStorageType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MplsTunnelHopTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsTunnelResourceIndexNext ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for MplsTunnelResourceTable. */
INT1
nmhValidateIndexInstanceMplsTunnelResourceTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for MplsTunnelResourceTable  */

INT1
nmhGetFirstIndexMplsTunnelResourceTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsTunnelResourceTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsTunnelResourceMaxRate ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetMplsTunnelResourceMeanRate ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetMplsTunnelResourceMaxBurstSize ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetMplsTunnelResourceMeanBurstSize ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetMplsTunnelResourceExBurstSize ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetMplsTunnelResourceFrequency ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetMplsTunnelResourceWeight ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetMplsTunnelResourceRowStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetMplsTunnelResourceStorageType ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsTunnelResourceMaxRate ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetMplsTunnelResourceMeanRate ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetMplsTunnelResourceMaxBurstSize ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetMplsTunnelResourceMeanBurstSize ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetMplsTunnelResourceExBurstSize ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetMplsTunnelResourceFrequency ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetMplsTunnelResourceWeight ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetMplsTunnelResourceRowStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetMplsTunnelResourceStorageType ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsTunnelResourceMaxRate ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2MplsTunnelResourceMeanRate ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2MplsTunnelResourceMaxBurstSize ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2MplsTunnelResourceMeanBurstSize ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2MplsTunnelResourceExBurstSize ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2MplsTunnelResourceFrequency ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2MplsTunnelResourceWeight ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2MplsTunnelResourceRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2MplsTunnelResourceStorageType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MplsTunnelResourceTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for MplsTunnelARHopTable. */
INT1
nmhValidateIndexInstanceMplsTunnelARHopTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for MplsTunnelARHopTable  */

INT1
nmhGetFirstIndexMplsTunnelARHopTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsTunnelARHopTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsTunnelARHopAddrType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetMplsTunnelARHopIpAddr ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMplsTunnelARHopAddrUnnum ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMplsTunnelARHopLspId ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Proto Validate Index Instance for MplsTunnelCHopTable. */
INT1
nmhValidateIndexInstanceMplsTunnelCHopTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for MplsTunnelCHopTable  */

INT1
nmhGetFirstIndexMplsTunnelCHopTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsTunnelCHopTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsTunnelCHopAddrType ARG_LIST((UINT4  , UINT4 ,INT4 *));

INT1
nmhGetMplsTunnelCHopIpAddr ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMplsTunnelCHopIpPrefixLen ARG_LIST((UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetMplsTunnelCHopAsNumber ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMplsTunnelCHopAddrUnnum ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMplsTunnelCHopLspId ARG_LIST((UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMplsTunnelCHopType ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Proto Validate Index Instance for MplsTunnelPerfTable. */
INT1
nmhValidateIndexInstanceMplsTunnelPerfTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for MplsTunnelPerfTable  */

INT1
nmhGetFirstIndexMplsTunnelPerfTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsTunnelPerfTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsTunnelPerfPackets ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetMplsTunnelPerfHCPackets ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetMplsTunnelPerfErrors ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetMplsTunnelPerfBytes ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetMplsTunnelPerfHCBytes ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_COUNTER64_TYPE *));

/* Proto Validate Index Instance for MplsTunnelCRLDPResTable. */
INT1
nmhValidateIndexInstanceMplsTunnelCRLDPResTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for MplsTunnelCRLDPResTable  */

INT1
nmhGetFirstIndexMplsTunnelCRLDPResTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsTunnelCRLDPResTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsTunnelCRLDPResMeanBurstSize ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetMplsTunnelCRLDPResExBurstSize ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetMplsTunnelCRLDPResFrequency ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetMplsTunnelCRLDPResWeight ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetMplsTunnelCRLDPResFlags ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetMplsTunnelCRLDPResRowStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetMplsTunnelCRLDPResStorageType ARG_LIST((UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsTunnelCRLDPResMeanBurstSize ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetMplsTunnelCRLDPResExBurstSize ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetMplsTunnelCRLDPResFrequency ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetMplsTunnelCRLDPResWeight ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetMplsTunnelCRLDPResFlags ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetMplsTunnelCRLDPResRowStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetMplsTunnelCRLDPResStorageType ARG_LIST((UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsTunnelCRLDPResMeanBurstSize ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2MplsTunnelCRLDPResExBurstSize ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2MplsTunnelCRLDPResFrequency ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2MplsTunnelCRLDPResWeight ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2MplsTunnelCRLDPResFlags ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2MplsTunnelCRLDPResRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2MplsTunnelCRLDPResStorageType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MplsTunnelCRLDPResTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsTunnelNotificationEnable ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsTunnelNotificationEnable ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsTunnelNotificationEnable ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MplsTunnelNotificationEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
