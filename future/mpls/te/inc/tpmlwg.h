/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: tpmlwg.h,v 1.1 2010/11/16 07:29:33 prabuc Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsTeP2mpTunnelConfigured ARG_LIST((UINT4 *));

INT1
nmhGetMplsTeP2mpTunnelActive ARG_LIST((UINT4 *));

INT1
nmhGetMplsTeP2mpTunnelTotalMaxHops ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for MplsTeP2mpTunnelTable. */
INT1
nmhValidateIndexInstanceMplsTeP2mpTunnelTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for MplsTeP2mpTunnelTable  */

INT1
nmhGetFirstIndexMplsTeP2mpTunnelTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsTeP2mpTunnelTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsTeP2mpTunnelP2mpIntegrity ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetMplsTeP2mpTunnelBranchRole ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetMplsTeP2mpTunnelP2mpXcIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMplsTeP2mpTunnelRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetMplsTeP2mpTunnelStorageType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsTeP2mpTunnelP2mpIntegrity ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetMplsTeP2mpTunnelBranchRole ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetMplsTeP2mpTunnelRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetMplsTeP2mpTunnelStorageType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsTeP2mpTunnelP2mpIntegrity ARG_LIST((UINT4 * , UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2MplsTeP2mpTunnelBranchRole ARG_LIST((UINT4 * , UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2MplsTeP2mpTunnelRowStatus ARG_LIST((UINT4 * , UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2MplsTeP2mpTunnelStorageType ARG_LIST((UINT4 * , UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MplsTeP2mpTunnelTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsTeP2mpTunnelSubGroupIDNext ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for MplsTeP2mpTunnelDestTable. */
INT1
nmhValidateIndexInstanceMplsTeP2mpTunnelDestTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for MplsTeP2mpTunnelDestTable  */

INT1
nmhGetFirstIndexMplsTeP2mpTunnelDestTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE *  , UINT4 * , INT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsTeP2mpTunnelDestTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE *  , UINT4 , UINT4 * , INT4 , INT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsTeP2mpTunnelDestBranchOutSegment ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetMplsTeP2mpTunnelDestHopTableIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetMplsTeP2mpTunnelDestPathInUse ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetMplsTeP2mpTunnelDestCHopTableIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetMplsTeP2mpTunnelDestARHopTableIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetMplsTeP2mpTunnelDestTotalUpTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetMplsTeP2mpTunnelDestInstanceUpTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetMplsTeP2mpTunnelDestPathChanges ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetMplsTeP2mpTunnelDestLastPathChange ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetMplsTeP2mpTunnelDestCreationTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetMplsTeP2mpTunnelDestStateTransitions ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetMplsTeP2mpTunnelDestDiscontinuityTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetMplsTeP2mpTunnelDestAdminStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetMplsTeP2mpTunnelDestOperStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetMplsTeP2mpTunnelDestRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

INT1
nmhGetMplsTeP2mpTunnelDestStorageType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE *,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsTeP2mpTunnelDestBranchOutSegment ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetMplsTeP2mpTunnelDestHopTableIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhSetMplsTeP2mpTunnelDestPathInUse ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhSetMplsTeP2mpTunnelDestAdminStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetMplsTeP2mpTunnelDestRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhSetMplsTeP2mpTunnelDestStorageType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsTeP2mpTunnelDestBranchOutSegment ARG_LIST((UINT4 * , UINT4  , UINT4  , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2MplsTeP2mpTunnelDestHopTableIndex ARG_LIST((UINT4 * , UINT4  , UINT4  , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhTestv2MplsTeP2mpTunnelDestPathInUse ARG_LIST((UINT4 * , UINT4  , UINT4  , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,UINT4 ));

INT1
nmhTestv2MplsTeP2mpTunnelDestAdminStatus ARG_LIST((UINT4 * , UINT4  , UINT4  , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2MplsTeP2mpTunnelDestRowStatus ARG_LIST((UINT4 * , UINT4  , UINT4  , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

INT1
nmhTestv2MplsTeP2mpTunnelDestStorageType ARG_LIST((UINT4 * , UINT4  , UINT4  , UINT4  , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * , UINT4  , INT4  , tSNMP_OCTET_STRING_TYPE * ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MplsTeP2mpTunnelDestTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for MplsTeP2mpTunnelBranchPerfTable. */
INT1
nmhValidateIndexInstanceMplsTeP2mpTunnelBranchPerfTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , tSNMP_OCTET_STRING_TYPE *));

/* Proto Type for Low Level GET FIRST fn for MplsTeP2mpTunnelBranchPerfTable  */

INT1
nmhGetFirstIndexMplsTeP2mpTunnelBranchPerfTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 * , tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexMplsTeP2mpTunnelBranchPerfTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , tSNMP_OCTET_STRING_TYPE *, tSNMP_OCTET_STRING_TYPE * ));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsTeP2mpTunnelBranchPerfPackets ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetMplsTeP2mpTunnelBranchPerfHCPackets ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetMplsTeP2mpTunnelBranchPerfErrors ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetMplsTeP2mpTunnelBranchPerfBytes ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

INT1
nmhGetMplsTeP2mpTunnelBranchPerfHCBytes ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , tSNMP_OCTET_STRING_TYPE *,tSNMP_COUNTER64_TYPE *));

INT1
nmhGetMplsTeP2mpTunnelBranchDiscontinuityTime ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , tSNMP_OCTET_STRING_TYPE *,UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetMplsTeP2mpTunnelNotificationEnable ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetMplsTeP2mpTunnelNotificationEnable ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2MplsTeP2mpTunnelNotificationEnable ARG_LIST((UINT4 * , INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2MplsTeP2mpTunnelNotificationEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

