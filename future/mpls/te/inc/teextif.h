/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: teextif.h,v 1.9 2016/03/11 11:32:54 siva Exp $
 *
 * Description: Contains extern declarations of the external
 *              interface routines.
 *******************************************************************/

#ifndef _TE_EXIF_H
#define _TE_EXIF_H
extern tTnlCliArgs  gaTnlCliArgs[CLI_MAX_SESSIONS];
extern tTeCliArgs gaTeCliArgs[CLI_MAX_SESSIONS];
extern UINT1        RpteDumpAllRsvpTeTnls (INT4
                                           *pi4RetValFsMplsDisplayRsvpTeTnls);
extern UINT1 RpteTEEventHandler ARG_LIST ((UINT4 u4Event, tTeTnlInfo *pTeTnlInfo, 
                                           UINT1 *pu1IsRpteAdminDown));
extern UINT1 LdpTEEventHandler ARG_LIST ((UINT4 u4Event, tTeTnlInfo *pTeTnlInfo));
extern INT1 nmhGetFsMplsRsvpTeMaxTnls ARG_LIST((INT4 *));
extern INT4
MplsOamIfHdlPathStatusChgForMeg (tMplsApiInInfo *pInMplsApiInfo,
                                 tMplsApiOutInfo *pOutMplsApiInfo);
extern UINT1 RpteTEProcessTeEvent ARG_LIST((tRpteEnqueueMsgInfo *pRpteEnqueueMsgInfo));

extern VOID RpteTEGetReoptTime ARG_LIST ((tTeTnlInfo *pTeTnlInfo, UINT4 *pu4RemainingTime));
#endif
