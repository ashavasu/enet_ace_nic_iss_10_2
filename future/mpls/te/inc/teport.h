/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: teport.h,v 1.6 2018/01/03 11:31:27 siva Exp $
 *
 * Description: The File Contains TE porting related definitions.
 *******************************************************************/

#ifndef _TE_PORT_H
#define _TE_PORT_H


/* CRLDP Related */
#ifdef MPLS_LDP_WANTED
#define TE_LDP_SUCCESS              LDP_SUCCESS
#define TE_LDP_FAILURE              LDP_FAILURE

#define TeLdpTEEventHandler         LdpTEEventHandler
#else
#define TE_LDP_SUCCESS              1
#define TE_LDP_FAILURE              0
#endif

/* RSVPTE Related */
#ifdef MPLS_RSVPTE_WANTED
#define TE_RPTE_SUCCESS             RPTE_SUCCESS
#define TE_RPTE_FAILURE             RPTE_FAILURE

#define TeRpteTEEventHandler        RpteTEEventHandler
#define TeRpteL3VPNEventHandler     RpteL3VPNEventHandler
#define TeRpteProcessTeEvent        RpteTEProcessTeEvent
#define TeRpteGetReoptTime          RpteTEGetReoptTime
#else
#define TE_RPTE_SUCCESS              1
#define TE_RPTE_FAILURE              0
#endif
#define TE_GET_IFACE_NAME_FRM_INDEX   CfaGetInterfaceNameFromIndex
#define TE_IP_GET_IF_INDEX_FROM_ADDR  IpGetIfIndexFromAddr
#define TE_CREATE_MPLS_TNL_IF_IN_CFA  CfaCreateMplsTunnelInterface
#define TE_DELETE_MPLS_TNL_IF_IN_CFA  CfaDeleteMplsTunnelInterface
#define TE_REG_MPLS_TNL_IF_WITH_CFA   CfaRegisterMplsTunnelInterface
#endif
