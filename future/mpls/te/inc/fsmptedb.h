/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: fsmptedb.h,v 1.11 2016/02/03 10:37:59 siva Exp $
*
* Description: Protocol Mib Data base for Proprietary MPLS TE table
*********************************************************************/
#ifndef _FSMPTEDB_H
#define _FSMPTEDB_H

UINT1 FsMplsTunnelTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMplsTpTunnelTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsTunnelAttributeTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMplsTunnelSrlgTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsTunnelAttributeSrlgTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMplsTunnelHopTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMplsReoptimizationTunnelTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fsmpte [] ={1,3,6,1,4,1,2076,13,12};
tSNMP_OID_TYPE fsmpteOID = {9, fsmpte};


UINT4 FsMplsTunnelType [ ] ={1,3,6,1,4,1,2076,13,12,1,2,1,1};
UINT4 FsMplsTunnelLSRIdMapInfo [ ] ={1,3,6,1,4,1,2076,13,12,1,2,1,2};
UINT4 FsMplsTunnelMode [ ] ={1,3,6,1,4,1,2076,13,12,1,2,1,3};
UINT4 FsMplsTunnelProactiveSessIndex [ ] ={1,3,6,1,4,1,2076,13,12,1,2,1,4};
UINT4 FsMplsTunnelMBBStatus [ ] ={1,3,6,1,4,1,2076,13,12,1,2,1,5};
UINT4 FsMplsTunnelDisJointType [ ] ={1,3,6,1,4,1,2076,13,12,1,2,1,6};
UINT4 FsMplsTunnelAttPointer [ ] ={1,3,6,1,4,1,2076,13,12,1,2,1,7};
UINT4 FsMplsTunnelEndToEndProtection [ ] ={1,3,6,1,4,1,2076,13,12,1,2,1,8};
UINT4 FsMplsTunnelPrConfigOperType [ ] ={1,3,6,1,4,1,2076,13,12,1,2,1,9};
UINT4 FsMplsTunnelSrlgType [ ] ={1,3,6,1,4,1,2076,13,12,1,2,1,10};
UINT4 FsMplsTunnelIfIndex [ ] ={1,3,6,1,4,1,2076,13,12,1,2,1,11};
UINT4 FsMplsTunnelInitReOptimize [ ] ={1,3,6,1,4,1,2076,13,12,1,2,1,12};
UINT4 FsMplsTunnelIsProtectingLsp [ ] ={1,3,6,1,4,1,2076,13,12,1,2,1,13};
UINT4 FsMplsLspTunnelMapIndex [ ] ={1,3,6,1,4,1,2076,13,12,1,2,1,14};
UINT4 FsMplsLspTunnelMapInstance [ ] ={1,3,6,1,4,1,2076,13,12,1,2,1,15};
UINT4 FsMplsLspTunnelMapIngressLSRId [ ] ={1,3,6,1,4,1,2076,13,12,1,2,1,16};
UINT4 FsMplsLspTunnelMapEgressLSRId [ ] ={1,3,6,1,4,1,2076,13,12,1,2,1,17};
UINT4 FsMplsTunnelSynchronizationStatus [ ] ={1,3,6,1,4,1,2076,13,12,1,2,1,18};
UINT4 FsMplsTunnelOutPathMsgId [ ] ={1,3,6,1,4,1,2076,13,12,1,2,1,19};
UINT4 FsMplsTunnelBackupHopTableIndex [ ] ={1,3,6,1,4,1,2076,13,12,1,2,1,20};
UINT4 FsMplsTunnelBackupPathInUse [ ] ={1,3,6,1,4,1,2076,13,12,1,2,1,21};
UINT4 FsMplsTpTunnelDestTunnelIndex [ ] ={1,3,6,1,4,1,2076,13,12,1,3,1,1};
UINT4 FsMplsTpTunnelDestTunnelLspNum [ ] ={1,3,6,1,4,1,2076,13,12,1,3,1,2};
UINT4 FsTunnelAttributeIndexNext [ ] ={1,3,6,1,4,1,2076,13,12,1,4};
UINT4 FsTunnelAttributeIndex [ ] ={1,3,6,1,4,1,2076,13,12,1,5,1,1};
UINT4 FsTunnelAttributeName [ ] ={1,3,6,1,4,1,2076,13,12,1,5,1,2};
UINT4 FsTunnelAttributeSetupPrio [ ] ={1,3,6,1,4,1,2076,13,12,1,5,1,3};
UINT4 FsTunnelAttributeHoldingPrio [ ] ={1,3,6,1,4,1,2076,13,12,1,5,1,4};
UINT4 FsTunnelAttributeIncludeAnyAffinity [ ] ={1,3,6,1,4,1,2076,13,12,1,5,1,5};
UINT4 FsTunnelAttributeIncludeAllAffinity [ ] ={1,3,6,1,4,1,2076,13,12,1,5,1,6};
UINT4 FsTunnelAttributeExcludeAnyAffinity [ ] ={1,3,6,1,4,1,2076,13,12,1,5,1,7};
UINT4 FsTunnelAttributeSessionAttributes [ ] ={1,3,6,1,4,1,2076,13,12,1,5,1,8};
UINT4 FsTunnelAttributeBandwidth [ ] ={1,3,6,1,4,1,2076,13,12,1,5,1,9};
UINT4 FsTunnelAttributeTeClassType [ ] ={1,3,6,1,4,1,2076,13,12,1,5,1,10};
UINT4 FsTunnelAttributeSrlgType [ ] ={1,3,6,1,4,1,2076,13,12,1,5,1,11};
UINT4 FsTunnelAttributeRowStatus [ ] ={1,3,6,1,4,1,2076,13,12,1,5,1,12};
UINT4 FsTunnelAttributeMask [ ] ={1,3,6,1,4,1,2076,13,12,1,5,1,13};
UINT4 FsMplsTunnelSrlgNo [ ] ={1,3,6,1,4,1,2076,13,12,1,6,1,1};
UINT4 FsMplsTunnelSrlgRowStatus [ ] ={1,3,6,1,4,1,2076,13,12,1,6,1,2};
UINT4 FsTunnelAttributeSrlgNo [ ] ={1,3,6,1,4,1,2076,13,12,1,7,1,1};
UINT4 FsTunnelAttributeSrlgRowStatus [ ] ={1,3,6,1,4,1,2076,13,12,1,7,1,2};
UINT4 FsMplsTunnelHopIncludeAny [ ] ={1,3,6,1,4,1,2076,13,12,1,8,1,1};
UINT4 FsGmplsTunnelNotifyErrorTrapEnable [ ] ={1,3,6,1,4,1,2076,13,12,1,1,1};
UINT4 FsMplsReoptimizationTunnelIndex [ ] ={1,3,6,1,4,1,2076,13,12,1,9,1,1};
UINT4 FsMplsReoptimizationTunnelIngressLSRId [ ] ={1,3,6,1,4,1,2076,13,12,1,9,1,2};
UINT4 FsMplsReoptimizationTunnelEgressLSRId [ ] ={1,3,6,1,4,1,2076,13,12,1,9,1,3};
UINT4 FsMplsReoptimizationTunnelStatus [ ] ={1,3,6,1,4,1,2076,13,12,1,9,1,4};
UINT4 FsMplsReoptimizationTunnelManualTrigger [ ] ={1,3,6,1,4,1,2076,13,12,1,9,1,5};
UINT4 FsMplsReoptimizationTunnelRowStatus [ ] ={1,3,6,1,4,1,2076,13,12,1,9,1,6};



tMbDbEntry fsmpteMibEntry[]= {

{{12,FsGmplsTunnelNotifyErrorTrapEnable}, NULL, FsGmplsTunnelNotifyErrorTrapEnableGet, FsGmplsTunnelNotifyErrorTrapEnableSet, FsGmplsTunnelNotifyErrorTrapEnableTest, FsGmplsTunnelNotifyErrorTrapEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{13,FsMplsTunnelType}, GetNextIndexFsMplsTunnelTable, FsMplsTunnelTypeGet, FsMplsTunnelTypeSet, FsMplsTunnelTypeTest, FsMplsTunnelTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMplsTunnelTableINDEX, 4, 0, 0, "128"},

{{13,FsMplsTunnelLSRIdMapInfo}, GetNextIndexFsMplsTunnelTable, FsMplsTunnelLSRIdMapInfoGet, FsMplsTunnelLSRIdMapInfoSet, FsMplsTunnelLSRIdMapInfoTest, FsMplsTunnelTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,FsMplsTunnelMode}, GetNextIndexFsMplsTunnelTable, FsMplsTunnelModeGet, FsMplsTunnelModeSet, FsMplsTunnelModeTest, FsMplsTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsTunnelTableINDEX, 4, 0, 0, "0"},

{{13,FsMplsTunnelProactiveSessIndex}, GetNextIndexFsMplsTunnelTable, FsMplsTunnelProactiveSessIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,FsMplsTunnelMBBStatus}, GetNextIndexFsMplsTunnelTable, FsMplsTunnelMBBStatusGet, FsMplsTunnelMBBStatusSet, FsMplsTunnelMBBStatusTest, FsMplsTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsTunnelTableINDEX, 4, 0, 0, "2"},

{{13,FsMplsTunnelDisJointType}, GetNextIndexFsMplsTunnelTable, FsMplsTunnelDisJointTypeGet, FsMplsTunnelDisJointTypeSet, FsMplsTunnelDisJointTypeTest, FsMplsTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsTunnelTableINDEX, 4, 0, 0, "1"},

{{13,FsMplsTunnelAttPointer}, GetNextIndexFsMplsTunnelTable, FsMplsTunnelAttPointerGet, FsMplsTunnelAttPointerSet, FsMplsTunnelAttPointerTest, FsMplsTunnelTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, FsMplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,FsMplsTunnelEndToEndProtection}, GetNextIndexFsMplsTunnelTable, FsMplsTunnelEndToEndProtectionGet, FsMplsTunnelEndToEndProtectionSet, FsMplsTunnelEndToEndProtectionTest, FsMplsTunnelTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsMplsTunnelTableINDEX, 4, 0, 0, "128"},

{{13,FsMplsTunnelPrConfigOperType}, GetNextIndexFsMplsTunnelTable, FsMplsTunnelPrConfigOperTypeGet, FsMplsTunnelPrConfigOperTypeSet, FsMplsTunnelPrConfigOperTypeTest, FsMplsTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsTunnelTableINDEX, 4, 0, 0, "2"},

{{13,FsMplsTunnelSrlgType}, GetNextIndexFsMplsTunnelTable, FsMplsTunnelSrlgTypeGet, FsMplsTunnelSrlgTypeSet, FsMplsTunnelSrlgTypeTest, FsMplsTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,FsMplsTunnelIfIndex}, GetNextIndexFsMplsTunnelTable, FsMplsTunnelIfIndexGet, FsMplsTunnelIfIndexSet, FsMplsTunnelIfIndexTest, FsMplsTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsTunnelTableINDEX, 4, 0, 0, "0"},

{{13,FsMplsTunnelInitReOptimize}, GetNextIndexFsMplsTunnelTable, FsMplsTunnelInitReOptimizeGet, FsMplsTunnelInitReOptimizeSet, FsMplsTunnelInitReOptimizeTest, FsMplsTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsTunnelTableINDEX, 4, 0, 0, "2"},

{{13,FsMplsTunnelIsProtectingLsp}, GetNextIndexFsMplsTunnelTable, FsMplsTunnelIsProtectingLspGet, FsMplsTunnelIsProtectingLspSet, FsMplsTunnelIsProtectingLspTest, FsMplsTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsTunnelTableINDEX, 4, 0, 0, "2"},

{{13,FsMplsLspTunnelMapIndex}, GetNextIndexFsMplsTunnelTable, FsMplsLspTunnelMapIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,FsMplsLspTunnelMapInstance}, GetNextIndexFsMplsTunnelTable, FsMplsLspTunnelMapInstanceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,FsMplsLspTunnelMapIngressLSRId}, GetNextIndexFsMplsTunnelTable, FsMplsLspTunnelMapIngressLSRIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,FsMplsLspTunnelMapEgressLSRId}, GetNextIndexFsMplsTunnelTable, FsMplsLspTunnelMapEgressLSRIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,FsMplsTunnelSynchronizationStatus}, GetNextIndexFsMplsTunnelTable, FsMplsTunnelSynchronizationStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,FsMplsTunnelOutPathMsgId}, GetNextIndexFsMplsTunnelTable, FsMplsTunnelOutPathMsgIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, FsMplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,FsMplsTunnelBackupHopTableIndex}, GetNextIndexFsMplsTunnelTable, FsMplsTunnelBackupHopTableIndexGet, FsMplsTunnelBackupHopTableIndexSet, FsMplsTunnelBackupHopTableIndexTest, FsMplsTunnelTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsTunnelTableINDEX, 4, 0, 0, "0"},

{{13,FsMplsTunnelBackupPathInUse}, GetNextIndexFsMplsTunnelTable, FsMplsTunnelBackupPathInUseGet, FsMplsTunnelBackupPathInUseSet, FsMplsTunnelBackupPathInUseTest, FsMplsTunnelTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsTunnelTableINDEX, 4, 0, 0, "0"},


{{13,FsMplsTpTunnelDestTunnelIndex}, GetNextIndexFsMplsTpTunnelTable, FsMplsTpTunnelDestTunnelIndexGet, FsMplsTpTunnelDestTunnelIndexSet, FsMplsTpTunnelDestTunnelIndexTest, FsMplsTpTunnelTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsTpTunnelTableINDEX, 4, 0, 0, NULL},

{{13,FsMplsTpTunnelDestTunnelLspNum}, GetNextIndexFsMplsTpTunnelTable, FsMplsTpTunnelDestTunnelLspNumGet, FsMplsTpTunnelDestTunnelLspNumSet, FsMplsTpTunnelDestTunnelLspNumTest, FsMplsTpTunnelTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsTpTunnelTableINDEX, 4, 0, 0, NULL},

{{11,FsTunnelAttributeIndexNext}, NULL, FsTunnelAttributeIndexNextGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,FsTunnelAttributeIndex}, GetNextIndexFsTunnelAttributeTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsTunnelAttributeTableINDEX, 1, 0, 0, NULL},

{{13,FsTunnelAttributeName}, GetNextIndexFsTunnelAttributeTable, FsTunnelAttributeNameGet, FsTunnelAttributeNameSet, FsTunnelAttributeNameTest, FsTunnelAttributeTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsTunnelAttributeTableINDEX, 1, 0, 0, ""},

{{13,FsTunnelAttributeSetupPrio}, GetNextIndexFsTunnelAttributeTable, FsTunnelAttributeSetupPrioGet, FsTunnelAttributeSetupPrioSet, FsTunnelAttributeSetupPrioTest, FsTunnelAttributeTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsTunnelAttributeTableINDEX, 1, 0, 0, NULL},

{{13,FsTunnelAttributeHoldingPrio}, GetNextIndexFsTunnelAttributeTable, FsTunnelAttributeHoldingPrioGet, FsTunnelAttributeHoldingPrioSet, FsTunnelAttributeHoldingPrioTest, FsTunnelAttributeTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsTunnelAttributeTableINDEX, 1, 0, 0, NULL},

{{13,FsTunnelAttributeIncludeAnyAffinity}, GetNextIndexFsTunnelAttributeTable, FsTunnelAttributeIncludeAnyAffinityGet, FsTunnelAttributeIncludeAnyAffinitySet, FsTunnelAttributeIncludeAnyAffinityTest, FsTunnelAttributeTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsTunnelAttributeTableINDEX, 1, 0, 0, NULL},

{{13,FsTunnelAttributeIncludeAllAffinity}, GetNextIndexFsTunnelAttributeTable, FsTunnelAttributeIncludeAllAffinityGet, FsTunnelAttributeIncludeAllAffinitySet, FsTunnelAttributeIncludeAllAffinityTest, FsTunnelAttributeTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsTunnelAttributeTableINDEX, 1, 0, 0, NULL},

{{13,FsTunnelAttributeExcludeAnyAffinity}, GetNextIndexFsTunnelAttributeTable, FsTunnelAttributeExcludeAnyAffinityGet, FsTunnelAttributeExcludeAnyAffinitySet, FsTunnelAttributeExcludeAnyAffinityTest, FsTunnelAttributeTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsTunnelAttributeTableINDEX, 1, 0, 0, NULL},

{{13,FsTunnelAttributeSessionAttributes}, GetNextIndexFsTunnelAttributeTable, FsTunnelAttributeSessionAttributesGet, FsTunnelAttributeSessionAttributesSet, FsTunnelAttributeSessionAttributesTest, FsTunnelAttributeTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsTunnelAttributeTableINDEX, 1, 0, 0, NULL},

{{13,FsTunnelAttributeBandwidth}, GetNextIndexFsTunnelAttributeTable, FsTunnelAttributeBandwidthGet, FsTunnelAttributeBandwidthSet, FsTunnelAttributeBandwidthTest, FsTunnelAttributeTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsTunnelAttributeTableINDEX, 1, 0, 0, NULL},

{{13,FsTunnelAttributeTeClassType}, GetNextIndexFsTunnelAttributeTable, FsTunnelAttributeTeClassTypeGet, FsTunnelAttributeTeClassTypeSet, FsTunnelAttributeTeClassTypeTest, FsTunnelAttributeTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, FsTunnelAttributeTableINDEX, 1, 0, 0, NULL},

{{13,FsTunnelAttributeSrlgType}, GetNextIndexFsTunnelAttributeTable, FsTunnelAttributeSrlgTypeGet, FsTunnelAttributeSrlgTypeSet, FsTunnelAttributeSrlgTypeTest, FsTunnelAttributeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsTunnelAttributeTableINDEX, 1, 0, 0, NULL},

{{13,FsTunnelAttributeRowStatus}, GetNextIndexFsTunnelAttributeTable, FsTunnelAttributeRowStatusGet, FsTunnelAttributeRowStatusSet, FsTunnelAttributeRowStatusTest, FsTunnelAttributeTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsTunnelAttributeTableINDEX, 1, 0, 1, NULL},

{{13,FsTunnelAttributeMask}, GetNextIndexFsTunnelAttributeTable, FsTunnelAttributeMaskGet, FsTunnelAttributeMaskSet, FsTunnelAttributeMaskTest, FsTunnelAttributeTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, FsTunnelAttributeTableINDEX, 1, 0, 0, NULL},

{{13,FsMplsTunnelSrlgNo}, GetNextIndexFsMplsTunnelSrlgTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMplsTunnelSrlgTableINDEX, 5, 0, 0, NULL},

{{13,FsMplsTunnelSrlgRowStatus}, GetNextIndexFsMplsTunnelSrlgTable, FsMplsTunnelSrlgRowStatusGet, FsMplsTunnelSrlgRowStatusSet, FsMplsTunnelSrlgRowStatusTest, FsMplsTunnelSrlgTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsTunnelSrlgTableINDEX, 5, 0, 1, NULL},

{{13,FsTunnelAttributeSrlgNo}, GetNextIndexFsTunnelAttributeSrlgTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsTunnelAttributeSrlgTableINDEX, 2, 0, 0, NULL},

{{13,FsTunnelAttributeSrlgRowStatus}, GetNextIndexFsTunnelAttributeSrlgTable, FsTunnelAttributeSrlgRowStatusGet, FsTunnelAttributeSrlgRowStatusSet, FsTunnelAttributeSrlgRowStatusTest, FsTunnelAttributeSrlgTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsTunnelAttributeSrlgTableINDEX, 2, 0, 1, NULL},

{{13,FsMplsTunnelHopIncludeAny}, GetNextIndexFsMplsTunnelHopTable, FsMplsTunnelHopIncludeAnyGet, FsMplsTunnelHopIncludeAnySet, FsMplsTunnelHopIncludeAnyTest, FsMplsTunnelHopTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsTunnelHopTableINDEX, 3, 0, 0, "2"},

{{13,FsMplsReoptimizationTunnelIndex}, GetNextIndexFsMplsReoptimizationTunnelTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMplsReoptimizationTunnelTableINDEX, 3, 0, 0, NULL},

{{13,FsMplsReoptimizationTunnelIngressLSRId}, GetNextIndexFsMplsReoptimizationTunnelTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMplsReoptimizationTunnelTableINDEX, 3, 0, 0, NULL},

{{13,FsMplsReoptimizationTunnelEgressLSRId}, GetNextIndexFsMplsReoptimizationTunnelTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMplsReoptimizationTunnelTableINDEX, 3, 0, 0, NULL},

{{13,FsMplsReoptimizationTunnelStatus}, GetNextIndexFsMplsReoptimizationTunnelTable, FsMplsReoptimizationTunnelStatusGet, FsMplsReoptimizationTunnelStatusSet, FsMplsReoptimizationTunnelStatusTest, FsMplsReoptimizationTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsReoptimizationTunnelTableINDEX, 3, 0, 0, "2"},

{{13,FsMplsReoptimizationTunnelManualTrigger}, GetNextIndexFsMplsReoptimizationTunnelTable, FsMplsReoptimizationTunnelManualTriggerGet, FsMplsReoptimizationTunnelManualTriggerSet, FsMplsReoptimizationTunnelManualTriggerTest, FsMplsReoptimizationTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsReoptimizationTunnelTableINDEX, 3, 0, 0, "2"},

{{13,FsMplsReoptimizationTunnelRowStatus}, GetNextIndexFsMplsReoptimizationTunnelTable, FsMplsReoptimizationTunnelRowStatusGet, FsMplsReoptimizationTunnelRowStatusSet, FsMplsReoptimizationTunnelRowStatusTest, FsMplsReoptimizationTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsReoptimizationTunnelTableINDEX, 3, 0, 1, NULL},
};
tMibData fsmpteEntry = { 49, fsmpteMibEntry };

#endif /* _FSMPTEDB_H */

