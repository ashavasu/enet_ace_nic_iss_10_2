/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fshlspdb.h,v 1.1 2010/11/11 12:48:43 prabuc Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSHLSPDB_H
#define _FSHLSPDB_H

UINT1 FsMplsLSPMapTunnelTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 FsMplsHLSPTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 fshlsp [] ={1,3,6,1,4,1,29601,2,58};
tSNMP_OID_TYPE fshlspOID = {9, fshlsp};


UINT4 FsMplsLSPMapTunnelIndex [ ] ={1,3,6,1,4,1,29601,2,58,1,1,1,1};
UINT4 FsMplsLSPMapTunnelInstance [ ] ={1,3,6,1,4,1,29601,2,58,1,1,1,2};
UINT4 FsMplsLSPMapTunnelIngressLSRId [ ] ={1,3,6,1,4,1,29601,2,58,1,1,1,3};
UINT4 FsMplsLSPMapTunnelEgressLSRId [ ] ={1,3,6,1,4,1,29601,2,58,1,1,1,4};
UINT4 FsMplsLSPMapSubTunnelIndex [ ] ={1,3,6,1,4,1,29601,2,58,1,1,1,5};
UINT4 FsMplsLSPMapSubTunnelInstance [ ] ={1,3,6,1,4,1,29601,2,58,1,1,1,6};
UINT4 FsMplsLSPMapSubTunnelIngressLSRId [ ] ={1,3,6,1,4,1,29601,2,58,1,1,1,7};
UINT4 FsMplsLSPMapSubTunnelEgressLSRId [ ] ={1,3,6,1,4,1,29601,2,58,1,1,1,8};
UINT4 FsMplsLSPMaptunnelOperation [ ] ={1,3,6,1,4,1,29601,2,58,1,1,1,9};
UINT4 FsMplsLSPMaptunnelRowStatus [ ] ={1,3,6,1,4,1,29601,2,58,1,1,1,10};
UINT4 FsMplsHLSPIndex [ ] ={1,3,6,1,4,1,29601,2,58,1,2,1,1};
UINT4 FsMplsHLSPInstance [ ] ={1,3,6,1,4,1,29601,2,58,1,2,1,2};
UINT4 FsMplsHLSPIngressLSRId [ ] ={1,3,6,1,4,1,29601,2,58,1,2,1,3};
UINT4 FsMplsHLSPEgressLSRId [ ] ={1,3,6,1,4,1,29601,2,58,1,2,1,4};
UINT4 FsMplsHLSPAvailableBW [ ] ={1,3,6,1,4,1,29601,2,58,1,2,1,5};
UINT4 FsMplsHLSPNoOfStackedTunnels [ ] ={1,3,6,1,4,1,29601,2,58,1,2,1,6};




tMbDbEntry fshlspMibEntry[]= {

{{13,FsMplsLSPMapTunnelIndex}, GetNextIndexFsMplsLSPMapTunnelTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMplsLSPMapTunnelTableINDEX, 8, 0, 0, NULL},

{{13,FsMplsLSPMapTunnelInstance}, GetNextIndexFsMplsLSPMapTunnelTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMplsLSPMapTunnelTableINDEX, 8, 0, 0, NULL},

{{13,FsMplsLSPMapTunnelIngressLSRId}, GetNextIndexFsMplsLSPMapTunnelTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMplsLSPMapTunnelTableINDEX, 8, 0, 0, NULL},

{{13,FsMplsLSPMapTunnelEgressLSRId}, GetNextIndexFsMplsLSPMapTunnelTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMplsLSPMapTunnelTableINDEX, 8, 0, 0, NULL},

{{13,FsMplsLSPMapSubTunnelIndex}, GetNextIndexFsMplsLSPMapTunnelTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMplsLSPMapTunnelTableINDEX, 8, 0, 0, NULL},

{{13,FsMplsLSPMapSubTunnelInstance}, GetNextIndexFsMplsLSPMapTunnelTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMplsLSPMapTunnelTableINDEX, 8, 0, 0, NULL},

{{13,FsMplsLSPMapSubTunnelIngressLSRId}, GetNextIndexFsMplsLSPMapTunnelTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMplsLSPMapTunnelTableINDEX, 8, 0, 0, NULL},

{{13,FsMplsLSPMapSubTunnelEgressLSRId}, GetNextIndexFsMplsLSPMapTunnelTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMplsLSPMapTunnelTableINDEX, 8, 0, 0, NULL},

{{13,FsMplsLSPMaptunnelOperation}, GetNextIndexFsMplsLSPMapTunnelTable, FsMplsLSPMaptunnelOperationGet, FsMplsLSPMaptunnelOperationSet, FsMplsLSPMaptunnelOperationTest, FsMplsLSPMapTunnelTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, FsMplsLSPMapTunnelTableINDEX, 8, 0, 0, NULL},

{{13,FsMplsLSPMaptunnelRowStatus}, GetNextIndexFsMplsLSPMapTunnelTable, FsMplsLSPMaptunnelRowStatusGet, FsMplsLSPMaptunnelRowStatusSet, FsMplsLSPMaptunnelRowStatusTest, FsMplsLSPMapTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, FsMplsLSPMapTunnelTableINDEX, 8, 0, 1, NULL},

{{13,FsMplsHLSPIndex}, GetNextIndexFsMplsHLSPTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMplsHLSPTableINDEX, 4, 0, 0, NULL},

{{13,FsMplsHLSPInstance}, GetNextIndexFsMplsHLSPTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMplsHLSPTableINDEX, 4, 0, 0, NULL},

{{13,FsMplsHLSPIngressLSRId}, GetNextIndexFsMplsHLSPTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMplsHLSPTableINDEX, 4, 0, 0, NULL},

{{13,FsMplsHLSPEgressLSRId}, GetNextIndexFsMplsHLSPTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, FsMplsHLSPTableINDEX, 4, 0, 0, NULL},

{{13,FsMplsHLSPAvailableBW}, GetNextIndexFsMplsHLSPTable, FsMplsHLSPAvailableBWGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMplsHLSPTableINDEX, 4, 0, 0, NULL},

{{13,FsMplsHLSPNoOfStackedTunnels}, GetNextIndexFsMplsHLSPTable, FsMplsHLSPNoOfStackedTunnelsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, FsMplsHLSPTableINDEX, 4, 0, 0, NULL},
};
tMibData fshlspEntry = { 16, fshlspMibEntry };

#endif /* _FSHLSPDB_H */

