/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: teprot.h,v 1.50 2018/01/03 11:31:27 siva Exp $
 *
 * Description: This file contains the prototypes of routines used
 *              by TE.
 *******************************************************************/

#ifndef _TE_PROT_H
#define _TE_PROT_H


UINT1 TeSetDefaultRsvpTeParams ARG_LIST ((VOID));


UINT1 TeAllocateGblMemory ARG_LIST ((VOID));

VOID  TeDeAllocateGblMemory ARG_LIST ((VOID));

UINT1 teCreateInfoHeads ARG_LIST ((VOID));

VOID  teDeleteInfoHeads ARG_LIST ((VOID));

VOID TeAddNodeToTnlTable  ARG_LIST ((tTeTnlInfo * pTeTnlInfo));

UINT1 TeDeleteTnlFromTnlTable  ARG_LIST ((UINT4 u4TunnelIndex, 
                                          UINT4 u4TunnelInstance,
                                          UINT4 u4TunnelIngressLSRId,
                                          UINT4 u4TunnelEgressLSRId));

UINT1 TeChkTnlOperStatusByPrimaryInst ARG_LIST ((UINT4 u4TunnelIndex,
                                                 UINT4 u4TunnelInstance,
                                                 UINT4 u4TnlIngressLSRId,
                                                 UINT4 u4TnlEgressLSRId,
                                                 UINT4 *pu4MplsLspId));

UINT1 TeUpdateTnlInUsebyL2Vpn ARG_LIST ((UINT1 u1AssociateFlag,
                           UINT4 u4TunnelIndex,
                                         UINT4 u4TunnelInstance,
                                         UINT4 u4TnlIngressLSRId,
                                         UINT4 u4TnlEgressLSRId));

UINT4 TePostEvent ARG_LIST ((tTeTnlInfo *pTeTnlInfo, UINT4 u4BkpTnlMPXcIndex, 
                             UINT4 u4EvtType));


UINT1 TeIsAllErHopsActive ARG_LIST ((tTePathInfo *pTePathInfo,
                                     BOOL1 *pbIsPathChanged));

UINT1 TeSetEROPathStructure ARG_LIST ((UINT4 u4TnlHopTableIndex,UINT4 u4Flag,
           tTeTnlInfo *pTeTnlInfo));

UINT1 TeCheckArHopInfo ARG_LIST ((UINT4 u4ArHopListindex,
                                  UINT4 u4ArHopIndex,
                                  tTeArHopInfo **ppTeArHopInfo));

UINT1 TeCreatePathListInfo ARG_LIST ((tTePathListInfo **ppTePathListInfo));

UINT1 TeCreatePathOptionInfo ARG_LIST ((UINT4 u4HopListIndex, 
                                        UINT4 u4PathOptionIndex,
                                        tTePathInfo **ppTePathInfo));

UINT1 TeCreateHopInfo ARG_LIST ((UINT4 u4HopListIndex, UINT4 u4PathOptionIndex,
                                 UINT4 u4HopIndex, tTeHopInfo **ppTeHopInfo));

VOID  TeDeletePathListInfo ARG_LIST ((UINT4 u4PathListInfoIndex));

VOID  TeDeletePathOptionInfo ARG_LIST ((tTePathInfo *pTePathInfo));


UINT1 TeCheckPathOptionInfo ARG_LIST ((UINT4 u4HopListIndex, 
                                       UINT4 u4PathOptionIndex,
                                       tTePathInfo **ppTePathInfo));

UINT1 TeCheckHopInfo ARG_LIST ((UINT4 u4HopListIndex, UINT4 u4PathOptionIndex,
                                UINT4 u4HopIndex, tTeHopInfo **ppTeHopInfo));

UINT1 TeGetNextAvailableIndex (INT4 *pi4Index);

UINT1 TeVerifyCfgParams(VOID);

#ifndef MPLS_RSVPTE_WANTED
UINT1 TeRpteTEEventHandler ARG_LIST ((UINT4 u4Event, tTeTnlInfo *pTeTnlInfo,
                                      UINT1 *pu1IsRpteAdminDown));
UINT1 TeRpteL3VPNEventHandler ARG_LIST ((UINT4 u4Event, tTeTnlInfo *pTeTnlInfo));
#endif

#ifndef MPLS_LDP_WANTED
UINT1 TeLdpTEEventHandler ARG_LIST ((UINT4 u4Event, tTeTnlInfo *pTeTnlInfo));
#endif

UINT1 TeGetTnlOperStatusBySrcAndDest ARG_LIST ((UINT4 u4TnlIngressLSRId,
                                                UINT4 u4TnlEgressLSRId,
                                                UINT4 u4PriTunnelIndex,
                                                UINT4 *pu4TunnelIndex,
                                                UINT1 *pu1TunnelOperStatus));

INT4 TeSendTnlTrapAndSyslog (tTeTnlInfo *pTeTnlInfo, UINT1 u1TrapType);

INT4 TeSendGmplsTnlTrapAndSyslog (tTeTnlInfo *pTeTnlInfo, UINT1 u1TrapType);

INT4 TnlIndicesCmp ARG_LIST 
                 ((tTeTnlInfo *pFirstTeTnlInfo, tTeTnlInfo *pNextTeTnlInfo));

INT4
ReoptTnlListCmp (tTeReoptTnlInfo * pFirstReoptTeTnlInfo, tTeReoptTnlInfo * pNextReoptTeTnlInfo);

INT4 TnlIfIndexCmp ARG_LIST 
                 ((tTeTnlInfo *pFirstTeTnlInfo, tTeTnlInfo *pNextTeTnlInfo));

UINT1 TeCreateTrfcParams ARG_LIST ((UINT1         u1Protocol, 
                                    tTeTrfcParams **ppTeTrfcParams,
                                    tTeTrfcParams *pTempTeTrfcParams));


UINT1 TeDeleteHopListInfo ARG_LIST ((tTePathListInfo *pPathListInfo));

UINT1 TeDeleteTnlInfo ARG_LIST ((tTeTnlInfo *pTeTnlInfo,UINT1 u1CallerId));

UINT1 TeDeleteTrfcParams ARG_LIST ((tTeTrfcParams *pTeTrfcParams));

UINT1 TeAdminStatus ARG_LIST ((UINT1 u1Protocol, UINT1  u1AdminStatus));

UINT1 TeDeleteArHopListInfo ARG_LIST ((tTeArHopListInfo *pArHopListInfo));

UINT1 TeDeleteDiffServElspList 
           ARG_LIST ((tMplsDiffServElspList *pMplsDiffServElspList));

UINT1 TeUpdateTrfcParamsElspList
            ARG_LIST ((tMplsDiffServElspList *pMplsDiffServElspList,
                       tTeTrfcParams         *pTempTrfcParams,
                       UINT1                 u1ElspInfoIndex,
                       UINT1                 u1Operation));

UINT1 TeCreateMplsTunnelInterfaceInCfa
           ARG_LIST ((UINT4  u4MplsTunnelIngressLSRId,
                      UINT4  *pu4MplsTunnelIfIndex));

UINT1 TeDeleteMplsTunnelInterfaceInCfa 
           ARG_LIST ((UINT4  u4MplsTunnelIngressLSRId,
                      UINT4  u4MplsTunnelIfIndex));

UINT1 TeDeleteHopInfo ARG_LIST ((tTePathListInfo *pPathListInfo, 
                                 tTePathInfo     *pTePathInfo, 
                                 tTeHopInfo      *pTeHopInfo));

VOID TeDeleteTnlFrrInfo ARG_LIST ((tTeTnlInfo *pTeTnlInfo,
                                   tTeTnlInfo *pInstance0Tnl));

INT4 TeGetTnlCount ARG_LIST ((UINT1 u1Type));

UINT1
TeIsLsrPartOfErHop (CONST tTeHopInfo * pTeErHopInfo,
                    UINT1 *pu1IsEgrOfHop);
VOID
TeIsLsrPartOfCHop (const tTeCHopInfo * pTeCHopInfo, 
                   UINT1 *pu1IsEgrOfHop);
VOID
TeReestablishSigTunnel ARG_LIST ((tTeTnlInfo *pTeTnlInfo));

VOID
TePrcsL2vpnAssociation ARG_LIST ((tTeTnlInfo *pTeTnlInfo, UINT4 u4OperStatus));

UINT1
TeDeleteStackTnlInterface (tTeTnlInfo * pTeTnlInfo);

UINT4 TeSigGetTnlDest (tTeTnlInfo *pTeTnlInfo);
UINT4 TeSigGetL3IntfFromDestAddr (UINT4 u4DestAddr);

PUBLIC INT4
TeReleaseTnlInfo (tRBElem * pRBElem, UINT4 u4Arg);

UINT1
TeCmnExtUpdateTnlOperStatus (tTeTnlInfo * pTeTnlInfo,
                               tTeUpdateInfo * pTeUpdateInfo,
                               tTeTnlInfo *pInstance0);

UINT1
TeCmnExtNotifyTnlStatusToMeg (tTeTnlInfo * pTeTnlInfo,
                              tTeUpdateInfo * pTeUpdateInfo,
                              UINT1 u1PrevCPOrMgmtStatus);
UINT1
TeCmnExtUpdateTnlProtStatus (tTeTnlInfo * pTeTnlInfo,
                       tTeUpdateInfo * pTeUpdateInfo);
INT4
TeCmnExtUpdateCPAndOamOperStatus (tTeTnlInfo *pTeTnlInfo, UINT1 u1TeUpdateOpr,
                                  UINT1 u1OperStatus, UINT1 u1CPOrOamOperChgReqd);

UINT1
TeCmnExtCalculateTnlOperStatus (tTeTnlInfo *pTeTnlInfo);


/* Static H-LSP and S-LSP function prototypes. */
UINT4
TeUpdateHLSPParams(tteFsMplsLSPMapTunnelTable *pteFsMplsLSPMapTunnelTable);

UINT4
TeInitHLSPParams (tTeTnlInfo *pTeTnlInfo);

UINT4
TeDeInitHLSPParams (tTeTnlInfo * pTeTnlInfo);

UINT4
TeDeleteHLSPParams(tteFsMplsLSPMapTunnelTable *pteFsMplsLSPMapTunnelTable);

UINT4
TeHLSPModifyILM(tTeTnlInfo *pTeHlspTnlInfo);

INT4
TeUpdateServiceTnlOperStatus(tTeTnlInfo *pTeHlspTnlInfo,UINT1 u1TnlOperStatus);

UINT4
TeUpdateSLSPParams (tteFsMplsLSPMapTunnelTable * pteFsMplsLSPMapTunnelTable);

UINT4
TeDeleteSLSPParams (tteFsMplsLSPMapTunnelTable * pteFsMplsLSPMapTunnelTable);

INT4
TeValidateAssociatedTnlsXC (tTeTnlInfo *pTeHlspOrSlspTnlInfo, 
                            tTeTnlInfo *pTeTnlInfo, UINT4 *pu4ErrorCode);

INT1
TeTestAllGmplsTunnelTable ARG_LIST ((UINT4 *pu4ErrorCode,
                                     UINT4 u4TnlIndex, UINT4 u4TnlInstance,
                                     UINT4 u4IngressId, UINT4 u4EgressId,
                                     tGmplsTnlInfo *pGmplsTnlInfo,
                                     UINT4 u4ObjectId));
INT1
TeSetAllGmplsTunnelTable ARG_LIST ((UINT4 u4TnlIndex, UINT4 u4TnlInstance,
                                    UINT4 u4IngressId, UINT4 u4EgressId,
                                    tGmplsTnlInfo *pGmplsTnlInfo,
                                    UINT4 u4ObjectId));
INT1
TeGetAllGmplsTunnelTable ARG_LIST ((UINT4 u4TnlIndex, UINT4 u4TnlInstance,
                                    UINT4 u4IngressId, UINT4 u4EgressId,
                                    tGmplsTnlInfo *pGmplsTnlInfo));
INT1
TeTestAllGmplsTunnelHopTable ARG_LIST ((UINT4 *pu4ErrorCode,
                                        UINT4 u4MplsTnlHopListIndex,
                                        UINT4 u4MplsTnlHopPathOptIndex,
                                        UINT4 u4MplsTnlHopIndex,
                                        tGmplsTnlHopInfo *pGmplsTnlHopInfo,
                                        UINT4 u4ObjectId));

INT1
TeSetAllGmplsTunnelHopTable ARG_LIST ((UINT4 u4MplsTnlHopListIndex,
                                       UINT4 u4MplsTnlHopPathOptIndex,
                                       UINT4 u4MplsTnlHopIndex,
                                       tGmplsTnlHopInfo *pGmplsTnlHopInfo,
                                       UINT4 u4ObjectId));
INT1
TeGetAllGmplsTunnelHopTable ARG_LIST ((UINT4 u4MplsTnlHopListIndex,
                                       UINT4 u4MplsTnlHopPathOptIndex,
                                       UINT4 u4MplsTnlHopIndex,
                                       tGmplsTnlHopInfo *pGmplsTnlHopInfo));

INT1
TeGetAllGmplsTunnelArHopTable ARG_LIST ((UINT4 u4MplsTnlARHopListIndex,
                                         UINT4 u4MplsTnlARHopIndex,
                                         tGmplsTnlArHopInfo *pGmplsTnlArHopInfo));

INT1
TeGetAllMplsTunnelCHopTable ARG_LIST ((UINT4 u4MplsTnlCHopListIndex,
                                       UINT4 u4MplsTnlCHopIndex,
                                       tTeCHopInfo *pTeCHopInfo));

INT1
TeGetAllGmplsTunnelErrorTable ARG_LIST ((UINT4 u4MplsTnlIndex,
                              UINT4 u4MplsTnlInstance,
                              UINT4 u4MplsTnlIngressLSRId,
                              UINT4 u4MplsTnlEgressLSRId,
                              tTunnelErrInfo *pGetGmplsTnlErrInfo));
INT1
TeTestAllFsMplsTunnelTable ARG_LIST ((UINT4 *pu4ErrorCode,
                                      UINT4 u4MplsTnlIndex,
                                      UINT4 u4MplsTnlInstance,
                                      UINT4 u4MplsTnlIngressLSRId,
                                      UINT4 u4MplsTnlEgressLSRId,
                                      tTeTnlInfo *pTeTnlInfo,
                                      UINT4 u4ObjectId));

INT1
TeSetAllFsMplsTunnelTable ARG_LIST ((UINT4 u4MplsTnlIndex,
                                     UINT4 u4MplsTnlInstance,
                                     UINT4 u4MplsTnlIngressLSRId,
                                     UINT4  u4MplsTnlEgressLSRId,
                                     tTeTnlInfo *pTeTnlInfo,
                                     UINT4 u4ObjectId));

INT1
TeGetAllFsMplsTunnelTable ARG_LIST ((UINT4 u4MplsTnlIndex,
                                     UINT4 u4MplsTnlInstance,
                                     UINT4 u4MplsTnlIngressLSRId,
                                     UINT4 u4MplsTnlEgressLSRId,
                                     tTeTnlInfo *pTeTnlInfo));

INT1
TeGetAllFsMplsAttributeTable ARG_LIST ((UINT4 u4FsTnlAttrIndex,
                                        tTeAttrListInfo *pGetAttrInfo));

INT1
TeTestAllFsMplsAttributeTable ARG_LIST ((UINT4 *pu4ErrorCode,
                                         UINT4 u4FsTunnelAttributeIndex,
                                         tTeAttrListInfo *pTestAttrInfo,
                                         UINT4 u4ObjectId));

INT1
TeSetAllFsMplsAttributeTable ARG_LIST ((UINT4 u4FsTunnelAttributeIndex,
                                        tTeAttrListInfo *pSetTeAttrInfo,
                                        UINT4 u4ObjectId));
INT1
TeGetAllFsMplsTunnelSrlgTable ARG_LIST ((UINT4 u4MplsTunnelIndex,
                                         UINT4 u4MplsTunnelInstance,
                                         UINT4 u4MplsTunnelIngressLSRId,
                                         UINT4 u4MplsTunnelEgressLSRId,
                                         UINT4 u4SrlgNo,
                                         tTeTnlSrlg *pGetTeTnlSrlg));
INT1
TeSetAllFsMplsTunnelSrlgTable ARG_LIST ((UINT4 u4TnlIndex,
                                         UINT4 u4TnlInstance,
                                         UINT4 u4TnlIngressLSRId,
                                         UINT4 u4TnlEgressLSRId,
                                         UINT4 u4TnlSrlg,
                                         tTeTnlSrlg *pSetTeTnlSrlg,
                                         UINT4 u4ObjectId));

INT1
TeTestAllFsMplsTunnelSrlgTable ARG_LIST ((UINT4 *pu4ErrorCode,
                                          UINT4 u4TnlIndex,
                                          UINT4 u4TnlInstance,
                                          UINT4 u4TnlIngressLSRId,
                                          UINT4 u4TnlEgressLSRId,
                                          UINT4 u4SrlgNo,
                                          tTeTnlSrlg *pTestTeTnlSrlg,
                                          UINT4 u4ObjectId));

INT1
TeGetAllFsMplsAttributeSrlgTable ARG_LIST ((UINT4 u4AttrListIndex,
                                            UINT4 u4SrlgNo,
                                            tTeAttrSrlg *pGetTeAttrSrlg));
INT1
TeSetAllFsMplsAttributeSrlgTable ARG_LIST ((UINT4 u4AttrListIndex,
                                            UINT4 u4SrlgNo,
                                            tTeAttrSrlg *pSetTeAttrSrlg,
                                            UINT4 u4ObjectId));
INT1
TeTestAllFsMplsAttributeSrlgTable ARG_LIST ((UINT4 *pu4ErrorCode,
                                             UINT4 u4AttrListIndex,
                                             UINT4 u4SrlgNo,
                                             tTeAttrSrlg *pTestTeAttrSrlg,
                                             UINT4 u4ObjectId));

INT4
TeCreateCHopListInfo ARG_LIST ((tTeCHopListInfo ** ppTeCHopListInfo));

INT4
TeDeleteCHopListInfo ARG_LIST ((tTeCHopListInfo * ppTeCHopListInfo));

INT4
TeCreateCHop ARG_LIST ((UINT4 u4RemoteId, UINT4 u4NextHopIp, UINT4 u4RouterId,
                        UINT1 u1HopType, tTeCHopInfo **ppTeCHopInfo));

tTeCHopInfo *
TeGetCHop ARG_LIST ((UINT4 u4HopListIndex, UINT4 u4HopIndex));

INT4
TeCreateCHopsForCspfPaths ARG_LIST ((tTeTnlInfo * pTeTnlInfo,
                                     tOsTeAppPath * pCspfPath,
                                     tTeCHopListInfo ** ppTeCHopListInfo));

VOID
TeSetAttributeListMask ARG_LIST ((UINT4 u4AttrListIndex,
                                  UINT4 u4BitMask));

VOID
TeResetAttributeListMask ARG_LIST ((UINT4 u4AttrListIndex,
                                    UINT4 u4BitMask));

UINT4
TeGetAttributeListMask ARG_LIST ((UINT4 u4AttrListIndex));

UINT4
TeGetFreeAttrListIndex ARG_LIST ((VOID));  

UINT4
TeGetNextAttrListIndex ARG_LIST ((UINT4 u4CurAttrListIndex));

INT4
TeCreateAttrListInfo ARG_LIST ((tTeAttrListInfo ** ppTeAttrListInfo, UINT4 u4AttrListIndex));

UINT1
TeDeleteAttrListInfo ARG_LIST ((tTeAttrListInfo *pTeAttrListInfo));

tTeAttrListInfo *
TeGetAttrListInfo ARG_LIST ((UINT4 u4AttrListIndex));

INT4
TeGetAttrListIndexFromName ARG_LIST ((tTeAttrListInfo *pInTeAttrListInfo));

VOID
TeAddSrlgToSortTnlSrlgList ARG_LIST ((tTeTnlSrlg *pInTnlSrlg, tTeTnlInfo * pTeTnlInfo));

VOID
TeRemoveSrlgFromSortTnlSrlgList ARG_LIST ((tTeTnlSrlg *pTnlSrlg, tTeTnlInfo * pTeTnlInfo));

tTeTnlSrlg *
TeGetSrlgFromTnlSrlgList ARG_LIST ((UINT4 u4SrlgNo, tTeTnlInfo * pTeTnlInfo));

VOID
TeAddSrlgToSortAttrSrlgList ARG_LIST ((tTeAttrSrlg *pInAttrSrlg,
                                       tTeAttrListInfo *pTeAttrListInfo));

VOID
TeRemoveSrlgFromSortAttrSrlgList ARG_LIST ((tTeAttrSrlg *pTeAttrSrlg,
                                            tTeAttrListInfo *pTeAttrListInfo));

tTeAttrSrlg *               
TeGetSrlgFromAttrSrlgList ARG_LIST ((UINT4 u4SrlgNo,
                                     tTeAttrListInfo * pTeAttrListInfo));

INT4
TeCheckOrUpdateHopInfo ARG_LIST ((UINT4 u4PathListIndex, UINT4 u4PathOptionIndex,
                                  UINT4 u4ObjValue1, UINT4 u4ObjValue2));
VOID
TePortTlmGetTeLinkUnResvBw (UINT4 u4TeIfIndex, UINT1 u1TnlSetPrio,
                            FLOAT *pUnResvBw, FLOAT *pAvailBw, UINT1 *pTeLinkOperStatus);
INT4
TePortTlmUpdateTeLinkUnResvBw (UINT4 u4TeIfIndex,
                               UINT1 u1TnlHoldPrio, FLOAT reqResBw,
                               UINT1 u1ResvRsrc,
                            UINT4 *pu4CompIfIndex);
BOOL1
TeMainIsCspfComputationReqd (tTeTnlInfo * pTeTnlInfo);

INT1
TePortIsOspfTeEnabled (VOID);

INT1 TePortTlmIsOurAddr (UINT4 u4Addr);

VOID
TeSendAdminStatusTrapAndSyslog (tTeTnlInfo *pTeTnlInfo);
VOID
TeDeleteTnlErrorTable (tTeTnlInfo * pTeTnlInfo);

VOID
TeMainFillErrorTable (UINT4 u4LastErrorType, UINT4 u4ErrNodeAddr,
                      tTeTnlInfo *pTeTnlInfo);
VOID
TeCreateTnlErrorTable (tTunnelErrInfo *pTunnelErrInfo, tTeTnlInfo * pTeTnlInfo);


VOID
TeSetDetourStatus (tTeTnlInfo *pTeTnlInfo, UINT1 u1DetourStatus);

UINT1 TeCreateTunnel (tTeTnlInfo * pInstance0Tnl, tTeTnlInfo ** pNewTeTnl);

UINT1
TeCreateNewTnl (tTeTnlInfo ** ppTeTnlInfo, tTeTnlInfo * pNewTeTnlInfo);

UINT1
TeCreateFrrConstInfo (tTeTnlInfo *pTmpTeTnlInfo,
                      tTeFrrConstInfo *pTmpFrrConstInfo);

UINT1
TeCreateDiffServTnl (tMplsDiffServTnlInfo ** ppMplsDiffServTnlInfo,
                     tMplsDiffServTnlInfo * pNewDiffServTnlInfo);

tTeTnlInfo *
TeGetNextTunnelInfo (tTeTnlInfo *pTeTnlInfo);

UINT1
TeUpdateMapTnlIndex (UINT4 u4MplsTnlIf, UINT4 *pu4TnlIndex, 
                     UINT4 *pu4TnlInstance, tTeRouterId *pIngressLsrId, 
                     tTeRouterId *pEgressLsrId, UINT1 *pu1TnlRole);

UINT1
TeAddToTeIfIdxbasedTable (tTeTnlInfo *pTeTnlInfo);

UINT1
TeDelFromTeIfIdxbasedTable (tTeTnlInfo *pTeTnlInfo);
UINT1
TeCopyOutLabel (tTeTnlInfo *pSrcTeTnlInfo, tTeTnlInfo *pDestTeTnlInfo);

UINT1
TeDeleteStackTnlIf (tTeTnlInfo *pTeTnlInfo);

VOID
TeFillFsMplsLspMapTnlTable (tHlspTnlCliArgs *pHlspTnlCliArgs);

INT4 TnlMsgIdCmp ARG_LIST
                 ((tTeTnlInfo *pFirstTeTnlInfo, tTeTnlInfo *pNextTeTnlInfo));

VOID
TeAddToTeMsgIdBasedTable (tTeTnlInfo *pTeTnlInfo, UINT4 u4MsgId);
VOID
TeDelFromTeMsgIdBasedTable (tTeTnlInfo *pTeTnlInfo);
UINT4
TeGetAttrFromTnlOrAttrListByMask (tTeTnlInfo * pTeTnlInfo, UINT4 u4ListBitMask);
VOID
TeDeleteSrlgFromTnl (tTeTnlInfo *pTeTnlInfo, tTeTnlInfo *pInstance0Tnl);
VOID
TeGetTrafficParams (tTeTnlInfo *pTeTnlInfo, UINT4 *pu4PeakDataRate,
                    UINT4 *pu4PeakBurstSize, UINT4 *pu4CommittedDataRate,
                    UINT4 *pu4CommittedBurstSize, UINT4 *pu4ExcessBurstSize);
VOID
TeSetGrSynchronizedStatus (tTeTnlInfo *pTeTnlInfo, UINT1 u1TnlSynStatus);
VOID
TeGrDeleteNonSynchronizedTnls (VOID);
VOID
TeGetTunnelInfoFromOutMsgId (tTeTnlInfo **pTeTnlInfo, UINT4 u4MsgId);

VOID
TeGetLabelInfoUsingTnl (tTeTnlInfo * pTeTnlInfo, UINT4 *pu4OutLbl,
                        UINT4 *pu4InLbl, UINT1 u1Direction);

VOID 
TeGetNoOfStackedTnls (UINT4 u4TnlIndex, UINT4 u4TnlInstance,
                      UINT4 u4TnlIngressId, UINT4 u4TnlEgressId,
                      UINT4 *pu4NoOfStackedTnl);

INT1
GetFirstIndexFsMplsTunnelSrlgNumber ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 *));

INT1
GetNextIndexFsMplsTunnelSrlgNumber ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * ,  UINT4 *));


VOID
TeDeleteAllTnls (VOID);

#endif
