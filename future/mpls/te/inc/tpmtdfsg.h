/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: tpmtdfsg.h,v 1.3 2011/10/25 09:29:48 siva Exp $
*
* Description: This file contains data structures defined for Te module.
*********************************************************************/
/* Structure used by CLI to indicate which 
 all objects to be set in MplsTeP2mpTunnelTable */

#include "teextrn.h"

typedef struct
{
 BOOL1  bMplsTeP2mpTunnelP2mpIntegrity;
 BOOL1  bMplsTeP2mpTunnelBranchRole;
 BOOL1  bMplsTeP2mpTunnelRowStatus;
 BOOL1  bMplsTeP2mpTunnelStorageType;
 BOOL1  bMplsTunnelIndex;
 BOOL1  bMplsTunnelInstance;
 BOOL1  bMplsTunnelIngressLSRId;
 BOOL1  bMplsTunnelEgressLSRId;
} tTeIsSetMplsTeP2mpTunnelTable;


/* Structure used by Te protocol for MplsTeP2mpTunnelTable */

typedef struct
{
 tRBNodeEmbd  MplsTeP2mpTunnelTableNode;
 UINT4 u4MplsTunnelIndex;
 UINT4 u4MplsTunnelInstance;
 UINT4 u4MplsTunnelIngressLSRId;
 UINT4 u4MplsTunnelEgressLSRId;
 INT4 i4MplsTeP2mpTunnelP2mpIntegrity;
 INT4 i4MplsTeP2mpTunnelBranchRole;
 INT4 i4MplsTeP2mpTunnelRowStatus;
 INT4 i4MplsTeP2mpTunnelStorageType;
 INT4 i4MplsTeP2mpTunnelP2mpXcIndexLen;
 UINT1 au1MplsTeP2mpTunnelP2mpXcIndex[256];
} tTeMibMplsTeP2mpTunnelTable;
/* Structure used by CLI to indicate which 
 all objects to be set in MplsTeP2mpTunnelDestTable */

typedef struct
{
 BOOL1  bMplsTeP2mpTunnelDestSrcSubGroupOriginType;
 BOOL1  bMplsTeP2mpTunnelDestSrcSubGroupOrigin;
 BOOL1  bMplsTeP2mpTunnelDestSrcSubGroupID;
 BOOL1  bMplsTeP2mpTunnelDestSubGroupOriginType;
 BOOL1  bMplsTeP2mpTunnelDestSubGroupOrigin;
 BOOL1  bMplsTeP2mpTunnelDestSubGroupID;
 BOOL1  bMplsTeP2mpTunnelDestDestinationType;
 BOOL1  bMplsTeP2mpTunnelDestDestination;
 BOOL1  bMplsTeP2mpTunnelDestBranchOutSegment;
 BOOL1  bMplsTeP2mpTunnelDestHopTableIndex;
 BOOL1  bMplsTeP2mpTunnelDestPathInUse;
 BOOL1  bMplsTeP2mpTunnelDestAdminStatus;
 BOOL1  bMplsTeP2mpTunnelDestRowStatus;
 BOOL1  bMplsTeP2mpTunnelDestStorageType;
 BOOL1  bMplsTunnelIndex;
 BOOL1  bMplsTunnelInstance;
 BOOL1  bMplsTunnelIngressLSRId;
 BOOL1  bMplsTunnelEgressLSRId;
} tTeIsSetMplsTeP2mpTunnelDestTable;


/* Structure used by Te protocol for MplsTeP2mpTunnelDestTable */

typedef struct
{
 tRBNodeEmbd  MplsTeP2mpTunnelDestTableNode;
 UINT4 u4MplsTeP2mpTunnelDestSrcSubGroupID;
 UINT4 u4MplsTeP2mpTunnelDestSubGroupID;
 UINT4 u4MplsTeP2mpTunnelDestHopTableIndex;
 UINT4 u4MplsTeP2mpTunnelDestPathInUse;
 UINT4 u4MplsTeP2mpTunnelDestCHopTableIndex;
 UINT4 u4MplsTeP2mpTunnelDestARHopTableIndex;
 UINT4 u4MplsTeP2mpTunnelDestTotalUpTime;
 UINT4 u4MplsTeP2mpTunnelDestInstanceUpTime;
 UINT4 u4MplsTeP2mpTunnelDestPathChanges;
 UINT4 u4MplsTeP2mpTunnelDestLastPathChange;
 UINT4 u4MplsTeP2mpTunnelDestCreationTime;
 UINT4 u4MplsTeP2mpTunnelDestStateTransitions;
 UINT4 u4MplsTeP2mpTunnelDestDiscontinuityTime;
 UINT4 u4MplsTunnelIndex;
 UINT4 u4MplsTunnelInstance;
 UINT4 u4MplsTunnelIngressLSRId;
 UINT4 u4MplsTunnelEgressLSRId;
 INT4 i4MplsTeP2mpTunnelDestSrcSubGroupOriginType;
 INT4 i4MplsTeP2mpTunnelDestSubGroupOriginType;
 INT4 i4MplsTeP2mpTunnelDestDestinationType;
 INT4 i4MplsTeP2mpTunnelDestAdminStatus;
 INT4 i4MplsTeP2mpTunnelDestOperStatus;
 INT4 i4MplsTeP2mpTunnelDestRowStatus;
 INT4 i4MplsTeP2mpTunnelDestStorageType;
 INT4 i4MplsTeP2mpTunnelDestSrcSubGroupOriginLen;
 INT4 i4MplsTeP2mpTunnelDestSubGroupOriginLen;
 INT4 i4MplsTeP2mpTunnelDestDestinationLen;
 INT4 i4MplsTeP2mpTunnelDestBranchOutSegmentLen;
 UINT1 au1MplsTeP2mpTunnelDestSrcSubGroupOrigin[256];
 UINT1 au1MplsTeP2mpTunnelDestSubGroupOrigin[16];
 UINT1 au1MplsTeP2mpTunnelDestDestination[16];
 UINT1 au1MplsTeP2mpTunnelDestBranchOutSegment[256];
} tTeMibMplsTeP2mpTunnelDestTable;


/* Structure used by Te protocol for MplsTeP2mpTunnelBranchPerfTable */

typedef struct
{
 tRBNodeEmbd  MplsTeP2mpTunnelBranchPerfTableNode;
 UINT4 u4MplsTeP2mpTunnelBranchPerfPackets;
 tSNMP_COUNTER64_TYPE u8MplsTeP2mpTunnelBranchPerfHCPackets;
 UINT4 u4MplsTeP2mpTunnelBranchPerfErrors;
 UINT4 u4MplsTeP2mpTunnelBranchPerfBytes;
 tSNMP_COUNTER64_TYPE u8MplsTeP2mpTunnelBranchPerfHCBytes;
 UINT4 u4MplsTeP2mpTunnelBranchDiscontinuityTime;
 UINT4 u4MplsTunnelIndex;
 UINT4 u4MplsTunnelInstance;
 UINT4 u4MplsTunnelIngressLSRId;
 UINT4 u4MplsTunnelEgressLSRId;
 INT4 i4MplsTeP2mpTunnelBranchPerfBranchLen;
 UINT1 au1MplsTeP2mpTunnelBranchPerfBranch[256];
} tTeMibMplsTeP2mpTunnelBranchPerfTable;

typedef struct
{
 tTeMibMplsTeP2mpTunnelTable       MibObject;
} tTeMplsTeP2mpTunnelTable;


typedef struct
{
 tTeMibMplsTeP2mpTunnelDestTable       MibObject;
} tTeMplsTeP2mpTunnelDestTable;


typedef struct
{
 tTeMibMplsTeP2mpTunnelBranchPerfTable       MibObject;
} tTeMplsTeP2mpTunnelBranchPerfTable;

/* Prototypes for functions defined in TE P2MP */
INT4 TeGetMplsTeP2mpTunnelConfigured PROTO ((UINT4 *));
INT4 TeGetMplsTeP2mpTunnelActive PROTO ((UINT4 *));
INT4 TeGetMplsTeP2mpTunnelTotalMaxHops PROTO ((UINT4 *));
INT4 TeGetFirstMplsTeP2mpTunnelTable PROTO ((tTeMplsTeP2mpTunnelTable *));
INT4 TeGetNextMplsTeP2mpTunnelTable PROTO ((tTeMplsTeP2mpTunnelTable *, tTeMplsTeP2mpTunnelTable *));
INT4 TeGetAllMplsTeP2mpTunnelTable PROTO ((tTeMplsTeP2mpTunnelTable *));
INT4 TeTestAllMplsTeP2mpTunnelTable PROTO ((UINT4 *, tTeMplsTeP2mpTunnelTable *, tTeIsSetMplsTeP2mpTunnelTable *, INT4 , INT4));
INT4 TeSetAllMplsTeP2mpTunnelTable PROTO ((tTeMplsTeP2mpTunnelTable *, tTeIsSetMplsTeP2mpTunnelTable *, INT4 , INT4 ));
INT4 TeInitializeMplsTeP2mpTunnelTable PROTO ((tTeP2mpTnlInfo *));
INT4 TeInitializeMibMplsTeP2mpTunnelTable PROTO ((tTeP2mpTnlInfo *));
INT4 TeSetAllMplsTeP2mpTunnelTableTrigger PROTO ((tTeMplsTeP2mpTunnelTable *, tTeIsSetMplsTeP2mpTunnelTable *, INT4));
INT4 MplsTeP2mpTunnelTableFilterInputs PROTO ((tTeMplsTeP2mpTunnelTable *, tTeMplsTeP2mpTunnelTable *, tTeIsSetMplsTeP2mpTunnelTable *));
INT4 TeUtilUpdateMplsTeP2mpTunnelTable PROTO ((tTeMplsTeP2mpTunnelTable *, tTeMplsTeP2mpTunnelTable *));
INT4 TeGetMplsTeP2mpTunnelSubGroupIDNext PROTO ((UINT4 *));
INT4 TeGetFirstMplsTeP2mpTunnelDestTable PROTO ((tTeMplsTeP2mpTunnelDestTable *));
INT4 TeGetNextMplsTeP2mpTunnelDestTable PROTO ((tTeMplsTeP2mpTunnelDestTable *, tTeMplsTeP2mpTunnelDestTable *));
INT4 TeGetAllMplsTeP2mpTunnelDestTable PROTO ((tTeMplsTeP2mpTunnelDestTable *));
INT4 TeTestAllMplsTeP2mpTunnelDestTable PROTO ((UINT4 *, tTeMplsTeP2mpTunnelDestTable *, tTeIsSetMplsTeP2mpTunnelDestTable *, INT4 , INT4));
INT4 TeSetAllMplsTeP2mpTunnelDestTable PROTO ((tTeMplsTeP2mpTunnelDestTable *, tTeIsSetMplsTeP2mpTunnelDestTable *, INT4 , INT4 ));
INT4 TeInitializeMplsTeP2mpTunnelDestTable PROTO ((tP2mpDestEntry *));
INT4 TeInitializeMibMplsTeP2mpTunnelDestTable PROTO ((tP2mpDestEntry *));
INT4 TeSetAllMplsTeP2mpTunnelDestTableTrigger PROTO ((tTeMplsTeP2mpTunnelDestTable *, tTeIsSetMplsTeP2mpTunnelDestTable *, INT4));
INT4 MplsTeP2mpTunnelDestTableFilterInputs PROTO ((tTeMplsTeP2mpTunnelDestTable *, tTeMplsTeP2mpTunnelDestTable *, tTeIsSetMplsTeP2mpTunnelDestTable *));
INT4 TeUtilUpdateMplsTeP2mpTunnelDestTable PROTO ((tTeMplsTeP2mpTunnelDestTable *, tTeMplsTeP2mpTunnelDestTable *));
INT4 TeGetFirstMplsTeP2mpTunnelBranchPerfTable PROTO ((tTeMplsTeP2mpTunnelBranchPerfTable *));
INT4 TeGetNextMplsTeP2mpTunnelBranchPerfTable PROTO ((tTeMplsTeP2mpTunnelBranchPerfTable *, tTeMplsTeP2mpTunnelBranchPerfTable *));
INT4 TeGetAllMplsTeP2mpTunnelBranchPerfTable PROTO ((tTeMplsTeP2mpTunnelBranchPerfTable *));
INT4 TeGetMplsTeP2mpTunnelNotificationEnable PROTO ((INT4 *));
INT4 TeTestMplsTeP2mpTunnelNotificationEnable PROTO ((UINT4 *,INT4 ));
INT4 TeSetMplsTeP2mpTunnelNotificationEnable PROTO ((INT4 ));
tP2mpBranchEntry  * TeAddP2mpBranchEntry PROTO ((tTeTnlInfo *, UINT4));
UINT1 TeRemoveP2mpBranchEntry PROTO ((tTeTnlInfo *, UINT4));
tP2mpDestEntry  * TeAddP2mpDestEntry PROTO ((tTeTnlInfo *, UINT4));
UINT1 TeRemoveP2mpDestEntry PROTO ((tTeTnlInfo *, UINT4));
UINT1 TeCheckP2mpTableEntry PROTO ((UINT4, UINT4, UINT4, UINT4, tTeTnlInfo **));
PUBLIC INT4 TeLock (VOID);
PUBLIC INT4 TeUnLock (VOID);

/* Extern declarations */




