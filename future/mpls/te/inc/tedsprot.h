/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tedsprot.h,v 1.4 2012/05/08 15:29:36 siva Exp $
 *
 * Description: Diffserv protocol definitions
 ********************************************************************/

#ifndef _TE_DSPROT_H
#define _TE_DSPROT_H

VOID
teDiffServDeleteInfoHeads ARG_LIST ((VOID));

VOID teDiffServInitDefault ARG_LIST ((tTeTnlInfo *pTeTnlInfo));

UINT1 teDiffServCreateInfoHeads ARG_LIST ((VOID));

UINT1 TeDeleteMplsDiffServTnlInfo ARG_LIST ((tTeTnlInfo *pTeTnlInfo,
                                             tTeTnlInfo *pInstance0Tnl));

UINT1
TeDiffServElspListRowStatusActive ARG_LIST ((INT4 i4ElspInfoListIndex));

UINT1
TeDiffServChkIsPerOAResrcConf ARG_LIST ((INT4 i4ElspInfoListIndex));
/*
UINT1
teDiffServChkRsrcForPreConf ARG_LIST ((tMplsDiffServParams *pDiffServParams));
*/
UINT1
TeDiffServGetElspInfoNode ARG_LIST ((INT4 i4ElspInfoListIndex,
                             INT4 i4ElspInfoIndex,
                             tMplsDiffServElspInfo ** ppElspInfoNode));

UINT1
teDiffServHandleSigPhbs ARG_LIST((tTeTnlInfo * pTeTnlInfo));

UINT1
TeDiffServGetNextAvailableIndex ARG_LIST ((UINT4 *pu4Index));

UINT1
teDiffServDeleteElspInfoMem ARG_LIST ((tTeTnlInfo * pTeTnlInfo));

extern UINT4 tedsInitRMIfInfo ARG_LIST ((VOID));
#endif

