/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdgmtdb.h,v 1.1 2011/11/30 09:58:14 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDGMTDB_H
#define _STDGMTDB_H

UINT1 GmplsTunnelTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 GmplsTunnelHopTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 GmplsTunnelARHopTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 GmplsTunnelCHopTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 GmplsTunnelReversePerfTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 GmplsTunnelErrorTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};

UINT4 stdgmt [] ={1,3,6,1,2,1,10,166,13};
tSNMP_OID_TYPE stdgmtOID = {9, stdgmt};


UINT4 GmplsTunnelsConfigured [ ] ={1,3,6,1,2,1,10,166,13,1,1};
UINT4 GmplsTunnelsActive [ ] ={1,3,6,1,2,1,10,166,13,1,2};
UINT4 GmplsTunnelUnnumIf [ ] ={1,3,6,1,2,1,10,166,13,2,1,1,1};
UINT4 GmplsTunnelAttributes [ ] ={1,3,6,1,2,1,10,166,13,2,1,1,2};
UINT4 GmplsTunnelLSPEncoding [ ] ={1,3,6,1,2,1,10,166,13,2,1,1,3};
UINT4 GmplsTunnelSwitchingType [ ] ={1,3,6,1,2,1,10,166,13,2,1,1,4};
UINT4 GmplsTunnelLinkProtection [ ] ={1,3,6,1,2,1,10,166,13,2,1,1,5};
UINT4 GmplsTunnelGPid [ ] ={1,3,6,1,2,1,10,166,13,2,1,1,6};
UINT4 GmplsTunnelSecondary [ ] ={1,3,6,1,2,1,10,166,13,2,1,1,7};
UINT4 GmplsTunnelDirection [ ] ={1,3,6,1,2,1,10,166,13,2,1,1,8};
UINT4 GmplsTunnelPathComp [ ] ={1,3,6,1,2,1,10,166,13,2,1,1,9};
UINT4 GmplsTunnelUpstreamNotifyRecipientType [ ] ={1,3,6,1,2,1,10,166,13,2,1,1,10};
UINT4 GmplsTunnelUpstreamNotifyRecipient [ ] ={1,3,6,1,2,1,10,166,13,2,1,1,11};
UINT4 GmplsTunnelSendResvNotifyRecipientType [ ] ={1,3,6,1,2,1,10,166,13,2,1,1,12};
UINT4 GmplsTunnelSendResvNotifyRecipient [ ] ={1,3,6,1,2,1,10,166,13,2,1,1,13};
UINT4 GmplsTunnelDownstreamNotifyRecipientType [ ] ={1,3,6,1,2,1,10,166,13,2,1,1,14};
UINT4 GmplsTunnelDownstreamNotifyRecipient [ ] ={1,3,6,1,2,1,10,166,13,2,1,1,15};
UINT4 GmplsTunnelSendPathNotifyRecipientType [ ] ={1,3,6,1,2,1,10,166,13,2,1,1,16};
UINT4 GmplsTunnelSendPathNotifyRecipient [ ] ={1,3,6,1,2,1,10,166,13,2,1,1,17};
UINT4 GmplsTunnelAdminStatusFlags [ ] ={1,3,6,1,2,1,10,166,13,2,1,1,18};
UINT4 GmplsTunnelExtraParamsPtr [ ] ={1,3,6,1,2,1,10,166,13,2,1,1,19};
UINT4 GmplsTunnelHopLabelStatuses [ ] ={1,3,6,1,2,1,10,166,13,2,2,1,1};
UINT4 GmplsTunnelHopExplicitForwardLabel [ ] ={1,3,6,1,2,1,10,166,13,2,2,1,2};
UINT4 GmplsTunnelHopExplicitForwardLabelPtr [ ] ={1,3,6,1,2,1,10,166,13,2,2,1,3};
UINT4 GmplsTunnelHopExplicitReverseLabel [ ] ={1,3,6,1,2,1,10,166,13,2,2,1,4};
UINT4 GmplsTunnelHopExplicitReverseLabelPtr [ ] ={1,3,6,1,2,1,10,166,13,2,2,1,5};
UINT4 GmplsTunnelARHopLabelStatuses [ ] ={1,3,6,1,2,1,10,166,13,2,3,1,1};
UINT4 GmplsTunnelARHopExplicitForwardLabel [ ] ={1,3,6,1,2,1,10,166,13,2,3,1,2};
UINT4 GmplsTunnelARHopExplicitForwardLabelPtr [ ] ={1,3,6,1,2,1,10,166,13,2,3,1,3};
UINT4 GmplsTunnelARHopExplicitReverseLabel [ ] ={1,3,6,1,2,1,10,166,13,2,3,1,4};
UINT4 GmplsTunnelARHopExplicitReverseLabelPtr [ ] ={1,3,6,1,2,1,10,166,13,2,3,1,5};
UINT4 GmplsTunnelARHopProtection [ ] ={1,3,6,1,2,1,10,166,13,2,3,1,6};
UINT4 GmplsTunnelCHopLabelStatuses [ ] ={1,3,6,1,2,1,10,166,13,2,4,1,1};
UINT4 GmplsTunnelCHopExplicitForwardLabel [ ] ={1,3,6,1,2,1,10,166,13,2,4,1,2};
UINT4 GmplsTunnelCHopExplicitForwardLabelPtr [ ] ={1,3,6,1,2,1,10,166,13,2,4,1,3};
UINT4 GmplsTunnelCHopExplicitReverseLabel [ ] ={1,3,6,1,2,1,10,166,13,2,4,1,4};
UINT4 GmplsTunnelCHopExplicitReverseLabelPtr [ ] ={1,3,6,1,2,1,10,166,13,2,4,1,5};
UINT4 GmplsTunnelReversePerfPackets [ ] ={1,3,6,1,2,1,10,166,13,2,5,1,1};
UINT4 GmplsTunnelReversePerfHCPackets [ ] ={1,3,6,1,2,1,10,166,13,2,5,1,2};
UINT4 GmplsTunnelReversePerfErrors [ ] ={1,3,6,1,2,1,10,166,13,2,5,1,3};
UINT4 GmplsTunnelReversePerfBytes [ ] ={1,3,6,1,2,1,10,166,13,2,5,1,4};
UINT4 GmplsTunnelReversePerfHCBytes [ ] ={1,3,6,1,2,1,10,166,13,2,5,1,5};
UINT4 GmplsTunnelErrorLastErrorType [ ] ={1,3,6,1,2,1,10,166,13,2,6,1,1};
UINT4 GmplsTunnelErrorLastTime [ ] ={1,3,6,1,2,1,10,166,13,2,6,1,2};
UINT4 GmplsTunnelErrorReporterType [ ] ={1,3,6,1,2,1,10,166,13,2,6,1,3};
UINT4 GmplsTunnelErrorReporter [ ] ={1,3,6,1,2,1,10,166,13,2,6,1,4};
UINT4 GmplsTunnelErrorCode [ ] ={1,3,6,1,2,1,10,166,13,2,6,1,5};
UINT4 GmplsTunnelErrorSubcode [ ] ={1,3,6,1,2,1,10,166,13,2,6,1,6};
UINT4 GmplsTunnelErrorTLVs [ ] ={1,3,6,1,2,1,10,166,13,2,6,1,7};
UINT4 GmplsTunnelErrorHelpString [ ] ={1,3,6,1,2,1,10,166,13,2,6,1,8};




tMbDbEntry stdgmtMibEntry[]= {

{{11,GmplsTunnelsConfigured}, NULL, GmplsTunnelsConfiguredGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,GmplsTunnelsActive}, NULL, GmplsTunnelsActiveGet, NULL, NULL, NULL, SNMP_DATA_TYPE_GAUGE32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,GmplsTunnelUnnumIf}, GetNextIndexGmplsTunnelTable, GmplsTunnelUnnumIfGet, GmplsTunnelUnnumIfSet, GmplsTunnelUnnumIfTest, GmplsTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, GmplsTunnelTableINDEX, 4, 0, 0, "2"},

{{13,GmplsTunnelAttributes}, GetNextIndexGmplsTunnelTable, GmplsTunnelAttributesGet, GmplsTunnelAttributesSet, GmplsTunnelAttributesTest, GmplsTunnelTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, GmplsTunnelTableINDEX, 4, 0, 0, "0"},

{{13,GmplsTunnelLSPEncoding}, GetNextIndexGmplsTunnelTable, GmplsTunnelLSPEncodingGet, GmplsTunnelLSPEncodingSet, GmplsTunnelLSPEncodingTest, GmplsTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, GmplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,GmplsTunnelSwitchingType}, GetNextIndexGmplsTunnelTable, GmplsTunnelSwitchingTypeGet, GmplsTunnelSwitchingTypeSet, GmplsTunnelSwitchingTypeTest, GmplsTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, GmplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,GmplsTunnelLinkProtection}, GetNextIndexGmplsTunnelTable, GmplsTunnelLinkProtectionGet, GmplsTunnelLinkProtectionSet, GmplsTunnelLinkProtectionTest, GmplsTunnelTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, GmplsTunnelTableINDEX, 4, 0, 0, "0"},

{{13,GmplsTunnelGPid}, GetNextIndexGmplsTunnelTable, GmplsTunnelGPidGet, GmplsTunnelGPidSet, GmplsTunnelGPidTest, GmplsTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, GmplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,GmplsTunnelSecondary}, GetNextIndexGmplsTunnelTable, GmplsTunnelSecondaryGet, GmplsTunnelSecondarySet, GmplsTunnelSecondaryTest, GmplsTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, GmplsTunnelTableINDEX, 4, 0, 0, "2"},

{{13,GmplsTunnelDirection}, GetNextIndexGmplsTunnelTable, GmplsTunnelDirectionGet, GmplsTunnelDirectionSet, GmplsTunnelDirectionTest, GmplsTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, GmplsTunnelTableINDEX, 4, 0, 0, "0"},

{{13,GmplsTunnelPathComp}, GetNextIndexGmplsTunnelTable, GmplsTunnelPathCompGet, GmplsTunnelPathCompSet, GmplsTunnelPathCompTest, GmplsTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, GmplsTunnelTableINDEX, 4, 0, 0, "1"},

{{13,GmplsTunnelUpstreamNotifyRecipientType}, GetNextIndexGmplsTunnelTable, GmplsTunnelUpstreamNotifyRecipientTypeGet, GmplsTunnelUpstreamNotifyRecipientTypeSet, GmplsTunnelUpstreamNotifyRecipientTypeTest, GmplsTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, GmplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,GmplsTunnelUpstreamNotifyRecipient}, GetNextIndexGmplsTunnelTable, GmplsTunnelUpstreamNotifyRecipientGet, GmplsTunnelUpstreamNotifyRecipientSet, GmplsTunnelUpstreamNotifyRecipientTest, GmplsTunnelTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, GmplsTunnelTableINDEX, 4, 0, 0, "0"},

{{13,GmplsTunnelSendResvNotifyRecipientType}, GetNextIndexGmplsTunnelTable, GmplsTunnelSendResvNotifyRecipientTypeGet, GmplsTunnelSendResvNotifyRecipientTypeSet, GmplsTunnelSendResvNotifyRecipientTypeTest, GmplsTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, GmplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,GmplsTunnelSendResvNotifyRecipient}, GetNextIndexGmplsTunnelTable, GmplsTunnelSendResvNotifyRecipientGet, GmplsTunnelSendResvNotifyRecipientSet, GmplsTunnelSendResvNotifyRecipientTest, GmplsTunnelTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, GmplsTunnelTableINDEX, 4, 0, 0, "0"},

{{13,GmplsTunnelDownstreamNotifyRecipientType}, GetNextIndexGmplsTunnelTable, GmplsTunnelDownstreamNotifyRecipientTypeGet, GmplsTunnelDownstreamNotifyRecipientTypeSet, GmplsTunnelDownstreamNotifyRecipientTypeTest, GmplsTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, GmplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,GmplsTunnelDownstreamNotifyRecipient}, GetNextIndexGmplsTunnelTable, GmplsTunnelDownstreamNotifyRecipientGet, GmplsTunnelDownstreamNotifyRecipientSet, GmplsTunnelDownstreamNotifyRecipientTest, GmplsTunnelTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, GmplsTunnelTableINDEX, 4, 0, 0, "0"},

{{13,GmplsTunnelSendPathNotifyRecipientType}, GetNextIndexGmplsTunnelTable, GmplsTunnelSendPathNotifyRecipientTypeGet, GmplsTunnelSendPathNotifyRecipientTypeSet, GmplsTunnelSendPathNotifyRecipientTypeTest, GmplsTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, GmplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,GmplsTunnelSendPathNotifyRecipient}, GetNextIndexGmplsTunnelTable, GmplsTunnelSendPathNotifyRecipientGet, GmplsTunnelSendPathNotifyRecipientSet, GmplsTunnelSendPathNotifyRecipientTest, GmplsTunnelTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, GmplsTunnelTableINDEX, 4, 0, 0, "0"},

{{13,GmplsTunnelAdminStatusFlags}, GetNextIndexGmplsTunnelTable, GmplsTunnelAdminStatusFlagsGet, GmplsTunnelAdminStatusFlagsSet, GmplsTunnelAdminStatusFlagsTest, GmplsTunnelTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, GmplsTunnelTableINDEX, 4, 0, 0, "0"},

{{13,GmplsTunnelExtraParamsPtr}, GetNextIndexGmplsTunnelTable, GmplsTunnelExtraParamsPtrGet, GmplsTunnelExtraParamsPtrSet, GmplsTunnelExtraParamsPtrTest, GmplsTunnelTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, GmplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,GmplsTunnelHopLabelStatuses}, GetNextIndexGmplsTunnelHopTable, GmplsTunnelHopLabelStatusesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, GmplsTunnelHopTableINDEX, 3, 0, 0, "0"},

{{13,GmplsTunnelHopExplicitForwardLabel}, GetNextIndexGmplsTunnelHopTable, GmplsTunnelHopExplicitForwardLabelGet, GmplsTunnelHopExplicitForwardLabelSet, GmplsTunnelHopExplicitForwardLabelTest, GmplsTunnelHopTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, GmplsTunnelHopTableINDEX, 3, 0, 0, NULL},

{{13,GmplsTunnelHopExplicitForwardLabelPtr}, GetNextIndexGmplsTunnelHopTable, GmplsTunnelHopExplicitForwardLabelPtrGet, GmplsTunnelHopExplicitForwardLabelPtrSet, GmplsTunnelHopExplicitForwardLabelPtrTest, GmplsTunnelHopTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, GmplsTunnelHopTableINDEX, 3, 0, 0, NULL},

{{13,GmplsTunnelHopExplicitReverseLabel}, GetNextIndexGmplsTunnelHopTable, GmplsTunnelHopExplicitReverseLabelGet, GmplsTunnelHopExplicitReverseLabelSet, GmplsTunnelHopExplicitReverseLabelTest, GmplsTunnelHopTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, GmplsTunnelHopTableINDEX, 3, 0, 0, NULL},

{{13,GmplsTunnelHopExplicitReverseLabelPtr}, GetNextIndexGmplsTunnelHopTable, GmplsTunnelHopExplicitReverseLabelPtrGet, GmplsTunnelHopExplicitReverseLabelPtrSet, GmplsTunnelHopExplicitReverseLabelPtrTest, GmplsTunnelHopTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, GmplsTunnelHopTableINDEX, 3, 0, 0, NULL},

{{13,GmplsTunnelARHopLabelStatuses}, GetNextIndexGmplsTunnelARHopTable, GmplsTunnelARHopLabelStatusesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, GmplsTunnelARHopTableINDEX, 2, 0, 0, NULL},

{{13,GmplsTunnelARHopExplicitForwardLabel}, GetNextIndexGmplsTunnelARHopTable, GmplsTunnelARHopExplicitForwardLabelGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, GmplsTunnelARHopTableINDEX, 2, 0, 0, NULL},

{{13,GmplsTunnelARHopExplicitForwardLabelPtr}, GetNextIndexGmplsTunnelARHopTable, GmplsTunnelARHopExplicitForwardLabelPtrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READONLY, GmplsTunnelARHopTableINDEX, 2, 0, 0, NULL},

{{13,GmplsTunnelARHopExplicitReverseLabel}, GetNextIndexGmplsTunnelARHopTable, GmplsTunnelARHopExplicitReverseLabelGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, GmplsTunnelARHopTableINDEX, 2, 0, 0, NULL},

{{13,GmplsTunnelARHopExplicitReverseLabelPtr}, GetNextIndexGmplsTunnelARHopTable, GmplsTunnelARHopExplicitReverseLabelPtrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READONLY, GmplsTunnelARHopTableINDEX, 2, 0, 0, NULL},

{{13,GmplsTunnelARHopProtection}, GetNextIndexGmplsTunnelARHopTable, GmplsTunnelARHopProtectionGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, GmplsTunnelARHopTableINDEX, 2, 0, 0, NULL},

{{13,GmplsTunnelCHopLabelStatuses}, GetNextIndexGmplsTunnelCHopTable, GmplsTunnelCHopLabelStatusesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, GmplsTunnelCHopTableINDEX, 2, 0, 0, NULL},

{{13,GmplsTunnelCHopExplicitForwardLabel}, GetNextIndexGmplsTunnelCHopTable, GmplsTunnelCHopExplicitForwardLabelGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, GmplsTunnelCHopTableINDEX, 2, 0, 0, NULL},

{{13,GmplsTunnelCHopExplicitForwardLabelPtr}, GetNextIndexGmplsTunnelCHopTable, GmplsTunnelCHopExplicitForwardLabelPtrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READONLY, GmplsTunnelCHopTableINDEX, 2, 0, 0, NULL},

{{13,GmplsTunnelCHopExplicitReverseLabel}, GetNextIndexGmplsTunnelCHopTable, GmplsTunnelCHopExplicitReverseLabelGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, GmplsTunnelCHopTableINDEX, 2, 0, 0, NULL},

{{13,GmplsTunnelCHopExplicitReverseLabelPtr}, GetNextIndexGmplsTunnelCHopTable, GmplsTunnelCHopExplicitReverseLabelPtrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READONLY, GmplsTunnelCHopTableINDEX, 2, 0, 0, NULL},

{{13,GmplsTunnelReversePerfPackets}, GetNextIndexGmplsTunnelReversePerfTable, GmplsTunnelReversePerfPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, GmplsTunnelReversePerfTableINDEX, 4, 0, 0, NULL},

{{13,GmplsTunnelReversePerfHCPackets}, GetNextIndexGmplsTunnelReversePerfTable, GmplsTunnelReversePerfHCPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, GmplsTunnelReversePerfTableINDEX, 4, 0, 0, NULL},

{{13,GmplsTunnelReversePerfErrors}, GetNextIndexGmplsTunnelReversePerfTable, GmplsTunnelReversePerfErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, GmplsTunnelReversePerfTableINDEX, 4, 0, 0, NULL},

{{13,GmplsTunnelReversePerfBytes}, GetNextIndexGmplsTunnelReversePerfTable, GmplsTunnelReversePerfBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, GmplsTunnelReversePerfTableINDEX, 4, 0, 0, NULL},

{{13,GmplsTunnelReversePerfHCBytes}, GetNextIndexGmplsTunnelReversePerfTable, GmplsTunnelReversePerfHCBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, GmplsTunnelReversePerfTableINDEX, 4, 0, 0, NULL},

{{13,GmplsTunnelErrorLastErrorType}, GetNextIndexGmplsTunnelErrorTable, GmplsTunnelErrorLastErrorTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, GmplsTunnelErrorTableINDEX, 4, 0, 0, NULL},

{{13,GmplsTunnelErrorLastTime}, GetNextIndexGmplsTunnelErrorTable, GmplsTunnelErrorLastTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, GmplsTunnelErrorTableINDEX, 4, 0, 0, NULL},

{{13,GmplsTunnelErrorReporterType}, GetNextIndexGmplsTunnelErrorTable, GmplsTunnelErrorReporterTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, GmplsTunnelErrorTableINDEX, 4, 0, 0, NULL},

{{13,GmplsTunnelErrorReporter}, GetNextIndexGmplsTunnelErrorTable, GmplsTunnelErrorReporterGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, GmplsTunnelErrorTableINDEX, 4, 0, 0, NULL},

{{13,GmplsTunnelErrorCode}, GetNextIndexGmplsTunnelErrorTable, GmplsTunnelErrorCodeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, GmplsTunnelErrorTableINDEX, 4, 0, 0, NULL},

{{13,GmplsTunnelErrorSubcode}, GetNextIndexGmplsTunnelErrorTable, GmplsTunnelErrorSubcodeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, GmplsTunnelErrorTableINDEX, 4, 0, 0, NULL},

{{13,GmplsTunnelErrorTLVs}, GetNextIndexGmplsTunnelErrorTable, GmplsTunnelErrorTLVsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, GmplsTunnelErrorTableINDEX, 4, 0, 0, NULL},

{{13,GmplsTunnelErrorHelpString}, GetNextIndexGmplsTunnelErrorTable, GmplsTunnelErrorHelpStringGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, GmplsTunnelErrorTableINDEX, 4, 0, 0, NULL},
};
tMibData stdgmtEntry = { 50, stdgmtMibEntry };

#endif /* _STDGMTDB_H */

