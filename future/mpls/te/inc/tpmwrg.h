/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
*$ Id: $
*
* Description: This file contains prototype for SNMP wrapper routines
*********************************************************************/

#ifndef _TEWR_H
#define _TEWR_H

/* MPLS_P2MP_LSP_CHANGES - S */
VOID RegisterP2MP (VOID);
VOID UnRegisterP2MP (VOID);
/* MPLS_P2MP_LSP_CHANGES - E */
INT4 MplsTeP2mpTunnelConfiguredGet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelActiveGet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelTotalMaxHopsGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexMplsTeP2mpTunnelTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsTeP2mpTunnelP2mpIntegrityGet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelBranchRoleGet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelP2mpXcIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelP2mpIntegrityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelBranchRoleTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelP2mpIntegritySet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelBranchRoleSet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 MplsTeP2mpTunnelSubGroupIDNextGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexMplsTeP2mpTunnelDestTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsTeP2mpTunnelDestBranchOutSegmentGet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelDestHopTableIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelDestPathInUseGet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelDestCHopTableIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelDestARHopTableIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelDestTotalUpTimeGet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelDestInstanceUpTimeGet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelDestPathChangesGet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelDestLastPathChangeGet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelDestCreationTimeGet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelDestStateTransitionsGet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelDestDiscontinuityTimeGet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelDestAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelDestOperStatusGet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelDestRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelDestStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelDestBranchOutSegmentTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelDestHopTableIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelDestPathInUseTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelDestAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelDestRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelDestStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelDestBranchOutSegmentSet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelDestHopTableIndexSet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelDestPathInUseSet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelDestAdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelDestRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelDestStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelDestTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexMplsTeP2mpTunnelBranchPerfTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsTeP2mpTunnelBranchPerfPacketsGet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelBranchPerfHCPacketsGet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelBranchPerfErrorsGet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelBranchPerfBytesGet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelBranchPerfHCBytesGet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelBranchDiscontinuityTimeGet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelNotificationEnableGet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelNotificationEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelNotificationEnableSet(tSnmpIndex *, tRetVal *);
INT4 MplsTeP2mpTunnelNotificationEnableDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

#endif /* _TEWR_H */
