/*---------------------------------------------------------------------------*/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * $Id: teclip.h,v 1.44 2016/07/22 09:45:48 siva Exp $
 *-----------------------------------------------------------------------------
 *    FILE  NAME             : teclip.h
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : TE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux 1.2.1 (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains the prototypes of functions 
 *                             present in tecli.c and tecli1.c
 *----------------------------------------------------------------------------*/


#ifndef __TECLIP_H__
#define __TECLIP_H__

typedef struct _FrrCliArgs
{
        UINT4 u4Flag;
        UINT4 u4Bandwidth;
        UINT4 u4BckSetupPrio;
        UINT4 u4BckHoldingPrior;
        UINT4 u4HopLimit;
        UINT4 u4IncAny;
        UINT4 u4ExcAny;
        UINT4 u4IncAll;
}
tFrrCliArgs;

typedef struct _teCliStBindArgs
{
    UINT4 u4InLabel;
    UINT4 u4InIfIndex;
    UINT4 u4OutLabel;
    UINT4 u4NextHopAddr;
    UINT4 u4OutIfIndex;
    UINT4 u4TnlId;
    UINT4 u4Direction;
    UINT4 u4Operation;
    UINT1 u1TnlPathType;
    UINT1 au1Rsvd[3];
}tteCliStBindArgs;


/*TE_CLI_ERR_TOKEN_BKT_RATE_SET_FAIL is deleted PA-fix*/

/* Function Definitions */
INT4
TeCliAttrParamPriorities (tCliHandle CliHandle, UINT4 u4AttrListIndex,
                          tTeCliArgs *pTeCliArgs);

INT4
TeCliAttrParamNoPriorities (tCliHandle CliHandle, UINT4 u4AttrListIndex,
                            tTeCliArgs *pTeCliArgs);

INT4
TeCliAttrParamAffinities (tCliHandle CliHandle, UINT4 u4AttrListIndex,
                          tTeCliArgs *pTeCliArgs);

INT4
TeCliAttrParamNoAffinities (tCliHandle CliHandle, UINT4 u4AttrListIndex,
                            tTeCliArgs *pTeCliArgs);

INT4
TeCliAttrParamBandwidth (tCliHandle CliHandle, UINT4 u4AttrListIndex,
                         tTeCliArgs *pTeCliArgs);

INT4
TeCliAttrParamNoBandwidth (tCliHandle CliHandle, UINT4 u4AttrListIndex,
                           tTeCliArgs *pTeCliArgs);
INT4
TeCliAttrParamRecordRoute (tCliHandle CliHandle, UINT4 u4AttrListIndex,
                           tTeCliArgs *pTeCliArgs);

INT4
TeCliAttrParamNoRecordRoute (tCliHandle CliHandle, UINT4 u4AttrListIndex,
                             tTeCliArgs *pTeCliArgs);

INT4
TeCliAddHopTableEntries (tCliHandle CliHandle, tTeCliArgs *pTeCliArgs);

INT4
TeCliDelHopTableEntries (tCliHandle CliHandle, tTeCliArgs *pTeCliArgs);

INT4
TeCliSetTnlAttrSrlgRowStatus (UINT4 u4AttrListIndex, UINT4 u4SrlgNo,
                              INT4 i4RowStatus);

INT4
TeCliSetMplsTunnelHopRowStatus (UINT4 u4HopListIndex,
                                UINT4 u4HopPathOptionIndex,
                                UINT4 u4HopNum,
                                INT4 i4SetValMplsTunnelHopRowStatus);

INT4
TeCliTunnelAttrListCreate (tCliHandle CliHandle, UINT1 *pu1Arg);

INT4
TeCliTunnelAttrModeInterface (tCliHandle CliHandle);

INT4
TeCliIpExplcitPathModeChange (tCliHandle CliHandle);

INT4
TeCliTunnelAttrListDelete (tCliHandle CliHandle, UINT1 *pu1Arg);

INT4
TeCliIpExplicitPathCreate (tCliHandle CliHandle, UINT4 u4PathListId,
                           UINT4 u4PathOptNum);

INT4
TeCliIpExplicitPathDelete (tCliHandle CliHandle, UINT4 u4PathListId,
                           UINT4 u4PathOptNum);

INT4
TeCliAttrParamSrlgType (tCliHandle CliHandle, UINT4 u4AttrListIndex,
                        tTeCliArgs *pTeCliArgs);

INT4
TeCliAttrParamNoSrlgType (tCliHandle CliHandle, UINT4 u4AttrListIndex,
                          tTeCliArgs *pTeCliArgs);

INT4
TeCliAttrParamSrlgValue (tCliHandle CliHandle, UINT4 u4AttrListIndex,
                         tTeCliArgs *pTeCliArgs);

INT4
TeCliAttrParamNoSrlgValue (tCliHandle CliHandle, UINT4 u4AttrListIndex,
                           tTeCliArgs *pTeCliArgs);
INT4
TeCliAttrParamClassTypeValue (tCliHandle CliHandle, UINT4 u4AttrListIndex,
                                                      tTeCliArgs *pTeCliArgs);

INT4
TeCliAttrParamNoClassTypeValue (tCliHandle CliHandle, UINT4 u4AttrListIndex,
                                                      tTeCliArgs *pTeCliArgs);
INT4
TeCliAttrParamRowStatus (UINT4 u4AttrListIndex, INT4 i4RowStatus);

VOID
TeCliDelOrUpdateTunnelHopTable(UINT4 u4PathListIndex, UINT4 u4PathOptionId,
                               UINT4 u4HopIndex, BOOL1 bNewEntry);

INT4
TeCliSetMplsTunnelHopAddrType (UINT4 u4PathListIndex, UINT4 u4PathOptionId,
                               UINT4 u4HopIndex, INT4 i4HopAddrType);


INT4
TeCliSetMplsTunnelHopIpAddr (UINT4 u4PathListIndex, UINT4 u4PathOptionId,
                             UINT4 u4HopIndex, UINT4 u4HopIpAddr);


INT4
TeCliSetMplsTunnelHopIpPrefixLen (UINT4 u4PathListIndex, UINT4 u4PathOptionId,
                                  UINT4 u4HopIndex, UINT4 u4HopIpPrefixLen);

INT4
TeCliValidatePathListOptionId (tCliHandle CliHandle, UINT4 u4PathListId,
                               UINT4 u4PathOptionId);

INT4
TeCliSetMplsTunnelHopUnnumIf (UINT4 u4PathListIndex, UINT4 u4PathOptionId,
                              UINT4 u4HopIndex, UINT4 u4HopUnnumIf);

INT4
TeCliSetMplsTunnelHopIncludeExclude (UINT4 u4PathListIndex,
                                     UINT4 u4PathOptionId,
                                     UINT4 u4HopIndex, INT4 i4IncludeExclude);

INT4
TeCliSetFsMplsTunnelHopIncludeAny (UINT4 u4PathListIndex,
                                   UINT4 u4PathOptionId,
                                   UINT4 u4HopIndex);
INT4
TeCliSetMplsTunnelHopType (UINT4 u4PathListIndex, UINT4 u4PathOptionId,
                           UINT4 u4HopIndex, INT4 i4HopType);
INT4
TeCliSetMplsTunnelHopPathComp (UINT4 u4PathListIndex, UINT4 u4PathOptionId,
                               UINT4 u4HopIndex, INT4 i4PathComp);
INT4
TeCliSetMplsTunnelHopForwardLbl (UINT4 u4PathListIndex, UINT4 u4PathOptionId,
                                 UINT4 u4HopIndex, UINT4 u4ForwardLbl);

INT4
TeCliSetMplsTunnelHopReverseLbl (UINT4 u4PathListIndex, UINT4 u4PathOptionId,
                                 UINT4 u4HopIndex, UINT4 u4ReverseLbl);

INT4
TeCliMplsCreateExpPhbMap (tCliHandle CliHandle, UINT4 u4ExpMapIndex);

INT4
TeCliMplsDeleteExpPhbMap (tCliHandle CliHandle, INT4 u4ExpMapIndex);

INT1
TeCliMplsCreateElspInfoPhbMap(tCliHandle CliHandle,UINT4 u4ElspInfoIndex);

INT1
TeCliMplsDeleteElspInfoPhbMap(tCliHandle CliHandle,INT4 i4ElspInfoIndex);
    
INT1
TeCliDeleteMplsDiffServElspInfoRowStatus (INT4 i4ElspInfoListIndex,
                                        INT4 i4ElspInfoIndex);

INT4
TeCliMplsSetDsteStatus (tCliHandle CliHandle, UINT4 u4DsTeStatus);

INT1 
TeCliMplsSetDsTeClassType (tCliHandle CliHandle, UINT4 u4DsTeClass,
                                 UINT1 *pu1Description, UINT4 u4BwPercent);

INT1 
TeCliMplsDeleteDsTeClassType (tCliHandle CliHandle, UINT4 u4ClassType);

INT1 TeCliMplsSetExpToPhbMapping (tCliHandle CliHandle, UINT4 u4ExpBits,
                                                            UINT4 u4Phb);

INT1 TeCliMplsDestroyExpToPhbMapping (tCliHandle CliHandle, UINT4 u4ExpBits);

INT1 TeCliSetPreConfExpPhbIndex(tCliHandle CliHandle,INT4 i4PreConfId);

INT1 TeCliMplsSetElspInfoPhbMapping (tCliHandle CliHandle, UINT4 u4ExpBits,UINT4 u4Phb);

INT1 TeCliMplsDestroyElspInfoPhbMapping (tCliHandle CliHandle, UINT4 u4ExpBits);


INT1
TeCliMplsSetDsTeClassTcMap (tCliHandle CliHandle, UINT4 u4DsTeClassType, 
                            UINT1 *pu1Description);

INT1
TeCliMplsDeleteDsTeClassTcMap (tCliHandle CliHandle, UINT4 u4DsTeClassType);

INT1
TeCliMplsSetDsteTeClass (tCliHandle CliHandle, UINT4 u4TePriority, 
                         UINT4 u4TeClassNum, UINT1 *pu1Description);
INT1
TeCliMplsDeleteDsteTeClass (tCliHandle CliHandle, UINT4 u4TePriority);

INT1
TeCliMplsSetDiffServElspType (tCliHandle CliHandle, UINT4 u4LspType);

INT1
TeCliMplsAssociateExpPhbMap (tCliHandle CliHandle, UINT4 u4MapIndex);

INT1
TeCliMplsDiffServLlspPsc (tCliHandle CliHandle, UINT4 u4PscValue);

INT4
MplsDsTeCliGetOctetString (UINT1 *pu1Input, tSNMP_OCTET_STRING_TYPE *pDescription);

INT4
TeCliMplsDsTeShowStatus (tCliHandle CliHandle);
INT1
TeCliMplsDiffServShowClass (tCliHandle CliHandle, UINT4 u4ClassType);

INT4
TeCliSetTnlAttrProperties (tCliHandle CliHandle, UINT4 u4Command,
                           tTeCliArgs *pTeCliArgs);

INT1
TeCliTunnelCreate (tCliHandle CliHandle, tTeCliArgs *pTeCliArgs,
                   BOOL1 bValidateLsrId);

INT4 TeCliSetDebugLevel (tCliHandle CliHandle, INT4 i4CliDebugLevel);
INT1
TeCliTunnelInterfaceCreate (tCliHandle CliHandle, UINT4 u4MplsTunnelId,
                            UINT4 u4MplsTnlIfIndex);
INT1
TeCliTunnelInterfaceDelete (tCliHandle CliHandle, UINT4 u4MplsTunnelId,
                            UINT4 u4MplsTnlIfIndex, UINT4 u4AdminStatus);
INT1 TeCliTunnelTrafficEng(tCliHandle CliHandle);
INT1 TeCliTunnelResourceCreate(tCliHandle CliHandle, UINT4 u4BandWidth);
INT4 TeCliTunnelCRLDPResourceTable (tCliHandle CliHandle, UINT4 u4Bandwidth,
                                    UINT4 u4TunnelResourceIndex);
INT1
TeCliStaticBind (tCliHandle CliHandle, UINT4 u4InLabel, UINT4 u4InIfIndex,
                 UINT4 u4OutLabel, UINT4 u4NextHop, UINT4 u4OutIfIndex,
                 UINT4 u4MplsTnlDir, UINT1 u1TnlPathType);
INT1
TeCliTunnelInActive (tCliHandle CliHandle);
INT1
TeCliTunnelActive (tCliHandle CliHandle);
INT1
TeCliStaticBindIngress (tCliHandle CliHandle, UINT4 u4OutLabel, UINT4 u4NextHop,
                        UINT4 u4OutIfIndex, UINT4 u4MplsTnlDir, 
                        UINT1 u1TnlPathType);
INT1
TeCliStaticBindEgress (tCliHandle CliHandle, UINT4 u4InLabel,
                       UINT4 u4InIntf, UINT4 u4MplsTnlDir, UINT1 u1TnlPathType);

INT1
TeCliShowTunnel (tCliHandle CliHandle,UINT4 u4Command,
                 UINT4 u4Mode,UINT1 *pu1Arg);
INT1
TeCliShowTnlBrief (tCliHandle CliHandle,tTeTnlInfo *pTeTnlInfo);

VOID
TeCliShowTnlSummary (tCliHandle CliHandle,tTeTnlInfo *pTeTnlInfo,
                     UINT4 *pTotalHeadTnls,UINT4 *pu4SigTnls,
                     UINT4 *pu4OperUpTnls,UINT4 *pu4AdmnUpTnls,
                     UINT4 *pu4AdmnDnTnls,UINT4 *pu4TransitTnls,
                     UINT4 *pu4TailTnls);

INT1
TeCliShowTnlDetail (tCliHandle CliHandle,tTeTnlInfo *pTeTnlInfo);

INT1
TeCliShowTnlCounters (tCliHandle CliHandle,UINT4 u4IngressId, 
                      UINT4 u4EgressId, UINT4 u4TnlIndex,
                      UINT4 u4TnlInstance);

INT4
TeCliTunnelSigProtocol (tCliHandle CliHandle, INT4 i4SigProtocol);

INT4
TeCliTunnelSetTnlType (tCliHandle CliHandle, UINT4 u4TnlType);

INT4
TeCliTunnelSetTnlMode (tCliHandle CliHandle, UINT4 u4TnlMode);

INT4
TeCliTunnelSetDestTnlInfo (tCliHandle CliHandle, UINT4 u4DestTnlIndex,
                           UINT4 u4DestLspNum);
INT4
TeCliTunnelResetDestTnlInfo (tCliHandle CliHandle, UINT4 u4Mask);

INT4
TeCliTunnelSigBandwidth ( tCliHandle CliHandle,UINT4 u4TunnelIndex, INT4 i4SigProtocol,
                          UINT4 u4BandWidth);
INT4
TeCliGetLocalMapNum (UINT4 u4ContextId, UINT4 *pu4LocalMapNum);
INT4
TeCliTunnelDelete (tCliHandle CliHandle, tTeCliArgs *pTeCliArgs,
                   BOOL1 bValidateLsrId);
INT4
TeCliGetTnlIndices (tCliHandle CliHandle,
                    UINT4 *pu4TunnelIndex, UINT4 *pu4TunnelInstance,
                    UINT4 *pu4IngressId, UINT4 *pu4EgressId);
INT4
TeCliDeleteXcInAndOutSegment (tSNMP_OCTET_STRING_TYPE *pXCSegmentIndex,
                              tSNMP_OCTET_STRING_TYPE *pInSegmentIndex,
                              tSNMP_OCTET_STRING_TYPE *pOutSegmentIndex,
                              UINT4 u4XCInd, UINT4 u4InIndex, UINT4 u4OutIndex,
                              UINT4 *pu4FwdL3Intf, INT4 *pi4MplsTnlIf,
                              tTeCliArgs *pTeCliArgs);
VOID
TeCliDisplayLabelInfo (tCliHandle CliHandle, UINT4 u4XcIndex,
                       eDirection Direction, UINT1 u1TnlSgnlPrtcl);
INT1
TeValidateAndGetIngressId (tCliHandle CliHandle, UINT4 u4EgressId, 
                           UINT4 u4EgressLocalMapNum,
                           UINT4 *pu4IngressId, UINT4 *pu4IngressLocalMapNum);

INT4
TeShowRunningConfigLabelInfoDisplay(tCliHandle CliHandle,UINT4 u4XCIndex, eDirection Direction);

INT4 
TeCliAddTunnelInReoptimizeList (tCliHandle CliHandle, tTeCliArgs * pTeCliArgs);

INT4 
TeCliDelTunnelFromReoptimizeList (tCliHandle CliHandle, tTeCliArgs * pTeCliArgs);

INT4 
TeCliShowReoptimizeTunnelList (tCliHandle CliHandle);

INT4 
TeCliTriggerManualLspReoptimization (tCliHandle CliHandle, tTeCliArgs * pTeCliArgs);

INT4
TeCliEnableLspReoptimization (tCliHandle CliHandle);

INT4
TeCliDisableLspReoptimization (tCliHandle CliHandle);


/*FRR*/
#ifdef MPLS_SIG_WANTED
INT4
TeCliFrrConstInfo (tCliHandle CliHandle,tFrrCliArgs *pFrrCliArgs);
INT4
TeCliFrrNoConstInfo (tCliHandle CliHandle);
#endif
INT4
TeCliTunnelPriorities (tCliHandle CliHandle,INT4 i4SetupPriority,
                       INT4 i4HoldPriority);
INT4
TeCliTunnelNoPriorities (tCliHandle CliHandle);
INT4
TeCliTunnelAffinities (tCliHandle CliHandle,UINT4 u4IncAny,
                       UINT4 u4ExcAny,UINT4 u4IncAll);
INT4
TeCliTunnelNoAffinities (tCliHandle CliHandle);
INT4
TeCliRecordRoute (tCliHandle CliHandle, BOOL1 bLabelRecordingReqd);
INT4
TeCliNoRecordRoute (tCliHandle CliHandle);

INT4 
TeCliGmplsSetTunnelSetAdminStatus (tCliHandle CliHandle,
                                   UINT4 u4AdminStat,UINT4 u4Instance);

INT4 
TeCliMplsSetTnlProperties (tCliHandle CliHandle, UINT4 u4Command, tTeCliArgs *pTeCliArgs);

INT4
TeCliMplsQosPolicy (tCliHandle CliHandle, tTeCliArgs * pTeCliArgs);

INT4 TeCliGmplsSetSwEncType (tCliHandle CliHandle,
                             UINT4 u1SwitchingType,
                             UINT4 u1EncodingType,
                             UINT4 u4ArgsInst);

INT4
TeCliHopTablePathOptAndPathNum (tCliHandle CliHandle,UINT4 u4PathOptNum,
                                UINT4 u4PathNum,INT4 i4PathComp,UINT1 *pAttrName);

INT4
TeCliNoHopTablePathOptAndPathNum (tCliHandle CliHandle,UINT4 u4PathOptNum,
                                  UINT4 u4PathNum);

INT4
TeCliBackupHopTablePathOptAndPathNum (tCliHandle CliHandle,UINT4 u4BackupPathOptNum,
                                UINT4 u4BackupPathNum,INT4 i4BackupPathComp,UINT1 *pAttrName);

INT4
TeCliNoBackupHopTablePathOptAndPathNum (tCliHandle CliHandle,UINT4 u4BackupPathOptNum,
                                  UINT4 u4BackupPathNum);
 
INT4
TeCliTunnelHopTableEntries (tCliHandle CliHandle, UINT4 u4HopNum,
                            UINT4 u4HopAddr, INT4 i4HopType);
INT4
TeCliTunnelNoHopTableEntries (tCliHandle CliHandle, UINT4 u4HopNum);

VOID TePrintGmplsTunnelInfo (tCliHandle CliHandle, tTeTnlInfo * pTeTnlInfo);

INT4
TeCliTunnelHopPathListCreate (tCliHandle CliHandle,UINT4 u4TnlType,
                              UINT4 u4PathListId,UINT4 u4PathOptionId,
                              UINT4 u4Instance,UINT1 *pu1AttrName,UINT4 AttrBandwidth );

VOID
TeCliRrHopShow (tCliHandle CliHandle,tTeTnlInfo * pTeTnlInfo);

VOID
TeCliErHopShow (tCliHandle CliHandle,tTeTnlInfo * pTeTnlInfo, BOOL1 b1ConfigFlag);

INT4
TeCliSetTunnelNotification(tCliHandle CliHandle,INT4 i4SnmpTrapStatus);


#ifdef MPLS_RSVPTE_WANTED
INT4
TeCliFrrGlobalRevertiveTime (tCliHandle CliHandle,INT4 u4GlobalRevertiveTime);
INT4
TeCliFrrShow (tCliHandle CliHandle,INT4 i4FrrDatabase);
INT4
TeCliFrrShowOne2One (tCliHandle CliHandle,
                     UINT4 u4MplsTunnelIndex,
                     UINT4 u4MplsTunnelInstance,
                     UINT4 u4MplsTunnelIngressLSRId,
                     UINT4 u4MplsTunnelEgressLSRId);
INT4
TeCliFrrShowMany2One (tCliHandle CliHandle,
                      UINT4 u4MplsTunnelIndex,
                      UINT4 u4MplsTunnelInstance,
                      UINT4 u4MplsTunnelIngressLSRId,
                      UINT4 u4MplsTunnelEgressLSRId);
INT4
TeCliFrrShowIngressMany2One (tCliHandle CliHandle,
                             UINT4 u4MplsTunnelIndex,
                             UINT4 u4MplsTunnelInstance,
                             UINT4 u4MplsTunnelIngressLSRId,
                             UINT4 u4MplsTunnelEgressLSRId,
                             BOOL1 *bTitleFound);

INT4
TeCliFrrShowIntermediateMany2One (tCliHandle CliHandle,
                                  UINT4 u4MplsTunnelIndex,
                                  UINT4 u4MplsTunnelInstance,
                                  UINT4 u4MplsTunnelIngressLSRId,
                                  UINT4 u4MplsTunnelEgressLSRId,
                                  BOOL1 bTitleFound);

INT4
TeCliFrrShowAllFrrDatabase (tCliHandle CliHandle,UINT4,UINT4,UINT4,UINT4);

/*End of FRR*/
#endif

INT4
TeCliSetMplsTunnelRowStatus (UINT4 u4TunnelIndex, UINT4 u4TunnelInstance,
                             UINT4 u4IngressId, UINT4 u4EgressId,
                             INT4 i4RowStatusValue);

/* STATIC_HLSP */
INT4
TeCliStaticBindIngressStack (tCliHandle CliHandle, UINT4 u4OutLabel,
                             UINT4 u4HlspTnlId, UINT4 u4MplsTnlDir,
                             UINT1 u1TnlPathType);

INT4
TeCliStaticBindStack (tCliHandle CliHandle, tteCliStBindArgs *pTeCliStBindArgs);


 INT4
TeCliShowHLSP(tCliHandle CliHandle, UINT4 Flag);
 
INT4
TeCliShowHLSPStackedTnl(tCliHandle CliHandle,UINT4 u4TnlIndex,UINT4 u4TnlInstance,UINT4 u4IngressId,UINT4 u4EgressId);

INT4
TeCliShowSLSPStitchedTnl (tCliHandle CliHandle, UINT4 u4TnlIndex, 
                         UINT4 u4TnlInstance, UINT4 u4IngressId, 
                         UINT4 u4EgressId);
INT4
TeCliGetInTnlIfIndex (tCliHandle CliHandle, UINT4 u4TnlIndex, 
                      UINT4 u4TnlInstance, UINT4 u4IngresId, 
                      UINT4 u4EgressId, UINT4 u4Direction, 
                      INT4 *pi4TnlIfIndex);
INT4
TeCliGetOutTnlIfIndex (tCliHandle CliHandle, UINT4 u4TnlIndex, 
                       UINT4 u4TnlInstance, UINT4 u4IngresId, 
                       UINT4 u4EgressId, UINT4 u4Direction, 
                       INT4 *pi4TnlIfIndex);

INT4
TeCliActivateXc (tCliHandle CliHandle, UINT4 u4TnlIndex, 
                 UINT4 u4TnlInstance, UINT4 u4IngresId, 
                 UINT4 u4EgressId, UINT4 u4Direction);



/* MPLS_P2MP_LSP_CHANGES - S */
INT1
TeCliP2mpTunnelCreate (tCliHandle CliHandle, UINT4 u4TunnelIndex,
                       UINT4 u4P2mpId,
                       UINT4 u4IngressId,
                       UINT4 u4IngressLocalMapNum,
                       UINT4 u4LspNumber, UINT1 u1TnlType);

INT1
TeCliP2mpTunnelDelete (tCliHandle CliHandle,
                       UINT4 u4TunnelIndex,
                       UINT4 u4TunnelInstance,
                       UINT4 u4P2mpId,
                       UINT4 u4IngressId,
                       UINT4 u4IngressLocalMapNum);

INT1 
TeCliP2mpAddDestinationAtIngress (tCliHandle CliHandle,
                                  UINT4 u4P2mpDestId,
                                  UINT4 u4P2mpDestLocalMapNum,
                                  UINT4 u4OutLabel,
                                  UINT4 u4NextHop,
                                  UINT4 u4OutIfIndex);

INT1 
TeCliP2mpStaticBind (tCliHandle CliHandle,
                     UINT4 u4P2mpDestId,
                     UINT4 u4P2mpDestLocalMapNum,
                     UINT4 u4InLabel,
                     UINT4 u4InIfIndex,
                     UINT4 u4OutLabel,
                     UINT4 u4NextHop,
                     UINT4 u4OutIfIndex);

INT1
TeCliP2mpStaticBindEgress (tCliHandle CliHandle,
                           UINT4 u4InLabel,
                           UINT4 u4InIfIndex);

INT1 
TeCliP2mpConfigureBudNode (tCliHandle CliHandle,
                           UINT4 u4P2mpDestId,
                           UINT4 u4P2mpDestLocalMapNum);

INT1
TeCliP2mpDeleteDestination (tCliHandle CliHandle,
                           UINT4 u4P2mpDestId,
                           UINT4 u4P2mpDestLocalMapNum);

INT1
TeCliShowP2mpDestinations (tCliHandle CliHandle,
                                                         tTeTnlInfo *pTeTnlInfo);

INT1
TeCliShowP2mpBranchStatistics (tCliHandle CliHandle,
                               UINT4 u4TunnelIndex,
                               UINT4 u4TunnelInstance,
                               UINT4 u4P2mpId,
                               UINT4 u4IngressId,
                               UINT4 u4IngressLocalMapNum);

INT4
MplsLspAttributesShowRunningConfig (tCliHandle CliHandle);

INT4
MplsIpExplicitShowRunningConfig (tCliHandle CliHandle);

VOID                                                                                                              TeCliShowDynInfoForNonZeroInst (tCliHandle CliHandle, tTeTnlInfo *pTeTnlInfo);

VOID
TeCliShowTnlDynamicInfo (tCliHandle CliHandle, tTeTnlInfo *pTeTnlInfo);

VOID
TeSRCDisplayStaticXCForNonZeroInst (tCliHandle CliHandle, UINT4 u4TnlIndex,
                                    UINT4 u4TnlInstance, UINT4 u4IngressId,
                                    UINT4 u4EgressId, UINT1 *pu1Space);

VOID 
TeSRCDisplayStaticXC (tCliHandle CliHandle, UINT4 u4TnlIndex,
                      UINT4 u4TnlInstance, UINT4 u4IngressId,
                      UINT4 u4EgressId, UINT1 *pu1Space);

INT4
TeCliCreateTunnel (tCliHandle CliHandle, tTeCliArgs *pTeCliArgs);

INT4
TeCliDeleteTunnel (tCliHandle CliHandle, tTeCliArgs *pTeCliArgs,
                   BOOL1 bValidateLsrId);

INT4
TeCliDeleteP2mpOrNormalTunnel (tCliHandle CliHandle, tTeCliArgs *pTeCliArgs,
                               BOOL1 bValidateLsrId);

INT4
TeCliP2mpCreateNonZeroInstTunnel (tCliHandle CliHandle,
                                  UINT4 u4TunnelIndex, UINT4 u4TunnelInstance,
                                  UINT4 u4IngressId, UINT4 u4EgressId,
                                  UINT4 *pu4CreatedInstance);

INT4
TeCliNormalCreateNonZeroInstTunnel (tCliHandle CliHandle,
                                    UINT4 u4TunnelIndex, UINT4 u4TunnelInstance,
                                    UINT4 u4IngressId, UINT4 u4EgressId,
                                    UINT1 u1TnlPathType,
                                    UINT4 *pu4CreatedInstance,
                                    BOOL1 *pbEntryCreated);

INT4
TeCliCheckAndDelNonZeroInstTnl (tCliHandle CliHandle, UINT4 u4TunnelIndex,
                                UINT4 u4TunnelInstance, UINT4 u4IngressId,
                                UINT4 u4EgressId, BOOL1 bInstance0Present);

INT1
TeCliUnMapPreConfExpPhbIndex(tCliHandle CliHandle,INT4 i4PreConfIndex);

VOID
TeCliSetIfMainStorageType (INT4 i4IfIndex, INT4 i4Value);

VOID
TeCliSetIfMainRowStatus (INT4 i4IfIndex, INT4 i4Value);

VOID
TeCliSetMplsTunnelOldBandwidth (UINT4 u4TunnelIndex, UINT4 u4TunnelInstance,
                                UINT4 u4IngressId, UINT4 u4EgressId);
/* MPLS_P2MP_LSP_CHANGES - E */
#endif 
