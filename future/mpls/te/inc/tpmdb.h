/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: tpmdb.h,v 1.1 2010/11/16 07:29:35 prabuc Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/

#ifndef _TEP2MPDB_H
#define _TEP2MPDB_H

UINT1 MplsTeP2mpTunnelTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 MplsTeP2mpTunnelDestTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_INTEGER ,SNMP_DATA_TYPE_OCTET_PRIM};
UINT1 MplsTeP2mpTunnelBranchPerfTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_OCTET_PRIM};

UINT4 fsp2mp [] ={1,3,6,1,4,1,29601,2,59};
tSNMP_OID_TYPE fsp2mpOID = {9, fsp2mp};


UINT4 MplsTeP2mpTunnelConfigured [ ] ={1,3,6,1,4,1,29601,2,59,1,1};
UINT4 MplsTeP2mpTunnelActive [ ] ={1,3,6,1,4,1,29601,2,59,1,2};
UINT4 MplsTeP2mpTunnelTotalMaxHops [ ] ={1,3,6,1,4,1,29601,2,59,1,3};
UINT4 MplsTeP2mpTunnelP2mpIntegrity [ ] ={1,3,6,1,4,1,29601,2,59,2,1,1,2};
UINT4 MplsTeP2mpTunnelBranchRole [ ] ={1,3,6,1,4,1,29601,2,59,2,1,1,3};
UINT4 MplsTeP2mpTunnelP2mpXcIndex [ ] ={1,3,6,1,4,1,29601,2,59,2,1,1,4};
UINT4 MplsTeP2mpTunnelRowStatus [ ] ={1,3,6,1,4,1,29601,2,59,2,1,1,5};
UINT4 MplsTeP2mpTunnelStorageType [ ] ={1,3,6,1,4,1,29601,2,59,2,1,1,6};
UINT4 MplsTeP2mpTunnelSubGroupIDNext [ ] ={1,3,6,1,4,1,29601,2,59,2,2};
UINT4 MplsTeP2mpTunnelDestSrcSubGroupOriginType [ ] ={1,3,6,1,4,1,29601,2,59,2,3,1,1};
UINT4 MplsTeP2mpTunnelDestSrcSubGroupOrigin [ ] ={1,3,6,1,4,1,29601,2,59,2,3,1,2};
UINT4 MplsTeP2mpTunnelDestSrcSubGroupID [ ] ={1,3,6,1,4,1,29601,2,59,2,3,1,3};
UINT4 MplsTeP2mpTunnelDestSubGroupOriginType [ ] ={1,3,6,1,4,1,29601,2,59,2,3,1,4};
UINT4 MplsTeP2mpTunnelDestSubGroupOrigin [ ] ={1,3,6,1,4,1,29601,2,59,2,3,1,5};
UINT4 MplsTeP2mpTunnelDestSubGroupID [ ] ={1,3,6,1,4,1,29601,2,59,2,3,1,6};
UINT4 MplsTeP2mpTunnelDestDestinationType [ ] ={1,3,6,1,4,1,29601,2,59,2,3,1,7};
UINT4 MplsTeP2mpTunnelDestDestination [ ] ={1,3,6,1,4,1,29601,2,59,2,3,1,8};
UINT4 MplsTeP2mpTunnelDestBranchOutSegment [ ] ={1,3,6,1,4,1,29601,2,59,2,3,1,9};
UINT4 MplsTeP2mpTunnelDestHopTableIndex [ ] ={1,3,6,1,4,1,29601,2,59,2,3,1,10};
UINT4 MplsTeP2mpTunnelDestPathInUse [ ] ={1,3,6,1,4,1,29601,2,59,2,3,1,11};
UINT4 MplsTeP2mpTunnelDestCHopTableIndex [ ] ={1,3,6,1,4,1,29601,2,59,2,3,1,12};
UINT4 MplsTeP2mpTunnelDestARHopTableIndex [ ] ={1,3,6,1,4,1,29601,2,59,2,3,1,13};
UINT4 MplsTeP2mpTunnelDestTotalUpTime [ ] ={1,3,6,1,4,1,29601,2,59,2,3,1,14};
UINT4 MplsTeP2mpTunnelDestInstanceUpTime [ ] ={1,3,6,1,4,1,29601,2,59,2,3,1,15};
UINT4 MplsTeP2mpTunnelDestPathChanges [ ] ={1,3,6,1,4,1,29601,2,59,2,3,1,16};
UINT4 MplsTeP2mpTunnelDestLastPathChange [ ] ={1,3,6,1,4,1,29601,2,59,2,3,1,17};
UINT4 MplsTeP2mpTunnelDestCreationTime [ ] ={1,3,6,1,4,1,29601,2,59,2,3,1,18};
UINT4 MplsTeP2mpTunnelDestStateTransitions [ ] ={1,3,6,1,4,1,29601,2,59,2,3,1,19};
UINT4 MplsTeP2mpTunnelDestDiscontinuityTime [ ] ={1,3,6,1,4,1,29601,2,59,2,3,1,20};
UINT4 MplsTeP2mpTunnelDestAdminStatus [ ] ={1,3,6,1,4,1,29601,2,59,2,3,1,21};
UINT4 MplsTeP2mpTunnelDestOperStatus [ ] ={1,3,6,1,4,1,29601,2,59,2,3,1,22};
UINT4 MplsTeP2mpTunnelDestRowStatus [ ] ={1,3,6,1,4,1,29601,2,59,2,3,1,23};
UINT4 MplsTeP2mpTunnelDestStorageType [ ] ={1,3,6,1,4,1,29601,2,59,2,3,1,24};
UINT4 MplsTeP2mpTunnelBranchPerfBranch [ ] ={1,3,6,1,4,1,29601,2,59,2,4,1,1};
UINT4 MplsTeP2mpTunnelBranchPerfPackets [ ] ={1,3,6,1,4,1,29601,2,59,2,4,1,2};
UINT4 MplsTeP2mpTunnelBranchPerfHCPackets [ ] ={1,3,6,1,4,1,29601,2,59,2,4,1,3};
UINT4 MplsTeP2mpTunnelBranchPerfErrors [ ] ={1,3,6,1,4,1,29601,2,59,2,4,1,4};
UINT4 MplsTeP2mpTunnelBranchPerfBytes [ ] ={1,3,6,1,4,1,29601,2,59,2,4,1,5};
UINT4 MplsTeP2mpTunnelBranchPerfHCBytes [ ] ={1,3,6,1,4,1,29601,2,59,2,4,1,6};
UINT4 MplsTeP2mpTunnelBranchDiscontinuityTime [ ] ={1,3,6,1,4,1,29601,2,59,2,4,1,7};
UINT4 MplsTeP2mpTunnelNotificationEnable [ ] ={1,3,6,1,4,1,29601,2,59,2,5};




tMbDbEntry fsp2mpMibEntry[]= {

{{11,MplsTeP2mpTunnelConfigured}, NULL, MplsTeP2mpTunnelConfiguredGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,MplsTeP2mpTunnelActive}, NULL, MplsTeP2mpTunnelActiveGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,MplsTeP2mpTunnelTotalMaxHops}, NULL, MplsTeP2mpTunnelTotalMaxHopsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,MplsTeP2mpTunnelP2mpIntegrity}, GetNextIndexMplsTeP2mpTunnelTable, MplsTeP2mpTunnelP2mpIntegrityGet, MplsTeP2mpTunnelP2mpIntegritySet, MplsTeP2mpTunnelP2mpIntegrityTest, MplsTeP2mpTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsTeP2mpTunnelTableINDEX, 4, 0, 0, "2"},

{{13,MplsTeP2mpTunnelBranchRole}, GetNextIndexMplsTeP2mpTunnelTable, MplsTeP2mpTunnelBranchRoleGet, MplsTeP2mpTunnelBranchRoleSet, MplsTeP2mpTunnelBranchRoleTest, MplsTeP2mpTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsTeP2mpTunnelTableINDEX, 4, 0, 0, "1"},

{{13,MplsTeP2mpTunnelP2mpXcIndex}, GetNextIndexMplsTeP2mpTunnelTable, MplsTeP2mpTunnelP2mpXcIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, MplsTeP2mpTunnelTableINDEX, 4, 0, 0, NULL},

{{13,MplsTeP2mpTunnelRowStatus}, GetNextIndexMplsTeP2mpTunnelTable, MplsTeP2mpTunnelRowStatusGet, MplsTeP2mpTunnelRowStatusSet, MplsTeP2mpTunnelRowStatusTest, MplsTeP2mpTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsTeP2mpTunnelTableINDEX, 4, 0, 1, NULL},

{{13,MplsTeP2mpTunnelStorageType}, GetNextIndexMplsTeP2mpTunnelTable, MplsTeP2mpTunnelStorageTypeGet, MplsTeP2mpTunnelStorageTypeSet, MplsTeP2mpTunnelStorageTypeTest, MplsTeP2mpTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsTeP2mpTunnelTableINDEX, 4, 0, 0, "2"},

{{11,MplsTeP2mpTunnelSubGroupIDNext}, NULL, MplsTeP2mpTunnelSubGroupIDNextGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,MplsTeP2mpTunnelDestSrcSubGroupOriginType}, GetNextIndexMplsTeP2mpTunnelDestTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, MplsTeP2mpTunnelDestTableINDEX, 12, 0, 0, NULL},

{{13,MplsTeP2mpTunnelDestSrcSubGroupOrigin}, GetNextIndexMplsTeP2mpTunnelDestTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, MplsTeP2mpTunnelDestTableINDEX, 12, 0, 0, NULL},

{{13,MplsTeP2mpTunnelDestSrcSubGroupID}, GetNextIndexMplsTeP2mpTunnelDestTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, MplsTeP2mpTunnelDestTableINDEX, 12, 0, 0, NULL},

{{13,MplsTeP2mpTunnelDestSubGroupOriginType}, GetNextIndexMplsTeP2mpTunnelDestTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, MplsTeP2mpTunnelDestTableINDEX, 12, 0, 0, NULL},

{{13,MplsTeP2mpTunnelDestSubGroupOrigin}, GetNextIndexMplsTeP2mpTunnelDestTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, MplsTeP2mpTunnelDestTableINDEX, 12, 0, 0, NULL},

{{13,MplsTeP2mpTunnelDestSubGroupID}, GetNextIndexMplsTeP2mpTunnelDestTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, MplsTeP2mpTunnelDestTableINDEX, 12, 0, 0, NULL},

{{13,MplsTeP2mpTunnelDestDestinationType}, GetNextIndexMplsTeP2mpTunnelDestTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_NOACCESS, MplsTeP2mpTunnelDestTableINDEX, 12, 0, 0, NULL},

{{13,MplsTeP2mpTunnelDestDestination}, GetNextIndexMplsTeP2mpTunnelDestTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, MplsTeP2mpTunnelDestTableINDEX, 12, 0, 0, NULL},

{{13,MplsTeP2mpTunnelDestBranchOutSegment}, GetNextIndexMplsTeP2mpTunnelDestTable, MplsTeP2mpTunnelDestBranchOutSegmentGet, MplsTeP2mpTunnelDestBranchOutSegmentSet, MplsTeP2mpTunnelDestBranchOutSegmentTest, MplsTeP2mpTunnelDestTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, MplsTeP2mpTunnelDestTableINDEX, 12, 0, 0, NULL},

{{13,MplsTeP2mpTunnelDestHopTableIndex}, GetNextIndexMplsTeP2mpTunnelDestTable, MplsTeP2mpTunnelDestHopTableIndexGet, MplsTeP2mpTunnelDestHopTableIndexSet, MplsTeP2mpTunnelDestHopTableIndexTest, MplsTeP2mpTunnelDestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsTeP2mpTunnelDestTableINDEX, 12, 0, 0, "0"},

{{13,MplsTeP2mpTunnelDestPathInUse}, GetNextIndexMplsTeP2mpTunnelDestTable, MplsTeP2mpTunnelDestPathInUseGet, MplsTeP2mpTunnelDestPathInUseSet, MplsTeP2mpTunnelDestPathInUseTest, MplsTeP2mpTunnelDestTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsTeP2mpTunnelDestTableINDEX, 12, 0, 0, "0"},

{{13,MplsTeP2mpTunnelDestCHopTableIndex}, GetNextIndexMplsTeP2mpTunnelDestTable, MplsTeP2mpTunnelDestCHopTableIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, MplsTeP2mpTunnelDestTableINDEX, 12, 0, 0, NULL},

{{13,MplsTeP2mpTunnelDestARHopTableIndex}, GetNextIndexMplsTeP2mpTunnelDestTable, MplsTeP2mpTunnelDestARHopTableIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, MplsTeP2mpTunnelDestTableINDEX, 12, 0, 0, NULL},

{{13,MplsTeP2mpTunnelDestTotalUpTime}, GetNextIndexMplsTeP2mpTunnelDestTable, MplsTeP2mpTunnelDestTotalUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MplsTeP2mpTunnelDestTableINDEX, 12, 0, 0, NULL},

{{13,MplsTeP2mpTunnelDestInstanceUpTime}, GetNextIndexMplsTeP2mpTunnelDestTable, MplsTeP2mpTunnelDestInstanceUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MplsTeP2mpTunnelDestTableINDEX, 12, 0, 0, NULL},

{{13,MplsTeP2mpTunnelDestPathChanges}, GetNextIndexMplsTeP2mpTunnelDestTable, MplsTeP2mpTunnelDestPathChangesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsTeP2mpTunnelDestTableINDEX, 12, 0, 0, NULL},

{{13,MplsTeP2mpTunnelDestLastPathChange}, GetNextIndexMplsTeP2mpTunnelDestTable, MplsTeP2mpTunnelDestLastPathChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MplsTeP2mpTunnelDestTableINDEX, 12, 0, 0, NULL},

{{13,MplsTeP2mpTunnelDestCreationTime}, GetNextIndexMplsTeP2mpTunnelDestTable, MplsTeP2mpTunnelDestCreationTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MplsTeP2mpTunnelDestTableINDEX, 12, 0, 0, NULL},

{{13,MplsTeP2mpTunnelDestStateTransitions}, GetNextIndexMplsTeP2mpTunnelDestTable, MplsTeP2mpTunnelDestStateTransitionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsTeP2mpTunnelDestTableINDEX, 12, 0, 0, NULL},

{{13,MplsTeP2mpTunnelDestDiscontinuityTime}, GetNextIndexMplsTeP2mpTunnelDestTable, MplsTeP2mpTunnelDestDiscontinuityTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MplsTeP2mpTunnelDestTableINDEX, 12, 0, 0, NULL},

{{13,MplsTeP2mpTunnelDestAdminStatus}, GetNextIndexMplsTeP2mpTunnelDestTable, MplsTeP2mpTunnelDestAdminStatusGet, MplsTeP2mpTunnelDestAdminStatusSet, MplsTeP2mpTunnelDestAdminStatusTest, MplsTeP2mpTunnelDestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsTeP2mpTunnelDestTableINDEX, 12, 0, 0, "1"},

{{13,MplsTeP2mpTunnelDestOperStatus}, GetNextIndexMplsTeP2mpTunnelDestTable, MplsTeP2mpTunnelDestOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, MplsTeP2mpTunnelDestTableINDEX, 12, 0, 0, NULL},

{{13,MplsTeP2mpTunnelDestRowStatus}, GetNextIndexMplsTeP2mpTunnelDestTable, MplsTeP2mpTunnelDestRowStatusGet, MplsTeP2mpTunnelDestRowStatusSet, MplsTeP2mpTunnelDestRowStatusTest, MplsTeP2mpTunnelDestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsTeP2mpTunnelDestTableINDEX, 12, 0, 1, NULL},

{{13,MplsTeP2mpTunnelDestStorageType}, GetNextIndexMplsTeP2mpTunnelDestTable, MplsTeP2mpTunnelDestStorageTypeGet, MplsTeP2mpTunnelDestStorageTypeSet, MplsTeP2mpTunnelDestStorageTypeTest, MplsTeP2mpTunnelDestTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsTeP2mpTunnelDestTableINDEX, 12, 0, 0, "2"},

{{13,MplsTeP2mpTunnelBranchPerfBranch}, GetNextIndexMplsTeP2mpTunnelBranchPerfTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_NOACCESS, MplsTeP2mpTunnelBranchPerfTableINDEX, 5, 0, 0, NULL},

{{13,MplsTeP2mpTunnelBranchPerfPackets}, GetNextIndexMplsTeP2mpTunnelBranchPerfTable, MplsTeP2mpTunnelBranchPerfPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsTeP2mpTunnelBranchPerfTableINDEX, 5, 0, 0, NULL},

{{13,MplsTeP2mpTunnelBranchPerfHCPackets}, GetNextIndexMplsTeP2mpTunnelBranchPerfTable, MplsTeP2mpTunnelBranchPerfHCPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, MplsTeP2mpTunnelBranchPerfTableINDEX, 5, 0, 0, NULL},

{{13,MplsTeP2mpTunnelBranchPerfErrors}, GetNextIndexMplsTeP2mpTunnelBranchPerfTable, MplsTeP2mpTunnelBranchPerfErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsTeP2mpTunnelBranchPerfTableINDEX, 5, 0, 0, NULL},

{{13,MplsTeP2mpTunnelBranchPerfBytes}, GetNextIndexMplsTeP2mpTunnelBranchPerfTable, MplsTeP2mpTunnelBranchPerfBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsTeP2mpTunnelBranchPerfTableINDEX, 5, 0, 0, NULL},

{{13,MplsTeP2mpTunnelBranchPerfHCBytes}, GetNextIndexMplsTeP2mpTunnelBranchPerfTable, MplsTeP2mpTunnelBranchPerfHCBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, MplsTeP2mpTunnelBranchPerfTableINDEX, 5, 0, 0, NULL},

{{13,MplsTeP2mpTunnelBranchDiscontinuityTime}, GetNextIndexMplsTeP2mpTunnelBranchPerfTable, MplsTeP2mpTunnelBranchDiscontinuityTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MplsTeP2mpTunnelBranchPerfTableINDEX, 5, 0, 0, NULL},

{{11,MplsTeP2mpTunnelNotificationEnable}, NULL, MplsTeP2mpTunnelNotificationEnableGet, MplsTeP2mpTunnelNotificationEnableSet, MplsTeP2mpTunnelNotificationEnableTest, MplsTeP2mpTunnelNotificationEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};
tMibData fsp2mpEntry = { 41, fsp2mpMibEntry };

#endif /* _TEP2MPDB_H */

