/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tedefs.h,v 1.22 2017/06/08 11:40:33 siva Exp $
 *
 * Description: This file contains the definations of constants
 *              used in the TE Module.
 *******************************************************************/

#ifndef _TEDEFS_H 
#define _TEDEFS_H 

/* Hash Table related constants */
#define TE_HASH_SIZE                128

/* Pools related constants */
#define TE_MAX_POOLS                 16

/* Max Entries Constant */
#define TE_MAX_ENTRIES                65535

#define TE_MAX_UINT4_VALUE            0xffffffff
#define TE_MIN_UINT4_VALUE            0x00000000
#define TE_MIN_MCAST_ADDR_VALUE       0xe0000000
#define TE_MAX_MCAST_ADDR_VALUE       0xe00000ff

#define TE_L2VPN_TNL_STAT_REQ        0x02000001
#define TE_L2VPN_TNL_ASSOCIATE_REQ   0x02000002

#define TE_TNL_RSRC_INDEX_MINVAL      1
#define TE_TNL_RSRC_INDEX_MAXVAL      65535  /* Currently marked to 64K to be
                                               changed if required. */
#define TE_TNLINDEX_MINVAL            1
#define TE_TNLINDEX_MAXVAL            65535    /* Currently marked to 64K to be
                                               changed if required. */
#define TE_TNLINST_MINVAL             0
#define TE_TNLINST_MAXVAL             65535    /* Currently marked to 64K to be
                                               changed if required. */
#define TE_TNL_HOPLSTINDEX_MINVAL     1
#define TE_TNL_HOPLSTINDEX_MAXVAL     65535    /* Currently marked to 64K to be
                                               changed if required. */

#define TE_TNL_POINDEX_MINVAL         1
#define TE_TNL_POINDEX_MAXVAL         8      /* Currently marked to 8 to be
                                               changed if required. */
#define TE_TNL_ERHOPINDEX_MINVAL      1
#define TE_TNL_ERHOPINDEX_MAXVAL      65535 /* Currently marked to 64K to be
                                               changed if required. */

#define TE_TNL_ARHOPLSTINDEX_MINVAL   1
#define TE_TNL_ARHOPLSTINDEX_MAXVAL   65535

#define TE_TNL_ARHOPINDEX_MINVAL   1
#define TE_TNL_ARHOPINDEX_MAXVAL   65535 /* Currently marked to 64K to be
                                               changed if required. */

#define TE_TNL_CHOPLSTINDEX_MINVAL   1
#define TE_TNL_CHOPLSTINDEX_MAXVAL   65535

#define TE_MAX_TERPTETNL             (TE_MAX_TNLS (gTeGblInfo)/2) /* Restrict user and RSVP to
                                                                   * create only Max number of
                                                                   * allowed tunnels */   
/*Constants associated with Tunnel Attribute Table */
#define TE_ATTRINDEX_MINVALUE         1
#define TE_ATTRINDEX_MAXVALUE         50

/* Constants associated with the Traffic Parameter table */

#define TE_CRLDP_MIN_PD               10
#define TE_CRLDP_MAX_PD               10000000    /* Max Peak Data Rate on an
                                                  Ethernet Interface */
#define TE_CRLDP_MIN_PB               512
#define TE_CRLDP_MAX_PB               1024
#define TE_CRLDP_MIN_CD               5
#define TE_CRLDP_MAX_CD               256
#define TE_CRLDP_MIN_CB               256
#define TE_CRLDP_MAX_CB               512
#define TE_CRLDP_MIN_EB               64
#define TE_CRLDP_MAX_EB               128

/* TBR - Token Bucket Rate */
#define TE_RPTE_TBR_MINVAL            0
#define TE_RPTE_TBR_MAXVAL            10000000  /* Currently marked to 10G to be
                                                 modified as required later. */

/* TBS - Token Bucket Size */
#define TE_RPTE_TBS_MINVAL            0
#define TE_RPTE_TBS_MAXVAL            10000000  /* Currently marked to 10G to be
                                                 modified as required later. */

/* PDR - Peak Data Rate */
#define TE_RPTE_PDR_MINVAL            0
#define TE_RPTE_PDR_MAXVAL            10000000  /* Currently marked to 10G to be
                                                 modified as required later. */

/* MPU - Minimum Policied Unit */
#define TE_RPTE_MPU_MINVAL            0
#define TE_RPTE_MPU_MAXVAL            65535     /* Currently marked to 64K to be
                                                modified as required later. */

/* MPS - Maximum Packet Size */
#define TE_RPTE_MPS_MINVAL            0
#define TE_RPTE_MPS_MAXVAL            65535    /* Currently marked to 64K to be
                                               modified as required later. */
#define TE_RPTE_MPS_DEFVAL            1500

#define TE_MIN_RES_FLAG_VAL           0
#define TE_MAX_RES_FLAG_VAL          63


#define TE_MIN_WGT_VAL                0
#define TE_MAX_WGT_VAL              255


/* Macros used in fsmpls mibs low level */

#define TE_TNL_ATTR_TABLE_DEF_OFFSET 14
#define TE_SETPRIO_MINVAL             0
#define TE_SETPRIO_MAXVAL             7
#define TE_HLDPRIO_MINVAL             0
#define TE_HLDPRIO_MAXVAL             7

#define TE_ATTR_SETPRIO_MINVAL        0
#define TE_ATTR_SETPRIO_MAXVAL        7
#define TE_ATTR_HLDPRIO_MINVAL        0
#define TE_ATTR_HLDPRIO_MAXVAL        7

/*ClassType values */
#define TE_DS_CLASS_TYPE0             0
#define TE_DS_CLASS_TYPE1             1
#define TE_DS_CLASS_TYPE2             2
#define TE_DS_CLASS_TYPE3             3
#define TE_DS_CLASS_TYPE4             4
#define TE_DS_CLASS_TYPE5             5
#define TE_DS_CLASS_TYPE6             6
#define TE_DS_CLASS_TYPE7             7

/* Default values */
#define TE_ATTR_SSN_ATTR_DEF_VAL      0

#define TE_ZERO                       0
#define TE_ONE                        1 
#define TE_TWO                        2
#define TE_THREE                      3
#define TE_FOUR                       4
#define TE_FIVE                       5

#define TE_UINT2_SIZE               255

/* TE Configuration for the Resource pointer Definitions */
#define TE_TNL_RSRC_TABLE_DEF_OFFSET 14
#define TE_TNL_XC_TABLE_DEF_OFFSET   14 
#define TE_TNL_XC_TABLE_MAX_OFFSET   28

/* word boundary */
#define TE_WORD_BNDRY                4
#define TE_SSN_NAME_MIN_LEN          8

/* Storage Types */
#define TE_STORAGE_OTHER             1
#define TE_STORAGE_VOLATILE          2
#define TE_STORAGE_NONVOLATILE       3
#define TE_STORAGE_PERMANENT         4
#define TE_STORAGE_READONLY          5

#define TE_SSN_FLAG_MASK             0xF8

/* Tunnel Owner */
#define TE_TNLOWN_UNKNOWN            1
#define TE_TNLOWN_OTHER              2
#define TE_TNLOWN_SNMP               3
#define TE_TNLOWN_LDP                4
#define TE_TNLOWN_CRLDP              5
#define TE_TNLOWN_RSVP               6
#define TE_TNLOWN_PLCYAGNT           7

/* Instance Priority */
#define TE_INSTPRIO_MINVAL           0
#define TE_INSTPRIO_MAXVAL           7

/* Values For CRLDP Resource CFG Flags */
#define TE_CRLDP_CFG_PDR_FLAG        0x0001
#define TE_CRLDP_CFG_PBS_FLAG        0x0002
#define TE_CRLDP_CFG_CDR_FLAG        0x0004
#define TE_CRLDP_CFG_CBS_FLAG        0x0008
#define TE_CRLDP_CFG_EBS_FLAG        0x0010
#define TE_CRLDP_CFG_FREQUENCY_FLAG  0x0020
#define TE_CRLDP_CFG_RESFLAGS_FLAG   0x0040

#define TE_CRLDP_DESIRED_CFG_FLAG    0x007f
/*TODO Change this values to default*/
/*DEF CRLDP Resource Table Values*/
#define TE_CRLDP_WGS                 2
#define TE_CRLDP_DR                  5
#define TE_CRLDP_FLAGS               10
#define TE_CRLDP_EBS                 100
#define TE_CRLDP_CBS                 500
#define TE_CRLDP_PBS                 700
/* Values For RSVPTE Resource CFG Flags */
#define TE_RSVPTE_CFG_TBR_FLAG       0x0001
#define TE_RSVPTE_CFG_TBS_FLAG       0x0002
#define TE_RSVPTE_CFG_PDR_FLAG       0x0004
#define TE_RSVPTE_CFG_MPU_FLAG       0x0008
#define TE_RSVPTE_CFG_MPS_FLAG       0x0010

#define TE_RSVPTE_DESIRED_CFG_FLAG   0x001f


#define TE_SIG_PROT_RSVP_SET         0x0f
#define TE_SIG_PROT_LDP_SET          0xf0

/* Default values*/
#define TE_DFLT_TRFC_PRAM_INDEX        1
#define TE_SETUP_PRIO_DEF_VAL          0
#define TE_HOLD_PRIO_DEF_VAL           0

#define TE_ROLE_DEF_VAL                TE_INGRESS
#define TE_TUNNEL_OWNER_DEF_VAL        TE_TNL_OWNER_ADMIN
#define TE_TUNNEL_ISIF_DEF_VAL         TE_FALSE
#define TE_TUNNEL_PROTECTINUSE_DEF_VAL TE_FALSE
#define TE_TUNNEL_SIGN_PROTO_DEF_VAL   TE_SIGPROTO_NONE
#define TE_TUNNEL_INS_PRIO_DEF_VAL     0
#define TE_TUNNEL_SSN_ATTR_DEF_VAL     0
#define TE_STORAGE_TYPE_DEF_VAL        TE_STORAGE_VOLATILE
#define TE_MEM_DEFAULT_MEMORY_TYPE     MEM_DEFAULT_MEMORY_TYPE 

#define TE_INET_ADDRSTRLEN      INET_ADDRSTRLEN

/* MPLS_P2MP_LSP_CHANGES - S */
#define TE_P2MP_NOT_BRANCH           0x1
#define TE_P2MP_BRANCH               0x2
#define TE_P2MP_BUD                  0x3
#define TE_P2MP_BRANCH_ROLE_DEF_VAL  TE_P2MP_NOT_BRANCH
/* MPLS_P2MP_LSP_CHANGES - E */

/* Tunnel Trap Macros */
#define TE_TNL_UP_TRAP         1
#define TE_TNL_DOWN_TRAP       2
#define TE_TNL_REROUTE_TRAP    3
#define TE_TNL_REOPTIMIZE_TRAP 4

/* FRR Related Default Values */
#define TE_TNL_FRR_PROT_METHOD_DEF_VAL    TE_TNL_FRR_ONE2ONE_METHOD
#define TE_TNL_FRR_PROT_TYPE_DEF_VAL      TE_TNL_FRR_PROT_LINK
#define TE_TNL_FRR_HOP_LIMIT_DEF_VAL      32
#define TE_TNL_FRR_SETUP_PRIO_DEF_VAL     7

#define TE_TNL_FRR_GBL_REVERT_DEF_TIME    600000 /* 10 minutes */

/* FRR Related Range Values */
#define TE_TNL_FRR_HOP_LIM_MIN_VAL    1
#define TE_TNL_FRR_HOP_LIM_MAX_VAL    64


#define GMPLS_FORWARD        TE_TNL_MODE_UNIDIRECTIONAL
#define GMPLS_BIDIRECTIONAL  TE_TNL_MODE_COROUTED_BIDIRECTIONAL

/* Macros for Set, Get and Test functions for stdgmte.mib */
#define TE_GMPLS_TUNNEL_UNNUM_IF                          1
#define TE_GMPLS_TUNNEL_ATTRIBUTES                        2
#define TE_GMPLS_TUNNEL_ENCODING_TYPE                     3
#define TE_GMPLS_TUNNEL_SWITCHING_TYPE                    4
#define TE_GMPLS_TUNNEL_LINK_PROTECTION_TYPE              5
#define TE_GMPLS_TUNNEL_GPID                              6
#define TE_GMPLS_TUNNEL_SECONDARY                         7 
#define TE_GMPLS_TUNNEL_DIRECTION                         8
#define TE_GMPLS_TUNNEL_PATH_COMP                         9
#define TE_GMPLS_UPSTREAM_NOTIFY_RECEPIENT_TYPE           10
#define TE_GMPLS_UPSTREAM_NOTIFY_RECEPIENT                11
#define TE_GMPLS_DOWNSTREAM_NOTIFY_RECEPIENT_TYPE         12
#define TE_GMPLS_DOWNSTREAM_NOTIFY_RECEPIENT              13
#define TE_GMPLS_SEND_PATH_NOTIFY_RECEPIENT_TYPE          14
#define TE_GMPLS_SEND_PATH_NOTIFY_RECEPIENT               15
#define TE_GMPLS_SEND_RESV_NOTIFY_RECEPIENT_TYPE          16
#define TE_GMPLS_SEND_RESV_NOTIFY_RECEPIENT               17
#define TE_GMPLS_TUNNEL_ADMIN_STATUS_FLAGS                18
#define TE_MPLS_TNL_HOP_INCLUDE_EXCLUDE                   19
#define TE_MPLS_TNL_HOP_PATH_COMP                         20
#define TE_MPLS_TNL_HOP_ROW_STATUS                        21
#define TE_GMPLS_TNL_HOP_FORWARD_LBL                      22
#define TE_GMPLS_TNL_HOP_REVERSE_LBL                      23
#define TE_GMPLS_TNL_HOP_FORWARD_LBL_PTR                  24
#define TE_GMPLS_TNL_HOP_REVERSE_LBL_PTR                  25


/* Macros for Set, Get and Test functions for fsmpte.mib*/
#define TE_FS_MPLS_END_TO_END_PROTECTION                  1
#define TE_FS_MPLS_PR_CONFIG_OPER_TYPE                    2
#define TE_FS_MPLS_SRLG_TYPE                              3
#define TE_FS_MPLS_ATTR_PTR                               4
#define TE_FS_MPLS_TNL_PATH_TYPE                          5
#define TE_FS_MPLS_MBB_STATUS                             6
#define TE_FS_MPLS_TUNNEL_IFINDEX                         7

/*Attribute table in fsmpte.mib */
#define TE_FS_MPLS_ATTR_NAME                              4
#define TE_FS_MPLS_ATTR_SETUP_PRIOR                       5
#define TE_FS_MPLS_ATTR_HOLDING_PRIOR                     6
#define TE_FS_MPLS_ATTR_INCLUDE_ANY                       7
#define TE_FS_MPLS_ATTR_INCLUDE_ALL                       8
#define TE_FS_MPLS_ATTR_EXCLUDE_ANY                       9
#define TE_FS_MPLS_ATTR_SSN_ATTR                          10
#define TE_FS_MPLS_ATTR_BANDWIDTH                         11
#define TE_FS_MPLS_ATTR_CLASS_TYPE                        12
#define TE_FS_MPLS_ATTR_SRLG_TYPE                         13
#define TE_FS_MPLS_ATTR_ROW_STATUS                        14

#define TE_FS_MPLS_SRLG_ROW_STATUS                        15
#define TE_FS_MPLS_ATTR_SRLG_ROW_STATUS                   16

/* Following macro converts kilo bits per second
 * to bytes per second */
#define TE_CONVERT_KBPS_TO_BYTES_PER_SEC(x)\
{\
   x = (x*1000)/8;\
}

#endif
/*----------------------------------------------------------------------------*
*                        End of File tedefs.h                                 *
*----------------------------------------------------------------------------*/
