/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: teincs.h,v 1.13 2012/10/24 10:44:22 siva Exp $
 *
 * Description: Contains all include files.
 *******************************************************************/

#ifndef _TE_H
#define _TE_H


#include "lr.h" 
#include "ip.h"
#include "fssocket.h"
#include "cfa.h"

/* Signalling protos Includes */
#include "rpteext.h"

/* TE module includes */
#include "teextrn.h"
#include "temacs.h"
#include "tefsap.h"
#include "tedefs.h"
#include "tetdfs.h"
#include "temacs.h"
#include "teport.h"
#include "teextif.h"
#include "tegblex.h"
#include "teprot.h"
#include "tedbg.h"
#include "ldpext.h"
/* MPLS_P2MP_LSP_CHANGES - S */
#include "tpmtdfsg.h"
/* MPLS_P2MP_LSP_CHANGES - E */

/* TE-DiffServ includes */
#include "mplsdiff.h"
#include "mplssize.h"
#include "mplsutil.h"
#include "tedsdefs.h"
#include "tedsmacs.h"
#include "tedsprot.h"
#include "mplsdsrm.h"
#include "dsrmgblex.h"

/* l2vpn includes */
#include "l2vpextn.h"
#include "tesz.h"
/* common database includes */
#include "mplcmndb.h"

/* STATIC_HLSP */
/*TE-HLSP includes */
#include "thllwg.h"
#include "tlm.h"
#include "cli.h"
#include "csr.h"

/* label manager  includes */
#include "lblmgrex.h"

#endif /* _TE_H */
