#ifndef _STDTEWR_H
#define _STDTEWR_H

VOID RegisterSTDTE(VOID);

VOID UnRegisterSTDTE(VOID);
INT4 MplsTunnelConfiguredGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelActiveGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelTEDistProtoGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelMaxHopsGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelNotificationMaxRateGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelNotificationMaxRateSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelNotificationMaxRateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelNotificationMaxRateDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

INT4 MplsTunnelIndexNextGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexMplsTunnelTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsTunnelNameGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelDescrGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelIsIfGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelIfIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelOwnerGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelRoleGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelXCPointerGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelSignallingProtoGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelSetupPrioGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHoldingPrioGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelSessionAttributesGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelLocalProtectInUseGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelResourcePointerGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelPrimaryInstanceGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelInstancePriorityGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopTableIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelPathInUseGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelARHopTableIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelCHopTableIndexGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelIncludeAnyAffinityGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelIncludeAllAffinityGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelExcludeAnyAffinityGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelTotalUpTimeGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelInstanceUpTimeGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelPrimaryUpTimeGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelPathChangesGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelLastPathChangeGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelCreationTimeGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelStateTransitionsGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelAdminStatusGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelOperStatusGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelNameSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelDescrSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelIsIfSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelRoleSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelXCPointerSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelSignallingProtoSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelSetupPrioSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHoldingPrioSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelSessionAttributesSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelLocalProtectInUseSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelResourcePointerSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelInstancePrioritySet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopTableIndexSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelPathInUseSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelIncludeAnyAffinitySet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelIncludeAllAffinitySet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelExcludeAnyAffinitySet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelAdminStatusSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelDescrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelIsIfTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelRoleTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelXCPointerTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelSignallingProtoTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelSetupPrioTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHoldingPrioTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelSessionAttributesTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelLocalProtectInUseTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelResourcePointerTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelInstancePriorityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopTableIndexTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelPathInUseTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelIncludeAnyAffinityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelIncludeAllAffinityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelExcludeAnyAffinityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelAdminStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);




















INT4 MplsTunnelHopListIndexNextGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexMplsTunnelHopTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsTunnelHopAddrTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopIpAddrGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopIpPrefixLenGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopAsNumberGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopAddrUnnumGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopLspIdGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopIncludeGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopPathOptionNameGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopEntryPathCompGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopAddrTypeSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopIpAddrSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopIpPrefixLenSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopAsNumberSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopAddrUnnumSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopLspIdSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopTypeSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopIncludeSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopPathOptionNameSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopEntryPathCompSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopAddrTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopIpAddrTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopIpPrefixLenTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopAsNumberTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopAddrUnnumTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopLspIdTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopIncludeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopPathOptionNameTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopEntryPathCompTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelHopTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);












INT4 MplsTunnelResourceIndexNextGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexMplsTunnelResourceTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsTunnelResourceMaxRateGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelResourceMeanRateGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelResourceMaxBurstSizeGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelResourceMeanBurstSizeGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelResourceExBurstSizeGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelResourceFrequencyGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelResourceWeightGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelResourceRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelResourceStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelResourceMaxRateSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelResourceMeanRateSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelResourceMaxBurstSizeSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelResourceMeanBurstSizeSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelResourceExBurstSizeSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelResourceFrequencySet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelResourceWeightSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelResourceRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelResourceStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelResourceMaxRateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelResourceMeanRateTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelResourceMaxBurstSizeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelResourceMeanBurstSizeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelResourceExBurstSizeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelResourceFrequencyTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelResourceWeightTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelResourceRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelResourceStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelResourceTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);









INT4 GetNextIndexMplsTunnelARHopTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsTunnelARHopAddrTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelARHopIpAddrGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelARHopAddrUnnumGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelARHopLspIdGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexMplsTunnelCHopTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsTunnelCHopAddrTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelCHopIpAddrGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelCHopIpPrefixLenGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelCHopAsNumberGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelCHopAddrUnnumGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelCHopLspIdGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelCHopTypeGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexMplsTunnelPerfTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsTunnelPerfPacketsGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelPerfHCPacketsGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelPerfErrorsGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelPerfBytesGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelPerfHCBytesGet(tSnmpIndex *, tRetVal *);
INT4 GetNextIndexMplsTunnelCRLDPResTable(tSnmpIndex *, tSnmpIndex *);
INT4 MplsTunnelCRLDPResMeanBurstSizeGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelCRLDPResExBurstSizeGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelCRLDPResFrequencyGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelCRLDPResWeightGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelCRLDPResFlagsGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelCRLDPResRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelCRLDPResStorageTypeGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelCRLDPResMeanBurstSizeSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelCRLDPResExBurstSizeSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelCRLDPResFrequencySet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelCRLDPResWeightSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelCRLDPResFlagsSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelCRLDPResRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelCRLDPResStorageTypeSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelCRLDPResMeanBurstSizeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelCRLDPResExBurstSizeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelCRLDPResFrequencyTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelCRLDPResWeightTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelCRLDPResFlagsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelCRLDPResRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelCRLDPResStorageTypeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelCRLDPResTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);







INT4 MplsTunnelNotificationEnableGet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelNotificationEnableSet(tSnmpIndex *, tRetVal *);
INT4 MplsTunnelNotificationEnableTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 MplsTunnelNotificationEnableDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);

#endif /* _STDTEWR_H */
