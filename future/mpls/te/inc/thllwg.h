/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: thllwg.h,v 1.5 2011/07/01 11:25:33 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level Routine All Objects.  */

/* Low level routines for Tunnel type */
INT1
nmhTestv2MplsTunnelType (UINT4 *, UINT4 ,UINT4 , UINT4 , UINT4 ,INT4 );

INT1
nmhSetMplsTunnelType (UINT4 , UINT4 , UINT4 , UINT4 , INT4 );

INT1
nmhGetMplsTunnelType (UINT4, UINT4,UINT4 ,UINT4 ,UINT4 *);


/* Low lovel routines for HLSP Parameters */
INT1 
nmhGetFsMplsHLSPAvailableBW (UINT4, UINT4 ,UINT4 ,
                  UINT4 ,UINT4 *);

INT1
nmhGetFsMplsHLSPNoOfStackedTunnels (UINT4 , UINT4 , UINT4 , UINT4 ,
                 UINT4 *);

INT1
nmhValidateIndexInstanceFsMplsHLSPTable(UINT4  , UINT4 ,UINT4,UINT4 );

INT1
nmhGetFirstIndexFsMplsHLSPTable(UINT4 *, UINT4 *, UINT4 *, UINT4 *);

INT1
nmhGetNextIndexFsMplsHLSPTable(UINT4  ,UINT4 * ,UINT4  ,UINT4 *,UINT4 ,UINT4 *,UINT4 ,UINT4 *);

/* Low level routines for FsMplsLSPMapTunnelTable */
INT1
nmhValidateIndexInstanceFsMplsLSPMapTunnelTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMplsLSPMapTunnelTable  */
INT1
nmhGetFirstIndexFsMplsLSPMapTunnelTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */
INT1
nmhGetNextIndexFsMplsLSPMapTunnelTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */
INT1
nmhGetFsMplsLSPMaptunnelOperation ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsLSPMaptunnelRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */
INT1
nmhSetFsMplsLSPMaptunnelOperation ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMplsLSPMaptunnelRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */
INT1
nmhTestv2FsMplsLSPMaptunnelOperation ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsLSPMaptunnelRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhDepv2FsMplsLSPMapTunnelTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT4 
TeGetAllFsMplsLSPMapTunnelTable PROTO ((tteFsMplsLSPMapTunnelTable *));

INT4
TeTestAllFsMplsLSPMapTunnelTable(UINT4 *,tteFsMplsLSPMapTunnelTable *,
tteIsSetFsMplsLSPMapTunnelTable *,INT4, INT4 );

INT4 
TeSetAllFsMplsLSPMapTunnelTable (tteFsMplsLSPMapTunnelTable
*,tteIsSetFsMplsLSPMapTunnelTable *, 
INT4 , INT4 );

INT4 FsMplsLSPMapTunnelTableFilterInputs PROTO ((tteFsMplsLSPMapTunnelTable *,
tteFsMplsLSPMapTunnelTable *, tteIsSetFsMplsLSPMapTunnelTable *));

INT4 TeUtilUpdateFsMplsLSPMapTunnelTable PROTO ((tteFsMplsLSPMapTunnelTable *,
tteFsMplsLSPMapTunnelTable *));

INT4 TeSetAllFsMplsLSPMapTunnelTableTrigger PROTO ((tteFsMplsLSPMapTunnelTable
*, tteIsSetFsMplsLSPMapTunnelTable *, INT4));

INT4  
FsMplsLSPMapTunnelTableRBCmp (tRBElem * pRBElem1,
                                                tRBElem * pRBElem2);

INT4
TeFsMplsLSPMapTunnelTableCreate (void);

INT4
TeFsMplsLSPMapTunnelTableDelete(void);

