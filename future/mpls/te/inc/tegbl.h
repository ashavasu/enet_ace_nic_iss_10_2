/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tegbl.h,v 1.10 2014/06/21 13:09:12 siva Exp $
 *
 * Description: This file contains definition of global variables.
 *******************************************************************/

#ifndef _TE_GBL_H
#define _TE_GBL_H

tTeGblInfo gTeGblInfo;
tTnlCliArgs gaTnlCliArgs[CLI_MAX_SESSIONS];
tTeCliArgs gaTeCliArgs[CLI_MAX_SESSIONS];

UINT4 au4TeTnlResourceTableOid[TE_TNL_RSRC_TABLE_DEF_OFFSET] = 
{1,3,6,1,2,1,10,166,3,2,6,1,2,0};
UINT4 au4TeTnlXCTableOid[TE_TNL_XC_TABLE_DEF_OFFSET]         = 
{1,3,6,1,2,1,10,166,2,1,10,1,4,4};
UINT4 au4DiffServElspInfoTableOid[TE_DS_ELSPINFO_TABLE_DEF_OFFSET] =
                        {1,3,6,1,4,1,2076,13,1,7,4,1,0};

UINT4 au4TeTnlAttributeTableOid[TE_TNL_ATTR_TABLE_DEF_OFFSET] =
                        {1,3,6,1,4,1,2076,13,15,1,5,1,2,0};      
UINT4 gu4TeDbgFlag;
UINT4 gu4TeDbgLvl;
UINT4 gu4TeRpteTnlCount = 0;

/* STATIC_HLSP */
tTeLSPMapTnlGblInfo gTeLSPMapTnlGblInfo; 
UINT4  gu4TeNoOfHlsp;
UINT4  gu4TeNoOfSlsp;
#endif /* _TE_GBL_H  */
