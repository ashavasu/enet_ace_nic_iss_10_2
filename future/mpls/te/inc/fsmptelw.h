/********************************************************************
* Copyright (C) 2010 Aricent Inc . All Rights Reserved
*
* $Id: fsmptelw.h,v 1.10 2016/02/03 10:37:59 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto Validate Index Instance for FsMplsTunnelTable. */
INT1
nmhValidateIndexInstanceFsMplsTunnelTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMplsTunnelTable  */

INT1
nmhGetFirstIndexFsMplsTunnelTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsTunnelTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsTunnelType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsTunnelLSRIdMapInfo ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsTunnelMode ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsTunnelProactiveSessIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsTunnelMBBStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsTunnelDisJointType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsTunnelAttPointer ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OID_TYPE * ));

INT1
nmhGetFsMplsTunnelEndToEndProtection ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsMplsTunnelPrConfigOperType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsTunnelSrlgType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsTunnelIfIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsTunnelInitReOptimize ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsTunnelIsProtectingLsp ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsLspTunnelMapIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsLspTunnelMapInstance ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsLspTunnelMapIngressLSRId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsLspTunnelMapEgressLSRId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsTunnelSynchronizationStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsTunnelOutPathMsgId ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsTunnelBackupHopTableIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsTunnelBackupPathInUse ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));


/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsTunnelType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMplsTunnelLSRIdMapInfo ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMplsTunnelMode ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsTunnelMBBStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsTunnelDisJointType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsTunnelAttPointer ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhSetFsMplsTunnelEndToEndProtection ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsMplsTunnelPrConfigOperType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsTunnelSrlgType ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsTunnelIfIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsTunnelInitReOptimize ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsTunnelIsProtectingLsp ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsTunnelBackupHopTableIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMplsTunnelBackupPathInUse ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));


/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsTunnelType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMplsTunnelLSRIdMapInfo ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMplsTunnelMode ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsTunnelMBBStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsTunnelDisJointType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsTunnelAttPointer ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OID_TYPE *));

INT1
nmhTestv2FsMplsTunnelEndToEndProtection ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsMplsTunnelPrConfigOperType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsTunnelSrlgType ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsTunnelIfIndex ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsTunnelInitReOptimize ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsTunnelIsProtectingLsp ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsTunnelBackupHopTableIndex ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsTunnelBackupPathInUse ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));


/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsTunnelTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMplsTpTunnelTable. */
INT1
nmhValidateIndexInstanceFsMplsTpTunnelTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMplsTpTunnelTable  */

INT1
nmhGetFirstIndexFsMplsTpTunnelTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsTpTunnelTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsTpTunnelDestTunnelIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

INT1
nmhGetFsMplsTpTunnelDestTunnelLspNum ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4 ,UINT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsTpTunnelDestTunnelIndex ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhSetFsMplsTpTunnelDestTunnelLspNum ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsTpTunnelDestTunnelIndex ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

INT1
nmhTestv2FsMplsTpTunnelDestTunnelLspNum ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  ,UINT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsTpTunnelTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsTunnelAttributeIndexNext ARG_LIST((UINT4 *));

/* Proto Validate Index Instance for FsTunnelAttributeTable. */
INT1
nmhValidateIndexInstanceFsTunnelAttributeTable ARG_LIST((UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsTunnelAttributeTable  */

INT1
nmhGetFirstIndexFsTunnelAttributeTable ARG_LIST((UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsTunnelAttributeTable ARG_LIST((UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsTunnelAttributeName ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsTunnelAttributeSetupPrio ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsTunnelAttributeHoldingPrio ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsTunnelAttributeIncludeAnyAffinity ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsTunnelAttributeIncludeAllAffinity ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsTunnelAttributeExcludeAnyAffinity ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsTunnelAttributeSessionAttributes ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsTunnelAttributeBandwidth ARG_LIST((UINT4 ,UINT4 *));

INT1
nmhGetFsTunnelAttributeTeClassType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsTunnelAttributeSrlgType ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsTunnelAttributeRowStatus ARG_LIST((UINT4 ,INT4 *));

INT1
nmhGetFsTunnelAttributeMask ARG_LIST((UINT4 ,tSNMP_OCTET_STRING_TYPE * ));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsTunnelAttributeName ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsTunnelAttributeSetupPrio ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsTunnelAttributeHoldingPrio ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsTunnelAttributeIncludeAnyAffinity ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsTunnelAttributeIncludeAllAffinity ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsTunnelAttributeExcludeAnyAffinity ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsTunnelAttributeSessionAttributes ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhSetFsTunnelAttributeBandwidth ARG_LIST((UINT4  ,UINT4 ));

INT1
nmhSetFsTunnelAttributeTeClassType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsTunnelAttributeSrlgType ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsTunnelAttributeRowStatus ARG_LIST((UINT4  ,INT4 ));

INT1
nmhSetFsTunnelAttributeMask ARG_LIST((UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsTunnelAttributeName ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsTunnelAttributeSetupPrio ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsTunnelAttributeHoldingPrio ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsTunnelAttributeIncludeAnyAffinity ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsTunnelAttributeIncludeAllAffinity ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsTunnelAttributeExcludeAnyAffinity ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsTunnelAttributeSessionAttributes ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

INT1
nmhTestv2FsTunnelAttributeBandwidth ARG_LIST((UINT4 *  ,UINT4  ,UINT4 ));

INT1
nmhTestv2FsTunnelAttributeTeClassType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsTunnelAttributeSrlgType ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsTunnelAttributeRowStatus ARG_LIST((UINT4 *  ,UINT4  ,INT4 ));

INT1
nmhTestv2FsTunnelAttributeMask ARG_LIST((UINT4 *  ,UINT4  ,tSNMP_OCTET_STRING_TYPE *));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsTunnelAttributeTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMplsTunnelSrlgTable. */
INT1
nmhValidateIndexInstanceFsMplsTunnelSrlgTable ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMplsTunnelSrlgTable  */

INT1
nmhGetFirstIndexFsMplsTunnelSrlgTable ARG_LIST((UINT4 * , UINT4 * , UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsTunnelSrlgTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsTunnelSrlgRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsTunnelSrlgRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsTunnelSrlgRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsTunnelSrlgTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsTunnelAttributeSrlgTable. */
INT1
nmhValidateIndexInstanceFsTunnelAttributeSrlgTable ARG_LIST((UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsTunnelAttributeSrlgTable  */

INT1
nmhGetFirstIndexFsTunnelAttributeSrlgTable ARG_LIST((UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsTunnelAttributeSrlgTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsTunnelAttributeSrlgRowStatus ARG_LIST((UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsTunnelAttributeSrlgRowStatus ARG_LIST((UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsTunnelAttributeSrlgRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsTunnelAttributeSrlgTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMplsTunnelHopTable. */
INT1
nmhValidateIndexInstanceFsMplsTunnelHopTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMplsTunnelHopTable  */

INT1
nmhGetFirstIndexFsMplsTunnelHopTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsTunnelHopTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsTunnelHopIncludeAny ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsTunnelHopIncludeAny ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsTunnelHopIncludeAny ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsTunnelHopTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsGmplsTunnelNotifyErrorTrapEnable ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsGmplsTunnelNotifyErrorTrapEnable ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsGmplsTunnelNotifyErrorTrapEnable ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsGmplsTunnelNotifyErrorTrapEnable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

/* Proto Validate Index Instance for FsMplsReoptimizationTunnelTable. */
INT1
nmhValidateIndexInstanceFsMplsReoptimizationTunnelTable ARG_LIST((UINT4  , UINT4  , UINT4 ));

/* Proto Type for Low Level GET FIRST fn for FsMplsReoptimizationTunnelTable  */

INT1
nmhGetFirstIndexFsMplsReoptimizationTunnelTable ARG_LIST((UINT4 * , UINT4 * , UINT4 *));

/* Proto type for GET_NEXT Routine.  */

INT1
nmhGetNextIndexFsMplsReoptimizationTunnelTable ARG_LIST((UINT4 , UINT4 * , UINT4 , UINT4 * , UINT4 , UINT4 *));

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsMplsReoptimizationTunnelStatus ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsReoptimizationTunnelManualTrigger ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

INT1
nmhGetFsMplsReoptimizationTunnelRowStatus ARG_LIST((UINT4  , UINT4  , UINT4 ,INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsMplsReoptimizationTunnelStatus ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsReoptimizationTunnelManualTrigger ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhSetFsMplsReoptimizationTunnelRowStatus ARG_LIST((UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsMplsReoptimizationTunnelStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsReoptimizationTunnelManualTrigger ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

INT1
nmhTestv2FsMplsReoptimizationTunnelRowStatus ARG_LIST((UINT4 *  ,UINT4  , UINT4  , UINT4  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsMplsReoptimizationTunnelTable ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

