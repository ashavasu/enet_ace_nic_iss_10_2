/* $Id: thlwrg.h,v 1.2 2010/11/17 07:00:13 siva Exp $ */
#ifndef _FSHLSPWR_H
#define _FSHLSPWR_H
INT4 GetNextIndexFsMplsLSPMapTunnelTable(tSnmpIndex *, tSnmpIndex *);

VOID RegisterFSHLSP(VOID);

VOID UnRegisterFSHLSP(VOID);
INT4 FsMplsLSPMaptunnelOperationGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLSPMaptunnelRowStatusGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLSPMaptunnelOperationSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLSPMaptunnelRowStatusSet(tSnmpIndex *, tRetVal *);
INT4 FsMplsLSPMaptunnelOperationTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsLSPMaptunnelRowStatusTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsMplsLSPMapTunnelTableDep(UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*);
INT4 GetNextIndexFsMplsHLSPTable(tSnmpIndex *, tSnmpIndex *);
INT4 FsMplsHLSPAvailableBWGet(tSnmpIndex *, tRetVal *);
INT4 FsMplsHLSPNoOfStackedTunnelsGet(tSnmpIndex *, tRetVal *);
#endif /* _FSHLSPWR_H */
