/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tetdfs.h,v 1.20 2016/02/03 10:37:59 siva Exp $
 *
 * Description: This file contains the structure type
 *              definitions declared and used in the TE Module.
 *******************************************************************/

#ifndef _TE_TDFS_H
#define _TE_TDFS_H
typedef FLT4 FLOAT;
typedef struct _TeCfgParams
{
   UINT4             u4MaxTnls;
   UINT4             u4MaxHopList;
   UINT4             u4MaxPOPerHopList;
   UINT4             u4MaxHopPerPO;
   UINT4             u4MaxTrfcParams;
   UINT4             u4MaxRPTETrfcParams;
   UINT4             u4MaxCRLDPTrfcParams;
   UINT4             u4MaxArHopList;
   UINT4             u4MaxArHopPerList;
   UINT4             u4MaxAttrList;  /* Maximum number of lsp attribute lists */

   /* The below variable denotes the maximum number of Computed Hop Lists 
    * in the system. */
   UINT4             u4MaxCHopList;

   /* he below variable denotes the maximum number of Hop in a Computed Hop 
    * List in the system. */
   UINT4             u4MaxCHopPerList;
   UINT4             u4NotificationMaxRate;
   UINT4    u4MaxReoptTnls;
   INT4              i4NotificationEnable;

   UINT1             u1TeAdminStatus;
   UINT1             u1TeAdminFlag;
   UINT2             u2MaxRpteErHops;
}
tTeCfgParams;

/* MPLS_P2MP_LSP_CHANGES - S */
typedef struct _TeP2mpParams 
{
   UINT4             u4P2mpTunnelCount;
   UINT4             u4P2mpActiveTunnelCount;
   UINT4             u4P2mpTunnelMaxHops;

   BOOLEAN           bP2mpTunnelNotification;
   UINT1             u1Pad[3];
}
tTeP2mpParams;
/* MPLS_P2MP_LSP_CHANGES - E */

/* DiffServ Configuration Structure */
typedef struct _TeDiffServCfgParams
{
   UINT4             u4MaxDfSrvELsps;
   UINT4             u4MaxDfSrvLLsps;
}
tTeDiffServCfgParams;

/* Global Structure associated with the DIFF-SERV-TE Module */
typedef struct _TeDiffServGlobalInfo {
   tTeDiffServCfgParams  TeDiffServCfgParams;
   tMplsDiffServElspList *pDiffServElspListEntry;
}
tTeDiffServGlobalInfo;

/* Global Structure associated with the TE Module */
typedef struct _TeGblInfo
{
   tRBTree               TnlInfoTbl;
   tTePathListInfo       *pPathListEntry;
   tTeArHopListInfo      *pArHopListEntry;

   /* The below variable denotes the pointer to the Computed Hop List. */
   tTeCHopListInfo       *pCHopListEntry;
   tTeTrfcParams         *pTrfcParams;
   tTeCfgParams          TeParams;
   UINT1                 *pTunnelIndexNext;
   tTeDiffServGlobalInfo TeDiffServGlobalInfo;  
   tTeP2mpParams         TeP2mpParams; /* MPLS_P2MP_LSP_CHANGES */

   /* The below variable denotes the pointer to the Atrribute List */
   tTeAttrListInfo       *pTeAttrListInfo;

   /* RB Tree to store the TnlInfo based on mplsTunnel Interface Index */
   tRBTree               TnlIfIndexBasedTbl;

   /* RB Tree to store the TnlInfo based on Message Id */
   tRBTree               TnlMsgIdBasedTbl;
   tRBTree      ReoptimizeTnlList;
}
tTeGblInfo;

typedef struct _TeCliArgs
{
    UINT4 u4TunnelIndex;
    UINT4 u4TunnelInstance;
    UINT4 u4IngressId;
    UINT4 u4EgressId;
    UINT4 u4IngressLocalMapNum;
    UINT4 u4EgressLocalMapNum;
    INT4  i4TnlMode;
    INT4  i4Gpid;
    UINT4 u4AdminStatus;
    UINT4 u4SrlgNo;
    INT4  i4SrlgType;
    UINT4 u4AttrListIndex;
    UINT4 u4PathListId;
    UINT4 u4PathOptionId;
    UINT4 u4HopIndex;
    UINT4 u4HopIpAddr;
    UINT4 u4HopUnnumIf;
    INT4  i4HopType;
    INT4  i4HopIncludeExclude;
    INT4  i4PathComp;
    INT4  i4SetupPrio;
    INT4  i4HoldingPrio;
    INT4  i4TeClassType;
    UINT4 u4TnlInstance;
    UINT4 u4ForwardLbl;
    UINT4 u4ReverseLbl;
    UINT4 u4Bandwidth;
    UINT4 u4IncludeAnyAffinity;
    UINT4 u4IncludeAllAffinity;
    UINT4 u4ExcludeAnyAffinity;
    INT4  i4EndToEndProtection;
    INT4  i4ProtnOperType;
    INT4  i4MbbStatus;
    INT4  i4ClassType;
    INT4  i4LspType;
    INT4  i4QosPolicy;
    INT4  i4TnlIfIndex;
    UINT4 u4ElspMapIndex;
    UINT4 u4ElspInfoIndex;
    UINT1 u1SwitchingType;
    UINT1 u1EncodingType;
    UINT1 u1TnlType;
    UINT1 u1TnlPathType;
    BOOL1 bTnlIfIndexFound;
    UINT1 au1Resv[3];
}tTeCliArgs;

typedef UINT4 tTeIp4Addr;

typedef struct _TnlCliArgs
{
  UINT4 u4TnlIndex;
  UINT4 u4TnlInstance;
  UINT4 u4IngressId;
  UINT4 u4EgressId;
  UINT4 u4MplsTnlIfIndex;
}tTnlCliArgs;

typedef struct _HlspTnlCliArgs
{
    tteFsMplsLSPMapTunnelTable teSetFsMplsLSPMapTunnelTable;
    tteIsSetFsMplsLSPMapTunnelTable teIsSetFsMplsLSPMapTunnelTable;
    UINT4 u4SetHlspTnlIndex;
    UINT4 u4SetHlspTnlInstance;
    UINT4 u4SetHlspTnlIngressId;
    UINT4 u4SetHlspTnlEgressId;
    UINT4 u4IsSetHlspTnlIndex;
    UINT4 u4IsSetHlspTnlInstance;
    UINT4 u4IsSetHlspTnlIngressId;
    UINT4 u4IsSetHlspTnlEgressId;
    UINT4 u4Operation;
    UINT4 u4RowStatus;
}tHlspTnlCliArgs;
#endif /*_TE_TDFS_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file tetdfs.h                               */
/*---------------------------------------------------------------------------*/

