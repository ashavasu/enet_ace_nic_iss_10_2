/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: tedbg.h,v 1.4 2013/12/07 10:58:56 siva Exp $
*
* Description: This file contains the routines for the protocol Database Access for the module Te
*********************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : tedbg.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : TE
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Contains all the definitions required 
 *                             for Debugging/Dumping in TE.
 *---------------------------------------------------------------------------*/

#ifndef _TE_DBG_H
#define _TE_DBG_H
#include "utltrc.h"

/* Macros used in TE Debugging */
#define     TE_DBG_FLAG     gu4TeDbgFlag
#define     TE_DBG_LVL     gu4TeDbgLvl 

#define     TE_DEF_DBG_FLAG 0x00000000
#define     TE_ALL          0xffffffff

/* Values assigned to the sub-modules of TE, for Logging purpose */
#define     ENTRY_AND_EXIT       0xf0000000

#define     TE_MAIN_PRCS         0x00001001
#define     TE_MAIN_FAIL         0x00001002
#define     TE_MAIN_MEM          0x00001004
#define     TE_MAIN_SNMP         0x00001008
#define     TE_MAIN_ALL          0x0000100f

/* Debug values set for the debug support in low level routines */
#define     TE_LLVL_PRCS         0x00002001
#define     TE_LLVL_FAIL         0x00002002
#define     TE_LLVL_SET_SCSS     0x00002004
#define     TE_LLVL_SET_FAIL     0x00002008
#define     TE_LLVL_GET_SCSS     0x00002010
#define     TE_LLVL_GET_FAIL     0x00002020
#define     TE_LLVL_TEST_SCSS    0x00002040
#define     TE_LLVL_TEST_FAIL    0x00002080
#define     TE_LLVL_ALL          0x000020ff

/* Debug values set for the debug support in External modules */
#define     TE_EXTN_PRCS     0x00004001
#define     TE_EXTN_FAIL     0x00004002
#define     TE_EXTN_ALL      0x0000400f

/* Debug values set for the debug support in DiffServ support */
#define     TE_DIFF_PRCS     0x00008001
#define     TE_DIFF_FAIL     0x00008002
#define     TE_DIFF_ALL      0x0000800f

/* Module Specfic Entry and Exit  */
#define     TE_MAIN_ETEXT     (ENTRY_AND_EXIT | TE_MAIN_ALL)
#define     TE_LLVL_ETEXT     (ENTRY_AND_EXIT | TE_LLVL_ALL)
#define     TE_EXTN_ETEXT     (ENTRY_AND_EXIT | TE_EXTN_ALL)
#define     TE_DIFF_ETEXT     (ENTRY_AND_EXIT | TE_DIFF_ALL)

#define TE_DBG(u4Value, pu1Format)     \
           if((u4Value) == (u4Value & TE_DBG_FLAG)) \
              UtlTrcLog  (TE_DBG_FLAG, u4Value, "TE", pu1Format)

#define TE_DBG1(u4Value, pu1Format, Arg1)     \
           if((u4Value) == (u4Value & TE_DBG_FLAG)) \
              UtlTrcLog  (TE_DBG_FLAG, u4Value, "TE", pu1Format, Arg1)

#define TE_DBG2(u4Value, pu1Format, Arg1, Arg2)     \
           if((u4Value) == (u4Value & TE_DBG_FLAG)) \
              UtlTrcLog  (TE_DBG_FLAG, u4Value, "TE", pu1Format, Arg1, Arg2)

#define TE_DBG3(u4Value, pu1Format, Arg1, Arg2, Arg3)     \
           if((u4Value) == (u4Value & TE_DBG_FLAG)) \
             UtlTrcLog  (TE_DBG_FLAG, u4Value, "TE", pu1Format, Arg1, Arg2, Arg3)

#define TE_DBG4(u4Value, pu1Format, Arg1, Arg2, Arg3, Arg4)     \
           if((u4Value) == (u4Value & TE_DBG_FLAG)) \
             UtlTrcLog  (TE_DBG_FLAG, u4Value, "TE", pu1Format, Arg1, Arg2, Arg3, Arg4)

#define TE_DBG5(u4Value, pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5)   \
           if((u4Value) == (u4Value & TE_DBG_FLAG)) \
             UtlTrcLog  (TE_DBG_FLAG, u4Value, "TE", pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5)

#define TE_DBG6(u4Value, pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)  \
           if((u4Value) == (u4Value & TE_DBG_FLAG)) \
             UtlTrcLog  (TE_DBG_FLAG, u4Value, "TE", pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5, Arg6)

#endif /*_TE_DBG_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file tedbg.h                                */
/*---------------------------------------------------------------------------*/
