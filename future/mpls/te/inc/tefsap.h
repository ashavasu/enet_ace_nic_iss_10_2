/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tefsap.h,v 1.5 2010/09/21 07:36:22 prabuc Exp $
 *
 * Description: Contains macros of FSAP library.
 *******************************************************************/

#ifndef _TE_FSAP_H
#define _TE_FSAP_H

#define TE_INIT_POOL_MNGR       MemInitMemPool
#define TE_CREATE_POOL          MemCreateMemPool
#define TE_DELETE_POOL          MemDeleteMemPool
#define TE_ALLOC_MEM_BLOCK      MemAllocMemBlk
#define TE_REL_MEM_BLOCK        MemReleaseMemBlock

#define TE_CRU_SUCCESS          CRU_SUCCESS
#define TE_CRU_FAILURE          CRU_FAILURE
#define TE_TMR_SUCCESS          TMR_SUCCESS
#define TE_TMR_FAILURE          TMR_FAILURE
#define TE_MEM_SUCCESS          MEM_SUCCESS
#define TE_MEM_FAILURE          MEM_FAILURE

#define TE_MEM_MALLOC           MEM_MALLOC
#define TE_MEM_FREE             MEM_FREE

/***** UTL related ****************/
/* UTL DLL related */
#define TE_DLL_INIT             TMO_DLL_Init
/* UTL SLL related */
#define TE_SLL_INIT             TMO_SLL_Init
#define TE_SLL_INIT_NODE        TMO_SLL_Init_Node
#define TE_SLL_ADD              TMO_SLL_Add
#define TE_SLL_INSERT           TMO_SLL_Insert
#define TE_SLL_FIRST            TMO_SLL_First
#define TE_SLL_NEXT             TMO_SLL_Next
#define TE_SLL_COUNT            TMO_SLL_Count
#define TE_SLL_SCAN             TMO_SLL_Scan
#define TE_SLL_DELETE           TMO_SLL_Delete

#define TE_HTONS                OSIX_HTONS
#define TE_NTOHS                OSIX_NTOHS
#define TE_HTONL                OSIX_HTONL
#define TE_NTOHL                OSIX_NTOHL

#define TE_INET_NTOP4(src,dst)  INET_NTOP (AF_INET, (const void *)src, \
                                              (char *)dst, TE_INET_ADDRSTRLEN)

#endif /*_TE_FSAP_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file tefsap.h                               */
/*---------------------------------------------------------------------------*/

