/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: stdtedb.h,v 1.4 2008/08/20 15:16:09 iss Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _STDTEDB_H
#define _STDTEDB_H

UINT1 MplsTunnelTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 MplsTunnelHopTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 MplsTunnelResourceTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};
UINT1 MplsTunnelARHopTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 MplsTunnelCHopTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 MplsTunnelPerfTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32 ,SNMP_DATA_TYPE_UNSIGNED32};
UINT1 MplsTunnelCRLDPResTableINDEX [] = {SNMP_DATA_TYPE_UNSIGNED32};

UINT4 stdte [] ={1,3,6,1,2,1,10,166,3};
tSNMP_OID_TYPE stdteOID = {9, stdte};


UINT4 MplsTunnelConfigured [ ] ={1,3,6,1,2,1,10,166,3,1,1};
UINT4 MplsTunnelActive [ ] ={1,3,6,1,2,1,10,166,3,1,2};
UINT4 MplsTunnelTEDistProto [ ] ={1,3,6,1,2,1,10,166,3,1,3};
UINT4 MplsTunnelMaxHops [ ] ={1,3,6,1,2,1,10,166,3,1,4};
UINT4 MplsTunnelNotificationMaxRate [ ] ={1,3,6,1,2,1,10,166,3,1,5};
UINT4 MplsTunnelIndexNext [ ] ={1,3,6,1,2,1,10,166,3,2,1};
UINT4 MplsTunnelIndex [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,1};
UINT4 MplsTunnelInstance [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,2};
UINT4 MplsTunnelIngressLSRId [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,3};
UINT4 MplsTunnelEgressLSRId [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,4};
UINT4 MplsTunnelName [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,5};
UINT4 MplsTunnelDescr [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,6};
UINT4 MplsTunnelIsIf [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,7};
UINT4 MplsTunnelIfIndex [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,8};
UINT4 MplsTunnelOwner [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,9};
UINT4 MplsTunnelRole [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,10};
UINT4 MplsTunnelXCPointer [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,11};
UINT4 MplsTunnelSignallingProto [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,12};
UINT4 MplsTunnelSetupPrio [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,13};
UINT4 MplsTunnelHoldingPrio [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,14};
UINT4 MplsTunnelSessionAttributes [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,15};
UINT4 MplsTunnelLocalProtectInUse [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,16};
UINT4 MplsTunnelResourcePointer [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,17};
UINT4 MplsTunnelPrimaryInstance [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,18};
UINT4 MplsTunnelInstancePriority [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,19};
UINT4 MplsTunnelHopTableIndex [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,20};
UINT4 MplsTunnelPathInUse [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,21};
UINT4 MplsTunnelARHopTableIndex [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,22};
UINT4 MplsTunnelCHopTableIndex [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,23};
UINT4 MplsTunnelIncludeAnyAffinity [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,24};
UINT4 MplsTunnelIncludeAllAffinity [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,25};
UINT4 MplsTunnelExcludeAnyAffinity [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,26};
UINT4 MplsTunnelTotalUpTime [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,27};
UINT4 MplsTunnelInstanceUpTime [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,28};
UINT4 MplsTunnelPrimaryUpTime [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,29};
UINT4 MplsTunnelPathChanges [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,30};
UINT4 MplsTunnelLastPathChange [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,31};
UINT4 MplsTunnelCreationTime [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,32};
UINT4 MplsTunnelStateTransitions [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,33};
UINT4 MplsTunnelAdminStatus [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,34};
UINT4 MplsTunnelOperStatus [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,35};
UINT4 MplsTunnelRowStatus [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,36};
UINT4 MplsTunnelStorageType [ ] ={1,3,6,1,2,1,10,166,3,2,2,1,37};
UINT4 MplsTunnelHopListIndexNext [ ] ={1,3,6,1,2,1,10,166,3,2,3};
UINT4 MplsTunnelHopListIndex [ ] ={1,3,6,1,2,1,10,166,3,2,4,1,1};
UINT4 MplsTunnelHopPathOptionIndex [ ] ={1,3,6,1,2,1,10,166,3,2,4,1,2};
UINT4 MplsTunnelHopIndex [ ] ={1,3,6,1,2,1,10,166,3,2,4,1,3};
UINT4 MplsTunnelHopAddrType [ ] ={1,3,6,1,2,1,10,166,3,2,4,1,4};
UINT4 MplsTunnelHopIpAddr [ ] ={1,3,6,1,2,1,10,166,3,2,4,1,5};
UINT4 MplsTunnelHopIpPrefixLen [ ] ={1,3,6,1,2,1,10,166,3,2,4,1,6};
UINT4 MplsTunnelHopAsNumber [ ] ={1,3,6,1,2,1,10,166,3,2,4,1,7};
UINT4 MplsTunnelHopAddrUnnum [ ] ={1,3,6,1,2,1,10,166,3,2,4,1,8};
UINT4 MplsTunnelHopLspId [ ] ={1,3,6,1,2,1,10,166,3,2,4,1,9};
UINT4 MplsTunnelHopType [ ] ={1,3,6,1,2,1,10,166,3,2,4,1,10};
UINT4 MplsTunnelHopInclude [ ] ={1,3,6,1,2,1,10,166,3,2,4,1,11};
UINT4 MplsTunnelHopPathOptionName [ ] ={1,3,6,1,2,1,10,166,3,2,4,1,12};
UINT4 MplsTunnelHopEntryPathComp [ ] ={1,3,6,1,2,1,10,166,3,2,4,1,13};
UINT4 MplsTunnelHopRowStatus [ ] ={1,3,6,1,2,1,10,166,3,2,4,1,14};
UINT4 MplsTunnelHopStorageType [ ] ={1,3,6,1,2,1,10,166,3,2,4,1,15};
UINT4 MplsTunnelResourceIndexNext [ ] ={1,3,6,1,2,1,10,166,3,2,5};
UINT4 MplsTunnelResourceIndex [ ] ={1,3,6,1,2,1,10,166,3,2,6,1,1};
UINT4 MplsTunnelResourceMaxRate [ ] ={1,3,6,1,2,1,10,166,3,2,6,1,2};
UINT4 MplsTunnelResourceMeanRate [ ] ={1,3,6,1,2,1,10,166,3,2,6,1,3};
UINT4 MplsTunnelResourceMaxBurstSize [ ] ={1,3,6,1,2,1,10,166,3,2,6,1,4};
UINT4 MplsTunnelResourceMeanBurstSize [ ] ={1,3,6,1,2,1,10,166,3,2,6,1,5};
UINT4 MplsTunnelResourceExBurstSize [ ] ={1,3,6,1,2,1,10,166,3,2,6,1,6};
UINT4 MplsTunnelResourceFrequency [ ] ={1,3,6,1,2,1,10,166,3,2,6,1,7};
UINT4 MplsTunnelResourceWeight [ ] ={1,3,6,1,2,1,10,166,3,2,6,1,8};
UINT4 MplsTunnelResourceRowStatus [ ] ={1,3,6,1,2,1,10,166,3,2,6,1,9};
UINT4 MplsTunnelResourceStorageType [ ] ={1,3,6,1,2,1,10,166,3,2,6,1,10};
UINT4 MplsTunnelARHopListIndex [ ] ={1,3,6,1,2,1,10,166,3,2,7,1,1};
UINT4 MplsTunnelARHopIndex [ ] ={1,3,6,1,2,1,10,166,3,2,7,1,2};
UINT4 MplsTunnelARHopAddrType [ ] ={1,3,6,1,2,1,10,166,3,2,7,1,3};
UINT4 MplsTunnelARHopIpAddr [ ] ={1,3,6,1,2,1,10,166,3,2,7,1,4};
UINT4 MplsTunnelARHopAddrUnnum [ ] ={1,3,6,1,2,1,10,166,3,2,7,1,5};
UINT4 MplsTunnelARHopLspId [ ] ={1,3,6,1,2,1,10,166,3,2,7,1,6};
UINT4 MplsTunnelCHopListIndex [ ] ={1,3,6,1,2,1,10,166,3,2,8,1,1};
UINT4 MplsTunnelCHopIndex [ ] ={1,3,6,1,2,1,10,166,3,2,8,1,2};
UINT4 MplsTunnelCHopAddrType [ ] ={1,3,6,1,2,1,10,166,3,2,8,1,3};
UINT4 MplsTunnelCHopIpAddr [ ] ={1,3,6,1,2,1,10,166,3,2,8,1,4};
UINT4 MplsTunnelCHopIpPrefixLen [ ] ={1,3,6,1,2,1,10,166,3,2,8,1,5};
UINT4 MplsTunnelCHopAsNumber [ ] ={1,3,6,1,2,1,10,166,3,2,8,1,6};
UINT4 MplsTunnelCHopAddrUnnum [ ] ={1,3,6,1,2,1,10,166,3,2,8,1,7};
UINT4 MplsTunnelCHopLspId [ ] ={1,3,6,1,2,1,10,166,3,2,8,1,8};
UINT4 MplsTunnelCHopType [ ] ={1,3,6,1,2,1,10,166,3,2,8,1,9};
UINT4 MplsTunnelPerfPackets [ ] ={1,3,6,1,2,1,10,166,3,2,9,1,1};
UINT4 MplsTunnelPerfHCPackets [ ] ={1,3,6,1,2,1,10,166,3,2,9,1,2};
UINT4 MplsTunnelPerfErrors [ ] ={1,3,6,1,2,1,10,166,3,2,9,1,3};
UINT4 MplsTunnelPerfBytes [ ] ={1,3,6,1,2,1,10,166,3,2,9,1,4};
UINT4 MplsTunnelPerfHCBytes [ ] ={1,3,6,1,2,1,10,166,3,2,9,1,5};
UINT4 MplsTunnelCRLDPResMeanBurstSize [ ] ={1,3,6,1,2,1,10,166,3,2,10,1,1};
UINT4 MplsTunnelCRLDPResExBurstSize [ ] ={1,3,6,1,2,1,10,166,3,2,10,1,2};
UINT4 MplsTunnelCRLDPResFrequency [ ] ={1,3,6,1,2,1,10,166,3,2,10,1,3};
UINT4 MplsTunnelCRLDPResWeight [ ] ={1,3,6,1,2,1,10,166,3,2,10,1,4};
UINT4 MplsTunnelCRLDPResFlags [ ] ={1,3,6,1,2,1,10,166,3,2,10,1,5};
UINT4 MplsTunnelCRLDPResRowStatus [ ] ={1,3,6,1,2,1,10,166,3,2,10,1,6};
UINT4 MplsTunnelCRLDPResStorageType [ ] ={1,3,6,1,2,1,10,166,3,2,10,1,7};
UINT4 MplsTunnelNotificationEnable [ ] ={1,3,6,1,2,1,10,166,3,2,11};


tMbDbEntry stdteMibEntry[]= {

{{11,MplsTunnelConfigured}, NULL, MplsTunnelConfiguredGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,MplsTunnelActive}, NULL, MplsTunnelActiveGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,MplsTunnelTEDistProto}, NULL, MplsTunnelTEDistProtoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,MplsTunnelMaxHops}, NULL, MplsTunnelMaxHopsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{11,MplsTunnelNotificationMaxRate}, NULL, MplsTunnelNotificationMaxRateGet, MplsTunnelNotificationMaxRateSet, MplsTunnelNotificationMaxRateTest, MplsTunnelNotificationMaxRateDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,MplsTunnelIndexNext}, NULL, MplsTunnelIndexNextGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,MplsTunnelIndex}, GetNextIndexMplsTunnelTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, MplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,MplsTunnelInstance}, GetNextIndexMplsTunnelTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, MplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,MplsTunnelIngressLSRId}, GetNextIndexMplsTunnelTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, MplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,MplsTunnelEgressLSRId}, GetNextIndexMplsTunnelTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, MplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,MplsTunnelName}, GetNextIndexMplsTunnelTable, MplsTunnelNameGet, MplsTunnelNameSet, MplsTunnelNameTest, MplsTunnelTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, MplsTunnelTableINDEX, 4, 0, 0, ""},

{{13,MplsTunnelDescr}, GetNextIndexMplsTunnelTable, MplsTunnelDescrGet, MplsTunnelDescrSet, MplsTunnelDescrTest, MplsTunnelTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, MplsTunnelTableINDEX, 4, 0, 0, ""},

{{13,MplsTunnelIsIf}, GetNextIndexMplsTunnelTable, MplsTunnelIsIfGet, MplsTunnelIsIfSet, MplsTunnelIsIfTest, MplsTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsTunnelTableINDEX, 4, 0, 0, "2"},

{{13,MplsTunnelIfIndex}, GetNextIndexMplsTunnelTable, MplsTunnelIfIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, MplsTunnelTableINDEX, 4, 0, 0, "0"},

{{13,MplsTunnelOwner}, GetNextIndexMplsTunnelTable, MplsTunnelOwnerGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, MplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,MplsTunnelRole}, GetNextIndexMplsTunnelTable, MplsTunnelRoleGet, MplsTunnelRoleSet, MplsTunnelRoleTest, MplsTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsTunnelTableINDEX, 4, 0, 0, "1"},

{{13,MplsTunnelXCPointer}, GetNextIndexMplsTunnelTable, MplsTunnelXCPointerGet, MplsTunnelXCPointerSet, MplsTunnelXCPointerTest, MplsTunnelTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, MplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,MplsTunnelSignallingProto}, GetNextIndexMplsTunnelTable, MplsTunnelSignallingProtoGet, MplsTunnelSignallingProtoSet, MplsTunnelSignallingProtoTest, MplsTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsTunnelTableINDEX, 4, 0, 0, "1"},

{{13,MplsTunnelSetupPrio}, GetNextIndexMplsTunnelTable, MplsTunnelSetupPrioGet, MplsTunnelSetupPrioSet, MplsTunnelSetupPrioTest, MplsTunnelTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, MplsTunnelTableINDEX, 4, 0, 0, "0"},

{{13,MplsTunnelHoldingPrio}, GetNextIndexMplsTunnelTable, MplsTunnelHoldingPrioGet, MplsTunnelHoldingPrioSet, MplsTunnelHoldingPrioTest, MplsTunnelTableDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, MplsTunnelTableINDEX, 4, 0, 0, "0"},

{{13,MplsTunnelSessionAttributes}, GetNextIndexMplsTunnelTable, MplsTunnelSessionAttributesGet, MplsTunnelSessionAttributesSet, MplsTunnelSessionAttributesTest, MplsTunnelTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, MplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,MplsTunnelLocalProtectInUse}, GetNextIndexMplsTunnelTable, MplsTunnelLocalProtectInUseGet, MplsTunnelLocalProtectInUseSet, MplsTunnelLocalProtectInUseTest, MplsTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsTunnelTableINDEX, 4, 0, 0, "2"},

{{13,MplsTunnelResourcePointer}, GetNextIndexMplsTunnelTable, MplsTunnelResourcePointerGet, MplsTunnelResourcePointerSet, MplsTunnelResourcePointerTest, MplsTunnelTableDep, SNMP_DATA_TYPE_OBJECT_ID, SNMP_READWRITE, MplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,MplsTunnelPrimaryInstance}, GetNextIndexMplsTunnelTable, MplsTunnelPrimaryInstanceGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, MplsTunnelTableINDEX, 4, 0, 0, "0"},

{{13,MplsTunnelInstancePriority}, GetNextIndexMplsTunnelTable, MplsTunnelInstancePriorityGet, MplsTunnelInstancePrioritySet, MplsTunnelInstancePriorityTest, MplsTunnelTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsTunnelTableINDEX, 4, 0, 0, "0"},

{{13,MplsTunnelHopTableIndex}, GetNextIndexMplsTunnelTable, MplsTunnelHopTableIndexGet, MplsTunnelHopTableIndexSet, MplsTunnelHopTableIndexTest, MplsTunnelTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsTunnelTableINDEX, 4, 0, 0, "0"},

{{13,MplsTunnelPathInUse}, GetNextIndexMplsTunnelTable, MplsTunnelPathInUseGet, MplsTunnelPathInUseSet, MplsTunnelPathInUseTest, MplsTunnelTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsTunnelTableINDEX, 4, 0, 0, "0"},

{{13,MplsTunnelARHopTableIndex}, GetNextIndexMplsTunnelTable, MplsTunnelARHopTableIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, MplsTunnelTableINDEX, 4, 0, 0, "0"},

{{13,MplsTunnelCHopTableIndex}, GetNextIndexMplsTunnelTable, MplsTunnelCHopTableIndexGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, MplsTunnelTableINDEX, 4, 0, 0, "0"},

{{13,MplsTunnelIncludeAnyAffinity}, GetNextIndexMplsTunnelTable, MplsTunnelIncludeAnyAffinityGet, MplsTunnelIncludeAnyAffinitySet, MplsTunnelIncludeAnyAffinityTest, MplsTunnelTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,MplsTunnelIncludeAllAffinity}, GetNextIndexMplsTunnelTable, MplsTunnelIncludeAllAffinityGet, MplsTunnelIncludeAllAffinitySet, MplsTunnelIncludeAllAffinityTest, MplsTunnelTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,MplsTunnelExcludeAnyAffinity}, GetNextIndexMplsTunnelTable, MplsTunnelExcludeAnyAffinityGet, MplsTunnelExcludeAnyAffinitySet, MplsTunnelExcludeAnyAffinityTest, MplsTunnelTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsTunnelTableINDEX, 4, 0, 0, "0"},

{{13,MplsTunnelTotalUpTime}, GetNextIndexMplsTunnelTable, MplsTunnelTotalUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,MplsTunnelInstanceUpTime}, GetNextIndexMplsTunnelTable, MplsTunnelInstanceUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,MplsTunnelPrimaryUpTime}, GetNextIndexMplsTunnelTable, MplsTunnelPrimaryUpTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,MplsTunnelPathChanges}, GetNextIndexMplsTunnelTable, MplsTunnelPathChangesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,MplsTunnelLastPathChange}, GetNextIndexMplsTunnelTable, MplsTunnelLastPathChangeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,MplsTunnelCreationTime}, GetNextIndexMplsTunnelTable, MplsTunnelCreationTimeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_TIME_TICKS, SNMP_READONLY, MplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,MplsTunnelStateTransitions}, GetNextIndexMplsTunnelTable, MplsTunnelStateTransitionsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,MplsTunnelAdminStatus}, GetNextIndexMplsTunnelTable, MplsTunnelAdminStatusGet, MplsTunnelAdminStatusSet, MplsTunnelAdminStatusTest, MplsTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,MplsTunnelOperStatus}, GetNextIndexMplsTunnelTable, MplsTunnelOperStatusGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, MplsTunnelTableINDEX, 4, 0, 0, NULL},

{{13,MplsTunnelRowStatus}, GetNextIndexMplsTunnelTable, MplsTunnelRowStatusGet, MplsTunnelRowStatusSet, MplsTunnelRowStatusTest, MplsTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsTunnelTableINDEX, 4, 0, 1, NULL},

{{13,MplsTunnelStorageType}, GetNextIndexMplsTunnelTable, MplsTunnelStorageTypeGet, MplsTunnelStorageTypeSet, MplsTunnelStorageTypeTest, MplsTunnelTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsTunnelTableINDEX, 4, 0, 0, "2"},

{{11,MplsTunnelHopListIndexNext}, NULL, MplsTunnelHopListIndexNextGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,MplsTunnelHopListIndex}, GetNextIndexMplsTunnelHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, MplsTunnelHopTableINDEX, 3, 0, 0, NULL},

{{13,MplsTunnelHopPathOptionIndex}, GetNextIndexMplsTunnelHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, MplsTunnelHopTableINDEX, 3, 0, 0, NULL},

{{13,MplsTunnelHopIndex}, GetNextIndexMplsTunnelHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, MplsTunnelHopTableINDEX, 3, 0, 0, NULL},

{{13,MplsTunnelHopAddrType}, GetNextIndexMplsTunnelHopTable, MplsTunnelHopAddrTypeGet, MplsTunnelHopAddrTypeSet, MplsTunnelHopAddrTypeTest, MplsTunnelHopTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsTunnelHopTableINDEX, 3, 0, 0, NULL},

{{13,MplsTunnelHopIpAddr}, GetNextIndexMplsTunnelHopTable, MplsTunnelHopIpAddrGet, MplsTunnelHopIpAddrSet, MplsTunnelHopIpAddrTest, MplsTunnelHopTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, MplsTunnelHopTableINDEX, 3, 0, 0, "0"},

{{13,MplsTunnelHopIpPrefixLen}, GetNextIndexMplsTunnelHopTable, MplsTunnelHopIpPrefixLenGet, MplsTunnelHopIpPrefixLenSet, MplsTunnelHopIpPrefixLenTest, MplsTunnelHopTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsTunnelHopTableINDEX, 3, 0, 0, "32"},

{{13,MplsTunnelHopAsNumber}, GetNextIndexMplsTunnelHopTable, MplsTunnelHopAsNumberGet, MplsTunnelHopAsNumberSet, MplsTunnelHopAsNumberTest, MplsTunnelHopTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, MplsTunnelHopTableINDEX, 3, 0, 0, NULL},

{{13,MplsTunnelHopAddrUnnum}, GetNextIndexMplsTunnelHopTable, MplsTunnelHopAddrUnnumGet, MplsTunnelHopAddrUnnumSet, MplsTunnelHopAddrUnnumTest, MplsTunnelHopTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, MplsTunnelHopTableINDEX, 3, 0, 0, NULL},

{{13,MplsTunnelHopLspId}, GetNextIndexMplsTunnelHopTable, MplsTunnelHopLspIdGet, MplsTunnelHopLspIdSet, MplsTunnelHopLspIdTest, MplsTunnelHopTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, MplsTunnelHopTableINDEX, 3, 0, 0, NULL},

{{13,MplsTunnelHopType}, GetNextIndexMplsTunnelHopTable, MplsTunnelHopTypeGet, MplsTunnelHopTypeSet, MplsTunnelHopTypeTest, MplsTunnelHopTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsTunnelHopTableINDEX, 3, 0, 0, NULL},

{{13,MplsTunnelHopInclude}, GetNextIndexMplsTunnelHopTable, MplsTunnelHopIncludeGet, MplsTunnelHopIncludeSet, MplsTunnelHopIncludeTest, MplsTunnelHopTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsTunnelHopTableINDEX, 3, 0, 0, "1"},

{{13,MplsTunnelHopPathOptionName}, GetNextIndexMplsTunnelHopTable, MplsTunnelHopPathOptionNameGet, MplsTunnelHopPathOptionNameSet, MplsTunnelHopPathOptionNameTest, MplsTunnelHopTableDep, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READWRITE, MplsTunnelHopTableINDEX, 3, 0, 0, NULL},

{{13,MplsTunnelHopEntryPathComp}, GetNextIndexMplsTunnelHopTable, MplsTunnelHopEntryPathCompGet, MplsTunnelHopEntryPathCompSet, MplsTunnelHopEntryPathCompTest, MplsTunnelHopTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsTunnelHopTableINDEX, 3, 0, 0, NULL},

{{13,MplsTunnelHopRowStatus}, GetNextIndexMplsTunnelHopTable, MplsTunnelHopRowStatusGet, MplsTunnelHopRowStatusSet, MplsTunnelHopRowStatusTest, MplsTunnelHopTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsTunnelHopTableINDEX, 3, 0, 1, NULL},

{{13,MplsTunnelHopStorageType}, GetNextIndexMplsTunnelHopTable, MplsTunnelHopStorageTypeGet, MplsTunnelHopStorageTypeSet, MplsTunnelHopStorageTypeTest, MplsTunnelHopTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsTunnelHopTableINDEX, 3, 0, 0, "2"},

{{11,MplsTunnelResourceIndexNext}, NULL, MplsTunnelResourceIndexNextGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, NULL, 0, 0, 0, NULL},

{{13,MplsTunnelResourceIndex}, GetNextIndexMplsTunnelResourceTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, MplsTunnelResourceTableINDEX, 1, 0, 0, NULL},

{{13,MplsTunnelResourceMaxRate}, GetNextIndexMplsTunnelResourceTable, MplsTunnelResourceMaxRateGet, MplsTunnelResourceMaxRateSet, MplsTunnelResourceMaxRateTest, MplsTunnelResourceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsTunnelResourceTableINDEX, 1, 0, 0, NULL},

{{13,MplsTunnelResourceMeanRate}, GetNextIndexMplsTunnelResourceTable, MplsTunnelResourceMeanRateGet, MplsTunnelResourceMeanRateSet, MplsTunnelResourceMeanRateTest, MplsTunnelResourceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsTunnelResourceTableINDEX, 1, 0, 0, NULL},

{{13,MplsTunnelResourceMaxBurstSize}, GetNextIndexMplsTunnelResourceTable, MplsTunnelResourceMaxBurstSizeGet, MplsTunnelResourceMaxBurstSizeSet, MplsTunnelResourceMaxBurstSizeTest, MplsTunnelResourceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsTunnelResourceTableINDEX, 1, 0, 0, NULL},

{{13,MplsTunnelResourceMeanBurstSize}, GetNextIndexMplsTunnelResourceTable, MplsTunnelResourceMeanBurstSizeGet, MplsTunnelResourceMeanBurstSizeSet, MplsTunnelResourceMeanBurstSizeTest, MplsTunnelResourceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsTunnelResourceTableINDEX, 1, 0, 0, NULL},

{{13,MplsTunnelResourceExBurstSize}, GetNextIndexMplsTunnelResourceTable, MplsTunnelResourceExBurstSizeGet, MplsTunnelResourceExBurstSizeSet, MplsTunnelResourceExBurstSizeTest, MplsTunnelResourceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsTunnelResourceTableINDEX, 1, 0, 0, NULL},

{{13,MplsTunnelResourceFrequency}, GetNextIndexMplsTunnelResourceTable, MplsTunnelResourceFrequencyGet, MplsTunnelResourceFrequencySet, MplsTunnelResourceFrequencyTest, MplsTunnelResourceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsTunnelResourceTableINDEX, 1, 0, 0, NULL},

{{13,MplsTunnelResourceWeight}, GetNextIndexMplsTunnelResourceTable, MplsTunnelResourceWeightGet, MplsTunnelResourceWeightSet, MplsTunnelResourceWeightTest, MplsTunnelResourceTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsTunnelResourceTableINDEX, 1, 0, 0, NULL},

{{13,MplsTunnelResourceRowStatus}, GetNextIndexMplsTunnelResourceTable, MplsTunnelResourceRowStatusGet, MplsTunnelResourceRowStatusSet, MplsTunnelResourceRowStatusTest, MplsTunnelResourceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsTunnelResourceTableINDEX, 1, 0, 1, NULL},

{{13,MplsTunnelResourceStorageType}, GetNextIndexMplsTunnelResourceTable, MplsTunnelResourceStorageTypeGet, MplsTunnelResourceStorageTypeSet, MplsTunnelResourceStorageTypeTest, MplsTunnelResourceTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsTunnelResourceTableINDEX, 1, 0, 0, "2"},

{{13,MplsTunnelARHopListIndex}, GetNextIndexMplsTunnelARHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, MplsTunnelARHopTableINDEX, 2, 0, 0, NULL},

{{13,MplsTunnelARHopIndex}, GetNextIndexMplsTunnelARHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, MplsTunnelARHopTableINDEX, 2, 0, 0, NULL},

{{13,MplsTunnelARHopAddrType}, GetNextIndexMplsTunnelARHopTable, MplsTunnelARHopAddrTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, MplsTunnelARHopTableINDEX, 2, 0, 0, NULL},

{{13,MplsTunnelARHopIpAddr}, GetNextIndexMplsTunnelARHopTable, MplsTunnelARHopIpAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, MplsTunnelARHopTableINDEX, 2, 0, 0, "0"},

{{13,MplsTunnelARHopAddrUnnum}, GetNextIndexMplsTunnelARHopTable, MplsTunnelARHopAddrUnnumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, MplsTunnelARHopTableINDEX, 2, 0, 0, NULL},

{{13,MplsTunnelARHopLspId}, GetNextIndexMplsTunnelARHopTable, MplsTunnelARHopLspIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, MplsTunnelARHopTableINDEX, 2, 0, 0, NULL},

{{13,MplsTunnelCHopListIndex}, GetNextIndexMplsTunnelCHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, MplsTunnelCHopTableINDEX, 2, 0, 0, NULL},

{{13,MplsTunnelCHopIndex}, GetNextIndexMplsTunnelCHopTable, NULL, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_NOACCESS, MplsTunnelCHopTableINDEX, 2, 0, 0, NULL},

{{13,MplsTunnelCHopAddrType}, GetNextIndexMplsTunnelCHopTable, MplsTunnelCHopAddrTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, MplsTunnelCHopTableINDEX, 2, 0, 0, NULL},

{{13,MplsTunnelCHopIpAddr}, GetNextIndexMplsTunnelCHopTable, MplsTunnelCHopIpAddrGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, MplsTunnelCHopTableINDEX, 2, 0, 0, "0"},

{{13,MplsTunnelCHopIpPrefixLen}, GetNextIndexMplsTunnelCHopTable, MplsTunnelCHopIpPrefixLenGet, NULL, NULL, NULL, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READONLY, MplsTunnelCHopTableINDEX, 2, 0, 0, "32"},

{{13,MplsTunnelCHopAsNumber}, GetNextIndexMplsTunnelCHopTable, MplsTunnelCHopAsNumberGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, MplsTunnelCHopTableINDEX, 2, 0, 0, NULL},

{{13,MplsTunnelCHopAddrUnnum}, GetNextIndexMplsTunnelCHopTable, MplsTunnelCHopAddrUnnumGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, MplsTunnelCHopTableINDEX, 2, 0, 0, NULL},

{{13,MplsTunnelCHopLspId}, GetNextIndexMplsTunnelCHopTable, MplsTunnelCHopLspIdGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, MplsTunnelCHopTableINDEX, 2, 0, 0, NULL},

{{13,MplsTunnelCHopType}, GetNextIndexMplsTunnelCHopTable, MplsTunnelCHopTypeGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER, SNMP_READONLY, MplsTunnelCHopTableINDEX, 2, 0, 0, NULL},

{{13,MplsTunnelPerfPackets}, GetNextIndexMplsTunnelPerfTable, MplsTunnelPerfPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsTunnelPerfTableINDEX, 4, 0, 0, NULL},

{{13,MplsTunnelPerfHCPackets}, GetNextIndexMplsTunnelPerfTable, MplsTunnelPerfHCPacketsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, MplsTunnelPerfTableINDEX, 4, 0, 0, NULL},

{{13,MplsTunnelPerfErrors}, GetNextIndexMplsTunnelPerfTable, MplsTunnelPerfErrorsGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsTunnelPerfTableINDEX, 4, 0, 0, NULL},

{{13,MplsTunnelPerfBytes}, GetNextIndexMplsTunnelPerfTable, MplsTunnelPerfBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER32, SNMP_READONLY, MplsTunnelPerfTableINDEX, 4, 0, 0, NULL},

{{13,MplsTunnelPerfHCBytes}, GetNextIndexMplsTunnelPerfTable, MplsTunnelPerfHCBytesGet, NULL, NULL, NULL, SNMP_DATA_TYPE_COUNTER64, SNMP_READONLY, MplsTunnelPerfTableINDEX, 4, 0, 0, NULL},

{{13,MplsTunnelCRLDPResMeanBurstSize}, GetNextIndexMplsTunnelCRLDPResTable, MplsTunnelCRLDPResMeanBurstSizeGet, MplsTunnelCRLDPResMeanBurstSizeSet, MplsTunnelCRLDPResMeanBurstSizeTest, MplsTunnelCRLDPResTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsTunnelCRLDPResTableINDEX, 1, 0, 0, NULL},

{{13,MplsTunnelCRLDPResExBurstSize}, GetNextIndexMplsTunnelCRLDPResTable, MplsTunnelCRLDPResExBurstSizeGet, MplsTunnelCRLDPResExBurstSizeSet, MplsTunnelCRLDPResExBurstSizeTest, MplsTunnelCRLDPResTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsTunnelCRLDPResTableINDEX, 1, 0, 0, NULL},

{{13,MplsTunnelCRLDPResFrequency}, GetNextIndexMplsTunnelCRLDPResTable, MplsTunnelCRLDPResFrequencyGet, MplsTunnelCRLDPResFrequencySet, MplsTunnelCRLDPResFrequencyTest, MplsTunnelCRLDPResTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsTunnelCRLDPResTableINDEX, 1, 0, 0, NULL},

{{13,MplsTunnelCRLDPResWeight}, GetNextIndexMplsTunnelCRLDPResTable, MplsTunnelCRLDPResWeightGet, MplsTunnelCRLDPResWeightSet, MplsTunnelCRLDPResWeightTest, MplsTunnelCRLDPResTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsTunnelCRLDPResTableINDEX, 1, 0, 0, "0"},

{{13,MplsTunnelCRLDPResFlags}, GetNextIndexMplsTunnelCRLDPResTable, MplsTunnelCRLDPResFlagsGet, MplsTunnelCRLDPResFlagsSet, MplsTunnelCRLDPResFlagsTest, MplsTunnelCRLDPResTableDep, SNMP_DATA_TYPE_UNSIGNED32, SNMP_READWRITE, MplsTunnelCRLDPResTableINDEX, 1, 0, 0, "0"},

{{13,MplsTunnelCRLDPResRowStatus}, GetNextIndexMplsTunnelCRLDPResTable, MplsTunnelCRLDPResRowStatusGet, MplsTunnelCRLDPResRowStatusSet, MplsTunnelCRLDPResRowStatusTest, MplsTunnelCRLDPResTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsTunnelCRLDPResTableINDEX, 1, 0, 1, NULL},

{{13,MplsTunnelCRLDPResStorageType}, GetNextIndexMplsTunnelCRLDPResTable, MplsTunnelCRLDPResStorageTypeGet, MplsTunnelCRLDPResStorageTypeSet, MplsTunnelCRLDPResStorageTypeTest, MplsTunnelCRLDPResTableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, MplsTunnelCRLDPResTableINDEX, 1, 0, 0, "2"},

{{11,MplsTunnelNotificationEnable}, NULL, MplsTunnelNotificationEnableGet, MplsTunnelNotificationEnableSet, MplsTunnelNotificationEnableTest, MplsTunnelNotificationEnableDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};
tMibData stdteEntry = { 98, stdteMibEntry };
#endif /* _STDTEDB_H */

