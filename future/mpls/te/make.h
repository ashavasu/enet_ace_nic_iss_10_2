#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# $Id: make.h,v 1.8 2011/12/05 13:50:29 siva Exp $
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                               |
# |                                                                          |
# |   MAKE TOOL(S) USED      : GNU make                                      |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 22/11/2001                                    |
# |                                                                          |
# |   DESCRIPTION            : Specifies the options and modules to be       |
# |                            including for building the FutureMPLS-TE      |
# |                            product.                                      |
# |                                                                          |
# +--------------------------------------------------------------------------+
 
# Set the PROJ_BASE_DIR as the directory where you untar the project files
PROJECT_NAME        = FutureTE
PROJECT_BASE_DIR    = ${BASE_DIR}/mpls/te
PROJECT_SOURCE_DIR  = ${PROJECT_BASE_DIR}/src
PROJECT_INCLUDE_DIR = ${PROJECT_BASE_DIR}/inc
PROJECT_OBJECT_DIR  = ${PROJECT_BASE_DIR}/obj
MPLS_INCL_DIR       = $(BASE_DIR)/mpls/mplsinc
MPLS_CDM_DIR        = $(BASE_DIR)/mpls/mplsdb
MPLS_OAM_INC_DIR    = $(BASE_DIR)/mpls/oam/inc
MPLS_FM_INCL_DIR    = $(BASE_DIR)/mpls/mplsrtr/inc


# Specify the project level compilation switches here
PROJECT_COMPILATION_SWITCHES +=

# Specify the project include directories and dependencies
PROJECT_INCLUDE_FILES  = $(PROJECT_INCLUDE_DIR)/tedefs.h \
                         $(PROJECT_INCLUDE_DIR)/tedbg.h \
                         $(PROJECT_INCLUDE_DIR)/teextif.h \
                         $(PROJECT_INCLUDE_DIR)/tefsap.h \
                         $(PROJECT_INCLUDE_DIR)/tegbl.h \
                         $(PROJECT_INCLUDE_DIR)/tegblex.h \
                         $(PROJECT_INCLUDE_DIR)/teincs.h \
                         $(PROJECT_INCLUDE_DIR)/teport.h \
                         $(PROJECT_INCLUDE_DIR)/teprot.h \
                         $(PROJECT_INCLUDE_DIR)/tetdfs.h \
                         $(PROJECT_INCLUDE_DIR)/thllwg.h \
    $(PROJECT_INCLUDE_DIR)/thlwrg.h \
    $(PROJECT_INCLUDE_DIR)/tpmwrg.h \
    $(PROJECT_INCLUDE_DIR)/tpmlwg.h \
                         $(PROJECT_INCLUDE_DIR)/tpmdb.h \
                         $(PROJECT_INCLUDE_DIR)/tpmtdfsg.h \
                         $(PROJECT_INCLUDE_DIR)/fsmptelw.h \
                         $(PROJECT_INCLUDE_DIR)/stdgmtlw.h \
                         $(PROJECT_INCLUDE_DIR)/fsmptewr.h \
                         $(PROJECT_INCLUDE_DIR)/fsmptedb.h \
                         $(PROJECT_INCLUDE_DIR)/stdgmtdb.h \
                         $(PROJECT_INCLUDE_DIR)/stdgmtwr.h \
                         $(MPLS_INCL_DIR)/temacs.h \
                         $(MPLS_INCL_DIR)/teextrn.h \
                         $(MPLS_INCL_DIR)/tedsmacs.h \
                         $(MPLS_INCL_DIR)/tedsdefs.h \
                         $(MPLS_INCL_DIR)/mplsdiff.h 


PROJECT_FINAL_INCLUDES_DIRS    = -I$(PROJECT_INCLUDE_DIR) \
                                 -I$(MPLS_INCL_DIR) \
                                 -I$(MPLS_FM_INCL_DIR) \
                                 -I$(MPLS_CDM_DIR) \
                                 -I$(MPLS_OAM_INC_DIR) \
                                 $(COMMON_INCLUDE_DIRS)

PROJECT_FINAL_INCLUDE_FILES    = $(PROJECT_INCLUDE_FILES)

PROJECT_DEPENDENCIES    += $(COMMON_DEPENDENCIES) \
                          $(PROJECT_FINAL_INCLUDE_FILES) \
                          $(MPLS_BASE_DIR)/make.h \
                          $(PROJECT_BASE_DIR)/Makefile \
                          $(PROJECT_BASE_DIR)/make.h


