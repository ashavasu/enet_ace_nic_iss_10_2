#!/bin/csh
# Copyright (C) 2006 Aricent Inc . All Rights Reserved
# +--------------------------------------------------------------------------+
# |   FILE  NAME             : make.h                                        |
# |                                                                          |
# |   PRINCIPAL AUTHOR       : Aricent Inc.                               |
# |                                                                          |
# |   MAKE TOOL(S) USED      : GNU make                                      |
# |                                                                          |
# |   TARGET ENVIRONMENT     : LINUX                                         |
# |                                                                          |
# |   DATE                   : 22/11/2001                                    |
# |                                                                          |
# |   DESCRIPTION            : Specifies the options and modules to be       |
# |                            including for building the FutureMPLS-TC      |
# |                            product.                                      |
# |                                                                          |
# +--------------------------------------------------------------------------+
 
# Set the PROJ_BASE_DIR as the directory where you untar the project files
PROJECT_NAME        = FutureTC
PROJECT_BASE_DIR    = ${BASE_DIR}/mpls/tc
PROJECT_SOURCE_DIR  = ${PROJECT_BASE_DIR}/src
PROJECT_INCLUDE_DIR = ${PROJECT_BASE_DIR}/inc
PROJECT_OBJECT_DIR  = ${PROJECT_BASE_DIR}/obj
MPLS_INCL_DIR       = $(BASE_DIR)/mpls/mplsinc


# Specify the project level compilation switches here
PROJECT_COMPILATION_SWITCHES +=

# Specify the project include directories and dependencies
PROJECT_INCLUDE_FILES  = $(PROJECT_INCLUDE_DIR)/tcdbg.h \
                         $(PROJECT_INCLUDE_DIR)/tcdefs.h \
                         $(PROJECT_INCLUDE_DIR)/tcglob.h \
                         $(PROJECT_INCLUDE_DIR)/tcincs.h \
                         $(PROJECT_INCLUDE_DIR)/tcmacs.h \
                         $(PROJECT_INCLUDE_DIR)/tcprot.h \
                         $(PROJECT_INCLUDE_DIR)/tctdfs.h \
                         $(MPLS_INCL_DIR)/tcapi.h \
                         $(MPLS_INCL_DIR)/tcextrn.h

PROJECT_FINAL_INCLUDES_DIRS    = -I$(PROJECT_INCLUDE_DIR) \
                                 -I$(MPLS_INCL_DIR) \
                                 $(COMMON_INCLUDE_DIRS)

PROJECT_FINAL_INCLUDE_FILES    = $(PROJECT_INCLUDE_FILES)

PROJECT_DEPENDENCIES    += $(COMMON_DEPENDENCIES) \
                          $(PROJECT_FINAL_INCLUDE_FILES) \
                          $(MPLS_BASE_DIR)/make.h \
                          $(PROJECT_BASE_DIR)/Makefile \
                          $(PROJECT_BASE_DIR)/make.h


