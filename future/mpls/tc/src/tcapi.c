/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tcapi.c,v 1.12 2014/01/07 10:38:31 siva Exp $
 *
 * Description: This file contains the functions that are used
 *              by applications that interacts with TC.
 ********************************************************************/

#include "tcincs.h"

/*****************************************************************************/
/* Function Name : TcRegisterApp
 * Description   : This API is first invoked by all Control Protocols /
 *                 Applications for registering themselves with the Traffic
 *                 Controler. During all subsequent Allocation of resources,
 *                 the Control Protocol / Application  needs to identify
 *                 itself by specifying its App Id.
 * Input(s)      : u4AppId     - Application Id requested, which has to be
 *                               used by this Application during all
 *                               subsequent interactions for Reservation of
 *                               resources.
 *                 pCallBackFn - Function pointer to a call back function
 *                               which will be invoked by TC in case of any
 *                               exceptions asynchoronous errors to be
 *                               intimated to the Application.
 * Output(s)     : NONE
 * Return(s)     : TC_SUCCESS / TC_FAILURE
 *****************************************************************************/
UINT1
TcRegisterApp (UINT4 u4AppId, tTcAppCallBackFnPtr pCallBackFn)
{
    TC_DBG (TC_API_ETEXT, "TcRegisterApp: ENTRY\n");

    switch (u4AppId)
    {
        case TC_CRLDP_APPID:
        case TC_RSVPTE_APPID:
            break;

        default:
            TC_DBG (TC_API_PRCS, "TcRegisterApp: Invalid App Id\n");
            TC_DBG (TC_API_ETEXT, "TcRegisterApp: EXIT1\n");
            return TC_FAILURE;
    }

    MPLS_TC_LOCK ();
    /* Search App Info Array for existence of Application with same AppId */
    if (TcSearchAppInfoEntry (u4AppId) == TC_SUCCESS)
    {
        /* Application with given u4AppId already exists */
        TC_DBG (TC_API_PRCS, "TcRegisterApp: App with given u4AppId already "
                "exists!\n");
        TC_DBG (TC_API_ETEXT, "TcRegisterApp: EXIT2\n");
        MPLS_TC_UNLOCK ();
        return TC_FAILURE;
    }
    if (TcAddAppInfoEntry (u4AppId, pCallBackFn) != TC_SUCCESS)
    {
        /* Unable to add the New App Info to the Array */
        TC_DBG (TC_API_PRCS, "TcRegisterApp: Unable to add App Info!\n");
        TC_DBG (TC_API_ETEXT, "TcRegisterApp: EXIT3\n");
        MPLS_TC_UNLOCK ();
        return TC_FAILURE;
    }
    /* New App Info added successfully */
    TC_DBG (TC_API_PRCS, "TcRegisterApp: New App Info added successfully!\n");
    TC_DBG (TC_API_ETEXT, "TcRegisterApp: EXIT4\n");
    MPLS_TC_UNLOCK ();
    return TC_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TcDregisterApp
 * Description   : This API is invoked by the Control Protocols / Applications
 *                 for de-registering themselves with the Traffic Controler.
 *                 All resources associated with them are freed/released.  
 *                 The Control Protocol / Application cannot make any
 *                 subsequent allocations without registering itself again
 *                 with the TC.
 * Input(s)      : u4AppId - Application Id of Application/Control protocol
 *                           which wants to de-register itself.
 * Output(s)     : NONE
 * Return(s)     : NONE
 *****************************************************************************/
VOID
TcDeregisterApp (UINT4 u4AppId)
{
    UINT4               u4Index;
    tTcPoolResInfo     *pTcPoolResInfo = NULL;
    tTcAllocResNode    *pTempResNode = NULL;
    tTcFilterInfoNode  *pTempFilterInfoNode = NULL;
    UINT1               u1Done = TC_FALSE;

    TC_DBG (TC_API_ETEXT, "TcDeregisterApp: ENTRY\n");

    MPLS_TC_LOCK ();
    for (u4Index = 0; u4Index < TC_MAX_APPS; u4Index++)
    {
        if ((gTcGlobal.aAppInfoArray[u4Index].u1Free == TC_FALSE) &&
            (gTcGlobal.aAppInfoArray[u4Index].u4AppId == u4AppId))
        {
            /* Found the required entry -- deleting it */
            gTcGlobal.aAppInfoArray[u4Index].u1Free = TC_TRUE;
            gTcGlobal.aAppInfoArray[u4Index].u4AppId = TC_INVALID_APPID;
            gTcGlobal.aAppInfoArray[u4Index].pCallBackFn = NULL;
            TC_DBG (TC_API_PRCS, "TcDeregisterApp: App Info Removed!\n");
            break;
        }
    }

    if (u4Index >= TC_MAX_APPS)
    {
        TC_DBG (TC_API_PRCS, "TcDeregisterApp: App Info Not Found!\n");
        TC_DBG (TC_API_ETEXT, "TcDeregisterApp: EXIT1\n");
        MPLS_TC_UNLOCK ();
        return;
    }

    while (u1Done != TC_TRUE)
    {
        u1Done = TC_TRUE;
        TMO_SLL_Scan (&(gTcGlobal.AllocResList), pTempResNode,
                      tTcAllocResNode *)
        {
            if ((UINT4) pTempResNode->u4AppId == u4AppId)
            {
                /* Found the Resource Entry -- Free It! */
                TcGetPoolResInfo (pTempResNode->u4InterfaceId,
                                  pTempResNode->u1ResPoolType, &pTcPoolResInfo);

                if (pTcPoolResInfo == NULL)
                {
                    continue;
                }
                pTcPoolResInfo->AvailResInfo.u4PeakDataRate +=
                    pTempResNode->MaxResInfo.u4PeakDataRate;
                TMO_SLL_Delete (&(gTcGlobal.AllocResList),
                                &(pTempResNode->NextAllocResNode));
                while (TMO_SLL_Count (&(pTempResNode->FilterInfoList))
                       != TC_ZERO)
                {
                    pTempFilterInfoNode = (tTcFilterInfoNode *)
                        TMO_SLL_First (&(pTempResNode->FilterInfoList));
                    TMO_SLL_Delete (&(pTempResNode->FilterInfoList),
                                    &(pTempFilterInfoNode->NextFilterInfoNode));
                    TC_DBG5 (TC_API_PRCS,
                             "TcDeregisterApp: \nHandle: %d\nFilter Info \nSrc: %d\n"
                             "Dest: %x\nTnl Id: %d\nTnl Instance: %d\nFreed "
                             "Successfully!\n",
                             (UINT4) (pTempResNode->ResHandle),
                             pTempFilterInfoNode->FilterInfo.u4SrcAddr,
                             pTempFilterInfoNode->FilterInfo.u4DestAddr,
                             pTempFilterInfoNode->FilterInfo.u4TnlId,
                             pTempFilterInfoNode->FilterInfo.u4TnlInstance);

                    TcFreeMemBlock (TCMemPoolIds
                                    [MAX_TC_FILTER_SPEC_NODE_SIZING_ID],
                                    (UINT1 *) pTempFilterInfoNode,
                                    tTcFilterInfoNode);
                }
                TC_DBG1 (TC_API_PRCS,
                         "TcDeregisterApp: Resource Handle %d Freed "
                         "Successfully!\n", (UINT4) (pTempResNode->ResHandle));

                TcFreeMemBlock (TCMemPoolIds[MAX_TC_ALLOC_RES_NODE_SIZING_ID],
                                (UINT1 *) pTempResNode, tTcAllocResNode);
                u1Done = TC_FALSE;
                break;
            }
        }
    }

    TC_DBG (TC_UTLS_ETEXT, "TcDeregisterApp: EXIT2\n");
    MPLS_TC_UNLOCK ();
    return;
}

/*****************************************************************************
 * Function Name : TcGetTcConfigFlag
 * Description   : This function outputs the current value of TcConfigFlag.
 * Input(s)      : NONE
 * Output(s)     : pu4TcConfigFlag - Pointer to the TC config Flag value.
 * Return(s)     : NONE
 *****************************************************************************/
VOID
TcGetTcConfigFlag (UINT4 *pu4TcConfigFlag)
{
    *pu4TcConfigFlag = gu4TcConfigFlag;
}

/*****************************************************************************
 * Function Name : TcConfigure
 * Description   : This function configures Tc Module for several testing
 *                 scenarios. 
 * Input(s)      : u4TcConfigFlag - Flag which indicates the Configuraiton
 *                                  scenario.
 * Output(s)     : NONE
 * Return(s)     : TC_SUCCESS or TC_FAILURE
 *****************************************************************************/
UINT1
TcConfigure (UINT4 u4TcConfigFlag)
{
    UINT4               u4IfIndex;
    UINT4               u4ResPoolIndex;

    TC_DBG (TC_MAIN_ETEXT, "TcConfigure: ENTRY\n");

    /* Currently the association of the Resources with the PSC of the
     * Interfaces is not configurable. Once the mib objects are
     * developed for the same this code needs to be properly modified */
    switch (u4TcConfigFlag)
    {
        case TC_SCENARIO_ONE:
            gu4TcConfigFlag = TC_SCENARIO_ONE;
            for (u4IfIndex = TC_ZERO; u4IfIndex < TC_MAX_INTERFACES;
                 u4IfIndex++)
            {
                gTcGlobal.aInterfaceInfo[u4IfIndex].u1Free = TC_FALSE;
                gTcGlobal.aInterfaceInfo[u4IfIndex].u4InterfaceId =
                    u4IfIndex + TC_INTEFACE_MIN;
                for (u4ResPoolIndex = TC_ZERO;
                     u4ResPoolIndex < TC_MAX_RES_POOLS; u4ResPoolIndex++)
                {
                    gTcGlobal.aInterfaceInfo[u4IfIndex].
                        aPoolResArray[u4ResPoolIndex].
                        AvailResInfo.u4PeakDataRate = TC_ETH_LINK_SPEED;
                }
            }
            TC_DBG (TC_MAIN_PRCS, "TcConfigure: Scenario One Configured!\n");
            break;
        default:
            TC_DBG (TC_MAIN_PRCS, "TcConfigure: Unsupported Scenario!\n");
            TC_DBG (TC_MAIN_ETEXT, "TcConfigure: EXIT1\n");
            return TC_FAILURE;
    }
    TC_DBG (TC_MAIN_ETEXT, "TcConfigure: EXIT2\n");
    return TC_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TcResvResources
 * Description   : This API is invoked to request the TC to reserve the
 *                 Resources specified on the given Interface.
 * Input(s)      : u4AppId       - Application Id of Control Protocol /
 *                                 Application requesting for allocation of
 *                                 resources.
 *                 u4InterfaceId - Interface on which the resource has to be
 *                                 allocated.
 *                 u1ResSpecType - Resource Spec Type value (CRLDP type or
 *                                 RSVPTE type, etc.).
 *                 pTcResSpec    - Pointer to the Resource Specification union.
 *                                 This is an input/output parameter. As an
 *                                 input, it specifies the request resources
 *                                 to be allocated.  The union is interpreted
 *                                 according to the u1ResSpecType Parameter
 *                                 specified as Input.
 *                 u1ResPoolType - Type of Resource Pool (INTSERV Pool or
 *                                 specific DIFFSERV class etc.) from which
 *                                 allocation is done.
 * Output(s)     : pTcResSpec    - Pointer to the Resource Specification union.
 *                                 This is a input/output parameter.  As an
 *                                 output, it returns the actual resources
 *                                 allocated by the TC.
 *                 pResHandle    - In case of successful allocation, a handle
 *                                 is returned to the allocated resrource,
 *                                 which can be used to refer to the resources
 *                                 while freeing - or by the external modules
 *                                 for associating the allocated resources
 *                                 with any other module data.
 *                 pu1ErrorCode  - Error Code indicating the reason for the
 *                                 failure of Resource Allocaiton (if
 *                                 allocation Fails).
 * Return(s)     : TC_SUCCESS or TC_FAILURE
 *****************************************************************************/
UINT1
TcResvResources (UINT4 u4AppId, UINT4 u4InterfaceId,
                 UINT1 u1ResSpecType, tuTcResSpec * pTcResSpec,
                 UINT1 u1ResPoolType, tTcResHandle * pResHandle,
                 UINT1 *pu1ErrorCode)
{
    tTcPoolResInfo     *pTcPoolResInfo = NULL;
    tTcAdmCtrlAlgoFnPtr pAdmCtrlAlgo =
        (tTcAdmCtrlAlgoFnPtr) TcDefaultAdmCtrlAlgo;
    tTcResInfo          ConvResReq;
    tTcAllocResNode    *pAllocResNode = NULL;

    TC_DBG (TC_API_ETEXT, "TcResvResources: ENTRY\n");
    MPLS_TC_LOCK ();
    /* Search App Info Array for existence of Application with given AppId */
    if (TcSearchAppInfoEntry (u4AppId) != TC_SUCCESS)
    {
        /* Application with given u4AppId doesn't exist */
        TC_DBG (TC_API_PRCS,
                "TcResvResources: App with given u4AppId doesn't " "exist!\n");
        TC_DBG (TC_API_ETEXT, "TcResvResources: EXIT1\n");
        *pu1ErrorCode = TC_ERR_APPID_DOES_NOT_EXIST;
        MPLS_TC_UNLOCK ();
        return TC_FAILURE;
    }
    /* Search Interface Info Array for existence of given Interface */
    if (TcSearchInterfaceInfoEntry (u4InterfaceId) != TC_SUCCESS)
    {
        /* Interface with given u4InterfaceId doesn't exist */
        TC_DBG (TC_API_PRCS, "TcResvResources: Interface doesn't exist!\n");
        TC_DBG (TC_API_ETEXT, "TcResvResources: EXIT2\n");
        *pu1ErrorCode = TC_ERR_INTERFACEID_DOES_NOT_EXIST;
        MPLS_TC_UNLOCK ();
        return TC_FAILURE;
    }
    if ((u1ResPoolType <= TC_ZERO) || (u1ResPoolType >= TC_MAX_RES_POOLS))
    {
        /* Invalid Resource Pool Type */
        TC_DBG (TC_API_PRCS, "TcResvResources: Invalid Resource Pool Type!\n");
        TC_DBG (TC_API_ETEXT, "TcResvResources: EXIT3\n");
        *pu1ErrorCode = TC_ERR_INVALID_RES_POOL_TYPE;
        MPLS_TC_UNLOCK ();
        return TC_FAILURE;
    }
    /* Convert the Resource Specification into standard tTcResInfo format */
    switch (u1ResSpecType)
    {
        case TC_STD_TYPE_RES_SPEC:
            ConvResReq.u4PeakDataRate =
                pTcResSpec->TcStdTrfcParams.u4PeakDataRate;
            break;
        case TC_CRLDP_TYPE_RES_SPEC:
            ConvResReq.u4PeakDataRate =
                (pTcResSpec->CrldpTrfcParams.u4PeakDataRate);
            break;
        case TC_RSVPTE_TYPE_RES_SPEC:
            ConvResReq.u4PeakDataRate =
                (UINT4) OSIX_NTOHF ((pTcResSpec->RsvpTrfcParams.ServiceParams.
                                     ClsService.TBParams.fPeakRate));
            TC_CONVERT_BYTES_PER_SEC_TO_BPS (ConvResReq.u4PeakDataRate);
            break;
        default:
            /* Invalid Res Spec Type */
            TC_DBG (TC_API_PRCS, "TcResvResources: Invalid Res Spec Type!\n");
            TC_DBG (TC_API_ETEXT, "TcResvResources: EXIT4\n");
            *pu1ErrorCode = TC_ERR_INVALID_RES_SPEC_TYPE;
            MPLS_TC_UNLOCK ();
            return TC_FAILURE;
    }

    if (gTcGlobal.pAdmCtrlAlgo != NULL)
    {
        pAdmCtrlAlgo = gTcGlobal.pAdmCtrlAlgo;
    }
    if ((*pAdmCtrlAlgo) (u4AppId, u4InterfaceId, u1ResPoolType, &ConvResReq,
                         pu1ErrorCode) != TC_SUCCESS)
    {
        TC_DBG (TC_API_PRCS, "TcResvResources: Admission Control Failed!\n");
        TC_DBG (TC_API_ETEXT, "TcResvResources: EXIT5\n");
        MPLS_TC_UNLOCK ();
        return TC_FAILURE;
    }
    if (TcGenerateNewResHandle (pResHandle) != TC_SUCCESS)
    {
        TC_DBG (TC_API_PRCS, "TcResvResources: Unable to Generate "
                "New Handle!\n");
        TC_DBG (TC_API_ETEXT, "TcResvResources: EXIT6\n");
        *pu1ErrorCode = TC_ERR_HANDLE_NOT_AVAILABLE;
        MPLS_TC_UNLOCK ();
        return TC_FAILURE;
    }

    pAllocResNode = (tTcAllocResNode *)
        TcAllocMemBlock (TCMemPoolIds[MAX_TC_ALLOC_RES_NODE_SIZING_ID]);

    if (pAllocResNode == NULL)
    {
        TC_DBG (TC_API_PRCS, "TcResvResources: Unable to Create "
                "Resource Node!\n");
        TC_DBG (TC_API_ETEXT, "TcResvResources: EXIT7\n");
        *pu1ErrorCode = TC_ERR_RESOURCE_NODE_ALLOC_FAILURE;
        MPLS_TC_UNLOCK ();
        return TC_FAILURE;
    }
    TMO_SLL_Init_Node (&(pAllocResNode->NextAllocResNode));
    TMO_SLL_Init (&(pAllocResNode->FilterInfoList));

    pAllocResNode->ResHandle = *pResHandle;
    pAllocResNode->u4AppId = u4AppId;
    pAllocResNode->u4InterfaceId = u4InterfaceId;
    pAllocResNode->u1ResPoolType = u1ResPoolType;
    pAllocResNode->MaxResInfo.u4PeakDataRate = ConvResReq.u4PeakDataRate;

    if (SLLInsertInOrder (gau2OffSet, gau2Length, gau1ByteOrder, TC_FOUR,
                          &(gTcGlobal.AllocResList), (VOID *) pAllocResNode,
                          TC_ZERO) != MPLS_SUCCESS)
    {
        TcFreeMemBlock (TCMemPoolIds[MAX_TC_ALLOC_RES_NODE_SIZING_ID],
                        (UINT1 *) pAllocResNode, tTcAllocResNode);
        TC_DBG (TC_API_PRCS, "TcResvResources: Unable to Update Info "
                "Database!\n");
        TC_DBG (TC_API_ETEXT, "TcResvResources: EXIT8\n");
        *pu1ErrorCode = TC_ERR_UPDATE_RES_INFO_DATABASE;
        MPLS_TC_UNLOCK ();
        return TC_FAILURE;
    }

    /* Will not fail here */
    TcGetPoolResInfo (u4InterfaceId, u1ResPoolType, &pTcPoolResInfo);

    if (pTcPoolResInfo == NULL)
    {
        TcFreeMemBlock (TCMemPoolIds[MAX_TC_ALLOC_RES_NODE_SIZING_ID],
                        (UINT1 *) pAllocResNode, tTcAllocResNode);
        TC_DBG (TC_API_PRCS, "TcResvResources: Unable to Update Info "
                "Database!\n");
        TC_DBG (TC_API_ETEXT, "TcResvResources: EXIT9\n");
        *pu1ErrorCode = TC_ERR_UPDATE_RES_INFO_DATABASE;
        MPLS_TC_UNLOCK ();
        return TC_FAILURE;
    }

    pTcPoolResInfo->AvailResInfo.u4PeakDataRate -= ConvResReq.u4PeakDataRate;

    TC_DBG (TC_API_PRCS, "TcResvResources: Reserved Resources Successfully!\n");
    TC_DBG (TC_API_ETEXT, "TcResvResources: EXIT9\n");
    *pu1ErrorCode = TC_NO_ERR;
    KW_FALSEPOSITIVE_FIX (pAllocResNode);
    MPLS_TC_UNLOCK ();
    return TC_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TcFreeResources
 * Description   : This API is invoked to request the TC to free the Resources
 *                 associated with a given Resource Handle.
 *                 If any external data has been associated with the given
 *                 resource handle, it is the responsibility of the external
 *                 modules which created the association to free all such
 *                 associations BEFORE calling this API.  The TC can reuse
 *                 the handles at any point of time after it has been released.
 * Input(s)      : ResHandle - Handle to the Resources, returned while
 *                             allocation of the resource.   
 * Output(s)     : NONE
 * Return(s)     : NONE
 *****************************************************************************/
VOID
TcFreeResources (tTcResHandle ResHandle)
{
    tTcPoolResInfo     *pTcPoolResInfo = NULL;
    tTcAllocResNode    *pTempResNode = NULL;
    tTcFilterInfoNode  *pTempFilterInfoNode = NULL;

    TC_DBG (TC_API_ETEXT, "TcFreeResources: ENTRY\n");

    MPLS_TC_LOCK ();
    TMO_SLL_Scan (&(gTcGlobal.AllocResList), pTempResNode, tTcAllocResNode *)
    {
        if ((UINT4) pTempResNode->ResHandle == (UINT4) ResHandle)
        {
            /* Found the Resource Entry -- Free It! */
            TcGetPoolResInfo (pTempResNode->u4InterfaceId,
                              pTempResNode->u1ResPoolType, &pTcPoolResInfo);
            if (pTcPoolResInfo == NULL)
            {
                continue;
            }
            pTcPoolResInfo->AvailResInfo.u4PeakDataRate +=
                pTempResNode->MaxResInfo.u4PeakDataRate;
            TMO_SLL_Delete (&(gTcGlobal.AllocResList),
                            &(pTempResNode->NextAllocResNode));
            while (TMO_SLL_Count (&(pTempResNode->FilterInfoList)) != TC_ZERO)
            {
                pTempFilterInfoNode = (tTcFilterInfoNode *)
                    TMO_SLL_First (&(pTempResNode->FilterInfoList));
                TMO_SLL_Delete (&(pTempResNode->FilterInfoList),
                                &(pTempFilterInfoNode->NextFilterInfoNode));
                TcFreeMemBlock (TCMemPoolIds
                                [MAX_TC_FILTER_SPEC_NODE_SIZING_ID],
                                (UINT1 *) pTempFilterInfoNode,
                                tTcFilterInfoNode);
            }
            TcFreeMemBlock (TCMemPoolIds[MAX_TC_ALLOC_RES_NODE_SIZING_ID],
                            (UINT1 *) pTempResNode, tTcAllocResNode);
            TC_DBG (TC_API_PRCS, "TcFreeResources: Resources Freed "
                    "Successfully!\n");
            TC_DBG (TC_API_ETEXT, "TcFreeResources: EXIT1\n");
            MPLS_TC_UNLOCK ();
            return;
        }
    }
    TC_DBG (TC_API_PRCS, "TcFreeResources: Handle Not Found!\n");
    TC_DBG (TC_API_ETEXT, "TcFreeResources: EXIT2\n");
    MPLS_TC_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name : TcModifyResources
 * Description   : This API is invoked from other module to modify the
 *                 resource and this will take the TC_LOCK and calls
 *                 the actual TcModifyRes function
 * Input(s)      : ResHandle     - Handle to the Resources, returned while  
 *                                 allocation of the resource.
 *                 u1ResSpecType - Resource Spec Type value (CRLDP type or
 *                                 RSVPTE type, etc.).
 *                 pTcResSpec    - Pointer to the Resource Specification union.
 *                                 This is an input/output parameter. As an
 *                                 input, it specifies the modified resources
 *                                 to be reserved. The union is interpreted
 *                                 according to the u1ResSpecType Parameter
 *                                 specified as Input.
 *                 u1ResPoolType - Type of Resource Pool (INTSERV Pool or
 *                                 specific DIFFSERV class etc.) from which
 *                                 allocation is done.
 * Output(s)     : pTcResSpec    - Pointer to the Resource Specification union.
 *                                 This is a input/output parameter.  As an
 *                                 output, it returns the actual resources
 *                                 allocated by the TC.
 *                 pu1ErrorCode  - Error Code indicating the reason for the
 *                                 failure of Resource Allocaiton (if
 *                                 allocation Fails).
 * Return(s)     : TC_SUCCESS or TC_FAILURE
 *****************************************************************************/
UINT1
TcModifyResources (tTcResHandle ResHandle, UINT1 u1ResSpecType,
                   tuTcResSpec * pTcResSpec, UINT1 u1ResPoolType,
                   UINT1 *pu1ErrorCode)
{
    UINT1               u1Return = 0;
    MPLS_TC_LOCK ();
    u1Return = TcModifyRes (ResHandle, u1ResSpecType,
                            pTcResSpec, u1ResPoolType, pu1ErrorCode);
    MPLS_TC_UNLOCK ();
    return u1Return;
}

/*****************************************************************************/
/* Function Name : TcModifyResources
 * Description   : This API is invoked to request the TC to modify the
 *                 Resources reserved for a given Resource Handle.
 *                 Option is also provided to allocate resources either from
 *                 resources available for Intserv or DiffServ classes
 *                 specifically.  The Resource Handle and the associated Filter
 *                 Specs do not get changed due to the modification of the
 *                 Resource Requirements.
 * Input(s)      : ResHandle     - Handle to the Resources, returned while  
 *                                 allocation of the resource.
 *                 u1ResSpecType - Resource Spec Type value (CRLDP type or
 *                                 RSVPTE type, etc.).
 *                 pTcResSpec    - Pointer to the Resource Specification union.
 *                                 This is an input/output parameter. As an
 *                                 input, it specifies the modified resources
 *                                 to be reserved. The union is interpreted
 *                                 according to the u1ResSpecType Parameter
 *                                 specified as Input.
 *                 u1ResPoolType - Type of Resource Pool (INTSERV Pool or
 *                                 specific DIFFSERV class etc.) from which
 *                                 allocation is done.
 * Output(s)     : pTcResSpec    - Pointer to the Resource Specification union.
 *                                 This is a input/output parameter.  As an
 *                                 output, it returns the actual resources
 *                                 allocated by the TC.
 *                 pu1ErrorCode  - Error Code indicating the reason for the
 *                                 failure of Resource Allocaiton (if
 *                                 allocation Fails).
 * Return(s)     : TC_SUCCESS or TC_FAILURE
 *****************************************************************************/

UINT1
TcModifyRes (tTcResHandle ResHandle, UINT1 u1ResSpecType,
             tuTcResSpec * pTcResSpec, UINT1 u1ResPoolType, UINT1 *pu1ErrorCode)
{
    tTcPoolResInfo     *pTcPoolResInfo = NULL;
    tTcAllocResNode    *pTempResNode = NULL;
    tTcAdmCtrlAlgoFnPtr pAdmCtrlAlgo =
        (tTcAdmCtrlAlgoFnPtr) TcDefaultAdmCtrlAlgo;
    tTcResInfo          ConvResReq;
    tTcResInfo          ExtraResReq;
    tTcFilterInfoNode  *pTempFilterInfoNode = NULL;

    TC_DBG (TC_API_ETEXT, "TcModifyResources: ENTRY\n");

    if ((u1ResPoolType <= TC_ZERO) || (u1ResPoolType >= TC_MAX_RES_POOLS))
    {
        /* Invalid Resource Pool Type */
        TC_DBG (TC_API_PRCS,
                "TcModifyResources: Invalid Resource Pool Type!\n");
        TC_DBG (TC_API_ETEXT, "TcModifyResources: EXIT1\n");
        *pu1ErrorCode = TC_ERR_INVALID_RES_POOL_TYPE;
        return TC_FAILURE;
    }
    TMO_SLL_Scan (&(gTcGlobal.AllocResList), pTempResNode, tTcAllocResNode *)
    {
        if ((UINT4) pTempResNode->ResHandle == (UINT4) ResHandle)
        {
            /* Convert the Res Specification into standard tTcResInfo format */
            switch (u1ResSpecType)
            {
                case TC_STD_TYPE_RES_SPEC:
                    ConvResReq.u4PeakDataRate =
                        pTcResSpec->TcStdTrfcParams.u4PeakDataRate;
                    break;
                case TC_CRLDP_TYPE_RES_SPEC:
                    ConvResReq.u4PeakDataRate =
                        (pTcResSpec->CrldpTrfcParams.u4PeakDataRate);
                    break;
                case TC_RSVPTE_TYPE_RES_SPEC:
                    ConvResReq.u4PeakDataRate =
                        ((UINT4)
                         OSIX_NTOHF (pTcResSpec->RsvpTrfcParams.ServiceParams.
                                     ClsService.TBParams.fPeakRate));
                    TC_CONVERT_BYTES_PER_SEC_TO_BPS (ConvResReq.u4PeakDataRate);
                    break;
                default:
                    /* Invalid Res Spec Type */
                    TC_DBG (TC_API_PRCS,
                            "TcModifyResources: Invalid Res Spec " "Type!\n");
                    TC_DBG (TC_API_ETEXT, "TcModifyResources: EXIT2\n");
                    *pu1ErrorCode = TC_ERR_INVALID_RES_SPEC_TYPE;
                    return TC_FAILURE;
            }

            if (gTcGlobal.pAdmCtrlAlgo != NULL)
            {
                pAdmCtrlAlgo = gTcGlobal.pAdmCtrlAlgo;
            }
            if (pTempResNode->u1ResPoolType == u1ResPoolType)
            {
                /* Modification on same Pool Type, Just check if
                 * Additional resources are available */
                if (pTempResNode->MaxResInfo.u4PeakDataRate <
                    ConvResReq.u4PeakDataRate)
                {
                    ExtraResReq.u4PeakDataRate = ConvResReq.u4PeakDataRate -
                        pTempResNode->MaxResInfo.u4PeakDataRate;

                    if ((*pAdmCtrlAlgo) (pTempResNode->u4AppId,
                                         pTempResNode->u4InterfaceId,
                                         u1ResPoolType, &ExtraResReq,
                                         pu1ErrorCode) != TC_SUCCESS)
                    {
                        TC_DBG (TC_API_PRCS, "TcModifyResources: Admission "
                                "Control Failed!\n");
                        TC_DBG (TC_API_ETEXT, "TcModifyResources: EXIT3\n");
                        return TC_FAILURE;
                    }
                }
            }
            else
            {
                if ((*pAdmCtrlAlgo) (pTempResNode->u4AppId,
                                     pTempResNode->u4InterfaceId,
                                     u1ResPoolType,
                                     &ConvResReq, pu1ErrorCode) != TC_SUCCESS)
                {
                    TC_DBG (TC_API_PRCS, "TcModifyResources: Admission "
                            "Control Failed!\n");
                    TC_DBG (TC_API_ETEXT, "TcModifyResources: EXIT4\n");
                    return TC_FAILURE;
                }
            }

            TcGetPoolResInfo (pTempResNode->u4InterfaceId,
                              pTempResNode->u1ResPoolType, &pTcPoolResInfo);
            if (pTcPoolResInfo == NULL)
            {
                continue;
            }

            pTcPoolResInfo->AvailResInfo.u4PeakDataRate +=
                pTempResNode->MaxResInfo.u4PeakDataRate;
            TMO_SLL_Delete (&(gTcGlobal.AllocResList),
                            &(pTempResNode->NextAllocResNode));

            TMO_SLL_Init_Node (&(pTempResNode->NextAllocResNode));
            pTempResNode->u1ResPoolType = u1ResPoolType;
            pTempResNode->MaxResInfo.u4PeakDataRate = ConvResReq.u4PeakDataRate;

            if (SLLInsertInOrder (gau2OffSet, gau2Length, gau1ByteOrder,
                                  TC_FOUR, &(gTcGlobal.AllocResList),
                                  (VOID *) pTempResNode,
                                  TC_ZERO) != MPLS_SUCCESS)
            {
                /* This should not fail realistically speaking! */
                while (TMO_SLL_Count (&(pTempResNode->FilterInfoList))
                       != TC_ZERO)
                {
                    pTempFilterInfoNode = (tTcFilterInfoNode *)
                        TMO_SLL_First (&(pTempResNode->FilterInfoList));
                    TMO_SLL_Delete (&(pTempResNode->FilterInfoList),
                                    &(pTempFilterInfoNode->NextFilterInfoNode));
                    TcFreeMemBlock (TCMemPoolIds
                                    [MAX_TC_FILTER_SPEC_NODE_SIZING_ID],
                                    (UINT1 *) pTempFilterInfoNode,
                                    tTcFilterInfoNode);
                }
                TcFreeMemBlock (TCMemPoolIds[MAX_TC_ALLOC_RES_NODE_SIZING_ID],
                                (UINT1 *) pTempResNode, tTcAllocResNode);
                TC_DBG (TC_API_PRCS, "TcModifyResources: Unable to Update "
                        "Info Database!\n");
                TC_DBG (TC_API_ETEXT, "TcModifyResources: EXIT4\n");
                *pu1ErrorCode = TC_ERR_UPDATE_RES_INFO_DATABASE;
                return TC_FAILURE;
            }

            TcGetPoolResInfo (pTempResNode->u4InterfaceId, u1ResPoolType,
                              &pTcPoolResInfo);
            if (pTcPoolResInfo == NULL)
            {
                continue;
            }

            pTcPoolResInfo->AvailResInfo.u4PeakDataRate -=
                ConvResReq.u4PeakDataRate;

            TC_DBG (TC_API_PRCS, "TcModifyResources: Resources Modified "
                    "Successfully!\n");
            TC_DBG (TC_API_ETEXT, "TcModifyResources: EXIT5\n");
            *pu1ErrorCode = TC_NO_ERR;
            return TC_SUCCESS;
        }
    }
    TC_DBG (TC_API_PRCS, "TcModifyResources: Handle Not Found!\n");
    TC_DBG (TC_API_ETEXT, "TcModifyResources: EXIT6\n");
    *pu1ErrorCode = TC_ERR_HANDLE_NOT_FOUND;
    return TC_FAILURE;
}

/*****************************************************************************/
/* Function Name : TcAssociateFilterInfo 
 * Description   : This API is used to specify "TO WHOM (ALL)" the allocated
 *                 resources are associated.  Multiple Filter Infos (Flows)
 *                 can be associated to a single Resource (single handle),
 *                 i.e. Resource Sharing, by repeatedly calling this API with
 *                 the same handle and associating each of the several Filter
 *                 Infos, one by one, with it.
 *                 Once a data packet belonging to a certain Resource Handle
 *                 arrives at the Traffic Controller, the TC could validate if
 *                 the packet belongs to the set of Filter Infos associated
 *                 with this handle, thus effectively policing the flow. 
 * Input(s)      : ResHandle     - Handle to the Resources which need to be
 *                                 associated with a given Filter Info
 *                 pFilterInfo   - Pointer to a Filter Info. This defines the
 *                                 flow which gets associated with the given
 *                                 Resource Handle. 
 *                 u1ResSpecType - Type of Resource Specification
 *                 pTcResSpec    - Pointer to the Resource Specification union.
 *                                 This is an input/output parameter. As an
 *                                 input, it specifies the resources required
 *                                 by this filter info.
 *                                 If, this is greater than the resources
 *                                 already associated with the Handle the
 *                                 additional resources get allocated.
 *                                 The union is interpreted according to the
 *                                 u1ResSpecType Parameter specified as Input.
 * Output(s)     : pTcResSpec    - Pointer to the Resource Specification union.
 *                                 This is an input/output parameter. As an
 *                                 output, it specifies the resources reserved
 *                                 iff the resource requirements of the given
 *                                 filter info are greater than those already
 *                                 reserved.
 *                 pu1ErrorCode  - Error Code specifying the reason for
 *                                 failure of association, if any.
 * Return(s)     : TC_SUCCESS or TC_FAILURE
 *****************************************************************************/
UINT1
TcAssociateFilterInfo (tTcResHandle ResHandle, tTcFilterInfo * pFilterInfo,
                       UINT1 u1ResSpecType, tuTcResSpec * pTcResSpec,
                       UINT1 *pu1ErrorCode)
{
    tTcFilterInfoNode  *pTempFilterInfoNode = NULL;
    tTcAllocResNode    *pTempResNode = NULL;
    tTcResInfo          ConvResReq;

    TC_DBG (TC_API_ETEXT, "TcAssociateFilterInfo: ENTRY\n");

    MPLS_TC_LOCK ();
    TMO_SLL_Scan (&(gTcGlobal.AllocResList), pTempResNode, tTcAllocResNode *)
    {
        if ((UINT4) pTempResNode->ResHandle == (UINT4) ResHandle)
        {
            /* Found the Resource Entry */
            /* Convert the Res Specification into standard tTcResInfo format */
            switch (u1ResSpecType)
            {
                case TC_STD_TYPE_RES_SPEC:
                    ConvResReq.u4PeakDataRate =
                        pTcResSpec->TcStdTrfcParams.u4PeakDataRate;
                    break;
                case TC_CRLDP_TYPE_RES_SPEC:
                    ConvResReq.u4PeakDataRate = OSIX_NTOHL
                        (pTcResSpec->CrldpTrfcParams.u4PeakDataRate);
                    break;
                case TC_RSVPTE_TYPE_RES_SPEC:
                    ConvResReq.u4PeakDataRate =
                        (UINT4) OSIX_NTOHF (pTcResSpec->RsvpTrfcParams.
                                            ServiceParams.ClsService.TBParams.
                                            fPeakRate);
                    TC_CONVERT_BYTES_PER_SEC_TO_BPS (ConvResReq.u4PeakDataRate);
                    break;
                default:
                    /* Invalid Res Spec Type */
                    TC_DBG (TC_API_PRCS, "TcAssociateFilterInfo: Invalid Res "
                            "Spec Type!\n");
                    TC_DBG (TC_API_ETEXT, "TcAssociateFilterInfo: EXIT1\n");
                    *pu1ErrorCode = TC_ERR_INVALID_RES_SPEC_TYPE;
                    MPLS_TC_UNLOCK ();
                    return TC_FAILURE;
            }
            if (ConvResReq.u4PeakDataRate >
                pTempResNode->MaxResInfo.u4PeakDataRate)
            {
                /* New Filter Info Requires more Bandwidth than Max */
                if (TcModifyRes (pTempResNode->ResHandle, u1ResSpecType,
                                 pTcResSpec, pTempResNode->u1ResPoolType,
                                 pu1ErrorCode) != TC_SUCCESS)
                {
                    TC_DBG (TC_API_PRCS, "TcAssociateFilterInfo: Not able "
                            "to allocate additional Bandwidth!\n");
                    TC_DBG (TC_API_ETEXT, "TcAssociateFilterInfo: EXIT2\n");
                    MPLS_TC_UNLOCK ();
                    return TC_FAILURE;
                }
            }

            pTempFilterInfoNode = (tTcFilterInfoNode *)
                TcAllocMemBlock (TCMemPoolIds
                                 [MAX_TC_FILTER_SPEC_NODE_SIZING_ID]);

            if (pTempFilterInfoNode == NULL)
            {
                TC_DBG (TC_API_PRCS, "TcAssociateFilterInfo: Cannot Allocate "
                        "Filter Info!");
                TC_DBG (TC_API_ETEXT, "TcAssociateFilterInfo: EXIT3\n");
                *pu1ErrorCode = TC_ERR_FILTER_SPEC_NODE_ALLOC_FAILURE;
                MPLS_TC_UNLOCK ();
                return TC_FAILURE;
            }
            TMO_SLL_Init_Node (&(pTempFilterInfoNode->NextFilterInfoNode));
            pTempFilterInfoNode->FilterInfo.u4SrcAddr = pFilterInfo->u4SrcAddr;
            pTempFilterInfoNode->FilterInfo.u4DestAddr
                = pFilterInfo->u4DestAddr;
            pTempFilterInfoNode->FilterInfo.u4TnlId = pFilterInfo->u4TnlId;
            pTempFilterInfoNode->FilterInfo.u4TnlInstance
                = pFilterInfo->u4TnlInstance;
            pTempFilterInfoNode->ResInfo.u4PeakDataRate
                = ConvResReq.u4PeakDataRate;

            TMO_SLL_Add (&(pTempResNode->FilterInfoList),
                         &(pTempFilterInfoNode->NextFilterInfoNode));

            TC_DBG (TC_API_PRCS, "TcAssociateFilterInfo: Filter Info "
                    "Associated Successfully!\n");
            TC_DBG (TC_API_ETEXT, "TcAssociateFilterInfo: EXIT4\n");
            *pu1ErrorCode = TC_NO_ERR;
            MPLS_TC_UNLOCK ();
            return TC_SUCCESS;
        }
    }
    TC_DBG (TC_API_PRCS, "TcAssociateFilterInfo: Handle Not Found!\n");
    TC_DBG (TC_API_ETEXT, "TcAssociateFilterInfo: EXIT4\n");
    *pu1ErrorCode = TC_ERR_HANDLE_NOT_FOUND;
    MPLS_TC_UNLOCK ();
    return TC_FAILURE;
}

/*****************************************************************************/
/* Function Name : TcDissociateFilterInfo 
 * Description   : This API is used to dissociate an already associated Filter
 *                 Info from its Resource Handle. All Flows associated with a
 *                 certain Resource Handle can be released by repeatedly
 *                 calling this API with the same Handle.
 * Input(s)      : ResHandle     - Handle to the Resources from which the given
 *                                 filter info needs to be dissociated
 *                 pFilterInfo   - Pointer to a Filter Info. This defines the
 *                                 flow which gets dissociated from the given
 *                                 Resource Handle.  The resources reserved
 *                                 for this Handle is modified to the maximum
 *                                 of all the remaining Filter Infos 
 *                                 associated with the given Handle.
 * Output(s)     : NONE
 * Return(s)     : NONE
 *****************************************************************************/
VOID
TcDissociateFilterInfo (tTcResHandle ResHandle, tTcFilterInfo * pFilterInfo)
{
    tTcFilterInfoNode  *pTempFilterInfoNode = NULL;
    tTcAllocResNode    *pTempResNode = NULL;
    UINT4               u4MaxPeakDataRate;
    tTcResInfo         *pMaxResInfo = NULL;
    UINT1               u1ErrorCode;

    TC_DBG (TC_API_ETEXT, "TcDissociateFilterInfo: ENTRY\n");

    MPLS_TC_LOCK ();
    TMO_SLL_Scan (&(gTcGlobal.AllocResList), pTempResNode, tTcAllocResNode *)
    {
        if ((UINT4) pTempResNode->ResHandle == (UINT4) ResHandle)
        {
            /* Found the Resource Entry */
            TMO_SLL_Scan (&(pTempResNode->FilterInfoList), pTempFilterInfoNode,
                          tTcFilterInfoNode *)
            {
                if ((pTempFilterInfoNode->FilterInfo.u4SrcAddr
                     == pFilterInfo->u4SrcAddr) &&
                    (pTempFilterInfoNode->FilterInfo.u4DestAddr
                     == pFilterInfo->u4DestAddr) &&
                    (pTempFilterInfoNode->FilterInfo.u4TnlId
                     == pFilterInfo->u4TnlId) &&
                    (pTempFilterInfoNode->FilterInfo.u4TnlInstance
                     == pFilterInfo->u4TnlInstance))
                {
                    /* Found the Filter Info -- Delete It! */
                    TMO_SLL_Delete (&(pTempResNode->FilterInfoList),
                                    &(pTempFilterInfoNode->NextFilterInfoNode));
                    TcFreeMemBlock (TCMemPoolIds
                                    [MAX_TC_FILTER_SPEC_NODE_SIZING_ID],
                                    (UINT1 *) pTempFilterInfoNode,
                                    tTcFilterInfoNode);

                    u4MaxPeakDataRate = TC_ZERO;
                    pMaxResInfo = NULL;
                    if (TMO_SLL_Count (&(pTempResNode->FilterInfoList)) !=
                        TC_ZERO)
                    {
                        TMO_SLL_Scan (&(pTempResNode->FilterInfoList),
                                      pTempFilterInfoNode, tTcFilterInfoNode *)
                        {
                            /* Find the Max Bandwidth of Remaining Filter
                             * Infos */
                            if (u4MaxPeakDataRate <
                                pTempFilterInfoNode->ResInfo.u4PeakDataRate)
                            {
                                u4MaxPeakDataRate =
                                    pTempFilterInfoNode->ResInfo.u4PeakDataRate;
                                pMaxResInfo = &(pTempFilterInfoNode->ResInfo);
                            }
                        }
                    }
                    if ((u4MaxPeakDataRate <
                         pTempResNode->MaxResInfo.u4PeakDataRate) &&
                        (pMaxResInfo != NULL))
                    {
                        /* Res Node Requires Lesser Bandwidth */
                        if (TcModifyRes (pTempResNode->ResHandle,
                                         TC_STD_TYPE_RES_SPEC,
                                         (tuTcResSpec *) pMaxResInfo,
                                         pTempResNode->u1ResPoolType,
                                         &u1ErrorCode) != TC_SUCCESS)
                        {
                            /* This should not happen! */
                            TC_DBG (TC_API_PRCS, "TcDissociateFilterInfo: "
                                    "Unable to allocate reduced "
                                    "Bandwidth!\n");
                            TC_DBG (TC_API_ETEXT, "TcDissociateFilterInfo: "
                                    "EXIT1\n");
                            MPLS_TC_UNLOCK ();
                            return;
                        }
                    }
                    TC_DBG (TC_API_PRCS,
                            "TcDissociateFilterInfo: Filter Info "
                            "Dissociated Successfully!\n");
                    TC_DBG (TC_API_ETEXT, "TcDissociateFilterInfo: EXIT2\n");
                    MPLS_TC_UNLOCK ();
                    return;
                }
            }
            TC_DBG (TC_API_PRCS, "TcDissociateFilterInfo: Filter Info "
                    "Not Found!\n");
            TC_DBG (TC_API_ETEXT, "TcDissociateFilterInfo: EXIT3\n");
            MPLS_TC_UNLOCK ();
            return;
        }
    }
    TC_DBG (TC_API_PRCS, "TcDissociateFilterInfo: Handle Not Found!\n");
    TC_DBG (TC_API_ETEXT, "TcAssociateFilterInfo: EXIT4\n");
    MPLS_TC_UNLOCK ();
    return;
}

/*****************************************************************************/
/* Function Name : TcGenerateAdSpec
 * Description   : This API is used to generate the AdSpec value at the given
 *                 Interface for the given Resource Pool Type.  It is used by 
 *                 RSVPTE to compute the AdSpec value to be propagated from 
 *                 the Ingres in the Forward direction.
 * Input(s)      : u4AppId       - Application Id which is requesting for Ad 
 *                                 Spec Generation.
 *                 u4InterfaceId - Interface Id in which the AdSpec is to be
 *                                 generated.
 *                 u1ResPoolType - Type of pool in which the AdSpec is to be
 *                                 generated.
 * Output(s)     : pAdSpec       - Pointer to the generated AdSpec value at 
 *                                 this interface for the Resource Type 
 *                                 (INTSERV, BEST_EFFORT, AF1, ...) to be 
 *                                 passed to the downstream LSR.
 *                 pu1ErrorCode  - Error value in case of failure.
 * Return(s)     : TC_SUCCESS or TC_FAILURE
 *****************************************************************************/
UINT1
TcGenerateAdSpec (UINT4 u4AppId, UINT4 u4InterfaceId, UINT1 u1ResPoolType,
                  tTcAdSpec * pAdSpec, UINT1 *pu1ErrorCode)
{
    TC_SUPPRESS_WARNING (u4AppId);
    TC_SUPPRESS_WARNING (u4InterfaceId);
    TC_SUPPRESS_WARNING (u1ResPoolType);

    TC_DBG (TC_API_ETEXT, "TcGenerateAdSpec: ENTRY\n");

    /* AdSpec message header */
    TC_ADSPEC_MSG_HDR_VER (pAdSpec) = TC_ADSPEC_MSG_HDR_VER_VAL;
    TC_ADSPEC_MSG_HDR_LEN (pAdSpec) = TC_ADSPEC_MSG_HDR_LEN_VAL;

    /* AdSpec Service Header */
    TC_ADSPEC_DFLT_PARAM_SRV_HDR_ID (pAdSpec) =
        TC_ADSPEC_DFLT_PARAM_SRV_HDR_ID_VAL;
    TC_ADSPEC_DFLT_PARAM_SRV_HDR_LEN (pAdSpec) =
        TC_ADSPEC_DFLT_PARAM_SRV_HDR_LEN_VAL;

    /* The HopCount structure */
    TC_ADSPEC_DFLT_PARAM_HOP_HDR_NUM (pAdSpec) =
        TC_ADSPEC_DFLT_PARAM_HOP_HDR_NUM_VAL;
    TC_ADSPEC_DFLT_PARAM_HOP_HDR_FLAG (pAdSpec) =
        TC_ADSPEC_DFLT_PARAM_HOP_HDR_FLAG_VAL;
    TC_ADSPEC_DFLT_PARAM_HOP_HDR_LEN (pAdSpec) =
        TC_ADSPEC_DFLT_PARAM_HOP_HDR_LEN_VAL;

    /* The Path Bandwidth estimate  */
    TC_ADSPEC_DFLT_PARAM_PATH_BW_HDR_NUM (pAdSpec) =
        TC_ADSPEC_DFLT_PARAM_PATH_BW_HDR_NUM_VAL;
    TC_ADSPEC_DFLT_PARAM_PATH_BW_HDR_FLAG (pAdSpec) =
        TC_ADSPEC_DFLT_PARAM_PATH_BW_HDR_FLAG_VAL;
    TC_ADSPEC_DFLT_PARAM_PATH_BW_HDR_LEN (pAdSpec) =
        TC_ADSPEC_DFLT_PARAM_PATH_BW_HDR_LEN_VAL;

    /* The Min Path Latency  */
    TC_ADSPEC_DFLT_PARAM_PATH_LATENCY_HDR_NUM (pAdSpec) =
        TC_ADSPEC_DFLT_PARAM_PATH_LATENCY_HDR_NUM_VAL;
    TC_ADSPEC_DFLT_PARAM_PATH_LATENCY_HDR_FLAG (pAdSpec) =
        TC_ADSPEC_DFLT_PARAM_PATH_LATENCY_HDR_FLAG_VAL;
    TC_ADSPEC_DFLT_PARAM_PATH_LATENCY_HDR_LEN (pAdSpec) =
        TC_ADSPEC_DFLT_PARAM_PATH_LATENCY_HDR_LEN_VAL;

    /* The Composed MTU  */
    TC_ADSPEC_DFLT_PARAM_TRANS_UNIT_HDR_NUM (pAdSpec) =
        TC_ADSPEC_DFLT_PARAM_TRANS_UNIT_HDR_NUM_VAL;
    TC_ADSPEC_DFLT_PARAM_TRANS_UNIT_HDR_FLAG (pAdSpec) =
        TC_ADSPEC_DFLT_PARAM_TRANS_UNIT_HDR_FLAG_VAL;
    TC_ADSPEC_DFLT_PARAM_TRANS_UNIT_HDR_LEN (pAdSpec) =
        TC_ADSPEC_DFLT_PARAM_TRANS_UNIT_HDR_LEN_VAL;

    /* Fill The CLS AdSpec */
    TC_ADSPEC_CLS_SRV_HDR_ID (pAdSpec) = TC_ADSPEC_CLS_SRV_HDR_ID_VAL;
    TC_ADSPEC_CLS_SRV_HDR_FLAG (pAdSpec) = TC_ADSPEC_CLS_SRV_HDR_FLAG_VAL;
    TC_ADSPEC_CLS_SRV_HDR_LEN (pAdSpec) = TC_ADSPEC_CLS_SRV_HDR_LEN_VAL;

    /* Fill The GS Adspec values */
    TC_ADSPEC_GS_SRV_HDR_ID (pAdSpec) = TC_ADSPEC_GS_SRV_HDR_ID_VAL;
    TC_ADSPEC_GS_SRV_HDR_FLAG (pAdSpec) = TC_ADSPEC_GS_SRV_HDR_FLAG_VAL;
    TC_ADSPEC_GS_SRV_HDR_LEN (pAdSpec) = TC_ADSPEC_GS_SRV_HDR_LEN_VAL;

    TC_ADSPEC_GS_CTOT_HDR_NUM (pAdSpec) = TC_ADSPEC_GS_CTOT_HDR_NUM_VAL;
    TC_ADSPEC_GS_CTOT_HDR_FLAG (pAdSpec) = TC_ADSPEC_GS_CTOT_HDR_FLAG_VAL;
    TC_ADSPEC_GS_CTOT_HDR_LEN (pAdSpec) = TC_ADSPEC_GS_CTOT_HDR_LEN_VAL;

    TC_ADSPEC_GS_DTOT_HDR_NUM (pAdSpec) = TC_ADSPEC_GS_DTOT_HDR_NUM_VAL;
    TC_ADSPEC_GS_DTOT_HDR_FLAG (pAdSpec) = TC_ADSPEC_GS_DTOT_HDR_FLAG_VAL;
    TC_ADSPEC_GS_DTOT_HDR_LEN (pAdSpec) = TC_ADSPEC_GS_DTOT_HDR_LEN_VAL;

    TC_ADSPEC_GS_CSUM_HDR_NUM (pAdSpec) = TC_ADSPEC_GS_CSUM_HDR_NUM_VAL;
    TC_ADSPEC_GS_CSUM_HDR_FLAG (pAdSpec) = TC_ADSPEC_GS_CSUM_HDR_FLAG_VAL;
    TC_ADSPEC_GS_CSUM_HDR_LEN (pAdSpec) = TC_ADSPEC_GS_CSUM_HDR_LEN_VAL;

    TC_ADSPEC_GS_DSUM_HDR_NUM (pAdSpec) = TC_ADSPEC_GS_DSUM_HDR_NUM_VAL;
    TC_ADSPEC_GS_DSUM_HDR_FLAG (pAdSpec) = TC_ADSPEC_GS_DSUM_HDR_FLAG_VAL;
    TC_ADSPEC_GS_DSUM_HDR_LEN (pAdSpec) = TC_ADSPEC_GS_DSUM_HDR_LEN_VAL;

    TC_DBG (TC_API_PRCS, "TcGenerateAdSpec: AdSpec Generated Successfully!\n");
    TC_DBG (TC_API_ETEXT, "TcGenerateAdSpec: EXIT1\n");
    *pu1ErrorCode = TC_NO_ERR;
    return TC_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TcCalcAdSpec
 * Description   : This API is used to calculate the AdSpec value at the given 
 *                 Interface for the given Resource Pool Type corresponding to
 *                 the Input AdSpec and Tspec.  It is used by RSVPTE to compute
 *                 the AdSpec value to be propagated by Intermediate LSRs based
 *                 on the incoming AdSPec and Tspec values.
 * Input(s)      : u4AppId       - Application Id which is requesting for Ad 
 *                                 Spec Calculation.
 *                 u4InterfaceId - Interface Id in which the AdSpec is to be
 *                                 Calculated.
 *                 u1ResPoolType - Type of pool in which the AdSpec is to be
 *                                 Calculated.
 *                 pAdSpec       - Pointer to the input AdSpec. 
 *                 pTSpec        - Pointer to the Tspec.
 * Output(s)     : pNewAdSpec    - Pointer to the calculated AdSpec. 
 *                 pu1ErrorCode  - Error value in case of failure.
 * Return(s)     : TC_SUCCESS or TC_FAILURE
 *****************************************************************************/
UINT1
TcCalcAdSpec (UINT4 u4AppId, UINT4 u4InterfaceId, UINT1 u1ResPoolType,
              tTcAdSpec * pAdSpec, tTcTSpec * pTSpec, tTcAdSpec * pNewAdSpec,
              UINT1 *pu1ErrorCode)
{
    TC_SUPPRESS_WARNING (u4AppId);
    TC_SUPPRESS_WARNING (u4InterfaceId);
    TC_SUPPRESS_WARNING (u1ResPoolType);
    TC_SUPPRESS_WARNING (pTSpec);

    TC_DBG (TC_API_ETEXT, "TcCalcAdSpec: ENTRY\n");

    /* AdSpec message header */
    TC_ADSPEC_MSG_HDR_VER (pNewAdSpec) = TC_ADSPEC_MSG_HDR_VER (pAdSpec);
    TC_ADSPEC_MSG_HDR_LEN (pNewAdSpec) = TC_ADSPEC_MSG_HDR_LEN (pAdSpec);

    /* AdSpec Service Header */
    TC_ADSPEC_DFLT_PARAM_SRV_HDR_ID (pNewAdSpec) =
        TC_ADSPEC_DFLT_PARAM_SRV_HDR_ID (pAdSpec);
    TC_ADSPEC_DFLT_PARAM_SRV_HDR_LEN (pNewAdSpec) =
        TC_ADSPEC_DFLT_PARAM_SRV_HDR_LEN (pAdSpec);

    /* The HopCount structure */
    TC_ADSPEC_DFLT_PARAM_HOP_HDR_NUM (pNewAdSpec) =
        TC_ADSPEC_DFLT_PARAM_HOP_HDR_NUM (pAdSpec);
    TC_ADSPEC_DFLT_PARAM_HOP_HDR_FLAG (pNewAdSpec) =
        TC_ADSPEC_DFLT_PARAM_HOP_HDR_FLAG (pAdSpec);
    TC_ADSPEC_DFLT_PARAM_HOP_HDR_LEN (pNewAdSpec) =
        TC_ADSPEC_DFLT_PARAM_HOP_HDR_LEN (pAdSpec);

    /* The Path Bandwidth estimate  */
    TC_ADSPEC_DFLT_PARAM_PATH_BW_HDR_NUM (pNewAdSpec) =
        TC_ADSPEC_DFLT_PARAM_PATH_BW_HDR_NUM (pAdSpec);
    TC_ADSPEC_DFLT_PARAM_PATH_BW_HDR_FLAG (pNewAdSpec) =
        TC_ADSPEC_DFLT_PARAM_PATH_BW_HDR_FLAG (pAdSpec);
    TC_ADSPEC_DFLT_PARAM_PATH_BW_HDR_LEN (pNewAdSpec) =
        TC_ADSPEC_DFLT_PARAM_PATH_BW_HDR_LEN (pAdSpec);

    /* The Min Path Latency  */
    TC_ADSPEC_DFLT_PARAM_PATH_LATENCY_HDR_NUM (pNewAdSpec) =
        TC_ADSPEC_DFLT_PARAM_PATH_LATENCY_HDR_NUM (pAdSpec);
    TC_ADSPEC_DFLT_PARAM_PATH_LATENCY_HDR_FLAG (pNewAdSpec) =
        TC_ADSPEC_DFLT_PARAM_PATH_LATENCY_HDR_FLAG (pAdSpec);
    TC_ADSPEC_DFLT_PARAM_PATH_LATENCY_HDR_LEN (pNewAdSpec) =
        TC_ADSPEC_DFLT_PARAM_PATH_LATENCY_HDR_LEN (pAdSpec);

    /* The Composed MTU  */
    TC_ADSPEC_DFLT_PARAM_TRANS_UNIT_HDR_NUM (pNewAdSpec) =
        TC_ADSPEC_DFLT_PARAM_TRANS_UNIT_HDR_NUM (pAdSpec);
    TC_ADSPEC_DFLT_PARAM_TRANS_UNIT_HDR_FLAG (pNewAdSpec) =
        TC_ADSPEC_DFLT_PARAM_TRANS_UNIT_HDR_FLAG (pAdSpec);
    TC_ADSPEC_DFLT_PARAM_TRANS_UNIT_HDR_LEN (pNewAdSpec) =
        TC_ADSPEC_DFLT_PARAM_TRANS_UNIT_HDR_LEN (pAdSpec);

    /* Fill The CLS AdSpec */
    TC_ADSPEC_CLS_SRV_HDR_ID (pNewAdSpec) = TC_ADSPEC_CLS_SRV_HDR_ID (pAdSpec);
    TC_ADSPEC_CLS_SRV_HDR_FLAG (pNewAdSpec) =
        TC_ADSPEC_CLS_SRV_HDR_FLAG (pAdSpec);
    TC_ADSPEC_CLS_SRV_HDR_LEN (pNewAdSpec) =
        TC_ADSPEC_CLS_SRV_HDR_LEN (pAdSpec);

    /* Fill The GS Adspec values */
    TC_ADSPEC_GS_SRV_HDR_ID (pNewAdSpec) = TC_ADSPEC_GS_SRV_HDR_ID (pAdSpec);
    TC_ADSPEC_GS_SRV_HDR_FLAG (pNewAdSpec) =
        TC_ADSPEC_GS_SRV_HDR_FLAG (pAdSpec);
    TC_ADSPEC_GS_SRV_HDR_LEN (pNewAdSpec) = TC_ADSPEC_GS_SRV_HDR_LEN (pAdSpec);

    TC_ADSPEC_GS_CTOT_HDR_NUM (pNewAdSpec) =
        TC_ADSPEC_GS_CTOT_HDR_NUM (pAdSpec);
    TC_ADSPEC_GS_CTOT_HDR_FLAG (pNewAdSpec) =
        TC_ADSPEC_GS_CTOT_HDR_FLAG (pAdSpec);
    TC_ADSPEC_GS_CTOT_HDR_LEN (pNewAdSpec) =
        TC_ADSPEC_GS_CTOT_HDR_LEN (pAdSpec);

    TC_ADSPEC_GS_DTOT_HDR_NUM (pNewAdSpec) =
        TC_ADSPEC_GS_DTOT_HDR_NUM (pAdSpec);
    TC_ADSPEC_GS_DTOT_HDR_FLAG (pNewAdSpec) =
        TC_ADSPEC_GS_DTOT_HDR_FLAG (pAdSpec);
    TC_ADSPEC_GS_DTOT_HDR_LEN (pNewAdSpec) =
        TC_ADSPEC_GS_DTOT_HDR_LEN (pAdSpec);

    TC_ADSPEC_GS_CSUM_HDR_NUM (pNewAdSpec) =
        TC_ADSPEC_GS_CSUM_HDR_NUM (pAdSpec);
    TC_ADSPEC_GS_CSUM_HDR_FLAG (pNewAdSpec) =
        TC_ADSPEC_GS_CSUM_HDR_FLAG (pAdSpec);
    TC_ADSPEC_GS_CSUM_HDR_LEN (pNewAdSpec) =
        TC_ADSPEC_GS_CSUM_HDR_LEN (pAdSpec);

    TC_ADSPEC_GS_DSUM_HDR_NUM (pNewAdSpec) =
        TC_ADSPEC_GS_DSUM_HDR_NUM (pAdSpec);
    TC_ADSPEC_GS_DSUM_HDR_FLAG (pNewAdSpec) =
        TC_ADSPEC_GS_DSUM_HDR_FLAG (pAdSpec);
    TC_ADSPEC_GS_DSUM_HDR_LEN (pNewAdSpec) =
        TC_ADSPEC_GS_DSUM_HDR_LEN (pAdSpec);

    TC_DBG (TC_API_PRCS, "TcCalcAdSpec: AdSpec Calculated Successfully!\n");
    TC_DBG (TC_API_ETEXT, "TcCalcAdSpec: EXIT1\n");
    *pu1ErrorCode = TC_NO_ERR;
    return TC_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TcGenerateFlowSpec 
 * Description   : This API is used to generate the FlowSpec value to be  
 *                 propagated by the Egres based on the incoming AdSpec and
 *                 Tspec values.
 * Input(s)      : u4AppId       - Application Id which is requesting for 
 *                                 Flow Spec Generation.
 *                 pAdSpec       - Pointer to the incoming AdSpec.
 *                 pTSpec        - Pointer to the Tspec. 
 * Output(s)     : pFlowSpec     - Pointer to the generated FlowSpec. 
 *                 pu1ErrorCode  - Error value in case of failure.
 * Return(s)     : TC_SUCCESS or TC_FAILURE
 *****************************************************************************/
UINT1
TcGenerateFlowSpec (UINT4 u4AppId, tTcAdSpec * pAdSpec, tTcTSpec * pTSpec,
                    tTcFlowSpec * pFlowSpec, UINT1 *pu1ErrorCode)
{
    TC_SUPPRESS_WARNING (u4AppId);
    TC_SUPPRESS_WARNING (pAdSpec);

    TC_DBG (TC_API_ETEXT, "TcGenerateFlowSpec: ENTRY\n");

    MEMCPY (&(pFlowSpec->MsgHdr), &(pTSpec->MsgHdr), sizeof (tMsgHdr));
    MEMCPY (&(pFlowSpec->SrvHdr), &(pTSpec->SrvHdr), sizeof (tSrvHdr));
    MEMCPY (&(pFlowSpec->ParamHdr), &(pTSpec->ParamHdr), sizeof (tParamHdr));
    MEMCPY (&(pFlowSpec->ServiceParams.ClsService.TBParams),
            &(pTSpec->TBParams), sizeof (tTBParams));
    /* Service number is changed to Controlled Load */
    pFlowSpec->SrvHdr.u1SrvID = TC_CNTRLD_LOAD_SRV_ID;    /* Controlled Load */

    TC_DBG (TC_API_PRCS, "TcGenerateFlowSpec: FlowSpec Generated!\n");
    TC_DBG (TC_API_ETEXT, "TcGenerateFlowSpec: EXIT1\n");
    *pu1ErrorCode = TC_NO_ERR;
    return TC_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TcCalculateFlowSpec
 * Description   : This API is used to calculate the FlowSpec value at the 
 *                 given Interface for the given Resource Pool Type to be 
 *                 propagated to the upstream LSR based on the input AdSpec, 
 *                 TSpec and FlowSpec values. 
 * Input(s)      : u4AppId       - Application Id which is requesting for  
 *                                 Flow Spec Calculation.
 *                 u4InterfaceId - Interface Id in which the FlowSpec is to be
 *                                 computed.
 *                 u1ResPoolType - Type of pool in which the FlowSpec
 *                                 is to be calculated.
 *                 pAdSpec       - Pointer to the input AdSpec.
 *                 pTSpec        - Pointer to the input Tspec.
 *                 pFlowSpec     - Pointer to the input Flow Spec.
 * Output(s)     : pNewFlowSpec  - Pointer to the calculated FlowSpec.  
 *                 pu1ErrorCode  - Error value in case of failure.
 * Return(s)     : TC_SUCCESS or TC_FAILURE
 *****************************************************************************/
UINT1
TcCalculateFlowSpec (UINT4 u4AppId, UINT4 u4InterfaceId, UINT1
                     u1ResPoolType, tTcAdSpec * pAdSpec, tTcTSpec * pTSpec,
                     tTcFlowSpec * pFlowSpec, tTcFlowSpec * pNewFlowSpec,
                     UINT1 *pu1ErrorCode)
{
    TC_SUPPRESS_WARNING (u4AppId);
    TC_SUPPRESS_WARNING (u4InterfaceId);
    TC_SUPPRESS_WARNING (u1ResPoolType);
    TC_SUPPRESS_WARNING (pAdSpec);
    TC_SUPPRESS_WARNING (pTSpec);

    TC_DBG (TC_API_ETEXT, "TcCalculateFlowSpec: ENTRY\n");

    MEMCPY (pNewFlowSpec, pFlowSpec, sizeof (tTcFlowSpec));

    TC_DBG (TC_API_PRCS, "TcCalculateFlowSpec: FlowSpec Calculated!\n");
    TC_DBG (TC_API_ETEXT, "TcCalculateFlowSpec: EXIT1\n");
    *pu1ErrorCode = TC_NO_ERR;
    return TC_SUCCESS;
}

/*****************************************************************************/
/* Function Name : TcFreeAppResources 
 * Description   : This API is invoked by the Control Protocols / Applications 
 *                 for releasing all resources associated with this 
 *                 Application. 
 * Input(s)      : u4AppId - Application Id of Applicationi / Control protocol
 *                 whose associated resources need to be freed 
 * Output(s)     : NONE
 * Return(s)     : NONE
 *****************************************************************************/
VOID
TcFreeAppResources (UINT4 u4AppId)
{
    tTcPoolResInfo     *pTcPoolResInfo = NULL;
    tTcAllocResNode    *pTempResNode = NULL;
    tTcFilterInfoNode  *pTempFilterInfoNode = NULL;
    UINT1               u1Done = TC_FALSE;

    TC_DBG (TC_API_ETEXT, "TcFreeAppResources: ENTRY\n");
    MPLS_TC_LOCK ();
    while (u1Done != TC_TRUE)
    {
        u1Done = TC_TRUE;
        TMO_SLL_Scan (&(gTcGlobal.AllocResList), pTempResNode,
                      tTcAllocResNode *)
        {
            if (pTempResNode->u4AppId == u4AppId)
            {
                /* Found the Resource Entry */
                TcGetPoolResInfo (pTempResNode->u4InterfaceId,
                                  pTempResNode->u1ResPoolType, &pTcPoolResInfo);
                if (pTcPoolResInfo == NULL)
                {
                    continue;
                }

                pTcPoolResInfo->AvailResInfo.u4PeakDataRate +=
                    pTempResNode->MaxResInfo.u4PeakDataRate;
                TMO_SLL_Delete (&(gTcGlobal.AllocResList),
                                &(pTempResNode->NextAllocResNode));
                while (TMO_SLL_Count (&(pTempResNode->FilterInfoList))
                       != TC_ZERO)
                {
                    pTempFilterInfoNode = (tTcFilterInfoNode *)
                        TMO_SLL_First (&(pTempResNode->FilterInfoList));
                    TMO_SLL_Delete (&(pTempResNode->FilterInfoList),
                                    &(pTempFilterInfoNode->NextFilterInfoNode));
                    TcFreeMemBlock (TCMemPoolIds
                                    [MAX_TC_FILTER_SPEC_NODE_SIZING_ID],
                                    (UINT1 *) pTempFilterInfoNode,
                                    tTcFilterInfoNode);
                }
                TcFreeMemBlock (TCMemPoolIds[MAX_TC_ALLOC_RES_NODE_SIZING_ID],
                                (UINT1 *) pTempResNode, tTcAllocResNode);
                TC_DBG (TC_API_PRCS, "TcFreeAppResources: Resources Freed "
                        "Successfully!\n");
                u1Done = TC_FALSE;
                break;
            }
        }
    }

    TC_DBG (TC_UTLS_ETEXT, "TcFreeAppResources: EXIT1\n");
    MPLS_TC_UNLOCK ();
    return;
}

/*---------------------------------------------------------------------------*/
/*                        End of file tcapi.c                                */
/*---------------------------------------------------------------------------*/
