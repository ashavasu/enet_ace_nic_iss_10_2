/********************************************************************
 *                                                                  *
 * $RCSfile: tcmain.c,v $
 *                                                                  *
 * $Date: 2010/09/24 06:35:09 $
 *                                                                  *
 * $Revision: 1.6 $
 *                                                                  *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *****************************************************************************
 *    FILE  NAME             : tcmain.c
 *    PRINCIPAL AUTHOR       : Aricent Inc.
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : TC
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains the main functions related
 *                             to the module.
 *---------------------------------------------------------------------------*/

#include "tcincs.h"

tTcGlobal           gTcGlobal;
UINT2               gau2OffSet[TC_FOUR] = {
    TC_FIELD_OFFSET (tTcAllocResNode, ResHandle),
    TC_FIELD_OFFSET (tTcAllocResNode, u4AppId),
    TC_FIELD_OFFSET (tTcAllocResNode, u4InterfaceId),
    TC_FIELD_OFFSET (tTcAllocResNode, u1ResPoolType)
};

UINT2               gau2Length[TC_FOUR] = {
    TC_FIELD_LENGTH (tTcAllocResNode, ResHandle),
    TC_FIELD_LENGTH (tTcAllocResNode, u4AppId),
    TC_FIELD_LENGTH (tTcAllocResNode, u4InterfaceId),
    TC_FIELD_LENGTH (tTcAllocResNode, u1ResPoolType)
};

UINT1               gau1ByteOrder[TC_FOUR] = {
    HOST_ORDER, HOST_ORDER, HOST_ORDER, NET_ORDER
};

UINT4               gu4TcDbg = TC_DEF_DBG_FLAG;
UINT4               gu4TcConfigFlag;

/*****************************************************************************
 * Function Name : TcMain
 * Description   : This is the Entry point function for the TC module. 
 * Input(s)      : NONE
 * Output(s)     : NONE
 * Return(s)     : TC_SUCCESS or TC_FAILURE
 *****************************************************************************/
UINT1
TcMain (VOID)
{
    TC_DBG (TC_MAIN_ETEXT, "TcMain: ENTRY\n");
    if (TcInit () != TC_SUCCESS)
    {
        TC_DBG (TC_MAIN_PRCS, "TcMain: Initialization Failure!\n");
        TC_DBG (TC_MAIN_ETEXT, "TcMain: EXIT1\n");
        return TC_FAILURE;
    }

    gu4TcConfigFlag = TC_SCENARIO_ONE;
    return TcConfigure (gu4TcConfigFlag);
}

/*****************************************************************************
 * Function Name : TcInit
 * Description   : This function initialises the global variables to the
                   default values and also creates MemPools.
 * Input(s)      : NONE
 * Output(s)     : NONE
 * Return(s)     : TC_SUCCESS or TC_FAILURE
 *****************************************************************************/
UINT1
TcInit (VOID)
{
    UINT4               u4Index;

    TC_DBG (TC_MAIN_ETEXT, "TcInit: ENTRY\n");

    gTcGlobal.pAdmCtrlAlgo = NULL;
    TMO_SLL_Init (&(gTcGlobal.AllocResList));

    for (u4Index = TC_ZERO; u4Index < TC_MAX_APPS; u4Index++)
    {
        gTcGlobal.aAppInfoArray[u4Index].u1Free = TC_TRUE;
    }
    for (u4Index = TC_ZERO; u4Index < TC_MAX_INTERFACES; u4Index++)
    {
        gTcGlobal.aInterfaceInfo[u4Index].u1Free = TC_TRUE;
    }

    if (TcSizingMemCreateMemPools () == OSIX_FAILURE)
    {
        return TC_FAILURE;
    }

    /* Semaphore Create */
    if (OsixCreateSem (MPLS_TC_SEM_NAME, 1, 0, &(gTcGlobal.gTcSemId)) !=
        OSIX_SUCCESS)

    {
        TcSizingMemDeleteMemPools ();
        TC_DBG (TC_MAIN_PRCS, "TcInit: Create Semaphore Failed \n");
        return TC_FAILURE;
    }

    TC_DBG (TC_MAIN_PRCS, "TcInit: Initialization Successful!\n");
    TC_DBG (TC_MAIN_ETEXT, "TcInit: EXIT3\n");
    return TC_SUCCESS;
}

/*****************************************************************************
 * Function Name : TcShutdown
 * Description   : This function gracefully shuts down the TC module. 
 * Input(s)      : NONE
 * Output(s)     : NONE
 * Return(s)     : NONE
 *****************************************************************************/
VOID
TcShutdown (VOID)
{
    UINT4               u4Index;

    TC_DBG (TC_MAIN_ETEXT, "TcShutdown: ENTRY\n");

    for (u4Index = TC_ZERO; u4Index < TC_MAX_APPS; u4Index++)
    {
        if (gTcGlobal.aAppInfoArray[u4Index].u1Free != TC_TRUE)
        {
            TcDeregisterApp (gTcGlobal.aAppInfoArray[u4Index].u4AppId);
            gTcGlobal.aAppInfoArray[u4Index].u1Free = TC_TRUE;
        }
    }

    for (u4Index = TC_ZERO; u4Index < TC_MAX_INTERFACES; u4Index++)
    {
        gTcGlobal.aInterfaceInfo[u4Index].u1Free = TC_TRUE;
    }

    TcSizingMemDeleteMemPools ();

    gTcGlobal.pAdmCtrlAlgo = NULL;
    TMO_SLL_Init (&(gTcGlobal.AllocResList));

    if (gTcGlobal.gTcSemId != TC_ZERO)
    {
        OsixSemDel (gTcGlobal.gTcSemId);
        gTcGlobal.gTcSemId = TC_ZERO;
    }
    TC_DBG (TC_MAIN_PRCS, "TcShutdown: Shutdown Successful!\n");
    TC_DBG (TC_MAIN_ETEXT, "TcShutdown: EXIT1\n");
}

/*---------------------------------------------------------------------------*/
/*                        End of file tcmain.c                               */
/*---------------------------------------------------------------------------*/
