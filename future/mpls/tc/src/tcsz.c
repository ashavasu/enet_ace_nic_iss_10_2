/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tcsz.c,v 1.4 2014/12/24 10:58:30 siva Exp $
 *
 * Description: This files for RADIUS.
 *******************************************************************/

#define _TCSZ_C
#include "tcincs.h"
extern INT4         IssSzRegisterModuleSizingParams (CHR1 * pu1ModName,
                                                     tFsModSizingParams *
                                                     pModSizingParams);
extern INT4         IssSzRegisterModulePoolId (CHR1 * pu1ModName,
                                               tMemPoolId * pModPoolId);
INT4
TcSizingMemCreateMemPools ()
{
    UINT4               u4RetVal;
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < TC_MAX_SIZING_ID; i4SizingId++)
    {
        u4RetVal = MemCreateMemPool (FsTCSizingParams[i4SizingId].u4StructSize,
                                     FsTCSizingParams[i4SizingId].
                                     u4PreAllocatedUnits,
                                     MEM_DEFAULT_MEMORY_TYPE,
                                     &(TCMemPoolIds[i4SizingId]));
        if (u4RetVal == MEM_FAILURE)
        {
            TcSizingMemDeleteMemPools ();
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

INT4
TcSzRegisterModuleSizingParams (CHR1 * pu1ModName)
{
    /* Copy the Module Name */
    IssSzRegisterModuleSizingParams (pu1ModName, FsTCSizingParams);
    IssSzRegisterModulePoolId (pu1ModName, TCMemPoolIds);
    return OSIX_SUCCESS;
}

VOID
TcSizingMemDeleteMemPools ()
{
    INT4                i4SizingId;

    for (i4SizingId = 0; i4SizingId < TC_MAX_SIZING_ID; i4SizingId++)
    {
        if (TCMemPoolIds[i4SizingId] != 0)
        {
            MemDeleteMemPool (TCMemPoolIds[i4SizingId]);
            TCMemPoolIds[i4SizingId] = 0;
        }
    }
    return;
}
