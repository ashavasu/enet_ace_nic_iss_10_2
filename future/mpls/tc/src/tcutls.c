/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: tcutls.c,v 1.4 2010/10/15 06:29:47 prabuc Exp $
 *
 * Description: This file contains the utility functions for the module.
 ********************************************************************/

#include "tcincs.h"

/*****************************************************************************
 * Function Name : TcSearchAppInfoEntry
 * Description   : This function searches the App Info Database for the 
 *                 existence of an Application with the given AppId.
 * Input(s)      : u4AppId - Application Id to be searched.
 * Output(s)     : NONE
 * Return(s)     : TC_SUCCESS or TC_FAILURE
 *****************************************************************************/
UINT1
TcSearchAppInfoEntry (UINT4 u4AppId)
{
    UINT4               u4Index;

    TC_DBG (TC_UTLS_ETEXT, "TcSearchAppInfoEntry: ENTRY\n");
    for (u4Index = 0; u4Index < TC_MAX_APPS; u4Index++)
    {
        if ((gTcGlobal.aAppInfoArray[u4Index].u1Free == TC_FALSE) &&
            (gTcGlobal.aAppInfoArray[u4Index].u4AppId == u4AppId))
        {
            TC_DBG (TC_UTLS_PRCS, "TcSearchAppInfoEntry: Found AppId Match!\n");
            TC_DBG (TC_UTLS_ETEXT, "TcSearchAppInfoEntry: EXIT1\n");
            return TC_SUCCESS;
        }
    }
    TC_DBG (TC_UTLS_PRCS, "TcSearchAppInfoEntry: AppId Match Not Found!\n");
    TC_DBG (TC_UTLS_ETEXT, "TcSearchAppInfoEntry: EXIT2\n");
    return TC_FAILURE;
}

/*****************************************************************************
 * Function Name : TcSearchInterfaceInfoEntry 
 * Description   : This function searches the Interface Info Database for 
 *                 the existence of a given Interface Id.
 * Input(s)      : u4InterfaceId  - Interface Id which is to be searched for.
 * Output(s)     : NONE
 * Return(s)     : TC_SUCCESS or TC_FAILURE
 *****************************************************************************/
UINT1
TcSearchInterfaceInfoEntry (UINT4 u4InterfaceId)
{
    UINT4               u4Index;

    TC_DBG (TC_UTLS_ETEXT, "TcSearchInterfaceInfoEntry: ENTRY\n");
    for (u4Index = 0; u4Index < TC_MAX_INTERFACES; u4Index++)
    {
        if ((gTcGlobal.aInterfaceInfo[u4Index].u1Free == TC_FALSE) &&
            (gTcGlobal.aInterfaceInfo[u4Index].u4InterfaceId == u4InterfaceId))
        {
            TC_DBG (TC_UTLS_PRCS, "TcSearchInterfaceInfoEntry: Found Match!\n");
            TC_DBG (TC_UTLS_ETEXT, "TcSearchInterfaceInfoEntry: EXIT1\n");
            return TC_SUCCESS;
        }
    }
    TC_DBG (TC_UTLS_PRCS, "TcSearchInterfaceInfoEntry: Match Not Found!\n");
    TC_DBG (TC_UTLS_ETEXT, "TcSearchInterfaceInfoEntry: EXIT2\n");
    return TC_FAILURE;
}

/*****************************************************************************
 * Function Name : TcAddAppInfoEntry 
 * Description   : This function is used to add an Application Entry to the
 *                 App Info Database.
 * Input(s)      : u4AppId     - Application Id which is to be added.
 *                 pCallBackFn - Call Back Function to be associated with
 *                               the given Application Entry.  This Call Back
 *                               function is used by the Tc to return
 *                               Asynchronous events back to the Application
 * Output(s)     : NONE
 * Return(s)     : TC_SUCCESS or TC_FAILURE
 *****************************************************************************/
UINT1
TcAddAppInfoEntry (UINT4 u4AppId, tTcAppCallBackFnPtr pCallBackFn)
{
    UINT4               u4Index;

    /* TODO: Mutex Lock can be done here to prevent multiple Apps adding
     * entries at the same time */
    TC_DBG (TC_UTLS_ETEXT, "TcAddAppInfoEntry: ENTRY\n");
    for (u4Index = 0; u4Index < TC_MAX_APPS; u4Index++)
    {
        if (gTcGlobal.aAppInfoArray[u4Index].u1Free == TC_TRUE)
        {
            gTcGlobal.aAppInfoArray[u4Index].u1Free = TC_FALSE;
            gTcGlobal.aAppInfoArray[u4Index].u4AppId = u4AppId;
            gTcGlobal.aAppInfoArray[u4Index].pCallBackFn = pCallBackFn;
            TC_DBG (TC_UTLS_PRCS, "TcAddAppInfoEntry: App Info Added!\n");
            TC_DBG (TC_UTLS_ETEXT, "TcAddAppInfoEntry: EXIT1\n");
            return TC_SUCCESS;
        }
    }
    TC_DBG (TC_UTLS_PRCS, "TcAddAppInfoEntry: App Info Table Full!\n");
    TC_DBG (TC_UTLS_ETEXT, "TcAddAppInfoEntry: EXIT2\n");
    return TC_FAILURE;
}

/*****************************************************************************
 * Function Name : TcGenerateNewResHandle 
 * Description   : This function is used to Generate a New Resource Handle
 * Input(s)      : NONE
 * Output(s)     : pResHandle - Pointer to the Newly Generated Handle.
 * Return(s)     : TC_SUCCESS or TC_FAILURE
 *****************************************************************************/
UINT1
TcGenerateNewResHandle (tTcResHandle * pResHandle)
{
    tTcAllocResNode    *pAllocResNode = NULL;
    UINT4               u4Handle = TC_ZERO;

    TC_DBG (TC_UTLS_ETEXT, "TcGenerateNewResHandle: ENTRY\n");

    /* This procedure finds an unused Handle and returns it */
    pAllocResNode =
        (tTcAllocResNode *) TMO_SLL_Last (&(gTcGlobal.AllocResList));
    if (pAllocResNode != NULL)
    {
        if (TC_MAX_HANDLE > (UINT4) pAllocResNode->ResHandle)
        {
            u4Handle = (UINT4) pAllocResNode->ResHandle + TC_ONE;
            TC_DBG1 (TC_UTLS_PRCS,
                     "TcGenerateNewResHandle: New Handle Generated %d!\n",
                     u4Handle);
            TC_DBG (TC_UTLS_ETEXT, "TcGenerateNewResHandle: EXIT2\n");
            *pResHandle = (tTcResHandle) u4Handle;
            return TC_SUCCESS;
        }
    }

    TMO_SLL_Scan (&(gTcGlobal.AllocResList), pAllocResNode, tTcAllocResNode *)
    {
        if (((UINT4) (pAllocResNode->ResHandle) - u4Handle) > TC_ONE)
        {
            break;
        }
        else
        {
            u4Handle = (UINT4) pAllocResNode->ResHandle;
        }
    }
    if (u4Handle >= TC_INVALID_HANDLE)
    {
        /* No Free Handle is available */
        TC_DBG (TC_UTLS_PRCS, "TcGenerateNewResHandle: No Handle Available!\n");
        TC_DBG (TC_UTLS_ETEXT, "TcGenerateNewResHandle: EXIT1\n");
        return TC_FAILURE;
    }
    else
    {
        u4Handle++;
    }

    TC_DBG1 (TC_UTLS_PRCS, "TcGenerateNewResHandle: New Handle Generated %d!\n",
             u4Handle);
    TC_DBG (TC_UTLS_ETEXT, "TcGenerateNewResHandle: EXIT2\n");
    *pResHandle = (tTcResHandle) u4Handle;
    return TC_SUCCESS;
}

/*****************************************************************************
 * Function Name : TcGetPoolResInfo 
 * Description   : This function gets the Resource Info entry corresponding to
 *                 a given Resource Pool Type on a given Interface.
 * Input(s)      : u4InterfaceId    - Interface Id corresponding to the
 *                                    Resource Info required.
 *                 u1ResPoolType    - Resource Pool type corresponding to the
 *                                    Resource Info required.
 * Output(s)     : ppTcPoolResInfo  - Pointer to the Resource Info Pointer
 * Return(s)     : TC_SUCCESS or TC_FAILURE
 *****************************************************************************/
UINT1
TcGetPoolResInfo (UINT4 u4InterfaceId, UINT1 u1ResPoolType,
                  tTcPoolResInfo ** ppTcPoolResInfo)
{
    UINT4               u4Index;

    TC_DBG (TC_UTLS_ETEXT, "TcGetPoolResInfo: ENTRY\n");
    *ppTcPoolResInfo = NULL;
    for (u4Index = 0; u4Index < TC_MAX_INTERFACES; u4Index++)
    {
        if ((gTcGlobal.aInterfaceInfo[u4Index].u1Free == TC_FALSE) &&
            (gTcGlobal.aInterfaceInfo[u4Index].u4InterfaceId == u4InterfaceId))
        {
            *ppTcPoolResInfo = &(gTcGlobal.aInterfaceInfo[u4Index].
                                 aPoolResArray[u1ResPoolType]);
            TC_DBG (TC_UTLS_PRCS, "TcGetPoolResInfo: Found Match!\n");
            TC_DBG (TC_UTLS_ETEXT, "TcGetPoolResInfo: EXIT1\n");
            return TC_SUCCESS;
        }
    }
    TC_DBG (TC_UTLS_PRCS, "TcGetPoolResInfo: Match Not Found!\n");
    TC_DBG (TC_UTLS_ETEXT, "TcGetPoolResInfo: EXIT2\n");
    return TC_FAILURE;
}

/*****************************************************************************
 * Function Name : TcDefaultAdmCtrlAlgo 
 * Description   : This function performs Default Admission Control by
 *                 checking if the required Peak Data Rate is less than or 
 *                 equal to the Available Peak Data Rate on the given
 *                 Resource Pool.
 * Input(s)      : u4AppId       - App Id for which Admission Control is
 *                                 performed.
 *                 u4InterfaceId - Interface Id at which Admission Control
 *                                 is performed.
 *                 u1ResPoolType - Resource Pool Type for which Admission
 *                                 Control is performed.
 *                 pReqResInfo   - Resources Requested which have to be
 *                                 tested for Admissibility.
 * Output(s)     : pu1ErrorCode  - Pointer to the Error Code in case of 
 *                                 failure.
 * Return(s)     : TC_SUCCESS or TC_FAILURE
 *****************************************************************************/
UINT1
TcDefaultAdmCtrlAlgo (UINT4 u4AppId, UINT4 u4InterfaceId,
                      UINT1 u1ResPoolType, tTcResInfo * pReqResInfo,
                      UINT1 *pu1ErrorCode)
{
    tTcPoolResInfo     *pTcPoolResInfo = NULL;

    TC_SUPPRESS_WARNING (u4AppId);
    TC_DBG (TC_UTLS_ETEXT, "TcDefaultAdmCtrlAlgo: ENTRY\n");

    /* Very simple admission control which just checks if the Available
     * Peak Data Rate is greater than or equal to the requested Peak Data
     * Rate.  If so, Admission control succeeds, otherwise it fails */

    /* This will not fail because check for Interface Existence is done before
     * calling this */
    TcGetPoolResInfo (u4InterfaceId, u1ResPoolType, &pTcPoolResInfo);

    /* Currently support for encoding floating point value */
    /* based on RFC 1852 is not present, as this value is  */
    /* equivalent of an infinite positive value we are     */
    /* allowing this request                               */
    if (pReqResInfo->u4PeakDataRate == TC_POSITIVE_INFINITE)
    {
        TC_DBG (TC_UTLS_PRCS,
                "TcDefaultAdmCtrlAlgo: Admission Control Success!\n");
        TC_DBG (TC_UTLS_ETEXT, "TcDefaultAdmCtrlAlgo: EXIT5\n");
        *pu1ErrorCode = TC_NO_ERR;
        return TC_SUCCESS;
    }
    if (pTcPoolResInfo == NULL)
    {
        return TC_FAILURE;
    }

    if (pTcPoolResInfo->AvailResInfo.u4PeakDataRate <
        (pReqResInfo->u4PeakDataRate))

    {
        TC_DBG (TC_UTLS_PRCS, "TcDefaultAdmCtrlAlgo: Bandwidth Not "
                "Available!\n");
        TC_DBG (TC_UTLS_ETEXT, "TcDefaultAdmCtrlAlgo: EXIT1\n");
        *pu1ErrorCode = TC_ERR_BANDWIDTH_NOT_AVAILABLE;
        return TC_FAILURE;
    }
    TC_DBG (TC_UTLS_PRCS, "TcDefaultAdmCtrlAlgo: Admission Control Success!\n");
    TC_DBG (TC_UTLS_ETEXT, "TcDefaultAdmCtrlAlgo: EXIT5\n");
    *pu1ErrorCode = TC_NO_ERR;
    return TC_SUCCESS;
}

/*---------------------------------------------------------------------------*/
/*                        End of file tcutls.c                               */
/*---------------------------------------------------------------------------*/
