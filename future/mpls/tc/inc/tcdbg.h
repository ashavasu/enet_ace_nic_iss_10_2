/********************************************************************
 *                                                                  *
 * $RCSfile: tcdbg.h,v $
 *                                                                  *
 * $Date: 2007/02/01 14:57:16 $                                                          *
 *                                                                  *
 * $Revision: 1.2 $                                                     *
 *                                                                  *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : tcdbg.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : TC
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Contains all the data structures and
 *                             definitions required for Debugging in Tc
 *---------------------------------------------------------------------------*/

#ifndef _TCDBG_H
#define _TCDBG_H

#include "utltrc.h"

/* Macros used in TC Debugging */
#define       TC_DBG_FLAG      gu4TcDbg
#define       TC_DEF_DBG_FLAG      0x00000000

/* Values assigned to the sub-modules of TC, for Logging purpose */
#define    TC_MAIN_DEBUG           0x10000000
#define    TC_API_DEBUG            0x20000000
#define    TC_PRCS_DEBUG           0x40000000
#define    TC_IFIF_DEBUG           0x80000000
#define    TC_ERR_DEBUG            0x01000000
#define    TC_UTLS_DEBUG           0x02000000

/* Log Types defined in TC */
#define     TC_DBG_MEM     0x00000001
#define     TC_DBG_TIMER   0x00000002
#define     TC_DBG_SEM     0x00000004
#define     TC_DBG_RX      0x00000008
#define     TC_DBG_TX      0x00000010
#define     TC_DBG_PRCS    0x00000020
#define     TC_DBG_SNMP    0x00000040
#define     TC_DBG_MISC    0x00000200
#define     TC_DBG_ETEXT   0x00000400
#define     TC_DBG_IF      0x00000800

#define     TC_DBG_ALL     0xffffffff

 /* values used in TC_DBG depending on the categories under which 
  * the log message falls. 
  */

/* Main Module : */
#define     TC_MAIN_MEM     (TC_MAIN_DEBUG | TC_DBG_MEM)
#define     TC_MAIN_PRCS    (TC_MAIN_DEBUG | TC_DBG_PRCS)
#define     TC_MAIN_ETEXT   (TC_MAIN_DEBUG | TC_DBG_ETEXT)
#define     TC_MAIN_MISC    (TC_MAIN_DEBUG | TC_DBG_MISC)
#define     TC_MAIN_ALL     (TC_MAIN_DEBUG | TC_DBG_ALL)

/* API Module : */
#define     TC_API_MEM      (TC_API_DEBUG | TC_DBG_MEM)
#define     TC_API_TIMER    (TC_API_DEBUG | TC_DBG_TIMER)
#define     TC_API_RX       (TC_API_DEBUG | TC_DBG_RX)
#define     TC_API_TX       (TC_API_DEBUG | TC_DBG_TX)
#define     TC_API_PRCS     (TC_API_DEBUG | TC_DBG_PRCS)
#define     TC_API_SNMP     (TC_API_DEBUG | TC_DBG_SNMP)
#define     TC_API_ETEXT    (TC_API_DEBUG | TC_DBG_ETEXT)
#define     TC_API_MISC     (TC_API_DEBUG | TC_DBG_MISC)
#define     TC_API_ALL      (TC_API_DEBUG | TC_DBG_ALL)

/*  Utils Module: */
#define     TC_UTLS_MEM    (TC_UTLS_DEBUG | TC_DBG_MEM)
#define     TC_UTLS_RX     (TC_UTLS_DEBUG | TC_DBG_RX)
#define     TC_UTLS_TX     (TC_UTLS_DEBUG | TC_DBG_TX)
#define     TC_UTLS_PRCS   (TC_UTLS_DEBUG | TC_DBG_PRCS)
#define     TC_UTLS_ETEXT  (TC_UTLS_DEBUG | TC_DBG_ETEXT)
#define     TC_UTLS_MISC   (TC_UTLS_DEBUG | TC_DBG_MISC)
#define     TC_UTLS_ALL    (TC_UTLS_DEBUG | TC_DBG_ALL)

#define TC_ERR(pu1Format)   \
        if(TC_ERR_DEBUG == (TC_ERR_DEBUG & TC_DBG_FLAG)) \
          UtlTrcLog   (TC_ERR_DEBUG, TC_ERR_DEBUG, "TC", pu1Format)

#define TC_ERR1(pu1Format, Arg1)       \
        if(TC_ERR_DEBUG == (TC_ERR_DEBUG & TC_DBG_FLAG)) \
             UtlTrcLog (TC_ERR_DEBUG, TC_ERR_DEBUG, "TC",pu1Format,Arg1)

#define TC_DBG(u4Value, pu1Format)     \
        if(u4Value == (u4Value & TC_DBG_FLAG)) \
                  UtlTrcLog  (TC_DBG_FLAG, u4Value, "TC", pu1Format)

#define TC_DBG1(u4Value, pu1Format, Arg1)     \
        if(u4Value == (u4Value & TC_DBG_FLAG)) \
             UtlTrcLog  (TC_DBG_FLAG, u4Value, "TC", pu1Format, Arg1)

#define TC_DBG2(u4Value, pu1Format, Arg1, Arg2)     \
        if(u4Value == (u4Value & TC_DBG_FLAG)) \
             UtlTrcLog  (TC_DBG_FLAG, u4Value, "TC", pu1Format, Arg1, Arg2)

#define TC_DBG3(u4Value, pu1Format, Arg1, Arg2, Arg3)     \
        if(u4Value == (u4Value & TC_DBG_FLAG)) \
             UtlTrcLog  (TC_DBG_FLAG, u4Value, "TC", pu1Format, Arg1, Arg2, \
	                 Arg3)

#define TC_DBG5(u4Value, pu1Format, Arg1, Arg2, Arg3, Arg4, Arg5)     \
        if(u4Value == (u4Value & TC_DBG_FLAG)) \
             UtlTrcLog  (TC_DBG_FLAG, u4Value, "TC", pu1Format, Arg1, Arg2, \
			 Arg3, Arg4, Arg5)

#endif /* _TCDBG_H */
/*---------------------------------------------------------------------------*/
/*                        End of file tcdgb.h                                */
/*---------------------------------------------------------------------------*/
