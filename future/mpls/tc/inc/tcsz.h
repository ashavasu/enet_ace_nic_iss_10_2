
 /* $Id: tcsz.h,v 1.3 2012/03/21 13:05:14 siva Exp $*/
enum {
    MAX_TC_ALLOC_RES_NODE_SIZING_ID,
    MAX_TC_FILTER_SPEC_NODE_SIZING_ID,
    TC_MAX_SIZING_ID
};

#ifdef  _TCSZ_C
tMemPoolId TCMemPoolIds[ TC_MAX_SIZING_ID];
INT4  TcSizingMemCreateMemPools(VOID);
VOID  TcSizingMemDeleteMemPools(VOID);
INT4  TcSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _TCSZ_C  */
extern tMemPoolId TCMemPoolIds[ ];
extern INT4  TcSizingMemCreateMemPools(VOID);
extern VOID  TcSizingMemDeleteMemPools(VOID);
extern INT4  TcSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _TCSZ_C  */


#ifdef  _TCSZ_C
tFsModSizingParams FsTCSizingParams [] = {
{ "tTcAllocResNode", "MAX_TC_ALLOC_RES_NODE", sizeof(tTcAllocResNode),MAX_TC_ALLOC_RES_NODE, MAX_TC_ALLOC_RES_NODE,0 },
{ "tTcFilterInfoNode", "MAX_TC_FILTER_SPEC_NODE", sizeof(tTcFilterInfoNode),MAX_TC_FILTER_SPEC_NODE, MAX_TC_FILTER_SPEC_NODE,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _TCSZ_C  */
extern tFsModSizingParams FsTCSizingParams [];
#endif /*  _TCSZ_C  */


