/********************************************************************
 *                                                                  *
 * $RCSfile: tctdfs.h,v $
 *                                                                  *
 * $Date: 2010/09/24 06:35:08 $                                                          *
 *                                                                  *
 * $Revision: 1.4 $                                                     *
 *                                                                  *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : tctdfs.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : TC
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains the structure definitions
 *                             used by TC.
 *---------------------------------------------------------------------------*/

#ifndef _TCTDFS_H
#define _TCTDFS_H

typedef struct _TcAppInfo
{
    UINT1 u1Free;
    UINT1 u1Rsvd;
    UINT2 u2Rsvd;
    UINT4 u4AppId;
    tTcAppCallBackFnPtr pCallBackFn;
} tTcAppInfo;

typedef struct _TcFilterInfoNode
{
    tTcSllNode NextFilterInfoNode;
    tTcFilterInfo FilterInfo;
    tTcResInfo ResInfo;
} tTcFilterInfoNode;

typedef struct _TcAllocResNode
{
    tTcSllNode NextAllocResNode;
    tTcResHandle ResHandle;
    UINT4 u4AppId;
    UINT4 u4InterfaceId;
    UINT1 u1ResPoolType;
    UINT1 u1Rsvd;
    UINT2 u2Rsvd;
    tTcResInfo MaxResInfo;
    tTcSll FilterInfoList; /* SLL of type tTcFilterInfoNode */
} tTcAllocResNode;

typedef struct _TcPoolResInfo
{
    tTcResInfo AvailResInfo; /* Contains info about available resources */
} tTcPoolResInfo;

typedef struct _TcInterfaceInfo
{
    UINT1 u1Free;
    UINT1 u1Rsvd;
    UINT2 u2Rsvd;
    UINT4 u4InterfaceId;
    tTcPoolResInfo aPoolResArray[TC_MAX_RES_POOLS];
} tTcInterfaceInfo;

typedef struct _TcGlobal
{
    tTcAdmCtrlAlgoFnPtr pAdmCtrlAlgo;
    tTcSll AllocResList; /* List of Type tTcAllocResNode */
    tTcAppInfo aAppInfoArray[TC_MAX_APPS];
    tTcInterfaceInfo aInterfaceInfo[TC_MAX_INTERFACES];
    tOsixSemId   gTcSemId;
} tTcGlobal;

#endif /* _TCTDFS_H */
/*---------------------------------------------------------------------------*/
/*                        End of file tctdfs.h                               */
/*---------------------------------------------------------------------------*/
