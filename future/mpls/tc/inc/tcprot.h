/********************************************************************
 *                                                                  *
 * $RCSfile: tcprot.h,v $
 *                                                                  *
 * $Date: 2007/10/26 09:42:54 $                                                          *
 *                                                                  *
 * $Revision: 1.4 $                                                     *
 *                                                                  *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : tcprot.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : TC
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains the prototypes of functions
 *                             used by TC.
 *---------------------------------------------------------------------------*/

#ifndef _TCPROT_H
#define _TCPROT_H

UINT1 TcInit(VOID);

UINT1 TcSearchAppInfoEntry(UINT4 u4AppId);

UINT1 TcSearchInterfaceInfoEntry(UINT4 u4InterfaceId);

UINT1 TcAddAppInfoEntry(UINT4 u4AppId, tTcAppCallBackFnPtr pCallBackFn);

UINT1 TcGenerateNewResHandle(tTcResHandle *pResHandle);

UINT1 TcGetPoolResInfo(UINT4 u4InterfaceId, UINT1 u1ResPoolType, 
      tTcPoolResInfo **ppTcPoolResInfo);

UINT1 TcDefaultAdmCtrlAlgo(UINT4 u4AppId, UINT4 u4InterfaceId, 
      UINT1 u1ResPoolType, tTcResInfo *pReqResInfo, UINT1 *pu1ErrorCode);

UINT1 TcModifyRes(tTcResHandle resHandle, UINT1 u1ResSpecType, 
      tuTcResSpec *pTcResSpec, UINT1 u1ResPoolType, UINT1 *pu1ErrorCode);
#endif /* _TCPROT_H */
/*---------------------------------------------------------------------------*/
/*                        End of file tcprot.h                               */
/*---------------------------------------------------------------------------*/
