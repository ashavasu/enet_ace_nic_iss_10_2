/*****************************************************************************/
/* Copyright (C) 2010 Aricent Inc . All Rights Reserved
 * $Id: tcmacs.h,v 1.5 2013/06/26 11:49:04 siva Exp $
 *---------------------------------------------------------------------------*/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : tcmacs.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : TC
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains the macro definitions
 *                             used by TC.
 *---------------------------------------------------------------------------*/

#ifndef _TCMACS_H
#define _TCMACS_H


/* Gives the offset in the structure */
#define TC_FIELD_OFFSET(str,field)  FSAP_OFFSETOF(str, field)
#define TC_FIELD_LENGTH(str,field)  (sizeof(((str *)TC_ZERO)->field))

/* Macros used to access AdSpec */

#define  TC_ADSPEC_MSG_HDR_VER(pNode)  (pNode)->MsgHdr.u1Version 
#define  TC_ADSPEC_MSG_HDR_LEN(pNode)  (pNode)->MsgHdr.u2HdrLen  

#define  TC_ADSPEC_DFLT_PARAM_SRV_HDR_ID(pNode)  \
         (pNode)->DfltParams.SrvHdr.u1SrvID
#define  TC_ADSPEC_DFLT_PARAM_SRV_HDR_LEN(pNode) \
         (pNode)->DfltParams.SrvHdr.u2SrvLength
      
#define  TC_ADSPEC_DFLT_PARAM_HOP_HDR_NUM(pNode) \
         (pNode)->DfltParams.HopCount.ParamHdr.u1ParamNum
#define  TC_ADSPEC_DFLT_PARAM_HOP_HDR_FLAG(pNode) \
         (pNode)->DfltParams.HopCount.ParamHdr.u1ParamFlags
#define  TC_ADSPEC_DFLT_PARAM_HOP_HDR_LEN(pNode) \
         (pNode)->DfltParams.HopCount.ParamHdr.u2ParamLen
#define  TC_ADSPEC_DFLT_PARAM_PATH_BW_HDR_NUM(pNode) \
         (pNode)->DfltParams.PathBw.ParamHdr.u1ParamNum
#define  TC_ADSPEC_DFLT_PARAM_PATH_BW_HDR_FLAG(pNode) \
         (pNode)->DfltParams.PathBw.ParamHdr.u1ParamFlags
#define  TC_ADSPEC_DFLT_PARAM_PATH_BW_HDR_LEN(pNode) \
         (pNode)->DfltParams.PathBw.ParamHdr.u2ParamLen
#define  TC_ADSPEC_DFLT_PARAM_PATH_LATENCY_HDR_NUM(pNode) \
         (pNode)->DfltParams.MinPathLatency.ParamHdr.u1ParamNum
#define  TC_ADSPEC_DFLT_PARAM_PATH_LATENCY_HDR_FLAG(pNode) \
         (pNode)->DfltParams.MinPathLatency.ParamHdr.u1ParamFlags
#define  TC_ADSPEC_DFLT_PARAM_PATH_LATENCY_HDR_LEN(pNode) \
         (pNode)->DfltParams.MinPathLatency.ParamHdr.u2ParamLen
#define  TC_ADSPEC_DFLT_PARAM_TRANS_UNIT_HDR_NUM(pNode) \
         (pNode)->DfltParams.MaxTransUnit.ParamHdr.u1ParamNum
#define  TC_ADSPEC_DFLT_PARAM_TRANS_UNIT_HDR_FLAG(pNode) \
         (pNode)->DfltParams.MaxTransUnit.ParamHdr.u1ParamFlags
#define  TC_ADSPEC_DFLT_PARAM_TRANS_UNIT_HDR_LEN(pNode) \
         (pNode)->DfltParams.MaxTransUnit.ParamHdr.u2ParamLen

#define  TC_ADSPEC_CLS_SRV_HDR_ID(pNode)    (pNode)->ClsAdSpec.SrvHdr.u1SrvID  
#define  TC_ADSPEC_CLS_SRV_HDR_LEN(pNode)   \
         (pNode)->ClsAdSpec.SrvHdr.u2SrvLength 
#define  TC_ADSPEC_CLS_SRV_HDR_FLAG(pNode)  (pNode)->ClsAdSpec.SrvHdr.u1Flag   
#define  TC_ADSPEC_GS_SRV_HDR_ID(pNode)     (pNode)->GsAdSpec.SrvHdr.u1SrvID   
#define  TC_ADSPEC_GS_SRV_HDR_FLAG(pNode)   (pNode)->GsAdSpec.SrvHdr.u1Flag    
#define  TC_ADSPEC_GS_SRV_HDR_LEN(pNode)    \
         (pNode)->GsAdSpec.SrvHdr.u2SrvLength 
#define  TC_ADSPEC_GS_CTOT_HDR_NUM(pNode) \
         (pNode)->GsAdSpec.CTot.ParamHdr.u1ParamNum
#define  TC_ADSPEC_GS_CTOT_HDR_FLAG(pNode) \
         (pNode)->GsAdSpec.CTot.ParamHdr.u1ParamFlags
#define  TC_ADSPEC_GS_CTOT_HDR_LEN(pNode) \
         (pNode)->GsAdSpec.CTot.ParamHdr.u2ParamLen
#define  TC_ADSPEC_GS_DTOT_HDR_NUM(pNode) \
         (pNode)->GsAdSpec.DTot.ParamHdr.u1ParamNum
#define  TC_ADSPEC_GS_DTOT_HDR_FLAG(pNode) \
         (pNode)->GsAdSpec.DTot.ParamHdr.u1ParamFlags
#define  TC_ADSPEC_GS_DTOT_HDR_LEN(pNode) \
         (pNode)->GsAdSpec.DTot.ParamHdr.u2ParamLen
      
#define  TC_ADSPEC_GS_CSUM_HDR_NUM(pNode) \
         (pNode)->GsAdSpec.CSum.ParamHdr.u1ParamNum
#define  TC_ADSPEC_GS_CSUM_HDR_FLAG(pNode) \
         (pNode)->GsAdSpec.CSum.ParamHdr.u1ParamFlags
#define  TC_ADSPEC_GS_CSUM_HDR_LEN(pNode) \
         (pNode)->GsAdSpec.CSum.ParamHdr.u2ParamLen

#define  TC_ADSPEC_GS_DSUM_HDR_NUM(pNode) \
         (pNode)->GsAdSpec.DSum.ParamHdr.u1ParamNum
#define  TC_ADSPEC_GS_DSUM_HDR_FLAG(pNode) \
         (pNode)->GsAdSpec.DSum.ParamHdr.u1ParamFlags
#define  TC_ADSPEC_GS_DSUM_HDR_LEN(pNode) \
         (pNode)->GsAdSpec.DSum.ParamHdr.u2ParamLen

#define  TC_ADSPEC_DFLT_PARAM_HOP_COUNT(pNode) \
      (pNode)->DfltParams.HopCount.u4ISHopCount
#define  TC_ADSPEC_DFLT_PARAM_PATH_BW(pNode) \
      (pNode)->DfltParams.PathBw.fPathBw
#define  TC_ADSPEC_DFLT_PARAM_PATH_LATENCY(pNode) \
      (pNode)->DfltParams.MinPathLatency.u4MinPathLatency
#define  TC_ADSPEC_DFLT_PARAM_TRANS_UNIT(pNode) \
      (pNode)->DfltParams.MaxTransUnit.u4MTUSize
#define  TC_ADSPEC_GS_CTOT(pNode)  (pNode)->GsAdSpec.CTot.u4CTot 
#define  TC_ADSPEC_GS_DTOT(pNode)  (pNode)->GsAdSpec.DTot.u4DTot 
#define  TC_ADSPEC_GS_CSUM(pNode)  (pNode)->GsAdSpec.CSum.u4CSum 
#define  TC_ADSPEC_GS_DSUM(pNode)  (pNode)->GsAdSpec.DSum.u4DSum 

#define TC_SUPPRESS_WARNING(x) UNUSED_PARAM(x)

/* Following macro converts bytes per second to
 *  * bits per second */
#define TC_CONVERT_BYTES_PER_SEC_TO_BPS(x)\
{\
   x = x*8;\
}

#endif /* _TCMACS_H */
/*---------------------------------------------------------------------------*/
/*                        End of file tcmacs.h                               */
/*---------------------------------------------------------------------------*/
