/********************************************************************
 *                                                                  *
 * $RCSfile: tcfsap.h,v $
 *                                                                  *
 * $Date: 2009/04/21 13:23:27 $                                                          *
 *                                                                  *
 * $Revision: 1.3 $                                                     *
 *                                                                  *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : tcfsap.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : TC
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Contains macros of FSAP library.
 *---------------------------------------------------------------------------*/

#ifndef _TCFSAP_H
#define _TCFSAP_H

#define tTcSllNode   tTMO_SLL_NODE  
#define tTcSll       tTMO_SLL
#define tTcMemPoolId tMemPoolId

#define TcFreeMemBlock(PoolId, pu1Block, typecast) \
    MEMSET(pu1Block, TC_ZERO, sizeof(typecast)); \
    MemReleaseMemBlock(PoolId, pu1Block);

#define TcAllocMemBlock MemAllocMemBlk

#define TcCreateMemPool(u4BlockSize, u4NumberOfBlocks, pPoolId) \
            MemCreateMemPool (u4BlockSize, u4NumberOfBlocks, \
            MEM_DEFAULT_MEMORY_TYPE, pPoolId)

#define TcDeleteMemPool MemDeleteMemPool

#define TC_MEM_SUCCESS                                   MEM_SUCCESS
#define TC_MEM_FAILURE                                   MEM_FAILURE

#endif /* _TCFSAP_H */
/*---------------------------------------------------------------------------*/
/*                        End of file tcfsap.h                               */
/*---------------------------------------------------------------------------*/
