
/* $Id: tcincs.h,v 1.5 2011/10/25 09:29:47 siva Exp $*/
/********************************************************************
 *                                                                  *
 * $RCSfile: tcincs.h,v $
 *                                                                  *
 * $Date: 2011/10/25 09:29:47 $                                                          *
 *                                                                  *
 * $Revision: 1.5 $                                                     *
 *                                                                  *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : tcincs.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : TC
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains include functions
 *                             used by TC.
 *---------------------------------------------------------------------------*/

#ifndef _TCINCS_H
#define _TCINCS_H

#include "lr.h"
#include "ip.h"
#include "cfa.h"
#include "include.h"

#include "mpls.h"


#include "mplsutil.h"
#include "mplssize.h"

#include "tcfsap.h"
#include "tcextrn.h"
#include "tcdefs.h"
#include "tctdfs.h"
#include "tcmacs.h"
#include "tcglob.h"
#include "tcdbg.h"
#include "tcapi.h"
#include "tcprot.h"
#include "tcsz.h"

#endif /*_TCINCS_H*/
/*---------------------------------------------------------------------------*/
/*                        End of file tcincs.h                               */
/*---------------------------------------------------------------------------*/
