/********************************************************************
 *                                                                  *
 * $RCSfile: tcglob.h,v $
 *                                                                  *
 * $Date: 2007/02/01 14:57:16 $                                                          *
 *                                                                  *
 * $Revision: 1.2 $                                                     *
 *                                                                  *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : tcglob.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : TC
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : Contains global definitions for TC.
 *---------------------------------------------------------------------------*/

#ifndef _TCGLOB_H
#define _TCGLOB_H

extern tTcGlobal gTcGlobal;
extern UINT2 gau2OffSet[];
extern UINT2 gau2Length[];
extern UINT1 gau1ByteOrder[];
extern UINT4 gu4TcDbg;
extern UINT4 gu4TcConfigFlag;

#endif /* _TCGLOB_H */
/*---------------------------------------------------------------------------*/
/*                        End of file tcglob.h                               */
/*---------------------------------------------------------------------------*/
