/********************************************************************
 *                                                                  *
 * $RCSfile: tcdefs.h,v $
 *                                                                  *
 * $Date: 2007/10/26 09:42:54 $                                                          *
 *                                                                  *
 * $Revision: 1.6 $                                                     *
 *                                                                  *
 *******************************************************************/

/*****************************************************************************/
/* Copyright (C) 2006 Aricent Inc . All Rights Reserved
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 ******************************************************************************
 *    FILE  NAME             : tcdefs.h
 *    PRINCIPAL AUTHOR       : Aricent Inc. 
 *    SUBSYSTEM NAME         : MPLS
 *    MODULE NAME            : TC
 *    LANGUAGE               : ANSI-C
 *    TARGET ENVIRONMENT     : Linux (Portable)                         
 *    DATE OF FIRST RELEASE  :
 *    DESCRIPTION            : This file contains constants used by TC
 *---------------------------------------------------------------------------*/

#ifndef _TCDEFS_H
#define _TCDEFS_H

#define TC_SCENARIO_ONE                                  1

#define TC_MAX_APPS                                      5
#define TC_MAX_RES_POOLS                                17
#define TC_CNTRLD_LOAD_SRV_ID                            5
#define TC_ETH_LINK_SPEED                         10000000
#define TC_POSITIVE_INFINITE                    0x7F800000

#define TC_ZERO                                          0
#define TC_ONE                                           1
#define TC_FOUR                                          4
#define TC_MAX_UINT1                                  0xff
#define TC_MAX_UINT4                            0xffffffff

#define TC_INVALID_HANDLE                       0xffffffff
#define TC_MAX_HANDLE                           0xfffffffe
#define TC_INVALID_HANDLE_COUNT                          0

#define TC_TRUE                                          1
#define TC_FALSE                                         0

/* Macros used to Fill the AdSpec Structure. These values are std (rfc 2210) */

#define  TC_ADSPEC_MSG_HDR_VER_VAL                       0              
#define  TC_ADSPEC_MSG_HDR_LEN_VAL                       OSIX_NTOHS(20) 

#define  TC_ADSPEC_DFLT_PARAM_SRV_HDR_ID_VAL             1              
#define  TC_ADSPEC_DFLT_PARAM_SRV_HDR_LEN_VAL            OSIX_NTOHS(8)  
			     
#define  TC_ADSPEC_DFLT_PARAM_HOP_HDR_NUM_VAL            4              
#define  TC_ADSPEC_DFLT_PARAM_HOP_HDR_FLAG_VAL           0              
#define  TC_ADSPEC_DFLT_PARAM_HOP_HDR_LEN_VAL            OSIX_NTOHS(1)  

#define  TC_ADSPEC_DFLT_PARAM_PATH_BW_HDR_NUM_VAL        6              
#define  TC_ADSPEC_DFLT_PARAM_PATH_BW_HDR_FLAG_VAL       0              
#define  TC_ADSPEC_DFLT_PARAM_PATH_BW_HDR_LEN_VAL        OSIX_NTOHS(1)  

#define  TC_ADSPEC_DFLT_PARAM_PATH_LATENCY_HDR_NUM_VAL   8              
#define  TC_ADSPEC_DFLT_PARAM_PATH_LATENCY_HDR_FLAG_VAL  0              
#define  TC_ADSPEC_DFLT_PARAM_PATH_LATENCY_HDR_LEN_VAL   OSIX_NTOHS(1)  

#define  TC_ADSPEC_DFLT_PARAM_TRANS_UNIT_HDR_NUM_VAL     10             
#define  TC_ADSPEC_DFLT_PARAM_TRANS_UNIT_HDR_FLAG_VAL    0              
#define  TC_ADSPEC_DFLT_PARAM_TRANS_UNIT_HDR_LEN_VAL     OSIX_NTOHS(1)  

#define  TC_ADSPEC_CLS_SRV_HDR_ID_VAL                    5              
#define  TC_ADSPEC_CLS_SRV_HDR_FLAG_VAL                  0              
#define  TC_ADSPEC_CLS_SRV_HDR_LEN_VAL                   0              

#define  TC_ADSPEC_GS_SRV_HDR_ID_VAL                     2              
#define  TC_ADSPEC_GS_SRV_HDR_FLAG_VAL                   0              
#define  TC_ADSPEC_GS_SRV_HDR_LEN_VAL                    OSIX_NTOHS(8)  

#define  TC_ADSPEC_GS_CTOT_HDR_NUM_VAL                   133
#define  TC_ADSPEC_GS_CTOT_HDR_FLAG_VAL                  0
#define  TC_ADSPEC_GS_CTOT_HDR_LEN_VAL                   1

#define  TC_ADSPEC_GS_DTOT_HDR_NUM_VAL                   134
#define  TC_ADSPEC_GS_DTOT_HDR_FLAG_VAL                  0
#define  TC_ADSPEC_GS_DTOT_HDR_LEN_VAL                   1

#define  TC_ADSPEC_GS_CSUM_HDR_NUM_VAL                   135
#define  TC_ADSPEC_GS_CSUM_HDR_FLAG_VAL                  0
#define  TC_ADSPEC_GS_CSUM_HDR_LEN_VAL                   1

#define  TC_ADSPEC_GS_DSUM_HDR_NUM_VAL                   136
#define  TC_ADSPEC_GS_DSUM_HDR_FLAG_VAL                  0
#define  TC_ADSPEC_GS_DSUM_HDR_LEN_VAL                   1

/* TC Lock and Unlock  TC Module Semaphore (TCMS) */
#define MPLS_TC_SEM_NAME ( const UINT1 *)"TCMS"

#define MPLS_TC_LOCK()   \
  if(gTcGlobal.gTcSemId) {OsixSemTake(gTcGlobal.gTcSemId);} 
#define MPLS_TC_UNLOCK() \
  if(gTcGlobal.gTcSemId) {OsixSemGive(gTcGlobal.gTcSemId);} 

#endif /* _TCDEFS_H */
/*---------------------------------------------------------------------------*/
/*                        End of file tcdefs.h                               */
/*---------------------------------------------------------------------------*/
