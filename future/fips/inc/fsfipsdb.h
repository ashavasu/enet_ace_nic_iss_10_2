/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsfipsdb.h,v 1.6 2011/07/20 08:42:25 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSFIPSDB_H
#define _FSFIPSDB_H


UINT4 fsfips [] ={1,3,6,1,4,1,29601,2,63};
tSNMP_OID_TYPE fsfipsOID = {9, fsfips};


UINT4 FsFipsOperMode [ ] ={1,3,6,1,4,1,29601,2,63,1,1};
UINT4 FsFipsTestAlgo [ ] ={1,3,6,1,4,1,29601,2,63,1,2};
UINT4 FsfipsZeroizeCryptoKeys [ ] ={1,3,6,1,4,1,29601,2,63,1,3};
UINT4 FsFipsTraceLevel [ ] ={1,3,6,1,4,1,29601,2,63,1,4};
UINT4 FsFipsTestExecutionResult [ ] ={1,3,6,1,4,1,29601,2,63,1,5};
UINT4 FsFipsFailedAlgorithm [ ] ={1,3,6,1,4,1,29601,2,63,1,6};
UINT4 FsFipsBypassCapability [ ] ={1,3,6,1,4,1,29601,2,63,1,7};




tMbDbEntry fsfipsMibEntry[]= {

{{11,FsFipsOperMode}, NULL, FsFipsOperModeGet, FsFipsOperModeSet, FsFipsOperModeTest, FsFipsOperModeDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsFipsTestAlgo}, NULL, FsFipsTestAlgoGet, FsFipsTestAlgoSet, FsFipsTestAlgoTest, FsFipsTestAlgoDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsfipsZeroizeCryptoKeys}, NULL, FsfipsZeroizeCryptoKeysGet, FsfipsZeroizeCryptoKeysSet, FsfipsZeroizeCryptoKeysTest, FsfipsZeroizeCryptoKeysDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},

{{11,FsFipsTraceLevel}, NULL, FsFipsTraceLevelGet, FsFipsTraceLevelSet, FsFipsTraceLevelTest, FsFipsTraceLevelDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsFipsTestExecutionResult}, NULL, FsFipsTestExecutionResultGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, "0"},

{{11,FsFipsFailedAlgorithm}, NULL, FsFipsFailedAlgorithmGet, NULL, NULL, NULL, SNMP_DATA_TYPE_INTEGER32, SNMP_READONLY, NULL, 0, 0, 0, "0"},

{{11,FsFipsBypassCapability}, NULL, FsFipsBypassCapabilityGet, FsFipsBypassCapabilitySet, FsFipsBypassCapabilityTest, FsFipsBypassCapabilityDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, "2"},
};
tMibData fsfipsEntry = { 7, fsfipsMibEntry };

#endif /* _FSFIPSDB_H */

