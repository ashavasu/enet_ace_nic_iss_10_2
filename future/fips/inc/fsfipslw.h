/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsfipslw.h,v 1.5 2011/07/20 08:42:25 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsFipsOperMode ARG_LIST((INT4 *));

INT1
nmhGetFsFipsTestAlgo ARG_LIST((INT4 *));

INT1
nmhGetFsfipsZeroizeCryptoKeys ARG_LIST((INT4 *));

INT1
nmhGetFsFipsTraceLevel ARG_LIST((INT4 *));

INT1
nmhGetFsFipsTestExecutionResult ARG_LIST((INT4 *));

INT1
nmhGetFsFipsFailedAlgorithm ARG_LIST((INT4 *));

INT1
nmhGetFsFipsBypassCapability ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsFipsOperMode ARG_LIST((INT4 ));

INT1
nmhSetFsFipsTestAlgo ARG_LIST((INT4 ));

INT1
nmhSetFsfipsZeroizeCryptoKeys ARG_LIST((INT4 ));

INT1
nmhSetFsFipsTraceLevel ARG_LIST((INT4 ));

INT1
nmhSetFsFipsBypassCapability ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsFipsOperMode ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsFipsTestAlgo ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsfipsZeroizeCryptoKeys ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsFipsTraceLevel ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsFipsBypassCapability ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsFipsOperMode ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsFipsTestAlgo ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsfipsZeroizeCryptoKeys ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsFipsTraceLevel ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsFipsBypassCapability ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
