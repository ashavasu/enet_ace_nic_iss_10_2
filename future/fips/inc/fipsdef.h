/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fipsdef.h,v 1.10 2017/09/12 13:26:39 siva Exp $
 *
 * Description: This file contains definitions for FIPS module
 *******************************************************************/

#ifndef __FIPSDEF_H__
#define __FIPSDEF_H__ 

typedef unsigned long long ULONG;

#define FIPS_TRUE              OSIX_TRUE
#define FIPS_FALSE             OSIX_FALSE

/* To print the results in console define PRINT_TEXT macro in this file */
/* #define PRINT_TEXT   1 */
#ifdef PRINT_TEXT
#define PRINT_RESULT(x)             printf (x)
#define PRINT_RESULT1(x, y)         printf (x, y)
#define PRINT_RESULT2(x, y, z)      printf (x, y, z)
#define PRINT_RESULT3(x, y, z, i)   printf (x, y, z, i)
#define FIPS_PRINT_RESULT           1
#else
#define PRINT_RESULT(x)
#define PRINT_RESULT1(x, y)
#define PRINT_RESULT2(x, y, z)
#define PRINT_RESULT3(x, y, z, i)
#define FIPS_PRINT_RESULT           0
#endif

#define FIPS_KNOWN_ANS_TEST_NONE   0x00000000
#define FIPS_KNOWN_ANS_TEST_SHA1   0x00000001
#define FIPS_KNOWN_ANS_TEST_SHA2   0x00000002
#define FIPS_KNOWN_ANS_TEST_HMAC   0x00000004
#define FIPS_KNOWN_ANS_TEST_AES    0x00000008
#define FIPS_KNOWN_ANS_TEST_DES    0x00000010
#define FIPS_KNOWN_ANS_TEST_RAND   0x00000020
#define FIPS_KNOWN_ANS_TEST_RSA    0x00000040
#define FIPS_KNOWN_ANS_TEST_DSA    0x00000080

#define FIPS_KNOWN_ANS_TEST_ALL    0x000000FF

#define FIPS_RAND_NUM_LEN         4
#define FIPS_RAND_AES_BLOCK_LEN   16
#define FIPS_RAND_AES128_KEY_LEN  16
#define FIPS_RAND_AES192_KEY_LEN  24
#define FIPS_RAND_AES256_KEY_LEN  32
#define FIPS_SHA_RAND_COUNT       1003

#define FIPS_UTIL_CALL_BACK gaFipsCustCallBackEntry

#define FIPS_DEF_ROOT_USER           "admin"
#define FIPS_DEF_ROOT_PASSWD         "Admin#123"

#define FIPS_TASK_ID            gFipsGlobalInfo.MainTaskId
#define FIPS_TASK_SEM_ID        gFipsGlobalInfo.MainTaskSemId
#define FIPS_SYSLOG_ID          gFipsGlobalInfo.u4FipsSysLogId

#define FIPS_TASK_NAME          ((UINT1 *)"FIPT")
#define FIPS_MOD_SEM            ((UINT1 *)"FIPS")

#define FIPS_LOCK()             FipsApiLock()
#define FIPS_UNLOCK()           FipsApiUnLock()

/* Events of FIPS Main task */
#define FIPS_KEY_ZEROISE_EVENT             0x01 /* Bit 1 */
#define FIPS_SWITCH_TO_LEGACY_EVENT       0x02 /* Bit 2 */
#define FIPS_SWITCH_TO_FIPS_EVENT          0x04 /* Bit 3 */

#define FIPS_SWITCH_TO_CNSA_EVENT          0x08 /* Bit 4 */
#define FIPS_SWITCH_FIPS_TO_CNSA_EVENT     0x10 /* Bit 5 */
#define FIPS_ALL_EVENTS                   (FIPS_KEY_ZEROISE_EVENT | \
                                           FIPS_SWITCH_TO_LEGACY_EVENT |\
                                           FIPS_SWITCH_TO_FIPS_EVENT |\
                                           FIPS_SWITCH_TO_CNSA_EVENT |\
                                           FIPS_SWITCH_FIPS_TO_CNSA_EVENT)

#define FIPS_CLI_SESSION_CLOSE_TIMEOUT     5


enum
{
    FIPS_KAT_SHA1_FAILED = 1,
    FIPS_KAT_SHA2_FAILED,
    FIPS_KAT_HMAC_FAILED,
    FIPS_KAT_AES_FAILED,
    FIPS_KAT_DES_FAILED,
    FIPS_KAT_RNG_FAILED,
    FIPS_KAT_RSA_FAILED,
    FIPS_KAT_DSA_FAILED
};

#ifdef __FIPSMAIN_C_
CONST CHR1  *ac1FipsErrString [] = {
    NULL,
    "FIPS Self-test failed for SHA-1 algorithm \r\n",
    "FIPS Self-test failed for SHA-2 algorithm \r\n",
    "FIPS Self-test failed for HMAC algorithm \r\n",
    "FIPS Self-test failed for AES algorithm \r\n",
    "FIPS Self-test failed for DES algorithm \r\n",
    "FIPS Self-test failed for RNG algorithm \r\n",
    "FIPS Self-test failed for RSA algorithm \r\n",
    "FIPS Self-test failed for DSA algorithm \r\n"
};
#else /* __FIPSMAIN_C_ */
extern CONST CHR1 *ac1FipsErrString[]; 
#endif /* __FIPSMAIN_C_ */

typedef struct
{
    UINT1     au1DTBuf[FIPS_RAND_AES_BLOCK_LEN];
    UINT1     au1Vect[FIPS_RAND_AES_BLOCK_LEN];
    UINT1     au1Result[FIPS_RAND_AES_BLOCK_LEN];
} tAesPrng;

typedef union FipsCustCallbackEntry
{
    INT4 (* pFipsCustDelSramContents) (VOID);
}unFipsCustCallBackEntry;

typedef struct _sFipsGlobalInfo
{
    tOsixTaskId           MainTaskId;
    tOsixSemId            MainTaskSemId;
    UINT4                 u4FipsSysLogId;

}tFipsGlobalInfo;

#endif
