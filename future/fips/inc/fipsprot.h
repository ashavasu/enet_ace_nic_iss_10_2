/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fipsprot.h,v 1.17 2017/09/12 13:26:39 siva Exp $
 *
 * Description: This file contains declaration of function 
 *              prototypes of FIPS.
 *******************************************************************/

#ifndef __FIPSPROT_H__
#define __FIPSPROT_H__ 

INT4 FipsAesArTest PROTO ((VOID));

VOID aes_ctr_testcases PROTO ((VOID));

VOID aes_ctr_inc_testcases PROTO ((VOID));

PUBLIC VOID aes_xcbc_testcases PROTO ((VOID));

PUBLIC INT4 FipsDesArTest PROTO ((VOID));

PUBLIC INT4 DsaFipsTest PROTO ((VOID));

PUBLIC INT4 RsaFipsTest PROTO ((VOID));

/* Prototypes of fipssha.c */
VOID FipsShaPrintStr PROTO ((CONST CHR1 *str, INT4 len));

VOID FipsShaPrintxStr PROTO ((CONST CHR1 *str, INT4 len));

INT4 FipsShaTestShaAndHmac PROTO ((INT4));

VOID FipsShaRandomTest PROTO ((INT4 hashno, CONST CHR1 *seed, INT4 hashsize,
                               CONST CHR1 **resultarrays, INT4 randomcount));

INT4 FipsShaHashFile PROTO ((INT4 hashno, CONST CHR1 *hashfilename, INT4 bits,
                             INT4 bitcount, INT4 skipSpaces,
                             CONST UINT1 *keyarray, INT4 keylen,
                             CONST CHR1 *resultarray, INT4 hashsize,
                             INT4 printResults, INT4 printPassFail));

INT4 FipsShaHash PROTO ((INT4 testno, INT4 loopno, INT4 hashno,
                         CHR1 *testarray, INT4 length, long repeatcount,
                         INT4 numberExtrabits, INT4 extrabits,
                         UINT1 *keyarray, INT4 keylen,
                         CONST CHR1 *resultarray, INT4 hashsize));

INT4 FipsShaPrintResult PROTO ((UINT1 *Message_Digest, INT4 hashsize,
                                CONST CHR1 *hashname, CONST CHR1 *testtype,
                                CONST CHR1 *testname, CONST CHR1 *resultarray));

INT4 FipsShaCheckMatch PROTO ((CONST UINT1 *hashvalue,
                               CONST CHR1 *hexstr, INT4 hashsize));

INT4 FipsShaTestSha1Algo PROTO ((VOID));

INT4
FipsUtilDelCryptoKeys PROTO ((VOID));


/* Switching between LEGACY, FIPS and CNSA mode  */
INT4 FipsUtilSwitchLegacyToFips PROTO ((VOID));

INT4 FipsUtilSwitchLegacyToCnsa PROTO ((VOID));

INT4 FipsUtilSwitchFipsToCnsa PROTO ((VOID));

VOID FipsUtilDisableAllProtocols PROTO ((VOID));

INT4 FipsRandTestRandNumGen (VOID);

PUBLIC INT1 Secv4GetBypassCapability PROTO ((INT4 * pi4RetValSecv4BypassCapability));
PUBLIC INT1 Secv4SetBypassCapability PROTO ((INT4 i4SetValFsVpnGlobalStatus));

PUBLIC INT1 Secv6GetBypassCapability PROTO ((INT4 * pi4RetValSecv4BypassCapability));
PUBLIC INT1 Secv6SetBypassCapability PROTO ((INT4 i4SetValFsVpnGlobalStatus));

PUBLIC INT4 FipsUtilCallBack PROTO((UINT4 u4Event));

PUBLIC INT1 FipsSetFipsTraceLevel PROTO ((UINT4 u4FipsTrcLevel));

PUBLIC INT4 FipsRegisterEvtCallBk PROTO ((UINT4 u4ModuleId,UINT4 u4Event,tFsCbInfo * pFsCbInfo));


PUBLIC INT4 FipsApiLock PROTO ((VOID));

PUBLIC INT4 FipsApiUnLock PROTO ((VOID));

PUBLIC INT4 FipsMainTaskInit PROTO ((VOID));

PUBLIC INT4 FipsUtilKeyZeroiseHandler PROTO ((VOID));

PUBLIC INT4 FipsUtilSwitchLegacyModeHandler (VOID);

PUBLIC INT4 FipsUtilCnsaKeyZeroiseHandler PROTO ((VOID));

PUBLIC INT4 FipsUtilSwitchFipsModeHandler PROTO ((VOID));

PUBLIC INT4 FipsUtilSwitchCnsaModeHandler PROTO ((VOID));

PUBLIC INT4 FipsUtilSwitchFipsToCnsaModeHandler PROTO ((VOID));

#endif
