/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsfipswr.h,v 1.5 2011/07/20 08:42:25 siva Exp $
*
* Description: Function prototypes for FIPS snmp module 
*********************************************************************/
#ifndef _FSFIPSWR_H
#define _FSFIPSWR_H

VOID RegisterFSFIPS(VOID);

VOID UnRegisterFSFIPS(VOID);
INT4 FsFipsOperModeGet(tSnmpIndex *, tRetVal *);
INT4 FsFipsTestAlgoGet(tSnmpIndex *, tRetVal *);
INT4 FsfipsZeroizeCryptoKeysGet(tSnmpIndex *, tRetVal *);
INT4 FsFipsTraceLevelGet(tSnmpIndex *, tRetVal *);
INT4 FsFipsTestExecutionResultGet(tSnmpIndex *, tRetVal *);
INT4 FsFipsFailedAlgorithmGet(tSnmpIndex *, tRetVal *);
INT4 FsFipsBypassCapabilityGet(tSnmpIndex *, tRetVal *);
INT4 FsFipsOperModeSet(tSnmpIndex *, tRetVal *);
INT4 FsFipsTestAlgoSet(tSnmpIndex *, tRetVal *);
INT4 FsfipsZeroizeCryptoKeysSet(tSnmpIndex *, tRetVal *);
INT4 FsFipsTraceLevelSet(tSnmpIndex *, tRetVal *);
INT4 FsFipsBypassCapabilitySet(tSnmpIndex *, tRetVal *);
INT4 FsFipsOperModeTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsFipsTestAlgoTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsfipsZeroizeCryptoKeysTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsFipsTraceLevelTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsFipsBypassCapabilityTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsFipsOperModeDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsFipsTestAlgoDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsfipsZeroizeCryptoKeysDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsFipsTraceLevelDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsFipsBypassCapabilityDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
#endif /* _FSFIPSWR_H */
