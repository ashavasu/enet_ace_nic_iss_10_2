/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fipsglob.h,v 1.8 2014/01/07 10:28:35 siva Exp $
 *
 * Description: This file contains the global variables refered 
 *              across FIPS module
 *******************************************************************/
#ifndef __FIPSGLOB_H__
#define __FIPSGLOB_H__ 

#ifdef __FIPSMAIN_C_
tFipsGlobalInfo     gFipsGlobalInfo;
INT4                gi4FipsTestExecResult = 0;
INT4                gi4FipsFailedAlgo = 0;
INT4                gi4FipsTraceOption = 0;
INT4                gi4FirstGenRandNum = 0;
unFipsCustCallBackEntry  
    gaFipsCustCallBackEntry[FIPS_CUST_MAX_CALL_BACK_EVENTS];
#else
PUBLIC tFipsGlobalInfo     gFipsGlobalInfo;
PUBLIC INT4                gi4FipsTraceOption;
PUBLIC INT4                gi4FipsTestExecResult;
PUBLIC INT4                gi4FipsFailedAlgo;
PUBLIC INT4                gi4FirstGenRandNum;
PUBLIC unFipsCustCallBackEntry  
    gaFipsCustCallBackEntry[FIPS_CUST_MAX_CALL_BACK_EVENTS];
#endif /* __FIPSMAIN_C_ */

#endif   /* __FIPSGLOB_H__*/

/*-----------------------------------------------------------------------*/
/*                       End of the file fipsglob.h                       */
/*-----------------------------------------------------------------------*/

