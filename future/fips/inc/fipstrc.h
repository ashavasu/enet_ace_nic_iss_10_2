/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fipstrc.h,v 1.2 2011/05/16 08:57:03 siva Exp $
*
* Description: This file contains all the Trace realated
*              information for the fips module
*********************************************************************/
#ifndef __FIPSTRC_H_
#define __FIPSTRC_H_

/* Trace and debug flags */
#define FIPS_TRC_FLAG          gi4FipsTraceOption

/* Module names */
#define   FIPS_MOD_NAME        ((CONST CHR1 *)"FIPS")

#define FIPS_MIN_VALID_TRACE   0x00000000
#define FIPS_MAX_VALID_TRACE   0x0000ffff

/* Trace definitions */
#ifdef TRACE_WANTED

#define   FIPS_TRC(TraceType, TraceString)\
    MOD_TRC(FIPS_TRC_FLAG, TraceType, FIPS_MOD_NAME, \
            (CONST CHR1 *)TraceString)

#define   FIPS_TRC_ARG1(TraceType, TraceString, Arg1)\
    MOD_TRC_ARG1(FIPS_TRC_FLAG, TraceType, FIPS_MOD_NAME, \
                 (CONST CHR1 *)TraceString, Arg1)

#else /* TRACE_WANTED */

#define FIPS_TRC(TraceType, TraceString)
#define  FIPS_TRC_ARG1(TraceType, TraceString, Arg1)

#endif /* TRACE_WANTED */
#endif /* __FIPSTRC_H_ */
