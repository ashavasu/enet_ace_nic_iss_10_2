/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fipsinc.h,v 1.12 2017/12/13 10:47:21 siva Exp $
 *
 * Description: This file contains include files of fips module.
 *******************************************************************/

#ifndef __FIPSINC_H__
#define __FIPSINC_H__ 

/* Global includes from the future/inc folder */
#include "lr.h"
#include "cust.h"
#include "fips.h"
#include "iss.h"
#include "cli.h"
#include "msr.h"
#include "fssnmp.h"
#include "snmputil.h"
#include "snmcdefn.h"
#include "snmctdfs.h"
#include "snmccons.h"
#include "sha2.h"
#include "trace.h"
#include "fsike.h"
#include "sshfs.h"
#include "secv6.h"
#include "utilipvx.h"
#include "utilalgo.h"
#include "radius.h"
#include "httpssl.h"
#include "fssyslog.h"
#include "utilrand.h"
#include "arHmac_defs.h"
#include "arHmac_api.h"
#include "shaarinc.h"
#include "aesarinc.h"
#include "desarinc.h"

/* To include use of FPAM APIs */
#include "fpam.h"

/* For CLI commands and CLI handling routines */
#include "fipscli.h"

/* Local includes in the fips module */
#include "fipsdef.h"
#include "fsfipslw.h"
#include "fsfipswr.h"
#include "fipstrc.h"
#include "fipsprot.h"
#include "fipsglob.h"
#include "fipsextn.h"
#include "fswebnm.h"
#endif   /* __FIPSINC_H__*/

/*-----------------------------------------------------------------------*/
/*                       End of the file fipsinc.h                       */
/*-----------------------------------------------------------------------*/
