/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: fipsrand.h,v 1.2 2011/05/17 09:46:25 siva Exp $
 *
 * Description: This file contains definitions required for random
 *              number generator
 *******************************************************************/
#ifndef _FIPS_RAND_H_
#define _FIPS_RAND_H_

/* Function codes. */
#define RAND_F_ENG_RAND_GET_RAND_METHOD   108
#define RAND_F_FIPS_RAND         103
#define RAND_F_FIPS_RAND_BYTES     102
#define RAND_F_FIPS_RAND_GET_RAND_METHOD  109
#define RAND_F_FIPS_RAND_SET_DT     106
#define RAND_F_FIPS_SET_DT         104
#define RAND_F_FIPS_SET_PRNG_SEED    107
#define RAND_F_FIPS_SET_TEST_MODE    105
#define RAND_F_RAND_GET_RAND_METHOD    101
#define RAND_F_SSLEAY_RAND_BYTES    100

/* Reason codes. */
#define RAND_R_NON_FIPS_METHOD     105
#define RAND_R_NOT_IN_TEST_MODE     106
#define RAND_R_NO_KEY_SET         107
#define RAND_R_PRNG_ASKING_FOR_TOO_MUCH   101
#define RAND_R_PRNG_ERROR         108
#define RAND_R_PRNG_KEYED         109
#define RAND_R_PRNG_NOT_REKEYED     102
#define RAND_R_PRNG_NOT_RESEEDED    103
#define RAND_R_PRNG_NOT_SEEDED     100
#define RAND_R_PRNG_SEED_MUST_NOT_MATCH_KEY  110
#define RAND_R_PRNG_STUCK         104

#endif
