/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: fipssha.c,v 1.6 2011/06/14 09:46:48 siva Exp $
 *
 * Description: This file contains the API code for testing SHA
 * and HMAC module.
 *
 * The algorithm implemented is from the RFC 4634. The code from the
 * RFC 4634 has been modified to conform to organizational coding 
 * guidelines
 *********************************************************************/
#include "fipsinc.h"

/*
 *  Define patterns for testing
 */
#define TEST1    "abc"
#define TEST2_1  \
        "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq"
#define TEST2_2a \
        "abcdefghbcdefghicdefghijdefghijkefghijklfghijklmghijklmn"
#define TEST2_2b \
        "hijklmnoijklmnopjklmnopqklmnopqrlmnopqrsmnopqrstnopqrstu"
#define TEST2_2  TEST2_2a TEST2_2b
#define TEST3    "a"
#define TEST4a   "01234567012345670123456701234567"
#define TEST4b   "01234567012345670123456701234567"
    /* an exact multiple of 512 bits */
#define TEST4   TEST4a TEST4b

#define TEST7_1 \
  "\x49\xb2\xae\xc2\x59\x4b\xbe\x3a\x3b\x11\x75\x42\xd9\x4a\xc8"
#define TEST8_1 \
  "\x9a\x7d\xfd\xf1\xec\xea\xd0\x6e\xd6\x46\xaa\x55\xfe\x75\x71\x46"
#define TEST9_1 \
  "\x65\xf9\x32\x99\x5b\xa4\xce\x2c\xb1\xb4\xa2\xe7\x1a\xe7\x02\x20" \
  "\xaa\xce\xc8\x96\x2d\xd4\x49\x9c\xbd\x7c\x88\x7a\x94\xea\xaa\x10" \
  "\x1e\xa5\xaa\xbc\x52\x9b\x4e\x7e\x43\x66\x5a\x5a\xf2\xcd\x03\xfe" \
  "\x67\x8e\xa6\xa5\x00\x5b\xba\x3b\x08\x22\x04\xc2\x8b\x91\x09\xf4" \
  "\x69\xda\xc9\x2a\xaa\xb3\xaa\x7c\x11\xa1\xb3\x2a"
#define TEST10_1 \
  "\xf7\x8f\x92\x14\x1b\xcd\x17\x0a\xe8\x9b\x4f\xba\x15\xa1\xd5\x9f" \
  "\x3f\xd8\x4d\x22\x3c\x92\x51\xbd\xac\xbb\xae\x61\xd0\x5e\xd1\x15" \
  "\xa0\x6a\x7c\xe1\x17\xb7\xbe\xea\xd2\x44\x21\xde\xd9\xc3\x25\x92" \
  "\xbd\x57\xed\xea\xe3\x9c\x39\xfa\x1f\xe8\x94\x6a\x84\xd0\xcf\x1f" \
  "\x7b\xee\xad\x17\x13\xe2\xe0\x95\x98\x97\x34\x7f\x67\xc8\x0b\x04" \
  "\x00\xc2\x09\x81\x5d\x6b\x10\xa6\x83\x83\x6f\xd5\x56\x2a\x56\xca" \
  "\xb1\xa2\x8e\x81\xb6\x57\x66\x54\x63\x1c\xf1\x65\x66\xb8\x6e\x3b" \
  "\x33\xa1\x08\xb0\x53\x07\xc0\x0a\xff\x14\xa7\x68\xed\x73\x50\x60" \
  "\x6a\x0f\x85\xe6\xa9\x1d\x39\x6f\x5b\x5c\xbe\x57\x7f\x9b\x38\x80" \
  "\x7c\x7d\x52\x3d\x6d\x79\x2f\x6e\xbc\x24\xa4\xec\xf2\xb3\xa4\x27" \
  "\xcd\xbb\xfb"
#define TEST7_224 \
  "\xf0\x70\x06\xf2\x5a\x0b\xea\x68\xcd\x76\xa2\x95\x87\xc2\x8d"
#define TEST8_224 \
  "\x18\x80\x40\x05\xdd\x4f\xbd\x15\x56\x29\x9d\x6f\x9d\x93\xdf\x62"
#define TEST9_224 \
  "\xa2\xbe\x6e\x46\x32\x81\x09\x02\x94\xd9\xce\x94\x82\x65\x69\x42" \
  "\x3a\x3a\x30\x5e\xd5\xe2\x11\x6c\xd4\xa4\xc9\x87\xfc\x06\x57\x00" \
  "\x64\x91\xb1\x49\xcc\xd4\xb5\x11\x30\xac\x62\xb1\x9d\xc2\x48\xc7" \
  "\x44\x54\x3d\x20\xcd\x39\x52\xdc\xed\x1f\x06\xcc\x3b\x18\xb9\x1f" \
  "\x3f\x55\x63\x3e\xcc\x30\x85\xf4\x90\x70\x60\xd2"
#define TEST10_224 \
  "\x55\xb2\x10\x07\x9c\x61\xb5\x3a\xdd\x52\x06\x22\xd1\xac\x97\xd5" \
  "\xcd\xbe\x8c\xb3\x3a\xa0\xae\x34\x45\x17\xbe\xe4\xd7\xba\x09\xab" \
  "\xc8\x53\x3c\x52\x50\x88\x7a\x43\xbe\xbb\xac\x90\x6c\x2e\x18\x37" \
  "\xf2\x6b\x36\xa5\x9a\xe3\xbe\x78\x14\xd5\x06\x89\x6b\x71\x8b\x2a" \
  "\x38\x3e\xcd\xac\x16\xb9\x61\x25\x55\x3f\x41\x6f\xf3\x2c\x66\x74" \
  "\xc7\x45\x99\xa9\x00\x53\x86\xd9\xce\x11\x12\x24\x5f\x48\xee\x47" \
  "\x0d\x39\x6c\x1e\xd6\x3b\x92\x67\x0c\xa5\x6e\xc8\x4d\xee\xa8\x14" \
  "\xb6\x13\x5e\xca\x54\x39\x2b\xde\xdb\x94\x89\xbc\x9b\x87\x5a\x8b" \
  "\xaf\x0d\xc1\xae\x78\x57\x36\x91\x4a\xb7\xda\xa2\x64\xbc\x07\x9d" \
  "\x26\x9f\x2c\x0d\x7e\xdd\xd8\x10\xa4\x26\x14\x5a\x07\x76\xf6\x7c" \
  "\x87\x82\x73"
#define TEST7_256 \
  "\xbe\x27\x46\xc6\xdb\x52\x76\x5f\xdb\x2f\x88\x70\x0f\x9a\x73"
#define TEST8_256 \
  "\xe3\xd7\x25\x70\xdc\xdd\x78\x7c\xe3\x88\x7a\xb2\xcd\x68\x46\x52"
#define TEST9_256 \
  "\x3e\x74\x03\x71\xc8\x10\xc2\xb9\x9f\xc0\x4e\x80\x49\x07\xef\x7c" \
  "\xf2\x6b\xe2\x8b\x57\xcb\x58\xa3\xe2\xf3\xc0\x07\x16\x6e\x49\xc1" \
  "\x2e\x9b\xa3\x4c\x01\x04\x06\x91\x29\xea\x76\x15\x64\x25\x45\x70" \
  "\x3a\x2b\xd9\x01\xe1\x6e\xb0\xe0\x5d\xeb\xa0\x14\xeb\xff\x64\x06" \
  "\xa0\x7d\x54\x36\x4e\xff\x74\x2d\xa7\x79\xb0\xb3"
#define TEST10_256 \
  "\x83\x26\x75\x4e\x22\x77\x37\x2f\x4f\xc1\x2b\x20\x52\x7a\xfe\xf0" \
  "\x4d\x8a\x05\x69\x71\xb1\x1a\xd5\x71\x23\xa7\xc1\x37\x76\x00\x00" \
  "\xd7\xbe\xf6\xf3\xc1\xf7\xa9\x08\x3a\xa3\x9d\x81\x0d\xb3\x10\x77" \
  "\x7d\xab\x8b\x1e\x7f\x02\xb8\x4a\x26\xc7\x73\x32\x5f\x8b\x23\x74" \
  "\xde\x7a\x4b\x5a\x58\xcb\x5c\x5c\xf3\x5b\xce\xe6\xfb\x94\x6e\x5b" \
  "\xd6\x94\xfa\x59\x3a\x8b\xeb\x3f\x9d\x65\x92\xec\xed\xaa\x66\xca" \
  "\x82\xa2\x9d\x0c\x51\xbc\xf9\x33\x62\x30\xe5\xd7\x84\xe4\xc0\xa4" \
  "\x3f\x8d\x79\xa3\x0a\x16\x5c\xba\xbe\x45\x2b\x77\x4b\x9c\x71\x09" \
  "\xa9\x7d\x13\x8f\x12\x92\x28\x96\x6f\x6c\x0a\xdc\x10\x6a\xad\x5a" \
  "\x9f\xdd\x30\x82\x57\x69\xb2\xc6\x71\xaf\x67\x59\xdf\x28\xeb\x39" \
  "\x3d\x54\xd6"
#define TEST7_384 \
  "\x8b\xc5\x00\xc7\x7c\xee\xd9\x87\x9d\xa9\x89\x10\x7c\xe0\xaa"
#define TEST8_384 \
  "\xa4\x1c\x49\x77\x79\xc0\x37\x5f\xf1\x0a\x7f\x4e\x08\x59\x17\x39"
#define TEST9_384 \
  "\x68\xf5\x01\x79\x2d\xea\x97\x96\x76\x70\x22\xd9\x3d\xa7\x16\x79" \
  "\x30\x99\x20\xfa\x10\x12\xae\xa3\x57\xb2\xb1\x33\x1d\x40\xa1\xd0" \
  "\x3c\x41\xc2\x40\xb3\xc9\xa7\x5b\x48\x92\xf4\xc0\x72\x4b\x68\xc8" \
  "\x75\x32\x1a\xb8\xcf\xe5\x02\x3b\xd3\x75\xbc\x0f\x94\xbd\x89\xfe" \
  "\x04\xf2\x97\x10\x5d\x7b\x82\xff\xc0\x02\x1a\xeb\x1c\xcb\x67\x4f" \
  "\x52\x44\xea\x34\x97\xde\x26\xa4\x19\x1c\x5f\x62\xe5\xe9\xa2\xd8" \
  "\x08\x2f\x05\x51\xf4\xa5\x30\x68\x26\xe9\x1c\xc0\x06\xce\x1b\xf6" \
  "\x0f\xf7\x19\xd4\x2f\xa5\x21\xc8\x71\xcd\x23\x94\xd9\x6e\xf4\x46" \
  "\x8f\x21\x96\x6b\x41\xf2\xba\x80\xc2\x6e\x83\xa9"
#define TEST10_384 \
  "\x39\x96\x69\xe2\x8f\x6b\x9c\x6d\xbc\xbb\x69\x12\xec\x10\xff\xcf" \
  "\x74\x79\x03\x49\xb7\xdc\x8f\xbe\x4a\x8e\x7b\x3b\x56\x21\xdb\x0f" \
  "\x3e\x7d\xc8\x7f\x82\x32\x64\xbb\xe4\x0d\x18\x11\xc9\xea\x20\x61" \
  "\xe1\xc8\x4a\xd1\x0a\x23\xfa\xc1\x72\x7e\x72\x02\xfc\x3f\x50\x42" \
  "\xe6\xbf\x58\xcb\xa8\xa2\x74\x6e\x1f\x64\xf9\xb9\xea\x35\x2c\x71" \
  "\x15\x07\x05\x3c\xf4\xe5\x33\x9d\x52\x86\x5f\x25\xcc\x22\xb5\xe8" \
  "\x77\x84\xa1\x2f\xc9\x61\xd6\x6c\xb6\xe8\x95\x73\x19\x9a\x2c\xe6" \
  "\x56\x5c\xbd\xf1\x3d\xca\x40\x38\x32\xcf\xcb\x0e\x8b\x72\x11\xe8" \
  "\x3a\xf3\x2a\x11\xac\x17\x92\x9f\xf1\xc0\x73\xa5\x1c\xc0\x27\xaa" \
  "\xed\xef\xf8\x5a\xad\x7c\x2b\x7c\x5a\x80\x3e\x24\x04\xd9\x6d\x2a" \
  "\x77\x35\x7b\xda\x1a\x6d\xae\xed\x17\x15\x1c\xb9\xbc\x51\x25\xa4" \
  "\x22\xe9\x41\xde\x0c\xa0\xfc\x50\x11\xc2\x3e\xcf\xfe\xfd\xd0\x96" \
  "\x76\x71\x1c\xf3\xdb\x0a\x34\x40\x72\x0e\x16\x15\xc1\xf2\x2f\xbc" \
  "\x3c\x72\x1d\xe5\x21\xe1\xb9\x9b\xa1\xbd\x55\x77\x40\x86\x42\x14" \
  "\x7e\xd0\x96"
#define TEST7_512 \
  "\x08\xec\xb5\x2e\xba\xe1\xf7\x42\x2d\xb6\x2b\xcd\x54\x26\x70"
#define TEST8_512 \
  "\x8d\x4e\x3c\x0e\x38\x89\x19\x14\x91\x81\x6e\x9d\x98\xbf\xf0\xa0"
#define TEST9_512 \
  "\x3a\xdd\xec\x85\x59\x32\x16\xd1\x61\x9a\xa0\x2d\x97\x56\x97\x0b" \
  "\xfc\x70\xac\xe2\x74\x4f\x7c\x6b\x27\x88\x15\x10\x28\xf7\xb6\xa2" \
  "\x55\x0f\xd7\x4a\x7e\x6e\x69\xc2\xc9\xb4\x5f\xc4\x54\x96\x6d\xc3" \
  "\x1d\x2e\x10\xda\x1f\x95\xce\x02\xbe\xb4\xbf\x87\x65\x57\x4c\xbd" \
  "\x6e\x83\x37\xef\x42\x0a\xdc\x98\xc1\x5c\xb6\xd5\xe4\xa0\x24\x1b" \
  "\xa0\x04\x6d\x25\x0e\x51\x02\x31\xca\xc2\x04\x6c\x99\x16\x06\xab" \
  "\x4e\xe4\x14\x5b\xee\x2f\xf4\xbb\x12\x3a\xab\x49\x8d\x9d\x44\x79" \
  "\x4f\x99\xcc\xad\x89\xa9\xa1\x62\x12\x59\xed\xa7\x0a\x5b\x6d\xd4" \
  "\xbd\xd8\x77\x78\xc9\x04\x3b\x93\x84\xf5\x49\x06"
#define TEST10_512 \
  "\xa5\x5f\x20\xc4\x11\xaa\xd1\x32\x80\x7a\x50\x2d\x65\x82\x4e\x31" \
  "\xa2\x30\x54\x32\xaa\x3d\x06\xd3\xe2\x82\xa8\xd8\x4e\x0d\xe1\xde" \
  "\x69\x74\xbf\x49\x54\x69\xfc\x7f\x33\x8f\x80\x54\xd5\x8c\x26\xc4" \
  "\x93\x60\xc3\xe8\x7a\xf5\x65\x23\xac\xf6\xd8\x9d\x03\xe5\x6f\xf2" \
  "\xf8\x68\x00\x2b\xc3\xe4\x31\xed\xc4\x4d\xf2\xf0\x22\x3d\x4b\xb3" \
  "\xb2\x43\x58\x6e\x1a\x7d\x92\x49\x36\x69\x4f\xcb\xba\xf8\x8d\x95" \
  "\x19\xe4\xeb\x50\xa6\x44\xf8\xe4\xf9\x5e\xb0\xea\x95\xbc\x44\x65" \
  "\xc8\x82\x1a\xac\xd2\xfe\x15\xab\x49\x81\x16\x4b\xbb\x6d\xc3\x2f" \
  "\x96\x90\x87\xa1\x45\xb0\xd9\xcc\x9c\x67\xc2\x2b\x76\x32\x99\x41" \
  "\x9c\xc4\x12\x8b\xe9\xa0\x77\xb3\xac\xe6\x34\x06\x4e\x6d\x99\x28" \
  "\x35\x13\xdc\x06\xe7\x51\x5d\x0d\x73\x13\x2e\x9a\x0d\xc6\xd3\xb1" \
  "\xf8\xb2\x46\xf1\xa9\x8a\x3f\xc7\x29\x41\xb1\xe3\xbb\x20\x98\xe8" \
  "\xbf\x16\xf2\x68\xd6\x4f\x0b\x0f\x47\x07\xfe\x1e\xa1\xa1\x79\x1b" \
  "\xa2\xf3\xc0\xc7\x58\xe5\xf5\x51\x86\x3a\x96\xc9\x49\xad\x47\xd7" \
  "\xfb\x40\xd2"
#define SHA1_SEED "\xd0\x56\x9c\xb3\x66\x5a\x8a\x43\xeb\x6e\xa2\x3d" \
  "\x75\xa3\xc4\xd2\x05\x4a\x0d\x7d"
#define SHA224_SEED "\xd0\x56\x9c\xb3\x66\x5a\x8a\x43\xeb\x6e\xa2" \
  "\x3d\x75\xa3\xc4\xd2\x05\x4a\x0d\x7d\x66\xa9\xca\x99\xc9\xce\xb0" \
  "\x27"
#define SHA256_SEED "\xf4\x1e\xce\x26\x13\xe4\x57\x39\x15\x69\x6b" \
  "\x5a\xdc\xd5\x1c\xa3\x28\xbe\x3b\xf5\x66\xa9\xca\x99\xc9\xce\xb0" \
  "\x27\x9c\x1c\xb0\xa7"
#define SHA384_SEED "\x82\x40\xbc\x51\xe4\xec\x7e\xf7\x6d\x18\xe3" \
  "\x52\x04\xa1\x9f\x51\xa5\x21\x3a\x73\xa8\x1d\x6f\x94\x46\x80\xd3" \
  "\x07\x59\x48\xb7\xe4\x63\x80\x4e\xa3\xd2\x6e\x13\xea\x82\x0d\x65" \
  "\xa4\x84\xbe\x74\x53"
#define SHA512_SEED "\x47\x3f\xf1\xb9\xb3\xff\xdf\xa1\x26\x69\x9a" \
  "\xc7\xef\x9e\x8e\x78\x77\x73\x09\x58\x24\xc6\x42\x55\x7c\x13\x99" \
  "\xd9\x8e\x42\x20\x44\x8d\xc3\x5b\x99\xbf\xdd\x44\x77\x95\x43\x92" \
  "\x4c\x1c\xe9\x3b\xc5\x94\x15\x38\x89\x5d\xb9\x88\x26\x1b\x00\x77" \
  "\x4b\x12\x27\x20\x39"

#define TESTCOUNT 19
#define HASHCOUNT 5
#define RANDOMCOUNT 4
#define HMACTESTCOUNT 19

#define PRINTNONE 0
#define PRINTTEXT 1
#define PRINTRAW 2
#define PRINTHEX 3
#define PRINTBASE64 4

#define PRINTPASSFAIL 1
#define PRINTFAIL 2

#define FIPS_LENGTH(x) (sizeof(x)-1)

PRIVATE CONST CHR1  c1HexDigits[] = "0123456789ABCDEF";

PRIVATE UINT1       gau1Md[FIPS_SHA_RAND_COUNT][USHA_MAX_HASH_SIZE];

/* Test arrays for hashes. */
struct _tHash
{
    CONST CHR1         *pc1Name;
    eShaVersion         whichSha;
    INT4                i4HashSize;
    struct
    {
        CHR1               *pc1TestArray;
        INT4                i4Length;
        FS_LONG             RepeatCount;
        INT4                i4ExtraBits;
        INT4                i4NoExtrabits;
        CONST CHR1         *pc1ResultArray;
    } tests[TESTCOUNT];
    CONST CHR1         *pc1RandomTest;
    CONST CHR1         *pc1RandomResults[RANDOMCOUNT];

} tHashes[HASHCOUNT] =
{
    {
        "AR_SHA1_ALGO", AR_SHA1_ALGO, SHA1_HASH_SIZE,
        {
            /* 1 */
            {
            TEST1, FIPS_LENGTH (TEST1), 1, 0, 0,
                    "A9993E364706816ABA3E25717850C26C9CD0D89D"},
                /* 2 */
            {
            TEST2_1, FIPS_LENGTH (TEST2_1), 1, 0, 0,
                    "84983E441C3BD26EBAAE4AA1F95129E5E54670F1"},
                /* 3 */
            {
            TEST3, FIPS_LENGTH (TEST3), 1000000, 0, 0,
                    "34AA973CD4C4DAA4F61EEB2BDBAD27316534016F"},
                /* 4 */
            {
            TEST4, FIPS_LENGTH (TEST4), 10, 0, 0,
                    "DEA356A2CDDD90C7A7ECEDC5EBB563934F460452"},
                /* 5 */
            {
            "", 0, 0, 0x98, 5, "29826B003B906E660EFF4027CE98AF3531AC75BA"},
                /* 6 */
            {
            "\x5e", 1, 1, 0, 0, "5E6F80A34A9798CAFC6A5DB96CC57BA4C4DB59C2"},
                /* 7 */
            {
            TEST7_1, FIPS_LENGTH (TEST7_1), 1, 0x80, 3,
                    "6239781E03729919C01955B3FFA8ACB60B988340"},
                /* 8 */
            {
            TEST8_1, FIPS_LENGTH (TEST8_1), 1, 0, 0,
                    "82ABFF6605DBE1C17DEF12A394FA22A82B544A35"},
                /* 9 */
            {
            TEST9_1, FIPS_LENGTH (TEST9_1), 1, 0xE0, 3,
                    "8C5B2A5DDAE5A97FC7F9D85661C672ADBF7933D4"},
                /* 10 */
            {
            TEST10_1, FIPS_LENGTH (TEST10_1), 1, 0, 0,
                    "CB0082C8F197D260991BA6A460E76E202BAD27B3"}
        }, SHA1_SEED,
        {
        "E216836819477C7F78E0D843FE4FF1B6D6C14CD4",
                "A2DBC7A5B1C6C0A8BCB7AAA41252A6A7D0690DBC",
                "DB1F9050BB863DFEF4CE37186044E2EEB17EE013",
                "127FDEDF43D372A51D5747C48FBFFE38EF6CDF7B"}
    },
    {
        "AR_SHA224_ALGO", AR_SHA224_ALGO, SHA224_HASH_SIZE,
        {
            /* 1 */
            {
            TEST1, FIPS_LENGTH (TEST1), 1, 0, 0,
                    "23097D223405D8228642A477BDA255B32AADBCE4BDA0B3F7E36C9DA7"},
                /* 2 */
            {
            TEST2_1, FIPS_LENGTH (TEST2_1), 1, 0, 0,
                    "75388B16512776CC5DBA5DA1FD890150B0C6455CB4F58B1952522525"},
                /* 3 */
            {
            TEST3, FIPS_LENGTH (TEST3), 1000000, 0, 0,
                    "20794655980C91D8BBB4C1EA97618A4BF03F42581948B2EE4EE7AD67"},
                /* 4 */
            {
            TEST4, FIPS_LENGTH (TEST4), 10, 0, 0,
                    "567F69F168CD7844E65259CE658FE7AADFA25216E68ECA0EB7AB8262"},
                /* 5 */
            {
            "", 0, 0, 0x68, 5,
                    "E3B048552C3C387BCAB37F6EB06BB79B96A4AEE5FF27F51531A9551C"},
                /* 6 */
            {
            "\x07", 1, 1, 0, 0,
                    "00ECD5F138422B8AD74C9799FD826C531BAD2FCABC7450BEE2AA8C2A"},
                /* 7 */
            {
            TEST7_224, FIPS_LENGTH (TEST7_224), 1, 0xA0, 3,
                    "1B01DB6CB4A9E43DED1516BEB3DB0B87B6D1EA43187462C608137150"},
                /* 8 */
            {
            TEST8_224, FIPS_LENGTH (TEST8_224), 1, 0, 0,
                    "DF90D78AA78821C99B40BA4C966921ACCD8FFB1E98AC388E56191DB1"},
                /* 9 */
            {
            TEST9_224, FIPS_LENGTH (TEST9_224), 1, 0xE0, 3,
                    "54BEA6EAB8195A2EB0A7906A4B4A876666300EEFBD1F3B8474F9CD57"},
                /* 10 */
            {
            TEST10_224, FIPS_LENGTH (TEST10_224), 1, 0, 0,
                    "0B31894EC8937AD9B91BDFBCBA294D9ADEFAA18E09305E9F20D5C3A4"}
        }, SHA224_SEED,
        {
        "100966A5B4FDE0B42E2A6C5953D4D7F41BA7CF79FD"
                "2DF431416734BE",
                "1DCA396B0C417715DEFAAE9641E10A2E99D55A"
                "BCB8A00061EB3BE8BD",
                "1864E627BDB2319973CD5ED7D68DA71D8B"
                "F0F983D8D9AB32C34ADB34",
                "A2406481FC1BCAF24DD08E6752E844" "709563FB916227FED598EB621F"}
    },
    {
        "AR_SHA256_ALGO", AR_SHA256_ALGO, SHA256_HASH_SIZE,
        {
            /* 1 */
            {
            TEST1, FIPS_LENGTH (TEST1), 1, 0, 0, "BA7816BF8F01CFEA4141"
                    "40DE5DAE2223B00361A396177A9CB410FF61F20015AD"},
                /* 2 */
            {
            TEST2_1, FIPS_LENGTH (TEST2_1), 1, 0, 0, "248D6A61D20638B8"
                    "E5C026930C3E6039A33CE45964FF2167F6ECEDD419DB06C1"},
                /* 3 */
            {
            TEST3, FIPS_LENGTH (TEST3), 1000000, 0, 0, "CDC76E5C9914FB92"
                    "81A1C7E284D73E67F1809A48A497200E046D39CCC7112CD0"},
                /* 4 */
            {
            TEST4, FIPS_LENGTH (TEST4), 10, 0, 0, "594847328451BDFA"
                    "85056225462CC1D867D877FB388DF0CE35F25AB5562BFBB5"},
                /* 5 */
            {
            "", 0, 0, 0x68, 5, "D6D3E02A31A84A8CAA9718ED6C2057BE"
                    "09DB45E7823EB5079CE7A573A3760F95"},
                /* 6 */
            {
            "\x19", 1, 1, 0, 0, "68AA2E2EE5DFF96E3355E6C7EE373E3D"
                    "6A4E17F75F9518D843709C0C9BC3E3D4"},
                /* 7 */
            {
            TEST7_256, FIPS_LENGTH (TEST7_256), 1, 0x60, 3, "77EC1DC8"
                    "9C821FF2A1279089FA091B35B8CD960BCAF7DE01C6A7680756BEB972"},
                /* 8 */
            {
            TEST8_256, FIPS_LENGTH (TEST8_256), 1, 0, 0, "175EE69B02BA"
                    "9B58E2B0A5FD13819CEA573F3940A94F825128CF4209BEABB4E8"},
                /* 9 */
            {
            TEST9_256, FIPS_LENGTH (TEST9_256), 1, 0xA0, 3, "3E9AD646"
                    "8BBBAD2AC3C2CDC292E018BA5FD70B960CF1679777FCE708FDB066E9"},
                /* 10 */
            {
        TEST10_256, FIPS_LENGTH (TEST10_256), 1, 0, 0, "97DBCA7D"
                    "F46D62C8A422C941DD7E835B8AD3361763F7E9B2D95F4F0DA6E1CCBC"},},
            SHA256_SEED,
        {
        "83D28614D49C3ADC1D6FC05DB5F48037C056F8D2A4CE44"
                "EC6457DEA5DD797CD1",
                "99DBE3127EF2E93DD9322D6A07909EB33B6399"
                "5E529B3F954B8581621BB74D39",
                "8D4BE295BB64661CA3C7EFD129A2F7"
                "25B33072DBDDE32385B9A87B9AF88EA76F",
                "40AF5D3F9716B040DF9408"
                "E31536B70FF906EC51B00447CA97D7DD97C12411F4"}
    },
    {
        "AR_SHA384_ALGO", AR_SHA384_ALGO, SHA384_HASH_SIZE,
        {
            /* 1 */
            {
            TEST1, FIPS_LENGTH (TEST1), 1, 0, 0,
                    "CB00753F45A35E8BB5A03D699AC65007272C32AB0EDED163"
                    "1A8B605A43FF5BED8086072BA1E7CC2358BAECA134C825A7"},
                /* 2 */
            {
            TEST2_2, FIPS_LENGTH (TEST2_2), 1, 0, 0,
                    "09330C33F71147E83D192FC782CD1B4753111B173B3B05D2"
                    "2FA08086E3B0F712FCC7C71A557E2DB966C3E9FA91746039"},
                /* 3 */
            {
            TEST3, FIPS_LENGTH (TEST3), 1000000, 0, 0,
                    "9D0E1809716474CB086E834E310A4A1CED149E9C00F24852"
                    "7972CEC5704C2A5B07B8B3DC38ECC4EBAE97DDD87F3D8985"},
                /* 4 */
            {
            TEST4, FIPS_LENGTH (TEST4), 10, 0, 0,
                    "2FC64A4F500DDB6828F6A3430B8DD72A368EB7F3A8322A70"
                    "BC84275B9C0B3AB00D27A5CC3C2D224AA6B61A0D79FB4596"},
                /* 5 */
            {
            "", 0, 0, 0x10, 5,
                    "8D17BE79E32B6718E07D8A603EB84BA0478F7FCFD1BB9399"
                    "5F7D1149E09143AC1FFCFC56820E469F3878D957A15A3FE4"},
                /* 6 */
            {
            "\xb9", 1, 1, 0, 0,
                    "BC8089A19007C0B14195F4ECC74094FEC64F01F90929282C"
                    "2FB392881578208AD466828B1C6C283D2722CF0AD1AB6938"},
                /* 7 */
            {
            TEST7_384, FIPS_LENGTH (TEST7_384), 1, 0xA0, 3,
                    "D8C43B38E12E7C42A7C9B810299FD6A770BEF30920F17532"
                    "A898DE62C7A07E4293449C0B5FA70109F0783211CFC4BCE3"},
                /* 8 */
            {
            TEST8_384, FIPS_LENGTH (TEST8_384), 1, 0, 0,
                    "C9A68443A005812256B8EC76B00516F0DBB74FAB26D66591"
                    "3F194B6FFB0E91EA9967566B58109CBC675CC208E4C823F7"},
                /* 9 */
            {
            TEST9_384, FIPS_LENGTH (TEST9_384), 1, 0xE0, 3,
                    "5860E8DE91C21578BB4174D227898A98E0B45C4C760F0095"
                    "49495614DAEDC0775D92D11D9F8CE9B064EEAC8DAFC3A297"},
                /* 10 */
            {
            TEST10_384, FIPS_LENGTH (TEST10_384), 1, 0, 0,
                    "4F440DB1E6EDD2899FA335F09515AA025EE177A79F4B4AAF"
                    "38E42B5C4DE660F5DE8FB2A5B2FBD2A3CBFFD20CFF1288C0"}
        }, SHA384_SEED,
        {
        "CE44D7D63AE0C91482998CF662A51EC80BF6FC68661A3C"
                "57F87566112BD635A743EA904DEB7D7A42AC808CABE697F38F",
                "F9C6D2"
                "61881FEE41ACD39E67AA8D0BAD507C7363EB67E2B81F45759F9C0FD7B503"
                "DF1A0B9E80BDE7BC333D75B804197D",
                "D96512D8C9F4A7A4967A366C01"
                "C6FD97384225B58343A88264847C18E4EF8AB7AEE4765FFBC3E30BD485D3"
                "638A01418F",
                "0CA76BD0813AF1509E170907A96005938BC985628290B2"
                "5FEF73CF6FAD68DDBA0AC8920C94E0541607B0915A7B4457F7"}
    },
    {
        "AR_SHA512_ALGO", AR_SHA512_ALGO, SHA512_HASH_SIZE,
        {
            /* 1 */
            {
            TEST1, FIPS_LENGTH (TEST1), 1, 0, 0,
                    "DDAF35A193617ABACC417349AE20413112E6FA4E89A97EA2"
                    "0A9EEEE64B55D39A2192992A274FC1A836BA3C23A3FEEBBD"
                    "454D4423643CE80E2A9AC94FA54CA49F"},
                /* 2 */
            {
            TEST2_2, FIPS_LENGTH (TEST2_2), 1, 0, 0,
                    "8E959B75DAE313DA8CF4F72814FC143F8F7779C6EB9F7FA1"
                    "7299AEADB6889018501D289E4900F7E4331B99DEC4B5433A"
                    "C7D329EEB6DD26545E96E55B874BE909"},
                /* 3 */
            {
            TEST3, FIPS_LENGTH (TEST3), 1000000, 0, 0,
                    "E718483D0CE769644E2E42C7BC15B4638E1F98B13B204428"
                    "5632A803AFA973EBDE0FF244877EA60A4CB0432CE577C31B"
                    "EB009C5C2C49AA2E4EADB217AD8CC09B"},
                /* 4 */
            {
            TEST4, FIPS_LENGTH (TEST4), 10, 0, 0,
                    "89D05BA632C699C31231DED4FFC127D5A894DAD412C0E024"
                    "DB872D1ABD2BA8141A0F85072A9BE1E2AA04CF33C765CB51"
                    "0813A39CD5A84C4ACAA64D3F3FB7BAE9"},
                /* 5 */
            {
            "", 0, 0, 0xB0, 5,
                    "D4EE29A9E90985446B913CF1D1376C836F4BE2C1CF3CADA0"
                    "720A6BF4857D886A7ECB3C4E4C0FA8C7F95214E41DC1B0D2"
                    "1B22A84CC03BF8CE4845F34DD5BDBAD4"},
                /* 6 */
            {
            "\xD0", 1, 1, 0, 0,
                    "9992202938E882E73E20F6B69E68A0A7149090423D93C81B"
                    "AB3F21678D4ACEEEE50E4E8CAFADA4C85A54EA8306826C4A"
                    "D6E74CECE9631BFA8A549B4AB3FBBA15"},
                /* 7 */
            {
            TEST7_512, FIPS_LENGTH (TEST7_512), 1, 0x80, 3,
                    "ED8DC78E8B01B69750053DBB7A0A9EDA0FB9E9D292B1ED71"
                    "5E80A7FE290A4E16664FD913E85854400C5AF05E6DAD316B"
                    "7359B43E64F8BEC3C1F237119986BBB6"},
                /* 8 */
            {
            TEST8_512, FIPS_LENGTH (TEST8_512), 1, 0, 0,
                    "CB0B67A4B8712CD73C9AABC0B199E9269B20844AFB75ACBD"
                    "D1C153C9828924C3DDEDAAFE669C5FDD0BC66F630F677398"
                    "8213EB1B16F517AD0DE4B2F0C95C90F8"},
                /* 9 */
            {
            TEST9_512, FIPS_LENGTH (TEST9_512), 1, 0x80, 3,
                    "32BA76FC30EAA0208AEB50FFB5AF1864FDBF17902A4DC0A6"
                    "82C61FCEA6D92B783267B21080301837F59DE79C6B337DB2"
                    "526F8A0A510E5E53CAFED4355FE7C2F1"},
                /* 10 */
            {
            TEST10_512, FIPS_LENGTH (TEST10_512), 1, 0, 0,
                    "C665BEFB36DA189D78822D10528CBF3B12B3EEF726039909"
                    "C1A16A270D48719377966B957A878E720584779A62825C18"
                    "DA26415E49A7176A894E7510FD1451F5"}
        }, SHA512_SEED,
        {
        "2FBB1E7E00F746BA514FBC8C421F36792EC0E11FF5EFC3"
                "78E1AB0C079AA5F0F66A1E3EDBAEB4F9984BE14437123038A452004A5576"
                "8C1FD8EED49E4A21BEDCD0",
                "25CBE5A4F2C7B1D7EF07011705D50C62C5"
                "000594243EAFD1241FC9F3D22B58184AE2FEE38E171CF8129E29459C9BC2"
                "EF461AF5708887315F15419D8D17FE7949",
                "5B8B1F2687555CE2D7182B"
                "92E5C3F6C36547DA1C13DBB9EA4F73EA4CBBAF89411527906D35B1B06C1B"
                "6A8007D05EC66DF0A406066829EAB618BDE3976515AAFC",
                "46E36B007D"
                "19876CDB0B29AD074FE3C08CDD174D42169D6ABE5A1414B6E79707DF5877"
                "6A98091CF431854147BB6D3C66D43BFBC108FD715BDE6AA127C2B0E79F"}
    }
};

/* Test arrays for HMAC. */
struct _tHmacHash
{
    CHR1               *pc1KeyArray[HASHCOUNT];
    INT4                ai4KeyLength[HASHCOUNT];
    CHR1               *pc1DataArray[HASHCOUNT];
    INT4                ai4DataLength[HASHCOUNT];
    CONST CHR1         *pc1ResultArray[HASHCOUNT];
    INT4                ai4ResultLength[HASHCOUNT];
} tHmacHashes[HMACTESTCOUNT] =
{
    {                            /* 1 */
        {
        "\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b"
                "\x0b\x0b\x0b\x0b\x0b"},
        {
        20},
        {
        "\x48\x69\x20\x54\x68\x65\x72\x65" /* "Hi There" */ },
        {
        8},
        {                        /* HMAC-SHA-1 */
            "B617318655057264E28BC0B6FB378C8EF146BE00",
                /* HMAC-SHA-224 */
                "896FB1128ABBDF196832107CD49DF33F47B4B1169912BA4F53684B22",
                /* HMAC-SHA-256 */
                "B0344C61D8DB38535CA8AFCEAF0BF12B881DC200C9833DA726E9376C2E32"
                "CFF7",
                /* HMAC-SHA-384 */
                "AFD03944D84895626B0825F4AB46907F15F9DADBE4101EC682AA034C7CEB"
                "C59CFAEA9EA9076EDE7F4AF152E8B2FA9CB6",
                /* HMAC-SHA-512 */
        "87AA7CDEA5EF619D4FF0B4241A1D6CB02379F4E2CE4EC2787AD0B30545E1"
                "7CDEDAA833B7D6B8A702038B274EAEA3F4E4BE9D914EEB61F1702E696C20"
                "3A126854"},
        {
        SHA1_HASH_SIZE, SHA224_HASH_SIZE, SHA256_HASH_SIZE,
                SHA384_HASH_SIZE, SHA512_HASH_SIZE}
    },
    {                            /* 2 */
        {
        "\x4a\x65\x66\x65" /* "Jefe" */ },
        {
        4},
        {
            "\x77\x68\x61\x74\x20\x64\x6f\x20\x79\x61\x20\x77\x61\x6e\x74"
                "\x20\x66\x6f\x72\x20\x6e\x6f\x74\x68\x69\x6e\x67\x3f"
                /* "what do ya want for nothing?" */
        },
        {
        28},
        {                        /* HMAC-SHA-1 */
            "EFFCDF6AE5EB2FA2D27416D5F184DF9C259A7C79",
                /* HMAC-SHA-224 */
                "A30E01098BC6DBBF45690F3A7E9E6D0F8BBEA2A39E6148008FD05E44",
                /* HMAC-SHA-256 */
                "5BDCC146BF60754E6A042426089575C75A003F089D2739839DEC58B964EC"
                "3843",
                /* HMAC-SHA-384 */
                "AF45D2E376484031617F78D2B58A6B1B9C7EF464F5A01B47E42EC3736322"
                "445E8E2240CA5E69E2C78B3239ECFAB21649",
                /* HMAC-SHA-512 */
        "164B7A7BFCF819E2E395FBE73B56E0A387BD64222E831FD610270CD7EA25"
                "05549758BF75C05A994A6D034F65F8F0E6FDCAEAB1A34D4A6B4B636E070A"
                "38BCE737"},
        {
        SHA1_HASH_SIZE, SHA224_HASH_SIZE, SHA256_HASH_SIZE,
                SHA384_HASH_SIZE, SHA512_HASH_SIZE}
    },
    {                            /* 3 */
        {
        "\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"
                "\xaa\xaa\xaa\xaa\xaa"},
        {
        20},
        {
        "\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd"
                "\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd"
                "\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd"
                "\xdd\xdd\xdd\xdd\xdd"},
        {
        50},
        {
            /* HMAC-SHA-1 */
            "125D7342B9AC11CD91A39AF48AA17B4F63F175D3",
                /* HMAC-SHA-224 */
                "7FB3CB3588C6C1F6FFA9694D7D6AD2649365B0C1F65D69D1EC8333EA",
                /* HMAC-SHA-256 */
                "773EA91E36800E46854DB8EBD09181A72959098B3EF8C122D9635514CED5"
                "65FE",
                /* HMAC-SHA-384 */
                "88062608D3E6AD8A0AA2ACE014C8A86F0AA635D947AC9FEBE83EF4E55966"
                "144B2A5AB39DC13814B94E3AB6E101A34F27",
                /* HMAC-SHA-512 */
        "FA73B0089D56A284EFB0F0756C890BE9B1B5DBDD8EE81A3655F83E33B227"
                "9D39BF3E848279A722C806B485A47E67C807B946A337BEE8942674278859"
                "E13292FB"},
        {
        SHA1_HASH_SIZE, SHA224_HASH_SIZE, SHA256_HASH_SIZE,
                SHA384_HASH_SIZE, SHA512_HASH_SIZE}
    },
    {                            /* 4 */
        {
        "\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f"
                "\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19"},
        {
        25},
        {
        "\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd"
                "\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd"
                "\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd"
                "\xcd\xcd\xcd\xcd\xcd"},
        {
        50},
        {                        /* HMAC-SHA-1 */
            "4C9007F4026250C6BC8414F9BF50C86C2D7235DA",
                /* HMAC-SHA-224 */
                "6C11506874013CAC6A2ABC1BB382627CEC6A90D86EFC012DE7AFEC5A",
                /* HMAC-SHA-256 */
                "82558A389A443C0EA4CC819899F2083A85F0FAA3E578F8077A2E3FF46729"
                "665B",
                /* HMAC-SHA-384 */
                "3E8A69B7783C25851933AB6290AF6CA77A9981480850009CC5577C6E1F57"
                "3B4E6801DD23C4A7D679CCF8A386C674CFFB",
                /* HMAC-SHA-512 */
        "B0BA465637458C6990E5A8C5F61D4AF7E576D97FF94B872DE76F8050361E"
                "E3DBA91CA5C11AA25EB4D679275CC5788063A5F19741120C4F2DE2ADEBEB"
                "10A298DD"},
        {
        SHA1_HASH_SIZE, SHA224_HASH_SIZE, SHA256_HASH_SIZE,
                SHA384_HASH_SIZE, SHA512_HASH_SIZE}
    },
    {                            /* 5 */
        {
        "\x0c\x0c\x0c\x0c\x0c\x0c\x0c\x0c\x0c\x0c\x0c\x0c\x0c\x0c\x0c"
                "\x0c\x0c\x0c\x0c\x0c"},
        {
        20},
        {
        "Test With Truncation"},
        {
        20},
        {                        /* HMAC-SHA-1 */
            "4C1A03424B55E07FE7F27BE1",
                /* HMAC-SHA-224 */
                "0E2AEA68A90C8D37C988BCDB9FCA6FA8",
                /* HMAC-SHA-256 */
                "A3B6167473100EE06E0C796C2955552B",
                /* HMAC-SHA-384 */
                "3ABF34C3503B2A23A46EFC619BAEF897",
                /* HMAC-SHA-512 */
        "415FAD6271580A531D4179BC891D87A6"},
        {
        12, 16, 16, 16, 16}
    },
    {                            /* 6 */
        {
        "\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"
                "\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"
                "\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"
                "\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"
                "\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"
                "\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"
                "\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"
                "\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"
                "\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"},
        {
        80, 131},
        {
        "Test Using Larger Than Block-Size Key - Hash Key First"},
        {
        54},
        {
            /* HMAC-SHA-1 */
            "AA4AE5E15272D00E95705637CE8A3B55ED402112",
                /* HMAC-SHA-224 */
                "95E9A0DB962095ADAEBE9B2D6F0DBCE2D499F112F2D2B7273FA6870E",
                /* HMAC-SHA-256 */
                "60E431591EE0B67F0D8A26AACBF5B77F8E0BC6213728C5140546040F0EE3"
                "7F54",
                /* HMAC-SHA-384 */
                "4ECE084485813E9088D2C63A041BC5B44F9EF1012A2B588F3CD11F05033A"
                "C4C60C2EF6AB4030FE8296248DF163F44952",
                /* HMAC-SHA-512 */
        "80B24263C7C1A3EBB71493C1DD7BE8B49B46D1F41B4AEEC1121B013783F8"
                "F3526B56D037E05F2598BD0FD2215D6A1E5295E64F73F63F0AEC8B915A98"
                "5D786598"},
        {
        SHA1_HASH_SIZE, SHA224_HASH_SIZE, SHA256_HASH_SIZE,
                SHA384_HASH_SIZE, SHA512_HASH_SIZE}
    },
    {                            /* 7 */
        {
        "\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"
                "\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"
                "\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"
                "\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"
                "\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"
                "\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"
                "\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"
                "\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"
                "\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"},
        {
        80, 131},
        {
            "Test Using Larger Than Block-Size Key and "
                "Larger Than One Block-Size Data",
                "\x54\x68\x69\x73\x20\x69\x73\x20\x61\x20\x74\x65\x73\x74\x20"
                "\x75\x73\x69\x6e\x67\x20\x61\x20\x6c\x61\x72\x67\x65\x72\x20"
                "\x74\x68\x61\x6e\x20\x62\x6c\x6f\x63\x6b\x2d\x73\x69\x7a\x65"
                "\x20\x6b\x65\x79\x20\x61\x6e\x64\x20\x61\x20\x6c\x61\x72\x67"
                "\x65\x72\x20\x74\x68\x61\x6e\x20\x62\x6c\x6f\x63\x6b\x2d\x73"
                "\x69\x7a\x65\x20\x64\x61\x74\x61\x2e\x20\x54\x68\x65\x20\x6b"
                "\x65\x79\x20\x6e\x65\x65\x64\x73\x20\x74\x6f\x20\x62\x65\x20"
                "\x68\x61\x73\x68\x65\x64\x20\x62\x65\x66\x6f\x72\x65\x20\x62"
                "\x65\x69\x6e\x67\x20\x75\x73\x65\x64\x20\x62\x79\x20\x74\x68"
                "\x65\x20\x48\x4d\x41\x43\x20\x61\x6c\x67\x6f\x72\x69\x74\x68"
                "\x6d\x2e"
                /* "This is a test using a larger than block-size key and a "
                   "larger than block-size data. The key needs to be hashed "
                   "before being used by the HMAC algorithm." */
        },
        {
        73, 152},
        {
            /* HMAC-SHA-1 */
            "E8E99D0F45237D786D6BBAA7965C7808BBFF1A91",
                /* HMAC-SHA-224 */
                "3A854166AC5D9F023F54D517D0B39DBD946770DB9C2B95C9F6F565D1",
                /* HMAC-SHA-256 */
                "9B09FFA71B942FCB27635FBCD5B0E944BFDC63644F0713938A7F51535C3A"
                "35E2",
                /* HMAC-SHA-384 */
                "6617178E941F020D351E2F254E8FD32C602420FEB0B8FB9ADCCEBB82461E"
                "99C5A678CC31E799176D3860E6110C46523E",
                /* HMAC-SHA-512 */
        "E37B6A775DC87DBAA4DFA9F96E5E3FFDDEBD71F8867289865DF5A32D20CD"
                "C944B6022CAC3C4982B10D5EEB55C3E4DE15134676FB6DE0446065C97440"
                "FA8C6A58"},
        {
        SHA1_HASH_SIZE, SHA224_HASH_SIZE, SHA256_HASH_SIZE,
                SHA384_HASH_SIZE, SHA512_HASH_SIZE}
    },
    {                            /* 8 */
        {
        "\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b"
                "\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b"
                "\x0b\x0b"},
        {
        32},
        {
        "\x48\x69\x20\x54\x68\x65\x72\x65" /* "Hi There" */ },
        {
        8},
        {                        /* HMAC-SHA-1 */
            "B617318655057264E28BC0B6FB378C8EF146BE00",
                /* HMAC-SHA-224 */
                "896FB1128ABBDF196832107CD49DF33F47B4B1169912BA4F53684B22",
                /* HMAC-SHA-256 */
                "198A607EB44BFBC69903A0F1CF2BBDC5BA0AA3F3D9AE3C1C7A3B1696A0B68CF7",
                /* HMAC-SHA-384 */
                "AFD03944D84895626B0825F4AB46907F15F9DADBE4101EC682AA034C7CEB"
                "C59CFAEA9EA9076EDE7F4AF152E8B2FA9CB6",
                /* HMAC-SHA-512 */
        "87AA7CDEA5EF619D4FF0B4241A1D6CB02379F4E2CE4EC2787AD0B30545E1"
                "7CDEDAA833B7D6B8A702038B274EAEA3F4E4BE9D914EEB61F1702E696C20"
                "3A126854"},
        {
        SHA1_HASH_SIZE, SHA224_HASH_SIZE, SHA256_HASH_SIZE,
                SHA384_HASH_SIZE, SHA512_HASH_SIZE}
    },
    {                            /* 9 */
        {
            "\x4a\x65\x66\x65\x4a\x65\x66\x65\x4a\x65\x66\x65\x4a\x65\x66\x65"
                /* "JefeJefJefeJefee" */
                "\x4a\x65\x66\x65\x4a\x65\x66\x65\x4a\x65\x66\x65\x4a\x65\x66\x65"
                /* "JefeJefJefeJefee" */
        },
        {
        32},
        {
            "\x77\x68\x61\x74\x20\x64\x6f\x20\x79\x61\x20\x77\x61\x6e\x74"
                "\x20\x66\x6f\x72\x20\x6e\x6f\x74\x68\x69\x6e\x67\x3f"
                /* "what do ya want for nothing?" */
        },
        {
        28},
        {                        /* HMAC-SHA-1 */
            "EFFCDF6AE5EB2FA2D27416D5F184DF9C259A7C79",
                /* HMAC-SHA-224 */
                "A30E01098BC6DBBF45690F3A7E9E6D0F8BBEA2A39E6148008FD05E44",
                /* HMAC-SHA-256 */
                "167F928588C5CC2EEF8E3093CAA0E87C9FF566A14794AA61648D81621A2A40C6",
                /* HMAC-SHA-384 */
                "AF45D2E376484031617F78D2B58A6B1B9C7EF464F5A01B47E42EC3736322"
                "445E8E2240CA5E69E2C78B3239ECFAB21649",
                /* HMAC-SHA-512 */
        "164B7A7BFCF819E2E395FBE73B56E0A387BD64222E831FD610270CD7EA25"
                "05549758BF75C05A994A6D034F65F8F0E6FDCAEAB1A34D4A6B4B636E070A"
                "38BCE737"},
        {
        SHA1_HASH_SIZE, SHA224_HASH_SIZE, SHA256_HASH_SIZE,
                SHA384_HASH_SIZE, SHA512_HASH_SIZE}
    },
    {                            /* 10 */
        {
        "\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"
                "\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"
                "\xaa\xaa"},
        {
        32},
        {
        "\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd"
                "\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd"
                "\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd"
                "\xdd\xdd\xdd\xdd\xdd"},
        {
        50},
        {                        /* HMAC-SHA-1 */
            "",
                /* HMAC-SHA-224 */
                "",
                /* HMAC-SHA-256 */
                "CDCB1220D1ECCCEA91E53ABA3092F962E549FE6CE9ED7FDC43191FBDE45C30B0",
                /* HMAC-SHA-384 */
                "",
                /* HMAC-SHA-512 */
        ""},
        {
        SHA1_HASH_SIZE, SHA224_HASH_SIZE, SHA256_HASH_SIZE,
                SHA384_HASH_SIZE, SHA512_HASH_SIZE}
    },
    {                            /* 11 */
        {
        "\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f"
                "\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1a\x1b\x1c\x1d\x1e"
                "\x1f\x20"},
        {
        32},
        {
        "\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd"
                "\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd"
                "\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd"
                "\xcd\xcd\xcd\xcd\xcd"},
        {
        50},
        {                        /* HMAC-SHA-1 */
            "",
                /* HMAC-SHA-224 */
                "",
                /* HMAC-SHA-256 */
                "372EFCF9B40B35C2115B1346903D2EF42FCED46F0846E7257BB156D3D7B30D3F",
                /* HMAC-SHA-384 */
                "",
                /* HMAC-SHA-512 */
        ""},
        {
        SHA1_HASH_SIZE, SHA224_HASH_SIZE, SHA256_HASH_SIZE,
                SHA384_HASH_SIZE, SHA512_HASH_SIZE}
    },
    {                            /* 12 */
        {
        "\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b"
                "\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b"
                "\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b"
                "\x0b\x0b\x0b"},
        {
        48},
        {
        "\x48\x69\x20\x54\x68\x65\x72\x65" /* "Hi There" */ },
        {
        8},
        {                        /* HMAC-SHA-1 */
            "B617318655057264E28BC0B6FB378C8EF146BE00",
                /* HMAC-SHA-224 */
                "896FB1128ABBDF196832107CD49DF33F47B4B1169912BA4F53684B22",
                /* HMAC-SHA-256 */
                "",
                /* HMAC-SHA-384 */
                "B6A8D5636F5C6A7224F9977DCF7EE6C7FB6D0C48CBDEE9737A959796489B"
                "DDBC4C5DF61D5B3297B4FB68DAB9F1B582C2",
                /* HMAC-SHA-512 */
        "87AA7CDEA5EF619D4FF0B4241A1D6CB02379F4E2CE4EC2787AD0B30545E1"
                "7CDEDAA833B7D6B8A702038B274EAEA3F4E4BE9D914EEB61F1702E696C20"
                "3A126854"},
        {
        SHA1_HASH_SIZE, SHA224_HASH_SIZE, SHA256_HASH_SIZE,
                SHA384_HASH_SIZE, SHA512_HASH_SIZE}
    },
    {                            /* 13 */
        {
            "\x4a\x65\x66\x65\x4a\x65\x66\x65\x4a\x65\x66\x65\x4a\x65\x66\x65"
                /* "JefeJefJefeJefee" */
                "\x4a\x65\x66\x65\x4a\x65\x66\x65\x4a\x65\x66\x65\x4a\x65\x66\x65"
                /* "JefeJefJefeJefee" */
                "\x4a\x65\x66\x65\x4a\x65\x66\x65\x4a\x65\x66\x65\x4a\x65\x66\x65"
                /* "JefeJefJefeJefee" */
        },
        {
        48},
        {
            "\x77\x68\x61\x74\x20\x64\x6f\x20\x79\x61\x20\x77\x61\x6e\x74"
                "\x20\x66\x6f\x72\x20\x6e\x6f\x74\x68\x69\x6e\x67\x3f"
                /* "what do ya want for nothing?" */
        },
        {
        28},
        {                        /* HMAC-SHA-1 */
            "EFFCDF6AE5EB2FA2D27416D5F184DF9C259A7C79",
                /* HMAC-SHA-224 */
                "A30E01098BC6DBBF45690F3A7E9E6D0F8BBEA2A39E6148008FD05E44",
                /* HMAC-SHA-256 */
                "167F928588C5CC2EEF8E3093CAA0E87C9FF566A14794AA61648D81621A2A40C6",
                /* HMAC-SHA-384 */
                "2C7353974F1842FD66D53C452CA42122B28C0B594CFB184DA86A368E9B8E16F5"
                "349524CA4E82400CBDE0686D403371C9",
                /* HMAC-SHA-512 */
        "164B7A7BFCF819E2E395FBE73B56E0A387BD64222E831FD610270CD7EA25"
                "05549758BF75C05A994A6D034F65F8F0E6FDCAEAB1A34D4A6B4B636E070A"
                "38BCE737"},
        {
        SHA1_HASH_SIZE, SHA224_HASH_SIZE, SHA256_HASH_SIZE,
                SHA384_HASH_SIZE, SHA512_HASH_SIZE}
    },
    {                            /* 14 */
        {
        "\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"
                "\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"
                "\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"
                "\xaa\xaa\xaa"},
        {
        48},
        {
        "\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd"
                "\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd"
                "\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd"
                "\xdd\xdd\xdd\xdd\xdd"},
        {
        50},
        {                        /* HMAC-SHA-1 */
            "",
                /* HMAC-SHA-224 */
                "",
                /* HMAC-SHA-256 */
                "CDCB1220D1ECCCEA91E53ABA3092F962E549FE6CE9ED7FDC43191FBDE45C30B0",
                /* HMAC-SHA-384 */
                "809F439BE00274321D4A538652164B53554A508184A0C3160353E3428597003D"
                "35914A18770F9443987054944B7C4B4A",
                /* HMAC-SHA-512 */
        ""},
        {
        SHA1_HASH_SIZE, SHA224_HASH_SIZE, SHA256_HASH_SIZE,
                SHA384_HASH_SIZE, SHA512_HASH_SIZE}
    },
    {                            /* 15 */
        {
        "\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f"
                "\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1a\x1b\x1c\x1d\x1e"
                "\x1f\x20\x0a\x0b\x0c\x0d\x0e\x0f\x10\x11\x12\x13\x14\x15\x16"
                "\x17\x18\x19"},
        {
        48},
        {
        "\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd"
                "\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd"
                "\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd"
                "\xcd\xcd\xcd\xcd\xcd"},
        {
        50},
        {                        /* HMAC-SHA-1 */
            "",
                /* HMAC-SHA-224 */
                "",
                /* HMAC-SHA-256 */
                "",
                /* HMAC-SHA-384 */
                "5B540085C6E6358096532B2493609ED1CB298F774F87BB5C2EBF182C83CC"
                "7428707FB92EAB2536A5812258228BC96687",
                /* HMAC-SHA-512 */
        ""},
        {
        SHA1_HASH_SIZE, SHA224_HASH_SIZE, SHA256_HASH_SIZE,
                SHA384_HASH_SIZE, SHA512_HASH_SIZE}
    },
    {                            /* 16 */
        {
        "\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b"
                "\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b"
                "\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b"
                "\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b\x0b"
                "\x0b\x0b\x0b\x0b"},
        {
        64},
        {
        "\x48\x69\x20\x54\x68\x65\x72\x65" /* "Hi There" */ },
        {
        8},
        {                        /* HMAC-SHA-1 */
            "B617318655057264E28BC0B6FB378C8EF146BE00",
                /* HMAC-SHA-224 */
                "896FB1128ABBDF196832107CD49DF33F47B4B1169912BA4F53684B22",
                /* HMAC-SHA-256 */
                "",
                /* HMAC-SHA-384 */
                "B6A8D5636F5C6A7224F9977DCF7EE6C7FB6D0C48CBDEE9737A959796489B"
                "DDBC4C5DF61D5B3297B4FB68DAB9F1B582C2",
                /* HMAC-SHA-512 */
        "637EDC6E01DCE7E6742A99451AAE82DF23DA3E92439E590E43E761B33E910"
                "FB8AC2878EBD5803F6F0B61DBCE5E251FF8789A4722C1BE65AEA45FD464E8"
                "9F8F5B"},
        {
        SHA1_HASH_SIZE, SHA224_HASH_SIZE, SHA256_HASH_SIZE,
                SHA384_HASH_SIZE, SHA512_HASH_SIZE}
    },
    {                            /* 17 */
        {
            "\x4a\x65\x66\x65\x4a\x65\x66\x65\x4a\x65\x66\x65\x4a\x65\x66\x65"
                /* "JefeJefJefeJefee" */
                "\x4a\x65\x66\x65\x4a\x65\x66\x65\x4a\x65\x66\x65\x4a\x65\x66\x65"
                /* "JefeJefJefeJefee" */
                "\x4a\x65\x66\x65\x4a\x65\x66\x65\x4a\x65\x66\x65\x4a\x65\x66\x65"
                /* "JefeJefJefeJefee" */
                "\x4a\x65\x66\x65\x4a\x65\x66\x65\x4a\x65\x66\x65\x4a\x65\x66\x65"
                /* "JefeJefJefeJefee" */
        },
        {
        64},
        {
            "\x77\x68\x61\x74\x20\x64\x6f\x20\x79\x61\x20\x77\x61\x6e\x74"
                "\x20\x66\x6f\x72\x20\x6e\x6f\x74\x68\x69\x6e\x67\x3f"
                /* "what do ya want for nothing?" */ },
        {
        28},
        {                        /* HMAC-SHA-1 */
            "EFFCDF6AE5EB2FA2D27416D5F184DF9C259A7C79",
                /* HMAC-SHA-224 */
                "A30E01098BC6DBBF45690F3A7E9E6D0F8BBEA2A39E6148008FD05E44",
                /* HMAC-SHA-256 */
                "167F928588C5CC2EEF8E3093CAA0E87C9FF566A14794AA61648D81621A2A40C6",
                /* HMAC-SHA-384 */
                "2C7353974F1842FD66D53C452CA42122B28C0B594CFB184DA86A368E9B8E16F5"
                "349524CA4E82400CBDE0686D403371C9",
                /* HMAC-SHA-512 */
        "CB370917AE8A7CE28CFD1D8F4705D6141C173B2A9362C15DF235DFB251B154546"
                "AA334AE9FB9AFC2184932D8695E397BFA0FFB93466CFCCEAAE38C833B7DBA38"},
        {
        SHA1_HASH_SIZE, SHA224_HASH_SIZE, SHA256_HASH_SIZE,
                SHA384_HASH_SIZE, SHA512_HASH_SIZE}
    },
    {                            /* 18 */
        {
        "\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"
                "\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"
                "\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"
                "\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"
                "\xaa\xaa\xaa\xaa"},
        {
        64},
        {
        "\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd"
                "\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd"
                "\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd\xdd"
                "\xdd\xdd\xdd\xdd\xdd"},
        {
        50},
        {
            /* HMAC-SHA-1 */
            "",
                /* HMAC-SHA-224 */
                "",
                /* HMAC-SHA-256 */
                "CDCB1220D1ECCCEA91E53ABA3092F962E549FE6CE9ED7FDC43191FBDE45C30B0",
                /* HMAC-SHA-384 */
                "809F439BE00274321D4A538652164B53554A508184A0C3160353E3428597003D"
                "35914A18770F9443987054944B7C4B4A",
                /* HMAC-SHA-512 */
        "2EE7ACD783624CA9398710F3EE05AE41B9F9B0510C87E49E586CC9BF961733D8"
                "623C7B55CEBEFCCF02D5581ACC1C9D5FB1FF68A1DE45509FBE4DA9A433922655"},
        {
        SHA1_HASH_SIZE, SHA224_HASH_SIZE, SHA256_HASH_SIZE,
                SHA384_HASH_SIZE, SHA512_HASH_SIZE}
    },
    {                            /* 19 */
        {
        "\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f\x10"
                "\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1a\x1b\x1c\x1d\x1e\x1f\x20"
                "\x21\x22\x23\x24\x25\x26\x27\x28\x29\x2a\x2b\x2c\x2d\x2e\x2f\x30"
                "\x31\x32\x33\x34\x35\x36\x37\x38\x39\x3a\x3b\x3c\x3d\x3e\x3f\x40"},
        {
        64},
        {
        "\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd"
                "\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd"
                "\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd\xcd"
                "\xcd\xcd\xcd\xcd\xcd"},
        {
        50},
        {
            /* HMAC-SHA-1 */
            "",
                /* HMAC-SHA-224 */
                "",
                /* HMAC-SHA-256 */
                "",
                /* HMAC-SHA-384 */
                "5B540085C6E6358096532B2493609ED1CB298F774F87BB5C2EBF182C83CC"
                "7428707FB92EAB2536A5812258228BC96687",
                /* HMAC-SHA-512 */
        "5E6688E5A3DAEC826CA32EAEA224EFF5E700628947470E13AD01302561BA"
                "B108B8C48CBC6B807DCFBD850521A685BABC7EAE4A2A2E660DC0E86B931D"
                "65503FD2"},
        {
        SHA1_HASH_SIZE, SHA224_HASH_SIZE, SHA256_HASH_SIZE,
                SHA384_HASH_SIZE, SHA512_HASH_SIZE}
},};

/***************************************************************************
 * Function           : FipsShaCheckMatch 
 *
 * Description        : Check the hash value against the expected string,
 *                      expressed in hex
 *
 * Input(s)           : pu1HashValue - Pointer to the Hash value
 *                      pc1HexStr - Pointer to the Hex String
 *                      u4HashSize - Hash size
 *                     
 * Output(s)          : None
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ***************************************************************************/
INT4
FipsShaCheckMatch (CONST UINT1 *pu1HashValue, CONST CHR1 * pc1HexStr,
                   INT4 u4HashSize)
{
    INT4                i4Count = 0;

    for (i4Count = 0; i4Count < u4HashSize; ++i4Count)
    {
        if (*pc1HexStr++ != c1HexDigits[(pu1HashValue[i4Count] >> 4) & 0xF])
        {
            return OSIX_FAILURE;
        }
        if (*pc1HexStr++ != c1HexDigits[pu1HashValue[i4Count] & 0xF])
        {
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/***************************************************************************
 * Function           : FipsShaPrintStr 
 *
 * Description        : Print the string, converting non-printable
 *                      characters to "."
 *
 * Input(s)           : pc1Str - Input string
 *                      i4Len - Length of Input string
 *                     
 * Output(s)          : None
 *
 * Returns            : None
 ***************************************************************************/
VOID
FipsShaPrintStr (CONST CHR1 * pc1Str, INT4 i4Len)
{
    for (; i4Len-- > 0; pc1Str++)
    {
        putchar (isprint ((UINT1) *pc1Str) ? *pc1Str : '.');
    }
}

/***************************************************************************
 * Function           : FipsShaPrintxStr
 *
 * Description        : Print the string, converting non-printable characters
 *                      to hex "## ".
 *
 * Input(s)           : pc1Str - Input string
 *                      i4Len - Length of Input string
 *                     
 * Output(s)          : None
 *
 * Returns            : None
 ***************************************************************************/
VOID
FipsShaPrintxStr (CONST CHR1 * pc1Str, INT4 i4Len)
{
    for (; i4Len-- > 0; pc1Str++)
    {
        PRINTF ("%c%c ", c1HexDigits[(*pc1Str >> 4) & 0xF],
                c1HexDigits[*pc1Str & 0xF]);
    }
}

/***************************************************************************
 * Function           : FipsShaPrintResult
 *
 * Description        : Print the results and PASS/FAIL.
 *
 * Input(s)           : pu1MsgDigest - Pointer to the Digest
 *                      i4HashSize - Hash size
 *                      pc1HashName - Pointer to the Hash name
 *                      pc1TestType - Test Type
 *                      pc1TestName - Test name
 *                      pc1ResultArray - Pointer to the expected result
 *                                       array
 *                     
 * Output(s)          : None
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ***************************************************************************/
INT4
FipsShaPrintResult (UINT1 *pu1MsgDigest, INT4 i4HashSize,
                    CONST CHR1 * pc1HashName, CONST CHR1 * pc1TestType,
                    CONST CHR1 * pc1TestName, CONST CHR1 * pc1ResultArray)
{
    INT4                i4Count1 = 0;
    INT4                i4Count2 = 0;
    INT4                i4RetVal = OSIX_SUCCESS;

    if (FIPS_PRINT_RESULT)
    {
        putchar ('\t');
        for (i4Count1 = 0; i4Count1 < i4HashSize; ++i4Count1)
        {
            putchar (c1HexDigits[(pu1MsgDigest[i4Count1] >> 4) & 0xF]);
            putchar (c1HexDigits[pu1MsgDigest[i4Count1] & 0xF]);
            putchar (' ');
        }
        putchar ('\n');
    }

    if (FIPS_PRINT_RESULT && pc1ResultArray)
    {
        PRINTF ("    Should match:\n\t");
        for (i4Count1 = 0, i4Count2 = 0; i4Count1 < i4HashSize;
             i4Count1++, i4Count2 += 2)
        {
            putchar (pc1ResultArray[i4Count2]);
            putchar (pc1ResultArray[i4Count2 + 1]);
            putchar (' ');
        }
        putchar ('\n');
    }

    if (pc1ResultArray)
    {
        i4RetVal = FipsShaCheckMatch (pu1MsgDigest, pc1ResultArray, i4HashSize);
    }
    if (FIPS_PRINT_RESULT)
    {
        PRINTF ("%s %s %s: %s\n", pc1HashName, pc1TestType, pc1TestName,
                (i4RetVal == OSIX_SUCCESS) ? "PASSED" : "FAILED");
    }
    return i4RetVal;
}

/***************************************************************************
 * Function           : FipsShaHash
 *
 * Description        : Exercise a hash series of functions. The input is
 *                      the testarray, repeated repeatcount times, followed
 *                      by the extrabits. If the result is known, it is in
 *                      resultarray in uppercase hex.
 *
 * Input(s)           : i4TestNo - Test Number
 *                      i4LoopNo - Loop count
 *                      pc1TestArray, i4Length - Input text and length,
 *                      RepeatCount - Repeat count
 *                      i4ExtraBits - Extra Bits
 *                      pu1KeyArray, i4KeyLen - Key, Key length,
 *                      pc1ResultArray, i4HashSize - Output and its Hash size
 *                     
 * Output(s)          : None
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ***************************************************************************/
INT4
FipsShaHash (INT4 i4TestNo, INT4 i4LoopNo, INT4 i4HashNo,
             CHR1 * pc1TestArray, INT4 i4Length, FS_LONG RepeatCount,
             INT4 i4NumExtraBits, INT4 i4ExtraBits, UINT1 *pu1KeyArray,
             INT4 i4KeyLen, CONST CHR1 * pc1ResultArray, INT4 i4HashSize)
{
    tUshaContext        Sha;
    tHmacContext        Hmac;
    unUtilAlgo          UtilAlgo;
    INT4                i4Error = OSIX_FAILURE;
    INT4                i4Count = 0;
    UINT1               u1AlgoType = 0;
    UINT1               au1MsgDigest[USHA_MAX_HASH_SIZE];
    CHR1                ac1Buf[SHA1_HASH_SIZE];

    if (FIPS_PRINT_RESULT)
    {
        PRINTF ("\nTest %d: Iteration %d, Repeat %ld\n\t'", i4TestNo + 1,
                i4LoopNo, RepeatCount);
        FipsShaPrintStr (pc1TestArray, i4Length);
        PRINTF ("'\n\t'");
        FipsShaPrintxStr (pc1TestArray, i4Length);
        PRINTF ("'\n");

        PRINTF ("    Length=%d bytes (%d bits), ", i4Length, i4Length * 8);
        PRINTF ("ExtraBits %d: %2.2x\n", i4NumExtraBits, i4ExtraBits);
    }

    MEMSET (&Sha, '\343', sizeof (Sha));    /* force bad data into struct */
    MEMSET (&Hmac, '\343', sizeof (Hmac));
    MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));

    /* Different implementation is supported for SHA1 Hashing Algorithm */
    /* This implementation test is not applicable */
    if (tHashes[i4HashNo].whichSha == AR_SHA1_ALGO)
    {
        return OSIX_SUCCESS;
    }

    if ((RepeatCount > 1) || (i4NumExtraBits > 0))
    {
        i4Error = pu1KeyArray ? arHmacReset (&Hmac, tHashes[i4HashNo].whichSha,
                                             pu1KeyArray, i4KeyLen) :
            UshaReset (&Sha, tHashes[i4HashNo].whichSha);
        if (i4Error != SHA_SUCCESS)
        {
            fprintf (stderr, "FipsShaHash(): %sReset Error %d.\n",
                     pu1KeyArray ? "hmac" : "sha", i4Error);
            return OSIX_FAILURE;
        }

        for (i4Count = 0; i4Count < RepeatCount; ++i4Count)
        {
            i4Error = pu1KeyArray ?
                arHmacInput (&Hmac, (CONST UINT1 *) pc1TestArray, i4Length) :
                UshaInput (&Sha, (CONST UINT1 *) pc1TestArray, i4Length);
            if (i4Error != SHA_SUCCESS)
            {
                fprintf (stderr, "FipsShaHash(): %sInput Error %d.\n",
                         pu1KeyArray ? "hmac" : "sha", i4Error);
                return OSIX_FAILURE;
            }
        }

        if (i4NumExtraBits > 0)
        {
            i4Error = pu1KeyArray ? arHmacFinalBits (&Hmac,
                                                     (UINT1) i4ExtraBits,
                                                     i4NumExtraBits) :
                UshaFinalBits (&Sha, (UINT1) i4ExtraBits, i4NumExtraBits);
            if (i4Error != SHA_SUCCESS)
            {
                fprintf (stderr, "FipsShaHash(): %sFinalBits Error %d.\n",
                         pu1KeyArray ? "hmac" : "sha", i4Error);
                return OSIX_FAILURE;
            }
        }

        i4Error = pu1KeyArray ? arHmacResult (&Hmac, au1MsgDigest) :
            UshaResult (&Sha, au1MsgDigest);
        if (i4Error != SHA_SUCCESS)
        {
            fprintf (stderr, "FipsShaHash(): %s Result Error %d, could not "
                     "compute message digest.\n",
                     pu1KeyArray ? "hmac" : "sha", i4Error);
            return OSIX_FAILURE;
        }
    }
    else
    {
        if (pu1KeyArray)
        {
            UtilAlgo.UtilHmacAlgo.HmacShaVersion = tHashes[i4HashNo].whichSha;
            UtilAlgo.UtilHmacAlgo.pu1HmacPktDataPtr = (UINT1 *) pc1TestArray;
            UtilAlgo.UtilHmacAlgo.i4HmacPktLength = (UINT4) i4Length;
            UtilAlgo.UtilHmacAlgo.pu1HmacKey = pu1KeyArray;
            UtilAlgo.UtilHmacAlgo.u4HmacKeyLen = (UINT4) i4KeyLen;
            UtilAlgo.UtilHmacAlgo.pu1HmacOutDigest = au1MsgDigest;
            i4Error = UtilHash (ISS_UTIL_ALGO_HMAC_SHA2, &UtilAlgo);
        }
        else
        {
            UtilAlgo.UtilSha2Algo.pu1Sha2InBuf = (UINT1 *) pc1TestArray;
            UtilAlgo.UtilSha2Algo.i4Sha2BufLen = i4Length;
            UtilAlgo.UtilSha2Algo.pu1Sha2MsgDigest = au1MsgDigest;
            if (tHashes[i4HashNo].whichSha == AR_SHA224_ALGO)
            {
                UtilAlgo.UtilSha2Algo.Sha2Version = AR_SHA224_ALGO;
                u1AlgoType = ISS_UTIL_ALGO_SHA224_ALGO;
            }
            if (tHashes[i4HashNo].whichSha == AR_SHA256_ALGO)
            {
                UtilAlgo.UtilSha2Algo.Sha2Version = AR_SHA256_ALGO;
                u1AlgoType = ISS_UTIL_ALGO_SHA256_ALGO;
            }
            if (tHashes[i4HashNo].whichSha == AR_SHA384_ALGO)
            {
                UtilAlgo.UtilSha2Algo.Sha2Version = AR_SHA384_ALGO;
                u1AlgoType = ISS_UTIL_ALGO_SHA384_ALGO;
            }
            if (tHashes[i4HashNo].whichSha == AR_SHA512_ALGO)
            {
                UtilAlgo.UtilSha2Algo.Sha2Version = AR_SHA512_ALGO;
                u1AlgoType = ISS_UTIL_ALGO_SHA512_ALGO;
            }
            i4Error = UtilMac (u1AlgoType, &UtilAlgo);
        }
        if (i4Error != OSIX_SUCCESS)
        {
            fprintf (stderr, "FipsShaHash(): %sReset Error %d.\n",
                     pu1KeyArray ? "hmac" : "sha", i4Error);
            return OSIX_FAILURE;
        }
    }

    SPRINTF (ac1Buf, "%d", i4TestNo + 1);

    return (FipsShaPrintResult (au1MsgDigest, i4HashSize,
                                tHashes[i4HashNo].pc1Name,
                                pu1KeyArray ?
                                "hmac standard test" : "sha standard test",
                                ac1Buf, pc1ResultArray));
}

/***************************************************************************
 * Function           : FipsShaRandomTest
 *
 * Description        : Exercise a hash series of functions through
 *                      multiple permutations.
 *                      The input is an initial seed. That seed is
 *                      replicated 3 times. For 1000 rounds, the previous
 *                      three results are used as the input. This result is
 *                      then checked, and used to seed the next cycle. If
 *                      the result is known, it is in resultarrays in
 *                      uppercase hex.
 *
 * Input(s)           : i4HashNo - Type of Hash Algorithm
 *                      pc1Seed - Poniter to the Seed
 *                      i4HashSize - Hash size,
 *                      ppc1ResultArrays - Expected result
 *                      i4RandomCount - Random count
 *                     
 * Output(s)          : None
 *
 * Returns            : None
 ***************************************************************************/
VOID
FipsShaRandomTest (INT4 i4HashNo, CONST CHR1 * pc1Seed, INT4 i4HashSize,
                   CONST CHR1 ** ppc1ResultArrays, INT4 i4RandomCount)
{
    INT4                i4Count1 = 0;
    INT4                i4Count2 = 0;
    CHR1                c1Buf[SHA1_HASH_SIZE];
    UINT1               au1Seed[USHA_MAX_HASH_SIZE];

    /* INPUT: Seed - A random seed n bits long */
    MEMCPY (au1Seed, pc1Seed, i4HashSize);
    if (FIPS_PRINT_RESULT)
    {
        PRINTF ("%s random test seed= '", tHashes[i4HashNo].pc1Name);
        FipsShaPrintxStr (pc1Seed, i4HashSize);
        PRINTF ("'\n");
    }

    for (i4Count2 = 0; i4Count2 < i4RandomCount; i4Count2++)
    {
        /* MD0 = MD1 = MD2 = Seed; */
        MEMCPY (gau1Md[0], au1Seed, i4HashSize);
        MEMCPY (gau1Md[1], au1Seed, i4HashSize);
        MEMCPY (gau1Md[2], au1Seed, i4HashSize);
        for (i4Count1 = 3; i4Count1 < FIPS_SHA_RAND_COUNT; i4Count1++)
        {
            /* Mi = MDi-3 || MDi-2 || MDi-1; */
            tUshaContext        Mi;
            /* force bad data into struct */
            MEMSET (&Mi, '\343', sizeof (Mi));
            UshaReset (&Mi, tHashes[i4HashNo].whichSha);
            UshaInput (&Mi, gau1Md[i4Count1 - 3], i4HashSize);
            UshaInput (&Mi, gau1Md[i4Count1 - 2], i4HashSize);
            UshaInput (&Mi, gau1Md[i4Count1 - 1], i4HashSize);
            /* MDi = SHA(Mi); */
            UshaResult (&Mi, gau1Md[i4Count1]);
        }
        /* MDj = Seed = MDi; */
        MEMCPY (au1Seed, gau1Md[i4Count1 - 1], i4HashSize);

        /* OUTPUT: MDj */
        SPRINTF (c1Buf, "%d", i4Count2);
        FipsShaPrintResult (au1Seed, i4HashSize, tHashes[i4HashNo].pc1Name,
                            "random test",
                            c1Buf, ppc1ResultArrays ?
                            ppc1ResultArrays[i4Count2] : 0);
    }
}

/****************************************************************************
 * Function           : FipsShaTestShaAndHmac
 *
 * Description        : This function runs HMAC-SHA tests abd SHA test based
 *                      on the input given.
 *
 * Input(s)           : i4RunHmacTests - If it is set, runs the HMAC-SHA tests
 *                      else runs SHA tests
 *                     
 * Output(s)          : None
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ***************************************************************************/
INT4
FipsShaTestShaAndHmac (INT4 i4RunHmacTests)
{
    INT4                i4Error = OSIX_FAILURE;
    INT4                i4Loop = 0, i4LoopHigh = 1;
    INT4                i4Hash, i4HashLow = 0, i4HashHigh = HASHCOUNT - 1;
    INT4                i4Test, i4TestLow = 0, i4TestHigh;
    INT4                i4NTestHigh = 0;

    i4TestHigh = (i4NTestHigh != 0) ? i4NTestHigh :
        i4RunHmacTests ? (HMACTESTCOUNT - 1) : (TESTCOUNT - 1);

    /*
     *  Perform SHA/HMAC tests
     */
    for (i4Hash = i4HashLow; i4Hash <= i4HashHigh; ++i4Hash)
    {
        if (tHashes[i4Hash].whichSha == AR_SHA1_ALGO)
        {
            continue;
        }

        if (FIPS_PRINT_RESULT)
            PRINTF ("Hash %s\n", tHashes[i4Hash].pc1Name);
        i4Error = OSIX_SUCCESS;

        for (i4Loop = 1; (i4Loop <= i4LoopHigh) && (i4Error == OSIX_SUCCESS);
             ++i4Loop)
        {
            /* standard tests */
            for (i4Test = i4TestLow;
                 (i4Test <= i4TestHigh) && (i4Error == OSIX_SUCCESS); ++i4Test)
            {
                if (i4RunHmacTests)
                {
                    i4Error = FipsShaHash (i4Test, i4Loop, i4Hash,
                                           tHmacHashes[i4Test].
                                           pc1DataArray[i4Hash] ?
                                           tHmacHashes[i4Test].
                                           pc1DataArray[i4Hash] :
                                           tHmacHashes[i4Test].
                                           pc1DataArray[1] ?
                                           tHmacHashes[i4Test].
                                           pc1DataArray[1] :
                                           tHmacHashes[i4Test].pc1DataArray[0],
                                           tHmacHashes[i4Test].
                                           ai4DataLength[i4Hash] ?
                                           tHmacHashes[i4Test].
                                           ai4DataLength[i4Hash] :
                                           tHmacHashes[i4Test].
                                           ai4DataLength[1] ?
                                           tHmacHashes[i4Test].
                                           ai4DataLength[1] :
                                           tHmacHashes[i4Test].ai4DataLength[0],
                                           1, 0, 0,
                                           (UINT1 *) (tHmacHashes[i4Test].
                                                      pc1KeyArray[i4Hash]
                                                      ?
                                                      tHmacHashes[i4Test].
                                                      pc1KeyArray[i4Hash]
                                                      :
                                                      tHmacHashes[i4Test].
                                                      pc1KeyArray[1] ?
                                                      tHmacHashes[i4Test].
                                                      pc1KeyArray[1] :
                                                      tHmacHashes[i4Test].
                                                      pc1KeyArray[0]),
                                           tHmacHashes[i4Test].
                                           ai4KeyLength[i4Hash] ?
                                           tHmacHashes[i4Test].
                                           ai4KeyLength[i4Hash] :
                                           tHmacHashes[i4Test].
                                           ai4KeyLength[1] ?
                                           tHmacHashes[i4Test].
                                           ai4KeyLength[1] :
                                           tHmacHashes[i4Test].ai4KeyLength[0],
                                           tHmacHashes[i4Test].
                                           pc1ResultArray[i4Hash],
                                           tHmacHashes[i4Test].
                                           ai4ResultLength[i4Hash]);
                }
                else
                {
                    i4Error =
                        FipsShaHash (i4Test, i4Loop, i4Hash,
                                     tHashes[i4Hash].tests[i4Test].pc1TestArray,
                                     tHashes[i4Hash].tests[i4Test].i4Length,
                                     tHashes[i4Hash].tests[i4Test].RepeatCount,
                                     tHashes[i4Hash].tests[i4Test].
                                     i4NoExtrabits,
                                     tHashes[i4Hash].tests[i4Test].i4ExtraBits,
                                     0, 0,
                                     tHashes[i4Hash].tests[i4Test].
                                     pc1ResultArray,
                                     tHashes[i4Hash].i4HashSize);
                }

                if (i4RunHmacTests)
                {
                    /* These test cases are not applicable for 
                     * HMAC-SHA224 */
                    i4Test =
                        (i4Hash == 1) ? (i4Test >= 4) ? 19 : i4Test : i4Test;

                    /* These test cases are not applicable for 
                     * HMAC-SHA256 */
                    i4Test =
                        (i4Hash == 2) ? (i4Test == 4) ? 6 : i4Test : i4Test;
                    i4Test =
                        (i4Hash == 2) ? (i4Test >= 10) ? 19 : i4Test : i4Test;

                    /* These test cases are not applicable for 
                     * HMAC-SHA384*/
                    i4Test =
                        (i4Hash == 3) ? (i4Test == 4) ? 10 : i4Test : i4Test;
                    i4Test =
                        (i4Hash == 3) ? (i4Test >= 14) ? 19 : i4Test : i4Test;

                    /* These test cases are not applicable for 
                     * HMAC-SHA512*/
                    i4Test =
                        (i4Hash == 4) ? (i4Test == 4) ? 14 : i4Test : i4Test;
                }
                else
                {
                    /* These test cases are not applicable for 
                     * SHA224 and SHA256*/
                    i4Test = ((i4Hash == 1)
                              || (i4Hash == 2)) ? (i4Test ==
                                                   1) ? 4 : i4Test : i4Test;
                    i4Test = ((i4Hash == 1)
                              || (i4Hash == 2)) ? (i4Test ==
                                                   5) ? 6 : i4Test : i4Test;
                    i4Test = ((i4Hash == 1)
                              || (i4Hash == 2)) ? (i4Test ==
                                                   7) ? 8 : i4Test : i4Test;
                    i4Test = ((i4Hash == 1)
                              || (i4Hash == 2)) ? (i4Test >=
                                                   9) ? 19 : i4Test : i4Test;

                    /* These test cases are not applicable for 
                     * SHA384 and SHA512*/
                    i4Test = ((i4Hash == 3)
                              || (i4Hash == 4)) ? (i4Test ==
                                                   1) ? 4 : i4Test : i4Test;
                    i4Test = ((i4Hash == 3)
                              || (i4Hash == 4)) ? (i4Test >=
                                                   5) ? 19 : i4Test : i4Test;
                }

                if (i4Error != OSIX_SUCCESS)
                {
                    return OSIX_FAILURE;
                }
            }
            if (!i4RunHmacTests)
            {
                FipsShaRandomTest (i4Hash, tHashes[i4Hash].pc1RandomTest,
                                   tHashes[i4Hash].i4HashSize,
                                   tHashes[i4Hash].pc1RandomResults,
                                   RANDOMCOUNT);
            }
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : FipsShaTestSha1Algo                                        *
 *                                                                           *
 * Description  : Generate the tha hash value by calling SHA1 calls for      *
 *                the given input and checks the output value aginst the     *
 *                known output value                                         *
 *                                                                           *
 * Input        : None                                                       *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : OSIX_SUCCESS/OSIX_FAILURE                                  *
 *                                                                           *
 *****************************************************************************/
INT4
FipsShaTestSha1Algo ()
{
    unArCryptoHash      Sha;
    unUtilAlgo          UtilAlgo;
    INT4                i4TestNo = 0;
    INT4                i4LoopNo = 0;
    INT4                i4HashNo = 0;
    INT4                i4Length = 0;
    INT4                i4NumExtraBits = 0;
    INT4                i4ExtraBits = 0;
    INT4                i4HashSize = 0;
    INT4                i4RetVal = OSIX_FAILURE;
    INT4                i4TestLow = 0, i4TestHigh = (TESTCOUNT - 1);
    FS_LONG             RepeatCount = 0;
    CHR1               *pc1TestArray;
    CONST CHR1         *pc1ResultArray;
    UINT1               au1MsgDigest[USHA_MAX_HASH_SIZE];
    CHR1                ac1Buf[SHA1_HASH_SIZE];
    INT1                ai1Sha1Output[USHA_MAX_HASH_SIZE];

    for (i4TestNo = i4TestLow; (i4TestNo <= i4TestHigh); ++i4TestNo)
    {
        pc1TestArray = tHashes[i4HashNo].tests[i4TestNo].pc1TestArray;
        i4Length = tHashes[i4HashNo].tests[i4TestNo].i4Length;
        RepeatCount = tHashes[i4HashNo].tests[i4TestNo].RepeatCount;
        i4NumExtraBits = tHashes[i4HashNo].tests[i4TestNo].i4NoExtrabits;
        i4ExtraBits = tHashes[i4HashNo].tests[i4TestNo].i4ExtraBits;
        pc1ResultArray = tHashes[i4HashNo].tests[i4TestNo].pc1ResultArray;
        i4HashSize = tHashes[i4HashNo].i4HashSize;

        if (RepeatCount > 1)
        {
            continue;
        }
        MEMSET (ai1Sha1Output, 0, USHA_MAX_HASH_SIZE);
        MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));

        if (FIPS_PRINT_RESULT)
        {
            PRINTF ("\nTest %d: Iteration %d, Repeat %ld\n\t'",
                    i4TestNo + 1, i4LoopNo, RepeatCount);
            FipsShaPrintStr (pc1TestArray, i4Length);
            PRINTF ("'\n\t'");
            FipsShaPrintxStr (pc1TestArray, i4Length);
            PRINTF ("'\n");

            PRINTF ("    Length=%d bytes (%d bits), ", i4Length, i4Length * 8);
            PRINTF ("ExtraBits %d: %2.2x\n", i4NumExtraBits, i4ExtraBits);
        }
        /* force bad data into struct */
        MEMSET (&Sha, '\343', sizeof (Sha));

        if (i4NumExtraBits > 0)
        {
            return OSIX_SUCCESS;
        }

        UtilAlgo.UtilSha1Algo.pu1Sha1Buf = (UINT1 *) pc1TestArray;
        UtilAlgo.UtilSha1Algo.u4Sha1BufLen = i4Length;
        UtilAlgo.UtilSha1Algo.pu1Sha1Digest = au1MsgDigest;
        UtilMac (ISS_UTIL_ALGO_SHA1_ALGO, &UtilAlgo);

        i4RetVal = FipsShaCheckMatch (au1MsgDigest, pc1ResultArray, i4HashSize);
        if (i4RetVal != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }

        SPRINTF (ac1Buf, "%d", i4TestNo + 1);

        i4RetVal = FipsShaPrintResult (au1MsgDigest, i4HashSize,
                                       tHashes[i4HashNo].pc1Name,
                                       "sha standard test", ac1Buf,
                                       pc1ResultArray);
        if (i4RetVal != OSIX_SUCCESS)
        {
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*-----------------------------------------------------------------------*/
/*                       End of the file fipssha.c                       */
/*-----------------------------------------------------------------------*/
