/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fipscli.c,v 1.11 2017/09/12 13:26:43 siva Exp $
*
* Description: This file contains the FIPS CLI related routines 
*********************************************************************/
#ifndef __FIPSCLI_C__
#define __FIPSCLI_C__

#include "fipscli.h"
#include "fipsinc.h"
#include "fsfipscli.h"

/*****************************************************************************
 *                                                                           *
 * Function     : cli_process_fips_cmd                                       *
 *                                                                           *
 * Description  : This module provide the CLI Mangement Interface for        *
 *                the Aricent FIPS Module                                    *
 *                                                                           *
 * Input        : Variable arguments                                         *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : CLI_SUCCESS/CLI_FAILURE                                    *
 *                                                                           *
 *****************************************************************************/
INT4
cli_process_fips_cmd (tCliHandle CliHandle, UINT4 u4Command, ...)
{
    va_list             vaList;
    UINT4              *apu4VaArgs[CLI_FIPS_MAX_ARGS];
    INT1                i1ArgNo = 0;
    INT4                i4Inst = 0;
    UINT4               u4SetAlgo = 0;
    INT4                i4Debug = 0;
    INT4                i4ExsitTrace = 0;
    INT4                i4RetVal = 0;

    MEMSET (&vaList, 0, sizeof (vaList));
    MEMSET (apu4VaArgs, 0, sizeof (apu4VaArgs));
    va_start (vaList, u4Command);

    /* Third arguement is always interface name/index */
    i4Inst = va_arg (vaList, INT4);

    /* Walk through the rest of the arguements and store in args array.  */

    for (i1ArgNo = 0; i1ArgNo < CLI_FIPS_MAX_ARGS; i1ArgNo += 1)
    {
        apu4VaArgs[i1ArgNo] = va_arg (vaList, UINT4 *);
    }
    va_end (vaList);

    switch (u4Command)
    {
        case CLI_FIPS_START_KNOWN_ANS_TEST:
            u4SetAlgo = CLI_PTR_TO_U4 (apu4VaArgs[0]);
            if (u4SetAlgo == 0)
            {
                u4SetAlgo = CLI_FIPS_KNOWN_ANS_TEST_ALL;
            }
            FipsCliRunKnownAnsTests (CliHandle, u4SetAlgo);
            break;

        case CLI_FIPS_ZEROIZE_CRYPTO_KEYS:
            i4RetVal = FipsCliZeroizeCryptoKeys (CliHandle);
            break;

        case CLI_FIPS_DEBUG:
            i4Debug = CLI_PTR_TO_I4 (apu4VaArgs[0]);
            nmhGetFsFipsTraceLevel (&i4ExsitTrace);
            i4ExsitTrace |= i4Debug;
            FipsCliSetDebugLevel (CliHandle, i4ExsitTrace);
            break;

        case CLI_FIPS_NO_DEBUG:
            i4Debug = CLI_PTR_TO_I4 (apu4VaArgs[0]);
            if (i4Debug == FIPS_MAX_VALID_TRACE)
            {
                /* Disable all level of trace */
                i4Debug = 0;
            }
            else
            {
                nmhGetFsFipsTraceLevel (&i4ExsitTrace);
                i4ExsitTrace &= ~i4Debug;
                i4Debug = i4ExsitTrace;
            }
            FipsCliSetDebugLevel (CliHandle, i4Debug);
            break;

        case CLI_FIPS_SET_OPER_MODE:
            i4RetVal = FipsCliSetOperMode (CliHandle,
                                           CLI_PTR_TO_I4 (apu4VaArgs[0]));
            break;

        case CLI_FIPS_SHOW_OPER_MODE:
            i4RetVal = FipsCliShowOperMode (CliHandle);
            break;

        case CLI_FIPS_SET_BYPASS_OPER_MODE:
            i4RetVal = FipsCliSetBypassOperMode (CliHandle,
                                                 CLI_PTR_TO_I4 (apu4VaArgs[0]));
            break;

        case CLI_FIPS_SHOW_BYPASS_OPER_MODE:
            i4RetVal = FipsCliShowBypassOperMode (CliHandle);
            break;

        default:
            break;
    }
    if (i4RetVal == CLI_FAILURE)
    {
        CliPrintf (CliHandle, "CLI command execution failed\n");
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : FipsCliRunKnownAnsTests                                    *
 *                                                                           *
 * Description  : This function runs FIPS Known answer tests for the         *
 *                corresponding algorithm that set                           *
 *                                                                           *
 * Input        : CliHandle - CLI Handle                                     *
 *                u4SetAlgo - Algorithms to test set in bitwise              *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : CLI_SUCCESS/CLI_FAILURE                                    *
 *                                                                           *
 *****************************************************************************/
INT4
FipsCliRunKnownAnsTests (tCliHandle CliHandle, UINT4 u4SetAlgo)
{
    INT4                i4RetVal = OSIX_FAILURE;
    INT4                i4ErrCode = 0;
    CHR1               *pc1IbitResult = "FAIL";

    /* During fips self-test cli command, the conditional self test
     * can be skipped. Setting the PowerOnStatus flag to 0 will skip
     * the conditional self-test. */
    FipsSetKnownAnsTestFlags (FIPS_KAT_UNSET_POWER_ON_STATUS);
    i4RetVal = FipsUtilRunKnownAnsTest ((INT4) u4SetAlgo, &i4ErrCode);
    if (i4RetVal == OSIX_SUCCESS)
    {
        pc1IbitResult = "PASS";
    }

    IssCustLedSet (pc1IbitResult);

    CliPrintf (CliHandle, "\rFIPS Known Answer Test Results\r\n");
    CliPrintf (CliHandle, "\r------------------------------\r\n");
    if (u4SetAlgo & FIPS_KNOWN_ANS_TEST_SHA1)
    {
        CliPrintf (CliHandle, "\rSHA1     : %s\r\n",
                   (i4ErrCode == FIPS_KAT_SHA1_FAILED) ? "[FAIL]" : "[PASS]");
        if (i4ErrCode == FIPS_KAT_SHA1_FAILED)
        {
            return CLI_FAILURE;
        }
    }
    if (u4SetAlgo & FIPS_KNOWN_ANS_TEST_SHA2)
    {
        CliPrintf (CliHandle, "\rSHA2     : %s\r\n",
                   (i4ErrCode == FIPS_KAT_SHA2_FAILED) ? "[FAIL]" : "[PASS]");
        if (i4ErrCode == FIPS_KAT_SHA2_FAILED)
        {
            return CLI_FAILURE;
        }
    }
    if (u4SetAlgo & FIPS_KNOWN_ANS_TEST_HMAC)
    {
        CliPrintf (CliHandle, "\rHMAC     : %s\r\n",
                   (i4ErrCode == FIPS_KAT_HMAC_FAILED) ? "[FAIL]" : "[PASS]");
        if (i4ErrCode == FIPS_KAT_HMAC_FAILED)
        {
            return CLI_FAILURE;
        }
    }
    if (u4SetAlgo & FIPS_KNOWN_ANS_TEST_AES)
    {
        CliPrintf (CliHandle, "\rAES      : %s\r\n",
                   (i4ErrCode == FIPS_KAT_AES_FAILED) ? "[FAIL]" : "[PASS]");
        if (i4ErrCode == FIPS_KAT_AES_FAILED)
        {
            return CLI_FAILURE;
        }
    }
    if (u4SetAlgo & FIPS_KNOWN_ANS_TEST_DES)
    {
        CliPrintf (CliHandle, "\rDES      : %s\r\n",
                   (i4ErrCode == FIPS_KAT_DES_FAILED) ? "[FAIL]" : "[PASS]");
        if (i4ErrCode == FIPS_KAT_DES_FAILED)
        {
            return CLI_FAILURE;
        }
    }
    if (u4SetAlgo & FIPS_KNOWN_ANS_TEST_RAND)
    {
        CliPrintf (CliHandle, "\rRNG      : %s\r\n",
                   (i4ErrCode == FIPS_KAT_RNG_FAILED) ? "[FAIL]" : "[PASS]");
        if (i4ErrCode == FIPS_KAT_RNG_FAILED)
        {
            return CLI_FAILURE;
        }
    }
    if (u4SetAlgo & FIPS_KNOWN_ANS_TEST_RSA)
    {
        CliPrintf (CliHandle, "\rRSA      : %s\r\n",
                   (i4ErrCode == FIPS_KAT_RSA_FAILED) ? "[FAIL]" : "[PASS]");
        if (i4ErrCode == FIPS_KAT_RSA_FAILED)
        {
            return CLI_FAILURE;
        }
    }
    if (u4SetAlgo & FIPS_KNOWN_ANS_TEST_DSA)
    {
        CliPrintf (CliHandle, "\rDSA      : %s\r\n",
                   (i4ErrCode == FIPS_KAT_DSA_FAILED) ? "[FAIL]" : "[PASS]");
        if (i4ErrCode == FIPS_KAT_DSA_FAILED)
        {
            return CLI_FAILURE;
        }
    }
    return CLI_SUCCESS;
}

/**************************************************************************/
/*  Function Name   : FipsCliZeroizeCryptoKeys                            */
/*                                                                        */
/*  Description     : This function deletes all cryptographic keys        */
/*                    in ike, ipsec, ssh, ssl and radius modules.         */
/*                                                                        */
/*  Input(s)        : CliHandle - CliContext ID                           */
/*                                                                        */
/*  Output(s)       : NONE                                                */
/*                                                                        */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                             */
/**************************************************************************/
INT4
FipsCliZeroizeCryptoKeys (tCliHandle CliHandle)
{
    UINT4               u4ErrorCode = 0;
    if (nmhTestv2FsfipsZeroizeCryptoKeys (&u4ErrorCode, FIPS_TRUE)
        == SNMP_FAILURE)
    {
        return CLI_FAILURE;
    }
    if (nmhSetFsfipsZeroizeCryptoKeys (FIPS_TRUE) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }
    return CLI_SUCCESS;
}

/*****************************************************************************
 *                                                                           *
 * Function     : FipsCliSetDebugLevel                                       *
 *                                                                           *
 * Description  : This function sets and resets the Trace input              *
 *                                                                           *
 * Input        : CliHandle - CLI Handle                                     *
 *                i4Trace - Trace to enable set in bitwise                   *
 *                                                                           *
 * Output       : None                                                       *
 *                                                                           *
 * Returns      : CLI_SUCCESS/CLI_FAILURE                                    *
 *                                                                           *
 *****************************************************************************/
INT4
FipsCliSetDebugLevel (tCliHandle CliHandle, INT4 i4Trace)
{
    UINT4               u4ErrorCode = 0;

    if (nmhTestv2FsFipsTraceLevel (&u4ErrorCode, i4Trace) == SNMP_FAILURE)
    {
        CliPrintf (CliHandle, "\r%% %s", "Wrong value\n");
        return CLI_FAILURE;
    }

    if (nmhSetFsFipsTraceLevel (i4Trace) == SNMP_FAILURE)
    {
        CLI_FATAL_ERROR (CliHandle);
        return CLI_FAILURE;
    }

    return CLI_SUCCESS;
}

/***************************************************************/
/*  Function Name   : FipsCliSetOperMode                       */
/*  Description     : This function is used to set the FIPS    */
/*                    Operating mode                           */
/*  Input(s)        : CliHandle - Control Handle to CLI prompt */
/*                    i4FipsOperMode - Operating Mode          */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
FipsCliSetOperMode (tCliHandle CliHandle, INT4 i4FipsOperMode)
{
    UINT4               u4ErrorCode = 0;

    if (SNMP_SUCCESS == nmhTestv2FsFipsOperMode (&u4ErrorCode, i4FipsOperMode))
    {
        if (SNMP_SUCCESS == nmhSetFsFipsOperMode (i4FipsOperMode))
        {
            return CLI_SUCCESS;
        }
    }
    CliPrintf (CliHandle, "Failed to set the FIPS OperationalMode\n");
    return CLI_FAILURE;

}

/***************************************************************/
/*  Function Name   : FipsCliShowOperMode                       */
/*  Description     : This function is used to show the        */
/*                    Operating mode, either FIPS or not       */
/*  Input(s)        : CliHandle - Control Handle to CLI prompt */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS                              */
/***************************************************************/
INT4
FipsCliShowOperMode (tCliHandle CliHandle)
{
    INT4                i4FipsOperMode = 0;

    nmhGetFsFipsOperMode (&i4FipsOperMode);
    if (FIPS_MODE == i4FipsOperMode)
    {
        CliPrintf (CliHandle, "FIPS mode\n");
    }
    else if (CNSA_MODE == i4FipsOperMode)
    {
        CliPrintf (CliHandle, "CNSA mode\n");
    }
    else
    {
        CliPrintf (CliHandle, "LEGACY mode\n");
    }
    return CLI_SUCCESS;

}

/***************************************************************/
/*  Function Name   : FipsCliSetBypassOperMode                 */
/*  Description     : This function is used to set the bypass  */
/*                    Operating mode                           */
/*  Input(s)        : CliHandle - Control Handle to CLI prompt */
/*                    i4FipsOperMode - Operating Mode          */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS/CLI_FAILURE                  */
/***************************************************************/
INT4
FipsCliSetBypassOperMode (tCliHandle CliHandle, INT4 i4FipsOperMode)
{
    UINT4               u4ErrorCode = 0;

    if (SNMP_SUCCESS ==
        nmhTestv2FsFipsBypassCapability (&u4ErrorCode, i4FipsOperMode))
    {
        if (SNMP_SUCCESS == nmhSetFsFipsBypassCapability (i4FipsOperMode))
        {
            return CLI_SUCCESS;
        }
    }
    CliPrintf (CliHandle, "Failed to set the Bypass Capability\n");
    return CLI_FAILURE;

}

/***************************************************************/
/*  Function Name   : FipsCliShowBypassOperMode                */
/*  Description     : This function is used to show the        */
/*                    Operating mode, either bypass enabled    */
/*                    or not                                   */
/*  Input(s)        : CliHandle - Control Handle to CLI prompt */
/*  Output(s)       : None                                     */
/*  Returns         : CLI_SUCCESS                              */
/***************************************************************/
INT4
FipsCliShowBypassOperMode (tCliHandle CliHandle)
{
    INT4                i4FipsOperMode = 0;

    nmhGetFsFipsBypassCapability (&i4FipsOperMode);
    if (BYPASS_ENABLED == i4FipsOperMode)
    {
        CliPrintf (CliHandle, "Bypass capability enabled\n");
    }
    else
    {
        CliPrintf (CliHandle, "Bypass capability disabled\n");
    }
    return CLI_SUCCESS;

}

#endif
