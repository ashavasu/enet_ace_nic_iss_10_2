/********************************************************************
 *  Copyright (C) 2008 Aricent Inc . All Rights Reserved            *
 *                                                                  *
 *  $Id: fipsdes.c,v 1.4 2011/06/10 08:18:12 siva Exp $         *
 *                                                                     *
 *  Description: This file contains the test vectors for verifying  *
 *               DES algorithm.                                     *
 ********************************************************************/
#include "fipsinc.h"

#define DES_NUM_VECTORS       5
#define DES_VECTOR_LEN        8
#define DES_KEY_LEN           16
#define TDES_TEST_VECTORS     2
#define TDES_BUFFER_LEN       32

typedef ULONG       tDesKey[DES_KEY_LEN];

typedef struct _t1DesTestVector
{
    UINT1               au1Buf[DES_VECTOR_LEN];
    UINT1               au1DesKey[DES_VECTOR_LEN];
    UINT1               au1Result[DES_VECTOR_LEN];
} t1DesTestVector;

typedef struct _t3DesTestVector
{
    UINT1               au1TDesBuf[TDES_BUFFER_LEN];
    UINT1               au1TDesKey1[DES_VECTOR_LEN];
    UINT1               au1TDesKey2[DES_VECTOR_LEN];
    UINT1               au1TDesKey3[DES_VECTOR_LEN];
    UINT1               au1TDesIv[DES_VECTOR_LEN];
    UINT1               au13DesResult[TDES_BUFFER_LEN];
} t3DesTestVector;

struct _t1DesTestVector testData[] = {
    {{0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11},
     {0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11},
     {0xf4, 0x03, 0x79, 0xab, 0x9e, 0x0e, 0xc5, 0x33}
     },

    {{0x01, 0xA1, 0xD6, 0xD0, 0x39, 0x77, 0x67, 0x42},
     {0x7C, 0xA1, 0x10, 0x45, 0x4A, 0x1A, 0x6E, 0x57},
     {0x69, 0x0F, 0x5B, 0x0D, 0x9A, 0x26, 0x93, 0x9B}
     },

    {{0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff},
     {0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff},
     {0x73, 0x59, 0xB2, 0x16, 0x3E, 0x4E, 0xDC, 0x58}
     },

    {{0x16, 0x4D, 0x5E, 0x40, 0x4F, 0x27, 0x52, 0x32},
     {0x37, 0xD0, 0x6B, 0xB5, 0x16, 0xCB, 0x75, 0x46},
     {0x0A, 0x2A, 0xEE, 0xAE, 0x3F, 0xF4, 0xAB, 0x77}
     },

    {{0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF},
     {0xE0, 0xFE, 0xE0, 0xFE, 0xF1, 0xFE, 0xF1, 0xFE},
     {0xED, 0xBF, 0xD1, 0xC6, 0x6C, 0x29, 0xCC, 0xC7}
     }
};

struct _t3DesTestVector TDestestData[] = {
    {{0x6B, 0xC1, 0xBE, 0xE2, 0x2E, 0x40, 0x9F, 0x96,
      0xE9, 0x3D, 0x7E, 0x11, 0x73, 0x93, 0x17, 0x2A,
      0xAE, 0x2D, 0x8A, 0x57, 0x1E, 0x03, 0xAC, 0x9C,
      0x9E, 0xB7, 0x6F, 0xAC, 0x45, 0xAF, 0x8E, 0x51},
     {0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF},
     {0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF, 0x01},
     {0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF, 0x01, 0x23},
     {0xF6, 0x9F, 0x24, 0x45, 0xDF, 0x4F, 0x9B, 0x17},
     {0x20, 0x79, 0xC3, 0xD5, 0x3A, 0xA7, 0x63, 0xE1,
      0x93, 0xB7, 0x9E, 0x25, 0x69, 0xAB, 0x52, 0x62,
      0x51, 0x65, 0x70, 0x48, 0x1F, 0x25, 0xB5, 0x0F,
      0x73, 0xC0, 0xBD, 0xA8, 0x5C, 0x8E, 0x0D, 0xA7}
     },

    {{0x6B, 0xC1, 0xBE, 0xE2, 0x2E, 0x40, 0x9F, 0x96,
      0xE9, 0x3D, 0x7E, 0x11, 0x73, 0x93, 0x17, 0x2A,
      0xAE, 0x2D, 0x8A, 0x57, 0x1E, 0x03, 0xAC, 0x9C,
      0x9E, 0xB7, 0x6F, 0xAC, 0x45, 0xAF, 0x8E, 0x51},
     {0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF},
     {0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF, 0x01},
     {0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF},
     {0xF6, 0x9F, 0x24, 0x45, 0xDF, 0x4F, 0x9B, 0x17},
     {0x74, 0x01, 0xCE, 0x1E, 0xAB, 0x6D, 0x00, 0x3C,
      0xAF, 0xF8, 0x4B, 0xF4, 0x7B, 0x36, 0xCC, 0x21,
      0x54, 0xF0, 0x23, 0x8F, 0x9F, 0xFE, 0xCD, 0x8F,
      0x6A, 0xCF, 0x11, 0x83, 0x92, 0xB4, 0x55, 0x81}
     }
};

/***************************************************************************
 * Function           : FipsDesArTest 
 *
 * Description        : Function that contains testcases for 1-DES and 
 *                      3-DES mode of various plain text inputs
 *
 * Input(s)           : NONE
 *                     
 * Output(s)          : Results of test vectors
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ***************************************************************************/
PUBLIC INT4
FipsDesArTest ()
{
    unArCryptoKey       ArCryptoKey;
    unUtilAlgo          UtilAlgo;
    ULONG               au8DesKey1[DES_KEY_LEN];
    ULONG               au8DesKey2[DES_KEY_LEN];
    ULONG               au8DesKey3[DES_KEY_LEN];
    UINT1               au1TempData[TDES_BUFFER_LEN];
    INT4                i4Count = 0;
    INT4                i4TestIdx = 0;
    INT4                i4BufLen = DES_VECTOR_LEN;

    UINT1               au1InitVect[] = {
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
    };

    /* DES */
    for (i4TestIdx = 0; i4TestIdx < DES_NUM_VECTORS; i4TestIdx++)
    {
        MEMSET (&ArCryptoKey, 0, sizeof (unArCryptoKey));
        MEMSET (&UtilAlgo, 0, sizeof (unUtilAlgo));
        MEMSET (au1InitVect, 0, i4BufLen);
        MEMCPY (au1TempData, testData[i4TestIdx].au1Buf, DES_VECTOR_LEN);

        PRINT_RESULT ("\n1-DES\n");
        PRINT_RESULT ("Plain Text : ");
        for (i4Count = 0; i4Count < i4BufLen; i4Count++)
        {
            PRINT_RESULT1 ("%02x ", au1TempData[i4Count]);
        }
        PRINT_RESULT ("\n");

        DesArKeyScheduler (testData[i4TestIdx].au1DesKey, &ArCryptoKey);

        UtilAlgo.UtilDesAlgo.pu1DesInBuffer = au1TempData;
        UtilAlgo.UtilDesAlgo.u4DesInBufSize = i4BufLen;
        UtilAlgo.UtilDesAlgo.punDesArCryptoKey = &ArCryptoKey;
        UtilAlgo.UtilDesAlgo.pu1DesInUserKey = testData[i4TestIdx].au1DesKey;
        UtilAlgo.UtilDesAlgo.pu1DesInitVect = au1InitVect;
        UtilAlgo.UtilDesAlgo.u4DesInitVectLen = DES_VECTOR_LEN;
        UtilAlgo.UtilDesAlgo.u4DesInKeyLen = DES_VECTOR_LEN;
        UtilAlgo.UtilDesAlgo.i4DesEnc = ISS_UTIL_ENCRYPT;
        UtilEncrypt (ISS_UTIL_ALGO_DES_CBC, &UtilAlgo);

        PRINT_RESULT ("After Encrypt : ");
        for (i4Count = 0; i4Count < i4BufLen; i4Count++)
        {
            PRINT_RESULT1 ("%02x ", au1TempData[i4Count]);
        }
        PRINT_RESULT ("\n");

        PRINT_RESULT ("Expected Text : ");
        for (i4Count = 0; i4Count < i4BufLen; i4Count++)
        {
            PRINT_RESULT1 ("%02x ", testData[i4TestIdx].au1Result[i4Count]);
        }
        PRINT_RESULT ("\n");

        if ((MEMCMP (au1TempData, testData[i4TestIdx].au1Result,
                     i4BufLen) != 0))
        {
            PRINTF ("Test Case %d Encryption : Failed\n", i4TestIdx + 1);
            return OSIX_FAILURE;
        }
        else
        {
            PRINT_RESULT1 ("Test Case %d Encryption : Passed\n", i4TestIdx + 1);
        }
        MEMSET (au1InitVect, 0, i4BufLen);

        UtilAlgo.UtilDesAlgo.pu1DesInBuffer = au1TempData;
        UtilAlgo.UtilDesAlgo.u4DesInBufSize = i4BufLen;
        UtilAlgo.UtilDesAlgo.punDesArCryptoKey = &ArCryptoKey;
        UtilAlgo.UtilDesAlgo.pu1DesInUserKey = testData[i4TestIdx].au1DesKey;
        UtilAlgo.UtilDesAlgo.pu1DesInitVect = au1InitVect;
        UtilAlgo.UtilDesAlgo.u4DesInitVectLen = DES_VECTOR_LEN;
        UtilAlgo.UtilDesAlgo.u4DesInKeyLen = DES_VECTOR_LEN;
        UtilAlgo.UtilDesAlgo.i4DesEnc = ISS_UTIL_DECRYPT;
        UtilDecrypt (ISS_UTIL_ALGO_DES_CBC, &UtilAlgo);

        PRINT_RESULT ("After Decrypt : ");
        for (i4Count = 0; i4Count < i4BufLen; i4Count++)
        {
            PRINT_RESULT1 ("%02x ", au1TempData[i4Count]);
        }
        PRINT_RESULT ("\n");

        if ((MEMCMP (au1TempData, testData[i4TestIdx].au1Buf, i4BufLen) != 0))
        {
            PRINTF ("Test Case %d Decryption : Failed\n", i4TestIdx + 1);
            return OSIX_FAILURE;
        }
        else
        {
            PRINT_RESULT1 ("Test Case %d Decryption : Passed\n", i4TestIdx + 1);
        }
    }

    /* 3-DES */
    for (i4TestIdx = 0; i4TestIdx < TDES_TEST_VECTORS; i4TestIdx++)
    {
        MEMSET (&ArCryptoKey, 0, sizeof (unArCryptoKey));
        MEMSET (au1InitVect, 0, i4BufLen);
        MEMCPY (au1TempData, TDestestData[i4TestIdx].au1TDesBuf,
                TDES_BUFFER_LEN);
        MEMCPY (au1InitVect, TDestestData[i4TestIdx].au1TDesIv, DES_VECTOR_LEN);

        PRINT_RESULT ("\n3-DES\n");
        PRINT_RESULT ("Plain Text : ");
        for (i4Count = 0; i4Count < TDES_BUFFER_LEN; i4Count++)
        {
            PRINT_RESULT1 ("%02x ", au1TempData[i4Count]);
        }
        PRINT_RESULT ("\n");

        PRINT_RESULT ("InitVector : ");
        for (i4Count = 0; i4Count < i4BufLen; i4Count++)
        {
            PRINT_RESULT1 ("%02x ", au1InitVect[i4Count]);
        }
        PRINT_RESULT ("\n");

        DesArKeyScheduler (TDestestData[i4TestIdx].au1TDesKey1, &ArCryptoKey);
        MEMCPY (au8DesKey1, ArCryptoKey.tArDes.au8ArSubkey,
                sizeof (ArCryptoKey.tArDes.au8ArSubkey));
        DesArKeyScheduler (TDestestData[i4TestIdx].au1TDesKey2, &ArCryptoKey);
        MEMCPY (au8DesKey2, ArCryptoKey.tArDes.au8ArSubkey,
                sizeof (ArCryptoKey.tArDes.au8ArSubkey));
        DesArKeyScheduler (TDestestData[i4TestIdx].au1TDesKey3, &ArCryptoKey);
        MEMCPY (au8DesKey3, ArCryptoKey.tArDes.au8ArSubkey,
                sizeof (ArCryptoKey.tArDes.au8ArSubkey));

        MEMCPY (ArCryptoKey.tArTdes.ArKey0.au8ArSubkey, au8DesKey1,
                sizeof (ArCryptoKey.tArDes.au8ArSubkey));
        MEMCPY (ArCryptoKey.tArTdes.ArKey1.au8ArSubkey, au8DesKey2,
                sizeof (ArCryptoKey.tArDes.au8ArSubkey));
        MEMCPY (ArCryptoKey.tArTdes.ArKey2.au8ArSubkey, au8DesKey3,
                sizeof (ArCryptoKey.tArDes.au8ArSubkey));
        UtilAlgo.UtilDesAlgo.pu1DesInBuffer = au1TempData;
        UtilAlgo.UtilDesAlgo.u4DesInBufSize = TDES_BUFFER_LEN;
        UtilAlgo.UtilDesAlgo.punDesArCryptoKey = &ArCryptoKey;
        UtilAlgo.UtilDesAlgo.pu1DesInUserKey =
            TDestestData[i4TestIdx].au1TDesKey1;
        UtilAlgo.UtilDesAlgo.pu1DesInUserKey2 =
            TDestestData[i4TestIdx].au1TDesKey2;
        UtilAlgo.UtilDesAlgo.pu1DesInUserKey3 =
            TDestestData[i4TestIdx].au1TDesKey3;
        UtilAlgo.UtilDesAlgo.pu1DesInitVect = au1InitVect;
        UtilAlgo.UtilDesAlgo.u4DesInitVectLen = DES_VECTOR_LEN;
        UtilAlgo.UtilDesAlgo.u4DesInKeyLen = DES_VECTOR_LEN;
        UtilAlgo.UtilDesAlgo.i4DesEnc = ISS_UTIL_ENCRYPT;
        UtilEncrypt (ISS_UTIL_ALGO_TDES_CBC, &UtilAlgo);

        PRINT_RESULT ("After Encrypt : ");
        for (i4Count = 0; i4Count < TDES_BUFFER_LEN; i4Count++)
        {
            PRINT_RESULT1 ("%02x ", au1TempData[i4Count]);
        }
        PRINT_RESULT ("\n");

        PRINT_RESULT ("Expected Text : ");
        for (i4Count = 0; i4Count < TDES_BUFFER_LEN; i4Count++)
        {
            PRINT_RESULT1 ("%02x ",
                           TDestestData[i4TestIdx].au13DesResult[i4Count]);
        }
        PRINT_RESULT ("\n");

        if ((MEMCMP (au1TempData, TDestestData[i4TestIdx].au13DesResult,
                     TDES_BUFFER_LEN) != 0))
        {
            PRINTF ("Test Case %d Encryption : Failed\n", i4TestIdx + 1);
            return OSIX_FAILURE;
        }
        else
        {
            PRINT_RESULT1 ("Test Case %d Encryption : Passed\n", i4TestIdx + 1);
        }
        MEMSET (au1InitVect, 0, i4BufLen);
        MEMCPY (au1InitVect, TDestestData[i4TestIdx].au1TDesIv, DES_VECTOR_LEN);

        UtilAlgo.UtilDesAlgo.pu1DesInBuffer = au1TempData;
        UtilAlgo.UtilDesAlgo.u4DesInBufSize = TDES_BUFFER_LEN;
        UtilAlgo.UtilDesAlgo.punDesArCryptoKey = &ArCryptoKey;
        UtilAlgo.UtilDesAlgo.pu1DesInUserKey =
            TDestestData[i4TestIdx].au1TDesKey1;
        UtilAlgo.UtilDesAlgo.pu1DesInUserKey2 =
            TDestestData[i4TestIdx].au1TDesKey2;
        UtilAlgo.UtilDesAlgo.pu1DesInUserKey3 =
            TDestestData[i4TestIdx].au1TDesKey3;
        UtilAlgo.UtilDesAlgo.pu1DesInitVect = au1InitVect;
        UtilAlgo.UtilDesAlgo.u4DesInitVectLen = DES_VECTOR_LEN;
        UtilAlgo.UtilDesAlgo.u4DesInKeyLen = DES_VECTOR_LEN;
        UtilAlgo.UtilDesAlgo.i4DesEnc = ISS_UTIL_DECRYPT;
        UtilDecrypt (ISS_UTIL_ALGO_TDES_CBC, &UtilAlgo);

        PRINT_RESULT ("After Decrypt : ");
        for (i4Count = 0; i4Count < TDES_BUFFER_LEN; i4Count++)
        {
            PRINT_RESULT1 ("%02x ", au1TempData[i4Count]);
        }
        PRINT_RESULT ("\n");

        if ((MEMCMP (au1TempData, TDestestData[i4TestIdx].au1TDesBuf,
                     TDES_BUFFER_LEN) != 0))
        {
            PRINTF ("Test Case %d Decryption : Failed\n", i4TestIdx + 1);
            return OSIX_FAILURE;
        }
        else
        {
            PRINT_RESULT1 ("Test Case %d Decryption : Passed\n", i4TestIdx + 1);
        }
    }
    return OSIX_SUCCESS;
}

/****************************************************************************
                        End of File fipsdes.c
****************************************************************************/
