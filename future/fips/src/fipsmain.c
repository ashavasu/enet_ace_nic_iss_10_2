/***********************************************************************
*  Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
*  $Id: fipsmain.c,v 1.7 2017/09/12 13:26:43 siva Exp $
*
*  Description: This file contains FIPS module initialization routines.
*             
************************************************************************/
#ifndef __FIPSMAIN_C_
#define __FIPSMAIN_C_

#include "fipsinc.h"
/***************************************************************/
/*  Function Name   : FipsInitialize                           */
/*  Description     : Entry point function of FIPS module      */
/*  Input(s)        : None                                     */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
VOID
FipsInitialize (VOID)
{
    tFsCbInfo           FsCbInfo;
    INT4                i4SysLogId = 0;
    UINT4               u4Events = 0;
    INT4                i4FipsOperMode = 0;

    MEMSET (&FsCbInfo, 0, sizeof (tFsCbInfo));

    /* MIB registration for the fips mib */
    RegisterFSFIPS ();

    if (FIPS_FAILURE == FipsMainTaskInit ())
    {
        FIPS_TRC (ALL_FAILURE_TRC, "Failed to initialise FIPS main Task\r\n");
        /* Indicate the status of initialization to the main routine */
        FIPS_INIT_COMPLETE (OSIX_FAILURE);
    }

    i4FipsOperMode = FipsGetFipsCurrOperMode ();
    if ((i4FipsOperMode == FIPS_MODE) || (i4FipsOperMode == CNSA_MODE))
    {
        if (FipsSetSwitchMode (i4FipsOperMode) != FIPS_SUCCESS)
        {
            FIPS_TRC (ALL_FAILURE_TRC, "Failed to enable FIPS/CNSA mode\r\n");
            /* Indicate the status of initialization to the main routine */
            FIPS_INIT_COMPLETE (OSIX_FAILURE);
            return;
        }
    }

#ifdef SSL_WANTED
    FsCbInfo.pIssCustCheckSslCustMode = FipsGetFipsCurrOperMode;
    if ((FipsRegisterEvtCallBk
         (ISS_SSL_MODULE, SSL_CUST_MODE_CHK, &FsCbInfo)) != FIPS_SUCCESS)
    {
        FIPS_TRC (MGMT_TRC, "Error!!! Failed to register FIPS Mode check "
                  "CallBack to SSL\n");
        /* Indicate the status of initialization to the main routine */
        FIPS_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
#endif

#ifdef SSH_WANTED
    FsCbInfo.pIssCustCheckSshCustMode = FipsGetFipsCurrOperMode;
    if ((FipsRegisterEvtCallBk
         (ISS_SSH_MODULE, SSH_CUST_MODE_CHK, &FsCbInfo)) != FIPS_SUCCESS)
    {
        FIPS_TRC (MGMT_TRC, "Error!!! Failed to register FIPS Mode check "
                  "CallBack to SSH\n");
        /* Indicate the status of initialization to the main routine */
        FIPS_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
#endif

#ifdef CLI_WANTED
    FsCbInfo.pCliCustCheckStrictPasswd = FipsGetFipsCurrOperMode;
    if ((FipsRegisterEvtCallBk
         (ISS_CLI_MODULE, CLI_STRICT_PASSWD_CHK, &FsCbInfo)) != FIPS_SUCCESS)
    {
        FIPS_TRC (MGMT_TRC, "Error!!! Failed to register FIPS Mode check "
                  "CallBack to CLI\n");
        /* Indicate the status of initialization to the main routine */
        FIPS_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
#endif

    /* setting the default root user as admin for FIPS,CNSA mode */
    if ((i4FipsOperMode == FIPS_MODE) || (i4FipsOperMode == CNSA_MODE))
    {
        FpamSetDefaultRootUser ((INT1 *) FIPS_DEF_ROOT_USER);
        FpamSetDefaultRootUserPasswd ((INT1 *) FIPS_DEF_ROOT_PASSWD);
    }

    /* Register the module with Syslog server */
    i4SysLogId =
        SYS_LOG_REGISTER ((CONST UINT1 *) "[FIPS]", SYSLOG_ALERT_LEVEL);

    if (i4SysLogId < 0)
    {
        FIPS_TRC (ALL_FAILURE_TRC, "Failed to register with Syslog\r\n");
        /* Indicate the status of initialization to the main routine */
        FIPS_INIT_COMPLETE (OSIX_FAILURE);
        return;
    }
    FIPS_SYSLOG_ID = (UINT4) i4SysLogId;
    UtlSetUtlTrcLevel (ALL_FAILURE_TRC);
#ifdef SSL_WANTED
    HttpsSetTraceLevel (MGMT_TRC | ALL_FAILURE_TRC);
#endif

    /* Indicate the status of initialization to the main routine */
    FIPS_INIT_COMPLETE (OSIX_SUCCESS);

    while (1)
    {
        if (OSIX_SUCCESS ==
            OsixEvtRecv (FIPS_TASK_ID, FIPS_ALL_EVENTS, OSIX_WAIT, &u4Events))
        {
            FIPS_LOCK ();

            if (u4Events & FIPS_KEY_ZEROISE_EVENT)
            {
                FipsUtilKeyZeroiseHandler ();
            }

            if (u4Events & FIPS_SWITCH_TO_LEGACY_EVENT)
            {
                FipsUtilSwitchLegacyModeHandler ();
            }

            if (u4Events & FIPS_SWITCH_TO_FIPS_EVENT)
            {
                FipsUtilSwitchFipsModeHandler ();
            }

            if (u4Events & FIPS_SWITCH_TO_CNSA_EVENT)
            {
                FipsUtilSwitchCnsaModeHandler ();
            }

            if (u4Events & FIPS_SWITCH_FIPS_TO_CNSA_EVENT)
            {
                FipsUtilSwitchFipsToCnsaModeHandler ();
            }

            FIPS_UNLOCK ();
        }
    }
    return;
}

/***************************************************************/
/*  Function Name   : FipsDeinitialize                         */
/*  Description     : Exit point function of FIPS module       */
/*  Input(s)        : None                                     */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
VOID
FipsDeinitialize (VOID)
{
    /* MIB unregistration for the fips mib */
    UnRegisterFSFIPS ();

    /* Deregister the module with Syslog server */
    SYS_LOG_DEREGISTER (FIPS_SYSLOG_ID);
    FIPS_SYSLOG_ID = 0;

    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : FipsMainTaskInit                                 */
/*                                                                           */
/*    Description         : This function will perform following task in FIPS*/
/*                          Module:                                          */
/*                          o  Initializes global variables                  */
/*                          o  Create queues, semaphore, mempools for FIPS   */
/*                                                                           */
/*    Input(s)            : None                                             */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : OSIX_SUCCESS - When this function successfully   */
/*                                         allocated Fips Module resource.   */
/*                          OSIX_FAILURE - When this function failed to      */
/*                                         allocate Fips Module resources.   */
/*****************************************************************************/
INT4
FipsMainTaskInit (VOID)
{
    MEMSET (&gFipsGlobalInfo, 0, sizeof (tFipsGlobalInfo));

    if (OsixGetTaskId (SELF, FIPS_TASK_NAME, &(FIPS_TASK_ID)) == OSIX_FAILURE)
    {
        FIPS_TRC (ALL_FAILURE_TRC, "FipsMainTaskInit: Get Task "
                  "Id FAILED !!!\r\n");
        return OSIX_FAILURE;
    }

    if (OsixSemCrt (FIPS_MOD_SEM, &(FIPS_TASK_SEM_ID)) == OSIX_FAILURE)
    {
        FIPS_TRC (ALL_FAILURE_TRC, "FipsMainTaskInit Sem "
                  "Creation failed\r\n");
        return OSIX_FAILURE;
    }

    /* Semaphore is by default created with initial value 0. So GiveSem
     * is called before using the semaphore. */
    OsixSemGive (FIPS_TASK_SEM_ID);

    return FIPS_SUCCESS;
}

#endif /* _FIPSMAIN_C */
