/********************************************************************  
*  Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
*  $Id: fipsstubs.c,v 1.7 2017/09/12 13:26:43 siva Exp $
*
*  Description: This file contains the stub routines of FIPS mode.
*               This file will be compiled when the option FIPS is
*               not selected while making the ISS
*             
********************************************************************/
#include "lr.h"
#include "fips.h"

/***************************************************************/
/*  Function Name   : FipsGetFipsCurrOperMode                  */
/*  Description     : This stub function is added here to      */
/*                    avoid any compilation issues             */
/*  Input(s)        : None                                     */
/*  Output(s)       : None                                     */
/*  Returns         : LEGACY_MODE                              */
/***************************************************************/
INT4
FipsGetFipsCurrOperMode (VOID)
{
    return LEGACY_MODE;
}

/***************************************************************/
/*  Function Name   : FipsApiSetDefaultFipsModeConfig          */
/*  Description     : This function is used to configure the   */
/*                    ISS to the configurations compliant to   */
/*                    to FIPS mode.                            */
/*                    This function acts as a stub.            */
/*                    invoked at startup in FIPS mode          */
/*  Input(s)        : u1RestorationFlag - Flag that contains   */
/*                    the information about the Mib restoration*/
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
VOID
FipsApiSetDefaultFipsModeConfig (UINT1 u1RestorationFlag)
{
    UNUSED_PARAM (u1RestorationFlag);
    return;
}

/***************************************************************/
/*  Function Name   : FipsApiSwitchFipsToLegacy                */
/*  Description     : This function is used to do the necessary*/
/*                    changes while switching from FIPS to     */
/*                    LEGACY compliant mode                    */
/*                    This function here acts as a stub        */
/*  Input(s)        : None                                     */
/*  Output(s)       : None                                     */
/*  Returns         : FIPS_FAILURE / FIPS_SUCCESS              */
/***************************************************************/
INT4
FipsApiSwitchFipsToLegacy (VOID)
{
    return FIPS_FAILURE;
}

/***************************************************************/
/*  Function Name   : FipsApiRunKnownAnsTest                   */
/*  Description     : This function is used to call FIPS util  */
/*                    util function to perform self-test       */
/*                    This function here acts as a stub        */
/*  Input(s)        : i4FipsTestAlgo - Algorithm to test       */
/*  Output(s)       : pi4ErrCode - Contains the value of faild */
/*                    algorithm                                */
/*  Returns         : FIPS_FAILURE / FIPS_SUCCESS              */
/***************************************************************/
INT4
FipsApiRunKnownAnsTest (INT4 i4FipsTestAlgo, INT4 *pi4ErrCode)
{
    UNUSED_PARAM (i4FipsTestAlgo);
    UNUSED_PARAM (pi4ErrCode);
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : FipsSetKnownAnsTestFlags                             */
/*                                                                           */
/* Description        : This function is used to set the global KAT flags    */
/*                            (POWER_ON_STATUS and CONDITIONAL_SELF_TEST)    */
/*                      This function here acts as a stub                    */
/*                                                                           */
/* Input(s)           : i4KatFlagValue                                       */
/*                      Bitwise options to either set or unset               */
/*                      POWER_ON_STATUS and CONDITIONAL_SELF_TEST flags      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
FipsSetKnownAnsTestFlags (INT4 i4KatFlagValue)
{
    UNUSED_PARAM (i4KatFlagValue);
}

/*****************************************************************************/
/* Function Name      : FipsGetKnownAnsTestFlags                             */
/*                                                                           */
/* Description        : This function is used to get the global KAT flag     */
/*                      status (POWER_ON_STATUS or CONDITIONAL_SELF_TEST)    */
/*                      This function here acts as a stub                    */
/*                                                                           */
/* Input(s)           : i4KatFlag (The status of the flag to be returned)    */
/*                                                                           */
/* Output(s)          : Status of the flag.                                  */
/*                                                                           */
/* Return Value(s)    : TRUE or FALSE                                        */
/*                                                                           */
/*****************************************************************************/
INT4
FipsGetKnownAnsTestFlags (INT4 i4KatFlag)
{
    /* Based on the FLAG value, if that flag is set, this function will
     * return TRUE. If not, FALSE.
     * In Stub function it always return FALSE*/

    UNUSED_PARAM (i4KatFlag);
    return FALSE;                /* FALSE */
}
