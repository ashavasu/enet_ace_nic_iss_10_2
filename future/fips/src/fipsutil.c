/********************************************************************
 *  Copyright (C) 2008 Aricent Inc . All Rights Reserved            *
 *                                                                  *
 *  $Id: fipsutil.c,v 1.23 2017/09/12 13:26:43 siva Exp $        *
 *                                                                  *
 *  Description: This File contains the utility functions           *
 *               for the Aricent FIPS Module.                       *
 *                                                                  *
 ********************************************************************/
#ifndef __FIPSUTIL_C__
#define __FIPSUTIL_C__
#include "fipsinc.h"

/* This global variable has two bitwise flags,
 * POWER_ON_STATUS and CONDITIONAL_SELF_TEST. */
INT4                gi4KatFlagStatus = FIPS_KAT_DISABLE_ALL_FLAGS;

extern VOID Secv4DeleteSecAssocEntries PROTO ((VOID));
extern INT1 FpamWriteUsers PROTO ((VOID));
INT4                FipsUtilRegisterCallBack (UINT4 u4Event,
                                              tFsCbInfo * pFsCbInfo);

/***************************************************************************
 * Function           : FipsUtilRunKnownAnsTest 
 *
 * Description        : This function runs FIPS Known answer tests for
 *                      the corresponding algorithm that set
 *
 * Input(s)           : i4FipsTestAlgo - Algorithm to test. Set the
 *                                       corresponding bit
 *                     
 * Output(s)          : pi4ErrCode - Contains the value of faild algorithm
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ***************************************************************************/
INT4
FipsUtilRunKnownAnsTest (INT4 i4FipsTestAlgo, INT4 *pi4ErrCode)
{
    INT4                i4RunHmacSHA = 0;
    INT4                i4RetVal = OSIX_SUCCESS;

    /* Clear last run test result */
    gi4FipsTestExecResult = 0;
    gi4FipsFailedAlgo = 0;
    if (i4FipsTestAlgo & FIPS_KNOWN_ANS_TEST_SHA1)
    {
        /* Call SHA-1 Algorithm FIPS test */
        i4RetVal = FipsShaTestSha1Algo ();
        if (i4RetVal == OSIX_FAILURE)
        {
            *pi4ErrCode = FIPS_KAT_SHA1_FAILED;
            gi4FipsFailedAlgo = gi4FipsFailedAlgo | FIPS_KNOWN_ANS_TEST_SHA1;
        }
        else
        {
            gi4FipsTestExecResult = gi4FipsTestExecResult |
                FIPS_KNOWN_ANS_TEST_SHA1;
        }
    }
    if ((i4FipsTestAlgo & FIPS_KNOWN_ANS_TEST_SHA2) &&
        (i4RetVal == OSIX_SUCCESS))
    {
        /* Call SHA-2 Algorithm FIPS test */
        i4RetVal = FipsShaTestShaAndHmac (i4RunHmacSHA);
        if (i4RetVal == OSIX_FAILURE)
        {
            *pi4ErrCode = FIPS_KAT_SHA2_FAILED;
            gi4FipsFailedAlgo = gi4FipsFailedAlgo | FIPS_KNOWN_ANS_TEST_SHA2;
        }
        else
        {
            gi4FipsTestExecResult = gi4FipsTestExecResult |
                FIPS_KNOWN_ANS_TEST_SHA2;
        }
    }
    if ((i4FipsTestAlgo & FIPS_KNOWN_ANS_TEST_HMAC) &&
        (i4RetVal == OSIX_SUCCESS))
    {
        /* Call HMAC Algorithm FIPS test */
        i4RunHmacSHA = 1;
        i4RetVal = FipsShaTestShaAndHmac (i4RunHmacSHA);
        if (i4RetVal == OSIX_FAILURE)
        {
            *pi4ErrCode = FIPS_KAT_HMAC_FAILED;
            gi4FipsFailedAlgo = gi4FipsFailedAlgo | FIPS_KNOWN_ANS_TEST_HMAC;
        }
        else
        {
            gi4FipsTestExecResult = gi4FipsTestExecResult |
                FIPS_KNOWN_ANS_TEST_HMAC;
        }
    }
    if ((i4FipsTestAlgo & FIPS_KNOWN_ANS_TEST_AES) &&
        (i4RetVal == OSIX_SUCCESS))
    {
        /* Call AES Algorithm FIPS test */
        i4RetVal = FipsAesArTest ();
        if (i4RetVal == OSIX_FAILURE)
        {
            *pi4ErrCode = FIPS_KAT_AES_FAILED;
            gi4FipsFailedAlgo = gi4FipsFailedAlgo | FIPS_KNOWN_ANS_TEST_AES;
        }
        else
        {
            gi4FipsTestExecResult = gi4FipsTestExecResult |
                FIPS_KNOWN_ANS_TEST_AES;
        }
    }
    if ((i4FipsTestAlgo & FIPS_KNOWN_ANS_TEST_DES) &&
        (i4RetVal == OSIX_SUCCESS))
    {
        /* Call DES Algorithm FIPS test */
        i4RetVal = FipsDesArTest ();
        if (i4RetVal == OSIX_FAILURE)
        {
            *pi4ErrCode = FIPS_KAT_DES_FAILED;
            gi4FipsFailedAlgo = gi4FipsFailedAlgo | FIPS_KNOWN_ANS_TEST_DES;
        }
        else
        {
            gi4FipsTestExecResult = gi4FipsTestExecResult |
                FIPS_KNOWN_ANS_TEST_DES;
        }
    }
    if ((i4FipsTestAlgo & FIPS_KNOWN_ANS_TEST_RAND) &&
        (i4RetVal == OSIX_SUCCESS))
    {
        /* Call Random Number Generator Algorithm FIPS test */
        i4RetVal = FipsRandTestRandNumGen ();
        if (i4RetVal == OSIX_FAILURE)
        {
            *pi4ErrCode = FIPS_KAT_RNG_FAILED;
            gi4FipsFailedAlgo = gi4FipsFailedAlgo | FIPS_KNOWN_ANS_TEST_RAND;
        }
        else
        {
            gi4FipsTestExecResult = gi4FipsTestExecResult |
                FIPS_KNOWN_ANS_TEST_RAND;
        }
    }
    if ((i4FipsTestAlgo & FIPS_KNOWN_ANS_TEST_RSA) &&
        (i4RetVal == OSIX_SUCCESS))
    {
        /* Call RSA Algorithm FIPS test */
        i4RetVal = FipsSelfTestRsa ();
        if (i4RetVal != OSIX_SUCCESS)
        {
            *pi4ErrCode = FIPS_KAT_RSA_FAILED;
            gi4FipsFailedAlgo = gi4FipsFailedAlgo | FIPS_KNOWN_ANS_TEST_RSA;
        }
        else
        {
            gi4FipsTestExecResult = gi4FipsTestExecResult |
                FIPS_KNOWN_ANS_TEST_RSA;
        }
    }
    if ((i4FipsTestAlgo & FIPS_KNOWN_ANS_TEST_DSA) &&
        (i4RetVal == OSIX_SUCCESS))
    {
        /* Call DSA Algorithm FIPS test */
        i4RetVal = FipsSelfTestDsa ();
        if (i4RetVal != OSIX_SUCCESS)
        {
            *pi4ErrCode = FIPS_KAT_DSA_FAILED;
            gi4FipsFailedAlgo = gi4FipsFailedAlgo | FIPS_KNOWN_ANS_TEST_DSA;
        }
        else
        {
            gi4FipsTestExecResult = gi4FipsTestExecResult |
                FIPS_KNOWN_ANS_TEST_DSA;
        }

        FipsSetKnownAnsTestFlags (FIPS_KAT_POWER_ON_STATUS);
    }

    /* Handle failure condition */
    if (i4RetVal == OSIX_FAILURE)
    {
        /* Log Error */
        SYS_LOG_MSG ((SYSLOG_ALERT_LEVEL, FIPS_SYSLOG_ID,
                      ac1FipsErrString[*pi4ErrCode]));
    }
    SYS_LOG_MSG ((SYSLOG_CRITICAL_LEVEL, FIPS_SYSLOG_ID,
                  "FIPS Known Answer Test Passed"));
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : FipsSetKnownAnsTestFlags                             */
/*                                                                           */
/* Description        : This function is used to set the global KAT flags    */
/*                            (POWER_ON_STATUS and CONDITIONAL_SELF_TEST)    */
/*                                                                           */
/* Input(s)           : i4KatFlagValue                                       */
/*                      Bitwise options to either set or unset               */
/*                      POWER_ON_STATUS and CONDITIONAL_SELF_TEST flags      */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gi4KatFlagStatus                                     */
/*                                                                           */
/* Return Value(s)    : None                                                 */
/*                                                                           */
/*****************************************************************************/
VOID
FipsSetKnownAnsTestFlags (INT4 i4KatFlagValue)
{
    if (i4KatFlagValue & FIPS_KAT_POWER_ON_STATUS)
    {
        gi4KatFlagStatus |= FIPS_KAT_POWER_ON_STATUS;
    }

    if (i4KatFlagValue & FIPS_KAT_CONDITIONAL_SELF_TEST)
    {
        gi4KatFlagStatus |= FIPS_KAT_CONDITIONAL_SELF_TEST;
    }

    if (i4KatFlagValue & FIPS_KAT_UNSET_POWER_ON_STATUS)
    {
        gi4KatFlagStatus &= (~FIPS_KAT_POWER_ON_STATUS);
    }

    if (i4KatFlagValue & FIPS_KAT_UNSET_CONDITIONAL_SELF_TEST)
    {
        gi4KatFlagStatus &= (~FIPS_KAT_CONDITIONAL_SELF_TEST);
    }
}

/*****************************************************************************/
/* Function Name      : FipsGetKnownAnsTestFlags                             */
/*                                                                           */
/* Description        : This function is used to get the global KAT flag     */
/*                      status (POWER_ON_STATUS or CONDITIONAL_SELF_TEST)    */
/*                                                                           */
/* Input(s)           : i4KatFlag (The status of the flag to be returned)    */
/*                                                                           */
/* Output(s)          : Status of the flag.                                  */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gi4KatFlagStatus                                     */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : TRUE or FALSE                                        */
/*                                                                           */
/*****************************************************************************/
INT4
FipsGetKnownAnsTestFlags (INT4 i4KatFlag)
{
    /* Based on the FLAG value, if that flag is set, this function will
     * return TRUE. If not, FALSE */
    if (i4KatFlag & gi4KatFlagStatus)
    {
        return TRUE;            /* TRUE */
    }
    else
    {
        return FALSE;            /* FALSE */
    }
}

/*****************************************************************************/
/* Function Name      : FipsUtilDelCryptoKeys                                */
/*                                                                           */
/* Description        : This function is called to delete all crytographic   */
/*                      keys configured in ipsecv4, ipsecv6, ike, radius,    */
/*                      ssh and ssl modules.                                 */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FIPS_FAILURE or FIPS_SUCCESS                         */
/*****************************************************************************/

INT4
FipsUtilDelCryptoKeys (VOID)
{
    if (OSIX_FAILURE == OsixEvtSend (FIPS_TASK_ID, FIPS_KEY_ZEROISE_EVENT))
    {
        FIPS_TRC (ALL_FAILURE_TRC, "FipsUtilDelCryptoKeys: Key Zeroise Event "
                  "Send Failed!!!\r\n");
        return FIPS_FAILURE;
    }

    return FIPS_SUCCESS;
}

/***************************************************************/
/*  Function Name   : FipsUtilSwitchFipsToLegacy               */
/*  Description     : This function is used to do the necessary*/
/*                    changes while switching from FIPS to     */
/*                    Legacy compliant mode                    */
/*  Input(s)        : None                                     */
/*  Output(s)       : None                                     */
/*  Returns         : FIPS_FAILURE / FIPS_SUCCESS              */
/***************************************************************/
INT4
FipsUtilSwitchFipsToLegacy (VOID)
{

    if (OSIX_FAILURE == OsixEvtSend (FIPS_TASK_ID, FIPS_SWITCH_TO_LEGACY_EVENT))
    {
        FIPS_TRC (ALL_FAILURE_TRC, "FipsUtilSwitchFipsToLegacy : Switch to "
                  "Legacy mode Event Send Failed!!!\r\n");
        return FIPS_FAILURE;
    }

    return FIPS_SUCCESS;
}

/***************************************************************/
/*  Function Name   : FipsUtilSwitchLegacyToFips               */
/*  Description     : This function is used to do the necessary*/
/*                    changes while switching from LEGACY to   */
/*                    FIPS compliant mode                      */
/*  Input(s)        : None                                     */
/*  Output(s)       : None                                     */
/*  Returns         : FIPS_FAILURE/FIPS_SUCCESS                */
/***************************************************************/
INT4
FipsUtilSwitchLegacyToFips (VOID)
{
    if (OSIX_FAILURE == OsixEvtSend (FIPS_TASK_ID, FIPS_SWITCH_TO_FIPS_EVENT))
    {
        FIPS_TRC (ALL_FAILURE_TRC, "FipsUtilSwitchLegacyToFips : Switch to "
                  "FIPS mode Event Send Failed!!!\r\n");
        return FIPS_FAILURE;
    }

    return FIPS_SUCCESS;
}

/***************************************************************/
/*  Function Name   : FipsUtilSwitchLegacyToCnsa               */
/*  Description     : This function is used to do the necessary*/
/*                    changes while switching from non-FIPS to */
/*                    CNSA compliant mode                      */
/*  Input(s)        : None                                     */
/*  Output(s)       : None                                     */
/*  Returns         : FIPS_FAILURE/FIPS_SUCCESS                */
/***************************************************************/
INT4
FipsUtilSwitchLegacyToCnsa (VOID)
{
    if (OSIX_FAILURE == OsixEvtSend (FIPS_TASK_ID, FIPS_SWITCH_TO_CNSA_EVENT))
    {
        FIPS_TRC (ALL_FAILURE_TRC, "FipsUtilSwitchLegacyToCnsa : Switch to "
                  "CNSA mode Event Send Failed!!!\r\n");
        return FIPS_FAILURE;
    }

    return FIPS_SUCCESS;
}

/***************************************************************/
/*  Function Name   : FipsUtilSwitchFipsToCnsa                 */
/*  Description     : This function is used to do the necessary*/
/*                    changes while switching from FIPS to     */
/*                    CNSA or CNSA to FIPS compliant mode      */
/*  Input(s)        : None                                     */
/*  Output(s)       : None                                     */
/*  Returns         : FIPS_FAILURE/FIPS_SUCCESS                */
/***************************************************************/
INT4
FipsUtilSwitchFipsToCnsa (VOID)
{
    if (OSIX_FAILURE ==
        OsixEvtSend (FIPS_TASK_ID, FIPS_SWITCH_FIPS_TO_CNSA_EVENT))
    {
        FIPS_TRC (ALL_FAILURE_TRC, "FipsUtilSwitchFipsToCnsa : Switch to "
                  "FIPS/CNSA mode Event Send Failed!!!\r\n");
        return FIPS_FAILURE;
    }

    return FIPS_SUCCESS;
}

/***************************************************************/
/*  Function Name   : FipsUtilDisableAllProtocols              */
/*  Description     : This function is used to execute the     */
/*                    required CLI commands so as to disable   */
/*                    all the protocols in FIPS compliant mode */
/*  Input(s)        : None                                     */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
VOID
FipsUtilDisableAllProtocols (VOID)
{
#ifdef CLI_WANTED
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("set port-channel disable");
    CliExecuteAppCmd ("ip igmp snooping send-query disable");
    CliExecuteAppCmd ("ipv6 mld snooping send-query disable");
    CliExecuteAppCmd ("no spanning-tree");
    CliExecuteAppCmd ("no dot1x system-auth-control");
    CliExecuteAppCmd ("set mvrp disable");
    CliExecuteAppCmd ("set mmrp disable");
    CliExecuteAppCmd ("no service dhcp");
    CliExecuteAppCmd ("no service dhcp-relay");
    CliExecuteAppCmd ("no service dhcp-server");
    CliExecuteAppCmd ("no tacacs use-server");
    CliExecuteAppCmd ("set rmon disable");
    CliExecuteAppCmd ("rmon2 disable");
    CliExecuteAppCmd ("dsmon disable");
    CliExecuteAppCmd ("no beep server active");
    CliExecuteAppCmd ("sntp; set sntp client disabled; exit");
    CliExecuteAppCmd ("set ethernet-oam disable");
    CliExecuteAppCmd ("set lldp disable");
    CliExecuteAppCmd ("set ip mcast profiling disable");
    CliExecuteAppCmd ("set poe disable");
    CliExecuteAppCmd ("no ipv6 dhcp relay");
    CliExecuteAppCmd ("no ipv6 dhcp server");
    CliExecuteAppCmd ("set cn disable");
    CliExecuteAppCmd ("ets disable");
    CliExecuteAppCmd ("shutdown ptp");
    CliExecuteAppCmd ("no ip domain lookup");
    CliExecuteAppCmd ("no ip routing");
    CliExecuteAppCmd ("set ipv6 mld disable");
    CliExecuteAppCmd ("ip msdp disable");
    CliExecuteAppCmd ("ipv6 msdp disable");
    CliExecuteAppCmd ("no router rip");
    CliExecuteAppCmd ("no router ospf");
    CliExecuteAppCmd ("no ipv6 router ospf");
    CliExecuteAppCmd ("no router isis");
    CliExecuteAppCmd ("no router vrrp");
    CliExecuteAppCmd ("no router bgp");
    CliExecuteAppCmd ("set ip dvmrp disable");
    CliExecuteAppCmd ("set ip pim disable");
    CliExecuteAppCmd ("set ipv6 pim disable");
    CliExecuteAppCmd ("set ip igmp disable");
    CliExecuteAppCmd ("no ip igmp proxy-service");
    CliExecuteAppCmd ("no ipv6 rip");
    CliExecuteAppCmd ("no ip nat");
    CliExecuteAppCmd ("no ip multicast routing");
    CliExecuteAppCmd ("set vpn disable");
    CliExecuteAppCmd ("qos disable");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
#endif
    return;
}

/***************************************************************/
/*  Function Name   : FipsGetFipsCurrOperMode                  */
/*  Description     : This function is used to get the current */
/*                    current operating mode from the          */
/*                    corresponding hardware API               */
/*  Input(s)        : None                                     */
/*  Output(s)       : None                                     */
/*  Returns         : FIPS_MODE or LEGACY_MODE                 */
/***************************************************************/
INT4
FipsGetFipsCurrOperMode (VOID)
{
    return (IssGetFipsOperModeFromNvRam ());
}

/***************************************************************/
/*  Function Name   : FipsGetFirstGenRandNum                   */
/*  Description     : This function is used to get the first   */
/*                    generated random number which is stored  */
/*                    in global variable                       */
/*  Input(s)        : None                                     */
/*  Output(s)       : None                                     */
/*  Returns         : Firs generated random number             */
/***************************************************************/
INT4
FipsGetFirstGenRandNum (VOID)
{
    return (gi4FirstGenRandNum);
}

/***************************************************************/
/*  Function Name   : FipsUpdtaeFirstGenRandNum                */
/*  Description     : This function is used to update the first*/
/*                    generated random number in global        */
/*                    variable                                 */
/*  Input(s)        : Generated random number                  */
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
VOID
FipsUpdtaeFirstGenRandNum (INT4 i4RandNum)
{
    gi4FirstGenRandNum = i4RandNum;
}

/*****************************************************************************/
/* Function Name      : FipsUtilRegisterCallBack                             */
/*                                                                           */
/* Description        : This function registers the call backs from          */
/*                      application                                          */
/*                                                                           */
/* Input(s)           : u4Event - Event for which callback is registered     */
/*                      pFsCbInfo - Call Back Function                       */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : gaFipsCustCallBackEntry                              */
/*                                                                           */
/* Return Value(s)    : FIPS_SUCCESS/FIPS_FAILURE                            */
/*****************************************************************************/
INT4
FipsUtilRegisterCallBack (UINT4 u4Event, tFsCbInfo * pFsCbInfo)
{
    INT4                i4RetVal = FIPS_SUCCESS;

    switch (u4Event)
    {
        case FIPS_CUST_DEL_SRAM_CONTENTS:
            FIPS_UTIL_CALL_BACK[u4Event].pFipsCustDelSramContents =
                pFsCbInfo->pIssCustDelSramContents;
            break;

        default:
            i4RetVal = FIPS_FAILURE;
            break;
    }

    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : FipsUtilCallBack                                     */
/*                                                                           */
/* Description        : This function processes the callback events invoked  */
/*                      in the program.                                      */
/*                                                                           */
/* Input(s)           : u4Event - Event for which callback is registered     */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : gaFipsCustCallBackEntry                              */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : FIPS_SUCCESS/FIPS_FAILURE                            */
/*****************************************************************************/
INT4
FipsUtilCallBack (UINT4 u4Event)
{
    INT4                i4RetVal = FIPS_SUCCESS;

    /* This will be executed only when the call back is registered priorly */
    switch (u4Event)
    {
        case FIPS_CUST_DEL_SRAM_CONTENTS:
            if (NULL != (FIPS_UTIL_CALL_BACK[u4Event]).pFipsCustDelSramContents)
            {
                if (ISS_FAILURE ==
                    FIPS_UTIL_CALL_BACK[u4Event].pFipsCustDelSramContents ())
                {
                    i4RetVal = FIPS_FAILURE;
                }
            }
            break;
        default:
            i4RetVal = FIPS_FAILURE;
            break;
    }
    return i4RetVal;
}

/*****************************************************************************/
/* Function Name      : FipsSetFipsTraceLevel                                */
/*                                                                           */
/* Description        : This function is used to set the global trace level  */
/*                                                                           */
/* Input(s)           : None                                                 */
/*                                                                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : IKE_SUCCESS or IKE_FAILURE                           */
/*                                                                           */
/*****************************************************************************/
INT1
FipsSetFipsTraceLevel (UINT4 u4FipsTrcLevel)
{
    gi4FipsTraceOption = u4FipsTrcLevel;
    return IKE_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FipsUtilKeyZeroiseHandler                        */
/*                                                                           */
/*    Description         : This function will handle the key zeroise        */
/*                          event posted to FIPS module. It will             */
/*                          delete all the cryptographic keys in security    */
/*                          modules.                                         */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FIPS_SUCCESS/FIPS_FAILURE                        */
/*****************************************************************************/
INT4
FipsUtilKeyZeroiseHandler (VOID)
{
    INT1                i1Index = 0;
    INT4                i4NoOfActiveSession = 0;
    INT4                i4FipsOperMode = 0;

    i4FipsOperMode = FipsGetFipsCurrOperMode ();

    /* To close all the active and inactive SSH and Telnet sessions that are on going */
    CliDestroyConnections (CLI_TELNET_SSH_CLEAR, OSIX_FALSE);
    CliDestroyDefaultConnections (OSIX_FALSE);

    /* Wait till all the ssh and telnet sessions are successfully closed. This
     * is done because when the SSH sessions are on-going and the keys 
     * are deleted, the session will be terminated abruptly with 
     * error messages being thrown in client as well as server side */
    for (i1Index = 0; i1Index < FIPS_CLI_SESSION_CLOSE_TIMEOUT; i1Index += 1)
    {
        i4NoOfActiveSession = (INT4) CliGetActiveSessions ();
        FIPS_TRC_ARG1 (ALL_FAILURE_TRC, "FipsUtilKeyZeroiseHandler: "
                       "i4NoOfActiveSession = %d \r\n", i4NoOfActiveSession);

        if (LEGACY_MODE == i4FipsOperMode)
        {
            /* In Legacy mode, CLI console might be present. so reducing
             * the count by 1 */
            i4NoOfActiveSession -= 1;
        }

        if (i4NoOfActiveSession <= 0)
        {
            /* All the sessions are closed */
            break;
        }

        /* Some sessions are still active. Waiting for a second */
        OsixTskDelay (1 * SYS_NUM_OF_TIME_UNITS_IN_A_SEC);
    }
    if (FIPS_CLI_SESSION_CLOSE_TIMEOUT == i1Index)
    {
        /* All the sessions are not closed within the timeout period.
         * Log error and continue. */
        FIPS_TRC (ALL_FAILURE_TRC, "Error!!! Timeout for closing the telnet "
                  "and ssh CLI sessions during crypto zeroisation\r\n");
    }

#ifdef IPSECv4_WANTED
    Secv4DeleteSecAssocEntries ();
#endif

#ifdef IPSECv6_WANTED
    if (Secv6DeleteAllSecAssocEntries () == SEC_FAILURE)
    {
        FIPS_TRC (MGMT_TRC, "Error!!! Failed to Delete IPsecv6 SAs\n");
        return FIPS_FAILURE;
    }
#endif

#ifdef IKE_WANTED
    if (IkeDeleteAllKeys () == IKE_FAILURE)
    {
        FIPS_TRC (MGMT_TRC, "Error!!! Failed to Delete IKE Keys\n");
        return FIPS_FAILURE;
    }
#endif

#ifdef RADIUS_WANTED
    if (RadDeleteSharedSecretKey () == SNMP_FAILURE)
    {
        FIPS_TRC (MGMT_TRC, "Error!!! Failed to Delete Radius Keys\n");
        return FIPS_FAILURE;
    }
#endif
#ifdef OPENSSH_WANTED
    SSHDeleteKeys ();
#endif

#ifdef SSL_WANTED
    SSLDeleteKeys ();
#endif

    return FIPS_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FipsUtilSwitchLegacyModeHandler                  */
/*                                                                           */
/*    Description         : This function will handle the event for          */
/*                          switching from Fips/CNSA Mode to Legacy Mode.    */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FIPS_SUCCESS/FIPS_FAILURE                        */
/*****************************************************************************/
INT4
FipsUtilSwitchLegacyModeHandler (VOID)
{
    INT4                i4FipsOperMode = 0;

    i4FipsOperMode = FipsGetFipsCurrOperMode ();

    /* If the current mode is FIPS, then switch to Legacy mode */
    if ((FIPS_MODE == i4FipsOperMode) || (CNSA_MODE == i4FipsOperMode))
    {
        if (ISS_SUCCESS == IssCustSetFipsCurrOperMode (LEGACY_MODE))
        {
#ifdef CLI_WANTED
            /* Remove iss.conf file */
            issDeleteLocalFile ((UINT1 *) ISS_CONFIG_FILE);
            /* Delete all the users and their related files */
            CliUtilDeleteAllUsersInfo ();
            CliTakeAppContext ();
            MGMT_UNLOCK ();
            CliExecuteAppCmd ("end");
            CliExecuteAppCmd ("configure terminal");
            CliExecuteAppCmd ("end");
            /* To enable the CLI console in the next boot up,
             * in Non-FIPS mode */
            CliExecuteAppCmd ("cli console");
            CliGiveAppContext ();
            FpamWriteUsersDefault ();
            FpamWriteUsers ();
            MGMT_LOCK ();
#endif

            /* Zeroise all the keys */
            if (FIPS_FAILURE == FipsUtilKeyZeroiseHandler ())
            {
                FIPS_TRC (MGMT_TRC, "Error!!! Failed to delete the keys."
                          "Cannot switch to FIPS mode \r\n");
                return FIPS_FAILURE;
            }

            /* ISS will be rebooted */
            IssSystemRestart ();
        }
        return FIPS_FAILURE;
    }

    return FIPS_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FipsUtilSwitchFipsModeHandler                    */
/*                                                                           */
/*    Description         : This function will handle the event for          */
/*                          switching from Legacy mode to Fips Mode.        */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FIPS_SUCCESS/FIPS_FAILURE                        */
/*****************************************************************************/
INT4
FipsUtilSwitchFipsModeHandler (VOID)
{

    if (ISS_FALSE == IssCustCheckSwitchToFipsMode ())
    {
        FIPS_TRC (MGMT_TRC, "Error!!! NVMRO signal is not asserted"
                  "Cannot switch to FIPS mode\r\n");
        return FIPS_FAILURE;
    }

    if (LEGACY_MODE == FipsGetFipsCurrOperMode ())
    {
        if (ISS_FAILURE == IssCustSetFipsCurrOperMode (FIPS_MODE))
        {
            FIPS_TRC (MGMT_TRC, "Error!!! Failed to update the FIPS mode "
                      "in the persistance storage\r\n");
            IssCustFipsErrHandler (ISS_FIPS_OPER_MODE, ISS_FAILURE);
            return FIPS_FAILURE;
        }

#ifdef CLI_WANTED
        /* Remove iss.conf file */
        issDeleteLocalFile ((UINT1 *) ISS_CONFIG_FILE);
        /* Delete all the users and their related files */
        CliUtilDeleteAllUsersInfo ();
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("end");
        CliExecuteAppCmd ("configure terminal");
        CliExecuteAppCmd ("no snmp community index NETMAN");
        CliExecuteAppCmd ("no snmp community index PUBLIC");
        CliExecuteAppCmd ("username admin password Admin#123 privilege 15");
        CliExecuteAppCmd ("end");
        CliGiveAppContext ();
        MGMT_LOCK ();
#endif

        /* Zeroise all the keys */
        if (FIPS_FAILURE == FipsUtilKeyZeroiseHandler ())
        {
            FIPS_TRC (MGMT_TRC, "Error!!! Failed to delete the keys."
                      "Cannot switch to FIPS mode \r\n");
            return FIPS_FAILURE;
        }

        /* ISS will be rebooted */
        IssSystemRestart ();

        /* In case if the system restart fails */
        return FIPS_FAILURE;
    }

    return FIPS_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FipsUtilSwitchCnsaModeHandler                    */
/*                                                                           */
/*    Description         : This function will handle the event for          */
/*                          switching from Legacy mode to Cnsa Mode.        */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FIPS_SUCCESS/FIPS_FAILURE                        */
/*****************************************************************************/
INT4
FipsUtilSwitchCnsaModeHandler (VOID)
{
    if (ISS_FALSE == IssCustCheckSwitchToFipsMode ())
    {
        FIPS_TRC (MGMT_TRC, "Error!!! NVMRO signal is not asserted"
                  "Cannot switch to CNSA mode\r\n");
        return FIPS_FAILURE;
    }

    if (LEGACY_MODE == FipsGetFipsCurrOperMode ())
    {
        if (ISS_FAILURE == IssCustSetFipsCurrOperMode (CNSA_MODE))
        {
            FIPS_TRC (MGMT_TRC, "Error!!! Failed to update the CNSA mode "
                      "in the persistance storage\r\n");
            IssCustFipsErrHandler (ISS_FIPS_OPER_MODE, ISS_FAILURE);
            return FIPS_FAILURE;
        }

#ifdef CLI_WANTED
        /* Remove iss.conf file */
        issDeleteLocalFile ((UINT1 *) ISS_CONFIG_FILE);
        /* Delete all the users and their related files */
        CliUtilDeleteAllUsersInfo ();
        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("end");
        CliExecuteAppCmd ("configure terminal");
        CliExecuteAppCmd ("no snmp community index NETMAN");
        CliExecuteAppCmd ("no snmp community index PUBLIC");
        CliExecuteAppCmd ("username admin password Admin#123 privilege 15");
        CliExecuteAppCmd ("end");
        CliGiveAppContext ();
        MGMT_LOCK ();
#endif

        /* Zeroise all the keys */
        if (FIPS_FAILURE == FipsUtilKeyZeroiseHandler ())
        {
            FIPS_TRC (MGMT_TRC, "Error!!! Failed to delete the keys."
                      "Cannot switch to CNSA mode \r\n");
            return FIPS_FAILURE;
        }

        /* ISS will be Rebooted */
        IssSystemRestart ();

        /* In case if the system restart fails */
        return FIPS_FAILURE;
    }

    return FIPS_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FipsUtilSwitchFipsToCnsaModeHandler              */
/*                                                                           */
/*    Description         : This function will handle the event for          */
/*                          switching from Fips mode to Cnsa Mode or         */
/*                          Cnsa Mode to Fips Mode.                          */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FIPS_SUCCESS/FIPS_FAILURE                        */
/*****************************************************************************/
INT4
FipsUtilSwitchFipsToCnsaModeHandler (VOID)
{
    INT4                i4FipsOperMode = 0;

    i4FipsOperMode = FipsGetFipsCurrOperMode ();

    if (ISS_FALSE == IssCustCheckSwitchToFipsMode ())
    {
        FIPS_TRC (MGMT_TRC, "Error!!! NVMRO signal is not asserted"
                  "Cannot switch to CNSA mode\r\n");
        return FIPS_FAILURE;
    }

    if (FIPS_MODE == i4FipsOperMode)
    {
        if (ISS_FAILURE == IssCustSetFipsCurrOperMode (CNSA_MODE))
        {
            FIPS_TRC (MGMT_TRC, "Error!!! Failed to update the CNSA mode "
                      "in the persistance storage\r\n");
            IssCustFipsErrHandler (ISS_FIPS_OPER_MODE, ISS_FAILURE);
            return FIPS_FAILURE;
        }
    }
    else if (CNSA_MODE == i4FipsOperMode)
    {
        if (ISS_FAILURE == IssCustSetFipsCurrOperMode (FIPS_MODE))
        {
            FIPS_TRC (MGMT_TRC, "Error!!! Failed to update the FIPS mode "
                      "in the persistance storage\r\n");
            IssCustFipsErrHandler (ISS_FIPS_OPER_MODE, ISS_FAILURE);
            return FIPS_FAILURE;
        }
    }
#ifdef CLI_WANTED
    /* Remove iss.conf file */
    issDeleteLocalFile ((UINT1 *) ISS_CONFIG_FILE);
    CliTakeAppContext ();
    MGMT_UNLOCK ();
    CliExecuteAppCmd ("end");
    CliExecuteAppCmd ("configure terminal");
    CliExecuteAppCmd ("no snmp community index NETMAN");
    CliExecuteAppCmd ("no snmp community index PUBLIC");
    CliExecuteAppCmd ("username admin password Admin#123 privilege 15");
    CliExecuteAppCmd ("end");
    CliGiveAppContext ();
    MGMT_LOCK ();
#endif

    /* Zeroise all the keys */
    if (FIPS_FAILURE == FipsUtilKeyZeroiseHandler ())
    {
        FIPS_TRC (MGMT_TRC, "Error!!! Failed to delete the keys."
                  "Cannot switch to FIPS mode \r\n");
        return FIPS_FAILURE;
    }

    /* ISS will be rebooted */
    IssSystemRestart ();

    return FIPS_SUCCESS;
}

/*****************************************************************************/
/*    Function Name       : FipsUtilCnsaKeyZeroiseHandler                    */
/*                                                                           */
/*    Description         : This function will handle the key zeroise        */
/*                          event posted to FIPS module when mode changed    */
/*                          from FIPS to CNSA or CNSA to FIPS. It will       */
/*                          delete all the cryptographic keys in security    */
/*                          modules.                                         */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Returns             : FIPS_SUCCESS/FIPS_FAILURE                        */
/*****************************************************************************/
INT4
FipsUtilCnsaKeyZeroiseHandler (VOID)
{
    INT1                i1Index = 0;
    INT4                i4NoOfActiveSession = 0;

    CliDestroyConnections (CLI_TELNET_SSH_CLEAR, OSIX_FALSE);
    /* To close all the active and inactive SSH and Telnet sessions that are on going */
    CliDestroyDefaultConnections (OSIX_FALSE);

    /* Wait till all the ssh and telnet sessions are successfully closed. This
     * is done because when the SSH sessions are on-going and the keys
     * are deleted, the session will be terminated abruptly with
     * error messages being thrown in client as well as server side */
    for (i1Index = 0; i1Index < FIPS_CLI_SESSION_CLOSE_TIMEOUT; i1Index += 1)
    {
        i4NoOfActiveSession = (INT4) CliGetActiveSessions ();

        if (i4NoOfActiveSession <= 0)
        {
            /* All the sessions are closed */
            break;
        }

        /* Some sessions are still active. Waiting for a second */
        OsixTskDelay (1 * SYS_NUM_OF_TIME_UNITS_IN_A_SEC);
    }
    if (FIPS_CLI_SESSION_CLOSE_TIMEOUT == i1Index)
    {
        /* All the sessions are not closed within the timeout period.
         * Log error and continue. */
        FIPS_TRC (ALL_FAILURE_TRC, "Error!!! Timeout for closing the telnet "
                  "and ssh CLI sessions during crypto zeroisation\r\n");
    }

#ifdef IPSECv4_WANTED
    Secv4DeleteSecAssocEntries ();
#endif

#ifdef IPSECv6_WANTED
    if (Secv6DeleteAllSecAssocEntries () == SEC_FAILURE)
    {
        FIPS_TRC (MGMT_TRC, "Error!!! Failed to Delete IPsecv6 SAs\n");
        return FIPS_FAILURE;
    }
#endif

#ifdef IKE_WANTED
    if (IkeDeleteAllKeys () == IKE_FAILURE)
    {
        FIPS_TRC (MGMT_TRC, "Error!!! Failed to Delete IKE Keys\n");
        return FIPS_FAILURE;
    }
#endif

#ifdef RADIUS_WANTED
    if (RadDeleteSharedSecretKey () == SNMP_FAILURE)
    {
        FIPS_TRC (MGMT_TRC, "Error!!! Failed to Delete Radius Keys\n");
        return FIPS_FAILURE;
    }
#endif
    FipsUtilCallBack (FIPS_CUST_DEL_SRAM_CONTENTS);

#ifdef OPENSSH_WANTED
    issDeleteLocalFile ((UINT1 *) SSL_CERT_SAVE_FILE);
    issDeleteLocalFile ((UINT1 *) SSH_KEY_SAVE_FILE_1);
    issDeleteLocalFile ((UINT1 *) SSH_KEY_SAVE_FILE_2);
#endif

#ifdef SSL_WANTED
    SSLDeleteKeys ();
#endif

    return FIPS_SUCCESS;
}

#endif /* __FIPSUTIL_C__ */

/****************************************************************************
                        End of File fipsutil.c
****************************************************************************/
