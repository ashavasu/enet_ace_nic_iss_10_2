/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsfipslw.c,v 1.9 2017/09/12 13:26:43 siva Exp $
*
* Description: Protocol Low Level Routines
*********************************************************************/
#include  "fipsinc.h"

/* Low Level GET Routine for All Objects  */

/****************************************************************************
 Function    :  nmhGetFsFipsOperMode
 Input       :  The Indices

                The Object 
                retValFsFipsOperMode
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFipsOperMode (INT4 *pi4RetValFsFipsOperMode)
{
    *pi4RetValFsFipsOperMode = FipsGetFipsCurrOperMode ();
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFipsTestAlgo
 Input       :  The Indices

                The Object 
                retValFsFipsTestAlgo
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFipsTestAlgo (INT4 *pi4RetValFsFipsTestAlgo)
{
    *pi4RetValFsFipsTestAlgo = FIPS_KNOWN_ANS_TEST_NONE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsfipsZeroizeCryptoKeys
 Input       :  The Indices

                The Object 
                retValFsfipsZeroizeCryptoKeys
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsfipsZeroizeCryptoKeys (INT4 *pi4RetValFsfipsZeroizeCryptoKeys)
{
    *pi4RetValFsfipsZeroizeCryptoKeys = FIPS_FALSE;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFipsTraceLevel
 Input       :  The Indices

                The Object 
                retValFsFipsTraceLevel
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFipsTraceLevel (INT4 *pi4RetValFsFipsTraceLevel)
{
    *pi4RetValFsFipsTraceLevel = gi4FipsTraceOption;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFipsTestExecutionResult
 Input       :  The Indices

                The Object 
                retValFsFipsTestExecutionResult
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFipsTestExecutionResult (INT4 *pi4RetValFsFipsTestExecutionResult)
{
    *pi4RetValFsFipsTestExecutionResult = gi4FipsTestExecResult;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFipsFailedAlgorithm
 Input       :  The Indices

                The Object 
                retValFsFipsFailedAlgorithm
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFipsFailedAlgorithm (INT4 *pi4RetValFsFipsFailedAlgorithm)
{
    *pi4RetValFsFipsFailedAlgorithm = gi4FipsFailedAlgo;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhGetFsFipsBypassCapability
 Input       :  The Indices

                The Object 
                retValFsFipsBypassCapability
 Output      :  The Get Low Lev Routine Take the Indices &
                store the Value requested in the Return val.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhGetFsFipsBypassCapability (INT4 *pi4RetValFsFipsBypassCapability)
{
    INT4                i1BypassCapability = 0;

    *pi4RetValFsFipsBypassCapability = i1BypassCapability;

#ifdef IKE_WANTED
    *pi4RetValFsFipsBypassCapability = IkeGetBypassCapability ();
#endif

#ifdef IPSECv4_WANTED
    Secv4GetBypassCapability (&i1BypassCapability);
    *pi4RetValFsFipsBypassCapability &= i1BypassCapability;
    i1BypassCapability = 0;
#endif

#ifdef IPSECv6_WANTED
    Secv6GetBypassCapability (&i1BypassCapability);
    *pi4RetValFsFipsBypassCapability &= i1BypassCapability;
#endif
    return SNMP_SUCCESS;
}

/* Low Level SET Routine for All Objects  */
/****************************************************************************
 Function    :  nmhSetFsFipsOperMode
 Input       :  The Indices

                The Object 
                setValFsFipsOperMode
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsFipsOperMode (INT4 i4SetValFsFipsOperMode)
{
    INT4                i4FipsOperMode = 0;
    UINT1               au1AuditLogMsg[AUDIT_LOG_SIZE];
    UINT1               au1CurMode[AUDIT_LEN];
    UINT1               au1NewMode[AUDIT_LEN];

    MEMSET (au1AuditLogMsg, 0, AUDIT_LOG_SIZE);
    MEMSET (au1CurMode, 0, AUDIT_LEN);
    MEMSET (au1NewMode, 0, AUDIT_LEN);

    i4FipsOperMode = FipsGetFipsCurrOperMode ();
    if (i4FipsOperMode != i4SetValFsFipsOperMode)
    {
        switch (i4SetValFsFipsOperMode)
        {
            case FIPS_MODE:
                if (i4FipsOperMode == LEGACY_MODE)
                {
                    if (FIPS_FAILURE == FipsUtilSwitchLegacyToFips ())
                    {
                        return SNMP_FAILURE;
                    }
                    STRNCPY (au1CurMode, "LEGACY", STRLEN ("LEGACY"));
                    FIPS_TRC (MGMT_TRC, "Changing from LEGACY "
                              "to FIPS Mode\n");
                }
                else if (i4FipsOperMode == CNSA_MODE)
                {
                    if (FIPS_FAILURE == FipsUtilSwitchFipsToCnsa ())
                    {
                        return SNMP_FAILURE;
                    }
                    STRNCPY (au1CurMode, "CNSA", STRLEN ("CNSA"));
                    FIPS_TRC (MGMT_TRC, "Changing from CNSA " "to FIPS Mode\n");
                }
                STRNCPY (au1NewMode, "FIPS", STRLEN ("FIPS"));
                break;
            case CNSA_MODE:
                if (i4FipsOperMode == LEGACY_MODE)
                {
                    if (FIPS_FAILURE == FipsUtilSwitchLegacyToCnsa ())
                    {
                        return SNMP_FAILURE;
                    }
                    STRNCPY (au1CurMode, "LEGACY", STRLEN ("LEGACY"));
                    FIPS_TRC (MGMT_TRC, "Changing from LEGACY "
                              "to CNSA Mode\n");
                }
                else if (i4FipsOperMode == FIPS_MODE)
                {
                    if (FIPS_FAILURE == FipsUtilSwitchFipsToCnsa ())
                    {
                        return SNMP_FAILURE;
                    }
                    STRNCPY (au1CurMode, "FIPS", STRLEN ("FIPS"));
                    FIPS_TRC (MGMT_TRC, "Changing from FIPS " "to CNSA Mode\n");
                }
                STRNCPY (au1NewMode, "CNSA", STRLEN ("CNSA"));
                break;
            case LEGACY_MODE:
                if (FIPS_FAILURE == FipsUtilSwitchFipsToLegacy ())
                {
                    return SNMP_FAILURE;
                }
                if (i4FipsOperMode == FIPS_MODE)
                {
                    STRNCPY (au1CurMode, "FIPS", STRLEN ("FIPS"));
                    FIPS_TRC (MGMT_TRC, "Changing from FIPS "
                              "to Legacy Mode\n");
                }
                else if (i4FipsOperMode == CNSA_MODE)
                {
                    STRNCPY (au1CurMode, "CNSA", STRLEN ("CNSA"));
                    FIPS_TRC (MGMT_TRC, "Changing from CNSA "
                              "to Legacy Mode\n");
                }

                STRNCPY (au1NewMode, "LEGACY", STRLEN ("LEGACY"));
                break;
            default:
                return SNMP_FAILURE;

        }
        if (ISS_IS_AUDIT_ENABLED () == ISS_TRUE)
        {
            /*Send log to Audit log file */
            SPRINTF ((CHR1 *) au1AuditLogMsg, "Changing %s to %s mode",
                     (CHR1 *) au1CurMode, (CHR1 *) au1NewMode);
            MsrAuditSendLogInfo (au1AuditLogMsg, AUDIT_INFO_LEVEL);
        }
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsFipsTestAlgo
 Input       :  The Indices

                The Object 
                setValFsFipsTestAlgo
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsFipsTestAlgo (INT4 i4SetValFsFipsTestAlgo)
{
    INT4                i4RetVal = 0;
    INT4                i4ErrCode = 0;

    i4RetVal = FipsUtilRunKnownAnsTest (i4SetValFsFipsTestAlgo, &i4ErrCode);

    if (i4RetVal == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsfipsZeroizeCryptoKeys
 Input       :  The Indices

                The Object 
                setValFsfipsZeroizeCryptoKeys
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsfipsZeroizeCryptoKeys (INT4 i4SetValFsfipsZeroizeCryptoKeys)
{
    if (i4SetValFsfipsZeroizeCryptoKeys == FIPS_TRUE)
    {
        if (FipsUtilDelCryptoKeys () == FIPS_FAILURE)
        {
            return SNMP_FAILURE;
        }
        return SNMP_SUCCESS;
    }
    else
    {
        return SNMP_SUCCESS;
    }
}

/****************************************************************************
 Function    :  nmhSetFsFipsTraceLevel
 Input       :  The Indices

                The Object 
                setValFsFipsTraceLevel
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsFipsTraceLevel (INT4 i4SetValFsFipsTraceLevel)
{
    gi4FipsTraceOption = i4SetValFsFipsTraceLevel;
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhSetFsFipsBypassCapability
 Input       :  The Indices

                The Object 
                setValFsFipsBypassCapability
 Output      :  The Set Low Lev Routine Take the Indices &
                Sets the Value accordingly.
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhSetFsFipsBypassCapability (INT4 i4SetValFsFipsBypassCapability)
{
    INT4                i4BypassCapability = 0;

    i4BypassCapability = i4SetValFsFipsBypassCapability;
#ifdef IKE_WANTED
    /* set the bypass capability to IKE */
    IkeSetBypassCapability (i4BypassCapability);
#endif
#ifdef IPSECv4_WANTED
    /* set the bypass capability to IPsecv4 */
    Secv4SetBypassCapability (i4BypassCapability);
#endif
#ifdef IPSECv6_WANTED
    /* set the bypass capability to IPsecv6 */
    Secv6SetBypassCapability (i4BypassCapability);
#endif
    return SNMP_SUCCESS;
}

/* Low Level TEST Routines for All Objects  */
/****************************************************************************
 Function    :  nmhTestv2FsFipsOperMode
 Input       :  The Indices

                The Object 
                testValFsFipsOperMode
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsFipsOperMode (UINT4 *pu4ErrorCode, INT4 i4TestValFsFipsOperMode)
{
    if ((i4TestValFsFipsOperMode != FIPS_MODE) &&
        (i4TestValFsFipsOperMode != LEGACY_MODE) &&
        (i4TestValFsFipsOperMode != CNSA_MODE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsFipsTestAlgo
 Input       :  The Indices

                The Object 
                testValFsFipsTestAlgo
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsFipsTestAlgo (UINT4 *pu4ErrorCode, INT4 i4TestValFsFipsTestAlgo)
{
    if ((i4TestValFsFipsTestAlgo < FIPS_KNOWN_ANS_TEST_NONE)
        || (i4TestValFsFipsTestAlgo > FIPS_KNOWN_ANS_TEST_ALL))
    {

        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsfipsZeroizeCryptoKeys
 Input       :  The Indices

                The Object 
                testValFsfipsZeroizeCryptoKeys
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsfipsZeroizeCryptoKeys (UINT4 *pu4ErrorCode,
                                  INT4 i4TestValFsfipsZeroizeCryptoKeys)
{
    if (i4TestValFsfipsZeroizeCryptoKeys == FIPS_TRUE ||
        i4TestValFsfipsZeroizeCryptoKeys == FIPS_FALSE)
    {
        return SNMP_SUCCESS;
    }
    *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
    return SNMP_FAILURE;
}

/****************************************************************************
 Function    :  nmhTestv2FsFipsTraceLevel
 Input       :  The Indices

                The Object 
                testValFsFipsTraceLevel
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsFipsTraceLevel (UINT4 *pu4ErrorCode, INT4 i4TestValFsFipsTraceLevel)
{
    if ((i4TestValFsFipsTraceLevel < FIPS_MIN_VALID_TRACE)
        || (i4TestValFsFipsTraceLevel > FIPS_MAX_VALID_TRACE))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhTestv2FsFipsBypassCapability
 Input       :  The Indices

                The Object 
                testValFsFipsBypassCapability
 Output      :  The Test Low Lev Routine Take the Indices &
                Test whether that Value is Valid Input for Set.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhTestv2FsFipsBypassCapability (UINT4 *pu4ErrorCode,
                                 INT4 i4TestValFsFipsBypassCapability)
{
    if ((i4TestValFsFipsBypassCapability != BYPASS_ENABLED) &&
        (i4TestValFsFipsBypassCapability != BYPASS_DISABLED))
    {
        *pu4ErrorCode = SNMP_ERR_WRONG_VALUE;
        return SNMP_FAILURE;
    }

    return SNMP_SUCCESS;
}

/* Low Level Dependency Routines for All Objects  */

/****************************************************************************
 Function    :  nmhDepv2FsFipsOperMode
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsFipsOperMode (UINT4 *pu4ErrorCode, tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsFipsTestAlgo
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsFipsTestAlgo (UINT4 *pu4ErrorCode,
                        tSnmpIndexList * pSnmpIndexList,
                        tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsfipsZeroizeCryptoKeys
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsfipsZeroizeCryptoKeys (UINT4 *pu4ErrorCode,
                                 tSnmpIndexList * pSnmpIndexList,
                                 tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsFipsTraceLevel
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsFipsTraceLevel (UINT4 *pu4ErrorCode,
                          tSnmpIndexList * pSnmpIndexList,
                          tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}

/****************************************************************************
 Function    :  nmhDepv2FsFipsBypassCapability
 Output      :  The Dependency Low Lev Routine Take the Indices &
                check whether dependency is met or not.
                Stores the value of error code in the Return val
 Error Codes :  The following error codes are to be returned
                SNMP_ERR_WRONG_LENGTH ref:(4 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_WRONG_VALUE ref:(6 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_NO_CREATION ref:(7 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_NAME ref:(8 of Sect 4.2.5 of rfc1905)
                SNMP_ERR_INCONSISTENT_VALUE ref:(10 of Sect 4.2.5 of rfc1905)
 Returns     :  SNMP_SUCCESS or SNMP_FAILURE
****************************************************************************/
INT1
nmhDepv2FsFipsBypassCapability (UINT4 *pu4ErrorCode,
                                tSnmpIndexList * pSnmpIndexList,
                                tSNMP_VAR_BIND * pSnmpVarBind)
{
    UNUSED_PARAM (pu4ErrorCode);
    UNUSED_PARAM (pSnmpIndexList);
    UNUSED_PARAM (pSnmpVarBind);
    return SNMP_SUCCESS;
}
