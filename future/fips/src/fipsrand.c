/********************************************************************
 *  Copyright (C) 2008 Aricent Inc . All Rights Reserved            *
 *                                                                  *
 *  $Id: fipsrand.c,v 1.2 2011/06/07 06:42:34 siva Exp $  *
 *                                                                  *
 *  Description: This file contains the test vectors for verifying  *
 *               RAND algorithm.                                    *
 ********************************************************************/
#include "fipsinc.h"

/***************************************************************************
 * Function           : FipsRandTestRandNumGen 
 *
 * Description        : This function tests the random number generator
 *
 * Input(s)           : None
 *                     
 * Output(s)          : None
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE
 ***************************************************************************/
INT4
FipsRandTestRandNumGen ()
{
    UINT1               au1InBuf[2500];
    INT4                i4Count = 0;
    INT4                i4Value = 0;
    INT4                i4Loop = 0;
    INT4                i4OutSign = 0;
    INT4                i4Sign = 0;
    INT4                i4NSign = 0;
    INT4                i4Error = 0;
    FS_ULONG            BitSetCnt = 0;
    FS_LONG             DivideReslut = 0;
    FS_ULONG            Segment[16];
    FS_ULONG            Runs[2][34];

    if ((UtilRandNumGen (au1InBuf, 2500)) != OSIX_SUCCESS)
    {
        PRINTF ("Random number generation failed !!!\n");
    }

    for (i4Count = 0; i4Count < 16; i4Count++)
    {
        Segment[i4Count] = 0;
    }
    for (i4Count = 0; i4Count < 34; i4Count++)
    {
        Runs[0][i4Count] = Runs[1][i4Count] = 0;
    }

    /* test 1 and 2 */
    i4Sign = 0;
    i4NSign = 0;
    for (i4Count = 0; i4Count < 2500; i4Count++)
    {
        i4Value = au1InBuf[i4Count];

        Segment[i4Value & 0x0f]++;
        Segment[(i4Value >> 4) & 0x0f]++;

        for (i4Loop = 0; i4Loop < 8; i4Loop++)
        {
            i4OutSign = (i4Value & 0x01);
            if (i4OutSign == i4Sign)
            {
                i4NSign++;
            }
            else
            {
                if (i4NSign > 34)
                {
                    i4NSign = 34;
                }
                if (i4NSign != 0)
                {
                    Runs[i4Sign][i4NSign - 1]++;
                    if (i4NSign > 6)
                    {
                        Runs[i4Sign][5]++;
                    }
                }
                i4Sign = i4OutSign;
                i4NSign = 1;
            }
            if (i4OutSign)
            {
                BitSetCnt++;
            }
            i4Value >>= 1;
        }
    }
    if (i4NSign > 34)
    {
        i4NSign = 34;
    }
    if (i4NSign != 0)
    {
        Runs[i4Sign][i4NSign - 1]++;
    }

    /* test 1 */
    /* The Monobit Test
     *   Count the number of ones in the 20,000 bit stream.
     *   Denote this quantity by X.
     *   
     *   The test is passed if 9,654 < X < 10,346.
     */
    if (!((9654 < BitSetCnt) && (BitSetCnt < 10346)))
    {
        PRINTF ("test 1 failed, X=%lu\n", BitSetCnt);
        i4Error++;
    }
    PRINT_RESULT ("Monobit Test Successfully done\n");

    /* test 2 */
    /* The Poker Test 
     *   Divide the 20,000 bit stream into 5,000 contiguous 4
     *   bit segments. Count and store the number of occurrences
     *   of each of the 16 possible 4 bit values.  Denote f(i)
     *   as the number of each 4 bit value i where 0 >= i <= 15
     *   Evaluate the following:
     *   X = (16/5000) * [SUM of f(i)^2, for i = 0 to 15] - 5000
     *   
     *   The test is passed if 1.03 < X < 57.4.
     */
    DivideReslut = 0;
    for (i4Count = 0; i4Count < 16; i4Count++)
    {
        DivideReslut += Segment[i4Count] * Segment[i4Count];
    }
    DivideReslut = (DivideReslut * 8) / 25 - 500000;
    if (!((103 < DivideReslut) && (DivideReslut < 5740)))
    {
        PRINTF ("test 2 failed, X=%ld.%02ld\n",
                DivideReslut / 100L, DivideReslut % 100L);
        i4Error++;
    }
    PRINT_RESULT ("Poker Test Successfully done\n");

    /* test 3 */
    /* The Runs Test
     * A run is defined as a maximal sequence of consecutive
     * bits of either all ones or all  zeros, which is part of
     * the 20,000 bit sample stream.  The incidences of runs
     * (for both  consecutive zeros and consecutive ones) of
     * all lengths in the sample stream should be
     * counted and stored.
     *
     * The test is passed if the number of runs that occur (of
     * lengths 1 through 6) is each within the corresponding
     * interval specified below.  This must hold for both the
     * zeros and ones; that is, all 12 counts must lie in the
     * specified interval.  For the purpose of this test, runs
     * of greater than 6 are considered to be of length 6.
     * 
     * Length of Run                Required Interval
     *       1                       2,267 - 2,733
     *       2                       1,079 - 1,421
     *       3                       502 - 748
     *       4                       223 - 402
     *       5                        90 - 223
     *       6+                       90 - 223
     */
    for (i4Count = 0; i4Count < 2; i4Count++)
    {
        if (!((2267 < Runs[i4Count][0]) && (Runs[i4Count][0] < 2733)))
        {
            PRINTF ("test 3 failed, bit=%d run=%d num=%lu\n",
                    i4Count, 1, Runs[i4Count][0]);
            i4Error++;
        }
        if (!((1079 < Runs[i4Count][1]) && (Runs[i4Count][1] < 1421)))
        {
            PRINTF ("test 3 failed, bit=%d run=%d num=%lu\n",
                    i4Count, 2, Runs[i4Count][1]);
            i4Error++;
        }
        if (!((502 < Runs[i4Count][2]) && (Runs[i4Count][2] < 748)))
        {
            PRINTF ("test 3 failed, bit=%d run=%d num=%lu\n",
                    i4Count, 3, Runs[i4Count][2]);
            i4Error++;
        }
        if (!((223 < Runs[i4Count][3]) && (Runs[i4Count][3] < 402)))
        {
            PRINTF ("test 3 failed, bit=%d run=%d num=%lu\n",
                    i4Count, 4, Runs[i4Count][3]);
            i4Error++;
        }
        if (!((90 < Runs[i4Count][4]) && (Runs[i4Count][4] < 223)))
        {
            PRINTF ("test 3 failed, bit=%d run=%d num=%lu\n",
                    i4Count, 5, Runs[i4Count][4]);
            i4Error++;
        }
        if (!((90 < Runs[i4Count][5]) && (Runs[i4Count][5] < 223)))
        {
            PRINTF ("test 3 failed, bit=%d run=%d num=%lu\n",
                    i4Count, 6, Runs[i4Count][5]);
            i4Error++;
        }
    }
    PRINT_RESULT ("Runs Test Successfully done\n");

    /* test 4 */
    /* The Long Run Test
     *
     * A long run is defined to be a run of length 34 or more
     * (of either zeros or ones).
     *
     * On the sample of 20,000 bits, the test is passed if
     * there are NO long runs.
     */
    if (Runs[0][33] != 0)
    {
        PRINTF ("test 4 failed, bit=%d run=%d num=%lu\n", 0, 34, Runs[0][33]);
        i4Error++;
    }
    if (Runs[1][33] != 0)
    {
        PRINTF ("test 4 failed, bit=%d run=%d num=%lu\n", 1, 34, Runs[1][33]);
        i4Error++;
    }
    PRINT_RESULT ("Long Run Test Successfully done\n");

    return (i4Error ? OSIX_FAILURE : OSIX_SUCCESS);
}

/****************************************************************************
                        End of File fipsrand.c
****************************************************************************/
