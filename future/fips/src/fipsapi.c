 /********************************************************************
 *  Copyright (C) 2008 Aricent Inc . All Rights Reserved             *
 *                                                                   *
 *  $Id: fipsapi.c,v 1.13 2017/09/12 13:26:43 siva Exp $              *
 *                                                                   *
 *  Description: This file contains the exported API of the FIPS     *
 *               module                                              *
 *                                                                   *
 *********************************************************************/
#ifndef __FIPSAPI_C_
#define __FIPSAPI_C_

#include "fipsinc.h"

extern INT4         gi4AuditStatus;
/***************************************************************/
/*  Function Name   : FipsApiSetDefaultFipsModeConfig          */
/*  Description     : This function is used to configure the   */
/*                    ISS to the configurations compliant to   */
/*                    to FIPS mode. This function will be      */
/*                    invoked at startup in FIPS mode          */
/*  Input(s)        : u1RestorationFlag - Flag that contains   */
/*                    the information about the Mib restoration*/
/*  Output(s)       : None                                     */
/*  Returns         : None                                     */
/***************************************************************/
VOID
FipsApiSetDefaultFipsModeConfig (UINT1 u1RestorationFlag)
{
    INT4                i4FipsOperMode = 0;
    UINT1               au1CurMode[AUDIT_LEN];
    UINT1               au1AuditLogMsg[AUDIT_LOG_SIZE];
#ifdef CLI_WANTED
    tCliSpecialPriv     sCliSpecialPriv;
    UINT4               u4Len = 0;

    MEMSET (au1AuditLogMsg, 0, AUDIT_LOG_SIZE);
    MEMSET (au1CurMode, 0, AUDIT_LEN);

    i4FipsOperMode = IssGetFipsOperModeFromNvRam ();
    if ((FIPS_MODE == i4FipsOperMode) || (CNSA_MODE == i4FipsOperMode))
    {
        /* Set the admin user as the user with special privilege. */
        MEMSET (&sCliSpecialPriv, 0, sizeof (sCliSpecialPriv));
        sCliSpecialPriv.b1UsernameBased = OSIX_TRUE;
        sCliSpecialPriv.b1PrivilegeBased = OSIX_FALSE;
        STRCPY (sCliSpecialPriv.au1UserName, FIPS_ROOT_USER);
        CliSetSpecialPriv (&sCliSpecialPriv);

        if (OSIX_FALSE == u1RestorationFlag)
        {
            FipsUtilDisableAllProtocols ();
        }

        CliTakeAppContext ();
        MGMT_UNLOCK ();
        CliExecuteAppCmd ("end");
        CliExecuteAppCmd ("configure terminal");
        CliExecuteAppCmd ("audit-logging enable");
        CliExecuteAppCmd ("set ip http disable");
        CliExecuteAppCmd ("no logging console");
        CliExecuteAppCmd ("no feature telnet");
        CliExecuteAppCmd ("end");
        CliGiveAppContext ();
        MGMT_LOCK ();

    }
    else                        /*Legacy mode default disabling the Telnet feature */
    {
        if (OSIX_FALSE == u1RestorationFlag)
        {
            CliTakeAppContext ();
            MGMT_UNLOCK ();
            CliExecuteAppCmd ("end");
            CliExecuteAppCmd ("configure terminal");
            CliExecuteAppCmd ("no feature telnet");
            CliExecuteAppCmd ("end");
            CliGiveAppContext ();
            MGMT_LOCK ();
        }
    }
#else
    UNUSED_PARAM (u1RestorationFlag);
#endif
    if (gi4AuditStatus == ISS_TRUE)
    {
#ifdef SNMP_3_WANTED
        /*Send log to Audit log file */
        u4Len = STRLEN ("EXT CRYPTO Init SUCCESS");
        STRNCPY (au1AuditLogMsg, "EXT CRYPTO Init SUCCESS", u4Len);
        au1AuditLogMsg[u4Len] = '\0';
        MsrAuditSendLogInfo (au1AuditLogMsg, AUDIT_CRITICAL_LEVEL);
#endif
        if (i4FipsOperMode == LEGACY_MODE)
        {
            STRNCPY (au1CurMode, "LEGACY", STRLEN ("LEGACY"));
        }
        else if (i4FipsOperMode == FIPS_MODE)
        {
            STRNCPY (au1CurMode, "FIPS", STRLEN ("FIPS"));
        }
        else if (i4FipsOperMode == CNSA_MODE)
        {
            STRNCPY (au1CurMode, "CNSA", STRLEN ("CNSA"));
        }
        /*Send log to Audit log file */
        SPRINTF ((CHR1 *) au1AuditLogMsg, "Switch in %s Oper Mode",
                 (CHR1 *) au1CurMode);
        MsrAuditSendLogInfo (au1AuditLogMsg, AUDIT_INFO_LEVEL);
    }
    return;
}

/***************************************************************/
/*  Function Name   : FipsApiSwitchFipsToLegacy               */
/*  Description     : This function is used to do the necessary*/
/*                    changes while switching from FIPS to     */
/*                    LEGACY compliant mode                  */
/*  Input(s)        : None                                     */
/*  Output(s)       : None                                     */
/*  Returns         : FIPS_FAILURE / FIPS_SUCCESS              */
/***************************************************************/
INT4
FipsApiSwitchFipsToLegacy (VOID)
{
    return (FipsUtilSwitchFipsToLegacy ());
}

/***************************************************************/
/*  Function Name   : FipsApiRunKnownAnsTest                   */
/*  Description     : This function is used to call FIPS util  */
/*                    util function to perform self-test       */
/*  Input(s)        : i4FipsTestAlgo - Algorithm to test       */
/*  Output(s)       : pi4ErrCode - Contains the value of faild */
/*                    algorithm                                */
/*  Returns         : FIPS_FAILURE / FIPS_SUCCESS              */
/***************************************************************/
INT4
FipsApiRunKnownAnsTest (INT4 i4FipsTestAlgo, INT4 *pi4ErrCode)
{
    return (FipsUtilRunKnownAnsTest (i4FipsTestAlgo, pi4ErrCode));
}

/****************************************************************************/
/*                                                                          */
/*    Function Name      : FipsRegisterEvtCallBk                            */
/*                                                                          */
/*    Description        : This function is to be invoked by application to */
/*                         register call back functions for required events */
/*                                                                          */
/*                         ISS protocols will call the function pointer     */
/*                         when this event occurs.                          */
/*                                                                          */
/*    Input(s)           : u4ModuleId - ISS module name                     */
/*                         u4Event - Event for which callback is registered */
/*                         pFsCbInfo - Pointer to call back information     */
/*                                                                          */
/*    Output(s)          : None.                                            */
/*                                                                          */
/*    Returns            : ISS_SUCCESS/ISS_FAILURE                          */
/****************************************************************************/

INT4
FipsRegisterEvtCallBk (UINT4 u4ModuleId, UINT4 u4Event, tFsCbInfo * pFsCbInfo)
{
    INT4                i4RetVal = FIPS_FAILURE;
    switch (u4ModuleId)
    {
#ifdef SSL_WANTED
        case ISS_SSL_MODULE:
            i4RetVal = SslRegisterCallBackforSSLModule (u4Event, pFsCbInfo);
            break;
#endif
#ifdef SSH_WANTED
        case ISS_SSH_MODULE:
            i4RetVal = SshRegisterCallBackforSSHModule (u4Event, pFsCbInfo);
            break;
#endif
#ifdef CLI_WANTED
        case ISS_CLI_MODULE:
            i4RetVal = CliUtilRegCallBackforCLIModule (u4Event, pFsCbInfo);
            break;
#endif
        default:
            break;
    }
    return i4RetVal;
}

/***************************************************************/
/*  Function Name   : FipsSetSwitchMode                        */
/*  Description     : This function is a wrapper function to   */
/*                    enable Switch mode in crypto library     */
/*  Input(s)        : None                                     */
/*  Output(s)       : None                                     */
/*  Returns         : FIPS_FAILURE / FIPS_SUCCESS              */
/***************************************************************/
INT4
FipsSetSwitchMode (INT4 i4FipsOperMode)
{
    UNUSED_PARAM (i4FipsOperMode);
#ifdef SSL_WANTED
    if (SslArSetFipsMode () != FIPS_SUCCESS)
#elif defined EXT_CRYPTO_WANTED
    if (ExtWrUtilSetSwitchMode (i4FipsOperMode) != FIPS_SUCCESS)
#endif
    {
        return FIPS_FAILURE;
    }

    return FIPS_SUCCESS;
}

/***************************************************************/
/*  Function Name   : FipsSelfTestRsa                          */
/*  Description     : This function is a wrapper function to   */
/*                    invoke FIPS supported RSA self test      */
/*  Input(s)        : None                                     */
/*  Output(s)       : None                                     */
/*  Returns         : FIPS_FAILURE / FIPS_SUCCESS              */
/***************************************************************/
INT4
FipsSelfTestRsa (VOID)
{
#ifdef SSL_WANTED
    if (SslArSelfTestRsa () != FIPS_SUCCESS)
    {
        return FIPS_FAILURE;
    }
#endif

    return FIPS_SUCCESS;
}

/***************************************************************/
/*  Function Name   : FipsSelfTestDsa                          */
/*  Description     : This function is a wrapper function to   */
/*                    invoke FIPS supported DSA self test      */
/*  Input(s)        : None                                     */
/*  Output(s)       : None                                     */
/*  Returns         : FIPS_FAILURE / FIPS_SUCCESS              */
/***************************************************************/
INT4
FipsSelfTestDsa (VOID)
{
#ifdef SSL_WANTED
    if (SslArSelfTestDsa () != FIPS_SUCCESS)
    {
        return FIPS_FAILURE;
    }
#endif

    return FIPS_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : FipsLock
 *
 * DESCRIPTION      : Function to take the mutual exclusion protocol
 *                    semaphore
 *
 * INPUT            : NONE
 *
 * OUTPUT           : NONE
 *
 * RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
FipsApiLock (VOID)
{
    if (OsixSemTake (FIPS_TASK_SEM_ID) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

/***************************************************************************
 * FUNCTION NAME    : FipsUnLock
 *
 * DESCRIPTION      : Function to release the mutual exclusion protocol
 *                    semaphore
 *
 * INPUT            : NONE
 *
 * OUTPUT           : NONE
 *
 * RETURNS          : SNMP_SUCCESS/SNMP_FAILURE
 *
 **************************************************************************/
PUBLIC INT4
FipsApiUnLock (VOID)
{
    if (OsixSemGive (FIPS_TASK_SEM_ID) == OSIX_FAILURE)
    {
        return SNMP_FAILURE;
    }
    return SNMP_SUCCESS;
}

#endif
