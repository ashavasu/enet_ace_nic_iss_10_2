/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsfipswr.c,v 1.5 2011/07/20 08:42:28 siva Exp $
*
* Description: Protocol Wrapper Routines
*********************************************************************/
#include "fipsinc.h"
#include "fsfipsdb.h"

VOID
RegisterFSFIPS ()
{
    SNMPRegisterMib (&fsfipsOID, &fsfipsEntry, SNMP_MSR_TGR_FALSE);
    SNMPAddSysorEntry (&fsfipsOID, (const UINT1 *) "fsfips");
}

VOID
UnRegisterFSFIPS ()
{
    SNMPUnRegisterMib (&fsfipsOID, &fsfipsEntry);
    SNMPDelSysorEntry (&fsfipsOID, (const UINT1 *) "fsfips");
}

INT4
FsFipsOperModeGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsFipsOperMode (&(pMultiData->i4_SLongValue)));
}

INT4
FsFipsTestAlgoGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsFipsTestAlgo (&(pMultiData->i4_SLongValue)));
}

INT4
FsfipsZeroizeCryptoKeysGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsfipsZeroizeCryptoKeys (&(pMultiData->i4_SLongValue)));
}

INT4
FsFipsTraceLevelGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsFipsTraceLevel (&(pMultiData->i4_SLongValue)));
}

INT4
FsFipsTestExecutionResultGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsFipsTestExecutionResult (&(pMultiData->i4_SLongValue)));
}

INT4
FsFipsFailedAlgorithmGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsFipsFailedAlgorithm (&(pMultiData->i4_SLongValue)));
}

INT4
FsFipsBypassCapabilityGet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhGetFsFipsBypassCapability (&(pMultiData->i4_SLongValue)));
}

INT4
FsFipsOperModeSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsFipsOperMode (pMultiData->i4_SLongValue));
}

INT4
FsFipsTestAlgoSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsFipsTestAlgo (pMultiData->i4_SLongValue));
}

INT4
FsfipsZeroizeCryptoKeysSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsfipsZeroizeCryptoKeys (pMultiData->i4_SLongValue));
}

INT4
FsFipsTraceLevelSet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsFipsTraceLevel (pMultiData->i4_SLongValue));
}

INT4
FsFipsBypassCapabilitySet (tSnmpIndex * pMultiIndex, tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhSetFsFipsBypassCapability (pMultiData->i4_SLongValue));
}

INT4
FsFipsOperModeTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsFipsOperMode (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsFipsTestAlgoTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                    tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsFipsTestAlgo (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsfipsZeroizeCryptoKeysTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                             tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsfipsZeroizeCryptoKeys
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsFipsTraceLevelTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                      tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsFipsTraceLevel (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsFipsBypassCapabilityTest (UINT4 *pu4Error, tSnmpIndex * pMultiIndex,
                            tRetVal * pMultiData)
{
    UNUSED_PARAM (pMultiIndex);
    return (nmhTestv2FsFipsBypassCapability
            (pu4Error, pMultiData->i4_SLongValue));
}

INT4
FsFipsOperModeDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsFipsOperMode (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsFipsTestAlgoDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                   tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsFipsTestAlgo (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsfipsZeroizeCryptoKeysDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                            tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsfipsZeroizeCryptoKeys
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsFipsTraceLevelDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                     tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsFipsTraceLevel (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}

INT4
FsFipsBypassCapabilityDep (UINT4 *pu4Error, tSnmpIndexList * pSnmpIndexList,
                           tSNMP_VAR_BIND * pSnmpvarbinds)
{
    return (nmhDepv2FsFipsBypassCapability
            (pu4Error, pSnmpIndexList, pSnmpvarbinds));
}
