#!/bin/csh
##############################################################################
# Copyright (C) 2011 Aricent Inc . All Rights Reserved
#
# $Id: make.h,v 1.8 2017/12/13 10:47:21 siva Exp $
#
# Description: Specifies the options and modules to be included for building 
#              the FIPS module 
##############################################################################
include ../LR/make.h
include ../LR/make.rule

# Set the PROJ_BASE_DIR as the directory where you untar the project files
PROJECT_NAME  = FutureFIPS
PROJECT_BASE_DIR = ${BASE_DIR}/fips
PROJECT_SOURCE_DIR = ${PROJECT_BASE_DIR}/src
PROJECT_INCLUDE_DIR = ${PROJECT_BASE_DIR}/inc
PROJECT_OBJECT_DIR = ${PROJECT_BASE_DIR}/obj
FUTURE_INC_DIR      = $(BASE_DIR)/inc
FUTURE_INC_CLI_DIR      = $(FUTURE_INC_DIR)/cli

# Specify the project include directories and dependencies
PROJECT_INCLUDE_FILES  = \
                         $(PROJECT_INCLUDE_DIR)/fsfipsdb.h       \
                         $(PROJECT_INCLUDE_DIR)/fsfipslw.h       \
                         $(PROJECT_INCLUDE_DIR)/fsfipswr.h       \
                         $(PROJECT_INCLUDE_DIR)/fipsglob.h       \
                         $(PROJECT_INCLUDE_DIR)/fipsprot.h       \
                         $(PROJECT_INCLUDE_DIR)/fipsinc.h        \
                         $(PROJECT_INCLUDE_DIR)/fipsdef.h        \
                         $(PROJECT_INCLUDE_DIR)/fipsextn.h       \
                         $(PROJECT_INCLUDE_DIR)/fipstrc.h

#### If any header files in FIPS moduleare required to build stubs, then those
#### files need to be included here.
PROJECT_STUB_INCLUDE_FILES = ${FUTURE_INC_DIR}/fips.h \
                             ${FUTURE_INC_DIR}/lr.h
                             

ifeq (${FIPS}, YES)
    PROJECT_FINAL_INCLUDE_FILES = $(PROJECT_INCLUDE_FILES)
    PROJECT_FINAL_INCLUDE_FILES    +=  $(FUTURE_INC_DIR)/fips.h 
    PROJECT_FINAL_INCLUDE_FILES    +=  $(FUTURE_INC_DIR)/iss.h 
    PROJECT_FINAL_INCLUDE_FILES    +=  $(FUTURE_INC_DIR)/msr.h 
    PROJECT_FINAL_INCLUDE_FILES    +=  $(FUTURE_INC_DIR)/cli.h 
    PROJECT_FINAL_INCLUDE_FILES    +=  $(FUTURE_INC_DIR)/cust.h 
    PROJECT_FINAL_INCLUDE_FILES    +=  $(FUTURE_INC_DIR)/iss.h
    PROJECT_FINAL_INCLUDE_FILES    +=  $(FUTURE_INC_CLI_DIR)/fipscli.h 

else
    PROJECT_FINAL_INCLUDE_FILES = $(PROJECT_STUB_INCLUDE_FILES)
endif

PROJECT_FINAL_INCLUDES_DIRS = -I$(PROJECT_INCLUDE_DIR)           \
                              -I$(COMMON_INCLUDE_DIRS)

# Specify the project level compilation switches here
PROJECT_COMPILATION_SWITCHES = 

PROJECT_DEPENDENCIES = $(COMMON_DEPENDENCIES)                    \
                       $(PROJECT_FINAL_INCLUDE_FILES)            \
                       $(PROJECT_BASE_DIR)/Makefile              \
                       $(PROJECT_BASE_DIR)/make.h

PROJECT_FINAL_OBJ = $(PROJECT_OBJECT_DIR)/$(PROJECT_NAME).o

PROJECT_FINAL_COMPILATION_SWITCHES = \
                                     $(GENERAL_COMPILATION_SWITCHES) \
                                     $(SYSTEM_COMPILATION_SWITCHES)  \
                                     ${PROJECT_COMPILATION_SWITCHES}

ifeq (${FIPS}, YES)
    PROJECT_FINAL_OBJECT_LIST = ${PROJECT_OBJECT_LIST}
else
    PROJECT_FINAL_OBJECT_LIST = ${PROJECT_STUB_OBJECT_LIST}
endif


