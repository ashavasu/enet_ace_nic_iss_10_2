####################################################
# Copyright (C) 2011 Aricent Inc . All Rights Reserved
#
# $Id: Makefile,v 1.20 2012/06/08 11:12:23 siva Exp $
#
# Description: Linux makefile for Security Kernel.
####################################################

CMN_INC_DIR   = /home/srinivas/CW-Sec/IPSecv6_kern/TOS_16_07_2011/code
LINUX_KERNEL_BASE_DIR := /usr/src/linux-`uname -r`

include ${CMN_INC_DIR}/future/LR/make.h
include ${CMN_INC_DIR}/future/LR/make.rule
include ${CMN_INC_DIR}/future/security/make.h
include ${CMN_INC_DIR}/future/nat/make.h.lk
include ${CMN_INC_DIR}/future/firewall/make.h.lk

ifeq (${VPN}, YES)
include  ${CMN_INC_DIR}/future/vpn/make.h.lk
include  ${CMN_INC_DIR}/future/ipsecv4/make.h.lk
endif


SYSTEM_COMPILATION1_SWITCHES = -DSMOD_WANTED -DSECURITY_KERNEL_WANTED -DEXTERNAL_MAIN_WANTED

KERNEL_MODULE_DIR = kbuild

FINAL_OUTPUT_DIR= ${CMN_INC_DIR}/future/security/KernelObj/

TARGET_IP  = LINUXIP

LINUX_INCLUDE = $(LINUX_KERNEL_BASE_DIR)/include -I/usr/include -I$(LINUX_KERNEL_BASE_DIR)/include/asm-x86/mach-default

FSAP2_SRC_DIR       = ../fsap2
IPV6_UTL_DIR        = ../util/ip6

ifeq (${FIREWALL}, YES)
FWL_SRC_DIR         = ../firewall
SYSTEM_COMPILATION1_SWITCHES += -DFIREWALL_WANTED
endif

ifeq (${NAT}, YES)
NAT_SRC_DIR         = ../nat
MICROSTACK_DIR      = $(NAT_SRC_DIR)/sip/microstack/sip_stack_3_1_src
MICROSTACK_INC_DIR  = $(CMN_INC_DIR)/future/nat/sip/microstack/sip_stack_3_1_src/stack_headers
MICRO_CMN_INC_DIR   = $(CMN_INC_DIR)/future/nat/sip/microstack/sip_stack_3_1_src/source/common/h
MICRO_ACC_INC_DIR   = $(CMN_INC_DIR)/future/nat/sip/microstack/sip_stack_3_1_src/source/accessor/h
MICRO_PAR_INC_DIR   = $(CMN_INC_DIR)/future/nat/sip/microstack/sip_stack_3_1_src/source/parser/h
MICRO_CLO_INC_DIR   = $(CMN_INC_DIR)/future/nat/sip/microstack/sip_stack_3_1_src/source/clone/h
SYSTEM_COMPILATION1_SWITCHES += -DNAT_WANTED
endif

ifeq (${VPN}, YES)
VPN_SRC_DIR         = ../vpn
SYSTEM_COMPILATION1_SWITCHES += -DVPN_WANTED
endif

ifeq (${IKE}, YES)
SYSTEM_COMPILATION1_SWITCHES += -DIKE_WANTED
endif

ifeq (${IPSECv4}, YES)
IPSECv4_SRC_DIR         = ../ipsecv4
DES_SRC_DIR         = ../util/des
SHA2_SRC_DIR         = ../util/sha2
AES_SRC_DIR         = ../util/aes
SHA1_SRC_DIR        = ../util/sha1
MD5_SRC_DIR         = ../util/md5
HMAC_SRC_DIR        = ../util/hmac
SYSTEM_COMPILATION1_SWITCHES += -DIPSECv4_WANTED
endif

ifeq (${IPSECv6}, YES)
IPSECv6_DIR = ../ipsecv6
UTIL_CMN_DIR        = ../util/cmn
SYSTEM_COMPILATION1_SWITCHES += -DIPSECv6_WANTED
endif

ifeq (${NAT}, YES)
GENERAL_COMPILATION_SWITCHES += -DNAT_ENH -DSIP_FSAP -DANSI_PROTO -DSIP_ALG_TRACE -D_GNU_SOURCE -DSIP_LINUX -D_REENTRANT -DSIP_BY_REFERENCE -DSIP_THREAD_SAFE -DSIP_OVERRIDE_SNPRINTF -DSIP_AUTHENTICATE  -DSIP_NO_CALLBACK  -DSIP_CORE_STACK_COMPATIBLE 
endif

# LINUX_KERNEL_VER is computed from the version.h file

LINUX_KERNEL_VER :=`grep LINUX_VERSION  ${LINUX_KERNEL_BASE_DIR}/include/linux/version.h | awk '{print $$3}'` 


############################################################################
##                     INCLUDE OPTIONS                                    ##
############################################################################

#KERNEL_SRC_DIR  = ${KERNEL_BASE_DIR}/src
#KERNEL_INC_DIR  = ${KERNEL_BASE_DIR}/inc
#KERNEL_OBJ_DIR  = ${KERNEL_BASE_DIR}/obj


KERNEL_FSAP_DIR     = ${FSAP_BASE_DIR}/lnxkern
KERNEL_FSAP_CMN_DIR     = ${FSAP_BASE_DIR}/cmn
KERNEL_FSAP_OBJ_DIR     = ${KERNEL_FSAP_DIR}
KERNEL_ISS_OBJ_DIR = ${ISS_COMMON_DIR}/system/obj
FLMGR_INC_DIR      = $(BASE_DIR)/flowmgr/inc
CFA_INC_DIR		= $(BASE_DIR)/cfa2/inc                              
GLOBAL_INCLUDES  = -I${KERNEL_FSAP_DIR} ${COMMON_INCLUDE_DIRS} -I${KERNEL_FSAP_CMN_DIR} -I${CFA_INC_DIR}

INCLUDES         = -I${SEC_INCLUDE_DIR} ${GLOBAL_INCLUDES} -I${LINUX_INCLUDE} -I${SWC_INC_DIR} -I${FLMGR_INC_DIR} ${PROJECT_FINAL_INCLUDES} -I$(MICRO_CMN_INC_DIR) -I$(MICRO_ACC_INC_DIR) -I$(MICRO_PAR_INC_DIR) -I$(MICRO_CLO_INC_DIR) -I$(MICROSTACK_INC_DIR)


#redefine CC_WARNING FLAGS, as -ansi flag should not be defined for KERNEL.
CC_WARNING_FLAGS             =  -fno-strict-aliasing          \
                                -Wall                         \
                                -Wunused                      \
                                -Wmissing-declarations        \
                                -Wimplicit                    \
                                -Wswitch                      \
                                -Wcast-qual                   \
                                -Wcast-align                  \
                                -Wshadow                      \
                                -Waggregate-return            \
                                -Wnested-externs              \
                                -Wmissing-prototypes          \
                                -fno-common                   \
                                -W


CC_FLAGS                     =    ${CC_OPTIMIZATION_FLAGS}   \
                                  ${CC_WARNING_FLAGS}


C_FLAGS += -DMODULE -DLINUX_KERN -D__KERNEL__
C_FLAGS += -D_GNU_SOURCE ${KFLAGS}

ifeq (${TARGET_OS}, OS_LINUX_PTHREADS)
SYSTEM_COMPILATION1_SWITCHES += -DOS_PTHREADS
endif

KERN_CFLAGS   = ${CC_FLAGS} ${C_FLAGS} ${INCLUDES} $(GENERAL_COMPILATION_SWITCHES) $(SYSTEM_COMPILATION1_SWITCHES) $(PROJECT_FINAL_INCLUDES_DIRS) $(VPN_FINAL_INCLUDE_DIR) $(IPSECv4_FINAL_INCLUDES_DIRS)

EXTRA_CFLAGS = ${KERN_CFLAGS} ${CFLAGS}

FINAL_OBJ_NAME=ISSSec
obj-m := $(FINAL_OBJ_NAME).o

# IKE_WANTED switch is undefined for lrmain coz enabling the switch spawns 
# IkeTaskMain in kernel and this functionality is not intended as IKE is a 
# user space UDP app.

CFLAGS_lrmain.o += -UIKE_WANTED
$(FINAL_OBJ_NAME)-y += ../LR/lrmain.o

$(FINAL_OBJ_NAME)-y += src/secprcs.o
$(FINAL_OBJ_NAME)-y += src/secport.o
$(FINAL_OBJ_NAME)-y += src/secmain.o
$(FINAL_OBJ_NAME)-y += src/seckstubs.o
$(FINAL_OBJ_NAME)-y += src/seckinit.o
$(FINAL_OBJ_NAME)-y += src/secsz.o
$(FINAL_OBJ_NAME)-y += src/secutil.o
$(FINAL_OBJ_NAME)-y += src/secdev.o

$(FINAL_OBJ_NAME)-y += ../cfa2/src/gddksec.o

$(FINAL_OBJ_NAME)-y += $(FSAP2_SRC_DIR)/cmn/fsutils.o
$(FINAL_OBJ_NAME)-y += $(FSAP2_SRC_DIR)/cmn/redblack.o
$(FINAL_OBJ_NAME)-y += $(FSAP2_SRC_DIR)/cmn/utldll.o
$(FINAL_OBJ_NAME)-y += $(FSAP2_SRC_DIR)/cmn/utlsll.o
$(FINAL_OBJ_NAME)-y += $(FSAP2_SRC_DIR)/lnxkern/srmmem.o
$(FINAL_OBJ_NAME)-y += $(FSAP2_SRC_DIR)/cmn/utlhash.o
$(FINAL_OBJ_NAME)-y += $(FSAP2_SRC_DIR)/lnxkern/osixwrap.o
$(FINAL_OBJ_NAME)-y += $(FSAP2_SRC_DIR)/lnxkern/osixlk.o
$(FINAL_OBJ_NAME)-y += $(FSAP2_SRC_DIR)/lnxkern/buflib.o
$(FINAL_OBJ_NAME)-y += $(FSAP2_SRC_DIR)/lnxkern/srmtmr.o
$(FINAL_OBJ_NAME)-y += $(FSAP2_SRC_DIR)/lnxkern/utltrc.o

ifeq (${NAT}, YES)
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/src/nattimer.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/src/natcusme.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/src/nath323.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/src/natrtsp.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/src/nathttp.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/src/natnfs.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/src/natdns.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/src/nathead.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/src/natmain.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/src/nattable.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/src/natcom.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/src/natftp.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/src/naticmp.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/src/natmemac.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/src/natllr.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/src/natipmul.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/src/natpptp.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/src/natexport.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/src/fsnatwr.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/src/fsnatlw.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/src/natike.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/src/natipsec.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/src/natutils.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/src/nattrig.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/src/natwkern.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/src/natdump.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/src/natapi.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/src/natkstub.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/src/napttmr.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/src/natsz.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/src/ntpolicy.o
$(FINAL_OBJ_NAME)-y += $(MICROSTACK_DIR)/source/common/src/microsip_portlayer.o
$(FINAL_OBJ_NAME)-y += $(MICROSTACK_DIR)/source/common/src/microsip_hash.o
$(FINAL_OBJ_NAME)-y += $(MICROSTACK_DIR)/source/common/src/microsip_list.o
$(FINAL_OBJ_NAME)-y += $(MICROSTACK_DIR)/source/common/src/microsip_init.o
$(FINAL_OBJ_NAME)-y += $(MICROSTACK_DIR)/source/common/src/microsip_free.o
$(FINAL_OBJ_NAME)-y += $(MICROSTACK_DIR)/source/common/src/microsip_bcptinit.o
$(FINAL_OBJ_NAME)-y += $(MICROSTACK_DIR)/source/common/src/microsip_bcptfree.o
$(FINAL_OBJ_NAME)-y += $(MICROSTACK_DIR)/source/common/src/microsip_trace.o
$(FINAL_OBJ_NAME)-y += $(MICROSTACK_DIR)/source/common/src/microsip_statistics.o

$(FINAL_OBJ_NAME)-y += $(MICROSTACK_DIR)/source/parser/src/microsip_subparsers.o
$(FINAL_OBJ_NAME)-y += $(MICROSTACK_DIR)/source/parser/src/microsip_decode.o
$(FINAL_OBJ_NAME)-y += $(MICROSTACK_DIR)/source/parser/src/microsip_encode.o
$(FINAL_OBJ_NAME)-y += $(MICROSTACK_DIR)/source/parser/src/microsip_hdrencoders.o
$(FINAL_OBJ_NAME)-y += $(MICROSTACK_DIR)/source/parser/src/microsip_decodeinternal.o
$(FINAL_OBJ_NAME)-y += $(MICROSTACK_DIR)/source/parser/src/microsip_encodeinternal.o
$(FINAL_OBJ_NAME)-y += $(MICROSTACK_DIR)/source/parser/src/microsip_hdrparsers.o
$(FINAL_OBJ_NAME)-y += $(MICROSTACK_DIR)/source/parser/src/microsip_timer.o
$(FINAL_OBJ_NAME)-y += $(MICROSTACK_DIR)/source/parser/src/microsip_sendmessage.o
$(FINAL_OBJ_NAME)-y += $(MICROSTACK_DIR)/source/parser/src/microsip_msgbodyparser.o

$(FINAL_OBJ_NAME)-y += $(MICROSTACK_DIR)/source/accessor/src/microsip_cloneintrnl.o
$(FINAL_OBJ_NAME)-y += $(MICROSTACK_DIR)/source/accessor/src/microsip_header.o
$(FINAL_OBJ_NAME)-y += $(MICROSTACK_DIR)/source/accessor/src/microsip_subapi.o
$(FINAL_OBJ_NAME)-y += $(MICROSTACK_DIR)/source/accessor/src/microsip_startline.o
$(FINAL_OBJ_NAME)-y += $(MICROSTACK_DIR)/source/accessor/src/microsip_msgbody.o
$(FINAL_OBJ_NAME)-y += $(MICROSTACK_DIR)/source/accessor/src/microsip_accessor.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/src/natsip.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/src/natsiptimer.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/sipalg/src/sipalg_microstack_callbacks.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/sipalg/src/sipalg_internal.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/sipalg/src/sipalg_hash.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/sipalg/src/sipalg_parser.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/sipalg/src/sipalg_utils.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/sipalg/src/sipalg_portlayer.o
$(FINAL_OBJ_NAME)-y += $(NAT_SRC_DIR)/sipalg/src/sipnat.o
endif

ifeq (${FIREWALL}, YES)
$(FINAL_OBJ_NAME)-y += $(FWL_SRC_DIR)/src/fwlinit.o  
$(FINAL_OBJ_NAME)-y += $(FWL_SRC_DIR)/src/fwlserv.o  
$(FINAL_OBJ_NAME)-y += $(FWL_SRC_DIR)/src/fwlif.o  
$(FINAL_OBJ_NAME)-y += $(FWL_SRC_DIR)/src/fwlbufif.o  
$(FINAL_OBJ_NAME)-y += $(FWL_SRC_DIR)/src/fwlinsp.o  
$(FINAL_OBJ_NAME)-y += $(FWL_SRC_DIR)/src/fwldbase.o  
$(FINAL_OBJ_NAME)-y += $(FWL_SRC_DIR)/src/fwlsnif.o 
$(FINAL_OBJ_NAME)-y += $(FWL_SRC_DIR)/src/fwlutil.o 
$(FINAL_OBJ_NAME)-y += $(FWL_SRC_DIR)/src/fwltmrif.o
$(FINAL_OBJ_NAME)-y += $(FWL_SRC_DIR)/src/fsfwllw.o
$(FINAL_OBJ_NAME)-y += $(FWL_SRC_DIR)/src/fwlblkwt.o
$(FINAL_OBJ_NAME)-y += $(FWL_SRC_DIR)/src/fwlstat.o  
$(FINAL_OBJ_NAME)-y += $(FWL_SRC_DIR)/src/fwlicmp.o  
$(FINAL_OBJ_NAME)-y += $(FWL_SRC_DIR)/src/fwltcp.o  
$(FINAL_OBJ_NAME)-y += $(FWL_SRC_DIR)/src/fwllimit.o  
$(FINAL_OBJ_NAME)-y += $(FWL_SRC_DIR)/src/fwldmz.o  
$(FINAL_OBJ_NAME)-y += $(FWL_SRC_DIR)/src/fwlurl.o  
$(FINAL_OBJ_NAME)-y += $(FWL_SRC_DIR)/src/fwllinks.o  
$(FINAL_OBJ_NAME)-y += $(FWL_SRC_DIR)/src/fwlwkern.o  
$(FINAL_OBJ_NAME)-y += $(FWL_SRC_DIR)/src/fwlsz.o
$(FINAL_OBJ_NAME)-y += $(FWL_SRC_DIR)/src/fwltftp.o
endif

$(FINAL_OBJ_NAME)-y += $(IPV6_UTL_DIR)/ip6util.o

ifeq (${VPN}, YES)
ifeq (${IKE}, YES)
#fsvpnlw.c for  kernel space will be compiled with IKE_WANTED=NO 
#as IKE will be in user space and KERNEL_WANTED=NO


# I am placing thie extra switch SECMOD_KERN_WANTED to avoid compilation issue
# for 2.6 kernels. This will be enabled only in case of Kernel .ko
# compilation, please refer code/future/vpn/src/fsvpnlw.c
# I have defined SECMOD_KERN_WANTED, This flag is not used any where else.
# This flag is used only in above mentioned file.
CFLAGS_fsvpnlw.o += -UIKE_WANTED
endif

$(FINAL_OBJ_NAME)-y += ${VPN_SRC_DIR}/src/vpnwkern.o
$(FINAL_OBJ_NAME)-y +=${VPN_SRC_DIR}/src/vpnkmain.o
$(FINAL_OBJ_NAME)-y += ${VPN_SRC_DIR}/src/vpnutils.o
$(FINAL_OBJ_NAME)-y += ${VPN_SRC_DIR}/src/fsvpnlw.o

ifeq (${IPSECv4}, YES)
$(FINAL_OBJ_NAME)-y += $(IPSECv4_SRC_DIR)/src/secv4init.o
$(FINAL_OBJ_NAME)-y += $(IPSECv4_SRC_DIR)/src/secv4vpn.o
$(FINAL_OBJ_NAME)-y += $(IPSECv4_SRC_DIR)/src/secv4conf.o
$(FINAL_OBJ_NAME)-y += $(IPSECv4_SRC_DIR)/src/secv4esp.o
$(FINAL_OBJ_NAME)-y += $(IPSECv4_SRC_DIR)/src/secv4ah.o
$(FINAL_OBJ_NAME)-y += $(IPSECv4_SRC_DIR)/src/secv4io.o
$(FINAL_OBJ_NAME)-y += $(IPSECv4_SRC_DIR)/src/secv4ip.o
$(FINAL_OBJ_NAME)-y += $(IPSECv4_SRC_DIR)/src/fssecv4lw.o
$(FINAL_OBJ_NAME)-y += $(IPSECv4_SRC_DIR)/src/secv4main.o
$(FINAL_OBJ_NAME)-y += $(IPSECv4_SRC_DIR)/src/secv4stat.o
$(FINAL_OBJ_NAME)-y += $(IPSECv4_SRC_DIR)/src/secv4tmr.o
$(FINAL_OBJ_NAME)-y += $(IPSECv4_SRC_DIR)/src/secv4prcfrg.o
$(FINAL_OBJ_NAME)-y += $(IPSECv4_SRC_DIR)/src/secv4util.o
$(FINAL_OBJ_NAME)-y += $(IPSECv4_SRC_DIR)/src/secv4kern.o

ifeq (${IKE}, YES)
$(FINAL_OBJ_NAME)-y += $(IPSECv4_SRC_DIR)/src/secv4ike.o
$(FINAL_OBJ_NAME)-y += $(IPSECv4_SRC_DIR)/src/secv4dyndb.o
endif

$(FINAL_OBJ_NAME)-y += $(IPSECv4_SRC_DIR)/src/secv4soft.o
$(FINAL_OBJ_NAME)-y += $(IPSECv4_SRC_DIR)/src/secv4port.o

##security algorithms
$(FINAL_OBJ_NAME)-y += $(DES_SRC_DIR)/src/desarapi.o
$(FINAL_OBJ_NAME)-y += $(DES_SRC_DIR)/src/desarfn.o
$(FINAL_OBJ_NAME)-y += $(AES_SRC_DIR)/src/aesarapi.o
$(FINAL_OBJ_NAME)-y += $(AES_SRC_DIR)/src/aesarfn.o
$(FINAL_OBJ_NAME)-y += $(MD5_SRC_DIR)/src/arMD5_hash.o
$(FINAL_OBJ_NAME)-y += $(MD5_SRC_DIR)/src/md5sec.o
$(FINAL_OBJ_NAME)-y += $(SHA1_SRC_DIR)/src/shaarfn.o
$(FINAL_OBJ_NAME)-y += $(SHA2_SRC_DIR)/src/usha.o
$(FINAL_OBJ_NAME)-y += $(SHA2_SRC_DIR)/src/sha224_256.o
$(FINAL_OBJ_NAME)-y += $(SHA2_SRC_DIR)/src/sha384_512.o
$(FINAL_OBJ_NAME)-y += $(HMAC_SRC_DIR)/src/arHmac_api.o
$(FINAL_OBJ_NAME)-y += $(HMAC_SRC_DIR)/src/arHmac_calc.o
endif  # IPSECv4_WANTED
endif  # VPN_WANTED

ifeq (${IPSECv6}, YES)
$(FINAL_OBJ_NAME)-y += ${IPSECv6_DIR}/secv6ah.o 
$(FINAL_OBJ_NAME)-y += ${IPSECv6_DIR}/secv6conf.o
$(FINAL_OBJ_NAME)-y += ${IPSECv6_DIR}/secv6stat.o
$(FINAL_OBJ_NAME)-y += ${IPSECv6_DIR}/secv6esp.o
$(FINAL_OBJ_NAME)-y += ${IPSECv6_DIR}/secv6init.o
$(FINAL_OBJ_NAME)-y += ${IPSECv6_DIR}/secv6io.o
$(FINAL_OBJ_NAME)-y += ${IPSECv6_DIR}/secv6low.o
$(FINAL_OBJ_NAME)-y += ${IPSECv6_DIR}/secv6main.o
$(FINAL_OBJ_NAME)-y += ${IPSECv6_DIR}/secv6tmr.o
$(FINAL_OBJ_NAME)-y += ${IPSECv6_DIR}/secv6util.o
$(FINAL_OBJ_NAME)-y += ${IPSECv6_DIR}/secv6sz.o
$(FINAL_OBJ_NAME)-y += ${IPSECv6_DIR}/sec6upor.o
$(FINAL_OBJ_NAME)-y += ${IPSECv6_DIR}/sec6kern.o
$(FINAL_OBJ_NAME)-y += ${UTIL_CMN_DIR}/fsutl.o 
$(FINAL_OBJ_NAME)-y += ${UTIL_CMN_DIR}/utilalgo.o
$(FINAL_OBJ_NAME)-y += ${IPSECv6_DIR}/../netip6/fsip6/ip6core/ip6usrke.o 
ifeq (${IKE}, YES)
$(FINAL_OBJ_NAME)-y += ${IPSECv6_DIR}/secv6ike.o
endif
ifeq (${VPN}, YES)
$(FINAL_OBJ_NAME)-y += ${IPSECv6_DIR}/secv6vpn.o
endif
endif

all:
	#make -w -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
#	make -w -C $(LINUX_KERNEL_BASE_DIR) M=`pwd` modules O=${CMN_INC_DIR}/future/security/KernelObj
	test -d $(FINAL_OUTPUT_DIR) || mkdir -p $(FINAL_OUTPUT_DIR);
	make -w -C $(LINUX_KERNEL_BASE_DIR) M=`pwd` modules
	mv -f *.o $(FINAL_OUTPUT_DIR);\
	mv -f *.ko $(FINAL_OUTPUT_DIR);
	cp -f $(FINAL_OUTPUT_DIR)/*.ko ../LR/
clean: cleanother
	make -w -C $(LINUX_KERNEL_BASE_DIR) M=`pwd` clean
	rm -rf $(FSAP2_SRC_DIR)/cmn/.*.cmd
	rm -rf $(FSAP2_SRC_DIR)/cmn/*.o
	rm -rf $(FSAP2_SRC_DIR)/lnxkern/.*.cmd
	rm -rf $(FSAP2_SRC_DIR)/lnxkern/*.o
	rm -rf ../LR/.*.cmd
	rm -rf ../LR/*.o
	rm -rf $(SECKRN_DIR)/modules.order
cleanother:
	rm -rf $(FSAP2_SRC_DIR)/cmn/.*.cmd
	rm -rf $(FSAP2_SRC_DIR)/cmn/*.o
	rm -rf $(IPV6_UTL_DIR)/*.o
	rm -rf $(IPV6_UTL_DIR)/.*.cmd
ifeq (${NAT}, YES)
	rm -rf $(NAT_SRC_DIR)/src/.*.cmd
	rm -rf $(NAT_SRC_DIR)/src/*.o
	rm -rf $(NAT_SRC_DIR)/sipalg/src/.*.cmd
	rm -rf $(NAT_SRC_DIR)/sipalg/src/*.o
	rm -rf $(MICRO_CMN_INC_DIR)/../src/*.o
	rm -rf $(MICRO_CMN_INC_DIR)/../src/.*.cmd
	rm -rf $(MICRO_ACC_INC_DIR)/../src/*.o
	rm -rf $(MICRO_ACC_INC_DIR)/../src/.*.cmd
	rm -rf $(MICRO_CLO_INC_DIR)/../src/*.o
	rm -rf $(MICRO_CLO_INC_DIR)/../src/.*.cmd
	rm -rf $(MICRO_PAR_INC_DIR)/../src/*.o
	rm -rf $(MICRO_PAR_INC_DIR)/../src/.*.cmd
	rm -rf $(NAT_SRC_DIR)/obj/*.o
endif
ifeq (${FIREWALL}, YES)
	rm -rf $(FWL_SRC_DIR)/src/.*.cmd
	rm -rf $(FWL_SRC_DIR)/src/*.o
	rm -rf $(FWL_SRC_DIR)/obj/*.o
endif
ifeq (${IPSECv4}, YES)
	rm -rf $(IPSECv4_SRC_DIR)/src/.*.cmd
	rm -rf $(IPSECv4_SRC_DIR)/src/*.o
	rm -rf $(IPSECv4_SRC_DIR)/obj/*.o
	rm -rf $(DES_SRC_DIR)/src/.*.cmd
	rm -rf $(DES_SRC_DIR)/src/*.o
	rm -rf $(DES_SRC_DIR)/obj/*.o
	rm -rf $(AES_SRC_DIR)/src/.*.cmd
	rm -rf $(AES_SRC_DIR)/src/*.o
	rm -rf $(AES_SRC_DIR)/obj/*.o
	rm -rf $(MD5_SRC_DIR)/src/.*.cmd
	rm -rf $(MD5_SRC_DIR)/src/*.o
	rm -rf $(MD5_SRC_DIR)/obj/*.o
	rm -rf $(SHA1_SRC_DIR)/src/.*.cmd
	rm -rf $(SHA1_SRC_DIR)/src/*.o
	rm -rf $(SHA1_SRC_DIR)/obj/*.o
	rm -rf $(HMAC_SRC_DIR)/src/.*.cmd
	rm -rf $(HMAC_SRC_DIR)/src/*.o
	rm -rf $(HMAC_SRC_DIR)/obj/*.o
endif
ifeq (${VPN}, YES)
	rm -rf $(VPN_SRC_DIR)/src/.*.cmd
	rm -rf $(VPN_SRC_DIR)/src/*.o
	rm -rf $(VPN_SRC_DIR)/obj/*.o
endif
ifeq (${IPSECv6}, YES)
	rm -rf $(IPSECv6_DIR)/.*.cmd
	rm -rf $(IPSECv6_DIR)/*.o
	rm -rf ${IPSECv6_DIR}/../netip6/fsip6/ip6core/*.o 
	rm -rf ${IPSECv6_DIR}/../netip6/fsip6/ip6core/.*.cmd
	rm -rf ${UTIL_CMN_DIR}/*.o 
	rm -rf ${UTIL_CMN_DIR}/.*.cmd 
endif

pkg:
	CUR_PWD=${PWD};\
        cd ${BASE_DIR}/../..;\
        tar cvzf ${ISS_PKG_CREATE_DIR}/security.tgz -T ${BASE_DIR}/security/FILES.NEW;\
        cd ${CUR_PWD};

