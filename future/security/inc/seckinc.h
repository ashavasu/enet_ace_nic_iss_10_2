/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: seckinc.h,v 1.2 2011/07/19 11:24:05 siva Exp $
 *
 * Description:This file contains the Required basic includes
 *             used accross  Security kernel  modules 
 *******************************************************************/

/* #include "kerninc.h" */
#include "lr.h"
#include "cfa.h"
#include "ip.h"
#include "secmod.h"
#include "arsec.h"
#include "secgen.h"
#include "seckgen.h"
#include "seckprot.h"
#include "osix.h"
#include "seckextn.h"
#include "secv4.h"
#include "secv6.h"
#include "firewall.h"
#include "vpn.h"
#include "nat.h"
#include "fsvlan.h"
#include "fwlwincs.h"
#include "natwincs.h"
#include "ipv6.h"
#ifdef VPN_WANTED
#include "vpnwincs.h"
#endif
#ifdef IPSECv4_WANTED
#include "secv4winc.h"
#endif
