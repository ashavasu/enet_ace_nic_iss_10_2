/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: secglob.h,v 1.9 2011/10/25 10:13:05 siva Exp $
 *
 * Description:This file contains the global variables
 *             used accross  Security module 
 *******************************************************************/

#ifndef _SECGLOB_H_
#define _SECGLOB_H_

extern tOsixSemId          gSecLock;
/* Related to IDS Logging */
UINT4                gu4IdsLogSize = IDS_MAX_LOG_FILESIZE;
UINT4                gu4IdsLogThreshold = IDS_DEFAULT_LOG_THRESHOLD;
CHR1                 gau1IdsLogFile [IDS_FILENAME_LEN];


tIDSGlobalInfo       gIDSGlobalInfo;
tIdsSockInfo         gIdsIssSockInfo;
tMemPoolId           gIdsPktBufPoolId = 0;
tOsixSemId           gIdsSemId = 0;
/* Global variable that controls the state of applying security to bridged packets.*/
UINT4                gu4SecBridgingStatus;

/* List of L2 VLANs to which security has to be applied. */
tSecVlanList        gSecvlanList;

/* Default L3 vlan index used for applying security for bridged packets. */
INT4                gi4SecIvrIfIndex;


tTMO_SLL gSecPppSessionInfoList; 
#endif /* _SECGLOB_H_ */
