#ifndef _FSSECWR_H
#define _FSSECWR_H

VOID RegisterFSSEC(VOID);

VOID UnRegisterFSSEC(VOID);
INT4 FsSecVersionInfoGet(tSnmpIndex *, tRetVal *);
INT4 FsSecDebugOptionGet(tSnmpIndex *, tRetVal *);
INT4 FsSecReloadIdsGet(tSnmpIndex *, tRetVal *);
INT4 FsSecDebugOptionSet(tSnmpIndex *, tRetVal *);
INT4 FsSecReloadIdsSet(tSnmpIndex *, tRetVal *);
INT4 FsSecDebugOptionTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSecReloadIdsTest(UINT4 *, tSnmpIndex *, tRetVal *);
INT4 FsSecDebugOptionDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
INT4 FsSecReloadIdsDep(UINT4 *, tSnmpIndexList *, tSNMP_VAR_BIND *);
#endif /* _FSSECWR_H */
