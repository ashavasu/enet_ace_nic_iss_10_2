/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secsz.h,v 1.1 2011/07/12 07:23:17 siva Exp $
 *
 * Description: This file contains mempool creation/deletion prototype
 *              declarations in Security module.
 *********************************************************************/
#ifndef _SECSZ_H_
#define _SECSZ_H_
enum {
    MAX_PPPOE_DEF_SESSION_SIZING_ID,
    SEC_MAX_SIZING_ID
};


#ifdef  _SECSZ_C
tMemPoolId SECMemPoolIds[ SEC_MAX_SIZING_ID];
INT4  SecSizingMemCreateMemPools(VOID);
VOID  SecSizingMemDeleteMemPools(VOID);
INT4  SecSzRegisterModuleSizingParams( CHR1 *pu1ModName);
#else  /*  _SECSZ_C  */
extern tMemPoolId SECMemPoolIds[ ];
extern INT4  SecSizingMemCreateMemPools(VOID);
extern VOID  SecSizingMemDeleteMemPools(VOID);
extern INT4  SecSzRegisterModuleSizingParams( CHR1 *pu1ModName); 
#endif /*  _SECSZ_C  */


#ifdef  _SECSZ_C
tFsModSizingParams FsSECSizingParams [] = {
{ "tSecPppSessionInfo", "MAX_PPPOE_DEF_SESSION", sizeof(tSecPppSessionInfo),MAX_PPPOE_DEF_SESSION, MAX_PPPOE_DEF_SESSION,0 },
{"\0","\0",0,0,0,0}
};
#else  /*  _SECSZ_C  */
extern tFsModSizingParams FsSECSizingParams [];
#endif /*  _SECSZ_C  */
#endif

