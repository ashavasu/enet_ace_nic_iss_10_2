/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: secextn.h,v 1.9 2011/10/25 10:13:05 siva Exp $
 *
 * Description:This file contains the exported variables
 *             used across  Security module 
 *******************************************************************/

#ifndef _SECEXTN_H_
#define _SECEXTN_H_

extern UINT4     gu4SecTrcFlag;
extern tSecIfInfo gaSecWanIfInfo[SYS_MAX_WAN_INTERFACES];
extern tSecIfInfo gaSecLanIfInfo[SYS_MAX_LAN_INTERFACES];

extern struct tasklet_struct gLrMainTasklet;
/* Global buffers for sending/Receiving data from user<->kernel space. 
 */
extern UINT1  gau1RxMsgBuf[SEC_MAX_MSG_BUF_SIZE];
extern UINT1  gau1TxMsgBuf[SEC_MAX_MSG_BUF_SIZE];
extern tIDSGlobalInfo       gIDSGlobalInfo;
extern tIdsSockInfo   gIdsIssSockInfo;
extern tMemPoolId     gIdsPktBufPoolId;
extern tOsixSemId     gIdsSemId;
extern tTMO_SLL gSecPppSessionInfoList;
extern INT4 CfaGddInit (VOID);
extern VOID SecMainInitGaModInfo (VOID);

extern VOID FwlGenerateMemFailureTrap  PROTO((UINT1 u1NodeName)); 
extern VOID FwlGenerateAttackSumTrap PROTO((VOID)); 
extern VOID IdsSendAttackPktTrap (UINT1 *pu1BlackListIpAddr);

extern UINT4 FsNpGetDummyVlanId (UINT4 u4IfIndex);
extern INT4 CfaIwfEnetProcessRxFrame (tCRU_BUF_CHAIN_HEADER * pCruBuf, 
                                      UINT4 u4IfIndex,
                                      UINT4 u4PktSize, UINT2 u2Protocol,
                                      UINT1 u1Direction);
/* Related to IDS Logging */
extern UINT4  gu4IdsLogSize;
extern UINT4  gu4IdsLogThreshold;
extern CHR1   gau1IdsLogFile [IDS_FILENAME_LEN];
/* Global variable that controls the state of applying security to bridged packets.*/
extern UINT4                gu4SecBridgingStatus;

/* List of L2 VLANs to which security has to be applied. */
extern tSecVlanList        gSecvlanList;

/* Default L3 vlan index used for applying security for bridged packets. */
extern INT4               gi4SecIvrIfIndex;


#endif /* _SECGLOB_H_ */
