/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fsseclw.h,v 1.1 2011/05/04 11:22:49 siva Exp $
*
* Description: Proto types for Low Level  Routines
*********************************************************************/

/* Proto type for Low Level GET Routine All Objects.  */

INT1
nmhGetFsSecVersionInfo ARG_LIST((tSNMP_OCTET_STRING_TYPE * ));

INT1
nmhGetFsSecDebugOption ARG_LIST((INT4 *));

INT1
nmhGetFsSecReloadIds ARG_LIST((INT4 *));

/* Low Level SET Routine for All Objects.  */

INT1
nmhSetFsSecDebugOption ARG_LIST((INT4 ));

INT1
nmhSetFsSecReloadIds ARG_LIST((INT4 ));

/* Low Level TEST Routines for.  */

INT1
nmhTestv2FsSecDebugOption ARG_LIST((UINT4 *  ,INT4 ));

INT1
nmhTestv2FsSecReloadIds ARG_LIST((UINT4 *  ,INT4 ));

/* Low Level DEP Routines for.  */

INT1
nmhDepv2FsSecDebugOption ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));

INT1
nmhDepv2FsSecReloadIds ARG_LIST((UINT4 *, tSnmpIndexList*, tSNMP_VAR_BIND*));
