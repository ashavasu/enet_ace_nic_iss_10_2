/********************************************************************
 * Copyright (C) 2011 Aricent Inc . All Rights Reserved
 *
 * $Id: seckern.h,v 1.1 2011/05/12 10:48:10 siva Exp $
 *
 * Description:This file contains the Macros defined for 
 *             the Security module
 *******************************************************************/

#ifndef _SECKERN_H_
#define _SECKERN_H_
#include <linux/kernel.h>

#define printf printk

extern INT4 BufInitManager (tBufConfig * pBufLibInitData);

extern UINT4 TmrTimerInit (tTimerCfg * pTimerCfg);


extern UINT1 IndexManagerUtilInit (VOID);

extern VOID CustomStartup (int argc, char *argv[]);


extern UINT4 OsixSetLocalTime (void);
extern INT4 TrieInit (void);

extern INT4 TrieLibInit (void);

extern INT4 TrieLibMemPoolInit (UINT4 u4Node);

extern VOID LrSetRestoreDefConfigFlag (UINT1 u1Status);
extern INT4 BufInitManager (tBufConfig * pBufLibInitData);

extern UINT4 TmrTimerInit (tTimerCfg * pTimerCfg);

extern VOID SysLogMsg (UINT4 u4Level, UINT4 u4ModuleId, CONST CHR1 * pFormat, ...);

extern UINT4 IssGetIpAddrFromNvRam (VOID);
extern UINT4 OsixGetTps (VOID);

extern UINT4 OsixInit (tOsixCfg * pOsixCfg);

extern VOID Fsap2Start (VOID);
#endif
