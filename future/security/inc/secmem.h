/********************************************************************
 * Copyright (C) 2006 Aricent Inc . All Rights Reserved
 *
 * $Id: secmem.h,v 1.1 2011/07/12 07:23:19 siva Exp $
 *
 * Description:This file contains macros related to MemPool.        
 *
 *******************************************************************/
#ifndef _SECMEM_H
#define _SECMEM_H
#include "lr.h"
/*****************************************************************************/
/*            Macros for defining respective PIDs (Pool Id)                  */
/*****************************************************************************/

#define PPPOE_DEF_SESSION_POOL_ID SECMemPoolIds[MAX_PPPOE_DEF_SESSION_SIZING_ID]

#endif
