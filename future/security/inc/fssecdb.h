/********************************************************************
* Copyright (C) 2006 Aricent Inc . All Rights Reserved
*
* $Id: fssecdb.h,v 1.1 2011/05/04 11:22:46 siva Exp $
*
* Description: Protocol Mib Data base
*********************************************************************/
#ifndef _FSSECDB_H
#define _FSSECDB_H


UINT4 fssec [] ={1,3,6,1,4,1,29601,2,64};
tSNMP_OID_TYPE fssecOID = {9, fssec};


UINT4 FsSecVersionInfo [ ] ={1,3,6,1,4,1,29601,2,64,1,1};
UINT4 FsSecDebugOption [ ] ={1,3,6,1,4,1,29601,2,64,1,2};
UINT4 FsSecReloadIds [ ] ={1,3,6,1,4,1,29601,2,64,1,3};




tMbDbEntry fssecMibEntry[]= {

{{11,FsSecVersionInfo}, NULL, FsSecVersionInfoGet, NULL, NULL, NULL, SNMP_DATA_TYPE_OCTET_PRIM, SNMP_READONLY, NULL, 0, 0, 0, ""},

{{11,FsSecDebugOption}, NULL, FsSecDebugOptionGet, FsSecDebugOptionSet, FsSecDebugOptionTest, FsSecDebugOptionDep, SNMP_DATA_TYPE_INTEGER32, SNMP_READWRITE, NULL, 0, 0, 0, "0"},

{{11,FsSecReloadIds}, NULL, FsSecReloadIdsGet, FsSecReloadIdsSet, FsSecReloadIdsTest, FsSecReloadIdsDep, SNMP_DATA_TYPE_INTEGER, SNMP_READWRITE, NULL, 0, 0, 0, NULL},
};
tMibData fssecEntry = { 3, fssecMibEntry };

#endif /* _FSSECDB_H */

