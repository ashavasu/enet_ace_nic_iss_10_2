/*****************************************************************************/
/* Copyright (C) 2011 Aricent Inc . All Rights Reserved                      */
/* Licensee Aricent Inc., 2001-2011                                          */
/*                                                                           */
/* $Id: secutil.c,v 1.22 2011/09/08 07:01:34 siva Exp $                        */
/*                                                                           */
/*  FILE NAME             : secutl.c                                         */
/*  PRINCIPAL AUTHOR      : Aricent Inc.                                     */
/*  SUBSYSTEM NAME        : Kern Module                                      */
/*  MODULE NAME           : KERN                                             */
/*  LANGUAGE              : C                                                */
/*  TARGET ENVIRONMENT    : Any                                              */
/*  DATE OF FIRST RELEASE : 16 Dec 2010                                      */
/*  AUTHOR                : Aricent Inc.                                     */
/*  DESCRIPTION           : This file contains Utility Functions required    */
/*                          for Security Kernel Module.                      */
/*****************************************************************************/

#ifndef _SECUTL_C_
#define _SECUTL_C_

#include "secinc.h"
#include "secextn.h"
#include "sectrc.h"
#include "ip6util.h"

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecGetDirectionFromMacAddress                    */
/*                                                                           */
/*    Description         : Utility function to Derive Direction             */
/*                                                                           */
/*                                                                           */
/*    Input(s)            : SrcMacAddress - Source MAC address.              */
/*                          DstMacAddress - Destination MAC address          */
/*                                                                           */
/*    Output(s)           : Direction, In Interface, Out Interface           */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : OSIX_SUCCESS if SrcMacAddress/Destination MAC     */
/*                         address is available in the security database.    */
/*****************************************************************************/

INT4
SecGetDirectionFromMacAddress (tMacAddr SrcMacAddress,
                               tMacAddr DstMacAddress, INT1 *pi1Dir)
{
    INT4                i4Index = 0;

    *pi1Dir = SEC_UNALLOCATED;

    for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
    {
        if (MEMCMP (SrcMacAddress, gaSecWanIfInfo[i4Index].MacAddress,
                    sizeof (tMacAddr)) == 0)
        {
            *pi1Dir = SEC_OUTBOUND;
            return OSIX_SUCCESS;
        }
        if (MEMCMP (DstMacAddress, gaSecWanIfInfo[i4Index].MacAddress,
                    sizeof (tMacAddr)) == 0)
        {
            *pi1Dir = SEC_INBOUND;
            return OSIX_SUCCESS;
        }
    }
    for (i4Index = 0; i4Index < SYS_MAX_LAN_INTERFACES; i4Index++)
    {
        if (MEMCMP (SrcMacAddress, gaSecLanIfInfo[i4Index].MacAddress,
                    sizeof (tMacAddr)) == 0)
        {
            *pi1Dir = SEC_OUTBOUND;
            return OSIX_SUCCESS;
        }
        if (MEMCMP (DstMacAddress, gaSecLanIfInfo[i4Index].MacAddress,
                    sizeof (tMacAddr)) == 0)
        {
            *pi1Dir = SEC_INBOUND;
            return OSIX_SUCCESS;
        }
    }

    return OSIX_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecUtilChkIsOurIpAddress                         */
/*                                                                           */
/*    Description         : Utility function to Check whether IP is of DUT.  */
/*                                                                           */
/*    Input(s)            : pCRUBuf  - Pointer to IP Packet Buffer           */
/*                                                                           */
/*    Output(s)           : None.                                            */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : SECMOD_SUCCESS / SECMOD_FAILURE                   */
/*                                                                           */
/*****************************************************************************/
INT4
SecUtilChkIsOurIpAddress (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    t_IP_HEADER         IPHdr;
    t_IP_HEADER        *pIPHdr = NULL;
    tIp6Hdr             Ip6Hdr;
    tIp6Hdr            *pIp6Hdr = NULL;
    tIp6Addr            Ip6IfAddr;
    INT4                i4Index = 0;
    UINT4               u4IpAddr = 0;
    UINT2               u2Protocol = 0;
    UINT1               u1Offset = CFA_VLAN_TAG_OFFSET;

    if (NULL == pBuf)
    {
        return OSIX_FAILURE;
    }

    CRU_BUF_Copy_FromBufChain (pBuf, ((UINT1 *) (&u2Protocol)), u1Offset,
                               sizeof (UINT2));

    if (CFA_VLAN_PROTOCOL_ID == u2Protocol)
    {
        u1Offset = CFA_VLAN_TAGGED_HEADER_SIZE;
        CRU_BUF_Copy_FromBufChain (pBuf, ((UINT1 *) (&u2Protocol)), u1Offset,
                                   sizeof (UINT2));
    }

    /* Get the offset of the IPv4/IPv6 header */
    if (u2Protocol == CFA_PPPOE_SESSION)
    {
        /* PPPoE HdrLen + Protocol inside pkt (IP) + VLAN tag and type len */
        u1Offset = PPPOE_FULL_HDR_LEN + sizeof (UINT2) + sizeof (UINT4);
    }
    else
    {
        u1Offset = u1Offset + sizeof (UINT2);
    }

    if ((CFA_ENET_IPV4 == u2Protocol) || (u2Protocol == CFA_PPPOE_SESSION))
    {
        /* Extract IPv4 header */
        pIPHdr = (t_IP_HEADER *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear (pBuf,
                                                                        u1Offset,
                                                                        CFA_IP_HDR_LEN);
        if (pIPHdr == NULL)
        {
            pIPHdr = &IPHdr;
            MEMSET (&IPHdr, 0, sizeof (t_IP_HEADER));
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &IPHdr, u1Offset,
                                       CFA_IP_HDR_LEN);
        }

        u4IpAddr = (OSIX_NTOHL (pIPHdr->u4Dest));

        for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
        {
            if (u4IpAddr == gaSecWanIfInfo[i4Index].u4IpAddr)
            {
                return OSIX_SUCCESS;
            }
        }

        for (i4Index = 0; i4Index < SYS_MAX_LAN_INTERFACES; i4Index++)
        {
            if (u4IpAddr == gaSecLanIfInfo[i4Index].u4IpAddr)
            {
                return OSIX_SUCCESS;
            }
        }
    }
    else if (CFA_ENET_IPV6 == u2Protocol)
    {
        /* Extract IPv6 header */
        pIp6Hdr =
            (tIp6Hdr *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear (pBuf, u1Offset,
                                                               IPV6_HEADER_LEN);
        if (pIp6Hdr == NULL)
        {
            pIp6Hdr = &Ip6Hdr;
            MEMSET (&Ip6Hdr, 0, sizeof (tIp6Hdr));
            CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) &Ip6Hdr,
                                       u1Offset, IPV6_HEADER_LEN);
        }

        MEMSET (&Ip6IfAddr, 0, sizeof (tIp6Addr));
        MEMCPY (&Ip6IfAddr, &pIp6Hdr->dstAddr, sizeof (tIp6Addr));

        for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
        {
            if (MEMCMP (&Ip6IfAddr, &gaSecWanIfInfo[i4Index].Ip6Addr,
                        sizeof (tIp6Addr)) == 0)
            {
                return OSIX_SUCCESS;
            }
        }

        for (i4Index = 0; i4Index < SYS_MAX_LAN_INTERFACES; i4Index++)
        {
            if (MEMCMP (&Ip6IfAddr, &gaSecLanIfInfo[i4Index].Ip6Addr,
                        sizeof (tIp6Addr)) == 0)
            {
                return OSIX_SUCCESS;
            }
        }
    }
    return OSIX_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name        : SecUtilExtractIpHdr
 *
 *    Description        : This function extracts the IP header from the input 
 *                         buffer and stores in pIp.
 *
 *    Input(s)            :   pIp - Pointer to the IP header.
 *                            pBuf -Pointer to the CRU buffer
 *
 *    Output(s)            : pIp - Extracted IP header.
 *
 *    Global Variables Referred : None
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : OSIX_SUCCESS /OSIX_FAILURE.
 *
 *****************************************************************************/
INT4
SecUtilExtractIpHdr (t_IP_HEADER * pIp, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    t_IP_HEADER        *pIpHdr = NULL;
    t_IP_HEADER         TmpIpHdr;
    UINT1               u1Tmp, u1Hlen;

    pIpHdr =
        (t_IP_HEADER *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear (pBuf, 0,
                                                               CFA_IP_HDR_LEN);
    if (pIpHdr == NULL)
    {

        /* The header is not contiguous in the buffer */
        pIpHdr = &TmpIpHdr;

        /* Copy the header */
        if (CRU_FAILURE == CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pIpHdr,
                                                      0, CFA_IP_HDR_LEN))
        {
            return OSIX_FAILURE;
        }
    }

    pIp->u1Ver_hdrlen = u1Tmp = pIpHdr->u1Ver_hdrlen;

    /* check version */
    if ((u1Tmp >> 4) != CFA_IP_V4)
    {
        return OSIX_FAILURE;
    }

    u1Hlen = (UINT1) ((u1Tmp & 0x0f) << 2);
    pIp->u1Tos = pIpHdr->u1Tos;
    pIp->u2Totlen = OSIX_NTOHS (pIpHdr->u2Totlen);

    /* check header and total lengths */
    if ((u1Hlen < CFA_IP_HDR_LEN) || (pIp->u2Totlen <= u1Hlen))
    {
        return OSIX_FAILURE;
    }

    pIp->u2Id = OSIX_NTOHS (pIpHdr->u2Id);
    pIp->u2Fl_offs = OSIX_NTOHS (pIpHdr->u2Fl_offs);
    pIp->u1Ttl = pIpHdr->u1Ttl;
    pIp->u1Proto = pIpHdr->u1Proto;
    pIp->u2Cksum = OSIX_NTOHS (pIpHdr->u2Cksum);

    MEMCPY (&pIp->u4Src, &pIpHdr->u4Src, sizeof (UINT4));
    pIp->u4Src = OSIX_NTOHL (pIp->u4Src);

    MEMCPY (&pIp->u4Dest, &pIpHdr->u4Dest, sizeof (UINT4));
    pIp->u4Dest = OSIX_NTOHL (pIp->u4Dest);

    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : SecUtilExtractIp6Hdr
 *
 *    Description        : This function extracts the IPv6 header from the input 
 *                         buffer and stores in pIp6Hdr.
 *
 *    Input(s)            :   pIp6 - Pointer to the IPv6 header.
 *                            pBuf - Pointer to the CRU buffer
 *
 *    Output(s)            : pIp6 - Extracted IP header.
 *
 *    Global Variables Referred : None
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : OSIX_SUCCESS /OSIX_FAILURE.
 *
 *****************************************************************************/
INT4
SecUtilExtractIp6Hdr (tIp6Hdr * pIp6, tCRU_BUF_CHAIN_HEADER * pBuf)
{
    tIp6Hdr            *pIp6Hdr = NULL;
    tIp6Hdr             TmpIp6Hdr;

    MEMSET (&TmpIp6Hdr, 0, sizeof (tIp6Hdr));

    pIp6Hdr =
        (tIp6Hdr *) (VOID *) CRU_BUF_Get_DataPtr_IfLinear (pBuf, 0,
                                                           IPV6_HEADER_LEN);
    if (pIp6Hdr == NULL)
    {
        /* The header is not contiguous in the buffer */
        pIp6Hdr = &TmpIp6Hdr;

        /* Copy the header */
        if (CRU_FAILURE == CRU_BUF_Copy_FromBufChain (pBuf, (UINT1 *) pIp6Hdr,
                                                      0, IPV6_HEADER_LEN))
        {
            return OSIX_FAILURE;
        }
    }

    MEMCPY (pIp6, pIp6Hdr, sizeof (tIp6Hdr));
    pIp6->u2Len = OSIX_NTOHS (pIp6->u2Len);

    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecUtilUpdateIfInfo                              */
/*                                                                           */
/*    Description         : Utility function to update interface Information.*/
/*                                                                           */
/*    Input(s)            : pIfInfo- Pointer to the tSecIfInfo structure.    */
/*                        : u4Command - Received command r device            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : OSIX_FAILURE or OSIX_SUCCESS                      */
/*                                                                           */
/*****************************************************************************/

INT4
SecUtilUpdateIfInfo (UINT4 u4Command, tSecIfInfo * pSecIfInfo)
{
    INT4                i4Index = 0;
    INT4                i4FreeIndex = SEC_UNALLOCATED;
    INT1                i1MatchFound = OSIX_FALSE;

    switch (u4Command)
    {
        case SEC_CFA_IF_CREATE:
            if (pSecIfInfo->u1NwType == CFA_NETWORK_TYPE_WAN)
            {
                for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
                {
                    if (gaSecWanIfInfo[i4Index].i4IfIndex == SEC_UNALLOCATED)
                    {
                        break;
                    }
                }
                if (i4Index == SYS_MAX_WAN_INTERFACES)
                {
                    return OSIX_FAILURE;
                }
                MEMCPY (&gaSecWanIfInfo[i4Index], pSecIfInfo,
                        sizeof (tSecIfInfo));
            }
            else
            {
                for (i4Index = 0; i4Index < SYS_MAX_LAN_INTERFACES; i4Index++)
                {
                    if (gaSecLanIfInfo[i4Index].i4IfIndex == SEC_UNALLOCATED)
                    {
                        break;
                    }
                }
                if (i4Index == SYS_MAX_LAN_INTERFACES)
                {
                    return OSIX_FAILURE;
                }
                MEMCPY (&gaSecLanIfInfo[i4Index], pSecIfInfo,
                        sizeof (tSecIfInfo));
            }
            break;

        case SEC_CFA_IF_DELETE:
            for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
            {
                if (gaSecWanIfInfo[i4Index].i4IfIndex == pSecIfInfo->i4IfIndex)
                {
                    MEMSET (&gaSecWanIfInfo[i4Index], 0, sizeof (tSecIfInfo));
                    gaSecWanIfInfo[i4Index].i4IfIndex = SEC_UNALLOCATED;
                    u4Command = SEC_CFA_IF_DELETE;
                    break;
                }
            }
            for (i4Index = 0; i4Index < SYS_MAX_LAN_INTERFACES; i4Index++)
            {
                if (gaSecLanIfInfo[i4Index].i4IfIndex == pSecIfInfo->i4IfIndex)
                {
                    MEMSET (&gaSecLanIfInfo[i4Index], 0, sizeof (tSecIfInfo));
                    gaSecLanIfInfo[i4Index].i4IfIndex = SEC_UNALLOCATED;
                    u4Command = SEC_CFA_IF_DELETE;
                    break;
                }
            }
            break;
        case SEC_CFA_IF_UPDATE:
            if (pSecIfInfo->u1NwType == CFA_NETWORK_TYPE_WAN)
            {
                for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
                {
                    if (gaSecWanIfInfo[i4Index].i4IfIndex ==
                        pSecIfInfo->i4IfIndex)
                    {
                        MEMCPY (&gaSecWanIfInfo[i4Index], pSecIfInfo,
                                sizeof (tSecIfInfo));
                        i1MatchFound = OSIX_TRUE;
                        break;
                    }
                    else if (SEC_UNALLOCATED ==
                             gaSecWanIfInfo[i4Index].i4IfIndex)
                    {
                        if (SEC_UNALLOCATED == i4FreeIndex)
                        {
                            i4FreeIndex = i4Index;
                        }
                    }
                }
                if (OSIX_TRUE == i1MatchFound)
                {
                    MEMCPY (&gaSecWanIfInfo[i4Index], pSecIfInfo,
                            sizeof (tSecIfInfo));
                }
                else
                {
                    if (SEC_UNALLOCATED != i4FreeIndex)
                    {
                        MEMCPY (&gaSecWanIfInfo[i4FreeIndex], pSecIfInfo,
                                sizeof (tSecIfInfo));
                    }
                }
            }
            else
            {
                for (i4Index = 0; i4Index < SYS_MAX_LAN_INTERFACES; i4Index++)
                {
                    if (gaSecLanIfInfo[i4Index].i4IfIndex ==
                        pSecIfInfo->i4IfIndex)
                    {
                        MEMCPY (&gaSecLanIfInfo[i4Index], pSecIfInfo,
                                sizeof (tSecIfInfo));
                        i1MatchFound = OSIX_TRUE;
                        break;
                    }
                    else if (SEC_UNALLOCATED ==
                             gaSecLanIfInfo[i4Index].i4IfIndex)
                    {
                        if (SEC_UNALLOCATED == i4FreeIndex)
                        {
                            i4FreeIndex = i4Index;
                        }
                    }
                }
                if (OSIX_TRUE == i1MatchFound)
                {
                    MEMCPY (&gaSecLanIfInfo[i4Index], pSecIfInfo,
                            sizeof (tSecIfInfo));
                }
                else
                {
                    if (SEC_UNALLOCATED != i4FreeIndex)
                    {
                        MEMCPY (&gaSecLanIfInfo[i4FreeIndex], pSecIfInfo,
                                sizeof (tSecIfInfo));
                    }
                }

            }
            break;

        case SEC_IP6_ADDR_ADD:
            if (pSecIfInfo->u1NwType == CFA_NETWORK_TYPE_WAN)
            {
                for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
                {
                    if (gaSecWanIfInfo[i4Index].i4IfIndex ==
                        pSecIfInfo->i4IfIndex)
                    {
                        MEMCPY (&gaSecWanIfInfo[i4Index].Ip6Addr,
                                &pSecIfInfo->Ip6Addr, sizeof (tIp6Addr));
                        break;
                    }
                }
            }
            else
            {
                for (i4Index = 0; i4Index < SYS_MAX_LAN_INTERFACES; i4Index++)
                {
                    if (gaSecLanIfInfo[i4Index].i4IfIndex ==
                        pSecIfInfo->i4IfIndex)
                    {
                        MEMCPY (&gaSecLanIfInfo[i4Index].Ip6Addr,
                                &pSecIfInfo->Ip6Addr, sizeof (tIp6Addr));
                        break;
                    }
                }
            }
            break;

        case SEC_IP6_ADDR_DEL:
            if (pSecIfInfo->u1NwType == CFA_NETWORK_TYPE_WAN)
            {
                for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
                {
                    if (gaSecWanIfInfo[i4Index].i4IfIndex ==
                        pSecIfInfo->i4IfIndex)
                    {
                        MEMSET (&gaSecWanIfInfo[i4Index].Ip6Addr, 0,
                                sizeof (tIp6Addr));
                        break;
                    }
                }
            }
            else
            {
                for (i4Index = 0; i4Index < SYS_MAX_LAN_INTERFACES; i4Index++)
                {
                    if (gaSecLanIfInfo[i4Index].i4IfIndex ==
                        pSecIfInfo->i4IfIndex)
                    {
                        MEMSET (&gaSecLanIfInfo[i4Index].Ip6Addr, 0,
                                sizeof (tIp6Addr));
                        break;
                    }
                }
            }
            break;

        default:
            return OSIX_FAILURE;

    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
*    Function Name            : SecUtilGetIfIndexFromVlanId
*
*    Description              :  Provides the interface having the given
*                                Vlan ID
*
*    Input(s)                  : u2VlanId - Vlan ID for which the
*                                interface index to be derived
*
*    Output(s)                 : Interface index.
*
*    Global Variables Referred : gaSecLanIfInfo/gaSecWanIfInfo
*
*    Returns                   : OSIX_SUCCESS/OSIX_FAILURE
*****************************************************************************/
INT4
SecUtilGetIfIndexFromVlanId (UINT2 u2VlanId, UINT4 *pu4CfaIfIndex)
{
    INT4                i4Index = 0;
    UINT4               u4PhyIfIndex = 0;

    for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
    {
        if (gaSecWanIfInfo[i4Index].u2VlanId == u2VlanId)
        {
            /* Get the underlying Physical Index, if the Interface is PPP */
            if (OSIX_SUCCESS == (SecUtilGetPhyIdxFromPPPIfdx
                                 ((UINT4) gaSecWanIfInfo[i4Index].i4IfIndex,
                                  &u4PhyIfIndex)))
            {
                *pu4CfaIfIndex = u4PhyIfIndex;
            }
            else
            {
                *pu4CfaIfIndex = (UINT4) gaSecWanIfInfo[i4Index].i4IfIndex;
            }
            return OSIX_SUCCESS;
        }
    }

    for (i4Index = 0; i4Index < SYS_MAX_LAN_INTERFACES; i4Index++)
    {
        if (gaSecLanIfInfo[i4Index].u2VlanId == u2VlanId)
        {
            *pu4CfaIfIndex = (UINT4) gaSecLanIfInfo[i4Index].i4IfIndex;
            return OSIX_SUCCESS;
        }
    }

    return OSIX_FAILURE;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecUtilUpdateEthHdrToModuleData                  */
/*                                                                           */
/*    Description         : Utility function to update module data           */
/*                                                                           */
/*    Input(s)            : pBuf -Pointer to the CRU buffer                  */
/*                          u2PID - Protocol Identifier Field                */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : OSIX_FAILURE or OSIX_SUCCESS                      */
/*                                                                           */
/*****************************************************************************/
INT4
SecUtilUpdateEthHdrToModuleData (tCRU_BUF_CHAIN_HEADER * pBuf,
                                 UINT2 *pu2L2HdrLen)
{
    UINT2               u2PID = 0;
    UINT1               u1Command;
    UINT4               u4IfIndex;
    UINT1              *pu1Buf = NULL;
    UINT1               au1Buf[SEC_MODULE_DATA_SIZE];
    tSecModuleData     *pSecModuleData = NULL;

    /* Get the Module Data from the CRU_BUFFER */

    if (NULL ==
        (pSecModuleData = (tSecModuleData *) CRU_BUF_Get_ModuleData (pBuf)))
    {
        return OSIX_FAILURE;
    }

    /* MEMSET all the ethernet header info in the Module data 
     * of the CRU_BUFFER */

    /* Back up the u1Command & u4IfIndex in module data before memset of 
     * entire structure */
    u1Command = pSecModuleData->u1Command;
    u4IfIndex = pSecModuleData->u4IfIndex;

    MEMSET (pSecModuleData, 0, sizeof (tSecModuleData));

    /* Restore the value of u1Command & u4IfIndex in module data */
    pSecModuleData->u1Command = u1Command;
    pSecModuleData->u4IfIndex = u4IfIndex;

    /* Make the local pu1Buf point to CRU_BUFFER, provided it is linear */

    pu1Buf =
        (UINT1 *) CRU_BUF_Get_DataPtr_IfLinear (pBuf, 0, SEC_MODULE_DATA_SIZE);

    if (NULL == pu1Buf)
    {
        /* CRU_BUFFER is not linear, so copy CRU_BUFFER ETHERNET HEADER contents 
         * to local buffer */

        pu1Buf = au1Buf;
        if (CRU_FAILURE == CRU_BUF_Copy_FromBufChain (pBuf, pu1Buf, 0,
                                                      SEC_MODULE_DATA_SIZE))
        {
            return OSIX_FAILURE;
        }
    }

    /* Populating Module data of the CRU_BUFFER using pu1Buf for */
    /* DestMACAddr, SrcMACAddr, u2LenOrType */

    MEMCPY (pSecModuleData->DestMACAddr, pu1Buf, CFA_ENET_ADDR_LEN);
    *pu2L2HdrLen = CFA_ENET_ADDR_LEN;

    MEMCPY (pSecModuleData->SrcMACAddr, pu1Buf + *pu2L2HdrLen,
            CFA_ENET_ADDR_LEN);
    *pu2L2HdrLen += CFA_ENET_ADDR_LEN;

    pSecModuleData->u2LenOrType =
        OSIX_NTOHS (*((UINT2 *) ((VOID *) pu1Buf + *pu2L2HdrLen)));
    *pu2L2HdrLen += CFA_VLAN_PROTOCOL_SIZE;

    u2PID = pSecModuleData->u2LenOrType;
    if (CFA_VLAN_PROTOCOL_ID == u2PID)
    {

        /* Support for single VLAN tags in Ethernet header */

        /* Populating Module data of the CRU_BUFFER using pu1Buf for */
        /* Vlan information */

        pSecModuleData->u2SVlanCtrlIdentifier =
            OSIX_NTOHS (*((UINT2 *) ((VOID *) pu1Buf + *pu2L2HdrLen)));
        *pu2L2HdrLen += CFA_VLAN_PROTOCOL_SIZE;

        /* Get the next two byte info from pu1Buf, if its VLAN identifier,
         * extract the VLAN info else extract the u2EthType info */

        u2PID = OSIX_NTOHS (*((UINT2 *) ((VOID *) pu1Buf + *pu2L2HdrLen)));
        *pu2L2HdrLen += CFA_VLAN_PROTOCOL_SIZE;

        if (CFA_VLAN_PROTOCOL_ID == u2PID)
        {
            /* Support for double VLAN tags in Ethernet header */

            /* Populating Module data of the CRU_BUFFER using pu1Buf for */
            /* Vlan information */

            pSecModuleData->u2CVlanCtrlIdentifier =
                OSIX_NTOHS (*((UINT2 *) ((VOID *) pu1Buf + *pu2L2HdrLen)));
            *pu2L2HdrLen += CFA_VLAN_PROTOCOL_SIZE;

            /* Get the next two byte info from pu1Buf, which is u2EthType,
             * store that in local variable u2PID, the local variable is used
             * so that updation in the Module data of the CRU_BUFFER related 
             * to u2EthType is done only in one place */

            u2PID = OSIX_NTOHS (*((UINT2 *) ((VOID *) pu1Buf + *pu2L2HdrLen)));
            *pu2L2HdrLen += CFA_VLAN_PROTOCOL_SIZE;
        }

    }

    /* Populating Module data of the CRU_BUFFER using pu1Buf for */
    /* u2EthType information if it is present, if not present then
     * the u2LenOrType is saved in u2EthType */

    pSecModuleData->u2EthType = u2PID;

    switch (u2PID)
    {

        case CFA_PPPOE_SESSION:

            /* Populating Module data of the CRU_BUFFER using pu1Buf for */
            /* PPPoE information */

            pSecModuleData->u1VerAndType = *((UINT1 *) (pu1Buf + *pu2L2HdrLen));
            *pu2L2HdrLen += sizeof (UINT1);

            pSecModuleData->u1Code = *((UINT1 *) (pu1Buf + *pu2L2HdrLen));
            *pu2L2HdrLen += sizeof (UINT1);

            pSecModuleData->u2SessionId =
                OSIX_NTOHS (*((UINT2 *) ((VOID *) pu1Buf + *pu2L2HdrLen)));
            *pu2L2HdrLen += sizeof (UINT2);

            pSecModuleData->u2Length =
                OSIX_NTOHS (*((UINT2 *) ((VOID *) pu1Buf + *pu2L2HdrLen)));
            *pu2L2HdrLen += sizeof (UINT2);

            pSecModuleData->u2PppProtocol =
                OSIX_NTOHS (*((UINT2 *) ((VOID *) pu1Buf + *pu2L2HdrLen)));
            *pu2L2HdrLen += sizeof (UINT2);

            break;

        case CFA_ENET_IPV4:
        case CFA_ENET_IPV6:
            break;

        default:
            return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecUtilPrependEthHdrToPkt                        */
/*                                                                           */
/*    Description         : Utility function to prepend Ethernet header to   */
/*                          packet                                           */
/*                                                                           */
/*    Input(s)            : pBuf -Pointer to the CRU buffer                  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : OSIX_FAILURE or OSIX_SUCCESS                      */
/*                                                                           */
/*****************************************************************************/

INT4
SecUtilPrependEthHdrToPkt (tCRU_BUF_CHAIN_HEADER * pBuf)
{
    UINT1               au1Buf[SEC_MODULE_DATA_SIZE];
    UINT2               u2PID = 0;
    UINT2               u2Offset = 0;
    tSecModuleData     *pSecModuleData = NULL;

    MEMSET (au1Buf, 0, SEC_MODULE_DATA_SIZE);

    /* Get the Module Data from the CRU_BUFFER */

    if (NULL ==
        (pSecModuleData = (tSecModuleData *) CRU_BUF_Get_ModuleData (pBuf)))
    {
        return OSIX_FAILURE;
    }

    /* Populating au1Buf temporary buffer using Module data of the CRU_BUFFER 
     * for DestMACAddr, SrcMACAddr, u2LenOrType */

    MEMCPY (au1Buf, pSecModuleData->DestMACAddr, CFA_ENET_ADDR_LEN);
    u2Offset = CFA_ENET_ADDR_LEN;

    if (u2Offset < SEC_MODULE_DATA_SIZE)
    {
        MEMCPY (&au1Buf[u2Offset], pSecModuleData->SrcMACAddr,
                CFA_ENET_ADDR_LEN);
        u2Offset += CFA_ENET_ADDR_LEN;
    }
    if (u2Offset < SEC_MODULE_DATA_SIZE)
    {
        *((UINT2 *) ((VOID *) &au1Buf[u2Offset])) =
            OSIX_HTONS (pSecModuleData->u2LenOrType);
        u2Offset += sizeof (UINT2);
    }
    u2PID = pSecModuleData->u2LenOrType;

    if (CFA_VLAN_PROTOCOL_ID == u2PID)
    {

        /* Populating au1Buf temporary buffer using Module data of the CRU_BUFFER 
         * for Vlan information */

        if (pSecModuleData->u2SVlanCtrlIdentifier)
        {
            /* Support for single Vlan tagging */
            if (u2Offset < SEC_MODULE_DATA_SIZE)
            {
                *((UINT2 *) ((VOID *) &au1Buf[u2Offset])) =
                    OSIX_HTONS (pSecModuleData->u2SVlanCtrlIdentifier);
                u2Offset += sizeof (UINT2);
            }
        }
        if (pSecModuleData->u2CVlanCtrlIdentifier)
        {
            /* Support for double Vlan tagging */
            if (u2Offset < SEC_MODULE_DATA_SIZE)
            {
                *((UINT2 *) ((VOID *) &au1Buf[u2Offset])) =
                    OSIX_HTONS (pSecModuleData->u2CVlanCtrlIdentifier);
                u2Offset += sizeof (UINT2);
            }
        }

        /* Populating au1Buf temporary buffer using Module data of the
         * CRU_BUF for u2EthType information */

        u2PID = pSecModuleData->u2EthType;
        if (u2Offset < SEC_MODULE_DATA_SIZE)
        {
            *((UINT2 *) ((VOID *) &au1Buf[u2Offset])) =
                OSIX_HTONS (pSecModuleData->u2EthType);
            u2Offset += sizeof (UINT2);
        }
    }

    switch (u2PID)
    {
        case CFA_PPPOE_SESSION:

            /* Populating au1Buf temporary buffer using Module data of the
             * CRU_BUF for PPPoE information */
            if (u2Offset < SEC_MODULE_DATA_SIZE)
            {
                au1Buf[u2Offset] = pSecModuleData->u1VerAndType;
                u2Offset += sizeof (UINT1);
            }
            if (u2Offset < SEC_MODULE_DATA_SIZE)
            {
                au1Buf[u2Offset] = pSecModuleData->u1Code;
                u2Offset += sizeof (UINT1);
            }
            if (u2Offset < SEC_MODULE_DATA_SIZE)
            {
                *((UINT2 *) ((VOID *) &au1Buf[u2Offset])) =
                    OSIX_HTONS (pSecModuleData->u2SessionId);
                u2Offset += sizeof (UINT2);
            }
            if (u2Offset < SEC_MODULE_DATA_SIZE)
            {
                *((UINT2 *) ((VOID *) &au1Buf[u2Offset])) =
                    OSIX_HTONS (pSecModuleData->u2Length);
                u2Offset += sizeof (UINT2);
            }
            if (u2Offset < SEC_MODULE_DATA_SIZE)
            {
                *((UINT2 *) ((VOID *) &au1Buf[u2Offset])) =
                    OSIX_HTONS (pSecModuleData->u2PppProtocol);
                u2Offset += sizeof (UINT2);
            }
            break;

        case CFA_ENET_IPV4:
        case CFA_ENET_IPV6:
            break;

        default:
            return OSIX_FAILURE;
    }

    /* au1Buf contains all the ethernet header information */

    /* Prepending CRU_BUFFER information with au1Buf temporary buffer,
     * hence the ethernet information in the CRU_BUFFER is restored */

    if (CRU_FAILURE == CRU_BUF_Prepend_BufChain (pBuf, au1Buf, u2Offset))
    {
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************
 *
 *    Function Name        : SecUtilGetIfInfo
 *
 *    Description          : This function returns the interface related params
 *                           assigned to this interface to the security module.
 *
 *    Input(s)             : u4IfIndex, *pIfInfo
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred :None
 *
 *    Global Variables Modified :None
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns            :OSIX_SUCCESS if u4IfIndex is valid
 *                        OSIX_FAILURE if u4IfIndex is Invalid
 *****************************************************************************/
INT4
SecUtilGetIfInfo (UINT4 u4IfIndex, tCfaIfInfo * pIfInfo)
{
    INT4                i4Index = 0;

    if (u4IfIndex > SYS_MAX_INTERFACES || u4IfIndex == 0)
    {
        SEC_TRC (SEC_FAILURE_TRC, "SecUtilGetIfInfo:"
                 "Interface Index is not valid !!!\r\n");
        return OSIX_FAILURE;
    }
    for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
    {
        if (gaSecWanIfInfo[i4Index].i4IfIndex == (INT4) u4IfIndex)
        {
            /* gaSeWancIfInfo[i4Index].u1IfNwType; */
            pIfInfo->u1WanType = gaSecWanIfInfo[i4Index].u1WanType;
            pIfInfo->u1NwType = gaSecWanIfInfo[i4Index].u1NwType;
            pIfInfo->u4IfMtu = gaSecWanIfInfo[i4Index].u4Mtu;
            pIfInfo->u1IfOperStatus = gaSecWanIfInfo[i4Index].u1OperStatus;
            pIfInfo->u1IfType = gaSecWanIfInfo[i4Index].u1IfType;
            STRCPY (pIfInfo->au1IfName, gaSecWanIfInfo[i4Index].au1InfName);
            return OSIX_SUCCESS;
        }
    }
    for (i4Index = 0; i4Index < SYS_MAX_LAN_INTERFACES; i4Index++)
    {
        if (gaSecLanIfInfo[i4Index].i4IfIndex == (INT4) u4IfIndex)
        {
            /* gaSecLanIfInfo[i4Index].u1IfNwType; */
            pIfInfo->u1WanType = gaSecLanIfInfo[i4Index].u1WanType;
            pIfInfo->u1NwType = gaSecLanIfInfo[i4Index].u1NwType;
            pIfInfo->u4IfMtu = gaSecLanIfInfo[i4Index].u4Mtu;
            pIfInfo->u1IfOperStatus = gaSecLanIfInfo[i4Index].u1OperStatus;
            pIfInfo->u1IfType = gaSecLanIfInfo[i4Index].u1IfType;
            STRCPY (pIfInfo->au1IfName, gaSecLanIfInfo[i4Index].au1InfName);
            return OSIX_SUCCESS;
        }
    }

    SEC_TRC_ARG1 (SEC_FAILURE_TRC, "SecUtilGetIfInfo:"
                  "Interface is not available %d !!!\r\n", u4IfIndex);;
    return OSIX_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name        : SecUtilGetIfTypeFromPhyIndex
 *
 *    Description          : This function returns the interface related params
 *                           assigned to this interface to the security module.
 *
 *    Input(s)             : u4IfIndex, *pu1IfType
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred :None
 *
 *    Global Variables Modified :None
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns            :OSIX_SUCCESS if u4IfIndex is valid
 *                        OSIX_FAILURE if u4IfIndex is Invalid
 *****************************************************************************/
INT4
SecUtilGetIfTypeFromPhyIndex (UINT4 u4IfIndex, UINT1 *pu1IfType)
{
    INT4                i4Index = 0;

    if (u4IfIndex > SYS_MAX_INTERFACES || u4IfIndex == 0)
    {
        SEC_TRC (SEC_FAILURE_TRC, "SecUtilGetIfTypeFromPhyIndex:"
                 "Interface Index is not valid !!!\r\n");
        return OSIX_FAILURE;
    }
    for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
    {
        if (gaSecWanIfInfo[i4Index].i4PhyIndex == (INT4) u4IfIndex)
        {
            *pu1IfType = gaSecWanIfInfo[i4Index].u1IfType;
            return OSIX_SUCCESS;
        }
    }

    SEC_TRC (SEC_FAILURE_TRC, "SecUtilGetIfTypeFromPhyIndex:"
             "Interface is not available !!!\r\n");
    return OSIX_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name       : SecUtilGetIfNwType
 *
 *    Description         : This function returns the interface Network type.
 *
 *    Input(s)            : u4IfIndex - interface index
 *
 *    Output(s)           : pu1IfNwType - Pointer to network type.
 *
 *    Global Variables Referred :None
 *
 *    Global Variables Modified :None
 *
 *    Exceptions or Operating
 *    System Error Handling     : None.
 *
 *    Use of Recursion          : None.
 *
 *    Returns             : OSIX_SUCCESS/OSIX_FAILURE.
 *****************************************************************************/
INT4
SecUtilGetIfNwType (UINT4 u4IfIndex, UINT1 *pu1IfNwType)
{
    INT4                i4Index = 0;

    if (u4IfIndex > SYS_MAX_INTERFACES || u4IfIndex == 0)
    {
        SEC_TRC (SEC_FAILURE_TRC, "SecUtilGetIfNwType:"
                 "Interface index is not valid !!!\r\n");
        return OSIX_FAILURE;
    }
    for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
    {
        if (gaSecWanIfInfo[i4Index].i4IfIndex == (INT4) u4IfIndex)
        {
            *pu1IfNwType = gaSecWanIfInfo[i4Index].u1NwType;
            return OSIX_SUCCESS;
        }
    }
    for (i4Index = 0; i4Index < SYS_MAX_LAN_INTERFACES; i4Index++)
    {
        if (gaSecLanIfInfo[i4Index].i4IfIndex == (INT4) u4IfIndex)
        {
            *pu1IfNwType = gaSecLanIfInfo[i4Index].u1NwType;
            return OSIX_SUCCESS;
        }
    }
    SEC_TRC_ARG1 (SEC_FAILURE_TRC, "SecUtilGetIfNwType:"
                  "interface is not available %d!!!\r\n", u4IfIndex);
    return OSIX_SUCCESS;
}

/**********************************************************************
 * Function           : SecUtilIpIfIsOurAddress 
 * Description        : This function used to takes an IP address and 
 *                      finds out if the address belongs to our host. 
 *                      All the address nodes mapped to the specified 
 *                      context are scanned for this purpose.
 *
 *
 * Input(s)           : u4Addr
 *
 * Output(s)          : None.
 *
 * Returns            : TRUE if IP_SUCCESS
 *                      FALSE otherwise.
 *
 ************************************************************************/
INT4
SecUtilIpIfIsOurAddress (UINT4 u4Addr)
{
    INT4                i4Index = 0;

    for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
    {
        if (gaSecWanIfInfo[i4Index].u4IpAddr == u4Addr)
        {
            return TRUE;
        }
    }
    for (i4Index = 0; i4Index < SYS_MAX_LAN_INTERFACES; i4Index++)
    {
        if (gaSecLanIfInfo[i4Index].u4IpAddr == u4Addr)
        {
            return TRUE;
        }
    }
    return FALSE;
}

/*****************************************************************************
 *
 *    Function Name      : SecUtilValidateIfIndex
 *
 *    Description        : Provides interface to the security module for
 *                         validation (interface exists and its CFA MIB's
 *                         ifMainRowStatus is ACTIVE) of the MIB-2 ifIndex
 *                         given to an interface.
 *
 *    Input(s)            : None.
 *
 *    Output(s)            : None.
 *
 *    Global Variables Referred : None.
 *
 *    Global Variables Modified : None
 *
 *    Exceptions or Operating
 *    System Error Handling    : None.
 *
 *    Use of Recursion        : None.
 *
 *    Returns            : OSIX_SUCCESS if the ifIndex is valid,
 *                otherwise OSIX_FAILURE.
 *****************************************************************************/
INT4
SecUtilValidateIfIndex (UINT4 u4IfIndex)
{
    INT4                i4Index = 0;

    if (u4IfIndex > SYS_MAX_INTERFACES || u4IfIndex == 0)
    {
        SEC_TRC (SEC_FAILURE_TRC, "SecUtilValidateIfIndex:"
                 "Interface index is not valid !!!\r\n");
        return OSIX_FAILURE;
    }

    for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
    {
        if (gaSecWanIfInfo[i4Index].i4IfIndex == (INT4) u4IfIndex)
        {
            return OSIX_SUCCESS;
        }
    }
    for (i4Index = 0; i4Index < SYS_MAX_LAN_INTERFACES; i4Index++)
    {
        if (gaSecLanIfInfo[i4Index].i4IfIndex == (INT4) u4IfIndex)
        {
            return OSIX_SUCCESS;
        }
    }
    SEC_TRC (SEC_FAILURE_TRC, "SecUtilValidateIfIndex:"
             "Interface is not available !!!\r\n");
    return OSIX_FAILURE;
}

/*****************************************************************************
*    Function Name            : SecUtilIpIfGetIfIndexFromIpAddress
*
*    Description              :  Provides the interface index having the given 
*                                IP Address
*
*    Input(s)                  : u4IpAddress - IP address for which the
*                                interface index to be derived
*
*    Output(s)                 :  Interface index.
*
*    Global Variables Referred : gIpIfInfo.pIpIfTable
*
*    Returns                   : OSIX_SUCCESS/OSIX_FAILURE
*****************************************************************************/
INT4
SecUtilIpIfGetIfIndexFromIpAddress (UINT4 u4IpAddress, UINT4 *pu4CfaIfIndex)
{
    INT4                i4Index = 0;

    for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
    {
        if (gaSecWanIfInfo[i4Index].u4IpAddr == u4IpAddress)
        {
            *pu4CfaIfIndex = (UINT4) gaSecWanIfInfo[i4Index].i4IfIndex;
            return OSIX_SUCCESS;
        }
    }

    for (i4Index = 0; i4Index < SYS_MAX_LAN_INTERFACES; i4Index++)
    {
        if (gaSecLanIfInfo[i4Index].u4IpAddr == u4IpAddress)
        {
            *pu4CfaIfIndex = (UINT4) gaSecLanIfInfo[i4Index].i4IfIndex;
            return OSIX_SUCCESS;
        }
    }

    SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecUtilIpIfGetIfIndexFromIpAddress:"
             "Interface not available !!!\r\n");
    return OSIX_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name       : SecUtilGetIfMtu
 *
 *    Description         : This function returns the interface MTU size.
 *
 *    Input(s)            : u4IfIdx - interface index
 *
 *    Output(s)           : pu4Mtu - Pointer to Mtu.
 *
 *    Returns             : OSIX_SUCCESS/OSIX_FAILURE.
 *****************************************************************************/
INT4
SecUtilGetIfMtu (UINT4 u4IfIdx, UINT4 *pu4Mtu)
{
    INT4                i4Index = 0;

    for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
    {
        if (gaSecWanIfInfo[i4Index].i4IfIndex == (INT4) u4IfIdx)
        {
            *pu4Mtu = gaSecWanIfInfo[i4Index].u4Mtu;
            return OSIX_SUCCESS;
        }
    }
    for (i4Index = 0; i4Index < SYS_MAX_LAN_INTERFACES; i4Index++)
    {
        if (gaSecLanIfInfo[i4Index].i4IfIndex == (INT4) u4IfIdx)
        {
            *pu4Mtu = gaSecLanIfInfo[i4Index].u4Mtu;
            return OSIX_SUCCESS;
        }
    }
    SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecUtilGetIfMtu:"
             "Interface not available !!!\r\n");
    return OSIX_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name       : SecUtilGetInterfaceNameFromIndex
 *
 *    Description         : This function returns the interface Name from Idx.
 *
 *    Input(s)            : u4IfIndex - interface index
 *
 *    Output(s)           : pu1Alias- Pointer to Interface Name.
 *
 *    Returns             : OSIX_SUCCESS/OSIX_FAILURE
 *****************************************************************************/
UINT4
SecUtilGetInterfaceNameFromIndex (UINT4 u4IfIndex, UINT1 *pu1Alias)
{
    INT4                i4Index = 0;

    if (u4IfIndex > SYS_MAX_INTERFACES || u4IfIndex == 0)
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecUtilGetInterfaceNameFromIndex:"
                 "Interface index is not valid !!!\r\n");
        return OSIX_FAILURE;
    }

    for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
    {
        if (gaSecWanIfInfo[i4Index].i4IfIndex == (INT4) u4IfIndex)
        {
            STRCPY (pu1Alias, gaSecWanIfInfo[i4Index].au1InfName);
            return OSIX_SUCCESS;
        }
    }

    for (i4Index = 0; i4Index < SYS_MAX_LAN_INTERFACES; i4Index++)
    {
        if (gaSecLanIfInfo[i4Index].i4IfIndex == (INT4) u4IfIndex)
        {
            STRCPY (pu1Alias, gaSecLanIfInfo[i4Index].au1InfName);
            return OSIX_SUCCESS;
        }
    }
    SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecUtilGetInterfaceNameFromIndex:"
             "Interface is not available !!!\r\n");
    return OSIX_FAILURE;
}

/*****************************************************************************/
/* Function Name      : SecUtilCheckIfLocalMac                               */
/*                                                                           */
/* Description        : This function checks whether the given Mac address   */
/*                      is self mac address                                  */
/*                                                                           */
/* Input(s)           : pHwAddr:Mac Address.                                 */
/*                      u4IfIndex :Interface Index                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS                                          */
/*****************************************************************************/
INT4
SecUtilCheckIfLocalMac (tEnetV2Header * pHwAddr, UINT4 u4IfIndex)
{
    INT4                i4Index = 0;

    for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
    {
        if (gaSecWanIfInfo[i4Index].i4IfIndex == (INT4) u4IfIndex)
        {
            if (MEMCMP (pHwAddr, gaSecWanIfInfo[i4Index].MacAddress,
                        sizeof (tMacAddr)) == 0)
            {
                return OSIX_SUCCESS;
            }
        }
    }

    for (i4Index = 0; i4Index < SYS_MAX_LAN_INTERFACES; i4Index++)
    {
        if (gaSecLanIfInfo[i4Index].i4IfIndex == (INT4) u4IfIndex)
        {
            if (MEMCMP (pHwAddr, gaSecLanIfInfo[i4Index].MacAddress,
                        sizeof (tMacAddr)) == 0)
            {
                return OSIX_SUCCESS;
            }
        }
    }

    SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecUtilCheckIfLocalMac:"
             "Interface is not available !!!\r\n");
    return OSIX_FAILURE;
}

/*****************************************************************************/
/* Function Name      : SecUtilIpifIsBroadCastLocal                          */
/*                                                                           */
/* Description        : This function checks whether the given address       */
/*                      is broad cast address of the any of the interfaces   */
/*                                                                           */
/* Input(s)           : pHwAddr:Mac Address.                                 */
/*                      u4IfIndex :Interface Index                           */
/* Output(s)          : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Referred           : None                                                 */
/*                                                                           */
/* Global Variables                                                          */
/* Modified           : None                                                 */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS                                          */
/*****************************************************************************/
INT4
SecUtilIpifIsBroadCastLocal (UINT4 u4Addr)
{
    INT4                i4Index;

    for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
    {
        if ((gaSecWanIfInfo[i4Index].u4BcastAddr == u4Addr) &&
            (gaSecWanIfInfo[i4Index].u4NetMask != CFA_31BIT_MASK_VALUE))
        {
            return OSIX_SUCCESS;
        }
    }

    for (i4Index = 0; i4Index < SYS_MAX_LAN_INTERFACES; i4Index++)
    {
        if ((gaSecLanIfInfo[i4Index].u4BcastAddr == u4Addr) &&
            (gaSecLanIfInfo[i4Index].u4NetMask != CFA_31BIT_MASK_VALUE))
        {
            return OSIX_SUCCESS;
        }
    }

    SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecUtilIpifIsBroadCastLocal:"
             "Interface is not available !!!\r\n");
    return OSIX_FAILURE;

}

/*-------------------------------------------------------------------+
 * Function           : SecUtilIpIsLocalNet 
 * *                     Checks whether IP belongs to Local Network
 *
 * Input(s)           : u4IpAddress - Ip Address
 * Output(s)          : pu4IfIndex - Returns the interface index
 *                      if the Ip belongs to Local network.
 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
 *
 * Action             : Checks whether IP belongs to Local Interface
------------------------------------------------------------------- */
INT4
SecUtilIpIsLocalNet (UINT4 u4IpAddress, UINT4 *pu4IfIndex)
{
    UINT4               u4IfIndex = 0;
    UINT4               u4Mask = 0;

    for (u4IfIndex = 0; u4IfIndex < SYS_MAX_WAN_INTERFACES; u4IfIndex++)
    {
        u4Mask = gaSecWanIfInfo[u4IfIndex].u4NetMask;
        /*
         * Check's whether the given IP Address & Mask
         * belong's to the local net or not.
         */
        if ((gaSecWanIfInfo[u4IfIndex].u4IpAddr & u4Mask) == 0)
        {
            /* Interfae does not have an IP address */
            continue;
        }
        if ((gaSecWanIfInfo[u4IfIndex].u4IpAddr & u4Mask) ==
            (u4IpAddress & u4Mask))
        {
            /* Interface Belongs to the same network */
            *pu4IfIndex = (UINT4) gaSecWanIfInfo[u4IfIndex].i4IfIndex;
            return NETIPV4_SUCCESS;
        }
    }

    for (u4IfIndex = 0; u4IfIndex < SYS_MAX_LAN_INTERFACES; u4IfIndex++)
    {
        u4Mask = gaSecLanIfInfo[u4IfIndex].u4NetMask;
        /*
         * Check's whether the given IP Address & Mask
         * belong's to the local net or not.
         */
        if ((gaSecLanIfInfo[u4IfIndex].u4IpAddr & u4Mask) == 0)
        {
            /* Interfae does not have an IP address */
            continue;
        }
        if ((gaSecLanIfInfo[u4IfIndex].u4IpAddr & u4Mask) ==
            (u4IpAddress & u4Mask))
        {
            /* Interface Belongs to the same network */
            *pu4IfIndex = (UINT4) gaSecLanIfInfo[u4IfIndex].i4IfIndex;
            return NETIPV4_SUCCESS;
        }
    }

    SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecUtilIpIsLocalNet:"
             "Interface is not available !!!\r\n");
    return NETIPV4_FAILURE;
}

/*-------------------------------------------------------------------+
 * Function           : Sec6UtilIpIsLocalNet 
 * *                     Checks whether v6 IP belongs to Local Network
 *
 * Input(s)           : Ip6Addr - Ip6 Address
 * Output(s)          : pu4IfIndex - Returns the interface index
 *                      if the v6 IP belongs to Local network.
 * Returns            : NETIPV4_SUCCESS or NETIPV4_FAILURE
 *
 * Action             : Checks whether IP belongs to Local Interface
------------------------------------------------------------------- */
INT4
Sec6UtilIpIsLocalNet (tIp6Addr * Ip6Addr, UINT4 *pu4IfIndex)
{
    UINT4               u4IfIndex = 0;
    UINT4               u4Mask = 0;

    for (u4IfIndex = 0; u4IfIndex < SYS_MAX_WAN_INTERFACES; u4IfIndex++)
    {
        u4Mask = gaSecWanIfInfo[u4IfIndex].u4NetMask;
        /*
         * Check's whether the given IP Address & Mask
         * belong's to the local net or not.
         */
        if (gaSecWanIfInfo[u4IfIndex].u4Ip6PrefixLen <= 0)
        {
            continue;
        }
        if (Ip6AddrMatch (&gaSecWanIfInfo[u4IfIndex].Ip6Addr, Ip6Addr,
                          gaSecWanIfInfo[u4IfIndex].u4Ip6PrefixLen) == TRUE)
        {
            /* Interface Belongs to the same network */
            *pu4IfIndex = (UINT4) gaSecWanIfInfo[u4IfIndex].i4IfIndex;
            return NETIPV6_SUCCESS;
        }
    }

    for (u4IfIndex = 0; u4IfIndex < SYS_MAX_LAN_INTERFACES; u4IfIndex++)
    {
        u4Mask = gaSecLanIfInfo[u4IfIndex].u4NetMask;
        /*
         * Check's whether the given IP Address & Mask
         * belong's to the local net or not.
         */
        if (gaSecLanIfInfo[u4IfIndex].u4Ip6PrefixLen <= 0)
        {
            continue;
        }
        if (Ip6AddrMatch (&gaSecLanIfInfo[u4IfIndex].Ip6Addr, Ip6Addr,
                          gaSecLanIfInfo[u4IfIndex].u4Ip6PrefixLen) == TRUE)
        {
            /* Interface Belongs to the same network */
            *pu4IfIndex = (UINT4) gaSecLanIfInfo[u4IfIndex].i4IfIndex;
            return NETIPV6_SUCCESS;
        }
    }

    SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecUtilIpIsLocalNet:"
             "Interface is not available !!!\r\n");
    return NETIPV6_FAILURE;
}

/** ---------------------------------------------------------------
 *   Function Name : SecUtlGetRandom
 *   Description   : Function to generate random number.
 *   Input(s)      : pu1Rand - pointer where the random number
 *                             has to be stored.
 *                   u4RandLen  - Required length of random number.
 *   Output(s)     : Random number of required length.
 *   Return Values : None.
 * --------------------------------------------------------*/

VOID
SecUtlGetRandom (UINT1 *pu1Rand, UINT4 u4RandLen)
{
    UINT4               u4Temp;
    UINT1              *pu1Temp = pu1Rand;

    while (u4RandLen > 4)
    {
        u4Temp = rand ();
        u4Temp = 0;
        MEMCPY (pu1Temp, &u4Temp, 4);
        pu1Temp += 4;
        u4RandLen -= 4;
    }

    if (u4RandLen > 0)
    {
        u4Temp = rand ();
        u4Temp = 0;
        MEMCPY (pu1Temp, &u4Temp, u4RandLen);
    }
    return;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecUtilAtoL                                      */
/*                                                                           */
/*    Description         : Utility function char string to long             */
/*                                                                           */
/*    Input(s)            : CHR1 array                                       */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            :   respective LONG value                           */
/*                                                                           */
/*****************************************************************************/

UINT4
SecUtilAtoL (CONST CHR1 * nptr)
{
    UINT4               total = 0;
    INT4                minus = 0;

    while (isspace (*nptr))
        nptr++;
    if (*nptr == '+')
        nptr++;
    else if (*nptr == '-')
    {
        minus = 1;
        nptr++;
    }
    while (isdigit (*nptr))
    {
        total *= 10;
        total += (*nptr++ - '0');
    }
    return minus ? -total : total;

}

/*****************************************************************************/
/* Function Name      : SecUtilSendDataPktToCfa                                     */
/*                                                                           */
/* Description        : This function is invoked from ids module to send     */
/*                      back the allowed packet to cfa.                      */
/*                                                                           */
/* Input(s)           : pBuf - data Frame                                    */
/*                      u4IfIndex - Interface Index                          */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS - On success.                           */
/*                      OSIX_FAILURE - On failure.                           */
/*****************************************************************************/

INT4
SecUtilSendDataPktToCfa (tCRU_BUF_CHAIN_HEADER * pBuf, UINT4 u4IfIndex)
{
    UINT4               u4PktSize = 0;
    UINT2               u2Protocol = 0;

    /* Set the command to SEC_TO_CFA since security is applied. 
     * on the packet. 
     */
    SEC_SET_COMMAND (pBuf, SEC_TO_CFA);

    /* If the packet is destined to ISS. */
    if (OSIX_SUCCESS == SecUtilChkIsOurIpAddress (pBuf))
    {
        if (CFA_FAILURE == CfaHandlePktFromSec (pBuf))
        {
            IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecUtilSendDataPktToCfa:"
                     "Unable to send the packet from security to CFA !!!\r\n");
            return OSIX_FAILURE;
        }
        return OSIX_SUCCESS;
    }
    else                        /* Packet has to be routed. */
    {
        /* Get the EthType as Ipv4/Ipv6/PPPoE */
        u2Protocol = (SEC_GET_MODULE_DATA_PTR (pBuf)->u2EthType);
        u4PktSize = CRU_BUF_Get_ChainValidByteCount (pBuf);

        if (CFA_FAILURE ==
            CfaIwfEnetProcessTxFrame (pBuf, u4IfIndex, NULL, u4PktSize,
                                      u2Protocol, CFA_ENCAP_ENETV2))
        {
            IDS_TRC (IDS_CONTROL_PLANE_TRC, "SecUtilSendDataPktToCfa:"
                     "Unable to send the packet to CFA for transmission !!!\r\n");
            return OSIX_FAILURE;
        }
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecGetSysOperMode                                */
/*                                                                           */
/*    Description         : API used for fetching the gi4SysOperMode flag    */
/*                                                                           */
/*    Input(s)            : None.                                            */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : gi4SysOperMode                             */
/*                                                                           */
/*    Global Variables Modified : None.                                      */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : gi4SysOperMode                                    */
/*                                                                           */
/*****************************************************************************/
INT4
SecGetSysOperMode (VOID)
{
    return gi4SysOperMode;
}

/*****************************************************************************/
/*                                                                           */
/*    Function Name       : SecSetSysOperMode                                */
/*                                                                           */
/*    Description         : API used to set the SysOperMode flag.            */
/*                                                                           */
/*    Input(s)            : i4OperMode - Value  assigned to gi4SysOperMode.  */
/*                                                                           */
/*    Output(s)           : None                                             */
/*                                                                           */
/*    Global Variables Referred : None.                                      */
/*                                                                           */
/*    Global Variables Modified : gi4SysOperMode                             */
/*                                                                           */
/*    Exceptions or Operating                                                */
/*    System Error Handling    : None.                                       */
/*                                                                           */
/*    Use of Recursion        : None.                                        */
/*                                                                           */
/*    Returns            : None                                              */
/*                                                                           */
/*****************************************************************************/
VOID
SecSetSysOperMode (INT4 i4OperMode)
{
    gi4SysOperMode = i4OperMode;
    return;
}

/*****************************************************************************/
/* Function Name      : SecNotifyHigherLayerIntfStatus                       */
/*                                                                           */
/* Description        : This function is invoked to send the interface status*/
/*                      change information to NAT and Firewall modules.      */
/*                      (i) If u1Action is set to CFA_IF_DEL, NAT/Firewall   */
/*                       modules are are notified with interface delete trig */
/*                      (ii) Else update the NAT/Firewall database with the  */
/*                           updated interface parameters.                   */
/*                       (iii) If the WAN type of the interface has changed  */
/*                          from Public to Private, NAT/Firewall is notified */
/*                          with interface delete trigger.                   */
/*                      back the allowed packet to cfa.                      */
/*                                                                           */
/* Input(s)           : pIfInfo - Interface parameters.                      */
/*                      pIpInfo - Ip related parameters.                     */
/*                      u4IfIndex - Interface Index                          */
/*                      u1Action  - DELETE/UPDATE.                           */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : None.                                                */
/*****************************************************************************/
VOID
SecNotifyHigherLayerIntfStatus (tSecIfInfo * pIfInfo,
                                tIpConfigInfo * pIpInfo,
                                UINT4 u4IfIndex, UINT1 u1Action)
{

    /* Interface type changed from WAN to LAN. */
    if (u1Action == CFA_IF_DELETE)
    {
#ifdef FIREWALL_WANTED
        FwlHandleInterfaceIndication (u4IfIndex, DESTROY);
#endif
#ifdef NAT_WANTED
        if (NULL != pIpInfo)
        {
            NatHandleInterfaceIndication (u4IfIndex, pIpInfo->u4Addr, DESTROY);
        }
        else
        {
            NatHandleInterfaceIndication (u4IfIndex, 0, DESTROY);
        }
#endif
        return;

    }

#ifdef NAT_WANTED
    if (pIpInfo != NULL)
    {
        if (pIpInfo->u4Addr == 0)
        {
            NatHandleInterfaceIndication (u4IfIndex, pIpInfo->u4Addr,
                                          CREATE_AND_WAIT);
        }
        else
        {
            if ((pIfInfo->u1NwType == CFA_NETWORK_TYPE_WAN) &&
                (u1Action == CFA_IF_CREATE))
            {
                NatHandleInterfaceIndication (u4IfIndex, pIpInfo->u4Addr,
                                              CREATE_AND_GO);
            }
            else
            {
                NatHandleInterfaceIndication (u4IfIndex, pIpInfo->u4Addr,
                                              ACTIVE);
            }
        }
    }
#endif
    /* Interface updation */
#ifdef FIREWALL_WANTED
    if (pIfInfo->u1WanType == CFA_WAN_TYPE_PUBLIC)
    {
        FwlHandleInterfaceIndication (u4IfIndex, CREATE_AND_GO);
    }
    else
    {
        FwlHandleInterfaceIndication (u4IfIndex, DESTROY);
    }
#else
    UNUSED_PARAM (pIpInfo);
#endif

#if (!defined  NAT_WANTED) || (!defined FIREWALL_WANTED)
    UNUSED_PARAM (pIfInfo);
    UNUSED_PARAM (pIpInfo);
    UNUSED_PARAM (u4IfIndex);
    UNUSED_PARAM (u1Action);
#endif
    return;
}

/*****************************************************************************
*    Function Name            : SecUtilGetPhyIndexFromVlanIndex
*
*    Description              :  Provides the Physical Interface Index having given the
*                                L3 VLAN interface index
*
*    Input(s)                  : u4IfIndex - interface index for which the
*                                VLAN ID to be derived
*
*    Output(s)                 : VLAN ID 
*
*    Global Variables Referred : gaSecWanIfInfo/gaSecLanIfInfo
*
*    Returns                   : OSIX_SUCCESS/OSIX_FAILURE
*****************************************************************************/
INT4
SecUtilGetPhyIndexFromL3VlanIndex (UINT4 u4IfIndex, UINT4 *pu4PhyIfIndex)
{
    INT4                i4Index = 0;

    for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
    {
        if (gaSecWanIfInfo[i4Index].i4IfIndex == (INT4) u4IfIndex)
        {
            *pu4PhyIfIndex = gaSecWanIfInfo[i4Index].i4PhyIndex;
            return OSIX_SUCCESS;
        }
    }
    for (i4Index = 0; i4Index < SYS_MAX_LAN_INTERFACES; i4Index++)
    {
        if (gaSecLanIfInfo[i4Index].i4IfIndex == (INT4) u4IfIndex)
        {
            *pu4PhyIfIndex = gaSecLanIfInfo[i4Index].i4PhyIndex;
            return OSIX_SUCCESS;
        }
    }

    return OSIX_FAILURE;
}

/*****************************************************************************
*    Function Name            : SecUtilGetVlanIdFromIfIndex
*
*    Description              :  Provides the VLAN ID having given the
*                                interface index
*
*    Input(s)                  : u4IfIndex - interface index for which the
*                                VLAN ID to be derived
*
*    Output(s)                 : VLAN ID 
*
*    Global Variables Referred : gaSecIfInfo
*
*    Returns                   : OSIX_SUCCESS/OSIX_FAILURE
*****************************************************************************/
INT4
SecUtilGetVlanIdFromIfIndex (UINT4 u4IfIndex, UINT2 *pu2VlanId)
{
    INT4                i4Index = 0;

    for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
    {
        if (gaSecWanIfInfo[i4Index].i4IfIndex == (INT4) u4IfIndex)
        {
            *pu2VlanId = gaSecWanIfInfo[i4Index].u2VlanId;
            return OSIX_SUCCESS;
        }
    }
    for (i4Index = 0; i4Index < SYS_MAX_LAN_INTERFACES; i4Index++)
    {
        if (gaSecLanIfInfo[i4Index].i4IfIndex == (INT4) u4IfIndex)
        {
            *pu2VlanId = gaSecLanIfInfo[i4Index].u2VlanId;
            return OSIX_SUCCESS;
        }
    }

    return OSIX_FAILURE;
}

/*****************************************************************************
*    Function Name            : SecUtilGetVlanIdFromPhyIfIndex
*
*    Description              :  Provides the VLAN ID having given the
*                                interface index
*
*    Input(s)                  : u4IfIndex - interface index for which the
*                                VLAN ID to be derived
*
*    Output(s)                 : VLAN ID 
*
*    Global Variables Referred : gaSecIfInfo
*
*    Returns                   : OSIX_SUCCESS/OSIX_FAILURE
*****************************************************************************/
INT4
SecUtilGetVlanIdFromPhyIfIndex (UINT4 u4PhyIfIndex, UINT2 *pu2VlanId)
{
    INT4                i4Index = 0;

    for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
    {
        if (gaSecWanIfInfo[i4Index].i4PhyIndex == (INT4) u4PhyIfIndex)
        {
            *pu2VlanId = gaSecWanIfInfo[i4Index].u2VlanId;
            return OSIX_SUCCESS;
        }
    }

    return OSIX_FAILURE;
}

/*****************************************************************************/
/* Function Name      : SecUtilPPPoEAddNewSession                            */
/*                                                                           */
/* Description        : This function is invoked from PPP module to add PPP  */
/*                      session info in security database.                   */
/*                                                                           */
/* Input(s)           : pSecPppSessionInfo - PPP session Info                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS - On success.                           */
/*                      OSIX_FAILURE - On failure.                           */
/*****************************************************************************/

INT4
SecUtilPPPoEAddNewSession (tSecPppSessionInfo * pSecPppSessionInfo)
{
    tSecPppSessionInfo *pNewSecPppSessionNode = NULL;
    tSecPppSessionInfo *pCurrSecPppSessionNode = NULL;
    tSecPppSessionInfo *pPrevSecPppSessionNode = NULL;

    /* Allocate memory for the tSecPppSessionInfo entry. */
    pNewSecPppSessionNode =
        (tSecPppSessionInfo *) MemAllocMemBlk (PPPOE_DEF_SESSION_POOL_ID);
    if (NULL == pNewSecPppSessionNode)
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecUtilPPPoEAddNewSession:"
                 "pNewSecPppSessionNode Alloc Failed\r\n");
        return OSIX_FAILURE;
    }

    pNewSecPppSessionNode->i4PPPIfIndex = pSecPppSessionInfo->i4PPPIfIndex;
    pNewSecPppSessionNode->i4IfIndex = pSecPppSessionInfo->i4IfIndex;
    pNewSecPppSessionNode->u2SessionId = pSecPppSessionInfo->u2SessionId;

    TMO_SLL_Scan (&gSecPppSessionInfoList, pCurrSecPppSessionNode,
                  tSecPppSessionInfo *)
    {
        if (pCurrSecPppSessionNode->i4IfIndex >
            pNewSecPppSessionNode->i4IfIndex)
        {
            break;
        }
        else
        {
            pPrevSecPppSessionNode = pCurrSecPppSessionNode;
        }
    }                            /* End of SLL_Scan */

    if (NULL == pCurrSecPppSessionNode)
    {
        TMO_SLL_Add (&gSecPppSessionInfoList,
                     &(pNewSecPppSessionNode->SecPppSessionNode));

    }
    else
    {
        TMO_SLL_Insert (&gSecPppSessionInfoList,
                        &(pPrevSecPppSessionNode->SecPppSessionNode),
                        &(pNewSecPppSessionNode->SecPppSessionNode));
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SecUtilPPPoEDelSession                               */
/*                                                                           */
/* Description        : This function is invoked from PPP module to delete   */
/*                      PPP session info in security database.               */
/*                                                                           */
/* Input(s)           : pSecPppSessionInfo - PPP session Info                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS - On success.                           */
/*                      OSIX_FAILURE - On failure.                           */
/*****************************************************************************/

INT4
SecUtilPPPoEDelSession (tSecPppSessionInfo * pSecPppSessionInfo)
{
    tSecPppSessionInfo *pSecPppSessionNode = NULL;

    TMO_SLL_Scan (&gSecPppSessionInfoList, pSecPppSessionNode,
                  tSecPppSessionInfo *)
    {
        if ((pSecPppSessionNode->i4PPPIfIndex ==
             pSecPppSessionInfo->i4PPPIfIndex)
            && (pSecPppSessionNode->i4IfIndex == pSecPppSessionInfo->i4IfIndex)
            && (pSecPppSessionNode->u2SessionId ==
                pSecPppSessionInfo->u2SessionId))
        {
            TMO_SLL_Delete (&gSecPppSessionInfoList,
                            &(pSecPppSessionNode->SecPppSessionNode));
            MemReleaseMemBlock (PPPOE_DEF_SESSION_POOL_ID,
                                (UINT1 *) pSecPppSessionNode);
            return OSIX_SUCCESS;
        }

    }                            /* End of SLL_Scan */

    SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecUtilPPPoEDelSession:"
             "PPP Session Info deletion failed\r\n");

    return OSIX_FAILURE;

}

/*****************************************************************************/
/* Function Name      : SecUtilPPPoEGetSessionInfo                           */
/*                                                                           */
/* Description        : This function is invoked from PPP module to get PPP  */
/*                      session info using sesiion id.                       */
/*                                                                           */
/* Input(s)           : pSecPppSessionInfo - PPP session Info                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS - On success.                           */
/*                      OSIX_FAILURE - On failure.                           */
/*****************************************************************************/

INT4
SecUtilPPPoEGetSessionInfo (tSecPppSessionInfo * pSecPppSessionInfo)
{
    tSecPppSessionInfo *pSecPppSessionNode = NULL;

    TMO_SLL_Scan (&gSecPppSessionInfoList, pSecPppSessionNode,
                  tSecPppSessionInfo *)
    {
        if ((pSecPppSessionNode->u2SessionId == pSecPppSessionInfo->u2SessionId)
            || (pSecPppSessionNode->i4IfIndex == pSecPppSessionInfo->i4IfIndex)
            || (pSecPppSessionNode->i4PPPIfIndex ==
                pSecPppSessionInfo->i4PPPIfIndex))
        {
            MEMCPY (pSecPppSessionInfo, pSecPppSessionNode,
                    sizeof (tSecPppSessionInfo));
            break;
        }
    }                            /* End of SLL_Scan */
    if (NULL == pSecPppSessionNode)
    {
        return OSIX_FAILURE;

    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SecUtilGetPppIdxFromPhyIfdx                          */
/*                                                                           */
/* Description        : This function is invoked from PPP module to get PPP  */
/*                      session info using sesiion id.                       */
/*                                                                           */
/* Input(s)           : pSecPppSessionInfo - PPP session Info                */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS - On success.                           */
/*                      OSIX_FAILURE - On failure.                           */
/*****************************************************************************/

INT4
SecUtilGetPppIdxFromPhyIfdx (UINT4 u4IfIndex, UINT4 *pu4PppIndex)
{
    tSecPppSessionInfo *pSecPppSessionNode = NULL;

    TMO_SLL_Scan (&gSecPppSessionInfoList, pSecPppSessionNode,
                  tSecPppSessionInfo *)
    {
        if (pSecPppSessionNode->i4IfIndex == (INT4) u4IfIndex)
        {
            *pu4PppIndex = pSecPppSessionNode->i4PPPIfIndex;
            break;
        }
    }                            /* End of SLL_Scan */
    if (NULL == pSecPppSessionNode)
    {
        return OSIX_FAILURE;

    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SecUtilGetPhyIdxFromPPPIfdx                          */
/*                                                                           */
/* Description        : This function is invoked from other modules to get   */
/*                      Physical index from PPP Index.                       */
/*                                                                           */
/* Input(s)           : u4PppIfIndex - PPP Index                             */
/*                                                                           */
/* Output(s)          : pu4Index - Physical Index.                           */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS - On success.                           */
/*                      OSIX_FAILURE - On failure.                           */
/*****************************************************************************/

INT4
SecUtilGetPhyIdxFromPPPIfdx (UINT4 u4PppIfIndex, UINT4 *pu4IfIndex)
{
    tSecPppSessionInfo *pSecPppSessionNode = NULL;

    TMO_SLL_Scan (&gSecPppSessionInfoList, pSecPppSessionNode,
                  tSecPppSessionInfo *)
    {
        if (pSecPppSessionNode->i4PPPIfIndex == (INT4) u4PppIfIndex)
        {
            *pu4IfIndex = pSecPppSessionNode->i4IfIndex;
            return OSIX_SUCCESS;
        }
    }                            /* End of SLL_Scan */
    return OSIX_FAILURE;
}

/*****************************************************************************/
/* Function Name      : SecUtilAddPppHdrToBuf                                */
/*                                                                           */
/* Description        : This function is invoked from cfa module to prepend  */
/*                      the output Buffer with PPP header information.       */
/*                                                                           */
/* Input(s)           : pCRUBuf - Output Buffer which is to prepended.       */
/*                      u4IfIndex - Physical index over which packet is sent */
/*                      au1DestHwAddr - Destination hardware address of      */
/*                      the packet to be transmitted.                        */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS - On success.                           */
/*                      OSIX_FAILURE - On failure.                           */
/*****************************************************************************/

INT4
SecUtilAddPppHdrToPkt (tCRU_BUF_CHAIN_HEADER * pCRUBuf, UINT4 u4IfIndex)
{
    UINT1              *pu1Buf = NULL;
    UINT1               au1Buf[PPPOE_FULL_HDR_LEN + CFA_ENET_V2_HEADER_SIZE];
    UINT2               u2L2HdrLen = 0;
    UINT2               u2Length = 0;
    tSecModuleData     *pSecModuleData = NULL;
    tSecPppSessionInfo  SecPppSessionInfo;

    MEMSET (au1Buf, 0, CFA_ENET_V2_HEADER_SIZE);
    MEMSET (&SecPppSessionInfo, 0, sizeof (tSecPppSessionInfo));

    if (NULL ==
        (pSecModuleData = (tSecModuleData *) CRU_BUF_Get_ModuleData (pCRUBuf)))
    {
        return OSIX_FAILURE;
    }

    /* Save the first CFA_ENET_V2_HEADER_SIZE(14) + VLAN Information(4) bytes.
     * in the local array au1Buf.
     */
    pu1Buf = au1Buf;
    if (CRU_FAILURE == CRU_BUF_Copy_FromBufChain (pCRUBuf, pu1Buf, 0,
                                                  (CFA_VLAN_TAGGED_HEADER_SIZE +
                                                   CFA_ENET_TYPE_OR_LEN)))
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecUtilAddPppHdrToPkt:"
                 "Failed to Copy ethernet header to array !!!\r\n");
        return OSIX_FAILURE;
    }

    if (CFA_VLAN_PROTOCOL_ID ==
        (*(UINT2 *) (VOID *) (au1Buf + CFA_VLAN_TAG_OFFSET)))
    {
        /* Move the 16 bytes which has vlan information */
        u2L2HdrLen = CFA_VLAN_TAG_OFFSET;
        u2L2HdrLen += sizeof (UINT4);
    }
    else
    {
        u2L2HdrLen = L2_ETH_TYPE_OFFSET;
    }

    if (CFA_PPPOE_SESSION == (*(UINT2 *) (VOID *) (au1Buf + u2L2HdrLen)))
    {
        return OSIX_SUCCESS;
    }

    /* Move the valid offset bytes to u2L2HdrLen bytes forward. */
    if (CRU_BUF_Move_ValidOffset (pCRUBuf, (u2L2HdrLen + CFA_ENET_TYPE_OR_LEN))
        != CRU_SUCCESS)
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecUtilAddPppHdrToPkt:"
                 "Failed to Move Valid Offset !!!\r\n");
        return OSIX_FAILURE;
    }

    *(UINT2 *) (VOID *) (au1Buf + u2L2HdrLen) = OSIX_HTONS (CFA_PPPOE_SESSION);
    u2L2HdrLen += sizeof (UINT2);

    au1Buf[u2L2HdrLen] = PPPOE_VER_AND_TYPE;
    u2L2HdrLen += sizeof (UINT1);

    au1Buf[u2L2HdrLen] = PPPOE_SESSION_CODE;
    u2L2HdrLen += sizeof (UINT1);

    SecPppSessionInfo.i4IfIndex = u4IfIndex;
    SecUtilPPPoEGetSessionInfo (&SecPppSessionInfo);
    *(UINT2 *) (VOID *) (au1Buf + u2L2HdrLen) = SecPppSessionInfo.u2SessionId;
    u2L2HdrLen += sizeof (UINT2);

    u2Length = CRU_BUF_Get_ChainValidByteCount (pCRUBuf);
    u2Length += sizeof (UINT2);
    *(UINT2 *) (VOID *) (au1Buf + u2L2HdrLen) = u2Length;
    u2L2HdrLen += sizeof (UINT2);

    *(UINT2 *) (VOID *) (au1Buf + u2L2HdrLen) = CFA_PPPOE_IP_PROTO;
    u2L2HdrLen += sizeof (UINT2);
    if (CRU_FAILURE == CRU_BUF_Prepend_BufChain (pCRUBuf, au1Buf, u2L2HdrLen))
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecUtilAddPppHdrToPkt:"
                 "Failed to Copy PPP information to the RX frame !!!\r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*****************************************************************************/
/* Function Name      : SecUtilStripPppHdrFromBuf                            */
/*                                                                           */
/* Description        : This function is invoked from cfa module to stripout */
/*                      PPP header information from Buffer.                  */
/*                                                                           */
/* Input(s)           : pCRUBuf - Output Buffer.                             */
/*                      u4IfIndex - Physical index over which packet is sent */
/*                                                                           */
/* Output(s)          : None.                                                */
/*                                                                           */
/* Return Value(s)    : OSIX_SUCCESS - On success.                           */
/*                      OSIX_FAILURE - On failure.                           */
/*****************************************************************************/

INT4
SecUtilStripPppHdrFromBuf (tCRU_BUF_CHAIN_HEADER * pCRUBuf, UINT4 u4IfIndex)
{
    UINT1              *pu1Buf = NULL;
    UINT1               au1Buf[CFA_ENET_V2_HEADER_SIZE];

    UNUSED_PARAM (u4IfIndex);
    MEMSET (au1Buf, 0, CFA_ENET_V2_HEADER_SIZE);

    /* Save the first CFA_ENET_V2_HEADER_SIZE(14) bytes.
     * in the local array au1Buf.
     */
    pu1Buf = au1Buf;
    if (CRU_FAILURE == CRU_BUF_Copy_FromBufChain (pCRUBuf, pu1Buf, 0,
                                                  (CFA_ENET_V2_HEADER_SIZE +
                                                   sizeof (UINT4))))
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecUtilStripPppHdrFromBuf:"
                 "Failed to Copy ethernet header to array !!!\r\n");
        return OSIX_FAILURE;
    }

    *(UINT2 *) (VOID *) (au1Buf + (L2_ETH_TYPE_OFFSET + sizeof (UINT4))) =
        OSIX_HTONS (CFA_ENET_IPV4);
    /* Move the valid offset bytes to 22 bytes forward. */
    if (CRU_BUF_Move_ValidOffset
        (pCRUBuf, (PPPOE_FULL_HDR_LEN + 2 + sizeof (UINT4))) != CRU_SUCCESS)
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecUtilStripPppHdrFromBuf:"
                 "Failed to Move Valid Offset !!!\r\n");
        return OSIX_FAILURE;
    }

    if (CRU_FAILURE ==
        CRU_BUF_Prepend_BufChain (pCRUBuf, au1Buf,
                                  (CFA_ENET_V2_HEADER_SIZE + sizeof (UINT4))))
    {
        SEC_TRC (SEC_CONTROL_PLANE_TRC, "SecUtilStripPppHdrFromBuf:"
                 "Failed to Delete PPP information to the TX frame !!!\r\n");
        return OSIX_FAILURE;
    }
    return OSIX_SUCCESS;
}

/*-------------------------------------------------------------------+
 * Function  Name     : Sec6UtilGetGlobalAddr
 *
 * Description        : This function fetches the IP6 Address
 *                      for the given interface
 *
 * Input(s)           : u4Index   - Interface Index
 * Output(s)          : pAddr6    - Ip6Address
 *
 * Returns            : tIp6Addr  - Ip6Address
------------------------------------------------------------------- */
tIp6Addr           *
Sec6UtilGetGlobalAddr (UINT4 u4IfIndex, tIp6Addr * pAddr6)
{
    UINT1               u1NwType = 0;
    INT4                i4Index = 0;

    UNUSED_PARAM (pAddr6);

    if (SecUtilGetIfNwType (u4IfIndex, &u1NwType) == OSIX_FAILURE)
    {
        return NULL;
    }

    if (u1NwType == CFA_NETWORK_TYPE_WAN)
    {
        for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
        {
            if (gaSecWanIfInfo[i4Index].i4IfIndex == (INT4) u4IfIndex)
            {
                return (&gaSecWanIfInfo[i4Index].Ip6Addr);
            }
        }
    }
    else
    {
        for (i4Index = 0; i4Index < SYS_MAX_LAN_INTERFACES; i4Index++)
        {
            if (gaSecLanIfInfo[i4Index].i4IfIndex == (INT4) u4IfIndex)
            {
                return (&gaSecLanIfInfo[i4Index].Ip6Addr);
            }
        }
    }

    return NULL;
}

/**********************************************************************
 * Function           : Sec6UtilIpIfIsOurAddress
 * Description        : This function takes  an IP6 address and
 *                       finds out if the address belongs to our host.
 *                       All the address nodes mapped to the specified
 *                       context are scanned for this purpose.
 *
 * Input(s)           : pIp6Addr - Ip6 Address
 *
 * Output(s)          : pu4Index -Interface index
 *
 * Returns            : OSIX_SUCCESS/OSIX_FAILURE.
 *
 ********************************************************************/
INT4
Sec6UtilIpIfIsOurAddress (tIp6Addr * pIp6Addr, UINT4 *pu4Index)
{
    INT4                i4Index = 0;

    for (i4Index = 0; i4Index < SYS_MAX_WAN_INTERFACES; i4Index++)
    {
        if (MEMCMP (pIp6Addr, &gaSecWanIfInfo[i4Index].Ip6Addr,
                    sizeof (tIp6Addr)) == 0)
        {
            *pu4Index = gaSecWanIfInfo[i4Index].i4IfIndex;
            return OSIX_SUCCESS;
        }
    }

    for (i4Index = 0; i4Index < SYS_MAX_LAN_INTERFACES; i4Index++)
    {
        if (MEMCMP (pIp6Addr, &gaSecLanIfInfo[i4Index].Ip6Addr,
                    sizeof (tIp6Addr)) == 0)
        {
            *pu4Index = gaSecLanIfInfo[i4Index].i4IfIndex;
            return OSIX_SUCCESS;
        }
    }

    return OSIX_FAILURE;
}

/*****************************************************************************
 *
 *    Function Name       : SecGetBrigdingStatus
 *
 *    Description         : This function outputs the security bridging status. 
 *
 *    Input(s)            : None.
 *
 *    Returns            :  Security bridging status. 
 *****************************************************************************/

UINT4
SecGetBrigdingStatus (VOID)
{
    return gu4SecBridgingStatus;
}

/*****************************************************************************
 *
 *    Function Name       : SecGetSecIvrIndex
 *
 *    Description         : This function outputs the security IVR index. 
 *
 *    Input(s)            : u2VlanId - VLAN identifier.
 *
 *    Returns            :  Security IVR index. 
 *****************************************************************************/
INT4
SecGetSecIvrIndex (VOID)
{
    return gi4SecIvrIfIndex;
}

/*****************************************************************************
 *
 *    Function Name       : SecUtilGetVlanId
 *
 *    Description         : This function checks if the given VLAN is member of 
 *                          security VLAN list.
 *
 *    Input(s)            : u2VlanId - VLAN identifier.
 *
 *    Returns            :  OSIX_TRUE if the input VLAN is security VLAN. 
 *                          Else, OSIX_FALSE.
 *****************************************************************************/
BOOL1
SecIsMemberOfSecVlanList (UINT2 u2VlanId)
{
    BOOL1               bResult;
    OSIX_BITLIST_IS_BIT_SET (gSecvlanList, u2VlanId,
                             sizeof (tSecVlanList), bResult);
    return bResult;
}

#endif /* _SECUTL_C_ */
